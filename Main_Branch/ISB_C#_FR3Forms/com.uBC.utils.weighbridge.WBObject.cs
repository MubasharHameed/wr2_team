﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.forms.oo;

namespace com.uBC.utils.weighbridge {
    public class WBObject : WBCommands{

        #region MemberVariables
        private string msID;
        #endregion

        #region Constructor
        public WBObject(string sID)
            : base(sID) {
                msID = sID;
                addHandlers();
                doInit();
        }
        #endregion

        #region Methods
        private void addHandlers() {
            doConfigure += new Configure(base.doConfigure);
            doReadWeightContinuous += new ReadContinuous(base.doReadContinuous);
            doReadWeightACC1 += new ReadACC1(base.doReadACC1);
            doReadWeightACC2 += new ReadACC2(base.doReadACC2);
            doReadWeightACCB += new ReadACCB(base.doReadACCB);
        }

        bool WBObject_doReadWeightContinuous() {
            throw new NotImplementedException();
        }


        private void doInit() {
            doConfigure();
        }
        public bool ReadWeightContinuous() {
            return doReadWeightContinuous();
        }
        public bool Configure() {
            return doConfigure();
        }
        public bool ReadWeightACC1() {
            return doReadACC1();
        }
        public bool ReadWeightACC2() {
            return doReadACC2();
        }
        public bool ReadWeightACCB() {
            return doReadACCB();
        }
        #endregion

        #region Handlers
       
        #endregion

        #region Delegates
        public delegate bool SetWBLogo();
        #endregion

        #region Events
        public event SetWBLogo doSetWBLogo;
        protected event Configure doConfigure;
        public event ReadContinuous doReadWeightContinuous;
        public event ReadACC1 doReadWeightACC1;
        public event ReadACC2 doReadWeightACC2;
        public event ReadACCB doReadWeightACCB;
        #endregion

    
   
    }
}
