﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.dbObjects.User;
using IDHAddOns.idh.controls;
using com.uBC.utils;
using com.idh.controls;
using com.idh.bridge.utils;

namespace com.uBC.forms.fr3.admin {
    public class TransactionCodes : com.uBC.forms.fr3.dbGrid.Base {

        public TransactionCodes(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
                mbIsSearch = false;
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDHTRCDAD", sParMenu, iMenuPos, "admin.srf", false, true, false, "Transaction Codes", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        protected override void doInitialize() {
            moDBObject = new IDH_TRANCD();
        }

#region FormOpenCreateFunctions
        /*
         * Do Complete the Form
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            base.doCompleteCreate(ref BubbleEvent);
        }

       /* 
        * Do the final form steps to show before loaddata
        */
        public override void doBeforeLoadData() {
            base.doBeforeLoadData();
        }

        /* 
         * Load the Form Data
         */
        public override void doLoadData() {
            base.doLoadData();
            doFillCombos();
        }

        /*
         * Set the List Fields
         */
        public override void doSetListFields() { 
            moAdminGrid.doAddListField(IDH_TRANCD._Process, "Process", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_TRANCD._Stock, "Stock", true, -1, ListFields.LISTTYPE_COMBOBOX, null, -1);
            moAdminGrid.doAddListField(IDH_TRANCD._WhseFrom, "Warehouse From", true, -1, ListFields.LISTTYPE_COMBOBOX, null, -1);
            moAdminGrid.doAddListField(IDH_TRANCD._WhseTo, "Warehouse To", true, -1, ListFields.LISTTYPE_COMBOBOX, null, -1);
            moAdminGrid.doAddListField(IDH_TRANCD._Trigger, "Trigger", true, -1, ListFields.LISTTYPE_COMBOBOX, null, -1);
            moAdminGrid.doAddListField(IDH_TRANCD._MarkDoc1, "Marketing Document 1", true, -1, ListFields.LISTTYPE_COMBOBOX, null, -1);
            moAdminGrid.doAddListField(IDH_TRANCD._MarkDoc2, "Marketing Document 2", true, -1, ListFields.LISTTYPE_COMBOBOX, null, -1);
            moAdminGrid.doAddListField(IDH_TRANCD._MarkDoc3, "Marketing Document 3", true, -1, ListFields.LISTTYPE_COMBOBOX, null, -1);
            moAdminGrid.doAddListField(IDH_TRANCD._MarkDoc4, "Marketing Document 4", true, -1, ListFields.LISTTYPE_COMBOBOX, null, -1);
            moAdminGrid.doAddListField(IDH_TRANCD._WR1, "WR1 Job (WO,DO)", true, -1, ListFields.LISTTYPE_COMBOBOX, null, -1);
            moAdminGrid.doAddListField(IDH_TRANCD._OrdType, "Order Type", true, -1, ListFields.LISTTYPE_COMBOBOX, null, -1);
            moAdminGrid.doAddListField(IDH_TRANCD._SRF, "Srf", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_TRANCD._BPType, "Applied BP Type", true, -1, ListFields.LISTTYPE_COMBOBOX, null, -1);
            moAdminGrid.doAddListField(IDH_TRANCD._Code, "Code", false, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_TRANCD._Name, "Name", false, 0, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_TRANCD._TrnCode, "Transaction Code", false, -1, ListFields.LISTTYPE_IGNORE, null, -1);
        }
#endregion

#region Events
        protected override void doSetHandlers() {
            base.doSetHandlers();
        }

        protected override void doSetGridHandlers() {
            base.doSetGridHandlers();

            moAdminGrid.Handler_GRID_FIELD_CHANGED = new IDHGrid.ev_GRID_EVENTS(doFieldChangeEvent); 
        }
#endregion 

#region ItemEventHandlers
        public bool doFieldChangeEvent(ref IDHAddOns.idh.events.Base pVal) {
            if (pVal.BeforeAction) {
            } else {
                IDH_TRANCD oTrnCode = ((IDH_TRANCD)moDBObject);
                string sTrnCode = "" +
                setVal(oTrnCode.U_Stock) + '-' + setVal(oTrnCode.U_WhseFrom) + '-' + setVal(oTrnCode.U_WhseTo) + '-' +
                setVal(oTrnCode.U_Trigger) + '-' + setVal(oTrnCode.U_MarkDoc1) + '-' + setVal(oTrnCode.U_MarkDoc2) + '-' +
                setVal(oTrnCode.U_MarkDoc3) + '-' + setVal(oTrnCode.U_MarkDoc4);
                oTrnCode.U_TrnCode = sTrnCode;
            }
            return true;
        }

        private string setVal(string sVal) {
            if (sVal.Length == 0)
                return "0";
            else
                return sVal;
        }
#endregion

#region fillCombos
        private void doFillCombos(){
            SAPbouiCOM.ComboBoxColumn oCombo;

            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_TRANCD._Trigger));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            FillCombos.TriggerCodesCombo(oCombo, "", "N/A");

            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_TRANCD._Stock));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            FillCombos.StockTransferCombo(oCombo, "", "N/A");

            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_TRANCD._MarkDoc1));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            FillCombos.MarketingDocTransaction(oCombo, "", "N/A");

            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_TRANCD._MarkDoc2));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            FillCombos.MarketingDocTransaction(oCombo, "", "N/A");

            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_TRANCD._MarkDoc3));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            FillCombos.MarketingDocTransaction(oCombo, "", "N/A");

            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_TRANCD._MarkDoc4));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            FillCombos.MarketingDocTransaction(oCombo, "", "N/A");

            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_TRANCD._WR1));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            FillCombos.WR1OrderTypeCombo(oCombo,"","Any");

            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_TRANCD._WhseFrom));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            FillCombos.WarehouseCombo(oCombo, "0", "N/A");

            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_TRANCD._WhseTo));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            FillCombos.WarehouseCombo(oCombo, "0", "N/A");

            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_TRANCD._OrdType));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            FillCombos.WRIJobTypesCombo(oCombo, null, "", "Any");

            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_TRANCD._BPType));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            FillCombos.BussinessPartnerTypeCombo(oCombo, "", "Any");
        }
#endregion
    }
}
