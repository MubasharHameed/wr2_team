﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.uBC.utils.Dynamic {
    public class SBOFFItem {
        private static int miControlWith = 80;
        public static int ControlWidth {
            get { return miControlWith; }
            set { miControlWith = value; }
        }

        private static int miHeight = 14;
        public static int Height {
            get { return miHeight; }
            set { miHeight = value; }
        }

        private static int miLableWidth = 80;
        public static int LableWidth {
            get { return miLableWidth; }
            set { miLableWidth = value; }
        }

        private static int miLeftRightWidth = miLableWidth + 1 + miControlWith + 5;
        public static int LeftRightWidth {
            get { return miLeftRightWidth; }
            set { miLeftRightWidth = value; }
        }

        private static string pt_TEXT = "pt_TEXT", pt_SQL = "pt_SQL", pt_LOOKUP = "pt_LOOKUP";
        private static string it_COMBO_BOX = "it_COMBO_BOX", it_EDITBOX = "it_EDITBOX";

        private Structs.Lable moLable;
        public Structs.Lable Lable {
            get { return moLable; }
        }

        private Structs.Control moControl;
        public Structs.Control Control {
            get { return moControl; }
        }

        private com.idh.dbObjects.User.IDH_FFIELDS.ComboParams moComboParams;

        private com.idh.dbObjects.User.IDH_COMLKP moIDH_COMLKP;

        private bool mbIsVisibleOnForm;
        public bool IsVisibleOnForm {
            get { return mbIsVisibleOnForm; }
            set { mbIsVisibleOnForm = value; }
        }
        private bool mbHideNotShow;
        public bool HideNotShow {
            get { return mbHideNotShow; }
            set { mbHideNotShow = value; }
        }

        SAPbouiCOM.StaticText moStaticText;
        SAPbouiCOM.EditText moEditText;
        SAPbouiCOM.ComboBox moComboBox;

        public SBOFFItem(Structs.Lable oLable, Structs.Control oControl) {
            moLable = oLable;
            moControl = oControl;
            moLable.oItem.LinkTo = moControl.oItem.UniqueID;

            moIDH_COMLKP = new idh.dbObjects.User.IDH_COMLKP();
            moIDH_COMLKP.getData();
        }

        public void doSetLableCaption() {
            moStaticText = (SAPbouiCOM.StaticText)moLable.oItem.Specific;
            moStaticText.Caption = moLable.sCaption;
        }
        public void doSetLableCaption(string sCaption) {
            moStaticText = (SAPbouiCOM.StaticText)moLable.oItem.Specific;
            moStaticText.Caption = sCaption;
        }
        public void doSetControlContent() {
            if (moControl.oItemType == SAPbouiCOM.BoFormItemTypes.it_EDIT) {
                moEditText = (SAPbouiCOM.EditText)moControl.oItem.Specific;
                doFill();
            }
            else if (moControl.oItemType == SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX) {
                moComboBox = (SAPbouiCOM.ComboBox)moControl.oItem.Specific;
                doFill();
            }
        }
        private void doFill() {
            if (moControl.oPopType == Structs.PopTypes.pt_TEXT)
                doTextFill();
            else if (moControl.oPopType == Structs.PopTypes.pt_SQL)
                doSQLFill();
            else if (moControl.oPopType == Structs.PopTypes.pt_LOOKUP)
                doLookupFill();
            else
                throw new NotImplementedException();
        }

        private bool doTextFill() {
            if (moEditText != null)
                return doFillEditText(moControl.sPopPar);
            else if (moComboBox != null) {
                moComboParams = new idh.dbObjects.User.IDH_FFIELDS.ComboParams(moControl.sPopPar);
                foreach (com.idh.controls.ValuePair oVP in moComboParams.getPairs) {
                    doFillCombo(oVP.Value1, oVP.Description);
                }
                return true;
            }
            return false;
        }
        private bool doSQLFill() {
            if (moEditText != null) {
                object oValue = null;
                com.idh.bridge.DataRecords oResult = null;
                string sQry = moControl.sPopPar;
                try {
                    oResult = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("TextFill", sQry);
                    if (oResult != null && oResult.RecordCount > 0) {
                        oValue = oResult.getValue(0);
                        doFillEditText((string)oValue);
                    }
                }
                catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBTX", new string[] { string.Empty });
                }
                finally {
                    com.idh.bridge.DataHandler.INSTANCE.doReleaseDataRecords(ref oResult);
                }
                return true;
            }
            else if (moComboBox != null) {
                string sVal, sDec;
                com.idh.bridge.DataRecords oRecords = null;

                try {
                    if (!string.IsNullOrEmpty(moControl.sPopPar)) {
                        oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("doFillCombo", moControl.sPopPar);

                        if (oRecords != null && oRecords.RecordCount > 0) {
                            for (int i = 0; i < oRecords.RecordCount; i++) {
                                oRecords.gotoRow(i);
                                sVal = oRecords.getValue(0).ToString();

                                try {
                                    sDec = oRecords.getValue(1).ToString();
                                }
                                catch (Exception) {
                                    sDec = sVal;
                                }
                                doFillCombo(sVal, sDec);
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", new string[] { moControl.sPopPar });
                }
                finally {
                    com.idh.bridge.DataHandler.INSTANCE.doReleaseDataRecords(ref oRecords);
                }
                return true;
            }
            return false;
        }
        private bool doLookupFill() {
            if (moEditText != null) {
                string sTableName = "[" + moIDH_COMLKP.DBOTableName + "]";
                try {
                    moIDH_COMLKP.getByKey(moControl.sPopPar);
                    doFillEditText(moIDH_COMLKP.U_ValF);
                }
                catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", new string[] { moControl.sPopPar });
                }
                return true;
            }
            else if (moComboBox != null) {
                try {
                    moIDH_COMLKP.getByKey(moControl.sPopPar);   
                    com.idh.bridge.utils.Combobox2.doFillCombo(moComboBox, moIDH_COMLKP.U_Table, moIDH_COMLKP.U_ValF, moIDH_COMLKP.U_DescF, moIDH_COMLKP.U_WHERE, moIDH_COMLKP.U_ORDER, moIDH_COMLKP.U_EmptyV);
                }
                catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", new string[] { "Row :" + moControl.sPopPar + " in TABLE:COMLKP" });
                }
                return true;
            }
            return false;
        }

        private bool doFillEditText(string sValue) {
            moEditText.Value = sValue;
            return true;
        }
        private void doFillCombo(string sValue,string sDesc) {
            moComboBox.ValidValues.Add(sValue, sDesc);
        }

        public static SAPbouiCOM.BoFormItemTypes getBoFormItemType(string sBoFormItemType) {
            if (sBoFormItemType.Equals("it_STATIC"))
                return SAPbouiCOM.BoFormItemTypes.it_STATIC;
            else if (sBoFormItemType.Equals("it_EDITBOX"))
                return SAPbouiCOM.BoFormItemTypes.it_EDIT;
            else if (sBoFormItemType.Equals("it_COMBO_BOX"))
                return SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX;
            else
                throw new NotImplementedException();
        }
        public static Structs.PopTypes getPopTypeFromString(string sPopType) {
            if (sPopType.Equals("pt_TEXT"))
                return Structs.PopTypes.pt_TEXT;
            else if (sPopType.Equals("pt_SQL"))
                return Structs.PopTypes.pt_SQL;
            else if (sPopType.Equals("pt_LOOKUP"))
                return Structs.PopTypes.pt_LOOKUP;
            else
                throw new NotImplementedException();
        }

        public bool doMove(int iLeft, int iTop) {
            moLable.oItem.Left = iLeft;
            moLable.oItem.Top = iTop;
            moControl.oItem.Left = iLeft + miLableWidth + 1;
            moControl.oItem.Top = iTop;
            return true;
        }

        public void doSetVisiblity() {
            bool bShow = mbIsVisibleOnForm && !mbHideNotShow;

            moLable.oItem.Visible = bShow;
            if (moControl.oItemType == SAPbouiCOM.BoFormItemTypes.it_EDIT) {
                moEditText = (SAPbouiCOM.EditText)moControl.oItem.Specific;
                if (moEditText.Active)
                    moEditText.Active = false;
            }
            if (moControl.oItemType == SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX) {
                moComboBox = (SAPbouiCOM.ComboBox)moControl.oItem.Specific;
                if (moComboBox.Active) {
                    moComboBox.Active = false;
                }
            }
            moControl.oItem.Visible = bShow;
        }
        public void doClear() {
            if (moEditText != null) {
                moEditText.Value = string.Empty;
            }
            if (moComboBox != null) {
                try {
                    moComboBox.ValidValues.Add("", "");
                    moComboBox.Select("");
                }
                catch {
                    moComboBox.Select("");
                }
            }
        }

        public static string getBoFormItemTypeAsString(Structs.BoFormItemTypes oBoFormItemType){
            switch(oBoFormItemType){
                case Structs.BoFormItemTypes.it_COMBO_BOX:
                    return it_COMBO_BOX;
                case Structs.BoFormItemTypes.it_EDITBOX:
                    return it_EDITBOX;
                default:
                    throw new NotImplementedException();
            }
        }
        public static string[] getBoFormItemsTypesAsStringArray() {
            return new string[]{it_COMBO_BOX,it_EDITBOX};
        }

        public static string getPopulationTypeAsString(Structs.PopTypes oPopType) {
            switch (oPopType) {
                case Structs.PopTypes.pt_LOOKUP:
                    return pt_LOOKUP;
                case Structs.PopTypes.pt_SQL:
                    return pt_SQL;
                case Structs.PopTypes.pt_TEXT:
                    return pt_TEXT;
                default:
                    throw new NotImplementedException();
            }
        }
        public static string[] getPopulationTypesAsStringArray() {
            return new string[]{pt_LOOKUP,pt_SQL,pt_TEXT};
        }
    }
}
