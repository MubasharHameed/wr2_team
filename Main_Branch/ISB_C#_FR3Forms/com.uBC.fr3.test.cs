﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using   com.uBC.utils;
namespace com.uBC.forms.fr3 {
    public class Test : com.idh.forms.oo.Form {
        #region Member Variables
        private SAPbouiCOM.Form moSBOForm;
        private DOWrapper moDOWrapper;
        private bool mbDoFilter = false;
        private int miSupOrProducer=0;
        private List<utils.weighbridge.WBObject> moWeighBridges = new List<utils.weighbridge.WBObject>();
        private utils.weighbridge.WBObject moWBObject;

        private static string F_SupplierCode = "uBC_BPCD", F_SupplierName = "uBC_BPNM", F_MaterialCode = "uBC_WSCD", F_MaterialName = "uBC_WNM",
            F_Comments = "uBC_COMM", F_VehicleDesc = "uBC_VEHDSC", F_VehicleReg = "uBC_VEHR", F_Driver = "uBC_DRVR",F_ContainerCode = "IDH_ITMCOD",
            F_ContainerGroupCode = "IDH_ITMGRC", F_ContainerGroupName = "IDH_ITMGRN", F_CurrentWeight= "IDH_WEIG",
            F_WeightBufferWEI = "IDH_WEIB", F_WeightBufferSER = "IDH_SERB", F_WeightBufferDate = "IDH_WDTB", 
            F_VehicleTarrW = "IDH_TARWEI", F_VehicleTarrWDate = "IDH_TRWTE1", F_VehicleSecondTarrW = "IDH_TRLTar", F_VehicleSecondTarrWDate = "IDH_TRWTE2",
            F_FirstW = "IDH_WEI1", F_FirstWSER = "IDH_SER1", F_FirstWDate = "IDH_WDT1",F_SecondW = "IDH_WEI2", F_SecondWSER = "IDH_SER2", F_SecondWDate = "IDH_WDT2",
            F_ReadW = "IDH_RDWGT",F_Bales = "IDH_CUSCM",F_WeightD = "IDH_WGTDED",F_AdjustedW = "IDH_ADJWGT",F_BPWeight = "IDH_BPWGT", F_ProducerCD = "uBC_PRCD",
            F_ProducerNM = "uBC_PRNM", F_TicketNum = "uBC_TNUM";
        
        private static string B_FirstWB = "IDH_ACC1", B_FirstWBU = "IDH_BUF1", B_FirstWT = "IDH_TAR1",
            B_SecondWB = "IDH_ACC2",B_SecondWBU = "IDH_BUF2",B_SecondWT = "IDH_TAR2",B_ProducerSearch = "B_PRODS",
            B_NewWeigh = "IDH_NEWWEI",B_CLEAR = "uBCClear", B_TicketNumSearch = "B_TNUMS",B_SupplierSearch = "B_BPSRH",
            B_MaterialSearch = "B_WSCDCF",B_VehicleSearch = "B_VEHRCF",B_DrillContainer = "B_CONT",B_Buffer = "IDH_ACCB",
            B_ContainerSearch = "IDH_ITCF",B_Refresh = "IDH_REF";

        private static string CHKB_PreviousOrders = "uBCShwPrev";

        private static string C_WR1OT = "uBC_WR1O", C_BRANCH = "uBC_BRCH",C_WEIGHB = "IDH_WEIBRG",C_ScaleOp = "uBC_USR";

        private static string RB_ReadWUse = "IDH_USERE",RB_BalesUse = "IDH_USEAU";

        private string[] UserFields = {F_AdjustedW,F_Bales,F_BPWeight,F_Comments,F_ContainerCode,F_ContainerGroupCode,F_ContainerGroupName,F_CurrentWeight,
                                          F_Driver,F_FirstW,F_FirstWDate,F_FirstWSER,F_MaterialCode,F_MaterialName,F_ProducerCD,F_ProducerNM,
                                          F_ReadW,F_SecondW,F_SecondWDate,F_SecondWSER,F_SupplierCode,F_SupplierName,F_TicketNum,F_VehicleDesc,F_VehicleReg,
                                          F_VehicleSecondTarrW,F_VehicleSecondTarrWDate,F_VehicleTarrW,F_VehicleTarrWDate,F_WeightBufferDate,F_WeightBufferSER,
                                          F_WeightBufferWEI,F_WeightD,C_BRANCH,C_ScaleOp,C_WEIGHB,C_WR1OT};

        #endregion

        #region Constructor
        public Test(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oSBOParentForm, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, oSBOParentForm, oSBOForm) {
                moSBOForm = SBOForm;
                moDOWrapper = new DOWrapper();
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_VEHM", sParMenu, iMenuPos, "uBC_DOPreSel.srf", false, true, false, "DOPreselctor", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }
        #endregion

        #region Overrides
        public override void doBeforeLoadData() {
            base.doBeforeLoadData();
            doSetFormUF();
            doAddFormHandlers();
        }
        public override void doLoadData() {
            base.doLoadData();
        }
        public override void doCompleteCreate(ref bool BubbleEvent) {
            base.doCompleteCreate(ref BubbleEvent);
            doFillFormCombos();
            
           
        }
        public override void doHandleModalResultShared(string sModalFormType, string sLastButton) {
            if(sModalFormType.Equals("IDHLOSCH")){
                moDOWrapper = (com.uBC.utils.DOWrapper)getSharedData(com.uBC.utils.DOWrapper.msDOWrapper);
                Branch.Value = moDOWrapper.Branch;
                Driver = moDOWrapper.Driver;
                VehicleDescription = moDOWrapper.VehicleDescription;
                VehicleRegistration = moDOWrapper.VehicleRegistration;
                SupplierCode = moDOWrapper.CarrierCode;
                SupplierName = moDOWrapper.CarrierName;
                WR1OT.Value = moDOWrapper.VehicleWR1ORD;
                //MaterialCode = moDOWrapper.WasteCode;
               
            } 
            else if (sModalFormType.Equals("IDHCSRCH")) {
                if (miSupOrProducer>0) {
                    miSupOrProducer = 0;
                    moDOWrapper.CarrierCode = (string)getSharedData("CARDCODE");
                    SupplierName = moDOWrapper.CarrierName;
                    SupplierCode = moDOWrapper.CarrierCode;
                    if (getSharedData("CALLEDITEM") != null && getSharedData("CALLEDITEM").ToString() != "") {
                        doSetFocus(getSharedData("CALLEDITEM").ToString());
                    }
                    //CarrierName Is set by the CarrierCode Property
                    //moDOWrapper.CarrierType = (SAPbobsCOM.BoCardTypes)getSharedData("CARDTYPE");
                    moDOWrapper.CarrierGroupCode = (int)getSharedData("GROUPCODE");
                    moDOWrapper.CarrierGroupName = (string)getSharedData("GROUPNAME");
                    moDOWrapper.CarrierCurrentAccountBalance = (double)getSharedData("BALANCEFC");
                    moDOWrapper.CarrierCreditLimit = (double)getSharedData("CREDITLINE");
                    moDOWrapper.CarrierPhone1 = (string)getSharedData("PHONE1");
                    moDOWrapper.CarrierContactPerson = (string)getSharedData("CNTCTPRSN");
                    moDOWrapper.CarrierWasteLisence = (string)getSharedData("WASLIC");
                    moDOWrapper.CarrierTarget = (string)getSharedData("TRG");
                } else if (miSupOrProducer<0) {
                    miSupOrProducer = 0;
                    moDOWrapper.ProducerCode = (string)getSharedData("CARDCODE");
                    ProducerName = moDOWrapper.ProducerName;
                    ProducerCode = moDOWrapper.ProducerCode;
                    if (getSharedData("CALLEDITEM") != null && getSharedData("CALLEDITEM").ToString() != "") {
                        doSetFocus(getSharedData("CALLEDITEM").ToString());
                    }
                    moDOWrapper.ProducerGroupCode = (int)getSharedData("GROUPCODE");
                    moDOWrapper.ProducerGroupName = (string)getSharedData("GROUPNAME");
                    moDOWrapper.ProducerCurrentAccountBalance = (double)getSharedData("BALANCEFC");
                    moDOWrapper.ProducerCreditLimit = (double)getSharedData("CREDITLINE");
                    moDOWrapper.ProducerPhone1 = (string)getSharedData("PHONE1");
                    moDOWrapper.ProducerContactPerson = (string)getSharedData("CNTCTPRSN");
                    moDOWrapper.ProducerWasteLisence = (string)getSharedData("WASLIC");
                    moDOWrapper.ProducerTarget = (string)getSharedData("TRG");
                }
            } else if (sModalFormType == "IDHWISRC") {
                if (moDOWrapper == null)
                    moDOWrapper = new DOWrapper();
                moDOWrapper.CarrierTarget = (string)getSharedData("TRG");
                moDOWrapper.WasteCode = (string)getSharedData("ITEMCODE");
                moDOWrapper.WasteDescription = (string)getSharedData("ITEMNAME");

                MaterialCode = moDOWrapper.WasteCode;
                MaterialName = moDOWrapper.WasteDescription;
                if (getSharedData("CALLEDITEM") != null && getSharedData("CALLEDITEM").ToString() != "") {
                    doSetFocus(getSharedData("CALLEDITEM").ToString());
                }
            }
            base.doHandleModalResultShared(sModalFormType, sLastButton);
        }
        public override void doFinalizeShow() {
            setUFValue(C_ScaleOp, idh.bridge.DataHandler.INSTANCE.User);
            setUFValue(C_WR1OT, "Disposal Order");
            setUFValue(C_WEIGHB, "-1");
            setUFValue(C_BRANCH, "Main Branch");
            base.doFinalizeShow();
        }
        #endregion

        #region Handlers
        private bool doLorrySearch(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                setSharedData("SHOWCREATE", "TRUE");
                setSharedData("IDH_VEHREG", getUFValueAsString(F_VehicleReg));
                if (!string.IsNullOrEmpty(getUFValueAsString(F_VehicleReg))) {
                    setSharedData("SILENT", "TRUE");
                }
                setSharedData(DOWrapper.msDOWrapper, moDOWrapper);
                doOpenModalForm("IDHLOSCH");
            }
            return true;
        }
        private bool doTicketNumberSearch(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            return true;
        }
        private bool doBPSearch(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                miSupOrProducer = 1;
                string sCardCode = (string)getUFValue(F_SupplierCode);
                moDOWrapper.CarrierCode = (string)getUFValue(F_SupplierCode);
                if (mbDoFilter) {
                    setSharedData("IDH_BPCOD", moDOWrapper.CarrierCode);
                    setSharedData("IDH_NAME", moDOWrapper.CarrierName);
                } else {
                    setSharedData("IDH_BPCOD", "");
                    setSharedData("IDH_NAME", "");
                }
                if (!string.IsNullOrEmpty(moDOWrapper.CarrierCode))
                    setSharedData("CALLEDITEM", moDOWrapper.CarrierCode);
                setSharedData("TRG", "BP");
                setSharedData("SILENT", "SHOWMULTI");
                doOpenModalForm("IDHCSRCH");
            }
            return true;
        }
        private bool doWasteItemsSearch(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            setSharedData("TRG", "WC");
            setSharedData("SILENT", "SHOWMULTI");
            moDOWrapper.WasteCode = (string)getUFValue(F_MaterialCode);
            if (!string.IsNullOrEmpty(moDOWrapper.WasteCode))
                setSharedData("CALLEDITEM", moDOWrapper.WasteCode);
            doOpenModalForm("IDHWISRC");
            return true;
        }
        private bool doProducerSearch(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                miSupOrProducer = -1;
                moDOWrapper.ProducerCode = (string)getUFValue(F_ProducerCD);
                moDOWrapper.ProducerName = (string)getUFValue(F_ProducerNM);

                setSharedData("TRG", "PR");
                if (!string.IsNullOrEmpty(moDOWrapper.ProducerCode) || !string.IsNullOrEmpty(moDOWrapper.ProducerName) ||
                    !idh.bridge.lookups.Config.INSTANCE.getParameterAsBool(moDOWrapper.VehicleWR1ORD + "BPFLT", true)) {
                    setSharedData("IDH_TYPE", string.Empty);
                    setSharedData("IDH_GROUP", string.Empty);
                    setSharedData("IDH_BRANCH", string.Empty);
                } else {
                    setSharedData("IDH_TYPE", "S");
                    setSharedData("IDH_GROUP", string.Empty);
                }
                setSharedData("IDH_BPCOD", moDOWrapper.ProducerCode);
                setSharedData("IDH_NAME", moDOWrapper.ProducerName);
                if (!string.IsNullOrEmpty(moDOWrapper.ProducerCode))
                    setSharedData("CALLEDITEM",moDOWrapper.ProducerCode);
                setSharedData("SILENT", "SHOWMULTI");
                doOpenModalForm("IDHCSRCH");
                
            }
            return true;
        }
        private bool doClearFormValues(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                foreach (string sUF in UserFields) {
                    setUFValue(sUF, "");
                }
            }
            return true;
        }
        private bool doBufferEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction)
                moWBObject.ReadWeightACCB();
            return true;
        }
        private bool doContainerSearch(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                moDOWrapper.ContainerCode = getUFValueAsString(F_ContainerCode);
                moDOWrapper.ContainerGroupName = getUFValueAsString(F_ContainerGroupName);
                setSharedData("IDH_ITMNAM", moDOWrapper.ContainerGroupName);
                setSharedData("IDH_ITMCOD", moDOWrapper.ContainerCode);
                setSharedData("TRG", "PROD");
                doOpenModalForm("IDHISRC");
            }
            return true;
        }
        private bool doDillContainer(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            return true;
        }
        private bool doFirstWeighBridge(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction)
                moWBObject.ReadWeightACC1();
            return true;
        }
        private bool doFirstWeighBU(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            return true;
        }
        private bool doFirstWeightTarr(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            return true;
        }
        private bool doNewWeigh(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction)
                moWBObject.ReadWeightContinuous();
            return true;
        }
        private bool doRefreshWeigbridgeData(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            return true;
        }
        private bool doSecondWeighBU(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            return true;
        }
        private bool doSecondWeighTarr(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            return true;
        }
        private bool doSecondWeighBridge(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction)
                moWBObject.ReadWeightACC2();
            return true;
        }
        private bool doUseBales(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            return true;
        }
        private bool doUseReadWeight(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            return true;
        }
        private bool doPreviousOrders(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            return true;
        }
        bool moWBObject_Weight2DateChanged(DateTime dtVal) {
            SecondWeighDate = dtVal;
            return true;
        }
        bool moWBObject_Weight2Changed(double dVal) {
            SecondWeigh = dVal;
            return true;
        }
        bool moWBObject_Weight1DateChanged(DateTime dtVal) {
            FirstWeighDate = dtVal;
            return true;
        }
        bool moWBObject_Weight1Changed(double dVal) {
            FirstWeigh = dVal;
            return true;
        }
        bool moWBObject_Serial2Changed(string sVal) {
            SecondWeighSER = sVal;
            return true;
        }
        bool moWBObject_Serial1Changed(string sVal) {
            FirstWeighSER = sVal;
            return true;
        }
        bool moWBObject_BufferWeightDateChanged(DateTime dtVal) {
            BufferWeighDate = dtVal;
            return true;
        }
        bool moWBObject_BufferWeightChanged(double dVal) {
            BufferWeigh = dVal;
            return true;
        }
        bool moWBObject_BufferSerialChanged(string sVal) {
            BufferWeighSER = sVal;
            return true;
        }
        bool moWBObject_WeightChanged(double dVal) {
            ReadWeight = dVal;
            return true;
        }
        bool moWBObject_doSetWBLogo() {
            if (!string.IsNullOrEmpty(moWBObject.Logo)) {
                if (moWBObject.Logo.ToUpper().Equals("TRUE")) {

                }
                //    //If Not sLogo Is Nothing Then
                //    //    If sLogo.ToUpper().Equals("TRUE") Then
                //    //        oForm.Items.Item("IDH_M").Visible = True
                //    //    Else
                //    //        oForm.Items.Item("IDH_M").Visible = False
                //    //    End If
                //    //End If
            }
            return false;
        }
        #endregion

        #region Instance Methods
        private void doSetFormUF() {
            doAddUF(C_WR1OT, SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
            doAddUF(F_SupplierCode, SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
            doAddUF(F_SupplierName,SAPbouiCOM.BoDataType.dt_LONG_TEXT);
            doAddUF(C_BRANCH, SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
            doAddUF(F_Driver, SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
            doAddUF(F_FirstW, SAPbouiCOM.BoDataType.dt_QUANTITY);
            doAddUF(F_FirstWSER,SAPbouiCOM.BoDataType.dt_LONG_TEXT);
            doAddUF(F_FirstWDate,SAPbouiCOM.BoDataType.dt_DATE);
            doAddUF(F_ProducerCD, SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
            doAddUF(F_ProducerNM,SAPbouiCOM.BoDataType.dt_LONG_TEXT);
            doAddUF(F_SecondW, SAPbouiCOM.BoDataType.dt_QUANTITY);
            doAddUF(F_SecondWSER,SAPbouiCOM.BoDataType.dt_LONG_TEXT);
            doAddUF(F_SecondWDate,SAPbouiCOM.BoDataType.dt_DATE);
            doAddUF(F_TicketNum, SAPbouiCOM.BoDataType.dt_LONG_NUMBER);
            doAddUF(F_ReadW, SAPbouiCOM.BoDataType.dt_QUANTITY);
            doAddUF(C_ScaleOp, SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
            doAddUF(F_VehicleDesc, SAPbouiCOM.BoDataType.dt_LONG_TEXT);
            doAddUF(F_VehicleReg, SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
            doAddUF(F_MaterialCode, SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
            doAddUF(C_WEIGHB,SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
            doAddUF(F_AdjustedW,SAPbouiCOM.BoDataType.dt_QUANTITY);
            doAddUF(F_Bales,SAPbouiCOM.BoDataType.dt_QUANTITY);
            doAddUF(F_Comments,SAPbouiCOM.BoDataType.dt_LONG_TEXT);
            doAddUF(F_ContainerCode,SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
            doAddUF(F_ContainerGroupCode,SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
            doAddUF(F_ContainerGroupName,SAPbouiCOM.BoDataType.dt_LONG_TEXT);
            doAddUF(F_CurrentWeight,SAPbouiCOM.BoDataType.dt_QUANTITY);
            doAddUF(F_MaterialName,SAPbouiCOM.BoDataType.dt_LONG_TEXT);
            doAddUF(F_ProducerCD,SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
            doAddUF(F_ProducerNM,SAPbouiCOM.BoDataType.dt_LONG_TEXT);
            doAddUF(F_VehicleSecondTarrW,SAPbouiCOM.BoDataType.dt_QUANTITY);
            doAddUF(F_VehicleSecondTarrWDate,SAPbouiCOM.BoDataType.dt_DATE);
            doAddUF(F_VehicleTarrW,SAPbouiCOM.BoDataType.dt_QUANTITY);
            doAddUF(F_VehicleTarrWDate,SAPbouiCOM.BoDataType.dt_DATE);
            doAddUF(F_WeightBufferDate,SAPbouiCOM.BoDataType.dt_DATE);
            doAddUF(F_WeightBufferSER,SAPbouiCOM.BoDataType.dt_QUANTITY);
            doAddUF(F_WeightBufferWEI,SAPbouiCOM.BoDataType.dt_QUANTITY);
            doAddUF(F_WeightD,SAPbouiCOM.BoDataType.dt_DATE);
            doAddUF(F_BPWeight, SAPbouiCOM.BoDataType.dt_QUANTITY);
        }
        private void doAddFormHandlers(){
            addHandler_ITEM_PRESSED(B_VehicleSearch, new ev_Item_Event(doLorrySearch));
            addHandler_ITEM_PRESSED(B_TicketNumSearch, new ev_Item_Event(doTicketNumberSearch));
            addHandler_ITEM_PRESSED(B_SupplierSearch, new ev_Item_Event(doBPSearch));
            addHandler_ITEM_PRESSED(B_MaterialSearch, new ev_Item_Event(doWasteItemsSearch));
            addHandler_ITEM_PRESSED(B_ProducerSearch, new ev_Item_Event(doProducerSearch));
            addHandler_ITEM_PRESSED(B_CLEAR, new ev_Item_Event(doClearFormValues));
            addHandler_ITEM_PRESSED(B_Buffer,new ev_Item_Event(doBufferEvent));
            addHandler_ITEM_PRESSED(B_ContainerSearch,new ev_Item_Event(doContainerSearch));
            addHandler_ITEM_PRESSED(B_DrillContainer,new ev_Item_Event(doDillContainer));
            addHandler_ITEM_PRESSED(B_FirstWB,new ev_Item_Event(doFirstWeighBridge));
            addHandler_ITEM_PRESSED(B_FirstWBU,new ev_Item_Event(doFirstWeighBU));
            addHandler_ITEM_PRESSED(B_FirstWT,new ev_Item_Event(doFirstWeightTarr));
            addHandler_ITEM_PRESSED(B_NewWeigh,new ev_Item_Event(doNewWeigh));
            addHandler_ITEM_PRESSED(B_Refresh,new ev_Item_Event(doRefreshWeigbridgeData));
            addHandler_ITEM_PRESSED(B_SecondWB,new ev_Item_Event(doSecondWeighBridge));
            addHandler_ITEM_PRESSED(B_SecondWBU,new ev_Item_Event(doSecondWeighBU));
            addHandler_ITEM_PRESSED(B_SecondWT,new ev_Item_Event(doSecondWeighTarr));
            addHandler_ITEM_PRESSED(RB_BalesUse, new ev_Item_Event(doUseBales));
            addHandler_ITEM_PRESSED(RB_ReadWUse, new ev_Item_Event(doUseReadWeight));
            addHandler_ITEM_PRESSED(CHKB_PreviousOrders, new ev_Item_Event(doPreviousOrders));
            moWBObject.doSetWBLogo += new utils.weighbridge.WBObject.SetWBLogo(moWBObject_doSetWBLogo);
            moWBObject.WeightChanged += new utils.weighbridge.WBCommands.dFieldChanged(moWBObject_WeightChanged);
            moWBObject.BufferSerialChanged += new utils.weighbridge.WBCommands.sFieldChanged(moWBObject_BufferSerialChanged);
            moWBObject.BufferWeightChanged += new utils.weighbridge.WBCommands.dFieldChanged(moWBObject_BufferWeightChanged);
            moWBObject.BufferWeightDateChanged += new utils.weighbridge.WBCommands.dtFieldChanged(moWBObject_BufferWeightDateChanged);
            moWBObject.Serial1Changed += new utils.weighbridge.WBCommands.sFieldChanged(moWBObject_Serial1Changed);
            moWBObject.Serial2Changed += new utils.weighbridge.WBCommands.sFieldChanged(moWBObject_Serial2Changed);
            moWBObject.Weight1Changed += new utils.weighbridge.WBCommands.dFieldChanged(moWBObject_Weight1Changed);
            moWBObject.Weight1DateChanged += new utils.weighbridge.WBCommands.dtFieldChanged(moWBObject_Weight1DateChanged);
            moWBObject.Weight2Changed += new utils.weighbridge.WBCommands.dFieldChanged(moWBObject_Weight2Changed);
            moWBObject.Weight2DateChanged += new utils.weighbridge.WBCommands.dtFieldChanged(moWBObject_Weight2DateChanged);
        }
       
        private void doFillFormCombos() {
           FillCombos.WR1OrderTypeCombo(FillCombos.getCombo(SBOForm,C_WR1OT));
           FillCombos.BranchCombo(FillCombos.getCombo(SBOForm,C_BRANCH));
           FillCombos.UserCombo(FillCombos.getCombo(SBOForm,C_ScaleOp));
           SAPbouiCOM.ComboBox oCB = (SAPbouiCOM.ComboBox)SBOForm.Items.Item(C_WEIGHB).Specific;
           foreach (idh.controls.ValuePair oVP in doGetWeighBridges()) {
               oCB.ValidValues.Add(oVP.Value, oVP.Description);
           }
        }
        public List<idh.controls.ValuePair> doGetWeighBridges() {
            //GET THE AVAILABLE WEIGH BRIDGES
            SAPbobsCOM.Recordset oRecordSet;
            List<idh.controls.ValuePair> oValidValues = new List<idh.controls.ValuePair>();
            try {
                oRecordSet = IDHForm.goParent.goDB.doSelectQuery("select U_Val, U_Desc from [@" + IDHForm.goParent.goConfigTable + "] WHERE Code LIKE 'WBID%'");
                if (oRecordSet.RecordCount > 0) {
                    for (int iCount = 0; iCount < oRecordSet.RecordCount - 1; iCount++) {
                        string sID = (string)oRecordSet.Fields.Item(0).Value;
                        moWBObject = new utils.weighbridge.WBObject(sID);

                        if (moWBObject.IsAvailable) {
                            if (moWBObject.Weighbridge == null && iCount == 0) {
                                return null;
                            }

                            oValidValues.Add(new idh.controls.ValuePair((string)oRecordSet.Fields.Item(0).Value, (string)oRecordSet.Fields.Item(1).Value));
                            moWeighBridges.Add(moWBObject);
                        }
                        oRecordSet.MoveNext();
                    }
                } else {
                    oValidValues.Add(new idh.controls.ValuePair("-1", "-1"));
                }  
            } catch (Exception ex) {

            } finally {
                oRecordSet = null;
            }
            return oValidValues;
        }
        #endregion

        #region Properties
        public double AdjustedWeight {
            get { return (double)getUFValue(F_AdjustedW); }
            set { setUFValue(F_AdjustedW, value); }
        }
        public double Bales {
            get { return (double)getUFValue(F_Bales); }
            set { setUFValue(F_Bales, value); }
        }
        public double BPWeight {
            get { return (double)getUFValue(F_BPWeight); }
            set { setUFValue(F_BPWeight, value); }
        }
        public string Comments {
            get { return getUFValueAsString(F_Comments); }
            set { setUFValue(F_Comments, value); }
        }
        public string ContainerCode {
            get { return getUFValueAsString(F_ContainerCode); }
            set { setUFValue(F_ContainerCode, value); }
        }
        public string ContainerGroupCode {
            get { return getUFValueAsString(F_ContainerGroupCode); }
            set { setUFValue(F_ContainerGroupCode, value); }
        }
        public string ContainerGroupName {
            get { return getUFValueAsString(F_ContainerGroupName); }
            set { setUFValue(F_ContainerGroupName, value); }
        }
        public double CurrentWeight {
            get { return (double)getUFValue(F_CurrentWeight); }
            set { setUFValue(F_CurrentWeight, value); }
        }
        public string Driver {
            get { return getUFValueAsString(F_Driver); }
            set { setUFValue(F_Driver, value); }
        }
        public double FirstWeigh {
            get { return (double)getUFValue(F_FirstW); }
            set { setUFValue(F_FirstW, value); }
        }
        public DateTime FirstWeighDate {
            get { return (DateTime)getUFValue(F_FirstWDate); }
            set { setUFValue(F_FirstWDate, value); }
        }
        public string FirstWeighSER {
            get { return getUFValueAsString(F_FirstWSER); }
            set { setUFValue(F_FirstWSER, value); }
        }
        public string MaterialCode {
            get { return getUFValueAsString(F_MaterialCode); }
            set { setUFValue(F_MaterialCode, value); }
        }
        public string MaterialName {
            get { return getUFValueAsString(F_MaterialName); }
            set { setUFValue(F_MaterialName, value); }
        }
        public string ProducerCode {
            get { return getUFValueAsString(F_ProducerCD); }
            set { setUFValue(F_ProducerCD, value); }
        }
        public string ProducerName {
            get { return getUFValueAsString(F_ProducerNM); }
            set { setUFValue(F_ProducerNM, value); }
        }
        public double ReadWeight {
            get { return (double)getUFValue(F_ReadW); }
            set { setUFValue(F_ReadW, value); }
        }
        public double SecondWeigh {
            get { return (double)getUFValue(F_SecondW); }
            set { setUFValue(F_SecondW, value); }
        }
        public DateTime SecondWeighDate {
            get { return (DateTime)getUFValue(F_SecondWDate); }
            set { setUFValue(F_SecondWDate, value); }
        }
        public string SecondWeighSER {
            get { return getUFValueAsString(F_SecondWSER); }
            set { setUFValue(F_SecondWSER, value); }
        }
        public string SupplierCode {
            get { return getUFValueAsString(F_SupplierCode); }
            set { setUFValue(F_SupplierCode, value); }
        }
        public string SupplierName {
            get { return getUFValueAsString(F_SupplierName); }
            set { setUFValue(F_SupplierName, value); }
        }
        public string TicketNumber {
            get { return getUFValueAsString(F_TicketNum); }
            set { setUFValue(F_TicketNum, value); }
        }
        public string VehicleDescription {
            get { return getUFValueAsString(F_VehicleDesc); }
            set { setUFValue(F_VehicleDesc, value); }
        }
        public string VehicleRegistration {
            get { return getUFValueAsString(F_VehicleReg); }
            set { setUFValue(F_VehicleReg, value); }
        }
        public double VehicleSecondTarrWeight {
            get { return (double)getUFValue(F_VehicleSecondTarrW); }
            set { setUFValue(F_VehicleSecondTarrW, value); }
        }
        public DateTime VehicleSecondTarrWeightDate {
            get { return (DateTime)getUFValue(F_VehicleSecondTarrWDate); }
            set { setUFValue(F_VehicleSecondTarrWDate, value); }
        }
        public double VehicleTarrWeight {
            get { return (double)getUFValue(F_VehicleTarrW); }
            set { setUFValue(F_VehicleTarrW, value); }
        }
        public DateTime VehicleTarrWeightDate {
            get { return (DateTime)getUFValue(F_VehicleTarrWDate); }
            set { setUFValue(F_VehicleTarrWDate, value); }
        }
        public DateTime BufferWeighDate {
            get { return (DateTime)getUFValue(F_WeightBufferDate); }
            set { setUFValue(F_WeightBufferDate, value); }
        }
        public string BufferWeighSER {
            get { return getUFValueAsString(F_WeightBufferSER); }
            set { setUFValue(F_WeightBufferSER, value); }
        }
        public double BufferWeigh {
            get { return (double)getUFValue(F_WeightBufferWEI); }
            set { setUFValue(F_WeightBufferWEI, value); }
        }
        public double WeightDeduction {
            get { return (double)getUFValue(F_WeightD); }
            set { setUFValue(F_WeightD, value); }
        }
        public idh.controls.ValuePair Branch {
            get { return new idh.controls.ValuePair(getUFValueAsString(C_BRANCH), ""); }
            set { setUFValue(C_BRANCH, value.Value); }
        }
        public idh.controls.ValuePair ScaleOpperator {
            get { return new idh.controls.ValuePair(getUFValueAsString(C_ScaleOp), ""); }
            set { setUFValue(C_ScaleOp, value.Value); }
        }
        public idh.controls.ValuePair WeighBridge {
            get { return new idh.controls.ValuePair(getUFValueAsString(C_WEIGHB), ""); }
            set { setUFValue(C_WEIGHB, value.Value); }
        }
        public idh.controls.ValuePair WR1OT {
            get { return new idh.controls.ValuePair(getUFValueAsString(C_WR1OT), ""); }
            set { setUFValue(C_WR1OT, value.Value); }
        }
        #endregion
    }   
}
