﻿using System.IO;
using System.Collections;
using System;

//Needed for the Framework controls
using IDHAddOns.idh.controls;
using IDHAddOns;
using com.idh.bridge.data;
using com.idh.bridge;
using com.idh.bridge.lookups;

namespace com.uBC.forms.fr3 {
    public class Import : com.idh.forms.oo.Form {

        public Import(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, ref SAPbouiCOM.Form oSBOForm) : base(oIDHForm, sParentId, oSBOForm) {
            doSetHandlers();
        }
        
        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_IMPORTS", sParMenu, 80, "Imports.srf", false, true, false, "Imports", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);

            return owForm;
        }
        
        //** Create the form
        public override void doCompleteCreate(ref bool BubbleEvent) {
            if (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_ALLOW_LOW_FINANCIALS)) {
                Items.Item("JOURNAL").Enabled = true;
            } else {
                Items.Item("JOURNAL").Enabled = false;
            }

            if (Config.INSTANCE.getParameterAsBool("IMPENGEN", false) == true) {
                Items.Item("GENERAL").Enabled = true;
            } else {
                Items.Item("GENERAL").Enabled = false;
            }
        }

#region Events
        private void doSetHandlers() {
            addHandler_ITEM_PRESSED("JOURNAL", new ev_Item_Event(doJournalEntryEvent));
            addHandler_ITEM_PRESSED("WEIGHTS", new ev_Item_Event(doWeightUpdateEvent));
            addHandler_ITEM_PRESSED("PBICSTREF", new ev_Item_Event(doPBICustomeReferenceEvent));
            addHandler_ITEM_PRESSED("INCOMPAYM", new ev_Item_Event(doIncomingPaymentsEvent));
            addHandler_ITEM_PRESSED("GENERAL", new ev_Item_Event(doGeneralImportEvent));
            
            addHandler_ITEM_PRESSED("ITMPRILIST", new ev_Item_Event(doItemPriceListEvent));

        }

        //**
        // Handle the Weight Import Button Event
        //**
        public bool doWeightUpdateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == true) {
                doCallWinFormOpener(doImportWeight);
            }
            return false;
        }

        //**
        // Handle the Customer Reference Import Button Event
        //**
        public bool doPBICustomeReferenceEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == true) {
                doCallWinFormOpener(doImportPBICustReference);
            }
            return false;
        }

        //**
        // Handle the Journal Import Button Event
        //**
        public bool doJournalEntryEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == true) {
                doCallWinFormOpener(doImportJournal);
            }
            return false;
        }

        //**
        // Handle the General Import Button Event
        //**
        public bool doGeneralImportEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == true) {
                doCallWinFormOpener(doImportGeneral);
            }
            return false;
        }
        //**
        // Handle the Incoming Payments Button Event
        //**
        public bool doIncomingPaymentsEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            if (pVal.BeforeAction == true)
            {
                doOpenModalForm("IDH_INCMPAY");
            }
            return false;
        }
        //**
        // Handle the Item Price List Event
        //**
        public bool doItemPriceListEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == true) {
                doCallWinFormOpener(doImportItemPrices);
            }
            return false;
        }
#endregion 

#region ImportHandlers
        //**
        // Do the Journal Imports
        //**
        protected void doImportJournal() {
            string sFileName;
            System.Windows.Forms.OpenFileDialog oOpenFile = new System.Windows.Forms.OpenFileDialog();

            try {
                string sExt = Config.INSTANCE.getImportExtension();
                if (sExt != null && sExt.Length > 0) {
                    if (sExt.IndexOf("|") == -1) {
                        sExt = "(" + sExt + ")|" + sExt;
                    }
                    oOpenFile.Filter = sExt;
                }

            } catch (Exception ex) {
                //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error importing the data.");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", new string[]{string.Empty});
            }
            oOpenFile.FileName = "OpenExcelFile";
            oOpenFile.InitialDirectory = Config.INSTANCE.getImportFolder();
            if (com.idh.win.AppForm.OpenFileDialog(oOpenFile) == System.Windows.Forms.DialogResult.OK) {
                sFileName = oOpenFile.FileName;
                try {
                    com.idh.bridge.DataHandler.INSTANCE.doInfo("Importing the file.");

                    com.idh.bridge.import.Journal oImporter = new com.idh.bridge.import.Journal();
                    if (oImporter.doReadFile(sFileName)) {
                        com.idh.bridge.DataHandler.INSTANCE.doInfo("Import done.");
                    }
                } catch (Exception ex) {
                    //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error importing the data.");
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", new string[] { string.Empty });
                } finally {
                    //goParent.doProgress("EXIMP", 100)

                    string sCurrDirectory = Directory.GetCurrentDirectory();
                    Directory.SetCurrentDirectory(IDHAddOns.idh.addon.Base.CURRENTWORKDIR);
                }
            }
        }

        //**
        // Do the Weight Imports
        //**
        protected void doImportWeight() {
            string sFileName;
            System.Windows.Forms.OpenFileDialog oOpenFile = new System.Windows.Forms.OpenFileDialog();

            try {
                string sExt = Config.INSTANCE.getImportExtension();
                if (sExt != null && sExt.Length > 0) {
                    if (sExt.IndexOf("|") == -1) {
                        sExt = "(" + sExt + ")|" + sExt;
                    }
                    oOpenFile.Filter = sExt;
                }

            } catch (Exception ex) {
                //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error importing the data.");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", new string[] { string.Empty });
            }
            oOpenFile.FileName = "OpenExcelFile";
            oOpenFile.InitialDirectory = Config.INSTANCE.getImportFolder();
            if (com.idh.win.AppForm.OpenFileDialog(oOpenFile) == System.Windows.Forms.DialogResult.OK) {
                sFileName = oOpenFile.FileName;
                try {
                    com.idh.bridge.DataHandler.INSTANCE.doInfo("Importing the file.");

                    com.idh.bridge.import.Weights oImporter = new com.idh.bridge.import.Weights();
                    //  "c:\\temp\\testfile.xls"
                    if (oImporter.doReadFile(sFileName)) {
                        com.idh.bridge.DataHandler.INSTANCE.doInfo("Import done.");
                    }
                } catch (Exception ex) {
                    //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error importing the data.");
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", new string[] { string.Empty });
                } finally {
                    //goParent.doProgress("EXIMP", 100)

                    string sCurrDirectory = Directory.GetCurrentDirectory();
                    Directory.SetCurrentDirectory(IDHAddOns.idh.addon.Base.CURRENTWORKDIR);
                }
            }
        }

        //**
        // Do Import the PBI Cust Reference Numbers
        //**
        protected void doImportPBICustReference() {
            string sFileName;
            System.Windows.Forms.OpenFileDialog oOpenFile = new System.Windows.Forms.OpenFileDialog();

            try {
                string sExt = Config.INSTANCE.getImportExtension();
                if (sExt != null && sExt.Length > 0) {
                    if (sExt.IndexOf("|") == -1) {
                        sExt = "(" + sExt + ")|" + sExt;
                    }
                    oOpenFile.Filter = sExt;
                }

            } catch (Exception ex) {
                //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error importing the data.");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", new string[] { string.Empty });
            }
            oOpenFile.FileName = "OpenExcelFile";
            oOpenFile.InitialDirectory = Config.INSTANCE.getImportFolder();
            if (com.idh.win.AppForm.OpenFileDialog(oOpenFile) == System.Windows.Forms.DialogResult.OK) {
                sFileName = oOpenFile.FileName;
                try {
                    com.idh.bridge.DataHandler.INSTANCE.doInfo("Importing the file.");

                    com.idh.bridge.import.PBICustRef oImporter = new com.idh.bridge.import.PBICustRef();
                    if (oImporter.doReadFile(sFileName)) {
                        com.idh.bridge.DataHandler.INSTANCE.doInfo("Import done.");
                    }
                } catch (Exception ex) {
                    //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error importing the data.");
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", new string[] { string.Empty });
                } finally {
                    //goParent.doProgress("EXIMP", 100)

                    string sCurrDirectory = Directory.GetCurrentDirectory();
                    Directory.SetCurrentDirectory(IDHAddOns.idh.addon.Base.CURRENTWORKDIR);
                }
            }
        }

        //**
        // Do the General Imports
        //**
        protected void doImportGeneral() {
            string sFileName;
            System.Windows.Forms.OpenFileDialog oOpenFile = new System.Windows.Forms.OpenFileDialog();

            try {
                string sExt = Config.INSTANCE.getImportExtension();
                if (sExt != null && sExt.Length > 0) {
                    if (sExt.IndexOf("|") == -1) {
                        sExt = "(" + sExt + ")|" + sExt;
                    }
                    oOpenFile.Filter = sExt;
                }

            } catch (Exception ex) {
                //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error importing the data.");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", new string[] { string.Empty });
            }
            oOpenFile.FileName = "OpenExcelFile";
            oOpenFile.InitialDirectory = Config.INSTANCE.getImportFolder();
            if (com.idh.win.AppForm.OpenFileDialog(oOpenFile) == System.Windows.Forms.DialogResult.OK) {
                sFileName = oOpenFile.FileName;
                try {
                    com.idh.bridge.DataHandler.INSTANCE.doInfo("Importing the file.");

                    com.idh.bridge.import.GeneralData oImporter = new com.idh.bridge.import.GeneralData();
                    if (oImporter.doReadFile(sFileName)) {
                        com.idh.bridge.DataHandler.INSTANCE.doInfo("Import done.");
                    }
                } catch (Exception ex) {
                    //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error importing the data.");
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", new string[] { string.Empty });
                } finally {
                    //goParent.doProgress("EXIMP", 100)

                    string sCurrDirectory = Directory.GetCurrentDirectory();
                    Directory.SetCurrentDirectory(IDHAddOns.idh.addon.Base.CURRENTWORKDIR);
                }
            }
        }

        //**
        // Do the SBO Price List Imports
        //**
        protected void doImportItemPrices() {
            string sFileName;
            System.Windows.Forms.OpenFileDialog oOpenFile = new System.Windows.Forms.OpenFileDialog();

            try {
                string sExt = Config.INSTANCE.getImportExtension();
                if (sExt != null && sExt.Length > 0) {
                    if (sExt.IndexOf("|") == -1) {
                        sExt = "(" + sExt + ")|" + sExt;
                    }
                    oOpenFile.Filter = sExt;
                }

            } catch (Exception ex) {
                //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error importing the data.");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", new string[] { string.Empty });
            }
            oOpenFile.FileName = "OpenExcelFile";
            oOpenFile.InitialDirectory = Config.INSTANCE.getImportFolder();
            if (com.idh.win.AppForm.OpenFileDialog(oOpenFile) == System.Windows.Forms.DialogResult.OK) {
                sFileName = oOpenFile.FileName;
                try {
                    com.idh.bridge.DataHandler.INSTANCE.doInfo("Importing the file.");

                    com.idh.bridge.import.PriceList oImporter = new com.idh.bridge.import.PriceList();
                    if (oImporter.doReadFile(sFileName)) {
                        com.idh.bridge.DataHandler.INSTANCE.doInfo("PriceList Import done.");
                    }
                } catch (Exception ex) {
                    //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error importing the data.");
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", new string[] { string.Empty });
                } finally {
                    //goParent.doProgress("EXIMP", 100)

                    string sCurrDirectory = Directory.GetCurrentDirectory();
                    Directory.SetCurrentDirectory(IDHAddOns.idh.addon.Base.CURRENTWORKDIR);
                }
            }
        }

#endregion
    }
}

