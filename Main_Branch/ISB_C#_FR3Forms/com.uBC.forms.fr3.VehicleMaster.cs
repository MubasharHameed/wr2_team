﻿using System.IO;

using System.Collections;
using System;
using SAPbouiCOM;

//Needed for the Framework controls
using IDHAddOns.idh.controls;
using IDHAddOns;
using com.idh.bridge.data;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.idh.dbObjects.User;
using com.uBC.utils;

namespace com.uBC.forms.fr3 {
    public class VehicleMaster : com.idh.forms.oo.Form {
        private IDH_VEHMAS2 moVehicleMaster;
        public IDH_VEHMAS2 DBVehicleMaster {
            get { return moVehicleMaster; }
        }
        
        private bool mbAddUpdateOk = false;
        private string msVehicleReg = null;
        public string VehicleReg {
            set { msVehicleReg = value; }
            get { return msVehicleReg; }
           
        }

        public VehicleMaster(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
            doSetHandlers();

            //Set the Data Object for this Form
            moVehicleMaster = new IDH_VEHMAS2(oIDHForm, oSBOForm);
        }
        
        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_VEHDE2", sParMenu, 80, "uBC_VehMDet22.srf", false, true, false, "Vehicle Master Detail", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            owForm.doEnableHistory(IDH_VEHMAS2.TableName); 
            return owForm;
        }

#region FormOpenCreateFunctions
        /** 
         * This Function will be called by the controller to allow the class to last minute Form Layout adjustments before it gets displayed
         * All changes made in hete will be cached if this is a cached form, the method will not be called again once the form has benn cached.
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            /**
             * Browser By is set using the Form Item Id for the Data Key field
             */
            DataBrowser.BrowseBy = "uBC_VR2"; // "uBC_VEHREG";
            SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_All;
            AutoManaged = true;

            PaneLevel = 1;

            Items.Item("F0_Gen").AffectsFormMode = false;
            Items.Item("F1_Ser/C").AffectsFormMode = false;
            Items.Item("F2_DrivCr").AffectsFormMode = false;
            Items.Item("F3_Notes").AffectsFormMode = false;

            Items.Item("uBC_CVEHT").DisplayDesc = true;
            Items.Item("uBC_CVEHC").DisplayDesc = true;
            Items.Item("uBC_CWFS").DisplayDesc = true;
            Items.Item("uBC_CWR1OT").DisplayDesc = true;
            Items.Item("uBC_CAVAIL").DisplayDesc = true;
            Items.Item("uBC_CREAS").DisplayDesc = true;

            Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE;
        }

        /** 
         * Do the final form steps to show before loaddata
         */
        public override void doBeforeLoadData() {
            bool bHide = Config.ParameterAsBool("VMHDFLW", false);
            setVisible("S_WfS", !bHide );
            setVisible("uBC_CWFS", !bHide );
            
            doFillCombos();
        }

        /** 
         * Load the Form Data
         */
        public override void doLoadData() {
            string sVehReg = "";
            bool bIsLike = false;
            if (!string.IsNullOrWhiteSpace(msVehicleReg))
                sVehReg = msVehicleReg;
            else
                sVehReg = (string)getFormDFValue(IDH_VEHMAS2._VehReg, true);

            if (!string.IsNullOrWhiteSpace(sVehReg)) {
                if (sVehReg.EndsWith("*")) {
                    sVehReg = sVehReg.Substring(0, sVehReg.LastIndexOf('*'));
                    bIsLike = true;
                }
                
                //FUTURE USE - moVehicleMaster.getByKey(msVehicleCode);

                DataBrowser.BrowseBy = "uBC_VR2"; //"";

                SAPbouiCOM.Conditions oCons = new SAPbouiCOM.Conditions();
                SAPbouiCOM.Condition oCon;
                SAPbouiCOM.DBDataSource oDataSrc;

                oCon = oCons.Add();
                oCon.Alias = IDH_VEHMAS2._VehReg;
                oCon.Operation = (bIsLike?SAPbouiCOM.BoConditionOperation.co_START:SAPbouiCOM.BoConditionOperation.co_EQUAL);
                oCon.CondVal = sVehReg;
                oDataSrc = FormMainDataSource;
                oDataSrc.Query(oCons);

                if (oDataSrc.Size == 0) {
                    //DataHandler.INSTANCE.doUserError("Vehicle record was found the registration: " + sVehReg);
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Vehicle record was not found,registration nr: " + sVehReg,"ERUSRCNF", new string[] { sVehReg });
                    Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE;
                } else {
                    sVehReg = (string)getFormDFValue(IDH_VEHMAS2._VehReg);
                    moVehicleMaster.getByRegistration(sVehReg);
                    Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
                    sVehReg = (string)getFormDFValue(IDH_VEHMAS2._VehReg);
                }
                msVehicleReg = "";
            }
        }

        /** 
         * Do the final form steps to show after loaddata
         */
        public override void doFinalizeShow() {
            base.doFinalizeShow();
            Items.Item("F0_Gen").Click();
        }
#endregion

#region Events
        /**
         * Register all your event Handlers in here.
         * Handler_ALL_EVENTS 0 All events, for use when filtering events 
         * Handler_ITEM_PRESSED 1 The main mouse button was clicked on one of the following: 
         * 		Tab 
         * 		Button 
         * 		Option button 
         * 		Check box (standalone or within a cell)
         * 		This event occurs when a mouse is released within an item, that is, mouse up.
         * Handler_KEY_DOWN 2 A key was pressed. 
         * Handler_GOT_FOCUS 3 An item received focus. 
         * Handler_LOST_FOCUS 4 An item lost focus. 
         * Handler_LOST_FOCUS_CHANGED 4 An item lost focus with the value changed
         * Handler_COMBO_SELECT 5 A value was selected in a combo box. 
         * Handler_CLICK 6 The main mouse button was clicked on an item, that is, mouse down. 
         * Handler_DOUBLE_CLICK 7 The main mouse button was double-clicked on an item. 
         * Handler_MATRIX_LINK_PRESSED 8 A link arrow in a matrix was pressed. 
         * Handler_MATRIX_COLLAPSE_PRESSED 9 A matrix list was collapsed or expanded. 
         * Handler_VALIDATE 10 An item lost focus and validation is required.
         * Handler_VALIDATE_CHANGED 10 An item lost focus and validation is required - only when the data has changed.
         * Handler_MATRIX_LOAD 11 Data was loaded from the database into a matrix data source. 
         * 		This event occurs for user-defined matrix objects only. If a form contains two matrix objects, the system generates two events.
         * Handler_DATASOURCE_LOAD 12 Data was loaded from the GUI into a matrix data source. 
         * 		This event occurs once per matrix. If a form has two matrix objects, the system generates two events.
         * Handler_FORM_LOAD 16 A form was opened (FormDataEvent). 
         * Handler_FORM_UNLOAD 17 A form was closed. 
         * Handler_FORM_ACTIVATE 18 A form received focus. 
         * Handler_FORM_DEACTIVATE 19 A form lost focus. 
         * Handler_FORM_CLOSE 20 A form is about to be closed. 
         * Handler_FORM_RESIZE 21 A form has been resized. 
         * Handler_FORM_KEY_DOWN 22 A key was pressed when no form had the focus. 
         * Handler_FORM_MENU_HILIGHT 23 A form is modifying the status of toolbar items, that is, enabling and disabling icons. 
         * 		By default, these events are not thrown. You can activate these events by setting an event filter.
         * Handler_PRINT 24 A print preview was requested for a report or document (PrintEvent). 
         * 		This event lets you exit from the application print operation and use your own printing.
         * Handler_PRINT_DATA 25 A print preview was requested for a report (ReportDataEvent).  
         * 		This event lets you get the report data in XML format.
         * Handler_CHOOSE_FROM_LIST 27 A ChooseFromList event occurred, as follows:  
         * 		The Before event occurs before the ChooseFromList form is displayed. If the BubbleEvent parameter is set to False, the form is not displayed. 
         * 		The After event occurs after the user makes a selection or chooses Cancel.		 
         * Handler_RIGHT_CLICK 28 The right mouse button was clicked on an item (RightClickEvent). 
         * Handler_MENU_CLICK 32 The main mouse button was released on a menu item without submenus. 
         * 		For future use.
         * Handler_FORM_DATA_ADD 33 A record in a business object was added (FormDataEvent). 
         * Handler_FORM_DATA_UPDATE 34 A record in a business object was updated (FormDataEvent). 
         * Handler_FORM_DATA_DELETE 35 A record in a business object was deleted (FormDataEvent). 
         * Handler_FORM_DATA_LOAD 36 A record in a business object was loaded -- via browse, link button, or find (FormDataEvent). 
         * Handler_PICKER_CLICKED 37 A Picker event occurred, as follows:  
         * 		The Before event occurs before the Picker form is displayed. If the BubbleEvent parameter is set to False, the picker is not displayed. 
         * 		The After event occurs after the user makes a selection or chooses Cancel.
         * Handler_GRID_SORT 38 A grid column was sorted, either by a user clicking the column title or an add-on calling the Sort method of the Grid object. 
         * Handler_Drag 39 The item was dragged and dropped at the position you want it to appear. 
         * Handler_PRINT_LAYOUT_KEY 80 A print or print preview was requested for an add-on form, and the layout needs the key of the add-on form 		
         * 
         * To Add a Item PressEvent for a specific Control use 
         * addHandler_ITEM_PRESSED("ITEMID", new ev_Item_Event(EventFunction));
         *      e.g. addHandler_ITEM_PRESSED("uBCCardSR", new ev_Item_Event(doHandleCardCodeSearch));
         *      
         *           public bool doHandleCardCodeSearch(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
         *               return false;
         *           }
         *           
         *  The following Special Event Handlers can be set when using the SBO default where Button 1 is OK 
         *  Handler_Button_Ok - When the Form is in Ok or View Mode
         *  Handler_Button_Find - When the Form is in Find Mode
         *  Handler_Button_Add - When the Form is in Add Mode
         *  Handler_Button_Update - When the Form is in Update Mode
         *   
         * The Menu Events
         * Handler_Menu_NAV_ADD
		 * Handler_Menu_NAV_FIND
		 * Handler_Menu_NAV_FIRST
		 * Handler_Menu_NAV_LAST
		 * Handler_Menu_NAV_NEXT
         * Handler_Menu_NAV_PREV
         * --------> If the BubbleEvent is not set to False the normal Overridable Function doMenuEvent will be called 
         *           doMenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent)
         *
         * Special Handler that will be called if this form was opened as a Dialog (Opened from another Form)
         * Handler_DialogButton1Return - When Button 1 was pressed irrespective of the Form Mode
         * Handler_DialogOkReturn - When Button 1 was Pressed and Form was in Ok/View Mode
         * Handler_DialogCancelReturn = When Button 2 was Pressed
         * --------> This will only be called if all the other Button 1 /2 events where called including the Overridable 
         *           Function doButtonID1 and the Bubble event is still true.
         */
        private void doSetHandlers() {
            Handler_Button_Add = new ev_Item_Event(doAddEvent);
            Handler_Button_Update = new ev_Item_Event(doUpdateEvent);
            Handler_Button_Find = new ev_Item_Event(doFindEvent);

            //Folder Press Events
            addHandler_ITEM_PRESSED("F0_Gen", new ev_Item_Event(doGeneralFolder));
            addHandler_ITEM_PRESSED("F1_Ser/C", new ev_Item_Event(doServiceCostFolder));
            addHandler_ITEM_PRESSED("F2_DrivCr", new ev_Item_Event(doDriverCrewFolder));
            addHandler_ITEM_PRESSED("F3_Notes", new ev_Item_Event(doNotesFolder));

            addHandler_ITEM_PRESSED("B1_Arrow", new ev_Item_Event(doWOOpenEvent));
            addHandler_ITEM_PRESSED("B2_Arrow", new ev_Item_Event(doDISPORDOpenEvent));

            addHandler_COMBO_SELECT("uBC_CDFDNU", new ev_Item_Event(doSelectDriverEvent));

            //addHandler_VALIDATE_CHANGED("uBC_VEHBTO", new ev_Item_Event(doBelongsToCodeChangedEvent));
            addHandler_VALIDATE("uBC_VEHBTO", new ev_Item_Event(doBelongsToCodeChangedEvent));
            addHandler_CLICK("IDH_CRCFL", new ev_Item_Event(doBelongsToCFLClickEvent));
            //Menu Events
            Handler_Menu_NAV_ADD = new et_Menu_Event(doMenuAddEvent);
            Handler_Menu_NAV_FIRST = new et_Menu_Event(doMenuBrowseEvents);
            Handler_Menu_NAV_LAST = new et_Menu_Event(doMenuBrowseEvents);
            Handler_Menu_NAV_NEXT = new et_Menu_Event(doMenuBrowseEvents);
            Handler_Menu_NAV_PREV = new et_Menu_Event(doMenuBrowseEvents);
            Handler_Menu_NAV_FIND = new et_Menu_Event(doMenuFindBUttonEvent);
            Handler_FormMode_Changed = new ev_Form_Mode(doHandleModeChange);
        }
        
#region Handlers
        public bool doFindEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                doLoadData();
                BubbleEvent = false;
            }
            return true;
        }
#endregion

#region ItemEventHandlers
        public bool doAddEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                DateTime oDate = DateTime.Now;

                moVehicleMaster.U_CREAOn = oDate;
                moVehicleMaster.U_LAMEND = oDate;

                //FUTURE USE - Updater if ( moVehicleMaster.doProcessData( )) {
                mbAddUpdateOk = doAutoSave(IDH_VEHMAS2.AUTONUMPREFIX, moVehicleMaster.AutoNumKey);
            } else {
                if (mbAddUpdateOk) {
                    doClearAll();
                    Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE;
                }
            }
            return true;
        }

        public bool doUpdateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if ( pVal.BeforeAction ) {
                DateTime oDate = DateTime.Now;

                moVehicleMaster.U_LAMEND = oDate;

                //FUTURE USE - Updater if ( moVehicleMaster.doProcessData( )) {
                mbAddUpdateOk = doAutoSave(IDH_VEHMAS2.AUTONUMPREFIX, moVehicleMaster.AutoNumKey);
            } else {
                if (mbAddUpdateOk)
                    Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
            }
            return true;
        }

        public bool doBelongsToCodeChangedEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            bool bSuccess = true;
            if (!pVal.BeforeAction) {
                //if (((string)getUFValue("uBC_VEHBTO")).Length > 0 ) {
                SAPbouiCOM.EditText oCCCodeItem;
                try {
                    oCCCodeItem = (SAPbouiCOM.EditText)SBOForm.Items.Item("uBC_VEHBTO").Specific;

                    if (oCCCodeItem != null && oCCCodeItem.Value.Length > 0) {
                        doChooseBPCode(true,oCCCodeItem.Value);
                    }
                    else {
                        moVehicleMaster.U_CCName = "";
                    }
                }
                catch (Exception Ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EREXGEN", null);
                    bSuccess = false;
                }
            }
            return bSuccess;
        }
        public bool doBelongsToCFLClickEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            if (!pVal.BeforeAction)
            {
                    doChooseBPCode(false,"");
            }
            return true;
        }

        public bool doSelectDriverEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)Items.Item("uBC_CDFDNU").Specific;
                SAPbouiCOM.ValidValue oValue = oCombo.Selected;
                string sDescription = oValue.Description;
                int iIndex = sDescription.IndexOf('-');
                if (iIndex != -1) {
                    sDescription = sDescription.Substring(iIndex + 1);
                }
                sDescription = sDescription.Trim();
                moVehicleMaster.U_Driver = sDescription;
            }
            return true;
        }

        public bool doWOOpenEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if ( pVal.BeforeAction ) {
                string sWOCode = moVehicleMaster.U_LWO;
                ArrayList oData = new ArrayList();
                oData.Add("IDHWO");
                oData.Add(sWOCode);
                doOpenModalForm("IDHJOBS", oData);
               
            }
            return true;
        }

        public bool doDISPORDOpenEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.Before_Action) {
                string sDISPORDRowCode = moVehicleMaster.U_LWT;
                string sDISPORDCode = (string)idh.bridge.lookups.Config.INSTANCE.doLookupTableField("[" +IDH_DISPROW.TableName+ "]", IDH_DISPROW._JobNr, IDH_DISPROW._Code + " = '" + sDISPORDRowCode +"'");
                if (string.IsNullOrEmpty(sDISPORDCode))
                    sDISPORDCode = "-1";
                if (sDISPORDRowCode.Trim().Length != 0) {
                    ArrayList oData = new ArrayList();
                    oData.Add("DoUpdate");
                    oData.Add(sDISPORDCode);
                    setSharedData("ROWCODE", sDISPORDRowCode);
                    doOpenModalForm("IDH_DISPORD", oData);
                }
            }
            return true;
        }

#region FolderEvents
        public bool doHandleModeChange(SAPbouiCOM.BoFormMode oSBOMode) {
            if (oSBOMode == BoFormMode.fm_ADD_MODE) {
                moVehicleMaster.doClearBuffers();
                moVehicleMaster.doAddEmptyRow(true, true);
                //EnableIAllEditItems(moEditDisabled);
            } else if (oSBOMode == BoFormMode.fm_FIND_MODE) {
              //  doSwitchToFind();
                doClearFormDFValues();
                doClearFormUFValues();
                //DisableAllEditAndButtonItems(ref moFindEnabled);
            }
            return true;
        }

        public bool doGeneralFolder(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            PaneLevel = 1;
            return true;
        }
        public bool doServiceCostFolder(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            PaneLevel = 2;
            return true;
        }
        public bool doDriverCrewFolder(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            PaneLevel = 3;
            return true;
        }
        public bool doNotesFolder(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            PaneLevel = 4;
            return true;
        }
        #endregion

#endregion

#region MenuEventHandlers
        public bool doMenuAddEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                moVehicleMaster.doClearBuffers();
            //    doClearAll();
            ////    moVehicleMaster.doAddEmptyRow(true, true);
            ////    //moDOR.doAddEmptyRow(true, true);
                Mode = BoFormMode.fm_ADD_MODE;
            }
            ////doClearAll();

            return true;
        }
        public bool doMenuFindBUttonEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                Mode = BoFormMode.fm_FIND_MODE;
            }
            return true;
        }
        public bool doMenuBrowseEvents(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if ( pVal.BeforeAction ) {
                Freeze(true);
                try {
                    string sCode = moVehicleMaster.Code;
                    doClearFormUFValues();
                } catch (Exception ex) {
                    //DataHandler.INSTANCE.doExceptionError(ex.ToString(), "Error handeling the Browsing event.");
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHANB", null);
                } finally {
                    Freeze(false);
                }
            }
            return true;
        }
#endregion
#endregion

#region OldEventHandlers
        /**
         * Most of these are replaced with Handlers as above... only use these if there is really no Event or in special events
         */

        /*
         * Handle all the Menu Events.
         * Return True if the Event must be handled by the other Objects
         */
        public override bool doMenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * The Event Handler that will receive all Activated Events for this Form
         * Return True if the Event must be handled by the other Objects
         */
        public override bool doItemEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Send By Custom Controls
         */
        public override bool doCustomItemEvent(ref IDHAddOns.idh.events.Base pVal) {
            return true;
        }

        /*
         * Handle the Request for a Help File
         */
        public override bool doHelpFile(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Handles the right click event
         */
        public override bool doRightClickEvent(ref SAPbouiCOM.ContextMenuInfo pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Handle the Button One Pressed Event
         */
        public override void doButtonID1(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction)
            {
                string sValidation = moVehicleMaster.doValidate_VehReg();
                if (!string.IsNullOrEmpty(sValidation))
                {
                    //DataHandler.INSTANCE.doError(sValidation);
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError(sValidation, "ERUSGEN", new string[] { sValidation });
                    setFocus("uBC_VR2");
                    BubbleEvent = false;
                    return;
                }
                sValidation = moVehicleMaster.doValidate_TRWTE1();
                if (!string.IsNullOrEmpty(sValidation))
                {
                    //DataHandler.INSTANCE.doError(sValidation);
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError(sValidation, "ERUSGEN", new string[] { sValidation });
                    setFocus("uBC_TRWTE1");
                    BubbleEvent = false;
                    return;
                }

            }
            ////    if (moVehicleMaster.U_VehReg.Trim().Equals(string.Empty)) {
            ////        DataHandler.INSTANCE.doError("Vehicle Registration is required.");
            ////        doSetFocus("uBC_VR2");
            ////        BubbleEvent = false;
            ////        return;
            ////    }
            ////    IDH_VEHMAS2 ObjVehMas = new IDH_VEHMAS2();
              
            ////   if( SBOForm.Mode==SAPbouiCOM.BoFormMode.fm_ADD_MODE &&  ObjVehMas.getByKey((moVehicleMaster.U_VehReg.Trim())))
            ////   {
            ////        DataHandler.INSTANCE.doError("Vehicle already added.");
            ////        doSetFocus("uBC_VR2");
            ////        BubbleEvent = false;
            ////        return;
            ////   }
            //    if (moVehicleMaster.U_Tarre != 0.0 && (moVehicleMaster.U_TRWTE1.Year <= 1900 || moVehicleMaster.U_TRWTE1.AddMonths(Config.ParameterAsInt("TARWTEXP", 6, true)) < DateTime.Today))
            //    {
            //        DataHandler.INSTANCE.doError("Vahicle Tare weight has been expired. Please update the tare weight.");
            //        doSetFocus("uBC_TRWTE1");
            //        BubbleEvent = false;
            //        return;
            //    }
            //}
        }
        /* 
         * Handle Button Two Presssed Event
         */
        public override void doButtonID2(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        }

        /* 
         * Return As Canceled If this was Opened as a Dialog from another form
         */
        public override void doReturnCanceled(string sModalFormType) {
        }

        ///* 
        // * Return as Ok If this was Opened as a Dialog from another form setting the Data in the Global Shared Buffer
        // * This will be called from the Conroller after all the other Button 1 Handlers and the BubbleEvent is still True.
        // */
        //public override void doReturnFromModalShared(bool bState) {
        //}

        //*** Return As Normal
        public override void doReturnNormal() {
        }

        /* 
         * Handle the Cancel Event when a Dialog returns to this form.
         */
        public override void doHandleModalCanceled(string sModalFormType) {
            if (sModalFormType == "IDHCSRCH")  {
                string sTarget = (string)getSharedData("TRG");
                if (sTarget == "BP")    {
                    setFocus("uBC_VEHBTO");
                }
            }
        }

        /* 
         * Handle the Return from a Dialog called from this Form with the data in oData
         */
        public override void doHandleModalBufferedResult(ref object oData, string sModalFormType, string sLastButton) {
        }

        /* 
         * Handle the Return from a Dialog called from this Form with the data in the Global Shared Buffer
         */
        public override void doHandleModalResultShared(string sModalFormType, string sLastButton) {
            if (sModalFormType == "IDHCSRCH") {
                string sTarget = (string)getSharedData("TRG");
                string sCardCode = (string)getSharedData("CARDCODE");
                string sCardName = (string)getSharedData("CARDNAME");

                //setUFValue("uBC_VEHBTO", sCardCode);
                moVehicleMaster.U_CCCrdCd = sCardCode;
                //setUFValue("uBC_CarrNm", sCardName);
                moVehicleMaster.U_CCName= sCardName;
            } 
        }
#endregion

#region RestOfLogic
        protected virtual void doChooseBPCode(bool bFilter, String sCardCode) {
            //string sCardCode = (string)getUFValue("uBC_VEHBTO");

            if (bFilter) {
                setSharedData("IDH_BPCOD", sCardCode);
            } else {
                setSharedData("IDH_BPCOD", "");
            }

            setSharedData("TRG", "BP");
            setSharedData("IDH_TYPE", "F-S");
            setSharedData("SILENT", "SHOWMULTI");

            doOpenModalForm("IDHCSRCH");
        }


        /*
         * Clear the form Data
         */
        private void doClearAll() {
            doClearFormDFValues();
        }

        /**
         * Fill the ComboBoxes
         */
        private void doFillCombos() {
            FillCombos.VehTyp(Items.Item("uBC_VEHT").Specific);
            FillCombos.VehTyp(Items.Item("uBC_CVEHT").Specific);
            FillCombos.VehClass(Items.Item("uBC_CVEHC").Specific);
            FillCombos.WFStatusCombo(Items.Item("uBC_CWFS").Specific);
            //FillCombos.WR1OrderTypeCombo(Items.Item("uBC_CWR1OT").Specific);
            FillCombos.AvailableCombo(Items.Item("uBC_CAVAIL").Specific);
            doFillCombo((SAPbouiCOM.ComboBox)Items.Item("uBC_CREAS").Specific, "[@IDH_VMREAS]", "Code", "U_Reason");
            FillCombos.EmployeesCombo(Items.Item("uBC_CDFDNU").Specific);
            FillCombos.EmployeesCombo(Items.Item("uBC_CDFCN1").Specific);
            FillCombos.EmployeesCombo(Items.Item("uBC_CDFCN2").Specific);
            FillCombos.EmployeesCombo(Items.Item("uBC_CDFCN3").Specific);
            FillCombos.VehicleMasterMarkDocs(Items.Item("uBC_MarkAc").Specific);
        }
#endregion
    
    }
}

