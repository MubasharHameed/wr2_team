﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.forms.oo;
using com.idh.bridge.data;
using com.idh.dbObjects.User;
using IDHAddOns.idh.controls;
using com.uBC.utils;
using com.idh.controls;
using com.idh.bridge.utils;
using com.uBC.forms.fr3;
using com.idh.dbObjects.strct;

namespace com.uBC.forms.fr3 {
    public class RouteClosure : com.idh.forms.oo.Form
    {
        private IDH_ROUTECL moRTCL;

        //protected bool mbIsFirstTime = true;

        //public string FilterRouteCl{
        //    set { setUFValue("RouteClosure", value); }
        //}

        //public IDH_ROUTECL RCMaster {
        //    get { return (IDH_ROUTECL)moDBObject; }
        //}

        public RouteClosure(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm)
        {
            doSetHandlers();

            //Set the Date Object for this Form
            moRTCL = new IDH_ROUTECL(oIDHForm, oSBOForm);
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_ROUTECL", sParMenu, iMenuPos, "RouteClosure.srf", false, true, false, "Route Closure", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        private void doSetHandlers()
        {
            Handler_Button_Add = new ev_Item_Event(doAddUpdateEvent);
            Handler_Button_Update = new ev_Item_Event(doAddUpdateEvent);

            //Folder Press Events
            addHandler_ITEM_PRESSED("IDH_TABMAT", new ev_Item_Event(doAddUpdateEvent));
            addHandler_ITEM_PRESSED("IDH_TABCAR", new ev_Item_Event(doAddUpdateEvent));
            addHandler_ITEM_PRESSED("IDH_TABCUS", new ev_Item_Event(doAddUpdateEvent));
            addHandler_ITEM_PRESSED("IDH_TABPRO", new ev_Item_Event(doAddUpdateEvent));
            addHandler_ITEM_PRESSED("IDH_TABSIT", new ev_Item_Event(doAddUpdateEvent));
            addHandler_ITEM_PRESSED("IDH_TABMLA", new ev_Item_Event(doAddUpdateEvent));
            addHandler_ITEM_PRESSED("IDH_TABSUM", new ev_Item_Event(doAddUpdateEvent));

            ////Menu Events
            //Handler_Menu_NAV_ADD = new et_Menu_Event(doMenuAddEvent);
        }

        public bool doAddUpdateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            if (pVal.BeforeAction)
            {
                //Future Updater if ( moVehicleMaster.doProcessData( )) {
                if (doAutoSave(IDH_ROUTECL._Code, moRTCL.AutoNumKey))
                {
                    doLoadData();
                }
            }
            return true;
        }

//        protected override void doInitialize() {
//            moDBObject = new IDH_ROUTECL();
//        }

//#region FormOpenCreateFunctions
//        /** 
//         * This Function will be called by the controller to allow the class to last minute Form Layout adjustments before it gets displayed
//         * All changes made in hete will be cached if this is a cached form, the method will not be called again once the form has benn cached.
//         */
//        public override void doCompleteCreate(ref bool BubbleEvent) {
//            base.doCompleteCreate(ref BubbleEvent);

//            DataBrowser.BrowseBy = "IDH_RCLCD"; // "uBC_VEHREG";
//            SupportedModes = -1;
//            AutoManaged = true;

//            //Items.Item("uBC_CVEHTY").DisplayDesc = true;
//            //Items.Item("uBC_CWFS").DisplayDesc = true;

//            //Items.Item("uBC_VEHREG").AffectsFormMode = false;
//            //Items.Item("uBC_CVEHTY").AffectsFormMode = false;
//            //Items.Item("uBC_VEHDIS").AffectsFormMode = false;
//            //Items.Item("uBC_CWFS").AffectsFormMode = false;
//            ////Items.Item("uBC_VEHREG").AffectsFormMode = false;
//            //Items.Item("uBC_CARR").AffectsFormMode = false;
//        }

//        public override void doBeforeLoadData() {
//            if (isModal())
//                mbIsSearch = true;

//            if (mbIsSearch) {
//                SupportedModes = (int)SAPbouiCOM.BoFormMode.fm_OK_MODE;
//                Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
//                SkipFormSettings = true;
//            } else {
//                SupportedModes = -1; // (int)SAPbouiCOM.BoFormMode.fm_FIND_MODE;
//                Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE;
//            } 

//            base.doBeforeLoadData();
//            //moAdminGrid.doApplyExpand(IDHGrid.EXPANDED);
//            //moAdminGrid.setOrderValue(IDH_ROUTECL._Code);
//        }

//        /* 
//         * Load the Form Data
//         */
//        public override void doLoadData() {
//            base.doLoadData();
//            if (mbIsFirstTime) 
//                doFillCombos();

//            doFillGridCombos();
            
//            mbIsFirstTime = false;
//        }

//        /*
//         * Set the Filter Fields
//         */
//        public override void doSetFilterFields() {
//            //moAdminGrid.doAddFilterField("uBC_VEHREG", IDH_ROUTECL._Code, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30);
//            //moAdminGrid.doAddFilterField("uBC_CVEHTY", IDH_ROUTECL._Code, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30);
//            //moAdminGrid.doAddFilterField("uBC_VEHDIS", IDH_ROUTECL._Code, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30);
//            //moAdminGrid.doAddFilterField("uBC_CWFS", IDH_ROUTECL._Code, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30);
//            ////moAdminGrid.doAddFilterField("uBC_CUS", IDH_ROUTECL._Code, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30);
//            //moAdminGrid.doAddFilterField("uBC_CARR", IDH_ROUTECL._Code, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30);
//        }

//        /*
//         * Set the List Fields
//         */
//        public override void doSetListFields() {
//            ////Hide the rest
//            //for (int i = 0; i < moDBObject.getColumnCount(); i++) {
//            //    DBFieldInfo oFieldInfo = moDBObject.getFieldInfo(i);
//            //    moAdminGrid.doAddListFieldNotPinned(oFieldInfo.FieldName, oFieldInfo.Description, false, 0, null, null);
//            //}
//            //moAdminGrid.doInsertListField(IDH_ROUTECL._Code, "Type", false, 100, ListFields.LISTTYPE_COMBOBOX, null, 0, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, false);
//            //moAdminGrid.doInsertListField(IDH_ROUTECL._Code, "Vehicle Registration", false, 100, ListFields.LISTTYPE_IGNORE, null, 2, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, false);
//            //moAdminGrid.doInsertListField(IDH_ROUTECL._Code, "Vehicle Description", false, 100, ListFields.LISTTYPE_IGNORE, null, 3, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, false);
//            //moAdminGrid.doInsertListField(IDH_ROUTECL._Code, "Tare", false, 50, ListFields.LISTTYPE_IGNORE, null, 4, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, false);
//            //moAdminGrid.doInsertListField(IDH_ROUTECL._Code, "Carrier", false, 100, ListFields.LISTTYPE_IGNORE, null, 5, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner, false, false);
//            //moAdminGrid.doInsertListField(IDH_ROUTECL._Code, "Available", false, 30, ListFields.LISTTYPE_COMBOBOX, null, 6, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, false);

//            moAdminGrid.doAddListField(IDH_ROUTECL._Code, "Type", false, 100, ListFields.LISTTYPE_COMBOBOX, null, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true, SAPbouiCOM.BoColumnSumType.bst_None, SAPbouiCOM.BoColumnDisplayType.cdt_Edit); //,0, false);
//            moAdminGrid.doAddListField(IDH_ROUTECL._Code, "Vehicle Registration", false, 100, ListFields.LISTTYPE_IGNORE, null, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true, SAPbouiCOM.BoColumnSumType.bst_None, SAPbouiCOM.BoColumnDisplayType.cdt_Edit);//, 1, false);
//            moAdminGrid.doAddListField(IDH_ROUTECL._Code, "Vehicle Description", false, 100, ListFields.LISTTYPE_IGNORE, null, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true, SAPbouiCOM.BoColumnSumType.bst_None, SAPbouiCOM.BoColumnDisplayType.cdt_Edit);//, 2, false);
//            moAdminGrid.doAddListField(IDH_ROUTECL._Code, "Tare", false, 50, ListFields.LISTTYPE_IGNORE, null, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true, SAPbouiCOM.BoColumnSumType.bst_None, SAPbouiCOM.BoColumnDisplayType.cdt_Edit); //, 3, false);
//            moAdminGrid.doAddListField(IDH_ROUTECL._Code, "Carrier", false, 100, ListFields.LISTTYPE_IGNORE, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner, false, true, SAPbouiCOM.BoColumnSumType.bst_None, SAPbouiCOM.BoColumnDisplayType.cdt_Edit); //, 4, false);
//            moAdminGrid.doAddListField(IDH_ROUTECL._Code, "Available", false, 30, ListFields.LISTTYPE_COMBOBOX, null, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true, SAPbouiCOM.BoColumnSumType.bst_None, SAPbouiCOM.BoColumnDisplayType.cdt_Edit); //, 5, false);

//            //Hide the rest
//            for (int i = 0; i < moDBObject.getColumnCount(); i++) {
//                DBFieldInfo oFieldInfo = moDBObject.getFieldInfo(i);
//                moAdminGrid.doAddListFieldNotPinned(oFieldInfo.FieldName, oFieldInfo.Description, false, 0, null, null);
//            }

//            //bool bInSearch = mbIsSearch;
//            //mbIsSearch = true;
//            //base.doSetListFields();
//            //mbIsSearch = bInSearch;
//        }
//#endregion

//#region Events
//        protected override void doSetHandlers() {
//            base.doSetHandlers();
//            Handler_VALIDATE_CHANGED = new ev_Item_Event(FilterFieldChangeEvent);
//            Handler_COMBO_SELECT = new ev_Item_Event(FilterFieldChangeEvent);
//        }

//        protected override void doSetGridHandlers() {
//            base.doSetGridHandlers();

//            moAdminGrid.Handler_GRID_DOUBLE_CLICK = new IDHGrid.ev_GRID_EVENTS(doRowDoubleClickEvent);
//        }
//#endregion
//#region ItemEventHandlers
//        public bool doFindEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
//            if (pVal.BeforeAction) {
//                doLoadData();
//                BubbleEvent = false;
//            } 
//            return false;
//        }

//        public bool doRowDoubleClickEvent(ref IDHAddOns.idh.events.Base pVal) {
//            if (pVal.BeforeAction) {
//            } else {
//                if (!mbIsSearch) {
//                    IDH_ROUTECL oRouteCl = ((IDH_ROUTECL)moDBObject);
//                    string sVehicleCode = oRouteCl.Code;
//                    string sVehReg = oRouteCl.Code;

//                    VehicleMaster oOOForm = new VehicleMaster(null, SBOForm.UniqueID, null);
//                    oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleOKReturn);
//                    if (!sVehicleCode.Equals("-1"))
//                        oOOForm.VehicleReg = sVehReg;
//                    oOOForm.doShowModal();
//                } else {
//                    doReturnFromModalShared(true);
//                    if (Handler_DialogOkReturn != null) {
//                        Handler_DialogOkReturn(this);
//                    }

//                    if (Handler_DialogButton1Return != null) {
//                        Handler_DialogButton1Return(this);
//                    }
//                    bool bBubble = true;
//                    IDHForm.doEndForm(SBOForm, ref bBubble);
//                    return false;
//                }
//            }
//            return true;
//        }

//        public bool FilterFieldChangeEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
//            if (!pVal.BeforeAction) {
//                doLoadData();
//                BubbleEvent = false;
//            } 
//            return false;
//        }

//        public bool doHandleOKReturn(com.idh.forms.oo.Form oOForm) {
//            doLoadData();
//            return true;
//        }

//#endregion
      
//#region doFillCombos
//        private void doFillCombos() {
//            //FillCombos.VehTyp(Items.Item("uBC_CVEHTY").Specific,"", "Any");
//            //FillCombos.VehTyp(Items.Item("uBC_CWFS").Specific, "", "Any");
//        }

//        private void doFillGridCombos() {
//            //SAPbouiCOM.ComboBoxColumn oCombo;

//            //int iIndex = moAdminGrid.doIndexFieldWC(IDH_ROUTECL._Code);
//            //oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(iIndex);
//            //oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
//            //FillCombos.VehTyp(oCombo);

//            //iIndex = moAdminGrid.doIndexFieldWC(IDH_ROUTECL._Code);
//            //oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(iIndex);
//            //oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
//            //FillCombos.AvailableCombo(oCombo);
//        }
//#endregion

    
    }
}

