﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.dbObjects.User;
using IDHAddOns.idh.controls;
using com.uBC.utils;
using com.idh.controls;
using com.idh.bridge.utils;

namespace com.uBC.forms.fr3.admin {
    public class VMReason : com.uBC.forms.fr3.dbGrid.Base {

        public VMReason(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
                mbIsSearch = false;
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDHVMREAS", sParMenu, iMenuPos, "admin.srf", false, true, false, "Vehicle Reason", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        protected override void doInitialize() {
            moDBObject = new IDH_VMREAS();
        }

#region FormOpenCreateFunctions
        /*
         * Do Complete the Form
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            base.doCompleteCreate(ref BubbleEvent);
        }

       /* 
        * Do the final form steps to show before loaddata
        */
        public override void doBeforeLoadData() {
            base.doBeforeLoadData();
        }

        /* 
         * Load the Form Data
         */
        public override void doLoadData() {
            base.doLoadData();
        }

        /*
         * Set the Filter Fields
         */
        public override void doSetFilterFields() {
        }

        /*
         * Set the List Fields
         */
        public override void doSetListFields() {
            moAdminGrid.doAddListField(IDH_VMREAS._Code, "Code", false, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_VMREAS._Name, "Name", true, 0, ListFields.LISTTYPE_IGNORE, null, -1);

            moAdminGrid.doAddListField(IDH_VMREAS._Reason, "Reason", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);
        }
#endregion

#region Events
        protected override void doSetHandlers() {
            base.doSetHandlers();
        }

        protected override void doSetGridHandlers() {
            base.doSetGridHandlers();

            //moAdminGrid.Handler_GRID_FIELD_CHANGED = new IDHGrid.ev_GRID_EVENTS(doFieldChangeEvent); 
        }
#endregion 

#region ItemEventHandlers
#endregion

#region fillCombos
#endregion
    }
}
