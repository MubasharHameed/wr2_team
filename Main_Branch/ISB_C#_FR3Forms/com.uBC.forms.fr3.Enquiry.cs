﻿using System.IO;

using System.Collections;
using System;
using SAPbouiCOM;

//Needed for the Framework controls
using IDHAddOns.idh.controls;
using IDHAddOns;
using com.idh.bridge.data;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.idh.dbObjects.User;
using com.uBC.utils;
using com.idh.controls;

namespace com.uBC.forms.fr3 {
    public class Enquiry : com.idh.forms.oo.Form {
        private IDH_ENQUIRY moEnquiry;
        public IDH_ENQUIRY DBEnquiry {
            get { return moEnquiry; }
        }
        private DBOGrid moGrid;
        private string _EnquiryCode = "-1";
        private bool mbFromActivity = false;
        private bool mbAddUpdateOk = false;
        private bool mbLoadEnquiryById = false;
        private Int32 mbActivityID = 0;
        public Enquiry(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
                doInitialize();
                doSetHandlers();
            //Set the Data Object for this Form
            //moEnquiry = new IDH_ENQUIRY(oIDHForm, oSBOForm);
            //moEnquiry.MustLoadChildren = true;
        }
        
        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_ENQ", sParMenu, 80, "uBC_Enquiry.srf", false, true, false, "Enquiry", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            owForm.doEnableHistory(IDH_ENQUIRY.TableName); 
            return owForm;
        }

        protected void doInitialize() {
            moEnquiry = new IDH_ENQUIRY(IDHForm, SBOForm);
            moEnquiry.MustLoadChildren = true;

            moEnquiry.AutoAddEmptyLine = false;
            moEnquiry.EnquiryItems.AutoAddEmptyLine = true;
        }

#region Properties
        //private string moEnquiryID;
        public Int32 ActivityID
        {
            get {
                if (moEnquiry.U_ClgCode != 0)
                    mbActivityID = moEnquiry.U_ClgCode;
                return mbActivityID; }
            set { mbActivityID = value; }
        }
        public string EnquiryID
        {
            get { return moEnquiry.Code; }
            set { _EnquiryCode = value; }
        }
        public bool bLoadEnquiry
        {
            get { return mbLoadEnquiryById; }
            set { mbLoadEnquiryById = value; }
        }
        public bool bAddUpdateOk
        {
            get { return mbAddUpdateOk; }
            set { mbAddUpdateOk = value; }
        }
        public bool bFromActivity 
        {
            get { return mbFromActivity; }
            set { mbFromActivity = value; }
        }
#endregion
#region FormOpenCreateFunctions
        /** 
         * This Function will be called by the controller to allow the class to last minute Form Layout adjustments before it gets displayed
         * All changes made in hete will be cached if this is a cached form, the method will not be called again once the form has benn cached.
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            /**
             * Browser By is set using the Form Item Id for the Data Key field
             */
            DataBrowser.BrowseBy = "IDH_DOCNUM"; // "uBC_VEHREG";
            SupportedModes = -1;
            AutoManaged = true;
           
            // doSetFilterFields();
            //if (!mbLoadEnquiryById)
            //    Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE;
            if (mbFromActivity) { 
            //disable Menue Buttons
                SBOForm.EnableMenu("1281", false);
                SBOForm.EnableMenu("1282", false);
                SBOForm.EnableMenu("1290", false);
                SBOForm.EnableMenu("1288", false);
                SBOForm.EnableMenu("1289", false);
                SBOForm.EnableMenu("1291", false);
            }
        }

        /** 
         * Do the final form steps to show before loaddata
         */
        public override void doBeforeLoadData() {
            try
            {
                moEnquiry.SBOForm = SBOForm;
                moEnquiry.doAddEmptyRow(true, true);

                Title = IDHForm.gsTitle;
                moGrid = new DBOGrid(IDHForm, SBOForm, "LINESGRID", moEnquiry.EnquiryItems);
                moGrid.doSetDeleteActive(true);
                moGrid.doAddEditLine ( true);
                //  moEnquiry.EnquiryItems.AutoAddEmptyLine = false;
                doSetGridHandlers();
                doGridLayout();

                doFillCombos();
                moGrid.doApplyRules();
            }
            catch (Exception ex)
            {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }
        }

        /** 
         * Load the Form Data
         */
        public override void doLoadData() {
            doFillGridCombos();
        }
        /** 
         * Do the final form steps to show after loaddata
         */
        public override void doFinalizeShow() {
            Items.Item("IDH_DOCNUM").SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable,
            (int)SAPbouiCOM.BoFormMode.fm_ADD_MODE + (int)SAPbouiCOM.BoFormMode.fm_EDIT_MODE +
            (int)SAPbouiCOM.BoFormMode.fm_OK_MODE + (int)SAPbouiCOM.BoFormMode.fm_PRINT_MODE +
            (int)SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + (int)SAPbouiCOM.BoFormMode.fm_VIEW_MODE,
            SAPbouiCOM.BoModeVisualBehavior.mvb_False);
          //  Items.Item("IDH_DOCNUM").SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, (int)SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + (int)SAPbouiCOM.BoFormMode.fm_ADD_MODE +(int)SAPbouiCOM.BoFormMode.fm_OK_MODE  , BoModeVisualBehavior.mvb_False);
            Items.Item("IDH_DOCNUM").SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, (int)SAPbouiCOM.BoFormMode.fm_FIND_MODE, BoModeVisualBehavior.mvb_True);
            base.doFinalizeShow();
            if (mbFromActivity && mbLoadEnquiryById)
            {
                if (_EnquiryCode != "0") {
                    if (moEnquiry.getByKey(_EnquiryCode)) {
                        Freeze ( true);
                        FillCombos.FillState(Items.Item("IDH_STATE").Specific, moEnquiry.U_Country, null);
             
                        doSwitchToEdit();
                        moGrid.doApplyRules();
                        doFillGridCombos();
                        Freeze ( false);
                    }
                }

                // Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
            }
            else
            {
                Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE;
            }
        }
#endregion

#region Events
        /**
         * Register all your event Handlers in here.
         * Handler_ALL_EVENTS 0 All events, for use when filtering events 
         * Handler_ITEM_PRESSED 1 The main mouse button was clicked on one of the following: 
         * 		Tab 
         * 		Button 
         * 		Option button 
         * 		Check box (standalone or within a cell)
         * 		This event occurs when a mouse is released within an item, that is, mouse up.
         * Handler_KEY_DOWN 2 A key was pressed. 
         * Handler_GOT_FOCUS 3 An item received focus. 
         * Handler_LOST_FOCUS 4 An item lost focus. 
         * Handler_LOST_FOCUS_CHANGED 4 An item lost focus with the value changed
         * Handler_COMBO_SELECT 5 A value was selected in a combo box. 
         * Handler_CLICK 6 The main mouse button was clicked on an item, that is, mouse down. 
         * Handler_DOUBLE_CLICK 7 The main mouse button was double-clicked on an item. 
         * Handler_MATRIX_LINK_PRESSED 8 A link arrow in a matrix was pressed. 
         * Handler_MATRIX_COLLAPSE_PRESSED 9 A matrix list was collapsed or expanded. 
         * Handler_VALIDATE 10 An item lost focus and validation is required.
         * Handler_VALIDATE_CHANGED 10 An item lost focus and validation is required - only when the data has changed.
         * Handler_MATRIX_LOAD 11 Data was loaded from the database into a matrix data source. 
         * 		This event occurs for user-defined matrix objects only. If a form contains two matrix objects, the system generates two events.
         * Handler_DATASOURCE_LOAD 12 Data was loaded from the GUI into a matrix data source. 
         * 		This event occurs once per matrix. If a form has two matrix objects, the system generates two events.
         * Handler_FORM_LOAD 16 A form was opened (FormDataEvent). 
         * Handler_FORM_UNLOAD 17 A form was closed. 
         * Handler_FORM_ACTIVATE 18 A form received focus. 
         * Handler_FORM_DEACTIVATE 19 A form lost focus. 
         * Handler_FORM_CLOSE 20 A form is about to be closed. 
         * Handler_FORM_RESIZE 21 A form has been resized. 
         * Handler_FORM_KEY_DOWN 22 A key was pressed when no form had the focus. 
         * Handler_FORM_MENU_HILIGHT 23 A form is modifying the status of toolbar items, that is, enabling and disabling icons. 
         * 		By default, these events are not thrown. You can activate these events by setting an event filter.
         * Handler_PRINT 24 A print preview was requested for a report or document (PrintEvent). 
         * 		This event lets you exit from the application print operation and use your own printing.
         * Handler_PRINT_DATA 25 A print preview was requested for a report (ReportDataEvent).  
         * 		This event lets you get the report data in XML format.
         * Handler_CHOOSE_FROM_LIST 27 A ChooseFromList event occurred, as follows:  
         * 		The Before event occurs before the ChooseFromList form is displayed. If the BubbleEvent parameter is set to False, the form is not displayed. 
         * 		The After event occurs after the user makes a selection or chooses Cancel.		 
         * Handler_RIGHT_CLICK 28 The right mouse button was clicked on an item (RightClickEvent). 
         * Handler_MENU_CLICK 32 The main mouse button was released on a menu item without submenus. 
         * 		For future use.
         * Handler_FORM_DATA_ADD 33 A record in a business object was added (FormDataEvent). 
         * Handler_FORM_DATA_UPDATE 34 A record in a business object was updated (FormDataEvent). 
         * Handler_FORM_DATA_DELETE 35 A record in a business object was deleted (FormDataEvent). 
         * Handler_FORM_DATA_LOAD 36 A record in a business object was loaded -- via browse, link button, or find (FormDataEvent). 
         * Handler_PICKER_CLICKED 37 A Picker event occurred, as follows:  
         * 		The Before event occurs before the Picker form is displayed. If the BubbleEvent parameter is set to False, the picker is not displayed. 
         * 		The After event occurs after the user makes a selection or chooses Cancel.
         * Handler_GRID_SORT 38 A grid column was sorted, either by a user clicking the column title or an add-on calling the Sort method of the Grid object. 
         * Handler_Drag 39 The item was dragged and dropped at the position you want it to appear. 
         * Handler_PRINT_LAYOUT_KEY 80 A print or print preview was requested for an add-on form, and the layout needs the key of the add-on form 		
         * 
         * To Add a Item PressEvent for a specific Control use 
         * addHandler_ITEM_PRESSED("ITEMID", new ev_Item_Event(EventFunction));
         *      e.g. addHandler_ITEM_PRESSED("uBCCardSR", new ev_Item_Event(doHandleCardCodeSearch));
         *      
         *           public bool doHandleCardCodeSearch(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
         *               return false;
         *           }
         *           
         *  The following Special Event Handlers can be set when using the SBO default where Button 1 is OK 
         *  Handler_Button_Ok - When the Form is in Ok or View Mode
         *  Handler_Button_Find - When the Form is in Find Mode
         *  Handler_Button_Add - When the Form is in Add Mode
         *  Handler_Button_Update - When the Form is in Update Mode
         *   
         * The Menu Events
         * Handler_Menu_NAV_ADD
		 * Handler_Menu_NAV_FIND
		 * Handler_Menu_NAV_FIRST
		 * Handler_Menu_NAV_LAST
		 * Handler_Menu_NAV_NEXT
         * Handler_Menu_NAV_PREV
         * --------> If the BubbleEvent is not set to False the normal Overridable Function doMenuEvent will be called 
         *           doMenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent)
         *
         * Special Handler that will be called if this form was opened as a Dialog (Opened from another Form)
         * Handler_DialogButton1Return - When Button 1 was pressed irrespective of the Form Mode
         * Handler_DialogOkReturn - When Button 1 was Pressed and Form was in Ok/View Mode
         * Handler_DialogCancelReturn = When Button 2 was Pressed
         * --------> This will only be called if all the other Button 1 /2 events where called including the Overridable 
         *           Function doButtonID1 and the Bubble event is still true.
         */
        private void doSetHandlers() {
            Handler_Button_Add = doAddUpdateEvent;
            Handler_Button_Update = doAddUpdateEvent;
            Handler_Button_Find = doFindEvent;
            Handler_FORM_DATA_LOAD = doHandleFormDataLoad;
            addHandler_ITEM_PRESSED("2", doCancelButton);
            addHandler_ITEM_PRESSED("IDH_FINDAD", doFindAddress);
            addHandler_ITEM_PRESSED("IDH_CRETBP", doCreateBP);
            addHandler_ITEM_PRESSED("IDH_ADRCFL", doSearchBPAddressCFL);
            addHandler_VALIDATE("IDH_CardNM", dovalidateBPEvent);
            addHandler_VALIDATE_CHANGED("IDH_CardNM", dovalidateBPChangeEvent);
            addHandler_COMBO_SELECT("IDH_CUNTRY", doHandleCountryCombo);
          //  Handler_FORM_CLOSE = doHandleFormClose;

         //   addHandler_ITEM_PRESSED("B1_Arrow", new ev_Item_Event(doWOOpenEvent));
          //  addHandler_ITEM_PRESSED("B2_Arrow", new ev_Item_Event(doDISPORDOpenEvent));

          //  addHandler_COMBO_SELECT("uBC_CDFDNU", new ev_Item_Event(doSelectDriverEvent));

            //addHandler_VALIDATE_CHANGED("uBC_VEHBTO", new ev_Item_Event(doBelongsToCodeChangedEvent));
           // addHandler_CLICK("IDH_CRCFL", new ev_Item_Event(doBelongsToCFLClickEvent));
            //Menu Events
            Handler_Menu_NAV_ADD = doMenuAddEvent;
            Handler_Menu_NAV_FIRST = doMenuBrowseEvents;
            Handler_Menu_NAV_LAST = doMenuBrowseEvents;
            Handler_Menu_NAV_NEXT = doMenuBrowseEvents;
            Handler_Menu_NAV_PREV = doMenuBrowseEvents;
            Handler_Menu_NAV_FIND = doMenuFindBUttonEvent;
            Handler_FormMode_Changed = doHandleModeChange;
        }
        protected void doSetGridHandlers()
        {
           // moGrid.Handler_GRID_FIELD_CHANGED = doHandleGridFieldValueChanged;

            moGrid.Handler_GRID_SEARCH = doHandleGridSearch;
            moGrid.Handler_GRID_DOUBLE_CLICK = doHandleGridDoubleClick;

            //base.doSetGridHandlers();
           // moGrid.Handler_GRID_ROW_ADD_EMPTY = new IDHGrid.ev_GRID_EVENTS(doItemRowAdded);
            //moGrid.Handler_GRID_DOUBLE_CLICK = new IDHGrid.ev_GRID_EVENTS(doRowDoubleClickEvent);
        }
       
#region Handlers
        private bool doFindEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction)
            {
                Freeze(true);
                try
                {
                    string sHeader = (string)getFormDFValue(IDH_ENQUIRY._Code, true);
                    if (!moEnquiry.getByKey(sHeader))
                    {
                        moGrid.doAddEditLine(true);
                        moGrid.doApplyRules();
                        Mode = BoFormMode.fm_FIND_MODE;
                        doSwitchToFind();
                        BubbleEvent = false;
                        return false;
                    }
                    else
                    {
                        FillCombos.FillState(Items.Item("IDH_STATE").Specific, moEnquiry.U_Country, null);
                        doSwitchToEdit();
                       moGrid.doApplyRules();
                       doFillGridCombos();
                    }
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
                finally
                {
                    Freeze(false);
                }
            }
            
            return true;
        }
        private bool doSearchBPAddressCFL(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            if (!pVal.BeforeAction && moEnquiry.U_CardCode.Trim() != string.Empty)
            {
                doChooseBPAddress();
            }
            return true;
        }
        private bool doHandleCountryCombo(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction ) {

             //   SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_CUNTRY").Specific;
              //  SAPbouiCOM.ValidValue oValue = oCombo.Selected;
                FillCombos.FillState(Items.Item("IDH_STATE").Specific,moEnquiry.U_Country,null);
        
            }
            return true;
        }
        private bool dovalidateBPEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            if (pVal.BeforeAction && moEnquiry.U_CardName.Trim().IndexOf("*")>-1 )
            {
                doChooseBPCode();
            }
            return true;
        }

        private bool dovalidateBPChangeEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            if (pVal.BeforeAction && moEnquiry.U_CardName.Trim()!=string.Empty )
            {
                doChooseBPCode();
            }
            else if (pVal.BeforeAction && moEnquiry.U_CardName.Trim() == string.Empty)
            {
                moEnquiry.U_CardCode = "";
            }
            return true;
        }
        public bool doCreateBP(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                if (Mode != BoFormMode.fm_OK_MODE) {
                    com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage("UNSAVED");
                    BubbleEvent = false;
                } else if (moEnquiry.U_CardCode.Trim() != string.Empty && Config.INSTANCE.doGetBPName(moEnquiry.U_CardCode) != string.Empty) {
                    com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage(("ERPKEXIS"), new string[] { moEnquiry.U_CardCode });
                    BubbleEvent = false;
                }
            } else {
                doSetFocus("IDH_ADR1");
                com.uBC.forms.fr3.CreateBP oOOForm = new com.uBC.forms.fr3.CreateBP(null, SBOForm.UniqueID, null);
                //string sAddress = (getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._Address).ToString() + " " +
                //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._Street).ToString() + " " +
                //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._City).ToString() + " " +
                //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._Country).ToString() + " " +
                //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._State).ToString() + " " +
                //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._ZipCode).ToString()).Replace("  ", " ").Trim();

                oOOForm.setUFValue("IDH_CARDCD", moEnquiry.U_CardCode);
                oOOForm.setUFValue("IDH_CARDNM", moEnquiry.U_CardName);

                oOOForm.setUFValue("IDH_CARDTY", "C");

                oOOForm.setUFValue("IDH_ADRID", moEnquiry.U_Address);//Address ID 50
                oOOForm.setUFValue("IDH_STREET", moEnquiry.U_Street);//Street 100
                oOOForm.setUFValue("IDH_BLOCK", moEnquiry.U_Block);//Block 100
                oOOForm.setUFValue("IDH_CITY", moEnquiry.U_City);//City 100
                oOOForm.setUFValue("IDH_PCODE", moEnquiry.U_ZipCode);//State 3
                oOOForm.setUFValue("IDH_COUNTY", moEnquiry.U_County);//PostCode 20
                oOOForm.setUFValue("IDH_STATE", moEnquiry.U_State);//County 100
                oOOForm.setUFValue("IDH_CONTRY", moEnquiry.U_Country);//Country 3

                oOOForm.setUFValue("IDH_CNTPRS", moEnquiry.U_CName);//Contact Person 50
                oOOForm.setUFValue("IDH_EMAIL", moEnquiry.U_E_Mail);//Email 100
                oOOForm.setUFValue("IDH_PHONE", moEnquiry.U_Phone1);//Phone 20
                oOOForm.setUFValue("IDH_INFO", "Address:");//Phone 20
                oOOForm.setUFValue("IDH_INFO2", "Contact:");//Phone 20
                oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleCreateBPOKReturn);
                oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleCreateBPCancelReturn);
                oOOForm.doShowModal(SBOForm);
            }
            return true;
        }
        public bool doFindAddress(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction)
            {
                   com.uBC.forms.fr3.PostCode oOOForm =new com.uBC.forms.fr3.PostCode(null, SBOForm.UniqueID, null);
                   string sAddress = (getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._Address).ToString() + " " +
                                       getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._Street).ToString() + " " +
                                       getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._City).ToString() + " " +
                                       getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._Country).ToString() + " " +
                                       getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._State).ToString() + " " +
                                       getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._ZipCode).ToString()).Replace("  ", " ").Trim();
                
                oOOForm.setUFValue("IDH_ADDRES", sAddress);
                                oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandlePostCodeOKReturn);
                                oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doHandlePostCodeCancelReturn);
                                oOOForm.doShowModal(SBOForm);
            }
            return true;
        }
        //public bool doHandleFormClose(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            
        //    if (pVal.BeforeAction && Mode == BoFormMode.fm_UPDATE_MODE) {
        //        BubbleEvent = false;
        //    }
        //    return true;
        //}
        public bool doCancelButton(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                return false;
            }
            return true;
        }
        private bool doHandleFormDataLoad(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.ActionSuccess && pVal.BeforeAction == false) {
                Mode = BoFormMode.fm_OK_MODE;
                doSetFocus("IDH_ADR1");
                EnableItem(false, "IDH_DOCNUM");
            }
            return true;
        }
#endregion
        #region CreateBP
        public bool doHandleCreateBPOKReturn(com.idh.forms.oo.Form oDialogForm) {
            com.uBC.forms.fr3.CreateBP oOOForm = (com.uBC.forms.fr3.CreateBP)oDialogForm;
            moEnquiry.U_CardCode = oOOForm.CardCode;
            doSetFocus("IDH_ADR1");
            Mode = BoFormMode.fm_UPDATE_MODE;
            EnableItem(false, "IDH_CRETBP");
            EnableItem(false, "IDH_CardNM");
           
            return true;
        }
        public bool doHandleCreateBPCancelReturn(com.idh.forms.oo.Form oDialogForm) {
            com.uBC.forms.fr3.CreateBP oOOForm = (com.uBC.forms.fr3.CreateBP)oDialogForm;
            if (oOOForm.CardCode.Trim() != string.Empty) {
                moEnquiry.U_CardCode = oOOForm.CardCode;
                doSetFocus("IDH_ADR1");
                Mode = BoFormMode.fm_UPDATE_MODE;             
                EnableItem(false, "IDH_CRETBP");
                EnableItem(false, "IDH_CardNM");
            }
            return true;
        }
        #endregion
        #region CreateWasteItem
        public bool doHandleCreateWasteItemOKReturn(com.idh.forms.oo.Form oDialogForm) {
            com.uBC.forms.fr3.WasteItemValidations oOOForm = (com.uBC.forms.fr3.WasteItemValidations)oDialogForm;
            moGrid.doSetFieldValue(IDH_ENQITEM._ItemCode, oOOForm.iParentRow, oOOForm.ItemCode);
            if (Mode != BoFormMode.fm_ADD_MODE)
                Mode = BoFormMode.fm_UPDATE_MODE;
            return true;
        }
        public bool doHandleCreateWasteItemCancelReturn(com.idh.forms.oo.Form oDialogForm) {
            com.uBC.forms.fr3.WasteItemValidations oOOForm = (com.uBC.forms.fr3.WasteItemValidations)oDialogForm;
            if (oOOForm.ItemCode.Trim() != string.Empty && moGrid.doGetFieldValue(IDH_ENQITEM._ItemCode, oOOForm.iParentRow)==string.Empty) {
                moGrid.doSetFieldValue(IDH_ENQITEM._ItemCode, oOOForm.iParentRow, oOOForm.ItemCode);
                if (Mode != BoFormMode.fm_ADD_MODE)
                    Mode = BoFormMode.fm_UPDATE_MODE;
            }
            return true;
        }
        #endregion
        #region Postcode
        public bool doHandlePostCodeOKReturn(com.idh.forms.oo.Form oDialogForm)
        {
            com.uBC.forms.fr3.PostCode oOOForm = (com.uBC.forms.fr3.PostCode)oDialogForm;
            string Company = oOOForm.Company;
            string SubBuilding = oOOForm.SubBuilding;
            string BuildingNumber = oOOForm.BuildingNumber;
            string BuildingName = oOOForm.BuildingName;
            string SecondaryStreet = oOOForm.SecondaryStreet;
            string Street = oOOForm.Street;
            string Block = oOOForm.Block;
            ////Neighbourhood
            string District = oOOForm.District;
            string City = oOOForm.City;
            string Line1 = oOOForm.Line1;
            string Line2 = oOOForm.Line2;
            string Line3 = oOOForm.Line3;
            string Line4 = oOOForm.Line4;
            string Line5 = oOOForm.Line5;
            string AdminAreaName = oOOForm.AdminAreaName;
            ////AdminAreaCode
            ////Province
            string ProvinceName = oOOForm.ProvinceName;
            string ProvinceCode = oOOForm.ProvinceCode;
            string PostalCode = oOOForm.PostalCode;
            string CountryName = oOOForm.CountryName;
            string CountryIso2 = oOOForm.CountryIso2;
            ////CountryIso3
            ////CountryIsoNumber
            ////SortingNumber1
            ////SortingNumber2
            ////Barcode
            string POBoxNumber = oOOForm.POBoxNumber;
            string Label = oOOForm.Label;
            string Type = oOOForm.Type;

            moEnquiry.U_Address = (SubBuilding + ' ' + BuildingNumber + ' ' + BuildingName).Replace("  "," ");
            moEnquiry.U_Street = SecondaryStreet  +  Street;
            moEnquiry.U_Block = Block;
            moEnquiry.U_City= City;
            moEnquiry.U_ZipCode= PostalCode;
            moEnquiry.U_County = AdminAreaName;
            
            moEnquiry.U_State = ProvinceCode;
            moEnquiry.U_Country = CountryIso2;
            com.idh.bridge.DataRecords oRecords = null;
            try {
                string sQry = "Select Code from OCRY where Code Like '" + CountryIso2 + "' ";
                if (CountryName != string.Empty) {
                    oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("dogetcountry" + CountryName, sQry);
                    if (oRecords != null && oRecords.RecordCount > 0) {
                        SAPbouiCOM.ComboBox oCOmbo = (SAPbouiCOM.ComboBox)Items.Item("IDH_CUNTRY").Specific;
                        if (oCOmbo.Selected == null ||(oCOmbo.Selected!= null && oCOmbo.Selected.Value != CountryIso2)) {
                            oCOmbo.SelectExclusive(CountryIso2, BoSearchKey.psk_ByValue);
                            FillCombos.FillState(Items.Item("IDH_STATE").Specific, moEnquiry.U_Country, null);
                        } 
                    }
                    if (ProvinceCode != string.Empty) {
                        sQry = "Select Code from OCST where Code Like '" + ProvinceCode + "' And Country='" + CountryIso2 + "' ";
                        oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("dogetstate" + ProvinceCode, sQry);
                        if (oRecords != null && oRecords.RecordCount > 0) {
                            SAPbouiCOM.ComboBox oCOmbo = (SAPbouiCOM.ComboBox)Items.Item("IDH_STATE").Specific;                            
                            if ((oCOmbo.Selected== null) || (oCOmbo.Selected!= null && oCOmbo.Selected.Value != ProvinceCode) ) {
                                oCOmbo.SelectExclusive(ProvinceCode, BoSearchKey.psk_ByValue);
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
            }
            return true;
        }
        public bool doHandlePostCodeCancelReturn(com.idh.forms.oo.Form oDeialogForm)
        {
            com.uBC.forms.fr3.PostCode oOOForm = (com.uBC.forms.fr3.PostCode)oDeialogForm;

            return true;
        }
        #endregion
#region ItemEventHandlers

        
         /** 
         * Update the Row Header Ref
         */
        private void doUpdateLineHeaderRef() {
            moEnquiry.EnquiryItems.setValueForAllRows(IDH_ENQITEM._EnqId, moEnquiry.Code, true);
        }
        public bool doAddUpdateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            if (pVal.BeforeAction)
            {
                if (doValidate()==false) {
                    BubbleEvent = false;
                    return true;
                }
                BoFormMode CurrentMode = Mode;
                Freeze(true);
                try
                {
                   // DateTime oDate = DateTime.Now;
                  //  moEnquiry.U_Recontact = oDate;
                    //FUTURE USE - Updater if ( moVehicleMaster.doProcessData( )) {
                    mbAddUpdateOk = doAutoSave(IDH_ENQUIRY.AUTONUMPREFIX, moEnquiry.AutoNumKey);
                    if (mbAddUpdateOk)
                    {
                        doUpdateLineHeaderRef();//moEnquiry.EnquiryItems.doUpdateAllDataRows("NC");                    
                        if (moGrid.doProcessData())
                        {
                            if (CurrentMode == BoFormMode.fm_ADD_MODE && mbFromActivity==false)
                            {
                                string msg=com.idh.bridge.resources.Messages.getGMessage("INFDOCNO", new string[] {"Enquiry",moEnquiry.Code });
                                com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(msg,BoMessageTime.bmt_Short,BoStatusBarMessageType.smt_Warning);
                                Mode = BoFormMode.fm_ADD_MODE;
                            }
                            else
                                Mode = BoFormMode.fm_OK_MODE;
                            BubbleEvent = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
                finally
                {
                    Freeze(false);
                }
            }
            return true;
        }
        //private bool doHandleGridFieldValueChanged(ref IDHAddOns.idh.events.Base pVal)
        //{
        //    //if (pVal.BeforeAction)
        //    //{
                
        //    //}
        //    return true;        
        //}
        private bool doHandleGridSearch(ref IDHAddOns.idh.events.Base pVal) {
            if (pVal.BeforeAction)
                if (pVal.ColUID == IDH_ENQITEM._ItemName && moEnquiry.EnquiryItems.getValueAsString(pVal.Row, IDH_ENQITEM._ItemCode) != string.Empty) {
                    moEnquiry.EnquiryItems.setValue(IDH_ENQITEM._ItemCode, "", false);
                } else if (pVal.ColUID == IDH_ENQITEM._WstGpNm && moEnquiry.EnquiryItems.getValueAsString(pVal.Row, IDH_ENQITEM._WstGpCd) != string.Empty) {
                    moEnquiry.EnquiryItems.setValue(IDH_ENQITEM._WstGpCd, "", false);
                }
            return true;
        }
        private bool doHandleGridDoubleClick(ref IDHAddOns.idh.events.Base pVal) {
            if (Mode!=BoFormMode.fm_ADD_MODE && pVal.BeforeAction)
                if (pVal.ColUID == IDH_ENQITEM._ItemCode && moGrid.doGetFieldValue(IDH_ENQITEM._ItemName, pVal.Row).ToString().Trim()!=string.Empty) {//&& moEnquiry.EnquiryItems.getValueAsString(pVal.Row, IDH_ENQITEM._ItemCode) == string.Empty) {
                    com.uBC.forms.fr3.WasteItemValidations oOOForm = new com.uBC.forms.fr3.WasteItemValidations(null, SBOForm.UniqueID, null);
                    oOOForm.iParentRow = pVal.Row;
                    oOOForm.setUFValue("IDH_ITEMCD", moGrid.doGetFieldValue(IDH_ENQITEM._ItemCode, pVal.Row));
                    oOOForm.setUFValue("IDH_ITEMNM", "");

                    oOOForm.setUFValue("IDH_FRNNAM", moGrid.doGetFieldValue(IDH_ENQITEM._ItemName, pVal.Row));

                    oOOForm.setUFValue("IDH_WSTGRP", moGrid.doGetFieldValue(IDH_ENQITEM._WstGpCd, pVal.Row));//Address ID 50
                   // oOOForm.setUFValue("IDH_ENQID", moGrid.doGetFieldValue(IDH_ENQITEM._EnqId, pVal.Row));//Address ID 50
                    oOOForm.WasteGroupCode = (string)moGrid.doGetFieldValue(IDH_ENQITEM._WstGpCd, pVal.Row);
                    oOOForm.EnquiryID = (string)moGrid.doGetFieldValue(IDH_ENQITEM._EnqId, pVal.Row);
                    oOOForm.ItemCode = (string)moGrid.doGetFieldValue(IDH_ENQITEM._ItemCode, pVal.Row);

                    oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleCreateWasteItemOKReturn);
                    oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleCreateWasteItemCancelReturn);
                    oOOForm.doShowModal(SBOForm);

                }
            return true;
        }
        
#region FolderEvents
       
        #endregion
#endregion

#region MenuEventHandlers

        public bool doHandleModeChange(SAPbouiCOM.BoFormMode oSBOMode) {
            Freeze(true);
            if (oSBOMode == BoFormMode.fm_ADD_MODE) {
                doSwitchToEdit();
                doClearFormUFValues();
                moEnquiry.doClearBuffers();
                moEnquiry.doAddEmptyRow(true, true);
                moGrid.doApplyRules();
                doFillGridCombos();
                FillCombos.FillState(Items.Item("IDH_STATE").Specific, moEnquiry.U_Country, null);
                if (mbFromActivity && mbActivityID != 0)
                    moEnquiry.U_ClgCode = mbActivityID;
                //   DateTime oDate = DateTime.Now;
                //moEnquiry.U_Recontact = oDate;
                doSetFocus("IDH_CardNM");
            } else if (oSBOMode == BoFormMode.fm_FIND_MODE) {
                doSwitchToFind();

                doClearFormDFValues();
                doClearFormUFValues();

                ////moGrid.doApplyRules();
            } else if (oSBOMode == BoFormMode.fm_OK_MODE || oSBOMode == BoFormMode.fm_UPDATE_MODE) {

                if (moEnquiry.U_CardCode != string.Empty) EnableItem(false, "IDH_CardNM");
                //doSetFocus("IDH_CardNM");
                //EnableItem(false, "IDH_DOCNUM");
            }

            Freeze(false);
            return true;
        }
        public bool doMenuAddEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                moEnquiry.doClearBuffers();
                Mode = BoFormMode.fm_ADD_MODE;
            }
            return true;
        }

        public bool doMenuFindBUttonEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                Mode = BoFormMode.fm_FIND_MODE;
            }
            return true;
        }
        public bool doMenuBrowseEvents(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                Freeze(true);
                try {
                    doClearFormUFValues();
                } catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHANB", null);
                } finally {
                    Freeze(false);
                }
            } else {
                FillCombos.FillState(Items.Item("IDH_STATE").Specific, moEnquiry.U_Country, null);
                moEnquiry.doLoadChildren();
                if (moEnquiry.EnquiryItems.Count == 0) {
                    moGrid.doAddEditLine(true);
                }
                moGrid.doApplyRules();
                doFillGridCombos();
                EnableItem(false, "IDH_DOCNUM");
                string sCardName="";
                if (doValidateBPbyCode(ref sCardName) == true) {
                    EnableItem(false, "IDH_CRETBP");                
                }
                else
                    EnableItem(true, "IDH_CRETBP");                
            }
            return true;
        }
#endregion
#endregion

#region OldEventHandlers
        /**
         * Most of these are replaced with Handlers as above... only use these if there is really no Event or in special events
         */

        /*
         * Handle all the Menu Events.
         * Return True if the Event must be handled by the other Objects
         */
        public override bool doMenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * The Event Handler that will receive all Activated Events for this Form
         * Return True if the Event must be handled by the other Objects
         */
        public override bool doItemEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction && ((int)pVal.FormMode == (int)BoFormMode.fm_UPDATE_MODE) && (pVal.EventType == BoEventTypes.et_KEY_DOWN || pVal.EventType == BoEventTypes.et_COMBO_SELECT)) {
              //  com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(pVal.EventType.ToString(), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                //EnableItem(false, "IDH_DOCNUM");
                if (moEnquiry.U_CardCode != string.Empty) {
                    EnableItem(false, "IDH_CRETBP");
                    EnableItem(false, "IDH_CardNM");
                }
            } return true;
        }

        /*
         * Send By Custom Controls
         */
        public override bool doCustomItemEvent(ref IDHAddOns.idh.events.Base pVal) {
            return true;
        }

        /*
         * Handle the Request for a Help File
         */
        public override bool doHelpFile(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Handles the right click event
         */
        public override bool doRightClickEvent(ref SAPbouiCOM.ContextMenuInfo pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Handle the Button One Pressed Event
         */
        public override void doButtonID1(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        }
        /* 
         * Handle Button Two Presssed Event
         */
        public override void doButtonID2(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        }

        /* 
         * Return As Canceled If this was Opened as a Dialog from another form
         */
        public override void doReturnCanceled(string sModalFormType) {
        }

        /* 
         * Return as Ok If this was Opened as a Dialog from another form setting the Data in the Global Shared Buffer
         * This will be called from the Conroller after all the other Button 1 Handlers and the BubbleEvent is still True.
         */
        public override void doReturnFromModalShared(bool bState) {
        }

        /* 
         * Handle the Cancel Event when a Dialog returns to this form.
         */
        public override void doHandleModalCanceled(string sModalFormType) {
            if (sModalFormType == "IDHCSRCH")//BP Search
            {
               // doSetFocus("IDH_CardNM");
                if (moEnquiry.U_CardCode!=string.Empty)
                {
                    string sCardName = Config.INSTANCE.doGetBPName(moEnquiry.U_CardCode);
                    if (!moEnquiry.U_CardName.Equals(sCardName))
                        moEnquiry.U_CardCode = "";
                }
                    
            }
            else if (sModalFormType == "IDHASRCH" || sModalFormType == "IDHASRCH3")
            {
                doSetFocus("IDH_ADR1");

            }
        }

        /* 
         * Handle the Return from a Dialog called from this Form with the data in oData
         */
        public override void doHandleModalBufferedResult(ref object oData, string sModalFormType, string sLastButton) {
        }

        /* 
         * Handle the Return from a Dialog called from this Form with the data in the Global Shared Buffer
         */
        public override void doHandleModalResultShared(string sModalFormType, string sLastButton) {
            if (sModalFormType == "IDHCSRCH")//BP
            {
                moEnquiry.U_CardCode= (string)getSharedData("CARDCODE");
                moEnquiry.U_CardName= (string)getSharedData("CARDNAME");
                ArrayList oData =Config.INSTANCE.doGetBPContactEmployee(moEnquiry.U_CardCode,"DEFAULTCONTACT");
                if (oData !=null ){
                moEnquiry.U_CName=oData[0].ToString();
                moEnquiry.U_Phone1=oData[4].ToString();
                moEnquiry.U_E_Mail=oData[7].ToString();
                }
            } 
            else if (sModalFormType == "IDHASRCH" || sModalFormType == "IDHASRCH3")//BP Address
            {
                moEnquiry.U_Address = (string)getSharedData("ADDRESS");
                moEnquiry.U_Block = (string)getSharedData("BLOCK");
                moEnquiry.U_Street = (string)getSharedData("STREET");
                moEnquiry.U_City = (string)getSharedData("CITY");
                moEnquiry.U_State = (string)getSharedData("STATE");
                moEnquiry.U_ZipCode = (string)getSharedData("ZIPCODE");
                moEnquiry.U_Country = (string)getSharedData("COUNTRY");
                moEnquiry.U_County = (string)getSharedData("COUNTY");
            }
        }


        public void HandleFormDataEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.BusinessObjectInfo BusinessObjectInfo, ref bool BubbleEvent) {
            try {
                if (BusinessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_LOAD && BusinessObjectInfo.ActionSuccess && BusinessObjectInfo.BeforeAction==false) {
                    Mode = BoFormMode.fm_OK_MODE;
                    doSetFocus("IDH_ADR1");
                    EnableItem(false, "IDH_DOCNUM");
                  if(moEnquiry.U_CardCode!=string.Empty)  EnableItem(false, "IDH_CardNM");
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHFE", null);
            }

            //'Try

            //'Catch ex As Exception
            //'    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHFE", {Nothing})
            //'End Try
        }
#endregion
#region RestOfLogic
        private void doFillGridCombos()
        {
            SAPbouiCOM.ComboBoxColumn oCombo;

            int iIndex = moGrid.doIndexFieldWC(IDH_ENQITEM._UOM);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            FillCombos.UOMCombo(oCombo);

            iIndex = moGrid.doIndexFieldWC(IDH_ENQITEM._Status);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            FillCombos.FillWR1StatusNew(oCombo, 102);
        }
        protected void doGridLayout()
        {
            string sWastGrp = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup();

            moGrid.doAddListField(IDH_ENQITEM._Code, "Code", false, 0, null, null);
            moGrid.doAddListField(IDH_ENQITEM._Name, "Name", false, 0, null, null);

            moGrid.doAddListField(IDH_ENQITEM._EnqId, "Enquiry ID", false, 0, null, null);
            moGrid.doAddListField(IDH_ENQITEM._ItemCode, "Item Code", false, -1,null,null,-1,SAPbouiCOM.BoLinkedObject.lf_Items);
            moGrid.doAddListField(IDH_ENQITEM._ItemName, "Description", true, 120, "SRC*IDHWISRC(FROMENQUIRY)[IDH_ITMNAM;IDH_GRPCOD=" + sWastGrp + "][FNAME;" + IDH_ENQITEM._ItemCode + "=ITEMCODE]", null);
            moGrid.doAddListField(IDH_ENQITEM._WstGpCd, "Waste Group", true, -1, "SRC*IDH_WGSRCH(WG01)[IDH_WGCOD][WGCODE;" + IDH_ENQITEM._WstGpNm + "=WGNAME]", null);
            //moGrid.doAddListField(IDH_ENQITEM._WstGpCd, "Waste Group", true, -1, "SRC*IDHWISRC(WST01)[IDH_ITMCOD;IDH_INVENT=;IDH_GRPCOD=115][ITEMCODE;U_WasteDs=ITEMNAME]", null, -1, SAPbouiCOM.BoLinkedObject.lf_None);
            moGrid.doAddListField(IDH_ENQITEM._WstGpNm, "Waste Group Name", true, -1, "SRC*IDH_WGSRCH(WG01)[IDH_NAME][WGNAME;" + IDH_ENQITEM._WstGpCd + "=WGCODE]", null);
            moGrid.doAddListField(IDH_ENQITEM._EstQty, "Eastimated Qty", true, -1, null, null);
            moGrid.doAddListField(IDH_ENQITEM._UOM, "UOM", true, -1, ListFields.LISTTYPE_COMBOBOX, null);
            moGrid.doAddListField(IDH_ENQITEM._Status, "Status", true, -1, ListFields.LISTTYPE_COMBOBOX, null);
            //moGrid.doAddListField(IDH_ENQITEM._Status, "Status", true, -1, ListFields.LISTTYPE_SEARCHALWAYS, null);
           // moGrid.doAddListField(IDH_ENQITEM._Created, "Created", false, 0, null, null);
            moGrid.doSynchDBandGridFieldPos();
        }
        /*
         * Set the Filter Fields
         */
        protected void doSetFilterFields()
        {
            moGrid.doAddFilterField("IDH_DOCNUM", IDH_ENQITEM._EnqId, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 16);
        }
        private bool doValidate()
        {
            if (moEnquiry.U_CardName.Trim() == string.Empty)
            {

                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Business Partner is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Business Partner") });
                //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar("Please enter value for Business Partner field.");
                return false;
            }
            if (moEnquiry.U_Address.Trim() == string.Empty) {
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Address is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Address") });
                //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar("Please enter value for Address field.");
                return false;
            }
            if (moEnquiry.U_ZipCode.Trim() == string.Empty) {
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Postcode is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Postcode") });
                //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar("Please enter value for Address field.");
                return false;
            }
            if (moEnquiry.U_City.Trim() == string.Empty)
            {
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: City is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("City") });
                //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar("Please enter value for City field.");
                return false;
            }

            if (moEnquiry.U_Country.Trim() == string.Empty)
            {
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Country is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Country") });
                //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar("Please enter value for Country field.");
                return false;
            }
            if (moEnquiry.U_CName.Trim() == string.Empty ) {
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Contact person is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Contact person") });
                //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar("Please enter value(s) for contact details.");
                return false;
            }
            if (moEnquiry.U_Phone1.Trim() == string.Empty && moEnquiry.U_E_Mail.Trim() == string.Empty)
            {
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Contact detail(s) is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Contact detail(s)") });
                //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar("Please enter value(s) for contact details.");
                return false;
            }
            return true;
        }
        private void doChooseBPCode()
        {
            //string sCardCode = (string)getUFValue("uBC_VEHBTO");
            //  moEnquiry.U_CardCode = "";
            setSharedData("IDH_BPCOD", "");
            setSharedData("IDH_NAME", moEnquiry.U_CardName );
            setSharedData("TRG", "BP");
            //setSharedData("IDH_TYPE", "F-S");
            //setSharedData("SILENT", "SHOWMULTI");
            doOpenModalForm("IDHCSRCH");
        }
        private void doChooseBPAddress()
        {
            string sCardCode = moEnquiry.U_CardCode.Trim();
            string sAddress = moEnquiry.U_Address.Trim();
            setSharedData("TRG", "IDH_ADR1");
            setSharedData("IDH_CUSCOD", sCardCode);
            setSharedData("IDH_ADRTYP", "S");
            setSharedData("CALLEDITEM", "IDH_ADR1");
            //setSharedData("SILENT", "SHOWMULTI");
            setSharedData("IDH_ADDRES",  sAddress );
            if (Config.ParameterAsBool("UNEWSRCH", false) == false)
            {
                doOpenModalForm("IDHASRCH");
            }
            else
            {
                doOpenModalForm("IDHASRCH3");
            }

        }

        private bool doValidateBPbyCode(ref string sCardName) {
            sCardName = Config.INSTANCE.doGetBPName(moEnquiry.U_CardCode);
            if (sCardName == string.Empty)
                return false;
            else
            return true;
        }

        /*
         * Clear the form Data
         */
        private void doClearAll() {
            doClearFormDFValues();
        }

        /**
         * Fill the ComboBoxes
         */
        private void doFillCombos() {
            FillCombos.UsersCombo(Items.Item("IDH_ASGNTO").Specific, idh.bridge.DataHandler.INSTANCE.User, null);
            FillCombos.FillCountry(Items.Item("IDH_CUNTRY").Specific);
            
            //SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_CUNTRY").Specific;
            //FillCombos.FillState(Items.Item("IDH_STATE").Specific,oCombo.Selected.Value);
            
            FillCombos.FillWR1StatusNew(Items.Item("IDH_STATUS").Specific, 101);
            
            SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_ASGNTO").Specific;
            oCombo.SelectExclusive(idh.bridge.DataHandler.INSTANCE.User, SAPbouiCOM.BoSearchKey.psk_ByValue);

            //oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_STATE").Specific;
            //oCombo.SelectExclusive(idh.bridge.DataHandler.INSTANCE.User, SAPbouiCOM.BoSearchKey.psk_ByValue);

            oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_STATUS").Specific;
            oCombo.SelectExclusive("1", SAPbouiCOM.BoSearchKey.psk_ByValue);
           

            oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_CUNTRY").Specific;
            //string sDfltCountry = Config.INSTANCE.getParameterWithDefault("DFTCNTRY", "GB");
            oCombo.SelectExclusive(Config.INSTANCE.getParameterWithDefault("DFTCNTRY", "GB"), SAPbouiCOM.BoSearchKey.psk_ByValue);

        }
        private void doSwitchToFind()
        {
            Items.Item("IDH_DOCNUM").Enabled = true;
            doSetFocus("IDH_DOCNUM");
            EnableItem(false, "IDH_CRETBP");
            string[] oExl = { "IDH_DOCNUM" };
            DisableAllEditItems(oExl);
            
        }

        private void doSwitchToEdit()
        {
            doSetFocus("IDH_ADR1");
            string[] oExl = { "IDH_DOCNUM", "IDH_ACTVTY", "IDH_STATUS", "IDH_USER", "IDH_CardCD" };
            EnableIAllEditItems(oExl);
            EnableItem(false, "IDH_DOCNUM");
            doSetFocus("IDH_ADR1");
            EnableItem(true, "IDH_CRETBP");

        }

#endregion
    
    }
}

