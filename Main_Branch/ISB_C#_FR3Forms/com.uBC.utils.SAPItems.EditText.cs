﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.forms.oo;
using com.uBC.utils.SAPItems.components.Base;

namespace com.uBC.utils.SAPItems {
    public class EditText<T,P> : components.FormItem<SAPbouiCOM.EditText,P> where P:PopulationType {

        #region Member Variables
        private Property<T> moValue;
        private List<T> moPreviousValues;
        #endregion

        #region Constructors
        public EditText(Form oForm, string sUniqueID, string sCaption, FieldInfo oFormInfo,bool bUF = true,bool bDF = false)
            : base(oForm, sUniqueID, SAPbouiCOM.BoFormItemTypes.it_EDIT, oFormInfo) {
            base.Caption = sCaption;
            UserField = bUF;
            DataField = bDF;
            doInit();
        }
        public EditText(Form oForm, SAPbouiCOM.Item oControl, SAPbouiCOM.Item oLable, FieldInfo oFormInfo,bool bUF = true,bool bDF = false)
            : base(oForm, oControl, oLable, oFormInfo) {
                doInit();
                UserField = bUF;
                DataField = bDF;
        }
        #endregion

        #region Methods
        private void doInit(){
            moPreviousValues = new List<T>(10);
            moValue = new Property<T>();
            Form.addHandler_VALIDATE(base.UniqueID,new Form.ev_Item_Event(Spesific_Validate));
            moValue.PropertyChanged += new Delegates.PropertyChanged<T>(moValue_PropertyChanged);
        }
        //Can use AutoComplete With Previous Value List : Future Use
        protected override bool doPopulate(string sPopPar) {
            Population = new Population<SAPbouiCOM.EditText, P>(sPopPar);
            Population.EditTextDataReady += new Delegates.PopulationComplete<object>(Population_EditTextDataReady);
            return base.doPopulate(sPopPar);
        }
        public bool doPopulation(string sPopPar) {
            return doPopulate(sPopPar);
        }
        #endregion

        #region Handlers
        private bool Spesific_Validate(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                if (typeof(T) == typeof(DateTime)) {
                    DateTime dtTemp;
                    if (UserField)
                        dtTemp = idh.bridge.utils.Dates.doStrToDate(Form.getUFValue(pVal.ItemUID).ToString());
                    else if (DataField)
                        dtTemp = idh.bridge.utils.Dates.doStrToDate(Form.getDFValue(FieldInfo.TableName, FieldInfo.DBFieldName).ToString());
                    else {
                        dtTemp = idh.bridge.utils.Dates.doStrToDate(Spesific.Value);
                    }
                    if (!moPreviousValues.Contains((T)(object)dtTemp)) 
                        moPreviousValues.Add((T)(object)dtTemp);
                    Value = (T)(object)dtTemp;
                } else if (typeof(T) == typeof(int)) {
                    int iTemp;
                    if (UserField)
                        Int32.TryParse(Form.getUFValue(pVal.ItemUID).ToString(), out iTemp);
                    else if (DataField)
                        Int32.TryParse(Form.getDFValue(FieldInfo.TableName, FieldInfo.DBFieldName).ToString(), out iTemp);
                    else {
                        Int32.TryParse(Spesific.Value, out iTemp);
                    }
                    if (!moPreviousValues.Contains((T)(object)iTemp))
                        moPreviousValues.Add((T)(object)iTemp);
                    Value = (T)(object)iTemp;
                } else {
                    if (!moPreviousValues.Contains((T)(object)Form.getUFValue(pVal.ItemUID)))
                        moPreviousValues.Add((T)(object)Form.getUFValue(pVal.ItemUID));
                    Value = (T)(object)Form.getUFValue(pVal.ItemUID);
                }
            }
            return true;
        }
        private bool Population_EditTextDataReady(object Data) {
            Value = (T)Data;
            return true;
        }
        private bool moValue_PropertyChanged(Property<T> oProperty) {
            return true;
        }
        #endregion

        #region Properties
        public Property<T> ValueProperty {
            get { return moValue; }
        }
        public T Value {
            get {
                if (!string.IsNullOrEmpty(base.Form.getUFValue(base.UniqueID).ToString())) {
                    if (UserField)
                        return (T)base.Form.getUFValue(base.UniqueID);
                    else if (DataField)
                        return (T)base.Form.getDFValue(FieldInfo.TableName, FieldInfo.DBFieldName);
                    else
                        return (T)(object)Spesific.Value;
                }
                if (typeof(T) == typeof(string))
                    return (T)(object)string.Empty;
                return default(T);
            }
            set {
                if (UserField)
                    if(typeof(T)==typeof(DateTime))
                        base.Form.setUFValue(base.UniqueID, idh.utils.Dates.doDateToSBODateStr((DateTime)(object)(T)value));
                    else
                        base.Form.setUFValue(base.UniqueID, value);
                else if (DataField)
                    base.Form.setDFValue(FieldInfo.TableName, FieldInfo.DBFieldName, value);
                else {
                    Spesific.Value = value.ToString();
                }
                moValue.Set(value);
            }
        }
        #endregion
    }
}
