﻿using System.IO;
using System.Collections;
using System;

using IDHAddOns.idh.controls;
using IDHAddOns;
using com.idh.controls;
using com.idh.bridge.data;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.idh.dbObjects.User;
using com.uBC.utils;
using com.uBC.data;
using SAPbouiCOM;

using com.idh.bridge.action;
using com.idh.utils;

namespace com.uBC.forms.fr3.popup {
    public class DOAmend : com.idh.forms.oo.Form {
        private IDH_DISPROW moDOR;
        public IDH_DISPROW DOR {
            get { return moDOR; }
        }

        private IDH_JOBSHD moWOR;
        public IDH_JOBSHD WOR {
            get { return moWOR; }
        }

        private string msDORCode;
        public string DORCode {
            set { 
                moDOR = new IDH_DISPROW();
                moDOR.SBOFormGUIOnly = SBOForm;
                if (!moDOR.getByKey(value)) {
                    value = "Not Found";
                    moDOR = null;
                } else {
                    if (moDOR.IsLinkedToOrder) {
                        moWOR = new IDH_JOBSHD();
                        if (!moWOR.getByKey(moDOR.U_WRRow))
                            moWOR = null;
                    } 
                }
                moDOR.doLoadChildren();

                msDORCode = value;

                setUFValue(FIELD_DOR, value);
            }
            get {
                return msDORCode;
                //return (string)getUFValue(FIELD_DOR); 
            }
        }

        private char mcPaymentType = 'C';
        public char PaymentType {
            get { return mcPaymentType; }
            set { mcPaymentType = value; }
        }

        private string msAmendType = "NONE";
        public string AmendmentType {
            set { msAmendType = value; }
            get { return msAmendType; }
        }

        private string PayLabelText {
            set {
                SAPbouiCOM.StaticText oLabel = (SAPbouiCOM.StaticText)Items.Item(LABEL_CHEQUENUMBER).Specific;
                oLabel.Caption = value;
            }
            get {
                SAPbouiCOM.StaticText oLabel = (SAPbouiCOM.StaticText)Items.Item(LABEL_CHEQUENUMBER).Specific;
                return oLabel.Caption;
            }
        }
        //public IDH_DISPROW.et_SetChequeNumberTrigger Handler_GetChequeNumber = null;

        public DOAmend(IDHAddOns.idh.forms.Base oIDHForm, string sParentId)
            : base(oIDHForm, sParentId) {
            doSetHandlers();
        }

        public DOAmend(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
            doSetHandlers();
        }
        
        //public new IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu) {
        public new static IDHAddOns.idh.forms.Base doRegisterFormClass() {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_DOAMD", null, 80, "DOAmendOption.srf", false, true, false, "Amend DO Row", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

#region FormOpenCreateFunctions
        /** 
         * This Function will be called by the controller to allow the class to last minute Form Layout adjustments before it gets displayed
         * All changes made in hete will be cached if this is a cached form, the method will not be called again once the form has benn cached.
         */
        public const string FIELD_DOR = "uBCDOR";
        public const string FIELD_CHEQUENUMBER = "uBCCQNUM";
        public const string FIELD_REASON = "uBCREASN";

        public const string BUTTON_PAY = "uBCPAY";
        public const string BUTTON_ALL = "uBCALL";

        public const string LABEL_CHEQUENUMBER = "LBL_CHQNUM";
        public const string LABEL_REASON = "LBL_REASON";

        public const string AMENDTYPE_PAYMENT = "Payment";
        public const string AMENDTYPE_ALL = "All";

        public override void doCompleteCreate(ref bool BubbleEvent) {
            doAddUF(FIELD_DOR, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, false, false);
            doAddUF(FIELD_CHEQUENUMBER, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, false, false);
            doAddUF(FIELD_REASON, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254, false, false);

            SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_Ok; 
            AutoManaged = false;
            Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
        }

        /** 
         * Do the final form steps to show before loaddata
         */
        public override void doBeforeLoadData() {
            FillCombos.AmendmentReasonCombo(FillCombos.getCombo(SBOForm, FIELD_REASON), "DOR Amended", "Select a Reason");
            setVisible(FIELD_CHEQUENUMBER, false);
            //setVisible(LABEL_CHEQUENUMBER, false);
            SAPbouiCOM.Item oItem = Items.Item(LABEL_CHEQUENUMBER); 
            oItem.LinkTo = "";
            oItem.Width = 150;
            PayLabelText = "";
            //setUFValue(FIELD_DOR, msDORCode);
        }

        /** 
         * Load the Form Data
         */
        public override void doLoadData() {
        }

        /** 
         * Do the final form steps to show after loaddata
         */
        public override void doFinalizeShow() {
            base.doFinalizeShow();
        }
#endregion

#region Events
        private void doSetHandlers() {
            Handler_Button_Ok = new ev_Item_Event(doButtonOkEvent);

            addHandler_ITEM_PRESSED(BUTTON_ALL, new ev_Item_Event(doCancelAll));
            addHandler_ITEM_PRESSED(BUTTON_PAY, new ev_Item_Event(doCancelPayment));
        }
#endregion 

#region ItemEventHandlers
        public bool doButtonOkEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                if (moDOR != null) {
                    if (msAmendType == AMENDTYPE_PAYMENT) {
                        int iChequeNumber = -1;
                        if (moWOR != null && !string.IsNullOrWhiteSpace(moDOR.U_TrnCode)) {
                            //check if there is a Linked WO 
                            if (MarketingDocs.doCancelOutgoingPayment(moWOR.U_ProCd, moWOR.U_PROINV, "AMD")) {
                                moWOR.doSupplierPaymentFromAPInvoice(true, iChequeNumber);
                            }
                        } else {
                            if (MarketingDocs.doCancelOutgoingPayment(moDOR.U_ProCd, moDOR.U_PROINV, "AMD")) {
                                return moDOR.doSupplierPaymentFromAPInvoice(true, iChequeNumber);
                            }
                        }
                    } else if (msAmendType == AMENDTYPE_ALL) {
                        string sReason = (string)getUFValue(FIELD_REASON);
                        if (moDOR.doReOpen(sReason==null||sReason.Length==0?"DOR Amended.":sReason)) {
                            bool bSkipMarkDocs = moDOR.DoSkipMarketingDocuments;
                            bool bSkipLinkUpdate = moDOR.DoSkipUpdateLinkedWO;
                            try {
                                moDOR.DoSkipMarketingDocuments = true;
                                moDOR.DoSkipUpdateLinkedWO = true;

                                if (!moDOR.doUpdateDataRow())
                                    return false;

                            } catch (Exception ex) {
                                throw ex;
                            } finally {
                                moDOR.DoSkipMarketingDocuments = bSkipMarkDocs;
                                moDOR.DoSkipUpdateLinkedWO = bSkipLinkUpdate;
                            }
                            
                            //Now do the linked WOR as well
                            if (moWOR != null && !string.IsNullOrWhiteSpace(moDOR.U_TrnCode)) {
                                if (moWOR.doReOpen(sReason == null || sReason.Length == 0 ? "DOR Amended." : sReason)) {
                                    bSkipMarkDocs = moWOR.DoSkipMarketingDocuments;
                                    try {
                                        moWOR.DoSkipMarketingDocuments = true;
                                        moWOR.doUpdateDataRow();
                                    } catch (Exception ex) {
                                        throw ex;
                                    } finally {
                                        moWOR.DoSkipMarketingDocuments = bSkipMarkDocs;
                                    }
                                }
                            }
                        }
                    }
                }
                //BubbleEvent = false;
            }
            return true;
        }

        public bool doCancelAll(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                EnableItem(true, FIELD_REASON);
                EnableItem(true, LABEL_REASON);

                msAmendType = AMENDTYPE_ALL;
                PayLabelText = ""; 
            }
            return true;
        }

        public bool doCancelPayment(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {

                EnableItem(false, FIELD_REASON);
                EnableItem(false, LABEL_REASON);

                msAmendType = AMENDTYPE_PAYMENT;
                PayLabelText = Translation.getTranslatedWord("Waiting for Repayment.");
            }
            return true;
        }
#endregion 

#region OtherLogic
        public void doGetChequeNumber(IDH_DISPROW oCaller, SAPbobsCOM.Payments oPay) {
            //oCaller.ChequeNumber = 112233;
        }
#endregion

#region OldEventHandlers
        /**
         * Most of these are replaced with Handlers as above... only use these if there is really no Event or in special events
         */

        /*
         * Handle all the Menu Events.
         * Return True if the Event must be handled by the other Objects
         */
        public override bool doMenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * The Event Handler that will receive all Activated Events for this Form
         * Return True if the Event must be handled by the other Objects
         */
        public override bool doItemEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Send By Custom Controls
         */
        public override bool doCustomItemEvent(ref IDHAddOns.idh.events.Base pVal) {
            return true;
        }

        /*
         * Handle the Request for a Help File
         */
        public override bool doHelpFile(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Handles the right click event
         */
        public override bool doRightClickEvent(ref SAPbouiCOM.ContextMenuInfo pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Handle the Button One Pressed Event
         */
        public override void doButtonID1(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        }

        /* 
         * Handle Button Two Presssed Event
         */
        public override void doButtonID2(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        }

        /* 
         * Return As Canceled If this was Opened as a Dialog from another form
         */
        public override void doReturnCanceled(string sModalFormType) {
        }

        ///* 
        // * Return as Ok If this was Opened as a Dialog from another form setting the Data in the Global Shared Buffer
        // * This will be called from the Conroller after all the other Button 1 Handlers and the BubbleEvent is still True.
        // */
        //public override void doReturnFromModalShared(bool bState) {
        //}

        //*** Return As Normal
        public override void doReturnNormal() {
        }

        /* 
         * Handle the Cancel Event when a Dialog returns to this form.
         */
        public override void doHandleModalCanceled(string sModalFormType) {
        }

        /* 
         * Handle the Return from a Dialog called from this Form with the data in oData
         */
        public override void doHandleModalBufferedResult(ref object oData, string sModalFormType, string sLastButton) {
        }

        /* 
         * Handle the Return from a Dialog called from this Form with the data in the Global Shared Buffer
         */
        public override void doHandleModalResultShared(string sModalFormType, string sLastButton) {
        }
#endregion

#region OldSearchForms
#endregion
    }
}

