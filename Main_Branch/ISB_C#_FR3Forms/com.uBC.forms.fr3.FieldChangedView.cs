﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using SAPbobsCOM;
using SAPbouiCOM;
using com.idh.dbObjects.User;


namespace com.uBC.forms.fr3 {
    public class FieldChangedView : com.idh.forms.oo.Form {
        private com.idh.dbObjects.DBBase moDBObject;
        public com.idh.dbObjects.DBBase DBObject {
            get { return moDBObject; }
            protected set {
                if (value != null)
                    moDBObject = value;
            }
        }

        private Dictionary<int,Hashtable> moChangedRows;
        private Hashtable mhPreviousValues;

        private SAPbouiCOM.EditTextColumn moSAPViewItem;
        public SAPbouiCOM.EditTextColumn SAPViewItem {
            get { return moSAPViewItem; }
            protected set {
                if (value != null)
                    moSAPViewItem = value;
            }
        }

        private Dictionary<String,String> moViewContent;

        public FieldChangedView(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm) : base(oIDHForm, sParentId, oSBOForm) {
            mhPreviousValues = new Hashtable();
            moViewContent = new Dictionary<String, String>();
            moDBObject = null;
            //try {
            //    moSAPViewItem = (EditTextColumn)SBOForm.Items.Item(sSAPViewItem).Specific;
            //} catch (InvalidCastException ex) {
            //    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXELF", new string[] { string.Empty });
            //}
            //if (!validateDBObject() || !validateSBOItem())
            //    throw new NotImplementedException("DBObject or SBOItem Not Set ");
        }

        public FieldChangedView(com.idh.dbObjects.DBBase oDBObject,String sSAPViewItem,IDHAddOns.idh.forms.Base oIDHForm, string sParentID, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentID, oSBOForm) {
            mhPreviousValues = new Hashtable();
            moViewContent = new Dictionary<String, String>();
            moDBObject = oDBObject;
            try {
                moSAPViewItem = (EditTextColumn)SBOForm.Items.Item(sSAPViewItem).Specific;
            } catch (InvalidCastException ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXELF", new string[] { string.Empty });
            }
            if (!validateDBObject() || !validateSBOItem())
                throw new NotImplementedException("DBObject or SBOItem Not Set ");

        }
        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new idh.forms.oo.FormController("IDH_FCHANG", sParMenu, iMenuPos, "IDH_FC.srf", false, true, false,
                "Fields Changed", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }
        private Boolean validateDBObject() {
            return (moDBObject != null);
        }
        private Boolean validateSBOItem() {
            return (moSAPViewItem != null);
        }
        public override void doFinalizeShow() {
            doGetChangedList();

            for (int iIndex = 0; iIndex < moViewContent.Count; iIndex++) {
                if (moSAPViewItem != null) {
                    moSAPViewItem.SetText(iIndex + 1, moViewContent.ElementAt(iIndex).Key + " __ " + moViewContent.ElementAt(iIndex).Value);
                }
            }

            base.doFinalizeShow();

        }
        private void doGetChangedList() {
            if (moDBObject == null)
                throw new NullReferenceException();

            moChangedRows = moDBObject.ChangedRows;
            string stest;
            string sVal = string.Empty;
            if (moChangedRows.Count != 0) {
                moDBObject.doBookmark();
                try {
                    foreach (int iCurrentRow in moChangedRows.Keys) {
                        if (moDBObject.gotoRow(iCurrentRow)) {
                            mhPreviousValues = moDBObject.getCurrentRowPreviousValues();

                            if (mhPreviousValues != null) {
                                foreach (String sFieldName in mhPreviousValues.Keys)
                                    if (!string.IsNullOrEmpty(sFieldName)) {
                                        object oNewValue = moDBObject.getValue(sFieldName);
                                        object oOldValue = mhPreviousValues[sFieldName];
                                        if (oNewValue != oOldValue)
                                            moViewContent.Add(sFieldName, "New value : " + (string)oNewValue + "- Previous Value : " + (string)oOldValue);
                                    }
                            }

                        }
                    }
                } catch (InvalidCastException ex) {
                    stest = ex.Message;
                } finally {
                    moDBObject.doRecallBookmark();
                }
            }
        }
    }
}
