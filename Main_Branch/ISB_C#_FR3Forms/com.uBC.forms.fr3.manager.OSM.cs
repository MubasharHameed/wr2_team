﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

using SAPbouiCOM;
using IDHAddOns.idh.controls;
using IDHAddOns;
using com.idh.bridge.data;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.idh.dbObjects.User;
using com.idh.controls;
using com.idh.dbObjects.strct;

using com.idh.win.controls.SBO;
using com.uBC.utils;
using com.idh.bridge.utils;
using com.uBC.utils.Dynamic;
using com.uBC.utils.Dynamic.Structs;
using com.idh.dbObjects.User.Shared;


namespace com.uBC.forms.fr3.managers {
    public class OSM : com.uBC.forms.fr3.dbGrid.Base {

        #region MethordVariables

        private SAPbouiCOM.Item moButtonTop, moButtonLeft, moButtonRight;
        private  SAPbouiCOM.Form moSBOForm;      
        private LayoutFF moDynamicLayout;

        #region ArrayInitializers

        #region SUMFIELDS

        private static string[] SUMFIELDS = { 
            IDH_JOBSHD._TipWgt,
            IDH_JOBSHD._TipCost,
            IDH_JOBSHD._TipTot,
            IDH_JOBSHD._TCTotal,
            IDH_JOBSHD._RdWgt, 
            IDH_JOBSHD._CstWgt, 
            IDH_JOBSHD._TaxAmt, 
            IDH_JOBSHD._TCharge,
            IDH_JOBSHD._CongCh,
            IDH_JOBSHD._Weight,
            IDH_JOBSHD._Price,
            IDH_JOBSHD._DisAmt,
            IDH_JOBSHD._Total,
            IDH_JOBSHD._SLicCh,
            IDH_JOBSHD._Quantity,
            IDH_JOBSHD._JCost,
            IDH_JOBSHD._OrdTot,
            IDH_JOBSHD._TAddChrg,
            IDH_JOBSHD._TAddCost,
            IDH_JOBSHD._VtCostAmt,
            IDH_JOBSHD._HlSQty,
            IDH_JOBSHD._PAUOMQt,
            IDH_JOBSHD._PRdWgt,
            IDH_JOBSHD._PCost,
            IDH_JOBSHD._PCTotal,
            IDH_JOBSHD._WgtDed,
            IDH_JOBSHD._AdjWgt,
            IDH_JOBSHD._ValDed,
            IDH_JOBSHD._PCharge,
            IDH_JOBSHD._TRdWgt,
            IDH_JOBSHD._TAddChVat,
            IDH_JOBSHD._TAddCost,
            IDH_JOBSHD._TAddCost,
            IDH_JOBSHD._BPWgt,
            IDH_JOBSHD._CarWgt
        };

        #endregion

        #region BlackListedFields

        private static String[] msaBlackListFields = {
            IDH_JOBSHD._CCExp,
            IDH_JOBSHD._CCIs,
            IDH_JOBSHD._CCSec,
            IDH_JOBSHD._CCHNum,
            IDH_JOBSHD._CCPCd,
            IDH_JOBSHD._SemSolid,
            IDH_JOBSHD._PaMuPu,
            IDH_JOBSHD._Dusty,
            IDH_JOBSHD._Fluid,
            IDH_JOBSHD._Firm,
            IDH_JOBSHD._Consiste,
            IDH_JOBSHD._TypPTreat,
            IDH_JOBSHD._DAAttach
        }; 

        //private List<String> moBlackListFields = new List<String> { IDH_JOBSHD._CCExp,IDH_JOBSHD._CCIs,IDH_JOBSHD._CCSec,
        //IDH_JOBSHD._CCHNum,IDH_JOBSHD._CCPCd,IDH_JOBSHD._SemSolid,IDH_JOBSHD._PaMuPu,IDH_JOBSHD._Dusty,IDH_JOBSHD._Fluid,
        //IDH_JOBSHD._Firm,IDH_JOBSHD._Consiste,IDH_JOBSHD._TypPTreat,IDH_JOBSHD._DAAttach};

        #endregion

        #region DISABLEDFIELDS

        private static String[] msaDisabledFields = {IDH_JOBSHD._Code,IDH_JOBSHD._Name,IDH_JOBSHD._JobNr,
        IDH_JOBSHD._TaxAmt,IDH_JOBSHD._Status,IDH_JOBSHD._SAINV,IDH_JOBSHD._SAORD,IDH_JOBSHD._TIPPO,IDH_JOBSHD._JOBPO,
        IDH_JOBSHD._CustCd,IDH_JOBSHD._CustNm,IDH_JOBSHD._CntrNo,IDH_JOBSHD._Wei1,IDH_JOBSHD._Wei2,IDH_JOBSHD._Ser1,
        IDH_JOBSHD._Ser2,IDH_JOBSHD._WDt1,IDH_JOBSHD._WDt2,IDH_JOBSHD._AddEx,IDH_JOBSHD._PStat,IDH_JOBSHD._WRRow,
        IDH_JOBSHD._WROrd,IDH_JOBSHD._PayMeth,IDH_JOBSHD._CCNum,IDH_JOBSHD._CCStat,IDH_JOBSHD._PayStat,IDH_JOBSHD._PARCPT,
        IDH_JOBSHD._Branch,IDH_JOBSHD._User,IDH_JOBSHD._TChrgVtRt,IDH_JOBSHD._HChrgVtRt,IDH_JOBSHD._VtCostAmt,
        IDH_JOBSHD._FPCoH,IDH_JOBSHD._FPCoT,IDH_JOBSHD._FPChH,IDH_JOBSHD._FPChT,IDH_JOBSHD._WastTNN,IDH_JOBSHD._HazWCNN,
        IDH_JOBSHD._SiteRef,IDH_JOBSHD._ExtWeig,IDH_JOBSHD._BDate,IDH_JOBSHD._BTime,IDH_JOBSHD._CarrReb,IDH_JOBSHD._TPCN,
        IDH_JOBSHD._MANPRC,IDH_JOBSHD._LnkPBI,IDH_JOBSHD._DPRRef,IDH_JOBSHD._MDChngd,IDH_JOBSHD._CustReb,IDH_JOBSHD._TCCN,
        IDH_JOBSHD._RowSta,IDH_JOBSHD._HaulAC,IDH_JOBSHD._ClgCode,IDH_JOBSHD._PCost,IDH_JOBSHD._PCTotal,IDH_JOBSHD._PCharge,
        IDH_JOBSHD._WgtDed,IDH_JOBSHD._AdjWgt,IDH_JOBSHD._ValDed,IDH_JOBSHD._ProGRPO,IDH_JOBSHD._GRIn,IDH_JOBSHD._CustGR,
        IDH_JOBSHD._LoadSht,IDH_JOBSHD._ExpLdWgt,IDH_JOBSHD._UseWgt,IDH_JOBSHD._PayTrm,IDH_JOBSHD._JStat,IDH_JOBSHD._Jrnl,
        IDH_JOBSHD._LckPrc,IDH_JOBSHD._PrcLink,IDH_JOBSHD._Rebate,IDH_JOBSHD._TAddCsVat,IDH_JOBSHD._TAddChVat,
        IDH_JOBSHD._TAddChrg,IDH_JOBSHD._TAddCost,IDH_JOBSHD._ReqArch,IDH_JOBSHD._TrnCode,IDH_JOBSHD._JbToBeDoneDt,
        IDH_JOBSHD._JbToBeDoneTm,IDH_JOBSHD._DrivrOnSitDt,IDH_JOBSHD._DrivrOnSitTm,IDH_JOBSHD._DrivrOfSitDt,
        IDH_JOBSHD._DrivrOfSitTm,IDH_JOBSHD._OTComments,IDH_JOBSHD._BPWgt,IDH_JOBSHD._CarWgt,IDH_JOBSHD._PROINV,
        IDH_JOBSHD._AddCharge,IDH_JOBSHD._AddCost,IDH_JOBSHD._TCostVtRt,IDH_JOBSHD._PCostVtRt,IDH_JOBSHD._HCostVtRt};

        //private List<String> moDisabledFields = new List<String> { IDH_JOBSHD._Code,IDH_JOBSHD._Name,IDH_JOBSHD._JobNr,
        //IDH_JOBSHD._TaxAmt,IDH_JOBSHD._Status,IDH_JOBSHD._SAINV,IDH_JOBSHD._SAORD,IDH_JOBSHD._TIPPO,IDH_JOBSHD._JOBPO,
        //IDH_JOBSHD._CustCd,IDH_JOBSHD._CustNm,IDH_JOBSHD._CntrNo,IDH_JOBSHD._Wei1,IDH_JOBSHD._Wei2,IDH_JOBSHD._Ser1,
        //IDH_JOBSHD._Ser2,IDH_JOBSHD._WDt1,IDH_JOBSHD._WDt2,IDH_JOBSHD._AddEx,IDH_JOBSHD._PStat,IDH_JOBSHD._WRRow,
        //IDH_JOBSHD._WROrd,IDH_JOBSHD._PayMeth,IDH_JOBSHD._CCNum,IDH_JOBSHD._CCStat,IDH_JOBSHD._PayStat,IDH_JOBSHD._PARCPT,
        //IDH_JOBSHD._Branch,IDH_JOBSHD._User,IDH_JOBSHD._TChrgVtRt,IDH_JOBSHD._HChrgVtRt,IDH_JOBSHD._VtCostAmt,
        //IDH_JOBSHD._FPCoH,IDH_JOBSHD._FPCoT,IDH_JOBSHD._FPChH,IDH_JOBSHD._FPChT,IDH_JOBSHD._WastTNN,IDH_JOBSHD._HazWCNN,
        //IDH_JOBSHD._SiteRef,IDH_JOBSHD._ExtWeig,IDH_JOBSHD._BDate,IDH_JOBSHD._BTime,IDH_JOBSHD._CarrReb,IDH_JOBSHD._TPCN,
        //IDH_JOBSHD._MANPRC,IDH_JOBSHD._LnkPBI,IDH_JOBSHD._DPRRef,IDH_JOBSHD._MDChngd,IDH_JOBSHD._CustReb,IDH_JOBSHD._TCCN,
        //IDH_JOBSHD._RowSta,IDH_JOBSHD._HaulAC,IDH_JOBSHD._ClgCode,IDH_JOBSHD._PCost,IDH_JOBSHD._PCTotal,IDH_JOBSHD._PCharge,
        //IDH_JOBSHD._WgtDed,IDH_JOBSHD._AdjWgt,IDH_JOBSHD._ValDed,IDH_JOBSHD._ProGRPO,IDH_JOBSHD._GRIn,IDH_JOBSHD._CustGR,
        //IDH_JOBSHD._LoadSht,IDH_JOBSHD._ExpLdWgt,IDH_JOBSHD._UseWgt,IDH_JOBSHD._PayTrm,IDH_JOBSHD._JStat,IDH_JOBSHD._Jrnl,
        //IDH_JOBSHD._LckPrc,IDH_JOBSHD._PrcLink,IDH_JOBSHD._Rebate,IDH_JOBSHD._TAddCsVat,IDH_JOBSHD._TAddChVat,
        //IDH_JOBSHD._TAddChrg,IDH_JOBSHD._TAddCost,IDH_JOBSHD._ReqArch,IDH_JOBSHD._TrnCode,IDH_JOBSHD._JbToBeDoneDt,
        //IDH_JOBSHD._JbToBeDoneTm,IDH_JOBSHD._DrivrOnSitDt,IDH_JOBSHD._DrivrOnSitTm,IDH_JOBSHD._DrivrOfSitDt,
        //IDH_JOBSHD._DrivrOfSitTm,IDH_JOBSHD._OTComments,IDH_JOBSHD._BPWgt,IDH_JOBSHD._CarWgt,IDH_JOBSHD._PROINV,
        //IDH_JOBSHD._AddCharge,IDH_JOBSHD._AddCost,IDH_JOBSHD._TCostVtRt,IDH_JOBSHD._HCostVtRt};


        #endregion

        #region Conditionally DisabledFields
        private static String[] msaCDisabledFields = {IDH_JOBSHD._OrdWgt,IDH_JOBSHD._CusQty,
        IDH_JOBSHD._RdWgt,IDH_JOBSHD._Comment,IDH_JOBSHD._Origin,IDH_JOBSHD._HlSQty,IDH_JOBSHD._PRdWgt,
        IDH_JOBSHD._ProWgt,IDH_JOBSHD._IsTrl,IDH_JOBSHD._CBICont,IDH_JOBSHD._TRdWgt};

        //private List<String> moCDisabledFields = new List<String> { IDH_JOBSHD._OrdWgt,IDH_JOBSHD._CusQty,
        //IDH_JOBSHD._RdWgt,IDH_JOBSHD._Comment,IDH_JOBSHD._Origin,IDH_JOBSHD._HlSQty,IDH_JOBSHD._PRdWgt,
        //IDH_JOBSHD._ProWgt,IDH_JOBSHD._IsTrl,IDH_JOBSHD._CBICont,IDH_JOBSHD._TRdWgt};

        #endregion

        #region AlternativeWasteDescriptions

        private static String[] msaAltWasDsc = { "Reason 1", "Reason 2", "ect" };

        //private List<String> moAltWasDsc = new List<string> { "Reason 1", "Reason 2", "ect" };

        #endregion

        #endregion

        #region Getters & Setters

        public IDH_JOBSHD OrderScheduleManager {
            get { return (IDH_JOBSHD)moDBObject; }
        }

        public String[] BlackListedFields {
            get { return msaBlackListFields; }
            protected set {
                if (value != null)
                    msaBlackListFields = value;
            }
        }

        public String[] DisabledFields {
            get { return msaDisabledFields; }
            protected set {
                if (value != null)
                    msaDisabledFields = value;
            }
        }

        public String[] ConditionallyDisabledFields {
            get { return msaCDisabledFields; }
            set {
                if (value != null)
                    msaCDisabledFields = value;
            }
        }

        public String[] AltWasDsc {
            get { return msaAltWasDsc; }
            protected set {
                if (value != null)
                    msaAltWasDsc = value;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public OSM(IDHAddOns.idh.forms.Base oIDHForm, string sParentID, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentID, oSBOForm) {
                moSBOForm = oSBOForm;
                mbIsSearch = false;
        }

        #endregion

        #region Overideable Methord Hiding

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new idh.forms.oo.FormController("IDH_F2OSM", sParMenu, iMenuPos, "uBC_OSM.srf", false, true, false,
                "Order Schedule Manager", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);

            return owForm;
        }

        #endregion

        #region Overrides

        protected override void doInitialize() {
            moDBObject = new IDH_JOBSHD();
            Handler_Button_Find = new ev_Item_Event(doAddFindEvent);
            Handler_FORM_RESIZE = new ev_Item_Event(doResizeEvent);

            addHandler_ITEM_PRESSED("B_LTOP", new ev_Item_Event(doLayoutTop));
            addHandler_ITEM_PRESSED("B_LLEFT", new ev_Item_Event(doLayoutLeft));
            addHandler_ITEM_PRESSED("B_LRIGHT", new ev_Item_Event(doLayoutRight));
            addHandler_ITEM_PRESSED("B_CALLFF", new ev_Item_Event(doClearAllFilterFields));
            
        }

        public override void doSetListFields()
        {
            for (int i = 0; i < moDBObject.getColumnCount(); i++) {
                DBFieldInfo oFieldInfo = moDBObject.getFieldInfo(i);

            }
        }

        protected override void doGridLayout()
        {
            base.doGridLayout();
            CompanyRules();
            
        }

        public override void doFinalizeShow()
        {
            doFillGridCombos();
            base.doFinalizeShow();
        }

        public override void doCompleteCreate(ref bool BubbleEvent) {
            AutoManaged = false;

            SBOForm.EnableMenu(Config.NAV_ADD, false);
            SBOForm.EnableMenu(Config.NAV_FIND, false);
            SBOForm.EnableMenu(Config.NAV_FIRST, false);
            SBOForm.EnableMenu(Config.NAV_NEXT, false);
            SBOForm.EnableMenu(Config.NAV_PREV, false);
            SBOForm.EnableMenu(Config.NAV_LAST, false);
            
            base.doCompleteCreate(ref BubbleEvent);
            SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_All;
        }
        
        public override void doBeforeLoadData() {
            base.doBeforeLoadData();
            Mode = BoFormMode.fm_OK_MODE;
            moAdminGrid.setRequiredFilter(IDH_JOBSHD._Code + " = '@!#'");
            moAdminGrid.Handler_GRID_RIGHT_CLICK = new IDHGrid.ev_GRID_EVENTS(AltMenu);
            moAdminGrid.Handler_GRID_MENU_EVENT = new IDHGrid.ev_GRID_EVENTS(GridMenu);
            DynamicGrid();
        }

        public override void doLoadData() {
            base.doLoadData();
            moAdminGrid.doSetSUMColumns(SUMFIELDS);

            doSetMode(SAPbouiCOM.BoFormMode.fm_FIND_MODE);
        }

        public override bool doCustomItemEvent(ref IDHAddOns.idh.events.Base pVal) {
            //if (pVal.BeforeAction) {
            //    if (moAdminGrid.getSBOGrid().Rows.IsLeaf(pVal.Row)) {

            //        SAPbouiCOM.MenuItem oMenuItem;
            //        SAPbouiCOM.Menus oMenu;
            //        SAPbouiCOM.MenuCreationParams oCreationPackage;
            //        SAPbouiCOM.SelectedRows oSelected;
            //        string sRMenuId = IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU;

            //        oMenuItem = IDHForm.goParent.goApplication.Menus.Item(sRMenuId);
            //        int iMenu = 1;
            //        oCreationPackage = IDHForm.goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);
            //        oCreationPackage.Enabled = true;
            //        oMenu = oMenuItem.SubMenus;
            //        oCreationPackage.Type = BoMenuType.mt_STRING;
            //        oCreationPackage.UniqueID = "NEW";
            //        oCreationPackage.String = Translation.getTranslatedWord("New &Order");
            //        oCreationPackage.Position = iMenu;
            //        iMenu += 1;
            //        if(IDHForm.goParent.goApplication.Menus.Exists("NEW")){
            //            IDHForm.goParent.goApplication.Menus.RemoveEx("NEW");
            //        }
            //        oMenu.AddEx(oCreationPackage);

            //        oSelected = moAdminGrid.getSBOGrid().Rows.SelectedRows;
            //        int iSelectionCount = oSelected.Count;

            //        if (iSelectionCount > 0) {
            //            setWFValue("LASTJOBMENU", null);

            //            int iCurrentDataRow;

            //            if (moAdminGrid.getSBOGrid().CollapseLevel > 0) {
            //                iCurrentDataRow = moAdminGrid.GetDataTableRowIndex(pVal.Row);
            //            } else {
            //                iCurrentDataRow = oSelected.Item(0, SAPbouiCOM.BoOrderType.ot_RowOrder);
            //            }

            //            /* 
            //             * Actions that can take place on miltiple Rows
            //             */

            //            if (iSelectionCount > 1) {
            //                //Allow Duplicate Option for multi-row selection as well. for Non-USA clients only.

            //                if (!Config.INSTANCE.getParameterAsBool("USAREL", false)) {
            //                    oCreationPackage.UniqueID = "DUPLIMULTI";
            //                    oCreationPackage.String = Translation.getTranslatedWord("Duplicate Multi Rows");
            //                    iMenu += 1;
            //                    if(IDHForm.goParent.goApplication.Menus.Exists("DUPLIMULTI")){
            //                        IDHForm.goParent.goApplication.Menus.RemoveEx("DUPLIMULTI");
            //                    }
            //                    oMenu.AddEx(oCreationPackage);
            //                }

            //                if (moAdminGrid.doGetDeleteActive()) {
            //                    if (!(pVal.Row == iCurrentDataRow)) {
            //                        oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUREMM;
            //                        oCreationPackage.String = Translation.getTranslatedWord("Delete &Selection");
            //                        oCreationPackage.Enabled = true;
            //                        if(IDHForm.goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREMM)){
            //                            IDHForm.goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREMM);
            //                        }
            //                        oMenu.AddEx(oCreationPackage);
            //                    }
            //                }

            //                if (!Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_ARCHIVE)) {
            //                    oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE;
            //                    oCreationPackage.String = Translation.getTranslatedWord("Archive Rows");
            //                    oCreationPackage.Enabled = true;
            //                    if(IDHForm.goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE)){
            //                        IDHForm.goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE);
            //                    }
            //                    oMenu.AddEx(oCreationPackage);
            //                }
            //            } else if (iSelectionCount == 1) {

            //                string sCurrentJobType, sJob, sJobCd, sJobNr, sRowNr, sStatus, sWORStatus;
            //                ArrayList oJobTypes = null;

            //                sJobNr = (string)moAdminGrid.doGetFieldValue(IDH_JOBSHD._JobNr, iCurrentDataRow);
            //                sRowNr = (string)moAdminGrid.doGetFieldValue(IDH_JOBSHD._Code, iCurrentDataRow);
            //                sCurrentJobType = (string)moAdminGrid.doGetFieldValue(IDH_JOBSHD._JobTp, iCurrentDataRow);
            //                sStatus = (string)moAdminGrid.doGetFieldValue(IDH_JOBSHD._Status, iCurrentDataRow);
            //                sWORStatus = (string)moAdminGrid.doGetFieldValue(IDH_JOBSHD._RowSta, iCurrentDataRow);

            //                if (!sStatus.Equals("9")) {
            //                    oJobTypes = Config.INSTANCE.doGetAvailableWOJobTypes(sJobNr);
            //                }

            //                if(Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_DELETE) && sWORStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusDeleted())){
            //                    if( iCurrentDataRow >= 0){
            //                        oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUUNREML;
            //                        oCreationPackage.String = Translation.getTranslatedWord("UnDelete Waste Order Row") + "(" + (iCurrentDataRow+1) + ") [" + sRowNr +"]";
            //                        oCreationPackage.Enabled = true;
            //                        if(IDHForm.goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUUNREML)){
            //                            IDHForm.goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUUNREML);
            //                        }
            //                        oMenu.AddEx(oCreationPackage);
            //                    }
            //                }

            //                if(!Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_ARCHIVE) || Config.INSTANCE.doGetIsSupperUser()){
            //                    if(iCurrentDataRow >=0){
            //                        oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE;
            //                        oCreationPackage.String = Translation.getTranslatedWord("Archive Row") + " (" + (iCurrentDataRow + 1) +") [" + sRowNr + "]";
            //                        oCreationPackage.Enabled = true;
            //                        if(IDHForm.goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE)){
            //                            IDHForm.goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE);
            //                        }
            //                        oMenu.AddEx(oCreationPackage);
            //                    }
            //                }

            //                if(!sWORStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusDeleted()) && moAdminGrid.doGetDeleteActive()){
            //                    if(iCurrentDataRow>=0){
            //                        oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUREML;
            //                        oCreationPackage.String = Translation.getTranslatedWord("Delete Waste Order Row") + " (" + (iCurrentDataRow +1) + ") [" + sRowNr + "]";
            //                        oCreationPackage.Enabled = true;
            //                        if(IDHForm.goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREML)){
            //                            IDHForm.goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREML);
            //                        }
            //                        oMenu.AddEx(oCreationPackage);
            //                    }
            //                }

            //                IDH_JOBTYPE_SEQ oJobSeq = IDH_JOBTYPE_SEQ.getInstance();

            //                if(!(oJobSeq == null) && (oJobTypes.Count > 0)){
            //                    ArrayList oMenuList = new ArrayList();
                                
            //                    for(int iJob = 0; iJob < oJobTypes.Count - 1; iJob++){
            //                        try{
            //                            oJobSeq.gotoRow((int)oJobTypes[iJob]);
            //                            sJob = oJobSeq.U_JobTp;
            //                            sJobCd = oJobSeq.Code;
            //                            object[] oDT = new object[2];
            //                            oDT[1] = sJob;

            //                            int iIndex = sJob.IndexOf("&");
            //                            if(iIndex < -1 && iIndex > -1){
            //                                sJob = sJob.Substring(0,iIndex) + "and" + sJob.Substring(iIndex+1);
            //                            }

            //                            if(sJob.Equals(sCurrentJobType)){
            //                                oCreationPackage.UniqueID = "DUPLICATE";
            //                                oCreationPackage.String = Translation.getTranslatedWord("Duplicate Waste Order Row") + " (" + (iCurrentDataRow + 1) + ") [" + sRowNr + "]";
            //                                oCreationPackage.Position = iMenu;
            //                                iMenu += 1;
            //                                if(IDHForm.goParent.goApplication.Menus.Exists("DUPLICATE")){
            //                                    IDHForm.goParent.goApplication.Menus.RemoveEx("DUPLICATE");
            //                                }
            //                                oMenu.AddEx(oCreationPackage);
            //                            }

            //                            string sTitle = Translation.getTranslatedWord("Create a [%1] Order",sJob);

            //                            if(sJobCd.Length > 8){
            //                                sJobCd ="JB_" + sJobCd.Substring(0,6).ToUpper();
            //                            }else{
            //                                sJobCd = "JB_" + sJobCd.ToUpper();
            //                            }

            //                            oDT[0] = sJobCd;
            //                            oMenuList.Add(oDT);

            //                            if(!IDHForm.goParent.goApplication.Menus.Exists(sJobCd)){
            //                                oCreationPackage.UniqueID = sJobCd;
            //                                oCreationPackage.String = sTitle;
            //                                oCreationPackage.Position = iMenu;
            //                                iMenu += 1;
            //                                if(IDHForm.goParent.goApplication.Menus.Exists(sJobCd)){
            //                                    IDHForm.goParent.goApplication.Menus.RemoveEx(sJobCd);
            //                                }
            //                                oMenu.AddEx(oCreationPackage);
            //                            }
            //                        }catch{

            //                        }
            //                    }
            //                    setWFValue("LASTJOBMENU",oMenuList);
            //                }else{
            //                    oCreationPackage.UniqueID = "NEWWO";
            //                    oCreationPackage.String = Translation.getTranslatedWord("Create a new Waste Order from Current");
            //                    oCreationPackage.Position = iMenu;
            //                    iMenu += 1;
            //                    if(IDHForm.goParent.goApplication.Menus.Exists("NEWWO")){
            //                        IDHForm.goParent.goApplication.Menus.RemoveEx("NEWWO");
            //                    }
            //                    oMenu.AddEx(oCreationPackage);
            //                }
            //            }
            //            oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUEDIT;
            //            oCreationPackage.String = Translation.getTranslatedWord("Set &Values for the selected items");
            //            oCreationPackage.Position = iMenu;
            //            iMenu++;
            //            if(IDHForm.goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUEDIT)){
            //                IDHForm.goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUEDIT);
            //            }
            //            oMenu.AddEx(oCreationPackage);
            //        }
            //        return false;
            //    }
            //    else{
            //        //to hide the popup menu
            //        if(IDHForm.goParent.goApplication.Menus.Exists("DUPLICATE")){
            //            IDHForm.goParent.goApplication.Menus.RemoveEx("DUPLICATE");
            //        }
            //        if(IDHForm.goParent.goApplication.Menus.Exists("NEW")){
            //            IDHForm.goParent.goApplication.Menus.RemoveEx("NEW");
            //        }
            //        if(IDHForm.goParent.goApplication.Menus.Exists("DUPLIMULTI")){
            //            IDHForm.goParent.goApplication.Menus.RemoveEx("DUPLIMULTI");
            //        }
            //        ArrayList oMenuList = (ArrayList)getWFValue("LASTJOBMENU");
            //        string sJob;
            //        object[] oPar = new object[2];
            //        if(!(oMenuList == null) && (oMenuList.Count > 0)){
            //            for(int iJob = 0; iJob < oMenuList.Count ;iJob ++){
            //                oPar = (object[])oMenuList[iJob];
            //                sJob = (string)oPar[0];

            //                if(IDHForm.goParent.goApplication.Menus.Exists(sJob)){
            //                    IDHForm.goParent.goApplication.Menus.RemoveEx(sJob);
            //                }
            //            }
            //        }
            //        if(IDHForm.goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREMM)){
            //            IDHForm.goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREMM);
            //        }
            //        if(IDHForm.goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREML)){
            //            IDHForm.goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREML);
            //        }
            //        if(IDHForm.goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUEDIT)){
            //            IDHForm.goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUEDIT);
            //        }
            //        if(IDHForm.goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUUNREML)){
            //            IDHForm.goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUUNREML);
            //        }
            //        if(IDHForm.goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE)){
            //            IDHForm.goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE);
            //        }
            //        if(IDHForm.goParent.goApplication.Menus.Exists("NEWWO")){
            //            IDHForm.goParent.goApplication.Menus.RemoveEx("NEWWO");
            //        }
                    
            //        setWFValue("LASTJOBMENU",null);
            //        return true;
            //    }
            //} 
            return true;
        }

        public override bool doAddUpdateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                if (moAdminGrid.doProcessData()) {
                    doLoadData();

                    Mode = BoFormMode.fm_OK_MODE;
                    doSetMode(BoFormMode.fm_FIND_MODE);
                    BubbleEvent = false;
                }
            }
            return true;
        }
         
        #endregion

        #region Events

        public bool doAddFindEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                //moAdminGrid.doReloadSetExtra(true, false);
                doLoadData();
            }
            return true;
        }

        #endregion

        #region Handlers

        public bool doLayoutTop(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                moDynamicLayout.doLayout(LPosition.pos_TOP,LOption.NORMAL);
                moSBOForm.Refresh();
            } 
            return true;
        }

        public bool doLayoutLeft(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                moDynamicLayout.doLayout(LPosition.pos_LEFT, LOption.NORMAL);
                moSBOForm.Refresh();
            } 
            return true;
        }

        public bool doLayoutRight(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                moDynamicLayout.doLayout(LPosition.pos_RIGHT, LOption.NORMAL);
                moSBOForm.Refresh();
            } 
            return true;
        }
        public bool doResizeEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                moDynamicLayout.doLayout(moDynamicLayout.HideShowFF.CurrentState.oLastButtonPressed, LOption.RESIZE);
                moButtonLeft = moSBOForm.Items.Item("B_LLEFT");
                moButtonLeft.Left = 0;
                moButtonLeft.Top = 12;
                moButtonLeft.Width = 10;
                moButtonLeft.Height = moSBOForm.ClientHeight - 12;

                moButtonTop = moSBOForm.Items.Item("B_LTOP");
                moButtonTop.Left = 0;
                moButtonTop.Top = 0;
                moButtonTop.Width = moSBOForm.ClientWidth;
                moButtonTop.Height = 10;

                moButtonRight = moSBOForm.Items.Item("B_LRIGHT");
                moButtonRight.Left = moSBOForm.ClientWidth - 10;
                moButtonRight.Top = 12;
                moButtonRight.Width = 10;
                moButtonRight.Height = moSBOForm.ClientHeight - 12;
            }

            return true;
        }

        public bool doClearAllFilterFields(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                doClear();
            }
            return true;
        }

        public bool AltMenu(ref IDHAddOns.idh.events.Base pVal) {
                if (pVal.BeforeAction) {
                    //Display the popup
                    if(moAdminGrid.getSBOGrid().Rows.IsLeaf(pVal.Row)){
                        SAPbouiCOM.MenuItem oMenuItem;
                        SAPbouiCOM.Menus oMenus;
                        SAPbouiCOM.MenuCreationParams oCreationPackage;
                        SAPbouiCOM.SelectedRows oSelected;

                        string sRMenuId = IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU;
                        oMenuItem = IDHForm.goParent.goApplication.Menus.Item(sRMenuId);
                        int iMenuPos = 1;
                        oCreationPackage = (SAPbouiCOM.MenuCreationParams)IDHForm.goParent.goApplication.CreateObject(BoCreatableObjectType.cot_MenuCreationParams);
                        oCreationPackage.Enabled = true;
                        oMenus = oMenuItem.SubMenus;
                        oCreationPackage.Type = BoMenuType.mt_STRING;

                        #region New
                        oCreationPackage.UniqueID = "NEW";
                        oCreationPackage.String = Translation.getTranslatedWord("New &Order");
                        oCreationPackage.Position = iMenuPos++;
                        oMenus.AddEx(oCreationPackage);
                        #endregion

                        oSelected = moAdminGrid.getSBOGrid().Rows.SelectedRows;
                        int  iSelectionCount = oSelected.Count;

                        if(iSelectionCount > 0){
                            setWFValue("LASTJOBMENU",null);
                            int iCurrentDataRow;
                            //Fix for RightClick Menu when Grid is in Group Mode
                            if(moAdminGrid.getSBOGrid().CollapseLevel > 0){
                                iCurrentDataRow = moAdminGrid.getSBOGrid().GetDataTableRowIndex(pVal.Row);
                            }else{
                                iCurrentDataRow = oSelected.Item(0,SAPbouiCOM.BoOrderType.ot_RowOrder);
                            }
                            //Actions that can take place on multiple rows

                            if(iSelectionCount > 1){
                                //Allow Duplicate Option for multi-row selection as well. For Non-USA clients only
                                if(!Config.INSTANCE.getParameterAsBool("USAREL",false)){
                                       
                                    #region DUPLIMULTI
                                    oCreationPackage.UniqueID = "DUPLIMULTI";
                                    oCreationPackage.String = Translation.getTranslatedWord("Duplicate Multi Rows");
                                    oCreationPackage.Position = iMenuPos++;
                                    oMenus.AddEx(oCreationPackage);
                                    #endregion
                                }
                                if(moAdminGrid.doGetDeleteActive()){

                                    if(!(pVal.Row == iCurrentDataRow)){
                                        #region GRIDMENUREMM
                                        oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUREMM;
                                        oCreationPackage.String = Translation.getTranslatedWord("Delete &Selection");
                                        oMenus.AddEx(oCreationPackage);
                                        #endregion
                                    }
                                }
                                if (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_ARCHIVE) == false || 
                                    Config.INSTANCE.doGetIsSupperUser()) {
                                    #region GRIDMENUARCHIVE
                                    oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE;
                                    oCreationPackage.String = Translation.getTranslatedWord("Archive Rows");
                                    oCreationPackage.Enabled = true;
                                    oMenus.AddEx(oCreationPackage);
                                    #endregion
                                }

                            }
                            else if(iSelectionCount == 1){
                                string sCurrentJobType,sJob,sJobCd,sJobNr,sRowNr,sStatus,sWORStatus;
                                ArrayList oJobTypes = null;

                                sJobNr = (string)moAdminGrid.doGetFieldValue(IDH_JOBSHD._JobNr,iCurrentDataRow);
                                sRowNr = (string)moAdminGrid.doGetFieldValue(IDH_JOBSHD._Code,iCurrentDataRow);
                                sStatus = (string)moAdminGrid.doGetFieldValue(IDH_JOBSHD._Status,iCurrentDataRow);
                                sWORStatus = (string)moAdminGrid.doGetFieldValue(IDH_JOBSHD._RowSta,iCurrentDataRow);
                                sCurrentJobType = (string)moAdminGrid.doGetFieldValue(IDH_JOBSHD._JobTp,iCurrentDataRow);

                                if(!sStatus.Equals("9")){
                                    oJobTypes = Config.INSTANCE.doGetAvailableWOJobTypes(sJobNr);
                                }

                                if(Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_DELETE) && sWORStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusDeleted())){
                                    if (iCurrentDataRow >= 0) {
                                        #region GRIDMENUUNREML
                                        oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUUNREML;
                                        oCreationPackage.String = Translation.getTranslatedWord("UnDelete Waste Order Row") + " (" + (iCurrentDataRow+1) + ")[" + sRowNr + "]";
                                        oCreationPackage.Enabled = true;
                                        oMenus.AddEx(oCreationPackage);
                                        #endregion
                                    }
                                }
                                if(Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_ARCHIVE) == false || 
                                    Config.INSTANCE.doGetIsSupperUser()){

                                    if(iCurrentDataRow >= 0){
                                        #region GRIDMENUARCHIVE
                                        oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE;
                                        oCreationPackage.String = Translation.getTranslatedWord("Archive Row") + " (" + (iCurrentDataRow+1) + ")[" + sRowNr + "]";
                                        oCreationPackage.Enabled = true;
                                        oMenus.AddEx(oCreationPackage);
                                        #endregion

                                    }
                                }
                                if(!sWORStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusDeleted()) && moAdminGrid.doGetDeleteActive()){

                                    if(iCurrentDataRow >= 0){
                                        #region GRIDMENUREML
                                        oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUREML;
                                        oCreationPackage.String = Translation.getTranslatedWord("Delete Waste Order Row") + " (" + (iCurrentDataRow + 1) + ")[" + sRowNr + "]";
                                        oCreationPackage.Enabled = true;
                                        oMenus.AddEx(oCreationPackage);
                                        #endregion
                                    }
                                }
                                IDH_JOBTYPE_SEQ oJobSeq = IDH_JOBTYPE_SEQ.getInstance();

                                if(!(oJobTypes == null) && (oJobTypes.Count > 0)){
                                    ArrayList oMenuList = new ArrayList();
                                    
                                    for(int iJob = 0; iJob<oJobTypes.Count -1 ; iJob ++){
                                        try{
                                            oJobSeq.gotoRow(oJobTypes.IndexOf(oJobTypes[iJob]));
                                            sJob = oJobSeq.U_JobTp;
                                            sJobCd = oJobSeq.Code;
                                            object[] oDt = new object[2];
                                            oDt[1] = sJob;

                                            //Filter out the &
                                            int iIndex = sJob.IndexOf("&");
                                            if(iIndex < -1 || iIndex > -1){
                                                sJob = sJob.Substring(0,iIndex) + "and " + sJob.Substring(iIndex+1);
                                            }

                                            if(sJob.Equals(sCurrentJobType)){
                                                #region DUPLICATE
                                                oCreationPackage.UniqueID = "DUPLICATE";
                                                oCreationPackage.String = Translation.getTranslatedWord("Duplicate Waste Order Row") + " (" + (iCurrentDataRow + 1) + ")[" + sRowNr + "]";
                                                oCreationPackage.Position = iMenuPos++;
                                                oMenus.AddEx(oCreationPackage);
                                                #endregion
                                            }
                                            string sTitle = Translation.getTranslatedWord("Create a [%1] Order",sJob);
                                            if(sJobCd.Length > 8){
                                                sJobCd = "JB_" + sJob.ToUpper();
                                            }else{
                                                sJobCd = "JB_" + sJobCd.ToUpper();
                                            }
                                            oDt[0] = sJobCd;
                                            oMenuList.Add(oDt);

                                            if(!IDHForm.goParent.goApplication.Menus.Exists(sJobCd)){
                                                #region sJobCd
                                                oCreationPackage.UniqueID = sJobCd;
                                                oCreationPackage.String = sTitle;
                                                oCreationPackage.Position = iMenuPos++;
                                                oMenus.AddEx(oCreationPackage);
	                                            #endregion
                                            }
                                        }catch{

                                        }
                                    }
                                    setWFValue("LASTJOBMENU",oMenuList);
                                }
                                else{
                                    #region NEWWO
                                    oCreationPackage.UniqueID = "NEWWO";
                                    oCreationPackage.String = Translation.getTranslatedWord("Create a new Waste Order from Current");
                                    oCreationPackage.Position = iMenuPos++;
                                    oMenus.AddEx(oCreationPackage);
                                    #endregion
                                }
                            }
                            #region GRIDMENUEDIT
		                    oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUEDIT;
                            oCreationPackage.String = Translation.getTranslatedWord("SET &Values for the Selected Items");
                            oCreationPackage.Position = iMenuPos++;
                            oMenus.AddEx(oCreationPackage);
                            #endregion
                        }
                        return false;
                    }
                }else{
                    //!pVal.BeforeAction
                    //To Hide the Popup Menu
                    RemoveMenu("DUPLICATE");
                    RemoveMenu("NEW");
                    RemoveMenu("DUPLIMULTI");
                    
                    ArrayList oMenuL = (ArrayList)getWFValue("LASTJOBMENU");
                    string sJob;
                    if(!(oMenuL == null) && oMenuL.Count > 0){
                        for(int iJob = 0;iJob<oMenuL.Count;iJob++){
                            sJob = (string)((object[])(oMenuL[iJob]))[0];
                            RemoveMenu(sJob);
                        }
                    }
                    RemoveMenu(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREMM);
                    RemoveMenu(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREML);
                    RemoveMenu(IDHAddOns.idh.controls.IDHGrid.GRIDMENUEDIT);
                    RemoveMenu(IDHAddOns.idh.controls.IDHGrid.GRIDMENUUNREML);
                    RemoveMenu(IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE);
                    RemoveMenu("NEWWO");
                    setWFValue("LASTJOBMENU",null);
                    return true;
                }
            return false;
        } 

        public bool GridMenu(ref IDHAddOns.idh.events.Base pVal){
            if(!pVal.BeforeAction){
                SAPbouiCOM.MenuEvent opVal = (SAPbouiCOM.MenuEvent)pVal.oData;
                if(opVal.MenuUID.Equals("NEW")){
                    //do new order row
                    return false;
                }
                else if(opVal.MenuUID.Equals(IDHAddOns.idh.controls.IDHGrid.GRIDMENUUNREML)){
                    //            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                    //            oGridN.doReactivateSelection()
                    Mode = BoFormMode.fm_UPDATE_MODE; 
                    return false;
                }
                else if(opVal.MenuUID.Equals(IDHAddOns.idh.controls.IDHGrid.GRIDMENUARCHIVE)){
                    //            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                    //            oGridN.doRequestArchive()
                    Mode = BoFormMode.fm_UPDATE_MODE;
                    return false;
                }
                else if(opVal.MenuUID.Equals("NEWWO")){
                    //doNewOrderFromCurrent()
                    return false;
                }
                else if(opVal.MenuUID.Equals("DUPLIMULTI")){
                    SAPbouiCOM.SelectedRows oSelected = moAdminGrid.getSBOGrid().Rows.SelectedRows;

                    if(!(oSelected == null) && oSelected.Count > 1){
                        //int iNoOfCopies = 1;
                        string bCopyADDI = Config.Parameter("WOADDITM",false);
                        string bCopyComm = Config.Parameter("WOCOMENT",false);
                        string bCopyPrice = Config.Parameter("WOCOPYPR",false);
                        string bCopyQty = Config.Parameter("WOCOPYQT",false);
                        string bCopyVehReg = Config.Parameter("WOCPYVHR",false);
                        string bCopyDriver = Config.Parameter("WOCPYDRV",false);
                        //                Dim sNewCode As String = doDuplicate(oForm, bCopyComm, bCopyADDI, bCopyPrice, bCopyQty, bCopyVehReg, bCopyDriver, "")
                       // do reload data
                    }
                    return false;
                }
                else if(opVal.MenuUID.Equals("DUPLICATE")){
                    //Check if this is a valid request
                    SAPbouiCOM.SelectedRows oSelected = moAdminGrid.getSBOGrid().Rows.SelectedRows;
                    if (!(oSelected == null) && oSelected.Count > 0) {
                        if (Config.INSTANCE.getParameterAsBool("MDDUWA", false)) {
                            setSharedData("SETDUP", "TRUE");
                            IDHForm.goParent.doOpenModalForm("IDH_DUPLICATE", SBOForm);
                        }
                        else {
                            int iNoOfCopies = com.idh.utils.Conversions.ToInt(Config.ParamaterWithDefault("WODUPLNO","1"));
                            string bCopyADDI = Config.Parameter("WOADDITM", false);
                            string bCopyComm = Config.Parameter("WOCOMENT", false);
                            string bCopyPrice = Config.Parameter("WOCOPYPR", false);
                            string bCopyQty = Config.Parameter("WOCOPYQT", false);
                            string bCopyVehReg = Config.Parameter("WOCPYVHR", false);
                            string bCopyDriver = Config.Parameter("WOCPYDRV", false);

                            if (iNoOfCopies < 1) { iNoOfCopies = 1; }
                            for (int iCnt = 1; iCnt < iNoOfCopies; iCnt++) {
                                 
                                //Dim sNewCode As String = doDuplicate(oForm, bCopyComm, bCopyADDI, bCopyPrice, bCopyQty, bCopyVehReg, bCopyDriver, "")
                            }
                            //moAdminGrid.doReloadData();
                        }
                    }
                    return false;
                }
                else if(opVal.MenuUID.StartsWith("JB_")){
                    ArrayList oMenuList = (ArrayList)getWFValue("LASTJOBMENU");
                    string sMenuItem,sJob;

                    if (!(oMenuList == null) && oMenuList.Count > 0) {
                        for (int iJob = 0; iJob < oMenuList.Count; iJob++) {
                            sMenuItem = (string)((object[])oMenuList[iJob])[0];
                            if (opVal.MenuUID.Equals(sMenuItem)) {
                                sJob = (string)((object[])oMenuList[iJob])[1];
                                //                        Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                                //                        oGridN.setCurrentLineByClick(pVal.Row)
                                //if(!(sRowCode == null) && sRowCode.Length > 0){
                                ArrayList oData = new ArrayList();
                                    oData.Add("DoAuto"); 
                                    //oData.Add(sRowCode);
                                    oData.Add(moAdminGrid.doGetFieldValue(IDH_JOBSHD._BDate));
                                    oData.Add(IDHForm.goParent.doTimeStrToStr((string)moAdminGrid.doGetFieldValue(IDH_JOBSHD._BTime)));
                                    oData.Add(moAdminGrid.doGetFieldValue(IDH_JOBENTR._ZpCd));
                                    oData.Add(moAdminGrid.doGetFieldValue(IDH_JOBENTR._Address));
                                    oData.Add("CANNOTDOCOVERAGE");
                                    oData.Add(moAdminGrid.doGetFieldValue(IDH_JOBENTR._SteId));

                                    //Added to handle the BP Switching
                                    oData.Add(null);
                                    oData.Add(moAdminGrid.doGetFieldValue(IDH_JOBENTR._PAddress));
                                    oData.Add(moAdminGrid.doGetFieldValue(IDH_JOBENTR._PZpCd));
                                    oData.Add(moAdminGrid.doGetFieldValue(IDH_JOBENTR._PPhone1));
                                    oData.Add(moAdminGrid.doGetFieldValue(IDH_JOBENTR._FirstBP));

                                    //Adding WO Header status for TFS 
                                    //oData.Add(getWOHStatus(oForm, oGridN.doGetFieldValue("r.U_JobNr")))
                                    doOpenModalForm("IDHJOBS");


                               // }
                              

                            }
                        }
                        return false;
                    } 
                    else {
                        if(opVal.MenuUID.Equals(IDHAddOns.idh.controls.IDHGrid.GRIDMENUEDIT)){
                            //                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                            //                If oGridN.getSBOGrid().Rows.SelectedRows.Count = 1 Then
                            //                    oGridN.setCurrentLineByClick(pVal.Row)
                            string sVehReg = (string)moAdminGrid.doGetFieldValue(IDH_JOBSHD._Lorry);
                            setSharedData("IDH_VEHREG",sVehReg);

                            string sCarrCd = (string)moAdminGrid.doGetFieldValue(IDH_JOBSHD._CarrCd);
                            setSharedData("IDH_CARCD",sCarrCd);

                            string sCarrName = (string)moAdminGrid.doGetFieldValue(IDH_JOBSHD._CarrNm);
                            setSharedData("IDH_CARNM",sCarrName);

                            string sDriver = (string)moAdminGrid.doGetFieldValue(IDH_JOBSHD._Driver);
                            setSharedData("IDH_DRIVER",sDriver);

                            string sCarrierRefNo = (string)moAdminGrid.doGetFieldValue(IDH_JOBSHD._SupRef);
                            setSharedData("IDH_CARREF",sCarrierRefNo);

                            setSharedData("IDH_OLDVR",sVehReg);
                            setSharedData("IDH_OLDCC",sCarrCd);
                        }
                        doOpenModalForm("IDH_OSMEDIT");
                        Mode =BoFormMode.fm_UPDATE_MODE;

                    }
                    return false;
                }
            }
            return true;
        }

        #endregion

        #region Dynamic Grid Functions
        private void DynamicGrid() {
            moDynamicLayout = new LayoutFF(ref moSBOForm, ref moAdminGrid,bGetByFormTypeId: true);
            
        }
        #endregion

        #region Other Logic

        protected virtual void CompanyRules() {
            for (int i = 0; i < moDBObject.getColumnCount(); i++) {
                DBFieldInfo oFieldInfo = moDBObject.getFieldInfo(i);
                /*
                 * Ignore the blacklisted Fields in moBlackListedFields
                 */
                if (!msaBlackListFields.Contains(oFieldInfo.FieldName)) {
                    if (msaDisabledFields.Contains(oFieldInfo.FieldName)) {
                        /*
                         * Spelling Corrections
                         */
                        if (oFieldInfo.FieldName.Equals(IDH_JOBSHD._HCostVtRt))
                            moAdminGrid.doAddListField(oFieldInfo.FieldName, "Haulage Buying VAT Rate", false, -1, ListFields.LISTTYPE_IGNORE, null);
                        else if (oFieldInfo.FieldName.Equals(IDH_JOBSHD._AddEx))
                            moAdminGrid.doAddListField(oFieldInfo.FieldName, "Additional Expenses", false, -1, ListFields.LISTTYPE_IGNORE, null);
                        else if (oFieldInfo.FieldName.Equals(IDH_JOBSHD._PCTotal))
                            moAdminGrid.doAddListField(oFieldInfo.FieldName, "Producer Co-Charge Total", false, -1, ListFields.LISTTYPE_IGNORE, null);
                        /*
                        * End
                        */
                        else
                            moAdminGrid.doAddListField(oFieldInfo.FieldName, oFieldInfo.Description, false, -1, ListFields.LISTTYPE_IGNORE, null);

                    }
                    else {
                        /*
                         * Spelling Corrections
                         */
                        if (oFieldInfo.FieldName.Equals(IDH_JOBSHD._TCTotal))
                            moAdminGrid.doAddListField(oFieldInfo.FieldName, "Tipping Co-Charge Total", true, -1, ListFields.LISTTYPE_IGNORE, null);
                        /*
                         * End
                         */

                        /*
                         * Make Alternative Description a Combo
                         */
                        if (oFieldInfo.FieldName.Equals(IDH_JOBSHD._AltWasDsc))
                            moAdminGrid.doAddListField(oFieldInfo.FieldName, oFieldInfo.Description, true, -1, ListFields.LISTTYPE_COMBOBOX, null);
                        else
                            moAdminGrid.doAddListField(oFieldInfo.FieldName, oFieldInfo.Description, true, -1, ListFields.LISTTYPE_IGNORE, null);
                    }
                }
                else
                    moAdminGrid.doAddListFieldNotPinned(oFieldInfo.FieldName, oFieldInfo.Description, false, 0, ListFields.LISTTYPE_IGNORE, null);
            }
        }

        private void doFillGridCombos() {
            SAPbouiCOM.ComboBoxColumn oCombo;

            /*
             * Alternative Waste Description
             */
            int iIndex = this.moAdminGrid.doIndexFieldWC(IDH_JOBSHD._AltWasDsc);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;

            foreach (String sDsc in msaAltWasDsc) {
                oCombo.ValidValues.Add(sDsc, "");

            }

        }

        protected void doSetMode(SAPbouiCOM.BoFormMode iMode) {
            try {
                if (iMode == SAPbouiCOM.BoFormMode.fm_FIND_MODE && Mode != SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
                    ((SAPbouiCOM.Button)Items.Item("1").Specific).Caption = Translation.getTranslatedWord("Find");
                }
                else if (iMode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
                    ((SAPbouiCOM.Button)Items.Item("1").Specific).Caption = Translation.getTranslatedWord("Update");
                    //Items.Item("2").Visible = true;
                }
                //oForm.Mode = iMode
            }
            catch (Exception) {
            }
        }

        private void doClear() {
            moDynamicLayout.doClearSBOFFItems();
            
        }

        private void RemoveMenu(string sMenuUniqueID){
            if(IDHForm.goParent.goApplication.Menus.Exists(sMenuUniqueID))
                IDHForm.goParent.goApplication.Menus.RemoveEx(sMenuUniqueID);
        }

        #endregion

    }
   }
