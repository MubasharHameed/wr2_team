﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.dbObjects.User;
using com.idh.controls;
using com.uBC.utils.Dynamic.Structs;

namespace com.uBC.utils.Dynamic {
    public class LayoutFF {
        private IDH_FFIELDS moIDH_FFIELDS;
        private SAPbouiCOM.Item moFrame;
        private SAPbouiCOM.Item moEffectiveFrame;

        private SAPbouiCOM.Form moSBOForm;
        private DBOGrid moAdminGrid;
        private List<SBOFFItem> moItemCache;

        private static int miFrameBuffer = 2;
        public static int FrameBuffer {
            get { return miFrameBuffer; }
            set { miFrameBuffer = value; }
        }

        private static int miFooter = 30;
        public static int Footer {
            get { return miFooter; }
            set { miFooter = value; }
        }

        private static int miFrameWidth = com.uBC.utils.Dynamic.SBOFFItem.LeftRightWidth + (2 * miFrameBuffer);
        public static int FrameWidth {
            get { return miFrameWidth; }
        }

        private int miNextVirticalValue;
        public int NextVerticalValue{
            get{return miNextVirticalValue;}
            set{miNextVirticalValue = value;}
        }

        private Structs.NextPos moCurrentPos = new NextPos();
        
        private HideShowFF moHideShowFF;
        public HideShowFF HideShowFF {
            get { return moHideShowFF; }
        }


        private static int miUIDCount = 1;

        private bool mbGetByUser;
        private bool mbGetByFormTypeId;

        private static string dt_LONG_NUMBER = "dt_LONG_NUMBER",
                             dt_SHORT_NUMBER = "dt_SHORT_NUMBER",
                             dt_QUANTITY = "dt_QUANTITY",
                             dt_PRICE = "dt_PRICE",
                             dt_RATE = "dt_RATE",
                             dt_MEASURE = "dt_MEASURE",
                             dt_SUM = "dt_SUM",
                             dt_PERCENT = "dt_PERCENT",
                             dt_LONG_TEXT = "dt_LONG_TEXT",
                             dt_SHORT_TEXT = "dt_SHORT_TEXT",
                             dt_DATE = "dt_DATE";


        public LayoutFF(ref SAPbouiCOM.Form oSBOForm,ref DBOGrid oAdminGrid,ref SAPbouiCOM.Item oFrame, bool bGetByUser = false, bool bGetByFormTypeId = true) {
            moFrame = oFrame;
            new LayoutFF(ref oSBOForm, ref oAdminGrid);
        }

        public LayoutFF(ref SAPbouiCOM.Form oSBOForm,ref DBOGrid oAdminGrid,bool bGetByUser = false,bool bGetByFormTypeId = true) {
            moSBOForm = oSBOForm;
            moAdminGrid = oAdminGrid;
            mbGetByUser = bGetByUser;
            mbGetByFormTypeId = bGetByFormTypeId;

            doInit();

        }

        private void doInit() {
            //Set up containers
            if (moFrame == null) {
                moFrame = moSBOForm.Items.Add("uBC_FRAMEA", SAPbouiCOM.BoFormItemTypes.it_RECTANGLE);
                moFrame.Visible = false;
            }
            moEffectiveFrame = moSBOForm.Items.Add("uBC_FRAMEB", SAPbouiCOM.BoFormItemTypes.it_RECTANGLE);
            moEffectiveFrame.Visible = false;

            //Set Up Buttons
            
            //Get Data From DBObject
            moIDH_FFIELDS = new IDH_FFIELDS();
            moIDH_FFIELDS.getData();

            //Initialize TopLayout
            
            //Frame
            moFrame.Top = 10;
            moFrame.Left = 10;
            moFrame.Width = moSBOForm.ClientWidth - 20;
            moFrame.Height = 150;
            doAlignment();
            moItemCache = doSBOFFItemCreate();
            doPopulateSBOFFItem();

            moHideShowFF = new HideShowFF();

        }
        public void doLayout(LPosition oLayoutPos,LOption oOption) {

            moHideShowFF.doRelocate(ref moSBOForm, ref moFrame, ref moAdminGrid, ref moItemCache, oLayoutPos, oOption, miNextVirticalValue);
            doAlignment();
            doMoveSBOFFItems();
        }
        
        private void doAlignment() {
            moEffectiveFrame.Left = moFrame.Left + miFrameBuffer;
            moEffectiveFrame.Top = moFrame.Top + miFrameBuffer;
            moEffectiveFrame.Width = moFrame.Width - (2 * miFrameBuffer);
            moEffectiveFrame.Height = moFrame.Height - (2 * miFrameBuffer);
            moCurrentPos.iLeft = moEffectiveFrame.Left;
            moCurrentPos.iTop = moEffectiveFrame.Top;

        }
        private void doMoveSBOFFItems() {
            foreach (SBOFFItem oSBOFFItem in moItemCache) {
                oSBOFFItem.doMove(moCurrentPos.iLeft, moCurrentPos.iTop);
                oSBOFFItem.HideNotShow = moHideShowFF.HideNotShow;
                oSBOFFItem.IsVisibleOnForm = (moCurrentPos.iTop + 15) < (moEffectiveFrame.Height + 15);
                oSBOFFItem.doSetVisiblity();
                doGetNextPos(oSBOFFItem);
            }
        }
        
        private void doGetNextPos(SBOFFItem oSBOFFItem){
            if ((moCurrentPos.iLeft + SBOFFItem.LeftRightWidth) < (moEffectiveFrame.Width - SBOFFItem.LeftRightWidth+5)) {
                moCurrentPos.iLeft += com.uBC.utils.Dynamic.SBOFFItem.LableWidth + 1 + com.uBC.utils.Dynamic.SBOFFItem.ControlWidth + 5;
            }
            else {
                if ((moCurrentPos.iTop + 15) < (moEffectiveFrame.Height + 15)) {
                    moCurrentPos.iTop += 15;
                    moCurrentPos.iLeft = moEffectiveFrame.Left;
                }
                
            }
        }
       
       

        private List<SBOFFItem> doSBOFFItemCreate() {
            com.uBC.utils.Dynamic.Structs.Lable oLable;
            com.uBC.utils.Dynamic.Structs.Control oControl;
            com.uBC.utils.Dynamic.SBOFFItem oSBOFFItem;
            List<com.uBC.utils.Dynamic.SBOFFItem> oSBOItems = new List<utils.Dynamic.SBOFFItem>();
            int iRecords = 0;

            string sFieldName;
            int iStartLeft = moEffectiveFrame.Left;
            int iStartTop = moEffectiveFrame.Top;
            if (mbGetByFormTypeId)
                iRecords = moIDH_FFIELDS.getByFormTypeId(moSBOForm.TypeEx);
            else if (mbGetByUser)
                iRecords = moIDH_FFIELDS.getByUser(com.idh.bridge.DataHandler.INSTANCE.User, moSBOForm.TypeEx);

            if (iRecords > 0) {
                while (moIDH_FFIELDS.next()) {
                    //LABLE
                    sFieldName = "uBC_S_" + miUIDCount++;
                    oLable = new utils.Dynamic.Structs.Lable();
                    oLable.oItem = moSBOForm.Items.Add(sFieldName, SAPbouiCOM.BoFormItemTypes.it_STATIC);
                    oLable.oItem.Left = moCurrentPos.iLeft;
                    oLable.oItem.Top = moCurrentPos.iTop;
                    oLable.oItem.Width = com.uBC.utils.Dynamic.SBOFFItem.LableWidth;
                    oLable.oItem.Height = com.uBC.utils.Dynamic.SBOFFItem.Height;
                    oLable.sCaption = moIDH_FFIELDS.U_Lable;
                    oLable.oItem.Visible = false;

                    //Filter Field
                    oLable.oFFInfo = new utils.Dynamic.Structs.FiterFieldInfo();
                    oLable.oFFInfo.sFieldName = sFieldName;
                    oLable.oFFInfo.sDBField = moIDH_FFIELDS.U_DBFld;
                    oLable.oFFInfo.oType = getBoDataTypeFromString(moIDH_FFIELDS.U_oType);
                    oLable.oFFInfo.iLenght = moIDH_FFIELDS.U_Length;
                    oLable.oFFInfo.sOperator = moIDH_FFIELDS.U_Op;
                    moAdminGrid.doAddFilterField(oLable.oFFInfo.sFieldName, oLable.oFFInfo.sDBField, oLable.oFFInfo.oType, oLable.oFFInfo.sOperator, oLable.oFFInfo.iLenght);

                    
                    //Control
                    sFieldName = "uBC_C_" + miUIDCount++;
                    oControl = new utils.Dynamic.Structs.Control();
                    oControl.oItemType = com.uBC.utils.Dynamic.SBOFFItem.getBoFormItemType(moIDH_FFIELDS.U_Type);
                    oControl.oItem = moSBOForm.Items.Add(sFieldName, oControl.oItemType);
                    oControl.oItem.Left = moCurrentPos.iLeft + 1 + com.uBC.utils.Dynamic.SBOFFItem.LableWidth;
                    oControl.oItem.Top = moCurrentPos.iTop;
                    oControl.oItem.Width = com.uBC.utils.Dynamic.SBOFFItem.ControlWidth;
                    oControl.oItem.Enabled = true;
                    oControl.oItem.Height = com.uBC.utils.Dynamic.SBOFFItem.Height;
                    oControl.oItem.LinkTo = moFrame.UniqueID;
                    oControl.oItem.Visible = false;
                    oControl.oItem.DisplayDesc = true;
                    oControl.sPopPar = moIDH_FFIELDS.U_PopPar;

                    //Create SBOFFItem
                    oSBOFFItem = new utils.Dynamic.SBOFFItem(oLable, oControl);
                    //Get Next Items Placement
                    doGetNextPos(oSBOFFItem);
                    //Add SBOFFItem to cache
                    oSBOItems.Add(oSBOFFItem);
                }
                miNextVirticalValue = moCurrentPos.iTop + 15;
                return oSBOItems;
            }
            return null;
        }

        public static SAPbouiCOM.BoDataType getBoDataTypeFromString(string sBoDataType) {
            if (sBoDataType.Equals(dt_LONG_NUMBER))
                return SAPbouiCOM.BoDataType.dt_LONG_NUMBER;
            else if (sBoDataType.Equals(dt_SHORT_NUMBER))
                return SAPbouiCOM.BoDataType.dt_SHORT_NUMBER;
            else if (sBoDataType.Equals(dt_QUANTITY))
                return SAPbouiCOM.BoDataType.dt_QUANTITY;
            else if (sBoDataType.Equals(dt_PRICE))
                return SAPbouiCOM.BoDataType.dt_PRICE;
            else if (sBoDataType.Equals(dt_RATE))
                return SAPbouiCOM.BoDataType.dt_RATE;
            else if (sBoDataType.Equals(dt_MEASURE))
                return SAPbouiCOM.BoDataType.dt_MEASURE;
            else if (sBoDataType.Equals(dt_SUM))
                return SAPbouiCOM.BoDataType.dt_SUM;
            else if (sBoDataType.Equals(dt_PERCENT))
                return SAPbouiCOM.BoDataType.dt_PERCENT;
            else if (sBoDataType.Equals(dt_LONG_TEXT))
                return SAPbouiCOM.BoDataType.dt_LONG_TEXT;
            else if (sBoDataType.Equals(dt_SHORT_TEXT))
                return SAPbouiCOM.BoDataType.dt_SHORT_TEXT;
            else if (sBoDataType.Equals(dt_DATE))
                return SAPbouiCOM.BoDataType.dt_DATE;
            else
                throw new NotImplementedException();
        }
        public static SAPbouiCOM.BoDataType getBoDataType(BoDataType oDataType) {
            switch (oDataType) {
                case BoDataType.dt_DATE:
                    return SAPbouiCOM.BoDataType.dt_DATE;
                case BoDataType.dt_LONG_NUMBER:
                    return SAPbouiCOM.BoDataType.dt_LONG_NUMBER;
                case BoDataType.dt_LONG_TEXT:
                    return SAPbouiCOM.BoDataType.dt_LONG_TEXT;
                case BoDataType.dt_MEASURE:
                    return SAPbouiCOM.BoDataType.dt_MEASURE;
                case BoDataType.dt_PERCENT:
                    return SAPbouiCOM.BoDataType.dt_PERCENT;
                case BoDataType.dt_PRICE:
                    return SAPbouiCOM.BoDataType.dt_PRICE;
                case BoDataType.dt_QUANTITY:
                    return SAPbouiCOM.BoDataType.dt_QUANTITY;
                case BoDataType.dt_RATE:
                    return SAPbouiCOM.BoDataType.dt_RATE;
                case BoDataType.dt_SHORT_NUMBER:
                    return SAPbouiCOM.BoDataType.dt_SHORT_NUMBER;
                case BoDataType.dt_SHORT_TEXT:
                    return SAPbouiCOM.BoDataType.dt_SHORT_TEXT;
                case BoDataType.dt_SUM:
                    return SAPbouiCOM.BoDataType.dt_SUM;
                default:
                    throw new NotImplementedException();
            }
        }
        public static string[] getBoDataTypesAsStringArray() {
            return new string[] { dt_DATE,
                                  dt_LONG_NUMBER,
                                  dt_LONG_TEXT,
                                  dt_MEASURE,
                                  dt_PERCENT,
                                  dt_PRICE,
                                  dt_QUANTITY,
                                  dt_RATE,
                                  dt_SHORT_NUMBER,
                                  dt_SHORT_TEXT, dt_SUM
            };
        }

        private void doPopulateSBOFFItem() {
            FFPopulation oPop = new FFPopulation(moItemCache);
        }

        public bool doClearSBOFFItems() {
            foreach (SBOFFItem oSBOFFItem in moItemCache) {
                oSBOFFItem.doClear();

            }
            return true;
        }
    }
}
