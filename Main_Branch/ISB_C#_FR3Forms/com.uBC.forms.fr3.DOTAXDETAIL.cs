using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.forms.oo;
using com.idh.bridge.data;
using com.idh.dbObjects.User;
using IDHAddOns.idh.controls;
using com.uBC.utils;
using com.idh.controls;
using com.idh.bridge.utils;
using com.uBC.forms.fr3;
using com.idh.dbObjects.strct;

namespace com.uBC.forms.fr3 {
    public class DOTAXDETAIL : com.uBC.forms.fr3.dbGrid.Base {

        private IDH_DISPROW msDISPROW;
        private string sDISPROWKey;
        
        public IDH_DOTXDT DO_Tax_Detail {
            get { return (IDH_DOTXDT )moDBObject; }
        }

        public IDH_DISPROW DISPROW
        {
            get { return (IDH_DISPROW)msDISPROW; }
            set { msDISPROW = value; }
        }

        public string DISPROWKey
        {
            get { return (string)sDISPROWKey; }
            set { sDISPROWKey = value;
            msDISPROW = new IDH_DISPROW();
            msDISPROW.getByKey(value);
            }
        }

        public DOTAXDETAIL(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
                mbIsSearch = true;
                SkipFormSettings = true;
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_DOTXDT", sParMenu, iMenuPos, "uBC_DOTAXDT.srf", false, true, false, "DO Charging Detail", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        protected override void doInitialize() {
            moDBObject = new IDH_DOTXDT();
        }

#region FormOpenCreateFunctions
        /** 
         * This Function will be called by the controller to allow the class to last minute Form Layout adjustments before it gets displayed
         * All changes made in hete will be cached if this is a cached form, the method will not be called again once the form has benn cached.
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            base.doCompleteCreate(ref BubbleEvent);
            AutoManaged = false; 
            Items.Item("1").AffectsFormMode = false;
        }

        public override void doBeforeLoadData() {
           // if (isModal())
                mbIsSearch = true;
               msDISPROW=(IDH_DISPROW) getParentSharedData("IDHDISPROW");
            if (mbIsSearch) {
                SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_Ok;
                Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
                SkipFormSettings = true;
            }  
            base.doBeforeLoadData();
           // moAdminGrid.doApplyExpand(IDHGrid.EXPANDED);
            moAdminGrid.setOrderValue(IDH_DOTXDT._RowNum);
        }

        /* 
         * Load the Form Data
         */
        public override void doLoadData() {
            //  base.doLoadData();
            Object oData = getParentSharedData("ADDITEMS");
            if (oData != null && oData is IDH_DOADDEXP) {
                DO_Tax_Detail.AdditionalExpenses = (IDH_DOADDEXP)oData;
                DO_Tax_Detail.doFillDataObject();
            } else {
                DO_Tax_Detail.doLoadDISPROW(msDISPROW);
                System.Collections.Hashtable hAddItems = (System.Collections.Hashtable)oData;
                DO_Tax_Detail.doFillDataObject(hAddItems);
            }
            moAdminGrid.doApplyRules();
            moAdminGrid.doEnabled(false);
        }

        /*
         * Set the Filter Fields
         */
        public override void doSetFilterFields() {
        }

        /*
         * Set the List Fields
         */
        public override void doSetListFields() {
            moAdminGrid.doAddListField(IDH_DOTXDT._RowNum, "Row No.", false, 20, null, null,-1,SAPbouiCOM.BoLinkedObject.lf_None,true,true);
            moAdminGrid.doAddListField(IDH_DOTXDT._ChargeName, "Item", false, 100, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_None, true, true);
            moAdminGrid.doAddListField(IDH_DOTXDT._BPCurr, "BP Currency", false, 30, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_None, true, true);
            moAdminGrid.doAddListField(IDH_DOTXDT._Amount, "Charged Amount", false, 30, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_None, true, true);
        }
#endregion

#region Events
        protected override void doSetHandlers() {
            base.doSetHandlers();
            //Handler_VALIDATE_CHANGED = new ev_Item_Event(FilterFieldChangeEvent);
            //Handler_COMBO_SELECT = new ev_Item_Event(FilterFieldChangeEvent);
        }

        protected override void doSetGridHandlers() {
            base.doSetGridHandlers();

         //   moAdminGrid.Handler_GRID_DOUBLE_CLICK = new IDHGrid.ev_GRID_EVENTS(doRowDoubleClickEvent);
        }
#endregion
#region ItemEventHandlers
        
 
        
#endregion
      
 
    }
}
