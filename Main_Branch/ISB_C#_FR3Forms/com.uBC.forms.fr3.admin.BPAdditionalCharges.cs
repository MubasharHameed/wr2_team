﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.dbObjects.User;
using IDHAddOns.idh.controls;
using com.uBC.utils;
using com.idh.controls;
using com.idh.bridge.utils;

namespace com.uBC.forms.fr3.admin {
    public class BPAdditionalCharges : com.uBC.forms.fr3.dbGrid.Base {
        private string msBPCode = null;
        private string msBPName = null;
        public string BPCode {
            set { msBPCode = value; }
            get { return msBPCode; }
        }
        public string BPName
        {
            set { msBPName = value; }
            get { return msBPName; }
        }

        public BPAdditionalCharges(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
                mbIsSearch = false;
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDHBPADCHG", sParMenu, iMenuPos, "admin.srf", false, true, false, "BP Additional Charges", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        protected override void doInitialize() {
            moDBObject = new IDH_BPADCHG();
        }

#region FormOpenCreateFunctions
        /*
         * Do Complete the Form
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            base.doCompleteCreate(ref BubbleEvent);
        }

       /* 
        * Do the final form steps to show before loaddata
        */
        public override void doBeforeLoadData() {
            base.doBeforeLoadData();

            if (msBPCode != null && msBPCode.Length > 0)
            {
                moDBObject.doAddDefaultValue(IDH_BPADCHG._BPCode, msBPCode);
            }

            if (msBPName != null && msBPName.Length > 0)
            {
                moDBObject.doAddDefaultValue(IDH_BPADCHG._BPName, msBPName);
            }
        }

        /* 
         * Load the Form Data
         */
        public override void doLoadData() {
            setUFValue("uBC_BPCD", msBPCode);
            setUFValue("uBC_BPNM", msBPName);

            base.doLoadData();
            doFillCombos();
        }

        /*
         * Set the Filter Fields
         */
        public override void doSetFilterFields() {
            moAdminGrid.doAddFilterField("uBC_BPCD", IDH_BPADCHG._BPCode, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 20);
        }

        /*
         * Set the List Fields
         */
        public override void doSetListFields() {

            //Set default selection for UOM 
            string sDefUOM = com.idh.bridge.lookups.Config.INSTANCE.getParameterWithDefault("BPACDUOM", com.idh.bridge.lookups.Config.INSTANCE.getDefaultUOM());
            //Dim sDefaultUOM As String = moDBObject.doGetDefaultValue(IDH_CSITPR._UOM)
            //If sDefaultUOM Is Nothing OrElse sDefaultUOM.Length = 0 Then
            //    sDefaultUOM = com.idh.bridge.lookups.Config.INSTANCE.getDefaultUOM()
            //End If

            moAdminGrid.doAddListField(IDH_BPADCHG._Code, "Code", false, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_BPADCHG._Name, "Name", true, 0, ListFields.LISTTYPE_IGNORE, null, -1);

            //"SRC*IDHCSRCH(BP)[IDH_BPCOD;IDH_TYPE=SC][CARDCODE;U_SuppNm=CARDNAME]"
            moAdminGrid.doAddListField(IDH_BPADCHG._BPCode, "BP Code", true, -1, "SRC*IDHCSRCH(BP)[IDH_BPCOD;IDH_TYPE=C][CARDCODE;U_BPName=CARDNAME]", null, -1);
            moAdminGrid.doAddListField(IDH_BPADCHG._BPName, "BP Name", false, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            //moAdminGrid.doAddListField(IDH_BPADCHG._SlpCode, "Sales Employee", true, -1, ListFields.LISTTYPE_COMBOBOX, null, -1);

            moAdminGrid.doAddListField(IDH_BPADCHG._EmpId, "Employee Id", true, -1, ListFields.LISTTYPE_COMBOBOX, null, -1);
            moAdminGrid.doAddListField(IDH_BPADCHG._EmpNm, "Employee Nm", false, 0, ListFields.LISTTYPE_IGNORE, null, -1);

            moAdminGrid.doAddListField(IDH_BPADCHG._ItemCode, "Item Code", true, -1, "SRC*IDHISRC(IT)[IDH_ITMCOD][ITEMCODE;U_ItemName=ITEMNAME]", null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            moAdminGrid.doAddListField(IDH_BPADCHG._ItemName, "Item Name", false, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_BPADCHG._UOM, "Sales UOM", true, -1, ListFields.LISTTYPE_COMBOBOX, null, -1);

            moAdminGrid.doAddListField(IDH_BPADCHG._StartDt, "Start Dt", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_BPADCHG._EndDt, "End Dt", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_BPADCHG._JobTp, "Job Type", true, -1, ListFields.LISTTYPE_COMBOBOX, null, -1);
            moAdminGrid.doAddListField(IDH_BPADCHG._Formula, "Formula", false, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_BPADCHG._FrmParse, "Formula Parse", false, 0, ListFields.LISTTYPE_IGNORE, null, -1);

            //moAdminGrid.doAddListField(IDH_BPADCHG._Formula, "Formula", true, -1, "SRC*IDHISRC(IT)[IDH_ITMCOD][ITEMCODE;U_ItemName=ITEMNAME]", null, -1);

            //moAdminGrid.doAddListField(IDH_BPADCHG._BPName, "BP Name", true, -1, ListFields.LISTTYPE_COMBOBOX, null, -1);
        }
#endregion

#region Events
        protected override void doSetHandlers() {
            base.doSetHandlers();
        }

        protected override void doSetGridHandlers() {
            base.doSetGridHandlers();

            moAdminGrid.Handler_GRID_FIELD_CHANGED = new IDHGrid.ev_GRID_EVENTS(doFieldChangeEvent);
            moAdminGrid.Handler_GRID_DOUBLE_CLICK = new IDHGrid.ev_GRID_EVENTS(doGridDoubleClick);
        }
#endregion 

#region ItemEventHandlers
        public bool doGridDoubleClick(ref IDHAddOns.idh.events.Base pVal)
        {
            if (pVal.BeforeAction)
            {
                string sFormula = (string)moAdminGrid.doGetFieldValue(IDH_BPADCHG._Formula, pVal.Row);
                string sFrmParse = (string)moAdminGrid.doGetFieldValue(IDH_BPADCHG._FrmParse, pVal.Row);
                setSharedData("IDH_FORMU", sFormula);
                setSharedData("IDH_FRMPRS", sFrmParse);
                doOpenModalForm("IDH_FORMEDT");
            }
            return true;
        }

        public bool doFieldChangeEvent(ref IDHAddOns.idh.events.Base pVal)
        {
            if (!pVal.BeforeAction)
            {
                if (pVal.ColUID == IDH_BPADCHG._EmpId)
                {
                    SAPbouiCOM.ComboBoxColumn oCombo;
                    oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(IDH_BPADCHG._EmpId);
                    string sEmpNm = oCombo.GetSelectedValue(pVal.Row).Description;
                    if (sEmpNm.Equals("-No Sales Employee-"))
                        sEmpNm = string.Empty; 
                    moAdminGrid.doSetFieldValue(IDH_BPADCHG._EmpNm, sEmpNm);
                }
            }
            return true;
        }

        private string setVal(string sVal)
        {
            if (sVal.Length == 0)
                return "0";
            else
                return sVal;
        }
#endregion

#region OldEventHandlers
/**
    * Most of these are replaced with Handlers as above... only use these if there is really no Event or in special events
    */

/*
    * Handle all the Menu Events.
    * Return True if the Event must be handled by the other Objects
    */
public override bool doMenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent)
{
    return true;
}

/*
    * The Event Handler that will receive all Activated Events for this Form
    * Return True if the Event must be handled by the other Objects
    */
public override bool doItemEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
{
    return true;
}

/*
    * Send By Custom Controls
    */
public override bool doCustomItemEvent(ref IDHAddOns.idh.events.Base pVal)
{
            //##S Start 26-01-2017
            if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED && !pVal.BeforeAction)
            {
                if (pVal.ColUID == "U_ItemCode")
                {
                    string ItemCode = moAdminGrid.doGetFieldValue(moAdminGrid.doIndexFieldWC("U_ItemCode"), pVal.Row).ToString();
                    if (ItemCode != "" && ItemCode != "*")
                    {
                        string sQry = null;
                        string unitName;
                        com.idh.bridge.DataRecords oRecords = null;
                        sQry = "select Isnull((select max(UnitDisply) from OWGT WITH (NOLOCK) where UnitDisply = (select SalUnitMsr from OITM WITH (NOLOCK) where ItemCode = '" + ItemCode + "' AND SalUnitMsr is not null)),(Select UnitDisply from OWGT WITH (NOLOCK) where UnitName = '" + com.idh.bridge.lookups.Config.INSTANCE.getParameterWithDefault("BPACDUOM", com.idh.bridge.lookups.Config.INSTANCE.getDefaultBPADCHGUOM()) + "')) as UnitDisplay";
                        //sQry = "select Isnull((select max(UnitDisply) from OWGT WITH (NOLOCK) where UnitDisply = (select SalUnitMsr from OITM WITH (NOLOCK) where ItemCode = '" + ItemCode + "' AND SalUnitMsr is not null)),(Select UnitDisply from OWGT WITH (NOLOCK) where UnitDisply = '" + com.idh.bridge.lookups.Config.INSTANCE.getParameterWithDefault("BPACDUOM", com.idh.bridge.lookups.Config.INSTANCE.getDefaultBPADCHGUOM()) + "')) as UnitDisplay";
                        oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("getBPAdditionalCharges", sQry);
                        oRecords.gotoFirst();
                        unitName = oRecords.getValueAsString("UnitDisplay");
                        SAPbouiCOM.ComboBoxColumn oCombo;
                        oCombo = (SAPbouiCOM.ComboBoxColumn)(moAdminGrid.Columns.Item(moAdminGrid.doIndexFieldWC("U_UOM")));
                        oCombo.SetSelectedValue(pVal.Row, oCombo.ValidValues.Item(unitName));
                        oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                    }
                }
            }
            //##S Start 26-01-2017

            //if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_ADD_EMPTY && !pVal.BeforeAction)
            //{
            //    if (moAdminGrid.doGetFieldValue(moAdminGrid.doIndexFieldWC("U_UOM"), pVal.Row).ToString() == "")
            //    {
            //        string sQry = null;
            //        string unitName;
            //        com.idh.bridge.DataRecords oRecords = null;
            //        sQry = "select UnitDisply as UnitDisplay from OWGT where UnitName = '" + com.idh.bridge.lookups.Config.INSTANCE.getParameterWithDefault("BPACDUOM", com.idh.bridge.lookups.Config.INSTANCE.getDefaultBPADCHGUOM()) + "'";
            //        oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("getBPAdditionalCharges", sQry);
            //        oRecords.gotoFirst();
            //        unitName = oRecords.getValueAsString("UnitDisplay");
            //        SAPbouiCOM.ComboBoxColumn oCombo;
            //        oCombo = (SAPbouiCOM.ComboBoxColumn)(moAdminGrid.Columns.Item(moAdminGrid.doIndexFieldWC("U_UOM")));
            //        oCombo.SetSelectedValue(pVal.Row, oCombo.ValidValues.Item(unitName));
            //        oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            //    }
            //}
    return true;
}

/*
    * Handle the Request for a Help File
    */
public override bool doHelpFile(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent)
{
    return true;
}

/*
    * Handles the right click event
    */
public override bool doRightClickEvent(ref SAPbouiCOM.ContextMenuInfo pVal, ref bool BubbleEvent)
{
    return true;
}

/*
    * Handle the Button One Pressed Event
    */
public override void doButtonID1(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
{
}

/* 
    * Handle Button Two Presssed Event
    */
public override void doButtonID2(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
{
}

/* 
    * Return As Canceled If this was Opened as a Dialog from another form
    */
public override void doReturnCanceled(string sModalFormType)
{
}

///* 
// * Return as Ok If this was Opened as a Dialog from another form setting the Data in the Global Shared Buffer
// * This will be called from the Conroller after all the other Button 1 Handlers and the BubbleEvent is still True.
// */
//public override void doReturnFromModalShared(bool bState) {
//}

//*** Return As Normal
public override void doReturnNormal() {
}

/* 
    * Handle the Cancel Event when a Dialog returns to this form.
    */
public override void doHandleModalCanceled(string sModalFormType)
{
}

/* 
    * Handle the Return from a Dialog called from this Form with the data in oData
    */
public override void doHandleModalBufferedResult(ref object oData, string sModalFormType, string sLastButton)
{
}

/* 
    * Handle the Return from a Dialog called from this Form with the data in the Global Shared Buffer
    */
public override void doHandleModalResultShared(string sModalFormType, string sLastButton)
{
    if (sModalFormType == "IDH_FORMEDT")
    {
        string sFormula = (string)getSharedData("FORMULA");
        string sFrmParse = (string)getSharedData("FRMPARS");
        moAdminGrid.doSetFieldValue(IDH_BPADCHG._Formula, sFormula);
        moAdminGrid.doSetFieldValue(IDH_BPADCHG._FrmParse, sFrmParse);
        Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
    }
}
#endregion


#region fillCombos
        private void doFillCombos(){
            SAPbouiCOM.ComboBoxColumn oCombo;
            
            //IDH_BPADCHG oAC = new IDH_BPADCHG();
            //oAC.U_UOM = ""; 
            //oAC.U_SlpCode = 1;
            //oAC.U_JobTp = 1; 

            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_BPADCHG._JobTp));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            FillCombos.WRIJobTypesCombo(oCombo, null, "", "N/A");

            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_BPADCHG._UOM));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            FillCombos.UOMCombo(oCombo, "", "N/A");

            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_BPADCHG._EmpId));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            FillCombos.EmployeesCombo(oCombo, "", "-No Sales Employee-", true);


        }
#endregion
    }
}
