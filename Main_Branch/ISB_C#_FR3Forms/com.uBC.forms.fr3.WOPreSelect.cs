﻿using System.IO;
using System.Collections;
using System;

using IDHAddOns.idh.controls;
using IDHAddOns;
using com.idh.controls;
using com.idh.bridge.data;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.idh.dbObjects.User;
using com.uBC.utils;
using com.uBC.data;
using SAPbouiCOM;

namespace com.uBC.forms.fr3 {
    public class WOPreSelect : com.idh.forms.oo.Form {
        private int miGridWidth = 400;
        private int miWidth = -1;
        private int miLastPrevWidth = -1;
        private int miPreviewLimit = 0;
        private PreSelect oPreData = new PreSelect();

        private FilterGrid moGrid;

        public WOPreSelect(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
            doSetHandlers();
        }
        
        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_WOPRE", sParMenu, 80, "WOPreSel.srf", false, true, false, "Waste Order - Pre Selector", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

#region FormOpenCreateFunctions
        /** 
         * This Function will be called by the controller to allow the class to last minute Form Layout adjustments before it gets displayed
         * All changes made in hete will be cached if this is a cached form, the method will not be called again once the form has benn cached.
         */
        public const string FIELD_WASTECODE = "uBCWasteCD";
        public const string FIELD_WASTENAME = "uBCWasteNM";
        public const string FIELD_WASTECF = "uBCWasteCF";

        public const string FIELD_BPCODE = "uBCBPCD";
        public const string FIELD_BPNAME = "uBCBPNM";
        public const string FIELD_BPCF = "uBCBPCF";

        public const string FIELD_VEHREG = "uBCVehReg";
        public const string FIELD_VEHREGCF = "uBCVehRgCF";

        public const string FIELD_WR1TYPE = "uBCWR1O";

        public const string FIELD_SHOWPREVIOUS = "uBCShwPrev";

        public const string FIELD_TRANSACTIONCODE = "uBCTranCD";

        public const string FIELD_CLEAR = "uBCClear";

        public override void doCompleteCreate(ref bool BubbleEvent) {
            doAddUF(FIELD_WASTECODE, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, false, false);
            doAddUF(FIELD_WASTENAME, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 200, false, false);

            doAddUF(FIELD_BPCODE, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, false, false);
            doAddUF(FIELD_BPNAME, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 200, false, false);
            doAddUF(FIELD_WR1TYPE, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, false, false);

            doAddUF(FIELD_VEHREG, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, false, false);

            doAddUF(FIELD_TRANSACTIONCODE, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, false, false);

            doAddUFCheck(FIELD_SHOWPREVIOUS, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, false, false, "Y", "N");

            SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_Ok; 
            AutoManaged = false;
            Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;

            FillCombos.TransactionCodeCombo(FillCombos.getCombo(SBOForm, FIELD_TRANSACTIONCODE), null, null,null,"","NONE");
            FillCombos.WR1OrderTypeCombo(FillCombos.getCombo(SBOForm,FIELD_WR1TYPE), "", "Please Select");
        }

        /** 
         * Do the final form steps to show before loaddata
         */
        public override void doBeforeLoadData() {
        }

        /** 
         * Load the Form Data
         */
        public override void doLoadData() {
        }

        /** 
         * Do the final form steps to show after loaddata
         */
        public override void doFinalizeShow() {
            base.doFinalizeShow();
        }
#endregion

#region Events
        private void doSetHandlers() {
            Handler_Button_Ok = new ev_Item_Event(doButtonOKEvent);

            addHandler_ITEM_PRESSED(FIELD_WASTECF, new ev_Item_Event(doWasteCodeLookup));
            addHandler_ITEM_PRESSED(FIELD_BPCF, new ev_Item_Event(doBPLookup));
            addHandler_ITEM_PRESSED(FIELD_SHOWPREVIOUS, new ev_Item_Event(doPreviousClick));
            addHandler_ITEM_PRESSED(FIELD_VEHREGCF, new ev_Item_Event(doLorryLookup));
            addHandler_ITEM_PRESSED(FIELD_CLEAR, new ev_Item_Event(doClear));

            //MA Start 26-02-2015 Issue#603
            //addHandler_VALIDATE_CHANGED(FIELD_WASTECODE, new ev_Item_Event(doWasteCodeChangedEvent));
            //addHandler_VALIDATE_CHANGED(FIELD_WASTENAME, new ev_Item_Event(doWasteNameChangedEvent));
            addHandler_VALIDATE(FIELD_WASTECODE, new ev_Item_Event(doWasteCodeValidateEvent));
            addHandler_VALIDATE(FIELD_WASTENAME, new ev_Item_Event(doWasteNameValidateEvent));
            //addHandler_VALIDATE_CHANGED(FIELD_BPCODE, new ev_Item_Event(doBPCodeChangedEvent));
            //addHandler_VALIDATE_CHANGED(FIELD_BPNAME, new ev_Item_Event(doBPNameChangedEvent));
            addHandler_VALIDATE(FIELD_BPCODE, new ev_Item_Event(doBPCodeValidateEvent));
            addHandler_VALIDATE(FIELD_BPNAME, new ev_Item_Event(doBPNameValidateEvent));
            //MA End 26-02-2015 Issue#603

            addHandler_VALIDATE_CHANGED(FIELD_VEHREG, new ev_Item_Event(doLorryChangedEvent));

            addHandler_COMBO_SELECT(FIELD_TRANSACTIONCODE, new ev_Item_Event(doTransCodeSelected));
            addHandler_COMBO_SELECT(FIELD_WR1TYPE, new ev_Item_Event(doWR1TypeSelected));
        }
#endregion 

#region ItemEventHandlers
        public bool doButtonOKEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                BubbleEvent = false;
                doOpenWO();
            }
            return true;
        }

        public bool doClear(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                setUFValue(FIELD_WASTECODE, "");
                setUFValue(FIELD_WASTENAME, "");
                setUFValue(FIELD_BPCODE, "");
                setUFValue(FIELD_BPNAME, "");
                setUFValue(FIELD_WR1TYPE, "");
                setUFValue(FIELD_VEHREG, "");
                setUFValue(FIELD_TRANSACTIONCODE, "");
            }
            return true;
        }
        
        public bool doWasteCodeLookup(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                doChooseWasteCode(false,pVal.ItemUID);
            }
            return true;
        }

        public bool doBPLookup(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                doChooseBPCode(false, pVal.ItemUID);
            }
            return true;
        }

        public bool doLorryLookup(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                doChooseLorry(false);
            }
            return true;
        }

        public bool doWasteCodeChangedEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                setUFValue(FIELD_WASTENAME, "");
                if (getUFValueAsString(FIELD_WASTECODE).Length > 0) {
                    doChooseWasteCode(true, pVal.ItemUID);
                } else if (((string)getUFValue(FIELD_SHOWPREVIOUS)) == "Y")
                    doLoadPrevious();
            }
            return true;
        }

        public bool doWasteNameChangedEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                setUFValue(FIELD_WASTECODE, "");
                if (getUFValueAsString(FIELD_WASTENAME).Length > 0) {
                    doChooseWasteCode(true, pVal.ItemUID);
                } else if (((string)getUFValue(FIELD_SHOWPREVIOUS)) == "Y")
                    doLoadPrevious();
            }
            return true;
        }
        //MA Start 27-02-2015 Issue#603
        public bool doWasteCodeValidateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            if (!pVal.BeforeAction)
            {
                if (getUFValueAsString(FIELD_WASTECODE).IndexOf("*") > -1 || (pVal.ItemChanged && wasSetWithCode(pVal.ItemUID) == false && getUFValueAsString(FIELD_WASTECODE).Length > 0))
                {
                    doChooseWasteCode(true, pVal.ItemUID);
                }
                else
                {
                    if (getUFValueAsString(FIELD_WASTECODE).Length > 0)
                    {
                        return true;
                    }
                    else
                    {
                        setUFValue(FIELD_WASTENAME, "");
                    }
                    if (((string)getUFValue(FIELD_SHOWPREVIOUS)) == "Y")
                        doLoadPrevious();
                }
               
            }
            return true;
        }

        public bool doWasteNameValidateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            if (!pVal.BeforeAction)
            {
                if (getUFValueAsString(FIELD_WASTENAME).IndexOf("*") > -1 || (pVal.ItemChanged && wasSetWithCode(pVal.ItemUID) == false && getUFValueAsString(FIELD_WASTENAME).Length > 0))
                {
                    doChooseWasteCode(true, pVal.ItemUID);
                }
                else
                {
                    if (getUFValueAsString(FIELD_WASTENAME).Length > 0)
                    {
                        return true;
                    }
                    else
                    {
                        setUFValue(FIELD_WASTECODE, "");
                    }
                    if (((string)getUFValue(FIELD_SHOWPREVIOUS)) == "Y")
                        doLoadPrevious();
                }
            }
            return true;
        }
        //MA End 27-02-2015 Issue#603
        public bool doBPCodeChangedEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                setUFValue(FIELD_BPNAME, "");
                if (getUFValueAsString(FIELD_BPCODE).Length > 0) {
                    doChooseBPCode(true, pVal.ItemUID);
                } else {
                    FillCombos.TransactionCodeCombo(FillCombos.getCombo(SBOForm, FIELD_TRANSACTIONCODE), null, null, null, "", "NONE");
                    if (((string)getUFValue(FIELD_SHOWPREVIOUS)) == "Y")
                        doLoadPrevious();
                }
            }
            return true;
        }

        public bool doBPNameChangedEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                setUFValue(FIELD_BPCODE, "");
                if (getUFValueAsString(FIELD_BPNAME).Length > 0) {
                    doChooseBPCode(true, pVal.ItemUID);
                } else if (((string)getUFValue(FIELD_SHOWPREVIOUS)) == "Y")
                    doLoadPrevious();
            }
            return true;
        }
        //MA Start 26-02-2015 Issue#603
        public bool doBPCodeValidateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            if (!pVal.BeforeAction )
            {
                //setUFValue(FIELD_BPNAME, "");
                if (getUFValueAsString(FIELD_BPCODE).IndexOf("*") > -1 || (pVal.ItemChanged && wasSetWithCode(pVal.ItemUID) == false && getUFValueAsString(FIELD_BPCODE).Length > 0))
                {
                    doChooseBPCode(true, pVal.ItemUID);
                }
                else
                {
                    if (getUFValueAsString(FIELD_BPCODE).Length >0)// || getUFValueAsString(FIELD_BPNAME).Length == 0)
                    {
                        return true;
                    }
                    else
                    {
                        setUFValue(FIELD_BPNAME, "");
                    }
                    FillCombos.TransactionCodeCombo(FillCombos.getCombo(SBOForm, FIELD_TRANSACTIONCODE), null, null, null, "", "NONE");
                    if (((string)getUFValue(FIELD_SHOWPREVIOUS)) == "Y")
                        doLoadPrevious();
                }
            }
            return true;
        }

        public bool doBPNameValidateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            if (!pVal.BeforeAction)
            {
                
                //setUFValue(FIELD_BPCODE, "");
                if (getUFValueAsString(FIELD_BPNAME).IndexOf("*") > -1 || (pVal.ItemChanged && wasSetWithCode(pVal.ItemUID)==false  && getUFValueAsString(FIELD_BPNAME).Length > 0))
                {
                    doChooseBPCode(true, pVal.ItemUID);
                }
                else
                {
                    if (getUFValueAsString(FIELD_BPNAME).Length > 0)// || getUFValueAsString(FIELD_BPNAME).Length == 0)
                    {
                        return true;
                    }
                    else
                    {
                        setUFValue(FIELD_BPCODE, "");
                    }
                    if (((string)getUFValue(FIELD_SHOWPREVIOUS)) == "Y")
                        doLoadPrevious();
                }
            }
            return true;
        }
        //MA End 26-02-2015 Issue#603
        /// <summary>
        /// Handle Lorry search event
        /// </summary>
        /// <param name="pVal"></param>
        /// <param name="BubbleEvent"></param>
        /// <returns></returns>
        public bool doLorryChangedEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                if (getUFValueAsString(FIELD_VEHREG).Length > 0) {
                    doChooseLorry(true);
                } else if (((string)getUFValue(FIELD_SHOWPREVIOUS)) == "Y")
                    doLoadPrevious();
            }
            return true;
        }

        public bool doTransCodeSelected(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                if (getUFValueAsString(FIELD_SHOWPREVIOUS) == "Y")
                    doLoadPrevious();
            }
            return true;
        }

        public bool doWR1TypeSelected(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                if (((string)getUFValue(FIELD_SHOWPREVIOUS)) == "Y")
                    doLoadPrevious();

                string sCardCode = (string)getUFValue(FIELD_BPCODE);
                string sWR1OrderType = (string)getUFValue(FIELD_WR1TYPE);

                if (sWR1OrderType == "DO") {
                    Items.Item(FIELD_VEHREG).Enabled = true;
                    Items.Item(FIELD_VEHREGCF).Enabled = true;
                } else {
                    setUFValue(FIELD_VEHREG, "");
                    Items.Item(FIELD_VEHREG).Enabled = false;
                    Items.Item(FIELD_VEHREGCF).Enabled = false;
                }
                FillCombos.TransactionCodeCombo(FillCombos.getCombo(SBOForm, FIELD_TRANSACTIONCODE), sCardCode, null, sWR1OrderType, "", "NONE");
            }
            return true;
        }

        public bool doPreviousClick(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                if (((string)getUFValue(FIELD_SHOWPREVIOUS)) == "Y") {
                    
                    if (miWidth < 0)
                        miWidth = Width;

                    Width = (miLastPrevWidth>0)?miLastPrevWidth:Width + miGridWidth;
                    if (moGrid != null) {
                        moGrid.getSBOItem().Visible = true;
                        moGrid.getSBOItem().Width = miGridWidth;
                    }

                    doLoadPrevious();
                } else {
                    if (moGrid != null) {
                        moGrid.getSBOItem().Visible = false;
                        miGridWidth = moGrid.getSBOItem().Width;
                    }

                    miLastPrevWidth = Width;
                    
                    Width = miWidth;
                }
            }
            return true;
        }

        public bool doRowDoubleClickEvent(ref IDHAddOns.idh.events.Base pVal) {
            if (pVal.BeforeAction) {
                string sWOCode = (string)moGrid.doGetFieldValue(IDH_JOBSHD._JobNr, pVal.Row);
                string sWORCode = (string)moGrid.doGetFieldValue(IDH_JOBSHD._Code, pVal.Row);

                doOpenWO(sWOCode, sWORCode);
            }
            return true;
        }

        public bool doLorrySelection(com.idh.forms.oo.Form oOForm) {
            VehicleM oOOForm = (VehicleM)oOForm;
            IDH_VEHMAS2 oVehMast = oOOForm.VehicleMaster;

            setUFValue(FIELD_VEHREG, oVehMast.U_VehReg);
            return true;
        }

        public bool doLorryCancel(com.idh.forms.oo.Form oOForm) {
            //VehicleM oOOForm = (VehicleM)oOForm;
            //IDH_VEHMAS2 oVehMast = oOOForm.VehicleMaster;

            //setUFValue(FIELD_VEHREG, oVehMast.U_VehReg);
            return true;
        }
#endregion 

#region OtherLogic
        protected virtual void doChooseLorry(bool bFilter) {
            //VehicleM oOOForm = new VehicleM(null, UniqueID, null);
            //oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doLorrySelection);
            //oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doLorryCancel);
            //oOOForm.FilterVehReg = getUFValueAsString(FIELD_VEHREG);
            //oOOForm.doShowModal();
            //SV 24/03/2015
            
            
            setSharedData("SHOWCREATE", "TRUE");
            setSharedData("IDH_VEHREG", getUFValue("uBCVehReg"));
            if (!string.IsNullOrEmpty((string)getUFValue("uBCVehReg"))) {
                setSharedData("SILENT", "TRUE");
            }
            doOpenModalForm("IDHLOSCH");
            //End
        }

        private void doOpenWO() {
            doOpenWO(null, null);
        }
        private void doOpenWO(string sWOCode, string sWORCode) {
            string sTranCode = getUFValueAsString(FIELD_TRANSACTIONCODE);
            string sWR1 = getUFValueAsString(FIELD_WR1TYPE);
            
            //PreSelect oPreData = new PreSelect();
            oPreData.WasteCode = getUFValueAsString(FIELD_WASTECODE);
            oPreData.WasteDescription = getUFValueAsString(FIELD_WASTENAME);

            oPreData.BPCode = getUFValueAsString(FIELD_BPCODE);
            oPreData.BPName = getUFValueAsString(FIELD_BPNAME);

            oPreData.VehReg = getUFValueAsString(FIELD_VEHREG);

            oPreData.TransactionCode = getUFValueAsString(FIELD_TRANSACTIONCODE);

            if (sWR1 == "WO") {
                //if (sWOCode != null && sWORCode != null) {
                //setSharedData("ACTION", "Duplicate");
                setSharedData("USESHARE", true);
                //setSharedData("WOCode", sWOCode==null?"":sWOCode);
                //setSharedData("RowCode", sWOCode == null ? "" : sWOCode);
                setSharedData("WOCode", sWOCode == null ? "" : sWOCode);
                //setSharedData("NewJobType", sNewJobType);
                setSharedData("PRESELECT", oPreData);
                //}
                doOpenForm("IDH_WASTORD");
            } else if (sWR1 == "DO") {
                if (sWOCode != null && sWOCode.Length > 0) {
                    ArrayList oData = new ArrayList();
                    oData.Add("DoUpdate");
                    oData.Add(sWOCode);
                    setSharedData("ROWCODE", sWORCode);
                    setSharedData("PRESELECT", oPreData);
                    doOpenForm("IDH_DISPORD", oData);
                } else {
                    ArrayList oData = new ArrayList();
                    oData.Add("DoAdd");
                    oData.Add("");
                    setSharedData("USESHARE", true);
                    setSharedData("WOCode", sWOCode == null ? "" : sWOCode);
                    setSharedData("PRESELECT", oPreData);
                    setSharedData("ROWCODE", sWORCode);
                    doOpenForm("IDH_DISPORD", oData);
                }
            }
        }

        //public void doLoadPrevious() {
        //    string sWR1 = (string)getUFValue(FIELD_WR1TYPE);

        //    if ( moGrid == null ) {
        //        moGrid = new FilterGrid(IDHForm, SBOForm, "LINESGRID", 380, 5, miGridWidth, ClientHeight - 10, false);
        //        moGrid.doAddListField("r." + IDH_JOBSHD._JobNr, "Header", false, -1, ListFields.LISTTYPE_IGNORE, null);
        //        moGrid.doAddListField("r." + IDH_JOBSHD._Code, "Row", false, -1, ListFields.LISTTYPE_IGNORE, null);
        //        moGrid.doAddListField("r." + IDH_JOBSHD._CustCd, "Customer", false, -1, ListFields.LISTTYPE_IGNORE, null);
        //        moGrid.doAddListField("r." + IDH_JOBSHD._WasCd, "Waste Code", false, -1, ListFields.LISTTYPE_IGNORE, null);
        //        moGrid.doAddListField("r." + IDH_JOBSHD._WasDsc, "Waste Description", false, -1, ListFields.LISTTYPE_IGNORE, null);
        //        moGrid.doAddListField("r." + IDH_JOBSHD._Lorry, "Registration", false, -1, ListFields.LISTTYPE_IGNORE, null);
        //        moGrid.doAddListField("r." + IDH_JOBSHD._CarrCd, "Haulier", false, -1, ListFields.LISTTYPE_IGNORE, null);
        //        moGrid.doAddListField("r." + IDH_JOBSHD._RDate, "Requested Date", false, -1, ListFields.LISTTYPE_IGNORE, null);
        //        moGrid.getSBOGrid().SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single;
        //    }

        //    string sReqString = IDH_JOBSHD._AEDate + " Is Not NULL ";
        //    if (sWR1 == "DO") {
        //        moGrid.doRemoveGridTable(IDH_JOBSHD.TableName, "r");
        //        moGrid.doAddGridTable(new GridTable(IDH_DISPROW.TableName, "r", "Code", false, true), true);
        //    } else {
        //        if ( sWR1 != "WO") {
        //            sReqString = IDH_JOBSHD._Code + " = ' '";
        //        }
        //        moGrid.doRemoveGridTable(IDH_DISPROW.TableName, "r");
        //        moGrid.doAddGridTable(new GridTable(IDH_JOBSHD.TableName, "r", "Code", false, true), true);
        //    }

        //    int iPreviewLimit = Config.ParameterAsInt("PRELIMIT");
        //    if (iPreviewLimit != miPreviewLimit) {
        //        miPreviewLimit = iPreviewLimit;
        //        ((SAPbouiCOM.CheckBox)Items.Item("uBCShwPrev").Specific).Caption = "Preview Last " + iPreviewLimit + " Jobs";
        //    }
        //    moGrid.setSoftLimit(iPreviewLimit);

        //    sReqString += (sReqString.Length > 0 ? " AND " : "") + "r." + IDH_JOBSHD._RowSta + " != '" + FixedValues.getStatusDeleted() + "'";
        //    string sCardCode = (string)getUFValue(FIELD_BPCODE);
        //    if (sCardCode.Length > 0) {
        //        sReqString += (sReqString.Length > 0 ? " AND " : "") + "(r." + IDH_JOBSHD._CustCd + " = '" + sCardCode + "' OR r." + IDH_JOBSHD._ProCd + " = '" + sCardCode + "')";
        //    }

        //    string sWasteCode = (string)getUFValue(FIELD_WASTECODE);
        //    if (sWasteCode.Length > 0) {
        //        sReqString += (sReqString.Length > 0 ? " AND " : "") + "r." + IDH_JOBSHD._WasCd + " = '" + sWasteCode + "'";
        //    }

        //    string sVehReg = (string)getUFValue(FIELD_VEHREG);
        //    if (sVehReg.Length > 0) {
        //        sReqString += (sReqString.Length > 0 ? " AND " : "") + "r." + IDH_JOBSHD._Lorry + " = '" + sVehReg + "'";
        //    }

        //    string sTransCode = (string)getUFValue(FIELD_TRANSACTIONCODE);
        //    if (sTransCode.Length > 0) {
        //        //moGrid.doAddListField(IDH_TRANCD._Process, "Transaction Code", false, -1, ListFields.LISTTYPE_IGNORE, null);
        //        //moGrid.doAddGridTable(new GridTable(IDH_TRANCD.TableName, "t", "Code", false, true), false);
        //        //sReqString += (sReqString.Length > 0 ? " AND " : "") + "r." + IDH_JOBSHD._TrnCode + " = '" + sTransCode + "' AND t." + IDH_TRANCD._Code + " = r." + IDH_JOBSHD._TrnCode;
        //        sReqString += (sReqString.Length > 0 ? " AND " : "") + "r." + IDH_JOBSHD._TrnCode + " = '" + sTransCode + "'";
        //    }

        //    if (sReqString.Length > 0)
        //        moGrid.setRequiredFilter(sReqString);

        //    //moGrid.setOrderValue("CAST(" + IDH_JOBSHD._Code + " AS Numeric)");
        //    moGrid.setOrderValue(IDH_JOBSHD._RDate);

        //    ((IDHGrid)moGrid).doReloadData(GridControl.SORTDESC,false,true);

        //    moGrid.Handler_GRID_DOUBLE_CLICK = new IDHGrid.ev_GRID_EVENTS(doRowDoubleClickEvent);
        //}

        public void doLoadPrevious() {
            if (((string)getUFValue(FIELD_SHOWPREVIOUS)) != "Y")
                return;

            string sWR1 = (string)getUFValue(FIELD_WR1TYPE);

            if (moGrid == null) {
                moGrid = new FilterGrid(IDHForm, SBOForm, "LINESGRID", 380, 5, miGridWidth, ClientHeight - 10, false);
                moGrid.doAddListField( IDH_JOBSHD._JobNr, "Header", false, -1, ListFields.LISTTYPE_IGNORE, null);
                moGrid.doAddListField( IDH_JOBSHD._Code, "Row", false, -1, ListFields.LISTTYPE_IGNORE, null);
                moGrid.doAddListField( IDH_JOBSHD._CustCd, "Customer", false, -1, ListFields.LISTTYPE_IGNORE, null);
                moGrid.doAddListField( IDH_JOBSHD._WasCd, "Waste Code", false, -1, ListFields.LISTTYPE_IGNORE, null);
                moGrid.doAddListField( IDH_JOBSHD._WasDsc, "Waste Description", false, -1, ListFields.LISTTYPE_IGNORE, null);
                moGrid.doAddListField( IDH_JOBSHD._Lorry, "Registration", false, -1, ListFields.LISTTYPE_IGNORE, null);
                moGrid.doAddListField( IDH_JOBSHD._CarrCd, "Haulier", false, -1, ListFields.LISTTYPE_IGNORE, null);
                moGrid.doAddListField( IDH_JOBSHD._RDate, "Requested Date", false, -1, ListFields.LISTTYPE_IGNORE, null);
                moGrid.getSBOGrid().SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single;
            }

            string sReqString = IDH_JOBSHD._AEDate + " Is Not NULL ";
            if (sWR1 == "DO") {
                moGrid.doRemoveGridSource(IDH_JOBSHD.TableName, "");
                moGrid.doAddGridTable(new GridTable(IDH_DISPROW.TableName, "", "Code", false, true), true);
            } else {
                if (sWR1 != "WO") {
                    sReqString = IDH_JOBSHD._Code + " = ' '";
                }
                moGrid.doRemoveGridSource(IDH_DISPROW.TableName, "");
                moGrid.doAddGridTable(new GridTable(IDH_JOBSHD.TableName, "", "Code", false, true), true);
            }

            int iPreviewLimit = Config.ParameterAsInt("PRELIMIT");
            if (iPreviewLimit != miPreviewLimit) {
                miPreviewLimit = iPreviewLimit;
                ((SAPbouiCOM.CheckBox)Items.Item("uBCShwPrev").Specific).Caption = "Preview Last " + iPreviewLimit + " Jobs";
            }
            moGrid.setSoftLimit(iPreviewLimit);

            sReqString += (sReqString.Length > 0 ? " AND " : "") + IDH_JOBSHD._RowSta + " != '" + FixedValues.getStatusDeleted() + "'";
            string sCardCode = (string)getUFValue(FIELD_BPCODE);
            if (sCardCode.Length > 0) {
                sReqString += (sReqString.Length > 0 ? " AND " : "") + "(" + IDH_JOBSHD._CustCd + " = '" + sCardCode + "' OR " + IDH_JOBSHD._ProCd + " = '" + sCardCode + "')";
            }

            string sWasteCode = (string)getUFValue(FIELD_WASTECODE);
            if (sWasteCode.Length > 0) {
                sReqString += (sReqString.Length > 0 ? " AND " : "") + IDH_JOBSHD._WasCd + " = '" + sWasteCode + "'";
            }

            string sVehReg = (string)getUFValue(FIELD_VEHREG);
            if (sVehReg.Length > 0) {
                sReqString += (sReqString.Length > 0 ? " AND " : "") + IDH_JOBSHD._Lorry + " = '" + sVehReg + "'";
            }

            string sTransCode = (string)getUFValue(FIELD_TRANSACTIONCODE);
            if (sTransCode.Length > 0) {
                //moGrid.doAddListField(IDH_TRANCD._Process, "Transaction Code", false, -1, ListFields.LISTTYPE_IGNORE, null);
                //moGrid.doAddGridTable(new GridTable(IDH_TRANCD.TableName, "t", "Code", false, true), false);
                //sReqString += (sReqString.Length > 0 ? " AND " : "") + "r." + IDH_JOBSHD._TrnCode + " = '" + sTransCode + "' AND t." + IDH_TRANCD._Code + " = r." + IDH_JOBSHD._TrnCode;
                sReqString += (sReqString.Length > 0 ? " AND " : "") + IDH_JOBSHD._TrnCode + " = '" + sTransCode + "'";
            }

            if (sReqString.Length > 0)
                moGrid.setRequiredFilter(sReqString);

            //moGrid.setOrderValue("CAST(" + IDH_JOBSHD._Code + " AS Numeric)");
            if (sWR1 == "DO")
                moGrid.setOrderValue(IDH_JOBSHD._AEDate);
            else
                moGrid.setOrderValue(IDH_JOBSHD._RDate);

            ((IDHGrid)moGrid).doReloadData(DBOGridControl.SORTDESC, false, true);

            moGrid.Handler_GRID_DOUBLE_CLICK = new IDHGrid.ev_GRID_EVENTS(doRowDoubleClickEvent);
        }

#endregion

#region OldEventHandlers
        /**
         * Most of these are replaced with Handlers as above... only use these if there is really no Event or in special events
         */

        /*
         * Handle all the Menu Events.
         * Return True if the Event must be handled by the other Objects
         */
        public override bool doMenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * The Event Handler that will receive all Activated Events for this Form
         * Return True if the Event must be handled by the other Objects
         */
        public override bool doItemEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Send By Custom Controls
         */
        public override bool doCustomItemEvent(ref IDHAddOns.idh.events.Base pVal) {
            return true;
        }

        /*
         * Handle the Request for a Help File
         */
        public override bool doHelpFile(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Handles the right click event
         */
        public override bool doRightClickEvent(ref SAPbouiCOM.ContextMenuInfo pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Handle the Button One Pressed Event
         */
        public override void doButtonID1(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        }

        /* 
         * Handle Button Two Presssed Event
         */
        public override void doButtonID2(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        }

        /* 
         * Return As Canceled If this was Opened as a Dialog from another form
         */
        public override void doReturnCanceled(string sModalFormType) {
        }

        ///* 
        // * Return as Ok If this was Opened as a Dialog from another form setting the Data in the Global Shared Buffer
        // * This will be called from the Conroller after all the other Button 1 Handlers and the BubbleEvent is still True.
        // */
        //public override void doReturnFromModalShared(bool bState) {
        //}

        //*** Return As Normal
        public override void doReturnNormal() {
        }

        /* 
         * Handle the Cancel Event when a Dialog returns to this form.
         */
        public override void doHandleModalCanceled(string sModalFormType) {
            if (getSharedData("CALLEDITEM") != null && getSharedData("CALLEDITEM").ToString() != "")
            {
                setFocus(getSharedData("CALLEDITEM").ToString());
            } 
        }

        /* 
         * Handle the Return from a Dialog called from this Form with the data in oData
         */
        public override void doHandleModalBufferedResult(ref object oData, string sModalFormType, string sLastButton) {
        }

        /* 
         * Handle the Return from a Dialog called from this Form with the data in the Global Shared Buffer
         */
        public override void doHandleModalResultShared(string sModalFormType, string sLastButton) {
            if (sModalFormType == "IDHCSRCH") {
                string sTarget = (string)getSharedData("TRG");
                string sCardCode = (string)getSharedData("CARDCODE");
                string sCardName = (string)getSharedData("CARDNAME");
                
                setUFValue(FIELD_BPCODE, sCardCode);
                setUFValue(FIELD_BPNAME, sCardName);

                string sWR1OrderType = (string)getUFValue(FIELD_WR1TYPE);
                FillCombos.TransactionCodeCombo(FillCombos.getCombo(SBOForm, FIELD_TRANSACTIONCODE),sCardCode, null, sWR1OrderType, "", "NONE");

                doLoadPrevious();
                if (getSharedData("CALLEDITEM") != null && getSharedData("CALLEDITEM").ToString() != "")
                {
                    setFocus(getSharedData("CALLEDITEM").ToString());
                } 
            } else if (sModalFormType == "IDHWISRC") {
                string sTarget = (string)getSharedData("TRG");
                string sWasteCode = (string)getSharedData("ITEMCODE");
                string sWasteName = (string)getSharedData("ITEMNAME");

                setUFValue(FIELD_WASTECODE, sWasteCode);
                setUFValue(FIELD_WASTENAME, sWasteName);

                doLoadPrevious();
                if (getSharedData("CALLEDITEM") != null && getSharedData("CALLEDITEM").ToString() != "")
                {
                    setFocus(getSharedData("CALLEDITEM").ToString());
                } 
            }
            else if (sModalFormType == "IDHLOSCH") {
                string sVehReg = string.Empty;
                sVehReg = (string)getSharedData("REG");
                //oPreData.VehReg = sVehReg;

                string sCarrier = string.Empty;
                sCarrier = (string)getSharedData("WCCD");
                if (string.IsNullOrEmpty(sCarrier))
                    sCarrier = (string)getSharedData("WCSCD");
                oPreData.sCarrier = sCarrier;

                string sVehicleDescription = string.Empty;
                sVehicleDescription = (string)getSharedData("VEHDIS");
                if (string.IsNullOrEmpty(sVehicleDescription))
                    sVehicleDescription = (string)getSharedData("VEHDESC");
                oPreData.sVehicleDescription = sVehicleDescription;

                if (sLastButton.Equals("IDH_CREATE")) {
                    if (sVehReg.Length == 0)
                        sVehReg = (string)getUFValue("uBCVehReg");

                    oPreData.Create = true;
                }  
                oPreData.VehReg = sVehReg;
                
                //SV 26/03/2015
                if (!Config.ParameterAsBool("VMUSENEW", false)) {
                    oPreData.Customer = (string)getSharedData("CUSCD");
                    oPreData.Branch = (string)getSharedData("BRANCH");
                    oPreData.ItemGroup = (string)getSharedData("ITEMGROUP");
                    oPreData.sDriver = (string)getSharedData("DRIVER");
                //End
                }
                setUFValue("uBCVehReg", sVehReg);
            }
        }
#endregion

#region OldSearchForms
        protected virtual void doChooseBPCode(bool bFilter,string sItemCode) {
            string sCardCode = (string)getUFValue(FIELD_BPCODE);
            string sCardName = (string)getUFValue(FIELD_BPNAME);

            if (bFilter) {
                setSharedData("IDH_BPCOD", sCardCode);
                setSharedData("IDH_NAME", sCardName);
            } else {
                setSharedData("IDH_BPCOD", "");
                setSharedData("IDH_NAME", "");
            }
            setSharedData("CALLEDITEM", sItemCode);
            setSharedData("TRG", "BP");
            //
            //setSharedData(oForm, "IDH_TYPE", "F-C")
            //
            setSharedData("SILENT", "SHOWMULTI");

            doOpenModalForm("IDHCSRCH");
        }

        protected virtual void doChooseWasteCode(bool bFilter,string sItemCode) {
            string sWasteCode = (string)getUFValue(FIELD_WASTECODE);
            string sWasteName = (string)getUFValue(FIELD_WASTENAME);

            string sTransactionCode = (string)getUFValue(FIELD_TRANSACTIONCODE);
            if (sTransactionCode != null && sTransactionCode.Length > 0) {
                IDH_TRANCD oTransactionCode = new IDH_TRANCD();
                if (!oTransactionCode.getByKey(sTransactionCode)) {
                    //DataHandler.INSTANCE.doUserError("Invalid Transaction Code. [" + sTransactionCode + "]");
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Invalid Transaction Code. [" + sTransactionCode + "]", "ERUSINVT", new string[] { sTransactionCode });
                } else {
                    if (Config.INSTANCE.isIncomingJob(null, oTransactionCode.U_OrdType))
                        setSharedData("IDH_PURWB", "Y");
                    else
                        setSharedData("IDH_PURWB", "^N");
                }
            }

            if (bFilter) {
                setSharedData("IDH_ITMCOD", sWasteCode);
                setSharedData("IDH_ITMNAM", sWasteName);
            } else {
                setSharedData("IDH_ITMCOD", "");
                setSharedData("IDH_ITMNAM", "");
            }
            setSharedData("TRG", "WC");
            setSharedData("SILENT", "SHOWMULTI");
            setSharedData("CALLEDITEM", sItemCode);
            doOpenModalForm("IDHWISRC");
        }
#endregion
    }
}

