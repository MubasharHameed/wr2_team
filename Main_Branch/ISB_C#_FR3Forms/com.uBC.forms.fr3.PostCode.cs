using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.forms.oo;
using com.idh.bridge.data;
using com.idh.dbObjects.User;
using IDHAddOns.idh.controls;
using com.uBC.utils;
using com.idh.controls;
using com.idh.bridge.utils;
using com.uBC.forms.fr3;
using com.idh.dbObjects.strct;
using com.idh.bridge.lookups;

namespace com.uBC.forms.fr3 {
    public class PostCode : com.uBC.forms.fr3.dbGrid.Base {
        
        public IDH_POSTCODE ObjPostCode {
            get { return (IDH_POSTCODE)moDBObject; }
        }

        public PostCode(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
            mbIsSearch = true;
            SkipFormSettings = true;
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_PSTCD", sParMenu, iMenuPos, "uBC_PostCodeSearch.srf", false, true, false, "Search Address", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        protected override void doInitialize() {
            moDBObject = new IDH_POSTCODE();
        }
        
#region Properties
        //Id
        //DomesticId
        //Language
        //LanguageAlternatives
        //Department
        private string _Company;
        public string Company {
            get { return _Company; }
            set { _Company = value; }
        }
        private string _SubBuilding;
        public string SubBuilding {
            get { return _SubBuilding; }
            set { _SubBuilding = value; }
        }
        private string _BuildingNumber;
        public string BuildingNumber {
            get { return _BuildingNumber; }
            set { _BuildingNumber = value; }
        }
        private string _BuildingName;
        public string BuildingName {
            get { return _BuildingName; }
            set { _BuildingName = value; }
        }

        private string _SecondaryStreet;
        public string SecondaryStreet {
            get { return _SecondaryStreet; }
            set { _SecondaryStreet = value; }
        }
        private string _Street;
        public string Street {
            get { return _Street; }
            set { _Street = value; }
        }
        private string _Block;
        public string Block {
            get { return _Block; }
            set { _Block = value; }
        }
        //Neighbourhood
        private string _District;
        public string District {
            get { return _District; }
            set { _District = value; }
        }
        private string _City;
        public string City {
            get { return _City; }
            set { _City = value; }
        }
        private string _Line1;
        public string Line1 {
            get { return _Line1; }
            set { _Line1 = value; }
        }
        private string _Line2;
        public string Line2 {
            get { return _Line2; }
            set { _Line2 = value; }
        }
        private string _Line3;
        public string Line3 {
            get { return _Line3; }
            set { _Line3 = value; }
        }
        private string _Line4;
        public string Line4 {
            get { return _Line4; }
            set { _Line4 = value; }
        }
        private string _Line5;
        public string Line5 {
            get { return _Line5; }
            set { _Line5 = value; }
        }
        private string _AdminAreaName;
        public string AdminAreaName {
            get { return _AdminAreaName; }
            set { _AdminAreaName = value; }
        }
        //AdminAreaCode
        //Province
        private string _ProvinceName;
        public string ProvinceName {
            get { return _ProvinceName; }
            set { _ProvinceName = value; }
        }
        private string _ProvinceCode;
        public string ProvinceCode {
            get { return _ProvinceCode; }
            set { _ProvinceCode = value; }
        }

        private string _PostalCode;
        public string PostalCode {
            get { return _PostalCode; }
            set { _PostalCode = value; }
        }
        private string _CountryName;
        public string CountryName {
            get { return _CountryName; }
            set { _CountryName = value; }
        }

        private string _CountryIso2;
        public string CountryIso2 {
            get { return _CountryIso2; }
            set { _CountryIso2 = value; }
        }
        //CountryIso3
        //CountryIsoNumber
        //SortingNumber1
        //SortingNumber2
        //Barcode
        private string _POBoxNumber;
        public string POBoxNumber {
            get { return _POBoxNumber; }
            set { _POBoxNumber = value; }
        }
        private string _Label;
        public string Label {
            get { return _Label; }
            set { _Label = value; }
        }
        private string _Type;
        public string Type {
            get { return _Type; }
            set { _Type = value; }
        }
        //DataLevel
        private string _AddressToSearch = "";
        /// <summary>
        /// Pass the address string to search, It will be prefilled in address field on form.
        /// </summary>
        public string AddressToSearch {
            get { return _AddressToSearch; }
            set { _AddressToSearch = value; }
        }
#endregion

#region FormOpenCreateFunctions
        /** 
         * This Function will be called by the controller to allow the class to last minute Form Layout adjustments before it gets displayed
         * All changes made in hete will be cached if this is a cached form, the method will not be called again once the form has benn cached.
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            base.doCompleteCreate(ref BubbleEvent);

            doAddUF("IDH_ADDRES", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 300);
            doAddUF("uBC_CONTRY", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50);
            AutoManaged = false;
            Items.Item("1").AffectsFormMode = false;

        }

        public override void doBeforeLoadData() {

            mbIsSearch = true;
            SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_Ok;
            SkipFormSettings = true;
            Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;

            Items.Item("uBC_CONTRY").AffectsFormMode = false;
            Items.Item("IDH_ADDRES").AffectsFormMode = false;
            doFillCombos();
            base.doBeforeLoadData();
            Items.Item("LINESGRID").AffectsFormMode = false;
            moAdminGrid.doSetDeleteActive(true);
            moAdminGrid.AddEditLine = false;
            moDBObject.AutoAddEmptyLine = false;
            setUFValue("IDH_ADDRES", _AddressToSearch);
        }

        /* 
         * Load the Form Data
         */
        public override void doLoadData() {
            base.doLoadData();
        }

        /*
         * Set the Filter Fields
         */
        public override void doSetFilterFields() {
        }

        /*
         * Set the List Fields
         */
        public override void doSetListFields() {
            moAdminGrid.doAddListField(IDH_POSTCODE._Code, "Code", false, 0, null, null);//null,-1,SAPbouiCOM.BoLinkedObject.lf_None,true,true);
            moAdminGrid.doAddListField(IDH_POSTCODE._Name, "Name", false, 0, null, null);//null,-1,SAPbouiCOM.BoLinkedObject.lf_None,true,true);
            moAdminGrid.doAddListField(IDH_POSTCODE._ID, "ID", false, 0, null, "ID");//null,-1,SAPbouiCOM.BoLinkedObject.lf_None,true,true);
            moAdminGrid.doAddListField(IDH_POSTCODE._Text, "Address", false, 250, null, "Address");//, -1, SAPbouiCOM.BoLinkedObject.lf_None, true, true);
            moAdminGrid.doAddListField(IDH_POSTCODE._Next, "Next", false, 30, null, "Next");//, -1, SAPbouiCOM.BoLinkedObject.lf_None, true, true);
            moAdminGrid.doAddListField(IDH_POSTCODE._Highlight, "Highlight", false, 0, null, "Highlight");//null,-1, SAPbouiCOM.BoLinkedObject.lf_None, true, true);
            moAdminGrid.doAddListField(IDH_POSTCODE._Cursor, "Cursor", false, 0, null, "Cursor");//, -1, SAPbouiCOM.BoLinkedObject.lf_None, true, true);
            moAdminGrid.doAddListField(IDH_POSTCODE._Description, "Description", false, 0, null, "Description");//, -1, SAPbouiCOM.BoLinkedObject.lf_None, true, true);
        }
#endregion

#region Events

        protected override void doSetHandlers() {
            base.doSetHandlers();
            addHandler_CLICK("IDH_SRADDR", new ev_Item_Event(doAddressSearchClickEvent));

        }

        protected override void doSetGridHandlers() {
            base.doSetGridHandlers();

            moAdminGrid.Handler_GRID_DOUBLE_CLICK = new IDHGrid.ev_GRID_EVENTS(doRowDoubleClickEvent);
        }
        #endregion

#region ItemEventHandlers
        public override bool doOkEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            //if (!pVal.BeforeAction)
            //  return true;
            try {
                if (!(moAdminGrid.getSelectedRows() != null && moAdminGrid.getSelectedRows().Count > 0)) {
                    BubbleEvent = false;
                    return true;
                } else {
                    System.Collections.ArrayList aRows = moAdminGrid.getSelectedRows();
                    int iRow = (int)aRows[0];
                    string sLastID = (string)moAdminGrid.doGetFieldValue(IDH_POSTCODE._ID, iRow);
                    string sText = (string)moAdminGrid.doGetFieldValue(IDH_POSTCODE._Text, iRow);
                    string sNext = (string)moAdminGrid.doGetFieldValue(IDH_POSTCODE._Next, iRow);
                    if (sLastID == null || sLastID.Trim().Equals(string.Empty))
                        return false;
                    if (sNext.Trim().Equals("Retrieve")) {
                        bool ret = doRetrieveAddress(sLastID);
                        if (ret == false)
                            return false;
                        doReturnNormal();
                        //doReturnFromModal();
                    } else if (sNext.Trim().Equals("Find")) {
                        if (sLastID.IndexOf("|") > -1) {
                            string[] aArr = sLastID.Split('|');
                            if (aArr.Length == 1) {
                                sLastID = "";
                                setUFValue("IDH_ADDRES", sText);
                            } else if (aArr.Length == 2) {
                                if (aArr[1].Trim().Equals(string.Empty)) {
                                    sLastID = "";
                                    setUFValue("IDH_ADDRES", sText);
                                }
                            }
                        }

                        ObjPostCode.LastId = sLastID;
                        BubbleEvent = false;
                        return doFindAddress(ref BubbleEvent);

                    }
                }
                return true;
            } catch (Exception Ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD001", null);
                return false;
            }
        }

        public bool doRowDoubleClickEvent(ref IDHAddOns.idh.events.Base pVal) {
            if (pVal.BeforeAction)
                return true;
            try {
                string sLastID = (string)pVal.oGrid.doGetFieldValue(IDH_POSTCODE._ID, pVal.Row);
                string sText = (string)pVal.oGrid.doGetFieldValue(IDH_POSTCODE._Text, pVal.Row);
                string sNext = (string)pVal.oGrid.doGetFieldValue(IDH_POSTCODE._Next, pVal.Row);
                if (sLastID == null || sLastID.Trim().Equals(string.Empty))
                    return true;
                if (sNext.Trim().Equals("Retrieve")) {
                    //SBOForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular);
                    //return true;
                    bool ret = doRetrieveAddress(sLastID);
                    if (ret == false)
                        return false;
                    //doReturnNormal();
                    doReturnFromModal();
                    //if (Handler_DialogOkReturn != null) {
                    //    Handler_DialogOkReturn(this);
                    //}

                    if (Handler_DialogButton1Return != null) {
                        Handler_DialogButton1Return(this);
                    }

                    //doHandleModalResultShared(SBOForm.TypeEx, "1");

                    //bool bBubble = true;
                    //following line is crashing SBO; asked LV to check, 
                    //IDHForm.doEndForm(SBOForm, ref bBubble);                    
                    return false;
                } else if (sNext.Trim().Equals("Find")) {
                    if (sLastID.IndexOf("|") > -1) {
                        string[] aArr = sLastID.Split('|');
                        if (aArr.Length == 1) {
                            sLastID = "";
                            setUFValue("IDH_ADDRES", sText);
                        } else if (aArr.Length == 2) {
                            if (aArr[1].Trim().Equals(string.Empty)) {
                                sLastID = "";
                                setUFValue("IDH_ADDRES", sText);
                            }
                        }
                    }
                    ObjPostCode.LastId = sLastID;
                    bool BubbleEvent = true;
                    return doFindAddress(ref BubbleEvent);
                }
            } catch (Exception Ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD001", null);
                return false;
            }
            return true;
        }

        private bool doRetrieveAddress(string sLastID) {
            try {
                string sError = "";
                System.Data.DataTable dt;
                dt = ObjPostCode.doRetrieveAddress(sLastID, ref sError);
                if (sError != string.Empty) {
                    //com.idh.bridge.DataHandler.INSTANCE.doResUserError(sError, "PCODE001", null);
                    com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar(sError);//show error
                    return false;
                }
                if (dt == null)
                    return false;
                doSharedAddress(dt);

            } catch (Exception Ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD002", null);
                return false;
            }
            return true;
        }

        public bool doAddressSearchClickEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == false) {
                if (!doValidateInputs())
                    return false;
                ObjPostCode.LastId = "";
                return doFindAddress(ref BubbleEvent);
            }
            return true;
        }

        private bool doFindAddress(ref bool BubbleEvent) {
            try {
                string sError = "";
                ObjPostCode.Country = (string)getUFValue("uBC_CONTRY");
                ObjPostCode.SearchTerm = getUFValue("IDH_ADDRES").ToString().Trim();
                System.Data.DataTable dt = ObjPostCode.doSearchAddresses(ref sError);
                if (sError != string.Empty) {
                    com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar(sError);//show error
                    BubbleEvent = false;
                    return false;
                }
                SBOForm.Freeze(true);
                if (moDBObject.Count > 0) {
                    for (int i = moDBObject.Count; i >= 0; i--) {
                        if (!(moAdminGrid.doGetFieldValue(IDH_POSTCODE._ID, i) == null || moAdminGrid.doGetFieldValue(IDH_POSTCODE._ID, i).ToString().Trim() == ""))
                            moAdminGrid.doRemoveRow(i, false, false);
                    }
                    moAdminGrid.doProcessRemovedRows(moDBObject.DBOTableName);
                }
                ObjPostCode.doFillAddresses(dt);
            } catch (Exception Ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD003", null);
                return false;
            } finally {
                SBOForm.Freeze(false);
            }
            return true;
        }

        public bool doValidateInputs() {
            try {
                ObjPostCode.SearchTerm = "";
                if (ObjPostCode.sKey == null || ObjPostCode.sKey == "") {
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("", "PCODE002", null);
                    //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar(com.idh.bridge.resources.Messages.getGMessage("PCODE002", null));//Address Sesrch is not configured. Please contact system administrator
                    return false;
                }
                if (getUFValue("IDH_ADDRES") == null || getUFValue("IDH_ADDRES").ToString().Trim() == string.Empty) {
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("", "PCODE001", null);
                    //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar(com.idh.bridge.resources.Messages.getGMessage("PCODE001", null));//Please enter PoseCode/address to search
                    return false;
                }
            } catch (Exception Ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD004", null);
                return false;
            }
            return true;
        }

        private void doSharedAddress(System.Data.DataTable dt) {
            //for (int i = 0; i <= dt.Columns.Count - 1; i++) {
            //    setSharedData(dt.Columns[i].ColumnName, dt.Rows[0][i].ToString().Trim());
            //}
            try {
                if (dt == null || dt.Rows.Count == 0)
                    return;
                _Company = dt.Rows[0]["Company"].ToString().Trim();
                _SubBuilding = dt.Rows[0]["SubBuilding"].ToString().Trim();
                _BuildingNumber = dt.Rows[0]["BuildingNumber"].ToString().Trim();
                _BuildingName = dt.Rows[0]["BuildingName"].ToString().Trim();
                _SecondaryStreet = dt.Rows[0]["SecondaryStreet"].ToString().Trim();
                _Street = dt.Rows[0]["Street"].ToString().Trim();
                _Block = dt.Rows[0]["Block"].ToString().Trim();
                //Neighbourhood
                _District = dt.Rows[0]["District"].ToString().Trim();
                _City = dt.Rows[0]["City"].ToString().Trim();
                _Line1 = dt.Rows[0]["Line1"].ToString().Trim();
                _Line2 = dt.Rows[0]["Line2"].ToString().Trim();
                _Line3 = dt.Rows[0]["Line3"].ToString().Trim();
                _Line4 = dt.Rows[0]["Line4"].ToString().Trim();
                _Line5 = dt.Rows[0]["Line5"].ToString().Trim();
                _AdminAreaName = dt.Rows[0]["AdminAreaName"].ToString().Trim();
                //AdminAreaCode
                //Province
                _ProvinceName = dt.Rows[0]["ProvinceName"].ToString().Trim();
                _ProvinceCode = dt.Rows[0]["ProvinceCode"].ToString().Trim();
                _PostalCode = dt.Rows[0]["PostalCode"].ToString().Trim();
                _CountryName = dt.Rows[0]["CountryName"].ToString().Trim();
                _CountryIso2 = dt.Rows[0]["CountryIso2"].ToString().Trim();
                //CountryIso3
                //CountryIsoNumber
                //SortingNumber1
                //SortingNumber2
                //Barcode
                _POBoxNumber = dt.Rows[0]["POBoxNumber"].ToString().Trim();
                _Label = dt.Rows[0]["Label"].ToString().Trim();
                _Type = dt.Rows[0]["Type"].ToString().Trim();

                if (_AdminAreaName == _City)
                    _AdminAreaName = "";
                if (_CountryIso2 == "GB")
                    _AdminAreaName = _ProvinceName;
            } catch (Exception Ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD005", null);
            }
        }
#endregion

#region doFillCombos
        private void doFillCombos() {
            FillCombos.FillCountry(Items.Item("uBC_CONTRY").Specific);
            SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)SBOForm.Items.Item("uBC_CONTRY").Specific;
            string sDfltCountry = Config.INSTANCE.getParameterWithDefault("DFTCNTRY", "GB");
            oCombo.SelectExclusive(sDfltCountry, SAPbouiCOM.BoSearchKey.psk_ByValue);
        }

        #endregion

    }
}
