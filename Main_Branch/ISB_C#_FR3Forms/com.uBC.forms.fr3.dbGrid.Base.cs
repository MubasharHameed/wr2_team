﻿using System.IO;
using System.Collections;
using System;

using IDHAddOns.idh.controls;
using IDHAddOns;
using com.idh.bridge.data;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.idh.dbObjects;
using com.idh.controls;
using com.idh.dbObjects.strct;
using com.idh.dbObjects.User;

namespace com.uBC.forms.fr3.dbGrid {
    public abstract class Base : com.idh.forms.oo.Form {
        private IDH_FORMSET moFormSettings;
        protected DBBase moDBObject;
        public DBBase DBObject {
            get { return moDBObject; }
        }

        private bool mbSkipFormSettings = Config.INSTANCE.getParameterAsBool("FORMSET", false);
        public bool SkipFormSettings {
            set { mbSkipFormSettings = value; }
            get { return mbSkipFormSettings; }
        }

        protected DBOGrid moAdminGrid;
        
        protected bool mbIsSearch = false;

        public Base(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm) : base(oIDHForm, sParentId, oSBOForm) {
            doInitialize();
            doSetHandlers();
        }
        
        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sFormTypeId, string sParMenu, int iMenuPos, string sSRF, string sTitle ) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController(sFormTypeId, sParMenu, iMenuPos, (sSRF==null||sSRF.Length==0?"admin.srf":sSRF), false, true, false, sTitle, IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        protected abstract void doInitialize();
        
#region FormOpenCreateFunctions
        /** 
         * This Function will be called by the controller to allow the class to last minute Form Layout adjustments before it gets displayed
         * All changes made in hete will be cached if this is a cached form, the method will not be called again once the form has benn cached.
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            ///*
            // * The unique ID of an item.
            // * When setting this property, navigation buttons are added that control moving from one 
            // * record to the next for the table or query that is bound to the specified field.
            // */
            //DataBrowser.BrowseBy = "Code";

            ///*
            // * The range of modes available to the form. 
            // * You can use one mode only, more than one mode (using OR), or all modes available in the application.
            // * In SAP Business One, forms can appear in several modes that determine the type of actions users can 
            // * perform on the form. For example, Add mode is used to add records to a form, while OK mode is used to display the form information.
            // *      fm_FIND_MODE 0 Find mode
            // *      fm_OK_MODE 1 OK mode
            // *      fm_UPDATE_MODE 2 Update mode
            // *      fm_ADD_MODE 3 Add mode
            // *      fm_VIEW_MODE 4 View mode
            // *      fm_PRINT_MODE 5 Print mode
            // *      
            // *      -1 is All modes
            // */
            //SupportedModes = -1;

            ///*
            // * Indicates whether the application manages your add-on form modes automatically.
            // * When set to true, the application controls the form mode when the form is loaded 
            // * and manages the status of items and menus (relevant to a specific mode) in mode changes.
            // * Specify the modes that are to be managed on the form with the SupportedModes property.
            // */
            //AutoManaged = true;

            ///*
            // * Form Mode when the Form is opened
            // *      fm_FIND_MODE 0 Find mode
            // *      fm_OK_MODE 1 OK mode
            // *      fm_UPDATE_MODE 2 Update mode
            // *      fm_ADD_MODE 3 Add mode
            // *      fm_VIEW_MODE 4 View mode
            // *      fm_PRINT_MODE 5 Print mode
            // */
            //Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE;
        }

        protected virtual void doGridLayout() {
            if (Config.INSTANCE.getParameterAsBool("FORMSET", false) && !mbSkipFormSettings) {
                if ( moFormSettings == null ) {
                    moFormSettings = new IDH_FORMSET();
                    moFormSettings.getFormGridFormSettings(SBOForm.TypeEx, moAdminGrid.GridId,"##@@##"); //doGetCapacityQuery())
                    moFormSettings.doAddFieldToGrid(moAdminGrid);

                    doSetListFields();
                    moFormSettings.doSyncDB(moAdminGrid);
                } else {
                    if ( moFormSettings.SkipFormSettings ) {
                        doSetListFields();
                    } else {
                        moFormSettings.doAddFieldToGrid(moAdminGrid);
                    }
                }
            } else {
                doSetListFields();
            }
        }
        
        /** 
         * Do the final form steps to show before loaddata
         */
        public override void doBeforeLoadData() {
            try {
                Title = IDHForm.gsTitle;

                SAPbouiCOM.Item oItem;
                oItem = Items.Item("IDH_CONTA");
                moAdminGrid = new DBOGrid(IDHForm, SBOForm, "LINESGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane, moDBObject);
                moAdminGrid.doSetDeleteActive(!mbIsSearch);
                moAdminGrid.AddEditLine = !mbIsSearch;
                moDBObject.AutoAddEmptyLine = !mbIsSearch;

                doSetGridHandlers();
                doSetFilterFields();

                doGridLayout();
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doExceptionError(ex.ToString(), "Error completing the form.");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
                //BubbleEvent = false;
            }
        }

        /*
         * Set the Filter Fields
         */
        public virtual void doSetFilterFields() {
        }

        /*
         * Set the List Fields
         */
        public virtual void doSetListFields() {
            for ( int i = 0; i < moDBObject.getColumnCount(); i++ ) {
                DBFieldInfo oFieldInfo = moDBObject.getFieldInfo(i);
                moAdminGrid.doAddListField(oFieldInfo.FieldName, oFieldInfo.Description, !mbIsSearch, -1, null, null);
            }
        }

        /** 
         * Load the Form Data
         */
        public override void doLoadData() {
            //moAdminGrid.doAutoListFields(true);
            //moDBObject.getData();
            moAdminGrid.doReloadSetExtra(true, false);
        }

        /** 
         * Do the final form steps to show after loaddata
         */
        public override void doFinalizeShow() {
            base.doFinalizeShow();
        }
#endregion

#region Events
        /**
         * Register all your event Handlers in here.
         * Handler_ALL_EVENTS 0 All events, for use when filtering events 
         * Handler_ITEM_PRESSED 1 The main mouse button was clicked on one of the following: 
         * 		Tab 
         * 		Button 
         * 		Option button 
         * 		Check box (standalone or within a cell)
         * 		This event occurs when a mouse is released within an item, that is, mouse up.
         * Handler_KEY_DOWN 2 A key was pressed. 
         * Handler_GOT_FOCUS 3 An item received focus. 
         * Handler_LOST_FOCUS 4 An item lost focus. 
         * Handler_LOST_FOCUS_CHANGED 4 An item lost focus with the value changed
         * Handler_COMBO_SELECT 5 A value was selected in a combo box. 
         * Handler_CLICK 6 The main mouse button was clicked on an item, that is, mouse down. 
         * Handler_DOUBLE_CLICK 7 The main mouse button was double-clicked on an item. 
         * Handler_MATRIX_LINK_PRESSED 8 A link arrow in a matrix was pressed. 
         * Handler_MATRIX_COLLAPSE_PRESSED 9 A matrix list was collapsed or expanded. 
         * Handler_VALIDATE 10 An item lost focus and validation is required.
         * Handler_VALIDATE_CHANGED 10 An item lost focus and validation is required - only when the data has changed.
         * Handler_MATRIX_LOAD 11 Data was loaded from the database into a matrix data source. 
         * 		This event occurs for user-defined matrix objects only. If a form contains two matrix objects, the system generates two events.
         * Handler_DATASOURCE_LOAD 12 Data was loaded from the GUI into a matrix data source. 
         * 		This event occurs once per matrix. If a form has two matrix objects, the system generates two events.
         * Handler_FORM_LOAD 16 A form was opened (FormDataEvent). 
         * Handler_FORM_UNLOAD 17 A form was closed. 
         * Handler_FORM_ACTIVATE 18 A form received focus. 
         * Handler_FORM_DEACTIVATE 19 A form lost focus. 
         * Handler_FORM_CLOSE 20 A form is about to be closed. 
         * Handler_FORM_RESIZE 21 A form has been resized. 
         * Handler_FORM_KEY_DOWN 22 A key was pressed when no form had the focus. 
         * Handler_FORM_MENU_HILIGHT 23 A form is modifying the status of toolbar items, that is, enabling and disabling icons. 
         * 		By default, these events are not thrown. You can activate these events by setting an event filter.
         * Handler_PRINT 24 A print preview was requested for a report or document (PrintEvent). 
         * 		This event lets you exit from the application print operation and use your own printing.
         * Handler_PRINT_DATA 25 A print preview was requested for a report (ReportDataEvent).  
         * 		This event lets you get the report data in XML format.
         * Handler_CHOOSE_FROM_LIST 27 A ChooseFromList event occurred, as follows:  
         * 		The Before event occurs before the ChooseFromList form is displayed. If the BubbleEvent parameter is set to False, the form is not displayed. 
         * 		The After event occurs after the user makes a selection or chooses Cancel.		 
         * Handler_RIGHT_CLICK 28 The right mouse button was clicked on an item (RightClickEvent). 
         * Handler_MENU_CLICK 32 The main mouse button was released on a menu item without submenus. 
         * 		For future use.
         * Handler_FORM_DATA_ADD 33 A record in a business object was added (FormDataEvent). 
         * Handler_FORM_DATA_UPDATE 34 A record in a business object was updated (FormDataEvent). 
         * Handler_FORM_DATA_DELETE 35 A record in a business object was deleted (FormDataEvent). 
         * Handler_FORM_DATA_LOAD 36 A record in a business object was loaded -- via browse, link button, or find (FormDataEvent). 
         * Handler_PICKER_CLICKED 37 A Picker event occurred, as follows:  
         * 		The Before event occurs before the Picker form is displayed. If the BubbleEvent parameter is set to False, the picker is not displayed. 
         * 		The After event occurs after the user makes a selection or chooses Cancel.
         * Handler_GRID_SORT 38 A grid column was sorted, either by a user clicking the column title or an add-on calling the Sort method of the Grid object. 
         * Handler_Drag 39 The item was dragged and dropped at the position you want it to appear. 
         * Handler_PRINT_LAYOUT_KEY 80 A print or print preview was requested for an add-on form, and the layout needs the key of the add-on form 		
         * 
         * To Add a Item PressEvent for a specific Control use 
         * addHandler_ITEM_PRESSED("ITEMID", new ev_Item_Event(EventFunction));
         *      e.g. addHandler_ITEM_PRESSED("uBCCardSR", new ev_Item_Event(doHandleCardCodeSearch));
         *      
         *           public bool doHandleCardCodeSearch(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
         *               return false;
         *           }
         *           
         *  The following Special Event Handlers can be set when using the SBO default where Button 1 is OK 
         *  Handler_Button_Ok - When the Form is in Ok or View Mode
         *  Handler_Button_Find - When the Form is in Find Mode
         *  Handler_Button_Add - When the Form is in Add Mode
         *  Handler_Button_Update - When the Form is in Update Mode
         *   
         * The Menu Events
         * Handler_Menu_NAV_ADD
		 * Handler_Menu_NAV_FIND
		 * Handler_Menu_NAV_FIRST
		 * Handler_Menu_NAV_LAST
		 * Handler_Menu_NAV_NEXT
         * Handler_Menu_NAV_PREV
         * --------> If the BubbleEvent is not set to False the normal Overridable Function doMenuEvent will be called 
         *           doMenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent)
         *protected
         * Special Handler that will be called if this form was opened as a Dialog (Opened from another Form)
         * Handler_DialogButton1Return - When Button 1 was pressed irrespective of the Form Mode
         * Handler_DialogOkReturn - When Button 1 was Pressed and Form was in Ok/View Mode
         * Handler_DialogCancelReturn = When Button 2 was Pressed
         * --------> This will only be called if all the other Button 1 /2 events where called including the Overridable 
         *           Function doButtonID1 and the Bubble event is still true.
         */
        protected virtual void doSetHandlers() {
            Handler_Button_Add = new ev_Item_Event(doAddUpdateEvent);
            Handler_Button_Update = new ev_Item_Event(doAddUpdateEvent);

            Handler_Button_Ok = new ev_Item_Event(doOkEvent);
        }

        /*
         * Handler_GRID_ROW_ADD
         * Handler_GRID_ROW_ADD_EMPTY
         * Handler_GRID_NORESULTS
         * Handler_GRID_ROW_VIEW_EDIT
         * Handler_GRID_ROW_DEL
         * Handler_GRID_ROW_DEL_MULTI
         * Handler_GRID_ROW_ADD_MULTI
         * Handler_GRID_ROW_ADD_EMPTY_MULTI
         * Handler_GRID_ROW_SELECT
         * Handler_GRID_ROW_DESELECT
         * Handler_GRID_DATA_SAVE
         * Handler_GRID_DATA_KEY_EMPTY
         * Handler_GRID_FIELD_CHANGED
         * Handler_GRID_MULTI_SELECT
         * Handler_GRID_RIGHT_CLICK
         * Handler_GRID_DOUBLE_CLICK
         * Handler_GRID_SORT_CLICK
         * Handler_GRID_MENU_EVENT
         * Handler_GRID_FORMAT_FIELDS
         * Handler_GRID_SORT
         * Handler_GRID_SEARCH
         * Handler_GRID_SEARCH_VALUESET
         * Handler_GRID_ALL
         */
        protected virtual void doSetGridHandlers() {
        }
#endregion 

#region ItemEventHandlers

        public virtual bool doAddUpdateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                Freeze(true);
                if (moAdminGrid.doProcessData()) {
                    doLoadData();
                    moAdminGrid.doApplyRules();
                }
                Freeze(false);
            }
            return true;
        }

        public virtual bool doOkEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
            }
            return true;
        }
#endregion 

#region OldEventHandlers
        /**
         * Most of these are replaced with Handlers as above... only use these if there is really no Event or in special events
         */

        /*
         * Handle all the Menu Events.
         * Return True if the Event must be handled by the other Objects
         */
        public override bool doMenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * The Event Handler that will receive all Activated Events for this Form
         * Return True if the Event must be handled by the other Objects
         */
        public override bool doItemEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Send By Custom Controls
         */
        public override bool doCustomItemEvent(ref IDHAddOns.idh.events.Base pVal) {
            return true;
        }

        /*
         * Handle the Request for a Help File
         */
        public override bool doHelpFile(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Handles the right click event
         */
        public override bool doRightClickEvent(ref SAPbouiCOM.ContextMenuInfo pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Handle the Button One Pressed Event
         */
        public override void doButtonID1(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        }

        /* 
         * Handle Button Two Presssed Event
         */
        public override void doButtonID2(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        }

        /* 
         * Return As Canceled If this was Opened as a Dialog from another form
         */
        public override void doReturnCanceled(string sModalFormType) {
        }

        ///* 
        // * Return as Ok If this was Opened as a Dialog from another form setting the Data in the Global Shared Buffer
        // * This will be called from the Conroller after all the other Button 1 Handlers and the BubbleEvent is still True.
        // */
        //public override void doReturnFromModalShared(bool bState) {
        //}

        //*** Return As Normal
        public override void doReturnNormal() {
        }

        /* 
         * Handle the Cancel Event when a Dialog returns to this form.
         */
        public override void doHandleModalCanceled(string sModalFormType) {
        }

        /* 
         * Handle the Return from a Dialog called from this Form with the data in oData
         */
        public override void doHandleModalBufferedResult(ref object oData, string sModalFormType, string sLastButton) {
        }

        /* 
         * Handle the Return from a Dialog called from this Form with the data in the Global Shared Buffer
         */
        public override void doHandleModalResultShared(string sModalFormType, string sLastButton) {
        }
#endregion
    }
}

