﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.uBC.utils.Dynamic {
    public class FFPopulation {
        public FFPopulation(List<SBOFFItem> oCache) {
            Parallel.For(0, oCache.Count, i => {
                oCache.ElementAt(i).doSetLableCaption();
                oCache.ElementAt(i).doSetControlContent();
            });
        }
       
    }
}
