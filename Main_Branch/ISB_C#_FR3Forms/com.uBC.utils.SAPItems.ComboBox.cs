﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.controls;
using com.uBC.utils.SAPItems.components;
using com.uBC.utils.SAPItems.components.Base;
using com.idh.forms.oo;

namespace com.uBC.utils.SAPItems {
    public class ComboBox<P> : FormItem<SAPbouiCOM.ComboBox,P> where P : components.Base.PopulationType {
        #region Member Variables
        private List<ValuePair> moValidValues;
        private SortedDictionary<string, ValuePair> moValues;
        private Property<ValuePair> moValue;
        #endregion

        #region Constructor
        public ComboBox(Form oForm, string sUniqueID, string sCaption, ValuePair oInitialValue = null)
            : base(oForm, sUniqueID, SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX, new FieldInfo()) {
            doInit();
            if (oInitialValue != null)
                addValidValue(oInitialValue.Value, oInitialValue.Description);
        }
        public ComboBox(Form oForm, SAPbouiCOM.Item oControl, SAPbouiCOM.Item oLable, ValuePair oInitialValue = null)
            : base(oForm, oControl, oLable, new FieldInfo()) {
            doInit();
            if (oInitialValue != null)
                addValidValue(oInitialValue.Value, oInitialValue.Description);
        }
        #endregion

        #region Methods
        private void doInit() {
            moValue = new Property<ValuePair>();
            moValidValues = new List<ValuePair>(10);
            moValues = new SortedDictionary<string, ValuePair>();
            moValue.PropertyChanged += new Delegates.PropertyChanged<ValuePair>(moValue_PropertyChanged);
            base.Form.addHandler_LOST_FOCUS(base.UniqueID, Spesific_Validate);
        }

        public bool doPopulateCombo(List<ValuePair> oValidValues) {
            foreach (ValuePair oVP in oValidValues) {
                addValidValue(oVP.Value, oVP.Description);
            }
            return true;
        }
        public bool doPopulateCombo(Delegates.ComboType oTarget) {
            oTarget.Invoke(FillCombos.getCombo(base.Form.SBOForm, base.UniqueID), null, null);
            var oVVE = base.Spesific.ValidValues.GetEnumerator();
            oVVE.Reset();
            while (oVVE.MoveNext()) {
                SAPbouiCOM.ValidValue oVV = (SAPbouiCOM.ValidValue)oVVE.Current;
                addKnownValidValue(oVV.Value, oVV.Description);
            }
            return true;
        }
        public bool doPopulateCombo(string sPopParameters) {
            return this.doPopulate(sPopParameters);
        }
        private bool addKnownValidValue(string sVal, string sDesc) {
            if (!moValues.ContainsKey(sVal)) {
                moValues.Add(sVal, new ValuePair(sVal,sDesc));
                moValidValues.Add(new ValuePair(sVal, sDesc));
                return true;
            }
            return false;
        }
        public bool addValidValue(string sVal, string sDesc) {
            if (!moValues.ContainsKey(sVal)) {
                moValues.Add(sVal, new ValuePair(sVal,sDesc));
                moValidValues.Add(new ValuePair(sVal,sDesc));
                base.Spesific.ValidValues.Add(sVal, sDesc);
            }
            return false;
        }
        #endregion

        #region Handlers
        private bool moValue_PropertyChanged(Property<ValuePair> oProperty) {
            return true;
        }
        private bool Population_ComboBoxDataReady(List<ValuePair> Data) {
            foreach (ValuePair oVP in Data) {
                addValidValue(oVP.Value, oVP.Description);
            }
            return true;
        }
        private bool Spesific_Validate(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (base.Spesific.Selected != null) {
                if (moValues.Keys.Contains(base.Spesific.Selected.Value)) {
                    moValue.Set(moValues[base.Spesific.Selected.Value]);
                }
            }
            return true;
        }
        #endregion

        #region Overrides
        protected override bool doPopulate(string sPopPar) {
            base.Population = new Population<SAPbouiCOM.ComboBox, P>(sPopPar);
            Population.ComboBoxDataReady += new Delegates.PopulationComplete<List<ValuePair>>(Population_ComboBoxDataReady);
            return base.doPopulate(sPopPar);
        }
        #endregion
        
        #region Properties
        public Property<ValuePair> ValueProperty {
            get { return moValue; }
        }
        public string Value {
            get { return base.Spesific.Value; }
            set {
                if (value != null) {
                    if (moValues.Keys.Contains(value)) {
                        moValue.Set(moValues[value]);
                        base.Spesific.Select(value);
                    }
                }
            }
        }
        #endregion
    }
}
