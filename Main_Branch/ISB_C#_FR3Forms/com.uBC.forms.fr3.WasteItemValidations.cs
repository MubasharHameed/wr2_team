using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

using com.idh.forms.oo;
using com.idh.bridge.data;
using com.idh.dbObjects.User;
using IDHAddOns.idh.controls;
using com.uBC.utils;
using com.idh.controls;
using com.idh.bridge.utils;
using com.uBC.forms.fr3;
using com.idh.dbObjects.strct;
using com.idh.bridge.lookups;
//using com.idh.dbObjects.Base;
using com.idh.bridge;
using com.uBc.forms;
namespace com.uBC.forms.fr3 {
    public class WasteItemValidations : com.idh.forms.oo.Form {

        private Hashtable clsValidatioControls = new Hashtable();
        protected class ValidationControls {
            public ValidationControls() {
                ValidationCode = "";
                ControlID = new ArrayList();
            }
            public string ValidationCode;
            public ArrayList ControlID;
            public SAPbouiCOM.BoFormItemTypes ControlType;
        }

        private string sDefaultNewControl = "";
        private IDH_WITMVLD moCreateWasteItemValidation;
        private bool bManualItemCode = true;
        private string[] FixedItems = { "1", "2", "IDH_ITEMCD", "IDH_ITEMNM", "IDH_FRNNAM", "IDH_WSTGRP", "IDH_STKITM", "IDH_PURITM", "IDH_RBTITM", "IDH_TMPITM" };
        //Select IsManual from NNM1,ONNM where ONNM.ObjectCode='2' And NNM1.ObjectCode='2' and ONNM.DfltSeries=NNM1.Series and NNM1.DocSubType='C'
        //private IDH_CREATEBP moCreateNewBP;
        //public IDH_CREATEBP ObjCreateNewBP {
        //    get { return (IDH_CREATEBP)moCreateNewBP; }
        //}
        public WasteItemValidations(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
            doInitialize();
            doSetHandlers();
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_WGITM", sParMenu, iMenuPos, "Waste Item Validations.srf", false, true, false, "Waste Item Validations", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        protected void doInitialize() {
            moCreateWasteItemValidation = new IDH_WITMVLD();
        }



        #region Properties
        private int _iParentRow = -1;
        public int iParentRow {
            get { return _iParentRow; }
            set { _iParentRow = value; }
        }

        private string _ItemCode = "";
        public string ItemCode {
            get { return _ItemCode; }
            set { _ItemCode = value; }
        }
        private string _ItemName = "";
        public string ItemName {
            get { return _ItemName; }
            //set { _CardCode = value; }
        }
        private string _WasteGroupCode = "";
        public string WasteGroupCode {
            get { return _WasteGroupCode; }
            set { _WasteGroupCode = value; }
        }

        private string _EnquiryID = "";
        public string EnquiryID {
            get { return _EnquiryID; }
            set { _EnquiryID = value; }
        }

        /*  private string _SubBuilding;
          public string SubBuilding
          {
              get { return _SubBuilding; }
              set { _SubBuilding = value; }
          }
          private string _BuildingNumber;
          public string BuildingNumber
          {
              get { return _BuildingNumber; }
              set { _BuildingNumber = value; }
          }
          private string _BuildingName;
          public string BuildingName
          {
              get { return _BuildingName; }
              set { _BuildingName = value; }
          }

          private string _SecondaryStreet;
          public string SecondaryStreet
          {
              get { return _SecondaryStreet; }
              set { _SecondaryStreet = value; }
          }
          private string _Street;
          public string Street
          {
              get { return _Street; }
              set { _Street = value; }
          }
          private string _Block;
          public string Block
          {
              get { return _Block; }
              set { _Block = value; }
          }
          //Neighbourhood
          private string _District;
          public string District
          {
              get { return _District; }
              set { _District = value; }
          }
          private string _City;
          public string City
          {
              get { return _City; }
              set { _City = value; }
          }
          private string _Line1;
          public string Line1
          {
              get { return _Line1; }
              set { _Line1 = value; }
          }
          private string _Line2;
          public string Line2
          {
              get { return _Line2; }
              set { _Line2 = value; }
          }
          private string _Line3;
          public string Line3
          {
              get { return _Line3; }
              set { _Line3 = value; }
          }
          private string _Line4;
          public string Line4
          {
              get { return _Line4; }
              set { _Line4 = value; }
          }
          private string _Line5;
          public string Line5
          {
              get { return _Line5; }
              set { _Line5 = value; }
          }
          private string _AdminAreaName;
          public string AdminAreaName
          {
              get { return _AdminAreaName; }
              set { _AdminAreaName = value; }
          }
          //AdminAreaCode
          //Province
          private string _ProvinceName;
          public string ProvinceName
          {
              get { return _ProvinceName; }
              set { _ProvinceName = value; }
          }
          //ProvinceCode
          private string _PostalCode;
          public string PostalCode
          {
              get { return _PostalCode; }
              set { _PostalCode = value; }
          }
          private string _CountryName;
          public string CountryName
          {
              get { return _CountryName; }
              set { _CountryName = value; }
          }
          //CountryIso2
          //CountryIso3
          //CountryIsoNumber
          //SortingNumber1
          //SortingNumber2
          //Barcode
          private string _POBoxNumber;
          public string POBoxNumber
          {
              get { return _POBoxNumber; }
              set { _POBoxNumber = value; }
          }
          private string _Label;
          public string Label
          {
              get { return _Label; }
              set { _Label = value; }
          }
          private string _Type;
          public string Type
          {
              get { return _Type; }
              set { _Type = value; }
          }
          //DataLevel
          private string _AddressToSearch = "";
          /// <summary>
          /// Pass the address string to search, It will be prefilled in address field on form.
          /// </summary>
          public string AddressToSearch
          {
              get { return _AddressToSearch; }
              set { _AddressToSearch = value; }
          }
            */
        #endregion

        #region FormOpenCreateFunctions
        /** 
         * This Function will be called by the controller to allow the class to last minute Form Layout adjustments before it gets displayed
         * All changes made in hete will be cached if this is a cached form, the method will not be called again once the form has benn cached.
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            base.doCompleteCreate(ref BubbleEvent);

            doAddUF("IDH_ITEMCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 15);
            doAddUF("IDH_ITEMNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100);
            doAddUF("IDH_FRNNAM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 200);
            doAddUF("IDH_WSTGRP", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50);//Waste Grouo Combo

            doAddUF("IDH_STKITM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);//Waste Grouo Combo
            doAddUF("IDH_PURITM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);//Waste Grouo Combo
            doAddUF("IDH_RBTITM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);//Waste Grouo Combo
            doAddUF("IDH_TMPITM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);//Waste Grouo Combo
            //doAddUF("IDH_ENQID", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 8);
            doConfigureForm();

            AutoManaged = false;
            /*Items.Item("1").AffectsFormMode = false;
            for (int i = 0; i <= Items.Count - 1; i++) {
                Items.Item(i).AffectsFormMode = false;
            }*/

        }

        private void doConfigureForm() {

            SAPbobsCOM.Recordset oRecordSet = null;
            SAPbobsCOM.Recordset oRecordSetValues = null;

            try {
                clsValidatioControls.Clear();
                string sQry = "Select a.*,b." + IDH_WGVDMS._DtType + ",b." + IDH_WGVDMS._Fields + ",b." + IDH_WGVDMS._Type + " from [" + IDH_WGVALDS.TableName + "] a,[" + IDH_WGVDMS.TableName + "] b Where a." + IDH_WGVALDS._ValidCd + "=b." + IDH_WGVDMS._ValidCd + " And  a." + IDH_WGVALDS._WstGpCd + "='" + _WasteGroupCode + "' And IsNull(a." + IDH_WGVALDS._Select + ",'N')='Y' Order by " + IDH_WGVALDS._Sort;
                oRecordSet = DataHandler.INSTANCE.doSBOSelectQuery("doGetWasteGroupValidationList", sQry);
                int top = SBOForm.Items.Item("lblTechInf").Top + SBOForm.Items.Item("lblTechInf").Height + 5;
                int lblleft = 5;
                int txtleft = SBOForm.Items.Item("IDH_ITEMCD").Left;
                int lblWidth = SBOForm.Items.Item("6").Width + 10;
                int txtwidth = SBOForm.Items.Item("IDH_ITEMCD").Width;
                int height = SBOForm.Items.Item("lblTechInf").Height;
                int maxtop = SBOForm.Items.Item("1").Top;
                ValidationControls obj;
                SAPbouiCOM.StaticText oLabel;
                SAPbouiCOM.EditText oTextbox;
                SAPbouiCOM.ComboBox oCombo;
                SAPbouiCOM.Item oItem;
                if (oRecordSet != null && oRecordSet.RecordCount > 0) {
                    int iType, iDataType, iFields;
                    string sListofValues = "", sValidationCode = "";
                    string sValue1 = "";
                    string sValue2 = "";
                    for (int iRecord = 0; iRecord <= oRecordSet.RecordCount - 1; iRecord++) {
                        iType = Convert.ToInt32(oRecordSet.Fields.Item(IDH_WGVDMS._Type).Value);
                        iDataType = Convert.ToInt32(oRecordSet.Fields.Item(IDH_WGVDMS._DtType).Value);
                        iFields = Convert.ToInt32(oRecordSet.Fields.Item(IDH_WGVDMS._Fields).Value);
                        sListofValues = Convert.ToString(oRecordSet.Fields.Item(IDH_WGVALDS._ValList).Value);
                        sValidationCode = Convert.ToString(oRecordSet.Fields.Item(IDH_WGVALDS._ValidCd).Value);
                        sValue1 = "";
                        sValue2 = "";
                        if (_ItemCode != string.Empty) {
                            sQry = "Select a.* from [" + IDH_WITMVLD.TableName + "] a Where a." + IDH_WITMVLD._ValidCd + "='" + sValidationCode + "' And  a." + IDH_WITMVLD._WstGpCd + "='" + _WasteGroupCode + "' And a." + IDH_WITMVLD._EnqId + "='" + _EnquiryID + "'";
                            oRecordSetValues = DataHandler.INSTANCE.doSBOSelectQuery("doGetWasteItemValuesByEnquiry", sQry);
                            if (oRecordSetValues != null && oRecordSetValues.RecordCount > 0) {
                                sValue1 = Convert.ToString(oRecordSetValues.Fields.Item(IDH_WITMVLD._Value1).Value);
                                sValue2 = Convert.ToString(oRecordSetValues.Fields.Item(IDH_WITMVLD._Value2).Value);
                            }
                        }
                        switch (iType) {
                            case 1:
                                //Field
                                SBOForm.Items.Add("lbl" + iRecord.ToString(), SAPbouiCOM.BoFormItemTypes.it_STATIC);
                                SBOForm.Items.Add("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoFormItemTypes.it_EDIT);
                                if (sDefaultNewControl == string.Empty)
                                    sDefaultNewControl = "IDH" + iRecord.ToString() + "_1";
                                oItem = SBOForm.Items.Item("lbl" + iRecord.ToString());
                                oItem.Left = lblleft;
                                oItem.Top = top;
                                oItem.Width = lblWidth;
                                oItem.Height = height;
                                oItem.LinkTo = "IDH" + iRecord.ToString() + "_1";
                                oLabel = (SAPbouiCOM.StaticText)oItem.Specific;
                                oLabel.Caption = sValidationCode;

                                oItem = SBOForm.Items.Item("IDH" + iRecord.ToString() + "_1");
                                oItem.Left = txtleft;
                                oItem.Top = top;
                                oItem.Width = txtwidth;
                                oItem.Height = height;
                                if (iDataType == 1) //AlphaNumeric
                                    doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                                else if (iDataType == 2) //Numeric
                                    doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_LONG_NUMBER, 11);
                                else if (iDataType == 3) //Decimal
                                    doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_MEASURE, 11);
                                //ArrayList arry = new ArrayList();
                                obj = new ValidationControls();
                                obj.ControlType = SAPbouiCOM.BoFormItemTypes.it_EDIT;
                                obj.ControlID.Add("IDH" + iRecord.ToString() + "_1");
                                obj.ValidationCode = (sValidationCode);

                                clsValidatioControls.Add(sValidationCode, obj);


                                if (top + height + 2 + height + 2 > maxtop) {
                                    top = SBOForm.Items.Item("lblTechInf").Top + SBOForm.Items.Item("lblTechInf").Height + 5;
                                    lblleft = txtleft + txtwidth + 15;
                                    txtleft = lblleft + lblWidth + 10;
                                } else
                                    top += height + 2;

                                if (sValue1 != string.Empty) setUFValue("IDH" + iRecord.ToString() + "_1", sValue1);
                                break;
                            case 2:
                                //Range
                                SBOForm.Items.Add("lbl" + iRecord.ToString(), SAPbouiCOM.BoFormItemTypes.it_STATIC);
                                oItem = SBOForm.Items.Item("lbl" + iRecord.ToString());
                                oItem.Left = lblleft;
                                oItem.Top = top;
                                oItem.Width = lblWidth;
                                oItem.Height = height;
                                oItem.LinkTo = "IDH" + iRecord.ToString() + "_1";
                                oLabel = (SAPbouiCOM.StaticText)oItem.Specific;
                                oLabel.Caption = sValidationCode;
                                if (iFields == 1) {
                                    SBOForm.Items.Add("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoFormItemTypes.it_EDIT);
                                    oItem = SBOForm.Items.Item("IDH" + iRecord.ToString() + "_1");
                                    if (sDefaultNewControl == string.Empty)
                                        sDefaultNewControl = "IDH" + iRecord.ToString() + "_1";
                                    oItem.Left = txtleft;
                                    oItem.Top = top;
                                    oItem.Width = txtwidth;
                                    oItem.Height = height;
                                    SBOForm.Items.Item("lbl" + iRecord.ToString()).LinkTo = oItem.UniqueID;

                                    if (iDataType == 2) //Numeric
                                        doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_LONG_NUMBER, 11);
                                    else if (iDataType == 3) //Decimal
                                        doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_MEASURE, 11);
                                    obj = new ValidationControls();
                                    obj.ControlType = SAPbouiCOM.BoFormItemTypes.it_EDIT;
                                    obj.ControlID.Add("IDH" + iRecord.ToString() + "_1");
                                    obj.ValidationCode = (sValidationCode);
                                    clsValidatioControls.Add(sValidationCode, obj);

                                    if (sValue1 != string.Empty) setUFValue("IDH" + iRecord.ToString() + "_1", sValue1);
                                } else {
                                    int _txtwidth = (txtwidth / iFields) - 2;
                                    int _txtleft = txtleft;
                                    obj = new ValidationControls();
                                    for (int iCtrls = 1; iCtrls <= iFields; iCtrls++) {
                                        SBOForm.Items.Add("IDH" + iRecord.ToString() + "_" + iCtrls.ToString(), SAPbouiCOM.BoFormItemTypes.it_EDIT);
                                        oItem = SBOForm.Items.Item("IDH" + iRecord.ToString() + "_" + iCtrls.ToString());
                                        SBOForm.Items.Item("lbl" + iRecord.ToString()).LinkTo = oItem.UniqueID;
                                        //oItem.LinkTo= SBOForm.Items.Item("IDH" + iRecord.ToString() + "_" + iCtrls.ToString())
                                        oItem.Left = _txtleft;
                                        oItem.Top = top;
                                        oItem.Width = _txtwidth;
                                        oItem.Height = height;
                                        if (iDataType == 2) //Numeric
                                            doAddUF("IDH" + iRecord.ToString() + "_" + iCtrls.ToString(), SAPbouiCOM.BoDataType.dt_LONG_NUMBER, 11);
                                        else if (iDataType == 3) //Decimal
                                            doAddUF("IDH" + iRecord.ToString() + "_" + iCtrls.ToString(), SAPbouiCOM.BoDataType.dt_MEASURE, 11);
                                        _txtleft += _txtwidth + 2;
                                        obj.ControlType = SAPbouiCOM.BoFormItemTypes.it_EDIT;
                                        obj.ControlID.Add("IDH" + iRecord.ToString() + "_" + iCtrls.ToString());
                                        obj.ValidationCode = (sValidationCode);

                                        if (sValue1 != string.Empty) setUFValue("IDH" + iRecord.ToString() + "_1", sValue1);
                                        if (sValue1 != string.Empty) setUFValue("IDH" + iRecord.ToString() + "_2", sValue2);
                                    }
                                    if (sDefaultNewControl == string.Empty)
                                        sDefaultNewControl = "IDH" + iRecord.ToString() + "_1";
                                    clsValidatioControls.Add(sValidationCode, obj);
                                }
                                if (top + height + 2 + height + 2 > maxtop) {
                                    top = SBOForm.Items.Item("lblTechInf").Top + SBOForm.Items.Item("lblTechInf").Height + 5;
                                    lblleft = txtleft + txtwidth + 15;
                                    txtleft = lblleft + lblWidth + 10;
                                } else
                                    top += height + 2;
                                break;
                            //End of Range
                            case 3:
                                //Boolean
                                SBOForm.Items.Add("lbl" + iRecord.ToString(), SAPbouiCOM.BoFormItemTypes.it_STATIC);
                                SBOForm.Items.Add("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX);
                                if (sDefaultNewControl == string.Empty)
                                    sDefaultNewControl = "IDH" + iRecord.ToString() + "_1";
                                oItem = SBOForm.Items.Item("lbl" + iRecord.ToString());
                                oItem.Left = lblleft;
                                oItem.Top = top;
                                oItem.Width = lblWidth;
                                oItem.Height = height;
                                oItem.LinkTo = "IDH" + iRecord.ToString() + "_1";
                                oLabel = (SAPbouiCOM.StaticText)oItem.Specific;
                                oLabel.Caption = sValidationCode;

                                oItem = SBOForm.Items.Item("IDH" + iRecord.ToString() + "_1");
                                oItem.Left = txtleft;
                                oItem.Top = top;
                                oItem.Width = txtwidth;
                                oItem.Height = height;

                                doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);

                                oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
                                oItem.DisplayDesc = true;
                                oCombo.ValidValues.Add("Y", "Yes");
                                oCombo.ValidValues.Add("N", "No");

                                obj = new ValidationControls();
                                obj.ControlType = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX;
                                obj.ControlID.Add("IDH" + iRecord.ToString() + "_1");
                                obj.ValidationCode = (sValidationCode);
                                clsValidatioControls.Add(sValidationCode, obj);


                                if (top + height + 2 + height + 2 > maxtop) {
                                    top = SBOForm.Items.Item("lblTechInf").Top + SBOForm.Items.Item("lblTechInf").Height + 5;
                                    lblleft = txtleft + txtwidth + 15;
                                    txtleft = lblleft + lblWidth + 10;
                                } else
                                    top += height + 2;
                                if (sValue1 != string.Empty) setUFValue("IDH" + iRecord.ToString() + "_1", sValue1);
                                break;
                            //End of Boolean
                            case 4:
                                //Combo
                                SBOForm.Items.Add("lbl" + iRecord.ToString(), SAPbouiCOM.BoFormItemTypes.it_STATIC);
                                SBOForm.Items.Add("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX);
                                if (sDefaultNewControl == string.Empty)
                                    sDefaultNewControl = "IDH" + iRecord.ToString() + "_1";
                                oItem = SBOForm.Items.Item("lbl" + iRecord.ToString());
                                oItem.Left = lblleft;
                                oItem.Top = top;
                                oItem.Width = lblWidth;
                                oItem.Height = height;
                                oItem.LinkTo = "IDH" + iRecord.ToString() + "_1";
                                oLabel = (SAPbouiCOM.StaticText)oItem.Specific;
                                oLabel.Caption = sValidationCode;

                                oItem = SBOForm.Items.Item("IDH" + iRecord.ToString() + "_1");
                                oItem.Left = txtleft;
                                oItem.Top = top;
                                oItem.Width = txtwidth;
                                oItem.Height = height;


                                doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);

                                oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
                                oItem.DisplayDesc = true;
                                string[] aListofValues = sListofValues.Split(';');
                                for (int iValidValues = 0; iValidValues <= aListofValues.Count() - 1; iValidValues++) {
                                    string[] aValues = aListofValues[iValidValues].Split(':');
                                    if (aValues.Count() == 2) {
                                        oCombo.ValidValues.Add(aValues[0], aValues[1]);
                                    }
                                }
                                obj = new ValidationControls();
                                obj.ControlType = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX;
                                obj.ControlID.Add("IDH" + iRecord.ToString() + "_1");
                                obj.ValidationCode = (sValidationCode);
                                clsValidatioControls.Add(sValidationCode, obj);
                                if (top + height + 2 + height + 2 > maxtop) {
                                    top = SBOForm.Items.Item("lblTechInf").Top + SBOForm.Items.Item("lblTechInf").Height + 5;
                                    lblleft = txtleft + txtwidth + 15;
                                    txtleft = lblleft + lblWidth + 10;
                                } else
                                    top += height + 2;
                                if (sValue1 != string.Empty) setUFValue("IDH" + iRecord.ToString() + "_1", sValue1);
                                break;
                            // End of Combo
                        }
                        oRecordSet.MoveNext();
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", new string[] { "Waste Group Validations" });
            } finally {
                DataHandler.INSTANCE.doReleaseRecordset(ref oRecordSet);
            }
        }
        public override void doBeforeLoadData() {

            //  moCreateNewBP.doAddEmptyRow(true, true);

            SupportedModes = (int)SAPbouiCOM.BoFormMode.fm_OK_MODE;

            Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;

            doFillCombos();
            base.doBeforeLoadData();
            //mo.doAddEmptyRow(true, true);
            //setUFValue("IDH_ITEMCD", moCreateNewBP.U_CardCode);
            //setUFValue("IDH_CARDNM", moCreateNewBP.U_CardName);
            //setUFValue("IDH_CARDTY", moCreateNewBP.U_CardType);

            //setUFValue("IDH_ADRID", moCreateNewBP.U_Address);//Address ID 50
            //setUFValue("IDH_STREET", moCreateNewBP.U_Street);//Street 100
            //setUFValue("IDH_BLOCK", moCreateNewBP.U_Block);//Block 100
            //setUFValue("IDH_CITY", moCreateNewBP.U_City);//City 100
            //setUFValue("IDH_PCODE", moCreateNewBP.U_ZipCode);//State 3
            //setUFValue("IDH_COUNTY", moCreateNewBP.U_County);//PostCode 20
            //setUFValue("IDH_STATE", moCreateNewBP.U_State);//County 100
            //setUFValue("IDH_CONTRY", moCreateNewBP.U_Country);//Country 3

            //setUFValue("IDH_CNTPRS", moCreateNewBP.U_CName);//Contact Person 50
            //setUFValue("IDH_EMAIL", moCreateNewBP.U_Email);//Email 100
            //setUFValue("IDH_PHONE", moCreateNewBP.U_Phone);//Phone 20
            //setUFValue("IDH_INFO", "Address:");//Phone 20

        }
        /** 
         * Do the final form steps to show after loaddata
         */
        public override void doFinalizeShow() {
            base.doFinalizeShow();
            string sQry = "Select IsManual from NNM1,ONNM where ONNM.ObjectCode='4' And NNM1.ObjectCode='4' and ONNM.DfltSeries=NNM1.Series ";
            com.idh.bridge.DataRecords oRecords = null;
            oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("doCheckItemCodeSeries", sQry);
            if (oRecords != null && oRecords.RecordCount > 0) {
                if (oRecords.getValue("IsManual").ToString() == "Y") {
                    bManualItemCode = true;
                    EnableItem(true, "IDH_ITEMCD");
                    // moCreateNewBP.U_CardCode = "";
                    doSetFocus("IDH_ITEMCD");

                } else
                    bManualItemCode = false;
            }
            _ItemName = "";
            if (doValidateItembyCode(ref _ItemName)) {
                setUFValue("IDH_ITEMNM", _ItemName);
                //if (sDefaultNewControl != string.Empty)
                //    doSetFocus(sDefaultNewControl);
                //EnableItem(false, "IDH_ITEMNM");
                //EnableItem(false, "IDH_ITEMCD");
                //EnableItem(false, "IDH_FRNNAM");
                doDisableItemFields();
                sQry = "Select InvntItem,PrchseItem,U_IDHREB,U_WPTemplate FROM OITM where ItemCode='" + _ItemCode + "'";
                //  SAPbobsCOM.Recordset oRecordSet = null;
                try {
                    oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("doFinalizeShow", sQry);
                    if (oRecords != null && oRecords.RecordCount > 0) {
                        if (oRecords.getValue("InvntItem").ToString() == "Y")
                            setUFValue("IDH_STKITM", "Y");
                        if (oRecords.getValue("PrchseItem").ToString() == "Y")
                            setUFValue("IDH_PURITM", "Y");
                        if (oRecords.getValue("U_IDHREB").ToString() == "Y")
                            setUFValue("IDH_RBTITM", "Y");
                        if (oRecords.getValue("U_WPTemplate").ToString() == "Y")
                            setUFValue("IDH_TMPITM", "Y");
                    }
                } catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", new string[] { "doFinalizeShow" });
                }
                //finally {
                //    DataHandler.INSTANCE.doReleaseRecordset(ref oRecordSet);
                //}
            }
        }
        /* 
         * Load the Form Data
         */
        public override void doLoadData() {
            base.doLoadData();
        }


        #endregion

        #region Events
        protected void doSetHandlers() {
            //Handler_Button_Ok = doCreateWasteItem;
            Handler_Button_Update = doCreateWasteItem;
            addHandler_ITEM_PRESSED("IDH_CIPSIP", doHandleButtonCIPSIP);

            //   base.doSetHandlers();
            //addHandler_ITEM_PRESSED("1", doCreateBP);

        }

        protected void doSetGridHandlers() {
            //base.doSetGridHandlers();
            addHandler_COMBO_SELECT("IDH_WSTGRP", doHandleWasteComboEvent);
            //moAdminGrid.Handler_GRID_DOUBLE_CLICK = new IDHGrid.ev_GRID_EVENTS(doRowDoubleClickEvent);
        }
        #endregion
        #region ItemEventHandlers
        public bool doHandleButtonCIPSIP(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent){
            if (pVal.BeforeAction)
                return true;
            string sSupplier =(string)getDFValue("OITM", "U_DispFacCd");
            //ility")
            string sBPCd = "";
            string sAddress = "";

            string[] saWPFlagSplit =(string[]) getWFValue("IDH_WPFLAG");
            if ((saWPFlagSplit != null) && saWPFlagSplit.Length > 0) {
                sBPCd = saWPFlagSplit[0];
                sAddress = saWPFlagSplit[2];

                string sICode = saWPFlagSplit[3];
                //Dim sIDesc As String = saWPFlagSplit(4)
                string sFlag = saWPFlagSplit[6];
                string sNewICode = saWPFlagSplit[7];
                string sCIPCPD = saWPFlagSplit[8];

                //Dim sNewICode As String = getDFValue(oForm, "OITM", "ItemCode")
                string sIDesc =(string) getDFValue("OITM", "ItemName");
                if (sFlag.Equals("BNEW") == false) {
                    if (sCIPCPD == "N") {
                        IDH_CSITPR oCIP = new IDH_CSITPR();
                        if (sNewICode == null || sNewICode.Length == 0) {
                            sNewICode = getDFValue( "OITM", "ItemCode").ToString();
                        }
                        if (sNewICode != null && sNewICode.Length > 0 && sICode != null && sICode.Length > 0) {
                            oCIP.doCopyItemCIP(sICode, sNewICode, sIDesc, sBPCd, sAddress, sSupplier);
                            saWPFlagSplit[8] = "Y";
                        }
                    }
                    saWPFlagSplit[9] = sSupplier;
                }
            }
          //  ItemProfileCIPSIP obj;

            //WR1_FR2Forms.idh.forms.fr2.prices.ItemProfileCIPSIP oOOForm = new WR1_FR2Forms.idh.forms.fr2.prices.ItemProfileCIPSIP(null, oForm.UniqueID, null);
            //oOOForm.ItemCode = getDFValue(oForm, "OITM", "ItemCode");
            //oOOForm.ItemDesc = getDFValue(oForm, "OITM", "ItemName");
            //oOOForm.ItemGroup = getDFValue(oForm, "OITM", "ItmsGrpCod");
            //oOOForm.Supplier = sSupplier;
            //oOOForm.Customer = sBPCd;
            //oOOForm.Address = sAddress;

            //oOOForm.doShowModal(oForm);

            return true;
        }
        public bool doCreateWasteItem(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                if (_ItemCode == string.Empty) {
                    string sItemName = "";
                    if (bManualItemCode == true && getUFValue("IDH_ITEMCD").ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Item Code missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Item Code") });
                        BubbleEvent = false;
                        return true;
                    }
                    if (bManualItemCode == true && doValidateItembyCode(ref sItemName) == true) {
                        com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage(("ERPKEXIS"), new string[] { getUFValue("IDH_ITEMCD").ToString() });
                        BubbleEvent = false;
                        return true;
                    } else if (getUFValue("IDH_ITEMNM").ToString().Trim() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Item Description missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Item Description") });
                        BubbleEvent = false;
                        return true;
                    }
                    try {
                        SAPbobsCOM.Items oItem = (SAPbobsCOM.Items)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
                        if (bManualItemCode)
                            oItem.ItemCode = getUFValue("IDH_ITEMCD").ToString();

                        oItem.ItemName = getUFValue("IDH_ITEMNM").ToString();
                        oItem.ForeignName = getUFValue("IDH_FRNNAM").ToString();
                        oItem.Valid = SAPbobsCOM.BoYesNoEnum.tYES;

                        if (getUFValue("IDH_STKITM").ToString() == "Y")
                            oItem.InventoryItem = SAPbobsCOM.BoYesNoEnum.tYES;
                        else
                            oItem.InventoryItem = SAPbobsCOM.BoYesNoEnum.tNO;

                        if (getUFValue("IDH_PURITM").ToString() == "Y")
                            oItem.PurchaseItem = SAPbobsCOM.BoYesNoEnum.tYES;
                        else
                            oItem.PurchaseItem = SAPbobsCOM.BoYesNoEnum.tNO;

                        if (getUFValue("IDH_RBTITM").ToString() == "Y")
                            oItem.UserFields.Fields.Item("U_IDHREB").Value = "Y";
                        else
                            oItem.UserFields.Fields.Item("U_IDHREB").Value = "N";

                        if (getUFValue("IDH_TMPITM").ToString() == "Y")
                            oItem.UserFields.Fields.Item("U_WPTemplate").Value = "Y";
                        else
                            oItem.UserFields.Fields.Item("U_WPTemplate").Value = "N";


                        int iResult = oItem.Add();
                        if (iResult != 0) {
                            BubbleEvent = false;
                            string sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
                            //DataHandler.INSTANCE.doSystemError("Stocktransfer Error - " + sResult, "Error processing the Wharehouse to Wharehouse stock transfer.");
                            com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Item Add Error - " + sResult + " Error while adding item.", "ERSYBPAD",
                                        new string[] { "Item", sResult });
                        } else {
                            if (bManualItemCode)
                                _ItemCode = oItem.ItemCode;
                            else
                                _ItemCode = DataHandler.INSTANCE.SBOCompany.GetNewObjectKey();//this line is yet to verify by using a db having BP code serice
                            string msg = "Item successfully added: " + _ItemCode;
                            com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(msg, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                            doDisableItemFields();
                        }
                    } catch (Exception Ex) {
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EREXGEN", null);
                        return false;
                    }
                }
                if (_ItemCode != string.Empty) {
                    doSaveValidationValues();
                }
            }
            return true;

        }
        private bool doSaveValidationValues() {
            try {
                //moCreateWasteItemValidation.doClearBuffers();
              //  moCreateWasteItemValidation.doReLoadData();
              //  moCreateWasteItemValidation.first();
                ValidationControls obj;
                string sValidationCode;
                string UF, sValue;
                int iresult = 0;
                foreach (System.Collections.DictionaryEntry entry in clsValidatioControls) {
                    //entry.Value
                    //entry.Key ;
                    sValidationCode = (string)entry.Key;
                    obj = (ValidationControls)entry.Value;
                    iresult = moCreateWasteItemValidation.getData(IDH_WITMVLD._EnqId + "='" + _EnquiryID + "' AND " + IDH_WITMVLD._ItemCode + "='" + _ItemCode + "' AND " + IDH_WITMVLD._WstGpCd + "='" + _WasteGroupCode + "' AND " + IDH_WITMVLD._ValidCd + "='" + sValidationCode + "'", "Code");
                    if (iresult != 0) {
                        for (int ifields = 0; ifields <= obj.ControlID.Count - 1; ifields++) {
                            UF = obj.ControlID[ifields].ToString();
                            sValue = getUFValue(UF).ToString();
                            if (moCreateWasteItemValidation.U_EnqId == _EnquiryID && moCreateWasteItemValidation.U_ItemCode == _ItemCode
                              && moCreateWasteItemValidation.U_WstGpCd == _WasteGroupCode && moCreateWasteItemValidation.U_ValidCd == sValidationCode) {
                                if (UF.EndsWith("1"))
                                    moCreateWasteItemValidation.U_Value1 = sValue;
                                else if (UF.EndsWith("2"))
                                    moCreateWasteItemValidation.U_Value2 = sValue;
                            }
                        }

                    } else {
                        moCreateWasteItemValidation.doAddEmptyRow(true, true, false);
                        moCreateWasteItemValidation.U_EnqId = _EnquiryID;
                        moCreateWasteItemValidation.U_ItemCode = _ItemCode;
                        moCreateWasteItemValidation.U_ValidCd = sValidationCode;
                        moCreateWasteItemValidation.U_WstGpCd = _WasteGroupCode;
                        for (int ifields = 0; ifields <= obj.ControlID.Count - 1; ifields++) {
                            UF = obj.ControlID[ifields].ToString();
                            sValue = getUFValue(UF).ToString();
                            if (UF.EndsWith("1"))
                                moCreateWasteItemValidation.U_Value1 = sValue;
                            else if (UF.EndsWith("2"))
                                moCreateWasteItemValidation.U_Value2 = sValue;
                        }
                    }
                    moCreateWasteItemValidation.doProcessData();
                }
                return true;
            } catch (Exception Ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EREXGEN", null);
                return false;
            }
        }


        private bool doHandleWasteComboEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)SBOForm.Items.Item("IDH_WSTGRP").Specific;
                if (oCombo.Selected == null) {
                    return true;
                }
                string sWasteCode = oCombo.Selected.Value;

            }
            return true;
        }
        #endregion

        #region doFillCombos
        private void doFillCombos() {
            try {
                //FillCombos.FillCountry(Items.Item("IDH_CARDTY").Specific);
                SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)SBOForm.Items.Item("IDH_WSTGRP").Specific;
                //string sDfltCountry = Config.INSTANCE.getParameterWithDefault("DFTCNTRY", "GB");
                doFillCombo(oCombo, "[@ISB_WASTEGRP]", "U_WstGpCd", "U_WstGpNm", null, null, null, null);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", new string[] { com.idh.bridge.Translation.getTranslatedWord("Waste Group") });
            }
        }
        #endregion

        #region Methods
        private void doDisableItemFields() {
            try {
                if (sDefaultNewControl != string.Empty)
                    doSetFocus(sDefaultNewControl);
                EnableItem(false, "IDH_ITEMNM");
                EnableItem(false, "IDH_ITEMCD");
                EnableItem(false, "IDH_FRNNAM");
            } catch (Exception Ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EREXGEN", null);
            }
        }
        private bool doValidateItembyCode(ref string sCardName) {
            sCardName = Config.INSTANCE.doGetItemDescription(getUFValue("IDH_ITEMCD").ToString());
            if (sCardName == string.Empty)
                return false;
            else {
                _ItemCode = getUFValue("IDH_ITEMCD").ToString();
                return true;
            }
        }
        #endregion
    }
}
