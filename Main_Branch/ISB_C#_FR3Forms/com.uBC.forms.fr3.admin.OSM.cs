﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using com.idh.dbObjects.User;
using com.uBC.utils.Dynamic;
using com.idh.dbObjects.User.Shared;

namespace com.uBC.forms.fr3.admin {
    public class OSM : com.uBC.forms.fr3.dbGrid.Base
    {

        public OSM(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm)
        {
            mbIsSearch = false;
        }
        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos)
        {
            com.idh.forms.oo.FormController owForm = new idh.forms.oo.FormController("IDH_AOSM", sParMenu, iMenuPos, "uBC_AOSM.srf", false, true, false,
                "OSM Admin", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        protected override void doInitialize()
        {
            moDBObject = new IDH_FFIELDS();
        }

        public override void doBeforeLoadData()
        {
            base.doBeforeLoadData();
        }
        public override void doFinalizeShow()
        {
            doFillGridCombos();
            base.doFinalizeShow();
        }

        public override void doSetListFields()
        {
            moAdminGrid.doAddListField(IDH_FFIELDS._PopT, "PopulationType", false, com.idh.dbObjects.User.IDH_FFIELDS.ControlWidth, com.idh.controls.ListFields.LISTTYPE_COMBOBOX, null, -1, SAPbouiCOM.BoLinkedObject.lf_None,
                false, true, SAPbouiCOM.BoColumnSumType.bst_None, SAPbouiCOM.BoColumnDisplayType.cdt_Edit);
            moAdminGrid.doAddListField(IDH_FFIELDS._oType, "SAP Data Type", false, com.idh.dbObjects.User.IDH_FFIELDS.ControlWidth, com.idh.controls.ListFields.LISTTYPE_COMBOBOX, null, -1, SAPbouiCOM.BoLinkedObject.lf_None,
                false, true, SAPbouiCOM.BoColumnSumType.bst_None, SAPbouiCOM.BoColumnDisplayType.cdt_Edit);
            moAdminGrid.doAddListField(IDH_FFIELDS._Type, "SAP Item Type", false, com.idh.dbObjects.User.IDH_FFIELDS.ControlWidth, com.idh.controls.ListFields.LISTTYPE_COMBOBOX, null, -1, SAPbouiCOM.BoLinkedObject.lf_None,
                false, true, SAPbouiCOM.BoColumnSumType.bst_None, SAPbouiCOM.BoColumnDisplayType.cdt_Edit);

            //Ignore the rest

            for (int i = 0; i < moDBObject.getColumnCount(); i++) {
                com.idh.dbObjects.strct.DBFieldInfo oFieldInfo = moDBObject.getFieldInfo(i);
                moAdminGrid.doAddListFieldNotPinned(oFieldInfo.FieldName, oFieldInfo.Description, false, 0, null, null);
            }

            base.doSetListFields();
        }
        private void doFillGridCombos()
        {
            SAPbouiCOM.ComboBoxColumn oCombo;

            /*
             * Population Parameters Combo
             */
            int iIndex = this.moAdminGrid.doIndexFieldWC(IDH_FFIELDS._PopT);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Value;

            foreach (String sType in SBOFFItem.getPopulationTypesAsStringArray())
            {
                oCombo.ValidValues.Add(sType, "");

            }

            /*
             * SAP ItemTypes Combo
             */
            iIndex = moAdminGrid.doIndexFieldWC(IDH_FFIELDS._oType);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Value;

            foreach (String sDType in LayoutFF.getBoDataTypesAsStringArray())
            {
                oCombo.ValidValues.Add(sDType, "");
            }

            /*
             * SAP ItemType Combo
             */
            iIndex = moAdminGrid.doIndexFieldWC(IDH_FFIELDS._Type);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Value;

            foreach (String sIType in SBOFFItem.getBoFormItemsTypesAsStringArray())
            {
                oCombo.ValidValues.Add(sIType, "");
            }
        }
    }
}
 