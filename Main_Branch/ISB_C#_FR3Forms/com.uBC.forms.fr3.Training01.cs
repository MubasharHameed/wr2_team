﻿using System.IO;
using System.Collections;
using System;

using IDHAddOns.idh.controls;
using IDHAddOns;
using com.idh.bridge.data;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.idh.dbObjects;
using com.idh.controls;
using com.idh.dbObjects.strct;
using com.idh.dbObjects.User;

using SAPbouiCOM;

namespace com.uBC.forms.fr3 {
    public class Training01 : com.idh.forms.oo.Form {
        private bool mbSkipFormSettings = Config.INSTANCE.getParameterAsBool("FORMSET", false);
        public bool SkipFormSettings {
            set { mbSkipFormSettings = value; }
            get { return mbSkipFormSettings; }
        }
        
        private IDH_TRAIN01 moTrainDB;
        protected DBOGrid moLinesGrid;

        public Training01(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
            doInitialize();
            doSetHandlers();
        }
        
        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_TRAIN01", sParMenu, 80, "training01_1.srf", false, true, false, "FR3 Training001", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);

            ////Sets the History table and enables the ChangeLog Menu option
            ////This will also Add the DBDataSource when the form gets loaded
            owForm.doEnableHistory("@IDH_TRAIN01");

            return owForm;
        }

        protected void doInitialize() {
            moTrainDB = new IDH_TRAIN01(IDHForm, SBOForm);
            moTrainDB.MustLoadChildren = true;

            moTrainDB.AutoAddEmptyLine = false;
            moTrainDB.Lines.AutoAddEmptyLine = true;
        }

#region FormOpenCreateFunctions
        /** 
         * This Function will be called by the controller to allow the class to last minute Form Layout adjustments before it gets displayed
         * All changes made in hete will be cached if this is a cached form, the method will not be called again once the form has benn cached.
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            /*
             * The unique ID of an item.
             * When setting this property, navigation buttons are added that control moving from one 
             * record to the next for the table or query that is bound to the specified field.
             */
            DataBrowser.BrowseBy = "IDHCode";

            /*
             * The range of modes available to the form. 
             * You can use one mode only, more than one mode (using OR), or all modes available in the application.
             * In SAP Business One, forms can appear in several modes that determine the type of actions users can 
             * perform on the form. For example, Add mode is used to add records to a form, while OK mode is used to display the form information.
             *      fm_FIND_MODE 0 Find mode
             *      fm_OK_MODE 1 OK mode
             *      fm_UPDATE_MODE 2 Update mode
             *      fm_ADD_MODE 3 Add mode
             *      fm_VIEW_MODE 4 View mode
             *      fm_PRINT_MODE 5 Print mode
             *      
             *      -1 is All modes
             */
            SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_All;

            /*
             * Indicates whether the application manages your add-on form modes automatically.
             * When set to true, the application controls the form mode when the form is loaded 
             * and manages the status of items and menus (relevant to a specific mode) in mode changes.
             * Specify the modes that are to be managed on the form with the SupportedModes property.
             */
            AutoManaged = true;

            ///*
            // * Form Mode when the Form is opened
            // *      fm_FIND_MODE 0 Find mode
            // *      fm_OK_MODE 1 OK mode
            // *      fm_UPDATE_MODE 2 Update mode
            // *      fm_ADD_MODE 3 Add mode
            // *      fm_VIEW_MODE 4 View mode
            // *      fm_PRINT_MODE 5 Print mode
            // */
            //Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE;
        }

        /** 
         * Do the final form steps to show before loaddata
         */
        public override void doBeforeLoadData() {
            try {
                Title = IDHForm.gsTitle;

                moLinesGrid = new DBOGrid(IDHForm, SBOForm, "LINESGRID", moTrainDB.Lines);
                moLinesGrid.doSetDeleteActive(true);
                moLinesGrid.doAddEditLine(true);

                doSetGridHandlers();

                doGridLayout();

                moLinesGrid.doApplyRules();
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }
        }

        /** 
         * Load the Form Data
         */
        public override void doLoadData() {
        
        }

        /** 
         * Do the final form steps to show after loaddata
         */
        public override void doFinalizeShow() {
            base.doFinalizeShow();

            /*
             * Form Mode when the Form is opened
             *      fm_FIND_MODE 0 Find mode
             *      fm_OK_MODE 1 OK mode
             *      fm_UPDATE_MODE 2 Update mode
             *      fm_ADD_MODE 3 Add mode
             *      fm_VIEW_MODE 4 View mode
             *      fm_PRINT_MODE 5 Print mode
            */
            Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE;
        }

        /**
         * Set the Grid Event Handlers
         */
        protected virtual void doSetGridHandlers() {
        }

        /**
         * Set the Grid Layout using the Formsettings if any or use the List Fields
         */
        protected virtual void doGridLayout() {
            moLinesGrid.doAddListField(IDH_TRAIN01_LNS._HDRCode, "Header No", false, 100, ListFields.LISTTYPE_EDITBOX, null, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true, SAPbouiCOM.BoColumnSumType.bst_None, SAPbouiCOM.BoColumnDisplayType.cdt_Edit);
            moLinesGrid.doAddListField(IDH_TRAIN01_LNS._Code, "Line No.", false, -1, ListFields.LISTTYPE_EDITBOX, null, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true, SAPbouiCOM.BoColumnSumType.bst_None, SAPbouiCOM.BoColumnDisplayType.cdt_Edit);
            moLinesGrid.doAddListField(IDH_TRAIN01_LNS._ItemCode, "Item Code", true, 50, ListFields.LISTTYPE_EDITBOX, null, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true, SAPbouiCOM.BoColumnSumType.bst_None, SAPbouiCOM.BoColumnDisplayType.cdt_Edit);
            moLinesGrid.doAddListField(IDH_TRAIN01_LNS._ItemDesc, "Item Desc.", true, 50, ListFields.LISTTYPE_EDITBOX, null, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true, SAPbouiCOM.BoColumnSumType.bst_None, SAPbouiCOM.BoColumnDisplayType.cdt_Edit);
            moLinesGrid.doAddListField(IDH_TRAIN01_LNS._QTY, "Quantity", true, 50, ListFields.LISTTYPE_EDITBOX, null, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true, SAPbouiCOM.BoColumnSumType.bst_None, SAPbouiCOM.BoColumnDisplayType.cdt_Edit);
            moLinesGrid.doAddListField(IDH_TRAIN01_LNS._Price, "Price", true, 50, ListFields.LISTTYPE_EDITBOX, null, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true, SAPbouiCOM.BoColumnSumType.bst_None, SAPbouiCOM.BoColumnDisplayType.cdt_Edit);
            moLinesGrid.doAddListField(IDH_TRAIN01_LNS._Total, "Total", false, 50, ListFields.LISTTYPE_EDITBOX, null, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true, SAPbouiCOM.BoColumnSumType.bst_None, SAPbouiCOM.BoColumnDisplayType.cdt_Edit);
            moLinesGrid.doAddListField(IDH_TRAIN01_LNS._Name, "Name", false, 0, ListFields.LISTTYPE_EDITBOX, null, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true, SAPbouiCOM.BoColumnSumType.bst_None, SAPbouiCOM.BoColumnDisplayType.cdt_Edit);
            moLinesGrid.doSynchDBandGridFieldPos();
        }

#endregion

#region Events
        /**
         * Register all your event Handlers in here.
         * Handler_ALL_EVENTS 0 All events, for use when filtering events 
         * Handler_ITEM_PRESSED 1 The main mouse button was clicked on one of the following: 
         * 		Tab 
         * 		Button 
         * 		Option button 
         * 		Check box (standalone or within a cell)
         * 		This event occurs when a mouse is released within an item, that is, mouse up.
         * Handler_KEY_DOWN 2 A key was pressed. 
         * Handler_GOT_FOCUS 3 An item received focus. 
         * Handler_LOST_FOCUS 4 An item lost focus. 
         * Handler_LOST_FOCUS_CHANGED 4 An item lost focus with the value changed
         * Handler_COMBO_SELECT 5 A value was selected in a combo box. 
         * Handler_CLICK 6 The main mouse button was clicked on an item, that is, mouse down. 
         * Handler_DOUBLE_CLICK 7 The main mouse button was double-clicked on an item. 
         * Handler_MATRIX_LINK_PRESSED 8 A link arrow in a matrix was pressed. 
         * Handler_MATRIX_COLLAPSE_PRESSED 9 A matrix list was collapsed or expanded. 
         * Handler_VALIDATE 10 An item lost focus and validation is required.
         * Handler_VALIDATE_CHANGED 10 An item lost focus and validation is required - only when the data has changed.
         * Handler_MATRIX_LOAD 11 Data was loaded from the database into a matrix data source. 
         * 		This event occurs for user-defined matrix objects only. If a form contains two matrix objects, the system generates two events.
         * Handler_DATASOURCE_LOAD 12 Data was loaded from the GUI into a matrix data source. 
         * 		This event occurs once per matrix. If a form has two matrix objects, the system generates two events.
         * Handler_FORM_LOAD 16 A form was opened (FormDataEvent). 
         * Handler_FORM_UNLOAD 17 A form was closed. 
         * Handler_FORM_ACTIVATE 18 A form received focus. 
         * Handler_FORM_DEACTIVATE 19 A form lost focus. 
         * Handler_FORM_CLOSE 20 A form is about to be closed. 
         * Handler_FORM_RESIZE 21 A form has been resized. 
         * Handler_FORM_KEY_DOWN 22 A key was pressed when no form had the focus. 
         * Handler_FORM_MENU_HILIGHT 23 A form is modifying the status of toolbar items, that is, enabling and disabling icons. 
         * 		By default, these events are not thrown. You can activate these events by setting an event filter.
         * Handler_PRINT 24 A print preview was requested for a report or document (PrintEvent). 
         * 		This event lets you exit from the application print operation and use your own printing.
         * Handler_PRINT_DATA 25 A print preview was requested for a report (ReportDataEvent).  
         * 		This event lets you get the report data in XML format.
         * Handler_CHOOSE_FROM_LIST 27 A ChooseFromList event occurred, as follows:  
         * 		The Before event occurs before the ChooseFromList form is displayed. If the BubbleEvent parameter is set to False, the form is not displayed. 
         * 		The After event occurs after the user makes a selection or chooses Cancel.		 
         * Handler_RIGHT_CLICK 28 The right mouse button was clicked on an item (RightClickEvent). 
         * Handler_MENU_CLICK 32 The main mouse button was released on a menu item without submenus. 
         * 		For future use.
         * Handler_FORM_DATA_ADD 33 A record in a business object was added (FormDataEvent). 
         * Handler_FORM_DATA_UPDATE 34 A record in a business object was updated (FormDataEvent). 
         * Handler_FORM_DATA_DELETE 35 A record in a business object was deleted (FormDataEvent). 
         * Handler_FORM_DATA_LOAD 36 A record in a business object was loaded -- via browse, link button, or find (FormDataEvent). 
         * Handler_PICKER_CLICKED 37 A Picker event occurred, as follows:  
         * 		The Before event occurs before the Picker form is displayed. If the BubbleEvent parameter is set to False, the picker is not displayed. 
         * 		The After event occurs after the user makes a selection or chooses Cancel.
         * Handler_GRID_SORT 38 A grid column was sorted, either by a user clicking the column title or an add-on calling the Sort method of the Grid object. 
         * Handler_Drag 39 The item was dragged and dropped at the position you want it to appear. 
         * Handler_PRINT_LAYOUT_KEY 80 A print or print preview was requested for an add-on form, and the layout needs the key of the add-on form 		
         * 
         * To Add a Item PressEvent for a specific Control use 
         * addHandler_ITEM_PRESSED("ITEMID", new ev_Item_Event(EventFunction));
         *      e.g. addHandler_ITEM_PRESSED("uBCCardSR", new ev_Item_Event(doHandleCardCodeSearch));
         *      
         *           public bool doHandleCardCodeSearch(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
         *               return false;
         *           }
         *           
         *  The following Special Event Handlers can be set when using the SBO default where Button 1 is OK 
         *  Handler_Button_Ok - When the Form is in Ok or View Mode
         *  Handler_Button_Find - When the Form is in Find Mode
         *  Handler_Button_Add - When the Form is in Add Mode
         *  Handler_Button_Update - When the Form is in Update Mode
         *   
         * The Menu Events
         * Handler_Menu_NAV_ADD
		 * Handler_Menu_NAV_FIND
		 * Handler_Menu_NAV_FIRST
		 * Handler_Menu_NAV_LAST
		 * Handler_Menu_NAV_NEXT
         * Handler_Menu_NAV_PREV
         * --------> If the BubbleEvent is not set to False the normal Overridable Function doMenuEvent will be called 
         *           doMenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent)
         *
         * Special Handler that will be called if this form was opened as a Dialog (Opened from another Form)
         * Handler_DialogButton1Return - When Button 1 was pressed irrespective of the Form Mode
         * Handler_DialogOkReturn - When Button 1 was Pressed and Form was in Ok/View Mode
         * Handler_DialogCancelReturn = When Button 2 was Pressed
         * --------> This will only be called if all the other Button 1 /2 events where called including the Overridable 
         *           Function doButtonID1 and the Bubble event is still true.
         */
        private void doSetHandlers() {
            Handler_Button_Add = doAddUpdateEvent_Handler;
            Handler_Button_Update = doAddUpdateEvent_Handler;
            Handler_Button_Find = doFindEvent_Handler;

            Handler_Menu_NAV_ADD = doMenuAddEvent_Handler;
            Handler_Menu_NAV_FIND = doMenuFindEvent_Handler;

            Handler_Menu_NAV_FIRST = doMenuBrowseEvents_Handler;
            Handler_Menu_NAV_LAST = doMenuBrowseEvents_Handler;
            Handler_Menu_NAV_NEXT = doMenuBrowseEvents_Handler;
            Handler_Menu_NAV_PREV = doMenuBrowseEvents_Handler;

            Handler_FormMode_Changed = doModeChange_Handler;
        }
#endregion 

#region ItemEventHandlers
        /** 
         * Update the Row Header Ref
         */
        private void doUpdateLineHeaderRef() {
            moTrainDB.Lines.setValueForAllRows(IDH_TRAIN01_LNS._HDRCode, moTrainDB.Code, true);
        }

        public bool doAddUpdateEvent_Handler(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                Freeze(true);
                try {
                    if (doAutoSave(null, moTrainDB.AutoNumKey)) {
                        doUpdateLineHeaderRef();
                    
                        if ( moLinesGrid.doProcessData() ) {
                            Mode = BoFormMode.fm_OK_MODE;
                            BubbleEvent = false;
                        }
                    }
                } catch (Exception ex) {
                    throw (ex);
                } finally {
                    Freeze(false);
                }
            }
            return true;
        }

        public bool doFindEvent_Handler(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                Freeze(true);
                try {
                    string sHeader = (string)getFormDFValue(IDH_TRAIN01._Code, true);
                    if (!moTrainDB.getByKey(sHeader)) {
                        moLinesGrid.doAddEditLine(true);
                        moLinesGrid.doApplyRules();
                        Mode = BoFormMode.fm_FIND_MODE;
                        BubbleEvent = false;
                        return false;
                    }
                } catch (Exception ex) {
                    throw (ex);
                } finally {
                    Freeze(false);
                }
            }
            return true;
        }

        public bool doModeChange_Handler(SAPbouiCOM.BoFormMode oSBOMode) {
            if (oSBOMode == BoFormMode.fm_ADD_MODE) {
                doSwitchToEdit();

                doClearFormUFValues();

                moTrainDB.doClearBuffers();
                moTrainDB.doAddEmptyRow(true, true);
                moLinesGrid.doApplyRules();
            } else if (oSBOMode == BoFormMode.fm_FIND_MODE) {
                doSwitchToFind();

                doClearFormDFValues();
                doClearFormUFValues();
            }

            return true;
        }

        private void doSwitchToFind() {
            setFocus("IDHCode");

            string[] oExl = { "IDHCode" };
            DisableAllEditItems(oExl);
        }

        private void doSwitchToEdit() {
            string[] oExl = { "IDHCode" };
            EnableAllEditItems(oExl);

            setFocus("IDHCardCod");
        }

#endregion 

#region MenuEventHandlers
        public bool doMenuAddEvent_Handler(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                Mode = BoFormMode.fm_ADD_MODE;
            }
            return true;
        }

        public bool doMenuFindEvent_Handler(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                Mode = BoFormMode.fm_FIND_MODE;
            }
            return true;
        }

        public bool doMenuBrowseEvents_Handler(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                Freeze(true);
                try {
                    doClearFormUFValues();
                } catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHANB", null);
                } finally {
                    Freeze(false);
                }
            } 
            else {
                moTrainDB.doLoadChildren();
                if (moTrainDB.Lines.Count == 0 ) {
                    moLinesGrid.doAddEditLine(true);
                }
                moLinesGrid.doApplyRules();
            }
            return true;
        }
#endregion

#region OldEventHandlers
        /**
         * Most of these are replaced with Handlers as above... only use these if there is really no Event or in special events
         */

        /*
         * Handle all the Menu Events.
         * Return True if the Event must be handled by the other Objects
         */
        public override bool doMenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * The Event Handler that will receive all Activated Events for this Form
         * Return True if the Event must be handled by the other Objects
         */
        public override bool doItemEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Send By Custom Controls
         */
        public override bool doCustomItemEvent(ref IDHAddOns.idh.events.Base pVal) {
            return true;
        }

        /*
         * Handle the Request for a Help File
         */
        public override bool doHelpFile(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Handles the right click event
         */
        public override bool doRightClickEvent(ref SAPbouiCOM.ContextMenuInfo pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Handle the Button One Pressed Event
         */
        public override void doButtonID1(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        }

        /* 
         * Handle Button Two Presssed Event
         */
        public override void doButtonID2(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        }

        /* 
         * Return As Canceled If this was Opened as a Dialog from another form
         */
        public override void doReturnCanceled(string sModalFormType) {
        }

        ///* 
        // * Return as Ok If this was Opened as a Dialog from another form setting the Data in the Global Shared Buffer
        // * This will be called from the Conroller after all the other Button 1 Handlers and the BubbleEvent is still True.
        // */
        //public override void doReturnFromModalShared(bool bState) {
        //}

        //*** Return As Normal
        public override void doReturnNormal() {
        }

        /* 
         * Handle the Cancel Event when a Dialog returns to this form.
         */
        public override void doHandleModalCanceled(string sModalFormType) {
        }

        /* 
         * Handle the Return from a Dialog called from this Form with the data in oData
         */
        public override void doHandleModalBufferedResult(ref object oData, string sModalFormType, string sLastButton) {
        }

        /* 
         * Handle the Return from a Dialog called from this Form with the data in the Global Shared Buffer
         */
        public override void doHandleModalResultShared(string sModalFormType, string sLastButton) {
        }
#endregion
    }
}

