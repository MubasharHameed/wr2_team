﻿using System.IO;
using System.Collections;
using System;

using IDHAddOns.idh.controls;
using IDHAddOns;
using com.idh.controls;
using com.idh.bridge.data;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.idh.dbObjects.User;
using com.uBC.utils;
using com.uBC.data;
using SAPbouiCOM;

using com.idh.bridge.action;
using com.idh.utils;

namespace com.uBC.forms.fr3.popup {
    public class ChequeNumber : com.idh.forms.oo.Form {
        public bool mbDoClose = false;
        public string ChequeNum {
            set { setUFValue(FIELD_uBCCHQNUM, value); }
            get { return (string)getUFValue(FIELD_uBCCHQNUM); }
        }

        private IDH_DISPROW moDOR;
        public IDH_DISPROW DOR {
            set { moDOR = value; }
            get { return moDOR; }
        }

        private SAPbobsCOM.Payments moPayment;
        public SAPbobsCOM.Payments Payment {
            set { moPayment = value; }
            get { return moPayment; }
        }

        public ChequeNumber(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
            doSetHandlers();
        }
        
        //public new IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu) {
        public new static IDHAddOns.idh.forms.Base doRegisterFormClass() {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_CHQNUM", null, 80, "ChequeNumber.srf", false, true, false, "Cheque Number", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

#region FormOpenCreateFunctions
        /** 
         * This Function will be called by the controller to allow the class to last minute Form Layout adjustments before it gets displayed
         * All changes made in hete will be cached if this is a cached form, the method will not be called again once the form has benn cached.
         */
        public const string FIELD_uBCCHQNUM = "uBCCHQNUM";

        public override void doCompleteCreate(ref bool BubbleEvent) {
            doAddUF(FIELD_uBCCHQNUM, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, false, false);

            SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_Ok; 
            AutoManaged = false;
            Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
        }

        /** 
         * Do the final form steps to show before loaddata
         */
        public override void doBeforeLoadData() {
        }

        /** 
         * Load the Form Data
         */
        public override void doLoadData() {
        }

        /** 
         * Do the final form steps to show after loaddata
         */
        public override void doFinalizeShow() {
            base.doFinalizeShow();
        }

        public void doModalWait() {

            try {
                while (!mbDoClose) {
                    System.Threading.Thread.Sleep(1000);
                }
            } catch (Exception) {
            }

        }
#endregion

#region Events
        private void doSetHandlers() {
            Handler_Button_Ok = new ev_Item_Event(doButtonOKEvent);
        }
#endregion 

#region ItemEventHandlers
        public bool doButtonOKEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                mbDoClose = true;
                SBOForm.Visible = false;
                //if (moDOR != null) {
                //    moDOR.doCompleteChequePayment(moPayment, Conversions.ToInt(ChequeNum));
                //}
                BubbleEvent = false;
            }
            return true;
        }
#endregion 

#region OtherLogic
#endregion

#region OldEventHandlers
        /**
         * Most of these are replaced with Handlers as above... only use these if there is really no Event or in special events
         */

        /*
         * Handle all the Menu Events.
         * Return True if the Event must be handled by the other Objects
         */
        public override bool doMenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * The Event Handler that will receive all Activated Events for this Form
         * Return True if the Event must be handled by the other Objects
         */
        public override bool doItemEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Send By Custom Controls
         */
        public override bool doCustomItemEvent(ref IDHAddOns.idh.events.Base pVal) {
            return true;
        }

        /*
         * Handle the Request for a Help File
         */
        public override bool doHelpFile(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Handles the right click event
         */
        public override bool doRightClickEvent(ref SAPbouiCOM.ContextMenuInfo pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Handle the Button One Pressed Event
         */
        public override void doButtonID1(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        }

        /* 
         * Handle Button Two Presssed Event
         */
        public override void doButtonID2(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        }

        /* 
         * Return As Canceled If this was Opened as a Dialog from another form
         */
        public override void doReturnCanceled(string sModalFormType) {
        }

        ///* 
        // * Return as Ok If this was Opened as a Dialog from another form setting the Data in the Global Shared Buffer
        // * This will be called from the Conroller after all the other Button 1 Handlers and the BubbleEvent is still True.
        // */
        //public override void doReturnFromModalShared(bool bState) {
        //}

        //*** Return As Normal
        public override void doReturnNormal() {
        }

        /* 
         * Handle the Cancel Event when a Dialog returns to this form.
         */
        public override void doHandleModalCanceled(string sModalFormType) {
        }

        /* 
         * Handle the Return from a Dialog called from this Form with the data in oData
         */
        public override void doHandleModalBufferedResult(ref object oData, string sModalFormType, string sLastButton) {
        }

        /* 
         * Handle the Return from a Dialog called from this Form with the data in the Global Shared Buffer
         */
        public override void doHandleModalResultShared(string sModalFormType, string sLastButton) {
        }
#endregion

#region OldSearchForms
#endregion
    }
}

