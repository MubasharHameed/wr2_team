﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.dbObjects.User;
using IDHAddOns.idh.controls;
using com.uBC.utils;
using com.idh.controls;
using com.idh.bridge.utils;

namespace com.uBC.forms.fr3.admin {
    public class BPTransactionCodesLink : com.uBC.forms.fr3.dbGrid.Base {
        private string msBPCode = null;
        public string BPCode {
            set { msBPCode = value; }
            get { return msBPCode; }
        }

        public BPTransactionCodesLink(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
                mbIsSearch = false;
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDHBPTRLNK", sParMenu, iMenuPos, "admin.srf", false, true, false, "BP Transaction Code Link", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        protected override void doInitialize() {
            moDBObject = new IDH_BPTRLNK();
        }

#region FormOpenCreateFunctions
        /*
         * Do Complete the Form
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            base.doCompleteCreate(ref BubbleEvent);
        }

       /* 
        * Do the final form steps to show before loaddata
        */
        public override void doBeforeLoadData() {
            base.doBeforeLoadData();

            if (msBPCode != null && msBPCode.Length > 0) {
                moDBObject.doAddDefaultValue(IDH_BPTRLNK._BPCd, msBPCode);
            }
        }

        /* 
         * Load the Form Data
         */
        public override void doLoadData() {
            setUFValue("uBC_BPCD", msBPCode);

            base.doLoadData();
            doFillCombos();
        }

        /*
         * Set the Filter Fields
         */
        public override void doSetFilterFields() {
            moAdminGrid.doAddFilterField("uBC_BPCD", IDH_BPTRLNK._BPCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30);
        }

        /*
         * Set the List Fields
         */
        public override void doSetListFields() {
            moAdminGrid.doAddListField(IDH_BPTRLNK._Code, "Code", false, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_BPTRLNK._Name, "Name", true, 0, ListFields.LISTTYPE_IGNORE, null, -1);

            //"SRC*IDHCSRCH(BP)[IDH_BPCOD;IDH_TYPE=SC][CARDCODE;U_SuppNm=CARDNAME]"
            moAdminGrid.doAddListField(IDH_BPTRLNK._BPCd, "Business Partner", true, -1, "SRC*IDHCSRCH(BP)[IDH_BPCOD;IDH_TYPE=SC][CARDCODE]", null, -1);
            moAdminGrid.doAddListField(IDH_BPTRLNK._TRNCd, "Transaction Code", true, -1, ListFields.LISTTYPE_COMBOBOX, null, -1);
        }
#endregion

#region Events
        protected override void doSetHandlers() {
            base.doSetHandlers();
        }

        protected override void doSetGridHandlers() {
            base.doSetGridHandlers();

            //moAdminGrid.Handler_GRID_FIELD_CHANGED = new IDHGrid.ev_GRID_EVENTS(doFieldChangeEvent); 
        }
#endregion 

#region ItemEventHandlers
        //public bool doFieldChangeEvent(ref IDHAddOns.idh.events.Base pVal) {
        //    if (pVal.BeforeAction) {
        //    } else {
        //        IDH_TRANCD oTrnCode = ((IDH_TRANCD)moDBObject);
        //        string sTrnCode = "" +
        //        setVal(oTrnCode.U_Stock) + '-' + setVal(oTrnCode.U_WhseFrom) + '-' + setVal(oTrnCode.U_WhseTo) + '-' +
        //        setVal(oTrnCode.U_Trigger) + '-' + setVal(oTrnCode.U_MarkDoc1) + '-' + setVal(oTrnCode.U_MarkDoc2) + '-' +
        //        setVal(oTrnCode.U_MarkDoc3) + '-' + setVal(oTrnCode.U_MarkDoc4);
        //        oTrnCode.U_TrnCode = sTrnCode;
        //    }
        //    return true;
        //}

        //private string setVal(string sVal) {
        //    if (sVal.Length == 0)
        //        return "0";
        //    else
        //        return sVal;
        //}
#endregion

#region fillCombos
        private void doFillCombos(){
            SAPbouiCOM.ComboBoxColumn oCombo;

            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_BPTRLNK._TRNCd));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            FillCombos.TransactionCodeCombo(oCombo, null, null, null);
        }
#endregion
    }
}
