﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.uBC.utils.Dynamic.Structs {
    public struct Lable {
        public SAPbouiCOM.Item oItem;
        public string sCaption;
        public FiterFieldInfo oFFInfo;
    }
    public struct Control {
        public SAPbouiCOM.Item oItem;
        public SAPbouiCOM.BoFormItemTypes oItemType;
        public PopTypes oPopType;
        public string sPopPar;
    }
    public struct FiterFieldInfo {
        public string sFieldName;
        public string sDBField;
        public SAPbouiCOM.BoDataType oType;
        public string sOperator;
        public int iLenght;
        public string sVal;
        public bool bDoFilter;
    }
    public struct NextPos {
        public int iLeft;
        public int iTop;
    }
    public struct HideShow {
        public bool bHideTop;
        public bool bHideLeft;
        public bool bHideRight;
        public LPosition oLastButtonPressed;
    }
    public enum PopTypes {
        pt_TEXT,
        pt_SQL,
        pt_LOOKUP,
    }
    public enum BoDataType {
        dt_LONG_NUMBER,
        dt_SHORT_NUMBER,
        dt_QUANTITY,
        dt_PRICE,
        dt_RATE,
        dt_MEASURE,
        dt_SUM,
        dt_PERCENT,
        dt_LONG_TEXT,
        dt_SHORT_TEXT,
        dt_DATE,
    }
    public enum BoFormItemTypes {
        it_COMBO_BOX,
        it_EDITBOX,
    }
    public enum LPosition {
        pos_NONE,
        pos_TOP,
        pos_LEFT,
        pos_RIGHT,
    }
    public enum LOption {
        NORMAL,
        RESIZE,
    }
}
