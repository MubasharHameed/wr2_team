﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.bridge;
using com.idh.dbObjects.User;
using com.idh.bridge.lookups;

namespace com.uBC.utils.weighbridge {
    public class WBCommands {
        #region Member Variables
        private string msID;
        protected int miPort;
        protected string msPort;
        protected string msLogo;
        protected Weighbridge moWBridge;
        #endregion

        #region Constructor
        public WBCommands(string sID) {
            msID = sID;
            msPort = idh.bridge.lookups.Config.Parameter("WBPORT" + sID);
            if(!string.IsNullOrEmpty(msPort))
                miPort = Int32.Parse(msPort);
            moWBridge = new idh.bridge.Weighbridge(sID);
            doSetHandlers();
        }
        #endregion

        #region Methods
        private void doSetHandlers() {

        }
        private bool doNullCheck() {
            if (moWBridge == null) {
                if (msID.Equals("-1"))
                    return false;
            } else {
                doConfigure();
                if (moWBridge == null)
                    return false;
            }
            return true;
        }
        protected bool doSetWBLogo() {
            throw new NotImplementedException();
        }
        #endregion

        #region Delegates
        public delegate bool sFieldChanged(string sVal);
        public delegate bool dFieldChanged(double dVal);
        public delegate bool dtFieldChanged(DateTime dtVal);
        public delegate bool ReadContinuous();
        public delegate bool ReadACC1();
        public delegate bool ReadACC2();
        public delegate bool ReadACCB();
        protected delegate bool Configure();
        #endregion

        #region Events
        public event dFieldChanged WeightChanged;
        public event dFieldChanged Weight1Changed;
        public event dFieldChanged Weight2Changed;
        public event dFieldChanged BufferWeightChanged;
        public event sFieldChanged Serial1Changed;
        public event sFieldChanged Serial2Changed;
        public event sFieldChanged BufferSerialChanged;
        public event dtFieldChanged Weight1DateChanged;
        public event dtFieldChanged Weight2DateChanged;
        public event dtFieldChanged BufferWeightDateChanged;
        
        #endregion

        #region Handlers
        protected bool doConfigure() {
            bool bConfigure = false;
            if (msID.Equals("-1")) {
                moWBridge.doClose();
                moWBridge = null;
                bConfigure = false;
                return bConfigure;
            }
            if (moWBridge == null) {
                moWBridge = new Weighbridge(msID);
                bConfigure = true;
            } else if (!moWBridge.getWBId().Equals(msID)) {
                moWBridge.doClose();
                moWBridge.setWBId(msID);
                bConfigure = true;
            } else {
                bConfigure = true;
            }
            
            if (bConfigure) {
                string sReadOnceCmd = Config.Parameter("WBFR" + msID);
                string sReadContinuousCmd = Config.Parameter("WBCR" + msID);
                int iWBFRSS = Config.ParameterAsInt("WBFRSS" + msID);
                int iWBFRSL = Config.ParameterAsInt("WBFRSL" + msID);
                int iWBFRWS = Config.ParameterAsInt("WBFRWS" + msID);
                int iWBFRWL = Config.ParameterAsInt("WBFRWL" + msID);
                int iWBCRWS = Config.ParameterAsInt("WBCRWS" + msID);
                int iWBCRWL = Config.ParameterAsInt("WBCRWL" + msID);
                string sPort = Config.Parameter("WBPORT" + msID);
                string sBaud = Config.Parameter("WBBAUD" + msID);
                string sDataBit = Config.Parameter("WBDBIT" + msID);

                //'N-None, O-Odd, E-Even, M-Mark
                string sParity = Config.Parameter("WBPARI" + msID);
                string sStop = Config.Parameter("WBSTOP" + msID);
                string sFlowControl = Config.Parameter("WBFLOW" + msID);
                string sInMode = Config.Parameter("WBIMOD" + msID);
                string sWBDelay = Config.Parameter("WBDELA" + msID);
                int iWBDELA;

                if (string.IsNullOrEmpty(sParity))
                    sParity = "N";
                if (string.IsNullOrEmpty(sWBDelay))
                    iWBDELA = 0;
                else {
                    iWBDELA = Int32.Parse(sWBDelay);
                }
                moWBridge.doConfigPort(Int32.Parse(sPort), Int32.Parse(sBaud), Int32.Parse(sDataBit), sParity.ToCharArray().ElementAt(0), Int32.Parse(sStop), sFlowControl);
                moWBridge.setInMode(sInMode);
                moWBridge.setReadDelay(iWBDELA);
                moWBridge.doConfigReadCointinues(sReadContinuousCmd, iWBCRWS, iWBCRWL);
                moWBridge.doConfigReadOnce(sReadOnceCmd, iWBFRWS, iWBFRWL, iWBFRSS, iWBFRSL);
                return true;
            }
            return false;
        }
        protected bool doReadContinuous() {
            if (doNullCheck()) {
                try {
                    if (!moWBridge.doReadContinues())
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("CR" + ": " + moWBridge.getError(), "ERSYGEN", new string[] { "CR" + ": " + moWBridge.getError() });
                    WeightChanged(moWBridge.getWeight());
                    return true;
                } catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRWGT", null);
                }
            }
            return false;
        }
        protected bool doReadACC1() {
            if (doNullCheck()) {
                try {
                    if (!moWBridge.doReadOnce())
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("ACC1" + ": " + moWBridge.getError(), "ERSYGEN", new string[] { "ACC1" + ": " + moWBridge.getError() });
                    WeightChanged(moWBridge.getWeight());
                    Weight1Changed(moWBridge.getWeight());
                    Serial1Changed(moWBridge.getSerial());
                    Weight1DateChanged(DateTime.Now);
                    return true;
                } catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRWGT", null);
                } finally {
                    moWBridge.doClose();
                }
            }
            return false;
        }
        protected bool doReadACC2() {
            if (doNullCheck()) {
                try {
                    if (!moWBridge.doReadOnce())
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("ACC2" + ": " + moWBridge.getError(), "ERSYGEN", new string[] { "ACC2" + ": " + moWBridge.getError() });
                    WeightChanged(moWBridge.getWeight());
                    Weight2Changed(moWBridge.getWeight());
                    Serial2Changed(moWBridge.getSerial());
                    Weight2DateChanged(DateTime.Now);
                    return true;
                } catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRWGT", null);
                } finally {
                    moWBridge.doClose();
                }
            }
            return false;
        }
        protected bool doReadACCB() {
            if (doNullCheck()) {
                try {
                    if (!moWBridge.doReadOnce())
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("ACCB" + ": " + moWBridge.getError(), "ERSYGEN", new string[] { "ACCB" + ": " + moWBridge.getError() });
                    WeightChanged(moWBridge.getWeight());
                    BufferWeightChanged(moWBridge.getWeight());
                    BufferSerialChanged(moWBridge.getSerial());
                    BufferWeightDateChanged(DateTime.Now);
                    return true;

                    //                //IDH_WDTB goParent.doDateToStr()
                } catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRWGT", null);
                } finally {
                    moWBridge.doClose();
                }
            }
            return false;
        }
        #endregion

        #region Properties
        public bool IsAvailable {
            get { return Weighbridge.IsPortAvailable(miPort); }
        }
        public Weighbridge Weighbridge {
            get { return moWBridge; }
        }
        public int Port {
            get { return miPort; }
        }
        public string Logo {
            get { return msLogo; }
        }
        #endregion
    }
}
