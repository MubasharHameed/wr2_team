using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.dbObjects.User;

namespace com.uBC.utils {
   public class DOWrapper {
       private IDH_DISPORD moIDH_DISPORD;
       private IDH_DISPROW moIDH_DISPROW;
       private string msTicketNumber;
       private static long miUniqueID = 0;
       public static string msDOWrapper = "DOWrapper";
       public static readonly string sVTYPE = "VTYPE", sREG = "REG", sVEHDESC = "VEHDESC",
            sTARRE = "TARRE", sWCSCD = "WCSCD", sAVAIL = "v.U_Avail", sDRIVER = "DRIVER",
            sBRANCH = "BRANCH", sDRVNR = "DRVNR", sVEHCODE = "VEHCODE", sWR1TYPE = "WR1TYPE";

       public DOWrapper() {
           miUniqueID++;
           msTicketNumber = msDOWrapper + miUniqueID++;
           
           moIDH_DISPORD = new IDH_DISPORD();
           moIDH_DISPROW = new IDH_DISPROW();
           
       }

       #region IDH_DISPORD Methods
       public bool doAddDataRow(string sComment = "NC") {
           bool bSuccess = false;
           bSuccess = moIDH_DISPORD.doAddDataRow(sComment);
           //moIDH_DISPROW = moIDH_DISPORD.WORows;
           return bSuccess;
       }
       public double getUnCommittedRowTotals() {
           return moIDH_DISPORD.getUnCommittedRowTotals();
       }
       public bool doSetSiteAddress(BPAddress oAddress) {
           return moIDH_DISPORD.doSetSiteAddress(oAddress);
       }
       public void setOrigin(string sOriginCd) {
           moIDH_DISPORD.setOrigin(sOriginCd);
       }
       public bool hasRows() {
           return moIDH_DISPORD.hasRows;
       }
       public bool setValue(string sFieldName, object oValue, bool bCheckChanged = false) {
           return moIDH_DISPORD.setValue(sFieldName, oValue, bCheckChanged);
       }
       public bool doUpdateDataRow(string sComment = "NC") {
           return moIDH_DISPORD.doUpdateDataRow(sComment);
       }
       #endregion

       #region Instance Methods
       private bool doAddDOANDDOR() {
           if (moIDH_DISPORD != null && moIDH_DISPROW != null) {
               if (moIDH_DISPROW.TareWeight != mdTarrWeight)
                   moIDH_DISPROW.TareWeight = mdTarrWeight;
               if (moIDH_DISPROW.TrailerTareWeight != mdTRLTar)
                   moIDH_DISPROW.TrailerTareWeight = mdTRLTar;
               if (mdtTRWTE1 != null)
                   moIDH_DISPROW.TareWeightDate = mdtTRWTE1;

               if (mdtTRWTE2 != null)
                   moIDH_DISPROW.SecondTareWeightDate = mdtTRWTE2;

               DoUpdateVehicle = true;
               //moIDH_DISPROW.doRequestCIPSIPUpdateOnOK();
               return doAddDataRow();
           }
           return false;
       }
       private bool doGetItemDetails() {
           if (!string.IsNullOrEmpty(msItemCode)) {
               moIDH_DISPROW.U_ItemDsc = com.idh.bridge.lookups.Config.INSTANCE.doGetItemDescription(msItemCode);
               return true;
           }
           return false;
       }
       private bool doGetCustomerDetails(){
           if(!string.IsNullOrEmpty(msCustomerCode)){
               moIDH_DISPORD.doValidate_CardNM();
               return true;
           }
           return false;
       }
       private bool doGetHaulierDetails() {
           if (!string.IsNullOrEmpty(msCarrierCode)) {
               moIDH_DISPROW.U_CarrNm = com.idh.bridge.lookups.Config.INSTANCE.doGetBPName(msCarrierCode);
               return true;
           }
           return false;
       }
       public void doSetPropertyFromString(string sProperty,string sValue) {
           if (sProperty.Equals(sVTYPE))
               VehicleType = sValue;
           else if (sProperty.Equals(sREG))
               VehicleRegistration = sValue;
           else if (sProperty.Equals(sVEHDESC))
               VehicleDescription = sValue;
           else if (sProperty.Equals(sWCSCD))
               CarrierCode = sValue;
           else if (sProperty.Equals(sAVAIL))
               Available = sValue;
           else if (sProperty.Equals(sDRIVER))
               Driver = sValue;
           else if (sProperty.Equals(sBRANCH))
               Branch = sValue;
           else if (sProperty.Equals(sDRVNR))
               DriverNumber = sValue;
           else if (sProperty.Equals(sVEHCODE))
               VehicleCode = sValue;
           else if (sProperty.Equals(sWR1TYPE))
               VehicleWR1ORD = sValue;
       }
       public void doSetPropertyFromString(string sProperty, double dValue) {
           if (sProperty.Equals(sTARRE))
               TarrWeight = dValue;
       }
       #endregion

       #region Getters & Setters
       public string TicketNumber {
           get { return msTicketNumber; }
       }

       public IDH_DISPORD DO {
           get { return moIDH_DISPORD; }
       }

       public IDH_DISPROW DOR {
           get { return moIDH_DISPROW; }
       }

       public IDH_DOADDEXP AdditionalExpenses {
           get { return moIDH_DISPROW.AdditionalExpenses; }
       }

       public IDH_DISPORD Order {
           get { return moIDH_DISPROW.Order; }
           set { moIDH_DISPROW.Order = value; }
       }

       public IDH_CSITPR CIP {
           get { return moIDH_DISPROW.CIP; }
       }

       public IDH_SUITPR SIPT {
           get { return moIDH_DISPROW.SIPT; }
       }

       public IDH_SUITPR SIPH {
           get { return moIDH_DISPROW.SIPH; }
       }

       public IDH_SUITPR SIPP {
           get { return moIDH_DISPROW.SIPP; }
       }

       public IDH_DODEDUCT Deductions {
           get { return moIDH_DISPROW.Deductions; }
           set { moIDH_DISPROW.Deductions = value; }
       }

       public bool AdditionalPricesLookups {
           get { return moIDH_DISPROW.AdditionalPricesLookups; }
           set { moIDH_DISPROW.AdditionalPricesLookups = value; }
       }

       public bool DoChargeVat {
           get { return moIDH_DISPROW.DoChargeVat; }
           set { moIDH_DISPROW.DoChargeVat = value; }
       }

       public bool DoSkipMarketingDocuments {
           get { return moIDH_DISPROW.DoSkipMarketingDocuments; }
           set { moIDH_DISPROW.DoSkipMarketingDocuments = value; }
       }

       public bool DoUpdateVehicle {
           get { return moIDH_DISPROW.DoUpdateVehicle; }
           set { moIDH_DISPROW.DoUpdateVehicle = value; }
       }

       public bool CalledFromGUI {
           get { return moIDH_DISPROW.CalledFromGUI; }
           set { moIDH_DISPROW.CalledFromGUI = value; }
       }

       public bool BlockChangeNotif {
           get { return moIDH_DISPROW.BlockChangeNotif; }
           set { moIDH_DISPROW.BlockChangeNotif = value; }
       }

       public bool UseCachedPrices {
           get { return moIDH_DISPROW.UseCachedPrices; }
           set { moIDH_DISPROW.UseCachedPrices = value; }
       }

       private string msOrdRow;
       public string OrderRow {
           get { return msOrdRow; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msOrdRow = value;
                   moIDH_DISPROW.OrderRow = value;
               }
           }
       }

       private string msBranch;
       public string Branch {
           get { return msBranch; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msBranch = value;
                   moIDH_DISPROW.U_Branch = value;
               }
           }
       }

       private double mdFirstWeight;
       public double FirstWeight {
           get { return mdFirstWeight; }
           set {
               if (value != null && value != 0) {
                   mdFirstWeight = value;
                   moIDH_DISPROW.U_Wei1 = value;
               }
           }
       }

       private double mdSecondWeight;
       public double SecondWeight {
           get { return mdSecondWeight; }
           set {
               if (value != null && value != 0) {
                   mdSecondWeight = value;
                   moIDH_DISPROW.U_Wei2 = value;
               }
           }
       }

       private double mdTarrWeight;
       public double TarrWeight {
           get { return mdTarrWeight; }
           set {
               if (value != null && value != 0) {
                   mdTarrWeight = value;
                   moIDH_DISPROW.TareWeight = value;
               }
           }
       }

       private double mdReadWeight;
       public double TotalWeight {
           get { return mdReadWeight; }
           set {
               if (value != null && value != 0) {
                   mdReadWeight = value;
                   moIDH_DISPROW.U_RdWgt = value;
               }
           }
       }

       private double mdTRLTar;
       public double TrailerTareWeight {
           get { return mdTRLTar; }
           set {
               if (value != null && value != 0) {
                   mdTRLTar = value;
               }
           }
       }

       private DateTime mdtTRWTE1;
       public DateTime TareWeightDate {
           get { return mdtTRWTE1; }
           set {
               mdtTRWTE1 = value; 
               moIDH_DISPROW.TareWeightDate = value; }
       }

       private DateTime mdtTRWTE2;
       public DateTime SecondTareWeightDate {
           get { return mdtTRWTE2; }
           set {
               mdtTRWTE2 = value;
               moIDH_DISPROW.SecondTareWeightDate = value;
           }
       }

       private string msItemCode;
       public string WasteCode { get; set; }

       private string msItemDescription;
       public string WasteDescription { get; set; }

       private string msItemGroup;
       public string WasteItemGroup {
           get { return msItemGroup; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msItemGroup = value;
                   moIDH_DISPROW.U_ItmGrp = value;
               }
           }
       }

       private string msComment;
       public string Comments {
           get { return msComment; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msComment = value;
                   moIDH_DISPROW.U_Comment = value;
               }
           }
       }

       private string msVehReg;
       public string VehicleRegistration {
           get { return msVehReg; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msVehReg = value;
                   moIDH_DISPROW.U_Lorry = value;
               }
           }
       }

       private string msVehDesc;
       public string VehicleDescription {
           get { return msVehDesc; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msVehDesc = value;
                   moIDH_DISPROW.U_VehTyp = value;
                   moIDH_DISPROW.VehicleDesc = value;
               }
           }
       }

       private string msVehicleWR1ORD;
       public string VehicleWR1ORD {
           get { return msVehicleWR1ORD; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msVehicleWR1ORD = value;
               }
           }
       }

       private string msDriver;
       public string Driver {
           get { return msDriver; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msDriver = value;
                   moIDH_DISPROW.U_Driver = value;
               }
           }
       }

       private string msUser;
       public string ScaleOpperator {
           get { return msUser; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msUser = value;
                   moIDH_DISPROW.U_User = value;
               }
           }
       }

       private string msContainerCode;
       public string ContainerCode {
           get { return msContainerCode; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msContainerCode = value;
                   moIDH_DISPROW.U_ItemCd = value;
                   doGetItemDetails();
               }
           }
       }

       private string msCustomerCode;
       public string CustomerCode {
           get { return msCustomerCode; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msCustomerCode = value;
                   moIDH_DISPORD.U_CardCd = value;
                   moIDH_DISPORD.doValidate_CardCd();
                   doGetCustomerDetails();
               }
           }
       }

       private string msCustomerAddress;
       public string CustomerAddress {
           get { return msCustomerAddress; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msCustomerAddress = value;
                   moIDH_DISPORD.U_Address = value;
               }
           }
       }

       private string msCustomerZipCode;
       public string CustomerZipCode {
           get { return msCustomerZipCode; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msCustomerZipCode = value;
                   moIDH_DISPORD.U_ZpCd = value;
               }
           }
       }

       private string msCarrierCode;
       public string CarrierCode {
           get { return msCarrierCode; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msCarrierCode = value;
                   moIDH_DISPROW.U_CarrCd = value;
                   moIDH_DISPROW.CarrierCd = value;
                   doGetHaulierDetails();
               }
           }
       }

       public string CarrierName { get { return moIDH_DISPROW.U_CarrNm; } }

       private string msProducerCode;
       public string ProducerCode {
           get { return msProducerCode; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msProducerCode = value;
                   moIDH_DISPROW.U_ProCd = value;
                   ProducerName = idh.bridge.lookups.Config.INSTANCE.doGetBPName(value);
               }
           }
       }

       private string msProducerName;
       public string ProducerName {
           get { return msProducerName; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msProducerName = value;
                   moIDH_DISPROW.U_ProNm = value;
               }
           }
       }

       private string msTipCode;
       public string DisposalSiteCode {
           get { return msTipCode; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msTipCode = value;
                   moIDH_DISPROW.U_Tip = value;
               }
           }
       }
       
       private string msTipName;
       public string DisposalSiteName {
           get { return msTipName; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msTipName = value;
                   moIDH_DISPROW.U_TipNm = value;
               }
           }
       }

       private string msTipAddress;
       public string DisposalSiteAddress {
           get { return msTipAddress; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msTipAddress = value;
                   moIDH_DISPORD.U_SAddress = value;
               }
           }
       }

       private string msOrigin;
       public string Origion {
           get { return msOrigin; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msOrigin = value;
                   this.setOrigin(value);
               }
           }
       }
       private string msVehType;
       public string VehicleType {
           get { return msVehType; }
           set {
               if (!string.IsNullOrEmpty(value)) {
                   msVehType = value;
               }
           }
       }
       private string msAvail;
       public string Available { get; set; }
       public string VehicleCode { get; set; }
       public string DriverNumber { get; set; }
       public SAPbobsCOM.BoCardTypes CarrierType { get; set; }
       public int CarrierGroupCode { get; set; }
       public string CarrierGroupName { get; set; }
       public double CarrierCurrentAccountBalance { get; set; }
       public double CarrierCreditLimit { get; set; }
       public string CarrierPhone1 { get; set; }
       public string CarrierContactPerson { get; set; }
       public string CarrierWasteLisence { get; set; }
       public string CarrierTarget { get; set; }
       public SAPbobsCOM.BoCardTypes ProducerCardType { get; set; }
       public int ProducerGroupCode { get; set; }
       public string ProducerGroupName { get; set; }
       public double ProducerCurrentAccountBalance { get; set; }
       public double ProducerCreditLimit { get; set; }
       public string ProducerPhone1 { get; set; }
       public string ProducerContactPerson { get; set; }
       public string ProducerWasteLisence { get; set; }
       public string ProducerTarget { get; set; }
       public string ContainerGroupName { get; set; }
       public int ContainerGroupCode { get; set; }
       public double ReadWeight {
           get { return moIDH_DISPROW.U_Weight; }
           set { moIDH_DISPROW.U_Weight = value; }
       }

       #endregion
   }
}
