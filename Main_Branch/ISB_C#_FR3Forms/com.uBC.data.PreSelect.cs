﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.uBC.data {
    public class PreSelect {
        private string msWasteCode;
        public string WasteCode {
            get { return msWasteCode; }
            set { msWasteCode = value; }
        }

        private string msWasteDescription;
        public string WasteDescription {
            get { return msWasteDescription; }
            set { msWasteDescription = value; }
        }

        private string msBPCode;
        public string BPCode {
            get { return msBPCode; }
            set { msBPCode = value; }
        }

        private string msBPName;
        public string BPName {
            get { return msBPName; }
            set { msBPName = value; }
        }

        private string msVehReg;
        public string VehReg {
            get { return msVehReg; }
            set { msVehReg = value; }
        }

        private string msTransactionCode;
        public string TransactionCode {
            get { return msTransactionCode; }
            set { msTransactionCode = value; }
        }

        private string msJobRowCode;
        public string JobRowCode {
            get { return msJobRowCode; }
            set { msJobRowCode = value; }
        }

        private string msJobCode;
        public string JobCode {
            get { return msJobCode; }
            set { msJobCode = value; }
        }

        public PreSelect() { }

        public override string ToString() {
            return "PreSelect Data: " + msTransactionCode;
        }
        //SV 24/03/2015
        private string msVehicleDescription;
        public string sVehicleDescription {
            get { return msVehicleDescription; }
            set { msVehicleDescription = value; }
        }
        private string msCarrier;
        public string sCarrier {
            get { return msCarrier; }
            set { msCarrier = value; }
        }
        private string msDriver;
        public string sDriver {
            get { return msDriver; }
            set { msDriver = value; }
        }
        private bool mbCreate;
        public bool Create {
            get { return mbCreate; }
            set { mbCreate = value; }
        }
        private string msCustomer;
        public string Customer {
            get { return msCustomer; }
            set { msCustomer = value; }
        }
        public string msBranch;
        public string Branch {
            get { return msBranch; }
            set { msBranch = value; }
        }
        private string msItemGroup;
        public string ItemGroup {
            get { return msItemGroup; }
            set { msItemGroup = value; }
        }

        //End
    }
}
