﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.uBC.utils.Dynamic {
    public class HideShowFF {
        private Structs.HideShow moState;
        public Structs.HideShow CurrentState {
            get { return moState; }
        }

        private Structs.HideShow moPrevState;
        

        private bool mbHideNotShow;
        public bool HideNotShow {
            get { return mbHideNotShow; }
        }
    
        public HideShowFF() {
            moState = new Structs.HideShow();
            moPrevState = new Structs.HideShow();
            init();

        }
        private void init() {
            moState.bHideLeft = true;
            moState.bHideRight = true;
            moState.bHideTop = true;
            moState.oLastButtonPressed = Structs.LPosition.pos_NONE;
            moPrevState = moState;
        }
        public bool doRelocate(ref SAPbouiCOM.Form oSBOForm, ref SAPbouiCOM.Item oFrame, ref com.idh.controls.DBOGrid oAdminGrid, ref List<SBOFFItem> oItems, Structs.LPosition oLayoutPos, Structs.LOption oOption, int iNextVirticalPlacement = 10) {
            if (oOption == Structs.LOption.NORMAL) {
                moPrevState = moState;
                if (oLayoutPos == Structs.LPosition.pos_TOP && !doCompareState(oLayoutPos)) {
                    moState.bHideTop = false;
                    moState.bHideLeft = true;
                    moState.bHideRight = true;
                }
                else if (oLayoutPos == Structs.LPosition.pos_LEFT && !doCompareState(oLayoutPos)) {
                    moState.bHideTop = true;
                    moState.bHideLeft = false;
                    moState.bHideRight = true;
                }
                else if (oLayoutPos == Structs.LPosition.pos_RIGHT && !doCompareState(oLayoutPos)) {
                    moState.bHideTop = true;
                    moState.bHideLeft = true;
                    moState.bHideRight = false;
                }
                else {
                    moState.bHideLeft = true;
                    moState.bHideRight = true;
                    moState.bHideTop = true;
                }
                moState.oLastButtonPressed = oLayoutPos;
                doRelocateGrid(ref oSBOForm, ref oAdminGrid, oLayoutPos, iNextVirticalPlacement);
                doRelocateFrame(ref oSBOForm, ref oFrame, oLayoutPos, iNextVirticalPlacement);
                doSetVisibility();
                return true;
            }
            else if (oOption == Structs.LOption.RESIZE) {
                doRelocateGrid(ref oSBOForm, ref oAdminGrid, oLayoutPos, iNextVirticalPlacement);
                doRelocateFrame(ref oSBOForm, ref oFrame, oLayoutPos, iNextVirticalPlacement);
                return true;
            }
            else
                return false;
        }
        private bool doCompareState(Structs.LPosition oPos) {
            if (oPos == Structs.LPosition.pos_TOP && !moPrevState.bHideTop) {
                return true;
            }
            else if (oPos == Structs.LPosition.pos_LEFT && !moPrevState.bHideLeft) {
                return true;
            }
            else if (oPos == Structs.LPosition.pos_RIGHT && !moPrevState.bHideRight) {
                return true;
            }
            else
                return false;
            
        }
        private void doRelocateGrid(ref SAPbouiCOM.Form oSBOForm, ref com.idh.controls.DBOGrid oAdminGrid, Structs.LPosition oLayoutPos,int iNextVerticalPlacement) {
            if (oLayoutPos == Structs.LPosition.pos_TOP && !moState.bHideTop) {
                    oAdminGrid.getSBOItem().Top = iNextVerticalPlacement + 15;
                    oAdminGrid.getSBOItem().Left = 10;
                    oAdminGrid.getSBOItem().Width = oSBOForm.ClientWidth - 30;
                    oAdminGrid.getSBOItem().Height = oSBOForm.ClientHeight - LayoutFF.Footer - iNextVerticalPlacement - 15;//105;
            }
            else if (oLayoutPos == Structs.LPosition.pos_LEFT && !moState.bHideLeft) {
                    oAdminGrid.getSBOItem().Top = 10;
                    oAdminGrid.getSBOItem().Left = 10 + LayoutFF.FrameWidth + 1;
                    oAdminGrid.getSBOItem().Width = oSBOForm.ClientWidth - 20 - LayoutFF.FrameWidth - 1;
                    oAdminGrid.getSBOItem().Height = oSBOForm.ClientHeight - LayoutFF.Footer - 10;
            }
            else if (oLayoutPos == Structs.LPosition.pos_RIGHT && !moState.bHideRight) {
                    oAdminGrid.getSBOItem().Top = 10;
                    oAdminGrid.getSBOItem().Left = 10;
                    oAdminGrid.getSBOItem().Width = oSBOForm.ClientWidth - 20 - LayoutFF.FrameWidth - 5;
                    oAdminGrid.getSBOItem().Height = oSBOForm.ClientHeight - LayoutFF.Footer - 10;
            }
            else {
                oAdminGrid.getSBOItem().Top = 10;
                oAdminGrid.getSBOItem().Left = 10;
                oAdminGrid.getSBOItem().Width = oSBOForm.ClientWidth - 25;
                oAdminGrid.getSBOItem().Height = oSBOForm.ClientHeight - LayoutFF.Footer - 10;
            }
        }
        
        private void doRelocateFrame(ref SAPbouiCOM.Form oSBOForm,ref SAPbouiCOM.Item oFrame, Structs.LPosition oLayoutPos,int iNextVerticalPlacement) {
            if (oLayoutPos == Structs.LPosition.pos_TOP && !moState.bHideTop) {
                    oFrame.Top = 10;
                    oFrame.Left = 10;
                    oFrame.Width = oSBOForm.ClientWidth - 20;
                    oFrame.Height = iNextVerticalPlacement - 5;
                    oFrame.Visible = true;
            }
            else if (oLayoutPos == Structs.LPosition.pos_LEFT && !moState.bHideLeft) {
                    oFrame.Top = 10;
                    oFrame.Left = 10;
                    oFrame.Width = LayoutFF.FrameWidth;
                    oFrame.Height = oSBOForm.ClientHeight - LayoutFF.Footer - 10;
                    oFrame.Visible = true;
            }
            else if (oLayoutPos == Structs.LPosition.pos_RIGHT && !moState.bHideRight) {
                    oFrame.Top = 10;
                    oFrame.Left = oSBOForm.ClientWidth - 10 - LayoutFF.FrameWidth - 1;
                    oFrame.Width = LayoutFF.FrameWidth;
                    oFrame.Height = oSBOForm.ClientHeight - LayoutFF.Footer - 10;
                    oFrame.Visible = true;
            }
            else {
                oFrame.Visible = false;
            }
        }

        private void doSetVisibility() {
            if ((moPrevState.bHideTop && !moState.bHideTop) || (moPrevState.bHideLeft && !moState.bHideLeft) || (moPrevState.bHideRight && !moState.bHideRight))
                mbHideNotShow = false;
            else if ((!moPrevState.bHideTop && moState.bHideTop) || (!moPrevState.bHideLeft && moState.bHideLeft) || (!moPrevState.bHideRight && moState.bHideRight))
                mbHideNotShow = true;
        }
    }
}
