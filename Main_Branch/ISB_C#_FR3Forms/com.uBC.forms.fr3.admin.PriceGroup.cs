﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.dbObjects.User;
using IDHAddOns.idh.controls;
using com.uBC.utils;
using com.idh.controls;
using com.idh.bridge.utils;

namespace com.uBC.forms.fr3.admin {
    public class PriceGroup : com.uBC.forms.fr3.dbGrid.Base {

        public PriceGroup(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
                mbIsSearch = false;
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDHPRCGRP", sParMenu, iMenuPos, "admin.srf", false, true, false, "Price Groups", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        protected override void doInitialize() {
            moDBObject = new IDH_PRCGRP();
        }

#region FormOpenCreateFunctions
        /*
         * Do Complete the Form
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            base.doCompleteCreate(ref BubbleEvent);
        }

       /* 
        * Do the final form steps to show before loaddata
        */
        public override void doBeforeLoadData() {
            base.doBeforeLoadData();
        }

        /* 
         * Load the Form Data
         */
        public override void doLoadData() {
            base.doLoadData();
            doFillCombos();
        }

        /*
         * Set the Filter Fields
         */
        public override void doSetFilterFields() {
        }

        /*
         * Set the List Fields
         */
        public override void doSetListFields() {
            moAdminGrid.doAddListField(IDH_PRCGRP._Code, "Code", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_PRCGRP._Name, "Name", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);

            //"SRC*IDHCSRCH(BP)[IDH_BPCOD;IDH_TYPE=SC][CARDCODE;U_SuppNm=CARDNAME]"
            //moAdminGrid.doAddListField(IDH_PRCGRP._GrpCode, "Group Code", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            //moAdminGrid.doAddListField(IDH_PRCGRP._GrpName, "Group Name", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_PRCGRP._GrpDesc, "Group Description", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_PRCGRP._GrpSC, "S/C", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);
        }
#endregion

#region Events
        protected override void doSetHandlers() {
            base.doSetHandlers();
        }

        protected override void doSetGridHandlers() {
            base.doSetGridHandlers();
        }
#endregion 

#region ItemEventHandlers
#endregion

#region fillCombos
        private void doFillCombos(){
        }
#endregion
    }
}
