﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.dbObjects.User;
using IDHAddOns.idh.controls;
using com.uBC.utils;
using com.idh.controls;
using com.idh.bridge.utils;

namespace com.uBC.forms.fr3.admin {
    public class Reason : com.uBC.forms.fr3.dbGrid.Base {

        public Reason(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
                mbIsSearch = false;
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDHREASON", sParMenu, iMenuPos, "admin.srf", false, true, false, "Reasons", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        protected override void doInitialize() {
            moDBObject = new IDH_REASONS();
        }

#region FormOpenCreateFunctions
        /*
         * Do Complete the Form
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            base.doCompleteCreate(ref BubbleEvent);
        }

       /* 
        * Do the final form steps to show before loaddata
        */
        public override void doBeforeLoadData() {
            base.doBeforeLoadData();

            moAdminGrid.doApplyExpand(IDHGrid.EXPANDED);
            moAdminGrid.setOrderValue(IDH_REASONS._Category);
        }

        /* 
         * Load the Form Data
         */
        public override void doLoadData() {
            base.doLoadData();
        }

        /*
         * Set the Filter Fields
         */
        public override void doSetFilterFields() {
        }

        /*
         * Set the List Fields
         */
        public override void doSetListFields() {
            moAdminGrid.doAddListField(IDH_REASONS._Category, "Category", true, 100, ListFields.LISTTYPE_IGNORE, null, -1, SAPbouiCOM.BoLinkedObject.lf_None, false, true, SAPbouiCOM.BoColumnSumType.bst_None, SAPbouiCOM.BoColumnDisplayType.cdt_Edit); 
            moAdminGrid.doAddListField(IDH_REASONS._SubCat, "Sub Category", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_REASONS._Code, "Reason Code", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_REASONS._Name, "Reason Name", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_REASONS._Reason, "Reason", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);
        }
#endregion

#region Events
        protected override void doSetHandlers() {
            base.doSetHandlers();
        }

        protected override void doSetGridHandlers() {
            base.doSetGridHandlers();

            //moAdminGrid.Handler_GRID_FIELD_CHANGED = new IDHGrid.ev_GRID_EVENTS(doFieldChangeEvent); 
        }
#endregion 

#region ItemEventHandlers
#endregion

#region fillCombos
#endregion
    }
}
