
Imports System.Collections
Imports System.IO
Imports System

Imports com.idh.utils.Conversions
Imports com.idh.bridge

Namespace idh.forms.SBO
    Public Class Activity
        Inherits IDHAddOns.idh.forms.Base

        '*** Item Groups
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "651", -1, Nothing)
            gsSystemMenuID = "2563"

            'doAddImagesToFix("IDH_WOHLK")
            'doAddImagesToFix("IDH_WORLK")
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        Public Overrides Sub doLoadForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                doActivityForm(oForm)
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString, "doLoadForm()")
            End Try
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim sEvtItem As String = pVal.ItemUID
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.ItemUID = "IDH_WOHLK" Then
                    Dim sOrd As String = getDFValue(oForm, "OCLG", "U_IDHWO")
                    Dim sRow As String = getDFValue(oForm, "OCLG", "U_IDHWOR")
                    If sOrd.Length > 0 Then
                        Dim oData As New ArrayList
                        oData.Add("DoUpdate")
                        oData.Add(sOrd)
                        goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)
                    End If
                ElseIf pVal.ItemUID = "IDH_WORLK" Then
                    Dim sOrd As String = getDFValue(oForm, "OCLG", "U_IDHWO")
                    Dim sRow As String = getDFValue(oForm, "OCLG", "U_IDHWOR")
                    If sRow.Length > 0 Then
                        Dim oData As New ArrayList
                        oData.Add("DoUpdate")
                        oData.Add(sRow)
                        setSharedData(oForm, "ROWCODE", sRow)
                        goParent.doOpenModalForm("IDHJOBS", oForm, oData)
                    End If
                ElseIf pVal.ItemUID = "IDH_DOHLK" Then
                    Dim sOrd As String = getDFValue(oForm, "OCLG", "U_IDHDO")
                    Dim sRow As String = getDFValue(oForm, "OCLG", "U_IDHDOR")
                    If sOrd.Length > 0 Then
                        Dim oData As New ArrayList
                        oData.Add("DoUpdate")
                        oData.Add(sOrd)
                        setSharedData(oForm, "ROWCODE", sRow)
                        goParent.doOpenModalForm("IDH_DISPORD", oForm, oData)
                    End If
                ElseIf pVal.ItemUID = "IDH_DORLK" Then
                    Dim sOrd As String = getDFValue(oForm, "OCLG", "U_IDHDO")
                    Dim sRow As String = getDFValue(oForm, "OCLG", "U_IDHDOR")
                    If sRow.Length > 0 Then
                        Dim oData As New ArrayList
                        oData.Add("DoUpdate")
                        oData.Add(sOrd)
                        setSharedData(oForm, "ROWCODE", sRow)
                        goParent.doOpenModalForm("IDH_DISPORD", oForm, oData)
                    End If
                End If
            End If
        End Function

        ''** The Menu Event handler
        ''** Return True if the Event must be handled by the other Objects
        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
        End Function

        Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        End Sub

        Public Overrides Sub doClose()
        End Sub

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = False Then
                Try
                    Dim isWODraftActivity As Boolean
                    If getParentSharedData(oForm, "WODRFTACTY") IsNot Nothing AndAlso getParentSharedData(oForm, "WODRFTACTY").ToString().Length > 0 Then
                        isWODraftActivity = getParentSharedData(oForm, "WODRFTACTY")
                        If isWODraftActivity Then
                            oForm.Close()
                        End If
                    End If
                Catch ex As Exception
                End Try
            End If
        End Sub

        Public Sub doActivityForm(ByVal oForm As SAPbouiCOM.Form)
            Dim sFMode As String = getParentSharedData(oForm, "sFMode")

            Dim owItemLabel As SAPbouiCOM.Item
            Dim owItem As SAPbouiCOM.Item

            Dim owItemControl As SAPbouiCOM.Item
            Dim oEdit As SAPbouiCOM.EditText
            Dim oBTNLink As SAPbouiCOM.Item
            Dim oMastLink As SAPbouiCOM.Button

            Dim oCmb As SAPbouiCOM.ComboBox

            Dim iHeight As Integer

            'WO Header Edit 
            owItemControl = oForm.Items.Item("148")
            owItem = oForm.Items.Add("IDH_WOHdr", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + owItemControl.Height + 5
            owItem.Width = owItemControl.Width
            owItem.Height = owItemControl.Height
            owItem.Visible = True
            'owItem.AffectsFormMode = False
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            'owItem.Enabled = False
            'Adding link buttons for future
            oBTNLink = oForm.Items.Add("IDH_WOHLK", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oBTNLink.Left = owItemControl.Left - 13
            oBTNLink.Top = owItemControl.Top + owItemControl.Height + 5
            oBTNLink.Width = 12
            oBTNLink.Height = 12
            oBTNLink.FromPane = owItemControl.FromPane
            oBTNLink.ToPane = owItemControl.ToPane
            oMastLink = oBTNLink.Specific
            oMastLink.Type = SAPbouiCOM.BoButtonTypes.bt_Image
            oMastLink.Image = IDHAddOns.idh.addon.Base.doFixImagePath("Drill.png")
            'WO Header Label 
            owItemLabel = oForm.Items.Item("149") 'Duration Label
            owItem = oForm.Items.Add("IDH_WOHLBL", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 1
            owItem.Width = owItemLabel.Width
            owItem.Height = owItemLabel.Height
            owItem.Visible = True
            owItem.Specific.Caption = Translation.getTranslatedWord("WO Header No")
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDH_WOHdr"


            'WO Row Edit 
            owItemControl = oForm.Items.Item("IDH_WOHdr")
            owItem = oForm.Items.Add("IDH_WORow", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + owItemControl.Height + 1
            owItem.Width = owItemControl.Width
            owItem.Height = owItemControl.Height
            owItem.Visible = True
            'owItem.AffectsFormMode = False
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            'owItem.Enabled = False
            'Adding link buttons for future
            oBTNLink = oForm.Items.Add("IDH_WORLK", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oBTNLink.Left = owItemControl.Left - 13
            oBTNLink.Top = owItemControl.Top + owItemControl.Height + 1
            oBTNLink.Width = 12
            oBTNLink.Height = 12
            oBTNLink.FromPane = owItemControl.FromPane
            oBTNLink.ToPane = owItemControl.ToPane
            oMastLink = oBTNLink.Specific
            oMastLink.Type = SAPbouiCOM.BoButtonTypes.bt_Image
            oMastLink.Image = IDHAddOns.idh.addon.Base.doFixImagePath("Drill.png")
            'WO Row Label 
            owItemLabel = oForm.Items.Item("IDH_WOHLBL") 'Duration Label
            owItem = oForm.Items.Add("IDH_WORLBL", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 1
            owItem.Width = owItemLabel.Width
            owItem.Height = owItemLabel.Height
            owItem.Visible = True
            owItem.Specific.Caption = Translation.getTranslatedWord("WO Row No")
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDH_WORow"

            'Now adding Disposal Order fields 
            'Disposal Order Header Edit 
            owItemControl = oForm.Items.Item("IDH_WORow")
            owItem = oForm.Items.Add("IDH_DOHdr", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + owItemControl.Height + 4
            owItem.Width = owItemControl.Width
            owItem.Height = owItemControl.Height
            owItem.Visible = True
            'owItem.AffectsFormMode = False
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            'owItem.Enabled = False
            'Adding link buttons for future
            oBTNLink = oForm.Items.Add("IDH_DOHLK", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oBTNLink.Left = owItemControl.Left - 13
            oBTNLink.Top = owItemControl.Top + owItemControl.Height + 4
            oBTNLink.Width = 12
            oBTNLink.Height = 12
            oBTNLink.FromPane = owItemControl.FromPane
            oBTNLink.ToPane = owItemControl.ToPane
            oMastLink = oBTNLink.Specific
            oMastLink.Type = SAPbouiCOM.BoButtonTypes.bt_Image
            oMastLink.Image = IDHAddOns.idh.addon.Base.doFixImagePath("Drill.png")
            'Disposal Order Header Label 
            owItemLabel = oForm.Items.Item("IDH_WORLBL") 'Duration Label
            owItem = oForm.Items.Add("IDH_DOHLBL", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 4
            owItem.Width = owItemLabel.Width
            owItem.Height = owItemLabel.Height
            owItem.Visible = True
            owItem.Specific.Caption = Translation.getTranslatedWord("Disposal Order No")
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDH_DOHdr"

            'Disposal Order Row Edit 
            owItemControl = oForm.Items.Item("IDH_DOHdr")
            owItem = oForm.Items.Add("IDH_DORow", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            owItem.Left = owItemControl.Left
            owItem.Top = owItemControl.Top + owItemControl.Height + 1
            owItem.Width = owItemControl.Width
            owItem.Height = owItemControl.Height
            owItem.Visible = True
            'owItem.AffectsFormMode = False
            owItem.FromPane = owItemControl.FromPane
            owItem.ToPane = owItemControl.ToPane
            'owItem.Enabled = False
            'Adding link buttons for future
            oBTNLink = oForm.Items.Add("IDH_DORLK", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oBTNLink.Left = owItemControl.Left - 13
            oBTNLink.Top = owItemControl.Top + owItemControl.Height + 1
            oBTNLink.Width = 12
            oBTNLink.Height = 12
            oBTNLink.FromPane = owItemControl.FromPane
            oBTNLink.ToPane = owItemControl.ToPane
            oMastLink = oBTNLink.Specific
            oMastLink.Type = SAPbouiCOM.BoButtonTypes.bt_Image
            oMastLink.Image = IDHAddOns.idh.addon.Base.doFixImagePath("Drill.png")
            'Disposal Order Row Label 
            owItemLabel = oForm.Items.Item("IDH_DOHLBL") 'Duration Label
            owItem = oForm.Items.Add("IDH_DORLBL", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            owItem.Left = owItemLabel.Left
            owItem.Top = owItemLabel.Top + owItemLabel.Height + 1
            owItem.Width = owItemLabel.Width
            owItem.Height = owItemLabel.Height
            owItem.Visible = True
            owItem.Specific.Caption = Translation.getTranslatedWord("Disposal Order Row")
            owItem.FromPane = owItemLabel.FromPane
            owItem.ToPane = owItemLabel.ToPane
            owItem.LinkTo = "IDH_DORow"

            'Customer Code 
            If sFMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE.ToString() Then
                oEdit = oForm.Items.Item("9").Specific
                If getParentSharedData(oForm, "IDH_CUST").ToString() <> String.Empty Then
                    oEdit.Value = getParentSharedData(oForm, "IDH_CUST").ToString()
                End If
            End If

            'WO Header Number 
            oEdit = oForm.Items.Item("IDH_WOHdr").Specific
            oEdit.DataBind.SetBound(True, "OCLG", "U_IDHWO")
            If (getParentSharedData(oForm, "IDH_WOH") <> String.Empty) And (getParentSharedData(oForm, "IDH_WOH") <> Nothing) Then
                oEdit.Value = getParentSharedData(oForm, "IDH_WOH").ToString()
            End If

            'WO Row Number
            oEdit = oForm.Items.Item("IDH_WORow").Specific
            oEdit.DataBind.SetBound(True, "OCLG", "U_IDHWOR")
            If (getParentSharedData(oForm, "IDH_WOR") <> String.Empty) And (getParentSharedData(oForm, "IDH_WOR") <> Nothing) Then
                oEdit.Value = getParentSharedData(oForm, "IDH_WOR").ToString()
            End If

            'Disposal Order Header Number 
            oEdit = oForm.Items.Item("IDH_DOHdr").Specific
            oEdit.DataBind.SetBound(True, "OCLG", "U_IDHDO")
            If (getParentSharedData(oForm, "IDH_DOH") <> String.Empty) And (getParentSharedData(oForm, "IDH_DOH") <> Nothing) Then
                oEdit.Value = getParentSharedData(oForm, "IDH_DOH").ToString()
            End If

            'Disposal Order Row Number
            oEdit = oForm.Items.Item("IDH_DORow").Specific
            oEdit.DataBind.SetBound(True, "OCLG", "U_IDHDOR")
            If (getParentSharedData(oForm, "IDH_DOR") <> String.Empty) And (getParentSharedData(oForm, "IDH_DOR") <> Nothing) Then
                oEdit.Value = getParentSharedData(oForm, "IDH_DOR").ToString()
            End If

            If idh.const.Lookup.INSTANCE.getParameterAsBool("USAREL", False) = True AndAlso getParentSharedData(oForm, "WODRFTACTY") IsNot Nothing AndAlso getParentSharedData(oForm, "WODRFTACTY").ToString().Length > 0 Then
                'Default Activity, Type and Subject fields value as per WR Config
                Dim sDfActivity As String = idh.const.Lookup.INSTANCE.getParamaterWithDefault("ADACTVTY", "") 'getParentSharedDataAsString(oForm, "")
                Dim sDfType As String = idh.const.Lookup.INSTANCE.getParamaterWithDefault("ADFTTYPE", "") 'getParentSharedDataAsString(oForm, "")
                Dim sDfSubject As String = idh.const.Lookup.INSTANCE.getParamaterWithDefault("ADFTSUBJ", "") 'getParentSharedDataAsString(oForm, "")

                Try
                    If sDfActivity.Length > 0 Then
                        oCmb = oForm.Items.Item("67").Specific 'Action
                        oCmb.Select(sDfActivity, SAPbouiCOM.BoSearchKey.psk_ByDescription)
                    End If

                    If sDfType.Length > 0 Then
                        oCmb = oForm.Items.Item("68").Specific 'Type 
                        oCmb.Select(sDfType, SAPbouiCOM.BoSearchKey.psk_ByDescription)
                    End If

                    If sDfSubject.Length > 0 Then
                        oCmb = oForm.Items.Item("35").Specific 'Subject
                        oCmb.Select(sDfSubject, SAPbouiCOM.BoSearchKey.psk_ByDescription)
                    End If
                Catch ex As Exception
                End Try
            End If

            'Check if the Activity is closed, enable the Drill buttons for WOH, WOR, DOH and DOR Link buttons 
            Try
                Dim sIsClosed As String = getDFValue(oForm, "OCLG", "Closed")

                If sIsClosed.Equals("Y") Then
                    oForm.Items.Item("IDH_WOHLK").Enabled = True
                    oForm.Items.Item("IDH_WORLK").Enabled = True
                    oForm.Items.Item("IDH_DOHLK").Enabled = True
                    oForm.Items.Item("IDH_DORLK").Enabled = True
                End If
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doError("Exception enabling Drill buttons: " + ex.ToString, "doActivityForm()")
            End Try

            ''Add a rectangle 
            'Dim oItm As SAPbouiCOM.Item
            'oItm = oForm.Items.Item("148")

            'owItem = oForm.Items.Add("IDH_RECT04", SAPbouiCOM.BoFormItemTypes.it_RECTANGLE)
            ''owItem.Left = oItm.Left - 25
            'owItem.Left = 12
            'owItem.Top = oItm.Top + oItm.Height + 2
            'owItem.Width = (oItm.Width * 2) + 15
            'owItem.Height = oItm.Height * 5
            'owItem.Visible = True
            ''owItem.AffectsFormMode = False
            'owItem.FromPane = oItm.FromPane
            'owItem.ToPane = oItm.ToPane

        End Sub

    End Class
End Namespace
