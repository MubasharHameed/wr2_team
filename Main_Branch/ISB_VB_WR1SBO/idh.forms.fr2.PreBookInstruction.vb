Imports System.IO
Imports System.Collections
Imports System

'Needed for the Framework controls
Imports IDHAddOns.idh.controls
Imports com.idh.bridge
Imports com.idh.bridge.data
'Imports idh.const.Lookup
Imports com.idh.bridge.lookups
Imports com.idh.utils

Namespace idh.forms.fr2
    Public Class PreBookInstruction
    	'This is the Base Form object from the Framework
        Inherits com.idh.forms.oo.Form

		Dim moPBIHandler As IDH_WOM.IDH_PBI_Handler
		Dim mbLastShow As Boolean = True
		
#Region "Constructer"
		Public Shared Function doRegisterFormClass(ByVal iMenuPosition As Integer) As IDHAddOns.idh.forms.Base
            Dim owForm As com.idh.forms.oo.FormController = New com.idh.forms.oo.FormController( _
            		"IDH_PBI2", _
            		"IDHORD", _
            		iMenuPosition, _
            		"frmPreBookIns.srf", _
            		False, _
            		True, _
            		False, _
            		"PreBook Instruction (OO)", _
            		IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL)
            			
           	'Add the item to the list of items to get their filepaths fixed.
           	owForm.doAddImagesToFix("IDHBPCFL")
            owForm.doAddImagesToFix("IDHCRCFL")
            owForm.doAddImagesToFix("IDHCTCFL")
            owForm.doAddImagesToFix("IDHJTCFL")
            owForm.doAddImagesToFix("IDHWTCFL")
            owForm.doAddImagesToFix("IDHADCFL")
           	
           	'Sets the History table and enables the ChangeLog Menu option
           	'This will also Add the DBDataSource when the form gets loaded
            owForm.doEnableHistory("@IDH_PBI")
            
            Return owForm
		End Function

		Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByVal sParentId As String, ByRef oSBOForm As SAPbouiCOM.Form)
			MyBase.New(oIDHForm, sParentId, oSBOForm)
			
			' Add te handlers
			Handler_Menu_NAV_ADD   =  New et_Handler_Menu_NAV_ADD( AddressOf doMenuAddEvent )
			Handler_Menu_NAV_FIND  =  New et_Handler_Menu_NAV_FIND( AddressOf doMenuFindEvent )
			Handler_Menu_NAV_FIRST =  New et_Handler_Menu_NAV_FIRST( AddressOf doMenuLoadEvent )
			Handler_Menu_NAV_LAST  =  New et_Handler_Menu_NAV_LAST( AddressOf doMenuLoadEvent )
			Handler_Menu_NAV_NEXT  =  New et_Handler_Menu_NAV_NEXT( AddressOf doMenuLoadEvent )
			Handler_Menu_NAV_PREV  =  New et_Handler_Menu_NAV_PREV( AddressOf doMenuLoadEvent )

'			addHandler_ITEM_PRESSED( "IDHBPCFL", New et_ITEM_PRESSED( AddressOf doCustomerPressed ) )
'			
'			Handler_ITEM_PRESSED = New et_ITEM_PRESSED( AddressOf doHandleItemPressedEvents )
'			Handler_Button_Ok = New et_Handler_Button_Ok( AddressOf doDoOkButtonEvent )
		End Sub

'        '*** Add event filters to avoid receiving all events from SBO
'        Protected Overrides Sub doSetEventFilters()
'            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
'            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN) ' For Tabs
'            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK) 'For Checkboxes and Option buttons
'            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
'            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
'        End Sub

        Public Overrides Sub doCompleteCreate(ByRef BubbleEvent As Boolean)
            Dim oChk As SAPbouiCOM.CheckBox  = Nothing
            Dim oOpt As SAPbouiCOM.OptionBtn = Nothing
            Dim oItem As SAPbouiCOM.Item = Nothing

			Try
                'oForm.DataBrowser.BrowseBy = "CODE"
                'oForm.DataSources.DBDataSources.Add("@IDH_PBI")

                'Create User Data sorces and bind to controls
                'Recurrence Pattern
                'Some issue with the Option's on values => D = 1, Y = 2
                oOpt = Items.Item("IDHOPTDAY").Specific
                oOpt.ValOn = "D"
                'oOpt.ValOff = "N"
                oOpt.Selected = True

                oOpt = Items.Item("IDHOPTWK").Specific
                oOpt.ValOn = "O"
                'oOpt.ValOff = "N"
                oOpt.GroupWith("IDHOPTDAY")

                oOpt = Items.Item("IDHOPTMT").Specific
                oOpt.ValOn = "M"
                'oOpt.ValOff = "N"
                oOpt.GroupWith("IDHOPTDAY")

                oOpt = Items.Item("IDHOPTYR").Specific
                oOpt.ValOn = "Y"
                'oOpt.ValOff = "N"
                oOpt.GroupWith("IDHOPTDAY")

                'Recurrence End
                oOpt = Items.Item("IDHRENDO").Specific
                oOpt.ValOn = "1"
                oOpt.ValOff = "0"

                oOpt = Items.Item("IDHRENDD").Specific
                oOpt.ValOff = "0"
                oOpt.ValOff = "1"
                oOpt.GroupWith("IDHRENDO")

'				doAddUFCheck( oForm, "IDHRDMON", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "1", "0")
                oChk = Items.Item("IDHRDMON").Specific
	            oChk.ValOff = "0"
                oChk.ValOn = "1"

'				doAddUFCheck( oForm, "IDHRDTUE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "1", "0")
                oChk = Items.Item("IDHRDTUE").Specific
                oChk.ValOff = "0"
                oChk.ValOn = "1"

'				doAddUFCheck( oForm, "IDHRDWED", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "1", "0")
                oChk = Items.Item("IDHRDWED").Specific
                oChk.ValOff = "0"
                oChk.ValOn = "1"

'				doAddUFCheck( oForm, "IDHRDTHU", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "1", "0")
                oChk = Items.Item("IDHRDTHU").Specific
                oChk.ValOff = "0"
                oChk.ValOn = "1"

'				doAddUFCheck( oForm, "IDHRDFRI", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "1", "0")
                oChk = Items.Item("IDHRDFRI").Specific
                oChk.ValOff = "0"
                oChk.ValOn = "1"

'				doAddUFCheck( oForm, "IDHRDSAT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "1", "0")
                oChk = Items.Item("IDHRDSAT").Specific
                oChk.ValOff = "0"
                oChk.ValOn = "1"

'				doAddUFCheck( oForm, "IDHRDSUN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "1", "0")
                oChk = Items.Item("IDHRDSUN").Specific
                oChk.ValOff = "0"
                oChk.ValOn = "1"

'                doAddUFCheck(oForm, "IDH_DETINV", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oChk = Items.Item("IDH_DETINV").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"
                
'                doAddUFCheck(oForm, "IDH_DETAIL", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oChk = Items.Item("IDH_DETAIL").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"
                
                oChk = Items.Item("IDH_UMONE").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"
                
                oChk = Items.Item("IDH_AUSTRT").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"
                
                oChk = Items.Item("IDH_AUEND").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"
                
                'Create the Extended Type Options
                oOpt = Items.Item("IDHEFTDAY").Specific
                'oOpt.ValOn = "D"
                'oOpt.ValOff = "N"
                oOpt.Selected = True

                oOpt = Items.Item("IDHEFTDATE").Specific
                'oOpt.ValOn = "W"
                'oOpt.ValOff = "N"
				oOpt.GroupWith("IDHEFTDAY")
                
				doAddUF( "IDH_ACDATE", SAPbouiCOM.BoDataType.dt_DATE, 10, False, False )
				
				PaneLevel = 1
            Catch ex As Exception
               com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oOpt)
            End Try
        End Sub
        
        Protected Sub doFillMonthCombos()
        	Dim oCombo As SAPbouiCOM.ComboBox
        	oCombo = Items.Item("IDHEFMTH1").Specific

            doClearValidValues(oCombo.ValidValues)
            Try
                oCombo.ValidValues.Add("1", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(1))
                oCombo.ValidValues.Add("2", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(2))
                oCombo.ValidValues.Add("3", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(3))
                oCombo.ValidValues.Add("4", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(4))
                oCombo.ValidValues.Add("5", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(5))
                oCombo.ValidValues.Add("6", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(6))
                oCombo.ValidValues.Add("7", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(7))
                oCombo.ValidValues.Add("8", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(8))
                oCombo.ValidValues.Add("9", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(9))
                oCombo.ValidValues.Add("10", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(10))
                oCombo.ValidValues.Add("11", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(11))
                oCombo.ValidValues.Add("12", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(12))
            Catch ex As Exception
            End Try
            
            oCombo = Items.Item("IDHEFMTH2").Specific
            doClearValidValues(oCombo.ValidValues)
            Try
                oCombo.ValidValues.Add("1", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(1))
                oCombo.ValidValues.Add("2", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(2))
                oCombo.ValidValues.Add("3", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(3))
                oCombo.ValidValues.Add("4", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(4))
                oCombo.ValidValues.Add("5", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(5))
                oCombo.ValidValues.Add("6", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(6))
                oCombo.ValidValues.Add("7", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(7))
                oCombo.ValidValues.Add("8", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(8))
                oCombo.ValidValues.Add("9", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(9))
                oCombo.ValidValues.Add("10", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(10))
                oCombo.ValidValues.Add("11", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(11))
                oCombo.ValidValues.Add("12", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(12))
            Catch ex As Exception
            End Try
        End Sub

        Protected Sub doFillWeekDayCombo()
        	Dim oCombo As SAPbouiCOM.ComboBox
        	oCombo = Items.Item("IDHEFDOW").Specific

            doClearValidValues(oCombo.ValidValues)
            Try
                oCombo.ValidValues.Add("0", com.idh.bridge.lookups.FixedValues.getDayOfWeekFromIndex(7))
                oCombo.ValidValues.Add("1", com.idh.bridge.lookups.FixedValues.getDayOfWeekFromIndex(1))
                oCombo.ValidValues.Add("2", com.idh.bridge.lookups.FixedValues.getDayOfWeekFromIndex(2))
                oCombo.ValidValues.Add("3", com.idh.bridge.lookups.FixedValues.getDayOfWeekFromIndex(3))
                oCombo.ValidValues.Add("4", com.idh.bridge.lookups.FixedValues.getDayOfWeekFromIndex(4))
                oCombo.ValidValues.Add("5", com.idh.bridge.lookups.FixedValues.getDayOfWeekFromIndex(5))
                oCombo.ValidValues.Add("6", com.idh.bridge.lookups.FixedValues.getDayOfWeekFromIndex(6))
            Catch ex As Exception
            End Try
        End Sub
        
        Protected Sub doFillMonthPeriodCombo()
        	Dim oCombo As SAPbouiCOM.ComboBox
        	oCombo = Items.Item("IDHEFWEEK").Specific

            doClearValidValues(oCombo.ValidValues)
            Try
                oCombo.ValidValues.Add("1", com.idh.bridge.lookups.FixedValues.getPosiotionFromIndex(1))
                oCombo.ValidValues.Add("3", com.idh.bridge.lookups.FixedValues.getPosiotionFromIndex(3))
                oCombo.ValidValues.Add("4", com.idh.bridge.lookups.FixedValues.getPosiotionFromIndex(4))
                oCombo.ValidValues.Add("5", com.idh.bridge.lookups.FixedValues.getPosiotionFromIndex(5))
                oCombo.ValidValues.Add("2", com.idh.bridge.lookups.FixedValues.getPosiotionFromIndex(2))
            Catch ex As Exception
            End Try
        End Sub
#End Region

#Region "Form Controls and Loads"
        Public Overrides Sub doBeforeLoadData()
            moPBIHandler = New IDH_WOM.IDH_PBI_Handler(IDHAddOns.idh.addon.Base.PARENT.goDICompany)

            Dim sCode As String = Nothing

            doFillCombo("IDHBRNCH", "OUBR", "Code", "Remarks")
            'Would be nice to auto select the relevant Branch of the owner/user automatically.

            doFillCombo("IDHCOGRP", "OITB g, [@IDH_JOBTYPE] jt", "ItmsGrpCod", "ItmsGrpNam", "g.ItmsGrpCod=jt.U_ItemGrp")
            
			Dim oItem As SAPbouiCOM.Item = Items.Item("IDHPERTYP")
			Dim oCombo As SAPbouiCOM.ComboBox = oItem.Specific()
			oCombo.ValidValues.Add(IDH_WOM.IDH_PBI_Handler.PER_Months, IDH_WOM.IDH_PBI_Handler.PER_Months)
			oCombo.ValidValues.Add(IDH_WOM.IDH_PBI_Handler.PER_Quarts, IDH_WOM.IDH_PBI_Handler.PER_Quarts)
			
			doFillMonthCombos()
            doFillWeekDayCombo()
            doFillMonthPeriodCombo()
			
            If isModal() = False Then
            Else
                sCode = getParentSharedData("PBICode")
                setFormDFValue("Code", sCode, True)
            End If
        End Sub
        
        Public Overrides Sub doLoadData()
        	Dim sCode As String = Nothing
			sCode = getFormDFValue("Code", True)
            setUFValue("IDH_ACDATE", "")

			If Not sCode Is Nothing AndAlso sCode.Length > 0 Then
				Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
				
                Dim oCons As New SAPbouiCOM.Conditions
                Dim oCon As SAPbouiCOM.Condition

                oCon = oCons.Add
                oCon.Alias = "Code"
                oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                oCon.CondVal = sCode
                SBOForm.DataSources.DBDataSources.Item("@IDH_PBI").Query(oCons)
                
                Dim sRecure As String = getFormDFValue("U_IDHRECUR")
                If sRecure = "2" Then
	                If Not PaneLevel = 3 Then
	            		PaneLevel = 3
	            	End If
                Else
	                If Not PaneLevel = 1 Then
	            		PaneLevel = 1
	            	End If
                End If

			Else
				Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
				
				Dim sCardCode As String = getParentSharedData("e_CardCode")
        		Dim sCardName As String = getParentSharedData("e_CardName")
        		Dim sAddr As String = getParentSharedData("e_Addr")
				
				doClearAll(False)
				
				setFormDFValue("U_IDHCCODE", sCardCode)
				setFormDFValue("U_IDHCNAME", sCardName)
				setFormDFValue("U_IDHSADDR", sAddr)
			End If
			mbLastShow = True
			DoViewActionDate(False)
        End Sub
        
        Private Sub DoViewActionDate( ByVal bDoShow As Boolean )
        	If bDoShow <> mbLastShow Then
	          	Items.Item("91").Visible = bDoShow
	        	Items.Item("IDH_ACDATE").Visible = bDoShow
	        	mbLastShow  = bDoShow
        	End If
        End Sub
        
        Private Sub doSwitchActionDate()
        	If Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
        		DoViewActionDate( True )
        	End If
        End Sub

        '** Create the form
        Private Sub doClearAll(ByVal bDoNewCode As Boolean)
        	doClearFormDFValues()
        	
        	Dim iFreq As Integer = Config.INSTANCE.getParamaterWithDefault("PBIFRQ","4")
            setFormDFValue("U_IDHRECFQ", iFreq)
            If iFreq = 3 Then
            	setFormDFValue("U_PerTyp", IDH_WOM.IDH_PBI_Handler.PER_Quarts)
            Else
            	setFormDFValue("U_PerTyp", IDH_WOM.IDH_PBI_Handler.PER_Months)
            End If
            
            setFormDFValue("U_IDHRACTW", 0)
            setFormDFValue("U_IDHRECSD", IDHAddOns.idh.addon.Base.PARENT.doDateToSBODisplay(Date.Today))
            setFormDFValue("U_IDHOWNER", IDHAddOns.idh.addon.Base.APPLICATION.goDICompany.UserName)
            
            Dim bTicket As Boolean
            bTicket = Config.INSTANCE.getParamaterAsBool("PBIAUSDT",True)
			If bTicket Then
				setFormDFValue("U_AutStrt", "Y")
			Else
				setFormDFValue("U_AutStrt", "N")
			End If
            
            bTicket = Config.INSTANCE.getParamaterAsBool("PBIAUEDT",True)
			If bTicket Then
				setFormDFValue("U_AutEnd", "Y")
			Else
				setFormDFValue("U_AutEnd", "N")
			End If
			
			setFormDFValue("U_IDHRECUR", "W")
            If Not PaneLevel = 1 Then
            	PaneLevel = 1
            End If
			
			Dim iVal As Integer
			setFormDFValue("U_EFDay", DateTime.Now.Day)
            Select Case (DateTime.Now.DayOfWeek)
                Case DayOfWeek.Monday
                    iVal = 1
                Case DayOfWeek.Tuesday
                    iVal = 2
                Case DayOfWeek.Wednesday
                    iVal = 3
                Case DayOfWeek.Thursday
                    iVal = 4
                Case DayOfWeek.Friday
                    iVal = 5
                Case DayOfWeek.Saturday
                    iVal = 6
                Case Else
                    iVal = 7
            End Select
			setFormDFValue("U_EFDOW", iVal)
			setFormDFValue("U_EFMonth", DateTime.Now.Month)
			setFormDFValue("U_EFWeek", 1)
        End Sub
#End Region

#Region "Event Handlers"
        '** The Menu Event handler
        '** Return True if the Event must be handled by the other Objects
        Public Function doMenuAddEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean 
        	If pVal.BeforeAction = False Then
        		doClearAll(True)
        	End If
        End Function
        Public Function doMenuFindEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean 
        	If pVal.BeforeAction = False Then
        		doClearAll(False)
        	End If
        End Function
        Public Function doMenuLoadEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean 
       		If pVal.BeforeAction = False Then
       			doLoadData()
       		End If
        End Function
        
        Public Function doCustomerChanged(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
        	If pVal.InnerEvent = False Then
        		doChooseCustomer()
        	End If
        End Function
        
        Public Function doCarrierChanged(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
        	If pVal.InnerEvent = False Then
        		doChooseCarrier()
        	End If
        End Function
        
        Public Function doJobTypeChanged(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
        	If pVal.InnerEvent = False Then
        		doChooseJobType()
        	End If
        End Function
        
        Public Function doContainerChanged(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
        	If pVal.InnerEvent = False Then
        		doChooseContainer()
        	End If
        End Function

        Public Function doChooseWasteTypeChanged(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
        	If pVal.InnerEvent = False Then
        		doChooseWasteType()
        	End If
        End Function

        Public Function doChooseSiteAdChanged(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
        	If pVal.InnerEvent = False Then
        		doChooseSiteAd()
        	End If
        End Function

        Public Function doRunFrequencyChanged(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
        	If pVal.InnerEvent = False Then
        		Dim iRunFreq As Integer = getFormDFValue("U_IDHRECFQ")
        		If iRunFreq <> 3 Then
        			setFormDFValue("U_PerTyp", IDH_WOM.IDH_PBI_Handler.PER_Months)
        		End If
        	End If
        End Function
        
        Public Function doRecStartDateChanged(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
        	If pVal.InnerEvent = False Then
				Dim dStartDate As DateTime = dates.doStrToDate(getFormDFValue("U_IDHRECSD"))
        		If dates.isValidDate( dStartDate ) Then
            		Dim sSwitch As String = getFormDFValue( "U_IDHRECEN" )
            		If sSwitch = "0" Then
                		setFormDFValue( "U_IDHRECEN", "2" )
                        Dim dStopDate As DateTime = dStartDate.AddYears(10)

            			Items.Item("IDHRECCE").Enabled = False
            			Items.Item("IDHRECED").Enabled = True
                        
                        setFormDFValue("U_IDHRECED", IDHAddOns.idh.addon.Base.PARENT.doDateToStr(dStopDate))
		                setFormDFValue("U_IDHRECCE", "0")
            		End If
        		End If        	
        	End If
        End Function
        
        '** Validation Event Handler
        Public Sub doAddValidatorEvents()
        	Handler_VALIDATE = New et_VALIDATE( AddressOf doValidateEvents )
        	addHandler_VALIDATE_CHANGED( "IDHCCODE", New et_VALIDATE_CHANGED( AddressOf doCustomerChanged ) )
        	addHandler_VALIDATE_CHANGED( "IDHCARCD", New et_VALIDATE_CHANGED( AddressOf doCarrierChanged ) )
        	addHandler_VALIDATE_CHANGED( "IDHJTYPE", New et_VALIDATE_CHANGED( AddressOf doJobTypeChanged ) )
        	addHandler_VALIDATE_CHANGED( "IDHCOICD", New et_VALIDATE_CHANGED( AddressOf doContainerChanged ) )
        	addHandler_VALIDATE_CHANGED( "IDHWCICD", New et_VALIDATE_CHANGED( AddressOf doChooseWasteTypeChanged ) )
        	addHandler_VALIDATE_CHANGED( "IDHSADDR", New et_VALIDATE_CHANGED( AddressOf doChooseSiteAdChanged ) )
        	addHandler_VALIDATE_CHANGED( "IDHRECFQ", New et_VALIDATE_CHANGED( AddressOf doRunFrequencyChanged ) )
        	addHandler_VALIDATE_CHANGED( "IDHRECSD", New et_VALIDATE_CHANGED( AddressOf doRecStartDateChanged ) )
        	
        	addHandler_VALIDATE_CHANGED( "IDHRECSD", New et_VALIDATE_CHANGED( AddressOf doRecStartDateChanged ) )
        End Sub
        
        'Handle all Validate events - just for testing
        Public Function doValidateEvents(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
        	If pVal.BeforeAction = False Then
        		If pVal.ItemChanged = True Then
        			Dim sItemId As String = pVal.ItemUID
        			If wasSetWithCode(sItemID) = False Then
            			If pVal.InnerEvent = False Then
                        	
                    	End If
                    End If
        		End If
        	End If
        End Function

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doItemEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Select Case pVal.BeforeAction
            	Case False
            		Dim sItemId As String = pVal.ItemUID
                    Select Case pVal.EventType
                    	Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                    		If sItemId.Equals("IDHPERTYP") Then
                    			If getFormDFValue("U_PERTYP").Equals(IDH_WOM.IDH_PBI_Handler.PER_Quarts) Then
                    				setFormDFValue("U_IDHRECFQ", 3)
                    			End If
                    		End If
                    	Case SAPbouiCOM.BoEventTypes.et_KEY_DOWN
                    		If sItemId.Equals("IDHCOIQT") OrElse _
                    			sItemId.Equals("IDHRECFQ") OrElse _
                    			sItemId.Equals("IDHRECTW") OrElse _
                    			sItemId.Equals("IDHRECSD") OrElse _
                    			sItemId.Equals("IDHRECCE") OrElse _
                    			sItemId.Equals("IDHRECED") Then
                    			doSwitchActionDate()
 	                  		End If
                        Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                            If sItemId.Equals("IDHBPCFL") Then 'BP Choose from
                                doChooseCustomer()
                            ElseIf sItemId.Equals("IDHCRCFL") Then 'Carrier Choose from
                                doChooseCarrier()
                            ElseIf sItemId.Equals("IDHJTCFL") Then 'Job Type Choose from
                                doChooseJobType()
                            ElseIf sItemId.Equals("IDHCTCFL") Then 'Container Choose from
                                doChooseContainer()
                            ElseIf sItemId.Equals("IDHWTCFL") Then 'Waste Type Choose from
                                doChooseWasteType()
                            ElseIf sItemId.Equals("IDHADCFL") Then 'Site Address Choose from
                                doChooseSiteAd()
                            ElseIf sItemId.Equals("IDHPO") Then
                            	doReportByBtn("IDHPO")
                            ElseIf pVal.ItemUID.Equals("IDHDOC") Then
                            	doReportByBtn("IDHDOC")
                            ElseIf sItemId.Equals("IDHCUST") Then
                            	doReportByBtn("IDHCUST")
                            ElseIf sItemId.Equals("IDHBM") Then
                        		setSharedData("IDH_ORDNO", getFormDFValue("U_IDHWO"))
                                doOpenModalForm("IDHWOBI")
                            ElseIf sItemId.Equals("IDHOSM") Then
                            	setSharedData("IDH_ORDNO", getFormDFValue("U_IDHWO"))
                                setSharedData("IDH_CUST", getFormDFValue("U_IDHCCODE"))
                                setSharedData("IDH_ADDR", getFormDFValue("U_IDHSADDR"))

								Dim sOSM As String = Config.INSTANCE.getParamaterWithDefault("OSMPBI","IDHJOBR")
                                doOpenModalForm(sOSM)
                            ElseIf sItemId = "IDH_PBIDT2" Then
                                setSharedData("IDH_PBI", getFormDFValue("Code"))
                                Dim sCount As String = getFormDFValue("U_IDHRECCT")
                                Dim iCount As Integer = Conversions.ToInt(sCount)
                                setSharedData("IDH_RUNCNT", iCount)
                                doOpenModalForm("IDHPBID")
                            ElseIf sItemId = "IDH_PBIDTL" Then
                                setSharedData("IDH_PBI", getFormDFValue("Code"))
                                Dim sCount As String = getFormDFValue("U_IDHRECCT")
                                Dim iCount As Integer = Conversions.ToInt(sCount)
                                setSharedData("IDH_RUNCNT", "")
                                doOpenModalForm("IDHPBID")
                            ElseIf sItemId.Equals("IDHCIP") Then
                                Dim sCardCode As String = getFormDFValue("U_IDHCCODE")
                                Dim sItemCode As String = getFormDFValue("U_IDHCOICD")
                                Dim sItemGrp As String = getFormDFValue("U_IDHCOGRP")
                                Dim sWastCd As String = getFormDFValue("U_IDHWCICD")
                                Dim sAddress As String = getFormDFValue("U_IDHSADDR")
                                Dim sJobType As String = getFormDFValue("U_IDHJTYPE")

                                setSharedData("CUSCD", sCardCode)
                                setSharedData("ITEMCD", sItemCode)
                                setSharedData("ITMGRP", sItemGrp)
                                setSharedData("WASCD", sWastCd)
                                setSharedData("STADDR", sAddress)
                                setSharedData("JOBTP", sJobType)

                                doOpenModalForm("IDH_CSITPR")
                                ElseIf sItemId.Equals("IDHSIP") Then
                                    Dim sDispSite As String = getFormDFValue("U_IDHCARCD")
                                    Dim sItemCode As String = getFormDFValue("U_IDHCOICD")
                                    Dim sItemGrp As String = getFormDFValue("U_IDHCOGRP")
                                    Dim sWastCd As String = getFormDFValue("U_IDHWCICD")
                                    Dim sAddress As String = getFormDFValue("U_IDHSADDR")
                                    Dim sJobType As String = getFormDFValue("U_IDHJTYPE")

                                    setSharedData("SUPCD", sDispSite)
                                    setSharedData("ITEMCD", sItemCode)
                                    setSharedData("ITMGRP", sItemGrp)
                                    setSharedData("WASCD", sWastCd)
                                    setSharedData("JOBTP", sJobType)
                                    setSharedData("TRG", "SIP")

                                    doOpenModalForm("IDH_SUITPR")
                                ElseIf pVal.ItemUID = "IDH_SUBCON" Then
                                    Dim sCardCode As String = getFormDFValue("U_IDHCCODE")

                                    If Not sCardCode Is Nothing AndAlso sCardCode.Length > 0 Then
                                        setSharedData("IDH_ZPCDTX", "")
                                        setSharedData("IDH_ITEM", "")
                                        setSharedData("IDH_WSCD", "")
                                        setSharedData("IDH_RTNG", "")

                                        setSharedData("IDH_CUSCOD", sCardCode)
                                        doOpenModalForm("IDH_SUBBY")
                                    End If
                                End If
                        Case SAPbouiCOM.BoEventTypes.et_CLICK
                                If sItemId = "lnk_WO" Then
                                    Dim oData As New ArrayList
                                    Dim Val As String = ""
                                    Val = getFormDFValue("U_IDHWO")
                                    oData.Add("IDHWO")
                                    oData.Add(Val)
                                    doOpenModalForm("IDH_WASTORD", oData)

                                    oData = Nothing
                                ElseIf sItemId = "lnk_WOR" Then
                                    Dim oData As New ArrayList
                                    Dim sWOR As String = ""
                                    sWOR = getFormDFValue("U_IDHWOR")
                                    oData.Add("IDHWO")
                                    oData.Add(sWOR)

                                    doOpenModalForm("IDHJOBS", oData)
                                    oData = Nothing
                                ElseIf sItemId = "IDHRENDO" OrElse sItemId = "IDHRENDD" Then
                                    Dim oOccOpt, oDtOpt As SAPbouiCOM.OptionBtn

                                    If sItemId = "IDHRENDO" Then
                                        Items.Item("IDHRECCE").Enabled = True
                                        Items.Item("IDHRECED").Enabled = False

                                        setFormDFValue("U_IDHRECED", Nothing)
                                        setFormDFValue("U_IDHRECCE", "1")
                                    Else
                                        Items.Item("IDHRECCE").Enabled = False
                                        Items.Item("IDHRECED").Enabled = True

                                        Dim dStartDate As DateTime = dates.doStrToDate(getFormDFValue("U_IDHRECSD"))
                                        Dim dStopDate As DateTime = dStartDate.AddYears(10)

                                        setFormDFValue("U_IDHRECED", IDHAddOns.idh.addon.Base.PARENT.doDateToStr(dStopDate))
                                        setFormDFValue("U_IDHRECCE", "0")
                                    End If
                                    doSetUpdate()
                                ElseIf sItemId = "IDHRDMON" OrElse _
                                    sItemId = "IDHRDTUE" OrElse _
                                    sItemId = "IDHRDWED" OrElse _
                                    sItemId = "IDHRDTHU" OrElse _
                                    sItemId = "IDHRDFRI" OrElse _
                                    sItemId = "IDHRDSAT" OrElse _
                                    sItemId = "IDHRDSUN" Then
                                    doSwitchActionDate()
                                ElseIf sItemId = "IDH_DETAIL" Then
                                    Dim sDoDetail As String = getFormDFValue("U_IDHDETAI")
                                    If sDoDetail = "N" Then
                                        setFormDFValue("U_IDHDETIN", "N")
                                    End If
                                ElseIf sItemId = "IDH_DETINV" Then
                                    Dim sDoDetail As String = getFormDFValue("U_IDHDETIN")
                                    If sDoDetail = "N" Then
                                        setFormDFValue("U_IDHDETAI", "N")
                                    End If
                                ElseIf sItemId = "IDH_UMONE" Then
                                    Dim sDoMONE As String = getFormDFValue("U_IDHUMONE")
'                                    If sDoMONE = "N" Then
'                                    End If
                                ElseIf sItemId = "IDHOPTDAY" OrElse sItemId = "IDHOPTWK" Then
                                	Items.Item("lblWkRecur").Specific.Caption = "week(s) on:"
                                    Items.Item("IDHRDMON").Enabled = True
                                    Items.Item("IDHRDTUE").Enabled = True
                                    Items.Item("IDHRDWED").Enabled = True
                                    Items.Item("IDHRDTHU").Enabled = True
                                    Items.Item("IDHRDFRI").Enabled = True
                                    Items.Item("IDHRDSAT").Enabled = True
                                    Items.Item("IDHRDSUN").Enabled = True

                                    setFormDFValue("U_IDHRACTW", 1)

                                    If sItemId = "IDHOPTDAY" Then
                                        setFormDFValue("U_IDHRDMON", 1)
                                        setFormDFValue("U_IDHRDTUE", 1)
                                        setFormDFValue("U_IDHRDWED", 1)
                                        setFormDFValue("U_IDHRDTHU", 1)
                                        setFormDFValue("U_IDHRDFRI", 1)
                                        setFormDFValue("U_IDHRDSAT", 0)
                                        setFormDFValue("U_IDHRDSUN", 0)
                                        Items.Item("IDHRACTW").Enabled = False
                                    Else
                                        Items.Item("IDHRACTW").Enabled = True
                                    End If
                                    doSwitchActionDate()
                                    If Not PaneLevel = 1 Then
                                    	PaneLevel = 1
                                    End If
                                ElseIf sItemId = "IDHOPTMT" Then
                                	Items.Item("lblWkRecur").Specific.Caption = "month(s) on:"
                                    Items.Item("IDHRDMON").Enabled = False
                                    Items.Item("IDHRDTUE").Enabled = False
                                    Items.Item("IDHRDWED").Enabled = False
                                    Items.Item("IDHRDTHU").Enabled = False
                                    Items.Item("IDHRDFRI").Enabled = False
                                    Items.Item("IDHRDSAT").Enabled = False
                                    Items.Item("IDHRDSUN").Enabled = False

                                    setFormDFValue("U_IDHRACTW", 0)
                                    Items.Item("IDHRACTW").Enabled = True

                                    setFormDFValue("U_IDHRDMON", 0)
                                    setFormDFValue("U_IDHRDTUE", 0)
                                    setFormDFValue("U_IDHRDWED", 0)
                                    setFormDFValue("U_IDHRDTHU", 0)
                                    setFormDFValue("U_IDHRDFRI", 0)
                                    setFormDFValue("U_IDHRDSAT", 0)
                                    setFormDFValue("U_IDHRDSUN", 0)
                                    doSwitchActionDate()
                                    If Not PaneLevel = 1 Then
                                    	PaneLevel = 1
                                    End If
                                ElseIf sItemId = "IDHOPTYR" Then
                                	Items.Item("lblWkRecur").Specific.Caption = "year(s) on:"
                                    Items.Item("IDHRDMON").Enabled = False
                                    Items.Item("IDHRDTUE").Enabled = False
                                    Items.Item("IDHRDWED").Enabled = False
                                    Items.Item("IDHRDTHU").Enabled = False
                                    Items.Item("IDHRDFRI").Enabled = False
                                    Items.Item("IDHRDSAT").Enabled = False
                                    Items.Item("IDHRDSUN").Enabled = False

                                    setFormDFValue("U_IDHRACTW", 1)
                                    Items.Item("IDHRACTW").Enabled = True

                                    setFormDFValue("U_IDHRDMON", 0)
                                    setFormDFValue("U_IDHRDTUE", 0)
                                    setFormDFValue("U_IDHRDWED", 0)
                                    setFormDFValue("U_IDHRDTHU", 0)
                                    setFormDFValue("U_IDHRDFRI", 0)
                                    setFormDFValue("U_IDHRDSAT", 0)
                                    setFormDFValue("U_IDHRDSUN", 0)
                                    doSwitchActionDate()
                                    If Not PaneLevel = 3 Then
                                    	PaneLevel = 3
                                    End If
                                End If
                    End Select
            End Select
            Return False
        End Function

        'Closing a Modal Form Retrieving Data from the Shared Buffer
        Public Overrides Sub doHandleModalResultShared( ByVal sModalFormType As String, ByVal sLastButton As String )
            Try
                If sModalFormType.Equals("IDHASRCH") Then
                    'setDFValue(oForm, "@IDH_PBI", "U_IDHSADDR", getSharedData(oForm, "ADDRESS") & ", " & getSharedData(oForm, "Street") & "," & getSharedData(oForm, "Block") & "," & getSharedData(oForm, "City"), True)
                    setFormDFValue("U_IDHSADDR", getSharedData("ADDRESS"), True)
                    If Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE And Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        doSetUpdate()
                    End If

                ElseIf sModalFormType = "IDHJTSRC" Then
                    setFormDFValue("U_IDHJTYPE", getSharedData("ORDTYPE"), False, True)
                    If Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE And Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        doSetUpdate()
                    End If
                ElseIf sModalFormType = "IDHISRC" Then
                    If getSharedData("TRG") = "CON" Then
                        setFormDFValue("U_IDHCOICD", getSharedData("ITEMCODE"), False, True)
                        setFormDFValue("U_IDHCOIDC", getSharedData("ITEMNAME"), False, True)
                        If Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE And Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            doSetUpdate()
                        End If
                    End If

                ElseIf sModalFormType = "IDHCSRCH" Then
                    Dim sTarget As String = getSharedData("TRG")
                    Dim code As String = getSharedData("CARDCODE")
                    Dim desc As String = getSharedData("CARDNAME")

                    If Not (code Is Nothing) AndAlso code.Length > 0 Then
                        If sTarget = "CUS" Then
                            setFormDFValue("U_IDHCCODE", code, True)
                            setFormDFValue("U_IDHCNAME", desc, True)
                            
                            setFormDFValue("U_IDHOBLGT", getSharedData( "IDHOBLGT" ))
                        ElseIf sTarget = "CAR" Then
                            setFormDFValue( "U_IDHCARCD", code, True)
                            setFormDFValue( "U_IDHCARDC", desc, True)
                        End If
                        If Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE And Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            doSetUpdate()
                        End If
                    End If
                ElseIf sModalFormType = "IDHWISRC" Then
                    setFormDFValue( "U_IDHWCICD", getSharedData( "ITEMCODE"), False, True)
                    setFormDFValue( "U_IDHWCIDC", getSharedData( "ITEMNAME"), False, True)
                    If Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE And Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        doSetUpdate()
                    End If
                End If

            Catch ex As Exception
               com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal Result - " & sModalFormType)
            End Try
        End Sub


#End Region

#Region "Search form Procs"

        Protected Overridable Sub doChooseCustomer()
            Dim sCardCode As String = getFormDFValue("U_IDHCCODE")
            Dim sCardName As String = getFormDFValue("U_IDHCNAME")

            setSharedData("TRG", "CUS")
            setSharedData("IDH_BPCOD", sCardCode)
            setSharedData("IDH_TYPE", "F-C")
            setSharedData("IDH_NAME", sCardName)
            setSharedData("SILENT", "SHOWMULTI")
            
            doOpenModalForm("IDHCSRCH")
        End Sub

        Protected Overridable Sub doChooseCarrier()
            Dim sCarrierCode As String = getFormDFValue("U_IDHCARCD")

            setSharedData("TRG", "CAR")
            setSharedData("IDHCARCD", sCarrierCode)
            setSharedData("IDH_TYPE", "S")
            setSharedData("SILENT", "SHOWMULTI")
            
            doOpenModalForm("IDHCSRCH")
        End Sub

        Protected Overridable Sub doChooseContainer()
            Dim sContainerCode As String = getFormDFValue("U_IDHCOICD")

            Dim sGrp As String
            sGrp = getFormDFValue("U_IDHCOGRP")

            setSharedData("TRG", "CON")
            setSharedData("IDH_ITMCOD", "")
            setSharedData("IDH_GRPCOD", sGrp)
            setSharedData("SILENT", "SHOWMULTI")
            
            doOpenModalForm("IDHISRC")
        End Sub

        Protected Overridable Sub doChooseJobType()
            Dim sJobCode As String = getFormDFValue("U_IDHJTYPE")
            Dim sGrp As String = getFormDFValue("U_IDHCOGRP")

            setSharedData("TRG", "JT")
            setSharedData("IDHJTYPE", "")
            setSharedData("IDH_ITMGRP", sGrp)
            setSharedData("SILENT", "SHOWMULTI")
            
            doOpenModalForm("IDHJTSRC")
        End Sub

        Protected Overridable Sub doChooseWasteType()
            Dim sCardCode As String = getFormDFValue("U_IDHCCODE")

            'setSharedData(oForm, "IDH_ITMCOD", sItem)
            setSharedData("TP", "WC")
            setSharedData("IDH_GRPCOD", Config.INSTANCE.doWasteMaterialGroup())
            setSharedData("IDH_CRDCD", sCardCode)
            setSharedData("IDH_ITMNAM", "")
            setSharedData("IDH_INVENT", "N")
            'setSharedData(oForm, "SILENT", "SHOWMULTI")

			setSharedData("SILENT", "SHOWMULTI")

            doOpenModalForm("IDHWISRC")
        End Sub

        Protected Overridable Sub doChooseSiteAd()
            Dim sCardCode As String = getFormDFValue("U_IDHCCODE")
			
			If Not sCardCode Is Nothing AndAlso sCardCode.Length > 0 Then
	            setSharedData("TRG", "SA")
	            setSharedData("IDH_CUSCOD", sCardCode)
	            setSharedData("IDH_ADRTYP", "S")
	            
	            setSharedData("SILENT", "SHOWMULTI")
	            
	            doOpenModalForm("IDHASRCH")
	     	End If
        End Sub

#End Region

#Region "Data Interface"
        Protected Function doUpdateHeader() As Boolean
            'This will now Automatically update all the current DB fields and
            'generate and set the Code value if the code was not set
            Return doAutoUpdate("PBIKEY")
        End Function
#End Region

#Region "Processing"

        Public Overrides Sub doButtonID1(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                  Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    If doValidateFields() Then
                        Dim sCode As String = getFormDFValue("Code")
                        If sCode Is Nothing OrElse sCode.Length = 0 Then
                            sCode = IDHAddOns.idh.addon.Base.PARENT.goDB.doNextNumber("PBIKEY").ToString()
                            setFormDFValue("Code", sCode)
                        End If
                        
                        doDescribeCoverage()
                    			
                    	Dim oData As DataHandler = DataHandler.INSTANCE
                        Try
                            oData.StartTransaction()
						
                            If doUpdateHeader() Then
                                'This DLL Creates/updates waste orders and row based on details of the WasteOrder Instruction. Also updates any documents linked to WO
                                '######################################################################################################################
                                Dim sDate As String = getUFValue("IDH_ACDATE")
                                If Not sDate Is Nothing Then
                                    If sDate.Length >= 6 Then
                                        sDate = com.idh.utils.dates.doSBODateToSQLDate(sDate)
                                    Else
                                        sDate = Nothing
                                    End If
                                End If
                                
                                Dim bDoRestOfUpdates As Boolean = True
                                If Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                                	If wasSetByUser("IDHRECED") Then
		                                Dim sTermDate As String = getFormDFValue("U_IDHRECED")
		                                If Not sTermDate Is Nothing AndAlso sTermDate.Length >= 6 Then
	                                        sTermDate = com.idh.utils.dates.doSBODateToSQLDate(sTermDate)
											moPBIHandler.doClearAfterTerminationDate(sCode, sTermDate)
		                                End If
		                                
		                                If getUserChangedFieldCount() > 1 Then
		                                	bDoRestOfUpdates = True
		                                Else
		                                	'Then only the Termination date was changed
		                                	bDoRestOfUpdates = False
		                                End If
                                	End If
                                End If
                                              
                                If bDoRestOfUpdates Then
                                	If moPBIHandler.ProcessOrderRun(sCode, sDate) Then
	                                    doSetFocus("IDHCCODE")
	                                    If oData.IsInTransaction = True Then
	                                        oData.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
	                                    End If
	                                    BubbleEvent = False
	                                    Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
	                                    doLoadData()
	                                Else
	                                    If oData.IsInTransaction = True Then
	                                        oData.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
	                                    End If
	                                    'oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
	                                    BubbleEvent = False
	                                    'setFormDFValue(oForm, "Code", "")
	                                End If
                                Else
                                    doSetFocus("IDHCCODE")
                                    If oData.IsInTransaction = True Then
                                        oData.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                                    End If
                                    BubbleEvent = False
                                    Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                                    doLoadData()
                                End If
                                
                                '######################################################################################################################
                            End If
                        Catch exx As Exception
                            If oData.IsInTransaction = True Then
                                oData.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                            End If
                           com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & exx.ToString(), "Adding/Updating the Instruction.")
                            'setFormDFValue(oForm, "Code", "")
                            BubbleEvent = False
                        End Try
                    End If
               	ElseIf Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
				    If isModal Then
	                	doReturnFromModalShared(True)
	                End If
                End If
            Else
            End If
        End Sub

        Private Function doValidateFields() As Boolean
            Dim sError As String = ""

            Try
                If getFormDFValue("U_IDHCCODE").Length <= 0 Then
                    sError &= "Cust. Code,"
                End If

                If getFormDFValue("U_IDHCARCD").Length <= 0 Then
                    sError &= "Carrier Code,"
                End If

                If getFormDFValue("U_IDHSADDR").Length <= 0 Then
                    sError &= "Site Address,"
                End If

                If getFormDFValue("U_IDHJTYPE").Length <= 0 Then
                    sError &= "JobType,"
                End If

                If getFormDFValue("U_IDHCOICD").Length <= 0 Then
                    sError &= "Container Code,"
                End If

                If getFormDFValue("U_IDHWCICD").Length <= 0 Then
                    sError &= "Waste Type,"
                End If


                If getFormDFValue("U_IDHRECFQ").Length <= 0 Then
                    sError &= "Run Frequency,"
                End If

                If getFormDFValue("U_IDHRECSD").Length <= 0 Then
                    sError &= "Start Date,"
                End If

				Dim sRecure As String = getFormDFValue("U_IDHRECUR")
				
				Try
					'If sRecure = "1" Then
					'	sRecure = "D"
					'	setFormDFValue(oform, "U_IDHRECUR", "D", False, False)
					'ElseIf sRecure = "2" Then
					'	sRecure = "Y"
					'	setFormDFValue(oform, "U_IDHRECUR", "Y", False, False)
					'End If
					
					'Some issue with the Option's on values => D = 1, Y = 2
                	If sRecure = "M" Then
                	ElseIf sRecure = "2" Then
                	Else
	                    If getFormDFValue("U_IDHRDMON") = 0 And _
	                                getFormDFValue("U_IDHRDTUE") = 0 And _
	                                getFormDFValue("U_IDHRDWED") = 0 And _
	                                getFormDFValue("U_IDHRDTHU") = 0 And _
	                                getFormDFValue("U_IDHRDFRI") = 0 And _
	                                getFormDFValue("U_IDHRDSAT") = 0 And _
	                                getFormDFValue("U_IDHRDSUN") = 0 Then
	                        sError &= "Action Day,"
	                    End If
	                End If
                Catch ex As Exception
                    If getFormDFValue("U_IDHRDMON") = "" And _
                                    getFormDFValue("U_IDHRDTUE") = "" And _
                                    getFormDFValue("U_IDHRDWED") = "" And _
                                    getFormDFValue("U_IDHRDTHU") = "" And _
                                    getFormDFValue("U_IDHRDFRI") = "" And _
                                    getFormDFValue("U_IDHRDSAT") = "" And _
                                    getFormDFValue("U_IDHRDSUN") = "" Then
                        sError &= "Action Day,"
                    End If
                End Try

                If sError.Length > 0 Then
                   com.idh.bridge.DataHandler.INSTANCE.doError("Mandatory fields not set.", "The following fields must be set: " & sError)
                    Return False
                Else
                    Return True
                End If

            Catch ex As Exception
               com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error validating fields.")
            End Try
        End Function

        Private Sub doDescribeCoverage()
            Dim sPreBook As String = getFormDFValue("U_IDHRECOM")
            Dim sPreBookDesc, Code As String
            Dim Pattern As String = getFormDFValue("U_IDHRECUR")
            Dim Day As Integer
            Dim Count As Integer = 0

            If Not getFormDFValue("Code") = Nothing Then
                Code = getFormDFValue("Code")
            Else
                Code = "TBA"
            End If
            
            'Some issue with the Option's on values => D = 1, Y = 2
            Select Case Pattern
                Case "1"
                    sPreBookDesc = "PBI# " & Code & " : Daily intervals as of " & com.idh.utils.dates.doStrToDate(getDFValue("@IDH_PBI", "U_IDHRECSD")) & " "
                Case "W"
                    sPreBookDesc = "PBI# " & Code & " : Weekly intervals as of " & com.idh.utils.dates.doStrToDate(getDFValue("@IDH_PBI", "U_IDHRECSD")) & " "
                Case "O"
                    sPreBookDesc = "PBI# " & Code & " : Weekly intervals as of " & com.idh.utils.dates.doStrToDate(getDFValue("@IDH_PBI", "U_IDHRECSD")) & " "
                Case "M"
                    sPreBookDesc = "PBI# " & Code & " : Monthly intervals as of " & com.idh.utils.dates.doStrToDate(getDFValue("@IDH_PBI", "U_IDHRECSD")) & " "
                Case "2"
                    sPreBookDesc = "PBI# " & Code & " : Yearly intervals as of " & com.idh.utils.dates.doStrToDate(getDFValue("@IDH_PBI", "U_IDHRECSD")) & " "
                Case Else
                    sPreBookDesc = ""
            End Select
            
            'Some issue with the Option's on values => D = 1, Y = 2
			If Pattern = "M" Then
				If Not getFormDFValue("U_IDHRACTW") = Nothing Then
	                sPreBookDesc &= " to be executed every " & getFormDFValue("U_IDHRACTW") & " months "
				End If
			ElseIf Pattern = "2" Then
				If Not getFormDFValue("U_IDHRACTW") = Nothing Then
	                sPreBookDesc &= " to be executed every " & getFormDFValue("U_IDHRACTW") & " years "
				End If
				
				Dim sEFType As String = getFormDFValue("U_EFType")
				Dim iEFMonth As Integer = getFormDFValue("U_EFMonth")
				Dim sEFMonth As String = com.idh.bridge.lookups.FixedValues.getMonthFromIndex(iEFMonth)
				'Option => D = 1, W = 2
				If Not sEFType = Nothing Then
					If sEFType = "1" Then
						Dim iEFDay As String = getFormDFValue("U_EFDay")
						
						sPreBookDesc &= "on day " & iEFDay & " of " & sEFMonth
					Else
						Dim iMFWeek As Integer = getFormDFValue("U_EFWeek")
            			Dim iMFDOW As Integer =  getFormDFValue("U_EFDOW")
            			
            			Dim sMFWeek As String = com.idh.bridge.lookups.FixedValues.getPosiotionFromIndex(iMFWeek)
            			Dim sMFDOW As String = com.idh.bridge.lookups.FixedValues.getDayOfWeekFromIndex(iMFDOW)
            		
            			sPreBookDesc &= " " & Translation.getTranslatedWord("on the") & " " & sMFWeek & " " & sMFDOW & " of " & sEFMonth
					End If
				End If
			Else
	            If Not getFormDFValue("U_IDHRACTW") = Nothing Then
	                sPreBookDesc &= " to be executed every " & getFormDFValue("U_IDHRACTW") & " weeks "
	            End If
	
	            sPreBookDesc &= "on the following day(s):"
	            If Not getFormDFValue("U_IDHRDMON") = Nothing Then
	                Day = getFormDFValue("U_IDHRDMON")
	                If Day = 1 Then
	                    sPreBookDesc &= " Mon,"
	                    Count += 1
	                End If
	            End If
	
	            If Not getFormDFValue("U_IDHRDTUE") = Nothing Then
	                Day = getFormDFValue("U_IDHRDTUE")
	                If Day = 1 Then
	                    sPreBookDesc &= " Tue,"
	                    Count += 1
	                End If
	            End If
	
	            If Not getFormDFValue("U_IDHRDWED") = Nothing Then
	                Day = getFormDFValue("U_IDHRDWED")
	                If Day = 1 Then
	                    sPreBookDesc &= " Wed,"
	                    Count += 1
	                End If
	            End If
	
	            If Not getFormDFValue("U_IDHRDTHU") = Nothing Then
	                Day = getFormDFValue("U_IDHRDTHU")
	                If Day = 1 Then
	                    sPreBookDesc &= " Thu,"
	                    Count += 1
	                End If
	            End If
	
	            If Not getFormDFValue("U_IDHRDFRI") = Nothing Then
	                Day = getFormDFValue("U_IDHRDFRI")
	                If Day = 1 Then
	                    sPreBookDesc &= " Fri,"
	                    Count += 1
	                End If
	            End If
	
	            If Not getFormDFValue("U_IDHRDSAT") = Nothing Then
	                Day = getFormDFValue("U_IDHRDSAT")
	                If Day = 1 Then
	                    sPreBookDesc &= " Sat,"
	                    Count += 1
	                End If
	            End If
	
	            If Not getFormDFValue("U_IDHRDSUN") = Nothing Then
	                Day = getFormDFValue("U_IDHRDSUN")
	                If Day = 1 Then
	                    sPreBookDesc &= " Sun,"
	                    Count += 1
	                End If
	            End If
            	sPreBookDesc.Remove(sPreBookDesc.LastIndexOf(","), 1)
			End If

            sPreBookDesc &= ". Termination "

			Dim iOccurance As Integer = Conversions.ToInt( getFormDFValue("U_IDHRECCE") )
            If iOccurance <> 0 Then
		        sPreBookDesc &= " after " & getFormDFValue("U_IDHRECCE") & " occurences."
            Else
            	Dim sTermDate As String = getFormDFValue("U_IDHRECED")
                If Not sTermDate = Nothing AndAlso sTermDate.Length > 0 Then
                    sPreBookDesc &= " on " & com.idh.utils.dates.doStrToDate(getFormDFValue("U_IDHRECED")) & "."
                End If
            End If

            If Not getFormDFValue("U_IDHRECCT") = Nothing Then
                sPreBookDesc &= " -- RunCount = " & getFormDFValue("U_IDHRECCT")
            End If


            setFormDFValue("U_IDHRECOM", sPreBookDesc)
        End Sub

#End Region

#Region "Reports"
		Protected Sub doReportByBtn(ByVal sBtn As String)
			Dim sReportFile As String = Nothing
			Dim sRepId As String
			Select Case sBtn
				Case "IDHPO"
					sRepId = "PBIPOR"
					sReportFile = Config.INSTANCE.getParamater(sRepId)
				Case "IDHDOC"
					sRepId = "PBIDOCR"
					sReportFile = Config.INSTANCE.getParamater(sRepId)
				Case "IDHCUST"
					sRepId = "PBICUSTR"
					sReportFile = Config.INSTANCE.getParamater(sRepId)
				Case Else
					sRepId = "DontKnow"
			End Select
			
			If Not sReportFile Is Nothing AndAlso sReportFile.Length > 0 Then
				doReport(sReportFile)
			Else
				com.idh.bridge.DataHandler.INSTANCE.doError("The report was not set: " & sRepId )
			End If
		End Sub
		
		Protected Overridable Sub doReport(ByVal sReportFile As String)
           	Dim sPBICode As String = getFormDFValue("Code")

        	If sPBICode Is Nothing Then
        		sPBICode = ""
        	End If
        	
        	If Not sReportFile Is Nothing AndAlso _
        		sReportFile.Length > 0 Then

				Dim oParams As New Hashtable
	            oParams.Add("PBINum", sPBICode)

				IDHAddOns.idh.report.Base.doCallReportDefaults( SBOForm, sReportFile, "TRUE", "FALSE", "1", oParams, IDHForm.gsType )
        	End If
        End Sub
#End Region

    End Class
End Namespace
