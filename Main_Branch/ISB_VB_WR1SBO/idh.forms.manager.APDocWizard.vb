﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.controls
Imports com.idh.controls.strct
Imports System

Imports com.idh.utils.Conversions
Imports com.idh.bridge.data
Imports WR.idh.controls.grid

Namespace idh.forms.manager
    Public Class APDocWizard
        Inherits IDHAddOns.idh.forms.Base

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHAPDWZ", "IDHPS", iMenuPosition, "APDocWizard.srf", False, True, False, "AP Doc. Generation Wizard", load_Types.idh_LOAD_NORMAL)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                'Add total field 
                doAddUF(oForm, "IDH_TOTAL", SAPbouiCOM.BoDataType.dt_PRICE, 20, False, False)

                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "APGRID")
                If oGridN Is Nothing Then
                    oGridN = New FilterGrid(Me, oForm, "APGRID", 7, 70, 1050, 322, False)
                End If

                oGridN.getSBOGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto

                oForm.AutoManaged = False
                oGridN.getSBOItem().AffectsFormMode = False
                oForm.SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_Find

                'Add Flag to close PO field 
                Dim oItem As SAPbouiCOM.Item
                oItem = doAddUFCheck(oForm, "IDH_POFLAG", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oItem.AffectsFormMode = False

                'Add Null filter field for DPR Ref 
                'Dim oItem As SAPbouiCOM.Item
                oItem = doAddUFCheck(oForm, "IDH_POOHLD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oItem.AffectsFormMode = False

                ''IDH_EXSELF
                'oItem = doAddUFCheck(oForm, "IDH_EXSELF", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                'oItem.AffectsFormMode = False

                oItem = doAddUF(oForm, "IDH_LKPSBL", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 100, False, False)
                oItem.DisplayDesc = True
                oItem.AffectsFormMode = False
                doSelfBillCombo(oForm)

                oItem = doAddUF(oForm, "IDH_SCHADH", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 100, False, False)
                oItem.DisplayDesc = True
                oItem.AffectsFormMode = False
                doAddComboBoxValidValues(oForm)

                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing form.")
            End Try
        End Sub

        Protected Overridable Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddFilterField("DATESTART", "vAPG.ActStartDt", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("DATEEND", "vAPG.ActEndDt", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_SUPPCD", "vAPG.SupplierCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_SUPPNM", "vAPG.SupplierNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_CONTCD", "vAPG.ContainerCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_CONTNM", "vAPG.ContainerNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WOHNO", "vAPG.OrderNo", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WORNO", "vAPG.OrderRowNo", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_SITEID", "vAPG.CustSiteID", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WASCD", "vAPG.WasteCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ADDRES", "vAPG.CustAddress", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_STZPCD", "vAPG.CustZpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_UNTSIP", "vAPG.UnitSIP", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_CUSTCD", "vAPG.CustCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_CUSTNM", "vAPG.CustNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_POOHLD", "vAPG.OnHold", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 1)
            oGridN.doAddFilterField("IDH_LKPSBL", "vAPG.SelfBillCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 1)
            oGridN.doAddFilterField("IDH_SCHADH", "vAPG.IsScheduled", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 1)
            oGridN.doAddFilterField("IDH_REASON", "vAPG.HoldComments", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
        End Sub

        Protected Overridable Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("vAPG.IsDrafted", "Is Drafted", False, -1, Nothing, Nothing, -1)
            oGridN.doAddListField("vAPG.DraftDocNumber", "Draft Doc#", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Drafts)

            oGridN.doAddListField("vAPG.DocEntry", "PO", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_PurchaseOrder)
            oGridN.doAddListField("vAPG.LineNum", "PO Row", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.DocNum", "PO DocNum", False, -1, Nothing, Nothing)

            'Add Supplier details from POH 
            oGridN.doAddListField("vAPG.SupplierCd", "Supplier Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.SupplierNm", "Supplier Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.CustCd", "Customer Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.CustNm", "Customer Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.ActStartDt", "Act. Start Date", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.ActEndDt", "Act. End Date", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.OrderNo", "Order No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.OrderRowNo", "Order Row", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.Rebate", "Rebate", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.CustSiteID", "Site ID", False, 0, Nothing, Nothing)
            oGridN.doAddListField("vAPG.CustAddress", "Site Address", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.CustZpCd", "Site Post Code", False, -1, Nothing, Nothing)

            oGridN.doAddListField("vAPG.ItemCd", "Item No", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.ItemNm", "Item Description", False, -1, Nothing, Nothing)

            oGridN.doAddListField("vAPG.ContainerCd", "Container Cd", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.ContainerNm", "Container Name", False, -1, Nothing, Nothing)

            oGridN.doAddListField("vAPG.ContainerGrp", "Container Group", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.OrderType", "Order Type", False, -1, Nothing, Nothing)

            oGridN.doAddListField("vAPG.WasteCd", "Waste Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.WasteNm", "Waste Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.UnitSIP", "Unit SIP", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.Quantity", "Quantity", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.RowTotal", "Row Total", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.OpenQuantity", "Open Quantity", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.OpenRowTotal", "Open Row Total", False, -1, Nothing, Nothing)


            oGridN.doAddListField("vAPG.OnHold", "OnHold", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.HoldComments", "Reason", False, -1, Nothing, Nothing)

            oGridN.doAddListField("vAPG.SelfBillCode", "Is SelfBill", False, -1, Nothing, Nothing)
            oGridN.doAddListField("vAPG.IsScheduled", "Is Scheduled", False, -1, Nothing, Nothing)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            Try
                'doSelfBillCombo(oForm)

                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "APGRID")
                If oGridN Is Nothing Then
                    oGridN = New FilterGrid(Me, oForm, "APGRID", 7, 70, 1050, 322, True)
                End If

                oGridN.doAddGridTable(New GridTable("IDH_APDOCGEN", "vAPG", "DocEntry", False, True), True)

                Dim sReqFilter As String = " vAPG.IsDrafted != 'Zz-Ignore' "
                oGridN.setRequiredFilter(sReqFilter)

                doSetFilterFields(oGridN)
                doSetListFields(oGridN)

                oGridN.setInitialFilterValue("vAPG.DocEntry = -1")

                If ghOldDialogParams.Contains(oForm.UniqueID) Then
                    Dim oData As ArrayList = ghOldDialogParams.Item(oForm.UniqueID)
                    If oData IsNot Nothing AndAlso oData.Count > 0 Then
                        Dim sSuppCd As String = oData.Item(0)
                        Dim sSuppNm As String = oData.Item(1)

                        setUFValue(oForm, "IDH_SUPPNM", sSuppNm)
                        oGridN.doReloadData()
                    End If
                End If

                If getHasSharedData(oForm) Then
                    setUFValue(oForm, "IDH_SUPPNM", getParentSharedData(oForm, "IDH_SUPPNM"))
                    oGridN.doReloadData()
                End If

                oForm.Items.Item("1").Specific.Caption = getTranslatedWord("Generate AP Document...")
                doAddWF((oForm), "APHasChanged", False)
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error preloading data.")
            End Try
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            Try
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "APGRID")

                'get Flag to Close checkbox value 
                Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("IDH_POFLAG")
                Dim chkToClose As SAPbouiCOM.CheckBox = oItem.Specific
                Dim sReqFilter As String = oGridN.getRequiredFilter()

                If chkToClose IsNot Nothing AndAlso chkToClose.Checked Then
                    If sReqFilter.Contains(" AND (IsNull(vAPG.FlagToClose, '') = '' OR IsNull(vAPG.FlagToClose, '') = 'N') ") Then
                        sReqFilter = sReqFilter.Replace(" AND (IsNull(vAPG.FlagToClose, '') = '' OR IsNull(vAPG.FlagToClose, '') = 'N')", " AND IsNull(vAPG.FlagToClose, '') = 'Y' ")
                    Else
                        If Not sReqFilter.Contains(" AND IsNull(vAPG.FlagToClose, '') = 'Y' ") Then
                            sReqFilter = sReqFilter + " AND IsNull(vAPG.FlagToClose, '') = 'Y' "
                        End If
                    End If
                Else
                    If sReqFilter.Contains(" AND IsNull(vAPG.FlagToClose, '') = 'Y' ") Then
                        sReqFilter = sReqFilter.Replace(" AND IsNull(vAPG.FlagToClose, '') = 'Y' ", " AND (IsNull(vAPG.FlagToClose, '') = '' OR IsNull(vAPG.FlagToClose, '') = 'N') ")
                    Else
                        If Not sReqFilter.Contains(" AND (IsNull(vAPG.FlagToClose, '') = '' OR IsNull(vAPG.FlagToClose, '') = 'N') ") Then
                            sReqFilter = sReqFilter + " AND (IsNull(vAPG.FlagToClose, '') = '' OR IsNull(vAPG.FlagToClose, '') = 'N') "
                        End If
                    End If
                End If


                'get On-hold checkbox value 
                oItem = oForm.Items.Item("IDH_POOHLD")
                Dim chkOnhold As SAPbouiCOM.CheckBox = oItem.Specific
                'sReqFilter = oGridN.getRequiredFilter()

                If chkOnhold IsNot Nothing AndAlso chkOnhold.Checked Then
                    If sReqFilter.Contains(" AND (IsNull(vAPG.OnHold, '') = '' OR IsNull(vAPG.OnHold, '') = 'N') ") Then
                        sReqFilter = sReqFilter.Replace(" AND (IsNull(vAPG.OnHold, '') = '' OR IsNull(vAPG.OnHold, '') = 'N')", " AND IsNull(vAPG.OnHold, '') = 'Y' ")
                    Else
                        If Not sReqFilter.Contains(" AND IsNull(vAPG.OnHold, '') = 'Y' ") Then
                            sReqFilter = sReqFilter + " AND IsNull(vAPG.OnHold, '') = 'Y' "
                        End If
                    End If
                Else
                    If sReqFilter.Contains(" AND IsNull(vAPG.OnHold, '') = 'Y' ") Then
                        sReqFilter = sReqFilter.Replace(" AND IsNull(vAPG.OnHold, '') = 'Y' ", " AND (IsNull(vAPG.OnHold, '') = '' OR IsNull(vAPG.OnHold, '') = 'N') ")
                    Else
                        If Not sReqFilter.Contains(" AND (IsNull(vAPG.OnHold, '') = '' OR IsNull(vAPG.OnHold, '') = 'N') ") Then
                            sReqFilter = sReqFilter + " AND (IsNull(vAPG.OnHold, '') = '' OR IsNull(vAPG.OnHold, '') = 'N') "
                        End If
                    End If
                End If

                '''Self Bill Filter 
                ''oItem = oForm.Items.Item("IDH_LKPSBL")
                ''Dim oLKPSelfBill As SAPbouiCOM.ComboBox = oItem.Specific
                ''Dim oValid As SAPbouiCOM.ValidValues
                ''Dim oSelected As SAPbouiCOM.ValidValue
                ''oValid = oLKPSelfBill.ValidValues
                ''oSelected = oLKPSelfBill.Selected

                ''Dim sSelfBill As String

                ''If Not oValid Is Nothing AndAlso _
                ''    oValid.Count > 0 AndAlso _
                ''    Not oSelected Is Nothing Then
                ''    sSelfBill = oLKPSelfBill.Selected.Value()
                ''Else
                ''    sSelfBill = ""
                ''End If

                ''If sSelfBill = "" Then
                ''    oGridN.doRemoveGridTable("IDH_VSELFBIL", "s")
                ''    If sReqFilter.Contains(" AND IsNull(s.RCode, '') = IsNull(vAPG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') = ''  ") Then
                ''        sReqFilter = sReqFilter.Replace(" AND IsNull(s.RCode, '') = IsNull(vAPG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') = ''  ", "")
                ''    ElseIf sReqFilter.Contains(" AND IsNull(s.RCode, '') = IsNull(vAPG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') <> ''  ") Then
                ''        sReqFilter = sReqFilter.Replace(" AND IsNull(s.RCode, '') = IsNull(vAPG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') <> ''  ", "")
                ''    End If
                ''ElseIf sSelfBill = "Y" Then
                ''    oGridN.doAddGridTable(New GridTable("IDH_VSELFBIL", "s"))
                ''    'sReqFilter = sReqFilter + " AND IsNull(s.RCode, '') = IsNull(vAPG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') <> ''  "
                ''    If sReqFilter.Contains(" AND IsNull(s.RCode, '') = IsNull(vAPG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') = ''  ") Then
                ''        sReqFilter = sReqFilter.Replace(" AND IsNull(s.RCode, '') = IsNull(vAPG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') = ''  ", " AND IsNull(s.RCode, '') = IsNull(vAPG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') <> ''  ")
                ''    Else
                ''        If Not sReqFilter.Contains(" AND IsNull(s.RCode, '') = IsNull(vAPG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') <> ''  ") Then
                ''            sReqFilter = sReqFilter + " AND IsNull(s.RCode, '') = IsNull(vAPG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') <> ''  "
                ''        End If
                ''    End If
                ''ElseIf sSelfBill = "N" Then
                ''    oGridN.doAddGridTable(New GridTable("IDH_VSELFBIL", "s"))
                ''    'sReqFilter = sReqFilter + " AND IsNull(s.RCode, '') = IsNull(vAPG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') = ''  "
                ''    If sReqFilter.Contains(" AND IsNull(s.RCode, '') = IsNull(vAPG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') <> ''  ") Then
                ''        sReqFilter = sReqFilter.Replace(" AND IsNull(s.RCode, '') = IsNull(vAPG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') <> ''  ", " AND IsNull(s.RCode, '') = IsNull(vAPG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') = ''  ")
                ''    Else
                ''        If Not sReqFilter.Contains(" AND IsNull(s.RCode, '') = IsNull(vAPG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') = ''  ") Then
                ''            sReqFilter = sReqFilter + " AND IsNull(s.RCode, '') = IsNull(vAPG.OrderRowNo, '') AND IsNull(s.SelfBillCode, '') = ''  "
                ''        End If
                ''    End If
                ''End If

                oGridN.setRequiredFilter(sReqFilter)

                oGridN.doReloadData()
                doSetGridTotals(oForm)
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Loading Data.")
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Try
                oForm.Freeze(True)
                If pVal.BeforeAction Then
                    If pVal.ItemUID = "1" Then
                        Dim bIsValid As Boolean = False

                        'Validate selection and Supplier filter requirement 
                        Dim oItm As SAPbouiCOM.EditText
                        oItm = oForm.Items.Item("IDH_SUPPCD").Specific
                        Dim sFilterSup As String = oItm.Value

                        If sFilterSup.Length > 0 Then
                            bIsValid = True
                        End If

                        If bIsValid Then
                            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "APGRID")
                            Dim oSelectedRows As SAPbouiCOM.SelectedRows
                            oSelectedRows = oGridN.getSBOGrid.Rows.SelectedRows
                            Dim iRowCnt As Integer = oSelectedRows.Count
                            Dim oData As ArrayList = Nothing

                            If iRowCnt > 0 Then
                                Dim oAPInvoice As SAPbobsCOM.Documents
                                'oAPInvoice = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseInvoices)
                                oAPInvoice = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts)

                                Dim sSUPCardCd As String = oGridN.doGetFieldValue("vAPG.SupplierCd")
                                Dim sSUPCardNm As String = oGridN.doGetFieldValue("vAPG.SupplierNm")

                                oAPInvoice.CardCode = sSUPCardCd
                                oAPInvoice.CardName = sSUPCardNm

                                'Check Admin Info for Supplier Ref. No. 
                                Dim oAdminInfo As SAPbobsCOM.AdminInfo = goParent.goDICompany.GetCompanyService.GetAdminInfo()
                                Dim oExtAdminInfo As SAPbobsCOM.ExtendedAdminInfo

                                oItm = oForm.Items.Item("IDH_SUPREF").Specific
                                Dim sSupRef As String = oItm.Value
                                oAPInvoice.NumAtCard = sSupRef

                                'Set the AP Invoice to Draft 
                                oAPInvoice.DocObjectCode = SAPbobsCOM.BoObjectTypes.oPurchaseInvoices

                                Dim oDraftDocNumberList As ArrayList = oGridN.doPrepareListFromSelection("vAPG.DraftDocNumber")

                                Dim oPONos As ArrayList = oGridN.doPrepareListFromSelection("vAPG.DocEntry")
                                Dim oPOLineNos As ArrayList = oGridN.doPrepareListFromSelection("vAPG.LineNum")

                                Dim sParams(3) As String
                                sParams(0) = sSUPCardCd
                                sParams(1) = sSUPCardNm
                                sParams(2) = ""

                                For iLines As Integer = 0 To iRowCnt - 1
                                    If iLines > 0 Then
                                        oAPInvoice.Lines.Add()
                                        sParams(2) = sParams(2) + ", "
                                    End If

                                    oAPInvoice.Lines.ItemCode = oGridN.doGetFieldValue("vAPG.ItemCd", oGridN.getSBOGrid.GetDataTableRowIndex(oSelectedRows.Item(iLines, SAPbouiCOM.BoOrderType.ot_RowOrder)))
                                    oAPInvoice.Lines.ItemDescription = oGridN.doGetFieldValue("vAPG.ItemNm", oGridN.getSBOGrid.GetDataTableRowIndex(oSelectedRows.Item(iLines, SAPbouiCOM.BoOrderType.ot_RowOrder)))

                                    oAPInvoice.Lines.BaseEntry = oGridN.doGetFieldValue("vAPG.DocEntry", oGridN.getSBOGrid.GetDataTableRowIndex(oSelectedRows.Item(iLines, SAPbouiCOM.BoOrderType.ot_RowOrder)))
                                    oAPInvoice.Lines.BaseLine = oGridN.doGetFieldValue("vAPG.LineNum", oGridN.getSBOGrid.GetDataTableRowIndex(oSelectedRows.Item(iLines, SAPbouiCOM.BoOrderType.ot_RowOrder)))
                                    oAPInvoice.Lines.BaseType = SAPbobsCOM.BoObjectTypes.oPurchaseOrders

                                    sParams(2) = sParams(2) + "PO: " + oAPInvoice.Lines.BaseEntry.ToString() + " - " + oAPInvoice.Lines.BaseLine.ToString()
                                Next

                                'Get user confirmation 
                                If goParent.doResourceMessageYN("APDOCMSG", sParams, 1) = 1 Then
                                    goParent.goDICompany.StartTransaction()

                                    Dim iResult As Integer = oAPInvoice.Add()
                                    Dim sResult As String = Nothing
                                    If iResult <> 0 Then
                                        BubbleEvent = False
                                        goParent.goDICompany.GetLastError(iResult, sResult)
                                        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & sResult, "Error doing AP Invoice Generation. " & sResult)
                                    Else
                                        'reset Supplier Ref# 
                                        oItm.Value = ""

                                        Dim sFinalResult As String = ""
                                        sFinalResult = doRemoveDraftDocument(oForm, oDraftDocNumberList)

                                        If sFinalResult.Length > 0 Then
                                            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                            com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & sFinalResult, "Error removing draft document(s).")
                                        Else
                                            If goParent.goDICompany.InTransaction Then
                                                goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                                            End If
                                            doWarnMess("Draft AP Invoice generated successfully. You can review the document and post it in the system.", SAPbouiCOM.BoMessageTime.bmt_Medium)
                                            setSharedData(oForm, "IDH_SUPCD", sFilterSup)
                                            goParent.doOpenModalForm("IDHDRAFTSRC", oForm)
                                        End If
                                    End If

                                    doLoadData(oForm)
                                End If
                            Else
                                doWarnMess("No rows selected.", SAPbouiCOM.BoMessageTime.bmt_Medium)
                            End If
                        Else
                            com.idh.bridge.DataHandler.INSTANCE.doError("The AP Invoice will be generated against the supplier. Provide Supplier filter.")
                        End If
                    End If
                End If
            Catch ex As Exception
                If goParent.goDICompany.InTransaction Then
                    goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                End If
                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error doing AP Invoice Generation.")
                com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            'If Not BubbleEvent Then
            '    Me.setWFValue((oForm), "APHasChanged", False)
            '    Return False
            'End If
            ''If (pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE) Then
            ''    'If ((pVal.BeforeAction = False) AndAlso Not pVal.ItemUID.Equals("APGRID")) Then
            ''    '    If pVal.ItemChanged Then
            ''    '        setWFValue((oForm), "APHasChanged", True)
            ''    '    Else
            ''    '        setWFValue((oForm), "APHasChanged", False)
            ''    '    End If
            ''    'End If
            ''ElseIf (pVal.EventType = SAPbouiCOM.BoEventTypes.et_LOST_FOCUS) Then
            ''    'If Not pVal.BeforeAction Then
            ''    '    If getWFValue((oForm), "APHasChanged").Equals(False) Then
            ''    '        Dim oGridN As FilterGrid = DirectCast(IDHGrid.getInstance((oForm), "APGRID"), FilterGrid)
            ''    '        If (oGridN.getGridControl.getFilterFields.indexOfFilterField(pVal.ItemUID) > -1) Then
            ''    '            doLoadData((oForm))
            ''    '        End If
            ''    '    End If
            ''    '    setWFValue((oForm), "APHasChanged", False)
            ''    'End If
            ''ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_CLICK Then
            ''    'If pVal.BeforeAction Then
            ''    '    If pVal.ItemUID = "IDH_FIND" Then
            ''    '        doLoadData(oForm)
            ''    '    ElseIf pVal.ItemUID = "IDH_RESET" Then
            ''    '        'clear filter fields 
            ''    '        doResetFilters(oForm)
            ''    '    ElseIf pVal.ItemUID = "IDH_DRFTRP" Then
            ''    '        Dim oItm As SAPbouiCOM.EditText
            ''    '        oItm = oForm.Items.Item("IDH_SUPPCD").Specific
            ''    '        Dim sFilterSup As String = oItm.Value
            ''    '        setSharedData(oForm, "IDH_SUPCD", sFilterSup)
            ''    '        goParent.doOpenModalForm("IDHDRAFTSRC", oForm)
            ''    '    End If
            ''    'Else
            ''    '    If pVal.ItemUID = "IDH_POOHLD" Then
            ''    '        Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("IDH_POOHLD")
            ''    '        Dim chkOnhold As SAPbouiCOM.CheckBox = oItem.Specific
            ''    '        If chkOnhold IsNot Nothing AndAlso chkOnhold.Checked Then
            ''    '            oForm.Items.Item("1").Enabled = True
            ''    '        Else
            ''    '            oForm.Items.Item("1").Enabled = False
            ''    '        End If
            ''    '    End If
            ''    'End If
            ''ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
            ''    If pVal.BeforeAction Then
            ''        If pVal.ItemUID = "IDH_FIND" Then
            ''            doLoadData(oForm)
            ''        ElseIf pVal.ItemUID = "IDH_RESET" Then
            ''            'clear filter fields 
            ''            doResetFilters(oForm)
            ''        ElseIf pVal.ItemUID = "IDH_DRFTRP" Then
            ''            Dim oItm As SAPbouiCOM.EditText
            ''            oItm = oForm.Items.Item("IDH_SUPPCD").Specific
            ''            Dim sFilterSup As String = oItm.Value
            ''            setSharedData(oForm, "IDH_SUPCD", sFilterSup)
            ''            goParent.doOpenModalForm("IDHDRAFTSRC", oForm)
            ''        End If
            ''    Else
            ''        If pVal.ItemUID = "IDH_POOHLD" Then
            ''            Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("IDH_POOHLD")
            ''            Dim chkOnhold As SAPbouiCOM.CheckBox = oItem.Specific
            ''            If chkOnhold IsNot Nothing AndAlso chkOnhold.Checked Then


            ''                oForm.Items.Item("1").Enabled = False
            ''            Else
            ''                oForm.Items.Item("1").Enabled = True
            ''            End If
            ''        End If
            ''    End If
            ''End If
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction Then
                    If pVal.ItemUID = "IDH_FIND" Then
                        doLoadData(oForm)
                    ElseIf pVal.ItemUID = "IDH_RESET" Then
                        'clear filter fields 
                        doResetFilters(oForm)
                    ElseIf pVal.ItemUID = "IDH_DRFTRP" Then
                        Dim oItm As SAPbouiCOM.EditText
                        oItm = oForm.Items.Item("IDH_SUPPCD").Specific
                        Dim sFilterSup As String = oItm.Value
                        setSharedData(oForm, "IDH_SUPCD", sFilterSup)
                        goParent.doOpenModalForm("IDHDRAFTSRC", oForm)
                    End If
                Else
                    If pVal.ItemUID = "IDH_POOHLD" Then
                        Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("IDH_POOHLD")
                        Dim chkOnhold As SAPbouiCOM.CheckBox = oItem.Specific
                        If chkOnhold IsNot Nothing AndAlso chkOnhold.Checked Then


                            oForm.Items.Item("1").Enabled = False
                        Else
                            oForm.Items.Item("1").Enabled = True
                        End If
                    ElseIf pVal.ItemUID = "IDH_POFLAG" Then
                        Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("IDH_POFLAG")
                        Dim chkFlagPO As SAPbouiCOM.CheckBox = oItem.Specific
                        If chkFlagPO IsNot Nothing AndAlso chkFlagPO.Checked Then
                            oForm.Items.Item("1").Enabled = False
                        Else
                            oForm.Items.Item("1").Enabled = True
                        End If
                    End If
                End If
            End If
            Return False
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oParentForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            'MyBase.doHandleModalResultShared(oParentForm, sModalFormType, sLastButton)
            If sModalFormType = "IDHDRAFTSRC" Then
                doLoadData(oParentForm)
                doSetGridTotals(oParentForm)
            ElseIf sModalFormType = "IDH_COMMFRM" Then
                Dim sTarget As String = getSharedData(oParentForm, "TARGET")
                If sTarget IsNot Nothing AndAlso sTarget.Length > 0 Then
                    Dim sReason As String = getSharedData(oParentForm, "CMMNT")

                    If sTarget = "FLAGPO" Then
                        doFlagToClosePurchaseOrders(oParentForm, "Y", sReason)
                    ElseIf sTarget = "UNFLAGPO" Then
                        doFlagToClosePurchaseOrders(oParentForm, "N", sReason)
                        'ElseIf sTarget = "CLOSEPO" Then
                        '    doClosePurchaseOrders(oParentForm, sReason)
                    ElseIf sTarget = "HOLDPO" Then
                        doHoldPurchaseOrders(oParentForm, "Y", sReason)
                    ElseIf sTarget = "UNHOLDPO" Then
                        doHoldPurchaseOrders(oParentForm, "N", sReason)
                    End If
                End If
            End If
        End Sub

        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK Then
                If pVal.ItemUID = "APGRID" Then
                    If pVal.BeforeAction = False Then
                        'doGridDoubleClick(oForm, pVal)
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
                'Set menu creation package settings 
                If pVal.BeforeAction = True Then
                    'Show Right Click Menu 
                    Dim oGridN As FilterGrid = DirectCast(IDHGrid.getInstance(oForm, "APGRID"), FilterGrid)
                    Dim oMenuItem As SAPbouiCOM.MenuItem
                    Dim sMenuID As String = IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU
                    oMenuItem = goParent.goApplication.Menus.Item(sMenuID)

                    Dim oMenus As SAPbouiCOM.Menus
                    Dim iMenuPos As Integer = 1
                    Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
                    oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
                    oCreationPackage.Enabled = True
                    oMenus = oMenuItem.SubMenus
                    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING

                    'Get selected rows from the grid 
                    Dim oSelectedRows As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows
                    Dim iSelectedRowsCnt As Integer = oSelectedRows.Count

                    If iSelectedRowsCnt > 0 Then
                        'oCreationPackage.UniqueID = "GENAPINV"
                        'oCreationPackage.String = getTranslatedWord("Generate AP Invoice")
                        'oCreationPackage.Position = iMenuPos
                        'iMenuPos += 1
                        'oMenus.AddEx(oCreationPackage)


                        ''get On-hold checkbox value 
                        Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("IDH_POOHLD")
                        Dim chkOnhold As SAPbouiCOM.CheckBox = oItem.Specific

                        If chkOnhold IsNot Nothing AndAlso chkOnhold.Checked Then
                            oCreationPackage.UniqueID = "UNHOLDPO"
                            oCreationPackage.String = getTranslatedWord("Un-hold PO")
                            oCreationPackage.Position = iMenuPos
                            iMenuPos += 1
                            oMenus.AddEx(oCreationPackage)
                        Else
                            oCreationPackage.UniqueID = "HOLDPO"
                            oCreationPackage.String = getTranslatedWord("Hold PO")
                            oCreationPackage.Position = iMenuPos
                            iMenuPos += 1
                            oMenus.AddEx(oCreationPackage)
                        End If

                        'get Flag to Close checkbox 
                        oItem = oForm.Items.Item("IDH_POFLAG")
                        Dim chkToClose As SAPbouiCOM.CheckBox = oItem.Specific

                        If chkToClose IsNot Nothing AndAlso chkToClose.Checked Then
                            oCreationPackage.UniqueID = "UNFLAGPO"
                            oCreationPackage.String = getTranslatedWord("Un-flag PO")
                            oCreationPackage.Position = iMenuPos
                            iMenuPos += 1
                            oMenus.AddEx(oCreationPackage)
                        Else
                            oCreationPackage.UniqueID = "FLAGPO"
                            oCreationPackage.String = getTranslatedWord("Flag to Close PO")
                            oCreationPackage.Position = iMenuPos
                            iMenuPos += 1
                            oMenus.AddEx(oCreationPackage)
                        End If

                        'oCreationPackage.UniqueID = "CLOSEPOR"
                        'oCreationPackage.String = getTranslatedWord("Close PO Rows")
                        'oCreationPackage.Position = iMenuPos
                        'iMenuPos += 1
                        'oMenus.AddEx(oCreationPackage)
                    End If

                Else
                    'Hide Right Click Menu 
                    If goParent.goApplication.Menus.Exists("GENAPINV") Then
                        goParent.goApplication.Menus.RemoveEx("GENAPINV")
                    End If
                    If goParent.goApplication.Menus.Exists("FLAGPO") Then
                        goParent.goApplication.Menus.RemoveEx("FLAGPO")
                    End If
                    If goParent.goApplication.Menus.Exists("UNFLAGPO") Then
                        goParent.goApplication.Menus.RemoveEx("UNFLAGPO")
                    End If
                    If goParent.goApplication.Menus.Exists("UNHOLDPO") Then
                        goParent.goApplication.Menus.RemoveEx("UNHOLDPO")
                    End If
                    If goParent.goApplication.Menus.Exists("HOLDPO") Then
                        goParent.goApplication.Menus.RemoveEx("HOLDPO")
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
                If pVal.BeforeAction = False Then
                    Dim oGridN As FilterGrid = DirectCast(IDHGrid.getInstance(oForm, "APGRID"), FilterGrid)
                    'If pVal.oData.MenuUID.Equals("DUPLICATE") Then
                    '    'Check if this is a valid request
                    '    Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid.Rows.SelectedRows() 'getGrid().Rows.SelectedRows()
                    '    If Not oSelected Is Nothing AndAlso oSelected.Count > 0 Then
                    '        'doDuplicateAdditionalItemRows()
                    '    End If
                    'ElseIf pVal.oData.MenuUID.Equals("CLOSEPOR") Then
                    '    'Check if this is a valid request
                    '    Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid.Rows.SelectedRows() 'getGrid().Rows.SelectedRows()
                    '    If Not oSelected Is Nothing AndAlso oSelected.Count > 0 Then
                    '        doClosePurchaseOrders(oForm)
                    '    End If
                    'ElseIf pVal.oData.MenuUID.Equals("UNHOLDPO") Then
                    '    'Check if this is a valid request
                    '    Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid.Rows.SelectedRows() 'getGrid().Rows.SelectedRows()
                    '    If Not oSelected Is Nothing AndAlso oSelected.Count > 0 Then
                    '        doHoldPurchaseOrders(oForm, "N")
                    '    End If
                    'ElseIf pVal.oData.MenuUID.Equals("HOLDPO") Then
                    '    'Check if this is a valid request
                    '    Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid.Rows.SelectedRows() 'getGrid().Rows.SelectedRows()
                    '    If Not oSelected Is Nothing AndAlso oSelected.Count > 0 Then
                    '        doHoldPurchaseOrders(oForm, "Y")
                    '    End If
                    'End If
                    Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid.Rows.SelectedRows() 'getGrid().Rows.SelectedRows()
                    If Not oSelected Is Nothing AndAlso oSelected.Count > 0 Then

                        Dim oSONos As ArrayList = oGridN.doPrepareListFromSelection("vAPG.DocEntry")
                        Dim sParams(1) As String
                        sParams(0) = String.Join(",", oSONos.ToArray(GetType(String)))

                        If pVal.oData.MenuUID.Equals("FLAGPO") Then
                            If goParent.doResourceMessageYN("APDCLSPO", sParams, 1) = 1 Then
                                setSharedData(oForm, "TARGET", "FLAGPO")
                                PARENT.doOpenModalForm("IDH_COMMFRM", oForm)
                            End If
                        ElseIf pVal.oData.MenuUID.Equals("UNFLAGPO") Then
                            If goParent.doResourceMessageYN("APDUCLPO", sParams, 1) = 1 Then
                                setSharedData(oForm, "TARGET", "UNFLAGPO")
                                PARENT.doOpenModalForm("IDH_COMMFRM", oForm)
                            End If
                        ElseIf pVal.oData.MenuUID.Equals("UNHOLDPO") Then
                            If goParent.doResourceMessageYN("AUNHLDPO", sParams, 1) = 1 Then
                                setSharedData(oForm, "TARGET", "UNHOLDPO")
                                PARENT.doOpenModalForm("IDH_COMMFRM", oForm)
                            End If
                        ElseIf pVal.oData.MenuUID.Equals("HOLDPO") Then
                            If goParent.doResourceMessageYN("APDHLDPO", sParams, 1) = 1 Then
                                setSharedData(oForm, "TARGET", "HOLDPO")
                                PARENT.doOpenModalForm("IDH_COMMFRM", oForm)
                            End If
                        End If
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_SELECT OrElse _
                    pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DESELECT OrElse _
                    pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MULTI_SELECT Then
                If pVal.BeforeAction = False Then
                    doSetGridTotals(oForm)
                    Return True
                End If
            End If
            Return True
        End Function

        Public Overrides Sub doClose()
        End Sub

        Public Sub doSetGridTotals(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "APGRID")
            Dim oSelectedRows As SAPbouiCOM.SelectedRows
            Dim iRowCount As Integer
            Dim oRowTotalsData As ArrayList
            Dim dTotal As Double = 0

            Try
                oSelectedRows = oGridN.getSBOGrid.Rows.SelectedRows
                iRowCount = oSelectedRows.Count

                If iRowCount > 0 Then
                    oRowTotalsData = oGridN.doPrepareListFromSelection("vAPG.RowTotal")
                    Dim sNewVal As String
                    For i As Integer = 0 To iRowCount - 1
                        sNewVal = ""
                        sNewVal = oGridN.doGetFieldValue("vAPG.RowTotal", oGridN.getSBOGrid.GetDataTableRowIndex(oSelectedRows.Item(i, SAPbouiCOM.BoOrderType.ot_RowOrder)))
                        If sNewVal.Length > 0 Then
                            dTotal = dTotal + Convert.ToDouble(sNewVal)
                        Else
                            dTotal = dTotal + Convert.ToDouble(0)
                        End If
                    Next
                    setUFValue(oForm, "IDH_TOTAL", dTotal)
                Else
                    setUFValue(oForm, "IDH_TOTAL", 0)
                End If
            Catch ex As Exception
            Finally
                oGridN = Nothing
                oSelectedRows = Nothing
                iRowCount = Nothing
                oRowTotalsData = Nothing
                dTotal = Nothing
                'Garbage collection 
                GC.Collect()
            End Try
        End Sub

        Public Sub doSetGridRowsColor(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "APGRID")
            Dim iRowCount As Integer

            Dim sIsDraftCol As String
            sIsDraftCol = sIsDraftCol + " CASE WHEN (select count(*) from DRF1 DRF WHERE drf.BaseEntry = vAPG.DocEntry AND drf.BaseLine = vAPG.LineNum ) >= 1 "
            sIsDraftCol = sIsDraftCol + " THEN 'DRAFTED' "
            sIsDraftCol = sIsDraftCol + " ELSE '' END "

            Try
                Dim oCmnSetting As SAPbouiCOM.CommonSetting
                oCmnSetting = oGridN.getSBOGrid.CommonSetting
                iRowCount = oGridN.getSBOGrid.Rows.Count

                Dim iColorRowNo As Integer
                For ii As Integer = 1 To iRowCount
                    If oGridN.doGetFieldValue("Is Drafted", ii) = "DRAFTED" Then
                        If ii = 1 Then
                            iColorRowNo = ii - 1
                        Else
                            iColorRowNo = ii + 1
                        End If
                        oCmnSetting.SetRowBackColor(iColorRowNo, (0 * 65536 + 255 * 256 + 0))
                    End If
                Next
            Catch ex As Exception

            End Try

        End Sub

        Public Sub doResetFilters(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)

            Dim oEdit As SAPbouiCOM.EditText

            oEdit = oForm.Items.Item("DATESTART").Specific
            oEdit.Value = String.Empty
            oEdit = oForm.Items.Item("DATEEND").Specific
            oEdit.Value = String.Empty

            setUFValue(oForm, "DATESTART", "")
            setUFValue(oForm, "DATEEND", "")


            oEdit = oForm.Items.Item("IDH_SUPPCD").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_SUPPNM").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_CONTCD").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_WOHNO").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_WORNO").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_SITEID").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_WASCD").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_ADDRES").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_STZPCD").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_UNTSIP").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_CUSTCD").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_CUSTNM").Specific
            oEdit.Value = ""
            oEdit = oForm.Items.Item("IDH_REASON").Specific
            oEdit.Value = ""

            'Reset checkbox 
            Dim oChkPOHold As SAPbouiCOM.CheckBox
            oChkPOHold = oForm.Items.Item("IDH_POOHLD").Specific
            oChkPOHold.Checked = False

            'Reset Flag to Close PO checkbox
            Dim oChkToClose As SAPbouiCOM.CheckBox
            oChkToClose = oForm.Items.Item("IDH_POFLAG").Specific
            oChkToClose.Checked = False

            'Reset Selfbill LKP 
            Dim oBPStaCombo As SAPbouiCOM.ComboBox
            oBPStaCombo = oForm.Items.Item("IDH_LKPSBL").Specific
            oBPStaCombo.Select(0)

            'Reset Scheduled LKP 
            Dim oSchCombo As SAPbouiCOM.ComboBox
            oSchCombo = oForm.Items.Item("IDH_SCHADH").Specific
            oSchCombo.Select(0)

            'Clear Grid from results now 
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "APGRID")
            If oGridN IsNot Nothing Then
                oGridN.setInitialFilterValue("vAPG.DocEntry = -1")
                oGridN.doReloadData()
            End If

            'doSetFocus(oForm, "DATESTART")
            'doLoadData(oForm) ''Grid is being refreshed with -1 clause to clear all rows. 
            oForm.Refresh()
            oForm.Freeze(False)
        End Sub

        Public Sub doClosePurchaseOrders(ByVal oForm As SAPbouiCOM.Form, ByVal sReason As String)
            Try
                oForm.Freeze(True)
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "APGRID")
                Dim oSelectedRows As SAPbouiCOM.SelectedRows
                oSelectedRows = oGridN.getSBOGrid.Rows.SelectedRows
                Dim iRowCnt As Integer = oSelectedRows.Count
                Dim oData As ArrayList = Nothing

                If iRowCnt > 0 Then
                    Dim sUserCode As String = goParent.goDICompany.UserName
                    Dim oPONos As ArrayList = oGridN.doPrepareListFromSelection("vAPG.DocEntry")
                    Dim oPO As SAPbobsCOM.Documents
                    Dim sResult As String = Nothing
                    Dim sFinalResult As String = ""
                    Dim sParams(1) As String

                    sParams(0) = String.Join(",", oPONos.ToArray(GetType(String)))
                    oPO = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders)

                    goParent.goDICompany.StartTransaction()

                    For iCnt As Integer = 0 To oPONos.Count - 1
                        oPO.GetByKey(Convert.ToInt32(oPONos.Item(iCnt)))
                        oPO.UserFields.Fields.Item("U_HLDCOMT").Value = sReason

                        If idh.const.Lookup.INSTANCE.getParameterAsBool("APNDCMT", True) AndAlso oPO.Comments.Length > 0 Then
                            oPO.Comments = oPO.Comments + Chr(13) + "AP Doc Gen: PO Closed By: '" + sUserCode + "'" + Chr(13) + "On: " + DateTime.Now.ToString()
                        Else
                            oPO.Comments = "AP Doc Gen: PO Closed By: '" + sUserCode + "'" + Chr(13) + "On: " + DateTime.Now.ToString()
                        End If

                        oPO.Update()

                        Dim iResult As Integer = oPO.Close()
                        If iResult <> 0 Then
                            goParent.goDICompany.GetLastError(iResult, sResult)
                            sFinalResult = sFinalResult + Chr(13) + sResult
                            sResult = ""
                            iResult = Nothing
                        End If
                    Next
                    If sFinalResult.Length > 0 Then
                        If goParent.goDICompany.InTransaction Then
                            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                        End If
                        'com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
                        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & sFinalResult, "Error closing Purchase Order(s).")
                    Else
                        If goParent.goDICompany.InTransaction Then
                            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                        End If
                        doWarnMess("Purchase Order(s) Closed successfully.", SAPbouiCOM.BoMessageTime.bmt_Long)
                    End If
                    doLoadData(oForm)
                Else
                    doWarnMess("No rows selected.", SAPbouiCOM.BoMessageTime.bmt_Medium)
                End If

                oForm.Freeze(False)
            Catch ex As Exception
                If goParent.goDICompany.InTransaction Then
                    goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                End If
                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error closing purchase orders. Review Error Log.")
                com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
            End Try

        End Sub

        Public Sub doFlagToClosePurchaseOrders(ByVal oForm As SAPbouiCOM.Form, ByVal sFlagToClose As String, ByVal sReason As String)
            Try
                oForm.Freeze(True)
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "APGRID")
                Dim oSelectedRows As SAPbouiCOM.SelectedRows
                oSelectedRows = oGridN.getSBOGrid.Rows.SelectedRows
                Dim iRowCnt As Integer = oSelectedRows.Count
                Dim oData As ArrayList = Nothing

                If iRowCnt > 0 Then
                    Dim sUserCode As String = goParent.goDICompany.UserName
                    Dim oPONos As ArrayList = oGridN.doPrepareListFromSelection("vAPG.DocEntry")
                    'Dim oPO As SAPbobsCOM.Documents
                    Dim sResult As String = Nothing
                    Dim sFinalResult As String = ""
                    Dim sParams(1) As String

                    sParams(0) = String.Join(",", oPONos.ToArray(GetType(String)))

                    Dim sAlertKey As String = IIf(sFlagToClose = "Y", "APDCLSPO", "APDUCLPO")

                    'Dim sUpdateQry = "Update [@IDH_JOBENTR] set U_Status = '6' where Code = '" & sU_JOBENTR & "'"
                    'DataHandler.INSTANCE.doUpdateQuery(sUpdateQry)
                    Dim sUpdateQry = "" '"Update OPOR set U_FlagToClose = 'sFlagToClose', U_HLDCOMT = 'sReason', Comments = 'sComments' where DocEntry = Convert.ToInt32(oPONos.Item(iCnt));"
                    For iCnt As Integer = 0 To oPONos.Count - 1
                        Dim sComments As String
                        If sFlagToClose = "Y" Then
                            sComments = "AP Doc Gen: PO Flag to Close By: " + sUserCode + " On: " + DateTime.Now.ToString()
                        Else
                            sComments = "AP Doc Gen: PO Un-flag to Close By: " + sUserCode + " On: " + DateTime.Now.ToString()
                        End If
                        Dim sCommentQry As String
                        If idh.const.Lookup.INSTANCE.getParameterAsBool("APNDCMT", True) AndAlso sComments.Length > 0 Then
                            'sCommentQry = "Comments = Comments + '" + sComments + "'"
                            sCommentQry = "Comments = CASE WHEN LEN(Comments + '" + sComments + "') > 254 THEN '" + sComments + "' ELSE Comments + '" + sComments + "' END "
                        Else
                            sCommentQry = "Comments = '" + sComments + "'"
                        End If
                        sUpdateQry = sUpdateQry + "Update OPOR set U_FlagToClose = '" + sFlagToClose + "', U_HLDCOMT = '" + sReason + "', " + sCommentQry + " where DocEntry = " + Convert.ToInt32(oPONos.Item(iCnt)).ToString() + "; "
                    Next

                    If Not goParent.goDB.doUpdateQuery("doFlagToClosePurchaseOrders()", sUpdateQry) Then
                        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & sFinalResult, "Error Flag To Close Purchase Order(s).")
                    Else
                        doWarnMess("Purchase Order(s) flaged to close successfully.", SAPbouiCOM.BoMessageTime.bmt_Long)

                        ''Testing call for the shared method for the commander
                        'com.idh.bridge.utils.General.doCloseDocument(SAPbobsCOM.BoObjectTypes.oPurchaseOrders, "Testing chk kero", 0, "17/07/2014 13:59:45")
                    End If
                    doLoadData(oForm)
                Else
                    doWarnMess("No rows selected.", SAPbouiCOM.BoMessageTime.bmt_Medium)
                End If

                oForm.Freeze(False)
            Catch ex As Exception
                If goParent.goDICompany.InTransaction Then
                    goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                End If
                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error flaging to close purchase orders. Review Error Log.")
                com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
            End Try

        End Sub

        Public Sub doHoldPurchaseOrders(ByVal oForm As SAPbouiCOM.Form, ByVal sHoldStatus As String, ByVal sReason As String)
            Try
                oForm.Freeze(True)
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "APGRID")
                Dim oSelectedRows As SAPbouiCOM.SelectedRows
                oSelectedRows = oGridN.getSBOGrid.Rows.SelectedRows
                Dim iRowCnt As Integer = oSelectedRows.Count
                Dim oData As ArrayList = Nothing

                If iRowCnt > 0 Then
                    Dim sUserCode As String = goParent.goDICompany.UserName
                    Dim oPONos As ArrayList = oGridN.doPrepareListFromSelection("vAPG.DocEntry")
                    Dim oPO As SAPbobsCOM.Documents
                    Dim sResult As String = Nothing
                    Dim sFinalResult As String = ""
                    Dim sParams(1) As String

                    sParams(0) = String.Join(",", oPONos.ToArray(GetType(String)))
                    oPO = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders)

                    Dim sAlertKey As String = IIf(sHoldStatus = "Y", "APDHLDPO", "AUNHLDPO")

                    goParent.goDICompany.StartTransaction()

                    For iCnt As Integer = 0 To oPONos.Count - 1
                        oPO.GetByKey(Convert.ToInt32(oPONos.Item(iCnt)))
                        Dim sComments As String
                        If sHoldStatus = "Y" Then
                            sComments = "AP Doc Gen: PO On Hold By: '" + sUserCode + "'" + Chr(13) + "On: " + DateTime.Now.ToString()
                        Else
                            sComments = "AP Doc Gen: PO Un-hold By: '" + sUserCode + "'" + Chr(13) + "On: " + DateTime.Now.ToString()
                        End If
                        'Dont append the comments
                        'oPO.Comments = sComments + Chr(13) + oPO.Comments
                        'instead override for the time being 
                        oPO.UserFields.Fields.Item("U_HLDCOMT").Value = sReason

                        If idh.const.Lookup.INSTANCE.getParameterAsBool("APNDCMT", True) AndAlso oPO.Comments.Length > 0 Then
                            oPO.Comments = oPO.Comments + Chr(13) + sComments
                        Else
                            oPO.Comments = sComments
                        End If

                        oPO.UserFields.Fields.Item("U_IDH_HLD").Value = sHoldStatus

                        Dim iResult As Integer = oPO.Update()
                        If iResult <> 0 Then
                            goParent.goDICompany.GetLastError(iResult, sResult)
                            sFinalResult = sFinalResult + Chr(13) + sResult
                            sResult = ""
                            iResult = Nothing
                        End If
                    Next
                    If sFinalResult.Length > 0 Then
                        If goParent.goDICompany.InTransaction Then
                            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                        End If
                        'com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
                        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & sFinalResult, "Error closing Purchase Order(s).")
                    Else
                        If goParent.goDICompany.InTransaction Then
                            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                        End If
                        doWarnMess("Purchase Order(s) Closed successfully.", SAPbouiCOM.BoMessageTime.bmt_Long)
                    End If
                    doLoadData(oForm)
                Else
                    doWarnMess("No rows selected.", SAPbouiCOM.BoMessageTime.bmt_Medium)
                End If

                oForm.Freeze(False)
            Catch ex As Exception
                If goParent.goDICompany.InTransaction Then
                    goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                End If
                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error closing purchase orders. Review Error Log.")
                com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
            End Try

        End Sub

        Public Function doRemoveDraftDocument(ByVal oForm As SAPbouiCOM.Form, ByVal oDraftDocNumberList As ArrayList) As String
            Try
                Dim sFinalResult As String = ""

                If oDraftDocNumberList.Count > 0 Then
                    Dim oDrfat As SAPbobsCOM.Documents
                    Dim sResult As String = Nothing
                    Dim sParams(1) As String

                    sParams(0) = String.Join(",", oDraftDocNumberList.ToArray(GetType(String)))
                    oDrfat = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts)
                    oDrfat.DocObjectCode = SAPbobsCOM.BoObjectTypes.oPurchaseInvoices

                    For iCnt As Integer = 0 To oDraftDocNumberList.Count - 1
                        Dim iDocNo As Integer = Convert.ToInt32(oDraftDocNumberList.Item(iCnt))
                        If iDocNo > 0 Then
                            oDrfat.GetByKey(iDocNo)

                            Dim iResult As Integer = oDrfat.Remove()
                            If iResult <> 0 Then
                                goParent.goDICompany.GetLastError(iResult, sResult)
                                sFinalResult = sFinalResult + Chr(13) + sResult
                                sResult = ""
                                iResult = Nothing
                                Exit For
                            End If
                        End If
                    Next
                End If
                Return sFinalResult
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error removing Draft Document(s). Review Error Log.")
            End Try
        End Function

        Private Sub doSelfBillCombo(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim oBPStaCombo As SAPbouiCOM.ComboBox
                oBPStaCombo = oForm.Items.Item("IDH_LKPSBL").Specific
                oBPStaCombo.ValidValues.Add("", getTranslatedWord("Both"))
                oBPStaCombo.ValidValues.Add("Y", "Yes")
                oBPStaCombo.ValidValues.Add("N", "No")
            Catch ex As Exception
            End Try
        End Sub

        Private Sub doAddComboBoxValidValues(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDH_SCHADH")
            oItem.DisplayDesc = True
            Dim oJobType As SAPbouiCOM.ComboBox = oItem.Specific

            doClearValidValues(oJobType.ValidValues)
            Try
                oJobType.ValidValues.Add("", "Both")
                oJobType.ValidValues.Add("Y", "Scheduled")
                oJobType.ValidValues.Add("N", "Adhoc")
                oJobType.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index)
            Catch ex As Exception
            End Try

        End Sub

    End Class
End Namespace
