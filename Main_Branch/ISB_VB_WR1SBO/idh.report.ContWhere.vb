Imports System.IO
Imports System.Collections

Namespace idh.report
    Public Class ContWhere
        Inherits IDHAddOns.idh.report.CrystalViewer

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHCWA", iMenuPosition, "Container Whereabouts")
        End Sub

        Protected Overrides Function getReport(ByVal oForm As SAPbouiCOM.Form) As String
            Dim sReport As String
            'sReport = IDHAddOns.idh.addon.Base.CURRENTWORKDIR & "\reports\" & "\Container Whereabouts.rpt"
			sReport = IDHAddOns.idh.addon.Base.REPORTPATH & "\Container Whereabouts.rpt"
            Return sReport
        End Function

    End Class
End Namespace