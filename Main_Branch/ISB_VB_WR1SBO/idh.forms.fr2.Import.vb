﻿Imports System.IO
Imports System.Collections
Imports System

'Needed for the Framework controls
Imports IDHAddOns.idh.controls
Imports IDHAddOns
Imports com.idh.bridge.data
Imports com.idh.bridge
Imports com.idh.bridge.lookups
Namespace idh.forms.fr2
    Public Class Import
        Inherits com.idh.forms.oo.Form
 
'        Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByRef oSBOParentForm As SAPbouiCOM.Form, ByRef oSBOForm As SAPbouiCOM.Form)
'            MyBase.New(oIDHForm, oSBOParentForm, oSBOForm)
'            doSetHandlers()
'        End Sub

        Public Sub New(ByVal oIDHForm As IDHAddOns.idh.forms.Base, ByVal sParentId As String, ByRef oSBOForm As SAPbouiCOM.Form)
            MyBase.New(oIDHForm, sParentId, oSBOForm)
            doSetHandlers()
        End Sub

        
        Public Shared Function doRegisterFormClass(ByVal sParMenu As String) As IDHAddOns.idh.forms.Base
            Dim owForm As com.idh.forms.oo.FormController = New com.idh.forms.oo.FormController( _
                "IDH_IMPORTS", _
                sParMenu, _
            	80, _
                "Imports.srf", _
                False, _
        		True, _
        		False, _
        		"Imports", _
        		IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL)
            
            Return owForm
        End Function        
        
        Private Sub doSetHandlers()
        	addHandler_ITEM_PRESSED("JOURNAL", New et_ITEM_PRESSED(AddressOf doJournalEntryEvent))
            addHandler_ITEM_PRESSED("WEIGHTS", New et_ITEM_PRESSED(AddressOf doWeightUpdateEvent))
            addHandler_ITEM_PRESSED("PBICSTREF", New et_ITEM_PRESSED(AddressOf doPBICustomeReferenceEvent))
            addHandler_ITEM_PRESSED("GENERAL", New et_ITEM_PRESSED(AddressOf doGeneralImportEvent))
        End Sub
        
        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef BubbleEvent As Boolean)
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_LOW_FINANCIALS) = TRUE Then
            	Items.Item("JOURNAL").Enabled = True
            Else
            	Items.Item("JOURNAL").Enabled = False
            End If

            If Config.INSTANCE.getParameterAsBool("IMPENGEN", False) = True Then
                Items.Item("GENERAL").Enabled = True
            Else
                Items.Item("GENERAL").Enabled = False
            End If
        End Sub
        
        '**
        ' Handle the Weight Import Button Event
        '**
        Public Function doWeightUpdateEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
        	If pVal.BeforeAction = True Then
                doCallWinFormOpener(AddressOf doImportWeight)
        	End If
        	Return False
        End Function

        '**
        ' Handle the Customer Reference Import Button Event
        '**
        Public Function doPBICustomeReferenceEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                doCallWinFormOpener(AddressOf doImportPBICustReference)
            End If
            Return False
        End Function

        '**
        ' Handle the Journal Import Button Event
        '**
        Public Function doJournalEntryEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
        	If pVal.BeforeAction = True Then
                doCallWinFormOpener(AddressOf doImportJournal)
        	End If
        	Return False
        End Function

        '**
        ' Handle the General Import Button Event
        '**
        Public Function doGeneralImportEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                doCallWinFormOpener(AddressOf doImportGeneral)
            End If
            Return False
        End Function

        '**
        ' Do the Journal Imports
        '**
        Protected Sub doImportJournal()    
            Dim sFileName As String
            Dim oOpenFile As System.Windows.Forms.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()

            Try
                Dim sExt As String = Config.INSTANCE.getImportExtension()
                If Not sExt Is Nothing AndAlso sExt.Length > 0 Then
                    If sExt.IndexOf("|") = -1 Then
                        sExt = "(" & sExt & ")|" & sExt
                    End If
                    oOpenFile.Filter = sExt
                End If
            Catch ex As Exception

            End Try
            oOpenFile.FileName = "OpenExcelFile"
            oOpenFile.InitialDirectory = Config.INSTANCE.getImportFolder()
            If com.idh.win.AppForm.OpenFileDialog(oOpenFile) = System.Windows.Forms.DialogResult.OK Then
                sFileName = oOpenFile.FileName
                Try
                    com.idh.bridge.DataHandler.INSTANCE.doInfo("Importing the file.")
                	
                	Dim oImporter As New com.idh.bridge.import.Journal()
                	If oImporter.doReadFile(sFileName) Then
                        com.idh.bridge.DataHandler.INSTANCE.doInfo("Import done.")
                	End If
                Catch ex As Exception
                    com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error importing the data.")
                Finally
                    'goParent.doProgress("EXIMP", 100)

                    Dim sCurrDirectory As String = Directory.GetCurrentDirectory()
                    Directory.SetCurrentDirectory(IDHAddOns.idh.addon.Base.CURRENTWORKDIR)
                End Try
            End If
        End Sub
        
        '**
        ' Do the Weight Imports
        '**
        Protected Sub doImportWeight()    
            Dim sFileName As String
            Dim oOpenFile As System.Windows.Forms.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()

            Try
                Dim sExt As String = Config.INSTANCE.getImportExtension()
                If Not sExt Is Nothing AndAlso sExt.Length > 0 Then
                    If sExt.IndexOf("|") = -1 Then
                        sExt = "(" & sExt & ")|" & sExt
                    End If
                    oOpenFile.Filter = sExt
                End If
            Catch ex As Exception

            End Try
            oOpenFile.FileName = "OpenExcelFile"
            oOpenFile.InitialDirectory = Config.INSTANCE.getImportFolder()
            If com.idh.win.AppForm.OpenFileDialog(oOpenFile) = System.Windows.Forms.DialogResult.OK Then
                sFileName = oOpenFile.FileName
                Try
                    com.idh.bridge.DataHandler.INSTANCE.doInfo("Importing the file.")
                	
                	Dim oImporter As New com.idh.bridge.import.Weights()
                	If oImporter.doReadFile(sFileName) Then
                        com.idh.bridge.DataHandler.INSTANCE.doInfo("Import done.")
                    End If
                Catch ex As Exception
                    com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error importing the data.")
                Finally
                    'goParent.doProgress("EXIMP", 100)

                    Dim sCurrDirectory As String = Directory.GetCurrentDirectory()
                    Directory.SetCurrentDirectory(IDHAddOns.idh.addon.Base.CURRENTWORKDIR)
                End Try
            End If
        End Sub	

        '**
        ' Do Import the PBI Cust Reference Numbers
        '**
        Protected Sub doImportPBICustReference()
            Dim sFileName As String
            Dim oOpenFile As System.Windows.Forms.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()

            Try
                Dim sExt As String = Config.INSTANCE.getImportExtension()
                If Not sExt Is Nothing AndAlso sExt.Length > 0 Then
                    If sExt.IndexOf("|") = -1 Then
                        sExt = "(" & sExt & ")|" & sExt
                    End If
                    oOpenFile.Filter = sExt
                End If
            Catch ex As Exception

            End Try
            oOpenFile.FileName = "OpenExcelFile"
            oOpenFile.InitialDirectory = Config.INSTANCE.getImportFolder()
            If com.idh.win.AppForm.OpenFileDialog(oOpenFile) = System.Windows.Forms.DialogResult.OK Then
                sFileName = oOpenFile.FileName
                Try
                    com.idh.bridge.DataHandler.INSTANCE.doInfo("Importing the file.")

                    Dim oImporter As New com.idh.bridge.import.PBICustRef()
                    If oImporter.doReadFile(sFileName) Then
                        com.idh.bridge.DataHandler.INSTANCE.doInfo("Import done.")
                    End If
                Catch ex As Exception
                    com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error importing the data.")
                Finally
                    'goParent.doProgress("EXIMP", 100)

                    Dim sCurrDirectory As String = Directory.GetCurrentDirectory()
                    Directory.SetCurrentDirectory(IDHAddOns.idh.addon.Base.CURRENTWORKDIR)
                End Try
            End If
        End Sub

        '**
        ' Do the General Imports
        '**
        Protected Sub doImportGeneral()
            Dim sFileName As String
            Dim oOpenFile As System.Windows.Forms.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()

            Try
                Dim sExt As String = Config.INSTANCE.getImportExtension()
                If Not sExt Is Nothing AndAlso sExt.Length > 0 Then
                    If sExt.IndexOf("|") = -1 Then
                        sExt = "(" & sExt & ")|" & sExt
                    End If
                    oOpenFile.Filter = sExt
                End If
            Catch ex As Exception

            End Try
            oOpenFile.FileName = "OpenExcelFile"
            oOpenFile.InitialDirectory = Config.INSTANCE.getImportFolder()
            If com.idh.win.AppForm.OpenFileDialog(oOpenFile) = System.Windows.Forms.DialogResult.OK Then
                sFileName = oOpenFile.FileName
                Try
                    com.idh.bridge.DataHandler.INSTANCE.doInfo("Importing the file.")

                    Dim oImporter As New com.idh.bridge.import.GeneralData()
                    If oImporter.doReadFile(sFileName) Then
                        com.idh.bridge.DataHandler.INSTANCE.doInfo("Import done.")
                    End If
                Catch ex As Exception
                    com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error importing the data.")
                Finally
                    'goParent.doProgress("EXIMP", 100)

                    Dim sCurrDirectory As String = Directory.GetCurrentDirectory()
                    Directory.SetCurrentDirectory(IDHAddOns.idh.addon.Base.CURRENTWORKDIR)
                End Try
            End If
        End Sub

    End Class
End Namespace

