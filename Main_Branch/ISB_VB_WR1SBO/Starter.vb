Imports System
Imports System.Reflection
Imports System.Security
Imports System.IO

Imports com.idh.dbObjects

Module Starter
    Public Sub Main()
        Dim sStartupClass As String
        Dim sProjectDll As String
        'Dim oAssembly As Assembly

        'DB Object dynamic Assembly
        'com.isb.core.controller.Loader.BOAssembly = "ISB_Shared.dll"
        'com.isb.core.controller.Loader.BONamespace = "com.idh.dbObjects.User"
        com.isb.core.controller.Loader.BOAssembly = "ISB_Core_Shared.dll"
        com.isb.core.controller.Loader.BONamespace = "com.idh.dbObjects.User"

        com.isb.core.controller.Loader.FormsAssembly = "ISB_Forms.dll"
        com.isb.core.controller.Loader.FormsNamespace = "WR1_Forms.idh.forms"

        com.isb.core.controller.Loader.SearchFormAssembly = "ISB_Search.dll"
        com.isb.core.controller.Loader.SearchFormNamespace = "WR1_Search.idh.forms.search"

        com.isb.core.controller.Loader.AdminFormAssembly = "ISB_AdminForms.dll"
        com.isb.core.controller.Loader.AdminFormNamespace = "WR1_Admin.idh.forms.admin"

        com.isb.core.controller.Loader.SBOFormAssembly = "ISB_SBOForms.dll"
        com.isb.core.controller.Loader.SBOFormNamespace = "WR1_SBOForms.idh.forms.SBO"

        'com.isb.core.controller.Loader.SBOFormAssembly = "ISS_Extension.dll"
        'com.isb.core.controller.Loader.SBOFormNamespace = "ISS_Extension.idh.forms.SBO"

        com.isb.core.controller.Loader.ManagerFormAssembly = "ISB_Managers.dll"
        com.isb.core.controller.Loader.ManagerFormNamespace = "WR1_Managers.idh.forms.manager"

        com.isb.core.controller.Loader.ReportFormAssembly = "ISB_Reports.dll"
        com.isb.core.controller.Loader.ReportFormNamespace = "WR1_Reports.idh.report"

        com.isb.core.controller.Loader.ConfigAssembly = "ISB_Shared.dll"
        com.isb.core.controller.Loader.ConfigClass = "com.isb.bridge.lookups.Config"

        'sStartupClass = "com.isb.iss.FormLoader"
        'sProjectDll = "ISS_Extension.dll"

        sStartupClass = "WR1_Forms.idh.main.MainForm"
        sProjectDll = "ISB_Forms.dll"

        'sStartupClass = "com.isb.core.addon.FormLoader" 'WR1_Forms.idh.main.MainForm"
        'sProjectDll = "ISB_Core_Forms.dll" 'ISB_Forms.dll"

        'oAssembly = Assembly.LoadFrom(Directory.GetCurrentDirectory() & "\" & sProjectDll)

        'sStartupClass = "com.uBC.main.MainForm"
        'sProjectDll = "uBC_RAndD.dll"
        'oAssembly = Assembly.LoadFrom(Directory.GetCurrentDirectory() & "\branches\" & sProjectDll)

        'Dim oStarter As Type
        ''oStarter = Type.GetType(sStartupClass)
        'oStarter = oAssembly.GetType(sStartupClass)

        'Dim oParams() As Type = {GetType(String), GetType(Integer)}

        'Dim cTor As ConstructorInfo = oStarter.GetConstructor(oParams)
        'cTor.Invoke(New Object() {"Waste and Recycle", 0})

        com.uBCoded.reflection.Loader.getObjectInstance(sProjectDll, sStartupClass, New Object() {"Waste and Recycling", 0})

        'Dim oForm As WR1_Forms.idh.main.MainForm = Activator.CreateInstance(oStarter)
        'Dim oForm As WR1_Forms.idh.main.MainForm = New WR1_Forms.idh.main.MainForm("Waste and Recycle")
        'Dim oForm As com.uBC.main.MainForm = New com.uBC.main.MainForm("Waste and Recycle", 0)
    End Sub
End Module
