Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class Zones
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByRef oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHZONSRC", "SearchForm.srf", 5, 45, 603, 320, "Zones Search")
        End Sub

        '        '*** Set the Form Title
        '        Protected Overrides Sub doSetTitle(ByVal oForm As SAPbouiCOM.Form)
        '        	oForm.Title = "Zones Search"
        '        End Sub

        '        '*** Set the Labels
        '        Protected Overridable Sub doSetLabels(ByVal oForm As SAPbouiCOM.Form)
        '        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("OPRJ")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_ZONES", Nothing, Nothing, False, True))

            oGridN.setOrderValue("U_ZpCd")

            oGridN.doAddFilterField("IDH_CODE", "U_ZpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 10, Nothing, True)
            oGridN.doAddFilterField("IDH_NAME", "U_Desc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100, Nothing, True)

            oGridN.doAddListField("U_ZpCd", "Postal Code", False, -1, Nothing, "IDH_CODE")
            oGridN.doAddListField("U_Desc", "Zone Description", False, -1, Nothing, "IDH_NAME")
            oGridN.doAddListField("U_ConChrg", "Congestion Charge", False, -1, Nothing, "IDH_CHRG")
            oGridN.doAddListField("U_Band", "Band", False, -1, Nothing, "IDH_BAND")
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace 
