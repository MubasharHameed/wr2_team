﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("ISB WR1")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("ISB")> 
<Assembly: AssemblyProduct("")> 
<Assembly: AssemblyCopyright("Copyright ©  2014")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("39ece305-7642-4823-8865-597b03e18582")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.0.1.1")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
