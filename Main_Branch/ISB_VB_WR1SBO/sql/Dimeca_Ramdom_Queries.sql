--Get the GL Account details for this account
select * from OACT where AcctCode = '1100-001-013'

--Get Company company bank
SELECT b.BankCode As BankCode, b.BankName As BankName, a.DflBnkAcct As BankAcc, '1100-001-013' As GLAcc
FROM OADM a, ODSC b 
WHERE b.BankCode = a.DflBnkCode

BankCode, BankName, BankAcc, GLAcc

--Dimeca Bank query
SELECT U_Banco, U_Cuenta, U_Caja FROM [@CUEN] T1 WHERE Code = '[%U]'
SELECT U_Banco as BankName, U_Cuenta as BankAcc, U_Caja As GLAcc FROM [@CUEN]
SELECT U_Banco as BankName, U_Cuenta as BankAcc, U_Caja As GLAcc, * FROM [@CUEN] WHERE Name = 'manager'
SELECT b.BankCode as BankCode, wb.U_Banco as BankName, wb.U_Cuenta as BankAcc, wb.U_Caja As GLAcc FROM [@CUEN] wb, ODSC b WHERE b.BankName = wb.U_Banco AND wb.Name ='manager'

--Dimeca get Bank details linked to SBO Banks
SELECT b.BankCode, wb.U_Banco as BankName, wb.U_Cuenta as BankAcc, wb.U_Caja As GLAcc 
FROM [@CUEN] wb, ODSC b 
WHERE b.BankName = wb.U_Banco 

SELECT b.BankCode, wb.U_Banco as BankName, wb.U_Cuenta as BankAcc, wb.U_Caja As GLAcc 
	FROM [@CUEN] wb, ODSC b 
	WHERE b.BankName = wb.U_Banco AND wb.Name = 'manager'

SELECT * FROM ODSC

SELECT * FROM OWTR

SELECT * FROM OCHO

--Formatted search queries
--SELECT * FROM OUQR

--Formatted search query link to Form fields
SELECT * FROM CSHS 
WHERE FormId = '143' 
--	AND QueryId Is Not NULL
Order By FormID 

--Formatted search query link to Form fields
--UPDATE CSHS SET QueryId = NULL 
--WHERE FormId = '143' 
--AND ItemID = '20'

--Select the Dow Row
SELECT * FROM [@IDH_DISPROW] WHERE Code = 'P1301020'