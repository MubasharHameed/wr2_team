SELECT Code, * 
FROM [@IDH_TRANCD]
WHERE 
		(
		    U_MarkDoc1 IN ('20','15')
		 OR U_MarkDoc2 IN ('20','15')
		 OR U_MarkDoc3 IN ('20','15')
		 OR U_MarkDoc4 IN ('20','15')
		)

SELECT U_TrnCode, U_BookIn, U_Status, U_PStat, U_WasCd, InvntItem, r.* FROM [@IDH_JOBSHD] r, OITM 
WHERE 
	  U_BookIn != 'B'
	  AND 
	  U_TrnCode IN (
		SELECT Code 
		FROM [@IDH_TRANCD]
		WHERE 
				( 
				    U_MarkDoc1 IN ('20','15')
				 OR U_MarkDoc2 IN ('20','15')
				 OR U_MarkDoc3 IN ('20','15')
				 OR U_MarkDoc4 IN ('20','15')
				)
	  )	
	  AND ItemCode = U_WasCd
	
SELECT U_TrnCode, U_BookIn, U_Status, U_PStat, U_WasCd, InvntItem, r.* FROM [@IDH_DISPROW] r, OITM 
WHERE 
	  U_BookIn != 'B'
	  AND 
	  U_TrnCode IN (
		SELECT Code 
		FROM [@IDH_TRANCD]
		WHERE 
				( 
				    U_MarkDoc1 IN ('20','15')
				 OR U_MarkDoc2 IN ('20','15')
				 OR U_MarkDoc3 IN ('20','15')
				 OR U_MarkDoc4 IN ('20','15')
				)
	  )	
	  AND ItemCode = U_WasCd