--Do the below for all these tables replace the ##### with these values, one by one..
--BPFORECST, CSITPR,CUSTREF, DISPORD, DISPROW, DOADDEXP, DODEDUCT, FORMSET, JOBENTR, JOBSHD, JOBTYPE, PBI, PBIDTL, SUITPR, VEHMAS, VEHMAS2, WOADDEXP, WODEDUCT 

CREATE TABLE [dbo].[AU_IDH_#####](
	[AuditId] [int] IDENTITY(1,1) NOT NULL,
	[U_UTABLE] [nvarchar](max) NULL,
	[U_CODE] [nvarchar](max) NULL,
	[U_ACTION] [nvarchar](max) NULL,
	[U_USER] [nvarchar](max) NULL,
	[U_ADATE] [datetime] NULL,
	[U_ATIME] [smallint] NULL,
	[U_FIELDS] [ntext] NULL,
	[U_CMMNTS] [ntext] NULL,
	[U_OLDCODE] [nvarchar](8) NULL,
 CONSTRAINT [AU_IDH_#####_PR] PRIMARY KEY CLUSTERED 
(
	[AuditId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];

INSERT INTO [AU_IDH_#####] ([U_UTABLE],[U_CODE],[U_ACTION],[U_USER],[U_ADATE],[U_ATIME],[U_FIELDS],[U_CMMNTS],[U_OLDCODE])
         SELECT [U_UTABLE],[U_CODE],[U_ACTION],[U_USER],[U_ADATE],[U_ATIME],[U_FIELDS],[U_CMMNTS],[Code]
         FROM [@IDH_AUDITRL]
         wHERE U_UTABLE = '@IDH_#####' 
         ORDER BY CAST(Code As Numeric); 
         
--DELETE FROM [@IDH_AUDITRL] WHERE U_UTABLE = '@IDH_#####' AND Code In (SELECT U_OLDCODE FROM [AU_IDH_#####])