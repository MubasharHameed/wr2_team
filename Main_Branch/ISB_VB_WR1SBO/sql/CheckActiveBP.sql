SELECT CardCode, validFor, validFrom, validTo, frozenFor, frozenFrom, frozenTo, GETDATE() NOW, * FROM OCRD
WHERE 
	CardCode = '803 Test'; 

SELECT CardCode, validFor, validFrom, validTo, frozenFor, frozenFrom, frozenTo, GETDATE() NOW, * FROM OCRD
WHERE 
	CardCode = '803 Test' 
	AND NOT (
	    -- This is the Froozen state check
		frozenFor = 'Y'
		AND (
			frozenFrom Is Not NULL 
			AND CAST(CONVERT(VARCHAR, frozenFrom,101) As DATETIME) <= CAST(CONVERT(VARCHAR, GETDATE(),101) AS DATETIME)
			AND ( 
				frozenTo Is NULL 
				OR CAST(CONVERT(VARCHAR, frozenTo,101) As DATETIME) >= CAST(CONVERT(VARCHAR, GETDATE(),101) AS DATETIME)
			) OR (
				frozenFrom Is NULL   
				AND (
					frozenTo Is NULL
					OR CAST(CONVERT(VARCHAR, frozenTo,101) As DATETIME) >= CAST(CONVERT(VARCHAR, GETDATE(),101) AS DATETIME)
				)
			)
		)
	)
	AND  (
	    -- This is the Active state check
		validFor = 'Y'
		AND (
			validFrom Is Not NULL   
			AND CAST(CONVERT(VARCHAR, validFrom,101) As DATETIME) <= CAST(CONVERT(VARCHAR, GETDATE(),101) AS DATETIME)
			AND (
				validTo Is NULL
				OR CAST(CONVERT(VARCHAR, validTo,101) As DATETIME) >= CAST(CONVERT(VARCHAR, GETDATE(),101) AS DATETIME)
			) OR (
				validFrom Is NULL   
				AND (
					validTo Is NULL
					OR CAST(CONVERT(VARCHAR, validTo,101) As DATETIME) >= CAST(CONVERT(VARCHAR, GETDATE(),101) AS DATETIME)
				)
			)
		) OR (
			validFor = 'N' 
			AND frozenFor = 'Y'
--			AND validFrom Is NULL   
--			AND validTo Is NULL 
		)
	)	