Imports com.idh.dbObjects.User
Imports com.idh.utils
Imports com.idh.bridge
Imports com.idh.bridge.lookups

Public Class IDH_PBI_Handler

#Region "Declarations"
    Public Shared PER_Months As String = "Months"
    Public Shared PER_Quarts As String = "Quarts"
    Public Shared PER_Weeks As String = "Weeks"
    Private mbDoResetHeaderRunDetails As Boolean = False

    'Private msHeaderIndicator As String 'HU - Header Update, HN - New Header
    'Private msDetailIndicator As Integer 'The number of Details Created

    Private mbDoUpdateInfo As Boolean = False
    Public Property DoUpdateInfo As Boolean
        Set(ByVal value As Boolean)
            mbDoUpdateInfo = value
        End Set
        Get
            Return mbDoUpdateInfo
        End Get
    End Property

    Private moPBI As com.idh.dbObjects.User.IDH_PBI
    Public ReadOnly Property IDH_PBI As com.idh.dbObjects.User.IDH_PBI
        Get
            Return moPBI
        End Get
    End Property
    Private mdCalculatedNextRun As Date
    Public ReadOnly Property CalculatedNextRun As Date
        Get
            Return mdCalculatedNextRun
        End Get
    End Property

    Private moPBIDetail As IDH_PBIDTL = Nothing

    Private PBI As PBI_STRUCT
    Private HIS_PBI As PBI_STRUCT

    'Dim oErr As New Error_Class
    'Dim oDebug As New Debug_Class
    'Dim oLookUp As New Lookup
    'Dim oCalc As IDH_JobCostCalculations
    Dim gNew As Boolean = True
    Dim mdThisRunDate As Date = Nothing
    Dim mbRunDateWasSet As Boolean = False

    Public miLastInstructionsDone As Integer
    Public miLastInstructionsToDo As Integer

    Private moSBO_Company As SAPbobsCOM.Company
    Private moData As com.idh.bridge.DataHandler
    Private moLookup As com.idh.bridge.lookups.Config

    '   Public Delegate Sub ExternalLogger(ByVal sID As String, ByVal sMessType As String, ByVal sMessage As String)
    '   Private moExternalLogger As ExternalLogger
    '   Public Sub setExternalLogger(ByVal oLogger As ExternalLogger)
    '	moExternalLogger = oLogger
    'End Sub

    Private Structure PBI_STRUCT
        Dim Code As String

        Dim CardCode As String
        Dim CardName As String
        Dim SiteAddress As String
        Dim SiteAddressAddrsCd As String
        Dim CusStreet As String
        Dim CusBlock As String
        Dim CusCity As String
        Dim CusZip As String
        Dim CusPhone As String
        Dim CarAddress As String
        Dim CarAddressAddrsCd As String
        Dim CarStreet As String
        Dim CarBlock As String
        Dim CarCity As String
        Dim CarZip As String
        Dim CarPhone As String
        Dim Contact As String
        Dim JobType As String
        Dim ContGrp As String
        Dim ContCode As String
        Dim ContDesc As String
        Dim Qty As Double
        Dim ContainerQty As Double
        Dim WasteCode As String
        Dim WasteDesc As String
        Dim CarrCode As String
        Dim CarrName As String

        Dim DisCode As String
        Dim DisName As String
        Dim DisAddress As String
        Dim DisAddressAddrsCd As String
        Dim DisStreet As String
        Dim DisBlock As String
        Dim DisCity As String
        Dim DisZip As String
        Dim DisPhone As String

        Dim Pattern As String
        Dim RecurFreq As Integer 'i.e. Every 4 months do full run.
        Dim WeekCount As Integer
        Dim EndType As String '1 = Date, 2 = Ocurrance
        Dim EndCount As Integer
        Dim RecurCount As Integer
        Dim Comment As String
        Dim Owner As String
        Dim Branch As String
        Dim Mon As String
        Dim Tue As String
        Dim Wed As String
        Dim Thu As String
        Dim Fri As String
        Dim Sat As String
        Dim Sun As String
        Dim WOCode As String
        Dim WOLineCode As String
        'Dim Obligated As String

        Dim sEFType As String
        Dim iEFDay As Integer
        Dim sEFWeek As String
        Dim iEFDOW As Integer
        Dim iEFMonth As Integer

        Dim StartDate As Date
        Dim EndDate As Date

        Dim LastRun As Date
        Dim NextRun As Date
        Dim RunCount As Integer
        Dim PeriodStartDate As Date
        Dim PeriodEndDate As Date

        Dim RecuringStr As String
        Dim JobDates As com.idh.bridge.utils.Dates.ScheduleDates
        Dim WORs As ArrayList
        Dim DoDetail As String
        Dim DoDetailInv As String

        Dim UsePeriodEnd As Boolean
        Dim PeriodType As String

        Dim AutoCompleteStartDate As Boolean
        Dim AutoCompleteEndDate As Boolean

        '*********************************
        '* Routing Information
        '*********************************
        Dim Routable As String                  '"IDHRT"
        Dim RouteCodeMonday As String           '"IDHRCDM"
        Dim RouteCodeTuesday As String          '"IDHRCDTU"
        Dim RouteCodeWednesday As String        '"IDHRCDW"
        Dim RouteCodeThursday As String         '"IDHRCDTH"
        Dim RouteCodeFriday As String           '"IDHRCDF"
        Dim RouteCodeSaturday As String         '"IDHRCDSA"
        Dim RouteCodeSunday As String           '"IDHRCDSU"
        Dim CommonComments As String            '"IDHCMT"
        Dim CommonDriverInstructions As String  '"IDHDRVI"

        '*********************************
        '* Routing Sequence Information
        '*********************************
        Dim RouteSeqMonday As Integer           '"IDHRSQM"
        Dim RouteSeqTuesday As Integer          '"IDHRSQTU"
        Dim RouteSeqWednesday As Integer        '"IDHRSQW"
        Dim RouteSeqThursday As Integer         '"IDHRSQTH"
        Dim RouteSeqFriday As Integer           '"IDHRSQF"
        Dim RouteSeqSaturday As Integer         '"IDHRSQSA"
        Dim RouteSeqSunday As Integer           '"IDHRSQSU"

        ''##MA Start Issue#439
        Dim TRNCd As String
        ''##MA End Issue#439
        '#MA Start 10-05-2017 issue# 402
        Dim WasteItmGrpCd As String               'U_WasItmGrp
        '#MA End 10-05-2017 
        '#MA Start 17-05-2017 issue#402 point 1&2
        Dim Latitude As String                      'U_LATI
        Dim Longitude As String                     'U_LONGI
        Dim IDHPOS As String                        'U_IDHPOS
        '#MA End 17-05-2017
    End Structure
#End Region

#Region "Setup"
    Public Sub New(ByRef SBO_Company As SAPbobsCOM.Company)
        Me.New(SBO_Company, False)
    End Sub

    Public Sub New(ByRef SBO_Company As SAPbobsCOM.Company, ByVal bFromService As Boolean)
        moSBO_Company = SBO_Company
        moData = com.idh.bridge.DataHandler.INSTANCE
        If moData Is Nothing Then
            moData = New com.idh.bridge.DataHandler(True, SBO_Company)
            If bFromService Then
                moData.setISV("IDH")
                com.idh.bridge.DataHandler.INSTANCE.setAddOnName("WR-PBI-Service")
                moData.doReadVersion()

                moData.setNewVersion(moData.CurrentVersion)
                'moData.setNewReleaseCount(moData.CurrentVersion)
                moData.setNewReleaseCount(moData.CurrentReleaseCount)
            End If
        End If

        moLookup = com.idh.bridge.lookups.Config.INSTANCE
        If moLookup Is Nothing Then
            moLookup = New com.idh.bridge.lookups.Config("[@IDH_WRCONFIG]")
        End If

        'oCalc = New IDH_JobCostCalculations(moSBO_Company, moData, moLookup)
    End Sub

    Public Function getLastError() As String
        Return moData.ErrorBuffer.getLastMessage().DetailMessage
    End Function

    '    Public Function doCheckDate(ByVal dDate As Date) As Boolean
    '        If dDate.Year < 1900 Then
    '            Return False
    '        Else
    '            Return True
    '        End If
    '    End Function
#End Region
    Public Function doAdjustPBIStartDate(ByVal PBI_Code As String, ByVal dActDate As DateTime, ByVal dSetNextRun As DateTime, ByVal sStartDate As String, ByVal bDoToEnd As Boolean) As Boolean
        Dim dStartDate As DateTime
        If Not sStartDate Is Nothing AndAlso sStartDate.Length >= 6 Then
            dStartDate = Conversions.ToDateTime(sStartDate)
            Return doAdjustPBIStartDate(PBI_Code, dActDate, dSetNextRun, dStartDate, bDoToEnd)
        End If
        Return True
    End Function

    Public Function doAdjustPBIStartDate(ByVal PBI_Code As String, ByVal dActDate As DateTime, ByVal dSetNextRun As DateTime, ByVal dStartDate As DateTime, ByVal bDoToEnd As Boolean) As Boolean
        Dim oPBIDetail As New IDH_PBIDTL()
        oPBIDetail.DoUpdateInfo = DoUpdateInfo

        Dim bResult As Boolean = oPBIDetail.doDeleteByPBI(PBI_Code, "PBI Start date adjusted", True, True)
        mbDoResetHeaderRunDetails = True
        Try
            'Dim bResult As Boolean = oPBIDetail.doAdjustPBIStartDate(PBI_Code, dStartDate, "PBI Start date adjusted", True)
            DataHandler.INSTANCE.doReleaseObject(CType(oPBIDetail, Object))

            If bResult = True Then
                ''    'First update the Period containing the new start date
                ''    If bDoToEnd Then
                ''        bResult = ProcessOrderRunToEnd(PBI_Code, dStartDate, dSetNextRun)
                ''    Else
                bResult = ProcessOrderRun(PBI_Code, dStartDate)
                ''        'If the Action date and the new Start date are not the same process the PBI on the action date as well
                ''        If dates.CompareDatePartOnly(dActDate, PBI.PeriodEndDate) > 0 Then
                ''            bResult = ProcessOrderRun(PBI_Code, dActDate)
                ''        End If
                ''    End If

            End If
        Catch ex As Exception
            Throw ex
        Finally
            mbDoResetHeaderRunDetails = False
        End Try
        Return bResult
    End Function

    Public Function doClearBeforeStartDate(ByVal PBI_Code As String, ByVal sStartDate As String) As Boolean
        Dim dStartDate As DateTime
        If Not sStartDate Is Nothing AndAlso sStartDate.Length >= 6 Then
            dStartDate = Conversions.ToDateTime(sStartDate)
            Return doClearBeforeStartDate(PBI_Code, dStartDate)
        End If
        Return True
    End Function

    Public Function doClearBeforeStartDate(ByVal PBI_Code As String, ByVal dStartDate As Date) As Boolean
        Dim oPBIDetail As New IDH_PBIDTL()
        oPBIDetail.DoUpdateInfo = DoUpdateInfo

        Dim bResult As Boolean = oPBIDetail.doDeleteJobsBeforeDate(PBI_Code, dStartDate, "PBI Start date adjusted", False, True)
        DataHandler.INSTANCE.doReleaseObject(CType(oPBIDetail, Object))
        Return bResult
    End Function


    Public Function doClearAfterTerminationDate(ByVal PBI_Code As String, ByVal sTermDate As String) As Boolean
        Dim dTermDate As DateTime
        If Not sTermDate Is Nothing AndAlso sTermDate.Length >= 6 Then
            dTermDate = Conversions.ToDateTime(sTermDate)
            Return doClearAfterTerminationDate(PBI_Code, dTermDate)
        End If
        Return True
    End Function

    Public Function doClearAfterTerminationDate(ByVal PBI_Code As String, ByVal dTermDate As Date) As Boolean
        Dim oPBIDetail As New IDH_PBIDTL()
        oPBIDetail.DoUpdateInfo = DoUpdateInfo

        Dim bResult As Boolean = oPBIDetail.doDeleteJobsAfterDate(PBI_Code, dTermDate, "PBI Termination date adjusted", False, True)
        DataHandler.INSTANCE.doReleaseObject(CType(oPBIDetail, Object))
        Return bResult
    End Function

    Public Function ProcessOrderRunToEnd(ByVal PBI_Code As String, ByVal dRunDate As Date, ByVal dNextRun As DateTime) As Boolean
        While dates.CompareDatePartOnly(dRunDate, dNextRun) < 0
            If ProcessOrderRun(PBI_Code, dRunDate) = False Then
                Return False
            End If
            dRunDate = CalculatedNextRun
        End While
        Return True
    End Function

    Public Function ProcessOrderRun(ByVal PBI_Code As String) As Boolean
        Return ProcessOrderRun(PBI_Code, False)
    End Function
    Public Function ProcessOrderRun(ByVal PBI_Code As String, ByVal bDoImported As Boolean) As Boolean
        Return ProcessOrderRun(PBI_Code, "getDate()", bDoImported)
    End Function

    Public Function ProcessOrderRun(ByVal PBI_Code As String, ByVal sRunDate As String) As Boolean
        Return ProcessOrderRun(PBI_Code, sRunDate, False)
    End Function
    Public Function ProcessOrderRun(ByVal PBI_Code As String, ByVal sRunDate As String, ByVal bDoImported As Boolean) As Boolean
        If sRunDate Is Nothing OrElse sRunDate.Length < 6 Then
            mdThisRunDate = Nothing
        ElseIf sRunDate.Equals("getDate()") = True Then
            mdThisRunDate = DateTime.Now() 'oRc.Fields.Item("ThisRun").Value
        Else
            mdThisRunDate = Conversions.ToDateTime(sRunDate)
        End If
        Return ProcessOrderRun(PBI_Code, mdThisRunDate, bDoImported)
    End Function

    Public Function ProcessOrderRun(ByVal PBI_Code As String, ByVal dRunDate As Date) As Boolean
        Return ProcessOrderRun(PBI_Code, dRunDate, -1, False, Nothing, Nothing, Nothing, Nothing, Nothing, DateTime.MinValue)
    End Function
    Public Function ProcessOrderRun(ByVal PBI_Code As String, ByVal dRunDate As Date, ByVal bDoImported As Boolean) As Boolean
        Return ProcessOrderRun(PBI_Code, dRunDate, -1, bDoImported, Nothing, Nothing, Nothing, Nothing, Nothing, DateTime.MinValue)
    End Function
    Public Function ProcessOrderRun(ByVal PBI_Code As String, ByVal dRunDate As Date, ByVal iMaxPBIs As Integer,
                                    ByVal bDoImported As Boolean, ByVal sBPCodes As String, ByVal sSite As String,
                                    ByVal sJobType As String, ByVal sFreq As String, ByVal sFreqType As String, ByVal dActionEndDate As DateTime) As Boolean
        If Dates.isValidDate(dRunDate) = False Then
            mdThisRunDate = DateTime.Now()
            mbRunDateWasSet = False
        Else
            mdThisRunDate = dRunDate
            mbRunDateWasSet = True
        End If
        Dim sRunDate As String = dates.doSimpleSQLDateNoTime(mdThisRunDate)

        moData.doClearError()
        miLastInstructionsDone = 0
        miLastInstructionsToDo = 0

        Dim sWhere As String = ""
        Dim Val As String = ""
        Dim bResult As Boolean = True
        Dim sCurrentPBI As String = "DK"

        Try
            If Not moSBO_Company Is Nothing Then
                '' U_IDHRECEN - 1 -> End After number of occorances
                '' U_IDHRECEN - 2 -> End After the date

                'Check if there is a , for a list or SELECT for a subquery  to filter on
                If Not PBI_Code Is Nothing AndAlso PBI_Code.Length > 0 Then
                    'If PBI_Code.IndexOf(" ") > 0 OrElse PBI_Code.IndexOf(",") > 0 Then
                    '    Dim sPBICode As String = PBI_Code.Trim()
                    '    If sPBICode.Length > 6 AndAlso sPBICode.Substring(0, 6).ToUpper().Equals("SELECT") Then
                    '        sWhere = " Code IN (" & PBI_Code & ") "
                    '    Else
                    '        sWhere = " Code IN (" & Formatter.doFormatStringList(PBI_Code) & ") "
                    '    End If
                    'ElseIf 
                    'Else
                    '    'Called from SBO
                    '    sWhere = " Code = '" & PBI_Code & "' "
                    'End If

                    sWhere = General.doCreateSQLFilter("Code", PBI_Code, False)

                    'BP Codes
                    'If Not sBPCodes Is Nothing AndAlso sBPCodes.Length > 0 Then
                    '    sWhere = sWhere & " AND " & IDH_PBI._IDHCCODE & " IN (" & Formatter.doFormatStringList(sBPCodes) & ") "
                    'End If
                    sWhere = sWhere & General.doCreateSQLFilter(IDH_PBI._IDHCCODE, sBPCodes, True)

                    ''BP Sites
                    'If Not sSite Is Nothing AndAlso sSite.Length > 0 Then
                    '    sWhere = sWhere & " AND " & IDH_PBI._IDHSADDR & " = '" & sSite & "'"
                    'End If
                    sWhere = sWhere & General.doCreateSQLFilter(IDH_PBI._IDHSADDR, sSite, True)

                    'JopType
                    If Not sJobType Is Nothing AndAlso sJobType.Length > 0 Then
                        sWhere = sWhere & " AND " & IDH_PBI._IDHJTYPE & " = '" & sJobType & "'"
                    End If

                    'Freq
                    If Not sFreq Is Nothing AndAlso sFreq.Length > 0 Then
                        sWhere = sWhere & " AND " & IDH_PBI._IDHRECFQ & " = '" & sFreq & "'"
                    End If

                    'Freq Type
                    If Not sFreqType Is Nothing AndAlso sFreqType.Length > 0 Then
                        'sWhere = sWhere & " AND " & IDH_PBI._IDHRECFQ & " = '" & sFreq & "'"
                        sWhere = sWhere & " AND " & IDH_PBI._PERTYP & " = '" & sFreqType & "'"
                    End If

                    If bDoImported Then
                        sWhere = "(" & sWhere & ") Or " & IDH_PBI._IDHIMPI & " = 'IW' "
                    End If
                Else
                    '                    sWhere = " ( " & _
                    '                      " convert(datetime,U_IDHRECED,126) >= convert(datetime,'" & sRunDate & "',126) " & _
                    '                         " or " & _
                    '                         "   (U_IDHRECCE > U_IDHRECCT) " & _
                    '                         "      or (U_IDHRECED is null and (U_IDHRECCE is null or U_IDHRECCE = 0 ))" & _
                    '                         ") and " & _
                    '                         "( U_IDHNEXTR is not null AND " & _
                    '                         "    convert(datetime,U_IDHNEXTR,126) <= convert(datetime,'" & sRunDate & "',126) " & _
                    '                         ")  "
                    sWhere = "(" &
                             "	(  U_IDHRECEN = 2 AND convert(datetime,U_IDHRECED,126) >= convert(datetime,'" & sRunDate & "',126) ) " &
                             "	OR " &
                             "	(  U_IDHRECEN = 1 AND " &
                             "		( " &
                             "			(U_IDHRECCE > U_IDHRECCT) " &
                             "			OR (" &
                             "				U_IDHRECED is null" &
                             "				AND (" &
                             "					U_IDHRECCE is null " &
                             "					OR U_IDHRECCE = 0 " &
                             "				) " &
                             "			) " &
                             "		) " &
                             "	) " &
                             ") " &
                             "AND ( " &
                             "	U_IDHNEXTR is not null " &
                             "	AND convert(datetime,U_IDHNEXTR,126) <= convert(datetime,'" & sRunDate & "',126) " &
                             ") "

                    'BP Codes
                    sWhere = sWhere & General.doCreateSQLFilter(IDH_PBI._IDHCCODE, sBPCodes, True)
                    'If Not sBPCodes Is Nothing AndAlso sBPCodes.Length > 0 Then
                    'sWhere = sWhere & " AND " & IDH_PBI._IDHCCODE & " IN (" & Formatter.doFormatStringList(sBPCodes) & ") "
                    'End If

                    'BP Sites
                    sWhere = sWhere & General.doCreateSQLFilter(IDH_PBI._IDHSADDR, sSite, True)
                    'If Not sSite Is Nothing AndAlso sSite.Length > 0 Then
                    'sWhere = sWhere & " AND " & IDH_PBI._IDHSADDR & " = '" & sSite & "'"
                    'End If

                    'JopType
                    If Not sJobType Is Nothing AndAlso sJobType.Length > 0 Then
                        sWhere = sWhere & " AND " & IDH_PBI._IDHJTYPE & " = '" & sJobType & "'"
                    End If

                    'Freq
                    If Not sFreq Is Nothing AndAlso sFreq.Length > 0 Then
                        sWhere = sWhere & " AND " & IDH_PBI._IDHRECFQ & " = '" & sFreq & "'"
                        'sWhere = sWhere & " AND " & IDH_PBI._PERTYP & " = '" & sFreq & "'"
                    End If

                    'Freq Type
                    If Not sFreqType Is Nothing AndAlso sFreqType.Length > 0 Then
                        'sWhere = sWhere & " AND " & IDH_PBI._IDHRECFQ & " = '" & sFreq & "'"
                        sWhere = sWhere & " AND " & IDH_PBI._PERTYP & " = '" & sFreqType & "'"
                    End If

                    If bDoImported Then
                        sWhere = "(" & sWhere & ") Or " & IDH_PBI._IDHIMPI & " = 'IW' "
                    End If
                End If

                moPBI = New com.idh.dbObjects.User.IDH_PBI()
                If moPBI.getData(sWhere, Nothing, iMaxPBIs) > 0 Then
                    'Quickly set the Import Progress If the result includes Imported PBI's"
                    If bDoImported Then
                        moPBI.first()
                        While (moPBI.next())
                            If Not moPBI.U_IDHIMPI Is Nothing AndAlso moPBI.U_IDHIMPI.Equals("IW") Then
                                moPBI.U_IDHIMPI = "IB"
                            End If
                        End While
                        moPBI.doProcessData()
                    End If

                    moPBI.first()
                    Dim sCountStr As String = ""
                    If iMaxPBIs > 0 AndAlso iMaxPBIs <= moPBI.Count Then
                        miLastInstructionsToDo = iMaxPBIs
                        'sCountStr = "Max(" & iMaxPBIs & ") from Total(" & moPBI.Count & ")"
                        sCountStr = "PBIMax(" & iMaxPBIs & ")"
                    Else
                        miLastInstructionsToDo = moPBI.Count
                        sCountStr = moPBI.Count.ToString()
                    End If
                    moData.doInfo("ProcessOrderRun", "Info", "PBI's Found to Do - " & sCountStr)

                    While moPBI.next
                        Try
                            ''MA 16-04-2015 Start Pt:706 ISS
                            If Config.INSTANCE.getParameterAsBool("VDPBILCR", False) = True Then
                                If moPBI.U_IDHOBLGT.Trim.ToLower = "yes" AndAlso moPBI.U_Rebate.Trim.ToLower = "y" Then
                                    If doValidateCarrierLinkBP(moPBI.Code, moPBI.U_IDHCARCD, moPBI.U_IDHWCICD) = True Then
                                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Linked Customer of Carrier " & moPBI.U_IDHCARCD & " is in-active.PBI cannot run.", "ERUSPBI1", {moPBI.U_IDHCARCD})
                                        Continue While
                                        'Return True ''we ll allow to update/save PBI but will not allow to run
                                    End If
                                End If
                            End If
                            'MA 16-04-2015 End
                            '#MA Start 07-06-207 Issue#426
                            If Config.INSTANCE.getParameterAsBool("PBIPOS") Then
                                If Not String.IsNullOrWhiteSpace(moPBI.U_IDHCCODE) AndAlso Not String.IsNullOrWhiteSpace(moPBI.U_IDHSADDR) Then
                                    Dim oBPAddressShipTo As BPAddress = New BPAddress()
                                    oBPAddressShipTo.doGetAddress(moPBI.U_IDHCCODE, "S", False, moPBI.U_IDHSADDR)
                                    If Not String.IsNullOrWhiteSpace(oBPAddressShipTo.U_onStop) AndAlso oBPAddressShipTo.U_onStop.Equals("Y") Then
                                        'If Not Config.INSTANCE.getParameterAsBool("PBIPBYPS", False) Then
                                        moPBI.U_IDHPOS = "Y"
                                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("PBI Pause Because Site is onStop" + moPBI.Code + ". PBI cannot run.", "ERUSPBI3", {moPBI.Code})
                                            moPBI.doProcessData()
                                            Continue While
                                        'Else
                                        ''Continue While
                                        'End If
                                    Else
                                            moPBI.U_IDHPOS = "N"
                                    End If
                                End If
                            End If
                            '#MA End 07-06-2017
                            If Not moPBI.doValidation() Then
                                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Failed to validate PBI " + moPBI.Code + ". PBI cannot run.", "ERUSPBI2", {moPBI.Code})
                                Continue While
                            End If
                            moData.doInfo("ProcessOrder", "Info", "PBI: " & moPBI.Code & " [ " & (moPBI.CurrentRowIndex + 1) & " of " & sCountStr & "]") ' & " - " & getLastError() & "]")

                            gNew = True

                            PBI = Nothing

                            PBI.StartDate = moPBI.U_IDHRECSD
                            If Date.Compare(PBI.StartDate, mdThisRunDate) > 0 Then
                                mdThisRunDate = PBI.StartDate
                            End If

                            PBI.Code = moPBI.Code
                            sCurrentPBI = moPBI.Code

                            PBI.CardCode = moPBI.U_IDHCCODE
                            PBI.CardName = moPBI.U_IDHCNAME
                            PBI.CarrCode = moPBI.U_IDHCARCD
                            PBI.CarrName = moPBI.U_IDHCARDC
                            PBI.DisCode = moPBI.U_IDHDISCD
                            PBI.DisName = moPBI.U_IDHDISDC
                            PBI.Owner = moPBI.U_IDHOWNER
                            PBI.Branch = moPBI.U_IDHBRNCH
                            ''##MA Start Issue#439
                            PBI.TRNCd = moPBI.U_TRNCd
                            ''##MA End Issue#439

                            PBI.SiteAddress = moPBI.U_IDHSADDR
                            PBI.CusStreet = moPBI.CustomerAddress.Street
                            PBI.CusBlock = moPBI.CustomerAddress.Block
                            PBI.CusCity = moPBI.CustomerAddress.City
                            PBI.CusZip = moPBI.CustomerAddress.ZipCode
                            PBI.CusPhone = moPBI.CustomerContact
                            PBI.SiteAddressAddrsCd = moPBI.U_IDHSADLN

                            PBI.CarAddress = moPBI.CarrierAddress.Address
                            PBI.CarAddressAddrsCd = moPBI.CarrierAddress.U_AddrsCd
                            PBI.CarStreet = moPBI.CarrierAddress.Street
                            PBI.CarBlock = moPBI.CarrierAddress.Block
                            PBI.CarCity = moPBI.CarrierAddress.City
                            PBI.CarZip = moPBI.CarrierAddress.ZipCode
                            PBI.CarPhone = moPBI.CarrierContact

                            If Not moPBI.DisposalAddress Is Nothing Then
                                PBI.DisAddress = moPBI.DisposalAddress.Address
                                PBI.DisAddressAddrsCd = moPBI.DisposalAddress.U_AddrsCd
                                PBI.DisStreet = moPBI.DisposalAddress.Street
                                PBI.DisBlock = moPBI.DisposalAddress.Block
                                PBI.DisCity = moPBI.DisposalAddress.City
                                PBI.DisZip = moPBI.DisposalAddress.ZipCode
                            Else
                                PBI.DisAddress = ""
                                PBI.DisAddressAddrsCd = ""
                                PBI.DisStreet = ""
                                PBI.DisBlock = ""
                                PBI.DisCity = ""
                                PBI.DisZip = ""
                            End If

                            PBI.ContGrp = moPBI.U_IDHCOGRP
                            PBI.ContCode = moPBI.U_IDHCOICD
                            PBI.ContDesc = moPBI.U_IDHCOIDC
                            PBI.WasteCode = moPBI.U_IDHWCICD
                            PBI.WasteDesc = moPBI.U_IDHWCIDC
                            PBI.JobType = moPBI.U_IDHJTYPE
                            PBI.EndType = Conversions.ToString(moPBI.U_IDHRECEN)
                            PBI.EndCount = moPBI.U_IDHRECCE
                            '#MA Start 10-05-2017 issue#402
                            PBI.WasteItmGrpCd = moPBI.U_WasItmGrp
                            '#MA End 10-05-2017
                            '#MA Start 17-05-207 issue#402 point 1&2
                            PBI.Latitude = moPBI.U_LATI
                            PBI.Longitude = moPBI.U_LONGI
                            'If doCheckDate(moPBI.U_IDHRECED) = True Then
                            If Dates.isValidDate(moPBI.U_IDHRECED) = True Then
                                PBI.EndDate = moPBI.U_IDHRECED
                                PBI.EndType = "1"
                            ElseIf moPBI.U_IDHRECEN = 2 Then
                                PBI.EndDate = DateTime.Now.AddYears(10)
                                PBI.EndType = "1"
                            ElseIf moPBI.U_IDHRECEN = 0 Then
                                PBI.EndType = "2"
                                PBI.EndDate = DateTime.Now.AddYears(10)
                            End If

                            PBI.Mon = moPBI.U_IDHRDMON2
                            PBI.Tue = moPBI.U_IDHRDTUE2
                            PBI.Wed = moPBI.U_IDHRDWED2
                            PBI.Thu = moPBI.U_IDHRDTHU2
                            PBI.Fri = moPBI.U_IDHRDFRI2
                            PBI.Sat = moPBI.U_IDHRDSAT2
                            PBI.Sun = moPBI.U_IDHRDSUN2
                            PBI.WeekCount = moPBI.U_IDHRACTW

                            PBI.Pattern = moPBI.U_IDHRECUR
                            PBI.RecurCount = moPBI.U_IDHRECCE
                            PBI.RecurFreq = moPBI.U_IDHRECFQ

                            PBI.sEFType = moPBI.U_EFType
                            PBI.iEFDay = moPBI.U_EFDay
                            PBI.sEFWeek = moPBI.U_EFWeek
                            PBI.iEFDOW = moPBI.U_EFDOW
                            PBI.iEFMonth = moPBI.U_EFMonth

                            PBI.RunCount = moPBI.U_IDHRECCT

                            If Dates.isValidDate(moPBI.U_IDHLASTR) = True Then
                                PBI.LastRun = moPBI.U_IDHLASTR
                            End If

                            If mbDoResetHeaderRunDetails Then
                                PBI.NextRun = PBI.StartDate
                            Else
                                If Dates.isValidDate(moPBI.U_IDHNEXTR) = True Then
                                    PBI.NextRun = moPBI.U_IDHNEXTR
                                Else
                                    PBI.NextRun = PBI.StartDate
                                End If
                            End If

                            PBI.Qty = moPBI.U_IDHCOIQT

                            'PBI.Obligated = moPBI.U_IDHOBLGT

                            If moPBI.U_IDHDETAI = "Y" Then
                                PBI.DoDetail = "Y"
                            Else
                                PBI.DoDetail = "N"
                            End If

                            If moPBI.U_IDHDETIN = "Y" Then
                                PBI.DoDetailInv = "Y"
                            Else
                                PBI.DoDetailInv = "N"
                            End If

                            PBI.PeriodType = moPBI.U_PERTYP

                            Dim sUsePeriodEnd As String = moPBI.U_IDHUMONE
                            If sUsePeriodEnd.Equals("Y") Then
                                PBI.UsePeriodEnd = True
                            Else
                                PBI.UsePeriodEnd = False
                            End If

                            Dim sAutoStartDate As String = moPBI.U_AUTSTRT
                            If sAutoStartDate.Equals("Y") Then
                                PBI.AutoCompleteStartDate = True
                            Else
                                PBI.AutoCompleteStartDate = False
                            End If

                            Dim sAutoEndDate As String = moPBI.U_AUTEND
                            If sAutoEndDate.Equals("Y") Then
                                PBI.AutoCompleteEndDate = True
                            Else
                                PBI.AutoCompleteEndDate = False
                            End If


                            '*********************************
                            '* Routing Information
                            '*********************************
                            PBI.Routable = moPBI.U_IDHRT
                            PBI.RouteCodeMonday = moPBI.U_IDHRCDM
                            PBI.RouteCodeTuesday = moPBI.U_IDHRCDTU
                            PBI.RouteCodeWednesday = moPBI.U_IDHRCDW
                            PBI.RouteCodeThursday = moPBI.U_IDHRCDTH
                            PBI.RouteCodeFriday = moPBI.U_IDHRCDF
                            PBI.RouteCodeSaturday = moPBI.U_IDHRCDSA
                            PBI.RouteCodeSunday = moPBI.U_IDHRCDSU
                            PBI.CommonComments = moPBI.U_IDHCMT
                            PBI.CommonDriverInstructions = moPBI.U_IDHDRVI

                            '*********************************
                            '* Routing Sequence Number
                            '*********************************
                            PBI.RouteSeqMonday = moPBI.U_IDHRSQM
                            PBI.RouteSeqTuesday = moPBI.U_IDHRSQTU
                            PBI.RouteSeqWednesday = moPBI.U_IDHRSQW
                            PBI.RouteSeqThursday = moPBI.U_IDHRSQTH
                            PBI.RouteSeqFriday = moPBI.U_IDHRSQF
                            PBI.RouteSeqSaturday = moPBI.U_IDHRSQSA
                            PBI.RouteSeqSunday = moPBI.U_IDHRSQSU

                            moPBI.U_IDHRECOM = moPBI.doDescribeCoverage()
                            PBI.Comment = moPBI.U_IDHRECOM

                            If AddUpdateWasteOrder() Then
                                bResult = UpdatePBI()
                                miLastInstructionsDone += 1
                                'If moData.mfInfoHandler IsNot Nothing Then
                                moData.doInfo("ProcessOrder", "Info", "PBI: " & moPBI.Code & " [ " & (moPBI.CurrentRowIndex + 1) & " of " & sCountStr & " - Done ]")
                                'End If
                                'If Not moExternalLogger Is Nothing Then
                                '    moExternalLogger("DoProcessing", "Info", "PBI: " & moPBI.Code & " [ " & (moPBI.CurrentRowIndex + 1) & " of " & sCountStr & " - Done ]")
                                'End If
                            Else
                                If Not moPBI.U_IDHIMPI Is Nothing AndAlso moPBI.U_IDHIMPI.Equals("IB") Then
                                    moPBI.U_IDHIMPI = "IE"
                                    moPBI.doProcessData()
                                End If

                                bResult = False
                                'If moData.mfInfoHandler IsNot Nothing Then
                                'moData.doInfo("ProcessOrder", "Info", "PBI: " & moPBI.Code & " [ " & (moPBI.CurrentRowIndex + 1) & " of " & sCountStr & "]") ' & " - " & getLastError() & "]")
                                moData.doInfo("ProcessOrder", "Info", "PBI: " & moPBI.Code & " [ " & (moPBI.CurrentRowIndex + 1) & " of " & sCountStr & " - Failed ]")

                                'If Not moExternalLogger Is Nothing Then
                                ''moExternalLogger("DoProcessing", "Error", "PBI: " & moPBI.Code & " [ " & (moPBI.CurrentRowIndex + 1) & " of " & sCountStr & " - " & getLastError() & "]")
                                '    moData.doError(DataHandler.ERRORLEVEL_SBO & ": " 
                                'End If
                                'End If
                            End If
                        Catch ex As Exception
                            ''moExternalLogger("DoProcessing", "Exception", "PBI: " & moPBI.Code & " [ " & (moPBI.CurrentRowIndex + 1) & " of " & sCountStr & " - " & ex.Message & "]")
                            'moData.doError(DataHandler.ERRORLEVEL_EXCEPTION & ": " & ex.ToString(), "PBI: " & moPBI.Code & " [ " & (moPBI.CurrentRowIndex + 1) & " of " & sCountStr & " - " & ex.Message & "]")
                            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPBI", {moPBI.Code, (moPBI.CurrentRowIndex + 1).ToString(), sCountStr})
                            bResult = False
                        End Try

                        If iMaxPBIs > 0 AndAlso moPBI.CurrentRowIndex >= (iMaxPBIs - 1) Then
                            'moData.doError("System: Maximum PBI's processed", "Maximum PBI's processed.")
                            com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Maximum PBI's processed", "ERSYPBIM", {Nothing})
                            Exit While
                        End If

                        If Not dActionEndDate = DateTime.MinValue AndAlso DateTime.Compare(DateTime.Now, dActionEndDate) >= 0 Then
                            'moData.doError("System: The Process timedout. The requested stop Date is [" & dActionEndDate & "].", "The Process timedout. The requested stop Date is [" & dActionEndDate & "].")
                            com.idh.bridge.DataHandler.INSTANCE.doResSystemError("The Process timedout. The requested stop Date is [" & dActionEndDate & "].", "ERSYPTO", {dActionEndDate.ToString()})
                            Exit While
                        End If
                    End While

                    Dim sCompletion As String
                    If miLastInstructionsToDo = miLastInstructionsDone Then
                        sCompletion = " [Done all " & miLastInstructionsToDo & " PBI's]"
                    Else
                        sCompletion = " [Done " & miLastInstructionsDone & " of " & miLastInstructionsToDo & " PBI's - Failed " & (miLastInstructionsToDo - miLastInstructionsDone) & " - Please review the Error table..]"
                    End If

                    If bResult = True Then
                        moData.doInfo("ProcessOrder", "Info", sCompletion & " - End normally.")
                    Else
                        moData.doInfo("ProcessOrder", "Info", "[Action Date: " & mdThisRunDate & sCompletion & " - Not all processes completed successfully.")
                    End If
                Else
                    moData.doInfo("ProcessOrder", "Info", "[There were no PBI's to process.")
                End If
                Return bResult
            Else
                'moData.doError("System: Company not connected", "Company not connected.")
                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Company not connected.", "ERSYCNC", {Nothing})
            End If
            Return True

        Catch ex As Exception
            'moData.doError("Exception: " + ex.Message, "Error processing PBI [" & sCurrentPBI & "].")
            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError("Error processing PBI [" & sCurrentPBI & "].", "EREXPPBI", {sCurrentPBI})
            Return False
        Finally
            If PBI_Code Is Nothing OrElse PBI_Code.Length = 0 Then
                DataHandler.INSTANCE.doReleaseObject(CType(moPBI, Object))
            End If

            PBI = Nothing
        End Try
    End Function
    Private Function doValidateCarrierLinkBP(sCurrentPBI As String, sCarrierCd As String, sWasteCode As String) As Boolean
        Try
            doValidateCarrierLinkBP = False
            Dim sIDHCWBB As String = Config.INSTANCE.getValueFromOITM(sWasteCode, "U_IDHCWBB").ToString
            If Not (sIDHCWBB.Equals("Y")) Then
                Exit Function
            End If
            Dim sLinkedBP_Carrier As String = If(Config.INSTANCE.getValueFromBP(sCarrierCd, "U_IDHLBPC") Is Nothing, "", Config.INSTANCE.getValueFromBP(sCarrierCd, "U_IDHLBPC").ToString())
            If sLinkedBP_Carrier.Trim = "" Then
                Exit Function
            End If
            If Not Config.INSTANCE.doCheckBPActive(sLinkedBP_Carrier, DateTime.Now) Then
                Return True
            End If
        Catch ex As Exception
            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError("Error processing PBI [" & sCurrentPBI & "].", "ERVLDPBI", {sCurrentPBI})
            Return False

        End Try
    End Function
#Region "Quantity and Price Calculations"
    '     0     - The Occurance D - Daily,W - Weekly,M - Monthly,Y - Yearly,O - Other
    '     1,2,3 - Frequency 0 - 999
    '
    ' #### FOR OCCURANCE D/W/M/O
    '     4     - Sunday - N/Y
    '     5     - Monday - N/Y
    '     6     - Tuesday - N/Y
    '     7     - Wednesday - N/Y
    '     8     - Thursday - N/Y
    '     9     - Friday - N/Y
    '     10    - Saterday - N/Y
    ' #### OPTION END
    '
    ' #### FOR OCCURANCE Y
    '     4 	 - OPTIONS - 1 - Date, 2 - DOW in month
    '     5,6   - MONTH - 1-12
    '
    '     ###   FOR OPTION 1
    ' 	7,8   - Day of the month
    '     ###   OPTION END
    '
    '	    ###   FOR OPTION 2
    '     7     - DOW where Sun = 0
    '     8     - WOM 1,2,3,4,L (L for last)
    '     ###   OPTION END
    '
    '     9     - 0
    '     10    - 0
    ' #### OPTION END
    '
    '     11-18 - Date from - 20071230 / 00000000
    '     19-26 - Date to or Occurences - 20071230 / 00000000
    '
    ' >>>Used for PBI
    '     27-34 - Period Start Date
    '     35-42 - Period End Date
    '
    'Some issue with the Option's on values => D = 1, Y = 2
    Private Function doCreateRecureString(ByVal mPBI As PBI_STRUCT) As String
        Dim sResult As String = ""
        Dim sWrk As String

        If mPBI.Pattern = "1" Then
            sResult = "D"
        ElseIf mPBI.Pattern = "2" Then
            sResult = "Y"
        Else
            sResult = mPBI.Pattern
        End If

        sWrk = com.idh.utils.Conversions.ToString(mPBI.WeekCount)
        If sWrk.Length = 0 Then
            sWrk = "000"
        Else
            sWrk = sWrk.PadLeft(3, "0"c)
        End If
        sResult = sResult & sWrk

        If sResult.Chars(0) = "Y" Then
            sResult = sResult & mPBI.sEFType

            If mPBI.iEFMonth < 10 Then
                sResult = sResult & "0" & mPBI.iEFMonth
            Else
                sResult = sResult & mPBI.iEFMonth
            End If

            If mPBI.sEFType = "1" Then
                If mPBI.iEFDay < 10 Then
                    sResult = sResult & "0" & mPBI.iEFDay
                Else
                    sResult = sResult & mPBI.iEFDay
                End If
            Else
                sResult = sResult & mPBI.iEFDOW & mPBI.sEFWeek
            End If

            sResult = sResult & "00"
        Else
            If mPBI.Sun = "Y" Then
                sResult = sResult & "Y"
            Else
                sResult = sResult & "N"
            End If

            If mPBI.Mon = "Y" Then
                sResult = sResult & "Y"
            Else
                sResult = sResult & "N"
            End If

            If mPBI.Tue = "Y" Then
                sResult = sResult & "Y"
            Else
                sResult = sResult & "N"
            End If

            If mPBI.Wed = "Y" Then
                sResult = sResult & "Y"
            Else
                sResult = sResult & "N"
            End If

            If mPBI.Thu = "Y" Then
                sResult = sResult & "Y"
            Else
                sResult = sResult & "N"
            End If

            If mPBI.Fri = "Y" Then
                sResult = sResult & "Y"
            Else
                sResult = sResult & "N"
            End If

            If mPBI.Sat = "Y" Then
                sResult = sResult & "Y"
            Else
                sResult = sResult & "N"
            End If
        End If

        If com.idh.utils.dates.isValidDate(mPBI.StartDate) Then
            sResult = sResult & mPBI.StartDate.ToString("yyyyMMdd")
        Else
            sResult = sResult & "00000000"
        End If

        Dim bValidEndDate As Boolean = False
        If com.idh.utils.dates.isValidDate(mPBI.EndDate) Then
            sResult = sResult & mPBI.EndDate.ToString("yyyyMMdd")
            bValidEndDate = True
        Else
            sWrk = com.idh.utils.Conversions.ToString(PBI.RecurCount)
            sWrk = sWrk.PadLeft(8, "0"c)
            sResult = sResult & sWrk
        End If

        If com.idh.utils.dates.isValidDate(mPBI.PeriodStartDate) Then
            sResult = sResult & mPBI.PeriodStartDate.ToString("yyyyMMdd")
        Else
            sResult = sResult & "00000000"
        End If

        If com.idh.utils.dates.isValidDate(mPBI.PeriodEndDate) Then
            'If bValidEndDate AndAlso Date.Compare(mPBI.EndDate, mPBI.PeriodEndDate) > 0Then
            '	sResult = sResult & mPBI.EndDate.ToString("yyyyMMdd")
            'Else
            sResult = sResult & mPBI.PeriodEndDate.ToString("yyyyMMdd")
            'End If
        Else
            sResult = sResult & "00000000"
        End If

        If sResult.StartsWith("O000NNNNNNN") = False Then
            Return "PBI-" & sResult
        Else
            Return ""
        End If
    End Function
#End Region

#Region "Data Adds & Updates"
    Private Function CalculateNextDate() As Date
        Return CalculateNextDate(PBI.NextRun)
    End Function

    Private Function CalculateNextDate(ByVal dStartDate As Date) As Date
        Dim dNextRun As Date

        If PBI.UsePeriodEnd Then
            'If this is a Quaterly run find the End of this Quater
            If PBI.PeriodType.Equals(PER_Quarts) Then
                'Jan-March [1-3] = 1
                'Apr-Jun [4-6] = 2
                'Jul-Sept [7-9] = 3
                'Oct-Dec [10-12] = 4
                If dStartDate.Month >= 0 AndAlso dStartDate.Month <= 3 Then
                    dNextRun = New Date(dStartDate.Year, 4, 1)
                ElseIf dStartDate.Month >= 4 AndAlso dStartDate.Month <= 6 Then
                    dNextRun = New Date(dStartDate.Year, 7, 1)
                ElseIf dStartDate.Month >= 7 AndAlso dStartDate.Month <= 9 Then
                    dNextRun = New Date(dStartDate.Year, 10, 1)
                ElseIf dStartDate.Month >= 10 AndAlso dStartDate.Month <= 12 Then
                    dNextRun = New Date(dStartDate.Year + 1, 1, 1)
                End If
                dNextRun = dNextRun.AddMonths((PBI.RecurFreq - 1) * 3)
            Else
                dNextRun = dStartDate.AddMonths(PBI.RecurFreq)
                dNextRun = New Date(dNextRun.Year, dNextRun.Month, 1)
            End If
            'If Config.ParameterAsBool("PBIRTDAY", True) Then
            '    While (Date.Compare(dNextRun, New Date(DateTime.Today.AddMonths(1).Year, DateTime.Today.AddMonths(1).Month, 1)) < 0)
            '        dNextRun = dNextRun.AddMonths(PBI.RecurFreq)
            '    End While
            'End If

        Else 'Then
            If PBI.PeriodType.Equals(PER_Months) Then
                dNextRun = dStartDate.AddMonths(PBI.RecurFreq)
            ElseIf PBI.PeriodType.Equals(PER_Quarts) Then
                dNextRun = dStartDate.AddMonths(PBI.RecurFreq * 3)
        Else
                dNextRun = dStartDate.AddDays(PBI.RecurFreq * 7)
            End If
            'If Config.ParameterAsBool("PBIRTDAY", True) Then
            '    While (Date.Compare(dNextRun, DateTime.Today.AddMonths(1)) < 0)
            '        dNextRun = dNextRun.AddMonths(PBI.RecurFreq)
            '    End While
            'End If
        End If

        Return dNextRun
    End Function

    Private Function AddUpdateWasteOrder() As Boolean
        Dim bIsNewWO As Boolean
        Dim bIsNewWOR As Boolean

        DataHandler.INSTANCE.DebugTick500 = "PBI.01"

        Dim oWO As IDH_JOBENTR = New IDH_JOBENTR()
        oWO.MustLoadChildren = True
        oWO.DoUpdateInfo = DoUpdateInfo
        Try
            DataHandler.INSTANCE.DebugTick500 = "PBI.02"

            Dim sWhere As String = IDH_JOBENTR._PBICODE & " = '" & PBI.Code & "'"
            Dim sOrder As String = "CAST(" & IDH_JOBENTR._PBICODE & " As int) DESC"
            If (oWO.getData(sWhere, sOrder, 1) = 0) Then

                DataHandler.INSTANCE.DebugTick500 = "PBI.03"

                bIsNewWO = True
                bIsNewWOR = True

                oWO.U_BDate = mdThisRunDate
                oWO.U_BTime = "0"
                oWO.U_RSDate = PBI.StartDate
                oWO.U_RSTime = "0"
                oWO.U_RETime = "0"
                oWO.U_CardCd = PBI.CardCode
                oWO.U_CardNM = PBI.CardName
                oWO.U_CustRef = ""
                oWO.U_Address = PBI.SiteAddress
                oWO.U_CAddrsLN = PBI.SiteAddressAddrsCd
                oWO.U_Street = PBI.CusStreet
                oWO.U_Block = PBI.CusBlock
                oWO.U_City = PBI.CusCity
                oWO.U_ZpCd = PBI.CusZip
                oWO.U_Phone1 = PBI.CusPhone
                oWO.U_ItemGrp = PBI.ContGrp
                oWO.U_Qty = 0
                oWO.U_ORoad = "N"
                oWO.U_SLicSp = ""
                oWO.U_SLicNr = ""
                oWO.U_SLicCh = 0
                oWO.U_SLicNm = ""
                oWO.U_RTIMEF = "0"
                oWO.U_RTIMET = "0"
                oWO.U_SupRef = ""
                oWO.U_SiteTl = ""
                ''##MA Start Issue#439
                oWO.U_TRNCd = PBI.TRNCd
                ''##MA End Issue#439

                DataHandler.INSTANCE.DebugTick500 = "PBI.03"

                Dim oLinkedBP As LinkedBP = lookups.Config.INSTANCE.doGetLinkedBP(PBI.CardCode)
                If oLinkedBP IsNot Nothing Then
                    oWO.U_PCardCd = oLinkedBP.LinkedBPCardCode
                    oWO.U_PCardNM = oLinkedBP.LinkedBPName

                Else
                    '# Start 1141 08-06-2016
                    'oWO.U_PCardCd = PBI.CardCode
                    'oWO.U_PCardNM = PBI.CardName
                    oWO.U_PCardCd = ""
                    oWO.U_PCardNM = ""
                    '# End 1141 08-06-2016
                End If

                DataHandler.INSTANCE.DebugTick500 = "PBI.04"
                If oWO.U_PCardCd <> "" Then
                    oWO.U_PAddress = PBI.SiteAddress
                    oWO.U_PAddrsLN = PBI.SiteAddressAddrsCd
                    oWO.U_PStreet = PBI.CusStreet
                    oWO.U_PBlock = PBI.CusBlock
                    oWO.U_PCity = PBI.CusCity
                    oWO.U_PZpCd = PBI.CusZip
                    oWO.U_PPhone1 = PBI.CusPhone
                End If

                oWO.U_SCardCd = PBI.DisCode
                oWO.U_SCardNM = PBI.DisName
                oWO.U_SAddress = PBI.DisAddress
                oWO.U_SAddrsLN = PBI.DisAddressAddrsCd
                oWO.U_SStreet = PBI.DisStreet
                oWO.U_SBlock = PBI.DisBlock
                oWO.U_SCity = PBI.DisCity
                oWO.U_SZpCd = PBI.DisZip
                oWO.U_SPhone1 = PBI.DisPhone

                oWO.U_CCardCd = PBI.CarrCode
                oWO.U_CCardNM = PBI.CarrName
                oWO.U_CAddress = PBI.CarAddress
                oWO.U_CAddrsLN = PBI.CarAddressAddrsCd
                oWO.U_CStreet = PBI.CarStreet
                oWO.U_CBlock = PBI.CarBlock
                oWO.U_CCity = PBI.CarCity
                oWO.U_CZpCd = PBI.CarZip
                oWO.U_CPhone1 = PBI.CarPhone

                oWO.U_PBICODE = PBI.Code
                oWO.U_Route = ""
                oWO.U_Seq = 0
                oWO.U_Status = "1"

                DataHandler.INSTANCE.DebugTick500 = "PBI.05"

                'msHeaderIndicator = "HN"
                If oWO.doAddDataRow() = False Then

                    DataHandler.INSTANCE.DebugTick500 = "PBI.06"

                    Return False
                End If
            Else

                DataHandler.INSTANCE.DebugTick500 = "PBI.07"

                bIsNewWO = False
                bIsNewWOR = False

                oWO.U_RSDate = PBI.StartDate
                oWO.U_CardCd = PBI.CardCode
                oWO.U_CardNM = PBI.CardName
                oWO.U_Address = PBI.SiteAddress
                oWO.U_AddrssLN = PBI.SiteAddressAddrsCd
                oWO.U_Street = PBI.CusStreet
                oWO.U_Block = PBI.CusBlock
                oWO.U_City = PBI.CusCity
                oWO.U_ZpCd = PBI.CusZip
                oWO.U_Phone1 = PBI.CusPhone
                oWO.U_ItemGrp = PBI.ContGrp
                oWO.U_Qty = 0
                oWO.U_SpInst = PBI.Comment
                ''##MA Start Issue#439
                oWO.U_TRNCd = PBI.TRNCd
                ''##MA End Issue#439


                DataHandler.INSTANCE.DebugTick500 = "PBI.08"

                Dim oLinkedBP As LinkedBP = lookups.Config.INSTANCE.doGetLinkedBP(PBI.CardCode)
                If oLinkedBP IsNot Nothing Then
                    oWO.U_PCardCd = oLinkedBP.LinkedBPCardCode
                    oWO.U_PCardNM = oLinkedBP.LinkedBPName

                Else
                    oWO.U_PCardCd = PBI.CardCode
                    oWO.U_PCardNM = PBI.CardName
                End If

                DataHandler.INSTANCE.DebugTick500 = "PBI.09"

                oWO.U_PAddress = PBI.SiteAddress
                oWO.U_PAddrsLN = PBI.SiteAddressAddrsCd
                oWO.U_PStreet = PBI.CusStreet
                oWO.U_PBlock = PBI.CusBlock
                oWO.U_PCity = PBI.CusCity
                oWO.U_PZpCd = PBI.CusZip
                oWO.U_PPhone1 = PBI.CusPhone

                oWO.U_CCardCd = PBI.CarrCode
                oWO.U_CCardNM = PBI.CarrName
                oWO.U_CAddress = PBI.CarAddress
                oWO.U_CAddrsLN = PBI.CarAddressAddrsCd
                oWO.U_CStreet = PBI.CarStreet
                '                oWO.U_CBlock = PBI.CarBlock
                oWO.U_CCity = PBI.CarCity
                oWO.U_CZpCd = PBI.CarZip
                oWO.U_CPhone1 = PBI.CarPhone

                oWO.U_SCardCd = PBI.DisCode
                oWO.U_SCardNM = PBI.DisName
                oWO.U_SAddress = PBI.DisAddress
                oWO.U_SAddrsLN = PBI.DisAddressAddrsCd
                oWO.U_SStreet = PBI.DisStreet
                oWO.U_SBlock = PBI.DisBlock
                oWO.U_SCity = PBI.DisCity
                oWO.U_SZpCd = PBI.DisZip
                oWO.U_SPhone1 = PBI.DisPhone

                DataHandler.INSTANCE.DebugTick500 = "PBI.10"

                'msHeaderIndicator = "HU"
                If oWO.doUpdateDataRow() = False Then

                    DataHandler.INSTANCE.DebugTick500 = "PBI.11"

                    Return False
                End If
            End If

            DataHandler.INSTANCE.DebugTick500 = "PBI.12"

            PBI.WOCode = oWO.Code 'sWOCode

            'Check if it is a new line or if it needs to be updated
            'New row to be created if the following is true:
            '1) A new Waste Order was created. i.e bIsNewWO = True
            '2) The Actual End date of the row has been reached. This date was calculated by adding the recurrance frequency of the
            'instruction (i.e. 4 months) to the Start Date.

            ''USED FOR DEBUGGING
            Dim sNextPeriodStartDate As String = ""
            Dim sPrevPeriodEndDate As String = ""
            Dim sLastJobDate As String = ""
            Dim sThisRunDate As String = mdThisRunDate.Date.ToString()

            Dim sRecurString As String = Nothing
            moPBIDetail = Nothing
            Dim oNextPeriodStartDate As DateTime = DateTime.MinValue
            Dim oPrevPeriodEndDate As DateTime = DateTime.MinValue

            DataHandler.INSTANCE.DebugTick500 = "PBI.13"

            If bIsNewWOR = False Then
                moPBIDetail = New IDH_PBIDTL()

                DataHandler.INSTANCE.DebugTick500 = "PBI.14"

                moPBIDetail.DoUpdateInfo = DoUpdateInfo
                If moPBIDetail.getPeriodJobs(PBI.Code, mdThisRunDate.Date) = 0 Then

                    DataHandler.INSTANCE.DebugTick500 = "PBI.15"

                    moPBIDetail = Nothing
                    bIsNewWOR = True

                    'This should return the start date of the First Period or the start date of the Period just after this dead period..
                    'oFirstPeriodStartDate = IDH_PBIDTL.getFirstPeriodStart(PBI.Code)

                    'If no period is returnd it might indicate that the Run date is before the First Period ... OR ...
                    'That this might be a dead period where no jobs where run...
                    oNextPeriodStartDate = IDH_PBIDTL.getNextPeriodStart(PBI.Code, mdThisRunDate.Date)


                    DataHandler.INSTANCE.DebugTick500 = "PBI.16"


                    If Dates.isValidDate(oNextPeriodStartDate) = False Then
                        oPrevPeriodEndDate = IDH_PBIDTL.getPreviousPeriodEnd(PBI.Code, mdThisRunDate.Date)
                    End If

                    DataHandler.INSTANCE.DebugTick500 = "PBI.17"

                End If
            End If

            DataHandler.INSTANCE.DebugTick500 = "PBI.18"

            sNextPeriodStartDate = oNextPeriodStartDate.ToString()
            sPrevPeriodEndDate = oPrevPeriodEndDate.ToString()

            'This will get the last job created - possably in the previous period...
            Dim oLastJobDate As DateTime = DateTime.MinValue
            If bIsNewWO = False Then
                oLastJobDate = IDH_PBIDTL.getLastCreatedJob(PBI.Code, mdThisRunDate.Date)
            End If
            sLastJobDate = oLastJobDate.ToString()

            DataHandler.INSTANCE.DebugTick500 = "PBI.19"

            'This Should only be True if the WO is new or no PBIDetails could be found
            If bIsNewWOR = False Then
                gNew = False
                ''If the NextRun Date is smaller than the LastRun Jobs date recalculate the NextRun Date
                'Can not do this because this might be a period in the middle of a serries
                'If Date.Compare( PBI.NextRun < oLastJobDate ) < 0 Then
                '	PBI.NextRun = CalculateNextDate()
                'End If

                'If Not moPBIDetail Is Nothing Then
                'mdCalculatedNextRun = CalculateNextDate(moPBIDetail.U_IDHPRDSD)
                'End If
                'Return doAddPBIDetailAndWORs(oWO, oLastJobDate, False)
                Dim bResult As Boolean = doAddPBIDetailAndWORs(oWO, oLastJobDate)

                DataHandler.INSTANCE.DebugTick500 = "PBI.20"

                Return bResult
            Else

                DataHandler.INSTANCE.DebugTick500 = "PBI.21"

                gNew = True
                Dim dStartDate As Date
                Dim dEndDate As Date

                'This is For an update with the Action date before the Next Run, indicating an update of an Period
                If mbRunDateWasSet AndAlso Date.Compare(mdThisRunDate, PBI.NextRun) < 0 Then

                    DataHandler.INSTANCE.DebugTick500 = "PBI.22"

                    If Dates.isValidDate(oPrevPeriodEndDate) Then

                        DataHandler.INSTANCE.DebugTick500 = "PBI.23"

                        dStartDate = oPrevPeriodEndDate
                        dStartDate = dStartDate.AddMonths(1)
                        dStartDate = New Date(dStartDate.Year, dStartDate.Month, 1)

                        dEndDate = doCalculatePeriodEndDate(dStartDate)

                        DataHandler.INSTANCE.DebugTick500 = "PBI.24"

                    Else

                        DataHandler.INSTANCE.DebugTick500 = "PBI.25"

                        If Dates.isValidDate(oNextPeriodStartDate) Then

                            DataHandler.INSTANCE.DebugTick500 = "PBI.26"

                            dEndDate = oNextPeriodStartDate
                            dEndDate = dEndDate.AddDays(-1)
                        Else

                            DataHandler.INSTANCE.DebugTick500 = "PBI.27"

                            dEndDate = PBI.NextRun
                            dEndDate = dEndDate.AddDays(-1)
                        End If

                        DataHandler.INSTANCE.DebugTick500 = "PBI.28"

                        dStartDate = doCalculatePeriodStartDate(dEndDate)

                        DataHandler.INSTANCE.DebugTick500 = "PBI.29"

                    End If

                    DataHandler.INSTANCE.DebugTick500 = "PBI.30"

                    PBI.PeriodStartDate = dStartDate
                    PBI.PeriodEndDate = dEndDate
                    PBI.RecuringStr = doCreateRecureString(PBI)

                    DataHandler.INSTANCE.DebugTick500 = "PBI.31"

                    PBI.JobDates = com.idh.bridge.utils.Dates.doCreateScheduleDates(PBI.CardCode, PBI.DisCode, PBI.CarrCode, PBI.RecuringStr, oLastJobDate)

                    DataHandler.INSTANCE.DebugTick500 = "PBI.32"

                Else

                    DataHandler.INSTANCE.DebugTick500 = "PBI.33"

                    While True

                        DataHandler.INSTANCE.DebugTick500 = "PBI.34"

                        dStartDate = PBI.NextRun
                        mdCalculatedNextRun = CalculateNextDate()
                        dEndDate = mdCalculatedNextRun.AddDays(-1)

                        If Dates.isValidDate(PBI.EndDate) Then
                            If Date.Compare(dEndDate, PBI.EndDate) > 0 Then
                                dEndDate = PBI.EndDate
                            End If
                        End If

                        PBI.NextRun = mdCalculatedNextRun

                        PBI.PeriodStartDate = dStartDate
                        PBI.PeriodEndDate = dEndDate
                        PBI.RecuringStr = doCreateRecureString(PBI)

                        DataHandler.INSTANCE.DebugTick500 = "PBI.35"

                        PBI.JobDates = com.idh.bridge.utils.Dates.doCreateScheduleDates(PBI.CardCode, PBI.DisCode, PBI.CarrCode, PBI.RecuringStr, oLastJobDate)

                        DataHandler.INSTANCE.DebugTick500 = "PBI.36"

                        If (Not PBI.JobDates Is Nothing AndAlso PBI.JobDates.Count > 0) _
                           OrElse (Dates.CompareDatePartOnly(mdThisRunDate, PBI.NextRun) > -1) Then
                            Exit While
                        ElseIf PBI.JobDates Is Nothing OrElse PBI.JobDates.Count <= 0 Then
                            Exit While
                        End If

                        DataHandler.INSTANCE.DebugTick500 = "PBI.37"

                        'If PBI.JobDates Is Nothing OrElse PBI.JobDates.Count <= 0 Then
                        '    Exit While
                        'ElseIf (dates.CompareDatePartOnly(mdThisRunDate, PBI.NextRun) > -1) Then
                        '    Exit While
                        'End If
                    End While
                End If

                DataHandler.INSTANCE.DebugTick500 = "PBI.38"

                If PBI.EndCount > 0 Then
                    If (Not PBI.JobDates Is Nothing AndAlso PBI.JobDates.Count > 0) Then
                        PBI.EndCount = PBI.EndCount - PBI.JobDates.Count
                        If PBI.EndCount < 0 Then
                            While PBI.EndCount < 0
                                PBI.JobDates.RemoveAt(PBI.JobDates.Count - 1)
                                PBI.EndCount += 1
                            End While
                        End If
                    End If
                End If

                Dim dQty2 As Double = PBI.Qty
                If PBI.DoDetail.Equals("Y") = False Then
                    If (Not PBI.JobDates Is Nothing AndAlso PBI.JobDates.Count > 0) Then
                        dQty2 = PBI.JobDates.Count * PBI.Qty
                    End If
                End If

                PBI.ContainerQty = dQty2

                DataHandler.INSTANCE.DebugTick500 = "PBI.39"

                Dim bResult As Boolean = doAddDetails(oWO, PBI.JobDates, PBI.PeriodStartDate, PBI.PeriodEndDate, PBI.RecuringStr)

                DataHandler.INSTANCE.DebugTick500 = "PBI.40"

                Return bResult
            End If
        Catch ex As Exception
            'moData.doError("Exception: " + ex.Message, "Error Adding or Updating the Waste Order.")
            DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXAUWO", {Nothing})
            Return False
        Finally
            DataHandler.INSTANCE.doReleaseObject(CType(oWO, Object))
        End Try
    End Function

    Private Function doCalculatePeriodEndDate(ByRef dStartDate As Date) As DateTime
        Dim dEndDate As DateTime

        If PBI.UsePeriodEnd Then
            'If this is a Quaterly run find the End of this Quater
            If PBI.PeriodType.Equals(PER_Quarts) Then
                'Jan-March [1-3] = 1
                'Apr-Jun [4-6] = 2
                'Jul-Sept [7-9] = 3
                'Oct-Dec [10-12] = 4
                If dStartDate.Month >= 0 AndAlso dStartDate.Month <= 3 Then
                    dEndDate = New Date(dStartDate.Year, 4, 1)
                    dEndDate = dEndDate.AddDays(-1)
                ElseIf dStartDate.Month >= 4 AndAlso dStartDate.Month <= 6 Then
                    dEndDate = New Date(dStartDate.Year, 7, 1)
                    dEndDate = dEndDate.AddDays(-1)
                ElseIf dStartDate.Month >= 7 AndAlso dStartDate.Month <= 9 Then
                    dEndDate = New Date(dStartDate.Year, 10, 1)
                    dEndDate = dEndDate.AddDays(-1)
                ElseIf dStartDate.Month >= 10 AndAlso dStartDate.Month <= 12 Then
                    dEndDate = New Date(dStartDate.Year + 1, 1, 1)
                    dEndDate = dEndDate.AddDays(-1)
                End If
                dEndDate = dEndDate.AddMonths((PBI.RecurFreq - 1) * 3)
            Else
                dEndDate = dStartDate.AddMonths(PBI.RecurFreq)
                dEndDate = (New Date(dEndDate.Year, dEndDate.Month, 1)).AddDays(-1)
            End If
        Else
            If Not PBI.PeriodType.Equals(PER_Months) Then
            dEndDate = dStartDate.AddMonths(PBI.RecurFreq)
            dEndDate = dEndDate.AddDays(-1)
            ElseIf Not PBI.PeriodType.Equals(PER_Quarts) Then
                dEndDate = dStartDate.AddMonths(PBI.RecurFreq * 3)
                dEndDate = dEndDate.AddDays(-1)
            Else
                dEndDate = dStartDate.AddDays(PBI.RecurFreq * 7)
                dEndDate = dEndDate.AddDays(-1)
            End If
        End If

        'The Period End Date must not be later than the PBI EndDate
        If dates.isValidDate(PBI.EndDate) Then
            If dates.CompareDatePartOnly(dEndDate, PBI.EndDate) > 0 Then
                dEndDate = PBI.EndDate
            End If
        End If
        Return dEndDate
    End Function

    Private Function doCalculatePeriodStartDate(ByRef dEndDate As DateTime) As DateTime
        Dim dStartDate As DateTime

        If PBI.UsePeriodEnd Then
            'If this is a Quaterly run find the End of this Quater
            If PBI.PeriodType.Equals(PER_Quarts) Then
                'Jan-March [1-3] = 1
                'Apr-Jun [4-6] = 2
                'Jul-Sept [7-9] = 3
                'Oct-Dec [10-12] = 4
                If dEndDate.Month >= 0 AndAlso dEndDate.Month <= 3 Then
                    dStartDate = New Date(dEndDate.Year - 1, 1, 1)
                ElseIf dEndDate.Month >= 4 AndAlso dEndDate.Month <= 6 Then
                    dStartDate = New Date(dEndDate.Year, 4, 1)
                ElseIf dEndDate.Month >= 7 AndAlso dEndDate.Month <= 9 Then
                    dStartDate = New Date(dEndDate.Year, 7, 1)
                ElseIf dEndDate.Month >= 10 AndAlso dEndDate.Month <= 12 Then
                    dStartDate = New Date(dEndDate.Year, 10, 1)
                End If
                dStartDate = dStartDate.AddMonths((PBI.RecurFreq - 1) * -3)
            Else
                dStartDate = dEndDate.AddMonths((PBI.RecurFreq - 1) * -1)
                dStartDate = New Date(dStartDate.Year, dStartDate.Month, 1)
            End If
        Else
            If PBI.PeriodType.Equals(PER_Months) Then
            dStartDate = dEndDate.AddMonths((PBI.RecurFreq - 1) * -1)
            dStartDate = dStartDate.AddDays(1)
            ElseIf PBI.PeriodType.Equals(PER_Quarts) Then
                dStartDate = dEndDate.AddMonths(((PBI.RecurFreq * 3) - 1) * -1)
                dStartDate = dStartDate.AddDays(1)
            Else
                dStartDate = dEndDate.AddDays(((PBI.RecurFreq * 7) - 1) * -1)
                dStartDate = dStartDate.AddDays(1)
            End If

        End If

        'The Period Start Date must not be before the PBI StartDate
        If dates.CompareDatePartOnly(dStartDate, PBI.StartDate) < 0 Then
            dStartDate = PBI.StartDate
        End If

        Return dStartDate
    End Function

    Private Function doAddDetails(ByRef oWO As IDH_JOBENTR, ByRef oJobDates As com.idh.bridge.utils.Dates.ScheduleDates, ByRef oStartDate As Date, ByRef oEndDate As Date, ByRef sRecurStr As String) As Boolean
        If moPBIDetail Is Nothing Then
            moPBIDetail = New IDH_PBIDTL()
            moPBIDetail.DoUpdateInfo = DoUpdateInfo
            'If PBI.EndCount > 0 Then
            'moPBIDetail.getByPBI(PBI.Code)
            'End If
        End If

        DataHandler.INSTANCE.DebugTick500 = "PBIADet.01"

        'If (moPBIDetail.doAddDetails(moPBI, oWO, Nothing, oJobDates, oStartDate, oEndDate, sRecurStr, True)) Then
        If (moPBIDetail.doAddDetails(moPBI, oWO, oJobDates, oStartDate, oEndDate, sRecurStr, True)) Then
            DataHandler.INSTANCE.DebugTick500 = "PBIADet.02"
            PBI.WOLineCode = moPBIDetail.LastWORCreated
            PBI.RunCount = PBI.RunCount + 1
            DataHandler.INSTANCE.DebugTick500 = "PBIADet.03"
            Return True
        Else
            DataHandler.INSTANCE.DebugTick500 = "PBIADet.04"
            Return False
        End If
        DataHandler.INSTANCE.DebugTick500 = "PBIADet.05"
        Return True
    End Function

    Private Function doAddUpdateDetails(ByRef oWO As IDH_JOBENTR, ByRef oActionDate As Date, ByRef oJobDates As com.idh.bridge.utils.Dates.ScheduleDates, ByRef oStartDate As Date, ByRef oEndDate As Date, ByRef sRecurStr As String) As Boolean
        If moPBIDetail Is Nothing Then
            moPBIDetail = New IDH_PBIDTL()
            moPBIDetail.DoUpdateInfo = DoUpdateInfo
        End If

        If (moPBIDetail.doAddUpdateDetails(moPBI, oWO, oActionDate, oJobDates, oStartDate, oEndDate, sRecurStr)) Then
            PBI.WOLineCode = moPBIDetail.LastWORCreated
            PBI.RunCount = PBI.RunCount + 1
            Return True
        Else
            Return False
        End If
    End Function

    'Private Function doAddPBIDetailAndWORs(ByRef oWO As IDH_JOBENTR, ByRef oLastJobDate As DateTime, ByVal bClearFirst As Boolean) As Boolean
    Private Function doAddPBIDetailAndWORs(ByRef oWO As IDH_JOBENTR, ByRef oLastJobDate As DateTime) As Boolean
        If moPBIDetail Is Nothing OrElse moPBIDetail.Count = 0 Then
            'moData.doError("System: [PBI - " & PBI.Code & "] The PBI Details could not be found.")
            com.idh.bridge.DataHandler.INSTANCE.doResSystemError("[PBI - " & PBI.Code & "] The PBI Details could not be found.", "ERSYPBII", {PBI.Code})
            Return False
        End If

        mdCalculatedNextRun = CalculateNextDate(moPBIDetail.U_IDHPRDSD)

        moPBIDetail.last()
        Dim dNewPeriodEnd As DateTime

        PBI.PeriodStartDate = moPBIDetail.U_IDHPRDSD

        Dim sStartDate As String = PBI.StartDate.ToString()
        Dim sPeriodStartDate As String = PBI.PeriodStartDate.ToString()

        If dates.CompareDatePartOnly(PBI.PeriodStartDate, PBI.StartDate) < 0 Then
            PBI.PeriodStartDate = PBI.StartDate
        End If

        'PBI.PeriodEndDate = moPBIDetail.U_IDHPRDED
        dNewPeriodEnd = doCalculatePeriodEndDate(PBI.PeriodStartDate)

        Dim sNewPeriodEnd As String = dNewPeriodEnd.ToString()
        Dim sPeriodEnd As String = moPBIDetail.U_IDHPRDED.ToString()

        If dates.CompareDatePartOnly(dNewPeriodEnd, moPBIDetail.U_IDHPRDED) <> 0 Then
            'The Calculated Period end is not the same as the current period end date
            'If there is jobs in the period following this Period raise and error and keep the Period End
            Dim iCount As Integer = IDH_PBIDTL.getJobCountAfterDate(PBI.Code, moPBIDetail.U_IDHPRDED)
            If iCount > 0 Then
                'moData.doError("System: [PBI - " & PBI.Code & "] Can not change the Frequency for PBI Periods with jobs generated after this period.")
                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("[PBI - " & PBI.Code & "] Can not change the Frequency for PBI Periods with jobs generated after this period.", "ERSYEPBI", {PBI.Code})
                PBI.PeriodEndDate = moPBIDetail.U_IDHPRDED
            Else
                Dim sPBINextRun As String = PBI.NextRun.ToString()
                Dim sNewNextRun As String = dNewPeriodEnd.ToString()
                PBI.PeriodEndDate = dNewPeriodEnd
                If Date.Compare(PBI.NextRun, dNewPeriodEnd) <> 0 Then
                    PBI.NextRun = dNewPeriodEnd.AddDays(1)
                End If
            End If
        Else
            PBI.PeriodEndDate = moPBIDetail.U_IDHPRDED
        End If
        moPBIDetail.first()

        '*
        ' Calculate the New dates from the Action Date to the end of the Period - Start
        '*
        PBI.RecuringStr = doCreateRecureString(PBI)
        Dim sRecurString As String = PBI.RecuringStr

        Dim sRDate As String = mdThisRunDate.ToString("yyyyMMdd")
        Dim sEDate As String

        Dim oPeriodEndDate As Date = PBI.PeriodEndDate
        Dim oTerminationDate As Date = PBI.EndDate
        If Date.Compare(oTerminationDate, oPeriodEndDate) < 0 Then
            sEDate = oTerminationDate.ToString("yyyyMMdd")
        Else
            sEDate = oPeriodEndDate.ToString("yyyyMMdd") 'PBI.PeriodEndDate.ToString("yyyyMMdd")
        End If

        '"PBI-W001NYNNNNN20091111201911112009111120100310"
        sRecurString = sRecurString.Substring(0, 31) & sRDate & sEDate
        Dim oNewDatesFromAction As com.idh.bridge.utils.Dates.ScheduleDates = com.idh.bridge.utils.Dates.doCreateScheduleDates(PBI.CardCode, PBI.DisCode, PBI.CarrCode, sRecurString, oLastJobDate)

        'This Details is the Detailed WOR's not the PBIDetail Record
        If PBI.DoDetail = "N" Then
            'If bClearFirst Then
            '    moPBIDetail.doDeleteFromDate(mdThisRunDate.Date, "PBI Instruction Changed.", False, True)
            'End If

            moPBIDetail.doCalculateQtyFromDate(mdThisRunDate.Date)

            Dim dCurrentQtyBeforeAction As Double = moPBIDetail.BeforeDateQty
            Dim dCurrentQtyFromAction As Double = moPBIDetail.FromDateQty
            Dim dNewQtyFromAction As Double = oNewDatesFromAction.Count * PBI.Qty
            Dim dNewTotalQty As Double = dCurrentQtyBeforeAction + dNewQtyFromAction

            PBI.JobDates = oNewDatesFromAction
            PBI.ContainerQty = dNewTotalQty

            'If bClearFirst = True Then
            '    Return doAddDetails(oWO, oNewDatesFromAction, PBI.PeriodStartDate, PBI.PeriodEndDate, PBI.RecuringStr)
            'Else
            Return doAddUpdateDetails(oWO, mdThisRunDate.Date, oNewDatesFromAction, PBI.PeriodStartDate, PBI.PeriodEndDate, PBI.RecuringStr)
            'End If

        Else
            'If bClearFirst Then
            '    moPBIDetail.doDeleteFromDate(mdThisRunDate.Date, "PBI Instruction Changed.", False, True)
            'End If

            PBI.JobDates = oNewDatesFromAction
            PBI.ContainerQty = PBI.Qty

            'If bClearFirst Then
            '    Return doAddDetails(oWO, oNewDatesFromAction, PBI.PeriodStartDate, PBI.PeriodEndDate, PBI.RecuringStr)
            'Else
            Return doAddUpdateDetails(oWO, mdThisRunDate.Date, oNewDatesFromAction, PBI.PeriodStartDate, PBI.PeriodEndDate, PBI.RecuringStr)
            'End If
        End If
    End Function


    'Private Function doRemoveAndAddPBIDetailAndWORs(ByRef oWO As IDH_JOBENTR, ByRef oLastJobDate As DateTime) As Boolean
    '    If moPBIDetail Is Nothing Then
    '        moData.doError("The PBI Details could not be found.")
    '        Return False
    '    End If
    '    moPBIDetail.last()
    '    PBI.PeriodStartDate = moPBIDetail.U_IDHPRDSD
    '    PBI.PeriodEndDate = moPBIDetail.U_IDHPRDED
    '    moPBIDetail.first()

    '    '*
    '    ' Calculate the New dates from the Action Date to the end of the Period - Start
    '    '*
    '    PBI.RecuringStr = doCreateRecureString(PBI)
    '    Dim sRecurString As String = PBI.RecuringStr

    '    Dim bResult As Boolean
    '    Dim sRDate As String = mdThisRunDate.ToString("yyyyMMdd")
    '    Dim sEDate As String

    '    Dim oPeriodEndDate As Date = PBI.PeriodEndDate
    '    Dim oTerminationDate As Date = PBI.EndDate
    '    If Date.Compare(oTerminationDate, oPeriodEndDate) < 0 Then
    '        sEDate = oTerminationDate.ToString("yyyyMMdd")
    '    Else
    '        sEDate = oPeriodEndDate.ToString("yyyyMMdd") 'PBI.PeriodEndDate.ToString("yyyyMMdd")
    '    End If

    '    '"PBI-W001NYNNNNN20091111201911112009111120100310"
    '    sRecurString = sRecurString.Substring(0, 31) & sRDate & sEDate
    '    Dim oNewDatesFromAction As ArrayList = moLookup.doCreateScheduleDates(sRecurString, oLastJobDate)

    '    'If the PBI Old PBI Information type was set to 'G' Or 'I' calculate the Quantity difference
    '    'G - Grouped
    '    'I - Invoiced
    '    'D - Detailed
    '    'B - Both Detailed And Drtailed Invoice

    '    'If moPBIDetail.U_IDHDTYP = "G" OrElse moPBIDetail.U_IDHDTYP = "I" Then

    '    'This Details is the Detailed WOR's not the PBIDetail Record
    '    If PBI.DoDetail = "N" Then
    '        moPBIDetail.doDeleteFromDate(mdThisRunDate.Date, "PBI Instruction Changed.", False, True)

    '        'If moPBIDetail.doDeleteFromDate(mdThisRunDate.Date, "PBI Instruction Changed.", False, True) = False Then
    '        'Return False
    '        'Else
    '        moPBIDetail.doCalculateQtyFromDate(mdThisRunDate.Date)

    '        Dim dCurrentQtyBeforeAction As Double = moPBIDetail.BeforeDateQty
    '        Dim dCurrentQtyFromAction As Double = moPBIDetail.FromDateQty
    '        Dim dNewQtyFromAction As Double = oNewDatesFromAction.Count * PBI.Qty
    '        Dim dNewTotalQty As Double = dCurrentQtyBeforeAction + dNewQtyFromAction

    '        PBI.JobDates = oNewDatesFromAction
    '        PBI.ContainerQty = dNewTotalQty

    '        'If AddUpateWORUseCode(oWO, Nothing) Then
    '        '    Return doAddDetails(True)
    '        'Else
    '        '    Return False
    '        'End If

    '        'Return moPBIDetail.doAddDetails(moPBI, oWO, oNewDatesFromAction, PBI.PeriodStartDate, PBI.PeriodEndDate, PBI.RecuringStr)
    '        Return doAddDetails(oWO, oNewDatesFromAction, PBI.PeriodStartDate, PBI.PeriodEndDate, PBI.RecuringStr)
    '        'End If

    '        'If moPBIDetail.doDeleteFromDate(mdThisRunDate.Date, "PBI Instruction Changed.") = False Then
    '        '    Return False
    '        'Else
    '        '    PBI.JobDates = oNewDatesFromAction
    '        '    PBI.ContainerQty = dNewTotalQty

    '        '    If moPBIDetail.WORInDeletedPBI.Count > 0 Then
    '        '        Dim oWOR As IDH_JOBSHD = New IDH_JOBSHD(oWO)
    '        '        For iRow As Integer = 0 To moPBIDetail.WORInDeletedPBI.Count - 1
    '        '            Dim sRowCode As String = moPBIDetail.WORInDeletedPBI.Item(iRow)
    '        '            If oWOR.getByKey(sRowCode) Then
    '        '                If oWOR.Count > 0 Then
    '        '                    oWOR.doDeleteData()
    '        '                End If
    '        '            End If
    '        '        Next
    '        '        moData.doReleaseObject(oWOR)
    '        '        bResult = AddUpateWORUseCode(oWO, Nothing)
    '        '    Else
    '        '        Dim sWOR As String = moPBIDetail.U_IDHWOR
    '        '        bResult = AddUpateWORUseCode(oWO, sWOR)
    '        '    End If

    '        '    If bResult Then
    '        '        Return doAddDetails(True)
    '        '    Else
    '        '        Return False
    '        '    End If

    '        'End If
    '    Else
    '        If moPBIDetail.Count > 0 Then
    '            moPBIDetail.doDeleteFromDate(mdThisRunDate.Date, "PBI Instruction Changed.", False, True)
    '            'If moPBIDetail.doDeleteFromDate(mdThisRunDate.Date, "PBI Instruction Changed.", False, True) = False Then
    '            '    Return False
    '            'Else
    '            ''If moPBIDetail.doDeleteFromDate(mdThisRunDate.Date) = False Then
    '            ''    Return False
    '            ''Else
    '            ''    If moPBIDetail.WORInDeletedPBI.Count > 0 Then
    '            ''        Dim oWOR As IDH_JOBSHD = New IDH_JOBSHD(oWO)
    '            ''        For iRow As Integer = 0 To moPBIDetail.WORInDeletedPBI.Count - 1
    '            ''            Dim sRowCode As String = moPBIDetail.WORInDeletedPBI.Item(iRow)
    '            ''            If oWOR.getByKey(sRowCode) AndAlso oWOR.Count > 0 Then
    '            ''                If oWOR.doDeleteData() = False Then
    '            ''                    Return False
    '            ''                End If
    '            ''            End If
    '            ''        Next
    '            ''        moData.doReleaseObject(oWOR)
    '            ''    End If
    '            ''End If

    '            PBI.JobDates = oNewDatesFromAction
    '            PBI.ContainerQty = PBI.Qty

    '            ''If AddUpateWORUseCode(oWO, Nothing) Then
    '            ''    Return doAddDetails(True)
    '            ''Else
    '            ''    Return False
    '            ''End If

    '            ''Return moPBIDetail.doAddDetails(moPBI, oWO, oNewDatesFromAction, PBI.PeriodStartDate, PBI.PeriodEndDate, PBI.RecuringStr)
    '            Return doAddDetails(oWO, oNewDatesFromAction, PBI.PeriodStartDate, PBI.PeriodEndDate, PBI.RecuringStr)
    '            'End If
    '        End If
    '    End If
    'End Function

    'Private Function AddUpateWORUseCode( ByRef oWO As IDH_JOBENTR, ByVal sRowCode As String ) As Boolean

    '       If sRowCode Is Nothing Then
    '       	Return AddUpateWORUseObj(oWO, Nothing )
    '       End If

    '       Dim oWOR As IDH_JOBSHD = New IDH_JOBSHD(oWO)
    '	If oWOR.getByKey( sRowCode ) AndAlso oWOR.Count > 0 Then
    '		Return AddUpateWORUseObj(oWO, oWOR )
    '	Else
    '		Return AddUpateWORUseObj(oWO, Nothing )
    '	End If
    '    End Function

    'Private Function AddUpateWORUseObj ( _
    '    ByRef oWO As IDH_JOBENTR, _
    '    ByVal oWOR As IDH_JOBSHD) As Boolean

    '    Dim oCost As Price ' = New Price()
    '    Dim oCharge As Price ' = New Price()
    '    Dim ChargeVat As Double = 0
    '    Dim CostVat As Double = 0
    '    Dim ChargeSum As Double = 0
    '    Dim CostSum As Double = 0
    '    Dim ChargeTot As Double = 0
    '    Dim CostTot As Double = 0

    '    Try
    '        'oCost = moLookup.doGetJobCostPrice("Waste Order", PBI.JobType, PBI.ContGrp, PBI.ContCode, PBI.CarrCode, 0, "t", PBI.WasteCode, PBI.CardCode, PBI.SiteAddress, Date.Now(), PBI.Branch, PBI.CusZip)
    '        'oCharge = moLookup.doGetJobChargePrices("Waste Order", PBI.JobType, PBI.ContGrp, PBI.ContCode, PBI.CardCode, 0, "t", PBI.WasteCode, PBI.SiteAddress, Date.Now(), PBI.Branch, PBI.CusZip)

    '        '#####################################################################################################
    '        'NEED TO CALCULATE ACTION DATE (RDATE, ASDATE) FROM FREQUENCY SETTINGS AND START DATE
    '        'ALSO NEED TO CALCULATE COST UPDATES WHEN ROW IS UPDATED.
    '        '#####################################################################################################
    '        PBI.WORs = New ArrayList()
    '        If oWOR Is Nothing OrElse oWOR.Count = 0 Then
    '            PBI.RunCount = PBI.RunCount + 1

    '            '*************************
    '            '* This is a New Row
    '            '*************************
    '            oWOR = New IDH_JOBSHD(oWO)
    '            oWOR.U_WasCd = PBI.WasteCode
    '            oWOR.U_WasDsc = PBI.WasteDesc

    '            oWOR.U_CustCd = PBI.CardCode
    '            oWOR.U_CustNm = PBI.CardName
    '            oWOR.U_Tip = PBI.CarrCode
    '            oWOR.U_TipNm = PBI.CarrName

    '            If oWOR.U_UOM Is Nothing OrElse oWOR.U_UOM.Length = 0 Then
    '                oWOR.U_UOM = "t"
    '            End If

    '            If oWOR.U_PUOM Is Nothing OrElse oWOR.U_PUOM.Length = 0 Then
    '                oWOR.U_PUOM = "t"
    '            End If

    '            Dim dRowStartDate As DateTime
    '            Dim dRowEndDate As DateTime

    '            dRowStartDate = PBI.PeriodStartDate
    '            dRowEndDate = PBI.PeriodEndDate

    '            oWOR.U_JobNr = oWO.Code
    '            oWOR.U_JobTp = PBI.JobType
    '            oWOR.U_RTime = 0
    '            oWOR.U_RTimeT = 0
    '            oWOR.U_ASTime = 0
    '            oWOR.U_AETime = 0

    '            oWOR.U_ItmGrp = PBI.ContGrp
    '            oWOR.U_ItemCd = PBI.ContCode
    '            oWOR.U_ItemDsc = PBI.ContDesc
    '            oWOR.U_Status = "Open"
    '            'oWOR.U_Tip = PBI.CarrCode
    '            'oWOR.U_TipNm = PBI.CarrName
    '            'oWOR.U_CustCd = PBI.CardCode
    '            'oWOR.U_CustNm = PBI.CardName
    '            oWOR.U_CarrCd = PBI.CarrCode
    '            oWOR.U_CarrNm = PBI.CarrName
    '            oWOR.U_OrdWgt = PBI.ContainerQty
    '            oWOR.U_CusQty = PBI.ContainerQty
    '            oWOR.U_HlSQty = PBI.ContainerQty
    '            oWOR.U_IntComm = PBI.Comment
    '            oWOR.U_User = PBI.Owner
    '            oWOR.U_Branch = PBI.Branch

    '            '                oWOR.U_VtCostAmt = CostVat
    '            oWOR.U_BTime = 0
    '            oWOR.U_VehTyp = ""
    '            oWOR.U_Lorry = ""
    '            oWOR.U_Driver = ""
    '            oWOR.U_DocNum = ""
    '            oWOR.U_SupRef = ""
    '            oWOR.U_UseWgt = "1"
    '            oWOR.U_SAINV = ""
    '            oWOR.U_SAORD = ""
    '            oWOR.U_TIPPO = ""
    '            oWOR.U_JOBPO = ""
    '            oWOR.U_Dista = 0
    '            oWOR.U_Wei1 = 0
    '            oWOR.U_Wei2 = 0
    '            oWOR.U_Ser1 = ""
    '            oWOR.U_Ser2 = ""
    '            oWOR.U_AddEx = 0
    '            oWOR.U_PStat = "Open"
    '            oWOR.U_TRLReg = ""
    '            oWOR.U_TRLNM = ""
    '            oWOR.U_WASLIC = ""
    '            oWOR.U_SLPO = ""
    '            oWOR.U_SLicCst = 0
    '            oWOR.U_SLicCTo = 0
    '            oWOR.U_SLicCQt = 1
    '            oWOR.U_WROrd = ""
    '            oWOR.U_WRRow = ""
    '            oWOR.U_PayMeth = "Accounts"
    '            oWOR.U_CustRef = ""
    '            oWOR.U_RdWgt = 0
    '            oWOR.U_LorryCd = ""
    '            oWOR.U_Covera = PBI.RecuringStr
    '            'oWOR.U_UOM = "t"
    '            'oWOR.U_PUOM = "t"
    '            oWOR.U_AUOM = ""
    '            oWOR.U_AUOMQt = 0
    '            oWOR.U_PayStat = "UnPaid"
    '            oWOR.U_CCType = 0
    '            oWOR.U_JobRmT = 0
    '            oWOR.U_RemNot = ""
    '            oWOR.U_RemCnt = 0
    '            oWOR.U_IssQty = 0
    '            oWOR.U_CoverHst = ""
    '            oWOR.U_Obligated = PBI.Obligated
    '            oWOR.U_Origin = ""
    '            oWOR.U_CustCs = ""
    '            oWOR.U_ConNum = ""
    '            oWOR.U_LnkPBI = PBI.Code
    '            oWOR.U_CarrReb = "N"
    '            oWOR.U_MANPRC = 0

    '            oWOR.U_Sched = "Y"
    '            oWOR.U_USched = "N"

    '            oWOR.U_RowSta = com.idh.bridge.lookups.FixedValues.getStatusOpen()

    '            '
    '            '                oWOR.U_Price = ChargeSum
    '            '               oWOR.U_TaxAmt = ChargeVat
    '            '              oWOR.U_Total = ChargeTot
    '            '
    '            '                oWOR.U_Quantity = 0
    '            '               oWOR.U_JCost = CostTot
    '            '                oWOR.U_OrdTot = CostSum
    '            oWOR.U_TipWgt = 0
    '            oWOR.U_TipCost = 0
    '            oWOR.U_TipTot = 0
    '            oWOR.U_CstWgt = 0
    '            oWOR.U_TCharge = 0
    '            oWOR.U_TCTotal = 0
    '            oWOR.U_CongCh = 0
    '            oWOR.U_Weight = 0
    '            oWOR.U_Discnt = 0
    '            oWOR.U_DisAmt = 0
    '            oWOR.U_TAddChrg = 0
    '            oWOR.U_TAddCost = 0

    '            gNew = True

    '            For iDate As Integer = 0 To PBI.JobDates.Count - 1
    '                If PBI.DoDetail = "Y" Then
    '                    dRowStartDate = PBI.JobDates.Item(iDate)
    '                    dRowEndDate = PBI.JobDates.Item(iDate)
    '                End If

    '                oWOR.Code = Nothing

    '                oWOR.U_RDate = dRowStartDate

    '                If PBI.AutoCompleteStartDate Then
    '                    oWOR.U_ASDate = dRowStartDate
    '                End If

    '                If PBI.AutoCompleteEndDate Then
    '                    oWOR.U_AEDate = dRowEndDate
    '                End If

    '                oWOR.U_BDate = dRowStartDate

    '                oCost = moLookup.doGetJobCostPrice("Waste Order", PBI.JobType, PBI.ContGrp, PBI.ContCode, PBI.CarrCode, 0, oWOR.U_PUOM, PBI.WasteCode, PBI.CardCode, PBI.SiteAddress, oWOR.U_RDate, PBI.Branch, PBI.CusZip)
    '                oCharge = moLookup.doGetJobChargePrices("Waste Order", PBI.JobType, PBI.ContGrp, PBI.ContCode, PBI.CardCode, 0, oWOR.U_UOM, PBI.WasteCode, PBI.SiteAddress, oWOR.U_RDate, PBI.Branch, PBI.CusZip)

    '                'If Not oCost Is Nothing Then
    '                '    CostSum = oCost.mdHaulPrice * PBI.ContainerQty
    '                '    CostVat = CostSum * (oCost.mdHaulageVat / 100)
    '                '    CostTot = CostSum + CostVat
    '                'Else
    '                '    CostSum = 0
    '                '    CostVat = 0
    '                '    CostTot = 0
    '                'End If

    '                'If Not oCharge Is Nothing Then
    '                '    ChargeSum = oCharge.mdHaulPrice * PBI.ContainerQty
    '                '    ChargeVat = ChargeSum * (oCharge.mdHaulageVat / 100)
    '                '    ChargeTot = ChargeSum + ChargeVat
    '                'Else
    '                '    ChargeSum = 0
    '                '    ChargeVat = 0
    '                '    ChargeTot = 0
    '                'End If

    '                If Not oCharge Is Nothing Then
    '                    oWOR.U_TChrgVtRt = oCharge.mdTipVat
    '                    oWOR.U_HChrgVtRt = oCharge.mdHaulageVat
    '                    oWOR.U_TChrgVtGrp = oCharge.msTipVatGroup
    '                    oWOR.U_HChrgVtGrp = oCharge.msHaulageVatGroup

    '                    oWOR.U_CusChr = oCharge.mdHaulPrice
    '                Else
    '                    oWOR.U_TChrgVtRt = 0
    '                    oWOR.U_HChrgVtRt = 0
    '                    oWOR.U_TChrgVtGrp = ""
    '                    oWOR.U_HChrgVtGrp = ""

    '                    oWOR.U_CusChr = 0
    '                End If

    '                If Not oCost Is Nothing Then
    '                    oWOR.U_TCostVtRt = oCost.mdTipVat
    '                    oWOR.U_HCostVtRt = oCost.mdHaulageVat
    '                    oWOR.U_TCostVtGrp = oCost.msTipVatGroup
    '                    oWOR.U_HCostVtGrp = oCost.msHaulageVatGroup

    '                    oWOR.U_OrdCost = oCost.mdHaulPrice
    '                Else
    '                    oWOR.U_TCostVtRt = 0
    '                    oWOR.U_HCostVtRt = 0
    '                    oWOR.U_TCostVtGrp = ""
    '                    oWOR.U_HCostVtGrp = ""

    '                    oWOR.U_OrdCost = 0
    '                End If

    '                '**************************************************************
    '                '* Routing Information
    '                '* Find the WeekDay and Add the Route code for that day if any
    '                '**************************************************************
    '                If Not PBI.Routable Is Nothing AndAlso PBI.Routable = "Y" Then
    '                    oWOR.U_IDHCMT = PBI.CommonComments 'moPBI.U_IDHCMT
    '                    oWOR.U_IDHDRVI = PBI.CommonDriverInstructions 'moPBI.U_IDHDRVI
    '                    oWOR.U_IDHRTDT = dRowStartDate

    '                    Select Case (dRowStartDate.DayOfWeek)
    '                        Case DayOfWeek.Monday
    '                            oWOR.U_IDHRTCD = PBI.RouteCodeMonday  'moPBI.U_IDHRCDM
    '                            oWOR.U_IDHSEQ = PBI.RouteSeqMonday
    '                        Case DayOfWeek.Tuesday
    '                            oWOR.U_IDHRTCD = PBI.RouteCodeTuesday 'moPBI.U_IDHRCDTU
    '                            oWOR.U_IDHSEQ = PBI.RouteSeqTuesday
    '                        Case DayOfWeek.Wednesday
    '                            oWOR.U_IDHRTCD = PBI.RouteCodeWednesday 'moPBI.U_IDHRCDW
    '                            oWOR.U_IDHSEQ = PBI.RouteSeqWednesday
    '                        Case DayOfWeek.Thursday
    '                            oWOR.U_IDHRTCD = PBI.RouteCodeThursday 'moPBI.U_IDHRCDTH
    '                            oWOR.U_IDHSEQ = PBI.RouteSeqThursday
    '                        Case DayOfWeek.Friday
    '                            oWOR.U_IDHRTCD = PBI.RouteCodeFriday 'moPBI.U_IDHRCDF
    '                            oWOR.U_IDHSEQ = PBI.RouteSeqFriday
    '                        Case DayOfWeek.Saturday
    '                            oWOR.U_IDHRTCD = PBI.RouteCodeSaturday 'moPBI.U_IDHRCDSA
    '                            oWOR.U_IDHSEQ = PBI.RouteSeqSaturday
    '                        Case Else
    '                            oWOR.U_IDHRTCD = PBI.RouteCodeSunday 'moPBI.U_IDHRCDSU
    '                            oWOR.U_IDHSEQ = PBI.RouteSeqSunday
    '                    End Select
    '                Else
    '                    oWOR.U_IDHCMT = ""
    '                    oWOR.U_IDHDRVI = ""
    '                    oWOR.U_IDHRTCD = ""
    '                End If

    '                If oWOR.doAddData() = False Then
    '                    Return False
    '                Else
    '                    PBI.WORs.Add(oWOR.Code)

    '                    If Not PBI.DoDetail = "Y" Then
    '                        PBI.WOLineCode = oWOR.Code
    '                        Return True
    '                    End If
    '                End If
    '            Next
    '            If PBI.WORs.Count > 0 Then
    '                PBI.WOLineCode = PBI.WORs.Item(PBI.WORs.Count - 1)
    '            End If
    '            Return True
    '        Else
    '            '*************************
    '            '* This is a Row Update
    '            '*************************
    '            oWOR.U_WasCd = PBI.WasteCode
    '            oWOR.U_WasDsc = PBI.WasteDesc

    '            oWOR.U_CustCd = PBI.CardCode
    '            oWOR.U_CustNm = PBI.CardName
    '            oWOR.U_Tip = PBI.CarrCode
    '            oWOR.U_TipNm = PBI.CarrName

    '            If oWOR.U_UOM Is Nothing OrElse oWOR.U_UOM.Length = 0 Then
    '                oWOR.U_UOM = "t"
    '            End If

    '            If oWOR.U_PUOM Is Nothing OrElse oWOR.U_PUOM.Length = 0 Then
    '                oWOR.U_PUOM = "t"
    '            End If

    '            oCost = moLookup.doGetJobCostPrice("Waste Order", PBI.JobType, PBI.ContGrp, PBI.ContCode, PBI.CarrCode, 0, oWOR.U_PUOM, PBI.WasteCode, PBI.CardCode, PBI.SiteAddress, oWOR.U_RDate, PBI.Branch, PBI.CusZip)
    '            oCharge = moLookup.doGetJobChargePrices("Waste Order", PBI.JobType, PBI.ContGrp, PBI.ContCode, PBI.CardCode, 0, oWOR.U_UOM, PBI.WasteCode, PBI.SiteAddress, oWOR.U_RDate, PBI.Branch, PBI.CusZip)

    '            Dim iTest As Integer
    '            iTest = oWOR.U_MANPRC And com.idh.bridge.lookups.Config.MASK_HAULCHRG
    '            If iTest <> 0 Then
    '                oCharge.mdHaulPrice = oWOR.U_CusChr
    '            End If

    '            iTest = oWOR.U_MANPRC And com.idh.bridge.lookups.Config.MASK_HAULCST
    '            If iTest <> 0 Then
    '                oCost.mdHaulPrice = oWOR.U_OrdCost
    '            End If

    '            'CostSum = oCost.mdHaulPrice * PBI.ContainerQty
    '            'CostVat = CostSum * (oCost.mdHaulageVat / 100)
    '            'CostTot = CostSum + CostVat

    '            'ChargeSum = oCharge.mdHaulPrice * PBI.ContainerQty
    '            'ChargeVat = ChargeSum * (oCharge.mdHaulageVat / 100)
    '            'ChargeTot = ChargeSum + ChargeVat

    '            oWOR.U_JobNr = PBI.WOCode
    '            oWOR.U_JobTp = PBI.JobType
    '            oWOR.U_RDate = PBI.PeriodStartDate

    '            If PBI.AutoCompleteStartDate Then
    '                oWOR.U_ASDate = PBI.PeriodStartDate
    '            End If

    '            If PBI.AutoCompleteEndDate Then
    '                oWOR.U_AEDate = PBI.PeriodEndDate
    '            End If

    '            If Not PBI.Routable Is Nothing AndAlso PBI.Routable = "Y" Then
    '                oWOR.U_IDHCMT = PBI.CommonComments 'moPBI.U_IDHCMT
    '                oWOR.U_IDHDRVI = PBI.CommonDriverInstructions 'moPBI.U_IDHDRVI
    '                oWOR.U_IDHRTDT = PBI.PeriodStartDate

    '                Select Case (PBI.PeriodStartDate.DayOfWeek)
    '                    Case DayOfWeek.Monday
    '                        oWOR.U_IDHRTCD = PBI.RouteCodeMonday  'moPBI.U_IDHRCDM
    '                        oWOR.U_IDHSEQ = PBI.RouteSeqMonday
    '                    Case DayOfWeek.Tuesday
    '                        oWOR.U_IDHRTCD = PBI.RouteCodeTuesday 'moPBI.U_IDHRCDTU
    '                        oWOR.U_IDHSEQ = PBI.RouteSeqTuesday
    '                    Case DayOfWeek.Wednesday
    '                        oWOR.U_IDHRTCD = PBI.RouteCodeWednesday 'moPBI.U_IDHRCDW
    '                        oWOR.U_IDHSEQ = PBI.RouteSeqWednesday
    '                    Case DayOfWeek.Thursday
    '                        oWOR.U_IDHRTCD = PBI.RouteCodeThursday 'moPBI.U_IDHRCDTH
    '                        oWOR.U_IDHSEQ = PBI.RouteSeqThursday
    '                    Case DayOfWeek.Friday
    '                        oWOR.U_IDHRTCD = PBI.RouteCodeFriday 'moPBI.U_IDHRCDF
    '                        oWOR.U_IDHSEQ = PBI.RouteSeqFriday
    '                    Case DayOfWeek.Saturday
    '                        oWOR.U_IDHRTCD = PBI.RouteCodeSaturday 'moPBI.U_IDHRCDSA
    '                        oWOR.U_IDHSEQ = PBI.RouteSeqSaturday
    '                    Case Else
    '                        oWOR.U_IDHRTCD = PBI.RouteCodeSunday 'moPBI.U_IDHRCDSU
    '                        oWOR.U_IDHSEQ = PBI.RouteSeqSunday
    '                End Select
    '            Else
    '                oWOR.U_IDHCMT = ""
    '                oWOR.U_IDHDRVI = ""
    '                oWOR.U_IDHRTCD = ""
    '            End If

    '            oWOR.U_Price = ChargeSum
    '            oWOR.U_TaxAmt = ChargeVat
    '            oWOR.U_Total = ChargeTot
    '            oWOR.U_ItmGrp = PBI.ContGrp
    '            oWOR.U_ItemCd = PBI.ContCode
    '            oWOR.U_ItemDsc = PBI.ContDesc
    '            oWOR.U_Status = "Open"
    '            oWOR.U_Quantity = 0
    '            oWOR.U_JCost = CostTot
    '            oWOR.U_CarrCd = PBI.CarrCode
    '            oWOR.U_CarrNm = PBI.CarrName
    '            oWOR.U_OrdWgt = PBI.ContainerQty
    '            oWOR.U_OrdCost = oCost.mdHaulPrice
    '            oWOR.U_OrdTot = CostSum
    '            oWOR.U_CusQty = PBI.ContainerQty
    '            oWOR.U_HlSQty = PBI.ContainerQty
    '            oWOR.U_CusChr = oCharge.mdHaulPrice
    '            oWOR.U_IntComm = PBI.Comment
    '            oWOR.U_User = PBI.Owner
    '            oWOR.U_Branch = PBI.Branch
    '            oWOR.U_TChrgVtRt = oCharge.mdTipVat
    '            oWOR.U_HChrgVtRt = oCharge.mdHaulageVat
    '            oWOR.U_TCostVtRt = oCost.mdTipVat
    '            oWOR.U_HCostVtRt = oCost.mdHaulageVat
    '            oWOR.U_TChrgVtGrp = oCharge.msTipVatGroup
    '            oWOR.U_HChrgVtGrp = oCharge.msHaulageVatGroup
    '            oWOR.U_TCostVtGrp = oCost.msTipVatGroup
    '            oWOR.U_HCostVtGrp = oCost.msHaulageVatGroup
    '            oWOR.U_VtCostAmt = CostVat
    '            oWOR.U_BDate = PBI.PeriodStartDate
    '            oWOR.U_Obligated = PBI.Obligated
    '            oWOR.U_Covera = PBI.RecuringStr

    '            PBI.WOLineCode = oWOR.Code
    '            gNew = False

    '            Dim bResult = True
    '            Try
    '                Dim sSINV As String = oWOR.U_SAINV
    '                Dim sSORD As String = oWOR.U_SAORD
    '                Dim sTipPO As String = oWOR.U_TIPPO
    '                Dim sJobPO As String = oWOR.U_JOBPO

    '                If Not sSORD Is Nothing AndAlso sSORD.Trim().Length > 0 AndAlso sSORD.Equals("ERROR") = False Then
    '                    bResult = (bResult And UpdateOrderDocument(sSORD, oWO.Code, oWOR.Code, "S"))
    '                End If

    '                If bResult = True AndAlso Not sSINV Is Nothing AndAlso sSINV.Trim().Length > 0 AndAlso sSINV.Equals("ERROR") = False Then
    '                    bResult = (bResult And UpdateInvoiceDocument(sSINV, oWO.Code, oWOR.Code))
    '                End If

    '                If bResult = True AndAlso Not sTipPO Is Nothing AndAlso sTipPO.Trim().Length > 0 AndAlso sTipPO.Equals("ERROR") = False Then
    '                End If

    '                If bResult = True AndAlso Not sJobPO Is Nothing AndAlso sJobPO.Trim().Length > 0 AndAlso sJobPO.Equals("ERROR") = False Then
    '                    bResult = (bResult And UpdateOrderDocument(sJobPO, oWO.Code, oWOR.Code, "P"))
    '                End If

    '                PBI.WORs.Add(oWOR.Code)
    '                If bResult = True Then
    '                    Return oWOR.doUpdateData()
    '                End If
    '            Catch ee As Exception
    '                bResult = False
    '            End Try

    '            Return bResult
    '        End If

    '    Catch ex As Exception
    '        moData.doError("Exception: " + ex.Message, "Error Adding or Updating the Waste Order.")
    '        Return False
    '    Finally
    '        moData.doReleaseObject(oWOR)
    '    End Try
    '    Return True
    'End Function

    'Private Function doAddDetails(ByVal mNew As Boolean) As Boolean
    '    If Not PBI.JobDates Is Nothing AndAlso PBI.JobDates.Count > 0 Then
    '        If Not PBI.WORs Is Nothing AndAlso PBI.WORs.Count > 0 Then
    '            Dim oPBIDetail As IDH_PBIDTL = New IDH_PBIDTL()
    '            Try
    '                If mNew Then
    '                    If PBI.JobDates.Count > 0 Then
    '                        Dim sRowCode As String = PBI.WORs.Item(0)
    '                        Dim dWrkDate As DateTime

    '                        oPBIDetail.U_IDHPRDSD = PBI.PeriodStartDate
    '                        oPBIDetail.U_IDHPRDED = PBI.PeriodEndDate
    '                        oPBIDetail.U_IDHPBI = PBI.Code
    '                        oPBIDetail.U_IDHJTYPE = PBI.JobType
    '                        oPBIDetail.U_IDHCOICD = PBI.ContCode
    '                        oPBIDetail.U_IDHWCICD = PBI.WasteCode
    '                        oPBIDetail.U_IDHCOIQT = PBI.Qty
    '                        oPBIDetail.U_IDHDONE = "Y"
    '                        oPBIDetail.U_IDHRECCT = PBI.RunCount
    '                        oPBIDetail.U_IDHDTYP = "G"

    '                        For iDate As Integer = 0 To PBI.JobDates.Count - 1
    '                            If PBI.DoDetail = "Y" Then
    '                                sRowCode = PBI.WORs.Item(iDate)
    '                                If PBI.DoDetailInv = "Y" Then
    '                                	oPBIDetail.U_IDHDTYP = "B"
    '                                Else
    '                                	oPBIDetail.U_IDHDTYP = "D"
    '                            	End If
    '                            ElseIf PBI.DoDetailInv = "Y" Then
    '                                oPBIDetail.U_IDHDTYP = "I"
    '                            End If
    '                            dWrkDate = PBI.JobDates.Item(iDate)

    '                            oPBIDetail.Code = Nothing
    '                            oPBIDetail.U_IDHWOR = sRowCode

    '                            Select Case (dWrkDate.DayOfWeek)
    '                                Case DayOfWeek.Monday
    '                                    oPBIDetail.U_IDHJDAY = "Mon"
    '                                Case DayOfWeek.Tuesday
    '                                    oPBIDetail.U_IDHJDAY = "Tue"
    '                                Case DayOfWeek.Wednesday
    '                                    oPBIDetail.U_IDHJDAY = "Wed"
    '                                Case DayOfWeek.Thursday
    '                                    oPBIDetail.U_IDHJDAY = "Thu"
    '                                Case DayOfWeek.Friday
    '                                    oPBIDetail.U_IDHJDAY = "Fri"
    '                                Case DayOfWeek.Saturday
    '                                    oPBIDetail.U_IDHJDAY = "Sat"
    '                                Case Else
    '                                    oPBIDetail.U_IDHJDAY = "Sun"
    '                            End Select
    '                            'oPBID.U_IDHJDAY = dWrkDate.DayOfWeek

    '                            oPBIDetail.U_IDHJDATE = dWrkDate

    '                            If oPBIDetail.doAddData() = False Then
    '                                Return False
    '                            End If
    '                        Next
    '                    End If
    '                End If
    '            Catch ex As Exception
    '                moData.doError("Exception: " + ex.Message, "Error Adding the PBI Details.")
    '                Return False
    '            Finally
    '                moData.doReleaseObject(oPBIDetail)
    '            End Try
    '        End If
    '    End If
    '    Return True
    'End Function

    Public Function UpdateInvoiceDocument(ByVal sInvNum As String, ByVal sWOCode As String, ByVal sRowCode As String) As Boolean
        Dim oINV As SAPbobsCOM.Documents = Nothing

        Dim oLines As SAPbobsCOM.Document_Lines = Nothing
        Dim oRc As SAPbobsCOM.Recordset = Nothing
        Dim sqlstr As String
        Dim DocEntry As Integer
        Try
            oINV = CType(moSBO_Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices), SAPbobsCOM.Documents)

            If oINV.GetByKey(Conversions.ToInt(sInvNum)) Then
                If oINV.DocumentStatus = SAPbobsCOM.BoStatus.bost_Open Then
                    oLines = oINV.Lines

                    ''Find the line with the PBI item
                    Dim iCount As Integer = oLines.Count
                    Dim bLineFound As Boolean = False
                    If iCount > 1 Then
                        'Find the Haulage Line
                        Dim sLineCode As String = PBI.ContCode
                        Dim sHaulItem As String = Config.Parameter("HAULCD")
                        Dim sItemCode As String
                        For i As Integer = 0 To iCount - 1
                            oLines.SetCurrentLine(i)
                            sItemCode = oLines.ItemCode
                            If sItemCode.Equals(sLineCode) OrElse sItemCode.Equals(sHaulItem) Then
                                bLineFound = True
                                Exit For
                            End If
                        Next
                    Else
                        oLines.SetCurrentLine(0) ''oLines.Count)
                        bLineFound = True
                    End If

                    If bLineFound = True Then
                        oINV.Address = PBI.SiteAddress
                        oINV.ShipToCode = PBI.SiteAddress
                        oINV.CardCode = PBI.CardCode
                        oINV.CardName = PBI.CardName

                        oINV.Comments = "JobRef: WO" & sWOCode & "." & sRowCode & Chr(10) & PBI.Comment

                        oLines.Quantity = PBI.ContainerQty

                        With oLines.UserFields.Fields
                            .Item("U_IDH_WOR").Value = "WO" & sWOCode & "." & sRowCode
                            .Item("U_IDHWCICD").Value = PBI.WasteCode
                            .Item("U_IDHWCIDC").Value = PBI.WasteDesc

                            .Item("U_IDHCOICD").Value = PBI.ContCode
                            .Item("U_IDHCOIDC").Value = PBI.ContDesc

                            .Item("U_IDHPCNM").Value = PBI.CardName
                            .Item("U_IDHPAdd").Value = PBI.SiteAddress
                            .Item("U_IDHPStr").Value = PBI.CusStreet
                            .Item("U_IDHPBlk").Value = PBI.CusBlock
                            .Item("U_IDHPCty").Value = PBI.CusCity
                            .Item("U_IDHPZCd").Value = PBI.CusZip
                        End With
                        If oINV.Update <> 0 Then
                            'moData.doError("System: Invoice Update - " & moSBO_Company.GetLastErrorCode & " - " & moSBO_Company.GetLastErrorDescription, "")
                            com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Invoice Update - " + moSBO_Company.GetLastErrorCode.ToString() + " - " + moSBO_Company.GetLastErrorDescription, "ERSYUINV", {moSBO_Company.GetLastErrorCode.ToString(), com.idh.bridge.Translation.getTranslatedWord(moSBO_Company.GetLastErrorDescription)})
                            Return False
                        Else
                            Return True
                        End If
                    Else
                        'moData.doError("User: The Job line could not be found on the Invoice.", "The Job line could not be found on the Invoice.")
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("The Job line could not be found on the Invoice.", "ERUSJBNF", {Nothing})
                        Return False
                    End If
                Else
                    If oINV.Cancel <> 0 Then
                        'moData.doError("User: Cancel Invoice - " & moSBO_Company.GetLastErrorCode & " - " & moSBO_Company.GetLastErrorDescription, "The invoice[" & DocEntry & "] could not be Canceled.")
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError(" Cancel Invoice - " & moSBO_Company.GetLastErrorCode & " - " & moSBO_Company.GetLastErrorDescription, "ERUSCINV", {DocEntry.ToString()})
                        Return False
                    End If
                End If

                sqlstr = "UPDATE [@IDH_JOBSHD] SET U_SAINV = '', U_Status = 'ReqInvoice' WHERE Code = '" & sRowCode & "'"
                moData.doUpdateQuery("UpdateOrderDocument", sqlstr)
                Return True
            Else
                sqlstr = "UPDATE [@IDH_JOBSHD] SET U_SAINV = '', U_Status = 'ReqInvoice' WHERE Code = '" & sRowCode & "'"
                moData.doUpdateQuery("UpdateOrderDocument", sqlstr)
                Return True
            End If
        Catch ex As Exception
            'moData.doError("Exception: " + ex.Message, "Error Updating the Invoice.")
            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXUINV", {Nothing})
        Finally
            com.idh.bridge.DataHandler.INSTANCE.doReleaseRecordset(oRc)
            com.idh.bridge.DataHandler.INSTANCE.doReleaseObject(CType(oINV, Object))
        End Try
        Return False
    End Function

    Public Function UpdateOrderDocument(ByVal sORDNum As String, ByVal sWOCode As String, ByVal sRowCode As String, ByVal sOrdType As String) As Boolean
        Dim oSO As SAPbobsCOM.Documents = Nothing

        Dim oLines As SAPbobsCOM.Document_Lines = Nothing
        Dim oRc As SAPbobsCOM.Recordset = Nothing
        Dim sqlstr As String = Nothing
        Dim DocEntry As Integer
        Dim bCancleFailed As Boolean = False
        Try
            If sOrdType = "S" Then
                oSO = CType(moSBO_Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders), SAPbobsCOM.Documents)
            ElseIf sOrdType = "P" Then
                oSO = CType(moSBO_Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders), SAPbobsCOM.Documents)
            End If

            If oSO.GetByKey(Conversions.ToInt(sORDNum)) Then
                If oSO.DocumentStatus = SAPbobsCOM.BoStatus.bost_Open Then
                    oLines = oSO.Lines

                    ''Fing the line with the PBI item
                    Dim iCount As Integer = oLines.Count
                    Dim bLineFound As Boolean = False
                    If iCount > 1 Then
                        'Find the Haulage Line
                        Dim sLineCode As String = PBI.ContCode
                        Dim sHaulItem As String = Config.Parameter("HAULCD")
                        Dim sItemCode As String
                        For i As Integer = 0 To iCount - 1
                            oLines.SetCurrentLine(i)
                            sItemCode = oLines.ItemCode
                            If sItemCode.Equals(sLineCode) OrElse sItemCode.Equals(sHaulItem) Then
                                bLineFound = True
                                Exit For
                            End If
                        Next
                    Else
                        oLines.SetCurrentLine(0) ''oLines.Count)
                        bLineFound = True
                    End If

                    If bLineFound = True Then
                        If sOrdType = "S" Then
                            oSO.Address = PBI.SiteAddress
                            oSO.ShipToCode = PBI.SiteAddress
                            oSO.CardCode = PBI.CardCode
                            oSO.CardName = PBI.CardName
                        ElseIf sOrdType = "P" Then
                            oSO.Address = PBI.CarAddress
                            oSO.CardCode = PBI.CarrCode
                            oSO.CardName = PBI.CarrName
                        End If

                        oSO.Comments = "JobRef: WO" & sWOCode & "." & sRowCode & Chr(10) & PBI.Comment

                        oLines.Quantity = PBI.ContainerQty

                        With oLines.UserFields.Fields
                            .Item("U_IDH_WOR").Value = "WO" & sWOCode & "." & sRowCode
                            .Item("U_IDHWCICD").Value = PBI.WasteCode
                            .Item("U_IDHWCIDC").Value = PBI.WasteDesc

                            .Item("U_IDHCOICD").Value = PBI.ContCode
                            .Item("U_IDHCOIDC").Value = PBI.ContDesc

                            .Item("U_IDHPCNM").Value = PBI.CardName
                            .Item("U_IDHPAdd").Value = PBI.SiteAddress
                            .Item("U_IDHPStr").Value = PBI.CusStreet
                            .Item("U_IDHPBlk").Value = PBI.CusBlock
                            .Item("U_IDHPCty").Value = PBI.CusCity
                            .Item("U_IDHPZCd").Value = PBI.CusZip
                        End With
                        If oSO.Update <> 0 Then
                            'moData.doError("System: [PBI - " & PBI.Code & "][" & sOrdType & "] Order Update: " & moSBO_Company.GetLastErrorCode & " - " & moSBO_Company.GetLastErrorDescription, "")
                            com.idh.bridge.DataHandler.INSTANCE.doResSystemError("[PBI - " & PBI.Code & "][" & sOrdType & "] Order Update: " & moSBO_Company.GetLastErrorCode & " - " & moSBO_Company.GetLastErrorDescription, "ERSYUPBI", _
                                                                                 {PBI.Code, com.idh.bridge.Translation.getTranslatedWord(sOrdType), moSBO_Company.GetLastErrorCode.ToString(), com.idh.bridge.Translation.getTranslatedWord(moSBO_Company.GetLastErrorDescription)})
                            Return False
                        Else
                            Return True
                        End If
                    Else
                        'moData.doError("System: [PBI - " & PBI.Code & "] The Job line could not be found on the Order.")
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("[PBI - " & PBI.Code & "] The Job line could not be found on the Order.", "ERSYPBIN", {PBI.Code})
                        Return False
                    End If

                    '           			If oSO.Cancel <> 0 Then
                    '                		oErr.DoError(oComp, oComp.GetLastErrorCode & " - " & oComp.GetLastErrorDescription, "", "IDH_PBI", "UpdateDocuments")
                    '                		Return False
                    '       				Else
                    '	                	If oRc Is Nothing Then
                    '	                		oRc = oComp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    '	                	End If
                    '	                	If sOrdType = "S" Then
                    '	                		sqlstr = "UPDATE [@IDH_JOBSHD] SET U_SAORD = '', U_Status = 'ReqOrder' WHERE Code = '" & sRowCode & "'"
                    '	                	ElseIf sOrdType = "P" Then
                    '	                		sqlstr = "UPDATE [@IDH_JOBSHD] SET U_JOBPO = '', U_PStat = 'ReqOrder' WHERE Code = '" & sRowCode & "'"
                    '	                	End If
                    '	                	oRc.DoQuery(sqlstr)
                    '	                	Return True
                    '            		End If
                Else
                    If oSO.DocumentStatus = SAPbobsCOM.BoStatus.bost_Close Then
                        If sOrdType = "S" Then
                            Dim oInv As SAPbobsCOM.Documents = Nothing
                            Try
                                sqlstr = "Select DocEntry from INV1 where BaseEntry = '" & sORDNum & "' and BaseType = '17'"
                                oRc = CType(moData.doSelectQuery("UpdateOrderDocument", sqlstr), SAPbobsCOM.Recordset)
                                If Not oRc Is Nothing AndAlso oRc.RecordCount > 0 Then
                                    oRc.MoveFirst()
                                    DocEntry = Conversions.ToInt(oRc.Fields.Item(0).Value)

                                    oInv = CType(moSBO_Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices), SAPbobsCOM.Documents)
                                    If oInv.GetByKey(DocEntry) Then
                                        If oInv.Cancel <> 0 Then
                                            bCancleFailed = True
                                            'moData.doError("System: [PBI - " & PBI.Code & "] You are updating a ROW where the Sales Order has been Closed. The Invoice[" & DocEntry & "] must be updated manually for a further invoice Or a credit note.")
                                            'moData.doError("Cancel Invoice: " & moSBO_Company.GetLastErrorCode & " - " & moSBO_Company.GetLastErrorDescription, "The invoice[" & DocEntry & "] related to the Order[" & sORDNum & "] could not be Canceled." )
                                            com.idh.bridge.DataHandler.INSTANCE.doResSystemError("[PBI - " & PBI.Code & "] You are updating a ROW where the Sales Order has been Closed. The Invoice[" & DocEntry & "] must be updated manually for a further invoice Or a credit note.", "ERSYPBIC", {PBI.Code, DocEntry.ToString()})
                                            'Return False
                                        End If
                                    End If
                                End If
                            Catch ex As Exception
                                'moData.doError("Exception: " + ex.Message, "Error Updating the Order.")
                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXUORD", {Nothing})
                                Return False
                            Finally
                                'moData.doReleaseObject(oInv)
                                ''moData.doReleaseSqlDataReader(oRc)
                                'moData.doReleaseRecordset(oRc)
                                com.idh.bridge.DataHandler.INSTANCE.doReleaseRecordset(oRc)
                                com.idh.bridge.DataHandler.INSTANCE.doReleaseObject(CType(oInv, Object))
                            End Try

                            If bCancleFailed = False AndAlso oSO.Cancel <> 0 Then
                                'moData.doError("System: " + moSBO_Company.GetLastErrorCode & " - " & moSBO_Company.GetLastErrorDescription, "Error Updating the Order.")
                                com.idh.bridge.DataHandler.INSTANCE.doResSystemError(moSBO_Company.GetLastErrorCode & " - " & moSBO_Company.GetLastErrorDescription + ",Error Updating the Order.", "ERSYUORD", _
                                                                                     {moSBO_Company.GetLastErrorCode.ToString(), com.idh.bridge.Translation.getTranslatedWord(moSBO_Company.GetLastErrorDescription)})
                                Return False
                            End If
                        End If
                    End If

                    If bCancleFailed = True Then
                        sqlstr = "UPDATE [@IDH_JOBSHD] SET U_MDChngd = 'Inv-" & DocEntry & "' WHERE Code = '" & sRowCode & "'"
                    ElseIf sOrdType = "S" Then
                        sqlstr = "UPDATE [@IDH_JOBSHD] SET U_SAORD = '', U_Status = 'ReqOrder' WHERE Code = '" & sRowCode & "'"
                    ElseIf sOrdType = "P" Then
                        sqlstr = "UPDATE [@IDH_JOBSHD] SET U_JOBPO = '', U_PStat = 'ReqOrder' WHERE Code = '" & sRowCode & "'"
                    End If
                    moData.doUpdateQuery("UpdateOrderDocument", sqlstr)
                    Return True
                End If
            Else
                If sOrdType = "S" Then
                    sqlstr = "UPDATE [@IDH_JOBSHD] SET U_SAORD = '', U_Status = 'ReqOrder' WHERE Code = '" & sRowCode & "'"
                ElseIf sOrdType = "P" Then
                    sqlstr = "UPDATE [@IDH_JOBSHD] SET U_JOBPO = '', U_PStat = 'ReqOrder' WHERE Code = '" & sRowCode & "'"
                End If
                moData.doUpdateQuery("UpdateOrderDocument", sqlstr)

                Return True
            End If
        Catch ex As Exception
            'moData.doError("Exception: " + ex.Message, "Error Updating the Order.")
            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXUORD", {Nothing})
        Finally
            'moData.doReleaseRecordset(oRc)
            'moData.doReleaseObject(oSO)
            com.idh.bridge.DataHandler.INSTANCE.doReleaseRecordset(oRc)
            com.idh.bridge.DataHandler.INSTANCE.doReleaseObject(CType(oSO, Object))
        End Try
        Return False
    End Function

    Private Function UpdatePBI() As Boolean
        Try
            'Only Update when a New Waste Order Line has been added.
            If PBI.Code.Length > 0 Then
                'Dim sqlstr As String
                If gNew = True Then
                    moPBI.U_IDHLASTR = mdThisRunDate
                    moPBI.U_IDHNEXTR = PBI.NextRun
                    moPBI.U_IDHRECCT = PBI.RunCount
                    moPBI.U_IDHWO = PBI.WOCode
                    moPBI.U_IDHWOR = PBI.WOLineCode
                    moPBI.U_IDHRECCE = PBI.EndCount
                Else
                    moPBI.U_IDHLASTR = mdThisRunDate
                    moPBI.U_IDHNEXTR = PBI.NextRun
                End If
                If Not moPBI.U_IDHIMPI Is Nothing AndAlso moPBI.U_IDHIMPI.Equals("IB") Then
                    moPBI.U_IDHIMPI = "ID"
                End If
                Return moPBI.doUpdateDataRow()
            Else
                Return False
            End If
        Catch ex As Exception
            'moData.doError("Exception: [PBI - " & PBI.Code & "] " + ex.Message, "Error Updating the PBI.")
            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXUPBI", {PBI.Code})
            Return False
        End Try
    End Function
#End Region
    '#Region "PBI Executer for Mass Updater"
    '    Public Shared Function doExecutePBIHandler(ByRef oPBI As IDH_PBI, oBatchDetail As IDH_LVSBTH, SBOCompany As SAPbobsCOM.Company, bFromService As Boolean,
    '                                               bDoNotRemove As Boolean, dActDate As Date) As Boolean
    '        Try
    '            Dim DoAddNewPBI As Boolean = False
    '            Dim oActionObject As New IDH_LVSAMD()
    '            oActionObject.UnLockTable = True
    '            Dim sAction As String = oBatchDetail.U_AmndTp
    '            Dim iRet As Integer = oActionObject.getData((Convert.ToString(IDH_LVSAMD._AmdDesc + "='") & sAction) + "' And " + IDH_LVSAMD._Active + "='Y'", "")
    '            DoAddNewPBI = oActionObject.doCheckAmendListChange(IDH_LVSAMD.AddPBI)

    '            Dim oWOM As New IDH_PBI_Handler(SBOCompany, bFromService)
    '            Dim bDoRestOfUpdates As Boolean = True
    '            Dim bResult As Boolean = True
    '            Dim bDoToEnd As Boolean = False
    '            Dim sValToEnd As String = ""
    '            ' a check box labeled To End on PBI; right to Action date getUFValue(oForm, "IDHTOE");
    '            If sValToEnd = "Y" Then
    '                bDoToEnd = True
    '            End If

    '            If Not bDoNotRemove AndAlso Not DoAddNewPBI Then

    '                Dim doRemovePBI As Boolean = False

    '                If oActionObject.doCheckAmendListChange(IDH_LVSAMD.RemovePBI) Then
    '                    doRemovePBI = True
    '                End If
    '                If doRemovePBI AndAlso bDoRestOfUpdates AndAlso oPBI.U_IDHRECEN = 2 AndAlso (oPBI.U_IDHRECED <> Date.MinValue) Then
    '                    If (oPBI.U_IDHRECED <> Date.MinValue) AndAlso oPBI.U_IDHRECED_AsString().Length >= 6 Then
    '                        bResult = oWOM.doClearAfterTerminationDate(oPBI.Code, oPBI.U_IDHRECED)
    '                    End If
    '                    bDoRestOfUpdates = False
    '                End If
    '            End If

    '            If bDoRestOfUpdates Then
    '                If bDoToEnd OrElse (oPBI.U_IDHNEXTR > dActDate) Then
    '                    Dim dSetNextRun As DateTime = oPBI.U_IDHNEXTR
    '                    If ((oPBI.U_IDHRECEN = 2 AndAlso oPBI.U_IDHRECED <> Date.MinValue AndAlso oPBI.U_IDHNEXTR > oPBI.U_IDHRECED)) Then
    '                        dSetNextRun = oPBI.U_IDHRECED 'oPBI.U_IDHNEXTR
    '                    End If
    '                    bResult = oWOM.ProcessOrderRunToEnd(oPBI.Code, dActDate, dSetNextRun)
    '                Else
    '                    bResult = oWOM.ProcessOrderRun(oPBI.Code, dActDate)
    '                    If bResult AndAlso Config.ParameterAsBool("PBIRTDAY", False) Then
    '                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
    '                        oPBI.getByKey(oPBI.Code)
    '                        While bResult AndAlso DateTime.Compare(oPBI.U_IDHNEXTR, New DateTime(DateTime.Today.AddMonths(1).Year, DateTime.Today.AddMonths(1).Month, 1)) < 0
    '                            dActDate = dActDate.AddMonths(oPBI.U_IDHRECFQ)
    '                            bResult = oWOM.ProcessOrderRun(oPBI.Code, dActDate)
    '                            oPBI.getByKey(oPBI.Code)
    '                        End While
    '                    End If
    '                End If
    '                If bResult = True Then
    '                Else
    '                    Return False
    '                End If
    '            Else
    '                If bResult = True Then
    '                    Return True
    '                Else
    '                    Return False
    '                End If
    '            End If
    '            '######################################################################################################################
    '        Catch ex As Exception
    '            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", New String() {"IDH_PBI_Handler.doExecutePBIHandler"})
    '            Return False
    '        Finally
    '        End Try
    '        Return True
    '    End Function
    '    Public Shared Function doExecutePBIHandler(ByRef oPBI As IDH_PBI, oBatchDetail As IDH_LVSBTH, SBOCompany As SAPbobsCOM.Company, bFromService As Boolean, bDoNotRemove As Boolean) As Boolean
    '        Try
    '            Dim dActDate As DateTime = oBatchDetail.U_EffDate
    '            If Not (oBatchDetail.U_EffDate.Year = 1 OrElse oBatchDetail.U_EffDate.Year = 1900) Then
    '                Dim sPBIStart As String = oPBI.U_IDHRECSD_AsString()
    '                Dim dPBIStart As System.DateTime = Conversions.ToDateTime(sPBIStart)
    '                dActDate = oBatchDetail.U_EffDate
    '                If System.DateTime.Compare(dActDate, dPBIStart) < 0 Then
    '                    dActDate = dPBIStart
    '                End If
    '            End If
    '            Return doExecutePBIHandler(oPBI, oBatchDetail, SBOCompany, bFromService, bDoNotRemove, dActDate)

    '            'Return bResult
    '        Catch ex As Exception
    '            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", New String() {"IDH_PBI_Handler.doExecutePBIHandler"})
    '            Return False
    '        Finally
    '        End Try
    '        Return True
    '    End Function

    '#End Region
End Class
