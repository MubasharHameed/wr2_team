'
' Created by SharpDevelop.
' User: Louis Viljoen
' Date: 2009/06/10
' Time: 05:13 PM
' 
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Public Class AuditSplit
	Private moEntries As New ArrayList
	
	Public Sub New(ByRef oComp As SAPbobsCOM.Company, ByVal sTable As String, ByVal sCode As String)
		Dim oRc As SAPbobsCOM.Recordset = Nothing
		Try
            oRc = CType(oComp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
			Dim sQry As String = "SELECT U_ACTION, U_ADATE, U_ATIME, U_FIELDS from [@IDH_AUDITRL] where U_UTABLE like '" & sTable & "' AND U_Code = '" & sCode & "' ORDER BY CAST(Code As Numeric) DESC"
        	oRc.DoQuery(sQry)
        	oRc.MoveFirst()
        	While Not oRc.EoF
                Dim sAction As String = CType(oRc.Fields.Item("U_ACTION").Value, String)
                Dim dDAte As Date = CType(oRc.Fields.Item("U_ADATE").Value, Date)
                Dim iTime As Integer = CType(oRc.Fields.Item("U_ATIME").Value, Integer)
                Dim sFields As String = CType(oRc.Fields.Item("U_FIELDS").Value, String)
        		
        		Dim oEntry As New AuditEntry(sAction, dDate, iTime, sFields)
        		
        		moEntries.Add(oEntry)
        		
        		oRc.MoveNext()
        	End While
        Catch ex As Exception
        Finally
            Try
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oRc)
            Catch ex As Exception
            End Try
            oRc = Nothing
            GC.Collect()
        End Try 
	End Sub
	
    Public Function getCurrentValue(ByVal sField As String) As String
        Dim oEntry As AuditEntry
        Dim sValue As String
        For i As Integer = 0 To moEntries.Count - 1
            oEntry = CType(moEntries.Item(i), AuditEntry)
            sValue = oEntry.getValue(sField)
            If Not sValue Is Nothing Then
                Return sValue
            End If
        Next
        Return Nothing
    End Function
	
    Public Function getLastUpdatesValue(ByVal sField As String) As String
        If moEntries.Count = 0 Then
            Return Nothing
        End If
        Dim oEntry As AuditEntry = CType(moEntries.Item(0), AuditEntry)
        Return CType(oEntry.getValue(sField), String)
    End Function
	
    Public Function getLastUpdatesPreviousValue(ByVal sField As String) As String
        If moEntries.Count = 0 Then
            Return Nothing
        End If
        Dim oEntry As AuditEntry = CType(moEntries.Item(0), AuditEntry)
        Return oEntry.getPreviousValue(sField)
    End Function
	
	Public Function getPreviousValue( ByVal sField As String ) As String
		Dim sValue As String = getLastUpdatesPreviousValue( sField )
		If sValue Is Nothing Then
			sValue = getCurrentValue( sField )
		End If
		Return sValue
	End Function
	
End Class
