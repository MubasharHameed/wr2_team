'
' Created by SharpDevelop.
' User: Louis Viljoen
' Date: 2009/06/10
' Time: 05:24 PM
'
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Public Class AuditEntry
	Public msAction As String
	Public mdDate As Date
	Public miTime As Integer
	Public mhFields As New Hashtable

	Public Sub New(ByVal sAction As String, ByVal dDate As Date, ByVal iTime As Integer, ByVal sFields As String)
		msAction = sAction
		mdDate = dDate
		miTime = iTime
		doSplit(sFields)
	End Sub
	
	'** ADD     - Name=9001346;
    '** UPDATDE - U_TCharge=55->10;
    Private Sub doSplit(ByVal sValue As String)
        Dim sFields() As String = sValue.Split(";"c)
    	Dim sFieldName As String
    	
    	Dim iIndex1 As Integer
        
    	For i As Integer = 0 To sFields.Length() - 1
            iIndex1 = sFields(i).IndexOf("="c)
    		If iIndex1 > -1 Then
	            Dim iIndex2 As Integer
	            Dim sFieldValues(2) As String
	            
	            iIndex2 = sFields(i).IndexOf("->")
	
	            sFieldName = sFields(i).Substring(0, iIndex1)
	            If iIndex2 > -1 Then
	                sFieldValues(0) = sFields(i).Substring(iIndex1 + 1, iIndex2 - iIndex1 - 1)
	                sFieldValues(1) = sFields(i).Substring(iIndex2 + 2)
	            Else
	                sFieldValues(0) = ""
	                sFieldValues(1) = sFields(i).Substring(iIndex1 + 1)
	            End If
	            
	            If mhFields.ContainsKey(sFieldName) = False Then
	            	mhFields.Add(sFieldName, sFieldValues)
	            End If
	        End If
    	Next
    End Sub
    
    Public Function getValue(ByVal sField As String) As String
        If mhFields.ContainsKey(sField) Then
            Dim saVal() As String = CType(mhFields.Item(sField), String())
            Return saVal(1)
            'Return mhFields.Item(sField)(1)
        Else
            Return Nothing
        End If
    End Function
    
    Public Function getPreviousValue(ByVal sField As String) As String
        If mhFields.ContainsKey(sField) Then
            Dim saVal() As String = CType(mhFields.Item(sField), String())
            Return saVal(0)
            'Return mhFields.Item(sField)(0)
        Else
            Return Nothing
        End If
    End Function
	
End Class
