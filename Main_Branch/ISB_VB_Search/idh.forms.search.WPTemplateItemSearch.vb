Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class WPTemplateItemSearch
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHWPTISRC", "WP Template Item Search.srf", 5, 40, 603, 350, "WP Template Item Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("OITM i, OITB ig")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OITM", "i", Nothing, False, True))
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OITB", "ig", Nothing, False, True))

            'oGridN.setRequiredFilter("i.ItmsGrpCod = ig.ItmsGrpCod AND i.U_WPTemplate = 'Y' ")
            oGridN.setRequiredFilter("i.ItmsGrpCod = ig.ItmsGrpCod AND i.U_WPTemplate = 'Y' AND i.FrozenFor <> 'Y'")
            oGridN.setOrderValue("i.ItmsGrpCod, i.ItemCode ")

            oGridN.doAddFilterField("IDH_ITMCOD", "i.ItemCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDH_ITMNAM", "i.ItemName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDH_GRPCOD", "i.ItmsGrpCod", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "IN", 30)
            oGridN.doAddFilterField("IDH_GRPNAM", "ig.ItmsGrpNam", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)

            oGridN.doAddListField("i.ItmsGrpCod", "Item Group Code", False, 0, Nothing, "ITMSGRPCOD")
            oGridN.doAddListField("ig.ItmsGrpNam", "Item Group Name", False, 0, Nothing, "ITMSGRPNAM")
            oGridN.doAddListField("i.ItemCode", "Item Code", False, 100, Nothing, "ITEMCODE", -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("i.ItemName", "Item Name", False, 410, Nothing, "ITEMNAME")
            oGridN.doAddListField("i.SalUnitMsr", "Sales UOM", False, 20, Nothing, "SUOM")
            oGridN.doAddListField("i.BuyUnitMsr", "Purchase UOM", False, 20, Nothing, "PUOM")
        End Sub
        ''## Start 05-07-2013
        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doFinalizeShow(oForm)
            If oForm.Visible = False Then
            Else
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                If oGridN Is Nothing Then
                    oGridN = New FilterGrid(Me, oForm, "LINESGRID", True)
                End If
                oGridN.doReloadData()
            End If

        End Sub
        ''## End
    End Class
End Namespace
