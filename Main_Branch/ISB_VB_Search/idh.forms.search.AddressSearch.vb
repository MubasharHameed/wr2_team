Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.bridge.lookups
Imports com.idh.dbObjects.User

Namespace idh.forms.search
    Public Class AddressSearch
        Inherits IDHAddOns.idh.forms.Search
        Private bClosingFormByOKButton As Boolean = False

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHASRCH", "Address Search.srf", 5, 45, 790, 350, "Address Search")
        End Sub
        Protected Overrides Sub doLoadData(oForm As SAPbouiCOM.Form)
            bClosingFormByOKButton = False
            Me.setParentSharedData(oForm, "IDH_ADRSRCHOPEND", "LOADED") 'setSharedData("IDH_BPSRCHOPEND", "LOADED")

            MyBase.doLoadData(oForm)
        End Sub

        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("CRD1", "ba", Nothing, False, True), True)
            '## Start12-06-2013
            'Add OCRD and update all the fields of query with respect to their aliase
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OCRD", "bp", Nothing, False, True), True)
            Dim sRequired As String
            sRequired = " ba.CardCode = bp.CardCode "
			''##MA Start 18-09-2014 Issue#403
            If getParentSharedData(oGridN.getSBOForm, "IDH_APPLYFLTR") IsNot Nothing AndAlso getParentSharedData(oGridN.getSBOForm, "IDH_APPLYFLTR").ToString = "True" Then
                'sRequired &= " And Not ba.Address in (Select adrs.U_Address from [@IDH_ADDRST] adrs Where adrs.U_CustCode=ba.CardCode And adrs.U_AddType=ba.AdresType And adrs.U_UserName='" & PARENT.gsUserName & "')"
                'Address for Customer block means block for linked BP as well
                sRequired &= " And Not ba.Address in (Select adrs.U_Address from [@IDH_ADDRST] adrs Where (adrs.U_CustCode=ba.CardCode or " _
                    & " adrs.U_CustCode  in (Select OCRD.CardCode from OCRD where OCRD.U_IDHLBPC=ba.CardCode)) " _
                    & " And adrs.U_AddType=ba.AdresType And adrs.U_UserName='" & PARENT.gsUserName & "')"

            End If
            oGridN.setRequiredFilter(sRequired)

            ''##MA End 18-09-2014 Issue#403
            '## End12-06-2013
            oGridN.doAddFilterField("IDH_CUSCOD", "ba.CardCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_ADDRES", "ba.Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_ADRTYP", "ba.AdresType", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            'KA -- START -- 20121025
            oGridN.doAddFilterField("IDH_STREET", "ba.Street", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            'KA -- END -- 20121025
            oGridN.doAddFilterField("IDH_ZIP", "ba.ZipCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddListField("ba.AdresType", "Type", False, -1, Nothing, "ADRESTYPE")
            oGridN.doAddListField("(Case When ba.Address= bp.ShipToDef AND ba.AdresType='S' Then 'Y'  When ba.Address= bp.BillToDef AND ba.AdresType='B' Then 'Y' Else '' End)", "Default", False, -1, Nothing, Nothing)

            oGridN.doAddListField("ba.CardCode", "Customer", False, -1, Nothing, "CARDCODE", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("ba.Address", "Address", False, -1, Nothing, "ADDRESS")
            oGridN.doAddListField("ba.U_AddrsCd", "AddrsCd", False, 0, Nothing, "ADDRSCD")
            oGridN.doAddListField("ba.Street", "Street", False, -1, Nothing, "STREET")
            oGridN.doAddListField("ba.Block", "Block", False, -1, Nothing, "BLOCK")
            oGridN.doAddListField("ba.ZipCode", "Postal Code", False, -1, Nothing, "ZIPCODE")
            oGridN.doAddListField("ba.City", "City", False, -1, Nothing, "CITY")
            oGridN.doAddListField("ba.County", "County", False, -1, Nothing, "COUNTY")
            oGridN.doAddListField("ba.STATE", "State Cd", False, -1, Nothing, "STATE")
            oGridN.doAddListField("(SELECT st.NAME FROM OCST st WHERE st.CODE = ba.STATE AND st.COUNTRY = ba.COUNTRY)", "State", False, -1, Nothing, "STATENM")
            oGridN.doAddListField("ba.Country", "Country", False, -1, Nothing, "COUNTRY")
            oGridN.doAddListField("ba.U_ROUTE", "Route", False, -1, Nothing, "ROUTE")
            oGridN.doAddListField("ba.U_ISBACCDF", "Site Access Day", False, -1, Nothing, "ISBACCDF")
            oGridN.doAddListField("ba.U_ISBACCTF", "Access Time From", False, -1, Nothing, "ISBACCTF")
            oGridN.doAddListField("ba.U_ISBACCTT", "Access Time To", False, -1, Nothing, "ISBACCTT")
            oGridN.doAddListField("ba.U_ISBACCRA", "Access Restriction", False, -1, Nothing, "ISBACCRA")
            oGridN.doAddListField("ba.U_ISBACCRV", "Vehicle Access Rest.", False, -1, Nothing, "ISBACCRV")
            oGridN.doAddListField("ba.U_ISBCUSTRN", "Customer Ref. No.", False, -1, Nothing, "ISBCUSTRN")
            oGridN.doAddListField("ba.U_TEL1", "Site Tel.", False, -1, Nothing, "TEL1")
            '## Start12-06-2013
            'add phone filed of BP master table ocrd
            oGridN.doAddListField("bp.Phone1", "Phone", False, -1, Nothing, "PHONE1")
            '## End
            oGridN.doAddListField("ba.U_ROUTE", "Route", False, -1, Nothing, "ROUTE")
            oGridN.doAddListField("ba.U_IDHSEQ", "Route Seq.", False, -1, Nothing, "SEQ")
            oGridN.doAddListField("ba.U_IDHACN", "Site Contact Perso", False, -1, Nothing, "CONA")
            '## Start 21-06-2013
            'add main contact person filed of BP master table ocrd
            oGridN.doAddListField("bp.CntctPrsn", "Main Contact Perso", False, -1, Nothing, "MCONA")
            '## End
            oGridN.doAddListField("ba.U_IDHPCD", "Premisses Code", False, -1, Nothing, "PRECD")
            oGridN.doAddListField("ba.U_Origin", "Origin", False, -1, Nothing, "ORIGIN")
            oGridN.doAddListField("ba.U_IDHSLCNO", "Site Licence Number", False, -1, Nothing, "SITELIC")
            oGridN.doAddListField("ba.U_IDHSteId", "Site Id", False, -1, Nothing, "STEID")

            oGridN.doAddListField("ba.U_IDHFAX", "Fax", False, -1, Nothing, "FAX")
            oGridN.doAddListField("ba.U_IDHEMAIL", "Email", False, -1, Nothing, "EMAIL")
            oGridN.doAddListField("ba.U_IDHSTRNO", "Street Number", False, -1, Nothing, "STRNO")
            '## End update filed with aliase
			'##MA Start 12-09-2014 Issue#413
            oGridN.doAddListField("ba.U_MinJob", "Minimum Job in a Day", False, 0, Nothing, "MINJOB")
            '##MA End 12-09-2014 Issue#413

            '##MA Start 15-02-2017
            oGridN.doAddListField("ba.U_CARID", "Carrier ID", False, 0, Nothing, "CARID")
            oGridN.doAddListField("ba.U_CUSID", "Customer ID", False, 0, Nothing, "CUSID")
            oGridN.doAddListField("ba.U_Freist", "Freistellungsnummer", False, 0, Nothing, "FREIST")
            '##MA Start 15-02-2017

            '##MA Start 04-05-2017
            oGridN.doAddListField("ba.U_ICOEntNr", "Disposal Site ID", False, 0, Nothing, "DISPSID")
            oGridN.doAddListField("ba.U_ICOPRODN", "Customer Ship To ID", False, 0, Nothing, "CUSSID")
            oGridN.doAddListField("ba.U_Bef", "Carrier Ship To ID", False, 0, Nothing, "CARSID")
            '##MA Start 04-05-2017

            '##MA Start 14-04-2017' issue#163
            oGridN.doAddListField("ba.U_onStop", "onStop", False, -1, Nothing, "ONSTOP")
            '##MA End 14-04-2017'

            '#MA Start 17-05-2017 issue#402 point 1&2
            oGridN.doAddListField("ba.U_LAT", "Latitude", False, -1, Nothing, "LATITUDE")
            oGridN.doAddListField("ba.U_LONG", "Longitude", False, -1, Nothing, "LONGITUDE")
            '#MA End 17-05-2017
        End Sub
        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doReadInputParams(oForm)
            setParentSharedData(oForm, "CALLEDITEM", getParentSharedData(oForm, "CALLEDITEM"))
        End Sub
        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doFinalizeShow(oForm)
            If Not oForm.Visible Then
                Return
            End If
            If getParentSharedData(oForm, "IDH_CUSCOD") IsNot Nothing AndAlso getParentSharedData(oForm, "IDH_CUSCOD").ToString.Trim <> "" Then
                Dim otxt As SAPbouiCOM.EditText = CType(oForm.Items.Item("IDH_ADDRES").Specific, SAPbouiCOM.EditText)
                otxt.Active = True
                setEnableItem(oForm, False, "IDH_CUSCOD")

            Else
                setEnableItem(oForm, True, "IDH_CUSCOD")
            End If

            '#MA Start 04-07-2017
            If getParentSharedData(oForm, "IDH_ADRTYP") IsNot Nothing AndAlso getParentSharedData(oForm, "IDH_ADRTYP").ToString.Trim <> "" Then
                setEnableItem(oForm, False, "IDH_ADRTYP")
            Else
                setEnableItem(oForm, True, "IDH_ADRTYP")
            End If
            '#MA End 04-07-2017
        End Sub

           
        '**
        '** THIS WILL RETURN A RECORD IF THERE IS ONLY ONE SHIP TO ADDRESS
        '**
        Public Shared Function doGetAddress(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sCardCode As String, ByVal sAddressType As String, Optional ByVal sSearchAddress As String = Nothing) As ArrayList
            Return Config.INSTANCE.doGetAddress(sCardCode, sAddressType, sSearchAddress)
        End Function

        '**
        '** THIS WILL RETURN THE COMPANY ADDRESS
        '**
        Public Shared Function doGetCompanyAddress(ByVal oParent As IDHAddOns.idh.addon.Base) As ArrayList
            Return Config.INSTANCE.doGetCompanyAddress()
            'Return Config.INSTANCE.doGetCompanyAddress(oParent)
        End Function

        Public Overrides Function doItemEvent(oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.setParentSharedData(oForm, "IDH_ADRSRCHOPEND", "")
                    If bClosingFormByOKButton = False Then
                        MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                    End If
                End If
                Return True
            End If
            If pVal.BeforeAction = False Then
                Select pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_FORM_CLOSE
                        Me.setParentSharedData(oForm, "IDH_ADRSRCHOPEND", "")
                        If bClosingFormByOKButton = False Then
                            MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                        End If
                End Select
            End If
            Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
        End Function

    End Class
End Namespace
