Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class HAZ
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDH_WTHZSR", "HAZ Search.srf", 5, 45, 603, 320, "HAZ Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("[@IDH_WTHAZ]")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_WTHAZ", Nothing, Nothing, False, True))
            oGridN.setOrderValue("Code")

            oGridN.doAddFilterField("IDH_HZCOD", "Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_WTWC", "U_WTWC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_WTWD", "U_WTWD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100)

            oGridN.doAddListField("Code", "HAZ Code", False, -1, Nothing, "IDH_HZCODO")
            oGridN.doAddListField("Name", "HAZ Name", False, -1, Nothing, "IDH_HZNAMO")
            oGridN.doAddListField("U_WTWC", "Waste Code", False, -1, Nothing, "IDH_WTWCO")
            oGridN.doAddListField("U_WTWD", "Waste Description", False, -1, Nothing, "IDH_WTWDO")
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace