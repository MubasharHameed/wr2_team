Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.bridge.lookups
Imports com.idh.dbObjects.User

Imports com.idh.bridge

Namespace idh.forms.search
    Public Class LinkWO
        Inherits IDHAddOns.idh.forms.Search

        Private bClosingFormByOKButton As Boolean = False

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHLNKWO", "Link WO.srf", 5, 40, 600, 350, "Available Waste Orders")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            Dim sDispOrd As String = getParentSharedDataAsString(oGridN.getSBOForm(), "DISPORD")
            Dim sDispRow As String = getParentSharedDataAsString(oGridN.getSBOForm(), "DISPROW")
            Dim sUsedRows As String = getParentSharedDataAsString(oGridN.getSBOForm(), "USEDROWS")

            ''Temp Disable 20170116 Start
            'Dim moLinkWOSearch As com.idh.bridge.search.LinkWO = New com.idh.bridge.search.LinkWO()
            'moLinkWOSearch.setSBOForm(oGridN.getIDHForm, oGridN.getSBOForm())
            'moLinkWOSearch.doSetGridOptions(oGridN.getGridControl(), sDispOrd, sDispRow, sUsedRows)
            'End

            'com.idh.bridge.search.LinkWO.doSetGridOptions(oGridN.getGridControl(), sDispOrd, sDispRow) 
        End Sub

        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            '## Start 12-07-2013
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
            '## End
        End Sub

        '        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
        '            MyBase.doCompleteCreate(oForm, BubbleEvent)
        '        End Sub

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            Try
                '## Start 15-07-2015
                bClosingFormByOKButton = False
                Me.setParentSharedData(oForm, "IDH_LKWOSRCHOPEND", "LOADED")
                '## End
                MyBase.doLoadData(oForm)

                Dim sWOOrd As String = getParentSharedDataAsString(oForm, "IDH_WOHD")
                If Not sWOOrd Is Nothing AndAlso sWOOrd.Length > 0 Then
                    Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")

                    If Config.INSTANCE.doGetDoBuyingAndTrade() Then
                        If oGridN.getRowCount() <= 1 Then
                            oForm.Items.Item("IDH_CREATE").Visible = True
                            setEnableItem(oForm, False, "IDH_WOH")
                            setEnableItem(oForm, False, "IDH_WOROW")
                        Else
                            oForm.Items.Item("IDH_CREATE").Visible = False
                        End If
                    Else
                        oForm.Items.Item("IDH_CREATE").Visible = False
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error loading the Data.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", {Nothing})
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            ''## Start 05-07-2013
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD OrElse _
                    (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.setParentSharedData(oForm, "IDH_LKWOSRCHOPEND", "")
                    If bClosingFormByOKButton = False Then
                        MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                        Return True
                    End If
                End If
            End If
            ''## End

            If MyBase.doItemEvent(oForm, pVal, BubbleEvent) Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                    If pVal.ItemUID = "IDH_CREATE" Then
                        If pVal.BeforeAction = True Then
                            Dim sWOOrd As String = getParentSharedDataAsString(oForm, "IDH_WOHD")
                            'Dim sWOR As String

                            Dim oWOR As IDH_JOBSHD = IDH_JOBSHD.doAutoCreateNewRowFromLastJobRow(sWOOrd, True, False, False, True)
                            'doAutoCreateNewRowFromLastJobRow(goParent, sWOOrd, True, False, False)
                            'If Not sWOR Is Nothing AndAlso sWOR.Length > 0 Then
                            If Not oWOR Is Nothing Then
                                doLoadData(oForm)
                            End If
                            IDH_JOBSHD.doReleaseObject(oWOR)
                            BubbleEvent = False
                        End If
                        '                        If pVal.BeforeAction = True Then
                        '                            doSetReturnButton(oForm, "IDH_CREATE")
                        '                            BubbleEvent = False
                        '                            If goParent.doCheckModal(oForm.UniqueID) = True Then
                        '                                doPrepareModalData(oForm)
                        '                            End If
                        '                        End If
                    End If
                End If
            End If
            Return True
        End Function

        '## Start 04-07-2013

        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doFinalizeShow(oForm)
            If oForm.Visible = False Then
                Me.setParentSharedData(oForm, "IDH_LKWOSRCHOPEND", "")
            End If
        End Sub
        ''## End
    End Class
End Namespace
