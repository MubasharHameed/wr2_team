Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class USACounty
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHCNTYSR", "USACounty Search.srf", 5, 45, 603, 320, "USA County Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("[@IDH_ORIGIN]")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@ISB_COUNTY", Nothing, Nothing, False, True))
            oGridN.setOrderValue("U_StateCd")
            'oGridN.setRequiredFilter("U_OC='Y'")

            'oGridN.doAddFilterField("IDH_LOAUTHL", "U_LOAUTHL", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_STATCD", "U_StateCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            'oGridN.doAddFilterField("IDH_OC", "U_OC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 2)
            'oGridN.doAddFilterField("IDH_AC", "U_AC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 2)

            oGridN.doAddListField("U_StateCd", "State Code", False, -1, Nothing, "IDH_STATCD")
            oGridN.doAddListField("U_CountyCd", "County Code", False, -1, Nothing, "IDH_CNTYCD")
            oGridN.doAddListField("U_CountyNm", "County Name", False, -1, Nothing, "IDH_CNTYNM")
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace
