Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class Route
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHROSRC", "Route Search.srf", 5, 45, 682, 320, "Route Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("[@IDH_ROUTES]")
            oGridN.doAddGridTable( New com.idh.bridge.data.GridTable( "@IDH_ROUTES" ) )
            oGridN.setOrderValue("Code")

			''## MA STart Issue#407: Fixed the filter parameter name apart from CR
            oGridN.doAddFilterField("IDH_RTCODE", "Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDH_RTNAM", "Name", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_RTDES", "U_DESCR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_RTSEQ", "U_SEQ", SAPbouiCOM.BoDataType.dt_SHORT_NUMBER, "LIKE", 30)
            oGridN.doAddFilterField("IDH_RTWDAY", "U_IDHWDAY", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            ''## MA End Issue#407: Fixed the filter parameter name apart from CR


            oGridN.doAddListField("Code", "Route Code", False, -1, Nothing, "IDH_RTCODE")
            oGridN.doAddListField("Name", "Route Name", False, -1, Nothing, "IDH_RTNAM")
            oGridN.doAddListField("U_DESCR", "Description", False, 0, Nothing, "IDH_RTDES")
            oGridN.doAddListField("U_SEQ", "Default Seq.", False, 0, Nothing, "IDH_RTSEQ")
            oGridN.doAddListField("U_IDHWDAY", "Week Days", False, 100, Nothing, "IDH_RTWDAY")
			''## MA STart Issue#407
            oGridN.doAddListField("U_COST", "COST", False, -1, Nothing, "IDH_RTCOST")
            ''## MA End Issue#407
        End Sub

        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doReadInputParams(oForm)
            setParentSharedData(oForm, "CALLEDITEM", getParentSharedData(oForm, "CALLEDITEM"))
        End Sub

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            Me.setParentSharedData(oForm, "IDH_ROUTSRCSRCHOPEND", "LOADED")
            MyBase.doLoadData(oForm)
        End Sub

        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
        End Sub
		
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

		Public Overrides Function doItemEvent(oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD OrElse _
                    (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.setParentSharedData(oForm, "IDH_ROUTSRCSRCHOPEND", "")
                    MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                End If
            End If
            Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
        End Function
		
    End Class
End Namespace