Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.controls
Imports com.idh.controls.strct
Imports System

Imports com.idh.bridge.lookups
Imports com.uBC.utils

Imports com.idh.bridge

Namespace idh.forms.search
    Public Class Lorry
        Inherits IDHAddOns.idh.forms.Search
        Private bClosingFormByOKButton As Boolean = False

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHLOSCH", CType(IIf(Config.ParameterAsBool("VMUSENEW", False) = True, "VehicleSearch2.srf", "VehicleSearch.srf"), String), 5, 55, 706, 335, "Vehicle Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            oGridN.getSBOGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto
            
            Dim bDoActivity As Boolean = False
            Dim sDoShowActivity As String = getParentSharedDataAsString(oGridN.getSBOForm(), "SHOWACTIVITY")
            Dim sDoShowOnSite As String = getParentSharedDataAsString(oGridN.getSBOForm(), "SHOWONSITE")
            Dim sSOPO As String = getParentSharedDataAsString(oGridN.getSBOForm(), "SOPO")
            Dim sJobTp As String = getParentSharedDataAsString(oGridN.getSBOForm(), "JOBTYPE")
            Dim sWR1OT As String = getParentSharedDataAsString(oGridN.getSBOForm(), "IDH_WR1OT")
			
			Dim bDoShowActivity As Boolean = False
			Dim bDoShowOnSite As Boolean = False
            If sDoShowActivity IsNot Nothing AndAlso sDoShowActivity.Equals("TRUE", StringComparison.CurrentCultureIgnoreCase) Then
				bDoShowActivity = True
			End If
			
            If sDoShowOnSite IsNot Nothing AndAlso sDoShowOnSite.Equals("TRUE", StringComparison.CurrentCultureIgnoreCase) Then
				bDoShowOnSite = True
			End If

            ''Temp Disable 20170116 Start
            Dim oSearch = New com.idh.bridge.search.Lorry()
            oSearch.doSetGridOptions(oGridN.getGridControl(), bDoShowActivity, bDoShowOnSite, sJobTp, sSOPO, sWR1OT)

        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            If Config.ParameterAsBool("VMUSENEW", False) = False Then
                doGetItemGroups(oForm)
                doFillBranchCombo(oForm)
            Else
                doFillWFStatus(oForm)
            End If
            doFillTypes(oForm)
            doWR1OrderTypeCombo(oForm)

            MyBase.doBeforeLoadData(oForm)
        End Sub

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            Try
                '## Start 15-07-2013
                bClosingFormByOKButton = False
                Me.setParentSharedData(oForm, "IDH_VEHSROPEND", "LOADED")
                '## End
                MyBase.doLoadData(oForm)
                oForm.Items.Item("IDH_VEHREG").Click(SAPbouiCOM.BoCellClickType.ct_Regular)

                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                Dim sReg As String
                sReg = getParentSharedDataAsString(oForm, "IDH_VEHREG")

                Dim sDoShowCreate As String
                sDoShowCreate = getParentSharedDataAsString(oForm, "SHOWCREATE")
                If sDoShowCreate IsNot Nothing AndAlso sDoShowCreate.ToUpper().Equals("TRUE", StringComparison.CurrentCultureIgnoreCase) Then
                    If oGridN.getRowCount() <= 1 Or sReg.Length = 0 Then 'AndAlso sReg.Length = 0 Then
                        oForm.Items.Item("IDH_CREATE").Visible = True
                    Else
                        oForm.Items.Item("IDH_CREATE").Visible = False
                    End If
                Else
                    oForm.Items.Item("IDH_CREATE").Visible = False
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error loading the Data.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", {Nothing})
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        Public Sub doFillWFStatus(ByVal oForm As SAPbouiCOM.Form)
            FillCombos.WFStatusCombo(FillCombos.getCombo(oForm, "IDH_CWFS"))
        End Sub

        Public Sub doGetItemGroups(ByVal oForm As SAPbouiCOM.Form)
            'GET THE ITEM GROUPS
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Dim oItem As SAPbouiCOM.Item
            Try
                Dim oItmGrp As SAPbouiCOM.ComboBox
                oRecordSet = CType(goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
                '...                Dim sQry As String = "select ItmsGrpCod, ItmsGrpNam from OITB g, [@IDH_JOBTYPE] jt where g.ItmsGrpNam=jt.U_ItemGrp group by ItmsGrpCod, ItmsGrpNam"
                Dim sQry As String = "select ItmsGrpCod, ItmsGrpNam from OITB g, [@IDH_JOBTYPE] jt where g.ItmsGrpCod=jt.U_ItemGrp group by ItmsGrpCod, ItmsGrpNam"
                oRecordSet.DoQuery(sQry)
                If oRecordSet.RecordCount > 0 Then
                    oItem = oForm.Items.Item("IDH_ITMGRP")
                    oItem.DisplayDesc = True
                    oItmGrp = CType(oItem.Specific, SAPbouiCOM.ComboBox)

                    doClearValidValues(oItmGrp.ValidValues)

                    oItmGrp.ValidValues.Add("", getTranslatedWord("Any"))
                    Dim iCount As Integer
                    Dim oVal1 As String
                    Dim oVal2 As String
                    For iCount = 0 To oRecordSet.RecordCount - 1
                        oVal1 = CType(oRecordSet.Fields.Item(0).Value, String)
                        oVal2 = CType(oRecordSet.Fields.Item(1).Value, String)
                        Try
                            oItmGrp.ValidValues.Add(oVal1, oVal2)
                        Catch ex As Exception
                        End Try
                        oRecordSet.MoveNext()
                    Next
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error retrieving the Item Groups.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Item Groups")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(Nothing)
            End Try
        End Sub

        Private Sub doFillTypes(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            Dim oCombo As SAPbouiCOM.ComboBox
            oItem = oForm.Items.Item("IDH_VEHTP")
            oItem.DisplayDesc = True
            oCombo = CType(oItem.Specific, SAPbouiCOM.ComboBox)

            doClearValidValues(oCombo.ValidValues)

            oCombo.ValidValues.Add("", getTranslatedWord("All"))
            oCombo.ValidValues.Add("A", getTranslatedWord("Asset-OnRoad"))
            oCombo.ValidValues.Add("O", getTranslatedWord("Asset-OffRoad"))
            oCombo.ValidValues.Add("S", getTranslatedWord("Sub Contract"))
            oCombo.ValidValues.Add("H", getTranslatedWord("Owner Driver"))
            oCombo.ValidValues.Add("C", getTranslatedWord("Customer"))
            'Dim sDefaultType As String = Config.ParameterWithDefault("DFLSRTYE", "All")
            'If sDefaultType IsNot Nothing Then 'AndAlso sDefaultType.Length > 1 Then
            '    'oCombo.SelectExclusive(sDefaultType, SAPbouiCOM.BoSearchKey.psk_ByDescription)
            '    setUFValue(oForm, "IDH_VEHTP", sDefaultType)
            'End If

        End Sub

        Private Sub doWR1OrderTypeCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            Dim oCombo As SAPbouiCOM.ComboBox
            oItem = oForm.Items.Item("IDH_WR1OT")
            oItem.DisplayDesc = True
            oCombo = CType(oItem.Specific, SAPbouiCOM.ComboBox)

            doClearValidValues(oCombo.ValidValues)

            oCombo.ValidValues.Add("", getTranslatedWord("All"))
            oCombo.ValidValues.Add("WO", getTranslatedWord("Waste Order"))
            oCombo.ValidValues.Add("DO", getTranslatedWord("Disposal  Order"))
            oCombo.ValidValues.Add("^WO", getTranslatedWord("Open And Waste Order"))
            oCombo.ValidValues.Add("^DO", getTranslatedWord("Open And Disposal  Order"))
        End Sub

        Private Sub doFillBranchCombo(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_BRANCH", "OUBR", "Code", "Remarks", Nothing, Nothing, True)
        End Sub

        Private Sub doFillSuppliers(ByVal oForm As SAPbouiCOM.Form)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                Dim oItem As SAPbouiCOM.Item
                Dim oCombo As SAPbouiCOM.ComboBox
                oRecordSet = CType(goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
                Dim sQry As String = "SELECT U_SCrdCd, U_SName FROM [@IDH_VEHMAS] WITH(NOLOCK) group by U_SCrdCd, U_SName "
                oRecordSet.DoQuery(sQry)
                If oRecordSet.RecordCount > 0 Then
                    oItem = oForm.Items.Item("IDH_VEHSUP")
                    oItem.DisplayDesc = True
                    oCombo = CType(oItem.Specific, SAPbouiCOM.ComboBox)
                    Dim iCount As Integer
                    Dim oVal1 As String
                    Dim oVal2 As String

                    doClearValidValues(oCombo.ValidValues)

                    oCombo.ValidValues.Add("", getTranslatedWord("All"))
                    For iCount = 0 To oRecordSet.RecordCount - 1
                        oVal1 = CType(oRecordSet.Fields.Item(0).Value, String).Trim()
                        oVal2 = CType(oRecordSet.Fields.Item(1).Value, String).Trim()
                        If oVal1.Length > 0 Then
                            Try
                                oCombo.ValidValues.Add(oVal1, oVal2)
                            Catch ex As Exception
                            End Try
                        End If
                        oRecordSet.MoveNext()
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the supplier Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("supplier")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            ''## Start 05-07-2013
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD OrElse _
                    (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.setParentSharedData(oForm, "IDH_VEHSROPEND", "")
                    If bClosingFormByOKButton = False Then
                        MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                        Return True
                    End If
                End If
            End If
            ''## End
            If MyBase.doItemEvent(oForm, pVal, BubbleEvent) Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                    If pVal.ItemUID = "IDH_CREATE" Then
                        If pVal.BeforeAction = True Then
                            doSetReturnButton(oForm, "IDH_CREATE")
                            BubbleEvent = False
                            If goParent.doCheckModal(oForm.UniqueID) = True Then
                                doPrepareModalData(oForm)
                            End If
                        End If
                    End If
                End If
            End If
            Return True
        End Function

        Protected Overrides Sub doPrepareModalData(ByVal oForm As SAPbouiCOM.Form, Optional ByVal iSel As Integer = -1)
            'Dim oDOWrapper As com.uBC.utils.DOWrapper = getParentSharedData(oForm, com.uBC.utils.DOWrapper.msDOWrapper)
            'If (oDOWrapper Is Nothing) Then
            '    oDOWrapper = New com.uBC.utils.DOWrapper()
            'End If

            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            Dim iIndex As Integer
            Dim iSelected As Integer
            Dim oSelectedRows As SAPbouiCOM.SelectedRows
            oSelectedRows = oGridN.getSBOGrid.Rows.SelectedRows

            If oSelectedRows.Count = 0 Then
                setParentSharedData(oForm, "REG", getUFValue(oForm, "IDH_VEHREG"))
                setParentSharedData(oForm, "DRIVER", getUFValue(oForm, "IDH_DRVR"))
                setParentSharedData(oForm, "VTYPE", getUFValue(oForm, "IDH_VEHTP"))

                If Config.ParameterAsBool("VMUSENEW", False) = True Then
                    setParentSharedData(oForm, "V_WR1ORD", "")
                    setParentSharedData(oForm, "VEHDIS", getUFValue(oForm, "IDH_VEHDIS"))
                    setParentSharedData(oForm, "WCCD", getUFValue(oForm, "IDH_CARR"))
                Else
                    setParentSharedData(oForm, "V_WR1ORD", getUFValue(oForm, "IDH_WR1OT"))
                    ''SV 25/03/2015
                    setParentSharedData(oForm, "CUSCD", getUFValue(oForm, "IDH_VEHCUS"))
                    setParentSharedData(oForm, "BRANCH", getUFValue(oForm, "IDH_BRANCH"))
                    setParentSharedData(oForm, "ITEMGROUP", getUFValue(oForm, "IDH_ITMGRP"))
                    ''End
                    setParentSharedData(oForm, "WCCD", getUFValue(oForm, "IDH_VEHCUS"))
                End If
                ''SV 21/04/2015
                'oDOWrapper.VehicleRegistration = getUFValue(oForm, "IDH_VEHREG")
                'oDOWrapper.Driver = getUFValue(oForm, "IDH_DRVR")
                'oDOWrapper.VehicleType = getUFValue(oForm, "IDH_VEHTP")
                'oDOWrapper.VehicleWR1ORD = getUFValue(oForm, "IDH_WR1OT")
                'oDOWrapper.VehicleDescription = getUFValue(oForm, "IDH_VEHDIS")
                'oDOWrapper.CarrierCode = getUFValue(oForm, "IDH_CARR")
                'oDOWrapper.CustomerCode = getUFValue(oForm, "IDH_VEHCUS")
                'oDOWrapper.Branch = getUFValue(oForm, "IDH_BRANCH")
                'oDOWrapper.WasteItemGroup = getUFValue(oForm, "IDH_ITMGRP")
                'oDOWrapper.WasteCode = getUFValue(oForm, "IDH_VEHCUS")
                'setParentSharedData(oForm, com.uBC.utils.DOWrapper.msDOWrapper, oDOWrapper)
                ''End
                doReturnFromModalShared(oForm, True)
                Exit Sub
            End If

            If iSel = -1 Then
                iSelected = oGridN.getSBOGrid.GetDataTableRowIndex(oSelectedRows.Item(iIndex, SAPbouiCOM.BoOrderType.ot_SelectionOrder))
            End If
            If iSelected > -1 Then
                oGridN.setCurrentDataRowIndex(iSelected)

                Dim iFields As Integer
                Dim oField As ListField
                For iFields = 0 To oGridN.getListfields().Count - 1
                    oField = CType(oGridN.getListfields().Item(iFields), ListField)
                    setParentSharedData(oForm, oField.msFieldId, oGridN.doGetFieldValue(oField.msFieldName))
                    'oDOWrapper.doSetPropertyFromString(oField.msFieldId, oGridN.doGetFieldValue(oField.msFieldName))
                Next
            End If
            'setParentSharedData(oForm, com.uBC.utils.DOWrapper.msDOWrapper, oDOWrapper)
            doReturnFromModalShared(oForm, True)
        End Sub

        '## Start 15-07-2013
        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
        End Sub

        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doFinalizeShow(oForm)
            If oForm.Visible = False Then
                Me.setParentSharedData(oForm, "IDH_VEHSROPEND", "")
            End If
        End Sub
        ''## End
    End Class
End Namespace
