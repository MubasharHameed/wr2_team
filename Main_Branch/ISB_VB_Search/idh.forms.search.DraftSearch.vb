Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class DraftSearch
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHDRAFTSRC", "Draft Search.srf", 5, 40, 603, 350, "Draft AP Documents List")
        End Sub

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)

            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New FilterGrid(Me, oForm, "LINESGRID", True)
            End If
            oGridN.doReloadData()

        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("ODRF", "d"))

            Dim sReqFilter As String
            sReqFilter = " d.DocStatus = 'O' AND d.ObjType = '18' "
            oGridN.setRequiredFilter(sReqFilter)

            oGridN.setOrderValue("d.DocEntry DESC ")

            oGridN.doAddFilterField("IDH_DOCCD", "d.DocEntry", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDH_DOCNM", "d.DocNum", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDH_SUPCD", "d.CardCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)

            oGridN.doAddListField("d.DocEntry", "Draft Entry #", False, -1, Nothing, "DOCENTRY", -1, SAPbouiCOM.BoLinkedObject.lf_Drafts)
            oGridN.doAddListField("d.DocNum", "Document Number", False, -1, Nothing, "DOCNUM")
            oGridN.doAddListField("d.DocDate", "Posting Date", False, -1, Nothing, "POSTDT")
            oGridN.doAddListField("d.DocTotal", "Total", False, -1, Nothing, "DOCTOTAL")
            oGridN.doAddListField("d.Comments", "Remarks", False, -1, Nothing, "DOCREMARK")

        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
            Dim oItm As SAPbouiCOM.Item = oForm.Items.Item("1")
            Dim oBtn As SAPbouiCOM.Button

            If oItm IsNot Nothing Then
                oBtn = CType(oItm.Specific, SAPbouiCOM.Button)
                oBtn.Caption = getTranslatedWord("Ok")
            End If
        End Sub

    End Class
End Namespace
