Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class ProfitCentre
        Inherits IDHAddOns.idh.forms.Search
        
    Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHPRCSRC", "ProfitCentreSearch.srf", 5, 45, 603, 320, "Profit Centre Search")
        End Sub   
        
'        '*** Set the Form Title
'        Protected Overrides Sub doSetTitle(ByVal oForm As SAPbouiCOM.Form)
'        	oForm.Title = "Profit Centre Search"
'        End Sub
        
'        '*** Set the Labels
'        Protected Overridable Sub doSetLabels(ByVal oForm As SAPbouiCOM.Form)
'        End Sub
        
        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
        	'oGridN.setTableValue("OOCR")
        	oGridN.doAddGridTable( new com.idh.bridge.data.GridTable( "OOCR" ))
        	oGridN.setOrderValue("OcrCode")
        	
        	oGridN.doAddFilterField("IDH_CODE", "OcrCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 15, Nothing, True)
            oGridN.doAddFilterField("IDH_NAME", "OcrName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100, Nothing, True)
            oGridN.doAddFilterField("IDH_DIMC", "DimCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 100, Nothing, True)
        	
            oGridN.doAddListField("OcrCode", "Profit Centre", False, -1, Nothing, "IDHCODE", -1, SAPbouiCOM.BoLinkedObject.lf_ProfitCenter)
            oGridN.doAddListField("OcrName", "Profit Centre Description", False, -1, Nothing, "IDHNAME")
            oGridN.doAddListField("DimCode", "Dimension Code", False, -1, Nothing, "DIMCODE")
        End Sub
        
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace 
