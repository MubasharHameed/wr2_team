Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.bridge
Imports com.idh.bridge.lookups

Namespace idh.forms.search
    Public Class MAddressSearch
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHMASRCH", "MAddress Search.srf", 5, 45, 790, 350, "Mailing Address Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("CRD1"), True)

            oGridN.doAddFilterField("IDH_CUSCOD", "CardCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_ADDRES", "Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_ADRTYP", "AdresType", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_ZIP", "ZipCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_MADD", "U_MAILADD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 1)

            oGridN.doAddListField("AdresType", "Type", False, -1, Nothing, "ADRESTYPE")
            oGridN.doAddListField("CardCode", "Customer", False, -1, Nothing, "CARDCODE", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("Address", "Address", False, -1, Nothing, "ADDRESS")
            oGridN.doAddListField("Street", "Street", False, -1, Nothing, "STREET")
            oGridN.doAddListField("Block", "Block", False, -1, Nothing, "BLOCK")
            oGridN.doAddListField("City", "City", False, -1, Nothing, "CITY")
            oGridN.doAddListField("State", "State Cd", False, -1, Nothing, "STATE")
            oGridN.doAddListField("(SELECT NAME FROM OCST WHERE CODE = CRD1.STATE AND COUNTRY = CRD1.COUNTRY)", "State", False, -1, Nothing, "STATENM")
            oGridN.doAddListField("ZipCode", "Postal Code", False, -1, Nothing, "ZIPCODE")
            oGridN.doAddListField("County", "County", False, -1, Nothing, "COUNTY")
            oGridN.doAddListField("Country", "Country", False, -1, Nothing, "COUNTRY")
            oGridN.doAddListField("U_ROUTE", "Route", False, -1, Nothing, "ROUTE")
            oGridN.doAddListField("U_ISBACCDF", "Site Access Day", False, -1, Nothing, "ISBACCDF")
            oGridN.doAddListField("U_ISBACCTF", "Access Time From", False, -1, Nothing, "ISBACCTF")
            oGridN.doAddListField("U_ISBACCTT", "Access Time To", False, -1, Nothing, "ISBACCTT")
            oGridN.doAddListField("U_ISBACCRA", "Access Restriction", False, -1, Nothing, "ISBACCRA")
            oGridN.doAddListField("U_ISBACCRV", "Vehicle Access Rest.", False, -1, Nothing, "ISBACCRV")
            oGridN.doAddListField("U_ISBCUSTRN", "Customer Ref. No.", False, -1, Nothing, "ISBCUSTRN")
            oGridN.doAddListField("U_TEL1", "Site Tel.", False, -1, Nothing, "TEL1")
            oGridN.doAddListField("U_ROUTE", "Route", False, -1, Nothing, "ROUTE")
            oGridN.doAddListField("U_IDHSEQ", "Route Seq.", False, -1, Nothing, "SEQ")
            oGridN.doAddListField("U_IDHACN", "Contact Perso", False, -1, Nothing, "CONA")
            oGridN.doAddListField("U_IDHPCD", "Premisses Code", False, -1, Nothing, "PRECD")
            oGridN.doAddListField("U_Origin", "Origin", False, -1, Nothing, "ORIGIN")
            oGridN.doAddListField("U_IDHSLCNO", "Site Licence Number", False, -1, Nothing, "SITELIC")
            oGridN.doAddListField("U_IDHSteId", "Site Id", False, -1, Nothing, "STEID")

            oGridN.doAddListField("U_IDHFAX", "Fax", False, -1, Nothing, "FAX")
            oGridN.doAddListField("U_IDHEMAIL", "Email", False, -1, Nothing, "EMAIL")
            oGridN.doAddListField("U_IDHSTRNO", "Street Number", False, -1, Nothing, "STRNO")
            oGridN.doAddListField("U_MAILADD", "Mailing Address", False, -1, Nothing, "MAILADD")

        End Sub

        '**
        '** THIS WILL RETURN A RECORD IF THERE IS ONLY ONE SHIP TO ADDRESS
        '**
        Public Shared Function doGetAddress(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sCardCode As String, ByVal sAddressType As String, Optional ByVal sSearchAddress As String = Nothing) As ArrayList
            Return Config.INSTANCE.doGetAddress(sCardCode, sAddressType, sSearchAddress)
        End Function

        '**
        '** THIS WILL RETURN THE COMPANY ADDRESS
        '**
        Public Shared Function doGetCompanyAddress(ByVal oParent As IDHAddOns.idh.addon.Base) As ArrayList
            Return Config.INSTANCE.doGetCompanyAddress()
        End Function

        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            doFillMailingAdd(oForm)

            MyBase.doReadInputParams(oForm)
            setParentSharedData(oForm, "CALLEDITEM", getParentSharedData(oForm, "CALLEDITEM"))
            Dim sVal As String = getParentSharedData(oForm, "IDH_MADD").ToString()
            If Not sVal Is Nothing Then
                setUFValue(oForm, "IDH_MADD", sVal)

            End If
        End Sub

        Private Sub doFillMailingAdd(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDH_MADD")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = CType(oItem.Specific, SAPbouiCOM.ComboBox)
            doClearValidValues(oCombo.ValidValues)

            oCombo.ValidValues.Add("Y", "Yes")
            oCombo.ValidValues.Add("N", "No")
        End Sub


    End Class
End Namespace
