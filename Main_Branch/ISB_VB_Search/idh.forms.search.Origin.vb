Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports SAPbouiCOM

Namespace idh.forms.search
    Public Class Origin
        Inherits IDHAddOns.idh.forms.Search
        Private bClosingFormByOKButton As Boolean = False


        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHEAORSR", "Origin Search.srf", 5, 45, 603, 320, "Origin Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("[@IDH_ORIGIN]")
            oGridN.doAddGridTable( new com.idh.bridge.data.GridTable( "@IDH_ORIGIN" ))
            oGridN.setOrderValue("U_ORCODE")
            oGridN.setRequiredFilter("U_OC='Y'")

            'oGridN.doAddFilterField("IDH_LOAUTHL", "U_LOAUTHL", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_ORCODE", "U_ORCODE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            'oGridN.doAddFilterField("IDH_OC", "U_OC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 2)
            'oGridN.doAddFilterField("IDH_AC", "U_AC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 2)

            oGridN.doAddListField("U_LOAUTHL", "Local Authority List", False, -1, Nothing, "IDH_LOAUTHL")
            oGridN.doAddListField("U_ORCODE", "Code", False, -1, Nothing, "IDH_ORCODE")
            oGridN.doAddListField("U_OC", "Origin Code", False, -1, Nothing, "IDH_OC")
            oGridN.doAddListField("U_AC", "Area Code", False, -1, Nothing, "IDH_AC")
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            ''## Start 05-07-2013
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD OrElse
                    (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.setParentSharedData(oForm, "IDH_ORGSROPEND", "")
                    If bClosingFormByOKButton = False Then
                        MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                        Return True
                    End If
                End If
            End If
            ''## End

            If (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN) Then
                If pVal.BeforeAction Then
                    If pVal.CharPressed = 27 Then
                        Dim sTempOrigin As String = getSharedDataAsString(oForm, "IDH_ORCODE")
                        If sTempOrigin Is Nothing Then
                            doPrepareModalData(oForm, -1)
                        End If
                    End If
                End If
            End If
            Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
        End Function

        Protected Overrides Sub doPrepareModalData(ByVal oForm As SAPbouiCOM.Form, Optional ByVal iSel As Integer = -1)
            MyBase.doPrepareModalData(oForm, iSel)
        End Sub

        '## Start 04-07-2013
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            bClosingFormByOKButton = False
            Me.setParentSharedData(oForm, "IDH_ORGSROPEND", "LOADED")
            MyBase.doLoadData(oForm)
        End Sub

        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
        End Sub
        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doReadInputParams(oForm)
            setParentSharedData(oForm, "CALLEDITEM", getParentSharedData(oForm, "CALLEDITEM"))
        End Sub
        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doFinalizeShow(oForm)
            If oForm.Visible = False Then
                Me.setParentSharedData(oForm, "IDH_ORGSROPEND", "")
            End If
        End Sub
        ''## End


    End Class
End Namespace
