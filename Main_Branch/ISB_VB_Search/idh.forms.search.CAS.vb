Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class CAS
        Inherits IDHAddOns.idh.forms.Search


        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "CASTBLSCR", "CAS Search.srf", 5, 45, 603, 320, "CAS Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("[@IDH_WTUN]")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_CHEM"))

            oGridN.setOrderValue("U_ChemicalNm")

            oGridN.doAddFilterField("IDH_CHMCD", "U_ChemicalCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_CHMNM", "U_ChemicalNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            oGridN.doAddListField("Code", "Code", False, 0, Nothing, "IDH_CODE")
            oGridN.doAddListField("Name", "Name", False, 0, Nothing, "IDH_NAME")
            oGridN.doAddListField("U_ChemicalCd", "Chemical Code", False, -1, Nothing, "IDH_CHMCD")
            oGridN.doAddListField("U_ChemicalNm", "Chemical Name", False, -1, Nothing, "IDH_CHMNM")

            'oGridN.doAddListField("U_WTUST", "Quantity", False, -1, Nothing, "IDH_WTUST")
            'oGridN.doAddListField("U_WTUFP", "Flashpoint", False, -1, Nothing, "IDH_WTUFP")
            'oGridN.doAddListField("U_WTUDE", "Density", False, -1, Nothing, "IDH_WTUDE")
            'oGridN.doAddListField("U_WTUPH", "PH", False, -1, Nothing, "IDH_WTUPH")
            'oGridN.doAddListField("U_WTUPG", "Pack Group", False, -1, Nothing, "IDH_WTUPG")
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace