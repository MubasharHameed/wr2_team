Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class Branch
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base) ', ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHBRANCHSRC", "Branch.srf", 5, 45, 410, 312, "Select Branch(s)")
            'MyBase.New(oParent, "IDHBRANCHSRC", Nothing, -1, "Option.srf", False, True, False, "Select Branch(s)", load_Types.idh_LOAD_NORMAL)
            'MyBase.New(oParent, "IDHBRANCHSRC", sParMenu, iMenuPosition, "Branch.srf", "Select Branch(s)")
        End Sub


        'Protected Overrides Sub doSetListFields(oGridN As IDHAddOns.idh.controls.FilterGrid)
        '    MyBase.doSetListFields(oGridN)
        'End Sub

        Public Overrides Sub doSetGridOptions(oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("IDH_BRANCHS", "", "Code", False, True), True) '(Select Code,Name,Remarks,(CAST (0 as bit)) As [Select] from OUBR)
            oGridN.setOrderValue("Name")
        'ko()
            oGridN.doAddListField("Code", "Code", False, 0, Nothing, "IDH_BRCODE")
            oGridN.doAddListField("[Select]", getTranslatedWord("Select"), True, -1, "CHECKBOX", Nothing)
            oGridN.doAddListField("Name", getTranslatedWord("Name"), False, -1, Nothing, Nothing)
            oGridN.doAddListField("Remarks", getTranslatedWord("Description"), False, 100, Nothing, Nothing)

        End Sub
        Public Overrides Sub doBeforeLoadData(oForm As SAPbouiCOM.Form)
            ' 
            'Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            'If oGridN Is Nothing Then
            '    oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
            'End If

            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New FilterGrid(Me, oForm, "LINESGRID", False)
            End If

            oGridN.AddEditLine = False
            'doSetFilterFields(oGridN)
            doSetGridOptions(oGridN)
            'oGridN.doAddGridTable(New GridTable(getTable(), Nothing, "Code", True, True), True)
            oGridN.doSetDoCount(True)
            'oGridN.doSetHistory(getUserTable(), "Code")

        End Sub
        Public Overrides Sub doFinalizeShow(oForm As SAPbouiCOM.Form)
            oForm.Items.Item("LINESGRID").Height = oForm.Height - 80
            oForm.Items.Item("LINESGRID").Top = 5

            MyBase.doFinalizeShow(oForm)
            oForm.SupportedModes = SAPbouiCOM.BoFormMode.fm_OK_MODE
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            For i As Int32 = 0 To oGridN.Columns.Count - 1
                If oGridN.Columns.Item(i).AffectsFormMode Then
                    oGridN.Columns.Item(i).AffectsFormMode = False
                End If
            Next

        End Sub
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            Me.setParentSharedData(oForm, "IDH_BRANCHOPEND", "LOADED")
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            oGridN.doReloadData()
            oGridN.doEnableColumn("[Select]", True)
            Me.setParentSharedData(oForm, "IDH_BRANCSELCD", "")
            Me.setParentSharedData(oForm, "IDH_BRANCSELNM", "")
        End Sub

        'Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
        '    Me.setParentSharedData(oForm, "IDH_BRANCHOPEND", "LOADED")
        '    'oGridN.Columns.Item("1").Editable = True
        '    MyBase.doLoadData(oForm)
        'End Sub

        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            '  doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

        Public Overrides Sub doButtonID1(oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction Then
                Dim sCodes As String = ""
                Dim sDesc As String = ""
                Dim bAll As Boolean = False
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                For i As Int32 = 0 To oGridN.getRowCount - 1
                    If oGridN.doGetFieldValue("[Select]", i) IsNot Nothing AndAlso oGridN.doGetFieldValue("[Select]", i).ToString = "Y" Then
                        If CType(oGridN.doGetFieldValue("Code", i), String) = "" Then
                            bAll = True
                        End If
                        sCodes &= CType(oGridN.doGetFieldValue("Code", i), String) & ", "
                        sDesc &= CType(oGridN.doGetFieldValue("Remarks", i), String) & ", "
                    End If
                Next
                If bAll Then
                    sCodes = ""
                Else
                    sCodes = sCodes.Trim.TrimEnd(","c)
                End If
                sDesc = sDesc.Trim.TrimEnd(","c)
                Me.setParentSharedData(oForm, "IDH_BRANCSELCD", sCodes)
                Me.setParentSharedData(oForm, "IDH_BRANCSELNM", sDesc)
            End If
            MyBase.doButtonID1(oForm, pVal, BubbleEvent)
        End Sub

        Public Overrides Function doItemEvent(oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD OrElse _
                    (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.setParentSharedData(oForm, "IDH_BRANCHOPEND", "")
                    MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                End If
            Else
                Dim s As String = pVal.ItemUID
            End If
            Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
        End Function

    End Class
End Namespace