Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System
Imports com.idh.utils
Imports com.idh.bridge.lookups

Namespace idh.forms.search
    Public Class WasteProfile
        Inherits IDHAddOns.idh.forms.Search

#Region "Initialization"
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHWPRS", "Waste Profile Search.srf", 5, 40, 530, 360, "Waste Profile Search")
        End Sub

        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE)
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@ISB_WSTPROFILE", "wp", Nothing, False, True))
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OITM", "i", Nothing, False, True))
            Dim sRequired As String = String.Empty

            Dim sCardCode As String = getParentSharedDataAsString(oGridN.getSBOForm(), "IDH_CUST")
            Dim sCardName As String = getParentSharedDataAsString(oGridN.getSBOForm(), "IDH_CUSTNM")
            Dim sAddress As String = getParentSharedDataAsString(oGridN.getSBOForm(), "IDH_ADDR")
            'sAddress = Formatter.doFormatStringForSQL(sAddress)

            'Dim sCardCode As String = getParentSharedData(oGridN.getSBOForm(), "IDH_CUST").ToString()
            'Dim sAddress As String = getParentSharedData(oGridN.getSBOForm(), "IDH_ADDR").ToString()

            sRequired = sRequired + If(sRequired.Length > 0, " AND wp.U_ItemCd = i.ItemCode ", " wp.U_ItemCd = i.ItemCode ")

            If (Not sCardCode Is Nothing AndAlso sCardCode.Length > 0) Then
                sRequired = sRequired + If(sRequired.Length > 0, " AND wp.U_CardCd = '" & sCardCode & "' ", " wp.U_CardCd = '" & sCardCode & "' ")
            End If

            If (Not sAddress Is Nothing AndAlso sAddress.Length > 0) Then
                sRequired = sRequired + If(sRequired.Length > 0, " AND wp.U_AddCd = '" & Formatter.doFormatStringForSQL(sAddress) & "' ", " wp.U_AddCd = '" & Formatter.doFormatStringForSQL(sAddress) & "' ")
            End If

            sRequired = sRequired + If(sRequired.Length > 0, " AND i.FrozenFor <> 'Y' ", " i.FrozenFor <> 'Y' ")
            'sRequired = sRequired + Config.INSTANCE.getActiveCheckSQL("wp", DateTime.Now)
            oGridN.setRequiredFilter(sRequired)

            oGridN.doAddFilterField("IDH_WPCD", "wp.U_ItemCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_WPNM", "wp.U_ItemNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100)
            oGridN.doAddFilterField("IDH_ADDCD", "wp.U_AddCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100)
            'oGridN.doAddFilterField("IDH_TFSNO", "wp.U_TFSNo", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            oGridN.doAddListField("wp.U_ProfileNo", "Profile No.", False, 0, Nothing, "PROFNO")
            '## Start 02-08-2013
            oGridN.doAddListField("wp.U_ItemCd", "Item Code", False, 40, Nothing, "DETAIL", -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("wp.U_ItemNm", "Item Name", False, 80, Nothing, "BPNAME")
            oGridN.doAddListField("wp.U_AddCd", "Address", False, 40, Nothing, "ADDR")
            'oGridN.doAddListField("i.frozenFor", "Frozen", False, 10, Nothing, "FROZEN")
            Dim sFrozenSQL As String = Config.INSTANCE.getActiveCheckSQL("i", DateTime.Now).Trim
            If sFrozenSQL.ToUpper.StartsWith("AND") Then
                sFrozenSQL = sFrozenSQL.Substring(sFrozenSQL.ToUpper.IndexOf("AND") + 3)
            End If
            oGridN.doAddListField("(CASE when " & sFrozenSQL & " Then 'N' else 'Y' end)", "Frozen", False, 10, Nothing, "FROZEN")
            '## End

    
        End Sub

#End Region

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            'Clear the global values
            IDHAddOns.idh.addon.Base.PARENT.setGlobalValue("WPS_CUST", Nothing)
            IDHAddOns.idh.addon.Base.PARENT.setGlobalValue("WPS_CUSTNM", Nothing)
            IDHAddOns.idh.addon.Base.PARENT.setGlobalValue("WPS_ADDR", Nothing)

            'setSharedData(oForm, "WPIT_ITEMCODE", sItemCode)
            'setSharedData(oForm, "WPIT_ITEMNAME", sItemName)
            IDHAddOns.idh.addon.Base.PARENT.setGlobalValue("WPITM", Nothing)
        End Sub

#Region "Loading Data"
        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)

            'Set the global values
            IDHAddOns.idh.addon.Base.PARENT.setGlobalValue("WPS_CUST", getParentSharedData(oForm, "IDH_CUST"))
            IDHAddOns.idh.addon.Base.PARENT.setGlobalValue("WPS_CUSTNM", getParentSharedData(oForm, "IDH_CUSTNM"))
            IDHAddOns.idh.addon.Base.PARENT.setGlobalValue("WPS_ADDR", getParentSharedData(oForm, "IDH_ADDR"))

            'setSharedData(oForm, "WPIT_ITEMCODE", sItemCode)
            'setSharedData(oForm, "WPIT_ITEMNAME", sItemName)
            IDHAddOns.idh.addon.Base.PARENT.setGlobalValue("WPITM", "TRUE")

            '    '        	doFillBPTypes(oForm)
            '    '            doFillGroups(oForm)
            '    '            doFillBranches(oForm)
        End Sub

        'Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
        '    'doFillGroups(oForm)
        '    'doFillBranches(oForm)

        '    MyBase.doReadInputParams(oForm)
        '    'Dim sVal As String = getParentSharedData(oForm, "IDH_TYPE")
        '    'If Not sVal Is Nothing Then
        '    '    If sVal.StartsWith("F-") Then
        '    '        sVal = sVal.Substring(2)
        '    '        doFillBPTypes(oForm)
        '    '        setEnableItem(oForm, False, "IDH_TYPE")
        '    '    Else
        '    '        doFillBPTypes(oForm, sVal)
        '    '        If sVal.Length > 1 Then
        '    '            sVal = sVal.Substring(0, 1)
        '    '        End If
        '    '        setEnableItem(oForm, True, "IDH_TYPE")
        '    '    End If
        '    '    setUFValue(oForm, "IDH_TYPE", sVal)
        '    'Else
        '    '    doFillBPTypes(oForm)
        '    'End If

        'End Sub

#End Region

#Region "Unloading Data"

#End Region

#Region "Event Handlers"
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_NEW" Then
                        'setSharedData(oForm, "ACT", "ADD")
                        'setSharedData(oForm, "TYPE", "C")
                        'goParent.doOpenModalForm("IDH_TFSANX", oForm)
                        'Dim sWPITMCollection() As String = {getSharedData(oForm, "IDH_CUST"), getSharedData(oForm, "IDH_ADDR"), "-", "-", "TRUE"}
                        setSharedData(oForm, "WPS_CUST", getParentSharedData(oForm, "IDH_CUST"))
                        setSharedData(oForm, "WPS_CUSTNM", getParentSharedData(oForm, "IDH_CUSTNM"))
                        setSharedData(oForm, "WPS_ADDR", getParentSharedData(oForm, "IDH_ADDR"))
                        setSharedData(oForm, "WPITM", "TRUE")
                        Dim sMsg As String = "Create new waste profile from...?"
                        Dim iSelection As Integer = goParent.doMessage(sMsg, 1, "Template", "Blank Item", "Cancel", False)
                        If iSelection = 1 Then
                            'Open Item Search with Waste Profile Template checkbox checked 
                            goParent.doOpenModalForm("IDHWPTISRC", oForm)
                        ElseIf iSelection = 2 Then
                            goParent.doOpenModalForm("150", oForm)
                            doGetForm(oForm.UniqueID)
                        End If
                    ElseIf pVal.ItemUID = "IDH_CPYWP" Then
                        Dim oUserName As String = goParent.gsUserName
                        Dim oUserListCopyWP As String = Config.ParameterWithDefault("USCPYWPU", "")
                        Try
                            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                            Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows
                            Dim iSelectionCount As Integer = oSelected.Count

                            If iSelectionCount > 0 Then
                                If oUserListCopyWP.Contains(oUserName.Trim()) Then
                                    Dim iRow As Integer
                                    iRow = oSelected.Item(0, SAPbouiCOM.BoOrderType.ot_RowOrder)

                                    Dim sItmCd As String = CType(oGridN.doGetFieldValue("wp.U_ItemCd", iRow), String)
                                    Dim sItmNm As String = CType(oGridN.doGetFieldValue("wp.U_ItemNm", iRow), String)
                                    setSharedData(oForm, "SELROWNM", iRow)
                                    setSharedData(oForm, "SELITMCD", sItmCd)
                                    setSharedData(oForm, "SELITMNM", sItmNm)

                                    setSharedData(oForm, "CPYTRG", "S")
                                    setSharedData(oForm, "IDH_ADRTYP", "S")
                                    goParent.doOpenModalForm("IDHASRCH2", oForm)
                                End If
                            Else
                                doWarnMess("Select a row to use this option.", SAPbouiCOM.BoMessageTime.bmt_Long)
                            End If

                        Catch ex As Exception
                        End Try
                    ElseIf pVal.ItemUID = "IDH_DELWP" Then
                        Try
                            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                            Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows
                            Dim iSelectionCount As Integer = oSelected.Count

                            If iSelectionCount > 0 Then
                                Dim iRow As Integer
                                iRow = oSelected.Item(0, SAPbouiCOM.BoOrderType.ot_RowOrder)

                                Dim sWPCode As String = CType(oGridN.doGetFieldValue("wp.U_ProfileNo", iRow), String)
                                Dim sDelQry As String = "DELETE [@ISB_WSTPROFILE] WHERE Code = " + sWPCode
                                If goParent.goDB.doUpdateQuery(sDelQry) Then
                                    doWarnMess("Waste Profile deleted successfully.", SAPbouiCOM.BoMessageTime.bmt_Long)
                                    oGridN.doReloadData()
                                End If
                            Else
                                doWarnMess("Select a row to use this option.", SAPbouiCOM.BoMessageTime.bmt_Long)
                            End If
                        Catch ex As Exception
                        End Try
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE Then
                If pVal.BeforeAction = False Then
                    'Refreshing the grid to get latest from WP Profiles table 
                    Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                    If oGridN Is Nothing Then
                        oGridN = New FilterGrid(Me, oForm, "LINESGRID", True)
                    End If
                    oGridN.doReloadData()
                End If
            End If
            Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType = "IDHWPTISRC" Then
                    Dim sItemCode As String = getSharedDataAsString(oForm, "ITEMCODE")
                    Dim sItemName As String = getSharedDataAsString(oForm, "ITEMNAME")

                    Dim oParentForm As SAPbouiCOM.Form = goParent.doGetParentForm(oForm.UniqueID)

                    If sItemCode.Length > 0 Then
                        setSharedData(oForm, "WPS_CUST", getSharedData(oForm, "WPS_CUST"))
                        setSharedData(oForm, "WPS_CUSTNM", getSharedData(oForm, "WPS_CUSTNM"))
                        setSharedData(oForm, "WPS_ADDR", getSharedData(oForm, "WPS_ADDR"))

                        setSharedData(oForm, "WPIT_ITEMCODE", sItemCode)
                        setSharedData(oForm, "WPIT_ITEMNAME", sItemName)
                        setSharedData(oForm, "WPITM", "TRUE")
                        Try
                            goParent.doOpenModalForm("150", oForm)
                        Catch ex As Exception
                            'com.idh.bridge.DataHandler.INSTANCE.doError("Exception loading Item Master Form: " + ex.ToString, "Error loading the form.")
                            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXELF", {Nothing})
                        End Try
                    End If
                ElseIf sModalFormType = "150" Then
                    'refresh the grid to get waste profile details 
                    Dim sNewitemCode As String = getSharedDataAsString(oForm, "WPIT_ITEMCODE")
                ElseIf sModalFormType = "IDHASRCH2" Then
                    Dim sCpyTarget As String = getSharedDataAsString(oForm, "CPYTRG")
                    Dim sCardCode As String = getSharedDataAsString(oForm, "CARDCODE")
                    Dim sCardName As String = getSharedDataAsString(oForm, "CARDNAME")
                    Dim sAddress As String = getSharedDataAsString(oForm, "ADDRESS")
                    Dim sAddType As String = getSharedDataAsString(oForm, "ADRESTYPE")
                    Dim sItemCode As String = getSharedDataAsString(oForm, "SELITMCD")
                    Dim sItemName As String = getSharedDataAsString(oForm, "SELITMNM")
                    Dim iRow As Integer = CInt(getSharedData(oForm, "SELROWNM"))

                    Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                    oGridN.setCurrentDataRowIndex(iRow)

                    Dim sNewItemCode As String = com.idh.bridge.lookups.Config.INSTANCE.doGetNextItemCode(sItemCode, com.idh.bridge.lookups.Config.ParameterWithDefault("ITMDPPOS", "_P"))
                    Dim sNewItemName As String = sItemName + com.idh.bridge.lookups.Config.ParameterWithDefault("ITMDPPOS", "_P").ToString()

                    Dim sMsg As String = "You are about to copy above selected Waste Profile to the following:"
                    sMsg = sMsg + Chr(13) + "Selected Waste Profile: " + sItemCode + ": " + sItemName
                    sMsg = sMsg + Chr(13)
                    sMsg = sMsg + Chr(13) + "   NEW Waste Profile: " + sNewItemCode + ": " + sNewItemName
                    sMsg = sMsg + Chr(13) + "   BP Code: " + sCardCode
                    sMsg = sMsg + Chr(13) + "   BP Name: " + sCardName
                    sMsg = sMsg + Chr(13) + "   Address: " + sAddress
                    sMsg = sMsg + Chr(13)
                    sMsg = sMsg + Chr(13) + "Are you sure you want to proceed?"

                    Dim iSelection As Integer = goParent.doMessage(sMsg, 1, " Yes ", " No ", Nothing, False)
                    If iSelection = 1 Then
                        Try
                            setSharedData(oForm, "WPS_CUST", sCardCode)
                            setSharedData(oForm, "WPS_CUSTNM", sCardName)
                            setSharedData(oForm, "WPS_ADDR", sAddress)

                            setSharedData(oForm, "WPIT_ITEMCODE", sItemCode)
                            setSharedData(oForm, "WPIT_ITEMNAME", sItemName)
                            setSharedData(oForm, "WPITM", "TRUE")

                            goParent.doOpenModalForm("150", oForm)

                            ''Firstly insert the new waste profile item 
                            'Dim oItem As SAPbobsCOM.Items
                            'Dim oNewItem As SAPbobsCOM.Items

                            'oItem = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems)
                            'oItem.GetByKey(sItemCode)
                            ''oItem.GetAsXML(sItemCode)

                            'oNewItem = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems)
                            ''oNewItem = oItem

                            'oNewItem.ItemCode = sNewItemCode
                            'oNewItem.ItemName = sNewItemName



                            'Dim iResult As Integer = oNewItem.Add()
                            'Dim sResult As String = Nothing
                            'If iResult <> 0 Then
                            '    goParent.goDICompany.GetLastError(iResult, sResult)
                            '    com.idh.bridge.DataHandler.INSTANCE.doError("Copy Item - " & sResult, "Error creating copy of Item.")
                            'End If

                            ''Insert record in [@ISB_WSTPROFILE] table 
                            'Dim sWPCode As String = goParent.goDB.doNextNumber("WPROS").ToString()
                            'Dim sQry As String
                            'sQry = "INSERT INTO [@ISB_WSTPROFILE]([Code], [Name], [U_ProfileNo] ,[U_Descp] , [U_AddCd] ,[U_CardCd] ,[U_CardNm] ,[U_ItemCd] ,[U_ItemNm])" & _
                            '        "VALUES('" + sWPCode + "', '" + sWPCode + "' ,'" + sWPCode + "' ,'" + sWPCode + "' ,'" + sAddress + "' ,'" + sCardCode + "' ,'" + sCardName + "' ,'" + sNewItemCode + "' ,'" + sNewItemName + "' )"
                            'goParent.goDB.doUpdateQuery(sQry)
                            ''doClearSharedData(oForm)
                            ''doClearParentSharedData(oForm)
                        Catch ex As Exception
                            'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error copying waste profile across BP's")
                            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXBPW", {Nothing})
                        End Try

                        doWarnMess("WP copied across successfully", SAPbouiCOM.BoMessageTime.bmt_Long)
                    End If

                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal result - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub
        '## Start 08-08-2013
        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doFinalizeShow(oForm)
            If oForm.Visible Then
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                If oGridN Is Nothing Then
                    oGridN = New FilterGrid(Me, oForm, "LINESGRID", True)
                End If
                oGridN.doReloadData()
            End If
        End Sub
        '## End
#End Region

#Region "Custom Functions"
        'Private Sub doFillBranches(ByVal oForm As SAPbouiCOM.Form)
        '    doFillCombo(oForm, "IDH_BRANCH", "OUBR", "Code", "Remarks", Nothing, "Code", True)
        'End Sub

        'Private Sub doFillGroups(ByVal oForm As SAPbouiCOM.Form)
        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '    Try
        '        Dim oGroups As SAPbouiCOM.ComboBox
        '        Dim oItem As SAPbouiCOM.Item
        '        Dim sBPType As String = oForm.DataSources.UserDataSources.Item("IDH_TYPE").ValueEx

        '        oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        '        Dim sQry As String = "select GroupCode, GroupName from OCRG where GroupType Like '%" & sBPType & "'"
        '        oRecordSet.DoQuery(sQry)
        '        If oRecordSet.RecordCount > 0 Then
        '            oItem = oForm.Items.Item("IDH_GROUP")
        '            oItem.DisplayDesc = True
        '            oGroups = oItem.Specific
        '            Dim iCount As Integer
        '            Dim oVal1 As String
        '            Dim oVal2 As String

        '            Dim oValValues As SAPbouiCOM.ValidValues
        '            oValValues = oGroups.ValidValues
        '            'First Clear all the values
        '            If Not oValValues Is Nothing Then
        '                While oValValues.Count > 0
        '                    oValValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
        '                End While
        '            End If

        '            oValValues.Add("", getTranslatedWord("Any"))
        '            For iCount = 0 To oRecordSet.RecordCount - 1
        '                oVal1 = oRecordSet.Fields.Item(0).Value
        '                oVal2 = oRecordSet.Fields.Item(1).Value
        '                oValValues.Add(oVal1, oVal2)
        '                oRecordSet.MoveNext()
        '            Next
        '        End If
        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Groups.")
        '    Finally
        '        IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
        '    End Try
        'End Sub

        ''* A Any
        ''* S Supplier
        ''* C Customer
        ''* L Lead
        'Private Sub doFillBPTypes(ByVal oForm As SAPbouiCOM.Form, Optional ByVal sSwitch As String = "ASCL")
        '    Dim oBPType As SAPbouiCOM.ComboBox
        '    Dim oItem As SAPbouiCOM.Item

        '    oItem = oForm.Items.Item("IDH_TYPE")
        '    oItem.DisplayDesc = True
        '    oBPType = oItem.Specific

        '    If oBPType.ValidValues.Count = 0 Then
        '        If sSwitch.Length = 0 Then
        '            oBPType.ValidValues.Add("", getTranslatedWord("Any"))
        '            oBPType.ValidValues.Add("S", getTranslatedWord("Vendor"))
        '            oBPType.ValidValues.Add("C", getTranslatedWord("Customer"))
        '            oBPType.ValidValues.Add("L", getTranslatedWord("Lead"))
        '        Else
        '            If sSwitch.IndexOf("A") > -1 Then
        '                oBPType.ValidValues.Add("", getTranslatedWord("Any"))
        '            End If
        '            If sSwitch.IndexOf("S") > -1 Then
        '                oBPType.ValidValues.Add("S", getTranslatedWord("Vendor"))
        '            End If
        '            If sSwitch.IndexOf("C") > -1 Then
        '                oBPType.ValidValues.Add("C", getTranslatedWord("Customer"))
        '            End If
        '            If sSwitch.IndexOf("L") > -1 Then
        '                oBPType.ValidValues.Add("L", getTranslatedWord("Lead"))
        '            End If
        '        End If
        '    End If
        'End Sub

        'Public Shared Function doGetBPInfo(ByVal oParent As IDHAddOns.idh.addon.Base, ByRef oCallerForm As SAPbouiCOM.Form, ByVal sCardCode As String) As Boolean
        '    Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
        '    Try
        '        oBP = oParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
        '        If oBP.GetByKey(sCardCode) Then
        '            setSharedData(oCallerForm, "CARDCODE", oBP.CardCode)
        '            setSharedData(oCallerForm, "CARDNAME", oBP.CardName)
        '            setSharedData(oCallerForm, "CARDTYPE", oBP.CardType)
        '            setSharedData(oCallerForm, "GROUPCODE", oBP.GroupCode)
        '            setSharedData(oCallerForm, "GROUPNAME", "")
        '            setSharedData(oCallerForm, "BALANCEFC", oBP.CurrentAccountBalance)
        '            setSharedData(oCallerForm, "CREDITLINE", oBP.CreditLimit)
        '            setSharedData(oCallerForm, "PHONE1", oBP.Phone1)
        '            setSharedData(oCallerForm, "CNTCTPRSN", oBP.ContactPerson)
        '            setSharedData(oCallerForm, "WASLIC", oBP.UserFields.Fields.Item("U_WASLIC").Value)
        '            Return True
        '        End If
        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error getting the BP Info - " & sCardCode)
        '    Finally
        '        IDHAddOns.idh.data.Base.doReleaseObject(oBP)
        '    End Try
        '    Return False
        'End Function

#End Region

    End Class
End Namespace
