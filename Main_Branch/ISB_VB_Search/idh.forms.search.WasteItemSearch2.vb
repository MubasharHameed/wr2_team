Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.bridge
Imports com.idh.bridge.lookups

Imports com.idh.dbObjects.User

Namespace idh.forms.search
    Public Class WasteItemSearch2
        Inherits IDHAddOns.idh.forms.Search

        Private FormLoaded As Boolean = False
        Private oMatrix As SAPbouiCOM.Matrix
        Private _sSQL As String = ""
        Private iLastselectedRow As Int32 = 0
        Private slastSearchedResult As String = ""
        Private bClosingFormByOKButton As Boolean = False
        Private WithEvents timer_bp As System.Timers.Timer
        Private timer_Form As SAPbouiCOM.Form
        Private timer_ColUID, timer_ItemUID As String

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHWISRC", "WasteItem Search2.srf", 5, 70, 603, 320, "Waste Item Search L-2")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid) 
        End Sub

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            bClosingFormByOKButton = False
            Me.setParentSharedData(oForm, "IDH_ITMSRCHOPEND", "LOADED")
            _sSQL = ""
            iLastselectedRow = 0
            slastSearchedResult = ""
            ''IDH_ITMCOD
            'setItemValue(oForm, "IDH_ITMNAM", getParentSharedData(oForm, "IDH_ITMNAM"))
            'setItemValue(oForm, "IDH_GRPCOD", getParentSharedData(oForm, "IDH_GRPCOD"))
            ''IDH_GRPNAM
            'setItemValue(oForm, "IDH_INVENT", getParentSharedData(oForm, "IDH_INVENT"))
            'setItemValue(oForm, "IDH_JOBTP", getParentSharedData(oForm, "IDH_JOBTP"))
            'setItemValue(oForm, "IDH_WSTGRP", getParentSharedData(oForm, "IDH_WSTGRP"))

            '
            setUFValue(oForm, "IDH_ITMCOD", getParentSharedData(oForm, "IDH_ITMCOD"))
            setUFValue(oForm, "IDH_ITMNAM", getParentSharedData(oForm, "IDH_ITMNAM"))
            setUFValue(oForm, "IDH_GRPCOD", getParentSharedData(oForm, "IDH_GRPCOD"))
            'IDH_GRPNAM
            setUFValue(oForm, "IDH_INVENT", getParentSharedData(oForm, "IDH_INVENT"))
            setUFValue(oForm, "IDH_JOBTP", getParentSharedData(oForm, "IDH_JOBTP"))
            setUFValue(oForm, "IDH_WSTGRP", getParentSharedData(oForm, "IDH_WSTGRP"))

            bClosingFormByOKButton = False
            Me.setParentSharedData(oForm, "IDH_ITMSRCHOPEND", "LOADED")

            reQuerynReloadMatrix(oForm)
        End Sub

        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            Try
                oForm.Items.Item("LINESGRID").Visible = False
                Dim bFlag As Boolean = False
                Dim sSILENT As String = ""
                If (Me.getHasSharedData(oForm)) Then
                    sSILENT = Me.getParentSharedDataAsString(oForm, "SILENT") '.ToString
                    If (sSILENT <> Nothing) Then
                        If (sSILENT.ToUpper().Equals("SHOWMULTI")) Then
                            bFlag = True 'Means we have some value in CardCode/cardname
                        End If
                    End If
                End If
                If (bFlag AndAlso oMatrix.RowCount > 0) Then
                    oMatrix.SelectRow(1, True, False)
                    If (oMatrix.RowCount = 1) Then
                        Me.SetParentSharedData(1, oForm)
                        Me.doReturnFromModalShared(oForm, True)
                        Me.SetParentSharedData(oForm, "IDH_ITMSRCHOPEND", "")
                        Return
                    End If
                End If
                oForm.Visible = True
                Dim otxt As SAPbouiCOM.EditText = CType(oForm.Items.Item("IDH_ITMCOD").Specific, SAPbouiCOM.EditText)
                otxt.Active = True
                FormLoaded = True

                timer_bp = New System.Timers.Timer(700)
                timer_bp.Enabled = False
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Finalizing Show Form.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXELF", {Nothing})
            End Try
        End Sub

        Private Function PrepareSQL(ByVal oform As SAPbouiCOM.Form, Optional ByVal sOrderbyCol As String = "") As String
            iLastselectedRow = 0
            slastSearchedResult = ""
            Dim sql As String = " Select i.ItmsGrpCod,ig.ItmsGrpNam,i.ItemCode, i.ItemName,i.SalUnitMsr, i.BuyUnitMsr, i.InvntItem, i.U_WTDFW,  " _
                                   & " i.U_WTUDW, i.U_IDHCWBB, i.U_IDHCREB, i.U_HAZCD " _
                                   & "     From OITM i , OITB ig Where i.ItmsGrpCod=ig.ItmsGrpCod " _
                                   & "And ((cast((case when ( (i.FrozenFor = 'Y' and (GetDate() Between isnull(i.FrozenFrom, DateAdd(d,-1,GetDate())) and isnull(i.FrozenTo, DateAdd(d,1,GetDate()))))) then 1 else 0 end) as bit)  " _
                                   & " ) | " _
                                   & " (cast((case when ( (i.validFor = 'Y' and NOT(GetDate() Between isnull(i.validFrom, DateAdd(d,-1,GetDate())) and isnull(i.validto, DateAdd(d,1,GetDate()))))) then 1 else 0 end) as bit)  " _
                                   & " ))=0 "

            PrepareSQL = sql

            Dim sItemCode As String = getItemValue(oform, "IDH_ITMCOD")

            Dim sItemName As String = getItemValue(oform, "IDH_ITMNAM")

            Dim sGroupCode As String = getItemValue(oform, "IDH_GRPCOD")

            Dim sGroupName As String = getItemValue(oform, "IDH_GRPNAM")

            Dim sStock As String = getItemValue(oform, "IDH_INVENT") 'otextBox.Value.Trim

            Dim sJobTypeGroup As String = getItemValue(oform, "IDH_JOBTP") 'otextBox.Value.Trim

            Dim sWasteGroup As String = getItemValue(oform, "IDH_WSTGRP") 'otextBox.Value.Trim

            If sWasteGroup.Length > 0 Then
                sWasteGroup = "'" & sWasteGroup & "'"
            ElseIf sJobTypeGroup.Length > 0 Then
                Dim oRecordSet As SAPbobsCOM.Recordset
                Dim sQry As String = "SELECT U_WstGpCd FROM [@IDH_WGRPJLK] WHERE U_JobTp = '" & sJobTypeGroup & "'"
                oRecordSet = goParent.goDB.doSelectQuery(sQry)

                Dim iRecords As Integer = oRecordSet.RecordCount
                If iRecords > 0 Then
                    While Not oRecordSet.EoF
                        If sWasteGroup.Length > 0 Then
                            sWasteGroup = sWasteGroup & ","
                        End If
                        sWasteGroup = "'" & CType(oRecordSet.Fields.Item(0).Value(), String) & "'"
                        oRecordSet.MoveNext()
                    End While
                End If
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End If

            If sWasteGroup.Length > 0 Then
                sql = sql & " AND i.ItemCode In ( SELECT wg.U_ItemCd FROM [@IDH_WGPCNTY] wg, [@IDH_WGRPJLK] wlj WHERE wg.U_WstGpCd = wlj.U_WstGpCd AND wlj.U_WstGpCd In (" & sWasteGroup & "))"
            End If


            Dim sWhereSql As String = ""
            If sItemCode <> "" Then
                sWhereSql &= " and i.ItemCode LIKE '" & sItemCode.Replace("*", "%") & "%' "
            End If

            If sItemName <> "" Then
                sWhereSql &= " and i.ItemName Like '" & sItemName.Replace("*", "%") & "%' "
            End If

            'If sGroupCode <> "" Then
            '    sWhereSql &= " and i.ItmsGrpCod Like '" & sGroupCode.Replace("*", "%") & "' "
            'End If
            If sGroupCode <> "" AndAlso sGroupCode.IndexOf(",") = -1 Then
                sWhereSql &= " and i.ItmsGrpCod Like '" & sGroupCode.Replace("*", "%") & "' "
            ElseIf sGroupCode <> "" AndAlso sGroupCode.IndexOf(",") > -1 Then
                sWhereSql &= " and i.ItmsGrpCod In (" & sGroupCode & ") "
            End If

            If sGroupName <> "" Then
                sWhereSql &= " and ig.ItmsGrpNam LIKE '" & sGroupName.Replace("*", "%") & "%' "
            End If

            If sStock <> "" Then
                sWhereSql &= " and i.InvntItem LIKE '" & sStock & "' "
            End If

            'If sCity <> "" Then
            '    sWhereSql &= " and ba.City LIKE '" & sCity.Replace("*", "%") & "%' "
            'End If

            'If sZipCode <> "" Then
            '    sWhereSql &= " and ba.ZipCode LIKE '" & sZipCode.Replace("*", "%") & "%' "
            'End If

            'If sState <> "" Then
            '    sWhereSql &= " and ba.State LIKE '" & sState.Replace("*", "%") & "%' "
            'End If


            'If sSiteTel <> "" Then
            '    sWhereSql &= " and ba.U_TEL1 LIKE '" & sSiteTel.Replace("*", "%") & "%' "
            'End If
            If sWhereSql.Trim <> "" Then
                sql &= sWhereSql
            End If
            If sOrderbyCol <> "" Then
                sql &= " Order by " & sOrderbyCol
            End If
            Return sql
        End Function

        Private Sub reQuerynReloadMatrix(ByVal oform As SAPbouiCOM.Form, Optional ByVal sOrderByCol As String = "")
            Try
                iLastselectedRow = 0
                slastSearchedResult = ""
                Dim sSQL As String = PrepareSQL(oform, sOrderByCol).Trim
                If (sSQL.Trim.ToLower.Equals(_sSQL.ToLower)) AndAlso _sSQL <> "" Then
                    Return
                End If
                _sSQL = sSQL
                oMatrix = CType(oform.Items.Item("LINESMTX").Specific, SAPbouiCOM.Matrix)
                oform.DataSources.DataTables.Item("LINESMTX").ExecuteQuery(sSQL)
                ' oform.Freeze(True)
                oMatrix.Clear()
                oMatrix.LoadFromDataSourceEx(False)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error in Quering db.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", {_sSQL})
            Finally
                '  oform.Freeze(False)
            End Try

        End Sub

        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_GOT_FOCUS)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            '   doPopulateWasteClass(oForm)
            doPopulateInventory(oForm)
            doJobTypeCombo(oForm)
            doWasteGroupCombo(oForm)

            'setUFValue(oForm, "IDH_JOBTP", getParentSharedData(oForm, "IDH_JOBTP"))
            'setUFValue(oForm, "IDH_WSTGRP", getParentSharedData(oForm, "IDH_WSTGRP"))

            'Dim sVal As String = getParentSharedData(oForm, "IDH_INVENT")
            'setUFValue(oForm, "IDH_INVENT", sVal)
            MyBase.doBeforeLoadData(oForm)
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub
        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)

        End Sub
        
        Private Function GetselectedRowIndex() As Int32
            Dim i As Int32 = 0
            i = oMatrix.GetNextSelectedRow(0, SAPbouiCOM.BoOrderType.ot_SelectionOrder)
            Return i
        End Function

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            ''## Start 05-07-2013
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD OrElse _
                    (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.SetParentSharedData(oForm, "IDH_ITMSRCHOPEND", "")
                    If bClosingFormByOKButton = False Then
                        MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                    End If
                End If
            End If
            If pVal.BeforeAction = False Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_FORM_CLOSE
                        Me.SetParentSharedData(oForm, "IDH_ITMSRCHOPEND", "")
                        If bClosingFormByOKButton = False Then
                            MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                        Dim sItem As String = pVal.ItemUID
                       If sItem = "IDH_JOBTP" Then
                            Dim sValue As String = CType(getUFValue(oForm, "IDH_JOBTP", True), String)
                            If sValue.Length > 0 Then
                                setUFValue(oForm, "IDH_WSTGRP", "", True)
                            End If
                            reQuerynReloadMatrix(oForm, "i.ItemCode")
                            BubbleEvent = False
                        ElseIf sItem = "IDH_WSTGRP" Then
                            Dim sValue As String = CType(getUFValue(oForm, "IDH_WSTGRP", True), String)
                            If sValue.Length > 0 Then
                                setUFValue(oForm, "IDH_JOBTP", "", True)
                            End If
                            reQuerynReloadMatrix(oForm, "i.ItemCode")
                            BubbleEvent = False
                        ElseIf sItem = "IDH_INVENT" Then
                            'Dim sValue As String = getUFValue(oForm, "IDH_INVENT", True)
                            'If sValue.Length > 0 Then
                            '    setUFValue(oForm, "IDH_JOBTP", "", True)
                            'End If
                            reQuerynReloadMatrix(oForm, "i.ItemCode")
                            BubbleEvent = False
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                        Dim sOrdrebyCol As String = "i.ItemCode"
                        If (pVal.ItemUID = "IDH_GRPCOD" OrElse pVal.ItemUID = "IDH_GRPNAM") Then
                            reQuerynReloadMatrix(oForm, sOrdrebyCol)
                        End If
                        BubbleEvent = False
                    Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS 'SAPbouiCOM.BoEventTypes.et_VALIDATE
                        Dim sOrdrebyCol As String = ""
                        If (pVal.ItemUID = "IDH_ITMCOD" OrElse pVal.ItemUID = "IDH_ITMNAM" OrElse pVal.ItemUID = "IDH_GRPCOD" OrElse pVal.ItemUID = "IDH_GRPNAM") Then
                            Select Case pVal.ItemUID
                                Case "IDH_ITMCOD"
                                    sOrdrebyCol = "i.ItemCode"
                                Case "IDH_ITMNAM"
                                    sOrdrebyCol = "i.ItemName"
                                Case Else
                                    sOrdrebyCol = "i.ItemCode"
                            End Select
                            reQuerynReloadMatrix(oForm, sOrdrebyCol)
                            BubbleEvent = False
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "LINESMTX" Then
                            If pVal.Row > 0 Then
                                oMatrix.SelectRow(pVal.Row, True, False)
                            End If
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                        'If pVal.BeforeAction = False Then
                        If pVal.ItemUID = "LINESMTX" Then
                            Me.setChoosenData(oForm, BubbleEvent)
                        End If
                        'End If
                    Case SAPbouiCOM.BoEventTypes.et_KEY_DOWN
                        If pVal.CharPressed <> 9 AndAlso pVal.CharPressed <> 38 AndAlso pVal.CharPressed <> 39 AndAlso pVal.CharPressed <> 40 AndAlso pVal.CharPressed <> 13 _
                             AndAlso (pVal.ItemUID = "IDH_ITMCOD" OrElse pVal.ItemUID = "IDH_ITMNAM" ) Then
                            Select Case pVal.ItemUID
                                Case "IDH_ITMCOD"
                                    timer_bp.Enabled = False
                                    timer_bp.Interval = 400
                                    timer_Form = oForm
                                    timer_ColUID = "ItemCode"
                                    timer_ItemUID = pVal.ItemUID
                                    If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
                                        iLastselectedRow = 0
                                        slastSearchedResult = ""
                                    End If
                                    timer_bp.Enabled = True
                                    'doSelectOnKeyPress("CardCode", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)
                                Case "IDH_ITMNAM"
                                    timer_bp.Enabled = False
                                    timer_bp.Interval = 400
                                    timer_Form = oForm
                                    timer_ColUID = "ItemName"
                                    timer_ItemUID = pVal.ItemUID
                                    If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
                                        iLastselectedRow = 0
                                        slastSearchedResult = ""
                                    End If
                                    timer_bp.Enabled = True
                                    'doSelectOnKeyPress("CardName", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)
                            End Select
                            BubbleEvent = False
                        End If

                        If Not (pVal.ItemUID = "IDH_TYPE" OrElse pVal.ItemUID = "IDH_GROUP" OrElse pVal.ItemUID = "IDH_BRANCH") Then
                            If (pVal.CharPressed = 40 OrElse pVal.CharPressed = 38 OrElse pVal.CharPressed = 34 OrElse pVal.CharPressed = 33) AndAlso (oMatrix.RowCount > 0) Then
                                handleKeyEventForMatrixRowSelection(pVal.CharPressed)
                                BubbleEvent = False
                            End If
                        End If
                End Select
            End If
            Return True
        End Function

#Region "Timer Delay"
        'Private timer_Form As SAPbouiCOM.Form
        'Private timer_ColUID, timer_ItemUID As String
        'Private Shared Sub OnTimedEvent(ByVal source As Object, ByVal e As Timers.ElapsedEventArgs)
        '    ' Console.WriteLine("The Elapsed event was raised at {0}", e.SignalTime)
        '    Dim timer As System.Timers.Timer = source

        '    doSelectOnKeyPress("CardCode", getItemValue(Me.MyForm_bpTimer, "IDH_BPCOD").Trim.ToLower)
        'End Sub

        Private Sub timer_bp_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles timer_bp.Elapsed
            timer_bp.Enabled = False
            doSelectOnKeyPressNew(timer_ColUID, getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
            'Select Case (timer_ColUID)
            '    Case "ItemCode"
            '        doSelectOnKeyPressNew("ItemCode", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
            '    Case "ItemName"
            '        doSelectOnKeyPressNew("ItemName", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
            '    Case "ItmsGrpCod"
            '        doSelectOnKeyPressNew("ItmsGrpCod", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
            '    Case "ItmsGrpNam"
            '        doSelectOnKeyPressNew("ItmsGrpNam", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
            'End Select

        End Sub
#End Region
        Private Sub doSelectOnKeyPressNew(ByVal sColUID As String, ByVal sValueToSearch As String)
            Dim iRowCount As Int32 = oMatrix.RowCount
            If sValueToSearch.Length = 0 Then
                iLastselectedRow = 0
                slastSearchedResult = ""
                Return
            ElseIf sValueToSearch.Length = 1 Then
                If slastSearchedResult <> "" AndAlso sValueToSearch.StartsWith(slastSearchedResult) Then
                    Return
                End If
                iLastselectedRow = 0
                slastSearchedResult = ""
                Dim iIndex As Int32 = SearchValue(sColUID, sValueToSearch)
                If oMatrix.RowCount >= iIndex Then
                    oMatrix.SelectRow(iIndex, True, False)
                    iLastselectedRow = iIndex
                    slastSearchedResult = sValueToSearch
                    oMatrix.SelectRow(iIndex, True, False)
                End If
            ElseIf sValueToSearch.Length > 1 Then
                iLastselectedRow = 0
                slastSearchedResult = ""
                Dim iIndex As Int32 = SearchValue(sColUID, sValueToSearch)
                If oMatrix.RowCount >= iIndex Then
                    oMatrix.SelectRow(iIndex, True, False)
                    iLastselectedRow = iIndex
                    slastSearchedResult = sValueToSearch
                    oMatrix.SelectRow(iIndex, True, False)
                End If
            End If

        End Sub

        Private Function midpoint(ByVal iMin As Int32, ByVal iMax As Int32) As Int32
            Return CType(Math.Floor((iMin + iMax) / 2.0), Integer)
        End Function

        Private Function SearchValue(ByVal sColUID As String, ByVal sValueToSearch As String) As Int32
            Try
                Dim iMin As Int32 = 1, iMax As Int32 = oMatrix.RowCount
                While (iMax >= iMin)
                    Dim imid As Int32 = midpoint(iMin, iMax)
                    If ((CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim().StartsWith(sValueToSearch)) Then
                        Return imid
                    ElseIf (CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim() < (sValueToSearch) OrElse _
                            String.Compare((CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim(), (sValueToSearch)) < 0 Then
                        iMin = imid + 1
                    ElseIf (CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim() > (sValueToSearch) OrElse _
                            String.Compare((CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim(), (sValueToSearch)) > 0 Then
                        iMax = imid - 1

                    End If
                End While
                Return 1
            Catch ex As Exception
                Return 1
            End Try
        End Function

        'Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
        '    MyBase.doCompleteCreate(oForm, BubbleEvent)
        '    'doAddUF(oForm, "IDH_CRDCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, True)
        '    'doAddUF(oForm, "IDH_LEV1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 10)
        '    'oForm.Items.Item("IDH_LEV1").AffectsFormMode = False

        '    doAddUF(oForm, "IDH_JOBTP", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, False, False)
        '    doAddUF(oForm, "IDH_WSTGRP", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, False, False)
        'End Sub



        
        '** The Initializer
        'Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
        '    Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")

        '    Dim sRequiredString As String = "i.ItmsGrpCod = ig.ItmsGrpCod AND i.FrozenFor <> 'Y' "
        '    Dim sWasteGroup As String = getUFValue(oForm, "IDH_WSTGRP", True)
        '    Dim sJobTypeGroup As String = getUFValue(oForm, "IDH_JOBTP", True)

        '    If sWasteGroup.Length > 0 Then
        '        sWasteGroup = "'" & sWasteGroup & "'"
        '    ElseIf sJobTypeGroup.Length > 0 Then
        '        Dim oRecordSet As SAPbobsCOM.Recordset
        '        Dim sQry As String = "SELECT U_WstGpCd FROM [@IDH_WGRPJLK] WHERE U_JobTp = '" & sJobTypeGroup & "'"
        '        oRecordSet = goParent.goDB.doSelectQuery(sQry)

        '        Dim iRecords As Integer = oRecordSet.RecordCount
        '        If iRecords > 0 Then
        '            While Not oRecordSet.EoF
        '                If sWasteGroup.Length > 0 Then
        '                    sWasteGroup = sWasteGroup & ","
        '                End If
        '                sWasteGroup = "'" & oRecordSet.Fields.Item(0).Value() & "'"
        '                oRecordSet.MoveNext()
        '            End While
        '        End If
        '        goParent.goDB.doReleaseObject(oRecordSet)
        '    End If

        '    If sWasteGroup.Length > 0 Then
        '        sRequiredString = sRequiredString & " AND i.ItemCode In ( SELECT wg.U_ItemCd FROM [@IDH_WGPCNTY] wg, [@IDH_WGRPJLK] wlj WHERE wg.U_WstGpCd = wlj.U_WstGpCd AND wlj.U_WstGpCd In (" & sWasteGroup & "))"
        '    End If

        '    oGridN.setRequiredFilter(sRequiredString)

        '    bClosingFormByOKButton = False
        '    Me.setParentSharedData(oForm, "IDH_ITMSRCHOPEND", "LOADED")
        '    MyBase.doLoadData(oForm)
        'End Sub

        Private Sub handleKeyEventForMatrixRowSelection(ByVal Keycode As Int32)
            If Keycode = 40 Then
                Dim row As Int32 = GetselectedRowIndex()
                If row < 1 Then
                    oMatrix.SelectRow(1, True, False)
                ElseIf row < oMatrix.RowCount Then
                    oMatrix.SelectRow(row + 1, True, False)
                End If
            ElseIf Keycode = 38 Then
                Dim row As Int32 = GetselectedRowIndex()
                If row <= 1 Then
                    oMatrix.SelectRow(1, True, False)
                ElseIf row <= oMatrix.RowCount Then
                    oMatrix.SelectRow(row - 1, True, False)
                End If
            ElseIf Keycode = 34 Then 'Pg Dn
                Dim row As Int32 = GetselectedRowIndex()
                If (row + 10) <= oMatrix.RowCount Then
                    oMatrix.SelectRow(row + 10, True, False)
                ElseIf row < 1 Then
                    oMatrix.SelectRow(1, True, False)
                Else
                    oMatrix.SelectRow(oMatrix.RowCount, True, False)
                End If
            ElseIf Keycode = 33 Then 'Pg Up
                Dim row As Int32 = GetselectedRowIndex()
                If (row - 10) > 0 Then
                    oMatrix.SelectRow(row - 10, True, False)
                ElseIf row <= 1 Then
                    oMatrix.SelectRow(1, True, False)
                Else
                    oMatrix.SelectRow(1, True, False)
                End If
            End If
        End Sub


        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.ItemUID = "1" AndAlso pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                Me.setChoosenData(oForm, BubbleEvent)
            End If
        End Sub

        Private Sub setChoosenData(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            If oMatrix.RowCount = 0 Then Exit Sub
            Dim i As Int32 = 0
            i = GetselectedRowIndex()
            If i > 0 AndAlso i <= oMatrix.RowCount Then
                Me.SetParentSharedData(i, oForm)
                Me.doReturnFromModalShared(oForm, True)
                BubbleEvent = False
            End If
        End Sub

        Private Overloads Sub SetParentSharedData(ByVal iRow As Int32, ByVal oForm As SAPbouiCOM.Form)
            bClosingFormByOKButton = True
            Me.SetParentSharedData(oForm, "ITMSGRPCOD", CType(oMatrix.Columns.Item("ItmsGrpCod").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "ITMSGRPNAM", CType(oMatrix.Columns.Item("ItmsGrpNam").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)

            Me.SetParentSharedData(oForm, "ITEMCODE", CType(oMatrix.Columns.Item("ItemCode").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "ITEMNAME", CType(oMatrix.Columns.Item("ItemName").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "UOM", CType(oMatrix.Columns.Item("SalUnitMsr").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "PUOM", CType(oMatrix.Columns.Item("BuyUnitMsr").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "INVENT", CType(oMatrix.Columns.Item("InvntItem").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim) 'U_WTDFW
            Me.SetParentSharedData(oForm, "DEFWEI", CType(oMatrix.Columns.Item("U_WTDFW").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "USEDEFWEI", CType(oMatrix.Columns.Item("U_WTUDW").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "CWBB", CType(oMatrix.Columns.Item("U_IDHCWBB").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "CREB", CType(oMatrix.Columns.Item("U_IDHCREB").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "HAZCD", CType(oMatrix.Columns.Item("U_HAZCD").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
        End Sub

        Private Sub doJobTypeCombo(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_JOBTP", "[@IDH_JOBTYPE]", IDH_JOBTYPE._JobTp, Nothing, Nothing, Nothing, True)
        End Sub

        Private Sub doWasteGroupCombo(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_WSTGRP", "[@ISB_WASTEGRP]", "U_WstGpCd", "U_WstGpNm", Nothing, "U_WstGpCd", True)
        End Sub

        Private Sub doPopulateInventory(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            Dim oInvent As SAPbouiCOM.ComboBox
            Try
                oItem = oForm.Items.Item("IDH_INVENT")
                oInvent = CType(oItem.Specific, SAPbouiCOM.ComboBox)

                Dim oValidValue As SAPbouiCOM.ValidValues
                oValidValue = oInvent.ValidValues
                doClearValidValues(oValidValue)

                oValidValue.Add("", getTranslatedWord("All"))
                oValidValue.Add("Y", getTranslatedWord("Stock"))
                oValidValue.Add("N", getTranslatedWord("None Stock"))
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error populating the Inventory Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Inventory")})
            End Try
        End Sub

    End Class
End Namespace
