Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class Driver
        Inherits IDHAddOns.idh.forms.Search
        
    Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHDRVSRC", "Driver Search.srf", 5, 45, 603, 320, "Driver Search")
        End Sub   
        
        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
        	'oGridN.setTableValue("[@IDH_DRVMAS]")
        	oGridN.doAddGridTable( new com.idh.bridge.data.GridTable( "@IDH_DRVMAS" ))
        	oGridN.setOrderValue("U_DRIVNM")
        	
        	oGridN.doAddFilterField("IDH_CODE", "Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_DRIVNM", "U_DRIVNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100)
        	oGridN.doAddFilterField("IDH_DRIVSN", "U_DRIVSN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
        	oGridN.doAddFilterField("IDH_VEHREG", "U_VehReg", SAPbouiCOM.BoDataType.dt_SHORT_NUMBER, "LIKE", 30)
        	oGridN.doAddFilterField("IDH_CONTN", "U_CONTN", SAPbouiCOM.BoDataType.dt_SHORT_NUMBER, "LIKE", 30)
        	
        	oGridN.doAddListField("CODE", "Driver Code", False, -1, Nothing, "IDH_CODE")
        	oGridN.doAddListField("U_DRIVNM", "Driver Name", False, -1, Nothing, "IDH_DRIVNM")
        	oGridN.doAddListField("U_DRIVSN", "Driver Surname", False, 30, Nothing, "IDH_DRIVSN")
        	oGridN.doAddListField("U_VehReg", "Vehicle Reg.", False, 30, Nothing, "IDH_VehReg")
        	oGridN.doAddListField("U_CONTN", "Contact No.", False, 30, Nothing, "IDH_CONTN")
        End Sub
        
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace 
