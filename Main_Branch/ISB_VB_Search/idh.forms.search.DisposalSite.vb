﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge.lookups

Namespace idh.forms.search
    Public Class DisposalSite
        Inherits idh.forms.Search.BPSearch

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHDSRCH", "BP Search.srf", 5, 40, 770, 360, "Disposal Site Search")
        End Sub
     
        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
			Dim bDoCompliance As Boolean = False
			Dim bDoUsa As Boolean = False
			Dim bDoSITESIP As Boolean = True
			If getHasSharedData(oGridN.getSBOForm) = True Then
				Dim sWValue As String
				
                sWValue = getParentSharedDataAsString(oGridN.getSBOForm, "ISCOMPLIANCE")
				bDoCompliance = sWValue Is Nothing OrElse sWValue.Equals("TRUE")
				
                bDoSITESIP = Config.INSTANCE.getParameterAsBool("WODSPSP", False)
                bDoUsa = Config.INSTANCE.getParameterAsBool("USAREL", False)
			End If
            
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OCRD", "b", Nothing, False, True))
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OCRG", "g", Nothing, False, True))
            If bDoSITESIP Then
                oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_SUITPR", "s", Nothing, False, True))
            Else
                oGridN.doAddGridTable(New com.idh.bridge.data.GridTable(Config.INSTANCE.getBalanceView(), "c"))
			End If
			
            Dim sRequired As String
            Dim sISCS As String = getParentSharedDataAsString(oGridN.getSBOForm(), "IDH_CS")
            If (Not sISCS Is Nothing AndAlso sISCS.Length > 0) Then
                sRequired = " AND U_IDHISCS = '" & sISCS & "' "
            Else
                sRequired = ""
            End If
            
            If bDoSITESIP Then
'            	SELECT b.CardCode, b.CardName, s.U_SupAddr, s.U_SupZipCd, s.U_WasteCd,
'					s.U_WasteDs, s.U_StAddr, s.U_ZpCd, s.U_Haulge, s.U_TipTon,
'					s.U_UOM, (s.U_Haulge + s.U_TipTon) As Total
'				FROM OCRD b, OCRG g, [@IDH_SUITPR] s
'				WHERE
'					b.GroupCode = g.GroupCode
'					AND s.U_CardCd = b.CardCode
'					AND (b.U_IDH_DUSJB is NULL Or Upper(b.U_IDH_DUSJB) != 'TRUE')
'					AND b.FrozenFor <> 'Y'
'					AND s.U_ZpCd != ''

'				And CAST(CONVERT(VARCHAR, U_StDate,101) AS DATETIME) <= CAST(CONVERT(VARCHAR, '" + sDocDate + "',101) AS DATETIME) 
'               And CAST(CONVERT(VARCHAR, U_EnDate,101) As DATETIME) >= CAST(CONVERT(VARCHAR, '" + sDocDate + "',101) AS DATETIME)
				Dim sDate As String = com.idh.utils.dates.doSimpleSQLDate(DateTime.Now)
				
            	sRequired = "b.GroupCode = g.GroupCode AND (b.U_IDH_DUSJB is NULL Or Upper(b.U_IDH_DUSJB) != 'TRUE') AND b.CardCode=s.U_CardCd" & sRequired & " AND b.FrozenFor <> 'Y'" & _ 
            	            " And CAST(CONVERT(VARCHAR, s.U_StDate,101) AS DATETIME) <= CAST(CONVERT(VARCHAR, '" + sDate + "',101) AS DATETIME) " & _ 
            	            " And CAST(CONVERT(VARCHAR, s.U_EnDate,101) As DATETIME) >= CAST(CONVERT(VARCHAR, '" + sDate + "',101) AS DATETIME) "
            Else
            	sRequired = "b.GroupCode = g.GroupCode AND (b.U_IDH_DUSJB is NULL Or Upper(b.U_IDH_DUSJB) != 'TRUE') AND b.CardCode=c.CardCode" & sRequired & " AND b.FrozenFor <> 'Y'"
			End If
			
            oGridN.setRequiredFilter(sRequired)

            oGridN.doAddFilterField("IDH_BPCOD", "b.CardCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDH_NAME", "b.CardName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDH_TYPE", "b.CardType", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_GROUP", "b.GroupCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_BRANCH", "b.U_IDHBRAN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

			If bDoSITESIP Then
	            oGridN.doAddListField("b.CardCode", "Code", False, 50, Nothing, "CARDCODE", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
	            oGridN.doAddListField("b.CardName", "Name", False, 150, Nothing, "CARDNAME")
	            oGridN.doAddListField("b.CardType", "Type", False, 0, Nothing, "CARDTYPE")
	            oGridN.doAddListField("b.GroupCode", "Group Code", False, 0, Nothing, "GROUPCODE")
	            oGridN.doAddListField("g.GroupName", "Group Name", False, 0, Nothing, "GROUPNAME")
	            
	            oGridN.doAddListField("s.U_SupAddr", "Supplier Address", False, -1, Nothing, "SUPADDR")
	            oGridN.doAddListField("s.U_SupZipCd", "Sup. Postal Code", False, -1, Nothing, "SUPZIP")
	            oGridN.doAddListField("s.U_WasteCd", "Waste Code", False, -1, Nothing, "WASTCD")
				oGridN.doAddListField("s.U_WasteDs", "Waste Description", False, -1, Nothing, "WASTDS")
				oGridN.doAddListField("s.U_StAddr", "Customer Address", False, -1, Nothing, "CUSTADDR")
				oGridN.doAddListField("s.U_ZpCd", "Cust. Postal Code", False, -1, Nothing, "CUSTZIP")
				oGridN.doAddListField("s.U_Haulge", "Haulage", False, -1, Nothing, "HAULAGE")
				oGridN.doAddListField("s.U_TipTon", "Tip", False, -1, Nothing, "TIP")
				oGridN.doAddListField("s.U_UOM", "UOM", False, -1, Nothing, "UOM")
				oGridN.doAddListField("(s.U_Haulge + s.U_TipTon)", "Total", False, -1, Nothing, "total")
	            
	            If bDoCompliance Then
	                oGridN.doAddListField("b.OrdersBal", "Orders", False, 0, Nothing, "ORDERS")
	                oGridN.doAddListField("b.CreditLine", "Credit Limit", False, 0, Nothing, "CREDITLINE")
	                oGridN.doAddListField("b.U_IDHBRAN", "Branch", False, 0, Nothing, "BRANCH")
	            Else
	                oGridN.doAddListField("b.OrdersBal", "Orders", False, 30, Nothing, "ORDERS")
	                oGridN.doAddListField("b.CreditLine", "Credit Limit", False, 30, Nothing, "CREDITLINE")
	                oGridN.doAddListField("b.U_IDHBRAN", "Branch", False, 20, Nothing, "BRANCH")
	            End If
	
	            oGridN.doAddListField("b.Balance", "FC Balance", False, 0, Nothing, "BALANCEFC")
	            oGridN.doAddListField("b.Phone1", "Phone", False, 0, Nothing, "PHONE1")
	            oGridN.doAddListField("b.CntctPrsn", "Contact Person", False, 0, Nothing, "CNTCTPRSN")
	            oGridN.doAddListField("b.U_WASLIC", "Waste Lic. Reg. No.", False, 0, Nothing, "WASLIC")
	            oGridN.doAddListField("b.U_IDHICL", "Don't do Credit Check", False, 0, Nothing, "IDHICL")
	            oGridN.doAddListField("b.U_IDHLBPC", "Linked BP Code", False, 0, Nothing, "IDHLBPC")
	            oGridN.doAddListField("b.U_IDHLBPN", "Linked BP Name", False, 0, Nothing, "IDHLBPN")
	            oGridN.doAddListField("b.U_IDHOBLGT", "Obligated", False, 0, Nothing, "IDHOBLGT")
	            oGridN.doAddListField("b.U_IDHUOM", "BP UOM", False, 0, Nothing, "IDHUOM")
	            oGridN.doAddListField("b.U_IDHONCS", "Compliance Scheme", False, 0, Nothing, "IDHONCS")
	            oGridN.doAddListField("b.ListNum", "Price List", False, 0, Nothing, "PLIST")
	            oGridN.doAddListField("b.GroupNum", "Payment Terms", False, 0, Nothing, "PAYTRM")

                oGridN.doAddFilterField("IDH_ZIPCOD", "s.U_ZpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
                oGridN.doAddFilterField("IDH_WSTCD", "s.U_WasteCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 50)

				'HIDE ITEMS
				setVisible(oGridN.getSBOForm(), "LBL_ADD", False)
				setVisible(oGridN.getSBOForm(), "IDH_ADRES", False)
				
				setVisible(oGridN.getSBOForm(), "LBL_STREET", False)
				setVisible(oGridN.getSBOForm(), "IDH_STREET", False)

				'SHOW ITEMS
				setVisible(oGridN.getSBOForm(), "IDH_LBLZIP", True)
                setVisible(oGridN.getSBOForm(), "IDH_ZIPCOD", True)
                
                setVisible(oGridN.getSBOForm(), "IDH_LBLWST", True)
                setVisible(oGridN.getSBOForm(), "IDH_WSTCD", True)
			Else
            
	            oGridN.doAddListField("b.CardCode", "Code", False, 50, Nothing, "CARDCODE", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
	            oGridN.doAddListField("b.CardName", "Name", False, 150, Nothing, "CARDNAME")
	            oGridN.doAddListField("b.CardType", "Type", False, 40, Nothing, "CARDTYPE")
	            oGridN.doAddListField("b.GroupCode", "Group Code", False, 0, Nothing, "GROUPCODE")
	            oGridN.doAddListField("g.GroupName", "Group Name", False, 150, Nothing, "GROUPNAME")

	            If bDoCompliance Then
	                oGridN.doAddListField("c.RemainBal", "Credit Remain", False, 0, Nothing, "CRREMAIN")
	                oGridN.doAddListField("b.OrdersBal", "Orders", False, 0, Nothing, "ORDERS")
	                oGridN.doAddListField("b.CreditLine", "Credit Limit", False, 0, Nothing, "CREDITLINE")
	                oGridN.doAddListField("c.Balance", "Balance", False, 0, Nothing, "BALANCE")
	                oGridN.doAddListField("c.TBalance", "Total Balance", False, 0, Nothing, "TBALANCE")
	                oGridN.doAddListField("c.WRBalance", "WR1 Orders", False, 0, Nothing, "WRORDERS")
	                oGridN.doAddListField("b.U_IDHBRAN", "Branch", False, 0, Nothing, "BRANCH")
	            Else
	                oGridN.doAddListField("c.RemainBal", "Credit Remain", False, 30, Nothing, "CRREMAIN")
	                oGridN.doAddListField("b.OrdersBal", "Orders", False, 30, Nothing, "ORDERS")
	                oGridN.doAddListField("b.CreditLine", "Credit Limit", False, 30, Nothing, "CREDITLINE")
	                oGridN.doAddListField("c.Balance", "Balance", False, 30, Nothing, "BALANCE")
	                oGridN.doAddListField("c.TBalance", "Total Balance", False, -1, Nothing, "TBALANCE")
	                oGridN.doAddListField("c.WRBalance", "WR1 Orders", False, 30, Nothing, "WRORDERS")
	                oGridN.doAddListField("b.U_IDHBRAN", "Branch", False, 20, Nothing, "BRANCH")
	            End If
	
	            oGridN.doAddListField("b.Balance", "FC Balance", False, 0, Nothing, "BALANCEFC")
	            oGridN.doAddListField("c.Deviation", "Deviation", False, 0, Nothing, "DEVIATION")
	            oGridN.doAddListField("c.frozenFor", "Frozen", False, 0, Nothing, "FROZEN")
	            oGridN.doAddListField("c.FrozenComm", "FrozenComm", False, 0, Nothing, "FROZENC")
	            oGridN.doAddListField("b.Phone1", "Phone", False, 0, Nothing, "PHONE1")
	            oGridN.doAddListField("b.CntctPrsn", "Contact Person", False, 0, Nothing, "CNTCTPRSN")
	            oGridN.doAddListField("b.U_WASLIC", "Waste Lic. Reg. No.", False, 0, Nothing, "WASLIC")
	            oGridN.doAddListField("b.U_IDHICL", "Don't do Credit Check", False, 0, Nothing, "IDHICL")
	            oGridN.doAddListField("b.U_IDHLBPC", "Linked BP Code", False, 0, Nothing, "IDHLBPC")
	            oGridN.doAddListField("b.U_IDHLBPN", "Linked BP Name", False, 0, Nothing, "IDHLBPN")
	            oGridN.doAddListField("b.U_IDHOBLGT", "Obligated", False, 0, Nothing, "IDHOBLGT")
	            oGridN.doAddListField("b.U_IDHUOM", "BP UOM", False, 0, Nothing, "IDHUOM")
	            oGridN.doAddListField("b.U_IDHONCS", "Compliance Scheme", False, 0, Nothing, "IDHONCS")
	            oGridN.doAddListField("c.frozenFrom", "Frozen From", False, 0, Nothing, "FROZENF")
	            oGridN.doAddListField("c.frozenTo", "Frozen To", False, 0, Nothing, "FROZENT")
	            oGridN.doAddListField("b.ListNum", "Price List", False, 0, Nothing, "PLIST")
	            oGridN.doAddListField("b.GroupNum", "Payment Terms", False, 0, Nothing, "PAYTRM")

	            'USA Requirement
	            If bDoUsa Then
	                oGridN.doAddFilterField("IDH_ADRES", "b.BillToDef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 150)
	                oGridN.doAddFilterField("IDH_STREET", "b.Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 150)
	
	                oGridN.doAddListField("b.BillToDef", "Billing Address", False, 150, Nothing, "ADDRESS")
	                oGridN.doAddListField("b.Address", "Street", False, -1, Nothing, "MAILADD")
	                oGridN.doAddListField("b.ZipCode", "Zip Code", False, -1, Nothing, "ZIPCODE")
	            Else
	                Dim oItem As SAPbouiCOM.Item
	                Try
	                    oItem = (oGridN.getSBOForm()).Items.Item("LBL_ADD")
	                    oItem.Visible = False
	
	                    oItem = (oGridN.getSBOForm()).Items.Item("IDH_ADRES")
	                    oItem.Visible = False
	
	                    oItem = (oGridN.getSBOForm()).Items.Item("LBL_STREET")
	                    oItem.Visible = False
	
	                    oItem = (oGridN.getSBOForm()).Items.Item("IDH_STREET")
	                    oItem.Visible = False
	
	                Catch ex As Exception
	                End Try
	            End If
			End If
        End Sub
     
     
	End Class
End Namespace