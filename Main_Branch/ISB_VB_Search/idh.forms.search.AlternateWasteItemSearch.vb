Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.dbObjects.User

Namespace idh.forms.search
    Public Class AlternateWasteItemSearch
        Inherits IDHAddOns.idh.forms.Search
        Private bClosingFormByOKButton As Boolean = False

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHALTWISRC", "Alternate WasteItem Search.srf", 5, 45, 340, 270, "Alternate Waste Item Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("OITM i, OITB ig")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OITM", "i"))
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_ITMDESC", "ai"))

            oGridN.setOrderValue("cast(ai.U_ItemAltName as varchar(max))")

            'oGridN.doAddFilterField("IDH_ITMCOD", "i.ItemCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            'If getParentSharedData(oGridN.getSBOForm, "TRG") = "FROMENQUIRY" Then
            '    oGridN.doAddFilterField("IDH_ITMNAM", "i.FrgnName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            'Else
            'oGridN.doAddFilterField("IDH_ITMNAM", "i.ItemName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            'End If
            oGridN.doAddListField("i.ItemCode", "Item Code", False, 100, Nothing, "ITEMCODE", -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("ai.Code", "Code", False, 0, Nothing, "ItemAltCode") ', "ITMSGRPCOD", -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("ai.U_ItemAltName", "Alternate Waste Desc.", False, -1, Nothing, "ItemAltName") ', "ITMSGRPCOD", -1, SAPbouiCOM.BoLinkedObject.lf_Items)
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
            doAddUF(oForm, "IDH_ITMCOD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30)
            doAddUF(oForm, "IDH_ITMNAM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 255)
        End Sub

        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)
        End Sub
        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")

            'Dim sRequiredString As String = "i.ItmsGrpCod = ig.ItmsGrpCod AND i.FrozenFor <> 'Y' "
            Dim sRequiredString As String = "i.ItemCode= ai.U_ItemCode"
            Dim sItemCode As String = getParentSharedDataAsString(oForm, "IDH_ITMCOD")
            If sItemCode Is Nothing Then
                sItemCode = ""
            End If
            Dim sItemName As String = getParentSharedDataAsString(oForm, "IDH_ITMNAM")
            If sItemName Is Nothing Then
                sItemName = ""
            End If
            sRequiredString &= " And i.ItemCode='" & sItemCode & "' And i.ItemName='" & sItemName & "'"

            oGridN.setRequiredFilter(sRequiredString)
            setUFValue(oForm, "IDH_ITMCOD", sItemCode)
            setUFValue(oForm, "IDH_ITMNAM", sItemName)

            bClosingFormByOKButton = False
            Me.setParentSharedData(oForm, "IDHALTWISRCOPEND", "LOADED")
            MyBase.doLoadData(oForm)
        End Sub
        ''## Start 05-07-2013
        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doFinalizeShow(oForm)
            If oForm.Visible = False Then
                Me.setParentSharedData(oForm, "IDHALTWISRCOPEND", "")
            Else
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                If oGridN Is Nothing Then
                    oGridN = New FilterGrid(Me, oForm, "LINESGRID", True)
                End If
                oGridN.doReloadData()
            End If

        End Sub
        ''## End

        Protected Overrides Sub doPrepareModalData(ByVal oForm As SAPbouiCOM.Form, Optional ByVal iSel As Integer = -1)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            Dim iIndex As Integer
            Dim iCurrentDataRow As Integer
            Dim oSelectedRows As SAPbouiCOM.SelectedRows
            oSelectedRows = oGridN.getSBOGrid.Rows.SelectedRows
            If oSelectedRows.Count = 0 Then
                oSelectedRows.Add(0)
            End If

            If iSel = -1 Then
                iCurrentDataRow = oGridN.getSBOGrid.GetDataTableRowIndex(oSelectedRows.Item(iIndex, SAPbouiCOM.BoOrderType.ot_SelectionOrder))
            End If

            If iCurrentDataRow > -1 Then
                oGridN.setCurrentDataRowIndex(iCurrentDataRow)

                'Dim sFrozenValue As String = oGridN.doGetFieldValue("FROZEN")
                'If (sFrozenValue IsNot Nothing AndAlso sFrozenValue = "Y") Then
                '    Dim sItemCode As String = oGridN.doGetFieldValue("i.ItemCode")
                '    com.idh.bridge.DataHandler.INSTANCE.doResUserError("The selected Item is Inactive.", "ERUSITF", {sItemCode})
                '    Exit Sub
                'Else
                Dim iFields As Integer
                Dim oField As com.idh.controls.strct.ListField
                For iFields = 0 To oGridN.getListfields().Count - 1
                    oField = CType(oGridN.getListfields().Item(iFields), com.idh.controls.strct.ListField)
                    setParentSharedData(oForm, oField.msFieldId, oGridN.doGetFieldValue(oField.msFieldName))
                Next
                'Exit For
                'End If
            End If
            'Next
            doReturnFromModalShared(oForm, True)
        End Sub

        '** The ItemSubEvent handler
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            ''## Start 05-07-2013
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD OrElse _
                    (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.setParentSharedData(oForm, "IDHALTWISRCOPEND", "")
                    If bClosingFormByOKButton = False Then
                        MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                    End If
                End If
            End If
            ''## End
            Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
        End Function

        Protected Overrides Sub doReadInputParams(oForm As SAPbouiCOM.Form)
            setParentSharedData(oForm, "CALLEDITEM", getParentSharedData(oForm, "CALLEDITEM"))
            MyBase.doReadInputParams(oForm)
        End Sub

    End Class
End Namespace
