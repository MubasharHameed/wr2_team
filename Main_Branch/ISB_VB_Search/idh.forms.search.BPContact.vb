Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class BPContact
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHBPCSRC", "BP Contact Search.srf", 5, 45, 603, 320, "BP Contact Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("[@IDH_DRVMAS]")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OCPR", Nothing, Nothing, False, True))
            oGridN.setOrderValue("CardCode")

            oGridN.doAddFilterField("IDH_BPCODE", "CardCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_CONTNM", "Name", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            oGridN.doAddListField("CardCode", "BP Code", False, -1, Nothing, "BPCODE")
            oGridN.doAddListField("Name", "Contact Person", False, -1, Nothing, "CONTNM")
            oGridN.doAddListField("FirstName", "First Name", False, -1, Nothing, "FNAME")
            oGridN.doAddListField("LastName", "Last Name", False, -1, Nothing, "LNAME")
            oGridN.doAddListField("Tel1", "Telephone 1", False, -1, Nothing, "CTEL1")
            oGridN.doAddListField("Fax", "Fax", False, -1, Nothing, "CFAX")
            oGridN.doAddListField("E_MailL", "Email", False, -1, Nothing, "CEMAIL")
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace 
