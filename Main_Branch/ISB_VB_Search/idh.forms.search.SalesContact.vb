Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class SalesContact
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHSALCNT", "SalesContact Search.srf", 5, 45, 603, 320, "Sales Contact Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("[@IDH_ORIGIN]")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OSLP", Nothing, Nothing, False, True))
            oGridN.setOrderValue("SlpName")

            oGridN.doAddFilterField("IDH_SALCNT", "SlpName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100, Nothing, True)

            oGridN.doAddListField("SlpCode", "Sales Contact Code", False, -1, Nothing, "IDHSLPCD")
            oGridN.doAddListField("SlpName", "Sales Contact Name", False, -1, Nothing, "IDHSLPNM")
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace
