Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class DRCodes
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDH_DRCODS", "DRCodeSearch.srf", 5, 45, 603, 320, "D and R Code Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("IDH_DRCode", Nothing, Nothing, False, True))
            oGridN.setOrderValue("Code")

            'oGridN.doAddFilterField("IDH_HZCOD", "Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_WTWC", "Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_WTWD", "Description", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100)

            'oGridN.doAddListField("Code", "DR Code", False, -1, Nothing, "IDH_HZCODO")
            'oGridN.doAddListField("Name", "DR Name", False, -1, Nothing, "IDH_HZNAMO")
            oGridN.doAddListField("Code", "DR Code", False, -1, Nothing, "IDH_WTWCO")
            oGridN.doAddListField("Description", "DR Description", False, -1, Nothing, "IDH_WTWDO")
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace