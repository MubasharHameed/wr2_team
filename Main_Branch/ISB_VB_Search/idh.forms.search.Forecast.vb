﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class Forecast
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDH_FORECSR", "Forecast Search.srf", 5, 45, 603, 320, "Forecast Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            oGridN.doAddGridTable( new com.idh.bridge.data.GridTable( "@IDH_BPFORECST" ) )
            oGridN.setOrderValue("Code")

            oGridN.doAddFilterField("IDH_FORECD", "Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_CUSTCD", "U_CardCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 30)
            oGridN.doAddFilterField("IDH_WASTCD", "U_ItemCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100)

            oGridN.doAddListField("Code", "Forecast", False, -1, Nothing, "IDH_FORECST")
            oGridN.doAddListField("U_DocType", "Doc Type", False, -1, "COMBOBOX", "IDH_TYPE")
            oGridN.doAddListField("U_CardType", "BP Type", False, -1, Nothing, "IDH_CTYPE")
            oGridN.doAddListField("U_CardCd", "Cardcode", False, -1, Nothing, "IDH_CUST")
            oGridN.doAddListField("U_ItemCd", "Waste Code", False, -1, Nothing, "IDH_WASTCD")
            oGridN.doAddListField("U_AltWasDsc", "Alternate Waste Code", False, -1, Nothing, "IDH_ALFCWC")
            oGridN.doAddListField("U_EstQty", "Estimated Qty", False, -1, Nothing, "IDH_ESTQTY")
            oGridN.doAddListField("U_RemQty", "Remaining Qty", False, -1, Nothing, "IDH_REMQTY")
            oGridN.doAddListField("U_RemCQty", "Uncommitted Qty", False, -1, Nothing, "IDH_REMCQTY")
            oGridN.doAddListField("U_UOM", "Unit Of Measure", False, -1, Nothing, "IDH_UOM")
            oGridN.doAddListField("U_ExpLdWgt", "Expected Load Weight", False, -1, Nothing, "IDH_EXPWGT")

            oGridN.setRequiredFilter( " U_LnkWOH = '' " )
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub
        
        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
            doDocTypeCombo(FilterGrid.getInstance(oForm, "LINESGRID"))
        End Sub

        Private Sub doDocTypeCombo(ByVal oGridN As FilterGrid)
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_DocType")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            Try
				doFillCombo(oGridN.getSBOForm(), oCombo, "[@IDH_WRSTATUS]", "Code", "Name", Nothing, Nothing, Nothing, Nothing)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Type Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Type")})
            End Try
        End Sub


    End Class
End Namespace
