Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge.lookups

Namespace idh.forms.search
    Public Class WasteGroupSearch
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDH_WGSRCH", "WasteGroup Search.srf", 5, 30, 485, 330, "Waste Group Search")
        End Sub

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sType As String, ByVal sSRF As String, ByVal iGL As Integer, ByVal iGT As Integer, ByVal iGW As Integer, ByVal iGH As Integer, ByVal sTitle As String)
            MyBase.New(oParent, sType, sSRF, iGL, iGT, iGW, iGH, sTitle)
        End Sub


        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            Me.setParentSharedData(oForm, "IDH_WGSRCHOPEND", "LOADED")
            MyBase.doLoadData(oForm)
        End Sub

        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
        End Sub
        '## End

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@ISB_WASTEGRP", "a", Nothing, False, True))
            
            oGridN.doAddFilterField("IDH_WGCOD", "a.U_WstGpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 20, Nothing, True)
            oGridN.doAddFilterField("IDH_NAME", "a.U_WstGpNm", SAPbouiCOM.BoDataType.dt_LONG_TEXT, "LIKE", 100, Nothing, True)


            oGridN.doAddListField("a.U_WstGpCd", "Group Code", False, 50, Nothing, "WGCODE")
            oGridN.doAddListField("a.U_WstGpNm", "Group Name", False, 150, Nothing, "WGNAME")
            oGridN.setOrderValue("a.U_WstGpCd ASC ")

        End Sub

         

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)
        End Sub

        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            
            MyBase.doReadInputParams(oForm)
            setParentSharedData(oForm, "CALLEDITEM", getParentSharedData(oForm, "CALLEDITEM"))
            'Dim sVal As String = getParentSharedData(oForm, "IDH_TYPE")
        End Sub

        
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            'If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
            '    If pVal.BeforeAction = False Then
            '        If pVal.ItemUID = "IDH_NEW" Then
            '            setSharedData(oForm, "ACT", "ADD")
            '            setSharedData(oForm, "TYPE", "C")
            '            goParent.doOpenModalForm("134", oForm)
            '        ElseIf pVal.ItemUID = "IDH_MAPS" Then
            '            Dim sCustPostCode As String = getParentSharedData(oForm, "IDH_CUSTZP")
            '            doGMaps(goParent, oForm, Config.Parameter("MAPURL").ToString() + "?" + "PC=" + sCustPostCode + "")
            '            'If oForm.Items.Item("IDH_MAPS").Specific.Caption = getTranslatedWord("Show on Maps" Then)
            '            '    oForm.Height = 727
            '            '    oForm.Items.Item("IDH_MAPS").Specific.Caption = getTranslatedWord("Hide Maps")
            '            'Else
            '            '    oForm.Height = 465
            '            '    oForm.Items.Item("IDH_MAPS").Specific.Caption = getTranslatedWord("Show on Maps")
            '            'End If
            '        End If
            '    End If
            'End If
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD OrElse _
                    (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.setParentSharedData(oForm, "IDH_WGSRCHOPEND", "")
                    MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                    'End If
                End If
            End If
            Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
        End Function

        'Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)

        'End Sub

    End Class
End Namespace
