Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.bridge
Imports com.idh.bridge.lookups

Imports com.idh.dbObjects.User

Namespace idh.forms.search
    Public Class ItemSearch2
        Inherits IDHAddOns.idh.forms.Search

        Private FormLoaded As Boolean = False
        Private oMatrix As SAPbouiCOM.Matrix
        Private _sSQL As String = ""
        Private iLastselectedRow As Int32 = 0
        Private slastSearchedResult As String = ""
        Private bClosingFormByOKButton As Boolean = False
        Private WithEvents timer_bp As System.Timers.Timer
        Private timer_Form As SAPbouiCOM.Form
        Private timer_ColUID, timer_ItemUID As String

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHISRC", "Item Search2.srf", 5, 70, 603, 320, "Item Search L-2")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
        End Sub

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            bClosingFormByOKButton = False
            Me.SetParentSharedData(oForm, "IDH_ITM1SRCHOPEND", "LOADED")
            _sSQL = ""
            iLastselectedRow = 0
            slastSearchedResult = ""
             
            setUFValue(oForm, "IDH_ITMCOD", getParentSharedData(oForm, "IDH_ITMCOD"))
            setUFValue(oForm, "IDH_ITMNAM", getParentSharedData(oForm, "IDH_ITMNAM"))
            setUFValue(oForm, "IDH_GRPCOD", getParentSharedData(oForm, "IDH_GRPCOD"))
            setUFValue(oForm, "IDH_GRPNAM", "")

            bClosingFormByOKButton = False
            Me.SetParentSharedData(oForm, "IDH_ITM1SRCHOPEND", "LOADED")

            reQuerynReloadMatrix(oForm)
        End Sub

        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            Try
                oForm.Items.Item("LINESGRID").Visible = False
                Dim bFlag As Boolean = False
                Dim sSILENT As String = ""
                If (Me.getHasSharedData(oForm)) Then
                    sSILENT = Me.getParentSharedDataAsString(oForm, "SILENT") '.ToString
                    If (sSILENT <> Nothing) Then
                        If (sSILENT.ToUpper().Equals("SHOWMULTI")) Then
                            bFlag = True 'Means we have some value in CardCode/cardname
                        End If
                    End If
                End If
                If (bFlag AndAlso oMatrix.RowCount > 0) Then
                    oMatrix.SelectRow(1, True, False)
                    If (oMatrix.RowCount = 1) Then
                        Me.SetParentSharedData(1, oForm)
                        Me.doReturnFromModalShared(oForm, True)
                        Me.SetParentSharedData(oForm, "IDH_ITM1SRCHOPEND", "")
                        Return
                    End If
                End If
                oForm.Visible = True
                Dim otxt As SAPbouiCOM.EditText = CType(oForm.Items.Item("IDH_ITMCOD").Specific, SAPbouiCOM.EditText)
                otxt.Active = True
                FormLoaded = True

                timer_bp = New System.Timers.Timer(700)
                timer_bp.Enabled = False
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Finalizing Show Form.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXELF", {Nothing})
            End Try
        End Sub

        Private Function PrepareSQL(ByVal oform As SAPbouiCOM.Form, Optional ByVal sOrderbyCol As String = "") As String
            iLastselectedRow = 0
            slastSearchedResult = ""
            Dim sql As String = " Select i.ItmsGrpCod,ig.ItmsGrpNam,i.ItemCode, i.ItemName,i.SalUnitMsr, i.BuyUnitMsr, i.InvntItem, i.U_WTDFW,  " _
                                   & " i.U_WTUDW " _
                                   & "     From OITM i , OITB ig Where i.ItmsGrpCod=ig.ItmsGrpCod " _
                                   & "And ((cast((case when ( (i.FrozenFor = 'Y' and (GetDate() Between isnull(i.FrozenFrom, DateAdd(d,-1,GetDate())) and isnull(i.FrozenTo, DateAdd(d,1,GetDate()))))) then 1 else 0 end) as bit)  " _
                                   & " ) | " _
                                   & " (cast((case when ( (i.validFor = 'Y' and NOT(GetDate() Between isnull(i.validFrom, DateAdd(d,-1,GetDate())) and isnull(i.validto, DateAdd(d,1,GetDate()))))) then 1 else 0 end) as bit)  " _
                                   & " ))=0 "

            PrepareSQL = sql

            Dim sItemCode As String = getItemValue(oform, "IDH_ITMCOD")

            Dim sItemName As String = getItemValue(oform, "IDH_ITMNAM")

            Dim sGroupCode As String = getItemValue(oform, "IDH_GRPCOD")

            Dim sGroupName As String = getItemValue(oform, "IDH_GRPNAM")


            Dim sWhereSql As String = ""
            If sItemCode <> "" Then
                sWhereSql &= " and i.ItemCode LIKE '" & sItemCode.Replace("*", "%") & "%' "
            End If

            If sItemName <> "" Then
                sWhereSql &= " and i.ItemName Like '" & sItemName.Replace("*", "%") & "%' "
            End If

            If sGroupCode <> "" AndAlso sGroupCode.IndexOf(",") = -1 Then
                sWhereSql &= " and i.ItmsGrpCod Like '" & sGroupCode.Replace("*", "%") & "' "
            ElseIf sGroupCode <> "" AndAlso sGroupCode.IndexOf(",") > -1 Then
                sWhereSql &= " and i.ItmsGrpCod In (" & sGroupCode & ") "
            End If

            If sGroupName <> "" Then
                sWhereSql &= " and ig.ItmsGrpNam LIKE '" & sGroupName.Replace("*", "%") & "%' "
            End If

            
            If sWhereSql.Trim <> "" Then
                sql &= sWhereSql
            End If
            If sOrderbyCol <> "" Then
                sql &= " Order by " & sOrderbyCol
            End If
            Return sql
        End Function

        Private Sub reQuerynReloadMatrix(ByVal oform As SAPbouiCOM.Form, Optional ByVal sOrderByCol As String = "")
            Try
                iLastselectedRow = 0
                slastSearchedResult = ""
                Dim sSQL As String = PrepareSQL(oform, sOrderByCol).Trim
                If (sSQL.Trim.ToLower.Equals(_sSQL.ToLower)) AndAlso _sSQL <> "" Then
                    Return
                End If
                _sSQL = sSQL
                oMatrix = CType(oform.Items.Item("LINESMTX").Specific, SAPbouiCOM.Matrix)
                oform.DataSources.DataTables.Item("LINESMTX").ExecuteQuery(sSQL)
                ' oform.Freeze(True)
                oMatrix.Clear()
                oMatrix.LoadFromDataSourceEx(False)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error in Quering db.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", {_sSQL})
            Finally
                '  oform.Freeze(False)
            End Try

        End Sub

        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_GOT_FOCUS)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub
        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)

        End Sub

        Private Function GetselectedRowIndex() As Int32
            Dim i As Int32 = 0
            i = oMatrix.GetNextSelectedRow(0, SAPbouiCOM.BoOrderType.ot_SelectionOrder)
            Return i
        End Function

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            ''## Start 05-07-2013
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD OrElse _
                    (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.SetParentSharedData(oForm, "IDH_ITM1SRCHOPEND", "")
                    If bClosingFormByOKButton = False Then
                        MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                    End If
                End If
            End If
            If pVal.BeforeAction = False Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_FORM_CLOSE
                        Me.SetParentSharedData(oForm, "IDH_ITM1SRCHOPEND", "")
                        If bClosingFormByOKButton = False Then
                            MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                        End If
                     
                    Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                        Dim sOrdrebyCol As String = "i.ItemCode"
                        If (pVal.ItemUID = "IDH_GRPCOD" OrElse pVal.ItemUID = "IDH_GRPNAM") Then
                            reQuerynReloadMatrix(oForm, sOrdrebyCol)
                        End If
                        BubbleEvent = False
                    Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS 'SAPbouiCOM.BoEventTypes.et_VALIDATE
                        Dim sOrdrebyCol As String = ""
                        If (pVal.ItemUID = "IDH_ITMCOD" OrElse pVal.ItemUID = "IDH_ITMNAM" OrElse pVal.ItemUID = "IDH_GRPCOD" OrElse pVal.ItemUID = "IDH_GRPNAM") Then
                            Select Case pVal.ItemUID
                                Case "IDH_ITMCOD"
                                    sOrdrebyCol = "i.ItemCode"
                                Case "IDH_ITMNAM"
                                    sOrdrebyCol = "i.ItemName"
                                Case Else
                                    sOrdrebyCol = "i.ItemCode"
                            End Select
                            reQuerynReloadMatrix(oForm, sOrdrebyCol)
                            BubbleEvent = False
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "LINESMTX" Then
                            If pVal.Row > 0 Then
                                oMatrix.SelectRow(pVal.Row, True, False)
                            End If
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                        'If pVal.BeforeAction = False Then
                        If pVal.ItemUID = "LINESMTX" Then
                            Me.setChoosenData(oForm, BubbleEvent)
                        End If
                        'End If
                    Case SAPbouiCOM.BoEventTypes.et_KEY_DOWN
                        If pVal.CharPressed <> 9 AndAlso pVal.CharPressed <> 38 AndAlso pVal.CharPressed <> 39 AndAlso pVal.CharPressed <> 40 AndAlso pVal.CharPressed <> 13 _
                             AndAlso (pVal.ItemUID = "IDH_ITMCOD" OrElse pVal.ItemUID = "IDH_ITMNAM") Then
                            Select Case pVal.ItemUID
                                Case "IDH_ITMCOD"
                                    timer_bp.Enabled = False
                                    timer_bp.Interval = 400
                                    timer_Form = oForm
                                    timer_ColUID = "ItemCode"
                                    timer_ItemUID = pVal.ItemUID
                                    If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
                                        iLastselectedRow = 0
                                        slastSearchedResult = ""
                                    End If
                                    timer_bp.Enabled = True
                                    'doSelectOnKeyPress("CardCode", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)
                                Case "IDH_ITMNAM"
                                    timer_bp.Enabled = False
                                    timer_bp.Interval = 400
                                    timer_Form = oForm
                                    timer_ColUID = "ItemName"
                                    timer_ItemUID = pVal.ItemUID
                                    If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
                                        iLastselectedRow = 0
                                        slastSearchedResult = ""
                                    End If
                                    timer_bp.Enabled = True
                                    'doSelectOnKeyPress("CardName", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)
                            End Select
                            BubbleEvent = False
                        End If

                        'If Not (pVal.ItemUID = "IDH_TYPE" OrElse pVal.ItemUID = "IDH_GROUP" OrElse pVal.ItemUID = "IDH_BRANCH") Then
                        If (pVal.CharPressed = 40 OrElse pVal.CharPressed = 38 OrElse pVal.CharPressed = 34 OrElse pVal.CharPressed = 33) AndAlso (oMatrix.RowCount > 0) Then
                            handleKeyEventForMatrixRowSelection(pVal.CharPressed)
                        End If
                        'End If
                End Select
            End If
            Return True
        End Function

#Region "Timer Delay"
        Private Sub timer_bp_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles timer_bp.Elapsed
            timer_bp.Enabled = False
            doSelectOnKeyPressNew(timer_ColUID, getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
            'Select Case (timer_ColUID)
            '    Case "ItemCode"
            '        doSelectOnKeyPressNew("ItemCode", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
            '    Case "ItemName"
            '        doSelectOnKeyPressNew("ItemName", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
            '    Case "ItmsGrpCod"
            '        doSelectOnKeyPressNew("ItmsGrpCod", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
            '    Case "ItmsGrpNam"
            '        doSelectOnKeyPressNew("ItmsGrpNam", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
            'End Select
        End Sub
#End Region
        Private Sub doSelectOnKeyPressNew(ByVal sColUID As String, ByVal sValueToSearch As String)
            Dim iRowCount As Int32 = oMatrix.RowCount
            If sValueToSearch.Length = 0 Then
                iLastselectedRow = 0
                slastSearchedResult = ""
                Return
            ElseIf sValueToSearch.Length = 1 Then
                If slastSearchedResult <> "" AndAlso sValueToSearch.StartsWith(slastSearchedResult) Then
                    Return
                End If
                iLastselectedRow = 0
                slastSearchedResult = ""
                Dim iIndex As Int32 = SearchValue(sColUID, sValueToSearch)
                If oMatrix.RowCount >= iIndex Then
                    oMatrix.SelectRow(iIndex, True, False)
                    iLastselectedRow = iIndex
                    slastSearchedResult = sValueToSearch
                    oMatrix.SelectRow(iIndex, True, False)
                End If
            ElseIf sValueToSearch.Length > 1 Then
                iLastselectedRow = 0
                slastSearchedResult = ""
                Dim iIndex As Int32 = SearchValue(sColUID, sValueToSearch)
                If oMatrix.RowCount >= iIndex Then
                    oMatrix.SelectRow(iIndex, True, False)
                    iLastselectedRow = iIndex
                    slastSearchedResult = sValueToSearch
                    oMatrix.SelectRow(iIndex, True, False)
                End If
            End If

        End Sub

        Private Function midpoint(ByVal iMin As Int32, ByVal iMax As Int32) As Int32
            Return CType(Math.Floor((iMin + iMax) / 2.0), Integer)
        End Function
        Private Function SearchValue(ByVal sColUID As String, ByVal sValueToSearch As String) As Int32
            Try
                Dim iMin As Int32 = 1, iMax As Int32 = oMatrix.RowCount
                While (iMax >= iMin)
                    Dim imid As Int32 = midpoint(iMin, iMax)
                    If ((CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim().StartsWith(sValueToSearch)) Then
                        Return imid
                    ElseIf (CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim() < (sValueToSearch) OrElse _
                            String.Compare((CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim(), (sValueToSearch)) < 0 Then
                        iMin = imid + 1
                    ElseIf (CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim() > (sValueToSearch) OrElse _
                            String.Compare((CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim(), (sValueToSearch)) > 0 Then
                        iMax = imid - 1

                    End If
                End While
                Return 1
            Catch ex As Exception
                Return 1
            End Try
        End Function

        Private Sub handleKeyEventForMatrixRowSelection(ByVal Keycode As Int32)
            If Keycode = 40 Then
                Dim row As Int32 = GetselectedRowIndex()
                If row < 1 Then
                    oMatrix.SelectRow(1, True, False)
                ElseIf row < oMatrix.RowCount Then
                    oMatrix.SelectRow(row + 1, True, False)
                End If
            ElseIf Keycode = 38 Then
                Dim row As Int32 = GetselectedRowIndex()
                If row <= 1 Then
                    oMatrix.SelectRow(1, True, False)
                ElseIf row <= oMatrix.RowCount Then
                    oMatrix.SelectRow(row - 1, True, False)
                End If
            ElseIf Keycode = 34 Then 'Pg Dn
                Dim row As Int32 = GetselectedRowIndex()
                If (row + 10) <= oMatrix.RowCount Then
                    oMatrix.SelectRow(row + 10, True, False)
                ElseIf row < 1 Then
                    oMatrix.SelectRow(1, True, False)
                Else
                    oMatrix.SelectRow(oMatrix.RowCount, True, False)
                End If
            ElseIf Keycode = 33 Then 'Pg Up
                Dim row As Int32 = GetselectedRowIndex()
                If (row - 10) > 0 Then
                    oMatrix.SelectRow(row - 10, True, False)
                ElseIf row <= 1 Then
                    oMatrix.SelectRow(1, True, False)
                Else
                    oMatrix.SelectRow(1, True, False)
                End If
            End If
        End Sub


        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.ItemUID = "1" AndAlso pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                Me.setChoosenData(oForm, BubbleEvent)
            End If
        End Sub

        Private Sub setChoosenData(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            If oMatrix.RowCount = 0 Then Exit Sub
            Dim i As Int32 = 0
            i = GetselectedRowIndex()
            If i > 0 AndAlso i <= oMatrix.RowCount Then
                Me.SetParentSharedData(i, oForm)
                Me.doReturnFromModalShared(oForm, True)
                BubbleEvent = False
            End If
        End Sub

        Private Overloads Sub SetParentSharedData(ByVal iRow As Int32, ByVal oForm As SAPbouiCOM.Form)
            bClosingFormByOKButton = True
            Me.SetParentSharedData(oForm, "ITMSGRPCOD", CType(oMatrix.Columns.Item("ItmsGrpCod").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "ITMSGRPNAM", CType(oMatrix.Columns.Item("ItmsGrpNam").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)

            Me.SetParentSharedData(oForm, "ITEMCODE", CType(oMatrix.Columns.Item("ItemCode").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "ITEMNAME", CType(oMatrix.Columns.Item("ItemName").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)

            Me.SetParentSharedData(oForm, "SUOM", CType(oMatrix.Columns.Item("SalUnitMsr").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "PUOM", CType(oMatrix.Columns.Item("BuyUnitMsr").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)

            Me.SetParentSharedData(oForm, "DEFWEI", CType(oMatrix.Columns.Item("U_WTDFW").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "USEDEFWEI", CType(oMatrix.Columns.Item("U_WTUDW").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)

        End Sub

    End Class
End Namespace