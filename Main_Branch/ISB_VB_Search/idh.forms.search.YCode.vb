Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class YCode
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDH_YCDSR", "YCode Search.srf", 5, 45, 603, 320, "YCode Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_YCODE", Nothing, Nothing, False, True))
            oGridN.setOrderValue("Code")

            oGridN.doAddFilterField("IDH_YCOD", "U_YCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_YDES", "U_YDesc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100)

            oGridN.doAddListField("U_YCode", "YCode", False, -1, Nothing, "IDH_YCODE")
            oGridN.doAddListField("U_YDesc", "Description", False, -1, Nothing, "IDH_YDESC")
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace