Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Imports com.idh.bridge.lookups
Imports com.idh.bridge

Namespace idh.forms.search
    Public Class BPSearchAlternate
        Inherits IDHAddOns.idh.forms.Search


        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "BPALTSCH", "BPAlternateSearch.srf", 5, 40, 770, 360, "BP Alternate Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("[@IDH_WTUN]")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("CRD1", "ba", Nothing, False, True), True)
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OCRD", "bp", Nothing, False, True), True)

            oGridN.setOrderValue("bp.CardCode")

            'oGridN.doAddFilterField("IDH_CHMCD", "U_ChemicalCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            'oGridN.doAddFilterField("IDH_CHMNM", "U_ChemicalNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            oGridN.doAddFilterField("IDH_BPCOD", "ba.CardCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDH_NAME", "bp.CardName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDH_TYPE", "bp.CardType", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_GROUP", "bp.GroupCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_BRANCH", "bp.U_IDHBRAN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_ADRES", "ba.Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 150)


            oGridN.doAddListField("ba.CardCode", "Customer", False, -1, Nothing, "CARDCODE", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("bp.CardName", "Name", False, -1, Nothing, "CARDNAME")
            oGridN.doAddListField("ba.Address", "Address", False, -1, Nothing, "ADDRESS")
            oGridN.doAddListField("ba.Street", "Street", False, -1, Nothing, "STREET")
            oGridN.doAddListField("ba.Block", "Block", False, -1, Nothing, "BLOCK")
            oGridN.doAddListField("ba.ZipCode", "Postal Code", False, -1, Nothing, "ZIPCODE")
            oGridN.doAddListField("ba.City", "City", False, -1, Nothing, "CITY")
            oGridN.doAddListField("ba.County", "County", False, -1, Nothing, "COUNTY")
            oGridN.doAddListField("ba.STATE", "State Cd", False, -1, Nothing, "STATE")
            oGridN.doAddListField("(SELECT st.NAME FROM OCST st WHERE st.CODE = ba.STATE AND st.COUNTRY = ba.COUNTRY)", "State", False, -1, Nothing, "STATENM")

            'oGridN.doAddListField("U_WTUST", "Quantity", False, -1, Nothing, "IDH_WTUST")
            'oGridN.doAddListField("U_WTUFP", "Flashpoint", False, -1, Nothing, "IDH_WTUFP")
            'oGridN.doAddListField("U_WTUDE", "Density", False, -1, Nothing, "IDH_WTUDE")
            'oGridN.doAddListField("U_WTUPH", "PH", False, -1, Nothing, "IDH_WTUPH")
            'oGridN.doAddListField("U_WTUPG", "Pack Group", False, -1, Nothing, "IDH_WTUPG")
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            doFillGroups(oForm)
            doFillBranches(oForm)

            MyBase.doReadInputParams(oForm)
            Dim sVal As String = getParentSharedDataAsString(oForm, "IDH_TYPE")
            If Not sVal Is Nothing Then
                If sVal.StartsWith("F-") Then
                    sVal = sVal.Substring(2)
                    doFillBPTypes(oForm)
                    'OnTime [#Ico000????] USA 
                    'START
                    'Enabling Customer Type Dropdown to allow selection of 'Lead' customer 
                    If Config.INSTANCE.getParameterAsBool("USAREL", False) Then
                        setEnableItem(oForm, True, "IDH_TYPE")
                    Else
                        setEnableItem(oForm, False, "IDH_TYPE")
                    End If
                    'END 
                    'OnTime [#Ico000????] USA 
                Else
                    doFillBPTypes(oForm, sVal)
                    If sVal.Length > 1 Then
                        sVal = sVal.Substring(0, 1)
                    End If
                    setEnableItem(oForm, True, "IDH_TYPE")
                End If
                setUFValue(oForm, "IDH_TYPE", sVal)
            Else
                doFillBPTypes(oForm)
            End If

        End Sub

        Private Sub doFillBranches(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_BRANCH", "OUBR", "Code", "Remarks", Nothing, "Code", True)
        End Sub

        Private Sub doFillGroups(ByVal oForm As SAPbouiCOM.Form)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                Dim oGroups As SAPbouiCOM.ComboBox
                Dim oItem As SAPbouiCOM.Item
                Dim sBPType As String = oForm.DataSources.UserDataSources.Item("IDH_TYPE").ValueEx

                oRecordSet = CType(goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
                Dim sQry As String = "select GroupCode, GroupName from OCRG where GroupType Like '%" & sBPType & "'"
                oRecordSet.DoQuery(sQry)
                If oRecordSet.RecordCount > 0 Then
                    oItem = oForm.Items.Item("IDH_GROUP")
                    oItem.DisplayDesc = True
                    oGroups = CType(oItem.Specific, SAPbouiCOM.ComboBox)
                    Dim iCount As Integer
                    Dim oVal1 As String
                    Dim oVal2 As String

                    Dim oValValues As SAPbouiCOM.ValidValues
                    oValValues = oGroups.ValidValues
                    'First Clear all the values
                    If Not oValValues Is Nothing Then
                        While oValValues.Count > 0
                            oValValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                        End While
                    End If

                    oValValues.Add("", getTranslatedWord("Any"))
                    For iCount = 0 To oRecordSet.RecordCount - 1
                        oVal1 = CType(oRecordSet.Fields.Item(0).Value, String)
                        oVal2 = CType(oRecordSet.Fields.Item(1).Value, String)
                        oValValues.Add(oVal1, oVal2)
                        oRecordSet.MoveNext()
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Groups.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Groups")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

        '* A Any
        '* S Supplier
        '* C Customer
        '* L Lead
        Private Sub doFillBPTypes(ByVal oForm As SAPbouiCOM.Form, Optional ByVal sSwitch As String = "ASCL")
            Dim oBPType As SAPbouiCOM.ComboBox
            Dim oItem As SAPbouiCOM.Item

            oItem = oForm.Items.Item("IDH_TYPE")
            oItem.DisplayDesc = True
            oBPType = CType(oItem.Specific, SAPbouiCOM.ComboBox)

            If oBPType.ValidValues.Count > 0 Then
                doClearValidValues(oBPType.ValidValues)
            End If

            If sSwitch.Length = 0 Then
                oBPType.ValidValues.Add("", getTranslatedWord("Any"))
                oBPType.ValidValues.Add("S", getTranslatedWord("Vendor"))
                oBPType.ValidValues.Add("C", getTranslatedWord("Customer"))
                oBPType.ValidValues.Add("L", getTranslatedWord("Lead"))
            Else
                If sSwitch.IndexOf("A") > -1 Then
                    oBPType.ValidValues.Add("", getTranslatedWord("Any"))
                End If
                If sSwitch.IndexOf("S") > -1 Then
                    oBPType.ValidValues.Add("S", getTranslatedWord("Vendor"))
                End If
                If sSwitch.IndexOf("C") > -1 Then
                    oBPType.ValidValues.Add("C", getTranslatedWord("Customer"))
                End If
                If sSwitch.IndexOf("L") > -1 Then
                    oBPType.ValidValues.Add("L", getTranslatedWord("Lead"))
                End If
            End If
        End Sub


    End Class
End Namespace