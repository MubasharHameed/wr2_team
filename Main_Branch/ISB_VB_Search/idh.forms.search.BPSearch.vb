Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge.lookups
Imports com.idh.bridge

Namespace idh.forms.search
    Public Class BPSearch
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHCSRCH", "BP Search.srf", 5, 40, 770, 360, "BP Search")
        End Sub
        
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sType As String, ByVal sSRF As String, ByVal iGL As Integer, ByVal iGT As Integer, ByVal iGW As Integer, ByVal iGH As Integer, ByVal sTitle As String)
            MyBase.New(oParent, sType, sSRF, iGL, iGT, iGW, iGH, sTitle)
        End Sub

        '## Start 16-06-2014
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            'setParentSharedData(oForm, "bClosingFormByOKButton", "False")
            'bClosingFormByOKButton = False
            Me.setParentSharedData(oForm, "IDH_ITM1SRCHOPEND", "LOADED")
            MyBase.doLoadData(oForm)
        End Sub

        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
        End Sub
        '## End

		Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
			Dim bDoCompliance As Boolean = False
			Dim bDoUsa As Boolean = False
			Dim bDoSITESIP As Boolean = True
			If getHasSharedData(oGridN.getSBOForm) = True Then
				Dim sWValue As String
				
                sWValue = getParentSharedDataAsString(oGridN.getSBOForm, "ISCOMPLIANCE")
				bDoCompliance = sWValue Is Nothing OrElse sWValue.Equals("TRUE")
				
                sWValue = getParentSharedDataAsString(oGridN.getSBOForm, "DOSIP")
				bDoSITESIP = Not (sWValue Is Nothing) AndAlso sWValue.Equals("TRUE")
				'bDoSITESIP = idh.const.Lookup.INSTANCE.getParameterAsBool("WODSPSP", False)

                bDoUsa = Config.INSTANCE.getParameterAsBool("USAREL", False)
            End If

            Dim sStripZerro As String = getParentSharedDataAsString(oGridN.getSBOForm, "STRIPZERROCOLS")
            If sStripZerro IsNot Nothing AndAlso sStripZerro = "TRUE" Then
                oGridN.StripZerroColumns = True
            End If
            
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OCRD", "b", Nothing, False, True))
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OCRG", "g", Nothing, False, True))

            If bDoSITESIP Then
                oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_SUITPR", "s", Nothing, False, True))
                oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_HAULCST", "h", Nothing, False, True))
                oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_MILEAGES", "m", Nothing, False, True))
            Else
                oGridN.doAddGridTable(New com.idh.bridge.data.GridTable(Config.INSTANCE.getBalanceView(), "c"))
			End If
			
            Dim sRequired As String
            Dim sISCS As String = getParentSharedDataAsString(oGridN.getSBOForm(), "IDH_CS")
            If (Not sISCS Is Nothing AndAlso sISCS.Length > 0) Then
                sRequired = " AND U_IDHISCS = '" & sISCS & "' "
            Else
                sRequired = ""
            End If
            
            If bDoSITESIP Then
                Dim sDate As String = com.idh.utils.dates.doSimpleSQLDate(DateTime.Now)
                Dim sCustZp As String = getParentSharedDataAsString(oGridN.getSBOForm(), "IDH_CUSTZP")
                Dim sContainerCd As String = getParentSharedDataAsString(oGridN.getSBOForm(), "IDH_CONTCD")
                'sRequired = "b.GroupCode = g.GroupCode AND (b.U_IDH_DUSJB is NULL Or Upper(b.U_IDH_DUSJB) != 'TRUE') AND b.CardCode=s.U_CardCd" & sRequired & " AND b.FrozenFor <> 'Y'" & _
                '            " AND CAST(CONVERT(VARCHAR, s.U_StDate,101) AS DATETIME) <= CAST(CONVERT(VARCHAR, '" + sDate + "',101) AS DATETIME) " & _
                '            " AND CAST(CONVERT(VARCHAR, s.U_EnDate,101) As DATETIME) >= CAST(CONVERT(VARCHAR, '" + sDate + "',101) AS DATETIME) " & _
                '            " AND (m.U_SupZip = s.U_SupZipCd " & _
                '            "   AND " & _
                '            "       '" & sCustZp & "' LIKE m.U_CustZip + '%' ) " & _
                '            " AND m.U_Distance > 0 " & _
                '            " AND (SELECT MIN(U_OneWDst) As HCDist FROM [@IDH_HAULCST] WHERE U_OneWDst > m.U_Distance) = h.U_OneWDst " & _
                '            " AND h.U_UOM = s.U_UOM " & _
                '            " AND ( " & _
                '            "      (h.U_ItemCd Is Null Or s.U_ItemCd Is Null) " & _
                '            "      OR h.U_ItemCd = s.U_ItemCd " & _
                '            "      OR ( h.U_ItemCd = '' OR s.U_ItemCd = '' ) " & _
                '            " ) "

                '20140728 - Updated clause to filter where the Supplier's ContainerCd = 'Something' OR IsEmpty
                'sRequired = "b.GroupCode = g.GroupCode AND (b.U_IDH_DUSJB is NULL Or Upper(b.U_IDH_DUSJB) != 'TRUE') AND b.CardCode=s.U_CardCd" & sRequired & " AND b.FrozenFor <> 'Y'" & _
                'The code needs to check for Active Customers
                sRequired = "b.GroupCode = g.GroupCode AND (b.U_IDH_DUSJB is NULL Or Upper(b.U_IDH_DUSJB) != 'TRUE') AND b.CardCode=s.U_CardCd" & sRequired &
                            " AND CAST(CONVERT(VARCHAR, s.U_StDate,101) AS DATETIME) <= CAST(CONVERT(VARCHAR, '" + sDate + "',101) AS DATETIME) " &
                            " AND CAST(CONVERT(VARCHAR, s.U_EnDate,101) As DATETIME) >= CAST(CONVERT(VARCHAR, '" + sDate + "',101) AS DATETIME) " &
                            " AND (m.U_SupZip = s.U_SupZipCd " &
                            "   AND " &
                            "       '" & sCustZp & "' LIKE m.U_CustZip + '%' ) " &
                            " AND m.U_Distance > 0 " &
                            " AND (SELECT MIN(U_OneWDst) As HCDist FROM [@IDH_HAULCST] WHERE U_OneWDst > m.U_Distance) = h.U_OneWDst " &
                            " AND h.U_UOM = s.U_UOM " &
                            " AND ( " &
                            "      ISNULL(h.U_ItemCd, '') = '' OR  " &
                            "      ISNULL(s.U_ItemCd, '') = '' OR " &
                            "      h.U_ItemCd = s.U_ItemCd " &
                            " ) "
                '20141121: OVDs: Have commented the container code clause as OVD want this to be in previous state. 
                'have added wrconfig key for them to switch between the two. 
                '& _
                '" AND (s.U_ItemCd = '" & sContainerCd & "' OR IsNull(s.U_ItemCd, '') = '') "
                If Config.INSTANCE.getParameterAsBool("OVCONTCL", True) Then
                    sRequired = sRequired + " AND (s.U_ItemCd = '" & sContainerCd & "' OR IsNull(s.U_ItemCd, '') = '') "
                End If
            Else
                'sRequired = "b.GroupCode = g.GroupCode AND (b.U_IDH_DUSJB is NULL Or Upper(b.U_IDH_DUSJB) != 'TRUE') AND b.CardCode=c.CardCode" & sRequired & " AND b.FrozenFor <> 'Y'"

                'Commented
                'sRequired = "b.GroupCode = g.GroupCode AND (b.U_IDH_DUSJB is NULL Or Upper(b.U_IDH_DUSJB) != 'TRUE') AND b.CardCode=c.CardCode" & sRequired &
                'Config.INSTANCE.getActiveCheckSQL("b", DateTime.Now)

                '#MA Start 14-04-2017
                sRequired = "b.GroupCode = g.GroupCode AND (b.U_IDH_DUSJB is NULL Or Upper(b.U_IDH_DUSJB) != 'TRUE') AND b.CardCode=c.CardCode" & sRequired
                If Config.INSTANCE.doCheckCustomerActivePBI() = False Then 'If config is false then check customer is active or not else skip this block
                    sRequired = sRequired & Config.INSTANCE.getActiveCheckSQL("b", DateTime.Now)
                End If
                '#MA End 14-04-2017

            End If
			
            oGridN.setRequiredFilter(sRequired)

            oGridN.doAddFilterField("IDH_BPCOD", "b.CardCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDH_NAME", "b.CardName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDH_TYPE", "b.CardType", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_GROUP", "b.GroupCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_BRANCH", "b.U_IDHBRAN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

			If bDoSITESIP Then
	            oGridN.doAddListField("b.CardCode", "Code", False, 50, Nothing, "CARDCODE", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
	            oGridN.doAddListField("b.CardName", "Name", False, 150, Nothing, "CARDNAME")
                oGridN.doAddListField("b.CardFName", "Foreign Name", False, 0, Nothing, "CARDFNAME")
                oGridN.doAddListField("b.CardType", "Type", False, 0, Nothing, "CARDTYPE")
	            oGridN.doAddListField("b.GroupCode", "Group Code", False, 0, Nothing, "GROUPCODE")
	            oGridN.doAddListField("g.GroupName", "Group Name", False, 0, Nothing, "GROUPNAME")
	            oGridN.doAddListField("s.U_SupAddr", "Supplier Address", False, -1, Nothing, "SUPADDR")
                oGridN.doAddListField("m.U_SupZip", "Sup. Postal Code", False, -1, Nothing, "SUPZIP")
	            oGridN.doAddListField("s.U_WasteCd", "Waste Code", False, -1, Nothing, "WASTCD")
				oGridN.doAddListField("s.U_WasteDs", "Waste Description", False, -1, Nothing, "WASTDS")
				oGridN.doAddListField("s.U_StAddr", "Customer Address", False, -1, Nothing, "CUSTADDR")
                oGridN.doAddListField("m.U_CustZip", "Cust. Postal Code", False, -1, Nothing, "CUSTZIP")
                'oGridN.doAddListField("m.U_Distance", "Distance", False, -1, Nothing, "DISTANCE")
                oGridN.doAddListField("m.U_Duration", "Duration", False, -1, Nothing, "DURATION")
                oGridN.doAddListField("h.U_LoadPD", "Loads Per Day", False, -1, Nothing, "LOADPDAY")
                oGridN.doAddListField("h.U_HaulChrg", "Haulage Chrg", False, -1, Nothing, "HAULAGECHRG")
                oGridN.doAddListField("h.U_PlusOH", "Haulage Cst", False, -1, Nothing, "HAULAGECST")
				oGridN.doAddListField("s.U_TipTon", "Tip", False, -1, Nothing, "TIP")
				oGridN.doAddListField("s.U_UOM", "UOM", False, -1, Nothing, "UOM")
                oGridN.doAddListField("(h.U_PlusOH + s.U_TipTon)", "Total", False, -1, Nothing, "total")
                oGridN.doAddListField("s.U_LinkNr", "LINK", False, 0, Nothing, "LINK")
                oGridN.doAddListField("b.Currency", "Currency", False, 0, Nothing, "CURRENCY")

	            If bDoCompliance Then
	                oGridN.doAddListField("b.OrdersBal", "Orders", False, 0, Nothing, "ORDERS")
	                oGridN.doAddListField("b.CreditLine", "Credit Limit", False, 0, Nothing, "CREDITLINE")
	                oGridN.doAddListField("b.U_IDHBRAN", "Branch", False, 0, Nothing, "BRANCH")
	            Else
	                oGridN.doAddListField("b.OrdersBal", "Orders", False, 30, Nothing, "ORDERS")
	                oGridN.doAddListField("b.CreditLine", "Credit Limit", False, 30, Nothing, "CREDITLINE")
	                oGridN.doAddListField("b.U_IDHBRAN", "Branch", False, 20, Nothing, "BRANCH")
	            End If
	
	            oGridN.doAddListField("b.Balance", "FC Balance", False, 0, Nothing, "BALANCEFC")
	            oGridN.doAddListField("b.Phone1", "Phone", False, 0, Nothing, "PHONE1")
	            oGridN.doAddListField("b.CntctPrsn", "Contact Person", False, 0, Nothing, "CNTCTPRSN")
	            oGridN.doAddListField("b.U_WASLIC", "Waste Lic. Reg. No.", False, 0, Nothing, "WASLIC")
	            oGridN.doAddListField("b.U_IDHICL", "Don't do Credit Check", False, 0, Nothing, "IDHICL")
	            oGridN.doAddListField("b.U_IDHLBPC", "Linked BP Code", False, 0, Nothing, "IDHLBPC")
	            oGridN.doAddListField("b.U_IDHLBPN", "Linked BP Name", False, 0, Nothing, "IDHLBPN")
	            oGridN.doAddListField("b.U_IDHOBLGT", "Obligated", False, 0, Nothing, "IDHOBLGT")
	            oGridN.doAddListField("b.U_IDHUOM", "BP UOM", False, 0, Nothing, "IDHUOM")
	            oGridN.doAddListField("b.U_IDHONCS", "Compliance Scheme", False, 0, Nothing, "IDHONCS")
	            oGridN.doAddListField("b.ListNum", "Price List", False, 0, Nothing, "PLIST")
                oGridN.doAddListField("b.GroupNum", "Payment Terms", False, 0, Nothing, "PAYTRM")
                oGridN.doAddListField("s.U_ItemCd", "Container", False, -1, Nothing, "CONTAINER")

                'oGridN.doAddFilterField("IDH_ZIPCOD", "s.U_ZpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "ILIKE-R", 30)
                oGridN.doAddFilterField("IDH_ZIPCOD", "s.U_SupZipCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
                oGridN.doAddFilterField("IDH_WSTCD", "s.U_WasteCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 50)
                oGridN.doAddFilterField("IDH_CONTCD", "s.U_ItemCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 50)

				Dim oItem As SAPbouiCOM.Item
				'HIDE ITEMS
				setVisible(oGridN.getSBOForm(), "LBL_ADD", False)
				setVisible(oGridN.getSBOForm(), "IDH_ADRES", False)
				
				setVisible(oGridN.getSBOForm(), "LBL_STREET", False)
				setVisible(oGridN.getSBOForm(), "IDH_STREET", False)

				'SHOW ITEMS
				oItem = setVisible(oGridN.getSBOForm(), "IDH_LBLZIP", True)
                If oItem.Top > 7 Then
                	oItem.Top = 5
                End If
                
                oItem = setVisible(oGridN.getSBOForm(), "IDH_ZIPCOD", True)
                If oItem.Top > 25 Then
                	oItem.Top = 20
                End If
                
                oItem = setVisible(oGridN.getSBOForm(), "IDH_LBLWST", True)
                If oItem.Top > 7 Then
                	oItem.Top = 5
                End If
                
                oItem = setVisible(oGridN.getSBOForm(), "IDH_WSTCD", True)
                If oItem.Top > 25 Then
                	oItem.Top = 20
                End If

                'Set Show maps buttone visible true 
                oItem = setVisible(oGridN.getSBOForm(), "IDH_MAPS", True)

                oGridN.setOrderValue("s.U_ZpCd DESC, m.U_Distance, (h.U_PlusOH + s.U_TipTon) ASC")

                oGridN.getSBOForm().Refresh()
			Else
            
	            oGridN.doAddListField("b.CardCode", "Code", False, 50, Nothing, "CARDCODE", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
	            oGridN.doAddListField("b.CardName", "Name", False, 150, Nothing, "CARDNAME")
                oGridN.doAddListField("b.CardFName", "Foreign Name", False, 0, Nothing, "CARDFNAME")
                oGridN.doAddListField("b.CardType", "Type", False, 40, Nothing, "CARDTYPE")

	            oGridN.doAddListField("b.GroupCode", "Group Code", False, 0, Nothing, "GROUPCODE")
	            oGridN.doAddListField("g.GroupName", "Group Name", False, 150, Nothing, "GROUPNAME")

	            If bDoCompliance Then
	                oGridN.doAddListField("c.RemainBal", "Credit Remain", False, 0, Nothing, "CRREMAIN")
	                oGridN.doAddListField("b.OrdersBal", "Orders", False, 0, Nothing, "ORDERS")
	                oGridN.doAddListField("b.CreditLine", "Credit Limit", False, 0, Nothing, "CREDITLINE")
	                oGridN.doAddListField("c.Balance", "Balance", False, 0, Nothing, "BALANCE")
	                oGridN.doAddListField("c.TBalance", "Total Balance", False, 0, Nothing, "TBALANCE")
	                oGridN.doAddListField("c.WRBalance", "WR1 Orders", False, 0, Nothing, "WRORDERS")
	                oGridN.doAddListField("b.U_IDHBRAN", "Branch", False, 0, Nothing, "BRANCH")
	            Else
	                oGridN.doAddListField("c.RemainBal", "Credit Remain", False, 30, Nothing, "CRREMAIN")
	                oGridN.doAddListField("b.OrdersBal", "Orders", False, 30, Nothing, "ORDERS")
	                oGridN.doAddListField("b.CreditLine", "Credit Limit", False, 30, Nothing, "CREDITLINE")
	                oGridN.doAddListField("c.Balance", "Balance", False, 30, Nothing, "BALANCE")
	                oGridN.doAddListField("c.TBalance", "Total Balance", False, -1, Nothing, "TBALANCE")
	                oGridN.doAddListField("c.WRBalance", "WR1 Orders", False, 30, Nothing, "WRORDERS")
	                oGridN.doAddListField("b.U_IDHBRAN", "Branch", False, 20, Nothing, "BRANCH")
	            End If
	
	            oGridN.doAddListField("b.Balance", "FC Balance", False, 0, Nothing, "BALANCEFC")
	            oGridN.doAddListField("c.Deviation", "Deviation", False, 0, Nothing, "DEVIATION")
	            oGridN.doAddListField("c.frozenFor", "Frozen", False, 0, Nothing, "FROZEN")
	            oGridN.doAddListField("c.FrozenComm", "FrozenComm", False, 0, Nothing, "FROZENC")
	            oGridN.doAddListField("b.Phone1", "Phone", False, 0, Nothing, "PHONE1")
	            oGridN.doAddListField("b.CntctPrsn", "Contact Person", False, 0, Nothing, "CNTCTPRSN")
	            oGridN.doAddListField("b.U_WASLIC", "Waste Lic. Reg. No.", False, 0, Nothing, "WASLIC")
	            oGridN.doAddListField("b.U_IDHICL", "Don't do Credit Check", False, 0, Nothing, "IDHICL")
	            oGridN.doAddListField("b.U_IDHLBPC", "Linked BP Code", False, 0, Nothing, "IDHLBPC")
	            oGridN.doAddListField("b.U_IDHLBPN", "Linked BP Name", False, 0, Nothing, "IDHLBPN")
	            oGridN.doAddListField("b.U_IDHOBLGT", "Obligated", False, 0, Nothing, "IDHOBLGT")
	            oGridN.doAddListField("b.U_IDHUOM", "BP UOM", False, 0, Nothing, "IDHUOM")
	            oGridN.doAddListField("b.U_IDHONCS", "Compliance Scheme", False, 0, Nothing, "IDHONCS")
	            oGridN.doAddListField("c.frozenFrom", "Frozen From", False, 0, Nothing, "FROZENF")
	            oGridN.doAddListField("c.frozenTo", "Frozen To", False, 0, Nothing, "FROZENT")
	            oGridN.doAddListField("b.ListNum", "Price List", False, 0, Nothing, "PLIST")
	            oGridN.doAddListField("b.GroupNum", "Payment Terms", False, 0, Nothing, "PAYTRM")
                oGridN.doAddListField("b.Currency", "Currency", False, 0, Nothing, "CURRENCY")

                '##MA Start 15-02-2017
                'oGridN.doAddListField("b.U_Freist", "Freistellungsnummer", False, 0, Nothing, "FREISTELL")
                '##MA End 15-02-2017

	            'USA Requirement
	            If bDoUsa Then
	                oGridN.doAddFilterField("IDH_ADRES", "b.BillToDef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 150)
	                oGridN.doAddFilterField("IDH_STREET", "b.Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 150)
	
	                oGridN.doAddListField("b.BillToDef", "Billing Address", False, 150, Nothing, "ADDRESS")
	                oGridN.doAddListField("b.Address", "Street", False, -1, Nothing, "MAILADD")
	                oGridN.doAddListField("b.ZipCode", "Zip Code", False, -1, Nothing, "ZIPCODE")

	            Else
                    'Dim oItem As SAPbouiCOM.Item
	                Try
	                    setVisible(oGridN.getSBOForm(), "LBL_ADD", False)
	                    setVisible(oGridN.getSBOForm(), "IDH_ADRES", False)
	                    setVisible(oGridN.getSBOForm(), "LBL_STREET", False)
	                    setVisible(oGridN.getSBOForm(), "IDH_STREET", False)

	                Catch ex As Exception
	                End Try
	                
					'HIDE ITEMS
					setVisible(oGridN.getSBOForm(), "IDH_LBLZIP", False)
	                setVisible(oGridN.getSBOForm(), "IDH_ZIPCOD", False)
                End If
                setVisible(oGridN.getSBOForm(), "IDH_LBLWST", False)
                setVisible(oGridN.getSBOForm(), "IDH_WSTCD", False)

			End If
		End Sub

        '        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
        '            'oGridN.setTableValue("OCRD b, OCRG g, " & idh.const.Lookup.INSTANCE.getBalanceView(goParent) & " c") 'IDH_VWR1BAL
        '            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OCRD", "b"))
        '            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OCRG", "g"))
        '            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable(idh.const.Lookup.INSTANCE.getBalanceView(goParent), "c"))
        '
        '            Dim sRequired As String
        '            Dim sISCS As String = getParentSharedData(oGridN.getSBOForm(), "IDH_CS")
        '            If (Not sISCS Is Nothing AndAlso sISCS.Length > 0) Then
        '                sRequired = " AND U_IDHISCS = '" & sISCS & "' "
        '            Else
        '                sRequired = ""
        '            End If
        '            sRequired = "b.GroupCode = g.GroupCode AND (b.U_IDH_DUSJB is NULL Or Upper(b.U_IDH_DUSJB) != 'TRUE') AND b.CardCode=c.CardCode" & sRequired & " AND b.FrozenFor <> 'Y'"
        '
        '            oGridN.setRequiredFilter(sRequired)
        '
        '            oGridN.doAddFilterField("IDH_BPCOD", "b.CardCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
        '            oGridN.doAddFilterField("IDH_NAME", "b.CardName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
        '            oGridN.doAddFilterField("IDH_TYPE", "b.CardType", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
        '            oGridN.doAddFilterField("IDH_GROUP", "b.GroupCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
        '            oGridN.doAddFilterField("IDH_BRANCH", "b.U_IDHBRAN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
        '
        '            oGridN.doAddListField("b.CardCode", "Code", False, 50, Nothing, "CARDCODE", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
        '            oGridN.doAddListField("b.CardName", "Name", False, 150, Nothing, "CARDNAME")
        '            oGridN.doAddListField("b.CardType", "Type", False, 40, Nothing, "CARDTYPE")
        '            oGridN.doAddListField("b.GroupCode", "Group Code", False, 0, Nothing, "GROUPCODE")
        '            oGridN.doAddListField("g.GroupName", "Group Name", False, 150, Nothing, "GROUPNAME")
        '
        '            Dim sIsCompliance As String = ""
        '            Dim bListSet As Boolean = False
        '            If getHasSharedData(oGridN.getSBOForm) = True Then
        '                sIsCompliance = getParentSharedData(oGridN.getSBOForm, "ISCOMPLIANCE")
        '                If sIsCompliance Is Nothing OrElse sIsCompliance.Equals("TRUE") Then
        '                    oGridN.doAddListField("c.RemainBal", "Credit Remain", False, 0, Nothing, "CRREMAIN")
        '                    oGridN.doAddListField("b.OrdersBal", "Orders", False, 0, Nothing, "ORDERS")
        '                    oGridN.doAddListField("b.CreditLine", "Credit Limit", False, 0, Nothing, "CREDITLINE")
        '                    oGridN.doAddListField("c.Balance", "Balance", False, 0, Nothing, "BALANCE")
        '                    oGridN.doAddListField("c.TBalance", "Total Balance", False, 0, Nothing, "TBALANCE")
        '                    oGridN.doAddListField("c.WRBalance", "WR1 Orders", False, 0, Nothing, "WRORDERS")
        '                    oGridN.doAddListField("b.U_IDHBRAN", "Branch", False, 0, Nothing, "BRANCH")
        '                    bListSet = True
        '                End If
        '            End If
        '
        '            If bListSet = False Then
        '                oGridN.doAddListField("c.RemainBal", "Credit Remain", False, 30, Nothing, "CRREMAIN")
        '                oGridN.doAddListField("b.OrdersBal", "Orders", False, 30, Nothing, "ORDERS")
        '                oGridN.doAddListField("b.CreditLine", "Credit Limit", False, 30, Nothing, "CREDITLINE")
        '                oGridN.doAddListField("c.Balance", "Balance", False, 30, Nothing, "BALANCE")
        '                oGridN.doAddListField("c.TBalance", "Total Balance", False, -1, Nothing, "TBALANCE")
        '                oGridN.doAddListField("c.WRBalance", "WR1 Orders", False, 30, Nothing, "WRORDERS")
        '                oGridN.doAddListField("b.U_IDHBRAN", "Branch", False, 20, Nothing, "BRANCH")
        '            End If
        '
        '            oGridN.doAddListField("b.Balance", "FC Balance", False, 0, Nothing, "BALANCEFC")
        '            oGridN.doAddListField("c.Deviation", "Deviation", False, 0, Nothing, "DEVIATION")
        '            oGridN.doAddListField("c.frozenFor", "Frozen", False, 0, Nothing, "FROZEN")
        '            oGridN.doAddListField("c.FrozenComm", "FrozenComm", False, 0, Nothing, "FROZENC")
        '            oGridN.doAddListField("b.Phone1", "Phone", False, 0, Nothing, "PHONE1")
        '            oGridN.doAddListField("b.CntctPrsn", "Contact Person", False, 0, Nothing, "CNTCTPRSN")
        '            oGridN.doAddListField("b.U_WASLIC", "Waste Lic. Reg. No.", False, 0, Nothing, "WASLIC")
        '            oGridN.doAddListField("b.U_IDHICL", "Don't do Credit Check", False, 0, Nothing, "IDHICL")
        '            oGridN.doAddListField("b.U_IDHLBPC", "Linked BP Code", False, 0, Nothing, "IDHLBPC")
        '            oGridN.doAddListField("b.U_IDHLBPN", "Linked BP Name", False, 0, Nothing, "IDHLBPN")
        '            oGridN.doAddListField("b.U_IDHOBLGT", "Obligated", False, 0, Nothing, "IDHOBLGT")
        '            oGridN.doAddListField("b.U_IDHUOM", "BP UOM", False, 0, Nothing, "IDHUOM")
        '            oGridN.doAddListField("b.U_IDHONCS", "Compliance Scheme", False, 0, Nothing, "IDHONCS")
        '            oGridN.doAddListField("c.frozenFrom", "Frozen From", False, 0, Nothing, "FROZENF")
        '            oGridN.doAddListField("c.frozenTo", "Frozen To", False, 0, Nothing, "FROZENT")
        '            oGridN.doAddListField("b.ListNum", "Price List", False, 0, Nothing, "PLIST")
        '            oGridN.doAddListField("b.GroupNum", "Payment Terms", False, 0, Nothing, "PAYTRM")
        '
        '            'USA Requirement
        '            If idh.const.Lookup.INSTANCE.getParameterAsBool("USAREL", False) = True Then
        '                oGridN.doAddFilterField("IDH_ADRES", "b.BillToDef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 150)
        '                oGridN.doAddFilterField("IDH_STREET", "b.Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 150)
        '
        '                oGridN.doAddListField("b.BillToDef", "Billing Address", False, 150, Nothing, "ADDRESS")
        '                oGridN.doAddListField("b.Address", "Street", False, -1, Nothing, "MAILADD")
        '                oGridN.doAddListField("b.ZipCode", "Zip Code", False, -1, Nothing, "ZIPCODE")
        '            Else
        '                Dim oItem As SAPbouiCOM.Item
        '                Try
        '                    oItem = (oGridN.getSBOForm()).Items.Item("LBL_ADD")
        '                    oItem.Visible = False
        '
        '                    oItem = (oGridN.getSBOForm()).Items.Item("IDH_ADRES")
        '                    oItem.Visible = False
        '
        '                    oItem = (oGridN.getSBOForm()).Items.Item("LBL_STREET")
        '                    oItem.Visible = False
        '
        '                    oItem = (oGridN.getSBOForm()).Items.Item("IDH_STREET")
        '                    oItem.Visible = False
        '
        '                Catch ex As Exception
        '                End Try
        '            End If
        '        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)
        End Sub

        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            doFillGroups(oForm)
            doFillBranches(oForm)

            MyBase.doReadInputParams(oForm)
            setParentSharedData(oForm, "CALLEDITEM", getParentSharedData(oForm, "CALLEDITEM"))
            Dim sVal As String = getParentSharedDataAsString(oForm, "IDH_TYPE")
            If Not sVal Is Nothing Then
                If sVal.StartsWith("F-") Then
                    sVal = sVal.Substring(2)
                    doFillBPTypes(oForm)
                    'OnTime [#Ico000????] USA 
                    'START
                    'Enabling Customer Type Dropdown to allow selection of 'Lead' customer 
                    If Config.INSTANCE.getParameterAsBool("USAREL", False) Then
                        setEnableItem(oForm, True, "IDH_TYPE")
                    Else
                        setEnableItem(oForm, False, "IDH_TYPE")
                    End If
                    'END 
                    'OnTime [#Ico000????] USA 
                Else
                    doFillBPTypes(oForm, sVal)
                    If sVal.Length > 1 Then
                        sVal = sVal.Substring(0, 1)
                    End If
                    setEnableItem(oForm, True, "IDH_TYPE")
                End If
                If Not getParentSharedData(oForm, "CalledFromEVN") Then
                setUFValue(oForm, "IDH_TYPE", sVal)
                End If
                setParentSharedData(oForm, "CalledFromEVN", False)
            Else
                doFillBPTypes(oForm)
            End If

        End Sub

        Private Sub doFillBranches(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_BRANCH", "OUBR", "Code", "Remarks", Nothing, "Code", True)
        End Sub

        Private Sub doFillGroups(ByVal oForm As SAPbouiCOM.Form)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                Dim oGroups As SAPbouiCOM.ComboBox
                Dim oItem As SAPbouiCOM.Item
                Dim sBPType As String = oForm.DataSources.UserDataSources.Item("IDH_TYPE").ValueEx

                oRecordSet = CType(goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
                Dim sQry As String = "select GroupCode, GroupName from OCRG where GroupType Like '%" & sBPType & "'"
                oRecordSet.DoQuery(sQry)
                If oRecordSet.RecordCount > 0 Then
                    oItem = oForm.Items.Item("IDH_GROUP")
                    oItem.DisplayDesc = True
                    oGroups = CType(oItem.Specific, SAPbouiCOM.ComboBox)
                    Dim iCount As Integer
                    Dim sVal1 As String
                    Dim sVal2 As String

                    Dim oValValues As SAPbouiCOM.ValidValues
                    oValValues = oGroups.ValidValues
                    'First Clear all the values
                    If Not oValValues Is Nothing Then
                        While oValValues.Count > 0
                            oValValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                        End While
                    End If

                    oValValues.Add("", getTranslatedWord("Any"))
                    For iCount = 0 To oRecordSet.RecordCount - 1
                        sVal1 = CType(oRecordSet.Fields.Item(0).Value, String)
                        sVal2 = CType(oRecordSet.Fields.Item(1).Value, String)
                        oValValues.Add(sVal1, sVal2)
                        oRecordSet.MoveNext()
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Groups.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Groups")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

        '* A Any
        '* S Supplier
        '* C Customer
        '* L Lead
        Private Sub doFillBPTypes(ByVal oForm As SAPbouiCOM.Form, Optional ByVal sSwitch As String = "ASCL")
            Dim oBPType As SAPbouiCOM.ComboBox
            Dim oItem As SAPbouiCOM.Item

            oItem = oForm.Items.Item("IDH_TYPE")
            oItem.DisplayDesc = True
            oBPType = CType(oItem.Specific, SAPbouiCOM.ComboBox)

            If oBPType.ValidValues.Count > 0 Then
				doClearValidValues(oBPType.ValidValues)
            End If
            
            If sSwitch.Length = 0 Then
                If getParentSharedData(oForm, "CalledFromEVN") Then
                    oBPType.ValidValues.Add("S", getTranslatedWord("Vendor"))
                    oBPType.ValidValues.Add("C", getTranslatedWord("Customer"))
                    setUFValue(oForm, "IDH_TYPE", "S")
                    'setParentSharedData(oForm, "CalledFromEVN", False)
                Else
                oBPType.ValidValues.Add("", getTranslatedWord("Any"))
                oBPType.ValidValues.Add("S", getTranslatedWord("Vendor"))
                oBPType.ValidValues.Add("C", getTranslatedWord("Customer"))
                oBPType.ValidValues.Add("L", getTranslatedWord("Lead"))
                End If
            Else
                If sSwitch.IndexOf("A") > -1 Then
                    oBPType.ValidValues.Add("", getTranslatedWord("Any"))
                End If
                If sSwitch.IndexOf("S") > -1 Then
                    oBPType.ValidValues.Add("S", getTranslatedWord("Vendor"))
                End If
                If sSwitch.IndexOf("C") > -1 Then
                    oBPType.ValidValues.Add("C", getTranslatedWord("Customer"))
                End If
                If sSwitch.IndexOf("L") > -1 Then
                    oBPType.ValidValues.Add("L", getTranslatedWord("Lead"))
                End If
            End If
        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_TYPE" Then
                        doFillGroups(oForm)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_NEW" Then
                        setSharedData(oForm, "ACT", "ADD")
                        setSharedData(oForm, "TYPE", "C")
                        goParent.doOpenModalForm("134", oForm)
                    ElseIf pVal.ItemUID = "IDH_MAPS" Then
                        Dim sCustPostCode As String = getParentSharedDataAsString(oForm, "IDH_CUSTZP")
                        doGMaps(goParent, oForm, Config.Parameter("MAPURL").ToString() + "?" + "PC=" + sCustPostCode + "")
                        'If oForm.Items.Item("IDH_MAPS").Specific.Caption = getTranslatedWord("Show on Maps" Then)
                        '    oForm.Height = 727
                        '    oForm.Items.Item("IDH_MAPS").Specific.Caption = getTranslatedWord("Hide Maps")
                        'Else
                        '    oForm.Height = 465
                        '    oForm.Items.Item("IDH_MAPS").Specific.Caption = getTranslatedWord("Show on Maps")
                        'End If
                    End If
                End If
            End If
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD OrElse _
                    (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.setParentSharedData(oForm, "IDH_ITM1SRCHOPEND", "")
                    'If getParentSharedData(oForm, "bClosingFormByOKButton") = "False" Then''bClosingFormByOKButton = False Then
                    MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                    'End If
                End If
            End If
            Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
        End Function

        Public Shared Function doGetBPInfo(ByVal oParent As IDHAddOns.idh.addon.Base, ByRef oCallerForm As SAPbouiCOM.Form, ByVal sCardCode As String) As Boolean
            Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            Try
                oBP = CType(oParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners), SAPbobsCOM.BusinessPartners)
                If oBP.GetByKey(sCardCode) Then
                    setSharedData(oCallerForm, "CARDCODE", oBP.CardCode)
                    setSharedData(oCallerForm, "CARDNAME", oBP.CardName)
                    setSharedData(oCallerForm, "CARDTYPE", oBP.CardType)
                    setSharedData(oCallerForm, "GROUPCODE", oBP.GroupCode)
                    setSharedData(oCallerForm, "GROUPNAME", "")
                    setSharedData(oCallerForm, "BALANCEFC", oBP.CurrentAccountBalance)
                    setSharedData(oCallerForm, "CREDITLINE", oBP.CreditLimit)
                    setSharedData(oCallerForm, "PHONE1", oBP.Phone1)
                    setSharedData(oCallerForm, "CNTCTPRSN", oBP.ContactPerson)
                    setSharedData(oCallerForm, "WASLIC", oBP.UserFields.Fields.Item("U_WASLIC").Value)
                    Return True
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error getting the BP Info - " & sCardCode)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXBPI", {sCardCode})
            Finally
                DataHandler.INSTANCE.doReleaseObject(CType(oBP, Object))
            End Try
            Return False
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType = "134" Then
                    Dim sCardCode As String = getSharedDataAsString(oForm, "CARDCODE")
                    Dim oParentForm As SAPbouiCOM.Form = goParent.doGetParentForm(oForm.UniqueID)
                    If doGetBPInfo(goParent, oParentForm, sCardCode) = True Then
                        doReturnFromModalShared(oForm, True)
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal result - " & sModalFormType)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub

        Public Function doGMaps(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal oForm As SAPbouiCOM.Form, ByRef sReq As String) As Boolean
            Dim cp As SAPbouiCOM.FormCreationParams
            Dim oItem As SAPbouiCOM.Item
            'Dim oStatic As SAPbouiCOM.StaticText
            Dim AxBrowser As SHDocVw.InternetExplorer
            Dim bModal As Boolean = False

            ' Create the form
            cp = CType(oParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams), SAPbouiCOM.FormCreationParams)

            cp.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Fixed
            cp.FormType = "Modal"
            cp.UniqueID = "Modal"

            oForm = oParent.goApplication.Forms.AddEx(cp)
            oForm.Title = "WR1 Browser"
            oForm.ClientHeight = 740
            oForm.ClientWidth = 600

            ' Create the form GUI elements
            oForm.AutoManaged = False
            oForm.SupportedModes = 0
            oItem = oForm.Items.Add("1", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.AffectsFormMode = False
            oItem.Top = 665
            oItem.Left = 10

            'oItem = oForm.Items.Add("MyStatic", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            'oStatic = oItem.Specific()
            'oStatic.Caption = getTranslatedWord("I am a modal form")

            Dim oBrowser As SAPbouiCOM.ActiveX
            oItem = oForm.Items.Add("ISBBRW", SAPbouiCOM.BoFormItemTypes.it_ACTIVE_X)
            oItem.Top = 10
            oItem.Left = 10
            oItem.Height = 650
            oItem.Width = 570
            oBrowser = CType(oItem.Specific, SAPbouiCOM.ActiveX)
            oBrowser.ClassID = Config.Parameter("AXBRW") '"Shell.Explorer.2"
            AxBrowser = CType(oBrowser.Object, SHDocVw.InternetExplorer)

            'Dim sURL As String = "http://servicecall.isbglobal.com/ISBMAPS/Default.aspx?PC=CRANLEIGH CRC, ELMBRIDGE ROAD, NANHURST, CRANLEIGH, GU6 8JX"
            AxBrowser.Navigate2(CType(sReq, Object))

            oForm.Visible = True
            bModal = True
            Return True
        End Function

    End Class
End Namespace
