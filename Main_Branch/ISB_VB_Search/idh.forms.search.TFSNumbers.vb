Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class TFSNumbers
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHTFSNSR", "TFS Numbers Search.srf", 5, 45, 603, 320, "TFS Numbers Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("[@IDH_ORIGIN]")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_TFSMAST", "tfs", Nothing, False, True))
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_TFSStatus", "stat", Nothing, False, True))
            oGridN.setRequiredFilter(" tfs.U_Status = stat.U_SCode ")
            oGridN.setOrderValue("U_TFSNUM")

            oGridN.doAddFilterField("IDHTFSN", "tfs.U_TFSNUM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDHEWCCD", "tfs.U_EWCCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "IN", 30, Nothing, True)
            oGridN.doAddFilterField("IDHTFSSTA", "stat.U_SCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "IN", 30, Nothing, True)
            oGridN.doAddFilterField("IDHDISPNM", "tfs.U_DSPName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)

            oGridN.doAddListField("tfs.Code", "TFS Code", False, -1, Nothing, "IDH_TFSCD")
            oGridN.doAddListField("tfs.U_TFSNUM", "TFS Number", False, -1, Nothing, "IDH_TFSNUM")
            oGridN.doAddListField("tfs.U_EWCCd", "EWC Code", False, -1, Nothing, "IDH_EWCCD")
            oGridN.doAddListField("stat.U_SName", "TFS Status", False, -1, Nothing, "IDH_TFSSTA")
            oGridN.doAddListField("tfs.U_DSPCd", "Disposal Site Cd", False, -1, Nothing, "IDH_DSPSCD")
            oGridN.doAddListField("tfs.U_DSPName", "Disposal Site Name", False, -1, Nothing, "IDH_DSPSNM")
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDHTFSSTA", "[@IDH_TFSStatus]", "U_SCode", "U_SName", " U_SCode <= 6 ")

            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDHTFSSTA")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = CType(oItem.Specific(), SAPbouiCOM.ComboBox)

            Dim oValidValues As SAPbouiCOM.ValidValues
            oValidValues = oCombo.ValidValues
            'doClearValidValues(oValidValues)

            oValidValues.Add("01,05,06", "Available, Acknowledged, Consented")

            MyBase.doBeforeLoadData(oForm)
        End Sub

    End Class
End Namespace
