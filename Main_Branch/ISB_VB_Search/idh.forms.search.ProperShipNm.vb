Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class ProperShipNm
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDH_PSNMSR", "PSName Search.srf", 5, 45, 603, 320, "Proper Shipping Name Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("[@IDH_WTUN]")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_PSHIPNM"))

            oGridN.setOrderValue("U_PSName")


            If IDHAddOns.idh.lookups.Base.INSTANCE.getParameterWithDefault("TFSMOD", "false") = "TFSMOD_TRUE" Then
                oGridN.doAddFilterField("IDH_PSName", "U_UNNumber", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

                oGridN.doAddListField("U_UNNumber", "UN Number", False, -1, Nothing, "IDH_UNNUM")
                oGridN.doAddListField("U_PSName", "Proper Shipping Name", False, -1, Nothing, "IDH_PSHPNM")
            Else
                oGridN.doAddFilterField("IDH_PSName", "U_PSName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

                oGridN.doAddListField("Code", "Code", False, 0, Nothing, "IDH_SNCODE")
                oGridN.doAddListField("Name", "Name", False, 0, Nothing, "IDH_SNNAM")
                oGridN.doAddListField("U_PSName", "Proper Shipping Name", False, -1, Nothing, "IDH_PSHPNM")
            End If

        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
            Dim lblUNNumber As SAPbouiCOM.StaticText = CType(oForm.Items.Item("IDH_LBL01").Specific, SAPbouiCOM.StaticText)
            If IDHAddOns.idh.lookups.Base.INSTANCE.getParameterWithDefault("TFSMOD", "false") = "TFSMOD_TRUE" Then
                lblUNNumber.Caption = "UN Number"
            Else
                lblUNNumber.Caption = "Proper Shipping Name"
            End If
        End Sub

    End Class
End Namespace