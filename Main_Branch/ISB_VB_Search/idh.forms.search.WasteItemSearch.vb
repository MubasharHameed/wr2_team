Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.dbObjects.User
Imports com.idh.bridge

Namespace idh.forms.search
    Public Class WasteItemSearch
        Inherits IDHAddOns.idh.forms.Search
        Protected bClosingFormByOKButton As Boolean = False

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHWISRC", "WasteItem Search.srf", 5, 70, 735, 320, "Waste Item Search")
        End Sub
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, sUNIQUEID As String)
            MyBase.New(oParent, sUNIQUEID, "WasteItem Search.srf", 5, 70, 735, 320, "Waste Item Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("OITM i, OITB ig")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OITM", "i"))
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OITB", "ig"))

            '			Dim sRequiredString As String = "i.ItmsGrpCod = ig.ItmsGrpCod AND i.FrozenFor <> 'Y' "
            '			Dim sWasteGroup As String = getUFValue(oForm, "IDH_WSTGRP", True)
            '			Dim sJobTypeGroup As String = getUFValue(oForm, "IDH_JOBTP", True)
            '        	If sWasteGroup.Length > 0 Then
            '        		sRequiredString = sRequiredString & " AND i.ItemCode In ( SELECT wg.U_ItemCd FROM [@IDH_WGPCNTY] wg, [@IDH_WGRPJLK] wlj WHERE wg.U_WstGpCd = wlj.U_WstGpCd AND wlj.U_WstGpCd = '" & sWasteGroup & "')"
            '        	ElseIf sJobTypeGroup.Length > 0 Then
            '        		'SELECT wg.U_ItemCd
            '				'	FROM [@IDH_WGPCNTY] wg, [@IDH_WGRPJLK] wlj
            '				'	WHERE wg.U_WstGpCd =  wlj.U_WstGpCd
            '				'	AND wlj.U_JobTp = 'Export'
            '        	End If
            '            oGridN.setRequiredFilter(sRequiredString)
            'oGridN.setOrderValue("i.ItmsGrpCod, i.ItemCode ")
            oGridN.setOrderValue("i.ItemCode, i.ItmsGrpCod ")

            oGridN.doAddFilterField("IDH_ITMCOD", "i.ItemCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            If getParentSharedDataAsString(oGridN.getSBOForm, "TRG") = "FROMENQUIRY" OrElse getParentSharedDataAsString(oGridN.getSBOForm, "TRG") = "FROMWOQ" Then
                oGridN.doAddFilterField("IDH_ITMFNM", "i.FrgnName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
                oGridN.doAddFilterField("IDH_ITMNAM", "i.ItemName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            Else
                oGridN.doAddFilterField("IDH_ITMNAM", "i.ItemName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            End If
            oGridN.doAddFilterField("IDH_GRPCOD", "i.ItmsGrpCod", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "IN", 30)
            oGridN.doAddFilterField("IDH_GRPNAM", "ig.ItmsGrpNam", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDH_INVENT", "i.InvntItem", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 1)
            oGridN.doAddFilterField("IDH_PURWB", "i.U_IDHCWBB", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 1)

            oGridN.doAddListField("i.ItmsGrpCod", "Item Group Code", False, 0, Nothing, "ITMSGRPCOD", -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("ig.ItmsGrpNam", "Item Group Name", False, 0, Nothing, "ITMSGRPNAM")
            '## Start 02-08-2013
            oGridN.doAddListField("i.ItemCode", "Item Code", False, 100, Nothing, "ITEMCODE", -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            If getParentSharedDataAsString(oGridN.getSBOForm, "TRG") = "FROMENQUIRY" OrElse getParentSharedDataAsString(oGridN.getSBOForm, "TRG") = "FROMWOQ" Then
                oGridN.doAddListField("i.FrgnName", "Foreign Name", False, 410, Nothing, "FNAME")
            End If
            '## End
            oGridN.doAddListField("i.ItemName", "Item Name", False, 410, Nothing, "ITEMNAME")
            oGridN.doAddListField("i.SalUnitMsr", "S_UOM", False, 50, Nothing, "UOM")
            oGridN.doAddListField("i.BuyUnitMsr", "P_UOM", False, 50, Nothing, "PUOM")
            oGridN.doAddListField("i.InvntItem", "Inventory", False, 10, Nothing, "INVENT")
            ' oGridN.doAddListField("i.frozenFor", "Frozen", False, 10, Nothing, "FROZEN")
            Dim sFrozenSQL As String = com.idh.bridge.lookups.Config.INSTANCE.getActiveCheckSQL("i", DateTime.Now).Trim
            If sFrozenSQL.ToUpper.StartsWith("AND") Then
                sFrozenSQL = sFrozenSQL.Substring(sFrozenSQL.ToUpper.IndexOf("AND") + 3)
            End If
            oGridN.doAddListField("(CASE when " & sFrozenSQL & " Then 'N' else 'Y' end)", "Frozen", False, 10, Nothing, "FROZEN")

            oGridN.doAddListField("i.U_WTDFW", "Default Weight", False, 10, Nothing, "DEFWEI")
            oGridN.doAddListField("i.U_WTUDW", "Use Default Weight", False, 10, Nothing, "USEDEFWEI")
            oGridN.doAddListField("i.U_IDHCWBB", "Weighbridge Purchase", False, 10, Nothing, "CWBB")
            oGridN.doAddListField("i.U_IDHCREB", "Carrier Rebate", False, 10, Nothing, "CREB")
            oGridN.doAddListField("i.U_HAZCD", "Haz Classification", False, 10, Nothing, "HAZCD")
            oGridN.doAddListField("i.U_ROUTECD", "Disposal Route Code", False, 0, Nothing, "DISPRTCD")
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
            doAddUF(oForm, "IDH_CRDCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, True)
            doAddUF(oForm, "IDH_LEV1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 10)
            oForm.Items.Item("IDH_LEV1").AffectsFormMode = False
            
            doAddUF(oForm, "IDH_JOBTP", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, False, False)
            doAddUF(oForm, "IDH_WSTGRP", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, False, False)
        End Sub

        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            doPopulateWasteClass(oForm)
            doPopulateInventory(oForm)
            doJobTypeCombo(oForm)
            doWasteGroupCombo(oForm)
            
            setUFValue(oForm, "IDH_JOBTP", getParentSharedData(oForm,"IDH_JOBTP"))
            setUFValue(oForm, "IDH_WSTGRP", getParentSharedData(oForm, "IDH_WSTGRP"))

            'Dim sVal As String = getParentSharedData(oForm, "IDH_INVENT")
            'setUFValue(oForm, "IDH_INVENT", sVal)
            MyBase.doBeforeLoadData(oForm)
        End Sub
        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
        	Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")

            'Dim sRequiredString As String = "i.ItmsGrpCod = ig.ItmsGrpCod AND i.FrozenFor <> 'Y' "
            Dim sRequiredString As String = "i.ItmsGrpCod = ig.ItmsGrpCod "

            Dim sWasteGroup As String = CType(getUFValue(oForm, "IDH_WSTGRP", True), String)
            Dim sJobTypeGroup As String = CType(getUFValue(oForm, "IDH_JOBTP", True), String)
        	
        	If sWasteGroup.Length > 0 Then
        		sWasteGroup = "'" & sWasteGroup & "'"
        	ElseIf sJobTypeGroup.Length > 0 Then
        		Dim oRecordSet As SAPbobsCOM.Recordset
        		Dim sQry As String = "SELECT U_WstGpCd FROM [@IDH_WGRPJLK] WHERE U_JobTp = '" & sJobTypeGroup & "'"
        		oRecordSet = goParent.goDB.doSelectQuery(sQry)
        		
        		Dim iRecords As Integer = oRecordSet.RecordCount
                If iRecords > 0 Then
                	While Not oRecordSet.EoF
                		If sWasteGroup.Length > 0 Then
                			sWasteGroup = sWasteGroup & ","
                		End If
                        sWasteGroup = "'" & CType(oRecordSet.Fields.Item(0).Value(), String) & "'"
                		oRecordSet.MoveNext()
                	End While
                End If
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
        	End If
        	
        	If sWasteGroup.Length > 0 Then
        		sRequiredString = sRequiredString & " AND i.ItemCode In ( SELECT wg.U_ItemCd FROM [@IDH_WGPCNTY] wg, [@IDH_WGRPJLK] wlj WHERE wg.U_WstGpCd = wlj.U_WstGpCd AND wlj.U_WstGpCd In (" & sWasteGroup & "))"
        	End If
        	
            oGridN.setRequiredFilter(sRequiredString)
            bClosingFormByOKButton = False
            Me.setParentSharedData(oForm, "IDH_ITMSRCHOPEND", "LOADED")
            MyBase.doLoadData(oForm)
        End Sub

        Private Sub doJobTypeCombo(ByVal oForm As SAPbouiCOM.Form)
        	'Dim sWasteGroup As String = getUFValue(oForm, "IDH_WSTGRP", True)
        	'If sWasteGroup.Length > 0 Then
        		
        	'End If
        	
        	doFillCombo(oForm, "IDH_JOBTP","[@IDH_JOBTYPE]", IDH_JOBTYPE._JobTp, Nothing, Nothing, Nothing, True)
        End Sub

        Private Sub doWasteGroupCombo(ByVal oForm As SAPbouiCOM.Form)
        	doFillCombo(oForm, "IDH_WSTGRP","[@ISB_WASTEGRP]", "U_WstGpCd", "U_WstGpNm", Nothing, "U_WstGpCd", True)
        End Sub

        Private Sub doPopulateInventory(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            Dim oInvent As SAPbouiCOM.ComboBox
            Try
                oItem = oForm.Items.Item("IDH_INVENT")
                oInvent = CType(oItem.Specific, SAPbouiCOM.ComboBox)

                Dim oValidValue As SAPbouiCOM.ValidValues
                oValidValue = oInvent.ValidValues
                doClearValidValues(oValidValue)

                oValidValue.Add("", getTranslatedWord("All"))
                oValidValue.Add("Y", getTranslatedWord("Stock"))
                oValidValue.Add("N", getTranslatedWord("None Stock"))
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error populating the Inventory Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Inventory")})
            End Try
        End Sub

        ''**Populate the Waste Classification Combo
        Private Sub doPopulateWasteClass(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_LEV1", "[@IDH_WASTCL]", "U_LCode", "U_Desc", Nothing, "CAST(U_LCode As Numeric)")
        End Sub

        ''## Start 05-07-2013
        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doFinalizeShow(oForm)
            If oForm.Visible = False Then
                Me.setParentSharedData(oForm, "IDH_ITMSRCHOPEND", "")
            Else
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                If oGridN Is Nothing Then
                    oGridN = New FilterGrid(Me, oForm, "LINESGRID", True)
                End If
                oGridN.doReloadData()
            End If

        End Sub
        ''## End

        Protected Overrides Sub doPrepareModalData(ByVal oForm As SAPbouiCOM.Form, Optional ByVal iSel As Integer = -1)
            'MyBase.doPrepareModalData(oForm, iSel)

            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            Dim iIndex As Integer
            Dim iCurrentDataRow As Integer
            Dim oSelectedRows As SAPbouiCOM.SelectedRows
            oSelectedRows = oGridN.getSBOGrid.Rows.SelectedRows
            If oSelectedRows.Count = 0 Then
                oSelectedRows.Add(0)
            End If

            If iSel = -1 Then
                iCurrentDataRow = oGridN.getSBOGrid.GetDataTableRowIndex(oSelectedRows.Item(iIndex, SAPbouiCOM.BoOrderType.ot_SelectionOrder))
            End If

            If iCurrentDataRow > -1 Then
                oGridN.setCurrentDataRowIndex(iCurrentDataRow)

                Dim sFrozenValue As String = CType(oGridN.doGetFieldValue("FROZEN"), String)
                If (sFrozenValue IsNot Nothing AndAlso sFrozenValue = "Y") Then
                    Dim sItemCode As String = CType(oGridN.doGetFieldValue("i.ItemCode"), String)
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("The selected Item is Inactive.", "ERUSITF", {sItemCode})
                    Exit Sub
                Else
                    Dim iFields As Integer
                    Dim oField As com.idh.controls.strct.ListField
                    For iFields = 0 To oGridN.getListfields().Count - 1
                        oField = CType(oGridN.getListfields().Item(iFields), com.idh.controls.strct.ListField)
                        setParentSharedData(oForm, oField.msFieldId, oGridN.doGetFieldValue(oField.msFieldName))
                    Next
                    'Exit For
                End If
            End If
            'Next
            doReturnFromModalShared(oForm, True)
        End Sub

        '** The ItemSubEvent handler
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            ''## Start 05-07-2013
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD OrElse _
                    (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.setParentSharedData(oForm, "IDH_ITMSRCHOPEND", "")
                    If bClosingFormByOKButton = False Then
                        MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                    End If
                End If
            End If
            ''## End
            MyBase.doItemEvent(oForm, pVal, BubbleEvent)

            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT AndAlso pVal.BeforeAction = False Then
                Dim sItem As String = pVal.ItemUID
                If sItem = "IDH_LEV1" Then
                    Dim sVal As String = CType(getUFValue(oForm, "IDH_LEV1"), String)
                    setUFValue(oForm, "IDH_ITMCOD", sVal)
                    doLoadData(oForm)
                    'doFilterData(oForm)
                ElseIf sItem = "IDH_JOBTP" Then
                    Dim sValue As String = CType(getUFValue(oForm, "IDH_JOBTP", True), String)
                    If sValue.Length > 0 Then
                        setUFValue(oForm, "IDH_WSTGRP", "", True)
                    End If
                    doLoadData(oForm)
                ElseIf sItem = "IDH_WSTGRP" Then
                    Dim sValue As String = CType(getUFValue(oForm, "IDH_WSTGRP", True), String)
                    If sValue.Length > 0 Then
                        setUFValue(oForm, "IDH_JOBTP", "", True)
                    End If
                    doLoadData(oForm)
                End If
            End If
            Return True
        End Function

        Protected Overrides Sub doReadInputParams(oForm As SAPbouiCOM.Form)
            setParentSharedData(oForm, "CALLEDITEM", getParentSharedData(oForm, "CALLEDITEM"))
            MyBase.doReadInputParams(oForm)
        End Sub

    End Class
End Namespace
