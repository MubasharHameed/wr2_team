Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class EWC
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDH_WTECSR", "EWC Search.srf", 5, 45, 603, 320, "EWC Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("[@IDH_WTEWC]")
            oGridN.doAddGridTable( new com.idh.bridge.data.GridTable( "@IDH_WTEWC" ))
            'oGridN.setOrderValue("Code")
            oGridN.setOrderValue("U_WTWC")

            'OnTime Ticket 25018 - EWC Code Search Bug
            'Restructuring the screen with two search fields and two columns in the grid 

            'oGridN.doAddFilterField("IDH_ECCOD", "Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_WTWC", "U_WTWC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 250, Nothing, True)
            'oGridN.doAddFilterField("IDH_WTWD", "U_Chapter", SAPbouiCOM.BoDataType.dt_LONG_TEXT, "LIKE", 100)
            oGridN.doAddFilterField("IDH_WTWD", "U_WTWD", SAPbouiCOM.BoDataType.dt_LONG_TEXT, "LIKE", 100, Nothing, True)

            'oGridN.doAddListField("Code", "EWC Code", False, -1, Nothing, "IDH_ECCODO")
            'oGridN.doAddListField("Name", "EWC Name", False, -1, Nothing, "IDH_ECNAMO")
            'oGridN.doAddListField("U_WTWC", "Waste Code", False, -1, Nothing, "IDH_WTWCO")
            'oGridN.doAddListField("U_WTWD", "Waste Description", False, -1, Nothing, "IDH_WTWDO")

            oGridN.doAddListField("U_WTWC", "EWC Code", False, -1, Nothing, "IDH_WTWCO")
            oGridN.doAddListField("U_WTWD", "EWC Description", False, -1, Nothing, "IDH_WTWDS")

        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace