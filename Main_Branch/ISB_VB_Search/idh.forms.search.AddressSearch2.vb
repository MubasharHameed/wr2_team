Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.bridge
Imports com.idh.bridge.lookups

Namespace idh.forms.search
    Public Class AddressSearch2
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHASRCH2", "Address Search2.srf", 5, 45, 790, 350, "Address Search - L2")
        End Sub
        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doReadInputParams(oForm)
            setParentSharedData(oForm, "CALLEDITEM", getParentSharedData(oForm, "CALLEDITEM"))
        End Sub
        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("CRD1", "ba", Nothing, False, True), True)
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OCRD", "bp", Nothing, False, True), True)
            Dim sRequired As String
            sRequired = " ba.CardCode = bp.CardCode "
            ''##MA Start 18-09-2014 Issue#403
            If getParentSharedData(oGridN.getSBOForm, "IDH_APPLYFLTR") IsNot Nothing AndAlso getParentSharedData(oGridN.getSBOForm, "IDH_APPLYFLTR").ToString = "True" Then
                'sRequired &= " And Not ba.Address in (Select adrs.U_Address from [@IDH_ADDRST] adrs Where adrs.U_CustCode=ba.CardCode And adrs.U_AddType=ba.AdresType And adrs.U_UserName='" & PARENT.gsUserName & "')"
                'Address for Customer block means block for linked BP as well
                sRequired &= " And Not ba.Address in (Select adrs.U_Address from [@IDH_ADDRST] adrs Where (adrs.U_CustCode=ba.CardCode or " _
                    & " adrs.U_CustCode  in (Select OCRD.CardCode from OCRD where OCRD.U_IDHLBPC=ba.CardCode)) " _
                    & " And adrs.U_AddType=ba.AdresType And adrs.U_UserName='" & PARENT.gsUserName & "')"

            End If
            oGridN.setRequiredFilter(sRequired)
            ''##MA End 18-09-2014 Issue#403

            'With Different Search Filters 
            'NEW Filters 
            oGridN.doAddFilterField("IDH_CUSCOD", "ba.CardCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_CUSNAM", "bp.CardName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 150)
            oGridN.doAddFilterField("IDH_ADDRES", "ba.Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_STREET", "ba.Street", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_ADRTYP", "ba.AdresType", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            oGridN.doAddFilterField("IDH_CITY", "ba.City", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_ZIPCD", "vPostcode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_STATE", "ba.State", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_TEL1", "ba.U_Tel1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            'OLD Filters 
            'oGridN.doAddFilterField("IDH_CUSCOD", "CardCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            'oGridN.doAddFilterField("IDH_ADDRES", "Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            'oGridN.doAddFilterField("IDH_ADRTYP", "AdresType", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            'oGridN.doAddFilterField("IDH_ZIP", "ZipCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            oGridN.doAddListField("ba.AdresType", "Type", False, -1, Nothing, "ADRESTYPE")
            oGridN.doAddListField("(Case When ba.Address= bp.ShipToDef AND ba.AdresType='S' Then 'Y'  When ba.Address= bp.BillToDef AND ba.AdresType='B' Then 'Y' Else '' End)", "Default", False, -1, Nothing, Nothing)
            oGridN.doAddListField("ba.CardCode", "Customer", False, -1, Nothing, "CARDCODE", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("bp.CardName", "Name", False, -1, Nothing, "CARDNAME")
            oGridN.doAddListField("ba.Address", "Address", False, -1, Nothing, "ADDRESS")
            oGridN.doAddListField("ba.U_AddrsCd", "Address Code", False, 0, Nothing, "ADDRSCD")
            oGridN.doAddListField("ba.Street", "Street", False, -1, Nothing, "STREET")
            oGridN.doAddListField("ba.Block", "Block", False, -1, Nothing, "BLOCK")
            oGridN.doAddListField("ba.ZipCode", "Postal Code", False, -1, Nothing, "ZIPCODE")
            oGridN.doAddListField("ba.City", "City", False, -1, Nothing, "CITY")
            oGridN.doAddListField("ba.County", "County", False, -1, Nothing, "COUNTY")
            oGridN.doAddListField("ba.State", "State Cd", False, -1, Nothing, "STATE")
            oGridN.doAddListField("(SELECT st.NAME FROM OCST st WHERE st.CODE = ba.STATE AND st.COUNTRY = ba.COUNTRY)", "State", False, -1, Nothing, "STATENM")
            oGridN.doAddListField("ba.Country", "Country", False, -1, Nothing, "COUNTRY")
            oGridN.doAddListField("ba.LicTradNum", "LicTradeNum", False, -1, Nothing, "LICTRADNUM")
            oGridN.doAddListField("ba.StreetNo", "StreetNo", False, -1, Nothing, "STREETNO")
            oGridN.doAddListField("ba.Building", "Building", False, -1, Nothing, "BUILDING")
            oGridN.doAddListField("ba.U_ROUTE", "Route", False, -1, Nothing, "ROUTE")
            oGridN.doAddListField("ba.U_ISBACCDF", "Site Access Day", False, -1, Nothing, "ISBACCDF")
            oGridN.doAddListField("ba.U_ISBACCTF", "Access Time From", False, -1, Nothing, "ISBACCTF")
            oGridN.doAddListField("ba.U_ISBACCTT", "Access Time To", False, -1, Nothing, "ISBACCTT")
            oGridN.doAddListField("ba.U_ISBACCRA", "Access Restriction", False, -1, Nothing, "ISBACCRA")
            oGridN.doAddListField("ba.U_ISBACCRV", "Vehicle Access Rest.", False, -1, Nothing, "ISBACCRV")
            oGridN.doAddListField("ba.U_ISBCUSTRN", "Customer Ref. No.", False, -1, Nothing, "ISBCUSTRN")
            oGridN.doAddListField("ba.U_TEL1", "Site Tel.", False, -1, Nothing, "TEL1")
            oGridN.doAddListField("ba.U_ROUTE", "Route", False, -1, Nothing, "ROUTE")
            oGridN.doAddListField("ba.U_IDHSEQ", "Route Seq.", False, -1, Nothing, "SEQ")
            oGridN.doAddListField("ba.U_IDHACN", "Contact Perso", False, -1, Nothing, "CONA")
            oGridN.doAddListField("ba.U_IDHPCD", "Premisses Code", False, -1, Nothing, "PRECD")
            oGridN.doAddListField("ba.U_Origin", "Origin", False, -1, Nothing, "ORIGIN")
            oGridN.doAddListField("ba.U_IDHSLCNO", "Site Licence Number", False, -1, Nothing, "SITELIC")
            oGridN.doAddListField("ba.U_IDHSteId", "Site Id", False, -1, Nothing, "STEID")

            oGridN.doAddListField("ba.U_IDHFAX", "Fax", False, -1, Nothing, "FAX")
            oGridN.doAddListField("ba.U_IDHEMAIL", "Email", False, -1, Nothing, "EMAIL")
            oGridN.doAddListField("ba.U_IDHSTRNO", "Street Number", False, -1, Nothing, "STRNO")

            oGridN.doAddListField("ba.U_PREEXPDTTO", "Premisses Exp Dt.", False, -1, Nothing, "PREEXPDTTO")
            oGridN.doAddListField("ba.U_EPASITEID", "EPA Site ID", False, -1, Nothing, "EPASITEID")

			'##MA Start 12-09-2014 Issue#413
            oGridN.doAddListField("ba.U_MinJob", "Minimum Job in a Day", False, 0, Nothing, "MINJOB")
            '##MA End 12-09-2014 Issue#413
        End Sub

        '**
        '** THIS WILL RETURN A RECORD IF THERE IS ONLY ONE SHIP TO ADDRESS
        '**
        Public Shared Function doGetAddress(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sCardCode As String, ByVal sAddressType As String, Optional ByVal sSearchAddress As String = Nothing) As ArrayList
            Return Config.INSTANCE.doGetAddress(sCardCode, sAddressType, sSearchAddress)
        End Function

        '**
        '** THIS WILL RETURN THE COMPANY ADDRESS
        '**
        Public Shared Function doGetCompanyAddress(ByVal oParent As IDHAddOns.idh.addon.Base) As ArrayList
            Return Config.INSTANCE.doGetCompanyAddress()
        End Function
    End Class
End Namespace
