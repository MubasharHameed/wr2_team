Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System
Imports com.idh.bridge.lookups

Imports com.idh.bridge

Namespace idh.forms.search
    Public Class WPWasteItemSearch
        Inherits IDHAddOns.idh.forms.Search

        Protected bClosingFormByOKButton As Boolean = False

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHWPWISRC", "WPWasteItem Search.srf", 5, 70, 603, 320, "Waste Profile Item Search")
        End Sub
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, sUNIQUEID As String)
            MyBase.New(oParent, sUNIQUEID, "WPWasteItem Search.srf", 5, 70, 603, 320, "Waste Profile Item Search")
        End Sub
        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("OITM i, OITB ig")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OITM", "i", Nothing, False, True))
            'oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OITB", "ig"))

            'oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@ISB_WSTPROFILE", "WP", Nothing, False, True))

            'oGridN.setRequiredFilter("i.ItemCode = WP.U_ItemCd AND (i.U_WPTemplate = 'N' OR i.U_WPTemplate IS NULL) ")

            Dim sReqFilter As String = "i.ItemCode = WP.U_ItemCd AND (i.U_WPTemplate = 'N' OR i.U_WPTemplate IS NULL) AND i.FrozenFor <> 'Y'"
            If Config.ParamaterWithDefault("ADWPITGR", "").Trim() <> "" Then
                Dim sCardCode As String = getParentSharedDataAsString(oGridN.getSBOForm, "IDH_CRDCD")
                Dim sCardAddress As String = getParentSharedDataAsString(oGridN.getSBOForm, "IDH_BPADDR")
                oGridN.doAddGridJoin(New com.idh.bridge.data.GridJoin("[@ISB_WSTPROFILE]", com.idh.bridge.data.GridJoin.TYPE_JOIN.LEFT, "WP", Nothing, " I.ItemCode=wp.U_ItemCd ", True))
                sReqFilter = " (i.U_WPTemplate = 'N' OR i.U_WPTemplate IS NULL) AND i.FrozenFor <> 'Y' " _
                    & " AND (((WP.U_CardCd LIKE '" & sCardCode.Trim().Replace("'", "''") & "%' )  AND (WP.U_AddCd ='" & sCardAddress.Trim().Replace("'", "''") & "' ) ) or i.ItmsGrpCod in (" & Config.ParamaterWithDefault("ADWPITGR", "").Trim() & ")  ) "
                Dim bAddCardUF As Boolean = True, bAddAddressUF As Boolean = True
                For i As Int32 = 0 To oGridN.getSBOForm.DataSources.UserDataSources.Count - 1
                    If oGridN.getSBOForm.DataSources.UserDataSources.Item(i).UID = "IDH_CRDCD" Then
                        bAddCardUF = False
                    ElseIf oGridN.getSBOForm.DataSources.UserDataSources.Item(i).UID = "IDH_BPADDR" Then
                        bAddAddressUF = False
                    End If
                Next
                If bAddCardUF Then
                    doAddUF(oGridN.getSBOForm, "IDH_CRDCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False, -1)
                End If
                If bAddAddressUF Then
                    doAddUF(oGridN.getSBOForm, "IDH_BPADDR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False, -1)
                End If

                setUFValue(oGridN.getSBOForm, "IDH_CRDCD", sCardCode, True, True)
                setUFValue(oGridN.getSBOForm, "IDH_BPADDR", sCardAddress, True, True)
            Else
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@ISB_WSTPROFILE", "WP", Nothing, False, True))
                sReqFilter = "i.ItemCode = WP.U_ItemCd AND (i.U_WPTemplate = 'N' OR i.U_WPTemplate IS NULL) AND i.FrozenFor <> 'Y'"

                oGridN.doAddFilterField("IDH_CRDCD", "WP.U_CardCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
                oGridN.doAddFilterField("IDH_CRDNM", "WP.U_CardNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
                oGridN.doAddFilterField("IDH_BPADDR", "WP.U_AddCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 50)

            End If
            'oGridN.setRequiredFilter("i.ItemCode = WP.U_ItemCd AND (i.U_WPTemplate = 'N' OR i.U_WPTemplate IS NULL) AND i.FrozenFor <> 'Y'")
            oGridN.setRequiredFilter(sReqFilter)
            oGridN.setOrderValue("i.ItemCode ")

            oGridN.doAddFilterField("IDH_ITMCOD", "i.ItemCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDH_ITMNAM", "i.ItemName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)

            oGridN.doAddFilterField("IDH_INVENT", "i.InvntItem", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 1)

            'TODO: need to add WP Item Filter dropdown to filter 
            'search on the WOR Waste item selection as per Customer's Site 
            'oGridN.doAddFilterField("IDH_BPADDR", "

            'oGridN.doAddListField("i.ItmsGrpCod", "Item Group Code", False, 0, Nothing, "ITMSGRPCOD")
            'oGridN.doAddListField("ig.ItmsGrpNam", "Item Group Name", False, 0, Nothing, "ITMSGRPNAM")
            oGridN.doAddListField("i.ItemCode", "Item Code", False, 100, Nothing, "ITEMCODE", -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("i.ItemName", "Item Name", False, 410, Nothing, "ITEMNAME")
            oGridN.doAddListField("WP.U_CardCd", "WP Customer Code", False, 100, Nothing, "WPCARDCD")
            oGridN.doAddListField("WP.U_CardNm", "WP Customer Name", False, 200, Nothing, "WPCARDNM")
            oGridN.doAddListField("i.SalUnitMsr", "S_UOM", False, 50, Nothing, "UOM")
            oGridN.doAddListField("i.BuyUnitMsr", "P_UOM", False, 50, Nothing, "PUOM")
            oGridN.doAddListField("i.InvntItem", "Inventory", False, -1, Nothing, "INVENT")
            oGridN.doAddListField("i.U_WTDFW", "Default Weight", False, -1, Nothing, "DEFWEI")
            oGridN.doAddListField("i.U_WTUDW", "Use Default Weight", False, -1, Nothing, "USEDEFWEI")
            oGridN.doAddListField("i.U_IDHCWBB", "Weighbridge Purchase", False, -1, Nothing, "CWBB")
            oGridN.doAddListField("i.U_IDHCREB", "Carrier Rebate", False, -1, Nothing, "CREB")
            oGridN.doAddListField("i.U_HAZCD", "Haz Classification", False, -1, Nothing, "HAZCD")
            oGridN.doAddListField("WP.U_ItemCd", "WP Item Code", False, -1, Nothing, "WPITEMCD")
            oGridN.doAddListField("i.U_DispFacility", "WP Disposal Facility", False, -1, Nothing, "WPDSPFAC")
            'KA -- START -- 20121025
            oGridN.doAddListField("i.U_DispFacCd", "WP Disposal Facility", False, -1, Nothing, "WPDFCD")
            oGridN.doAddListField("i.U_DFAddress", "WP Disp.Facility Address", False, -1, Nothing, "WPDFADD")
            'KA -- END -- 20121025
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
            'doAddUF(oForm, "IDH_CRDCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, True)
            doAddUF(oForm, "IDH_LEV1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 10)
            oForm.Items.Item("IDH_LEV1").AffectsFormMode = False
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            doPopulateWasteClass(oForm)
            doPopulateInventory(oForm)
            doFillBPAddress(oForm)
            'Disable Address filter field 
            Dim oItm As SAPbouiCOM.Item = oForm.Items.Item("IDH_BPADDR")
            oItm.Enabled = Config.INSTANCE.getParameterAsBool("EBWPWIAD", True)
            MyBase.doBeforeLoadData(oForm)
        End Sub

        '## Start 04-07-2013
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            bClosingFormByOKButton = False
            Me.setParentSharedData(oForm, "IDH_WISOPEND", "LOADED")
            MyBase.doLoadData(oForm)
        End Sub
        '## End

        Private Sub doPopulateInventory(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            Dim oInvent As SAPbouiCOM.ComboBox
            Try
                oItem = oForm.Items.Item("IDH_INVENT")
                oInvent = CType(oItem.Specific, SAPbouiCOM.ComboBox)

                Dim oValidValue As SAPbouiCOM.ValidValues
                oValidValue = oInvent.ValidValues
                doClearValidValues(oValidValue)

                oValidValue.Add("", getTranslatedWord("All"))
                oValidValue.Add("Y", getTranslatedWord("Stock"))
                oValidValue.Add("N", getTranslatedWord("None Stock"))
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error populating the Inventory Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Inventory")})
            End Try
        End Sub

        Private Sub doFillBPAddress(ByVal oForm As SAPbouiCOM.Form)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                Dim oCombo As SAPbouiCOM.ComboBox
                oCombo = CType(oForm.Items.Item("IDH_BPADDR").Specific, SAPbouiCOM.ComboBox)
                If oCombo IsNot Nothing Then
                    doClearValidValues(oCombo.ValidValues)
                End If
                Dim sBPCode As String = getParentSharedDataAsString(oForm, "IDH_CRDCD")
                oRecordSet = goParent.goDB.doSelectQuery("SELECT DISTINCT Address, Address FROM CRD1 where CardCode = '" + sBPCode + "' ")
                While Not oRecordSet.EoF
                    oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(1).Value, String))
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Address Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Address")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

        ''**Populate the Waste Classification Combo
        Private Sub doPopulateWasteClass(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_LEV1", "[@IDH_WASTCL]", "U_LCode", "U_Desc", Nothing, "CAST(U_LCode As Numeric)")
        End Sub

        '** The ItemSubEvent handler
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD OrElse _
                    (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.setParentSharedData(oForm, "IDH_WISOPEND", "")
                    If bClosingFormByOKButton = False Then
                        MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                    End If
                End If
            End If

            MyBase.doItemEvent(oForm, pVal, BubbleEvent)

            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_LEV1" Then
                        Dim sVal As String = CType(getUFValue(oForm, "IDH_LEV1"), String)
                        setUFValue(oForm, "IDH_ITMCOD", sVal)
                        doLoadData(oForm)
                        'doFilterData(oForm)
                    End If
                End If
            End If
            Return True
        End Function

    End Class
End Namespace
