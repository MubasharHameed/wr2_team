Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class EPA
        Inherits IDHAddOns.idh.forms.Search


        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "EPASCR", "EPA Search.srf", 5, 45, 603, 320, "EPA Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("[@IDH_WTUN]")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_EPAWASTE"))

            oGridN.setOrderValue("U_Desc")

            oGridN.doAddFilterField("IDH_EPACD", "U_EPACode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_EPANM", "U_Desc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            oGridN.doAddListField("Code", "Code", False, 0, Nothing, "IDH_CODE")
            oGridN.doAddListField("Name", "Name", False, 0, Nothing, "IDH_NAME")
            oGridN.doAddListField("U_EPACode", "EPA Code", False, -1, Nothing, "IDH_EPACD")
            oGridN.doAddListField("U_Desc", "EPA Name", False, -1, Nothing, "IDH_EPANM")

            'oGridN.doAddListField("U_WTUST", "Quantity", False, -1, Nothing, "IDH_WTUST")
            'oGridN.doAddListField("U_WTUFP", "Flashpoint", False, -1, Nothing, "IDH_WTUFP")
            'oGridN.doAddListField("U_WTUDE", "Density", False, -1, Nothing, "IDH_WTUDE")
            'oGridN.doAddListField("U_WTUPH", "PH", False, -1, Nothing, "IDH_WTUPH")
            'oGridN.doAddListField("U_WTUPG", "Pack Group", False, -1, Nothing, "IDH_WTUPG")
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace