Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class Warehouse
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHWHSSRC", "Whse Search.srf", 5, 45, 603, 320, "Warehouse Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("[@IDH_WTHAZ]")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OWHS", Nothing, Nothing, False, True))
            oGridN.setOrderValue("WhsCode")

            oGridN.doAddFilterField("IDH_WHCODE", "WhsCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_WHNAME", "WhsName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100)
            oGridN.doAddFilterField("IDH_WHLOCA", "Location", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100)

            oGridN.doAddListField("WhsCode", "Warehouse Code", False, -1, Nothing, "WHSCODE")
            oGridN.doAddListField("WhsName", "Warehouse Name", False, -1, Nothing, "WHSNAME")
            oGridN.doAddListField("Location", "Location", False, -1, Nothing, "WHSLOC")
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace