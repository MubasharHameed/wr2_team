Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class EmployeeSearch
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHEMPSCR", "Employee Search.srf", 5, 40, 603, 350, "Employee Search")
        End Sub

        '## Start 23-09-2014
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            setParentSharedData(oForm, "bClosingFormByOKButton", "False")
            'bClosingFormByOKButton = False
            Me.setParentSharedData(oForm, "IDH_EMPSRCHOPEND", "LOADED")
            MyBase.doLoadData(oForm)
        End Sub
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD OrElse _
                    (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.setParentSharedData(oForm, "IDH_EMPSRCHOPEND", "")
                    If getParentSharedDataAsString(oForm, "bClosingFormByOKButton") = "False" Then
                        MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                    End If
                End If
            End If
            Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
        End Function

        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doFinalizeShow(oForm)
            If oForm.Visible = False Then
                Me.setParentSharedData(oForm, "IDH_EMPSRCHOPEND", "")
            Else
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                If oGridN Is Nothing Then
                    oGridN = New FilterGrid(Me, oForm, "LINESGRID", True)
                End If
                oGridN.doReloadData()
            End If
        End Sub
        '## End 23-09-2014
        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OHEM", "e"))
            'oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("HEM6", "r"))
            'oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OHTY", "rl"))

            'oGridN.setRequiredFilter(" e.EmpId = r.EmpId AND r.RoleID = rl.TypeID ")
            'oGridN.setOrderValue("e.EmpId, r.EmpId ")
            oGridN.setOrderValue("e.EmpId ")

            oGridN.doAddFilterField("IDH_EMPID", "e.EmpID", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDH_EMPFN", "e.FirstName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDH_EMPMN", "e.MiddleName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_EMPLN", "e.LastName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            'oGridN.doAddFilterField("IDH_EMPRID", "rl.Name", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)

            oGridN.doAddListField("e.EmpID", "Employee ID", False, 20, Nothing, "EMPID")
            oGridN.doAddListField("e.FirstName", "First Name", False, 50, Nothing, "EMPFN")
            oGridN.doAddListField("e.MiddleName", "Middle Name", False, 50, Nothing, "EMPMN")
            oGridN.doAddListField("e.LastName", "Last Name", False, 50, Nothing, "EMPLN")
            'oGridN.doAddListField("r.RoleID", "Role ID", False, 20, Nothing, "EMPRID")
            'oGridN.doAddListField("rl.Name", "Role Name", False, 50, Nothing, "EMPRNM")
            oGridN.doAddListField("e.LastName + ', ' + e.FirstName", "Full Name", False, 0, Nothing, "FULLNM")
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace
