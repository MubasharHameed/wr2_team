Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class EVNNumbers
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHEVNNSR", "EVN Numbers Search.srf", 5, 45, 603, 320, "EVN Numbers Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("[@IDH_ORIGIN]")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_EVNMASTER", "evn", Nothing, False, True))
            'oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_TFSStatus", "stat", Nothing, False, True))
            'oGridN.setRequiredFilter(" tfs.U_Status = stat.U_SCode ")
			Dim sRequired As String
            If getParentSharedData(oGridN.getSBOForm, "ReqDate") IsNot Nothing Then
                sRequired &= "convert(date, '" & getParentSharedData(oGridN.getSBOForm, "ReqDate") & "', 103) >= Convert(date,U_StartDt,103) AND convert(date, '" & getParentSharedData(oGridN.getSBOForm, "ReqDate") & "', 103) <= Convert(date,U_EndDt,103) And ((Select Isnull(BalanceRemaining,0) from IDH_VEVNMGR where U_EVNNumber = evn.U_EVNNumber) > 0 or (Select Isnull(BalanceRemaining,0) from IDH_VEVNMGR where U_EVNNumber = evn.U_EVNNumber) is null)"
            End If
            oGridN.setRequiredFilter(sRequired)
            oGridN.setOrderValue("evn.U_EVNNumber")

            oGridN.doAddFilterField("IDHEVNN", "evn.U_EVNNumber", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDHEWCCD", "evn.U_EWCCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "IN", 30, Nothing, True)
            'oGridN.doAddFilterField("IDHEVNSTA", "stat.U_SCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "IN", 30, Nothing, True)
            oGridN.doAddFilterField("IDHDISPNM", "evn.U_DispSiteCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)

			'##MA Start 2017-03-01
            oGridN.doAddFilterField("IDHWASCD", "evn.U_WasCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDHCUSCD", "evn.U_CusCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDHCUSADR", "evn.U_CusSAddr", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDHDSPSADR", "evn.U_DSPSAddr", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            '##MA Start 2017-03-01
            oGridN.doAddListField("evn.Code", "EVN Code", False, -1, Nothing, "IDH_EVNCD")
            oGridN.doAddListField("evn.U_EVNNumber", "EVN Number", False, -1, Nothing, "IDH_EVNNUM")
            oGridN.doAddListField("evn.U_EWCCd", "EWC Code", False, -1, Nothing, "IDH_EWCCD")
            'oGridN.doAddListField("stat.U_SName", "TFS Status", False, -1, Nothing, "IDH_TFSSTA")
            oGridN.doAddListField("evn.U_DispSiteCd", "Disposal Site Cd", False, -1, Nothing, "IDH_DSPSCD")
            oGridN.doAddListField("evn.U_DispSiteNm", "Disposal Site Name", False, -1, Nothing, "IDH_DSPSNM")
			'##MA Start 2017-03-01
            oGridN.doAddListField("evn.U_StartDt", "EVN Start Date", False, -1, Nothing, "IDH_STDT")
            oGridN.doAddListField("evn.U_EndDt", "EVN End Date", False, -1, Nothing, "IDH_ENDT")
            oGridN.doAddListField("evn.U_CusCd", "Customer Code", False, -1, Nothing, "IDH_CUSCD")
            oGridN.doAddListField("evn.U_CusNm", "Customer Name", False, -1, Nothing, "IDH_CUSNM")
            oGridN.doAddListField("evn.U_WasCd", "Waste Code", False, -1, Nothing, "IDH_WASCD")
            oGridN.doAddListField("evn.U_WasDsc", "Waste Desc", False, -1, Nothing, "IDH_WASNM")
            oGridN.doAddListField("evn.U_DSPSAddr", "Disposal Site Addr", False, -1, Nothing, "IDH_DSPSADR")
            oGridN.doAddListField("evn.U_CusSAddr", "Customer Site Addr", False, -1, Nothing, "IDH_CUSSADR")
            '##MA Start 2017-03-01
        End Sub
		Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doReadInputParams(oForm)
            setUFValue(oForm, "IDHWASCD", getParentSharedData(oForm, "WasCd"))
            setUFValue(oForm, "IDHCUSCD", getParentSharedData(oForm, "CusCd"))
            setUFValue(oForm, "IDHCUSADR", getParentSharedData(oForm, "CusAdr"))
            setUFValue(oForm, "IDHDSPSADR", getParentSharedData(oForm, "DspAdr"))
            setUFValue(oForm, "IDHDISPNM", getParentSharedData(oForm, "DspCd"))
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            'doFillCombo(oForm, "IDHTFSSTA", "[@IDH_TFSStatus]", "U_SCode", "U_SName", " U_SCode <= 6 ")

            'Dim oItem As SAPbouiCOM.Item
            'oItem = oForm.Items.Item("IDHTFSSTA")
            'Dim oCombo As SAPbouiCOM.ComboBox
            'oCombo = CType(oItem.Specific(), SAPbouiCOM.ComboBox)

            'Dim oValidValues As SAPbouiCOM.ValidValues
            'oValidValues = oCombo.ValidValues
            ''doClearValidValues(oValidValues)

            'oValidValues.Add("01,05,06", "Available, Acknowledged, Consented")

            MyBase.doBeforeLoadData(oForm)
        End Sub

    End Class
End Namespace
