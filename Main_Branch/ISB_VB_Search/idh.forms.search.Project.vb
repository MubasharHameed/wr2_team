Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class Project
        Inherits IDHAddOns.idh.forms.Search
        
    Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHPRJSRC", "SearchForm.srf", 5, 45, 603, 320, "Projects Search")
        End Sub   
        
'        '*** Set the Form Title
'        Protected Overrides Sub doSetTitle(ByVal oForm As SAPbouiCOM.Form)
'        	oForm.Title = "Projects Search"
'        End Sub
        
'        '*** Set the Labels
'        Protected Overridable Sub doSetLabels(ByVal oForm As SAPbouiCOM.Form)
'        End Sub
        
        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
        	'oGridN.setTableValue("OPRJ")
        	oGridN.doAddGridTable( New com.idh.bridge.data.GridTable( "OPRJ" ))
        	
        	oGridN.setOrderValue("PrjCode")
        	
        	oGridN.doAddFilterField("IDH_CODE", "PrjCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 15, Nothing, True)
        	oGridN.doAddFilterField("IDH_NAME", "PrjName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100, Nothing, True)
        	
        	oGridN.doAddListField("PrjCode", "Project", False, -1, Nothing, "IDH_CODE", -1, SAPbouiCOM.BoLinkedObject.lf_ProjectCodes)
        	oGridN.doAddListField("PrjName", "Project Name", False, -1, Nothing, "IDH_NAME")
        End Sub
        
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace 
