Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class GlAccnt
        Inherits IDHAddOns.idh.forms.Search
        
    Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHGLSRC", "GlAccnt Search.srf", 5, 45, 603, 320, "GL Accounts Search")
        End Sub

        '        '*** Set the Form Title
        '        Protected Overrides Sub doSetTitle(ByVal oForm As SAPbouiCOM.Form)
        '        	oForm.Title = "GL Accounts Search"
        '        End Sub

        '        '*** Set the Labels
        '        Protected Overridable Sub doSetLabels(ByVal oForm As SAPbouiCOM.Form)
        '        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("OACT")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OACT", Nothing, Nothing, False, True))
            oGridN.setOrderValue("AcctCode")
            oGridN.setRequiredFilter("Postable = 'Y'")

            oGridN.doAddFilterField("IDH_CODE", "AcctCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 15, Nothing, True)
            oGridN.doAddFilterField("IDH_FCODE", "FormatCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 15, Nothing, True)
            oGridN.doAddFilterField("IDH_NAME", "AcctName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100, Nothing, True)

            oGridN.doAddListField("AcctCode", "G/L Account Code", False, -1, Nothing, "IDH_CODE", 0, SAPbouiCOM.BoLinkedObject.lf_GLAccounts)
            oGridN.doAddListField("FormatCode", "G/L Format Account Code", False, -1, Nothing, "IDHFCODE", -1)
            oGridN.doAddListField("AcctName", "Name", False, -1, Nothing, "IDH_NAME")
        End Sub
        
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace 
