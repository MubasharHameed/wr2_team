'Imports System.IO
'Imports System.Collections
'Imports IDHAddOns.idh.controls
'Imports System

'Namespace idh.forms.search
'    Public Class WasteProfile2
'        Inherits IDHAddOns.idh.forms.Search

'        Private FormLoaded As Boolean = False
'        Private oMatrix As SAPbouiCOM.Matrix
'        Private _sSQL As String = ""
'        Private iLastselectedRow As Int32 = 0
'        Private slastSearchedResult As String = ""
'        Private bClosingFormByOKButton As Boolean = False
'        Private WithEvents timer_bp As System.Timers.Timer
'        Private timer_Form As SAPbouiCOM.Form
'        Private timer_ColUID, timer_ItemUID As String


'#Region "Initialization"
'        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
'            MyBase.New(oParent, "IDHWPRS", "Waste Profile Search2.srf", 5, 40, 530, 360, "Waste Profile Search L-2")
'        End Sub
'#End Region

'        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
'        End Sub

'        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
'            bClosingFormByOKButton = False

'            _sSQL = ""
'            iLastselectedRow = 0
'            slastSearchedResult = ""

'            'setUFValue(oForm, "IDH_ITMCOD", getParentSharedData(oForm, "IDH_ITMCOD"))
'            'setUFValue(oForm, "IDH_ITMNAM", getParentSharedData(oForm, "IDH_ITMNAM"))
'            'setUFValue(oForm, "IDH_GRPCOD", getParentSharedData(oForm, "IDH_GRPCOD"))
'            'setUFValue(oForm, "IDH_GRPNAM", "")

'            bClosingFormByOKButton = False
'            'Me.SetParentSharedData(oForm, "IDH_ITM1SRCHOPEND", "LOADED")

'            reQuerynReloadMatrix(oForm)
'        End Sub

'        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
'            Try
'                oForm.Items.Item("LINESGRID").Visible = False
'                Dim bFlag As Boolean = False
'                Dim sSILENT As String = ""
'                If (Me.getHasSharedData(oForm)) Then
'                    sSILENT = Me.getParentSharedData(oForm, "SILENT") '.ToString
'                    If (sSILENT <> Nothing) Then
'                        If (sSILENT.ToUpper().Equals("SHOWMULTI")) Then
'                            bFlag = True 'Means we have some value in CardCode/cardname
'                        End If
'                    End If
'                End If
'                If (bFlag AndAlso oMatrix.RowCount > 0) Then
'                    oMatrix.SelectRow(1, True, False)
'                    If (oMatrix.RowCount = 1) Then
'                        Me.SetParentSharedData(1, oForm)
'                        Me.doReturnFromModalShared(oForm, True)
'                        'Me.SetParentSharedData(oForm, "IDH_ITM1SRCHOPEND", "")
'                        Return
'                    End If
'                End If
'                oForm.Visible = True
'                Dim otxt As SAPbouiCOM.EditText = oForm.Items.Item("IDH_WPCD").Specific
'                otxt.Active = True
'                FormLoaded = True

'                timer_bp = New System.Timers.Timer(700)
'                timer_bp.Enabled = False
'            Catch ex As Exception
'                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Finalizing Show Form.")
'            End Try
'        End Sub
'        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
'            'Clear the global values
'            IDHAddOns.idh.addon.Base.PARENT.setGlobalValue("WPS_CUST", Nothing)
'            IDHAddOns.idh.addon.Base.PARENT.setGlobalValue("WPS_CUSTNM", Nothing)
'            IDHAddOns.idh.addon.Base.PARENT.setGlobalValue("WPS_ADDR", Nothing)

'            'setSharedData(oForm, "WPIT_ITEMCODE", sItemCode)
'            'setSharedData(oForm, "WPIT_ITEMNAME", sItemName)
'            IDHAddOns.idh.addon.Base.PARENT.setGlobalValue("WPITM", Nothing)
'            'MyBase.doCloseForm(oForm, BubbleEvent)
'        End Sub
'        Private Function PrepareSQL(ByVal oform As SAPbouiCOM.Form, Optional ByVal sOrderbyCol As String = "") As String
'            iLastselectedRow = 0
'            slastSearchedResult = ""
'            Dim sCardCode As String = getParentSharedData(oform, "IDH_CUST")
'            Dim sAddress As String = getParentSharedData(oform, "IDH_ADDR")

'            Dim sql As String = " Select wp.U_ProfileNo, wp.U_ItemCd, wp.U_ItemNm, wp.U_AddCd  " _
'                                   & " From OITM i , [@ISB_WSTPROFILE] wp Where wp.U_ItemCd = i.ItemCode " _
'                                   & " And wp.U_CardCd = '" & sCardCode & "' AND wp.U_AddCd = '" & sAddress & "' " _
'                                   & "And ((cast((case when ( (i.FrozenFor = 'Y' and (GetDate() Between isnull(i.FrozenFrom, DateAdd(d,-1,GetDate())) and isnull(i.FrozenTo, DateAdd(d,1,GetDate()))))) then 1 else 0 end) as bit)  " _
'                                   & " ) | " _
'                                   & " (cast((case when ( (i.validFor = 'Y' and NOT(GetDate() Between isnull(i.validFrom, DateAdd(d,-1,GetDate())) and isnull(i.validto, DateAdd(d,1,GetDate()))))) then 1 else 0 end) as bit)  " _
'                                   & " ))=0 "

'            PrepareSQL = sql

'            Dim sWPItemCode As String = getItemValue(oform, "IDH_WPCD")

'            Dim sWPItemName As String = getItemValue(oform, "IDH_WPNM")

'            Dim sWPAddress As String = getItemValue(oform, "IDH_ADDCD")


'            Dim sWhereSql As String = ""
'            If sWPItemCode <> "" Then
'                sWhereSql &= " and wp.U_ItemCd LIKE '" & sWPItemCode.Replace("*", "%") & "%' "
'            End If

'            If sWPItemName <> "" Then
'                sWhereSql &= " and wp.U_ItemNm Like '" & sWPItemName.Replace("*", "%") & "%' "
'            End If

'            If sWPAddress <> "" Then
'                sWhereSql &= " and wp.U_AddCd Like '" & sWPAddress.Replace("*", "%") & "%' "
'            End If



'            If sWhereSql.Trim <> "" Then
'                sql &= sWhereSql
'            End If
'            If sOrderbyCol <> "" Then
'                sql &= " Order by " & sOrderbyCol
'            End If
'            Return sql
'        End Function

'        Private Sub reQuerynReloadMatrix(ByVal oform As SAPbouiCOM.Form, Optional ByVal sOrderByCol As String = "")
'            Try
'                iLastselectedRow = 0
'                slastSearchedResult = ""
'                Dim sSQL As String = PrepareSQL(oform, sOrderByCol).Trim
'                If (sSQL.Trim.ToLower.Equals(_sSQL.ToLower)) AndAlso _sSQL <> "" Then
'                    Return
'                End If
'                _sSQL = sSQL
'                oMatrix = oform.Items.Item("LINESMTX").Specific
'                oform.DataSources.DataTables.Item("LINESMTX").ExecuteQuery(sSQL)
'                ' oform.Freeze(True)
'                oMatrix.Clear()
'                oMatrix.LoadFromDataSourceEx(False)
'            Catch ex As Exception
'                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error in Quering db.")
'            Finally
'                '  oform.Freeze(False)
'            End Try

'        End Sub

'        Protected Overrides Sub doSetEventFilters()
'            MyBase.doSetEventFilters()
'            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
'            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
'            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
'            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
'            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
'            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
'            doAddEvent(SAPbouiCOM.BoEventTypes.et_GOT_FOCUS)
'            doAddEvent(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS)
'            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
'        End Sub

'        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
'            MyBase.doBeforeLoadData(oForm)
'            'Set the global values
'            IDHAddOns.idh.addon.Base.PARENT.setGlobalValue("WPS_CUST", getParentSharedData(oForm, "IDH_CUST"))
'            IDHAddOns.idh.addon.Base.PARENT.setGlobalValue("WPS_CUSTNM", getParentSharedData(oForm, "IDH_CUSTNM"))
'            IDHAddOns.idh.addon.Base.PARENT.setGlobalValue("WPS_ADDR", getParentSharedData(oForm, "IDH_ADDR"))

'            'setSharedData(oForm, "WPIT_ITEMCODE", sItemCode)
'            'setSharedData(oForm, "WPIT_ITEMNAME", sItemName)
'            IDHAddOns.idh.addon.Base.PARENT.setGlobalValue("WPITM", "TRUE")

'            '    '        	doFillBPTypes(oForm)
'            '    '            doFillGroups(oForm)
'            '    '            doFillBranches(oForm)

'        End Sub

'        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
'            MyBase.doCompleteCreate(oForm, BubbleEvent)
'        End Sub
'        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)

'        End Sub

'        Private Function GetselectedRowIndex() As Int32
'            Dim i As Int32 = 0
'            i = oMatrix.GetNextSelectedRow(0, SAPbouiCOM.BoOrderType.ot_SelectionOrder)
'            Return i
'        End Function

'        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
'            If pVal.BeforeAction = True Then
'                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD OrElse _
'                    (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
'                    If bClosingFormByOKButton = False Then
'                        MyBase.doButtonID2(oForm, pVal, BubbleEvent)
'                    End If
'                End If
'            End If
'            If pVal.BeforeAction = False Then
'                Select Case pVal.EventType
'                    Case SAPbouiCOM.BoEventTypes.et_FORM_CLOSE
'                        If bClosingFormByOKButton = False Then
'                            MyBase.doButtonID2(oForm, pVal, BubbleEvent)
'                        End If
'                        'Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
'                        '    Dim sOrdrebyCol As String = "i.ItemCode"
'                        '    If (pVal.ItemUID = "IDH_GRPCOD" OrElse pVal.ItemUID = "IDH_GRPNAM") Then
'                        '        reQuerynReloadMatrix(oForm, sOrdrebyCol)
'                        '    End If
'                        '    BubbleEvent = False
'                    Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS 'SAPbouiCOM.BoEventTypes.et_VALIDATE
'                        Dim sOrdrebyCol As String = ""
'                        If (pVal.ItemUID = "IDH_WPCD" OrElse pVal.ItemUID = "IDH_WPNM" OrElse pVal.ItemUID = "IDH_ADDCD") Then
'                            Select Case pVal.ItemUID
'                                Case "IDH_WPCD"
'                                    sOrdrebyCol = "wp.U_ItemCd"
'                                Case "IDH_WPNM"
'                                    sOrdrebyCol = "wp.U_ItemNm"
'                                Case "IDH_ADDCD"
'                                    sOrdrebyCol = "wp.U_AddCd"
'                            End Select
'                            reQuerynReloadMatrix(oForm, sOrdrebyCol)
'                            BubbleEvent = False
'                        End If
'                    Case SAPbouiCOM.BoEventTypes.et_CLICK
'                        If pVal.ItemUID = "LINESMTX" Then
'                            If pVal.Row > 0 Then
'                                oMatrix.SelectRow(pVal.Row, True, False)
'                            End If
'                        End If
'                    Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
'                        'If pVal.BeforeAction = False Then
'                        If pVal.ItemUID = "LINESMTX" Then
'                            Me.setChoosenData(oForm, BubbleEvent)
'                        End If
'                        'End If
'                    Case SAPbouiCOM.BoEventTypes.et_KEY_DOWN
'                        If pVal.CharPressed <> 9 AndAlso pVal.CharPressed <> 38 AndAlso pVal.CharPressed <> 39 AndAlso pVal.CharPressed <> 40 AndAlso pVal.CharPressed <> 13 _
'                             AndAlso (pVal.ItemUID = "IDH_WPCD" OrElse pVal.ItemUID = "IDH_WPNM" OrElse pVal.ItemUID = "IDH_ADDCD") Then
'                            Select Case pVal.ItemUID
'                                Case "IDH_WPCD"
'                                    timer_bp.Enabled = False
'                                    timer_bp.Interval = 400
'                                    timer_Form = oForm
'                                    timer_ColUID = "ItemCd"
'                                    timer_ItemUID = pVal.ItemUID
'                                    If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
'                                        iLastselectedRow = 0
'                                        slastSearchedResult = ""
'                                    End If
'                                    timer_bp.Enabled = True
'                                    'doSelectOnKeyPress("CardCode", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)
'                                Case "IDH_WPNM"
'                                    timer_bp.Enabled = False
'                                    timer_bp.Interval = 400
'                                    timer_Form = oForm
'                                    timer_ColUID = "ItemNm"
'                                    timer_ItemUID = pVal.ItemUID
'                                    If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
'                                        iLastselectedRow = 0
'                                        slastSearchedResult = ""
'                                    End If
'                                    timer_bp.Enabled = True
'                                    'doSelectOnKeyPress("CardName", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)
'                                Case "IDH_ADDCD"
'                                    timer_bp.Enabled = False
'                                    timer_bp.Interval = 400
'                                    timer_Form = oForm
'                                    timer_ColUID = "AddCd"
'                                    timer_ItemUID = pVal.ItemUID
'                                    If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
'                                        iLastselectedRow = 0
'                                        slastSearchedResult = ""
'                                    End If
'                                    timer_bp.Enabled = True
'                            End Select
'                            BubbleEvent = False
'                        End If
'                        'If Not (pVal.ItemUID = "IDH_TYPE" OrElse pVal.ItemUID = "IDH_GROUP" OrElse pVal.ItemUID = "IDH_BRANCH") Then
'                        If (pVal.CharPressed = 40 OrElse pVal.CharPressed = 38 OrElse pVal.CharPressed = 34 OrElse pVal.CharPressed = 33) AndAlso (oMatrix.RowCount > 0) Then
'                            handleKeyEventForMatrixRowSelection(pVal.CharPressed)
'                        End If
'                        'End If
'                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
'                        If pVal.ItemUID = "IDH_NEW" Then
'                            'setSharedData(oForm, "ACT", "ADD")
'                            'setSharedData(oForm, "TYPE", "C")
'                            'goParent.doOpenModalForm("IDH_TFSANX", oForm)
'                            'Dim sWPITMCollection() As String = {getSharedData(oForm, "IDH_CUST"), getSharedData(oForm, "IDH_ADDR"), "-", "-", "TRUE"}
'                            setSharedData(oForm, "WPS_CUST", getParentSharedData(oForm, "IDH_CUST"))
'                            setSharedData(oForm, "WPS_CUSTNM", getParentSharedData(oForm, "IDH_CUSTNM"))
'                            setSharedData(oForm, "WPS_ADDR", getParentSharedData(oForm, "IDH_ADDR"))
'                            setSharedData(oForm, "WPITM", "TRUE")
'                            Dim sMsg As String = "Create new waste profile from...?"
'                            Dim iSelection As Integer = goParent.doMessage(sMsg, 1, "Template", "Blank Item", "Cancel", False)
'                            If iSelection = 1 Then
'                                'Open Item Search with Waste Profile Template checkbox checked 
'                                goParent.doOpenModalForm("IDHWPTISRC", oForm)
'                            ElseIf iSelection = 2 Then
'                                goParent.doOpenModalForm("150", oForm)
'                            End If
'                        ElseIf pVal.ItemUID = "IDH_CPYWP" Then
'                            Dim oUserName As String = goParent.gsUserName
'                            Dim oUserListCopyWP As String = Config.ParameterWithDefault("USCPYWPU", "")
'                            Try
'                                Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
'                                Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows
'                                Dim iSelectionCount As Integer = oSelected.Count

'                                If iSelectionCount > 0 Then
'                                    If oUserListCopyWP.Contains(oUserName.Trim()) Then
'                                        Dim iRow As Integer
'                                        iRow = oSelected.Item(0, SAPbouiCOM.BoOrderType.ot_RowOrder)

'                                        Dim sItmCd As String = oGridN.doGetFieldValue("wp.U_ItemCd", iRow)
'                                        Dim sItmNm As String = oGridN.doGetFieldValue("wp.U_ItemNm", iRow)
'                                        setSharedData(oForm, "SELROWNM", iRow)
'                                        setSharedData(oForm, "SELITMCD", sItmCd)
'                                        setSharedData(oForm, "SELITMNM", sItmNm)

'                                        setSharedData(oForm, "CPYTRG", "S")
'                                        setSharedData(oForm, "IDH_ADRTYP", "S")
'                                        goParent.doOpenModalForm("IDHASRCH2", oForm)
'                                    End If
'                                Else
'                                    doWarnMess("Select a row to use this option.", SAPbouiCOM.BoMessageTime.bmt_Long)
'                                End If

'                            Catch ex As Exception
'                            End Try
'                        ElseIf pVal.ItemUID = "IDH_DELWP" Then
'                            Try
'                                Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
'                                Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows
'                                Dim iSelectionCount As Integer = oSelected.Count

'                                If iSelectionCount > 0 Then
'                                    Dim iRow As Integer
'                                    iRow = oSelected.Item(0, SAPbouiCOM.BoOrderType.ot_RowOrder)

'                                    Dim sWPCode As String = oGridN.doGetFieldValue("wp.U_ProfileNo", iRow)
'                                    Dim sDelQry As String = "DELETE [@ISB_WSTPROFILE] WHERE Code = " + sWPCode
'                                    If goParent.goDB.doUpdateQuery(sDelQry) Then
'                                        doWarnMess("Waste Profile deleted successfully.", SAPbouiCOM.BoMessageTime.bmt_Long)
'                                        oGridN.doReloadData()
'                                    End If
'                                Else
'                                    doWarnMess("Select a row to use this option.", SAPbouiCOM.BoMessageTime.bmt_Long)
'                                End If
'                            Catch ex As Exception
'                            End Try
'                        End If
'                End Select
'            End If
'        End Function

'        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
'            Try
'                If sModalFormType = "IDHWPTISRC" Then
'                    Dim sItemCode As String = getSharedData(oForm, "ITEMCODE")
'                    Dim sItemName As String = getSharedData(oForm, "ITEMNAME")

'                    Dim oParentForm As SAPbouiCOM.Form = goParent.doGetParentForm(oForm.UniqueID)

'                    If sItemCode.Length > 0 Then
'                        setSharedData(oForm, "WPS_CUST", getSharedData(oForm, "WPS_CUST"))
'                        setSharedData(oForm, "WPS_CUSTNM", getSharedData(oForm, "WPS_CUSTNM"))
'                        setSharedData(oForm, "WPS_ADDR", getSharedData(oForm, "WPS_ADDR"))

'                        setSharedData(oForm, "WPIT_ITEMCODE", sItemCode)
'                        setSharedData(oForm, "WPIT_ITEMNAME", sItemName)
'                        setSharedData(oForm, "WPITM", "TRUE")
'                        Try
'                            goParent.doOpenModalForm("150", oForm)
'                        Catch ex As Exception
'                            com.idh.bridge.DataHandler.INSTANCE.doError("Exception loading Item Master Form: " + ex.ToString, "Error loading the form.")
'                        End Try
'                    End If
'                ElseIf sModalFormType = "150" Then
'                    'refresh the grid to get waste profile details 
'                    Dim sNewitemCode As String = getSharedData(oForm, "WPIT_ITEMCODE")
'                ElseIf sModalFormType = "IDHASRCH2" Then
'                    Dim sCpyTarget As String = getSharedData(oForm, "CPYTRG")
'                    Dim sCardCode As String = getSharedData(oForm, "CARDCODE")
'                    Dim sCardName As String = getSharedData(oForm, "CARDNAME")
'                    Dim sAddress As String = getSharedData(oForm, "ADDRESS")
'                    Dim sAddType As String = getSharedData(oForm, "ADRESTYPE")
'                    Dim sItemCode As String = getSharedData(oForm, "SELITMCD")
'                    Dim sItemName As String = getSharedData(oForm, "SELITMNM")
'                    Dim iRow As Integer = CInt(getSharedData(oForm, "SELROWNM"))

'                    Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
'                    oGridN.setCurrentRowIndex(iRow)

'                    Dim sNewItemCode As String = com.idh.bridge.lookups.Config.INSTANCE.doGetNextItemCode(sItemCode, com.idh.bridge.lookups.Config.ParameterWithDefault("ITMDPPOS", "_P"))
'                    Dim sNewItemName As String = sItemName + com.idh.bridge.lookups.Config.ParameterWithDefault("ITMDPPOS", "_P").ToString()

'                    Dim sMsg As String = "You are about to copy above selected Waste Profile to the following:"
'                    sMsg = sMsg + Chr(13) + "Selected Waste Profile: " + sItemCode + ": " + sItemName
'                    sMsg = sMsg + Chr(13)
'                    sMsg = sMsg + Chr(13) + "   NEW Waste Profile: " + sNewItemCode + ": " + sNewItemName
'                    sMsg = sMsg + Chr(13) + "   BP Code: " + sCardCode
'                    sMsg = sMsg + Chr(13) + "   BP Name: " + sCardName
'                    sMsg = sMsg + Chr(13) + "   Address: " + sAddress
'                    sMsg = sMsg + Chr(13)
'                    sMsg = sMsg + Chr(13) + "Are you sure you want to proceed?"

'                    Dim iSelection As Integer = goParent.doMessage(sMsg, 1, " Yes ", " No ", Nothing, False)
'                    If iSelection = 1 Then
'                        Try
'                            setSharedData(oForm, "WPS_CUST", sCardCode)
'                            setSharedData(oForm, "WPS_CUSTNM", sCardName)
'                            setSharedData(oForm, "WPS_ADDR", sAddress)

'                            setSharedData(oForm, "WPIT_ITEMCODE", sItemCode)
'                            setSharedData(oForm, "WPIT_ITEMNAME", sItemName)
'                            setSharedData(oForm, "WPITM", "TRUE")

'                            goParent.doOpenModalForm("150", oForm)

'                        Catch ex As Exception
'                            com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error copying waste profile across BP's")
'                        End Try

'                        doWarnMess("WP copied across successfully", SAPbouiCOM.BoMessageTime.bmt_Long)
'                    End If

'                End If
'            Catch ex As Exception
'                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal result - " & sModalFormType)
'            End Try
'        End Sub

'#Region "Timer Delay"
'        Private Sub timer_bp_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles timer_bp.Elapsed
'            timer_bp.Enabled = False
'            doSelectOnKeyPressNew(timer_ColUID, getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
'        End Sub
'#End Region
'        Private Sub doSelectOnKeyPressNew(ByVal sColUID As String, ByVal sValueToSearch As String)
'            Dim iRowCount As Int32 = oMatrix.RowCount
'            If sValueToSearch.Length = 0 Then
'                iLastselectedRow = 0
'                slastSearchedResult = ""
'                Return
'            ElseIf sValueToSearch.Length = 1 Then
'                If slastSearchedResult <> "" AndAlso sValueToSearch.StartsWith(slastSearchedResult) Then
'                    Return
'                End If
'                iLastselectedRow = 0
'                slastSearchedResult = ""
'                Dim iIndex As Int32 = SearchValue(sColUID, sValueToSearch)
'                If oMatrix.RowCount >= iIndex Then
'                    oMatrix.SelectRow(iIndex, True, False)
'                    iLastselectedRow = iIndex
'                    slastSearchedResult = sValueToSearch
'                    oMatrix.SelectRow(iIndex, True, False)
'                End If
'            ElseIf sValueToSearch.Length > 1 Then
'                iLastselectedRow = 0
'                slastSearchedResult = ""
'                Dim iIndex As Int32 = SearchValue(sColUID, sValueToSearch)
'                If oMatrix.RowCount >= iIndex Then
'                    oMatrix.SelectRow(iIndex, True, False)
'                    iLastselectedRow = iIndex
'                    slastSearchedResult = sValueToSearch
'                    oMatrix.SelectRow(iIndex, True, False)
'                End If
'            End If

'        End Sub

'        Private Function midpoint(ByVal iMin As Int32, ByVal iMax As Int32) As Int32
'            Return Math.Floor((iMin + iMax) / 2.0)
'        End Function

'        Private Function SearchValue(ByVal sColUID As String, ByVal sValueToSearch As String) As Int32
'            Try
'                Dim iMin As Int32 = 1, iMax As Int32 = oMatrix.RowCount
'                While (iMax >= iMin)
'                    Dim imid As Int32 = midpoint(iMin, iMax)
'                    If ((CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim().StartsWith(sValueToSearch)) Then
'                        Return imid
'                    ElseIf (CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim() < (sValueToSearch) OrElse _
'                            String.Compare((CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim(), (sValueToSearch)) < 0 Then
'                        iMin = imid + 1
'                    ElseIf (CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim() > (sValueToSearch) OrElse _
'                            String.Compare((CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim(), (sValueToSearch)) > 0 Then
'                        iMax = imid - 1

'                    End If
'                End While
'                Return 1
'            Catch ex As Exception
'                Return 1
'            End Try
'        End Function

'        Private Sub handleKeyEventForMatrixRowSelection(ByVal Keycode As Int32)
'            If Keycode = 40 Then
'                Dim row As Int32 = GetselectedRowIndex()
'                If row < 1 Then
'                    oMatrix.SelectRow(1, True, False)
'                ElseIf row < oMatrix.RowCount Then
'                    oMatrix.SelectRow(row + 1, True, False)
'                End If
'            ElseIf Keycode = 38 Then
'                Dim row As Int32 = GetselectedRowIndex()
'                If row <= 1 Then
'                    oMatrix.SelectRow(1, True, False)
'                ElseIf row <= oMatrix.RowCount Then
'                    oMatrix.SelectRow(row - 1, True, False)
'                End If
'            ElseIf Keycode = 34 Then 'Pg Dn
'                Dim row As Int32 = GetselectedRowIndex()
'                If (row + 10) <= oMatrix.RowCount Then
'                    oMatrix.SelectRow(row + 10, True, False)
'                ElseIf row < 1 Then
'                    oMatrix.SelectRow(1, True, False)
'                Else
'                    oMatrix.SelectRow(oMatrix.RowCount, True, False)
'                End If
'            ElseIf Keycode = 33 Then 'Pg Up
'                Dim row As Int32 = GetselectedRowIndex()
'                If (row - 10) > 0 Then
'                    oMatrix.SelectRow(row - 10, True, False)
'                ElseIf row <= 1 Then
'                    oMatrix.SelectRow(1, True, False)
'                Else
'                    oMatrix.SelectRow(1, True, False)
'                End If
'            End If
'        End Sub


'        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
'            If pVal.ItemUID = "1" AndAlso pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
'                Me.setChoosenData(oForm, BubbleEvent)
'            End If
'        End Sub

'        Private Sub setChoosenData(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
'            If oMatrix.RowCount = 0 Then Exit Sub
'            Dim i As Int32 = 0
'            i = GetselectedRowIndex()
'            If i > 0 AndAlso i <= oMatrix.RowCount Then
'                Me.SetParentSharedData(i, oForm)
'                Me.doReturnFromModalShared(oForm, True)
'                BubbleEvent = False
'            End If
'        End Sub

'        Overloads Sub SetParentSharedData(ByVal iRow As Int32, ByVal oForm As SAPbouiCOM.Form)
'            bClosingFormByOKButton = True
'            Me.SetParentSharedData(oForm, "PROFNO", oMatrix.Columns.Item("ProfileNo").Cells.Item(iRow).Specific.value.ToString.Trim)
'            Me.SetParentSharedData(oForm, "DETAIL", oMatrix.Columns.Item("ItemCd").Cells.Item(iRow).Specific.value.ToString.Trim)

'            Me.SetParentSharedData(oForm, "BPNAME", oMatrix.Columns.Item("ItemNm").Cells.Item(iRow).Specific.value.ToString.Trim)
'            Me.SetParentSharedData(oForm, "ADDR", oMatrix.Columns.Item("AddCd").Cells.Item(iRow).Specific.value.ToString.Trim)
'        End Sub
'    End Class

'End Namespace