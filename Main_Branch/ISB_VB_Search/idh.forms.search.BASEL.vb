Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class BASEL
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHBASELSC", "TFS BASEL Search.srf", 5, 45, 603, 320, "TFS BASEL Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_BASEL"))
            oGridN.setOrderValue("Code")

            oGridN.doAddFilterField("IDH_BSCD", "U_BASELCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDH_BSNM", "U_Desc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)

            oGridN.doAddListField("U_BASELCd", "BASEL Code", False, -1, Nothing, "IDHBSCD")
            oGridN.doAddListField("U_Desc", "Description", False, -1, Nothing, "IDHBSNM")
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            'doFillCombo(oForm, "IDHTFSSTA", "[@IDH_TFSStatus]", "U_SCode", "U_SName", " U_SCode <= 6 ")

            'Dim oItem As SAPbouiCOM.Item
            'oItem = oForm.Items.Item("IDHTFSSTA")
            'Dim oCombo As SAPbouiCOM.ComboBox
            'oCombo = oItem.Specific()

            'Dim oValidValues As SAPbouiCOM.ValidValues
            'oValidValues = oCombo.ValidValues
            ''doClearValidValues(oValidValues)

            'oValidValues.Add("01,05,06", "Available, Acknowledged, Consented")

            MyBase.doBeforeLoadData(oForm)
        End Sub

    End Class
End Namespace
