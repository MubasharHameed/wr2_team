Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class UN
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDH_WTUNSR", "UN Search.srf", 5, 45, 603, 320, "UN Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("[@IDH_WTUN]")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_WTUN", Nothing, Nothing, False, True))
            
            oGridN.setOrderValue("Code")

            oGridN.doAddFilterField("IDH_UNCOD", "U_WTUNCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 50)
            oGridN.doAddFilterField("IDH_UNNAM", "U_WTUNName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100)
            oGridN.doAddFilterField("IDH_WTUCL", "U_WTUCL", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100)

            'oGridN.doAddListField("Code", "Code", False, -1, Nothing, "IDH_CODE")
            'oGridN.doAddListField("Name", "Name", False, -1, Nothing, "IDH_NAME")
            If IDHAddOns.idh.lookups.Base.INSTANCE.getParameterWithDefault("TFSMOD", "false") = "TFSMOD_TRUE" Then
                oGridN.doAddListField("U_WTUNCode", "UN Class", False, -1, Nothing, "IDH_UNCODE")
                oGridN.doAddListField("U_WTUNName", "Description", False, -1, Nothing, "IDH_UNNAM")
            Else
                oGridN.doAddListField("U_WTUNCode", "UN Code", False, -1, Nothing, "IDH_UNCODE")
                oGridN.doAddListField("U_WTUNName", "UN Name", False, -1, Nothing, "IDH_UNNAM")

                oGridN.doAddListField("U_WTUCL", "UN Class", False, -1, Nothing, "IDH_WTUCL")
                oGridN.doAddListField("U_WTUMT", "Material Type", False, -1, Nothing, "IDH_WTUMT")
                oGridN.doAddListField("U_WTUST", "State", False, -1, Nothing, "IDH_WTUST")
                oGridN.doAddListField("U_WTUFP", "Flashpoint", False, -1, Nothing, "IDH_WTUFP")
                oGridN.doAddListField("U_WTUDE", "Density", False, -1, Nothing, "IDH_WTUDE")
                oGridN.doAddListField("U_WTUPH", "PH", False, -1, Nothing, "IDH_WTUPH")
                oGridN.doAddListField("U_WTUPG", "Pack Group", False, -1, Nothing, "IDH_WTUPG")
            End If
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace