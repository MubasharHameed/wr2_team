Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge.lookups
Imports com.idh.bridge

Namespace idh.forms.search
    Public Class BPSearch2
        Inherits IDHAddOns.idh.forms.Search
        Private FormLoaded As Boolean = False
        Private oMatrix As SAPbouiCOM.Matrix
        Private _sSQL As String = ""
        Private iLastselectedRow As Int32 = 0
        Private slastSearchedResult As String = ""
        Private bClosingFormByOKButton As Boolean = False
        Private WithEvents timer_bp As System.Timers.Timer

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHNCSRCH", "BP Search_2.srf", 5, 40, 770, 360, "BP Search.")
        End Sub

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            bClosingFormByOKButton = False
            Me.SetParentSharedData(oForm, "IDH_BPSRCHOPEND", "LOADED") 'setSharedData("IDH_BPSRCHOPEND", "LOADED")
            _sSQL = ""
            iLastselectedRow = 0
            slastSearchedResult = ""
            setUFValue(oForm, "IDH_BPCOD", getParentSharedData(oForm, "IDH_BPCOD"))
            setUFValue(oForm, "IDH_NAME", getParentSharedData(oForm, "IDH_NAME"))
            setUFValue(oForm, "IDH_ADRES", "")
            setUFValue(oForm, "IDH_STREET", "")
            reQuerynReloadMatrix(oForm)
        End Sub
        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            Try

                oForm.Items.Item("LINESGRID").Visible = False
                Dim bFlag As Boolean = False
                Dim sSILENT As String = ""
                If (Me.getHasSharedData(oForm)) Then
                    sSILENT = Me.getParentSharedDataAsString(oForm, "SILENT") '.ToString
                    If (sSILENT <> Nothing) Then
                        If (sSILENT.ToUpper().Equals("SHOWMULTI")) Then
                            bFlag = True 'Means we have some value in CardCode/cardname
                        End If
                    End If
                End If
                If (bflag AndAlso oMatrix.RowCount > 0) Then
                    oMatrix.SelectRow(1, True, False)
                    If (oMatrix.RowCount = 1) Then
                        Me.SetParentSharedData(1, oForm)
                        Me.doReturnFromModalShared(oForm, True)
                        Me.SetParentSharedData(oForm, "IDH_BPSRCHOPEND", "")
                        bClosingFormByOKButton = True
                        Return
                    End If
                End If
                oForm.Visible = True
                'test.EndTime = Now
                'oForm.Title = test.StartTime.Subtract(test.EndTime).TotalMilliseconds.ToString
                Dim otxt As SAPbouiCOM.EditText = CType(oForm.Items.Item("IDH_BPCOD").Specific, SAPbouiCOM.EditText)
                otxt.Active = True
                FormLoaded = True

                'oForm.Title = Now.Subtract(test.StartTime).TotalMilliseconds
                timer_bp = New System.Timers.Timer(700)
                timer_bp.Enabled = False
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Groups.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Groups")})
            End Try

        End Sub



        Private Function PrepareSQL(ByVal oform As SAPbouiCOM.Form, Optional ByVal sOrderbyCol As String = "") As String

            iLastselectedRow = 0
            slastSearchedResult = ""
            Dim sSQL As String = "SELECT  b.CardCode ,b.CardName ,b.CardType,b.CardFName ,b.GroupCode ,g.GroupName , " _
                                    & " c.RemainBal, b.OrdersBal, b.CreditLine, c.Balance, c.TBalance, c.WRBalance, " _
                                    & " b.U_IDHBRAN, b.Balance, c.Deviation, c.frozenFor, c.FrozenComm, b.Phone1, " _
                                    & " b.CntctPrsn, b.U_WASLIC, b.U_IDHICL, b.U_IDHLBPC, b.U_IDHLBPN, b.U_IDHOBLGT, " _
                                    & " b.U_IDHUOM, b.U_IDHONCS, c.frozenFrom, c.frozenTo, b.ListNum, b.GroupNum, " _
                                    & " b.BillToDef ,b.Address ,b.ZipCode FROM   " _
                                    & " [OCRD] b,[OCRG] g, " _
                                    & " [IDH_VWR1BAL] c " _
                                    & " WHERE b.GroupCode = g.GroupCode And b.CardCode=c.CardCode "

            PrepareSQL = sSQL
            Dim oCombo As SAPbouiCOM.ComboBox

            oCombo = CType(oform.Items.Item("IDH_TYPE").Specific, SAPbouiCOM.ComboBox)
            Dim sType As String = oCombo.Selected.Value

            oCombo = CType(oform.Items.Item("IDH_GROUP").Specific, SAPbouiCOM.ComboBox)
            Dim sGroup As String = oCombo.Selected.Value

            oCombo = CType(oform.Items.Item("IDH_BRANCH").Specific, SAPbouiCOM.ComboBox)
            Dim sBranch As String = oCombo.Selected.Value

            Dim sBPCode As String = getItemValue(oform, "IDH_BPCOD") 'otextBox.Value.Trim


            Dim sBPName As String = getItemValue(oform, "IDH_NAME") 'otextBox.Value.Trim

            Dim sBPBillingAddress As String = getItemValue(oform, "IDH_ADRES") 'otextBox.Value.Trim

            Dim sBPStreet As String = getItemValue(oform, "IDH_STREET") 'otextBox.Value.Trim

            Dim sWhereSql As String = ""
            If sType <> "" Then
                sWhereSql &= " and b.CardType LIKE '" & sType.Replace("*", "%") & "' "
            End If

            If sGroup <> "" Then
                sWhereSql &= " and g.GroupCode ='" & sGroup.Replace("*", "%") & "' "
            End If

            If sBranch <> "" Then
                sWhereSql &= " and b.U_IDHBRAN = '" & sBranch.Replace("*", "%") & "' "
            End If

            If sBPCode <> "" Then
                sWhereSql &= " and b.CardCode LIKE '" & sBPCode.Replace("*", "%") & "%' "
            End If

            If sBPName <> "" Then
                sWhereSql &= " and b.CardName LIKE '" & sBPName.Replace("*", "%") & "%' "
            End If


            If sBPBillingAddress <> "" Then
                sWhereSql &= " and b.BillToDef LIKE '" & sBPBillingAddress.Replace("*", "%") & "%' "
            End If


            If sBPStreet <> "" Then
                sWhereSql &= " and b.Address LIKE '" & sBPStreet.Replace("*", "%") & "%' "
            End If
            If sWhereSql <> "" Then
                sSQL &= " " & sWhereSql
            End If
            If sOrderbyCol <> "" Then
                sSQL &= " Order by " & sOrderbyCol
            End If
            Return sSQL
        End Function
        Private Sub reQuerynReloadMatrix(ByVal oform As SAPbouiCOM.Form, Optional ByVal sOrderByCol As String = "")
            iLastselectedRow = 0
            slastSearchedResult = ""
            Dim sSQL As String = PrepareSQL(oform, sOrderByCol).Trim
            If (sSQL.Trim.ToLower.Equals(_sSQL.ToLower)) AndAlso _sSQL <> "" Then
                Return
            End If
            _sSQL = sSQL
            oMatrix = CType(oform.Items.Item("LINESMTX").Specific, SAPbouiCOM.Matrix)
            oform.DataSources.DataTables.Item("LINESMTX").ExecuteQuery(sSQL)
            oMatrix.Clear()
            oMatrix.LoadFromDataSourceEx(False)
            Return
        End Sub
        
        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
        End Sub
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_GOT_FOCUS)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
        End Sub
        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)
        End Sub
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub
        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            
            doFillGroups(oForm)
            doFillBranches(oForm)
            MyBase.doReadInputParams(oForm)
            SetParentSharedData(oForm, "CALLEDITEM", getParentSharedData(oForm, "CALLEDITEM"))
            Dim sVal As String = getParentSharedDataAsString(oForm, "IDH_TYPE")
            If Not sVal Is Nothing Then
                If sVal.StartsWith("F-") Then
                    sVal = sVal.Substring(2)
                    doFillBPTypes(oForm, sVal)

                    'OnTime [#Ico000????] USA 
                    'START
                    'Enabling Customer Type Dropdown to allow selection of 'Lead' customer 
                    If Config.INSTANCE.getParameterAsBool("USAREL", False) Then
                        setEnableItem(oForm, True, "IDH_TYPE")
                    Else
                        setEnableItem(oForm, False, "IDH_TYPE")
                    End If
                    'END 
                    'OnTime [#Ico000????] USA 
                Else
                    doFillBPTypes(oForm, sVal)
                    If sVal.Length > 1 Then
                        sVal = sVal.Substring(0, 1)
                    End If
                    setEnableItem(oForm, True, "IDH_TYPE")
                End If
                'setUFValue(oForm, "IDH_TYPE", sVal)
            Else
                doFillBPTypes(oForm)
            End If

        End Sub

        Private Sub doFillBranches(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_BRANCH", "OUBR", "Code", "Remarks", Nothing, "Code", True)
        End Sub

        Private Sub doFillGroups(ByVal oForm As SAPbouiCOM.Form)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                Dim oGroups As SAPbouiCOM.ComboBox = Nothing
                Dim oItem As SAPbouiCOM.Item
                Dim sBPType As String = oForm.DataSources.UserDataSources.Item("IDH_TYPE").ValueEx

                oRecordSet = CType(goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
                Dim sQry As String = "select GroupCode, GroupName from OCRG where GroupType Like '%" & sBPType & "'"
                oRecordSet.DoQuery(sQry)
                If oRecordSet.RecordCount > 0 Then
                    oItem = oForm.Items.Item("IDH_GROUP")
                    oItem.DisplayDesc = True
                    oGroups = CType(oItem.Specific, SAPbouiCOM.ComboBox)
                    Dim iCount As Integer
                    Dim oVal1 As String
                    Dim oVal2 As String

                    Dim oValValues As SAPbouiCOM.ValidValues
                    oValValues = oGroups.ValidValues
                    'First Clear all the values
                    If Not oValValues Is Nothing Then
                        While oValValues.Count > 0
                            oValValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                        End While
                    End If

                    oValValues.Add("", getTranslatedWord("Any"))
                    For iCount = 0 To oRecordSet.RecordCount - 1
                        oVal1 = CType(oRecordSet.Fields.Item(0).Value, String)
                        oVal2 = CType(oRecordSet.Fields.Item(1).Value, String)
                        oValValues.Add(oVal1, oVal2)
                        oRecordSet.MoveNext()
                    Next
                End If

                Dim sSelectGroupTypeValue As String = getParentSharedDataAsString(oForm, "IDH_GROUP")
                If sSelectGroupTypeValue IsNot Nothing AndAlso sSelectGroupTypeValue.Trim <> "" AndAlso oGroups.ValidValues.Count > 0 Then
                    Try 'If user selects some other BP Type which might not have BP Group ID provided then to handle exception
                        oGroups.SelectExclusive(sSelectGroupTypeValue, SAPbouiCOM.BoSearchKey.psk_ByValue)
                    Catch ex As Exception
                        oGroups.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index)
                    End Try
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Groups.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Groups")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

        '* A Any
        '* S Supplier
        '* C Customer
        '* L Lead
        Private Sub doFillBPTypes(ByVal oForm As SAPbouiCOM.Form, Optional ByVal selectedvalue As String = "", Optional ByVal sSwitch As String = "ASCL")
            Dim oBPType As SAPbouiCOM.ComboBox
            Dim oItem As SAPbouiCOM.Item

            oItem = oForm.Items.Item("IDH_TYPE")
            oItem.DisplayDesc = True
            oBPType = CType(oItem.Specific, SAPbouiCOM.ComboBox)

            If oBPType.ValidValues.Count = 0 Then
                If sSwitch.Length = 0 Then
                    oBPType.ValidValues.Add("", getTranslatedWord("Any"))
                    oBPType.ValidValues.Add("S", getTranslatedWord("Vendor"))
                    oBPType.ValidValues.Add("C", getTranslatedWord("Customer"))
                    oBPType.ValidValues.Add("L", getTranslatedWord("Lead"))
                Else
                    If sSwitch.IndexOf("A") > -1 Then
                        oBPType.ValidValues.Add("", getTranslatedWord("Any"))
                    End If
                    If sSwitch.IndexOf("S") > -1 Then
                        oBPType.ValidValues.Add("S", getTranslatedWord("Vendor"))
                    End If
                    If sSwitch.IndexOf("C") > -1 Then
                        oBPType.ValidValues.Add("C", getTranslatedWord("Customer"))
                    End If
                    If sSwitch.IndexOf("L") > -1 Then
                        oBPType.ValidValues.Add("L", getTranslatedWord("Lead"))
                    End If
                End If
            End If
            oBPType.SelectExclusive(selectedvalue, SAPbouiCOM.BoSearchKey.psk_ByValue)
        End Sub
        Private Function GetselectedRowIndex() As Int32
            Dim i As Int32 = 0
            i = oMatrix.GetNextSelectedRow(0, SAPbouiCOM.BoOrderType.ot_SelectionOrder)
            Return i
        End Function
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.SetParentSharedData(oForm, "IDH_BPSRCHOPEND", "")
                    If bClosingFormByOKButton = False Then
                        bClosingFormByOKButton = True
                        MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                    End If
                End If
                Return True
            End If

            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_CLOSE
                    setSharedData(oForm, "IDH_BPSRCHOPEND", "")
                    If bClosingFormByOKButton = False Then
                        MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                    End If
                Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                    'If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_TYPE" Then
                        doFillGroups(oForm)
                        reQuerynReloadMatrix(oForm)
                        BubbleEvent = False
                    ElseIf pVal.ItemUID = "IDH_GROUP" OrElse pVal.ItemUID = "IDH_BRANCH" Then
                        reQuerynReloadMatrix(oForm)
                        BubbleEvent = False
                    End If
                    'End If
                Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS
                    'If pVal.BeforeAction = False Then
                    Dim sOrdrebyCol As String = ""
                    If pVal.ItemUID = "IDH_BPCOD" OrElse pVal.ItemUID = "IDH_NAME" OrElse _
                    pVal.ItemUID = "IDH_ADRES" OrElse pVal.ItemUID = "IDH_STREET" Then
                        Select Case pVal.ItemUID
                            Case "IDH_BPCOD"
                                sOrdrebyCol = "b.CardCode"
                            Case "IDH_NAME"
                                sOrdrebyCol = "b.CardName"
                            Case "IDH_ADRES"
                                sOrdrebyCol = "b.BillToDef"
                            Case "IDH_STREET"
                                sOrdrebyCol = "b.Address"
                        End Select
                        reQuerynReloadMatrix(oForm, sOrdrebyCol)
                        BubbleEvent = False
                        'End If
                    End If
                    'End If
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    'If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_NEW" Then
                        setSharedData(oForm, "ACT", "ADD")
                        setSharedData(oForm, "TYPE", "C")
                        goParent.doOpenModalForm("134", oForm)
                    End If
                    'End If
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "LINESMTX" Then
                        If pVal.Row > 0 Then
                            oMatrix.SelectRow(pVal.Row, True, False)
                        End If
                    End If
                Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                    'If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "LINESMTX" Then
                        Me.setChoosenData(oForm, BubbleEvent)
                    End If
                    'End If
                Case SAPbouiCOM.BoEventTypes.et_KEY_DOWN
                    ''AndAlso pVal.CharPressed <> 38 AndAlso pVal.CharPressed <> 39 AndAlso pVal.CharPressed <> 40 AndAlso pVal.CharPressed <> 13 _
                    If pVal.CharPressed <> 9 AndAlso pVal.CharPressed <> 38 AndAlso pVal.CharPressed <> 39 AndAlso pVal.CharPressed <> 40 AndAlso pVal.CharPressed <> 13 _
                    AndAlso (pVal.ItemUID = "IDH_BPCOD" OrElse pVal.ItemUID = "IDH_NAME" OrElse pVal.ItemUID = "IDH_ADRES" OrElse pVal.ItemUID = "IDH_STREET") Then
                        Select Case pVal.ItemUID
                            Case "IDH_BPCOD"
                                timer_bp.Enabled = False
                                timer_bp.Interval = 400
                                timer_Form = oForm
                                timer_ColUID = "CardCode"
                                timer_ItemUID = pVal.ItemUID
                                If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
                                    iLastselectedRow = 0
                                    slastSearchedResult = ""
                                End If
                                timer_bp.Enabled = True
                                'doSelectOnKeyPress("CardCode", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)
                            Case "IDH_NAME"
                                timer_bp.Enabled = False
                                timer_bp.Interval = 400
                                timer_Form = oForm
                                timer_ColUID = "CardName"
                                timer_ItemUID = pVal.ItemUID
                                If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
                                    iLastselectedRow = 0
                                    slastSearchedResult = ""
                                End If
                                timer_bp.Enabled = True
                                'doSelectOnKeyPress("CardName", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)
                            Case "IDH_ADRES"
                                timer_bp.Enabled = False
                                timer_bp.Interval = 400
                                timer_Form = oForm
                                timer_ColUID = "BillToDef"
                                timer_ItemUID = pVal.ItemUID
                                If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
                                    iLastselectedRow = 0
                                    slastSearchedResult = ""
                                End If
                                timer_bp.Enabled = True
                                'doSelectOnKeyPress("BillToDef", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)
                            Case "IDH_STREET"
                                timer_bp.Enabled = False
                                timer_bp.Interval = 400
                                timer_Form = oForm
                                timer_ColUID = "Address"
                                timer_ItemUID = pVal.ItemUID
                                If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
                                    iLastselectedRow = 0
                                    slastSearchedResult = ""
                                End If
                                timer_bp.Enabled = True
                                'doSelectOnKeyPress("Address", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)
                        End Select
                        BubbleEvent = False
                    End If

                    If Not (pVal.ItemUID = "IDH_TYPE" OrElse pVal.ItemUID = "IDH_GROUP" OrElse pVal.ItemUID = "IDH_BRANCH") Then
                        If (pVal.CharPressed = 40 OrElse pVal.CharPressed = 38 OrElse pVal.CharPressed = 34 OrElse pVal.CharPressed = 33) AndAlso (oMatrix.RowCount > 0) Then
                            handleKeyEventForMatrixRowSelection(pVal.CharPressed)
                            BubbleEvent = False
                        End If
                    End If
            End Select
            Return True
        End Function
#Region "Timer Delay"
        Private timer_Form As SAPbouiCOM.Form
        Private timer_ColUID, timer_ItemUID As String
        'Private Shared Sub OnTimedEvent(ByVal source As Object, ByVal e As Timers.ElapsedEventArgs)
        '    ' Console.WriteLine("The Elapsed event was raised at {0}", e.SignalTime)
        '    Dim timer As System.Timers.Timer = source

        '    doSelectOnKeyPress("CardCode", getItemValue(Me.MyForm_bpTimer, "IDH_BPCOD").Trim.ToLower)
        'End Sub

        Private Sub timer_bp_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles timer_bp.Elapsed
            timer_bp.Enabled = False
            doSelectOnKeyPressNew(timer_ColUID, getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
            'Select Case (timer_ColUID)
            '    Case "CardCode"
            '        doSelectOnKeyPressNew("CardCode", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
            '    Case "CardName"
            '        doSelectOnKeyPressNew("CardName", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
            '    Case "BillToDef"
            '        doSelectOnKeyPressNew("BillToDef", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
            '    Case "Address"
            '        doSelectOnKeyPressNew("Address", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
            '        'Case ""
            '        '   doSelectOnKeyPress("CardCode", getItemValue(Me.timer_Form, "IDH_BPCOD").Trim.ToLower)
            'End Select

        End Sub
#End Region
        Private Sub doSelectOnKeyPress(ByVal sColUID As String, ByVal sValueToSearch As String)
            Dim iRowCount As Int32 = oMatrix.RowCount
            If sValueToSearch.Length = 0 Then
                iLastselectedRow = 0
                slastSearchedResult = ""
                Return
            ElseIf sValueToSearch.Length = 1 Then
                If slastSearchedResult <> "" AndAlso sValueToSearch.StartsWith(slastSearchedResult) Then
                    Return
                End If
                iLastselectedRow = 0
                slastSearchedResult = ""
                For i As Int32 = 1 To iRowCount
                    If ((CType(oMatrix.Columns.Item(sColUID).Cells.Item(i).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim().StartsWith(sValueToSearch)) Then
                        oMatrix.SelectRow(i, True, False)
                        iLastselectedRow = i
                        slastSearchedResult = sValueToSearch
                        oMatrix.SelectRow(i, True, False)
                        Exit For
                    End If
                Next
            ElseIf sValueToSearch.Length > 1 Then
                'If (iLastselectedRow = 0) Then
                '    Return
                'End If
                Dim iStart As Int32 = 1
                If iLastselectedRow <> 0 AndAlso (oMatrix.GetNextSelectedRow(0, SAPbouiCOM.BoOrderType.ot_SelectionOrder) > 0) Then
                    If iLastselectedRow <= oMatrix.RowCount AndAlso (sValueToSearch) >= (CType(oMatrix.Columns.Item(sColUID).Cells.Item(iLastselectedRow).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim() Then
                        iStart = oMatrix.GetNextSelectedRow(0, SAPbouiCOM.BoOrderType.ot_SelectionOrder)
                    End If
                End If
                iLastselectedRow = 0
                slastSearchedResult = ""
                'If (oMatrix.GetNextSelectedRow(0, SAPbouiCOM.BoOrderType.ot_SelectionOrder) <> 0) Then
                For i As Int32 = iStart To iRowCount
                    If ((CType(oMatrix.Columns.Item(sColUID).Cells.Item(i).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim().StartsWith(sValueToSearch)) Then
                        oMatrix.SelectRow(i, True, False)
                        iLastselectedRow = i
                        slastSearchedResult = sValueToSearch
                        oMatrix.SelectRow(i, True, False)
                        Exit For
                    End If
                Next
                'End If
            End If

        End Sub

        Private Sub doSelectOnKeyPressNew(ByVal sColUID As String, ByVal sValueToSearch As String)
            Dim iRowCount As Int32 = oMatrix.RowCount
            If sValueToSearch.Length = 0 Then
                iLastselectedRow = 0
                slastSearchedResult = ""
                Return
            ElseIf sValueToSearch.Length = 1 Then
                If slastSearchedResult <> "" AndAlso sValueToSearch.StartsWith(slastSearchedResult) Then
                    Return
                End If
                iLastselectedRow = 0
                slastSearchedResult = ""
                Dim iIndex As Int32 = SearchValue(sColUID, sValueToSearch)
                If oMatrix.RowCount >= iIndex Then
                    oMatrix.SelectRow(iIndex, True, False)
                    iLastselectedRow = iIndex
                    slastSearchedResult = sValueToSearch
                    oMatrix.SelectRow(iIndex, True, False)
                End If
            ElseIf sValueToSearch.Length > 1 Then
                iLastselectedRow = 0
                slastSearchedResult = ""
                Dim iIndex As Int32 = SearchValue(sColUID, sValueToSearch)
                If oMatrix.RowCount >= iIndex Then
                    oMatrix.SelectRow(iIndex, True, False)
                    iLastselectedRow = iIndex
                    slastSearchedResult = sValueToSearch
                    oMatrix.SelectRow(iIndex, True, False)
                End If
            End If

        End Sub
        Private Function midpoint(ByVal iMin As Int32, ByVal iMax As Int32) As Int32
            Return CType(Math.Floor((iMin + iMax) / 2.0), Integer)
        End Function

        Private Function SearchValue(ByVal sColUID As String, ByVal sValueToSearch As String) As Int32
            Try
                Dim iMin As Int32 = 1, iMax As Int32 = oMatrix.RowCount
                While (iMax >= iMin)
                    Dim imid As Int32 = midpoint(iMin, iMax)
                    If ((CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim().StartsWith(sValueToSearch)) Then
                        Return imid
                    ElseIf (CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim() < (sValueToSearch) OrElse _
                            String.Compare((CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim(), (sValueToSearch)) < 0 Then
                        iMin = imid + 1
                    ElseIf (CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim() > (sValueToSearch) OrElse _
                            String.Compare((CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim(), (sValueToSearch)) > 0 Then
                        iMax = imid - 1

                    End If
                End While
                Return 1
            Catch ex As Exception
                Return 1
            End Try
        End Function


        Private Sub handleKeyEventForMatrixRowSelection(ByVal Keycode As Int32)
            If Keycode = 40 Then
                Dim row As Int32 = GetselectedRowIndex()
                If row < 1 Then
                    oMatrix.SelectRow(1, True, False)
                ElseIf row < oMatrix.RowCount Then
                    oMatrix.SelectRow(row + 1, True, False)
                End If
            ElseIf Keycode = 38 Then
                Dim row As Int32 = GetselectedRowIndex()
                If row <= 1 Then
                    oMatrix.SelectRow(1, True, False)
                ElseIf row <= oMatrix.RowCount Then
                    oMatrix.SelectRow(row - 1, True, False)
                End If
            ElseIf Keycode = 34 Then 'Pg Dn
                Dim row As Int32 = GetselectedRowIndex()
                If (row + 10) <= oMatrix.RowCount Then
                    oMatrix.SelectRow(row + 10, True, False)
                ElseIf row < 1 Then
                    oMatrix.SelectRow(1, True, False)
                Else
                    oMatrix.SelectRow(oMatrix.RowCount, True, False)
                End If
            ElseIf Keycode = 33 Then 'Pg Up
                Dim row As Int32 = GetselectedRowIndex()
                If (row - 10) > 0 Then
                    oMatrix.SelectRow(row - 10, True, False)
                ElseIf row <= 1 Then
                    oMatrix.SelectRow(1, True, False)
                Else
                    oMatrix.SelectRow(1, True, False)
                End If
            End If
        End Sub


        Public Shared Function doGetBPInfo(ByVal oParent As IDHAddOns.idh.addon.Base, ByRef oCallerForm As SAPbouiCOM.Form, ByVal sCardCode As String) As Boolean
            Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            Try
                oBP = CType(oParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners), SAPbobsCOM.BusinessPartners)
                If oBP.GetByKey(sCardCode) Then
                    setSharedData(oCallerForm, "CARDCODE", oBP.CardCode)
                    setSharedData(oCallerForm, "CARDNAME", oBP.CardName)
                    setSharedData(oCallerForm, "CARDTYPE", oBP.CardType)
                    setSharedData(oCallerForm, "GROUPCODE", oBP.GroupCode)
                    setSharedData(oCallerForm, "GROUPNAME", "")
                    setSharedData(oCallerForm, "BALANCEFC", oBP.CurrentAccountBalance)
                    setSharedData(oCallerForm, "CREDITLINE", oBP.CreditLimit)
                    setSharedData(oCallerForm, "PHONE1", oBP.Phone1)
                    setSharedData(oCallerForm, "CNTCTPRSN", oBP.ContactPerson)
                    setSharedData(oCallerForm, "WASLIC", oBP.UserFields.Fields.Item("U_WASLIC").Value)
                    Return True
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error getting the BP Info - " & sCardCode)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXBPI", {sCardCode})
            Finally
                DataHandler.INSTANCE.doReleaseObject(CType(oBP, Object))
            End Try
            Return False
        End Function
        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.ItemUID = "1" AndAlso pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                Me.setChoosenData(oForm, BubbleEvent)
            End If
        End Sub

        Private Sub setChoosenData(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            If oMatrix.RowCount = 0 Then Exit Sub
            Dim i As Int32 = 0
            i = GetselectedRowIndex()
            If i > 0 AndAlso i <= oMatrix.RowCount Then
                Me.SetParentSharedData(i, oForm)
                Me.doReturnFromModalShared(oForm, True)
                BubbleEvent = False
            End If
        End Sub

        Overloads Sub SetParentSharedData(ByVal iRow As Int32, ByVal oForm As SAPbouiCOM.Form)

            Me.SetParentSharedData(oForm, "CARDCODE", CType(oMatrix.Columns.Item("CardCode").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "CARDNAME", CType(oMatrix.Columns.Item("CardName").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "CARDFNAME", CType(oMatrix.Columns.Item("CardFName").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)

            Me.SetParentSharedData(oForm, "CARDTYPE", CType(oMatrix.Columns.Item("CardType").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "GROUPCODE", CType(oMatrix.Columns.Item("GroupCode").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "GROUPNAME", CType(oMatrix.Columns.Item("GroupName").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "CRREMAIN", CType(oMatrix.Columns.Item("RemainBal").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "ORDERS", CType(oMatrix.Columns.Item("OrdersBal").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "CREDITLINE", CType(oMatrix.Columns.Item("CreditLine").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "BALANCE", CType(oMatrix.Columns.Item("Balance").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "TBALANCE", CType(oMatrix.Columns.Item("TBalance").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "WRORDERS", CType(oMatrix.Columns.Item("WRBalance").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "BRANCH", CType(oMatrix.Columns.Item("U_IDHBRAN").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "BALANCEFC", CType(oMatrix.Columns.Item("Balance").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "DEVIATION", CType(oMatrix.Columns.Item("Deviation").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "FROZEN", CType(oMatrix.Columns.Item("frozenFor").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "FROZENC", CType(oMatrix.Columns.Item("FrozenComm").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "PHONE1", CType(oMatrix.Columns.Item("Phone1").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "CNTCTPRSN", CType(oMatrix.Columns.Item("CntctPrsn").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "WASLIC", CType(oMatrix.Columns.Item("U_WASLIC").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "IDHICL", CType(oMatrix.Columns.Item("U_IDHICL").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "IDHLBPC", CType(oMatrix.Columns.Item("U_IDHLBPC").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "IDHLBPN", CType(oMatrix.Columns.Item("U_IDHLBPN").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)

            Me.SetParentSharedData(oForm, "IDHOBLGT", CType(oMatrix.Columns.Item("U_IDHOBLGT").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "IDHUOM", CType(oMatrix.Columns.Item("U_IDHUOM").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "IDHONCS", CType(oMatrix.Columns.Item("U_IDHONCS").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            If CType(oMatrix.Columns.Item("frozenFrom").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim = "" Then
                Me.SetParentSharedData(oForm, "FROZENF", "1900-01-01")
            Else
                Dim dt As String = oForm.DataSources.DataTables.Item("LINESMTX").GetValue("frozenFrom", iRow - 1).ToString
                Me.SetParentSharedData(oForm, "FROZENF", CDate(dt).ToString("yyyy/MM/dd")) 'CDate(oMatrix.Columns.Item("frozenFrom").Cells.Item(iRow).Specific.value).ToString("yyyy/MM/dd"))
            End If
            If CType(oMatrix.Columns.Item("frozenTo").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim = "" Then
                Me.SetParentSharedData(oForm, "FROZENT", "1900-01-01")
            Else
                Dim dt As String = oForm.DataSources.DataTables.Item("LINESMTX").GetValue("frozenTo", iRow - 1).ToString

                Me.SetParentSharedData(oForm, "FROZENT", CDate(dt).ToString("yyyy/MM/dd")) 'CDate(oMatrix.Columns.Item("frozenTo").Cells.Item(iRow).Specific.value).ToString("yyyy/MM/dd"))
            End If
            Me.SetParentSharedData(oForm, "PLIST", CType(oMatrix.Columns.Item("ListNum").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "PAYTRM", CType(oMatrix.Columns.Item("GroupNum").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "ADDRESS", CType(oMatrix.Columns.Item("BillToDef").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "MAILADD", CType(oMatrix.Columns.Item("Address").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)

            Me.SetParentSharedData(oForm, "ZIPCODE", CType(oMatrix.Columns.Item("ZipCode").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)

        End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType = "134" Then
                    Dim sCardCode As String = getParentSharedDataAsString(oForm, "CARDCODE")
                    Dim oParentForm As SAPbouiCOM.Form = goParent.doGetParentForm(oForm.UniqueID)
                    If doGetBPInfo(goParent, oParentForm, sCardCode) = True Then
                        doReturnFromModalShared(oForm, True)
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal result - " & sModalFormType)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub

    End Class
End Namespace
