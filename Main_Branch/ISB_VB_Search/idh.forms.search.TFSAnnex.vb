Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Namespace idh.forms.search
    Public Class TFSAnnex
        Inherits IDHAddOns.idh.forms.Search

#Region "Initialization"
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHANXS", "TFS Annex Search.srf", 5, 40, 530, 360, "TFS Annex Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_TFSANX", Nothing, Nothing, False, True))

            Dim sRequired As String = String.Empty
            Dim sAnnexNo As String = getParentSharedData(oGridN.getSBOForm(), "IDH_ANXNO").ToString()

            Dim sTFSCd As String = getParentSharedData(oGridN.getSBOForm(), "IDH_TFSCD").ToString()
            Dim sTFSNo As String = getParentSharedData(oGridN.getSBOForm(), "IDH_TFSNO").ToString()

            If (Not sAnnexNo Is Nothing AndAlso sAnnexNo.Length > 0) Then
                sRequired = " U_AnexNo = '" & sAnnexNo & "' "
            End If

            If (Not sTFSNo Is Nothing AndAlso sTFSNo.Length > 0) Then
                sRequired = If(sRequired = String.Empty, "", sRequired + " AND ") + " U_TFSNo = '" & sTFSNo & "' AND U_TFSCd = '" + sTFSCd + "'"
            End If
            sRequired = String.Empty
            oGridN.setRequiredFilter(sRequired)

            oGridN.doAddFilterField("IDH_ANXCD", "Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_ANXNO", "U_AnexNo", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            oGridN.doAddFilterField("IDH_TFSCD", "U_TFSCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_TFSNO", "U_TFSNo", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            oGridN.doAddListField("Code", "Annex Code", False, 40, Nothing, "ANXCD")
            oGridN.doAddListField("U_AnexNo", "Annex No.", False, 40, Nothing, "ANXNO")
            oGridN.doAddListField("U_TFSCd", "TFS Code", False, 40, Nothing, "TFSCODE")
            oGridN.doAddListField("U_TFSNo", "TFS No.", False, 40, Nothing, "TFSNUM")
            oGridN.doAddListField("U_Detail", "Detail", False, 40, Nothing, "DETAIL")


            'oGridN.doAddListField("b.CardCode", "Code", False, 50, Nothing, "CARDCODE", -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            'oGridN.doAddListField("b.CardName", "Name", False, 150, Nothing, "CARDNAME")
            'oGridN.doAddListField("b.CardType", "Type", False, 40, Nothing, "CARDTYPE")
            'oGridN.doAddListField("b.GroupCode", "Group Code", False, 0, Nothing, "GROUPCODE")
            'oGridN.doAddListField("g.GroupName", "Group Name", False, 150, Nothing, "GROUPNAME")

            'Dim sIsCompliance As String = ""
            'Dim bListSet As Boolean = False
            'If getHasSharedData(oGridN.getSBOForm) = True Then
            '    sIsCompliance = getParentSharedData(oGridN.getSBOForm, "ISCOMPLIANCE")
            '    If sIsCompliance Is Nothing OrElse sIsCompliance.Equals("TRUE") Then
            '        oGridN.doAddListField("c.RemainBal", "Credit Remain", False, 0, Nothing, "CRREMAIN")
            '        oGridN.doAddListField("b.OrdersBal", "Orders", False, 0, Nothing, "ORDERS")
            '        oGridN.doAddListField("b.CreditLine", "Credit Limit", False, 0, Nothing, "CREDITLINE")
            '        oGridN.doAddListField("c.Balance", "Balance", False, 0, Nothing, "BALANCE")
            '        oGridN.doAddListField("c.TBalance", "Total Balance", False, 0, Nothing, "TBALANCE")
            '        oGridN.doAddListField("c.WRBalance", "WR1 Orders", False, 0, Nothing, "WRORDERS")
            '        oGridN.doAddListField("b.U_IDHBRAN", "Branch", False, 0, Nothing, "BRANCH")
            '        bListSet = True
            '    End If
            'End If

            'If bListSet = False Then
            '    oGridN.doAddListField("c.RemainBal", "Credit Remain", False, 30, Nothing, "CRREMAIN")
            '    oGridN.doAddListField("b.OrdersBal", "Orders", False, 30, Nothing, "ORDERS")
            '    oGridN.doAddListField("b.CreditLine", "Credit Limit", False, 30, Nothing, "CREDITLINE")
            '    oGridN.doAddListField("c.Balance", "Balance", False, 30, Nothing, "BALANCE")
            '    oGridN.doAddListField("c.TBalance", "Total Balance", False, -1, Nothing, "TBALANCE")
            '    oGridN.doAddListField("c.WRBalance", "WR1 Orders", False, 30, Nothing, "WRORDERS")
            '    oGridN.doAddListField("b.U_IDHBRAN", "Branch", False, 20, Nothing, "BRANCH")
            'End If

            'oGridN.doAddListField("b.Balance", "FC Balance", False, 0, Nothing, "BALANCEFC")
            'oGridN.doAddListField("c.Deviation", "Deviation", False, 0, Nothing, "DEVIATION")
            'oGridN.doAddListField("c.frozenFor", "Frozen", False, 0, Nothing, "FROZEN")
            'oGridN.doAddListField("c.FrozenComm", "FrozenComm", False, 0, Nothing, "FROZENC")
            'oGridN.doAddListField("b.Phone1", "Phone", False, 0, Nothing, "PHONE1")
            'oGridN.doAddListField("b.CntctPrsn", "Contact Person", False, 0, Nothing, "CNTCTPRSN")
            'oGridN.doAddListField("b.U_WASLIC", "Waste Lic. Reg. No.", False, 0, Nothing, "WASLIC")
            'oGridN.doAddListField("b.U_IDHICL", "Don't do Credit Check", False, 0, Nothing, "IDHICL")
            'oGridN.doAddListField("b.U_IDHLBPC", "Linked BP Code", False, 0, Nothing, "IDHLBPC")
            'oGridN.doAddListField("b.U_IDHLBPN", "Linked BP Name", False, 0, Nothing, "IDHLBPN")
            'oGridN.doAddListField("b.U_IDHOBLGT", "Obligated", False, 0, Nothing, "IDHOBLGT")
            'oGridN.doAddListField("b.U_IDHUOM", "BP UOM", False, 0, Nothing, "IDHUOM")
            'oGridN.doAddListField("b.U_IDHONCS", "Compliance Scheme", False, 0, Nothing, "IDHONCS")
            'oGridN.doAddListField("c.frozenFrom", "Frozen From", False, 0, Nothing, "FROZENF")
            'oGridN.doAddListField("c.frozenTo", "Frozen To", False, 0, Nothing, "FROZENT")
            'oGridN.doAddListField("b.ListNum", "Price List", False, 0, Nothing, "PLIST")
        End Sub

#End Region

#Region "Loading Data"
        'Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
        '    MyBase.doBeforeLoadData(oForm)

        '    '        	doFillBPTypes(oForm)
        '    '            doFillGroups(oForm)
        '    '            doFillBranches(oForm)
        'End Sub

        'Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
        '    'doFillGroups(oForm)
        '    'doFillBranches(oForm)

        '    MyBase.doReadInputParams(oForm)
        '    'Dim sVal As String = getParentSharedData(oForm, "IDH_TYPE")
        '    'If Not sVal Is Nothing Then
        '    '    If sVal.StartsWith("F-") Then
        '    '        sVal = sVal.Substring(2)
        '    '        doFillBPTypes(oForm)
        '    '        setEnableItem(oForm, False, "IDH_TYPE")
        '    '    Else
        '    '        doFillBPTypes(oForm, sVal)
        '    '        If sVal.Length > 1 Then
        '    '            sVal = sVal.Substring(0, 1)
        '    '        End If
        '    '        setEnableItem(oForm, True, "IDH_TYPE")
        '    '    End If
        '    '    setUFValue(oForm, "IDH_TYPE", sVal)
        '    'Else
        '    '    doFillBPTypes(oForm)
        '    'End If

        'End Sub

#End Region

#Region "Unloading Data"

#End Region

#Region "Event Handlers"
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_NEW" Then
                        'setSharedData(oForm, "ACT", "ADD")
                        'setSharedData(oForm, "TYPE", "C")

                        setSharedData(oForm, "TFSCODE", getUFValue(oForm, "IDH_TFSCD"))
                        setSharedData(oForm, "TFSNUM", getUFValue(oForm, "IDH_TFSNO"))

                        goParent.doOpenModalForm("IDH_TFSANX", oForm)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE Then
                If pVal.BeforeAction = False Then
                    'Refreshing the grid to get latest from WP Profiles table 
                    Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                    If oGridN Is Nothing Then
                        oGridN = New FilterGrid(Me, oForm, "LINESGRID", True)
                    End If
                    oGridN.doReloadData()
                End If
            End If
            Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            
        End Sub
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub
#End Region

#Region "Custom Functions"
        'Private Sub doFillBranches(ByVal oForm As SAPbouiCOM.Form)
        '    doFillCombo(oForm, "IDH_BRANCH", "OUBR", "Code", "Remarks", Nothing, "Code", True)
        'End Sub

        'Private Sub doFillGroups(ByVal oForm As SAPbouiCOM.Form)
        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '    Try
        '        Dim oGroups As SAPbouiCOM.ComboBox
        '        Dim oItem As SAPbouiCOM.Item
        '        Dim sBPType As String = oForm.DataSources.UserDataSources.Item("IDH_TYPE").ValueEx

        '        oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        '        Dim sQry As String = "select GroupCode, GroupName from OCRG where GroupType Like '%" & sBPType & "'"
        '        oRecordSet.DoQuery(sQry)
        '        If oRecordSet.RecordCount > 0 Then
        '            oItem = oForm.Items.Item("IDH_GROUP")
        '            oItem.DisplayDesc = True
        '            oGroups = oItem.Specific
        '            Dim iCount As Integer
        '            Dim oVal1 As String
        '            Dim oVal2 As String

        '            Dim oValValues As SAPbouiCOM.ValidValues
        '            oValValues = oGroups.ValidValues
        '            'First Clear all the values
        '            If Not oValValues Is Nothing Then
        '                While oValValues.Count > 0
        '                    oValValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
        '                End While
        '            End If

        '            oValValues.Add("", getTranslatedWord("Any"))
        '            For iCount = 0 To oRecordSet.RecordCount - 1
        '                oVal1 = oRecordSet.Fields.Item(0).Value
        '                oVal2 = oRecordSet.Fields.Item(1).Value
        '                oValValues.Add(oVal1, oVal2)
        '                oRecordSet.MoveNext()
        '            Next
        '        End If
        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Groups.")
        '    Finally
        '        IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
        '    End Try
        'End Sub

        ''* A Any
        ''* S Supplier
        ''* C Customer
        ''* L Lead
        'Private Sub doFillBPTypes(ByVal oForm As SAPbouiCOM.Form, Optional ByVal sSwitch As String = "ASCL")
        '    Dim oBPType As SAPbouiCOM.ComboBox
        '    Dim oItem As SAPbouiCOM.Item

        '    oItem = oForm.Items.Item("IDH_TYPE")
        '    oItem.DisplayDesc = True
        '    oBPType = oItem.Specific

        '    If oBPType.ValidValues.Count = 0 Then
        '        If sSwitch.Length = 0 Then
        '            oBPType.ValidValues.Add("", getTranslatedWord("Any"))
        '            oBPType.ValidValues.Add("S", getTranslatedWord("Vendor"))
        '            oBPType.ValidValues.Add("C", getTranslatedWord("Customer"))
        '            oBPType.ValidValues.Add("L", getTranslatedWord("Lead"))
        '        Else
        '            If sSwitch.IndexOf("A") > -1 Then
        '                oBPType.ValidValues.Add("", getTranslatedWord("Any"))
        '            End If
        '            If sSwitch.IndexOf("S") > -1 Then
        '                oBPType.ValidValues.Add("S", getTranslatedWord("Vendor"))
        '            End If
        '            If sSwitch.IndexOf("C") > -1 Then
        '                oBPType.ValidValues.Add("C", getTranslatedWord("Customer"))
        '            End If
        '            If sSwitch.IndexOf("L") > -1 Then
        '                oBPType.ValidValues.Add("L", getTranslatedWord("Lead"))
        '            End If
        '        End If
        '    End If
        'End Sub

        'Public Shared Function doGetBPInfo(ByVal oParent As IDHAddOns.idh.addon.Base, ByRef oCallerForm As SAPbouiCOM.Form, ByVal sCardCode As String) As Boolean
        '    Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
        '    Try
        '        oBP = oParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
        '        If oBP.GetByKey(sCardCode) Then
        '            setSharedData(oCallerForm, "CARDCODE", oBP.CardCode)
        '            setSharedData(oCallerForm, "CARDNAME", oBP.CardName)
        '            setSharedData(oCallerForm, "CARDTYPE", oBP.CardType)
        '            setSharedData(oCallerForm, "GROUPCODE", oBP.GroupCode)
        '            setSharedData(oCallerForm, "GROUPNAME", "")
        '            setSharedData(oCallerForm, "BALANCEFC", oBP.CurrentAccountBalance)
        '            setSharedData(oCallerForm, "CREDITLINE", oBP.CreditLimit)
        '            setSharedData(oCallerForm, "PHONE1", oBP.Phone1)
        '            setSharedData(oCallerForm, "CNTCTPRSN", oBP.ContactPerson)
        '            setSharedData(oCallerForm, "WASLIC", oBP.UserFields.Fields.Item("U_WASLIC").Value)
        '            Return True
        '        End If
        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error getting the BP Info - " & sCardCode)
        '    Finally
        '        IDHAddOns.idh.data.Base.doReleaseObject(oBP)
        '    End Try
        '    Return False
        'End Function

#End Region

    End Class
End Namespace
