﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class CustRef
        Inherits IDHAddOns.idh.forms.Search

        Private bClosingFormByOKButton As Boolean = False
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHCREF", "CustRefForm.srf", 5, 45, 603, 320, "Customer Reference Search")
        End Sub
        Protected Overrides Sub doLoadData(oForm As SAPbouiCOM.Form)
            bClosingFormByOKButton = False
            Me.setParentSharedData(oForm, "IDH_CUSTREFOPEND", "LOADED") 'setSharedData("IDH_BPSRCHOPEND", "LOADED")

            MyBase.doLoadData(oForm)
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)

            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_CUSTREF"))
            oGridN.setOrderValue("U_CustCd, U_SAddress")

            oGridN.doAddFilterField("VAL1", "U_CustCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 15, Nothing, True)
            oGridN.doAddFilterField("VAL2", "U_SAddress", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100, Nothing, True)

            oGridN.doAddListField("U_CustCd", "Customer Code", False, -1, Nothing, "CUSTCD")
            oGridN.doAddListField("U_SAddress", "Site Address", False, -1, Nothing, "SITE")
            oGridN.doAddListField("U_CustRef", "Customer Reference", False, -1, Nothing, "CUSTREF")
            oGridN.doAddListField("U_FrmDate", "Valid From", False, -1, Nothing, "FROMDATE")
            oGridN.doAddListField("U_ToDate", "Valid To", False, -1, Nothing, "TODATE")
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub
        Public Overrides Function doItemEvent(oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.setParentSharedData(oForm, "IDH_CUSTREFOPEND", "")
                    If bClosingFormByOKButton = False Then
                        MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                    End If
                End If
                Return True
            End If
            If pVal.BeforeAction = False Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_FORM_CLOSE
                        Me.setParentSharedData(oForm, "IDH_CUSTREFOPEND", "")
                        If bClosingFormByOKButton = False Then
                            MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                        End If
                End Select
            End If
            Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
        End Function
    End Class
End Namespace
