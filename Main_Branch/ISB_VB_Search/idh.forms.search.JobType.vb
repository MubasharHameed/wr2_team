Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class JobType
        Inherits IDHAddOns.idh.forms.Search

        Private bClosingFormByOKButton As Boolean = False

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHJTSRC", "Job Type Search.srf", 5, 40, 443, 350, "Job Type Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("[@IDH_JOBTYPE] jt, OITB ig")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_JOBTYPE", "jt", Nothing, False, True))
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OITB", "ig", Nothing, False, True))
            
'...            oGridN.setRequiredFilter("jt.U_ItemGrp = ig.ItmsGrpNam AND U_OrdCat = 'Waste Order' ")
            oGridN.setRequiredFilter("jt.U_ItemGrp = ig.ItmsGrpCod AND U_OrdCat = 'Waste Order' ")
            oGridN.setOrderValue("jt.U_ItemGrp, jt.U_JobTp ")

            oGridN.doAddFilterField("IDH_CURJOB", "jt.U_JobTp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_ITMGRP", "ig.ItmsGrpCod", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            oGridN.doAddListField("jt.U_JobTp", "Order Type", False, -1, Nothing, "ORDTYPE")
            oGridN.doAddListField("jt.U_ItemGrp", "Item Group Code", False, -1, Nothing, "ITMSGRPCOD")
            oGridN.doAddListField("ig.ItmsGrpNam", "Item Group Name", False, -1, Nothing, "ITMSGRPNAM")
            oGridN.doAddListField("jt.U_Seq", "Sequence", False, -1, Nothing, "SEQ")
            oGridN.doAddListField("jt.U_Billa", "Tipping Order-able", False, 0, Nothing, "ORDERABLE")
            oGridN.doAddListField("jt.U_HBilla", "Haulage Order-able", False, 0, Nothing, "HORDERABLE")
        End Sub

        '## Start 04-07-2013
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            bClosingFormByOKButton = False
            Me.setParentSharedData(oForm, "IDH_JBSRCHOPEND", "LOADED")
            MyBase.doLoadData(oForm)
        End Sub

        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD OrElse _
                    (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.setParentSharedData(oForm, "IDH_JBSRCHOPEND", "")
                    If bClosingFormByOKButton = False Then
                        MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                    End If
                End If
            End If
            Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
        End Function

        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doFinalizeShow(oForm)
            If oForm.Visible = False Then
                Me.setParentSharedData(oForm, "IDH_JBSRCHOPEND", "")
            End If
        End Sub
        '## End
    End Class
End Namespace