Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.bridge
Imports com.idh.bridge.lookups

Namespace idh.forms.search
    Public Class AddressSearch3
        Inherits IDHAddOns.idh.forms.Search

        Private oMatrix As SAPbouiCOM.Matrix
        Private FormLoaded As Boolean = False
        Private _sSQL As String = ""
        Private iLastselectedRow As Int32 = 0
        Private slastSearchedResult As String = ""
        Private bClosingFormByOKButton As Boolean = False
        Private WithEvents timer_bp As System.Timers.Timer

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHASRCH3", "Address Search3.srf", 5, 45, 790, 350, "Address Search - L2.")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
        End Sub
      
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            bClosingFormByOKButton = False
            Me.SetParentSharedData(oForm, "IDH_ADRSRCHOPEND", "LOADED") 'setSharedData("IDH_BPSRCHOPEND", "LOADED")
            _sSQL = ""
            iLastselectedRow = 0
            slastSearchedResult = ""

            setUFValue(oForm, "IDH_CUSCOD", getParentSharedData(oForm, "IDH_CUSCOD"))
            'setUFValue(oForm, "IDH_CUSNAM", getParentSharedData(oForm, "IDH_CUSNAM"))
            setUFValue(oForm, "IDH_ADRTYP", getParentSharedData(oForm, "IDH_ADRTYP"))

            'IDH_CUSNAM
            setUFValue(oForm, "IDH_CUSNAM", "")

            'IDH_ADDRES
            setUFValue(oForm, "IDH_ADDRES", getParentSharedData(oForm, "IDH_ADDRES"))
            'IDH_STREET
            setUFValue(oForm, "IDH_STREET", "")
            'IDH_CITY
            setUFValue(oForm, "IDH_CITY", "")
            'IDH_ZIPCD
            setUFValue(oForm, "IDH_ZIPCD", "")
            'IDH_STATE
            setUFValue(oForm, "IDH_STATE", "")
            'IDH_TEL1
            setUFValue(oForm, "IDH_TEL1", "")
            reQuerynReloadMatrix(oForm)
        End Sub

        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            Try
                oForm.Items.Item("LINESGRID").Visible = False
                Dim bFlag As Boolean = False
                Dim sSILENT As String = ""
                If (Me.getHasSharedData(oForm)) Then
                    sSILENT = Me.getParentSharedDataAsString(oForm, "SILENT") '.ToString
                    If (sSILENT <> Nothing) Then
                        If (sSILENT.ToUpper().Equals("SHOWMULTI")) Then
                            bFlag = True 'Means we have some value in CardCode/cardname
                        End If
                    End If
                End If
                If (bflag AndAlso oMatrix.RowCount > 0) Then
                    oMatrix.SelectRow(1, True, False)
                    If (oMatrix.RowCount = 1) Then
                        Me.SetParentSharedData(1, oForm)
                        Me.doReturnFromModalShared(oForm, True)
                        Me.SetParentSharedData(oForm, "IDH_ADRSRCHOPEND", "")
                        Return
                    End If
                End If
                oForm.Visible = True
                'test.EndTime = Now
                'oForm.Title = test.StartTime.Subtract(test.EndTime).TotalMilliseconds.ToString
                If getParentSharedData(oForm, "IDH_CUSCOD") IsNot Nothing AndAlso getParentSharedData(oForm, "IDH_CUSCOD").ToString.Trim <> "" Then
                    Dim otxt As SAPbouiCOM.EditText = CType(oForm.Items.Item("IDH_ADDRES").Specific, SAPbouiCOM.EditText)
                    otxt.Active = True
                    setEnableItem(oForm, False, "IDH_CUSCOD")
                Else
                    setEnableItem(oForm, True, "IDH_CUSCOD")
                    Dim otxt As SAPbouiCOM.EditText = CType(oForm.Items.Item("IDH_CUSCOD").Specific, SAPbouiCOM.EditText)
                    otxt.Active = True
                End If

                FormLoaded = True

                timer_bp = New System.Timers.Timer(700)
                timer_bp.Enabled = False
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Finalizing Show Form.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXELF", {Nothing})
            End Try

        End Sub

        Private Function PrepareSQL(ByVal oform As SAPbouiCOM.Form, Optional ByVal sOrderbyCol As String = "") As String

            iLastselectedRow = 0
            slastSearchedResult = ""
            Dim sql As String = " select ba.AdresType,ba.CardCode,bp.CardName,ba.Address,ba.Street, ba.Block, ba.ZipCode, ba.City, ba.County," _
                        & " ba.State, ba.Country, ba.LicTradNum, ba.StreetNo, ba.Building, ba.U_ROUTE, " _
                        & " ba.U_ISBACCDF, ba.U_ISBACCTF, ba.U_ISBACCTT, ba.U_ISBACCRA, " _
                        & " ba.U_ISBACCRV, ba.U_ISBCUSTRN, ba.U_TEL1, " _
                        & " ba.U_IDHSEQ,ba.U_IDHACN, ba.U_IDHPCD,ba.U_Origin,ba.U_IDHSLCNO,ba.U_IDHSteId, " _
                        & " ba.U_IDHFAX, ba.U_IDHEMAIL, ba.U_IDHSTRNO, ba.U_PREEXPDTTO, ba.U_EPASITEID, bp.Phone1,bp.CntctPrsn, " _
                        & " (SELECT st.NAME FROM OCST st WHERE st.CODE = ba.STATE AND st.COUNTRY = ba.COUNTRY) As StateNM,ba.U_MinJob,ba.U_AddrsCd " _
                        & " from OCRD bp ,CRD1 ba " _
                        & " Where ba.Cardcode=bp.CardCode "
            ''##MA Start 18-09-2014 Issue#403
            If getParentSharedData(oform, "IDH_APPLYFLTR") IsNot Nothing AndAlso getParentSharedData(oform, "IDH_APPLYFLTR").ToString = "True" Then
                sql &= " And Not ba.Address in (Select adrs.U_Address from [@IDH_ADDRST] adrs Where (adrs.U_CustCode=ba.CardCode or " _
                    & " adrs.U_CustCode  in (Select OCRD.CardCode from OCRD where OCRD.U_IDHLBPC=ba.CardCode)) " _
                    & " And adrs.U_AddType=ba.AdresType And adrs.U_UserName='" & PARENT.gsUserName & "')"

            End If
            ''##MA End 18-09-2014 Issue#403
            PrepareSQL = sql
            'Exit Function
            '  Dim oCombo As SAPbouiCOM.ComboBox
            'Dim otextBox As SAPbouiCOM.EditText

            ' otextBox = oform.Items.Item("IDH_CUSCOD").Specific
            Dim SCustCode As String = getItemValue(oform, "IDH_CUSCOD") 'otextBox.Value

            ' otextBox = oform.Items.Item("IDH_CUSNAM").Specific
            Dim sCustName As String = getItemValue(oform, "IDH_CUSNAM") ' oCombo.Selected.Value

            ' otextBox = oform.Items.Item("IDH_ADDRES").Specific
            Dim sAddress As String = getItemValue(oform, "IDH_ADDRES") 'oCombo.Selected.Value

            ' otextBox = oform.Items.Item("IDH_STREET").Specific
            Dim sStreet As String = getItemValue(oform, "IDH_STREET") 'otextBox.Value.Trim

            ' otextBox = oform.Items.Item("IDH_ADRTYP").Specific
            Dim sAddressType As String = getItemValue(oform, "IDH_ADRTYP") 'otextBox.Value.Trim

            ' otextBox = oform.Items.Item("IDH_CITY").Specific
            Dim sCity As String = getItemValue(oform, "IDH_CITY") 'otextBox.Value.Trim

            ' otextBox = oform.Items.Item("IDH_ZIPCD").Specific
            Dim sZipCode As String = getItemValue(oform, "IDH_ZIPCD") 'otextBox.Value.Trim

            ' otextBox = oform.Items.Item("IDH_STATE").Specific
            Dim sState As String = getItemValue(oform, "IDH_STATE") 'otextBox.Value.Trim

            ' otextBox = oform.Items.Item("IDH_TEL1").Specific
            Dim sSiteTel As String = getItemValue(oform, "IDH_TEL1") 'otextBox.Value.Trim

            Dim sWhereSql As String = ""
            If SCustCode <> "" Then
                sWhereSql &= " and ba.CardCode LIKE '" & SCustCode.Replace("*", "%") & "%' "
            End If

            If sCustName <> "" Then
                sWhereSql &= " and bp.CardName Like '" & sCustName.Replace("*", "%") & "%' "
            End If

            If sAddress <> "" Then
                sWhereSql &= " and ba.Address Like '" & sAddress.Replace("*", "%") & "%' "
            End If

            If sStreet <> "" Then
                sWhereSql &= " and ba.Street LIKE '" & sStreet.Replace("*", "%") & "%' "
            End If

            If sAddressType <> "" Then
                sWhereSql &= " and ba.AdresType LIKE '" & sAddressType.Replace("*", "%") & "%' "
            End If

            If sCity <> "" Then
                sWhereSql &= " and ba.City LIKE '" & sCity.Replace("*", "%") & "%' "
            End If

            If sZipCode <> "" Then
                sWhereSql &= " and ba.ZipCode LIKE '" & sZipCode.Replace("*", "%") & "%' "
            End If

            If sState <> "" Then
                sWhereSql &= " and ba.State LIKE '" & sState.Replace("*", "%") & "%' "
            End If


            If sSiteTel <> "" Then
                sWhereSql &= " and ba.U_TEL1 LIKE '" & sSiteTel.Replace("*", "%") & "%' "
            End If
            If sWhereSql.Trim <> "" Then
                sql &= sWhereSql
            End If
            If sOrderbyCol <> "" Then
                sql &= " Order by " & sOrderbyCol
            End If
            Return sql
        End Function

        Private Sub reQuerynReloadMatrix(ByVal oform As SAPbouiCOM.Form, Optional ByVal sOrderByCol As String = "")
            Try
                iLastselectedRow = 0
                slastSearchedResult = ""
                Dim sSQL As String = PrepareSQL(oform, sOrderByCol).Trim
                If (sSQL.Trim.ToLower.Equals(_sSQL.ToLower)) AndAlso _sSQL <> "" Then
                    Return
                End If
                _sSQL = sSQL
                oMatrix = CType(oform.Items.Item("LINESMTX").Specific, SAPbouiCOM.Matrix)
                oform.DataSources.DataTables.Item("LINESMTX").ExecuteQuery(sSQL)
                ' oform.Freeze(True)
                oMatrix.Clear()
                oMatrix.LoadFromDataSourceEx(False)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error in Quering db.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", {_sSQL})
            Finally
                '  oform.Freeze(False)
            End Try

        End Sub

        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_GOT_FOCUS)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
        End Sub
        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)
        End Sub
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            ' MyBase.doReadInputParams(oForm)
            'Dim sVal As String = getParentSharedData(oForm, "IDH_TYPE")
            'If Not sVal Is Nothing Then
            '    If sVal.StartsWith("F-") Then
            '        sVal = sVal.Substring(2)
            '        doFillBPTypes(oForm)

            '        'OnTime [#Ico000????] USA 
            '        'START
            '        'Enabling Customer Type Dropdown to allow selection of 'Lead' customer 
            '        If Config.INSTANCE.getParameterAsBool("USAREL", False) Then
            '            setEnableItem(oForm, True, "IDH_TYPE")
            '        Else
            '            setEnableItem(oForm, False, "IDH_TYPE")
            '        End If
            '        'END 
            '        'OnTime [#Ico000????] USA 
            '    Else
            '        doFillBPTypes(oForm, sVal)
            '        If sVal.Length > 1 Then
            '            sVal = sVal.Substring(0, 1)
            '        End If
            '        setEnableItem(oForm, True, "IDH_TYPE")
            '    End If
            '    'setUFValue(oForm, "IDH_TYPE", sVal)
            'Else
            '    doFillBPTypes(oForm)
            'End If
            setParentSharedData(oForm, "CALLEDITEM", getParentSharedData(oForm, "CALLEDITEM"))
        End Sub
        '        select 
        'ba.AdresType,ba.CardCode,bp.CardName,ba.Address,ba.Street, ba.Block, ba.ZipCode, ba.City, ba.County, 
        'ba.State, ba.Country, ba.LicTradNum, ba.StreetNo, ba.Building, ba.U_ROUTE,
        'ba.U_ISBACCDF, ba.U_ISBACCTF, ba.U_ISBACCTT, ba.U_ISBACCRA,
        'ba.U_ISBACCRV, ba.U_ISBCUSTRN, ba.U_TEL1, ba.U_ROUTE,
        'ba.U_IDHSEQ,ba.U_IDHACN, ba.U_IDHPCD,ba.U_Origin,ba.U_IDHSLCNO,ba.U_IDHSteId,
        'ba.U_IDHFAX,ba.U_IDHEMAIL,ba.U_IDHSTRNO,ba.U_PREEXPDTTO,ba.U_EPASITEID
        'from OCRD bp ,CRD1 ba
        'Where ba.Cardcode=bp.CardCode

        Private Function GetselectedRowIndex() As Int32
            Dim i As Int32 = 0
            i = oMatrix.GetNextSelectedRow(0, SAPbouiCOM.BoOrderType.ot_SelectionOrder)
            Return i
        End Function

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.SetParentSharedData(oForm, "IDH_ADRSRCHOPEND", "")
                    If bClosingFormByOKButton = False Then
                        MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                    End If
                End If
                Return True
            End If
            If pVal.BeforeAction = False Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_FORM_CLOSE
                        Me.SetParentSharedData(oForm, "IDH_ADRSRCHOPEND", "")
                        If bClosingFormByOKButton = False Then
                            MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS 'SAPbouiCOM.BoEventTypes.et_VALIDATE
                        Dim sOrdrebyCol As String = ""
                        If (pVal.ItemUID = "IDH_CUSCOD" OrElse pVal.ItemUID = "IDH_CUSNAM" _
                        OrElse pVal.ItemUID = "IDH_ADDRES" OrElse pVal.ItemUID = "IDH_STREET" _
                         OrElse pVal.ItemUID = "IDH_ADRTYP" OrElse pVal.ItemUID = "IDH_CITY" _
                         OrElse pVal.ItemUID = "IDH_ZIPCD" OrElse pVal.ItemUID = "IDH_STATE" _
                          OrElse pVal.ItemUID = "IDH_TEL1") Then
                            Select Case pVal.ItemUID
                                Case "IDH_CUSCOD"
                                    sOrdrebyCol = "ba.CardCode"
                                Case "IDH_CUSNAM"
                                    sOrdrebyCol = "bp.CardName"
                                Case "IDH_ADDRES"
                                    sOrdrebyCol = "ba.Address"
                                Case "IDH_STREET"
                                    sOrdrebyCol = "ba.Street"
                                Case "IDH_ADRTYP"
                                    sOrdrebyCol = "ba.AdresType,ba.CardCode"
                                Case "IDH_CITY"
                                    sOrdrebyCol = "ba.City"
                                Case "IDH_ZIPCD"
                                    sOrdrebyCol = "ba.ZipCode"
                                Case "IDH_STATE"
                                    sOrdrebyCol = "ba.State"
                                Case "IDH_TEL1"
                                    sOrdrebyCol = "ba.U_TEL1"
                            End Select
                            reQuerynReloadMatrix(oForm, sOrdrebyCol)
                            BubbleEvent = False
                        End If
                        'If pVal.ItemUID = "IDH_CUSCOD" OrElse pVal.ItemUID = "IDH_CUSNAM" _
                        'OrElse pVal.ItemUID = "IDH_ADDRES" OrElse pVal.ItemUID = "IDH_STREET" _
                        ' OrElse pVal.ItemUID = "IDH_ADRTYP" OrElse pVal.ItemUID = "IDH_CITY" OrElse pVal.ItemUID = "IDH_ZIPCD" OrElse pVal.ItemUID = "IDH_STATE" _
                        '  OrElse pVal.ItemUID = "IDH_TEL1" Then
                        '    If pVal.InnerEvent = False And pVal.ItemChanged Then
                        '        reQuerynReloadMatrix(oForm)
                        '        BubbleEvent = False
                        '    End If
                        'End If
                        ''Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        ''    If pVal.BeforeAction = False Then
                        ''        If pVal.ItemUID = "IDH_NEW" Then
                        ''            setSharedData(oForm, "ACT", "ADD")
                        ''            setSharedData(oForm, "TYPE", "C")
                        ''            goParent.doOpenModalForm("134", oForm)
                        ''        End If
                        ''    End If
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "LINESMTX" Then
                            If pVal.Row > 0 Then
                                oMatrix.SelectRow(pVal.Row, True, False)
                            End If
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                        'If pVal.BeforeAction = False Then
                        If pVal.ItemUID = "LINESMTX" Then
                            Me.setChoosenData(oForm, BubbleEvent)
                        End If
                        'End If
                    Case SAPbouiCOM.BoEventTypes.et_KEY_DOWN
                        If pVal.CharPressed <> 9 AndAlso pVal.CharPressed <> 38 AndAlso pVal.CharPressed <> 39 AndAlso pVal.CharPressed <> 40 AndAlso pVal.CharPressed <> 13 _
                             AndAlso (pVal.ItemUID = "IDH_CUSCOD" OrElse pVal.ItemUID = "IDH_ADRTYP" OrElse pVal.ItemUID = "IDH_CUSNAM" _
                                      OrElse pVal.ItemUID = "IDH_ADDRES" OrElse pVal.ItemUID = "IDH_STREET" OrElse pVal.ItemUID = "IDH_CITY" _
                                       OrElse pVal.ItemUID = "IDH_ZIPCD" OrElse pVal.ItemUID = "IDH_STATE" OrElse pVal.ItemUID = "IDH_TEL1") Then
                            Select Case pVal.ItemUID
                                Case "IDH_CUSCOD"
                                    timer_bp.Enabled = False
                                    timer_bp.Interval = 400
                                    timer_Form = oForm
                                    timer_ColUID = "CardCode"
                                    timer_ItemUID = pVal.ItemUID
                                    If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
                                        iLastselectedRow = 0
                                        slastSearchedResult = ""
                                    End If
                                    timer_bp.Enabled = True
                                    'doSelectOnKeyPress("CardCode", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)
                                Case "IDH_CUSNAM"
                                    timer_bp.Enabled = False
                                    timer_bp.Interval = 400
                                    timer_Form = oForm
                                    timer_ColUID = "CardName"
                                    timer_ItemUID = pVal.ItemUID
                                    If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
                                        iLastselectedRow = 0
                                        slastSearchedResult = ""
                                    End If
                                    timer_bp.Enabled = True
                                    'doSelectOnKeyPress("CardName", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)
                                Case "IDH_ADRTYP"
                                    timer_bp.Enabled = False
                                    timer_bp.Interval = 100
                                    timer_Form = oForm
                                    timer_ColUID = "AdresType"
                                    timer_ItemUID = pVal.ItemUID
                                    If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
                                        iLastselectedRow = 0
                                        slastSearchedResult = ""
                                    End If
                                    timer_bp.Enabled = True
                                    'doSelectOnKeyPress("AdresType", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)
                                Case "IDH_ADDRES"
                                    timer_bp.Enabled = False
                                    timer_bp.Interval = 400
                                    timer_Form = oForm
                                    timer_ColUID = "Address"
                                    timer_ItemUID = pVal.ItemUID
                                    If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
                                        iLastselectedRow = 0
                                        slastSearchedResult = ""
                                    End If
                                    timer_bp.Enabled = True
                                    'doSelectOnKeyPress("Address", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)
                                Case "IDH_STREET"
                                    timer_bp.Enabled = False
                                    timer_bp.Interval = 400
                                    timer_Form = oForm
                                    timer_ColUID = "Street"
                                    timer_ItemUID = pVal.ItemUID
                                    If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
                                        iLastselectedRow = 0
                                        slastSearchedResult = ""
                                    End If
                                    timer_bp.Enabled = True
                                    'doSelectOnKeyPress("Street", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)
                                Case "IDH_CITY"
                                    timer_bp.Enabled = False
                                    timer_bp.Interval = 400
                                    timer_Form = oForm
                                    timer_ColUID = "City"
                                    timer_ItemUID = pVal.ItemUID
                                    If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
                                        iLastselectedRow = 0
                                        slastSearchedResult = ""
                                    End If
                                    timer_bp.Enabled = True
                                    'doSelectOnKeyPress("City", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)
                                Case "IDH_ZIPCD"
                                    timer_bp.Enabled = False
                                    timer_bp.Interval = 400
                                    timer_Form = oForm
                                    timer_ColUID = "ZipCode"
                                    timer_ItemUID = pVal.ItemUID
                                    If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
                                        iLastselectedRow = 0
                                        slastSearchedResult = ""
                                    End If
                                    timer_bp.Enabled = True
                                    'doSelectOnKeyPress("ZipCode", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)
                                Case "IDH_STATE"
                                    timer_bp.Enabled = False
                                    timer_bp.Interval = 400
                                    timer_Form = oForm
                                    timer_ColUID = "State"
                                    timer_ItemUID = pVal.ItemUID
                                    If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
                                        iLastselectedRow = 0
                                        slastSearchedResult = ""
                                    End If
                                    timer_bp.Enabled = True
                                    'doSelectOnKeyPress("State", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)
                                Case "IDH_TEL1"
                                    timer_bp.Enabled = False
                                    timer_bp.Interval = 400
                                    timer_Form = oForm
                                    timer_ColUID = "U_TEL1"
                                    timer_ItemUID = pVal.ItemUID
                                    If getItemValue(Me.timer_Form, timer_ItemUID).Trim = "" Then
                                        iLastselectedRow = 0
                                        slastSearchedResult = ""
                                    End If
                                    timer_bp.Enabled = True
                                    'doSelectOnKeyPress("U_TEL1", getItemValue(oForm, pVal.ItemUID).Trim.ToLower)

                            End Select
                            BubbleEvent = False
                        End If

                        If Not (pVal.ItemUID = "IDH_TYPE" OrElse pVal.ItemUID = "IDH_GROUP" OrElse pVal.ItemUID = "IDH_BRANCH") Then
                            If (pVal.CharPressed = 40 OrElse pVal.CharPressed = 38 OrElse pVal.CharPressed = 34 OrElse pVal.CharPressed = 33) AndAlso (oMatrix.RowCount > 0) Then
                                handleKeyEventForMatrixRowSelection(pVal.CharPressed)
                                BubbleEvent = False
                            End If
                        End If
                End Select
            End If
            Return True
        End Function
#Region "Timer Delay"
        Private timer_Form As SAPbouiCOM.Form
        Private timer_ColUID, timer_ItemUID As String
        'Private Shared Sub OnTimedEvent(ByVal source As Object, ByVal e As Timers.ElapsedEventArgs)
        '    ' Console.WriteLine("The Elapsed event was raised at {0}", e.SignalTime)
        '    Dim timer As System.Timers.Timer = source

        '    doSelectOnKeyPress("CardCode", getItemValue(Me.MyForm_bpTimer, "IDH_BPCOD").Trim.ToLower)
        'End Sub

        Private Sub timer_bp_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles timer_bp.Elapsed
            timer_bp.Enabled = False
            doSelectOnKeyPressNew(timer_ColUID, getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
            Select Case (timer_ColUID)
                Case "CardCode"
                    doSelectOnKeyPressNew("CardCode", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
                Case "CardName"
                    doSelectOnKeyPressNew("CardName", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
                Case "Address"
                    doSelectOnKeyPressNew("Address", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
                Case "AdresType"
                    doSelectOnKeyPressNew("AdresType", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
                Case "Street"
                    doSelectOnKeyPressNew("Street", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
                Case "City"
                    doSelectOnKeyPressNew("City", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
                Case "ZipCode"
                    doSelectOnKeyPressNew("ZipCode", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
                Case "State"
                    doSelectOnKeyPressNew("State", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
                Case "U_TEL1"
                    doSelectOnKeyPressNew("U_TEL1", getItemValue(Me.timer_Form, timer_ItemUID).Trim.ToLower)
            End Select

        End Sub
#End Region
        Private Sub doSelectOnKeyPress(ByVal sColUID As String, ByVal sValueToSearch As String)
            Dim iRowCount As Int32 = oMatrix.RowCount
            If sValueToSearch.Length = 0 Then
                iLastselectedRow = 0
                slastSearchedResult = ""
                Return
            ElseIf sValueToSearch.Length = 1 Then
                If slastSearchedResult <> "" AndAlso sValueToSearch.StartsWith(slastSearchedResult) Then
                    Return
                End If
                iLastselectedRow = 0
                slastSearchedResult = ""
                For i As Int32 = 1 To iRowCount
                    If ((CType(oMatrix.Columns.Item(sColUID).Cells.Item(i).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim().StartsWith(sValueToSearch)) Then
                        oMatrix.SelectRow(i, True, False)
                        iLastselectedRow = i
                        slastSearchedResult = sValueToSearch
                        oMatrix.SelectRow(i, True, False)
                        Exit For
                    End If
                Next
            ElseIf sValueToSearch.Length > 1 Then
                If (iLastselectedRow = 0) Then
                    Return
                End If
                iLastselectedRow = 0
                slastSearchedResult = ""
                If (oMatrix.GetNextSelectedRow(0, SAPbouiCOM.BoOrderType.ot_SelectionOrder) <> 0) Then
                    For i As Int32 = (oMatrix.GetNextSelectedRow(0, SAPbouiCOM.BoOrderType.ot_SelectionOrder)) To iRowCount
                        If ((CType(oMatrix.Columns.Item(sColUID).Cells.Item(i).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim().StartsWith(sValueToSearch)) Then
                            oMatrix.SelectRow(i, True, False)
                            iLastselectedRow = i
                            slastSearchedResult = sValueToSearch
                            oMatrix.SelectRow(i, True, False)
                            Exit For
                        End If
                    Next
                End If
            End If

        End Sub
        Private Sub doSelectOnKeyPressNew(ByVal sColUID As String, ByVal sValueToSearch As String)
            Dim iRowCount As Int32 = oMatrix.RowCount
            If sValueToSearch.Length = 0 Then
                iLastselectedRow = 0
                slastSearchedResult = ""
                Return
            ElseIf sValueToSearch.Length = 1 Then
                If slastSearchedResult <> "" AndAlso sValueToSearch.StartsWith(slastSearchedResult) Then
                    Return
                End If
                iLastselectedRow = 0
                slastSearchedResult = ""
                Dim iIndex As Int32 = SearchValue(sColUID, sValueToSearch)
                If oMatrix.RowCount >= iIndex Then
                    oMatrix.SelectRow(iIndex, True, False)
                    iLastselectedRow = iIndex
                    slastSearchedResult = sValueToSearch
                    oMatrix.SelectRow(iIndex, True, False)
                End If
            ElseIf sValueToSearch.Length > 1 Then
                iLastselectedRow = 0
                slastSearchedResult = ""
                Dim iIndex As Int32 = SearchValue(sColUID, sValueToSearch)
                If oMatrix.RowCount >= iIndex Then
                    oMatrix.SelectRow(iIndex, True, False)
                    iLastselectedRow = iIndex
                    slastSearchedResult = sValueToSearch
                    oMatrix.SelectRow(iIndex, True, False)
                End If
            End If

        End Sub
        Private Function midpoint(ByVal iMin As Int32, ByVal iMax As Int32) As Int32
            Return CType(Math.Floor((iMin + iMax) / 2.0), Integer)
        End Function

        Private Function SearchValue(ByVal sColUID As String, ByVal sValueToSearch As String) As Int32
            Try
                Dim iMin As Int32 = 1, iMax As Int32 = oMatrix.RowCount
                While (iMax >= iMin)
                    Dim imid As Int32 = midpoint(iMin, iMax)
                    If ((CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim().StartsWith(sValueToSearch)) Then
                        Return imid
                    ElseIf (CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim() < (sValueToSearch) OrElse _
                            String.Compare((CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim(), (sValueToSearch)) < 0 Then
                        iMin = imid + 1
                    ElseIf (CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim() > (sValueToSearch) OrElse _
                            String.Compare((CType(oMatrix.Columns.Item(sColUID).Cells.Item(imid).Specific, SAPbouiCOM.EditText)).Value.ToLower().Trim(), (sValueToSearch)) > 0 Then
                        iMax = imid - 1
                    End If
                End While
                Return 1
            Catch ex As Exception
                Return 1
            End Try
        End Function

        Private Sub handleKeyEventForMatrixRowSelection(ByVal Keycode As Int32)
            If Keycode = 40 Then
                Dim row As Int32 = GetselectedRowIndex()
                If row < 1 Then
                    oMatrix.SelectRow(1, True, False)
                ElseIf row < oMatrix.RowCount Then
                    oMatrix.SelectRow(row + 1, True, False)
                End If
            ElseIf Keycode = 38 Then
                Dim row As Int32 = GetselectedRowIndex()
                If row <= 1 Then
                    oMatrix.SelectRow(1, True, False)
                ElseIf row <= oMatrix.RowCount Then
                    oMatrix.SelectRow(row - 1, True, False)
                End If
            ElseIf Keycode = 34 Then 'Pg Dn
                Dim row As Int32 = GetselectedRowIndex()
                If (row + 10) <= oMatrix.RowCount Then
                    oMatrix.SelectRow(row + 10, True, False)
                ElseIf row < 1 Then
                    oMatrix.SelectRow(1, True, False)
                Else
                    oMatrix.SelectRow(oMatrix.RowCount, True, False)
                End If
            ElseIf Keycode = 33 Then 'Pg Up
                Dim row As Int32 = GetselectedRowIndex()
                If (row - 10) > 0 Then
                    oMatrix.SelectRow(row - 10, True, False)
                ElseIf row <= 1 Then
                    oMatrix.SelectRow(1, True, False)
                Else
                    oMatrix.SelectRow(1, True, False)
                End If
            End If
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.ItemUID = "1" AndAlso pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                Me.setChoosenData(oForm, BubbleEvent)
            End If
        End Sub

        Private Sub setChoosenData(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            If oMatrix.RowCount = 0 Then Exit Sub
            Dim i As Int32 = 0
            i = GetselectedRowIndex()
            If i > 0 AndAlso i <= oMatrix.RowCount Then
                Me.SetParentSharedData(i, oForm)
                Me.doReturnFromModalShared(oForm, True)
                BubbleEvent = False
            End If
        End Sub

        Private Overloads Sub SetParentSharedData(ByVal iRow As Int32, ByVal oForm As SAPbouiCOM.Form)
            bClosingFormByOKButton = True
            Me.SetParentSharedData(oForm, "ADRESTYPE", CType(oMatrix.Columns.Item("AdresType").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "CARDCODE", CType(oMatrix.Columns.Item("CardCode").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)

            Me.SetParentSharedData(oForm, "CARDNAME", CType(oMatrix.Columns.Item("CardName").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "ADDRESS", CType(oMatrix.Columns.Item("Address").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.setParentSharedData(oForm, "ADDRSCD", CType(oMatrix.Columns.Item("U_AddrsCd").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "STREET", CType(oMatrix.Columns.Item("Street").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "BLOCK", CType(oMatrix.Columns.Item("Block").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "ZIPCODE", CType(oMatrix.Columns.Item("ZipCode").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "CITY", CType(oMatrix.Columns.Item("City").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "COUNTY", CType(oMatrix.Columns.Item("County").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "STATE", CType(oMatrix.Columns.Item("State").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "COUNTRY", CType(oMatrix.Columns.Item("Country").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "LICTRADNUM", CType(oMatrix.Columns.Item("LicTradNum").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "STREETNO", CType(oMatrix.Columns.Item("StreetNo").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "BUILDING", CType(oMatrix.Columns.Item("Building").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "ROUTE", CType(oMatrix.Columns.Item("U_ROUTE").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "ISBACCDF", CType(oMatrix.Columns.Item("U_ISBACCDF").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "ISBACCTF", CType(oMatrix.Columns.Item("U_ISBACCTF").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "ISBACCTT", CType(oMatrix.Columns.Item("U_ISBACCTT").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "ISBACCRA", CType(oMatrix.Columns.Item("U_ISBACCRA").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "ISBACCRV", CType(oMatrix.Columns.Item("U_ISBACCRV").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "ISBCUSTRN", CType(oMatrix.Columns.Item("U_ISBCSTRN").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "TEL1", CType(oMatrix.Columns.Item("U_TEL1").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)

            Me.SetParentSharedData(oForm, "ROUTE", CType(oMatrix.Columns.Item("U_ROUTE").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "SEQ", CType(oMatrix.Columns.Item("U_IDHSEQ").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "CONA", CType(oMatrix.Columns.Item("U_IDHACN").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "MCONA", CType(oMatrix.Columns.Item("CntctPrsn").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "STATENM", CType(oMatrix.Columns.Item("StateNM").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)

            'If oMatrix.Columns.Item("frozenFrom").Cells.Item(iRow).Specific.value.ToString.Trim = "" Then
            '    Me.SetParentSharedData(oForm, "FROZENF", "1900-01-01")
            'Else
            '    Dim dt As String = oForm.DataSources.DataTables.Item("LINESMTX").GetValue("frozenFrom", iRow - 1).ToString
            '    Me.SetParentSharedData(oForm, "FROZENF", CDate(dt).ToString("yyyy/MM/dd")) 'CDate(oMatrix.Columns.Item("frozenFrom").Cells.Item(iRow).Specific.value).ToString("yyyy/MM/dd"))
            'End If
            'If oMatrix.Columns.Item("frozenTo").Cells.Item(iRow).Specific.value.ToString.Trim = "" Then
            '    Me.SetParentSharedData(oForm, "FROZENT", "1900-01-01")
            'Else
            '    Dim dt As String = oForm.DataSources.DataTables.Item("LINESMTX").GetValue("frozenTo", iRow - 1).ToString
            '    Me.SetParentSharedData(oForm, "FROZENT", CDate(dt).ToString("yyyy/MM/dd")) 'CDate(oMatrix.Columns.Item("frozenTo").Cells.Item(iRow).Specific.value).ToString("yyyy/MM/dd"))
            'End If
            Me.SetParentSharedData(oForm, "PRECD", CType(oMatrix.Columns.Item("U_IDHPCD").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "ORIGIN", CType(oMatrix.Columns.Item("U_Origin").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "SITELIC", CType(oMatrix.Columns.Item("U_IDHSLCNO").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "STEID", CType(oMatrix.Columns.Item("U_IDHSteId").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)

            Me.SetParentSharedData(oForm, "FAX", CType(oMatrix.Columns.Item("U_IDHFAX").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)

            Me.SetParentSharedData(oForm, "EMAIL", CType(oMatrix.Columns.Item("U_IDHEMAIL").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "STRNO", CType(oMatrix.Columns.Item("U_IDHSTRNO").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            'Me.SetParentSharedData(oForm, "PREEXPDTTO", oMatrix.Columns.Item("U_PREEXPDTTO").Cells.Item(iRow).Specific.value.ToString.Trim)
            If CType(oMatrix.Columns.Item("U_PREXPDT").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim = "" Then
                Me.SetParentSharedData(oForm, "PREEXPDTTO", "1900-01-01")
            Else
                Dim dt As String = oForm.DataSources.DataTables.Item("LINESMTX").GetValue("U_PREEXPDTTO", iRow - 1).ToString
                Me.SetParentSharedData(oForm, "PREEXPDTTO", CDate(dt).ToString("yyyy/MM/dd")) 'CDate(oMatrix.Columns.Item("frozenTo").Cells.Item(iRow).Specific.value).ToString("yyyy/MM/dd"))
            End If
            Me.SetParentSharedData(oForm, "EPASITEID", CType(oMatrix.Columns.Item("U_EPASTID").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)
            Me.SetParentSharedData(oForm, "MINJOB", CType(oMatrix.Columns.Item("U_MinJob").Cells.Item(iRow).Specific, SAPbouiCOM.EditText).Value.ToString.Trim)

        End Sub

        '**
        '** THIS WILL RETURN A RECORD IF THERE IS ONLY ONE SHIP TO ADDRESS
        '**
        Public Shared Function doGetAddress(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sCardCode As String, ByVal sAddressType As String, Optional ByVal sSearchAddress As String = Nothing) As ArrayList
            Return Config.INSTANCE.doGetAddress(sCardCode, sAddressType, sSearchAddress)
        End Function

        '**
        '** THIS WILL RETURN THE COMPANY ADDRESS
        '**
        Public Shared Function doGetCompanyAddress(ByVal oParent As IDHAddOns.idh.addon.Base) As ArrayList
            Return Config.INSTANCE.doGetCompanyAddress()
        End Function
    End Class
End Namespace
