Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class Subby
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDH_SUBBY", "SubContractor.srf", 5, 24, 860, 362, "Sub-Contractor")
        End Sub
        
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iPos As Integer )
            MyBase.New(oParent, "IDH_SUBBY", "SubContractor.srf", 5, 24, 860, 362, "Sub-Contractor", sParMenu, iPos)
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("[@IDH_SUITPR], OCRD")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_SUITPR", Nothing, Nothing, False, True), True)
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OCRD", Nothing, Nothing, False, True))
            
            oGridN.setOrderValue("U_ZPCD, U_ItemCd, U_WasteCd, U_IDHRTNG, U_EnDate DESC")
            oGridN.setRequiredFilter("CardCode = U_CardCd")

            oGridN.doAddFilterField("IDH_ZPCDTX", "U_ZPCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_ITEM", "U_ItemCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_WSCD", "U_WasteCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_RTNG", "U_IDHRTNG", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            oGridN.doAddListField("U_CardCd", "Carrier", False, -1, Nothing, "IDH_CARR")
			oGridN.doAddListField("U_ZpCd", "Postal Code", False, -1, Nothing, "IDH_ZPCD")
			oGridN.doAddListField("U_StAddr", "Site Address", False, -1, Nothing, "IDH_SADDR")
			oGridN.doAddListField("U_ItemCd", "Container Code", False, -1, Nothing, "IDH_CONTCD")
			oGridN.doAddListField("U_ItemDs", "Container Description", False, -1, Nothing, "IDH_CONTDS")
            oGridN.doAddListField("U_WasteCd", "Waste Code", False, -1, Nothing, "IDH_WSTCD")
            oGridN.doAddListField("U_WasteDs", "Waste Description", False, -1, Nothing, "IDH_WSTDS")
            oGridN.doAddListField("U_IDHRTNG", "Rating", False, -1, Nothing, "IDH_RTNG")
			oGridN.doAddListField("U_EnDate", "End Date", False, -1, Nothing, "IDH_EDT")
			oGridN.doAddListField("U_FTon", "From Weight", False, -1, Nothing, "IDH_FT")
			oGridN.doAddListField("U_TTon", "To Weight", False, -1, Nothing, "IDH_TT")
			oGridN.doAddListField("U_ChrgCal", "Calculation Method", False, -1, Nothing, "IDH_CM")
            '#MA  20170515 start
            oGridN.doAddListField("U_TipTon", "Disposal Cost", False, -1, Nothing, "IDH_TCST")
            oGridN.doAddListField("U_Haulge", "Haulage Cost", False, -1, Nothing, "IDH_Haul")
            '#MA  20170515 End
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
            'doAddUF(oForm, "IDH_ZPCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT)
        End Sub
        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doFinalizeShow(oForm)
            doSetFocus(oForm, "IDH_ZPCDTX")

        End Sub
        
        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            'doZipCodesCombo(oForm)
           
            MyBase.doReadInputParams(oForm)
            Dim sZipCode As String = getParentSharedDataAsString(oForm, "IDH_ZPCDTX")
            If sZipCode Is Nothing Then
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            	oGridN.setInitialFilterValue("U_ZpCd = '...'")
			Else
            	setUFValue(oForm, "IDH_ZPCDTX", sZipCode )
            End If
        End Sub
        
        'Private Sub doZipCodesCombo(ByVal oForm As SAPbouiCOM.Form)
        '    doFillCombo(oForm, "IDH_ZPCD", "[@IDH_SUITPR]", "U_ZPCD", "U_ZPCD as 'Desc'", "( U_CardCd Is Not Null And U_CardCd != '' )", "U_ZPCD", True)
        'End Sub
       	
       	Private Sub doContainerCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim sZipCode As String = CType(getUFValue(oForm, "IDH_ZPCDTX"), String)
       		If sZipCode.Length = 0 Then
       			doClearCombo(oForm, "IDH_ITEM", True)
			Else
				doFillCombo(oForm, "IDH_ITEM", "[@IDH_SUITPR]", "U_ItemCd", "U_ItemDs", " U_ZpCd like '" & sZipCode & "%' AND ( U_CardCd Is Not Null And U_CardCd != '' )", "U_ItemDs", True)
       			doWasteCodeCombo(oForm)
       		End If
       	End Sub

       	Private Sub doWasteCodeCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim sItemCode As String = CType(getUFValue(oForm, "IDH_ITEM"), String)
            Dim sZipCode As String = CType(getUFValue(oForm, "IDH_ZPCDTX"), String)
       		If sZipCode.Length = 0 OrElse sItemCode.Length = 0 Then
       			doClearCombo(oForm, "IDH_WSCD", True)
			Else
				doFillCombo(oForm, "IDH_WSCD", "[@IDH_SUITPR]", "U_WasteCd", "U_WasteDs", " U_ZpCd like '" & sZipCode & "%' AND U_ItemCd = '" & sItemCode & "' AND ( U_CardCd Is Not Null And U_CardCd != '' )", "U_WasteDs", True)
       			doRatingCombo(oForm)
       		End If
       	End Sub

       	Private Sub doRatingCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim sItemCode As String = CType(getUFValue(oForm, "IDH_ITEM"), String)
            Dim sZipCode As String = CType(getUFValue(oForm, "IDH_ZPCDTX"), String)
            Dim sWasteCode As String = CType(getUFValue(oForm, "IDH_WSCD"), String)
       		
       		If sZipCode.Length = 0 OrElse _
       			sItemCode.Length = 0 OrElse _
       			sWasteCode.Length = 0 Then
       			doClearCombo(oForm, "IDH_RTNG", True)
			Else
				doFillCombo(oForm, "IDH_RTNG", "OCRD, [@IDH_SUITPR] ", "U_IDHRTNG", "U_IDHRTNG As 'Desc'", "CardCode = U_CardCd AND CardType = 'S' AND U_ZpCd like '" & sZipCode & "%' AND U_ItemCd = '" & sItemCode & "' AND U_WasteCd = '" & sWasteCode & "'", "U_IDHRTNG", True)
       		End If
       	End Sub

	   Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim bReturn As Boolean = MyBase.doItemEvent(oForm, pVal, BubbleEvent)
	        If pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
	            If pVal.BeforeAction = False Then
	                'If pVal.ItemUID = "IDH_ZPCD" Then
                    '    Dim sZipCode As String = getUFValue(oForm, "IDH_ZPCD")
                    '    setUFValue(oForm, "IDH_ZPCDTX", sZipCode)
	                '    doContainerCombo(oForm)
	                '    doLoadData(oForm)
	                'Else
	                If pVal.ItemUID = "IDH_ITEM" Then
	                	doWasteCodeCombo(oForm)
                        doLoadData(oForm)
					ElseIf pVal.ItemUID = "IDH_WSCD" Then
	               		doRatingCombo(oForm)
                        doLoadData(oForm)
                    ElseIf pVal.ItemUID = "IDH_RTNG" Then
                        doLoadData(oForm)
	                End If
	            End If
	        Else If pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
	        	If pVal.BeforeAction = False Then
	        		If pVal.ItemUID = "IDH_ZPCDTX" Then
                        setUFValue(oForm, "IDH_ZPCD", "")
                        doContainerCombo(oForm)
                        doLoadData(oForm)
	        		End If
	        	End If
	        End If
	        Return bReturn
        End Function

    End Class
End Namespace
