Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.bridge.lookups


Namespace idh.forms.search
    Public Class ItemSearch
        Inherits IDHAddOns.idh.forms.Search

        'Private  As Boolean = False

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHISRC", "Item Search.srf", 5, 40, 603, 350, "Item Search")
        End Sub

        '## Start 04-07-2013
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            setParentSharedData(oForm, "bClosingFormByOKButton", "False")
            'bClosingFormByOKButton = False
            Me.setParentSharedData(oForm, "IDH_ITM1SRCHOPEND", "LOADED")
            MyBase.doLoadData(oForm)
        End Sub

        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD OrElse _
                    (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN And pVal.CharPressed.ToString = "27") Then
                    Me.setParentSharedData(oForm, "IDH_ITM1SRCHOPEND", "")
                    If getParentSharedDataAsString(oForm, "bClosingFormByOKButton") = "False" Then
                        MyBase.doButtonID2(oForm, pVal, BubbleEvent)
                    End If
                End If
            End If
            Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
        End Function

        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doFinalizeShow(oForm)
            If oForm.Visible = False Then
                Me.setParentSharedData(oForm, "IDH_ITM1SRCHOPEND", "")
            Else
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                If oGridN Is Nothing Then
                    oGridN = New FilterGrid(Me, oForm, "LINESGRID", True)
                End If
                oGridN.doReloadData()
            End If
        End Sub
        '## End

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            'oGridN.setTableValue("OITM i, OITB ig")
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OITM", "i", Nothing, False, True))
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("OITB", "ig", Nothing, False, True))

            Dim sReqFilter As String

            If Config.INSTANCE.getParameterAsBool("USAREL", False) = True OrElse Config.INSTANCE.getParameterAsBool("ENBWPITM", False) = True Then
                sReqFilter = "i.ItmsGrpCod = ig.ItmsGrpCod "
                sReqFilter = sReqFilter + " AND i.ItmsGrpCod NOT IN (" + Config.ParameterWithDefault("IGNWSTGP", "-1") + ")"
            Else
                sReqFilter = "i.ItmsGrpCod = ig.ItmsGrpCod "
            End If
            sReqFilter = sReqFilter + If(sReqFilter.Length > 0, " AND i.FrozenFor <> 'Y' ", " i.FrozenFor <> 'Y' ")

            If (getParentSharedData(oGridN.getSBOForm(), "IDH_ONLYFINISHED") IsNot Nothing) Then
                Dim sOnlyFinished As String = getParentSharedData(oGridN.getSBOForm(), "IDH_ONLYFINISHED").ToString
                If sOnlyFinished <> String.Empty Then
                    sReqFilter = sReqFilter + If(sReqFilter.Length > 0, " AND isnull(i.SWW,'') = 'Y' ", " isnull(i.SWW,'') = 'Y' ")
                End If
            End If
            If (getParentSharedData(oGridN.getSBOForm(), "ManBtchNum") IsNot Nothing) Then
                Dim sManBtchNum As String = getParentSharedData(oGridN.getSBOForm(), "ManBtchNum").ToString
                If sManBtchNum <> String.Empty Then
                    sReqFilter = sReqFilter + If(sReqFilter.Length > 0, " AND isnull(i.ManBtchNum,'') = '" + sManBtchNum + "' ", " isnull(i.ManBtchNum,'') = '" + sManBtchNum + "' ")
                End If
            End If
            '

            oGridN.setRequiredFilter(sReqFilter)
            oGridN.setOrderValue("i.ItmsGrpCod, i.ItemCode ")

            oGridN.doAddFilterField("IDH_ITMCOD", "i.ItemCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDH_ITMNAM", "i.ItemName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddFilterField("IDH_GRPCOD", "i.ItmsGrpCod", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "IN", 30)
            oGridN.doAddFilterField("IDH_GRPNAM", "ig.ItmsGrpNam", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, Nothing, True)
            oGridN.doAddListField("i.ItmsGrpCod", "Item Group Code", False, 0, Nothing, "ITMSGRPCOD")
            oGridN.doAddListField("ig.ItmsGrpNam", "Item Group Name", False, -1, Nothing, "ITMSGRPNAM")
            '## Start 02-08-2013
            oGridN.doAddListField("i.ItemCode", "Item Code", False, 100, Nothing, "ITEMCODE", -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            '## End
            oGridN.doAddListField("i.ItemName", "Item Name", False, 410, Nothing, "ITEMNAME")
            oGridN.doAddListField("i.SalUnitMsr", "Sales UOM", False, 20, Nothing, "SUOM")
            oGridN.doAddListField("i.BuyUnitMsr", "Purchase UOM", False, 20, Nothing, "PUOM")
            oGridN.doAddListField("i.U_WTDFW", "Default Weight", False, 50, Nothing, "DEFWEI")
            oGridN.doAddListField("i.U_WTUDW", "Use Default Weight", False, 50, Nothing, "USEDEFWEI")
            oGridN.doAddListField("i.frozenFor", "Frozen", False, 10, Nothing, "FROZEN")

            If getParentSharedData(oGridN.getSBOForm(), "SRCHCONT") IsNot Nothing AndAlso getParentSharedData(oGridN.getSBOForm(), "SRCHCONT").ToString().Equals("Yes") Then
                Dim sQry As String = "select DISTINCT ItmsGrpCod, ItmsGrpNam from OITB g With(nolock), [@IDH_JOBTYPE] jt  With(nolock) where g.ItmsGrpCod=jt.U_ItemGrp " _
                    & " AND jt.U_OrdCat='Waste Order'"
                Dim oRecordSet As com.idh.bridge.DataRecords
                oRecordSet = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("doGetWOContainer", sQry)
                If oRecordSet.RecordCount > 0 Then
                    If getParentSharedData(oGridN.getSBOForm(), "IDH_GRPCOD") Is Nothing OrElse getParentSharedData(oGridN.getSBOForm(), "IDH_GRPCOD").ToString().Trim().Equals(String.Empty) Then
                        Dim sContainercode As String = ""
                        For i As Int16 = 0 To oRecordSet.RecordCount - 2
                            sContainercode &= oRecordSet.getValueAsStringNotNull("ItmsGrpCod") & ","
                            oRecordSet.gotoRow(i)
                        Next
                        oRecordSet.gotoLast()
                        sContainercode &= oRecordSet.getValueAsStringNotNull("ItmsGrpCod")
                        'setUFValue(oGridN.getSBOForm(), "IDH_GRPCOD", sContainercode, True, True)
                        setParentSharedData(oGridN.getSBOForm, "IDH_GRPCOD", sContainercode)
                        'setSharedData(oGridN.getSBOForm, "IDH_GRPCOD", sContainercode)
                    End If
                End If
            End If

        End Sub

    End Class
End Namespace
