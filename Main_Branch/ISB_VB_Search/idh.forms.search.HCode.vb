Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.search
    Public Class HCode
        Inherits IDHAddOns.idh.forms.Search

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDH_HCDSR", "HCode Search.srf", 5, 45, 603, 320, "HCode Search")
        End Sub

        Public Overrides Sub doSetGridOptions(ByVal oGridN As FilterGrid)
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_HCODE", Nothing, Nothing, False, True))
            oGridN.setOrderValue("Code")

            oGridN.doAddFilterField("IDH_HCOD", "U_HCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_HDES", "U_HDesc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100)

            oGridN.doAddListField("U_HCode", "HCode", False, -1, Nothing, "IDH_HCODE")
            oGridN.doAddListField("U_HDesc", "Description", False, -1, Nothing, "IDH_HDESC")
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace