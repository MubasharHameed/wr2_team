Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Namespace idh.forms.options
    Public Class Worksheet
        Inherits IDHAddOns.idh.forms.Base

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDH_WTWSOP", Nothing, -1, "Option.srf", False, True, False, "Option", load_Types.idh_LOAD_NORMAL)
        End Sub      


        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                doAddUFCheck(oForm, "IDH_WTPW", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
                doAddUFCheck(oForm, "IDH_WTND", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            If getHasSharedData(oForm) Then
                Dim sValue As String = getParentSharedData(oForm, "ParamName")
            End If
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        '** Do the final form steps to show after loaddata
        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            oForm.Visible = True
        End Sub

        '*** Get the selected rows
        Private Sub doPrepareModalData(ByVal oForm As SAPbouiCOM.Form)
            Dim sPrintWorksheet As String = getUFValue(oForm, "IDH_WTPW")
            Dim sNotifyDriver As String = getUFValue(oForm, "IDH_WTND")

            setParentSharedData(oForm, "IDH_WTPW", sPrintWorksheet)
            setParentSharedData(oForm, "IDH_WTND", sNotifyDriver)

            doReturnFromModalShared(oForm, True)
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If goParent.doCheckModal(oForm.UniqueID) = True Then
                    doPrepareModalData(oForm)
                End If
                BubbleEvent = False
            End If
        End Sub

        Public Overrides Sub doButtonID2(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            doReturnFromCanceled(oForm, BubbleEvent)
            BubbleEvent = False
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then

            End If
            Return True
        End Function

        Public Overrides Sub doClose()
        End Sub

    End Class
End Namespace
