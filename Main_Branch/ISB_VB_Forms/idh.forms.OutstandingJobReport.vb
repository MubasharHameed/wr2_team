Imports System.IO
Imports System.Collections
Imports System

Imports com.idh.bridge
Imports com.idh.dbObjects.User
Imports com.idh.dbObjects.numbers

Namespace idh.forms
    Public Class OutstandingJobReport
        Inherits IDHAddOns.idh.forms.Base

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHOJOB", Nothing, 0, "Outstanding Job Report.srf", False, True, False)
        End Sub

        '*** Create Sub-Menu
        Protected Overrides Sub doCreateSubMenu()
        	doCreateFormMenu("IDHREP", "Outstanding Job Report")
'            Dim oMenus As SAPbouiCOM.Menus
'            Dim oMenuItem As SAPbouiCOM.MenuItem
'            Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
'
'            Try
'                oMenuItem = goParent.goApplication.Menus.Item("IDHREP")
'            Catch ex1 As Exception
'                oMenuItem = goParent.goApplication.Menus.Item(goParent.goMenuID)
'                oMenus = oMenuItem.SubMenus
'
'                oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
'                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP
'                oCreationPackage.UniqueID = "IDHREP"
'                oCreationPackage.String = "Reports"
'                oCreationPackage.Position = 80
'                Try
'                    oMenus.AddEx(oCreationPackage)
'                Catch ex As Exception
'                End Try
'                oMenuItem = goParent.goApplication.Menus.Item("IDHREP")
'            End Try
'
'            oMenus = oMenuItem.SubMenus
'
'            oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
'            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING
'            oCreationPackage.UniqueID = gsType
'            oCreationPackage.String = "Outstanding Job Report"
'            oCreationPackage.Position = 4
'
'            Try
'                oMenus.AddEx(oCreationPackage)
'            Catch ex As Exception
'            End Try
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Dim oMatrix As SAPbouiCOM.Matrix
            'Dim odsSetup As SAPbouiCOM.DBDataSource
            Dim bCanChange As Boolean = True
            Try
                'Dim odbSrc As SAPbouiCOM.DBDataSource
                'Dim oItem As SAPbouiCOM.Item
                'Dim oItem1 As SAPbouiCOM.Item

                'Dim oLink As SAPbouiCOM.LinkedButton
                'Dim iRecords As Long
                'Dim iCount As Long

                oForm.Freeze(True)
                oMatrix = oForm.Items.Item("IDH_MATRIX").Specific

                oForm.DataSources.DBDataSources.Add("@IDH_DOCPARTS")
                oForm.DataSources.DBDataSources.Add("@IDH_DOCPRTLNS")

                oForm.Items.Item("IDH_PARTCD").Specific.DataBind.SetBound(True, "@IDH_DOCPARTS", "Code")
                oForm.Items.Item("IDH_DOCPAR").Specific.DataBind.SetBound(True, "@IDH_DOCPARTS", "Name")
                oForm.DataBrowser.BrowseBy = "IDH_DOCPAR"

                oMatrix.Columns.Item("Code").DataBind.SetBound(True, "@IDH_DOCPRTLNS", "Code")
                oMatrix.Columns.Item("ItmGrCd").DataBind.SetBound(True, "@IDH_DOCPRTLNS", "U_ITMGRCD")
                oMatrix.Columns.Item("ItmGrDsc").DataBind.SetBound(True, "@IDH_DOCPRTLNS", "U_ITMDSC")

                oForm.AutoManaged = True
                oForm.SupportedModes = -1
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                oForm.Visible = True

                oForm.Freeze(False)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub

        '** The Initializer
        Protected Sub doReLoadData(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        Public Sub doLoadLines(ByVal oForm As SAPbouiCOM.Form)
            Dim sPart As String = oForm.Items.Item("IDH_PARTCD").Specific.Value.trim()
            Dim oMatrix As SAPbouiCOM.Matrix
            If sPart.Length > 0 Then
                Dim oConditions As SAPbouiCOM.Conditions
                Dim oCondition As SAPbouiCOM.Condition
                oConditions = New SAPbouiCOM.Conditions

                oCondition = oConditions.Add
                oCondition.Alias = "U_DOCPRTCD"
                oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                oCondition.CondVal = sPart
                oForm.DataSources.DBDataSources.Add("@IDH_DOCPRTLNS").Query(oConditions)

                oMatrix = oForm.Items.Item("IDH_MATRIX").Specific
                oMatrix.LoadFromDataSource()
                If oMatrix.VisualRowCount > 0 Then
                    doFillItmGrpCombo(oForm)
                End If
            End If
        End Sub

        Public Sub doFillItmGrpCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Dim oMatrix As SAPbouiCOM.Matrix
            Dim otfitmgrp As SAPbouiCOM.ComboBox
            Dim sKey As String
            Dim sValue As String
            Try
                oMatrix = oForm.Items.Item("IDH_MATRIX").Specific
                otfitmgrp = oMatrix.Columns.Item("ItmGrCd").Cells.Item(1).Specific

                If Not (otfitmgrp.ValidValues Is Nothing) AndAlso otfitmgrp.ValidValues.Count > 0 Then
                    Exit Sub
                End If

                oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                oRecordSet.DoQuery("select ItmsGrpCod, ItmsGrpNam from OITB")
                If oRecordSet.RecordCount > 0 Then
                    While oRecordSet.EoF = False
                        sKey = oRecordSet.Fields.Item("ItmsGrpCod").Value().Trim()
                        sValue = oRecordSet.Fields.Item("ItmsGrpNam").Value().Trim()
                        otfitmgrp.ValidValues.Add(sKey, sValue)
                        oRecordSet.MoveNext()
                    End While
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling Item Group Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Item Group")})
            Finally
                oRecordSet = Nothing
            End Try
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                   oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    If doAddPart(oForm) = True Then
                        If doUpdateLines(oForm) = True Then
                            doRemoveFormRows(oForm.UniqueID)
                            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                doCreateNewEntry(oForm)
                            Else
                                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                            End If
                        End If
                    End If
                End If
            Else
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    oForm.Freeze(True)
                    doLoadLines(oForm)
                    oForm.Freeze(False)
                End If
            End If
        End Sub

        Public Overrides Sub doButtonID2(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = False Then
                doRemoveFormRows(oForm.UniqueID)
            Else
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    doReLoadData(oForm)
                End If
            End If
        End Sub

        Private Function doAddPart(ByVal oForm As SAPbouiCOM.Form) As Boolean
            If Not (oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE) Then
                Return True
            End If

            Dim owEdit As SAPbouiCOM.EditText
            owEdit = oForm.Items.Item("IDH_DOCPAR").Specific
            If owEdit.Value Is Nothing OrElse owEdit.Value.Length = 0 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("The Document Part must be entered")
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("The Document Part must be entered", "ERUSDOCE", {Nothing})
                Return False
                Exit Function
            End If

            Dim utGeneralSetting As SAPbobsCOM.UserTable
            Dim sPart As String
            Dim sKey As String
            Dim iwResult As Integer = 0
            Dim swResult As String = Nothing

            Try
                sKey = oForm.Items.Item("IDH_PARTCD").Specific.Value
                sPart = oForm.Items.Item("IDH_DOCPAR").Specific.Value

                utGeneralSetting = goParent.goDICompany.UserTables.Item("IDH_DOCPARTS")
                utGeneralSetting.Code = sKey
                utGeneralSetting.Name = sPart
                iwResult = utGeneralSetting.Add()
                If iwResult <> 0 Then
                    goParent.goDICompany.GetLastError(iwResult, swResult)
                    goParent.goApplication.StatusBar.SetText("Error Updating: " + swResult, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    Return False
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error adding the part.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXADDV", {com.idh.bridge.Translation.getTranslatedWord("part")})
                Return False
            Finally
            End Try
            Return True
        End Function

        Private Function doUpdateLines(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim iwResult As Integer = 0
            Dim swResult As String = Nothing
            Dim oMatrix As SAPbouiCOM.Matrix
            Dim utGeneralSetting As SAPbobsCOM.UserTable
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

            Try
                oMatrix = oForm.Items.Item("IDH_MATRIX").Specific
                utGeneralSetting = goParent.goDICompany.UserTables.Item("IDH_DOCPRTLNS")

                Dim iCount As Integer
                Dim sKey As String
                Dim sPart As String
                'Dim sValue As String
                'Dim iValue As Integer
                oMatrix.FlushToDataSource()

                sPart = oForm.Items.Item("IDH_PARTCD").Specific.Value.trim()
                With oForm.DataSources.DBDataSources.Item("@IDH_DOCPRTLNS")
                    For iCount = 0 To oMatrix.VisualRowCount - 1
                        sKey = .GetValue("Code", iCount).Trim()

                        If doContainsNewRow(oForm.UniqueID, "IDH_MATRIX", sKey) = True Then
                            utGeneralSetting.Code = sKey
                            utGeneralSetting.Name = sKey
                            utGeneralSetting.UserFields.Fields.Item("U_DOCPRTCD").Value = sPart
                            utGeneralSetting.UserFields.Fields.Item("U_ITMGRCD").Value = .GetValue("U_ITMGRCD", iCount)
                            utGeneralSetting.UserFields.Fields.Item("U_ITMDSC").Value = .GetValue("U_ITMDSC", iCount).Trim()
                            iwResult = utGeneralSetting.Add()
                            If iwResult <> 0 Then
                                goParent.goDICompany.GetLastError(iwResult, swResult)
                                goParent.goApplication.StatusBar.SetText("Error Updating: " + swResult, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                Return False
                            End If
                            doRemoveNewRow(oForm.UniqueID, "IDH_MATRIX", sKey)
                        Else
                            Dim sQry As String
                            sQry = "UPDATE [@IDH_DOCPRTLNS] set " & _
                                    " U_DOCPRTCD = '" & sPart & "', " & _
                                    " U_ITMGRCD = " & .GetValue("U_ITMGRCD", iCount) & ", " & _
                                    " U_ITMDSC = '" & .GetValue("U_ITMDSC", iCount).Trim() & "' " & _
                                "WHERE Code = '" & sKey & "'"
                            oRecordSet.DoQuery(sQry)
                        End If
                    Next

                    '*** DELETE THE ITEMS CONTAINED IN THE REMOVED ROW LIST
                    Dim aKeys As ArrayList = doGetRemovedKeyList(oForm.UniqueID, "IDH_MATRIX")
                    Dim sKeys As String = ""
                    If Not (aKeys Is Nothing) Then
                        While aKeys.Count > 0
                            If sKeys.Length > 0 Then
                                sKeys = sKeys & ","
                            End If
                            sKeys = sKeys & "'" & aKeys.Item(0) & "'"
                            doRemoveRemovedRow(oForm.UniqueID, "IDH_MATRIX", aKeys.Item(0))
                        End While
                        If sKeys.Length > 0 Then
                            oRecordSet.DoQuery("DELETE from [@IDH_DOCPRTLNS] where Code In (" & sKeys & ")")
                        End If
                    End If

                End With
                Return True
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error updating the lines.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXUVAR", {com.idh.bridge.Translation.getTranslatedWord("lines")})
                Return False
            Finally
            End Try
        End Function

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                Dim sValue As String
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT AndAlso _
                    pVal.ColUID = "ItmGrCd" Then
                    Dim oMatrix As SAPbouiCOM.Matrix
                    Dim oCombo As SAPbouiCOM.ComboBox
                    Dim oDesc As SAPbouiCOM.EditText
                    oMatrix = oForm.Items.Item("IDH_MATRIX").Specific
                    oCombo = oMatrix.Columns.Item("ItmGrCd").Cells.Item(pVal.Row).Specific
                    oDesc = oMatrix.Columns.Item("ItmGrDsc").Cells.Item(pVal.Row).Specific
                    oDesc.Value = oCombo.Selected.Description
                ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                       oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE OrElse _
                       oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                        If pVal.ItemUID = "IDH_DEL" Then
                            Dim oItem As SAPbouiCOM.Item
                            Dim otblMatrix As SAPbouiCOM.Matrix
                            oItem = oForm.Items.Item("IDH_MATRIX")
                            oItem.AffectsFormMode = False
                            otblMatrix = oItem.Specific
                            otblMatrix.Clear()

                            'Dim iRecords As Integer
                            Dim odsDBDataSource As SAPbouiCOM.DBDataSource
                            oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

                            odsDBDataSource = oForm.DataSources.DBDataSources.Item("@IDH_DOCPARTS")

                            Dim iCount As Integer = odsDBDataSource.Size
                            If odsDBDataSource.Size() > 0 Then
                                sValue = odsDBDataSource.GetValue("Code", odsDBDataSource.Offset)
                                oRecordSet.DoQuery("DELETE from [@IDH_DOCPARTS] where Code='" & sValue & "'")
                                oRecordSet.DoQuery("DELETE from [@IDH_DOCPRTLNS] where U_DOCPRTCD='" & sValue & "'")
                            End If

                            oRecordSet = Nothing
                        ElseIf pVal.ItemUID = "IDH_ADD" Then
                            Try
                                Dim oMatrix As SAPbouiCOM.Matrix
                                Dim iCount As Integer
                                'Dim sKey As String
                                Dim sPart As String

                                oMatrix = oForm.Items.Item("IDH_MATRIX").Specific
                                oMatrix.AddRow()
                                oMatrix.FlushToDataSource()

                                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, "DOCPRTLN")
                                sPart = oForm.Items.Item("IDH_PARTCD").Specific.Value

                                With oForm.DataSources.DBDataSources.Item("@IDH_DOCPRTLNS")
                                    iCount = .Offset
                                    .SetValue("Code", iCount, oNumbers.CodeCode)
                                    .SetValue("Name", iCount, oNumbers.NameCode)
                                    .SetValue("U_DOCPRTCD", iCount, sPart)
                                    .SetValue("U_ITMGRCD", iCount, "")
                                    .SetValue("U_ITMDSC", iCount, "")
                                End With
                                doAddNewRow(oForm.UniqueID, "IDH_MATRIX", oNumbers.CodeCode)
                                If oMatrix.VisualRowCount = 1 Then
                                    doFillItmGrpCombo(oForm)
                                End If
                                oMatrix.LoadFromDataSource()
                            Catch ex As Exception
                                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Item Event - " & oForm.UniqueID & "." & pVal.ItemUID)
                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPIEV", {oForm.UniqueID, pVal.ItemUID})
                            End Try
                        ElseIf pVal.ItemUID = "IDH_REM" Then
                            Dim oMatrix As SAPbouiCOM.Matrix
                            oMatrix = oForm.Items.Item("IDH_MATRIX").Specific

                            Dim iRow As Integer = 1
                            Dim iRowCount As Integer = oMatrix.VisualRowCount
                            Dim bIsSelected As Boolean = False
                            While iRow <= iRowCount
                                If oMatrix.IsRowSelected(iRow) Then
                                    bIsSelected = True
                                    Dim sKey As String = oMatrix.Columns.Item("Code").Cells.Item(iRow).Specific.Value
                                    oMatrix.DeleteRow(iRow)
                                    doAddRemovedRow(oForm.UniqueID, "IDH_MATRIX", sKey)
                                    iRowCount = iRowCount - 1
                                Else
                                    iRow = iRow + 1
                                End If
                            End While
                            If bIsSelected = False Then
                                'goParent.goApplication.SetStatusBarMessage("Please select a row to be deleted", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                                goParent.goDB.doErrorToStatusbar( getTranslatedWord("Please select a row to be deleted."), true )
                            Else
                                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                End If
                            End If
                        End If
                    End If
                End If
            End If
            Return False
        End Function

        '** Create a new Entry
        Public Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form)
            With oForm.DataSources.DBDataSources.Item("@IDH_DOCPARTS")
                oForm.DataSources.DBDataSources.Item("@IDH_DOCPARTS").InsertRecord(.Offset)

                'Dim sCode As String = DataHandler.doGenerateCode(Nothing, "DOCPRT")
                oForm.Items.Item("IDH_PARTCD").Specific.Value = DataHandler.INSTANCE.doGenerateCode("DOCPRT")

                Dim oMatrix As SAPbouiCOM.Matrix
                Dim oItem As SAPbouiCOM.Item

                oItem = oForm.Items.Item("IDH_DOCPAR")
                oItem.Specific.Value = ""

                oMatrix = oForm.Items.Item("IDH_MATRIX").Specific
                oMatrix.Clear()
                oMatrix.FlushToDataSource()

                oItem.Click()
            End With
        End Sub

        '** The Menu Event handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            Return True
        End Function

        Public Overrides Sub doClose()
        End Sub

    End Class
End Namespace
