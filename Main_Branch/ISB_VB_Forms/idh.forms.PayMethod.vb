Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge.lookups

Namespace idh.forms
    Public Class PayMethod
        Inherits IDHAddOns.idh.forms.Base

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHPAYMET", Nothing, -1, "PayMet.srf", False, True, False, "Payment Method", load_Types.idh_LOAD_NORMAL)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            oForm.Freeze(True)
            Try
                doAddUF(oForm, "IDH_PAYMET", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, False, False)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
            oForm.Freeze(False)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)
            doFillPayMethods(oForm)
            doAddWF(oForm, "PREFIX", getParentSharedData(oForm, "PREFIX"))
            doAddWF(oForm, "ACTION", getParentSharedData(oForm, "ACTION"))
            doAddWF(oForm, "ROWS", getParentSharedData(oForm, "ROWS"))
        End Sub

        Public Sub doFillPayMethods(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            Dim oCombo As SAPbouiCOM.ComboBox
            oItem = oForm.Items.Item("IDH_PAYMET")
            oItem.DisplayDesc = False
            oCombo = oItem.Specific

            doClearValidValues(oCombo.ValidValues)

            Dim sMethods As String = Config.Parameter("MDPATME")
            If sMethods Is Nothing OrElse sMethods.Length() = 0 Then
                oCombo.ValidValues.Add("Cash", "0")
            Else
                Dim saValues() As String = sMethods.Split(",")
                Dim sVal As String
                Dim sDesc As String
                For iInx As Integer = 0 To saValues.Length - 1
                    sVal = saValues(iInx).Trim()
                    sDesc = "" 'saValues(iInx).Trim()
                    oCombo.ValidValues.Add(sVal, sDesc)
                Next
            End If
            oCombo.Select(0, SAPbouiCOM.BoSearchKey.psk_Index)
        End Sub

        '*** Get the selected rows
        Protected Sub doPrepareModalData(ByVal oForm As SAPbouiCOM.Form)
            Dim sMethod As String = getUFValue(oForm, "IDH_PAYMET")

            'Dim sPrefix As String = getWFValue(oForm, "PREFIX")
            'If sPrefix Is Nothing Then
            '    sPrefix = ""
            'End If
            'setParentSharedData(oForm, "STATUS", sPrefix & com.idh.bridge.lookups.FixedValues.getStatusInvoice())
            
            Dim sAction As  String = getWFValue(oForm, "ACTION")
            Dim sStatus As String
            If sAction Is Nothing Then
                sStatus = com.idh.bridge.lookups.FixedValues.getStatusInvoice()
            ElseIf sAction = "D" Then
				sStatus = com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus()
			Else
				sStatus = com.idh.bridge.lookups.FixedValues.getReqInvoiceStatus()
			End If
            
            setParentSharedData(oForm, "STATUS", sStatus)
            setParentSharedData(oForm, "METHOD", sMethod)
            setParentSharedData(oForm, "ROWS", getWFValue(oForm, "ROWS"))
            doReturnFromModalShared(oForm, True)
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If goParent.doCheckModal(oForm.UniqueID) = True Then
                    doPrepareModalData(oForm)
                End If
            End If
        End Sub

        Public Overrides Sub doClose()
        End Sub

    End Class
End Namespace
