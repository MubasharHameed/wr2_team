Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge.lookups

Namespace idh.forms
    Public Class OSMEdit
        Inherits IDHAddOns.idh.forms.Base

        Private moSelectList As New IDHAddOns.idh.Grid.SelectList

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDH_OSMEDIT", Nothing, -1, "OSM Edit.srf", False, True, False, "OSM Edit", load_Types.idh_LOAD_NORMAL)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                doAddUF(oForm, "IDH_REQDAT", SAPbouiCOM.BoDataType.dt_DATE, 10, False, False)
                doAddUF(oForm, "IDH_REQTF", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 10, False, False)
                doAddUF(oForm, "IDH_REQTT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 10, False, False)
                doAddUF(oForm, "IDH_STARDT", SAPbouiCOM.BoDataType.dt_DATE, 10, False, False)
                doAddUF(oForm, "IDH_ENDDT", SAPbouiCOM.BoDataType.dt_DATE, 10, False, False)
                doAddUF(oForm, "IDH_VEHREG", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, False, False)
                doAddUF(oForm, "IDH_DRIVER", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, False, False)
                doAddUF(oForm, "IDH_RTCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, False, False)
                doAddUF(oForm, "IDH_CONNO", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, False, False)

                doAddUF(oForm, "IDH_CARCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50, False, False)
                doAddUF(oForm, "IDH_CARNM", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 100, False, False)
				doAddUF(oForm, "IDH_CARREF", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 100, False, False)
                doAddUF(oForm, "IDH_VEHCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50, False, False)
                doAddUF(oForm, "IDH_VEHDS", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 100, False, False)
                ''MA Start 23-02-2015 Issue#256
                doAddUF(oForm, "IDH_CUSREF", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 50, False, False)
                ''MA End 23-02-2015 Issue#256

                doAddUFCheck(oForm, "IDH_INRQDT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
                doAddUFCheck(oForm, "IDH_INRQTF", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
                doAddUFCheck(oForm, "IDH_INRQTT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
                doAddUFCheck(oForm, "IDH_INSTDT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
                doAddUFCheck(oForm, "IDH_INENDT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
                doAddUFCheck(oForm, "IDH_INVERE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
                doAddUFCheck(oForm, "IDH_INDRV", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
                doAddUFCheck(oForm, "IDH_INRT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
                doAddUFCheck(oForm, "IDH_INCN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
                ''MA Start 23-02-2015 Issue#256
                doAddUFCheck(oForm, "IDH_INCSRF", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
                ''MA End 23-02-2015 Issue#256

                'Fields to save OLD Vehicle Reg and Carrier Code to compare and alert users 
                doAddUF(oForm, "IDH_OLDVR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, False, False)
                doAddUF(oForm, "IDH_OLDCC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50, False, False)

                Dim sTime As String = Config.Parameter("WOTMFR")
                setUFValue(oForm, "IDH_REQTF", sTime)
                sTime = Config.Parameter("WOTMTO")
                setUFValue(oForm, "IDH_REQTT", sTime)

                ''##MA Start 17-09-2014 Issue#427
                If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_WOR_REQUESTDATE) = False Then '8192
                    ' doSetFocus(oForm, "IDH_STARDT")
                    oForm.Items.Item("IDH_STARDT").Click()
                    setEnableItem(oForm, False, "IDH_REQDAT")
                    'oForm.Items.Item("IDH_REQDAT").Enabled = False
                    setEnableItem(oForm, False, "IDH_REQTF")
                    setEnableItem(oForm, False, "IDH_REQTT")
                End If
                ''##MA End 17-09-2014 Issue#427
                ''MA Start 23-02-2015 Issue#256
                If Config.Parameter("MDREAE").ToUpper().Equals("FALSE") Then
                    setEnableItem(oForm, False, "IDH_CUSREF")
                    setEnableItem(oForm, False, "IDH_INCSRF")
                End If
                ''MA End 23-02-2015 Issue#256
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            If getHasSharedData(oForm) Then
                Dim sVehReg As String = getParentSharedData(oForm, "IDH_VEHREG")
                setUFValue(oForm, "IDH_VEHREG", sVehReg)

                Dim sCarrCd As String = getParentSharedData(oForm, "IDH_CARCD")
                Dim sCarrNm As String = getParentSharedData(oForm, "IDH_CARNM")
                Dim sDriver As String = getParentSharedData(oForm, "IDH_DRIVER")
                Dim sCarrierRefNo As String = getParentSharedData(oForm, "IDH_CARREF")
                ''MA Start 23-02-2015 Issue#256
                Dim sCustRefNo As String = getParentSharedData(oForm, "IDH_CUSREF")
                ''MA End 23-02-2015 Issue#256

                setUFValue(oForm, "IDH_CARCD", sCarrCd)
                setUFValue(oForm, "IDH_CARNM", sCarrNm)
                setUFValue(oForm, "IDH_DRIVER", sDriver)
                setUFValue(oForm, "IDH_CARREF", sCarrierRefNo)


                setUFValue(oForm, "IDH_OLDVR", sVehReg)
                setUFValue(oForm, "IDH_OLDCC", sCarrCd)
                ''MA Start 23-02-2015 Issue#256
                setUFValue(oForm, "IDH_CUSREF", sCustRefNo)
                ''MA Start 23-02-2015 Issue#256

            End If
        End Sub

        '*** Get the selected rows
        Protected Sub doPrepareModalData(ByVal oForm As SAPbouiCOM.Form, Optional ByVal iSel As Integer = -1)
            If getUFValue(oForm, "IDH_INRQDT").Equals("Y") Then
                setParentSharedData(oForm, "RDATE", getUFValue(oForm, "IDH_REQDAT"))
            Else
                setParentSharedData(oForm, "RDATE", "IGNORE")
            End If

            If getUFValue(oForm, "IDH_INRQTF").Equals("Y") Then
                setParentSharedData(oForm, "RTIMF", getUFValue(oForm, "IDH_REQTF"))
            Else
                setParentSharedData(oForm, "RTIMF", "IGNORE")
            End If

            If getUFValue(oForm, "IDH_INRQTT").Equals("Y") Then
                setParentSharedData(oForm, "RTIMT", getUFValue(oForm, "IDH_REQTT"))
            Else
                setParentSharedData(oForm, "RTIMT", "IGNORE")
            End If

            If getUFValue(oForm, "IDH_INSTDT").Equals("Y") Then
                setParentSharedData(oForm, "ASDATE", getUFValue(oForm, "IDH_STARDT"))
            Else
                setParentSharedData(oForm, "ASDATE", "IGNORE")
            End If

            If getUFValue(oForm, "IDH_INENDT").Equals("Y") Then
                setParentSharedData(oForm, "AEDATE", getUFValue(oForm, "IDH_ENDDT"))
            Else
                setParentSharedData(oForm, "AEDATE", "IGNORE")
            End If
            ''MA Start 23-02-2015 Issue#256
            If getUFValue(oForm, "IDH_INCSRF").Equals("Y") Then
                setParentSharedData(oForm, "CUSREF", getUFValue(oForm, "IDH_CUSREF"))
            Else
                setParentSharedData(oForm, "CUSREF", "IGNORE")
            End If
            ''MA End 23-02-2015 Issue#256
            'IDH_CUSREF
            If getUFValue(oForm, "IDH_INVERE").Equals("Y") Then
                setParentSharedData(oForm, "REG", getUFValue(oForm, "IDH_VEHREG"))
                setParentSharedData(oForm, "VEHCODE", getUFValue(oForm, "IDH_VEHCD"))
                setParentSharedData(oForm, "VEHDESC", getUFValue(oForm, "IDH_VEHDS"))
                setParentSharedData(oForm, "WCSCD", getUFValue(oForm, "IDH_CARCD"))
                setParentSharedData(oForm, "WCSNM", getUFValue(oForm, "IDH_CARNM"))
                '## Start 12-09-2013; Ask LV if he has any value in Grid for Carrier Ref No (U_SupRef)
                ''Need to test this
                'Dim oData As ArrayList = SRCH.idh.forms.search.AddressSearch.doGetAddress(goParent, getSharedData(oForm, "WCSCD"), "S")
                'If oData IsNot Nothing AndAlso oData.Count > 12 Then
                'setUFValue(oForm, "IDH_CARREF", oData(12))
                'End If
                setParentSharedData(oForm, "WCRFNM", getUFValue(oForm, "IDH_CARREF"))
                '## End
                Dim sOldVehReg As String = getUFValue(oForm, "IDH_OLDVR")
                Dim sNewVehReg As String = getUFValue(oForm, "IDH_VEHREG")
                Dim sOldCarr As String = getUFValue(oForm, "IDH_OLDCC")
                Dim sNewCarr As String = getUFValue(oForm, "IDH_CARCD")

                If sOldVehReg <> "" Then
                    If (sOldVehReg <> sNewVehReg) Then
                        If (sOldCarr <> sNewCarr) Then
                            If Config.INSTANCE.getParameterAsBool("SETVEHDT", False) Then
                                goParent.doMessage("Warning: Changing Vehicle Reg. No., may have changed Carrier details on selected row(s). Make sure you review each WOR and action acordingly. Thank you.")
                            End If
                        End If
                    Else
                        If (sOldCarr <> sNewCarr) Then
                            If Config.INSTANCE.getParameterAsBool("SETVEHDT", False) Then
                                goParent.doMessage("Warning: Changing Vehicle Reg. No., may have changed Carrier details on selected row(s). Make sure you review each WOR and action acordingly. Thank you.")
                            End If
                        End If
                    End If
                End If
            Else
                setParentSharedData(oForm, "REG", "IGNORE")
            End If

            If getUFValue(oForm, "IDH_INDRV").Equals("Y") Then
                setParentSharedData(oForm, "DRIVER", getUFValue(oForm, "IDH_DRIVER"))
            Else
                setParentSharedData(oForm, "DRIVER", "IGNORE")
            End If

            If getUFValue(oForm, "IDH_INRT").Equals("Y") Then
                setParentSharedData(oForm, "ROUTECD", getUFValue(oForm, "IDH_RTCD"))
            Else
                setParentSharedData(oForm, "ROUTECD", "IGNORE")
            End If

            If getUFValue(oForm, "IDH_INCN").Equals("Y") Then
                setParentSharedData(oForm, "CONNO", getUFValue(oForm, "IDH_CONNO"))
            Else
                setParentSharedData(oForm, "CONNO", "IGNORE")
            End If

            doReturnFromModalShared(oForm, True)
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
				If doValidate(oForm) Then
                    If goParent.doCheckModal(oForm.UniqueID) = True Then
                        doPrepareModalData(oForm)
                    End If
                Else
                    BubbleEvent = False
                End If
            End If
        End Sub

		Private Function doValidate(oForm As SAPbouiCOM.Form) As Boolean
            doValidate = True
            ''##MA Start 03-09-2014 Issue#423
            '*** Check for a vehicle if start date is provided
            If Config.INSTANCE.getParameterAsBool("VALIDVEH", True) = True Then
                Dim sASDate As String = getUFValue(oForm, "IDH_STARDT")
                Dim sVehicle As String = getUFValue(oForm, "IDH_VEHREG")
                If getUFValue(oForm, "IDH_INSTDT").Equals("Y") Then
                    If sASDate.Trim <> "" AndAlso sVehicle.Trim = "" Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Vehicle must be enter to start WOR")
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Vehicle must be enter to start WOR", "ERUSEVEH", {Nothing})
                        doSetFocus(oForm, "IDH_VEHREG")
                        Return False
                    End If
                End If
            End If
            ''##MA End 03-09-2014 Issue#423
        End Function
        '** The ItemEvent handler
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = True Then
                    If pVal.ItemChanged Then
                        If pVal.ItemUID.Equals("IDH_REQDAT") Then
                            setUFValue(oForm, "IDH_INRQDT", "Y")
                        ElseIf pVal.ItemUID.Equals("IDH_REQTF") Then
                            setUFValue(oForm, "IDH_INRQTF", "Y")
                        ElseIf pVal.ItemUID.Equals("IDH_REQTT") Then
                            setUFValue(oForm, "IDH_INRQTT", "Y")
                        ElseIf pVal.ItemUID.Equals("IDH_STARDT") Then
                            setUFValue(oForm, "IDH_INSTDT", "Y")
                        ElseIf pVal.ItemUID.Equals("IDH_ENDDT") Then
                            setUFValue(oForm, "IDH_INENDT", "Y")
                        ElseIf pVal.ItemUID.Equals("IDH_VEHREG") Then
                            Dim sValue As String
                            sValue = getUFValue(oForm, "IDH_VEHREG")
                            sValue = com.idh.bridge.utils.General.doFixReg(sValue)
                            setSharedData(oForm, "IDH_VEHREG", sValue)
                            setSharedData(oForm, "IDH_WR1OT", "^WO")
                            setSharedData(oForm, "SHOWACTIVITY", Config.Parameter("WOSVAC"))
                            goParent.doOpenModalForm("IDHLOSCH", oForm)
                            ''MA Start 23-02-2015 Issue#256
                        ElseIf pVal.ItemUID.Equals("IDH_CUSREF") Then
                            Dim sCustRef As String = ""
                            sCustRef = getUFValue(oForm, "IDH_CUSREF")
                            If sCustRef IsNot Nothing AndAlso (sCustRef.Trim.IndexOf("*") > -1) Then
                                doChooseCustomerRef(oForm)
                            Else
                                setUFValue(oForm, "IDH_INCSRF", "Y")
                            End If
                            ''MA Start 23-02-2015 Issue#256
                        ElseIf pVal.ItemUID.Equals("IDH_DRIVER") Then
                            setUFValue(oForm, "IDH_INDRV", "Y")
                        ElseIf pVal.ItemUID.Equals("IDH_RTCD") Then
                            setUFValue(oForm, "IDH_INRT", "Y")
                        ElseIf pVal.ItemUID.Equals("IDH_CONNO") Then
                            setUFValue(oForm, "IDH_INCN", "Y")
                        End If
                    End If
                End If
            End If
            Return True
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType = "IDHLOSCH" Then
                    setUFValue(oForm, "IDH_VEHREG", getSharedData(oForm, "REG"))
                    setUFValue(oForm, "IDH_INVERE", "Y")
                    setUFValue(oForm, "IDH_DRIVER", getSharedData(oForm, "DRIVER"))
                    setUFValue(oForm, "IDH_INDRV", "Y")
                    'Set Carrier Details 
                    setUFValue(oForm, "IDH_VEHCD", getSharedData(oForm, "VEHCODE"))
                    setUFValue(oForm, "IDH_VEHDS", getSharedData(oForm, "VEHDESC"))
					'## Start 12-09-2013; Ask LV if he has any value in Grid for Carrier Ref No (U_SupRef)
                    Dim oData As ArrayList = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, getSharedData(oForm, "WCSCD"), "S")
                    If oData IsNot Nothing AndAlso oData.Count > 12 Then
                        setUFValue(oForm, "IDH_CARREF", oData(12))
                    End If
                    '## End
                    setUFValue(oForm, "IDH_CARCD", getSharedData(oForm, "WCSCD"))
                    setUFValue(oForm, "IDH_CARNM", getSharedData(oForm, "WCSNAM"))
                    ''MA Start 23-02-2015 Issue#256
                ElseIf sModalFormType.Equals("IDHCREF") Then
                      Dim sVal As String = getSharedData(oForm, "CUSTREF")
                    setUFValue(oForm, "IDH_CUSREF", sVal)
                    setUFValue(oForm, "IDH_INCSRF", "Y")
                    ''MA End 23-02-2015 Issue#256
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal Results - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub

        Protected Overrides Sub doReturnCanceled(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String)
            Try
                If sModalFormType = "IDHLOSCH" Then
                    Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                    setUFValue(oForm, "IDH_VEHREG", "")
                    setUFValue(oForm, "IDH_INVERE", "N")
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error canceling the Modal - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCMR", {sModalFormType})
            End Try
        End Sub

        Public Overrides Sub doClose()
        End Sub
        Private Sub doChooseCustomerRef(ByVal oForm As SAPbouiCOM.Form)
            Dim sCardCode As String = getParentSharedData(oForm, "IDH_CUSTCD")
            Dim sCustAddress As String = getParentSharedData(oForm, "IDH_CUSADD")
            ' Dim sCustRefNo As String = getParentSharedData(oForm, "IDH_CUSREF")

            setSharedData(oForm, "TRG", "CUSREF")
            setSharedData(oForm, "VAL1", sCardCode)
            setSharedData(oForm, "VAL2", sCustAddress)
            setSharedData(oForm, "SILENT", "SHOWMULTI")

            goParent.doOpenModalForm("IDHCREF", oForm)
        End Sub
    End Class
End Namespace
