Imports System.IO
Imports System.Collections
Imports System

'Needed for the Framework controls
Imports IDHAddOns.idh.controls
Imports com.idh.bridge.data
Imports com.idh.bridge
Imports com.idh.dbObjects.User
Imports com.idh.dbObjects.numbers
Imports com.isb.bridge.lookups

Namespace idh.forms
    Public Class Training01
        'This is the Base Form object from the Framework
        Inherits IDHAddOns.idh.forms.Base

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParentMenu As String, ByVal iMenuPosition As Integer)
            'Base.New(oParent, ByVal sType As String, ByVal sParMenu As String, ByVal iMenuPosition As Integer, ByVal sSRF As String, ByVal bDoCache As Boolean, ByVal bDoReloadData As Boolean, ByVal bOnlyOne As Boolean, ByVal sTitle As String, Optional ByVal iLoadType As load_Types = load_Types.idh_LOAD_NORMAL)
            'oParent - the Addon Controller
            'ByVal oParent As idh.addon.Base	- Addon Controller
            'ByVal sType As String				- Forms Type ID
            'ByVal sParMenu As String			- Parent Menu ID
            'ByVal iMenuPosition As Integer		- Menu position under the nominated Menu item
            'ByVal sSRF As String				- SRF file for this form
            'ByVal bDoCache As Boolean			- Cache the form into a xml file
            'ByVal bDoLoadData As Boolean		- Activate the Dataloading on startup
            'ByVal bOnlyOne As Boolean			- Load only one instance of the form at a time
            'ByVal sTitle As String				- Form and Menu title
            'ByVal iLoadType As load_Types 		- Form Memory caching option:
            '								            idh_LOAD_NORMAL = 0 'The form does not get loaded into memeory
            '											idh_LOAD_PRE = 1	'Loads the form into memory at AddOn startup
            '											idh_LOAD_POST = 2	'Loads the form into memory after all the data has been loaded and the form was made visible
            '											idh_LOAD_KEEP = 3 	'Keep in memory after the form is closed
            '
            MyBase.New(oParent, "IDH_Train01", sParentMenu, iMenuPosition, "training01_1.srf", False, True, False, "Training Form VB", load_Types.idh_LOAD_NORMAL)

            ''Add the item to the list of items to get their filepaths fixed.
            'doAddImagesToFix("IDHBPCFL")

            'Sets the History table and enables the ChangeLog Menu option
            'This will also Add the DBDataSource when the form gets loaded
            doEnableHistory("@IDH_TRAIN01")
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            'OLD - WAY - this will automatically be done when the Form History or Main table was set in the contructor
            'oForm.DataSources.DBDataSources.Add("@IDH_TRAIN01")

            'Dim oItem As SAPbouiCOM.Item
            'Dim oEdit As SAPbouiCOM.EditText
            'oItem = oForm.Items.Item("IDHCardCod")
            'oEdit = oItem.Specific
            'oEdit.DataBind.SetBound(True, "@IDH_TRAIN01", "U_CardCode")

            'Using a table other than the Form's main table        	
            'doAddDF(oForm, "@IDH_TRAIN01", "IDHCode", "Code")
            'doAddDF(oForm, "@IDH_TRAIN01", "IDHCardCod", "U_CardCode")
            'doAddDF(oForm, "@IDH_TRAIN01", "IDHCardNam", "U_CardName")
            'doAddDF(oForm, "@IDH_TRAIN01", "IDHInv", "U_InvID")
            'doAddDF(oForm, "@IDH_TRAIN01", "IDHInvDate", "U_InvDate")
            'doAddDF(oForm, "@IDH_TRAIN01", "IDHTotal", "U_DocTotal")

            'Using the Form's main table
            doAddFormDF(oForm, "IDHCode", "Code")
            doAddFormDF(oForm, "IDHCardCod", "U_CardCode")
            doAddFormDF(oForm, "IDHCardNam", "U_CardName")
            doAddFormDF(oForm, "IDHInv", "U_InvID")
            doAddFormDF(oForm, "IDHInvDate", "U_InvDate")
            doAddFormDF(oForm, "IDHTotal", "U_DocTotal")

            'oForm.DataSources.UserDataSources.Add("DS01", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30)
            'Dim oEdit As SAPbouiCOM.EditText
            'oItem = oForm.Items.Item("IDHCardCod")
            'oEdit = oItem.Specific
            'oEdit.DataBind.SetBound(True, "", "DS01")

            'doAddUF(oForm, "IDHCardCod", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)

            'doAddUFCheck(oForm, "IDH_ASNEW", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")

            'Dim oRadio As SAPbouiCOM.OptionBtn
            'oRadio = oForm.Items.Item("IDH_APPROV").Specific
            'oRadio.DataBind.SetBound(True, "", "U_APPR")

            'oRadio = oForm.Items.Item("IDH_DECLIN").Specific
            'oRadio.DataBind.SetBound(True, "", "U_DECL")
            'oRadio.GroupWith("IDH_APPROV")

            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
            End If
            oGridN.getSBOItem().AffectsFormMode = True

            oForm.DataBrowser.BrowseBy = "IDHCode"
            oForm.SupportedModes = -1
            oForm.AutoManaged = True

            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
            End If
            doSetFilterFields(oGridN)
            doSetListFields(oGridN)

            'oGridN.setTableValue("[@IDH_TRAIN01_LNS]")
            'oGridN.setKeyField("Code")
            oGridN.doAddGridTable(New GridTable("@IDH_TRAIN01_LNS", Nothing, "Code", True, True), True)

            oGridN.doSetDoCount(True)
            oGridN.doSetHistory("@IDH_TRAIN01_LNS", "Code")
        End Sub

        Protected Overridable Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            'oGridN.doAddFilterField("IDHCode", "Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30 )

            '** Can Optionally set a default value **/
            'oGridN.doAddFilterField("IDHCode", "Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "XYZ")
        End Sub

        Protected Overridable Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", True, 20, Nothing, Nothing)
            oGridN.doAddListField("U_HDRCode", "HDRCode", True, 20, Nothing, Nothing)
            oGridN.doAddListField("U_ItemCode", "Item Code", True, 50, Nothing, Nothing)
            oGridN.doAddListField("U_ItemDesc", "Description", True, 150, Nothing, Nothing)
            oGridN.doAddListField("U_QTY", "QTY", True, 20, Nothing, Nothing)
            oGridN.doAddListField("U_Price", "Price", True, 20, Nothing, Nothing)
            oGridN.doAddListField("U_Total", "Total", True, 20, Nothing, Nothing)
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            Dim oGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            '        	oGrid.doReloadData()

            'Using a table other than the Form's main table        	
            'Dim sCode As String = getDFValue(oForm, "@IDH_TRAIN01", "Code")

            'Using the Form's main table
            Dim sCode As String = getFormDFValue(oForm, "Code")
            oGrid.setRequiredFilter("U_HDRCode = '" & sCode & "'")
            oGrid.doReloadData()
        End Sub

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCloseForm(oForm, BubbleEvent)
        End Sub

        Public Overrides Sub doClose()
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form,
                ByRef pVal As SAPbouiCOM.ItemEvent,
                ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse
                   oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    If doUpdateHeader(oForm) Then
                        Dim oGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                        If oGrid.doProcessData() Then
                        End If

                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        BubbleEvent = False
                    End If
                End If
            End If
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID.Equals("IDHBPCFL") Then
                        doChooseCustomer(oForm)
                    End If
                End If
            End If
            Return False
        End Function

        'OPENING MODAL MOVING DATA UP
        Protected Overridable Sub doChooseCustomer(ByVal oForm As SAPbouiCOM.Form)
            'Using a table other than the Form's main table        	
            'dim sCardCode as String = getDFValue(oForm, "@IDH_TRAIN01", "U_CardCode")

            'Using the Form's main table
            Dim sCardCode As String = getFormDFValue(oForm, "U_CardCode")
            setSharedData(oForm, "IDH_BPCOD", sCardCode)
            'goParent.doOpenModalForm("IDHCSRCH", oForm)
            '            goParent.doOpenModalForm("134", oForm)
            '            goParent.doOpenModalForm("150", oForm)
            setSharedData(oForm, "IDH_WOH", "Louis")
            goParent.doOpenModalForm("651", oForm)

            '            Dim oForm2 As IDHAddOns.idh.forms.Base = goParent.doGetForm("150")
            '            Dim owForm As SAPbouiCOM.Form
            '            If Not oForm2 Is Nothing Then
            '                owForm = IDHAddOns.idh.addon.Base.APPLICATION.Forms.Add("150", 1)
            '                oForm2.doInternalLoad(owForm, True)
            '                oForm2.doAddFormToList(owForm)
            '                'oForm2.doShowForm(BubbleEvent)
            '            End If
            '            goParent.doOpenModalForm("150", oForm)

        End Sub

        'CLOSING MODAL RECEIVING DATA
        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form,
            ByVal sModalFormType As String,
            Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType.Equals("IDHCSRCH") Then
                    Dim sCardCode, sCardName As String
                    sCardCode = getSharedData(oForm, "CARDCODE")
                    sCardName = getSharedData(oForm, "CARDNAME")

                    'Using a table other than the Form's main table        	
                    'setDFValue(oForm,"@IDH_TRAIN01","U_CardCode",sCardCode,True)
                    'setDFValue(oForm,"@IDH_TRAIN01","U_CardName",sCardName,True)

                    'Using the form's main table
                    setFormDFValue(oForm, "U_CardCode", sCardCode, True)
                    setFormDFValue(oForm, "U_CardName", sCardName, True)
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal result - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub

        Protected Function doUpdateHeader(ByVal oForm As SAPbouiCOM.Form) As Boolean
            'This will now Automatically update all the Form's Main table fields and 
            'generate and set the Code value if the code was not set
            Return doAutoSave(oForm, IDH_TRAIN01.AUTONUMPREFIX, "TRN1KEY")
        End Function

        '        Protected Function doUpdateHeader(ByVal oForm As SAPbouiCOM.Form) As Boolean
        '            Dim iwResult As Integer = 0
        '            Dim swResult As String = Nothing
        '
        '            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(goParent, "@" & getHistoryTable())
        '            Try
        '            	dim sCode as String = getDFValue(oform,"@IDH_TRAIN01","Code")
        '				If sCode.Length > 0 Then
        '					oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE)
        '				Else
        '					sCode = goParent.goDB.doNextNumber("TRN1KEY")
        '					oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
        '					oAuditObj.setName(sCode)
        '				End If
        '			
        '				oAuditObj.setFieldValue("U_CardCode", getDFValue(oform,"@IDH_TRAIN01","U_CardCode"))
        '				oAuditObj.setFieldValue("U_CardName", getDFValue(oform,"@IDH_TRAIN01","U_CardName"))
        '        		oAuditObj.setFieldValue("U_InvID", getDFValue(oForm, "@IDH_TRAIN01", "U_InvID"))
        '        		oAuditObj.setFieldValue("U_InvDate", getDFValue(oForm, "@IDH_TRAIN01", "U_InvDate"))
        '        		oAuditObj.setFieldValue("U_DocTotal", getDFValue(oForm, "@IDH_TRAIN01", "U_DocTotal"))
        '				
        '				iwResult = oAuditObj.Commit()
        '				
        '				If iwResult <> 0 Then
        '					goParent.goDICompany.GetLastError(iwResult, swResult)
        '					com.idh.bridge.DataHandler.INSTANCE.doError("doUpdateHeader", "Error Adding: " + swResult, "")
        '					Return False
        '				End If
        '				
        '				Return True
        '            Catch ex As Exception
        '               com.idh.bridge.DataHandler.INSTANCE.doError("doUpdateHeader", "Exception: " & ex.ToString, "")
        '                Return False
        '            Finally
        '                IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj)
        '            End Try
        '        End Function

        '** The Menu Event handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                If pVal.MenuUID = Config.NAV_ADD Then
                    oForm.Freeze(True)
                    Try
                        doClearAll(oForm, True)
                        doLoadData(oForm)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Menu Event - " & oForm.UniqueID & "." & pVal.MenuUID)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try
                ElseIf pVal.MenuUID = Config.NAV_FIND OrElse
                       pVal.MenuUID = Config.NAV_FIRST OrElse
                       pVal.MenuUID = Config.NAV_LAST OrElse
                       pVal.MenuUID = Config.NAV_NEXT OrElse
                       pVal.MenuUID = Config.NAV_PREV Then
                    oForm.Freeze(True)
                    Try
                        If pVal.MenuUID = Config.NAV_FIND Then
                            doClearAll(oForm, False)
                            doLoadData(oForm)
                        Else
                            doLoadData(oForm)
                        End If
                    Catch ex As Exception
                    Finally
                        oForm.Freeze(False)
                    End Try
                End If
            End If
            Return False
        End Function

        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY Then
                Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, "TRN1KEYL")
                oGridN.doSetFieldValue("Code", pVal.Row, oNumbers.CodeCode, True)
                oGridN.doSetFieldValue("Name", pVal.Row, oNumbers.NameCode, True)

                'Using a table other than the Form's main table        	
                'Dim sCode As String = getDFValue(oForm, "@IDH_TRAIN01", "Code" )

                'Using the Form's Main Table
                Dim sCode As String = getFormDFValue(oForm, "Code")
                oGridN.doSetFieldValue("U_HDRCode", pVal.Row, sCode, True)
            End If
            Return MyBase.doCustomItemEvent(oForm, pVal)
        End Function

        Private Sub doClearAll(ByVal oForm As SAPbouiCOM.Form, ByVal bDoNewCode As Boolean)
            'Using a table other than the Form's main table        	
            'setDFValue(oForm, "@IDH_TRAIN01", "U_CardCode", "")
            'setDFValue(oForm, "@IDH_TRAIN01", "U_CardName", "")
            'setDFValue(oForm, "@IDH_TRAIN01", "U_InvID", "")
            'setDFValue(oForm, "@IDH_TRAIN01", "U_InvDate", "")
            'setDFValue(oForm, "@IDH_TRAIN01", "U_DocTotal", "")

            'Using the Form's main table
            setFormDFValue(oForm, "U_CardCode", "")
            setFormDFValue(oForm, "U_CardName", "")
            setFormDFValue(oForm, "U_InvID", "")
            setFormDFValue(oForm, "U_InvDate", "")
            setFormDFValue(oForm, "U_DocTotal", "")

            Dim sCode As String = ""
            If bDoNewCode = True Then
                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(IDH_TRAIN01.AUTONUMPREFIX, "TRN1KEY")

                'Using the Form's main table
                setFormDFValue(oForm, "Code", oNumbers.CodeCode)
                setFormDFValue(oForm, "Name", oNumbers.NameCode)
            Else
                'Using the Form's main table
                setFormDFValue(oForm, "Code", sCode)
                setFormDFValue(oForm, "Name", sCode)
            End If

            'Using a table other than the Form's main table        	
            'setDFValue(oForm, "@IDH_TRAIN01", "Code", sCode)    		

        End Sub

    End Class
End Namespace
