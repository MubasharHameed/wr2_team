﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge.lookups
Imports com.idh.bridge
Imports com.idh.utils

Namespace idh.forms
    Public Class FormulaEditor
        Inherits IDHAddOns.idh.forms.Base

        Private moSelectList As New IDHAddOns.idh.Grid.SelectList

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDH_FORMEDT", Nothing, -1, "FormulaEdit.srf", False, True, False, "Formula Editor", load_Types.idh_LOAD_NORMAL)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                doAddUF(oForm, "IDH_FIELD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100, False, False)
                doAddUF(oForm, "IDH_NUMBER", SAPbouiCOM.BoDataType.dt_QUANTITY, 10, False, False)
                doAddUF(oForm, "IDH_FORMU", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 200, False, False)
                doAddUF(oForm, "IDH_FACTOR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 200, False, False)
                doAddUF(oForm, "IDH_FRMPRS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 200, False, False)

                ''Fields to save OLD Vehicle Reg and Carrier Code to compare and alert users 
                'doAddUF(oForm, "IDH_OLDVR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, False, False)
                'doAddUF(oForm, "IDH_OLDCC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50, False, False)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", Nothing)
            End Try
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            If getHasSharedData(oForm) Then
                Dim sFormula As String = getParentSharedDataAsString(oForm, "IDH_FORMU")
                Dim sFrmParse As String = getParentSharedDataAsString(oForm, "IDH_FRMPRS")

                setUFValue(oForm, "IDH_FORMU", sFormula)
                setUFValue(oForm, "IDH_FRMPRS", sFrmParse)
            End If
            doFillFieldsCombo(oForm)
            doFillFactorCombo(oForm)
        End Sub

        Public Sub doFillFieldsCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oCombo As SAPbouiCOM.ComboBox
            Dim oItem As SAPbouiCOM.Item
            Try
                oItem = oForm.Items.Item("IDH_FIELD")
                oCombo = CType(oItem.Specific, SAPbouiCOM.ComboBox)
            Catch ex As Exception
                Exit Sub
            End Try
            oItem.DisplayDesc = True
            doClearValidValues(oCombo.ValidValues)

            Dim sFields As String = Config.Parameter("FORMEDIT")

            Try
                For Each sAR As String In sFields.Split(","c)
                    oCombo.ValidValues.Add(sAR, sAR)
                Next
                'oCombo.ValidValues.Add("U_RdWgt", "U_RdWgt")
                'oCombo.ValidValues.Add("U_CstWgt", "U_CstWgt")
                'oCombo.ValidValues.Add("U_TCharge", "U_TCharge")
                'oCombo.ValidValues.Add("U_AUOMQt", "U_AUOMQt")
                'oCombo.ValidValues.Add("U_AddCharge", "U_AddCharge")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Formula Fields combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Formula Fields")})
            Finally
            End Try


        End Sub

        Public Sub doFillFactorCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oCombo As SAPbouiCOM.ComboBox
            Dim oItem As SAPbouiCOM.Item
            Try
                oItem = oForm.Items.Item("IDH_FACTOR")
                oCombo = CType(oItem.Specific, SAPbouiCOM.ComboBox)
            Catch ex As Exception
                Exit Sub
            End Try
            oItem.DisplayDesc = True
            doClearValidValues(oCombo.ValidValues)

            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDB.doSelectQuery("SELECT U_FactorNM, U_FactorPC FROM [@IDH_BPACFACTOR]")
                While Not oRecordSet.EoF
                    oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item("U_FactorNM").Value, String), Conversions.ToString(oRecordSet.Fields.Item("U_FactorNM").Value))
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Type combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Type")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

        '*** Get the selected rows
        Protected Sub doPrepareModalData(ByVal oForm As SAPbouiCOM.Form, Optional ByVal iSel As Integer = -1)
            Dim sFormula As String = Conversions.ToString(getUFValue(oForm, "IDH_FORMU"))
            Dim sFormParse As String = Conversions.ToString(getUFValue(oForm, "IDH_FRMPRS"))

            setParentSharedData(oForm, "FORMULA", sFormula)
            setParentSharedData(oForm, "FRMPARS", sFormParse)

            doReturnFromModalShared(oForm, True)
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If goParent.doCheckModal(oForm.UniqueID) = True Then
                    doPrepareModalData(oForm)
                    BubbleEvent = False
                End If
            End If
        End Sub

        Private Function doValidate(oForm As SAPbouiCOM.Form) As Boolean
            doValidate = True
            ''##MA Start 03-09-2014 Issue#423
            '*** Check for a vehicle if start date is provided
            If Config.INSTANCE.getParameterAsBool("VALIDVEH", True) = True Then
                Dim sASDate As String = Conversions.ToString(getUFValue(oForm, "IDH_STARDT"))
                Dim sVehicle As String = Conversions.ToString(getUFValue(oForm, "IDH_VEHREG"))
                If getUFValue(oForm, "IDH_INSTDT").Equals("Y") Then
                    If sASDate.Trim <> "" AndAlso sVehicle.Trim = "" Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Vehicle must be enter to start WOR")
                        DataHandler.INSTANCE.doResUserError("Vehicle must be enter to start WOR", "ERUSEVEH", Nothing)
                        doSetFocus(oForm, "IDH_VEHREG")
                        Return False
                    End If
                End If
            End If
            ''##MA End 03-09-2014 Issue#423
        End Function

        '** The ItemEvent handler
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemChanged Then
                        If pVal.ItemUID = "IDH_FIELD" Then
                            Dim sField As String = Conversions.ToString(getUFValue(oForm, "IDH_FIELD"))
                            Dim sFormula As String = Conversions.ToString(getUFValue(oForm, "IDH_FORMU"))
                            Dim sFormulaParse As String = Conversions.ToString(getUFValue(oForm, "IDH_FRMPRS"))

                            setUFValue(oForm, "IDH_FORMU", sFormula + sField)
                            setUFValue(oForm, "IDH_FRMPRS", sFormulaParse + sField + "#")
                        ElseIf pVal.ItemUID = "IDH_FACTOR" Then
                            Dim sFactor As String = Conversions.ToString(getUFValue(oForm, "IDH_FACTOR"))
                            Dim sFormula As String = Conversions.ToString(getUFValue(oForm, "IDH_FORMU"))
                            Dim sFormulaParse As String = Conversions.ToString(getUFValue(oForm, "IDH_FRMPRS"))

                            setUFValue(oForm, "IDH_FORMU", sFormula + sFactor)
                            setUFValue(oForm, "IDH_FRMPRS", sFormulaParse + "~" + sFactor + "#")
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = True Then
                    If pVal.ItemChanged Then
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_BOPEN" OrElse pVal.ItemUID = "IDH_PLUS" OrElse pVal.ItemUID = "IDH_MINUS" OrElse pVal.ItemUID = "IDH_MULTI" OrElse pVal.ItemUID = "IDH_DIV" OrElse pVal.ItemUID = "IDH_BCLOSE" Then
                        Dim sFormula As String = Conversions.ToString(getUFValue(oForm, "IDH_FORMU"))
                        Dim sFormulaParse As String = Conversions.ToString(getUFValue(oForm, "IDH_FRMPRS"))
                        setUFValue(oForm, "IDH_FORMU", sFormula + oForm.Items.Item(pVal.ItemUID).Specific.Caption)
                        setUFValue(oForm, "IDH_FRMPRS", sFormulaParse + oForm.Items.Item(pVal.ItemUID).Specific.Caption + "#")
                    ElseIf pVal.ItemUID = "IDH_ADDNUM" Then
                        Dim sNumValue As String = Conversions.ToString(getUFValue(oForm, "IDH_NUMBER"))
                        Dim sFormula As String = Conversions.ToString(getUFValue(oForm, "IDH_FORMU"))
                        Dim sFormulaParse As String = Conversions.ToString(getUFValue(oForm, "IDH_FRMPRS"))
                        setUFValue(oForm, "IDH_FORMU", sFormula + sNumValue)
                        setUFValue(oForm, "IDH_FRMPRS", sFormulaParse + sNumValue + "#")
                        setUFValue(oForm, "IDH_NUMBER", 0)
                    ElseIf pVal.ItemUID = "IDH_BACK" Then
                        Dim sFormula As String = Conversions.ToString(getUFValue(oForm, "IDH_FORMU"))
                        Dim sFormulaParse As String = Conversions.ToString(getUFValue(oForm, "IDH_FRMPRS"))
                        Dim sNewFormulaParse As String = String.Empty

                        If sFormula.Length > 0 Then
                            Dim sSplitF As Array = sFormulaParse.Split("#"c).ToArray()
                            If sSplitF.Length = 2 Then
                                '1 token 
                                sNewFormulaParse = ""
                            ElseIf sSplitF.Length = 3 Then
                                '2 token
                                sNewFormulaParse = sSplitF(0).ToString() + "#"
                            ElseIf sSplitF.Length > 3 Then
                                'more than 2 
                                For i As Int16 = 0 To sSplitF.Length - 3
                                    'sFormula = sFormula + sSplitF(i).ToString()
                                    'sFormulaParse = sFormulaParse + sSplitF(i).ToString() + "#"
                                    If sSplitF(i).ToString().Length > 0 Then
                                        sNewFormulaParse = sNewFormulaParse + sSplitF(i).ToString() + "#"
                                    End If
                                Next
                            End If

                            sFormula = sNewFormulaParse.Replace("#", "")
                            setUFValue(oForm, "IDH_FORMU", sFormula)
                            setUFValue(oForm, "IDH_FRMPRS", sNewFormulaParse)
                        End If
                    ElseIf pVal.ItemUID = "IDH_CLEAR" Then
                        setUFValue(oForm, "IDH_FORMU", "")
                        setUFValue(oForm, "IDH_FRMPRS", "")
                    End If
                End If
            End If
            Return True
        End Function



        Public Overrides Sub doClose()
        End Sub
    End Class
End Namespace
