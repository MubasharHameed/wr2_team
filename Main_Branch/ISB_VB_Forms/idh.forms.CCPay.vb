Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge
Imports com.idh.dbObjects.numbers

Namespace idh.forms
    Public Class CCPay
        Inherits IDHAddOns.idh.forms.Base

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHCCPAY", Nothing, -1, "CCPay.srf", False, True, False, "Creditcard Payment", load_Types.idh_LOAD_NORMAL)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                doAddUF(oForm, "IDH_CUSTCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
                doAddUF(oForm, "IDH_CUSTNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100, False, False)
                doAddUF(oForm, "IDH_AMOUNT", SAPbouiCOM.BoDataType.dt_PRICE, 10, False, False)
                doAddUF(oForm, "IDH_CCNUM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
                doAddUF(oForm, "IDH_CCNMSE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
                doAddUF(oForm, "IDH_CCTYPE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
                doAddUF(oForm, "IDH_CCEXP", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
                doAddUF(oForm, "IDH_CCDEL", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
                doAddUF(oForm, "IDH_CCSTAT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
                doAddUF(oForm, "IDH_IDNUM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
                doAddUF(oForm, "IDH_COMPID", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
                                
                doAddUF(oForm, "IDH_CCIs", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
                doAddUF(oForm, "IDH_CCSec", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
                doAddUF(oForm, "IDH_CCHNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
                doAddUF(oForm, "IDH_CCPCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)

                oForm.DataSources.UserDataSources.Add("U_APPR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                oForm.DataSources.UserDataSources.Add("U_DECL", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                
                doAddUFCheck(oForm, "IDH_ASNEW", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")

                Dim oRadio As SAPbouiCOM.OptionBtn
                oRadio = oForm.Items.Item("IDH_APPROV").Specific
                oRadio.DataBind.SetBound(True, "", "U_APPR")

                oRadio = oForm.Items.Item("IDH_DECLIN").Specific
                oRadio.DataBind.SetBound(True, "", "U_DECL")
                oRadio.GroupWith("IDH_APPROV")

                setUFValue(oForm, "U_APPR", "1")
                setUFValue(oForm, "U_DECL", "0")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            If getHasSharedData(oForm) Then
                Dim sCardCode As String = getParentSharedData(oForm, "CUSTCD")
                Dim sCardName As String = getParentSharedData(oForm, "CUSTNM")
                Dim dAmount As Double = getParentSharedData(oForm, "AMOUNT")
                'sAmount = sAmount.Trim()

                setUFValue(oForm, "IDH_CUSTCD", sCardCode)
                setUFValue(oForm, "IDH_CUSTNM", sCardName)
                setUFValue(oForm, "IDH_AMOUNT", dAmount)

                doAddWF(oForm, "CCInfo")
                doAddWF(oForm, "ROWS", getParentSharedData(oForm, "ROWS"))
            End If
            doFillCCNumbers(oForm)
            doFillTypeCombo(oForm)
        End Sub

        Public Sub doFillTypeCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oCombo As SAPbouiCOM.ComboBox
            Dim oItem As SAPbouiCOM.Item
            Try
            	oItem = oForm.Items.Item("IDH_CCTYPE")
                oCombo = oItem.Specific
            Catch ex As Exception
                Exit Sub
            End Try
            oItem.DisplayDesc = True
            doClearValidValues(oCombo.ValidValues)

            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDB.doSelectQuery("SELECT CreditCard, CardName FROM OCRC")
                While Not oRecordSet.EoF
                    oCombo.ValidValues.Add(oRecordSet.Fields.Item(0).Value, oRecordSet.Fields.Item(1).Value)
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Type combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Type")})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
            End Try
        End Sub

        Private Sub doFillCCNumbers(ByVal oForm As SAPbouiCOM.Form)
            Dim sCustomer As String
            sCustomer = getUFValue(oForm, "IDH_CUSTCD")

            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBox
            Dim oItem As SAPbouiCOM.Item
            Try
                oItem = oForm.Items.Item("IDH_CCNMSE")
                oCombo = oItem.Specific
            Catch ex As Exception
                Exit Sub
            End Try
            oItem.DisplayDesc = False

            doClearValidValues(oCombo.ValidValues)

            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Dim hCCs As Hashtable = New Hashtable
            Try
                Dim sQry As String = "select c1.CardCode, c1.CrCardNum, ct.CardName, c1.CardValid, c1.AvrageLate, ct.CompanyId, ct.Phone, ct.AcctCode, c1.OwnerIdNum, c1.U_IDHCCIs, c1.U_IDHCCSec, c1.U_IDHCCHNm, c1.U_IDHCCPCd, ct.CreditCard from OCRD c1, OCRC ct where c1.CreditCard = ct.CreditCard AND c1.CardCode = '" & sCustomer & "' " & _
                                     "UNION " & _
                                     "select c2.U_CardCd, c2.U_CCNum, ct.CardName, c2.U_CCExp, c2.U_CCAvDe, ct.CompanyId, ct.Phone, ct.AcctCode, c2.U_CCId, c2.U_IDHCCIs, c2.U_IDHCCSec, c2.U_IDHCCHNm, c2.U_IDHCCPCd, ct.CreditCard  from [@IDH_CCDTL] c2, OCRC ct where c2.U_CCType = ct.CreditCard AND c2.U_CardCd = '" & sCustomer & "'"

                oRecordSet = goParent.goDB.doSelectQuery(sQry)

                Dim hCCInfo As Hashtable = New Hashtable
                hCCInfo.Add("CardCd", oRecordSet.Fields.Item(0).Value)
                hCCInfo.Add("CCNum", "New")
                hCCInfo.Add("CCTypeCode", "")
                hCCInfo.Add("CCType", "")
                hCCInfo.Add("CCExp", "")
                hCCInfo.Add("CCAvDe", "")
                hCCInfo.Add("CompId", "")
                hCCInfo.Add("APhone", "")
                hCCInfo.Add("CCAcctCd", "")
                hCCInfo.Add("CCId", "")
                
                hCCInfo.Add("CCIs", "")
                hCCInfo.Add("CCSec", "")
                hCCInfo.Add("CCHNm", "")
                hCCInfo.Add("CCPCd", "")
                
                oCombo.ValidValues.Add("New", "")
                hCCs.Add("New", hCCInfo)

                Dim sCCNumber As String
                Dim sCCType As String
                While Not oRecordSet.EoF
                    hCCInfo = New Hashtable
                    sCCNumber = oRecordSet.Fields.Item(1).Value
                    sCCType = oRecordSet.Fields.Item(2).Value

                    hCCInfo.Add("CardCd", oRecordSet.Fields.Item(0).Value)
                    hCCInfo.Add("CCNum", sCCNumber)
                    hCCInfo.Add("CCType", sCCType)
                    hCCInfo.Add("CCExp", oRecordSet.Fields.Item(3).Value)
                    hCCInfo.Add("CCAvDe", oRecordSet.Fields.Item(4).Value)
                    hCCInfo.Add("CompId", oRecordSet.Fields.Item(5).Value)
                    hCCInfo.Add("APhone", oRecordSet.Fields.Item(6).Value)
                    hCCInfo.Add("CCAcctCd", oRecordSet.Fields.Item(7).Value)
                    hCCInfo.Add("CCId", oRecordSet.Fields.Item(8).Value)
                    
                    hCCInfo.Add("CCIs", oRecordSet.Fields.Item(9).Value)
                    hCCInfo.Add("CCSec", oRecordSet.Fields.Item(10).Value)
                    hCCInfo.Add("CCHNm", oRecordSet.Fields.Item(11).Value)
                    hCCInfo.Add("CCPCd", oRecordSet.Fields.Item(12).Value)
                    hCCInfo.Add("CCTypeCode",oRecordSet.Fields.Item(13).Value) 
                    
                    oCombo.ValidValues.Add(sCCNumber, sCCType)
                    oRecordSet.MoveNext()
                    hCCs.Add(sCCNumber, hCCInfo)
                End While
                setWFValue(oForm, "CCInfo", hCCs)

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the CC Numbers Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("CC Numbers")})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
            End Try

        End Sub

        '*** Get the selected rows
        Protected Sub doPrepareModalData(ByVal oForm As SAPbouiCOM.Form)
            Dim sCCNum As String = getUFValue(oForm, "IDH_CCNUM")
			Dim sCCTypeCode As String = getUFValue(oForm, "IDH_CCTYPE")
			
            setParentSharedData(oForm, "ROWS", getWFValue(oForm, "ROWS"))

            setParentSharedData(oForm, "CCNUM", sCCNum)
            setParentSharedData(oForm, "CCTYPE", sCCTypeCode)
            
            setParentSharedData(oForm, "CCEXP", goParent.doDateToStr(getUFValue(oForm, "IDH_CCEXP")))
            setParentSharedData(oForm, "CCDEL", getUFValue(oForm, "IDH_CCDEL"))
            setParentSharedData(oForm, "CCSTAT", getUFValue(oForm, "IDH_CCSTAT"))

            setParentSharedData(oForm, "COMPID", getUFValue(oForm, "IDH_COMPID"))
            setParentSharedData(oForm, "APHONE", oForm.Items.Item("IDH_APTEL").Specific.Caption)
            setParentSharedData(oForm, "CCACCTCD", getWFValue(oForm, "CCACCTCD"))
            setParentSharedData(oForm, "CCID", getUFValue(oForm, "IDH_IDNUM"))
            
            setParentSharedData(oForm, "CCIs", getUFValue(oForm, "IDH_CCIs"))
            setParentSharedData(oForm, "CCSec", getUFValue(oForm, "IDH_CCSec"))
            setParentSharedData(oForm, "CCHNm", getUFValue(oForm, "IDH_CCHNm"))
            setParentSharedData(oForm, "CCPCd", getUFValue(oForm, "IDH_CCPCd"))

            Dim sPayStatus As String = com.idh.bridge.lookups.FixedValues.getStatusUnPaid()
            Dim sAppr As String = getUFValue(oForm, "U_APPR")
            Dim sDecl As String = getUFValue(oForm, "U_DECL")

            If sCCNum.Length() > 0 Then
                If sAppr = "1" Then
                    sPayStatus = com.idh.bridge.lookups.FixedValues.getStatusPaid()
                ElseIf sAppr = "2" Then
                    sPayStatus = com.idh.bridge.lookups.FixedValues.getStatusUnPaid()
                End If
            Else
                sPayStatus = com.idh.bridge.lookups.FixedValues.getStatusUnPaid()
            End If

            setParentSharedData(oForm, "PSTAT", sPayStatus)

            doReturnFromModalShared(oForm, True)
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If goParent.doCheckModal(oForm.UniqueID) = True Then
                	
                	Dim sAsNew As String = getUFValue(oForm, "IDH_ASNEW")
                	If sAsNew.Equals("Y") Then
                        Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode("CCSEQ")

                        Dim sCustCd As String = getUFValue(oForm, "IDH_CUSTCD")
                		Dim sCCTypeCode As String = getUFValue(oForm, "IDH_CCTYPE")
                		Dim sCCNum As String = getUFValue(oForm, "IDH_CCNUM")
                		Dim sCCExp As String = goParent.doDateToStr(getUFValue(oForm, "IDH_CCEXP"))
                		Dim sCCDel As String = getUFValue(oForm, "IDH_CCDEL")
                		Dim sCCID As String = getUFValue(oForm, "IDH_IDNUM")
                		Dim sCCIs As String = getUFValue(oForm, "IDH_CCIs")
            			Dim sCCSec As String = getUFValue(oForm, "IDH_CCSec")
            			Dim sCCHNm As String = getUFValue(oForm, "IDH_CCHNm")
            			Dim sCCPCd As String = getUFValue(oForm, "IDH_CCPCd")
                		
'                		Dim sCCStat As String = getUFValue(oForm, "IDH_CCSTAT")
'                		Dim sCompId As String = getUFValue(oForm, "IDH_COMPID")
'                		Dim sAPhone As String = oForm.Items.Item("IDH_APTEL").Specific.Caption
'                		Dim sCCAcctCD As String = getWFValue(oForm, "CCACCTCD")
            			
            			Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_CCDTL")
            			Dim iwResult As Integer
            			Dim swResult As String = Nothing
            			Try
                            oAuditObj.setKeyPair(oNumbers.CodeCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
                            oAuditObj.setName(oNumbers.NameCode)
           				
                    		oAuditObj.setFieldValue("U_CardCd", sCustCd)
                    		oAuditObj.setFieldValue("U_CCType", sCCTypeCode)
                    		oAuditObj.setFieldValue("U_CCNum", sCCNum)
                    		oAuditObj.setFieldValue("U_CCExp", sCCExp)
                    		oAuditObj.setFieldValue("U_CCAvDe", sCCDel)
	                		oAuditObj.setFieldValue("U_CCId", sCCID)
    	        			oAuditObj.setFieldValue("U_IDHCCIs", sCCIs)
        	    			oAuditObj.setFieldValue("U_IDHCCSec", sCCSec)
            				oAuditObj.setFieldValue("U_IDHCCHNm", sCCHNm)
            				oAuditObj.setFieldValue("U_IDHCCPCd", sCCPCd)
            				            				
            				iwResult = oAuditObj.Commit()
	
	                        If iwResult <> 0 Then
	                            goParent.goDICompany.GetLastError(iwResult, swResult)
                                'com.idh.bridge.DataHandler.INSTANCE.doError("Error Adding the CCPay: " + swResult)
                                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Adding the CCPay: " + swResult, "ERSYACCP", {swResult})
	                        End If
            			Catch ex As Exception
                            'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error processing button 1.")
                            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXBNTO", {Nothing})
            			Finally
                			oAuditObj.Close()
               	 			IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj)
            			End Try
                	End If
                	
                	doPrepareModalData(oForm)
                    BubbleEvent = False
                End If
            End If
        End Sub

        '** The ItemEvent handler
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_CCNMSE" Then
                        Dim sCCNum As String = getUFValue(oForm, "IDH_CCNMSE")
                        If Not sCCNum Is Nothing AndAlso sCCNum.Length() > 0 Then
                            Dim hCCs As Hashtable = getWFValue(oForm, "CCInfo")
                            Dim hCCInfo As Hashtable
                            If Not hCCs Is Nothing AndAlso hCCs.ContainsKey(sCCNum) Then
                                hCCInfo = hCCs.Item(sCCNum)
                                setUFValue(oForm, "IDH_CCTYPE", hCCInfo.Item("CCTypeCode"))
                                setUFValue(oForm, "IDH_CCEXP", hCCInfo.Item("CCExp"))
                                setUFValue(oForm, "IDH_CCDEL", hCCInfo.Item("CCAvDe"))
                                setUFValue(oForm, "IDH_COMPID", hCCInfo.Item("CompId"))
                                
                                setUFValue(oForm, "IDH_CCIs", hCCInfo.Item("CCIs"))
                                setUFValue(oForm, "IDH_CCSec", hCCInfo.Item("CCSec"))
                                setUFValue(oForm, "IDH_CCHNm", hCCInfo.Item("CCHNm"))
                                setUFValue(oForm, "IDH_CCPCd", hCCInfo.Item("CCPCd"))
                                
                                oForm.Items.Item("IDH_APTEL").Specific.Caption = hCCInfo.Item("APhone")
                                setWFValue(oForm, "CCACCTCD", hCCInfo.Item("CCAcctCd"))
                                setUFValue(oForm, "IDH_IDNUM", hCCInfo.Item("CCId"))
                                
                                setUFValue(oForm, "IDH_ASNEW", "N")
                                setEnableItem(oForm, False, "IDH_ASNEW")
                                setUFValue(oForm, "IDH_CCNUM", sCCNum)
                            End If
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
            	If pVal.BeforeAction = False Then
            		If pVal.ItemUID = "IDH_CCNUM" Then
            			setEnableItem(oForm, True, "IDH_ASNEW")
            		End If
            	End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_APPROV" OrElse pVal.ItemUID = "IDH_DECLIN" Then
                        Dim sAppr As String = getUFValue(oForm, "U_APPR")
                        If sAppr = "1" Then
                            oForm.Items.Item("IDH_CCSTLB").Specific.Caption = getTranslatedWord("Voucher No.")
                        ElseIf sAppr = "2" Then
                            oForm.Items.Item("IDH_CCSTLB").Specific.Caption = getTranslatedWord("Reason")
                        End If
                    End If
                End If
            End If
            Return True
        End Function

        Public Overrides Sub doClose()
        End Sub

    End Class
End Namespace
