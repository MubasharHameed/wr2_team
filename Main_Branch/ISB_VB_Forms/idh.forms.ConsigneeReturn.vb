Imports System.IO
Imports System.Collections
Imports System
Imports com.isb.bridge.lookups

Namespace idh.forms
    Public Class ConsigneeReturn
        Inherits IDHAddOns.idh.forms.Base

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            'MyBase.New(oParent, "IDH_CONSRET", iMenuPosition, "Consignee Return.srf", False, True, False)
            MyBase.New(oParent, "IDH_CONSRET", "IDHRE", iMenuPosition, "Consignee Return.srf", False, True, False, "Consignee Return", load_Types.idh_LOAD_NORMAL)
        End Sub

        '        '*** Create Sub-Menu
        '        Protected Overrides Sub doCreateSubMenu()
        '            Dim oMenus As SAPbouiCOM.Menus
        '            Dim oMenuItem As SAPbouiCOM.MenuItem
        '            Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
        '
        '            oMenuItem = goParent.goApplication.Menus.Item("IDHRE")
        '
        '            oMenus = oMenuItem.SubMenus
        '            oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        '            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING
        '            oCreationPackage.UniqueID = gsType
        '            oCreationPackage.String = "Consignee Return"
        '            oCreationPackage.Position = giMenuPosition
        '
        '            Try
        '                oMenus.AddEx(oCreationPackage)
        '            Catch ex As Exception
        '            End Try
        '        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
        End Sub

        '** Do the final actions to show the form
        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            oForm.Visible = True
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        Public Sub doLoadLines(ByVal oForm As SAPbouiCOM.Form)
            Dim sPart As String = oForm.Items.Item("IDH_PARTCD").Specific.Value.trim()
            Dim oMatrix As SAPbouiCOM.Matrix
            If sPart.Length > 0 Then
                Dim oConditions As SAPbouiCOM.Conditions
                Dim oCondition As SAPbouiCOM.Condition
                oConditions = New SAPbouiCOM.Conditions

                oCondition = oConditions.Add
                oCondition.Alias = "U_DOCPRTCD"
                oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                oCondition.CondVal = sPart
                oForm.DataSources.DBDataSources.Add("@IDH_CUSTITPR").Query(oConditions)

                oMatrix = oForm.Items.Item("IDH_MATRIX").Specific
                oMatrix.LoadFromDataSource()
                If oMatrix.VisualRowCount > 0 Then
                    ' doFillItmGrpCombo(oForm)
                End If
            End If
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                   oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                End If
            Else
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    oForm.Freeze(True)
                    doLoadLines(oForm)
                    oForm.Freeze(False)
                End If
            End If
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then

                End If
            End If
            Return False
        End Function

        '** Create a new Entry if the Add option is selected
        Private Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        '** Clears all the fields when going into the find mode.
        Private Sub doClearAll(ByVal oForm As SAPbouiCOM.Form)
        End Sub


        '** The Menu Event handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                If pVal.MenuUID = Config.NAV_ADD Then
                    oForm.Freeze(True)
                    Try
                        doCreateNewEntry(oForm)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing the menu event - " & oForm.UniqueID & "." & pVal.MenuUID)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try
                ElseIf pVal.MenuUID = Config.NAV_FIND OrElse
                       pVal.MenuUID = Config.NAV_FIRST OrElse
                       pVal.MenuUID = Config.NAV_LAST OrElse
                       pVal.MenuUID = Config.NAV_NEXT OrElse
                       pVal.MenuUID = Config.NAV_PREV Then
                    oForm.Freeze(True)
                    Try
                        If pVal.MenuUID = Config.NAV_FIND Then
                            doClearAll(oForm)
                        Else
                            doLoadData(oForm)
                        End If
                    Catch ex As Exception
                    Finally
                        oForm.Freeze(False)
                    End Try
                End If
            End If
            Return True
        End Function

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCloseForm(oForm, BubbleEvent)
        End Sub

        Public Overrides Sub doClose()
        End Sub

        '** This method is called by the returning modal form to set the data before closing
        Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal results " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub

        ''MODAL DIALOG
        ''*** Set the return data when called as a modal dialog 
        Private Sub doSetModalData(ByVal oForm As SAPbouiCOM.Form)
            Dim sMode As String = ""
            If ghOldDialogParams.Contains(oForm.UniqueID) Then
                Dim oData As ArrayList = ghOldDialogParams.Item(oForm.UniqueID)
                If Not (oData Is Nothing) AndAlso oData.Count > 2 Then
                    sMode = oData.Item(1)
                End If
            End If

            Dim aData As New ArrayList
            If sMode.Length = 0 Then
                sMode = "Add"
            Else
                sMode = "Change"
            End If

            With oForm.DataSources.DBDataSources.Item("@IDH_JOBENTR")
                aData.Add(sMode)
                aData.Add("")
            End With

            doReturnFromModalBuffered(oForm, aData)
        End Sub
    End Class
End Namespace
