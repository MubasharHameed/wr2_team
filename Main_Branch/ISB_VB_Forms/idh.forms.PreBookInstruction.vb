'
' Created by SharpDevelop.
' User: Tjaard
' Date: 07/05/2009
' Time: 08:21 AM
'
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System
Imports com.idh.utils
Imports com.idh.bridge
Imports com.idh.bridge.lookups
Imports com.idh.dbObjects.User
Imports WR1_PBI
Imports com.uBC.utils
Imports SAPbouiCOM

Namespace idh.forms
	
    Public Class PreBookInstruction
        Inherits IDHAddOns.idh.forms.Base
        'Private moWOM As IDH_WOM.IDH_PBI_Handler
        Shared addressLine As String = ""
        Public bRouting As Boolean
        Private addrType As String = ""

        Public Property AddrType1 As String
            Get
                Return addrType
            End Get
            Set(value As String)
                addrType = value
            End Set
        End Property




        Shared moEditableHeaderFields() As String = {
            "IDH_CUSREF", "IDH_OBLED", "IDHCARCD", "IDHCRCFL",
            "IDHDISCD", "IDHDSCFL", "IDHDADDR", "IDHADDFL",
            "IDHCOGRP", "IDHCOICD", "IDHJTYPE", "IDHJCFL",
            "IDHWCICD", "IDHWTCFL"}
        '##MA Start 07-02-2017
        Private doHandleDuplicate As Boolean = False
        '##MA End 07-02-2017

        'Shared moEditableHeaderFields() As String = {
        '    "IDH_CUSREF", "IDH_OBLED", "IDHCARCD", "IDHCRCFL",
        '    "IDHDISCD", "IDHDSCFL", "IDHDADDR", "IDHADDFL",
        '    "IDHCOGRP", "IDHCOICD", "IDHJTYPE", "IDHJCFL",
        '    "IDHWCICD", "IDHWTCFL"
        '}

        Shared moDisabledEditFields() As String = {
            "IDHCNAME", "CODE", "IDHWO", "IDHWOR",
            "IDHCARDC", "IDHLASTR", "IDHDISDC", "IDHNEXTR",
            "IDHRECCT", "IDHCOIDC", "IDHOWNER", "IDHWCIDC",
            "IDHRECCE", "IDHRECED", "IDHPOS"
            }

        Shared moFindFields() As String = {"CODE"}

#Region "Constructer"

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_PBI", "IDHORD", iMenuPosition, "frmPreBookIns.srf", False, True, False, "PreBook Instruction", load_Types.idh_LOAD_NORMAL)

            ''Add the item to the list of items to get their filepaths fixed.
            'doAddImagesToFix("IDHBPCFL")
            'doAddImagesToFix("IDHCRCFL")
            'doAddImagesToFix("IDHDSCFL")
            'doAddImagesToFix("IDHCTCFL")
            'doAddImagesToFix("IDHJTCFL")
            'doAddImagesToFix("IDHWTCFL")
            'doAddImagesToFix("IDHADCFL")
            'doAddImagesToFix("IDHADDFL")
            'doAddImagesToFix("IDHCSTREF")
            
            'Sets the History table and enables the ChangeLog Menu option
            doEnableHistory("@IDH_PBI")
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN) ' For Tabs
            doAddEvent(SAPbouiCOM.BoEventTypes.et_PICKER_CLICKED) ' For Tabs
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK) 'For Checkboxes and Option buttons
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Dim oChk As SAPbouiCOM.CheckBox  = Nothing
            Dim oOpt As SAPbouiCOM.OptionBtn = Nothing
            Dim oItem As SAPbouiCOM.Item = Nothing

			Try
	
                oForm.DataBrowser.BrowseBy = "CODE"

                'oForm.DataSources.DBDataSources.Add("@IDH_PBI")

                'Create User Data sorces and bind to controls
                'Recurrence Pattern
                'Some issue with the Option's on values => D = 1, Y = 2
                oOpt = oForm.Items.Item("IDHOPTDAY").Specific
                oOpt.ValOn = "D"
                'oOpt.ValOff = "N"
                oOpt.Selected = True

                oOpt = oForm.Items.Item("IDHOPTWK").Specific
                oOpt.ValOn = "O"
                'oOpt.ValOff = "N"
                oOpt.GroupWith("IDHOPTDAY")

                oOpt = oForm.Items.Item("IDHOPTMT").Specific
                oOpt.ValOn = "M"
                'oOpt.ValOff = "N"
                oOpt.GroupWith("IDHOPTDAY")

                oOpt = oForm.Items.Item("IDHOPTYR").Specific
                oOpt.ValOn = "Y"
                'oOpt.ValOff = "N"
                oOpt.GroupWith("IDHOPTDAY")

                'Recurrence End
                oOpt = oForm.Items.Item("IDHRENDO").Specific
                oOpt.ValOn = "1"
                oOpt.ValOff = "0"

                oOpt = oForm.Items.Item("IDHRENDD").Specific
                oOpt.ValOff = "0"
                oOpt.ValOff = "1"
                oOpt.GroupWith("IDHRENDO")

                oChk = oForm.Items.Item("IDH_REBATE").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"

'				doAddUFCheck( oForm, "IDHRDMON", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "1", "0")
                oChk = oForm.Items.Item("IDHRDMON").Specific
	            oChk.ValOff = "N"
                oChk.ValOn = "Y"

'				doAddUFCheck( oForm, "IDHRDTUE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "1", "0")
                oChk = oForm.Items.Item("IDHRDTUE").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"

'				doAddUFCheck( oForm, "IDHRDWED", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "1", "0")
                oChk = oForm.Items.Item("IDHRDWED").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"

'				doAddUFCheck( oForm, "IDHRDTHU", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "1", "0")
                oChk = oForm.Items.Item("IDHRDTHU").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"

'				doAddUFCheck( oForm, "IDHRDFRI", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "1", "0")
                oChk = oForm.Items.Item("IDHRDFRI").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"

'				doAddUFCheck( oForm, "IDHRDSAT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "1", "0")
                oChk = oForm.Items.Item("IDHRDSAT").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"

'				doAddUFCheck( oForm, "IDHRDSUN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "1", "0")
                oChk = oForm.Items.Item("IDHRDSUN").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"

'                doAddUFCheck(oForm, "IDH_DETINV", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oChk = oForm.Items.Item("IDH_DETINV").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"
                
'                doAddUFCheck(oForm, "IDH_DETAIL", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oChk = oForm.Items.Item("IDH_DETAIL").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"

                oChk = oForm.Items.Item("IDH_FOC").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"

                oChk = oForm.Items.Item("IDH_UMONE").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"
                
                oChk = oForm.Items.Item("IDH_AUSTRT").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"
                
                oChk = oForm.Items.Item("IDH_AUEND").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"
                '##MA Start 14-04-2017 issue# 163'
                oChk = oForm.Items.Item("IDHPOS").Specific
                oChk.ValOff = "N"
                oChk.ValOn = "Y"
                '##MA End 14-04-2017'
                'Create the Extended Type Options
                oOpt = oForm.Items.Item("IDHEFTDAY").Specific
                'oOpt.ValOn = "D"
                'oOpt.ValOff = "N"
                oOpt.Selected = True

                oOpt = oForm.Items.Item("IDHEFTDATE").Specific
                'oOpt.ValOn = "W"
                'oOpt.ValOff = "N"
				oOpt.GroupWith("IDHEFTDAY")
                
                doAddUF(oForm, "IDH_ACDATE", SAPbouiCOM.BoDataType.dt_DATE, 10, False, False)
                doAddUFCheck(oForm, "IDHTOE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                setUFValue(oForm, "IDHTOE", "N")
				
				oForm.PaneLevel = 1

                'OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
                'Start additional functionality Joachim Alleritz =================================
                'OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
                If Config.INSTANCE.getParameterAsBool("ROUENABL", True) Then
                    bRouting = True
                Else
                    bRouting = False
                End If

                If bRouting = True Then
                    oForm.Items.Item("114").Visible = True
                    oForm.Items.Item("IDHRT").Visible = True
                Else
                    oForm.Items.Item("114").Visible = False
                    oForm.Items.Item("IDHRT").Visible = False
                End If
                oForm.Items.Item("IDHRCDM").Visible = False
                oForm.Items.Item("IDHRCDTU").Visible = False
                oForm.Items.Item("IDHRCDW").Visible = False
                oForm.Items.Item("IDHRCDTH").Visible = False
                oForm.Items.Item("IDHRCDF").Visible = False
                oForm.Items.Item("IDHRCDSA").Visible = False
                oForm.Items.Item("IDHRCDSU").Visible = False
                oForm.Items.Item("IDHCMT").Visible = False
                oForm.Items.Item("IDHDRVI").Visible = False
                oForm.Items.Item("IDHROUTE").Visible = False 'this is the Route Button
                oForm.Items.Item("IDHRSQM").Visible = False 'new function Route Sequence
                oForm.Items.Item("IDHRSQTU").Visible = False 'new function Route Sequence
                oForm.Items.Item("IDHRSQW").Visible = False 'new function Route Sequence
                oForm.Items.Item("IDHRSQTH").Visible = False 'new function Route Sequence
                oForm.Items.Item("IDHRSQF").Visible = False 'new function Route Sequence
                oForm.Items.Item("IDHRSQSA").Visible = False 'new function Route Sequence
                oForm.Items.Item("IDHRSQSU").Visible = False 'new function Route Sequence

                '##MA Start 03-02-2017
                oForm.EnableMenu(IDHAddOns.idh.lookups.Base.DUPLICATE, True)
                '##MA End 03-02-2017

                'End additional functionality Joachim Alleritz =================================
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oOpt)
            End Try
        End Sub
        
        Private Sub doFillObligatedCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDH_OBLED")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = oItem.Specific
            doClearValidValues(oCombo.ValidValues)

            Dim sWord As String

            sWord = getTranslatedWord("None")
            oCombo.ValidValues.Add("", sWord)

            sWord = Config.Parameter("OBTRUE")
            If sWord Is Nothing OrElse sWord.Length = 0 Then
                sWord = getTranslatedWord("Obligated")
            End If
            oCombo.ValidValues.Add(sWord, sWord)

            sWord = Config.Parameter("OBFALSE")
            If sWord Is Nothing OrElse sWord.Length = 0 Then
                sWord = getTranslatedWord("Non-Obligated")
            End If
            oCombo.ValidValues.Add(sWord, sWord)
        End Sub

        Private Sub doUOMCombos(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDHUOM", "OWGT", "UnitDisply", "UnitName")
        End Sub

        Protected Sub doFillMonthCombos(ByVal oForm As SAPbouiCOM.Form)
        	Dim oCombo As SAPbouiCOM.ComboBox
        	oCombo = oForm.Items.Item("IDHEFMTH1").Specific

            doClearValidValues(oCombo.ValidValues)
            Try
                oCombo.ValidValues.Add("1", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(1))
                oCombo.ValidValues.Add("2", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(2))
                oCombo.ValidValues.Add("3", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(3))
                oCombo.ValidValues.Add("4", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(4))
                oCombo.ValidValues.Add("5", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(5))
                oCombo.ValidValues.Add("6", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(6))
                oCombo.ValidValues.Add("7", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(7))
                oCombo.ValidValues.Add("8", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(8))
                oCombo.ValidValues.Add("9", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(9))
                oCombo.ValidValues.Add("10", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(10))
                oCombo.ValidValues.Add("11", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(11))
                oCombo.ValidValues.Add("12", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(12))
            Catch ex As Exception
            End Try
            
            oCombo = oForm.Items.Item("IDHEFMTH2").Specific
            doClearValidValues(oCombo.ValidValues)
            Try
                oCombo.ValidValues.Add("1", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(1))
                oCombo.ValidValues.Add("2", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(2))
                oCombo.ValidValues.Add("3", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(3))
                oCombo.ValidValues.Add("4", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(4))
                oCombo.ValidValues.Add("5", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(5))
                oCombo.ValidValues.Add("6", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(6))
                oCombo.ValidValues.Add("7", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(7))
                oCombo.ValidValues.Add("8", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(8))
                oCombo.ValidValues.Add("9", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(9))
                oCombo.ValidValues.Add("10", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(10))
                oCombo.ValidValues.Add("11", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(11))
                oCombo.ValidValues.Add("12", com.idh.bridge.lookups.FixedValues.getMonthFromIndex(12))
            Catch ex As Exception
            End Try
        End Sub

        Protected Sub doFillWeekDayCombo(ByVal oForm As SAPbouiCOM.Form)
        	Dim oCombo As SAPbouiCOM.ComboBox
        	oCombo = oForm.Items.Item("IDHEFDOW").Specific

            doClearValidValues(oCombo.ValidValues)
            Try
                oCombo.ValidValues.Add("0", com.idh.bridge.lookups.FixedValues.getDayOfWeekFromIndex(7))
                oCombo.ValidValues.Add("1", com.idh.bridge.lookups.FixedValues.getDayOfWeekFromIndex(1))
                oCombo.ValidValues.Add("2", com.idh.bridge.lookups.FixedValues.getDayOfWeekFromIndex(2))
                oCombo.ValidValues.Add("3", com.idh.bridge.lookups.FixedValues.getDayOfWeekFromIndex(3))
                oCombo.ValidValues.Add("4", com.idh.bridge.lookups.FixedValues.getDayOfWeekFromIndex(4))
                oCombo.ValidValues.Add("5", com.idh.bridge.lookups.FixedValues.getDayOfWeekFromIndex(5))
                oCombo.ValidValues.Add("6", com.idh.bridge.lookups.FixedValues.getDayOfWeekFromIndex(6))
            Catch ex As Exception
            End Try
        End Sub
        
        Protected Sub doFillMonthPeriodCombo(ByVal oForm As SAPbouiCOM.Form)
        	Dim oCombo As SAPbouiCOM.ComboBox
        	oCombo = oForm.Items.Item("IDHEFWEEK").Specific

            doClearValidValues(oCombo.ValidValues)
            Try
                oCombo.ValidValues.Add("1", com.idh.bridge.lookups.FixedValues.getPosiotionFromIndex(1))
                oCombo.ValidValues.Add("3", com.idh.bridge.lookups.FixedValues.getPosiotionFromIndex(3))
                oCombo.ValidValues.Add("4", com.idh.bridge.lookups.FixedValues.getPosiotionFromIndex(4))
                oCombo.ValidValues.Add("5", com.idh.bridge.lookups.FixedValues.getPosiotionFromIndex(5))
                oCombo.ValidValues.Add("2", com.idh.bridge.lookups.FixedValues.getPosiotionFromIndex(2))
            Catch ex As Exception
            End Try
        End Sub
#End Region

#Region "Form Controls and Loads"
        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            Dim oWOM As WR1_PBI.IDH_PBI_Handler = New IDH_PBI_Handler(goParent.goDICompany)
            doAddWF(oForm, "PBI_HANDLER", oWOM)
            doAddWF(oForm, "HeaderChanged", False)

            Dim sCode As String = Nothing

            doFillCombo(oForm, "IDHBRNCH", "OUBR", "Code", "Remarks")
            'Would be nice to auto select the relevant Branch of the owner/user automatically.
            
'...            doFillCombo(oForm, "IDHCOGRP", "OITB g, [@IDH_JOBTYPE] jt", "ItmsGrpCod", "ItmsGrpNam", "g.ItmsGrpNam=jt.U_ItemGrp")

            '...doFillCombo(oForm, "IDHCOGRP", "OITB g, [@IDH_JOBTYPE] jt", "ItmsGrpCod", "ItmsGrpNam", "g.ItmsGrpNam=jt.U_ItemGrp")
            doFillCombo(oForm, "IDHCOGRP", "OITB g, [@IDH_JOBTYPE] jt", "ItmsGrpCod", "ItmsGrpNam", "g.ItmsGrpCod=jt.U_ItemGrp")
            
			Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("IDHPERTYP")
			Dim oCombo As SAPbouiCOM.ComboBox = oItem.Specific()
            oCombo.ValidValues.Add(IDH_PBI_Handler.PER_Months, IDH_PBI_Handler.PER_Months)
            oCombo.ValidValues.Add(IDH_PBI_Handler.PER_Quarts, IDH_PBI_Handler.PER_Quarts)
            oCombo.ValidValues.Add(IDH_PBI_Handler.PER_Weeks, IDH_PBI_Handler.PER_Weeks)
			
			doFillObligatedCombo(oForm)
			doFillMonthCombos(oForm)
            doFillWeekDayCombo(oForm)
            doFillMonthPeriodCombo(oForm)
            doUOMCombos(oForm)

            FillCombos.TransactionCodeCombo(FillCombos.getCombo(oForm, "uBC_TRNCd"), Nothing, Nothing, "WO")

            If Not goParent.doCheckModal(oForm.UniqueID) Then
            Else
                sCode = getParentSharedData(oForm, "PBICode")
                setFormDFValue(oForm, "Code", sCode, True)
            End If
            '#MA Issue:270 2017215 start
            'Need to reload PBI after being called from 
            Dim oData As ArrayList = ghOldDialogParams.Item(oForm.UniqueID)
            If Not (oData Is Nothing) Then
                If oData.Count > 1 Then
                    Dim sExtra As String = oData.Item(0)
                    sCode = oData.Item(1)
                    If Not String.IsNullOrEmpty(sCode) Then
                        If oData.Count = 2 Then
                            setFormDFValue(oForm, "Code", sCode, True)
                        End If
                    End If
                End If
            End If
            '#MA Issue:270 2017215 End
        End Sub
        
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            Try
                setWFValue(oForm, "HeaderChanged", False)
                oForm.Freeze(True)
                Dim sCode As String = Nothing
                sCode = getFormDFValue(oForm, "Code", True)
                setUFValue(oForm, "IDH_ACDATE", "")
                If Not sCode Is Nothing AndAlso sCode.Length > 0 Then
                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                    Dim oCons As New SAPbouiCOM.Conditions
                    Dim oCon As SAPbouiCOM.Condition
                    oCon = oCons.Add
                    oCon.Alias = "Code"
                    oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                    oCon.CondVal = sCode
                    oForm.DataSources.DBDataSources.Item("@IDH_PBI").Query(oCons)
                    Dim sRecure As String = getFormDFValue(oForm, IDH_PBI._IDHRECUR)
                    If sRecure = "2" Then
                        If Not oForm.PaneLevel = 3 Then
                            oForm.PaneLevel = 3
                        End If
                    Else
                        If Not oForm.PaneLevel = 1 Then
                            oForm.PaneLevel = 1
                        End If
                        'OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
                        If getFormDFValue(oForm, "U_IDHRT") = "Y" Then
                            oForm.Items.Item("IDHROUTE").Visible = True  'this is the Route Button
                            '#MA Start 15-06-2017 point#144
                            If getFormDFValue(oForm, "U_IDHRECUR") = "M" Then
                                oForm.Items.Item("IDHRCDM").Visible = True
                                oForm.Items.Item("IDHRSQM").Visible = True
                                oForm.Items.Item("IDHRCDTU").Visible = True
                                oForm.Items.Item("IDHRSQTU").Visible = True
                                oForm.Items.Item("IDHRCDW").Visible = True
                                oForm.Items.Item("IDHRSQW").Visible = True
                                oForm.Items.Item("IDHRCDTH").Visible = True
                                oForm.Items.Item("IDHRSQTH").Visible = True
                                oForm.Items.Item("IDHRCDF").Visible = True
                                oForm.Items.Item("IDHRSQF").Visible = True
                                oForm.Items.Item("IDHRCDSA").Visible = True
                                oForm.Items.Item("IDHRSQSA").Visible = True
                                oForm.Items.Item("IDHRCDSU").Visible = True
                                oForm.Items.Item("IDHRSQSU").Visible = True
                            Else
                            If getFormDFValue(oForm, IDH_PBI._IDHRDMON2) = "Y" Then
                                oForm.Items.Item("IDHRCDM").Visible = True
                                oForm.Items.Item("IDHRSQM").Visible = True 'new function Route Sequence
                            Else
                                oForm.Items.Item("IDHRCDM").Visible = False
                                oForm.Items.Item("IDHRSQM").Visible = False 'new function Route Sequence
                            End If
                            If getFormDFValue(oForm, IDH_PBI._IDHRDTUE2) = "Y" Then
                                oForm.Items.Item("IDHRCDTU").Visible = True
                                oForm.Items.Item("IDHRSQTU").Visible = True 'new function Route Sequence
                            Else
                                oForm.Items.Item("IDHRCDTU").Visible = False
                                oForm.Items.Item("IDHRSQTU").Visible = False 'new function Route Sequence
                            End If
                            If getFormDFValue(oForm, IDH_PBI._IDHRDWED2) = "Y" Then
                                oForm.Items.Item("IDHRCDW").Visible = True
                                oForm.Items.Item("IDHRSQW").Visible = True 'new function Route Sequence
                            Else
                                oForm.Items.Item("IDHRCDW").Visible = False
                                oForm.Items.Item("IDHRSQW").Visible = False 'new function Route Sequence
                            End If
                            If getFormDFValue(oForm, IDH_PBI._IDHRDTHU2) = "Y" Then
                                oForm.Items.Item("IDHRCDTH").Visible = True
                                oForm.Items.Item("IDHRSQTH").Visible = True 'new function Route Sequence
                            Else
                                oForm.Items.Item("IDHRCDTH").Visible = False
                                oForm.Items.Item("IDHRSQTH").Visible = False 'new function Route Sequence
                            End If
                            If getFormDFValue(oForm, IDH_PBI._IDHRDFRI2) = "Y" Then
                                oForm.Items.Item("IDHRCDF").Visible = True
                                oForm.Items.Item("IDHRSQF").Visible = True 'new function Route Sequence
                            Else
                                oForm.Items.Item("IDHRCDF").Visible = False
                                oForm.Items.Item("IDHRSQF").Visible = False 'new function Route Sequence
                            End If
                            If getFormDFValue(oForm, IDH_PBI._IDHRDSAT2) = "Y" Then
                                oForm.Items.Item("IDHRCDSA").Visible = True
                                oForm.Items.Item("IDHRSQSA").Visible = True 'new function Route Sequence
                            Else
                                oForm.Items.Item("IDHRCDSA").Visible = False
                                oForm.Items.Item("IDHRSQSA").Visible = False 'new function Route Sequence
                            End If
                            If getFormDFValue(oForm, IDH_PBI._IDHRDSUN2) = "Y" Then
                                oForm.Items.Item("IDHRCDSU").Visible = True
                                oForm.Items.Item("IDHRSQSU").Visible = True 'new function Route Sequence
                            Else
                                oForm.Items.Item("IDHRCDSU").Visible = False
                                oForm.Items.Item("IDHRSQSU").Visible = False 'new function Route Sequence
                            End If
                            oForm.Items.Item("IDHRECOM").Height = 66
                            oForm.Items.Item("IDHCMT").Visible = True
                            oForm.Items.Item("IDHDRVI").Visible = True
                            End If
                            '#MA End 15-06-2017
                        Else
                            Try
                                oForm.Items.Item("IDHRCDM").Visible = False
                                oForm.Items.Item("IDHRCDTU").Visible = False
                                oForm.Items.Item("IDHRCDW").Visible = False
                                oForm.Items.Item("IDHRCDTH").Visible = False
                                oForm.Items.Item("IDHRCDF").Visible = False
                                oForm.Items.Item("IDHRCDSA").Visible = False
                                oForm.Items.Item("IDHRCDSU").Visible = False
                                oForm.Items.Item("IDHCMT").Visible = False
                                oForm.Items.Item("IDHDRVI").Visible = False
                                oForm.Items.Item("IDHRECOM").Height = 150
                                oForm.Items.Item("IDHROUTE").Visible = False 'this is the Route Button
                                oForm.Items.Item("IDHRSQM").Visible = False 'new function Route Sequence
                                oForm.Items.Item("IDHRSQTU").Visible = False 'new function Route Sequence
                                oForm.Items.Item("IDHRSQW").Visible = False 'new function Route Sequence
                                oForm.Items.Item("IDHRSQTH").Visible = False 'new function Route Sequence
                                oForm.Items.Item("IDHRSQF").Visible = False 'new function Route Sequence
                                oForm.Items.Item("IDHRSQSA").Visible = False 'new function Route Sequence
                                oForm.Items.Item("IDHRSQSU").Visible = False 'new function Route Sequence
                            Catch ex As Exception
                            End Try
                        End If
                        doSwitchRebate(oForm)
                    End If
                    'setEnableItem(oForm, False, "IDHRECSD")

                    Dim sRecurrePattern As String = getFormDFValue(oForm, IDH_PBI._IDHRECUR)
                    If sRecurrePattern = "M" Then
                        oForm.Items.Item("lblWkRecur").Specific.Caption = getTranslatedWord("month(s) on:")
                    ElseIf sRecurrePattern = "Y" Then
                        oForm.Items.Item("lblWkRecur").Specific.Caption = getTranslatedWord("year(s) on:")
                    Else
                        oForm.Items.Item("lblWkRecur").Specific.Caption = getTranslatedWord("week(s) on:")
                    End If
                    '#MA issue:406 20170619
                    'If oForm.Mode = BoFormMode.fm_OK_MODE AndAlso getFormDFValue(oForm, "U_AutEnd") = "Y" AndAlso getFormDFValue(oForm, "U_AUTSTRT") = "N" AndAlso getWFValue(oForm, "AUTSTRTPRS") = False Then
                    '    If getFormDFValue(oForm, "U_AutEnd") = "Y" Then
                    '        setFormDFValue(oForm, "U_AUTSTRT", "Y")
                    '    End If '#MA end 20170619 
                    '    setWFValue(oForm, "AUTSTRTPRS", False)
                    '    oForm.Mode = BoFormMode.fm_UPDATE_MODE
                    'End If
                    '#MA end 20170619
                Else
                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                    'setEnableItem(oForm, True, "IDHRECSD")
                    Dim sCardCode As String = getParentSharedData(oForm, "e_CardCode")
                    Dim sCardName As String = getParentSharedData(oForm, "e_CardName")
                    Dim sAddr As String = getParentSharedData(oForm, "e_Addr")
                    Dim sAddrLineNum As String = getParentSharedData(oForm, "e_AddrLineNum")

                    doClearAll(oForm, False)

                    ''OnTime [#Ico000????] USA
                    ''START
                    ''Select Detail Checkbox by default
                    'Dim oItm As SAPbouiCOM.CheckBox
                    'oItm = oForm.Items.Item("IDH_DETAIL").Specific
                    'oItm.Checked = True
                    ''END
                    ''OnTime [#Ico000????] USA

                    setFormDFValue(oForm, IDH_PBI._IDHCCODE, sCardCode)
                    setFormDFValue(oForm, IDH_PBI._IDHCNAME, sCardName)
                    setFormDFValue(oForm, IDH_PBI._IDHSADDR, sAddr)
                    setFormDFValue(oForm, IDH_PBI._IDHSADLN, sAddrLineNum)
                    setFormDFValue(oForm, IDH_PBI._IDHBRNCH, Config.INSTANCE.doGetCurrentBranch())
                End If
                setWFValue(oForm, "LastShow", True)

                doSetFocus(oForm, "IDHCCODE")
                EnableAllEditItems(oForm, moDisabledEditFields)

                '#MA Start 07-07-2017 Point#144    Enable False All Days Names CheckBox Fields when Monthly Radio Button is Tick
                If getFormDFValue(oForm, "U_IDHRECUR") = "M" Then
                    oForm.Items.Item("IDHRDMON").Enabled = False
                    oForm.Items.Item("IDHRDTUE").Enabled = False
                    oForm.Items.Item("IDHRDWED").Enabled = False
                    oForm.Items.Item("IDHRDTHU").Enabled = False
                    oForm.Items.Item("IDHRDFRI").Enabled = False
                    oForm.Items.Item("IDHRDSAT").Enabled = False
                    oForm.Items.Item("IDHRDSUN").Enabled = False
                End If
                '#MA End 07-07-2017
                DoLockUnLock(oForm, False)

                DoViewActionDate(oForm, False)

                '##MA Start 03-02-2017
                If getParentSharedData(oForm, "CallFromManagerToDuplicate") = True Then
                    doAddWF(oForm, "callFromPBIManager", True)
                    Dim OPBI As IDH_PBI = New IDH_PBI()
                    OPBI.SBOForm = oForm
                    sCode = getParentSharedData(oForm, "PBICode")
                    doHandleDuplicate = True
                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                    DoUnlockAllEdit(oForm)
                    doClearFormDFValues(oForm)
                    OPBI.doClearBuffers()
                    OPBI.duplicateDocument(sCode)
                    setWasSetInternally(oForm, "IDHRECSD", data_SetFrom.idh_SET_FROMUSERFIELD)
                    setWFValue(oForm, "duplicatePBIResetIDHRECSD", True)
                    setFormDFValue(oForm, "U_IDHOWNER", goParent.goDICompany.UserName)
                    oForm.Items.Item("IDHADCFL").Enabled = True
                    doSetFocus(oForm, IDH_PBI._IDHCARCD)
                End If
                '##MA End 03-02-2017

            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBL", Nothing)
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        '#MA start 14-04-2017
        Private Sub doCheckonSite(ByVal oForm As SAPbouiCOM.Form)
            If Config.INSTANCE.getParameterAsBool("PBIPOS") Then
                Dim oBPAddressShipTo As BPAddress = New BPAddress()
                oBPAddressShipTo.doGetAddress(getFormDFValue(oForm, "U_IDHCCODE"), "S", False, getFormDFValue(oForm, "U_IDHSADDR"))
                If (oBPAddressShipTo.U_onStop.Equals("Y") And getFormDFValue(oForm, "U_IDHSADDR").ToString.Length > 0) Then
                    setFormDFValue(oForm, "U_IDHPOS", "Y")
                Else
                    setFormDFValue(oForm, "U_IDHPOS", "N")
                End If
            End If
        End Sub
        '#MA End 14-04-2017

        Private Sub DoUnlockAllEdit(ByVal oForm As SAPbouiCOM.Form)
            EnableAllEditItems(oForm, moDisabledEditFields)
        End Sub

        Private Sub DoLockUnLock(ByVal oForm As SAPbouiCOM.Form, ByVal bDoHead As Boolean)
            Dim sWOR As String = getFormDFValue(oForm, IDH_PBI._IDHWOR)
            Dim sWO As String = getFormDFValue(oForm, IDH_PBI._IDHWO)
            Dim bPBIEnable As Boolean = Config.ParameterAsBool("PBICUSIT", False)
            If Not bPBIEnable Then
                If sWOR.Length > 0 OrElse sWO.Length > 0 Then
                    doSetFocus(oForm, "IDHCARCD")
                    setEnableItem(oForm, False, "IDHCCODE")
                    setEnableItem(oForm, False, "IDHBPCFL")
                    setEnableItem(oForm, False, "IDHSADDR")
                    setEnableItem(oForm, False, "IDHADCFL")
                Else
                    setEnableItem(oForm, True, "IDHCCODE")
                    setEnableItem(oForm, True, "IDHBPCFL")
                    setEnableItem(oForm, True, "IDHSADDR")
                    setEnableItem(oForm, True, "IDHADCFL")
                End If
            End If

            If bDoHead Then
                setEnableItems(oForm, True, moEditableHeaderFields)
            End If
        End Sub



        Private Sub DoViewActionDate(ByVal oForm As SAPbouiCOM.Form, ByVal bDoShow As Boolean)
            Dim bDoShowT As Boolean = getWFValue(oForm, "LastShow")
            If bDoShow <> bDoShowT Then
                oForm.Items.Item("91").Visible = bDoShow
                oForm.Items.Item("IDH_ACDATE").Visible = bDoShow
                oForm.Items.Item("IDHTOE").Visible = bDoShow

                setWFValue(oForm, "LastShow", bDoShow)
            End If
        End Sub

        Private Sub doSwitchActionDate(ByVal oForm As SAPbouiCOM.Form)
            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                DoViewActionDate(oForm, True)
            End If
        End Sub

        '** Create the form
        Private Sub doClearAll(ByVal oForm As SAPbouiCOM.Form, ByVal bDoNewCode As Boolean)
            doClearFormDFValues(oForm)

            'Start addional form fettings for Route
            oForm.Items.Item("IDHRCDM").Visible = False
            oForm.Items.Item("IDHRCDTU").Visible = False
            oForm.Items.Item("IDHRCDW").Visible = False
            oForm.Items.Item("IDHRCDTH").Visible = False
            oForm.Items.Item("IDHRCDF").Visible = False
            oForm.Items.Item("IDHRCDSA").Visible = False
            oForm.Items.Item("IDHRCDSU").Visible = False
            oForm.Items.Item("IDHCMT").Visible = False
            oForm.Items.Item("IDHDRVI").Visible = False
            oForm.Items.Item("IDHRECOM").Height = 150
            oForm.Items.Item("IDHROUTE").Visible = False 'this is the Route Button
            oForm.Items.Item("IDHRSQM").Visible = False 'new function Route Sequence
            oForm.Items.Item("IDHRSQTU").Visible = False 'new function Route Sequence
            oForm.Items.Item("IDHRSQW").Visible = False 'new function Route Sequence
            oForm.Items.Item("IDHRSQTH").Visible = False 'new function Route Sequence
            oForm.Items.Item("IDHRSQF").Visible = False 'new function Route Sequence
            oForm.Items.Item("IDHRSQSA").Visible = False 'new function Route Sequence
            oForm.Items.Item("IDHRSQSU").Visible = False 'new function Route Sequence
            'End additional form settings for Route

            Dim iFreq As Integer = Config.ParameterWithDefault("PBIFRQ", "4")
            setFormDFValue(oForm, "U_IDHRECFQ", iFreq)
            'If iFreq = 3 Then
                setFormDFValue(oForm, "U_PerTyp", IDH_PBI_Handler.PER_Quarts)
            'Else
                setFormDFValue(oForm, "U_PerTyp", IDH_PBI_Handler.PER_Months)
            'End If

            setFormDFValue(oForm, "U_IDHRACTW", 0)

            setFormDFValue(oForm, "U_IDHRECSD", Date.Today)
            setFormDFValue(oForm, "U_IDHOWNER", goParent.goDICompany.UserName)

            Dim bTicket As Boolean
            bTicket = Config.ParameterAsBool("PBIAUSDT", True)
            If bTicket Then
                setFormDFValue(oForm, "U_AutStrt", "Y")
            Else
                setFormDFValue(oForm, "U_AutStrt", "N")
            End If

            bTicket = Config.ParameterAsBool("PBIAUEDT", True)
            If bTicket Then
                setFormDFValue(oForm, "U_AutEnd", "Y")
            Else
                setFormDFValue(oForm, "U_AutEnd", "N")
            End If

            'Set the default Haulage Qty
            Dim iDefHaulQty = Config.ParameterAsInt("PBIDHQTY")
            setFormDFValue(oForm, IDH_PBI._IDHCOIQT, iDefHaulQty)

            bTicket = Config.ParameterAsBool("PBIUPE", True)
            If bTicket Then
                setFormDFValue(oForm, "U_IDHUMONE", "Y")
            Else
                setFormDFValue(oForm, "U_IDHUMONE", "N")
            End If

            setFormDFValue(oForm, "U_IDHRECUR", "W")
            If Not oForm.PaneLevel = 1 Then
                oForm.PaneLevel = 1
            End If

            Dim iVal As Integer
            setFormDFValue(oForm, "U_EFDay", DateTime.Now.Day)
            Select Case (DateTime.Now.DayOfWeek)
                Case DayOfWeek.Monday
                    iVal = 1
                Case DayOfWeek.Tuesday
                    iVal = 2
                Case DayOfWeek.Wednesday
                    iVal = 3
                Case DayOfWeek.Thursday
                    iVal = 4
                Case DayOfWeek.Friday
                    iVal = 5
                Case DayOfWeek.Saturday
                    iVal = 6
                Case Else
                    iVal = 7
            End Select
            setFormDFValue(oForm, "U_EFDOW", iVal)
            setFormDFValue(oForm, "U_EFMonth", DateTime.Now.Month)
            setFormDFValue(oForm, "U_EFWeek", 1)

            setFormDFValue(oForm, "U_IDHRECEN", 0)

            'OnTime [#Ico000????] USA
            'START
            'Select Detail Checkbox by default
            Dim bDetailChkedDefault As Boolean = Config.ParameterAsBool("PBIDETDF", False)
            If bDetailChkedDefault Then
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    Dim oItm As SAPbouiCOM.CheckBox
                    oItm = oForm.Items.Item("IDH_DETAIL").Specific
                    oItm.Checked = True
                End If
            End If
            'END
            'OnTime [#Ico000????] USA

            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                setEnableItem(oForm, True, "CODE")
            Else
                setEnableItem(oForm, False, "CODE")
            End If

            setFormDFValue(oForm, "U_IDHFOC", "N")
        End Sub

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCloseForm(oForm, BubbleEvent)
        End Sub

        Public Overrides Sub doClose()
        End Sub
#End Region

#Region "Event Handlers"
        '** The Menu Event handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                Try

                    '##MA Start 03-02-2017
                    If (Not pVal.BeforeAction AndAlso pVal.MenuUID = IDHAddOns.idh.lookups.Base.DUPLICATE) Then
                        oForm.Freeze(True)
                        Dim OPBI As IDH_PBI = New IDH_PBI()
                        OPBI.SBOForm = oForm
                        Dim sCode As String = OPBI.Code
                        doHandleDuplicate = True
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                        DoUnlockAllEdit(oForm)
                        doClearFormDFValues(oForm)
                        OPBI.doClearBuffers()
                        OPBI.duplicateDocument(sCode)
                        setWasSetInternally(oForm, "IDHRECSD", data_SetFrom.idh_SET_FROMUSERFIELD)
                        setWFValue(oForm, "duplicatePBIResetIDHRECSD", True)
                        setFormDFValue(oForm, "U_IDHOWNER", goParent.goDICompany.UserName)
                        doSetFocus(oForm, IDH_PBI._IDHCARCD)
                        oForm.Items.Item("IDHADCFL").Enabled = True
                        oForm.Freeze(False)
                        '##MA End 03-02-2017

                    ElseIf pVal.MenuUID = Config.NAV_ADD Then
                        doClearAll(oForm, True)
                        setFormDFValue(oForm, IDH_PBI._IDHBRNCH, Config.INSTANCE.doGetCurrentBranch())
                        DoUnlockAllEdit(oForm)
                        DoLockUnLock(oForm, False)

                        'setEnableItem(oForm, True, "IDHRECSD")
                    ElseIf pVal.MenuUID = Config.NAV_FIND OrElse
                       pVal.MenuUID = Config.NAV_FIRST OrElse
                       pVal.MenuUID = Config.NAV_LAST OrElse
                       pVal.MenuUID = Config.NAV_NEXT OrElse
                       pVal.MenuUID = Config.NAV_PREV Then
                        oForm.Freeze(True)
                    Try
                            If pVal.MenuUID = Config.NAV_FIND Then
                                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                doClearAll(oForm, False)
                                'setEnableItem(oForm, "CODE")
                                doSetFocus(oForm, "CODE")
                                DisableAllEditItems(oForm, moFindFields)
                            Else
                                If isEnabled(oForm.Items.Item("CODE")) Then
                                DoUnlockAllEdit(oForm)
                            End If
                            doLoadData(oForm)
                        End If
                    Catch ex As Exception
                    Finally
                        oForm.Freeze(False)
                    End Try
                End If
                Catch ex As Exception
                Finally
                    oForm.Freeze(False)
                End Try
            End If
            Return True
        End Function

        Private Function doCheckHeadersChange(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim bHasChanged As Boolean = getWFValue(oForm, "HeaderChanged")
            If bHasChanged Then
                Dim sActDate As String = getUFValue(oForm, "IDH_ACDATE")
                If Not sActDate Is Nothing Then
                    If sActDate.Length >= 6 Then
                        sActDate = com.idh.utils.Dates.doSBODateToSQLDate(sActDate)

                        Dim dActDate As DateTime
                        Dim sNextDate As String = getFormDFValue(oForm, IDH_PBI._IDHNEXTR)
                        Dim dNextDate As Date = Conversions.ToDateTime(sNextDate)
                        dActDate = Conversions.ToDateTime(sActDate)

                        If Date.Compare(dActDate, dNextDate) < 0 Then
                            'DataHandler.INSTANCE.doError(DataHandler.ERRORLEVEL_USER & ": You can not change the Header deatils in a existing Period.")
                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("You can not change the Header deatils in a existing Period.", "ERUSCHD", {Nothing})
                            Return False
                        End If
                    End If
                End If
            End If
            Return True
        End Function

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim sItemId As String = pVal.ItemUID
            '##MA 2-6-2017 for Refreshing PBI on deleting row from OSM Button Issue:41
            If pVal.EventType = BoEventTypes.et_FORM_ACTIVATE AndAlso getSharedData(oForm, "rowDeleted") IsNot Nothing Then
                If pVal.BeforeAction = False Then
                    doLoadData(oForm)
                    setSharedData(oForm, "rowDeleted", Nothing)
                End If
            End If
            '##MA End 2-6-2017
            Select Case pVal.BeforeAction
                Case True
                    If pVal.EventType = SAPbouiCOM.BoEventTypes.et_CLICK Then
                        If sItemId = "IDHRENDO" OrElse sItemId = "IDHRENDD" Then
                            oForm.Items.Item("IDHRECSD").Click()
                        End If
                    ElseIf SAPbouiCOM.BoEventTypes.et_PICKER_CLICKED Then
                        If sItemId.Equals("IDHRECSD") OrElse
                         sItemId.Equals("U_IDHRECED") Then
                            doSwitchActionDate(oForm)
                        End If
                    End If
                Case False
                    Select Case pVal.EventType
                        Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                            If pVal.ItemChanged = True Then
                                ''MA 23-11-2015 in Copy paste wasSetWithCode was returning true and
                                ''the validaion event did not fire. skyped LV( combined with HH), No Reply
                                'If wasSetWithCode(oForm, sItemId) = False Then
                                If sItemId.Equals("IDH_CUSREF") OrElse
                                    sItemId.Equals("IDH_OBLED") OrElse
                                    sItemId.Equals("IDHCATCD") OrElse
                                    sItemId.Equals("IDHDISCD") OrElse
                                    sItemId.Equals("IDHDADDR") OrElse
                                    sItemId.Equals("IDHCOGRP") OrElse
                                    sItemId.Equals("IDHCOICD") OrElse
                                    sItemId.Equals("IDHCOIQT") OrElse
                                    sItemId.Equals("IDHJTYPE") OrElse
                                    sItemId.Equals("IDHWCICD") Then
                                    setWFValue(oForm, "HeaderChanged", True)
                                End If

                                If pVal.InnerEvent = False Then
                                    If sItemId.Equals("IDH_ACDATE") Then
                                        If (getFormDFValue(oForm, "U_IDHDETIN") = "N" AndAlso getFormDFValue(oForm, "U_IDHDETAI") = "N") _
                                            OrElse getFormDFValue(oForm, "U_IDHDETIN") = "Y" Then
                                            If doCheckHeadersChange(oForm) = False Then
                                                setUFValue(oForm, "IDH_ACDATE", "")
                                            Else
                                                setEnableItems(oForm, False, moEditableHeaderFields)
                                            End If
                                        End If
                                    ElseIf sItemId.Equals("IDHCCODE") Then 'BP Choose from
                                        doChooseCustomer(oForm)
                                    ElseIf sItemId.Equals("IDHCARCD") Then 'Carrier Choose from
                                        doChooseCarrier(oForm)
                                    ElseIf sItemId.Equals("IDHDISCD") Then 'Disposal Choose from
                                        doChooseDisposal(oForm)
                                    ElseIf sItemId.Equals("IDHJTYPE") Then 'Job Type Choose from
                                        doChooseJobType(oForm)
                                    ElseIf sItemId.Equals("IDHCOICD") Then 'Container Choose from
                                        doChooseContainer(oForm)
                                    ElseIf sItemId.Equals("IDHWCICD") Then 'Waste Type Choose from
                                        doChooseWasteType(oForm)
                                    ElseIf sItemId.Equals("IDHSADDR") Then 'Site Address Choose from
                                        doChooseSiteAd(oForm)
                                    ElseIf sItemId.Equals("IDHDADDR") Then 'Disposal Address Choose from
                                        doChooseDisposalAd(oForm)
                                    ElseIf sItemId.Equals("IDHRECFQ") Then 'Run Freq
                                        Dim iRunFreq As Integer = getFormDFValue(oForm, "U_IDHRECFQ")
                                        If iRunFreq <> 3 Then
                                            '   setFormDFValue(oForm, "U_PerTyp", IDH_PBI_Handler.PER_Months)
                                        End If
                                    ElseIf sItemId.Equals("IDHRECSD") Then 'Auto set the end date
                                        Dim dStartDate As DateTime = Dates.doStrToDate(getFormDFValue(oForm, "U_IDHRECSD"))
                                        If Dates.isValidDate(dStartDate) Then
                                            'setFormDFValue(oForm, "U_IDHLASTR", dStartDate)
                                            'setFormDFValue(oForm, "U_IDHNEXTR", dStartDate)
                                            Dim sSwitch As String = getFormDFValue(oForm, "U_IDHRECEN")
                                            If sSwitch = "0" Then
                                                setFormDFValue(oForm, "U_IDHRECEN", 2)
                                                Dim dStopDate As DateTime = dStartDate.AddYears(10)

                                                oForm.Items.Item("IDHRECCE").Enabled = False
                                                oForm.Items.Item("IDHRECED").Enabled = True

                                                setFormDFValue(oForm, "U_IDHRECED", goParent.doDateToStr(dStopDate))
                                                setFormDFValue(oForm, "U_IDHRECCE", "0")
                                            End If
                                        End If
                                        ''MA Start 14-09-2015 Issue#665
                                    ElseIf sItemId.Equals("IDHRCDM") OrElse
                                        sItemId.Equals("IDHRCDTU") OrElse
                                        sItemId.Equals("IDHRCDW") OrElse
                                        sItemId.Equals("IDHRCDTH") OrElse
                                        sItemId.Equals("IDHRCDF") OrElse
                                        sItemId.Equals("IDHRCDSA") OrElse
                                        sItemId.Equals("IDHRCDSU") Then
                                        doSearchRout(sItemId, oForm)
                                        ''MA End 14-09-2016 Issue#665
                                    Else
                                        If sItemId.Equals("IDHRCDM") OrElse
                                            sItemId.Equals("IDHRCDTU") OrElse
                                            sItemId.Equals("IDHRCDW") OrElse
                                            sItemId.Equals("IDHRCDTH") OrElse
                                            sItemId.Equals("IDHRCDF") OrElse
                                            sItemId.Equals("IDHRCDSA") OrElse
                                            sItemId.Equals("IDHRCDSU") OrElse
                                            sItemId.Equals("IDHRSQM") OrElse
                                            sItemId.Equals("IDHRSQTU") OrElse
                                            sItemId.Equals("IDHRSQW") OrElse
                                            sItemId.Equals("IDHRSQTH") OrElse
                                            sItemId.Equals("IDHRSQF") OrElse
                                            sItemId.Equals("IDHRSQSA") OrElse
                                            sItemId.Equals("IDHRSQSU") OrElse
                                            sItemId.Equals("IDHCMT") OrElse
                                            sItemId.Equals("IDHDRVI") Then
                                            doSwitchActionDate(oForm)
                                        End If
                                    End If
                                End If
                                'End If
                            Else
                                ''MA Start 14-09-2015 Issue#665
                                If sItemId.Equals("IDHRCDM") OrElse
                                            sItemId.Equals("IDHRCDTU") OrElse
                                            sItemId.Equals("IDHRCDW") OrElse
                                            sItemId.Equals("IDHRCDTH") OrElse
                                            sItemId.Equals("IDHRCDF") OrElse
                                            sItemId.Equals("IDHRCDSA") OrElse
                                            sItemId.Equals("IDHRCDSU") Then
                                    If getItemValue(oForm, sItemId).Trim <> String.Empty Then
                                        doSearchRout(sItemId, oForm)
                                    End If
                                End If
                                ''MA End 14-09-2016 Issue#665
                            End If
                        Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                            If sItemId.Equals("IDHPERTYP") Then
                                If getFormDFValue(oForm, "U_PERTYP").Equals(IDH_PBI_Handler.PER_Quarts) Then
                                    'setFormDFValue(oForm, "U_IDHRECFQ", 3)
                                ElseIf getFormDFValue(oForm, "U_PERTYP").Equals(IDH_PBI_Handler.PER_Weeks) Then
                                    setFormDFValue(oForm, "U_IDHRECFQ", 1)
                                    setFormDFValue(oForm, "U_IDHUMONE", "N")
                                End If
                            End If
                        Case SAPbouiCOM.BoEventTypes.et_KEY_DOWN
                            If sItemId.Equals("IDHCOIQT") OrElse
                             sItemId.Equals("IDHRECFQ") OrElse
                             sItemId.Equals("IDHRECTW") OrElse
                             sItemId.Equals("IDHRECSD") OrElse
                             sItemId.Equals("IDHRECCE") OrElse
                             sItemId.Equals("IDHRECED") Then
                                doSwitchActionDate(oForm)
                            End If
                            'start modifications by Joachim Alleritz ===================
                            'If pVal.CharPressed = 9 Then
                            '    If sItemId.Equals("IDHRCDM") Then
                            '        If getFormDFValue(oForm, "U_IDHRCDM") = "*" Then
                            '            setSharedData(oForm, "IDH_RTCOD", "")
                            '            setSharedData(oForm, "IDH_RTWDY", "*M*")
                            '            goParent.doOpenModalForm("IDHROSRC", oForm)
                            '        End If
                            '    End If
                            '    If sItemId.Equals("IDHRCDTU") Then
                            '        If getFormDFValue(oForm, "U_IDHRCDTU") = "*" Then
                            '            setSharedData(oForm, "IDH_RTCOD", "")
                            '            setSharedData(oForm, "IDH_RTWDY", "*Tu*")
                            '            goParent.doOpenModalForm("IDHROSRC", oForm)
                            '        End If
                            '    End If
                            '    If sItemId.Equals("IDHRCDW") Then
                            '        If getFormDFValue(oForm, "U_IDHRCDW") = "*" Then
                            '            setSharedData(oForm, "IDH_RTCOD", "")
                            '            setSharedData(oForm, "IDH_RTWDY", "*W*")
                            '            goParent.doOpenModalForm("IDHROSRC", oForm)
                            '        End If
                            '    End If
                            '    If sItemId.Equals("IDHRCDTH") Then
                            '        If getFormDFValue(oForm, "U_IDHRCDTH") = "*" Then
                            '            setSharedData(oForm, "IDH_RTCOD", "")
                            '            setSharedData(oForm, "IDH_RTWDY", "*Th*")
                            '            goParent.doOpenModalForm("IDHROSRC", oForm)
                            '        End If
                            '    End If
                            '    If sItemId.Equals("IDHRCDF") Then
                            '        If getFormDFValue(oForm, "U_IDHRCDF") = "*" Then
                            '            setSharedData(oForm, "IDH_RTCOD", "")
                            '            setSharedData(oForm, "IDH_RTWDY", "*F*")
                            '            goParent.doOpenModalForm("IDHROSRC", oForm)
                            '        End If
                            '    End If
                            '    If sItemId.Equals("IDHRCDSA") Then
                            '        If getFormDFValue(oForm, "U_IDHRCDSA") = "*" Then
                            '            setSharedData(oForm, "IDH_RTCOD", "")
                            '            setSharedData(oForm, "IDH_RTWDY", "*Sa*")
                            '            goParent.doOpenModalForm("IDHROSRC", oForm)
                            '        End If
                            '    End If
                            '    If sItemId.Equals("IDHRCDSU") Then
                            '        If getFormDFValue(oForm, "U_IDHRCDSU") = "*" Then
                            '            setSharedData(oForm, "IDH_RTCOD", "")
                            '            setSharedData(oForm, "IDH_RTWDY", "*Su*")
                            '            goParent.doOpenModalForm("IDHROSRC", oForm)
                            '        End If
                            '    End If
                            'End If
                            'end modifications by Joachim Alleritz =====================
                        Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                            If sItemId.Equals("IDHCSTREF") Then
                                doChooseCustomerRef(oForm)
                            ElseIf sItemId.Equals("IDHBPCFL") Then 'BP Choose from
                                doChooseCustomer(oForm)
                            ElseIf sItemId.Equals("IDHCRCFL") Then 'Carrier Choose from
                                doChooseCarrier(oForm)
                            ElseIf sItemId.Equals("IDHDSCFL") Then 'Disposal Choose from
                                doChooseDisposal(oForm)
                            ElseIf sItemId.Equals("IDHJTCFL") Then 'Job Type Choose from
                                doChooseJobType(oForm)
                            ElseIf sItemId.Equals("IDHCTCFL") Then 'Container Choose from
                                doChooseContainer(oForm)
                            ElseIf sItemId.Equals("IDHWTCFL") Then 'Waste Type Choose from
                                doChooseWasteType(oForm)
                            ElseIf sItemId.Equals("IDHADCFL") Then 'Site Address Choose from
                                doChooseSiteAd(oForm)
                            ElseIf sItemId.Equals("IDHADDFL") Then 'Disposal Address Choose from
                                doChooseDisposalAd(oForm)
                            ElseIf sItemId.Equals("IDHPO") Then
                                doReportByBtn(oForm, "IDHPO")
                            ElseIf pVal.ItemUID.Equals("IDHDOC") Then
                                doReportByBtn(oForm, "IDHDOC")
                            ElseIf sItemId.Equals("IDHCUST") Then
                                doReportByBtn(oForm, "IDHCUST")
                            ElseIf sItemId.Equals("IDHBM") Then
                                setSharedData(oForm, "IDH_ORDNO", getFormDFValue(oForm, IDH_PBI._IDHWO))
                                goParent.doOpenModalForm("IDHWOBI", oForm)
                            ElseIf sItemId.Equals("IDHOSM") Then
                                setSharedData(oForm, "IDH_ORDNO", getFormDFValue(oForm, IDH_PBI._IDHWO))
                                setSharedData(oForm, "IDH_CUST", getFormDFValue(oForm, IDH_PBI._IDHCCODE))
                                setSharedData(oForm, "IDH_ADDR", getFormDFValue(oForm, IDH_PBI._IDHSADDR))
                                setSharedData(oForm, "IDH_ADLN", getFormDFValue(oForm, IDH_PBI._IDHSADLN))
                                setSharedData(oForm, "FILTERUB", "FALSE")

                                Dim sOSM As String = Config.ParameterWithDefault("OSMPBI", "IDHJOBR")
                                goParent.doOpenModalForm(sOSM, oForm)
                            ElseIf sItemId = "IDH_PBIDT2" Then
                                setSharedData(oForm, "IDH_PBI", getFormDFValue(oForm, IDH_PBI._Code))
                                Dim sCount As String = getFormDFValue(oForm, IDH_PBI._IDHRECCT)
                                Dim iCount As Integer = Conversions.ToInt(sCount)
                                setSharedData(oForm, "IDH_RUNCNT", iCount - 1)
                                goParent.doOpenModalForm("IDHPBID", oForm)
                            ElseIf sItemId = "IDH_PBIDTL" Then
                                setSharedData(oForm, "IDH_PBI", getFormDFValue(oForm, IDH_PBI._Code))
                                Dim sCount As String = getFormDFValue(oForm, IDH_PBI._IDHRECCT)
                                Dim iCount As Integer = Conversions.ToInt(sCount)
                                setSharedData(oForm, "IDH_RUNCNT", "")
                                goParent.doOpenModalForm("IDHPBID", oForm)
                            ElseIf sItemId.Equals("IDHCIP") Then
                                Dim sCardCode As String = getFormDFValue(oForm, IDH_PBI._IDHCCODE)
                                Dim sItemCode As String = getFormDFValue(oForm, IDH_PBI._IDHCOICD)
                                Dim sItemGrp As String = getFormDFValue(oForm, IDH_PBI._IDHCOGRP)
                                Dim sWastCd As String = getFormDFValue(oForm, IDH_PBI._IDHWCICD)
                                Dim sAddress As String = getFormDFValue(oForm, IDH_PBI._IDHSADDR)
                                Dim sAddressLineNum As String = getFormDFValue(oForm, IDH_PBI._IDHSADLN)
                                Dim sJobType As String = getFormDFValue(oForm, IDH_PBI._IDHJTYPE)
                                Dim sBranch As String = getFormDFValue(oForm, IDH_PBI._IDHBRNCH)

                                'setSharedData(oForm, "CUSCD", sCardCode)
                                'setSharedData(oForm, "ITEMCD", sItemCode)
                                'setSharedData(oForm, "ITMGRP", sItemGrp)
                                'setSharedData(oForm, "WASCD", sWastCd)
                                'setSharedData(oForm, "STADDR", sAddress)
                                'setSharedData(oForm, "JOBTP", sJobType)

                                'goParent.doOpenModalForm("IDH_CSITPR", oForm)

                                Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.prices.CIP = New WR1_FR2Forms.idh.forms.fr2.prices.CIP(Nothing, oForm.UniqueID, Nothing)

                                oOOForm.CustomerCode = sCardCode
                                'oOOForm.Address = sAddress
                                'oOOForm.AddressLineNum = sAddressLineNum
                                If Config.ParamaterWithDefault("PCPCSADR", "DEFAULT") = "DEFAULT" Then
                                    oOOForm.Address = sAddress
                                    oOOForm.AddressLineNum = sAddressLineNum
                                End If
                                If Config.ParamaterWithDefault("PCPBRNCH", "ALL/BLANK") = "DEFAULT" Then
                                    oOOForm.BrancheCode = sBranch
                                End If
                                If Config.ParamaterWithDefault("PCPCNCOD", "DEFAULT") = "DEFAULT" Then
                                    oOOForm.ContainerCode = sItemCode
                                End If
                                If Config.ParamaterWithDefault("PCPORDTP", "ALL/BLANK") = "DEFAULT" Then
                                    oOOForm.JobType = sJobType
                                End If
                                If Config.ParamaterWithDefault("PCPWSTCD", "DEFAULT") = "DEFAULT" Then
                                    oOOForm.WasteCode = sWastCd
                                End If
                                If Config.ParamaterWithDefault("PCPWR1TP", "DEFAULT") = "DEFAULT" Then
                                    oOOForm.WR1OrderType = "WO"
                                End If

                                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    'oOOForm.ContainerCode = sItemCode
                                    'oOOForm.WasteCode = sWastCd

                                    'Dim bUseAny As String = goParent.goLookup.getParameterAsBool("PRCDWRT", False)
                                    'If bUseAny Then
                                    '    oOOForm.WR1OrderType = ""
                                    'Else
                                    '    oOOForm.WR1OrderType = "WO"
                                    'End If
                                    oOOForm.FilterSet = WR1_FR2Forms.idh.forms.fr2.prices.CIP.PBIADD
                                Else
                                    oOOForm.FilterSet = WR1_FR2Forms.idh.forms.fr2.prices.CIP.PBIOK
                                End If

                                oOOForm.CustomerCode = sCardCode
                                'oOOForm.ItemGroup = sItemGrp

                                oOOForm.FilterMode = 1
                                oOOForm.doShowModal()
                            ElseIf sItemId.Equals("IDHSIP") Then
                                Dim sCustCode As String = getFormDFValue(oForm, IDH_PBI._IDHCCODE)
                                Dim sDispSite As String = getFormDFValue(oForm, IDH_PBI._IDHDISCD)
                                Dim sCarrCode As String = getFormDFValue(oForm, IDH_PBI._IDHCARCD)
                                Dim sItemCode As String = getFormDFValue(oForm, IDH_PBI._IDHCOICD)
                                Dim sItemGrp As String = getFormDFValue(oForm, IDH_PBI._IDHCOGRP)
                                Dim sWastCd As String = getFormDFValue(oForm, IDH_PBI._IDHWCICD)
                                Dim sAddress As String = getFormDFValue(oForm, IDH_PBI._IDHSADDR)
                                Dim sAddressLineNum As String = getFormDFValue(oForm, IDH_PBI._IDHSADLN)
                                Dim sJobType As String = getFormDFValue(oForm, IDH_PBI._IDHJTYPE)
                                Dim sDisposalAddress As String = getFormDFValue(oForm, IDH_PBI._IDHDADDR)
                                Dim sBranch As String = getFormDFValue(oForm, IDH_PBI._IDHBRNCH)

                                'setSharedData(oForm, "SUPCD", sCarrCode)
                                'setSharedData(oForm, "ITEMCD", sItemCode)
                                'setSharedData(oForm, "ITMGRP", sItemGrp)
                                'setSharedData(oForm, "WASCD", sWastCd)
                                'setSharedData(oForm, "JOBTP", sJobType)
                                'setSharedData(oForm, "TRG", "SIP")

                                'goParent.doOpenModalForm("IDH_SUITPR", oForm)

                                Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.prices.SIP = New WR1_FR2Forms.idh.forms.fr2.prices.SIP(Nothing, oForm.UniqueID, Nothing)
                                oOOForm.SupplierCode = sCarrCode
                                'oOOForm.CustomerCode = sCustCode
                                If Config.ParamaterWithDefault("PSPCUSCD", "ALL/BLANK") = "DEFAULT" Then
                                    oOOForm.CustomerCode = sCustCode
                                End If
                                If Config.ParamaterWithDefault("PSPCSADR", "ALL/BLANK") = "DEFAULT" Then
                                    oOOForm.Address = sAddress
                                    oOOForm.AddressLineNum = sAddressLineNum
                                End If
                                If Config.ParamaterWithDefault("PSPPRADR", "ALL/BLANK") = "DEFAULT" Then
                                    oOOForm.SupplierAddress = sDisposalAddress
                                End If
                                If Config.ParamaterWithDefault("PSPBRNCH", "ALL/BLANK") = "DEFAULT" Then
                                    oOOForm.BrancheCode = sBranch
                                End If
                                oOOForm.ItemGroup = sItemGrp
                                'oOOForm.ContainerCode = sItemCode
                                If Config.ParamaterWithDefault("PSPCNCOD", "DEFAULT") = "DEFAULT" Then
                                    oOOForm.ContainerCode = sItemCode
                                End If
                                'oOOForm.WasteCode = sWastCd
                                If Config.ParamaterWithDefault("PSPWSTCD", "DEFAULT") = "DEFAULT" Then
                                    oOOForm.WasteCode = sWastCd
                                End If
                                '''''oOOForm.JobType = sJobType
                                If Config.ParamaterWithDefault("PSPORDTP", "ALL/BLANK") = "DEFAULT" Then
                                    oOOForm.JobType = sJobType
                                End If
                                'oOOForm.WR1OrderType = "WO"
                                If Config.ParamaterWithDefault("PSPWR1TP", "DEFAULT") = "DEFAULT" Then
                                    oOOForm.WR1OrderType = "WO"
                                End If
                                oOOForm.FilterMode = 1
                                'oOOForm.FilterSet = WR1_FR2Forms.idh.forms.fr2.prices.CIP.WORADD

                                oOOForm.doShowModal()
                            ElseIf pVal.ItemUID = "IDH_SUBCON" Then
                                Dim sCardCode As String = getFormDFValue(oForm, "U_IDHCCODE")

                                If Not sCardCode Is Nothing AndAlso sCardCode.Length > 0 Then
                                    setSharedData(oForm, "IDH_ZPCDTX", "")
                                    setSharedData(oForm, "IDH_ITEM", "")
                                    setSharedData(oForm, "IDH_WSCD", "")
                                    setSharedData(oForm, "IDH_RTNG", "")

                                    setSharedData(oForm, "IDH_CUSCOD", sCardCode)
                                    goParent.doOpenModalForm("IDH_SUBBY", oForm)
                                End If
                                'Start OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
                            ElseIf pVal.ItemUID = "IDHROUTE" Then
                                goParent.doOpenModalForm("IDHRTADM2", oForm)
                                'End OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
                            ElseIf sItemId = "IDH_ROWHTR" Then '#MA  start 20170516
                                setSharedData(oForm, "TABLE", "@IDH_PBI")
                                setSharedData(oForm, "CODE", getItemValue(oForm, "CODE"))
                                goParent.doOpenModalForm("IDH_HIST", oForm)
                                '#MA  end 20170516
                            End If
                            If sItemId.Equals("IDH_AUEND") Then '#MA issue:406 20170619
                                If getFormDFValue(oForm, "U_AutEnd") = "Y" Then
                                    setFormDFValue(oForm, "U_AUTSTRT", "Y")
                                End If
                            End If
                            If sItemId.Equals("IDH_AUSTRT") Then
                                setWFValue(oForm, "AUTSTRTPRS", True)
                            End If
                            '#MA end 20170619 
                        Case SAPbouiCOM.BoEventTypes.et_CLICK
                            If sItemId = "lnk_WO" Then
                                Try
                                    Dim sJobEntr As String = ""
                                    sJobEntr = getFormDFValue(oForm, "U_IDHWO")

                                    Dim oOrder As com.isb.core.forms.WasteOrder = New com.isb.core.forms.WasteOrder(oForm.UniqueID, sJobEntr)
                                    'oOrder.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WOROKReturn)
                                    'oOrder.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WORCancelReturn)

                                    oOrder.doShowModal()
                                Catch Ex As Exception
                                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD001", Nothing)
                                End Try

                                'Dim oData As New ArrayList
                                'Dim Val As String = ""
                                'Val = getFormDFValue(oForm, "U_IDHWO")
                                'oData.Add("IDHWO")
                                'oData.Add(Val)
                                'goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)

                                'oData = Nothing
                            ElseIf sItemId = "lnk_WOR" Then
                                Try
                                    Dim sRowCode As String = ""
                                    sRowCode = getFormDFValue(oForm, "U_IDHWOR")

                                    Dim oOrderRow As com.isb.core.forms.OrderRow = New com.isb.core.forms.OrderRow(oForm.UniqueID, sRowCode)
                                    'oOrderRow.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WOROKReturn)
                                    'oOrderRow.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WORCancelReturn)

                                    oOrderRow.doShowModal()
                                Catch Ex As Exception
                                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD001", Nothing)
                                End Try

                                'Dim oData As New ArrayList
                                'Dim sWOR As String = ""
                                'sWOR = getFormDFValue(oForm, "U_IDHWOR")
                                'oData.Add("IDHWO")
                                'oData.Add(sWOR)

                                'goParent.doOpenModalForm("IDHJOBS", oForm, oData)
                                'oData = Nothing
                            ElseIf sItemId = "IDHRENDO" OrElse sItemId = "IDHRENDD" Then
                                'Dim oOccOpt, oDtOpt As SAPbouiCOM.OptionBtn

                                If sItemId = "IDHRENDO" Then
                                    setEnableItem(oForm, True, "IDHRECCE")
                                    setEnableItem(oForm, False, "IDHRECED")

                                    setFormDFValue(oForm, "U_IDHRECED", Nothing)
                                    setFormDFValue(oForm, "U_IDHRECCE", "1")

                                    setFormDFValue(oForm, "U_IDHRECEN", 0)
                                Else
                                    setEnableItem(oForm, False, "IDHRECCE")
                                    setEnableItem(oForm, True, "IDHRECED")

                                    Dim dStartDate As DateTime = Dates.doStrToDate(getFormDFValue(oForm, "U_IDHRECSD"))
                                    Dim dStopDate As DateTime = dStartDate.AddYears(10)

                                    setFormDFValue(oForm, "U_IDHRECED", goParent.doDateToStr(dStopDate))
                                    setFormDFValue(oForm, "U_IDHRECCE", "0")
                                End If
                                doSwitchActionDate(oForm)
                                doSetUpdate(oForm)
                                'OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
                                'The following statements are executed later
                                'ElseIf sItemId = "IDHRDMON" OrElse _
                                '    sItemId = "IDHRDTUE" OrElse _
                                '    sItemId = "IDHRDWED" OrElse _
                                '    sItemId = "IDHRDTHU" OrElse _
                                '    sItemId = "IDHRDFRI" OrElse _
                                '    sItemId = "IDHRDSAT" OrElse _
                                '    sItemId = "IDHRDSUN" Then
                                '    doSwitchActionDate(oForm)
                            ElseIf sItemId = "IDH_DETAIL" Then
                                Dim sDoDetail As String = getFormDFValue(oForm, "U_IDHDETAI")
                                If sDoDetail = "N" Then
                                    setFormDFValue(oForm, "U_IDHDETIN", "N")
                                End If
                            ElseIf sItemId = "IDH_DETINV" Then
                                Dim sDoDetail As String = getFormDFValue(oForm, "U_IDHDETIN")
                                If sDoDetail = "N" Then
                                    setFormDFValue(oForm, "U_IDHDETAI", "N")
                                End If
                            ElseIf sItemId = "IDH_UMONE" Then
                                Dim sDoMONE As String = getFormDFValue(oForm, "U_IDHUMONE")
                                '                                    If sDoMONE = "N" Then
                                '                                    End If
                            ElseIf sItemId = "IDHOPTDAY" OrElse sItemId = "IDHOPTWK" Then
                                oForm.Items.Item("lblWkRecur").Specific.Caption = getTranslatedWord("week(s) on:")
                                oForm.Items.Item("IDHRDMON").Enabled = True
                                oForm.Items.Item("IDHRDTUE").Enabled = True
                                oForm.Items.Item("IDHRDWED").Enabled = True
                                oForm.Items.Item("IDHRDTHU").Enabled = True
                                oForm.Items.Item("IDHRDFRI").Enabled = True
                                oForm.Items.Item("IDHRDSAT").Enabled = True
                                oForm.Items.Item("IDHRDSUN").Enabled = True

                                setFormDFValue(oForm, "U_IDHRACTW", 1)
                                doSetFocus(oForm, "IDHWCICD")        '#MA Changed Focus When Focus on IDHRACTW Field
                                If sItemId = "IDHOPTDAY" Then
                                    setFormDFValue(oForm, IDH_PBI._IDHRDMON2, "Y")
                                    setFormDFValue(oForm, IDH_PBI._IDHRDTUE2, "Y")
                                    setFormDFValue(oForm, IDH_PBI._IDHRDWED2, "Y")
                                    setFormDFValue(oForm, IDH_PBI._IDHRDTHU2, "Y")
                                    setFormDFValue(oForm, IDH_PBI._IDHRDFRI2, "Y")
                                    setFormDFValue(oForm, IDH_PBI._IDHRDSAT2, "N")
                                    setFormDFValue(oForm, IDH_PBI._IDHRDSUN2, "N")
                                    oForm.Items.Item("IDHRACTW").Enabled = False
                                Else
                                    oForm.Items.Item("IDHRACTW").Enabled = True
                                End If
                                If getFormDFValue(oForm, IDH_PBI._IDHRT) = "Y" Then
                                    If getFormDFValue(oForm, IDH_PBI._IDHRDMON2) = "Y" Then
                                        oForm.Items.Item("IDHRCDM").Visible = True
                                        oForm.Items.Item("IDHRSQM").Visible = True 'new function Route Sequence
                                    Else
                                        oForm.Items.Item("IDHRCDM").Visible = False
                                        oForm.Items.Item("IDHRSQM").Visible = False 'new function Route Sequence
                                    End If
                                    If getFormDFValue(oForm, IDH_PBI._IDHRDTUE2) = "Y" Then
                                        oForm.Items.Item("IDHRCDTU").Visible = True
                                        oForm.Items.Item("IDHRSQTU").Visible = True 'new function Route Sequence
                                    Else
                                        oForm.Items.Item("IDHRCDTU").Visible = False
                                        oForm.Items.Item("IDHRSQTU").Visible = False 'new function Route Sequence
                                    End If
                                    If getFormDFValue(oForm, IDH_PBI._IDHRDWED2) = "Y" Then
                                        oForm.Items.Item("IDHRCDW").Visible = True
                                        oForm.Items.Item("IDHRSQW").Visible = True 'new function Route Sequence
                                    Else
                                        oForm.Items.Item("IDHRCDW").Visible = False
                                        oForm.Items.Item("IDHRSQW").Visible = False 'new function Route Sequence
                                    End If
                                    If getFormDFValue(oForm, IDH_PBI._IDHRDTHU2) = "Y" Then
                                        oForm.Items.Item("IDHRCDTH").Visible = True
                                        oForm.Items.Item("IDHRSQTH").Visible = True 'new function Route Sequence
                                    Else
                                        oForm.Items.Item("IDHRCDTH").Visible = False
                                        oForm.Items.Item("IDHRSQTH").Visible = False 'new function Route Sequence
                                    End If
                                    If getFormDFValue(oForm, IDH_PBI._IDHRDFRI2) = "Y" Then
                                        oForm.Items.Item("IDHRCDF").Visible = True
                                        oForm.Items.Item("IDHRSQF").Visible = True 'new function Route Sequence
                                    Else
                                        oForm.Items.Item("IDHRCDF").Visible = False
                                        oForm.Items.Item("IDHRSQF").Visible = False 'new function Route Sequence
                                    End If
                                    If getFormDFValue(oForm, IDH_PBI._IDHRDSAT2) = "Y" Then
                                        oForm.Items.Item("IDHRCDSA").Visible = True
                                        oForm.Items.Item("IDHRSQSA").Visible = True 'new function Route Sequence
                                    Else
                                        oForm.Items.Item("IDHRCDSA").Visible = False
                                        oForm.Items.Item("IDHRSQSA").Visible = False 'new function Route Sequence
                                    End If
                                    If getFormDFValue(oForm, IDH_PBI._IDHRDSUN2) = "Y" Then
                                        oForm.Items.Item("IDHRCDSU").Visible = True
                                        oForm.Items.Item("IDHRSQSU").Visible = True 'new function Route Sequence
                                    Else
                                        oForm.Items.Item("IDHRCDSU").Visible = False
                                        oForm.Items.Item("IDHRSQSU").Visible = False 'new function Route Sequence
                                    End If
                                End If
                                doSwitchActionDate(oForm)
                                If Not oForm.PaneLevel = 1 Then
                                    oForm.PaneLevel = 1
                                End If
                                'OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
                                If bRouting = True Then
                                    oForm.Items.Item("114").Visible = True
                                    oForm.Items.Item("IDHRT").Visible = True
                                Else
                                    oForm.Items.Item("114").Visible = False
                                    oForm.Items.Item("IDHRT").Visible = False
                                End If
                            ElseIf sItemId = "IDHOPTMT" Then
                                oForm.Items.Item("lblWkRecur").Specific.Caption = getTranslatedWord(("month(s) on:"))
                                oForm.Items.Item("IDHRACTW").Enabled = True   '#MA Enable Recur Field When Monthly RadioButton is Tick
                                oForm.Items.Item("IDHRDMON").Enabled = False
                                oForm.Items.Item("IDHRDTUE").Enabled = False
                                oForm.Items.Item("IDHRDWED").Enabled = False
                                oForm.Items.Item("IDHRDTHU").Enabled = False
                                oForm.Items.Item("IDHRDFRI").Enabled = False
                                oForm.Items.Item("IDHRDSAT").Enabled = False
                                oForm.Items.Item("IDHRDSUN").Enabled = False
                                'OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
                                '#MA Start 15-06-2017 point#144
                                oForm.Items.Item("114").Visible = True     '#MA Start 15-06-2017 Point#144  visable Routable CheckBox When Monthly RadioButton is Tick
                                oForm.Items.Item("IDHRT").Visible = True
                                doSetFocus(oForm, "IDHRACTW")               '#MA SetFocus on IDHRACTW field
                                If getFormDFValue(oForm, "U_IDHRT") = "Y" Then
                                    oForm.Items.Item("IDHRCDM").Visible = True
                                    oForm.Items.Item("IDHRSQM").Visible = True
                                    oForm.Items.Item("IDHRCDTU").Visible = True
                                    oForm.Items.Item("IDHRSQTU").Visible = True
                                    oForm.Items.Item("IDHRCDW").Visible = True
                                    oForm.Items.Item("IDHRSQW").Visible = True
                                    oForm.Items.Item("IDHRCDTH").Visible = True
                                    oForm.Items.Item("IDHRSQTH").Visible = True
                                    oForm.Items.Item("IDHRCDF").Visible = True
                                    oForm.Items.Item("IDHRSQF").Visible = True
                                    oForm.Items.Item("IDHRCDSA").Visible = True
                                    oForm.Items.Item("IDHRSQSA").Visible = True
                                    oForm.Items.Item("IDHRCDSU").Visible = True
                                    oForm.Items.Item("IDHRSQSU").Visible = True

                                Else
                                    oForm.Items.Item("IDHRCDM").Visible = False
                                    oForm.Items.Item("IDHRCDTU").Visible = False
                                    oForm.Items.Item("IDHRCDW").Visible = False
                                    oForm.Items.Item("IDHRCDTH").Visible = False
                                    oForm.Items.Item("IDHRCDF").Visible = False
                                    oForm.Items.Item("IDHRCDSA").Visible = False
                                    oForm.Items.Item("IDHRCDSU").Visible = False
                                    oForm.Items.Item("IDHCMT").Visible = False
                                    oForm.Items.Item("IDHDRVI").Visible = False
                                    oForm.Items.Item("IDHROUTE").Visible = False 'this is the Route Button
                                    oForm.Items.Item("IDHRSQM").Visible = False 'new function Route Sequence
                                    oForm.Items.Item("IDHRSQTU").Visible = False 'new function Route Sequence
                                    oForm.Items.Item("IDHRSQW").Visible = False 'new function Route Sequence
                                    oForm.Items.Item("IDHRSQTH").Visible = False 'new function Route Sequence
                                    oForm.Items.Item("IDHRSQF").Visible = False 'new function Route Sequence
                                    oForm.Items.Item("IDHRSQSA").Visible = False 'new function Route Sequence
                                    oForm.Items.Item("IDHRSQSU").Visible = False 'new function Route Sequence
                                End If
                                '#MA End 15-06-2017
                                setFormDFValue(oForm, "U_IDHRACTW", 1)
                                oForm.Items.Item("IDHRACTW").Enabled = True

                                setFormDFValue(oForm, IDH_PBI._IDHRDMON2, "N")
                                setFormDFValue(oForm, IDH_PBI._IDHRDTUE2, "N")
                                setFormDFValue(oForm, IDH_PBI._IDHRDWED2, "N")
                                setFormDFValue(oForm, IDH_PBI._IDHRDTHU2, "N")
                                setFormDFValue(oForm, IDH_PBI._IDHRDFRI2, "N")
                                setFormDFValue(oForm, IDH_PBI._IDHRDSAT2, "N")
                                setFormDFValue(oForm, IDH_PBI._IDHRDSUN2, "N")
                                doSwitchActionDate(oForm)
                                If Not oForm.PaneLevel = 1 Then
                                    oForm.PaneLevel = 1
                                End If
                            ElseIf sItemId = "IDHOPTYR" Then
                                oForm.Items.Item("lblWkRecur").Specific.Caption = getTranslatedWord("year(s) on:")
                                oForm.Items.Item("IDHRDMON").Enabled = False
                                oForm.Items.Item("IDHRDTUE").Enabled = False
                                oForm.Items.Item("IDHRDWED").Enabled = False
                                oForm.Items.Item("IDHRDTHU").Enabled = False
                                oForm.Items.Item("IDHRDFRI").Enabled = False
                                oForm.Items.Item("IDHRDSAT").Enabled = False
                                oForm.Items.Item("IDHRDSUN").Enabled = False
                                'OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
                                oForm.Items.Item("114").Visible = False
                                oForm.Items.Item("IDHRT").Visible = False
                                oForm.Items.Item("IDHRCDM").Visible = False
                                oForm.Items.Item("IDHRCDTU").Visible = False
                                oForm.Items.Item("IDHRCDW").Visible = False
                                oForm.Items.Item("IDHRCDTH").Visible = False
                                oForm.Items.Item("IDHRCDF").Visible = False
                                oForm.Items.Item("IDHRCDSA").Visible = False
                                oForm.Items.Item("IDHRCDSU").Visible = False
                                oForm.Items.Item("IDHCMT").Visible = False
                                oForm.Items.Item("IDHDRVI").Visible = False
                                oForm.Items.Item("IDHROUTE").Visible = False 'this is the Route Button
                                oForm.Items.Item("IDHRSQM").Visible = False 'new function Route Sequence
                                oForm.Items.Item("IDHRSQTU").Visible = False 'new function Route Sequence
                                oForm.Items.Item("IDHRSQW").Visible = False 'new function Route Sequence
                                oForm.Items.Item("IDHRSQTH").Visible = False 'new function Route Sequence
                                oForm.Items.Item("IDHRSQF").Visible = False 'new function Route Sequence
                                oForm.Items.Item("IDHRSQSA").Visible = False 'new function Route Sequence
                                oForm.Items.Item("IDHRSQSU").Visible = False 'new function Route Sequence

                                setFormDFValue(oForm, "U_IDHRACTW", 1)
                                oForm.Items.Item("IDHRACTW").Enabled = True

                                setFormDFValue(oForm, IDH_PBI._IDHRDMON2, "N")
                                setFormDFValue(oForm, IDH_PBI._IDHRDTUE2, "N")
                                setFormDFValue(oForm, IDH_PBI._IDHRDWED2, "N")
                                setFormDFValue(oForm, IDH_PBI._IDHRDTHU2, "N")
                                setFormDFValue(oForm, IDH_PBI._IDHRDFRI2, "N")
                                setFormDFValue(oForm, IDH_PBI._IDHRDSAT2, "N")
                                setFormDFValue(oForm, IDH_PBI._IDHRDSUN2, "N")
                                doSwitchActionDate(oForm)
                                If Not oForm.PaneLevel = 3 Then
                                    oForm.PaneLevel = 3
                                End If
                            ElseIf sItemId = "IDHRT" Then
                                'be careful: at this stage the value of the field is opposite. It will change later.
                                Dim oCheck As SAPbouiCOM.CheckBox = oForm.Items.Item("IDHRT").Specific
                                If Not oCheck.Checked Then
                                    oForm.Items.Item("IDHROUTE").Visible = True  'this is the Route Button
                                    '#MA Start 15-06-2017 point144'
                                    If getFormDFValue(oForm, "U_IDHRECUR") = "M" Then
                                        oForm.Items.Item("IDHRCDM").Visible = True
                                        oForm.Items.Item("IDHRSQM").Visible = True 'new function Route Sequence
                                        oForm.Items.Item("IDHRCDTU").Visible = True
                                        oForm.Items.Item("IDHRSQTU").Visible = True 'new function Route Sequence
                                        oForm.Items.Item("IDHRCDW").Visible = True
                                        oForm.Items.Item("IDHRSQW").Visible = True 'new function Route Sequence
                                        oForm.Items.Item("IDHRCDTH").Visible = True
                                        oForm.Items.Item("IDHRSQTH").Visible = True 'new function Route Sequence
                                        oForm.Items.Item("IDHRCDF").Visible = True
                                        oForm.Items.Item("IDHRSQF").Visible = True 'new function Route Sequence
                                        oForm.Items.Item("IDHRCDSA").Visible = True
                                        oForm.Items.Item("IDHRSQSA").Visible = True 'new function Route Sequence
                                        oForm.Items.Item("IDHRCDSU").Visible = True
                                        oForm.Items.Item("IDHRSQSU").Visible = True 'new function Route Sequence

                                    Else
                                        If getFormDFValue(oForm, IDH_PBI._IDHRDMON2) = "Y" Then
                                            oForm.Items.Item("IDHRCDM").Visible = True
                                            oForm.Items.Item("IDHRSQM").Visible = True 'new function Route Sequence
                                        Else
                                            oForm.Items.Item("IDHRCDM").Visible = False
                                            oForm.Items.Item("IDHRSQM").Visible = False 'new function Route Sequence
                                        End If

                                        If getFormDFValue(oForm, IDH_PBI._IDHRDTUE2) = "Y" Then
                                            oForm.Items.Item("IDHRCDTU").Visible = True
                                            oForm.Items.Item("IDHRSQTU").Visible = True 'new function Route Sequence

                                        Else
                                            oForm.Items.Item("IDHRCDTU").Visible = False
                                            oForm.Items.Item("IDHRSQTU").Visible = False 'new function Route Sequence
                                        End If

                                        If getFormDFValue(oForm, IDH_PBI._IDHRDWED2) = "Y" Then
                                            oForm.Items.Item("IDHRCDW").Visible = True
                                            oForm.Items.Item("IDHRSQW").Visible = True 'new function Route Sequence
                                        Else
                                            oForm.Items.Item("IDHRCDW").Visible = False
                                            oForm.Items.Item("IDHRSQW").Visible = False 'new function Route Sequence
                                        End If

                                        If getFormDFValue(oForm, IDH_PBI._IDHRDTHU2) = "Y" Then
                                            oForm.Items.Item("IDHRCDTH").Visible = True
                                            oForm.Items.Item("IDHRSQTH").Visible = True 'new function Route Sequence
                                        Else
                                            oForm.Items.Item("IDHRCDTH").Visible = False
                                            oForm.Items.Item("IDHRSQTH").Visible = False 'new function Route Sequence
                                        End If

                                        If getFormDFValue(oForm, IDH_PBI._IDHRDFRI2) = "Y" Then
                                            oForm.Items.Item("IDHRCDF").Visible = True
                                            oForm.Items.Item("IDHRSQF").Visible = True 'new function Route Sequence
                                        Else
                                            oForm.Items.Item("IDHRCDF").Visible = False
                                            oForm.Items.Item("IDHRSQF").Visible = False 'new function Route Sequence
                                        End If

                                        If getFormDFValue(oForm, IDH_PBI._IDHRDSAT2) = "Y" Then
                                            oForm.Items.Item("IDHRCDSA").Visible = True
                                            oForm.Items.Item("IDHRSQSA").Visible = True 'new function Route Sequence
                                        Else
                                            oForm.Items.Item("IDHRCDSA").Visible = False
                                            oForm.Items.Item("IDHRSQSA").Visible = False 'new function Route Sequence
                                        End If

                                        If getFormDFValue(oForm, IDH_PBI._IDHRDSUN2) = "Y" Then
                                            oForm.Items.Item("IDHRCDSU").Visible = True
                                            oForm.Items.Item("IDHRSQSU").Visible = True 'new function Route Sequence
                                        Else
                                            oForm.Items.Item("IDHRCDSU").Visible = False
                                            oForm.Items.Item("IDHRSQSU").Visible = False 'new function Route Sequence
                                        End If
                                        oForm.Items.Item("IDHRECOM").Height = 66
                                        oForm.Items.Item("IDHCMT").Visible = True
                                        oForm.Items.Item("IDHDRVI").Visible = True
                                    End If
                                    '#MA End 15-06-2017
                                Else
                                    Try
                                        doSetFocus(oForm, "IDHWCICD")               '#MA SetFocus on IDHWCICD field
                                        oForm.Items.Item("IDHRCDM").Visible = False
                                        oForm.Items.Item("IDHRCDTU").Visible = False
                                        oForm.Items.Item("IDHRCDW").Visible = False
                                        oForm.Items.Item("IDHRCDTH").Visible = False
                                        oForm.Items.Item("IDHRCDF").Visible = False
                                        oForm.Items.Item("IDHRCDSA").Visible = False
                                        oForm.Items.Item("IDHRCDSU").Visible = False
                                        oForm.Items.Item("IDHCMT").Visible = False
                                        oForm.Items.Item("IDHDRVI").Visible = False
                                        oForm.Items.Item("IDHRECOM").Height = 150
                                        oForm.Items.Item("IDHROUTE").Visible = False 'this is the Route Button
                                        oForm.Items.Item("IDHRSQM").Visible = False 'new function Route Sequence
                                        oForm.Items.Item("IDHRSQTU").Visible = False 'new function Route Sequence
                                        oForm.Items.Item("IDHRSQW").Visible = False 'new function Route Sequence
                                        oForm.Items.Item("IDHRSQTH").Visible = False 'new function Route Sequence
                                        oForm.Items.Item("IDHRSQF").Visible = False 'new function Route Sequence
                                        oForm.Items.Item("IDHRSQSA").Visible = False 'new function Route Sequence
                                        oForm.Items.Item("IDHRSQSU").Visible = False 'new function Route Sequence
                                    Catch ex As Exception
                                    End Try
                                End If
                            ElseIf sItemId = "IDHRDMON" Then
                                doSwitchActionDate(oForm)
                                If getFormDFValue(oForm, "U_IDHRT") = "Y" Then
                                    Try
                                        If getFormDFValue(oForm, IDH_PBI._IDHRDMON2) = "N" Then
                                            oForm.Items.Item("IDHRCDM").Visible = True
                                            oForm.Items.Item("IDHRSQM").Visible = True 'new function Route Sequence
                                        Else
                                            oForm.Items.Item("IDHRCDM").Visible = False
                                            oForm.Items.Item("IDHRSQM").Visible = False 'new function Route Sequence
                                        End If
                                    Catch ex As Exception
                                    End Try
                                End If
                            ElseIf sItemId = "IDHRDTUE" Then
                                doSwitchActionDate(oForm)
                                If getFormDFValue(oForm, "U_IDHRT") = "Y" Then
                                    Try
                                        If getFormDFValue(oForm, IDH_PBI._IDHRDTUE2) = "N" Then
                                            oForm.Items.Item("IDHRCDTU").Visible = True
                                            oForm.Items.Item("IDHRSQTU").Visible = True 'new function Route Sequence
                                        Else
                                            oForm.Items.Item("IDHRCDTU").Visible = False
                                            oForm.Items.Item("IDHRSQTU").Visible = False 'new function Route Sequence
                                        End If
                                    Catch ex As Exception
                                    End Try
                                End If
                            ElseIf sItemId = "IDHRDWED" Then
                                doSwitchActionDate(oForm)
                                If getFormDFValue(oForm, "U_IDHRT") = "Y" Then
                                    Try
                                        If getFormDFValue(oForm, IDH_PBI._IDHRDWED2) = "N" Then
                                            oForm.Items.Item("IDHRCDW").Visible = True
                                            oForm.Items.Item("IDHRSQW").Visible = True 'new function Route Sequence
                                        Else
                                            oForm.Items.Item("IDHRCDW").Visible = False
                                            oForm.Items.Item("IDHRSQW").Visible = False 'new function Route Sequence
                                        End If
                                    Catch ex As Exception
                                    End Try
                                End If
                            ElseIf sItemId = "IDHRDTHU" Then
                                doSwitchActionDate(oForm)
                                If getFormDFValue(oForm, "U_IDHRT") = "Y" Then
                                    Try
                                        If getFormDFValue(oForm, IDH_PBI._IDHRDTHU2) = "N" Then
                                            oForm.Items.Item("IDHRCDTH").Visible = True
                                            oForm.Items.Item("IDHRSQTH").Visible = True 'new function Route Sequence
                                        Else
                                            oForm.Items.Item("IDHRCDTH").Visible = False
                                            oForm.Items.Item("IDHRSQTH").Visible = False 'new function Route Sequence
                                        End If
                                    Catch ex As Exception
                                    End Try
                                End If
                            ElseIf sItemId = "IDHRDFRI" Then
                                doSwitchActionDate(oForm)
                                If getFormDFValue(oForm, "U_IDHRT") = "Y" Then
                                    Try
                                        If getFormDFValue(oForm, IDH_PBI._IDHRDFRI2) = "N" Then
                                            oForm.Items.Item("IDHRCDF").Visible = True
                                            oForm.Items.Item("IDHRSQF").Visible = True 'new function Route Sequence
                                        Else
                                            oForm.Items.Item("IDHRCDF").Visible = False
                                            oForm.Items.Item("IDHRSQF").Visible = False 'new function Route Sequence
                                        End If
                                    Catch ex As Exception
                                    End Try
                                End If
                            ElseIf sItemId = "IDHRDSAT" Then
                                doSwitchActionDate(oForm)
                                If getFormDFValue(oForm, "U_IDHRT") = "Y" Then
                                    Try
                                        If getFormDFValue(oForm, IDH_PBI._IDHRDSAT2) = "N" Then
                                            oForm.Items.Item("IDHRCDSA").Visible = True
                                            oForm.Items.Item("IDHRSQSA").Visible = True 'new function Route Sequence
                                        Else
                                            oForm.Items.Item("IDHRCDSA").Visible = False
                                            oForm.Items.Item("IDHRSQSA").Visible = False 'new function Route Sequence
                                        End If
                                    Catch ex As Exception
                                    End Try
                                End If
                            ElseIf sItemId = "IDHRDSUN" Then
                                doSwitchActionDate(oForm)
                                If getFormDFValue(oForm, "U_IDHRT") = "Y" Then
                                    Try
                                        If getFormDFValue(oForm, IDH_PBI._IDHRDSUN2) = "N" Then
                                            oForm.Items.Item("IDHRCDSU").Visible = True
                                            oForm.Items.Item("IDHRSQSU").Visible = True 'new function Route Sequence
                                        Else
                                            oForm.Items.Item("IDHRCDSU").Visible = False
                                            oForm.Items.Item("IDHRSQSU").Visible = False 'new function Route Sequence
                                        End If
                                    Catch ex As Exception
                                    End Try
                                End If
                                'End additional functionality Joachim Alleritz =================================
                            End If 'sItemID
                    End Select 'pVal.EventType
            End Select 'pVal.BeforeAction
            Return False
        End Function

        'Closing a Modal Form Retrieving Data from the Shared Buffer
        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form,
         ByVal sModalFormType As String,
         Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType.Equals("IDHASRCH") Then
                    Dim sTrg As String = getSharedData(oForm, "TRG")
                    If sTrg = "SA" Then
                        'setDFValue(oForm, "@IDH_PBI", "U_IDHSADDR", getSharedData(oForm, "ADDRESS") & ", " & getSharedData(oForm, "Street") & "," & getSharedData(oForm, "Block") & "," & getSharedData(oForm, "City"), True)
                        setFormDFValue(oForm, "U_IDHSADDR", getSharedData(oForm, "ADDRESS"), True)
                        setFormDFValue(oForm, "U_IDHSADLN", getSharedData(oForm, "ADDRSCD"), True)

                        '##MA Start 14-04-2017' issue#163
                        doCheckonSite(oForm)
                        '##MA End 14-04-2017'

                        '#MA Start 17-05-2017 issue#402 point 1&2
                        Dim oBPSiteAddress As BPAddress = New BPAddress()
                        If Convert.ToBoolean(oBPSiteAddress.doGetAddress(getFormDFValue(oForm, "U_IDHCCODE"), "S", False, getFormDFValue(oForm, "U_IDHSADDR"))) Then
                            If getFormDFValue(oForm, "U_IDHSADDR").ToString.Length > 0 AndAlso Not getFormDFValue(oForm, "U_IDHSADDR").ToString() = "" Then
                                setFormDFValue(oForm, "U_LATI", oBPSiteAddress.U_LAT.ToString())
                                setFormDFValue(oForm, "U_LONGI", oBPSiteAddress.U_LONG.ToString())
                            End If
                        End If
                        '#MA End 17-05-2017
                        addressLine = getSharedData(oForm, "ADDRSCD").ToString()
                        If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE And oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            doSetUpdate(oForm)
                        End If
                    ElseIf sTrg = "DA" Then
                        setFormDFValue(oForm, "U_IDHDADDR", getSharedData(oForm, "ADDRESS"), True)
                        setFormDFValue(oForm, "U_IDHDADLN", getSharedData(oForm, "ADDRSCD"), True)
                        If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE And oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            doSetUpdate(oForm)
                        End If
                    End If
                ElseIf sModalFormType = "IDHCREF" Then
                    Dim sVal As String = getSharedData(oForm, "CUSTREF")
                    setFormDFValue(oForm, IDH_PBI._CustRef, sVal, False, True)
                ElseIf sModalFormType = "IDHJTSRC" Then
                    setFormDFValue(oForm, "U_IDHJTYPE", getSharedData(oForm, "ORDTYPE"), False, True)
                    If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE And oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        doSetUpdate(oForm)
                    End If
                ElseIf sModalFormType = "IDHISRC" Then
                    If getSharedData(oForm, "TRG") = "CON" Then
                        '#MA 2-7-2016
                        If getSharedData(oForm, "ITEMCODE").ToString.Length = 0 Then 'To ignore invalid value
                            setFormDFValue(oForm, "U_IDHCOICD", "", False, True)
                            setFormDFValue(oForm, "U_IDHCOIDC", "", False, True)
                        Else
                        setFormDFValue(oForm, "U_IDHCOICD", getSharedData(oForm, "ITEMCODE"), False, True)
                        setFormDFValue(oForm, "U_IDHCOIDC", getSharedData(oForm, "ITEMNAME"), False, True)
                        End If
                        '#End 2-7-2016
                        If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE And oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            doSetUpdate(oForm)
                        End If
                    End If

                ElseIf sModalFormType = "IDHCSRCH" Then
                    Dim sTarget As String = getSharedData(oForm, "TRG")
                    Dim code As String = getSharedData(oForm, "CARDCODE")
                    Dim desc As String = getSharedData(oForm, "CARDNAME")

                    If Not (code Is Nothing) AndAlso code.Length > 0 Then
                        If sTarget = "CUS" Then
                            If Not doSetChoosenCustomerFromShared(oForm) = False Then '#MA point:386 20170609
                            setFormDFValue(oForm, "U_IDHCCODE", code, True)
                            setFormDFValue(oForm, "U_IDHCNAME", desc, True)

                            setFormDFValue(oForm, "U_IDHOBLGT", getSharedData(oForm, "IDHOBLGT"))
                            setFormDFValue(oForm, "U_IDHSADDR", "", True)
                            setFormDFValue(oForm, "U_IDHSADLN", "", True)
                                '##MA Start 14-04-2017 issue#163 if Change BP so set checked false '
                                setFormDFValue(oForm, "U_IDHPOS", "N")
                                '##MA End 14-04-2017'
                            Else
                                setFormDFValue(oForm, "U_IDHCCODE", "", True)
                                setFormDFValue(oForm, "U_IDHCNAME", "", True)

                                setFormDFValue(oForm, "U_IDHOBLGT", "")
                                setFormDFValue(oForm, "U_IDHSADDR", "", True)
                                setFormDFValue(oForm, "U_IDHSADLN", "", True)
                                '#MA point:386 end 20170609
                            End If
                        ElseIf sTarget = "CAR" Then
                            setFormDFValue(oForm, "U_IDHCARCD", code, True)
                            setFormDFValue(oForm, "U_IDHCARDC", desc, True)
                            ''##MA Start 12-04-2017 issue#384      Check Config value is true or false
                            If Not Config.INSTANCE.getParameterAsBool("PBIDPST") Then
                            Dim sDisp As String = getFormDFValue(oForm, "U_IDHDISCD")
                            If sDisp.Length = 0 Then
                                setFormDFValue(oForm, "U_IDHDISCD", code, True)
                                setFormDFValue(oForm, "U_IDHDISDC", desc, True)
                                End If
                            End If
                            ''##MA End 12-04-2017
                                setFormDFValue(oForm, "U_IDHDADDR", "", True)
                                setFormDFValue(oForm, "U_IDHDADLN", "", True)
                        ElseIf sTarget = "DIS" Then
                            setFormDFValue(oForm, "U_IDHDISCD", code, True)
                            setFormDFValue(oForm, "U_IDHDISDC", desc, True)
                            setFormDFValue(oForm, "U_IDHDADDR", "", True)
                            setFormDFValue(oForm, "U_IDHDADLN", "", True)
                        End If
                        If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE And oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            doSetUpdate(oForm)
                        End If
                    End If
                ElseIf sModalFormType = "IDHWISRC" Then
                    setFormDFValue(oForm, "U_IDHWCICD", getSharedData(oForm, "ITEMCODE"), False, True)
                    setFormDFValue(oForm, "U_IDHWCIDC", getSharedData(oForm, "ITEMNAME"), False, True)


                    '#MA  Start 10-05-2017 Issue#402   
                    'setFormDFValue(oForm, "U_WasItmGrp", getSharedData(oForm, "IDH_GRPCOD"), False, True)
                    If getFormDFValue(oForm, "U_IDHWCICD") IsNot Nothing AndAlso getFormDFValue(oForm, "U_IDHWCICD").ToString().Trim() <> String.Empty Then
                        Dim objItemGrpCode As Object = Config.INSTANCE.getValueFromOITM(getFormDFValue(oForm, "U_IDHWCICD").ToString(), "ItmsGrpCod")
                        If objItemGrpCode IsNot Nothing AndAlso objItemGrpCode.ToString() <> String.Empty Then
                            setFormDFValue(oForm, "U_WasItmGrp", objItemGrpCode.ToString)
                        End If
                    End If
                    'Dim WasOItm As OITM = New OITM()
                    'WasOItm.getByField(OITM._ItemCode, getFormDFValue(oForm, "U_IDHWCICD"))
                    'setFormDFValue(oForm, "U_WasItmGrp", WasOItm.ItmsGrpCod.ToString)
                    '#MA End 10-05-2017


                    Dim sCanPurchase As String = getSharedData(oForm, "CWBB")
                    If sCanPurchase IsNot Nothing AndAlso sCanPurchase.Equals("Y") Then
                        setEnableItem(oForm, True, "IDH_REBATE")
                    Else
                        setEnableItem(oForm, False, "IDH_REBATE")
                    End If

                    If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE And oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        doSetUpdate(oForm)
                    End If
                    'OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
                    'Start additional functionality Joachim Alleritz =================================
                ElseIf sModalFormType = "IDHROSRC" Then
                    Dim sDay As String = getSharedData(oForm, "IDH_RTWDY")
                    Dim sCode As String = getSharedData(oForm, "IDH_RTCODE")
                    If sDay = "*M*" Then
                        setFormDFValue(oForm, "U_IDHRCDM", sCode, False, True)
                    End If
                    If sDay = "*Tu*" Then
                        setFormDFValue(oForm, "U_IDHRCDTU", sCode, False, True)
                    End If
                    If sDay = "*W*" Then
                        setFormDFValue(oForm, "U_IDHRCDW", sCode, False, True)
                    End If
                    If sDay = "*Th*" Then
                        setFormDFValue(oForm, "U_IDHRCDTH", sCode, False, True)
                    End If
                    If sDay = "*F*" Then
                        setFormDFValue(oForm, "U_IDHRCDF", sCode, False, True)
                    End If
                    If sDay = "*Sa*" Then
                        setFormDFValue(oForm, "U_IDHRCDSA", sCode, False, True)
                    End If
                    If sDay = "*Su*" Then
                        setFormDFValue(oForm, "U_IDHRCDSU", sCode, False, True)
                    End If
                    If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE And oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        doSetUpdate(oForm)
                    End If
                    'End additional functionality Joachim Alleritz =================================
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal Result - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub
        Protected Overrides Sub doReturnNormal(oForm As Form)
            MyBase.doReturnNormal(oForm)
        End Sub

        Protected Overrides Sub doHandleModalCanceled(oParentForm As SAPbouiCOM.Form, sModalFormType As String)
            If sModalFormType = "IDHROSRC" Then
                If getSharedData(oParentForm, "CALLEDITEM") IsNot Nothing AndAlso getSharedData(oParentForm, "CALLEDITEM").Trim <> "" Then
                    doSetFocus(oParentForm, getSharedData(oParentForm, "CALLEDITEM"))
                End If
            End If

            '#MA 2-6-2017
            If sModalFormType = "IDHISRC" Then
                If getSharedData(oParentForm, "TRG") = "CON" Then
                    setFormDFValue(oParentForm, "U_IDHCOICD", "", False, True)
                    setFormDFValue(oParentForm, "U_IDHCOIDC", "", False, True)
                End If
            End If
            '#MA 2-6-2017
            '#MA Start 17-05-2017 issue#402 point 1&2
            If sModalFormType = "IDHASRCH" Then
                If getFormDFValue(oParentForm, "U_IDHSADDR").ToString = "" Then
                    setFormDFValue(oParentForm, "U_LATI", "", False, True)
                    setFormDFValue(oParentForm, "U_LONGI", "", False, True)
                End If
            End If
            '#MA End 17-05-2017
            MyBase.doHandleModalCanceled(oParentForm, sModalFormType)
        End Sub

        Private Sub doSwitchRebate(ByVal oForm As SAPbouiCOM.Form)
            Dim sWasteCode As String = getFormDFValue(oForm, IDH_PBI._IDHWCICD)
            Dim bCanPurchase As Boolean = Config.INSTANCE.doCheckCanPurchaseWB(sWasteCode)

            If bCanPurchase Then
                setEnableItem(oForm, True, "IDH_REBATE")
            Else
                setEnableItem(oForm, False, "IDH_REBATE")
            End If
        End Sub
#End Region

#Region "Search form Procs"
        Protected Overridable Sub doChooseCustomerRef(ByVal oForm As SAPbouiCOM.Form)
            Dim sCardCode As String = getFormDFValue(oForm, "U_IDHCCODE")
            Dim sSite As String = getFormDFValue(oForm, "U_IDHSADDR")

            setSharedData(oForm, "TRG", "CUSREF")
            setSharedData(oForm, "VAL1", sCardCode)
            setSharedData(oForm, "VAL2", sSite)
            setSharedData(oForm, "SILENT", "SHOWMULTI")

            goParent.doOpenModalForm("IDHCREF", oForm)
        End Sub

        Protected Overridable Sub doChooseCustomer(ByVal oForm As SAPbouiCOM.Form)
            Dim sCardCode As String = getFormDFValue(oForm, "U_IDHCCODE")
            'Dim sCardName As String = getFormDFValue(oForm, "U_IDHCNAME")

            setSharedData(oForm, "TRG", "CUS")
            setSharedData(oForm, "IDH_BPCOD", sCardCode)
            setSharedData(oForm, "IDH_TYPE", "F-C")
            setSharedData(oForm, "IDH_NAME", "")
            setSharedData(oForm, "SILENT", "SHOWMULTI")

            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub

        Protected Overridable Sub doChooseCarrier(ByVal oForm As SAPbouiCOM.Form)
            Dim sCarrierCode As String = getFormDFValue(oForm, "U_IDHCARCD")

            setSharedData(oForm, "TRG", "CAR")
            setSharedData(oForm, "IDH_BPCOD", sCarrierCode)
            setSharedData(oForm, "IDH_TYPE", "S")
            setSharedData(oForm, "SILENT", "SHOWMULTI")

            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub

        Protected Overridable Sub doChooseDisposal(ByVal oForm As SAPbouiCOM.Form)
            Dim sDisposalCode As String = getFormDFValue(oForm, "U_IDHDISCD")

            setSharedData(oForm, "TRG", "DIS")
            setSharedData(oForm, "IDH_BPCOD", sDisposalCode)
            setSharedData(oForm, "IDH_TYPE", "S")
            setSharedData(oForm, "SILENT", "SHOWMULTI")

            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub


        Protected Overridable Sub doChooseContainer(ByVal oForm As SAPbouiCOM.Form)
            Dim sContainerCode As String = getFormDFValue(oForm, "U_IDHCOICD")

            Dim sGrp As String
            sGrp = getFormDFValue(oForm, "U_IDHCOGRP")

            setSharedData(oForm, "TRG", "CON")
            setSharedData(oForm, "IDH_ITMCOD", sContainerCode)
            setSharedData(oForm, "IDH_GRPCOD", sGrp)
            setSharedData(oForm, "SILENT", "SHOWMULTI")

            goParent.doOpenModalForm("IDHISRC", oForm)
        End Sub

        Protected Overridable Sub doChooseJobType(ByVal oForm As SAPbouiCOM.Form)
            Dim sJobCode As String = getFormDFValue(oForm, "U_IDHJTYPE")
            Dim sGrp As String = getFormDFValue(oForm, "U_IDHCOGRP")

            setSharedData(oForm, "TRG", "JT")
            setSharedData(oForm, "IDH_CURJOB", sJobCode) 'setSharedData(oForm, "IDHJTYPE", "")
            setSharedData(oForm, "IDH_ITMGRP", sGrp)
            setSharedData(oForm, "SILENT", "SHOWMULTI")

            goParent.doOpenModalForm("IDHJTSRC", oForm)
        End Sub

        Protected Overridable Sub doChooseWasteType(ByVal oForm As SAPbouiCOM.Form)
            Dim sCardCode As String = getFormDFValue(oForm, "U_IDHCCODE")

			Dim itemcode As String = getFormDFValue(oForm, "U_IDHWCICD")
            'setSharedData(oForm, "IDH_ITMCOD", sItem)
            setSharedData(oForm, "TP", "WC")
            setSharedData(oForm, "IDH_GRPCOD", Config.INSTANCE.doWasteMaterialGroup())
            setSharedData(oForm, "IDH_CRDCD", sCardCode)
			setSharedData(oForm, "IDH_ITMCOD", itemcode)
            setSharedData(oForm, "IDH_ITMNAM", "")
            setSharedData(oForm, "IDH_INVENT", "") '"N")
            'setSharedData(oForm, "SILENT", "SHOWMULTI")

            setSharedData(oForm, "SILENT", "SHOWMULTI")

            goParent.doOpenModalForm("IDHWISRC", oForm)
        End Sub

        Protected Overridable Sub doChooseSiteAd(ByVal oForm As SAPbouiCOM.Form)
            Dim sCardCode As String = getFormDFValue(oForm, IDH_PBI._IDHCCODE)

            If Not sCardCode Is Nothing AndAlso sCardCode.Length > 0 Then
                setSharedData(oForm, "TRG", "SA")
                setSharedData(oForm, "IDH_CUSCOD", sCardCode)
                setSharedData(oForm, "IDH_ADRTYP", "S")

                setSharedData(oForm, "SILENT", "SHOWMULTI")
                ''##MA Start 18-09-2014 Issue#403
                setSharedData(oForm, "IDH_APPLYFLTR", Config.ParameterAsBool("FLTRADDR", False).ToString)
                ''##MA End 18-09-2014 Issue#403

                goParent.doOpenModalForm("IDHASRCH", oForm)
            End If
        End Sub

        Protected Overridable Sub doChooseDisposalAd(ByVal oForm As SAPbouiCOM.Form)
            Dim sCardCode As String = getFormDFValue(oForm, IDH_PBI._IDHDISCD)

            If Not sCardCode Is Nothing AndAlso sCardCode.Length > 0 Then
                setSharedData(oForm, "TRG", "DA")
                setSharedData(oForm, "IDH_CUSCOD", sCardCode)
                setSharedData(oForm, "IDH_ADRTYP", "S")

                setSharedData(oForm, "SILENT", "SHOWMULTI")
                setSharedData(oForm, "IDH_APPLYFLTR", Config.ParameterAsBool("FLTRADDR", False).ToString)
                goParent.doOpenModalForm("IDHASRCH", oForm)
            End If
        End Sub

#End Region

#Region "Data Interface"
        Protected Function doUpdateHeader(ByVal oForm As SAPbouiCOM.Form) As Boolean
            'This will now Automatically update all the current DB fields and
            'generate and set the Code value if the code was not set
            Return doAutoSave(oForm, IDH_PBI.AUTONUMPREFIX, "PBIKEY")
        End Function
#End Region

#Region "Processing"
        Public Overrides Sub doButtonID2(oForm As Form, ByRef pVal As ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then

                doReturnFromModalShared(oForm, True)
            End If
        End Sub
        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            'Dim values As String = getParentSharedData(oForm, "RRemoveFlag")
            'If pVal.EventType = BoEventTypes.et_FORM_ACTIVATE And getParentSharedData(oForm, "RRemoveFlag") = True Then
            '    doLoadData(oForm)
            'End If

            If pVal.BeforeAction = True Then
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse
                  oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    '##MA Start 14-04-2017 issue#163
                    doCheckonSite(oForm)
                    '##MA End 14-04-2017
                    If getFormDFValue(oForm, "U_PERTYP").Equals(IDH_PBI_Handler.PER_Weeks) AndAlso getFormDFValue(oForm, "U_IDHUMONE") IsNot Nothing AndAlso getFormDFValue(oForm, "U_IDHUMONE").ToString().Trim().ToUpper() = "Y" Then
                        setFormDFValue(oForm, "U_IDHUMONE", "N")
                    End If
                    If doValidateFields(oForm) Then

                        If getFormDFValue(oForm, "U_IDHDADDR").Length <= 0 Then
                            setFormDFValue(oForm, "U_IDHDADLN", "")
                        End If
                        If getFormDFValue(oForm, "U_IDHSADDR").Length <= 0 Then
                            setFormDFValue(oForm, "U_IDHSADLN", "")
                        End If
                        Dim sCode As String = getFormDFValue(oForm, "Code")
                        'Dim sName As String
                        If sCode Is Nothing OrElse sCode.Length = 0 Then
                            sCode = DataHandler.INSTANCE.doGenerateCode(IDH_PBI.AUTONUMPREFIX, "PBIKEY").CodeCode

                            setFormDFValue(oForm, "Code", sCode)
                        End If
                        '#MA Start 29-05-2017
                        If (oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE) Then
                            Dim dStartDate As DateTime = Dates.doStrToDate(getFormDFValue(oForm, "U_IDHRECSD"))
                            If Dates.isValidDate(dStartDate) Then
                                setFormDFValue(oForm, "U_IDHLASTR", dStartDate)
                                setFormDFValue(oForm, "U_IDHNEXTR", dStartDate)
                            End If
                            If getWFValue(oForm, "duplicatePBIResetIDHRECSD") Then
                                setWasSetInternally(oForm, "IDHRECSD", data_SetFrom.idh_SET_FROMUSERFIELD)
                                setWFValue(oForm, "duplicatePBIResetIDHRECSD", False)
                            End If
                        End If
                        '#MA End 29-05-2017

                        doDescribeCoverage(oForm)

                        Dim oData As DataHandler = DataHandler.INSTANCE
                        Dim dPBIStartDate As DateTime
                        'Dim iCompare As Integer
                        Dim bRowsDeleted As Boolean = False
                        Try
                            'oData.StartTransaction()

                            'If oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                            '                     	If wasSetByUser(oForm, "IDHRECSD") Then
                            '                         	Dim sStartDate As String = getFormDFValue(oForm, "U_IDHRECSD")
                            '                             If Not sStartDate Is Nothing AndAlso sStartDate.Length >= 6 Then
                            '                             	dPBIStartDate = com.idh.utils.dates.doStrToDate(sStartDate)

                            '                             	Dim oPBIDTL As IDH_PBIDTL = New IDH_PBIDTL()

                            '      							Dim dFirstPeriodDate As DateTime = oPBIDTL.getFirstPeriodStart( sCode )
                            '      							If dates.isValidDate(dFirstPeriodDate) = False Then
                            '      								iCompare = 1
                            '      							Else
                            '      								iCompare = dates.CompareDatePartOnly(dFirstPeriodDate, dPBIStartDate)
                            '      							End If
                            ' 								If iCompare = 1 Then
                            '                                     'setFormDFValue(oForm, com.idh.dbObjects.User.IDH_PBI._IDHNEXTR, "")
                            '                                     'setFormDFValue(oForm, com.idh.dbObjects.User.IDH_PBI._IDHLASTR, "")
                            '                                     'setFormDFValue(oForm, com.idh.dbObjects.User.IDH_PBI._IDHWO, "")
                            '                                     'setFormDFValue(oForm, com.idh.dbObjects.User.IDH_PBI._IDHWOR, "")
                            '                                     'setFormDFValue(oForm, com.idh.dbObjects.User.IDH_PBI._IDHRECCT, "0")
                            '                                     'setUFValue(oForm, "IDH_ACDATE", "")

                            '                                     'oPBIDTL.doDeleteByPBI(sCode, "PBI Start date adjusted", False, True)
                            '                                     'bRowsDeleted = True
                            ' 								End If
                            ' 								IDHAddOns.idh.data.Base.doReleaseObject(oPBIDTL)
                            '                         	End If
                            '	End If
                            'End If

                            If doUpdateHeader(oForm) Then
                                'This DLL Creates/updates waste orders and row based on details of the WasteOrder Instruction. Also updates any documents linked to WO
                                '######################################################################################################################
                                Dim sActDate As String = getUFValue(oForm, "IDH_ACDATE")
                                Dim dActDate As DateTime
                                If Not sActDate Is Nothing Then
                                    If sActDate.Length >= 6 Then
                                        sActDate = com.idh.utils.Dates.doSBODateToSQLDate(sActDate)

                                        Dim sPBIStart As String = getFormDFValue(oForm, "U_IDHRECSD")
                                        Dim dPBIStart As Date = Conversions.ToDateTime(sPBIStart)
                                        dActDate = Conversions.ToDateTime(sActDate)

                                        If Date.Compare(dActDate, dPBIStart) < 0 Then
                                            dActDate = dPBIStart
                                        End If
                                    Else
                                        dActDate = DateTime.Now()
                                    End If
                                End If

                                Dim oWOM As IDH_PBI_Handler = getWFValue(oForm, "PBI_HANDLER")
                                Dim bDoRestOfUpdates As Boolean = True
                                Dim bResult As Boolean = True
                                Dim bDoToEnd As Boolean = False
                                Dim sVal As String = getUFValue(oForm, "IDHTOE")

                                If sVal = "Y" Then
                                    bDoToEnd = True
                                End If

                                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                                    If bRowsDeleted = False Then
                                        If wasSetByUser(oForm, "IDHRECSD") Then
                                            Dim sStartDate As String = getFormDFValue(oForm, "U_IDHRECSD")

                                            'If Not sStartDate Is Nothing AndAlso sStartDate.Length >= 6 Then
                                            '    Dim dStartDate As DateTime = dates.doStrToDate(sStartDate)
                                            '    oWOM.doAdjustPBIStartDate(sCode, dPBIStartDate)
                                            'End If

                                            'If getUserChangedFieldCount(oForm) > 1 Then
                                            '    bDoRestOfUpdates = True
                                            'Else
                                            '    'Then only the Start date was changed
                                            '    bDoRestOfUpdates = False
                                            'End If
                                            If Not sStartDate Is Nothing AndAlso sStartDate.Length >= 6 Then
                                                dPBIStartDate = Dates.doStrToDate(sStartDate)
                                                Dim sSetNextRun As String = getFormDFValue(oForm, com.idh.dbObjects.User.IDH_PBI._IDHNEXTR)
                                                Dim dSetNextRun As Date = com.idh.utils.Dates.doStrToDate(sSetNextRun)

                                                bResult = oWOM.doAdjustPBIStartDate(sCode, dActDate, dSetNextRun, dPBIStartDate, bDoToEnd)

                                                bDoRestOfUpdates = False
                                            End If
                                        End If

                                        If bDoRestOfUpdates AndAlso wasSetByUser(oForm, "IDHRECED") Then
                                            Dim sTermDate As String = getFormDFValue(oForm, "U_IDHRECED")
                                            If Not sTermDate Is Nothing AndAlso sTermDate.Length >= 6 Then
                                                sTermDate = com.idh.utils.Dates.doSBODateToSQLDate(sTermDate)
                                                oWOM.doClearAfterTerminationDate(sCode, sTermDate)
                                            End If

                                            If getUserChangedFieldCount(oForm) > 1 Then
                                                bDoRestOfUpdates = True
                                            Else
                                                'Then only the Termination date was changed
                                                bDoRestOfUpdates = False
                                            End If
                                        End If
                                    End If
                                End If

                                If bDoRestOfUpdates Then
                                    'If sDate Is Nothing Then
                                    '    dRunDate = DateTime.Now()
                                    'Else
                                    '    dRunDate = Conversions.ToDateTime(sDate)
                                    'End If

                                    If bDoToEnd Then
                                        Dim sSetNextRun As String = getFormDFValue(oForm, com.idh.dbObjects.User.IDH_PBI._IDHNEXTR)
                                        Dim dSetNextRun As Date = com.idh.utils.Dates.doStrToDate(sSetNextRun)
                                        bResult = oWOM.ProcessOrderRunToEnd(sCode, dActDate, dSetNextRun)

                                        'While Date.Compare(dCalculatedNextRun, dSetNextRun) < 0
                                        '    bResult = oWOM.ProcessOrderRun(sCode, dCalculatedNextRun)
                                        '    If bResult = False Then
                                        '        Exit While
                                        '    Else
                                        '        dCalculatedNextRun = oWOM.CalculatedNextRun
                                        '    End If
                                        'End While
                                    Else
                                        bResult = oWOM.ProcessOrderRun(sCode, dActDate)
                                    End If

                                    If bResult = True Then
                                        doSetFocus(oForm, "IDHCCODE")
                                        If oData.IsInTransaction = True Then
                                            oData.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                                        End If
                                        BubbleEvent = False
                                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                                        doLoadData(oForm)
                                    Else
                                        If oData.IsInTransaction = True Then
                                            oData.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                        End If
                                        BubbleEvent = False
                                    End If
                                Else
                                    If bResult = True Then
                                        doSetFocus(oForm, "IDHCCODE")
                                        If oData.IsInTransaction = True Then
                                            oData.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                                        End If
                                        BubbleEvent = False
                                        setWasSetInternally(oForm, "IDHRECSD", data_SetFrom.idh_SET_FROMDATABASE)
                                        setWasSetInternally(oForm, "IDHRECED", data_SetFrom.idh_SET_FROMDATABASE)
                                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                                        doLoadData(oForm)
                                    Else
                                        If oData.IsInTransaction = True Then
                                            oData.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                        End If
                                        BubbleEvent = False
                                    End If
                                End If
                                '######################################################################################################################
                            Else
                                If oData.IsInTransaction = True Then
                                    oData.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                End If
                            End If

                            '##MA Start 13-02-2017
                            If getWFValue(oForm, "callFromPBIManager") Then
                                doReturnFromModalShared(oForm, True)
                                setWFValue(oForm, "callFromPBIManager", False)
                            End If
                            '##MA End 13-02-2017

                        Catch exx As Exception
                            If oData.IsInTransaction = True Then
                                oData.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                            End If
                            'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & exx.ToString(), "Adding/Updating the Instruction.")
                            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(exx.Message, "EREXAUIN", {Nothing})
                            'setFormDFValue(oForm, "Code", "")
                            BubbleEvent = False
                        End Try
                    Else
                        BubbleEvent = False
                    End If
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    If goParent.doCheckModal(oForm.UniqueID) Then
                        doReturnFromModalShared(oForm, True)
                    End If
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    BubbleEvent = False
                    doFind(oForm)
                    oForm.Update()
                End If
            Else
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    setEnableItem(oForm, False, "CODE")
                End If
            End If
        End Sub

        'Find the record containing the Customer and/or Order number
        Protected Function doFind(ByVal oForm As SAPbouiCOM.Form) As Boolean
            oForm.Freeze(True)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing

            '            Dim sMode As String
            Try
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    Dim sPBICode As String = ""

                    sPBICode = getItemValue(oForm, "CODE").Trim()

                    Dim sQry As String
                    oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    sQry = "SELECT Top 1 Code from [@IDH_PBI] Where Code = '" & sPBICode & "'"

                    oRecordSet.DoQuery(sQry)
                    If oRecordSet.RecordCount > 0 Then
                        sPBICode = oRecordSet.Fields.Item("Code").Value.Trim()

                        setFormDFValue(oForm, "Code", sPBICode, True)

                        'Dim db As SAPbouiCOM.DBDataSource
                        'db = getFormMainDataSource(oForm)

                        'sPBICode = oRecordSet.Fields.Item("Code").Value.Trim()
                        'db.Clear()
                        'Dim oConditions As SAPbouiCOM.Conditions = New SAPbouiCOM.Conditions
                        'Dim oCondition As SAPbouiCOM.Condition = oConditions.Add
                        'oCondition.Alias = "Code"
                        'oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                        'oCondition.CondVal = sPBICode
                        'db.Query(oConditions)

                        'oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        doLoadData(oForm)
                        Return True
                    Else
                        'com.idh.bridge.DataHandler.INSTANCE.doError("The no PBI was found for Code : " & sPBICode)
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("The no PBI was found for Code : " & sPBICode, "ERSYPBII", {sPBICode})
                        Return False
                    End If
                End If
                Return False
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error finding the PBI.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFPBI", {Nothing})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
                oForm.Freeze(False)
            End Try
            Return False
        End Function

        Private Function doValidateFields(ByVal oform As SAPbouiCOM.Form) As Boolean
            Dim sError As String = ""

            Try
                Dim sCardCode As String = getFormDFValue(oform, "U_IDHCCODE")
                Dim oRecordSet1 As SAPbobsCOM.Recordset = Nothing

                If sCardCode.Length <= 0 Then
                    sError &= "Cust. Code,"

                    ' ***** Theoritically not needed as the BP should not be in the Search Form
                    'ElseIf Config.INSTANCE.doCheckBPActive(sCardCode, DateTime.Now) = False Then ''re-active the check as may require in duplicate PBI
                    '    com.idh.bridge.DataHandler.INSTANCE.doError("User: The Customer is not active.", "The Customer is not active.")
                    '    Return False
                End If

                If getFormDFValue(oform, "U_IDHCARCD").Length <= 0 Then
                    sError &= "Carrier Code,"
                    'ElseIf Config.INSTANCE.doCheckBPActive(getFormDFValue(oform, "U_IDHCARCD").ToString(), DateTime.Now) = False Then ''re-active the check as may require in duplicate PBI
                    '    com.idh.bridge.DataHandler.INSTANCE.doError("User: The Customer is not active.", "The Customer is not active.")
                    '    Return False
                End If

                If Config.INSTANCE.getParameterAsBool("PBIMDIS", False) Then
                    If getFormDFValue(oform, "U_IDHDISCD").Length <= 0 Then
                        sError &= "Disposal Site,"
                    End If

                    If Config.INSTANCE.getParameterAsBool("PBIMDSA", False) Then
                        If getFormDFValue(oform, "U_IDHSADDR").Length <= 0 Then
                            sError &= "Site Address,"
                        End If
                    End If
                Else
                    '#MA 2017102 start
                    If Not getFormDFValue(oform, "U_IDHSADDR").Length = 0 Then
                        Dim sQry2 As String

                        Dim oRecs As DataRecords = Nothing
                        sQry2 = "select isnull((a.AdresType ),'')as addrs from CRD1 a with(nolock) where a.address = '" + getFormDFValue(oform, "U_IDHSADDR").ToString.Trim.Replace("'", "''") + "'  and a.CardCode = '" + getFormDFValue(oform, "U_IDHCCODE").ToString.Trim.Replace("'", "''") + "' and a.AdresType = 'S' " 'and a.U_AddrsCd = '" + addressLine + "'"

                        oRecs = DataHandler.INSTANCE.doBufferedSelectQuery("SiteAddress" + DateTime.Now.ToString("HHmmttss"), sQry2)

                        If (oRecs.RecordCount = 0) Then
                            sError &= "Please select valid ship to address. "
                        End If
                    End If
                    '#MA 2017102 End
                End If
                'If getFormDFValue(oform, "U_IDHDISCD").Length > 0 AndAlso Config.INSTANCE.doCheckBPActive(getFormDFValue(oform, "U_IDHDISCD").ToString(), DateTime.Now) = False Then ''re-active the check as may require in duplicate PBI
                '    com.idh.bridge.DataHandler.INSTANCE.doError("User: Disposal site is not active.", "Disposal site is not active.")
                '    Return False
                'End If
                '##MA Start 14-04-2017 issue#163
                If Config.INSTANCE.getParameterAsBool("PBIPOS") Then
                    If (getFormDFValue(oform, "U_IDHPOS").ToString().Length >= 0 AndAlso getFormDFValue(oform, "U_IDHPOS").Equals("Y")) Then
                        If Config.INSTANCE.getParameterWithDefault("PBIPBYPS", False) Then
                            sError &= ""
                        Else
                            sError &= "PBI Pause Because Site is onStop,"
                        End If
                End If
                End If
                '##MA End 14-04-2017

                If getFormDFValue(oform, "U_IDHJTYPE").Length <= 0 Then
                    sError &= "JobType,"
                End If

                If getFormDFValue(oform, "U_IDHCOICD").Length <= 0 Then
                    sError &= "Container Code,"
                Else
                    '#MA 2017102 start
                    Dim sQry1 As String
                    Dim oRecs As DataRecords = Nothing
                    'oRecordSet1 = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    sQry1 = "select isnull((a.ItemCode),'') as itmCode from OITM a with (NOLOCK) inner join OITB b on a.ItmsGrpCod=b.ItmsGrpCod where b.ItmsGrpCod='" + getFormDFValue(oform, "U_IDHCOGRP").ToString + "' and a.ItemCode='" + getFormDFValue(oform, "U_IDHCOICD").ToString + "'"
                    oRecs = DataHandler.INSTANCE.doBufferedSelectQuery("ContainerCode" + DateTime.Now.ToString("HHmmttss"), sQry1)
                    'oRecordSet1.DoQuery(sQry1)
                    'Dim itemCode As String = oRecordSet1.Fields.Item("itmCode").Value.Trim()
                    If (oRecs.RecordCount = 0) Then
                        sError &= "Container Code does not belong to the Selected Container Group,"
                    End If
                    '#MA 2017102 End
                End If

                If getFormDFValue(oform, "U_IDHWCICD").Length <= 0 Then
                    sError &= "Waste Type,"
                End If


                If getFormDFValue(oform, "U_IDHRECFQ").Length <= 0 Then
                    sError &= "Run Frequency,"
                End If

                If getFormDFValue(oform, "U_IDHRECSD").Length <= 0 Then
                    sError &= "Start Date,"
                End If

                '#MM Start 1-17-2016
                If getFormDFValue(oform, "U_IDHCOGRP").Length <= 0 Then
                    sError &= "Container Group,"
                End If
                '#END 1-17-2016
                '#MM Start 1-24-2016
                If getFormDFValue(oform, "U_IDHSADDR").Length <= 0 Then
                    sError &= "Site Address,"
                End If
                '#END 1-24-2016
                Dim sRunFreqType As String = ""
                If getFormDFValue(oform, "U_PERTYP") Is Nothing OrElse getFormDFValue(oform, "U_PERTYP").ToString() = String.Empty OrElse getFormDFValue(oform, "U_IDHRECFQ") Is Nothing OrElse Not IsNumeric(getFormDFValue(oform, "U_IDHRECFQ")) Then
                    sError &= "Run Frequency,"
                Else
                    sRunFreqType = getFormDFValue(oform, "U_PERTYP")
                    Dim sRunFreqValue As String = Convert.ToInt16(getFormDFValue(oform, "U_IDHRECFQ"))
                    If sRunFreqType = IDH_PBI_Handler.PER_Months AndAlso (sRunFreqValue < 1 OrElse sRunFreqValue > 12) Then
                        sError &= "Run Frequency can be between 1-12,"
                    ElseIf sRunFreqType = IDH_PBI_Handler.PER_Quarts AndAlso (sRunFreqValue < 1 OrElse sRunFreqValue > 4) Then
                        sError &= "Run Frequency can be between 1-4,"
                    ElseIf sRunFreqType = IDH_PBI_Handler.PER_Weeks AndAlso (sRunFreqValue < 1 OrElse sRunFreqValue > 52) Then
                        sError &= "Run Frequency can be between 1-12,"
                        'Else
                        '    sError &= "Run Frequency,"
                    End If
                End If
                Dim sRecure As String = getFormDFValue(oform, "U_IDHRECUR")

                Try
                    'If sRecure = "1" Then
                    '	sRecure = "D"
                    '	setFormDFValue(oform, "U_IDHRECUR", "D", False, False)
                    'ElseIf sRecure = "2" Then
                    '	sRecure = "Y"
                    '	setFormDFValue(oform, "U_IDHRECUR", "Y", False, False)
                    'End If

                    'Some issue with the Option's on values => D = 1, Y = 2
                    If sRecure = "M" Then
                        If Not String.IsNullOrWhiteSpace(sRunFreqType) AndAlso (sRunFreqType = IDH_PBI_Handler.PER_Weeks) Then
                            sError &= "Run Frequency cannot be week,"
                        End If
                    ElseIf sRecure = "2" Then
                        If Not String.IsNullOrWhiteSpace(sRunFreqType) AndAlso (sRunFreqType = IDH_PBI_Handler.PER_Weeks) Then
                            sError &= "Run Frequency cannot be week,"
                        End If
                    Else
                        If getFormDFValue(oform, IDH_PBI._IDHRDMON2) = "N" And
                                    getFormDFValue(oform, IDH_PBI._IDHRDTUE2) = "N" And
                                    getFormDFValue(oform, IDH_PBI._IDHRDWED2) = "N" And
                                    getFormDFValue(oform, IDH_PBI._IDHRDTHU2) = "N" And
                                    getFormDFValue(oform, IDH_PBI._IDHRDFRI2) = "N" And
                                    getFormDFValue(oform, IDH_PBI._IDHRDSAT2) = "N" And
                                    getFormDFValue(oform, IDH_PBI._IDHRDSUN2) = "N" Then
                            sError &= "Action Day,"
                        End If
                    End If
                Catch ex As Exception
                    If getFormDFValue(oform, IDH_PBI._IDHRDMON2) = "" And
                                    getFormDFValue(oform, IDH_PBI._IDHRDTUE2) = "" And
                                    getFormDFValue(oform, IDH_PBI._IDHRDWED2) = "" And
                                    getFormDFValue(oform, IDH_PBI._IDHRDTHU2) = "" And
                                    getFormDFValue(oform, IDH_PBI._IDHRDFRI2) = "" And
                                    getFormDFValue(oform, IDH_PBI._IDHRDSAT2) = "" And
                                    getFormDFValue(oform, IDH_PBI._IDHRDSUN2) = "" Then
                        sError &= "Action Day,"
                    End If
                End Try

                If sError.Length > 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("User: Mandatory fields not set.", "The following fields must be set: " & sError)
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Mandatory fields not set: The following fields must be set: " & sError, "ERUSGENS", {com.idh.bridge.Translation.getTranslatedWord(sError)})
                    Return False
                Else
                    Return True
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error validating fields.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBVF", {Nothing})
            End Try
            Return True
        End Function

        Private Sub doDescribeCoverage(ByVal oForm As SAPbouiCOM.Form)
            Dim sPreBook As String = getFormDFValue(oForm, "U_IDHRECOM")
            Dim sPreBookDesc, Code As String
            Dim Pattern As String = getFormDFValue(oForm, "U_IDHRECUR")
            Dim Day As String
            Dim Count As Integer = 0

            If Not getFormDFValue(oForm, "Code") = Nothing Then
                Code = getFormDFValue(oForm, "Code")
            Else
                Code = "TBA"
            End If

            'Some issue with the Option's on values => D = 1, Y = 2
            Select Case Pattern
                Case "1"
                    sPreBookDesc = "PBI# " & Code & " : Daily intervals as of " & com.idh.utils.Dates.doStrToDate(getDFValue(oForm, "@IDH_PBI", "U_IDHRECSD")) & " "
                Case "W"
                    sPreBookDesc = "PBI# " & Code & " : Weekly intervals as of " & com.idh.utils.Dates.doStrToDate(getDFValue(oForm, "@IDH_PBI", "U_IDHRECSD")) & " "
                Case "O"
                    sPreBookDesc = "PBI# " & Code & " : Weekly intervals as of " & com.idh.utils.Dates.doStrToDate(getDFValue(oForm, "@IDH_PBI", "U_IDHRECSD")) & " "
                Case "M"
                    sPreBookDesc = "PBI# " & Code & " : Monthly intervals as of " & com.idh.utils.Dates.doStrToDate(getDFValue(oForm, "@IDH_PBI", "U_IDHRECSD")) & " "
                Case "2"
                    sPreBookDesc = "PBI# " & Code & " : Yearly intervals as of " & com.idh.utils.Dates.doStrToDate(getDFValue(oForm, "@IDH_PBI", "U_IDHRECSD")) & " "
                Case Else
                    sPreBookDesc = ""
            End Select

            'Some issue with the Option's on values => D = 1, Y = 2
            If Pattern = "M" Then
                If Not getFormDFValue(oForm, "U_IDHRACTW") = Nothing Then
                    sPreBookDesc &= " to be executed every " & getFormDFValue(oForm, "U_IDHRACTW") & " months "
                End If
            ElseIf Pattern = "2" Then
                If Not getFormDFValue(oForm, "U_IDHRACTW") = Nothing Then
                    sPreBookDesc &= " to be executed every " & getFormDFValue(oForm, "U_IDHRACTW") & " years "
                End If

                Dim sEFType As String = getFormDFValue(oForm, "U_EFType")
                Dim iEFMonth As Integer = getFormDFValue(oForm, "U_EFMonth")
                Dim sEFMonth As String = com.idh.bridge.lookups.FixedValues.getMonthFromIndex(iEFMonth)
                'Option => D = 1, W = 2
                If Not sEFType = Nothing Then
                    If sEFType = "1" Then
                        Dim iEFDay As String = getFormDFValue(oForm, "U_EFDay")

                        sPreBookDesc &= "on day " & iEFDay & " of " & sEFMonth
                    Else
                        Dim iMFWeek As Integer = getFormDFValue(oForm, "U_EFWeek")
                        Dim iMFDOW As Integer = getFormDFValue(oForm, "U_EFDOW")

                        Dim sMFWeek As String = com.idh.bridge.lookups.FixedValues.getPosiotionFromIndex(iMFWeek)
                        Dim sMFDOW As String = com.idh.bridge.lookups.FixedValues.getDayOfWeekFromIndex(iMFDOW)

                        sPreBookDesc &= " " & getTranslatedWord("on the") & " " & sMFWeek & " " & sMFDOW & " of " & sEFMonth
                    End If
                End If
            Else
                If Not getFormDFValue(oForm, "U_IDHRACTW") = Nothing Then
                    sPreBookDesc &= " to be executed every " & getFormDFValue(oForm, "U_IDHRACTW") & " weeks "
                End If

                sPreBookDesc &= "on the following day(s):"
                If Not getFormDFValue(oForm, IDH_PBI._IDHRDMON2) = Nothing Then
                    Day = getFormDFValue(oForm, IDH_PBI._IDHRDMON2)
                    If Day = "Y" Then
                        sPreBookDesc &= " Mon,"
                        Count += 1
                    End If
                End If

                If Not getFormDFValue(oForm, IDH_PBI._IDHRDTUE2) = Nothing Then
                    Day = getFormDFValue(oForm, IDH_PBI._IDHRDTUE2)
                    If Day = "Y" Then
                        sPreBookDesc &= " Tue,"
                        Count += 1
                    End If
                End If

                If Not getFormDFValue(oForm, IDH_PBI._IDHRDWED2) = Nothing Then
                    Day = getFormDFValue(oForm, IDH_PBI._IDHRDWED2)
                    If Day = "Y" Then
                        sPreBookDesc &= " Wed,"
                        Count += 1
                    End If
                End If

                If Not getFormDFValue(oForm, IDH_PBI._IDHRDTHU2) = Nothing Then
                    Day = getFormDFValue(oForm, IDH_PBI._IDHRDTHU2)
                    If Day = "Y" Then
                        sPreBookDesc &= " Thu,"
                        Count += 1
                    End If
                End If

                If Not getFormDFValue(oForm, IDH_PBI._IDHRDFRI2) = Nothing Then
                    Day = getFormDFValue(oForm, IDH_PBI._IDHRDFRI2)
                    If Day = "Y" Then
                        sPreBookDesc &= " Fri,"
                        Count += 1
                    End If
                End If

                If Not getFormDFValue(oForm, IDH_PBI._IDHRDSAT2) = Nothing Then
                    Day = getFormDFValue(oForm, IDH_PBI._IDHRDSAT2)
                    If Day = "Y" Then
                        sPreBookDesc &= " Sat,"
                        Count += 1
                    End If
                End If

                If Not getFormDFValue(oForm, IDH_PBI._IDHRDSUN2) = Nothing Then
                    Day = getFormDFValue(oForm, IDH_PBI._IDHRDSUN2)
                    If Day = "Y" Then
                        sPreBookDesc &= " Sun,"
                        Count += 1
                    End If
                End If
                If (sPreBookDesc.LastIndexOf(",") > -1) Then
                sPreBookDesc.Remove(sPreBookDesc.LastIndexOf(","), 1)
            End If
            End If

            sPreBookDesc &= ". Termination "

            Dim iOccurance As Integer = Conversions.ToInt(getFormDFValue(oForm, "U_IDHRECCE"))
            If iOccurance <> 0 Then
                sPreBookDesc &= " after " & getFormDFValue(oForm, "U_IDHRECCE") & " occurences."
            Else
                Dim sTermDate As String = getFormDFValue(oForm, "U_IDHRECED")
                If Not sTermDate = Nothing AndAlso sTermDate.Length > 0 Then
                    sPreBookDesc &= " on " & com.idh.utils.Dates.doStrToDate(getFormDFValue(oForm, "U_IDHRECED")) & "."
                End If
            End If

            If Not getFormDFValue(oForm, "U_IDHRECCT") = Nothing Then
                Dim iRunCount As Integer = getFormDFValue(oForm, "U_IDHRECCT")
                sPreBookDesc &= " -- RunCount = " & (iRunCount + 1)
            End If


            setFormDFValue(oForm, "U_IDHRECOM", sPreBookDesc)
        End Sub

#End Region

#Region "Reports"
        Protected Sub doReportByBtn(ByVal oForm As SAPbouiCOM.Form, ByVal sBtn As String)
            Dim sReportFile As String = Nothing
            Dim sRepId As String
            Select Case sBtn
                Case "IDHPO"
                    sRepId = "PBIPOR"
                    sReportFile = Config.Parameter(sRepId)
                Case "IDHDOC"
                    sRepId = "PBIDOCR"
                    sReportFile = Config.Parameter(sRepId)
                Case "IDHCUST"
                    sRepId = "PBICUSTR"
                    sReportFile = Config.Parameter(sRepId)
                Case Else
                    sRepId = "DontKnow"
            End Select

            If Not sReportFile Is Nothing AndAlso sReportFile.Length > 0 Then
                doReport(oForm, sReportFile)
            Else
                'com.idh.bridge.DataHandler.INSTANCE.doError("The report was not set: " & sRepId)
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("The report was not set: " & sRepId, "ERUSGENS", {com.idh.bridge.Translation.getTranslatedWord("Report") + " [" + sRepId + "]"})
            End If
        End Sub

        Protected Overridable Sub doReport(ByVal oForm As SAPbouiCOM.Form, ByVal sReportFile As String)
            Dim sPBICode As String = getFormDFValue(oForm, "Code")

            If sPBICode Is Nothing Then
                sPBICode = ""
            End If

            If Not sReportFile Is Nothing AndAlso
             sReportFile.Length > 0 Then

                Dim oParams As New Hashtable
                oParams.Add("PBINum", sPBICode)

				If (Config.INSTANCE.useNewReportViewer()) Then
                    IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                    IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                    IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
                Else
                    IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
                End If
            End If
        End Sub
#End Region

#Region "Methods"
        ''MA Start 14-09-2016 Issue#665
        Private Sub doSearchRout(sItemID As String, oForm As SAPbouiCOM.Form)
            setSharedData(oForm, "SILENT", "SHOWMULTI")
            setSharedData(oForm, "CALLEDITEM", sItemID)

            If sItemID.Equals("IDHRCDM") Then
                'If getFormDFValue(oForm, "U_IDHRCDM").ToString.IndexOf("*") > -1 Then
                '    setSharedData(oForm, "IDH_RTCODE", )
                'Else
                '    setSharedData(oForm, "IDH_RTCODE", getFormDFValue(oForm, "U_IDHRCDM").ToString)
                'End If
                setSharedData(oForm, "IDH_RTCODE", getFormDFValue(oForm, "U_IDHRCDM").ToString)
                setSharedData(oForm, "IDH_RTWDY", "*M*")
                goParent.doOpenModalForm("IDHROSRC", oForm)
            ElseIf sItemID.Equals("IDHRCDTU") Then
                'If getFormDFValue(oForm, "U_IDHRCDTU") = "*" Then
                setSharedData(oForm, "IDH_RTCODE", getFormDFValue(oForm, "U_IDHRCDTU").ToString)
                setSharedData(oForm, "IDH_RTWDY", "*Tu*")
                goParent.doOpenModalForm("IDHROSRC", oForm)
                'End If
            ElseIf sItemID.Equals("IDHRCDW") Then
                'If getFormDFValue(oForm, "U_IDHRCDW") = "*" Then
                'setSharedData(oForm, "IDH_RTCODE", "")
                setSharedData(oForm, "IDH_RTCODE", getFormDFValue(oForm, "U_IDHRCDW").ToString)
                setSharedData(oForm, "IDH_RTWDY", "*W*")
                goParent.doOpenModalForm("IDHROSRC", oForm)
                'End If
            ElseIf sItemID.Equals("IDHRCDTH") Then
                'If getFormDFValue(oForm, "U_IDHRCDTH") = "*" Then
                'setSharedData(oForm, "IDH_RTCODE", "")
                setSharedData(oForm, "IDH_RTCODE", getFormDFValue(oForm, "U_IDHRCDTH").ToString)
                setSharedData(oForm, "IDH_RTWDY", "*Th*")
                goParent.doOpenModalForm("IDHROSRC", oForm)
                'End If
            ElseIf sItemID.Equals("IDHRCDF") Then
                'If getFormDFValue(oForm, "U_IDHRCDF") = "*" Then
                'setSharedData(oForm, "IDH_RTCODE", "")
                setSharedData(oForm, "IDH_RTCODE", getFormDFValue(oForm, "U_IDHRCDF").ToString)
                setSharedData(oForm, "IDH_RTWDY", "*F*")
                goParent.doOpenModalForm("IDHROSRC", oForm)
                'End If
            ElseIf sItemID.Equals("IDHRCDSA") Then
                'If getFormDFValue(oForm, "U_IDHRCDSA") = "*" Then
                'setSharedData(oForm, "IDH_RTCODE", "")
                setSharedData(oForm, "IDH_RTCODE", getFormDFValue(oForm, "U_IDHRCDSA").ToString)
                setSharedData(oForm, "IDH_RTWDY", "*Sa*")
                goParent.doOpenModalForm("IDHROSRC", oForm)
                'End If
            ElseIf sItemID.Equals("IDHRCDSU") Then
                'If getFormDFValue(oForm, "U_IDHRCDSU") = "*" Then
                'setSharedData(oForm, "IDH_RTCODE", "")
                setSharedData(oForm, "IDH_RTCODE", getFormDFValue(oForm, "U_IDHRCDSU").ToString)
                setSharedData(oForm, "IDH_RTWDY", "*Su*")
                goParent.doOpenModalForm("IDHROSRC", oForm)
                'End If
            End If

        End Sub
        Public Overrides Function doRightClickEvent(oForm As Form, ByRef pVal As ContextMenuInfo, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction AndAlso pVal.EventType = BoEventTypes.et_RIGHT_CLICK Then
                If oForm.Mode = BoFormMode.fm_ADD_MODE Or oForm.Mode = BoFormMode.fm_UPDATE_MODE Or oForm.Mode = BoFormMode.fm_FIND_MODE Then
                    oForm.EnableMenu(IDHAddOns.idh.lookups.Base.DUPLICATE, False)
                Else
                    oForm.EnableMenu(IDHAddOns.idh.lookups.Base.DUPLICATE, True)
                End If
            End If
            Return MyBase.doRightClickEvent(oForm, pVal, BubbleEvent)
        End Function
        '##MA End 08-02-2017


#End Region
        Protected Function doSetChoosenCustomerFromShared(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoBPRules As Boolean = True, Optional ByVal bDoBuyAndTrade As Boolean = False) As Boolean
            Dim val As String = Nothing
            Dim val2 As String
            Dim val3 As String
            Dim val4 As String
            Dim dCreditLimit As Decimal = 0
            Dim dCreditRemain As Decimal = 0
            Dim dDeviationPrc As Decimal = 0
            Dim dTBalance As Decimal = 0
            Dim sFrozen As String = "N"
            Dim sFrozenComm As String = Nothing
            Dim sIgnoreCheck As String = "N"
            Dim sObligated As String
            Dim sCustomerAddress As String = Nothing
            Dim sPaymentTermCode As String
            Dim dBalance As Decimal = 0
            Dim dWROrders As Decimal = 0

            Dim sFrozenFrom As String
            Dim sFrozenTo As String
            Dim oFrozenFrom As Date
            Dim oFrozenTo As Date

            Dim WasteLicRegNo As String = ""
            Dim CusFName As String = ""
            Try
                val = getSharedData(oForm, "CARDCODE")
                val2 = getSharedData(oForm, "PHONE1")
                val3 = getSharedData(oForm, "CNTCTPRSN")
                val4 = getSharedData(oForm, "CARDNAME")
                dCreditLimit = getSharedData(oForm, "CREDITLINE")
                dCreditRemain = getSharedData(oForm, "CRREMAIN")
                dDeviationPrc = getSharedData(oForm, "DEVIATION")
                dTBalance = getSharedData(oForm, "TBALANCE")

                sFrozen = getSharedData(oForm, "FROZEN")
                sFrozenComm = getSharedData(oForm, "FROZENC")
                sFrozenFrom = getSharedData(oForm, "FROZENF")
                sFrozenTo = getSharedData(oForm, "FROZENT")

                sIgnoreCheck = getSharedData(oForm, "IDHICL")
                sObligated = getSharedData(oForm, "IDHOBLGT")
                sPaymentTermCode = getSharedData(oForm, "PAYTRM")
                dBalance = getSharedData(oForm, "BALANCE")
                dWROrders = getSharedData(oForm, "WRORDERS")
                WasteLicRegNo = getSharedData(oForm, "WASLIC")
                CusFName = getSharedData(oForm, "CARDFNAME")
                oFrozenFrom = com.idh.utils.Dates.doStrToDate(sFrozenFrom)
                oFrozenTo = com.idh.utils.Dates.doStrToDate(sFrozenTo)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the chosen Customer.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSVS", {com.idh.bridge.Translation.getTranslatedWord("Chosen Customer"), val.ToString()})
                Return False
            End Try

            If Not sIgnoreCheck = "Y" Then
                Dim bBlockOnFail As Boolean = Config.INSTANCE.getParameterAsBool("DOBCCL", True)
                'If idh.const.Lookup.INSTANCE.doCheckCredit2(goParent, val, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc, 0, sFrozen, sFrozenComm, False, bBlockOnFail, Nothing, oFrozenFrom, oFrozenTo) = False Then
                '    Return False
                'End If
                If Config.INSTANCE.doCheckCredit2(val, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc, 0, False, bBlockOnFail, Nothing) = False Then
                    Return False
                End If

            End If
            Return True
        End Function
    End Class
End Namespace
