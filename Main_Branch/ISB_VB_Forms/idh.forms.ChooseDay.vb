﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Namespace idh.forms
    Public Class ChooseDay
        Inherits IDHAddOns.idh.forms.Base
        'OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
        'Joachim Alleritz
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHCHD", Nothing, -1, "ChooseDay.srf", False, True, False, "Choose Day", load_Types.idh_LOAD_NORMAL)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                doAddUF(oForm, "IDHMON", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHTUE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHWED", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHTHU", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHFRI", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHSAT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHSUN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        '*** Get the selected fields
        Protected Function doPrepareModalData(ByVal oForm As SAPbouiCOM.Form, Optional ByVal iSel As Integer = -1) As Boolean
            Dim sMON As String = getUFValue(oForm, "IDHMON")
            Dim sTUE As String = getUFValue(oForm, "IDHTUE")
            Dim sWED As String = getUFValue(oForm, "IDHWED")
            Dim sTHU As String = getUFValue(oForm, "IDHTHU")
            Dim sFRI As String = getUFValue(oForm, "IDHFRI")
            Dim sSAT As String = getUFValue(oForm, "IDHSAT")
            Dim sSUN As String = getUFValue(oForm, "IDHSUN")

            Dim sField As String = ""

            If sMON.Length <> 0 Then
                If sMON = "Y" Then
                    sField = "M"
                End If
            End If

            If sTUE.Length <> 0 Then
                If sTUE = "Y" Then
                    If sField.Length <> 0 Then
                        sField = sField + ","
                    End If
                    sField = sField + "Tu"
                End If
            End If

            If sWED.Length <> 0 Then
                If sWED = "Y" Then
                    If sField.Length <> 0 Then
                        sField = sField + ","
                    End If
                    sField = sField + "W"
                End If
            End If

            If sTHU.Length <> 0 Then
                If sTHU = "Y" Then
                    If sField.Length <> 0 Then
                        sField = sField + ","
                    End If
                    sField = sField + "Th"
                End If
            End If

            If sFRI.Length <> 0 Then
                If sFRI = "Y" Then
                    If sField.Length <> 0 Then
                        sField = sField + ","
                    End If
                    sField = sField + "F"
                End If
            End If

            If sSAT.Length <> 0 Then
                If sSAT = "Y" Then
                    If sField.Length <> 0 Then
                        sField = sField + ","
                    End If
                    sField = sField + "Sa"
                End If
            End If

            If sSUN.Length <> 0 Then
                If sSUN = "Y" Then
                    If sField.Length <> 0 Then
                        sField = sField + ","
                    End If
                    sField = sField + "Su"
                End If
            End If

            If sField.Length = 0 Then
                setParentSharedData(oForm, "IDHWDAY", "")
                Return False
            Else
                setParentSharedData(oForm, "IDHWDAY", sField)
                doReturnFromModalShared(oForm, True)
                Return True
            End If
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If goParent.doCheckModal(oForm.UniqueID) = True Then
                    If doPrepareModalData(oForm) = False Then
                        BubbleEvent = False
                    End If
                End If
            End If
        End Sub

        '** The ItemEvent handler
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Return True
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        End Sub

        Protected Overrides Sub doReturnCanceled(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String)
        End Sub

        Public Overrides Sub doClose()
        End Sub
    End Class
End Namespace
