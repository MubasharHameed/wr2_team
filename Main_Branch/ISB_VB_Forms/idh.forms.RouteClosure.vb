Imports System.IO
Imports System.Collections
Imports System

'Needed for the Framework controls
Imports IDHAddOns.idh.controls
Imports com.idh.bridge.data
Imports com.idh.bridge
Imports com.idh.dbObjects.User
Imports com.idh.bridge.lookups
Imports com.idh.controls
Imports com.idh.dbObjects.numbers

Namespace idh.forms
    Public Class RouteClosure
        Inherits IDHAddOns.idh.forms.Base

        Dim sHeadTable As String = "@IDH_ROUTECL" 'will be assigned a value in the constructor

#Region "Initialization"

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParentMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHROUTECL", sParentMenu, iMenuPosition, "RouteClosure.srf", False, True, False, "Route Closure", load_Types.idh_LOAD_NORMAL)

            sHeadTable = "@IDH_ROUTECL"

            doEnableHistory(sHeadTable)

        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)

        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            oForm.Freeze(True)

            Try
                oForm.DataSources.DBDataSources.Add("@IDH_ROUTECL")

                'Mapp fields with data fields 
                doAddFormDF(oForm, "IDH_RCLCD", "Code")
                doAddFormDF(oForm, "IDH_RCLOB", "U_RCLOB")
                doAddFormDF(oForm, "IDH_CHKRSQ", "U_RCRSEQ")
                doAddFormDF(oForm, "IDH_CHKDOT", "U_RCDOTLS")
                doAddFormDF(oForm, "IDH_RCDATE", "U_RCDATE")
                doAddFormDF(oForm, "IDH_RCTRUC", "U_RCTRUCK")
                doAddFormDF(oForm, "IDH_RCROUT", "U_RCROUTE")
                doAddFormDF(oForm, "IDH_CHKPR", "U_RCPROUTE")
                doAddFormDF(oForm, "IDH_RCFUEL", "U_RCFUEL")
                doAddFormDF(oForm, "IDH_RCEXTY", "U_RCEXTYD")
                doAddFormDF(oForm, "IDH_RCINTY", "U_RCINTYD")
                doAddFormDF(oForm, "IDH_RCTANK", "U_RCTANK")
                doAddFormDF(oForm, "IDH_RCTSTO", "U_RCTSTOP1")
                doAddFormDF(oForm, "IDH_RCTLIF", "U_RCTLIFT")
                doAddFormDF(oForm, "IDH_RCEXTH", "U_RCEXTHOME")
                doAddFormDF(oForm, "IDH_RCINTH", "U_RCINTHOME")

                'doAddFormDF(oForm, "IDH_RCDRIVER", "U_RCDRIVER")
                'doAddFormDF(oForm, "IDH_RCCLOCKIN", "U_RCCLOCKIN")
                'doAddFormDF(oForm, "IDH_RCCLOCKOUT", "U_RCCLOCKOUT")
                'doAddFormDF(oForm, "IDH_RCTHOUR1", "U_RCTHOUR1")

                doAddFormDF(oForm, "IDH_PLANCI", "U_RCPLANCIT")
                doAddFormDF(oForm, "IDH_PLANCO", "U_RCPLANCOT")
                doAddFormDF(oForm, "IDH_PLANYD", "U_RCPLANYDS")
                doAddFormDF(oForm, "IDH_THOUR2", "U_RCTHOUR2")
                doAddFormDF(oForm, "IDH_TIMEOY", "U_RCTIMEOY")
                doAddFormDF(oForm, "IDH_TIMEIY", "U_RCTIMEIY")
                doAddFormDF(oForm, "IDH_THOUR3", "U_RCTHOUR3")
                doAddFormDF(oForm, "IDH_MILESR", "U_RCMILESRT")
                doAddFormDF(oForm, "IDH_MILEEN", "U_RCMILEEND")
                doAddFormDF(oForm, "IDH_TMILE", "U_RCTMILE")
                doAddFormDF(oForm, "IDH_ADMINR", "U_RCADMINR")
                doAddFormDF(oForm, "IDH_ADMINH", "U_RCADMINH")
                doAddFormDF(oForm, "IDH_MEALS", "U_RCMEALS")
                doAddFormDF(oForm, "IDH_MEALE", "U_RCMEALE")
                doAddFormDF(oForm, "IDH_SAFEMT", "U_RCSAFEMT")
                doAddFormDF(oForm, "IDH_BREAK1", "U_RCBREAK1")
                doAddFormDF(oForm, "IDH_BREAK2", "U_RCBREAK2")

                'doAddFormDF(oForm, "IDH_RCCOMM", "U_RCCOMM") 'not added 

                'doAddFormDF(oForm, "IDH_RCTSTOP2", "U_RCTSTOP2")
                'doAddFormDF(oForm, "IDH_RCTCONTS", "U_RCTCONTS")
                'doAddFormDF(oForm, "IDH_RCTYARDS", "U_RCTYARDS")
                'doAddFormDF(oForm, "IDH_RCTONS", "U_RCTONS")

                'Form level settings 
                oForm.DataBrowser.BrowseBy = "IDH_RCLCD"

                oForm.SupportedModes = -1
                oForm.AutoManaged = True

                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
                BubbleEvent = False
            End Try


            oForm.Freeze(False)

            'MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

#End Region

#Region "Loading Data"

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            'Using the Form's main table
            Dim sRCCode As String = getFormDFValue(oForm, "Code").ToString()

            If Not sRCCode Is Nothing AndAlso sRCCode.Length > 0 Then
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE

                Dim oCons As New SAPbouiCOM.Conditions
                Dim oCon As SAPbouiCOM.Condition

                oCon = oCons.Add
                oCon.Alias = "Code"
                oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                oCon.CondVal = sRCCode
                oForm.DataSources.DBDataSources.Item("@IDH_ROUTECL").Query(oCons)

                doFillGrids(oForm, sRCCode)
            End If

            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                doCreateNewEntry(oForm)
                'ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                '    doCreateFindEntry(oForm)
            End If
            sRCCode = "-999"
            oForm.Freeze(False)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)

            MyBase.doBeforeLoadData(oForm)
            doFillCombo(oForm, "IDH_RCLOB", "OUBR", "Code", "Remarks")
            doFillAdminReason(oForm)

            doManageEmployeeGrids(oForm)
            doManageLandfillGrids(oForm)
            doManageDownTimeGrids(oForm)

            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            doCreateFindEntry(oForm)
            doFillGrids(oForm, "-1")

            oForm.Items.Item("IDH_THOUR2").AffectsFormMode = False
            oForm.Items.Item("IDH_THOUR3").AffectsFormMode = False
            oForm.Items.Item("IDH_TMILE").AffectsFormMode = False


            oForm.Freeze(False)
        End Sub

#End Region

#Region "Unloading Data"

        Public Overrides Sub doClose()
        End Sub

#End Region

#Region "Event Handlers"

        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                If pVal.MenuUID = Config.NAV_ADD Then
                    oForm.Freeze(True)
                    doCreateNewEntry(oForm)
                    'Dim sCode As String = getFormDFValue(oForm, "Code").ToString()

                    'If sCode Is Nothing OrElse sCode.Length = 0 Then
                    '    Dim oNumbers As DataHandler.NextNumbers = New DataHandler.NextNumbers(IDH_ROUTECL.AUTONUMPREFIX, "RTCLO")
                    '    setFormDFValue(oForm, "Code", oNumbers.CodeCode)
                    'End If
                    oForm.Freeze(False)
                ElseIf pVal.MenuUID = Config.NAV_FIND OrElse
                       pVal.MenuUID = Config.NAV_FIRST OrElse
                       pVal.MenuUID = Config.NAV_LAST OrElse
                       pVal.MenuUID = Config.NAV_NEXT OrElse
                       pVal.MenuUID = Config.NAV_PREV Then
                    oForm.Freeze(True)
                    Try
                        If pVal.MenuUID = Config.NAV_FIND Then
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                            doCreateFindEntry(oForm)
                            doSetFocus(oForm, "CODE")
                            DisableAllEditItems(oForm, {"IDH_RCLCD"})
                        Else
                            Dim oExclude() As String = {"IDH_RCLCD", "IDH_THOUR2", "IDH_THOUR3", "IDH_TMILE"}
                            doLoadData(oForm)
                            EnableAllEditItems(oForm, oExclude)
                            doSetGridsEnable(oForm, True)
                        End If
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Menu Event - " & oForm.UniqueID & "." & pVal.MenuUID)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try
                End If
            End If
            Return True
        End Function

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If pVal.BeforeAction = True Then
                    If pVal.CharPressed = 13 Then
                        goParent.goApplication.SendKeys("{TAB}")
                        BubbleEvent = False
                    End If
                Else
                    If pVal.ItemUID = "IDH_RCROUT" Then
                        If pVal.CharPressed = 9 Then
                            If getItemValue(oForm, "IDH_RCROUT").StartsWith("*") OrElse getItemValue(oForm, "IDH_RCROUT").EndsWith("*") Then
                                goParent.doOpenModalForm("IDHROSRC", oForm)
                                BubbleEvent = False
                            End If
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                'If pVal.BeforeAction = False Then
                '    If pVal.ItemUID = "IDH_RCROUT" Then
                '        If pVal.CharPressed = 9 Then
                '            If getItemValue(oForm, "IDH_RCROUT").StartsWith("*") OrElse getItemValue(oForm, "IDH_RCROUT").EndsWith("*") Then
                '                goParent.doOpenModalForm("IDHROSRC", oForm)
                '                BubbleEvent = False
                '            End If
                '        End If
                '    End If
                'End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemChanged Then
                        If pVal.ItemUID = "IDH_PLANCI" OrElse pVal.ItemUID = "IDH_PLANCO" Then
                            Dim iClockIN As Int16 = getDFValue(oForm, "@IDH_ROUTECL", "U_RCPLANCIT") 'getItemValue(oForm, "IDH_PLANCI")
                            Dim iClockOUT As Int16 = getDFValue(oForm, "@IDH_ROUTECL", "U_RCPLANCOT") ' getItemValue(oForm, "IDH_PLANCO")

                            If iClockOUT >= iClockIN Then

                                setDFValue(oForm, "@IDH_ROUTECL", "U_RCTHOUR2", Config.getTimeAsString(iClockIN, iClockOUT, ".")) '(iClockOUT - iClockIN).ToString())
                                'Dim oI As SAPbouiCOM.Item
                                'Dim oTB As SAPbouiCOM.EditText
                                'oI = oForm.Items.Item("IDH_THOUR2")
                                'oI.Enabled = False
                                'oTB = oI.Specific


                                'oTB.Value = (iClockOUT - iClockIN).ToString()
                            End If
                            BubbleEvent = False
                        ElseIf pVal.ItemUID = "IDH_TIMEOY" OrElse pVal.ItemUID = "IDH_TIMEIY" Then
                            Dim iTimeOut As Int16 = getItemValue(oForm, "IDH_TIMEOY")
                            Dim iTimeIn As Int16 = getItemValue(oForm, "IDH_TIMEIY")
                            If iTimeIn >= iTimeOut Then
                                setDFValue(oForm, "@IDH_ROUTECL", "U_RCTHOUR3", Config.getTimeAsString(iTimeOut, iTimeIn, ".")) '(iClockOUT - iClockIN).ToString())

                                'Dim oI As SAPbouiCOM.Item
                                'Dim oTB As SAPbouiCOM.EditText
                                'oI = oForm.Items.Item("IDH_THOUR3")
                                'oI.Enabled = False
                                'oTB = oI.Specific


                                'oTB.Value = (iTimeOut - iTimeIn).ToString()
                            End If
                            BubbleEvent = False
                        ElseIf pVal.ItemUID = "IDH_MILESR" OrElse pVal.ItemUID = "IDH_MILEEN" Then
                            Dim iMileStart As Int64 = getItemValue(oForm, "IDH_MILESR")
                            Dim iMileEnd As Int64 = getItemValue(oForm, "IDH_MILEEN")
                            If iMileEnd >= iMileStart Then
                                Dim oI As SAPbouiCOM.Item
                                Dim oTB As SAPbouiCOM.EditText
                                oI = oForm.Items.Item("IDH_TMILE")
                                oI.Enabled = False
                                oTB = oI.Specific
                                oTB.Value = (iMileEnd - iMileStart).ToString()
                            End If
                            BubbleEvent = False
                        End If
                    End If
                End If
            End If
            'Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            Return True
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    oForm.Freeze(True)
                    Dim sCode As String = getFormDFValue(oForm, "Code").ToString()
                    If sCode Is Nothing OrElse sCode.Length = 0 Then
                        Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(IDH_ROUTECL.AUTONUMPREFIX, "RTCLO")
                        setFormDFValue(oForm, IDH_ROUTECL._Code, oNumbers.CodeCode)
                        setFormDFValue(oForm, IDH_ROUTECL._Name, oNumbers.NameCode)
                    End If

                    If doUpdateHeader(oForm, True) Then
                        doUpdateGridRows(oForm)

                        oForm.Update()
                        doCreateNewEntry(oForm)
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                        BubbleEvent = False
                        'doLoadData(oForm) ''Commented to avoid issue when adding two consecutive records. 
                    End If
                    oForm.Freeze(False)
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    oForm.Freeze(True)
                    'Find record 
                    If doFind(oForm) Then
                        doSetFocus(oForm, "IDH_RCLOB")
                        EnableAllEditItems(oForm, {"IDH_RCLCD"})
                        doSetGridsEnable(oForm, True)
                    End If
                    oForm.Freeze(False)
                    BubbleEvent = False
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    oForm.Freeze(True)
                    If doUpdateHeader(oForm, False) Then
                        doUpdateGridRows(oForm)
                        oForm.Update()
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        BubbleEvent = False
                    End If
                    oForm.Freeze(False)
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    'If goParent.doCheckModal(oForm.UniqueID) = True Then
                    '    If doPrepareModalData(oForm) = False Then
                    '        BubbleEvent = False
                    '    End If
                    'End If
                End If
            Else
            End If

        End Sub

        Public Overrides Function doCustomItemEvent(oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            'If MyBase.doCustomItemEvent(oForm, pVal) = False Then
            '    Return False
            'End If
            If pVal.ItemUID = "EMPLGR" OrElse pVal.ItemUID = "LANDFGR" OrElse pVal.ItemUID = "DOWNTGR" Then
                If pVal.BeforeAction = False Then
                    oForm.Freeze(True)
                    Dim oGrid As DBOGrid = getWFValue(oForm, pVal.ItemUID)
                    If Not oGrid Is Nothing Then
                        If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_ADD_EMPTY Then
                            ''
                        ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED Then
                            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE _
                                    OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                doSetUpdate(oForm)
                            End If

                            'Calculate the Clock Hours column in Employee grid 
                            If pVal.ItemUID = "EMPLGR" Then
                                If pVal.ColUID = IDH_RCEMPLOYEE._RCCLOCKIN OrElse pVal.ColUID = IDH_RCEMPLOYEE._RCCLOCKOUT Then
                                    Dim iClockIN As Int16 = pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_RCEMPLOYEE._RCCLOCKIN), pVal.Row)
                                    Dim iClockOUT As Int16 = pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_RCEMPLOYEE._RCCLOCKOUT), pVal.Row)
                                    If iClockOUT >= iClockIN Then
                                        'Dim iClkHours As Int16 = iClockOUT - iClockIN
                                        'pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_RCEMPLOYEE._RCTHOUR1), pVal.Row, iClkHours)
                                        Dim sTime As String = Config.getTimeAsString(iClockIN, iClockOUT, ".")
                                        pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_RCEMPLOYEE._RCTHOUR1), pVal.Row, sTime)
                                    End If
                                End If
                            End If
                        ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SEARCH Then
                        ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
                        ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY Then
                            Dim sRCCode As String = getItemValue(oForm, "IDH_RCLCD").ToString.Trim
                            If sRCCode = "" AndAlso oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                Return False
                            End If
                            doSetLastLine(oForm, pVal.Row, pVal.ItemUID)
                        End If
                    End If
                    oForm.Freeze(False)
                End If
            End If
            Return MyBase.doCustomItemEvent(oForm, pVal)
        End Function

        Protected Overrides Sub doHandleModalResultShared(oForm As SAPbouiCOM.Form, sModalFormType As String, Optional sLastButton As String = Nothing)
            If sModalFormType = "IDHROSRC" Then
                Dim sCode As String = getSharedData(oForm, "IDH_RTCODE")
                setItemValue(oForm, "IDH_RCROUT", sCode)
            End If
            'MyBase.doHandleModalResultShared(oParentForm, sModalFormType, sLastButton)
        End Sub
#End Region

#Region "Custom Functions"

        Protected Function doUpdateHeader(ByVal oForm As SAPbouiCOM.Form, ByVal bIsAdd As Boolean) As Boolean
            'This will now Automatically update all the current DB fields and
            'generate and set the Code value if the code was not set
            'Return doAutoSave(oForm, IDH_ROUTECL.AUTONUMPREFIX, "RTCLO")
            Return doAutoSave(oForm, Nothing, "RTCLO")
        End Function

        Public Overridable Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form)
            Dim sCode As String = ""

            setFormDFValue(oForm, "Code", sCode)
            'setFormDFValue(oForm, "Name", sCode)
            setFormDFValue(oForm, "U_RCLOB", "")
            setFormDFValue(oForm, "U_RCRSEQ", "")
            setFormDFValue(oForm, "U_RCDOTLS", "")
            setFormDFValue(oForm, "U_RCDATE", "")
            setFormDFValue(oForm, "U_RCTRUCK", "")
            setFormDFValue(oForm, "U_RCROUTE", "")
            setFormDFValue(oForm, "U_RCPROUTE", "")
            setFormDFValue(oForm, "U_RCFUEL", "")
            setFormDFValue(oForm, "U_RCEXTYD", "")
            setFormDFValue(oForm, "U_RCINTYD", "")
            setFormDFValue(oForm, "U_RCTANK", "")
            setFormDFValue(oForm, "U_RCTSTOP1", "")
            setFormDFValue(oForm, "U_RCTLIFT", "")
            setFormDFValue(oForm, "U_RCEXTHOME", "")
            setFormDFValue(oForm, "U_RCINTHOME", "")

            setFormDFValue(oForm, "U_RCPLANCIT", "00:00")
            setFormDFValue(oForm, "U_RCPLANCOT", "00:00")
            setFormDFValue(oForm, "U_RCPLANYDS", "")
            setFormDFValue(oForm, "U_RCTHOUR2", "00:00")
            setFormDFValue(oForm, "U_RCTIMEOY", "00:00")
            setFormDFValue(oForm, "U_RCTIMEIY", "00:00")
            setFormDFValue(oForm, "U_RCTHOUR3", "00:00")
            setFormDFValue(oForm, "U_RCMILESRT", "")
            setFormDFValue(oForm, "U_RCMILEEND", "")
            setFormDFValue(oForm, "U_RCTMILE", "")
            setFormDFValue(oForm, "U_RCADMINR", "")
            setFormDFValue(oForm, "U_RCADMINH", "00:00")
            setFormDFValue(oForm, "U_RCMEALS", "00:00")
            setFormDFValue(oForm, "U_RCMEALE", "00:00")
            setFormDFValue(oForm, "U_RCSAFEMT", "00:00")
            setFormDFValue(oForm, "U_RCBREAK1", "00:00")
            setFormDFValue(oForm, "U_RCBREAK2", "00:00")
            doSetFocus(oForm, "IDH_RCLOB")

            Dim oExclude() As String = {"IDH_RCLCD", "IDH_THOUR2", "IDH_THOUR3", "IDH_TMILE"}

            EnableAllEditItems(oForm, oExclude)
            doSetGridsEnable(oForm, True)
            'doSetLastLine(oForm, 0, "EMPLGR")
            doFillGrids(oForm, "-1")
            oForm.Update()
        End Sub

        Public Overridable Sub doCreateFindEntry(ByVal oForm As SAPbouiCOM.Form)
            setFormDFValue(oForm, "Code", "", True)
            'setFormDFValue(oForm, "Name", sCode)
            setFormDFValue(oForm, "U_RCLOB", "")
            setFormDFValue(oForm, "U_RCRSEQ", "")
            setFormDFValue(oForm, "U_RCDOTLS", "")
            setFormDFValue(oForm, "U_RCDATE", "")
            setFormDFValue(oForm, "U_RCTRUCK", "")
            setFormDFValue(oForm, "U_RCROUTE", "")
            setFormDFValue(oForm, "U_RCPROUTE", "")
            setFormDFValue(oForm, "U_RCFUEL", "")
            setFormDFValue(oForm, "U_RCEXTYD", "")
            setFormDFValue(oForm, "U_RCINTYD", "")
            setFormDFValue(oForm, "U_RCTANK", "")
            setFormDFValue(oForm, "U_RCTSTOP1", "")
            setFormDFValue(oForm, "U_RCTLIFT", "")
            setFormDFValue(oForm, "U_RCEXTHOME", "")
            setFormDFValue(oForm, "U_RCINTHOME", "")

            setFormDFValue(oForm, "U_RCPLANCIT", "")
            setFormDFValue(oForm, "U_RCPLANCOT", "")
            setFormDFValue(oForm, "U_RCPLANYDS", "")
            setFormDFValue(oForm, "U_RCTHOUR2", "")
            setFormDFValue(oForm, "U_RCTIMEOY", "")
            setFormDFValue(oForm, "U_RCTIMEIY", "")
            setFormDFValue(oForm, "U_RCTHOUR3", "")
            setFormDFValue(oForm, "U_RCMILESRT", "")
            setFormDFValue(oForm, "U_RCMILEEND", "")
            setFormDFValue(oForm, "U_RCTMILE", "")
            setFormDFValue(oForm, "U_RCADMINR", "")
            setFormDFValue(oForm, "U_RCADMINH", "")
            setFormDFValue(oForm, "U_RCMEALS", "")
            setFormDFValue(oForm, "U_RCMEALE", "")
            setFormDFValue(oForm, "U_RCSAFEMT", "")
            setFormDFValue(oForm, "U_RCBREAK1", "")
            setFormDFValue(oForm, "U_RCBREAK2", "")

            Dim oExclude() As String = {"IDH_RCLCD"}
            DisableAllEditItems(oForm, oExclude)
            doSetGridsEnable(oForm, False)
            oForm.Update()
        End Sub

        Protected Sub doSetMode(ByVal oForm As SAPbouiCOM.Form, ByVal iMode As SAPbouiCOM.BoFormMode)
            If iMode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                oForm.Items.Item("1").Specific.Caption = getTranslatedWord("Find")
            ElseIf iMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                oForm.Items.Item("1").Specific.Caption = getTranslatedWord("Update")
                doSetUpdate(oForm)
            ElseIf iMode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                oForm.Items.Item("1").Specific.Caption = getTranslatedWord("Ok")
            End If
        End Sub

        Private Sub doFillAdminReason(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDH_ADMINR")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = oItem.Specific
            doClearValidValues(oCombo.ValidValues)

            Dim sAdminReasons As String = Config.Parameter("RTCLRSON")
            Try
                For Each sAR As String In sAdminReasons.Split(",")
                    oCombo.ValidValues.Add(sAR, sAR)
                Next
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Admin Reason Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Admin Reason")})
            Finally
            End Try

        End Sub

        Private Function doFind(ByVal oForm As SAPbouiCOM.Form) As Boolean
            'Using the Form's main table
            Dim sRCCode As String = getItemValue(oForm, "IDH_RCLCD").Trim()

            If Not sRCCode Is Nothing AndAlso sRCCode.Length > 0 Then

                Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                Dim sQry As String = "SELECT Top 1 Code from [@IDH_ROUTECL] Where Code = '" & sRCCode & "'"
                oRecordSet.DoQuery(sQry)
                If oRecordSet.RecordCount > 0 Then
                    setFormDFValue(oForm, "Code", sRCCode)
                    doLoadData(oForm)
                    Return True
                Else
                    'com.idh.bridge.DataHandler.INSTANCE.doError("No Records were found: " & sRCCode)
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("No Records were found: " & sRCCode, "ERUSNRFR", {sRCCode})
                    Return False
                End If
            End If
            Return False
        End Function

        Private Sub doManageEmployeeGrids(ByVal oForm As SAPbouiCOM.Form)
            'Employee Grid 
            Dim oEmpGrid As DBOGrid = getWFValue(oForm, "EMPLGR")
            Dim oRCEmployee As IDH_RCEMPLOYEE = Nothing
            If oEmpGrid Is Nothing Then
                oEmpGrid = DBOGrid.getInstance(oForm, "EMPLGR")
                If oEmpGrid Is Nothing Then
                    If oRCEmployee Is Nothing Then
                        oRCEmployee = New IDH_RCEMPLOYEE(Me, oForm)
                        'oRCEmployee.U_ROUTECL = getFormDFValue(oForm, "U_ROUTECL")
                    End If

                    oEmpGrid = New DBOGrid(Me, oForm, "EMPLGR", "EMPLGR", oRCEmployee)
                    oEmpGrid.getSBOItem().AffectsFormMode = True
                    oEmpGrid.doSetDeleteActive(True)
                Else
                    oRCEmployee = oEmpGrid.DBObject()
                End If
                setWFValue(oForm, "EMPLGR", oEmpGrid)
            Else
                oRCEmployee = oEmpGrid.DBObject()
            End If
            oRCEmployee.getData()
            'If oRCEmployee.Handler_TotalWeightChanged Is Nothing Then
            '    oRCEmployee.Handler_TotalWeightChanged = New IDH_DODEDUCT.ev_DBObject_Event(AddressOf handleDeductionTotalWeightChanged)
            'End If

            'If oRCEmployee.Handler_TotalValueChanged Is Nothing Then
            '    oRCEmployee.Handler_TotalValueChanged = New IDH_DODEDUCT.ev_DBObject_Event(AddressOf handleDeductionTotalValueChanged)
            'End If

            oEmpGrid.doAddListField(IDH_RCEMPLOYEE._Code, "Code", False, 0, Nothing, Nothing)
            oEmpGrid.doAddListField(IDH_RCEMPLOYEE._Name, "Name", False, 0, Nothing, Nothing)
            oEmpGrid.doAddListField(IDH_RCEMPLOYEE._ROUTECL, "RC Code", False, 0, Nothing, Nothing)
            oEmpGrid.doAddListField(IDH_RCEMPLOYEE._RCEMP, "Employee", True, -1, "COMBOBOX", Nothing)
            oEmpGrid.doAddListField(IDH_RCEMPLOYEE._RCPRIM, "Primary", True, -1, "CHECKBOX", Nothing)
            oEmpGrid.doAddListField(IDH_RCEMPLOYEE._RCRTASST, "Route Assistant", True, -1, "CHECKBOX", Nothing)
            oEmpGrid.doAddListField(IDH_RCEMPLOYEE._RCCLOCKIN, "Clock In", True, -1, Nothing, Nothing)
            oEmpGrid.doAddListField(IDH_RCEMPLOYEE._RCCLOCKOUT, "Clock Out", True, -1, Nothing, Nothing)
            oEmpGrid.doAddListField(IDH_RCEMPLOYEE._RCTHOUR1, "Clock Hours", False, -1, Nothing, Nothing)

            'oGridControl.doAddListField(IDH_DOADDEXP._FPCost, "Freeze Cost Price", bCanEdit, -1, "CHECKBOX", null, -1);
            ' oEmpGrid.doAddListField(IDH_RCEMPLOYEE._RCCLOCKIN, "Clock In", True, -1, "SRC:IDHISRC(DED)[IDH_ITMCOD][ITEMCODE;U_ItemDsc=ITEMNAME;U_ItmWgt=DEFWEI;U_UOM=SUOM]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            'oEmpGrid.doAddListField(IDH_RCEMPLOYEE._ItmWgt, "Item Weight", True, -1, Nothing, Nothing)
            'oEmpGrid.doAddListField(IDH_RCEMPLOYEE._UOM, "Unit Of Measure", True, -1, "COMBOBOX", Nothing)
            'oEmpGrid.doAddListField(IDH_RCEMPLOYEE._WgtDed, "Weight Deduction", True, -1, Nothing, Nothing)
            'oEmpGrid.doAddListField(IDH_RCEMPLOYEE._ValDed, "Value Deduction", True, -1, Nothing, Nothing)
            oEmpGrid.doSynchFields()
            'LandFill Grid 

            'Down Time Grid 

        End Sub

        Private Sub doManageLandfillGrids(ByVal oForm As SAPbouiCOM.Form)
            'Employee Grid 
            Dim oLandFillGrid As DBOGrid = getWFValue(oForm, "LANDFGR")
            Dim oRCLandFill As IDH_RCLANDFILL = Nothing
            If oLandFillGrid Is Nothing Then
                oLandFillGrid = DBOGrid.getInstance(oForm, "LANDFGR")
                If oLandFillGrid Is Nothing Then
                    If oRCLandFill Is Nothing Then
                        oRCLandFill = New IDH_RCLANDFILL(Me, oForm)
                        'oRCEmployee.U_ROUTECL = getFormDFValue(oForm, "U_ROUTECL")
                    End If

                    oLandFillGrid = New DBOGrid(Me, oForm, "LANDFGR", "LANDFGR", oRCLandFill)
                    oLandFillGrid.getSBOItem().AffectsFormMode = True
                    oLandFillGrid.doSetDeleteActive(True)
                Else
                    oRCLandFill = oLandFillGrid.DBObject()
                End If
                setWFValue(oForm, "LANDFGR", oLandFillGrid)
            Else
                oRCLandFill = oLandFillGrid.DBObject()
            End If
            oRCLandFill.getData()
            'If oRCEmployee.Handler_TotalWeightChanged Is Nothing Then
            '    oRCEmployee.Handler_TotalWeightChanged = New IDH_DODEDUCT.ev_DBObject_Event(AddressOf handleDeductionTotalWeightChanged)
            'End If

            'If oRCEmployee.Handler_TotalValueChanged Is Nothing Then
            '    oRCEmployee.Handler_TotalValueChanged = New IDH_DODEDUCT.ev_DBObject_Event(AddressOf handleDeductionTotalValueChanged)
            'End If

            oLandFillGrid.doAddListField(IDH_RCLANDFILL._Code, "Code", False, 0, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._Name, "Name", False, 0, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._ROUTECL, "RC Code", False, 0, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._RCONROUTE, "On Route", True, -1, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._RCOFFROUTE, "Off Route", True, -1, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._RCLOADTP, "Load Type", True, -1, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._RCSITE, "Site", True, -1, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._RCTIMEIN, "Time In", True, -1, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._RCTIMEOUT, "Clock Hours", True, -1, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._RCTKTNO, "Ticket No.", True, -1, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._RCTONS, "Tons", True, -1, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._RCTCOST, "Total Cost", True, -1, Nothing, Nothing)
            oLandFillGrid.doSynchFields()

        End Sub

        Private Sub doManageDownTimeGrids(ByVal oForm As SAPbouiCOM.Form)
            'Employee Grid 
            Dim oDownGrid As DBOGrid = getWFValue(oForm, "DOWNTGR")
            Dim oRCDownTime As IDH_RCDOWNT = Nothing
            If oDownGrid Is Nothing Then
                oDownGrid = DBOGrid.getInstance(oForm, "DOWNTGR")
                If oDownGrid Is Nothing Then
                    If oRCDownTime Is Nothing Then
                        oRCDownTime = New IDH_RCDOWNT(Me, oForm)
                        'oRCEmployee.U_ROUTECL = getFormDFValue(oForm, "DOWNTGR")
                    End If

                    oDownGrid = New DBOGrid(Me, oForm, "DOWNTGR", "DOWNTGR", oRCDownTime)
                    oDownGrid.getSBOItem().AffectsFormMode = True
                    oDownGrid.doSetDeleteActive(True)
                Else
                    oRCDownTime = oDownGrid.DBObject()
                End If
                setWFValue(oForm, "DOWNTGR", oDownGrid)
            Else
                oRCDownTime = oDownGrid.DBObject()
            End If
            oRCDownTime.getData()
            'If oRCEmployee.Handler_TotalWeightChanged Is Nothing Then
            '    oRCEmployee.Handler_TotalWeightChanged = New IDH_DODEDUCT.ev_DBObject_Event(AddressOf handleDeductionTotalWeightChanged)
            'End If

            'If oRCEmployee.Handler_TotalValueChanged Is Nothing Then
            '    oRCEmployee.Handler_TotalValueChanged = New IDH_DODEDUCT.ev_DBObject_Event(AddressOf handleDeductionTotalValueChanged)
            'End If

            oDownGrid.doAddListField(IDH_RCDOWNT._Code, "Code", False, 0, Nothing, Nothing)
            oDownGrid.doAddListField(IDH_RCDOWNT._Name, "Name", False, 0, Nothing, Nothing)
            oDownGrid.doAddListField(IDH_RCDOWNT._ROUTECL, "RC Code", False, 0, Nothing, Nothing)
            oDownGrid.doAddListField(IDH_RCDOWNT._RCTIMED, "Time Down", True, -1, Nothing, Nothing)
            oDownGrid.doAddListField(IDH_RCDOWNT._RCTIMEU, "Time Up", True, -1, Nothing, Nothing)
            oDownGrid.doAddListField(IDH_RCDOWNT._RCSPILL, "Spill", True, -1, "CHECKBOX", Nothing)
            oDownGrid.doAddListField(IDH_RCDOWNT._RCREASON, "Reason", True, -1, Nothing, Nothing)
            oDownGrid.doSynchFields()

        End Sub

        Private Sub doSetGridsEnable(ByVal oForm As SAPbouiCOM.Form, ByVal bEnable As Boolean)
            Dim oEmpGrid As DBOGrid = getWFValue(oForm, "EMPLGR")
            If Not oEmpGrid Is Nothing Then
                oEmpGrid = DBOGrid.getInstance(oForm, "EMPLGR")
                oEmpGrid.doEnabled(bEnable)
            End If

            Dim oLandFillGrid As DBOGrid = getWFValue(oForm, "LANDFGR")
            If Not oLandFillGrid Is Nothing Then
                oLandFillGrid = DBOGrid.getInstance(oForm, "LANDFGR")
                oLandFillGrid.doEnabled(bEnable)
            End If

            Dim oDownGrid As DBOGrid = getWFValue(oForm, "DOWNTGR")
            If Not oDownGrid Is Nothing Then
                oDownGrid = DBOGrid.getInstance(oForm, "DOWNTGR")
                oDownGrid.doEnabled(bEnable)
            End If
        End Sub

        Private Sub doFillGrids(ByVal oForm As SAPbouiCOM.Form, ByVal sRCCode As String)
            Dim oEmpGrid As DBOGrid = getWFValue(oForm, "EMPLGR")
            Dim oEData As IDH_RCEMPLOYEE = oEmpGrid.DBObject
            oEData.getByRCRow(sRCCode)
            oEmpGrid.doPostReloadData(True)
            doEmployeeCombo(oEmpGrid)

            Dim oLandGrid As DBOGrid = getWFValue(oForm, "LANDFGR")
            Dim oLFData As IDH_RCLANDFILL = oLandGrid.DBObject
            oLFData.getByRCRow(sRCCode)
            oLandGrid.doPostReloadData(True)

            Dim oDownGrid As DBOGrid = getWFValue(oForm, "DOWNTGR")
            Dim oDTData As IDH_RCDOWNT = oDownGrid.DBObject
            oDTData.getByRCRow(sRCCode)
            oDownGrid.doPostReloadData(True)

        End Sub

        Private Function doUpdateGridRows(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim sRCCode As String = getFormDFValue(oForm, "Code")

            Dim oEmpGrid As DBOGrid = getWFValue(oForm, "EMPLGR")
            If oEmpGrid.dataHasChanged Then
                For ctr As Integer = 0 To oEmpGrid.getRowCount() - 1
                    oEmpGrid.doSetFieldValue("U_ROUTECL", ctr, sRCCode, True)
                Next
                oEmpGrid.doProcessData()
            End If

            Dim oLandGrid As DBOGrid = getWFValue(oForm, "LANDFGR")
            If oLandGrid.dataHasChanged Then
                For ctr As Integer = 0 To oLandGrid.getRowCount() - 1
                    oLandGrid.doSetFieldValue("U_ROUTECL", ctr, sRCCode, True)
                Next
                oLandGrid.doProcessData()
            End If

            Dim oDownGrid As DBOGrid = getWFValue(oForm, "DOWNTGR")
            If oDownGrid.dataHasChanged Then
                For ctr As Integer = 0 To oDownGrid.getRowCount() - 1
                    oDownGrid.doSetFieldValue("U_ROUTECL", ctr, sRCCode, True)
                Next
                oDownGrid.doProcessData()
            End If

            Return True
        End Function

        Public Sub doSetLastLine(ByVal oForm As SAPbouiCOM.Form, iRow As Int32, sGridID As String)
            Dim oGridN As DBOGrid = DBOGrid.getInstance(oForm, sGridID)
            If iRow = -1 Then
                iRow = oGridN.getCurrentDataRowIndex()
            End If
            Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode("GENSEQ")
            oGridN.doSetFieldValue("Code", iRow, oNumbers.CodeCode, True)
            oGridN.doSetFieldValue("Name", iRow, oNumbers.NameCode, True)
            Dim sRCCode As String = getItemValue(oForm, "IDH_RCLCD").ToString.Trim
            oGridN.doSetFieldValue("U_ROUTECL", iRow, sRCCode, True)

            If sGridID = "EMPLGR" Then
                'oGridN.doSetFieldValue("U_RCEMP", iRow, "", True)
                'oGridN.doSetFieldValue("U_RCPRIM", iRow, "", True)
                'oGridN.doSetFieldValue("U_RCRTASST", iRow, "", True)
                oGridN.doSetFieldValue("U_RCCLOCKIN", iRow, "00:00", True)
                oGridN.doSetFieldValue("U_RCCLOCKOUT", iRow, "00:00", True)
                oGridN.doSetFieldValue("U_RCHOUR1", iRow, "00:00", True)
            ElseIf sGridID = "LANDFGR" Then
                oGridN.doSetFieldValue("U_RCONROUTE", iRow, "00:00", True)
                oGridN.doSetFieldValue("U_RCOFFROUTE", iRow, "00:00", True)
                'oGridN.doSetFieldValue("U_RCLOADTP", iRow, "", True)
                'oGridN.doSetFieldValue("U_RCSITE", iRow, "", True)
                oGridN.doSetFieldValue("U_RCTIMEIN", iRow, "00:00", True)
                oGridN.doSetFieldValue("U_RCTIMEOUT", iRow, "00:00", True)
                'oGridN.doSetFieldValue("U_RCTKTNO", iRow, "", True)
                oGridN.doSetFieldValue("U_RCTONS", iRow, 0, True)
                oGridN.doSetFieldValue("U_RCCOST", iRow, 0, True)
            ElseIf sGridID = "DOWNTGR" Then
                oGridN.doSetFieldValue("U_RCTIMED", iRow, "00:00", True)
                oGridN.doSetFieldValue("U_RCTIMEU", iRow, "00:00", True)
                'oGridN.doSetFieldValue("U_RCSPILL", iRow, "", True)
                'oGridN.doSetFieldValue("U_RCREASON", iRow, "", True)
            End If
        End Sub

        Private Sub doEmployeeCombo(ByVal oGridN As DBOGrid)
            'Combobox2.doFillCombo(oCombo, "OHEM", "empID", "(CAST(empID as nvarchar) + ' - ' + lastName + ' ' + firstName) As Detail", null, "lastName", sBlankDescription);
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC(IDH_RCEMPLOYEE._RCEMP))
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

                doFillCombo(oGridN.getSBOForm(), oCombo, "OHEM", "empID", "(CAST(empID as nvarchar) + ' - ' + lastName + ' ' + firstName) As Detail", "JobTitle Like '%Driver%'", Nothing, Nothing, Nothing)
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Employee")})
            End Try
        End Sub

#End Region

    End Class
End Namespace
