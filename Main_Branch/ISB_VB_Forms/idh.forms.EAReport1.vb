Imports System.IO
Imports System.Collections
Imports System
Imports com.isb.bridge.lookups

Namespace idh.forms
    Public Class EAReport1
        Inherits IDHAddOns.idh.forms.Base

        Private moSelectList As New IDHAddOns.idh.Grid.SelectList

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            'MyBase.New(oParent, "IDH_EAREP1", iMenuPosition, "EAReport1.srf", False, True, False)
            MyBase.New(oParent, "IDH_EAREP1", "IDHRE", iMenuPosition, "EAReport1.srf", False, True, False, "EA Report 1", load_Types.idh_LOAD_NORMAL)

            moSelectList.doAddField("h.U_BDate", "Order Date", False)
            moSelectList.doAddField("h.Code", "Order No.", False)

            moSelectList.doAddField("#", "Producer Name", False)
            moSelectList.doAddField("#", "Producer Registration Number", False)
            moSelectList.doAddField("#", "Producer Postcode", False)

            moSelectList.doAddField("r.U_ItemCd", "Container Code", False)
            moSelectList.doAddField("r.U_ItemDsc", "Container Desc.", False)

            moSelectList.doAddField("#", "Waste Code", False)
            moSelectList.doAddField("#", "Waste Material", False)

            moSelectList.doAddField("r.U_Weight", "Quantity", False)

            moSelectList.doAddField("#", "Disposal Site", False)
            moSelectList.doAddField("#", "Disposal Site Name", False)
            moSelectList.doAddField("#", "Waste Carrier", False)
            moSelectList.doAddField("#", "Waste Carrier Name", False)

        End Sub


        '        '*** Create Sub-Menu
        '        Protected Overrides Sub doCreateSubMenu()
        '            Dim oMenus As SAPbouiCOM.Menus
        '            Dim oMenuItem As SAPbouiCOM.MenuItem
        '            Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
        '
        '            oMenuItem = goParent.goApplication.Menus.Item("IDHRE")
        '
        '            oMenus = oMenuItem.SubMenus
        '            oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        '            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING
        '            oCreationPackage.UniqueID = gsType
        '            oCreationPackage.String = "EA Report 1"
        '            oCreationPackage.Position = giMenuPosition
        '
        '            Try
        '                oMenus.AddEx(oCreationPackage)
        '            Catch ex As Exception
        '            End Try
        '        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
        End Sub

        '** Do the final actions to show the form
        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            oForm.Visible = True
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                Dim oItem As SAPbouiCOM.Item
                oItem = oForm.Items.Add("LINESGRID", SAPbouiCOM.BoFormItemTypes.it_GRID)
                ' Set the grid dimensions and position
                oItem.Left = 7
                oItem.Top = 100
                oItem.Width = 785
                oItem.Height = 180

                oItem.AffectsFormMode = False

                Dim oGrid As SAPbouiCOM.Grid
                oGrid = oItem.Specific
                oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_None

                oForm.DataSources.DataTables.Add("IDH_JOBSHD")
                oGrid.DataTable = oForm.DataSources.DataTables.Item("IDH_JOBSHD")

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            'If oForm.Visible = False Then
            '    Exit Sub
            'End If
            oForm.Freeze(True)
            Try
                Dim oGrid As SAPbouiCOM.Grid
                oGrid = oForm.Items.Item("LINESGRID").Specific

                Dim sQryFilter As String = ""
                Dim sQrySelectList As String = moSelectList.doCreateSelectList

                Dim sQry As String = _
                    "   select " & _
                    sQrySelectList & _
                    "   from [@IDH_JOBSHD] r, [@IDH_JOBENTR] h" & _
                    "   where(r.U_JobNr = h.Code)"

                oGrid.DataTable.ExecuteQuery(sQry)
                moSelectList.doFormatFields(oGrid)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error loading the data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXLDBD", {Nothing})
            End Try
            oForm.Freeze(False)
        End Sub


        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
            End If
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_TABGEN" Then
                        oForm.PaneLevel = 1
                    ElseIf pVal.ItemUID = "IDH_TABCOV" Then
                        oForm.PaneLevel = 2
                    ElseIf pVal.ItemUID = "IDH_TABORD" Then
                        oForm.PaneLevel = 3
                    ElseIf pVal.ItemUID = "IDH_TABATT" Then
                        oForm.PaneLevel = 4
                    End If
                End If
            End If
            Return False
        End Function

        '** Create a new Entry if the Add option is selected
        Private Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        '** Clears all the fields when going into the find mode.
        Private Sub doClearAll(ByVal oForm As SAPbouiCOM.Form)
        End Sub


        '** The Menu Event handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                If pVal.MenuUID = Config.NAV_ADD Then
                    oForm.Freeze(True)
                    Try
                        doCreateNewEntry(oForm)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing the menu event - " & oForm.UniqueID & "." & pVal.MenuUID)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try
                ElseIf pVal.MenuUID = Config.NAV_FIND OrElse
                       pVal.MenuUID = Config.NAV_FIRST OrElse
                       pVal.MenuUID = Config.NAV_LAST OrElse
                       pVal.MenuUID = Config.NAV_NEXT OrElse
                       pVal.MenuUID = Config.NAV_PREV Then
                    oForm.Freeze(True)
                    Try
                        If pVal.MenuUID = Config.NAV_FIND Then
                            doClearAll(oForm)
                        Else
                            doLoadData(oForm)
                        End If
                    Catch ex As Exception
                    Finally
                        oForm.Freeze(False)
                    End Try
                End If
            End If
            Return True
        End Function

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCloseForm(oForm, BubbleEvent)
        End Sub

        Public Overrides Sub doClose()
        End Sub

        '** This method is called by the returning modal form to set the data before closing
        Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Buffered Modal results.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPBMR", {Nothing})
            End Try
        End Sub

        ''MODAL DIALOG
        ''*** Set the return data when called as a modal dialog 
        Private Sub doSetModalData(ByVal oForm As SAPbouiCOM.Form)
            Dim sMode As String = ""
            If ghOldDialogParams.Contains(oForm.UniqueID) Then
                Dim oData As ArrayList = ghOldDialogParams.Item(oForm.UniqueID)
                If Not (oData Is Nothing) AndAlso oData.Count > 2 Then
                    sMode = oData.Item(1)
                End If
            End If

            Dim aData As New ArrayList
            If sMode.Length = 0 Then
                sMode = "Add"
            Else
                sMode = "Change"
            End If

            With oForm.DataSources.DBDataSources.Item("@IDH_JOBENTR")
                aData.Add(sMode)
                aData.Add("")
            End With

            doReturnFromModalBuffered(oForm, aData)
        End Sub
    End Class
End Namespace
