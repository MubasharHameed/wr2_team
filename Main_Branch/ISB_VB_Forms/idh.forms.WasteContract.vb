Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge.lookups
Imports com.idh.bridge
Imports com.idh.dbObjects.User
Imports com.uBC.utils
Imports com.idh.bridge.resources

Namespace idh.forms
    Public Class WasteContract
        Inherits IDHAddOns.idh.forms.Base

        Private msSkipGrp As String = Nothing

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_WASCON", "IDHORD", iMenuPosition, "Waste Contract.srf", False, True, False, "Waste Contract")

            doEnableHistory("@IDH_WASTCONT")

            'doAddImagesToFix("IDH_CUSL")
            'doAddImagesToFix("IDH_BADD")
            'doAddImagesToFix("IDH_SLSL")
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
        End Sub

        '** Do the final actions to show the form
        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            oForm.Visible = True
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                msSkipGrp = Nothing

                Dim oItem As SAPbouiCOM.Item
                'AddChooseCustomerList(oForm)

                'oForm.DataSources.DBDataSources.Add("@IDH_WASTCONT")

                Dim oGridN As UpdateGrid = New UpdateGrid(Me, oForm, "LINESGRID", 30, 180, 745, 230)
                oGridN.getSBOItem.FromPane = 2
                oGridN.getSBOItem.ToPane = 2
                oGridN.getSBOGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_None
                'oGridN.setTableValue("[@IDH_JOBENTR]")
                'oGridN.setKeyField("Code")
                oGridN.doAddGridTable(New com.idh.bridge.data.GridTable("@IDH_JOBENTR", Nothing, "Code", False, True), True)

                oGridN.doAddListField("Code", "Code", False, 0, Nothing, Nothing)
                oGridN.doAddListField("Name", "Name", False, 0, Nothing, Nothing)
                oGridN.doAddListField("U_CntrNo", "Contract No.", False, 0, Nothing, Nothing)
                oGridN.doAddListField("U_ItemGrp", "Item Group", False, -1, Nothing, Nothing)
                'oGridN.doAddListField("U_ItemCd", "Container code", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_Qty", "Quantity", False, -1, Nothing, Nothing)
                'oGridN.doAddListField("U_WasCd", "Waste code", False, -1, Nothing, Nothing)
                'oGridN.doAddListField("U_WasDsc", "Waste description", False, -1, Nothing, Nothing)



                oGridN.doAddListField("U_Street", "Street", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_Block", "Block", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_ZpCd", "Postal Code", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_City", "City", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_Contact", "Contact Name", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_Phone1", "Phone Number", False, -1, Nothing, Nothing)


                oGridN.doAddListField("U_User", "Signed In User", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_Status", "Order Status", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_BDate", "Booking Date", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_BTime", "Booking Time", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_RSDate", "Request Start Date", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_RSTime", "Request End Time", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_REDate", "Request End Date", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_RETime", "Request End Time", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_CardCd", "Customer", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_CardNM", "Customer Name", False, -1, Nothing, Nothing)

                oGridN.doAddListField("U_Address", "Address", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_CustRef", "Customer Ref", False, -1, Nothing, Nothing)

                oGridN.doAddListField("U_ORoad", "Off Road", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_SLicSp", "Skip License Supplier", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_SLicNr", "Skip License No", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_SLicExp", "Skip License Expiry Date", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_SLicCh", "Skip License Charge", False, -1, Nothing, Nothing)
                oGridN.doAddListField("U_SpInst", "Special Instruction", False, -1, Nothing, Nothing)

                oGridN.doSetDoCount(True)

                doAddFormDF(oForm, "IDH_CONTRN", "Code")
                doAddFormDF(oForm, "IDH_CUST", "U_Cust")
                doAddFormDF(oForm, "IDH_CUSTNM", "U_CustNm")
                doAddFormDF(oForm, "IDH_CONTNM", "U_ContNm")
                doAddFormDF(oForm, "IDH_CUSREF", "U_CusRef")
                doAddFormDF(oForm, "IDH_SALEMP", "U_SalEmp")
                doAddFormDF(oForm, "IDH_OWNER", "U_Owner")
                doAddFormDF(oForm, "IDH_SPECIN", "U_SpecIn")
                doAddFormDF(oForm, "IDH_ADDRES", "U_Addres")
                doAddFormDF(oForm, "IDH_STREET", "U_Street")
                doAddFormDF(oForm, "IDH_BLOCK", "U_Block")
                doAddFormDF(oForm, "IDH_CITY", "U_City")
                doAddFormDF(oForm, "IDH_ZIP", "U_Zip")
                doAddFormDF(oForm, "IDH_PHONE", "U_Phone")
                doAddFormDF(oForm, "IDH_STAT", "U_Stat")
                ''##MA Start Issue#439
                doAddFormDF(oForm, "uBC_TRNCd", "U_TRNCd")
                ''##MA End Issue#439
                doAddFormDF(oForm, "IDH_STRDAT", "U_StarDat")
                doAddFormDF(oForm, "IDH_ENDDAT", "U_EndDat")
                doAddFormDF(oForm, "IDH_TERMDT", "U_TermDt")
                doAddFormDF(oForm, "IDH_EXP", "U_Exp")
                doAddFormDF(oForm, "IDH_DURMNT", "U_DurMnt")
                doAddFormDF(oForm, "IDH_CHKMNT", "U_ChkMnt")
                doAddFormDF(oForm, "IDH_REN", "U_Ren")
                doAddFormDF(oForm, "IDH_REMDAY", "U_RemDay")
                doAddFormDF(oForm, "IDH_CHKDAY", "U_ChkDay")
                doAddFormDF(oForm, "IDH_CONTYP", "U_ConTyp")
                doAddFormDF(oForm, "IDH_SERLEV", "U_SerLev")
                doAddFormDF(oForm, "IDH_EXCEPT", "U_Except")
                doAddFormDF(oForm, "IDH_EXCDET", "U_ExcDet")
                doAddFormDF(oForm, "IDH_PAYTRM", "U_PayTrm")
                doAddFormDF(oForm, "IDH_DEPREQ", "U_DepReq")
                doAddFormDF(oForm, "IDH_REVEN", "U_Reven")
                doAddFormDF(oForm, "IDH_COSTS", "U_Costs")
                doAddFormDF(oForm, "IDH_GP", "U_GP")
                doAddFormDF(oForm, "IDH_GPPRC", "U_GPPrc")
                doAddFormDF(oForm, "IDH_REMARK", "U_Remark")

                doAddFormDF(oForm, "IDH_ANAL1", "U_Analys1")
                doAddFormDF(oForm, "IDH_ANAL2", "U_Analys2")
                doAddFormDF(oForm, "IDH_ANAL3", "U_Analys3")
                doAddFormDF(oForm, "IDH_ANAL4", "U_Analys4")
                doAddFormDF(oForm, "IDH_ANAL5", "U_Analys5")

                oForm.DataBrowser.BrowseBy = "IDH_CONTRN"

                'CHOOSE CUSTOMER FROM LISTS
                'oItem = oForm.Items.Item("IDH_CUST")
                'Dim oEdit As SAPbouiCOM.EditText
                'oEdit = oItem.Specific
                'oEdit.ChooseFromListUID = "CFL1"
                'oEdit.ChooseFromListAlias = "CardCode"

                'Dim oButton As SAPbouiCOM.Button
                'oItem = oForm.Items.Item("IDH_CUSL")
                'oButton = oItem.Specific
                'oButton.ChooseFromListUID = "CFL2"

                'LINK(BUTTONS)
                Dim oLink As SAPbouiCOM.LinkedButton
                oLink = oForm.Items.Item("IDH_LINKCU").Specific
                oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

                oLink = oForm.Items.Item("IDH_PTLK").Specific
                oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_PaymentTermsTypes

                'oLink = oForm.Items.Item("IDH_LINKSE").Specific
                'oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_SalesEmployee

                '                Dim oCombo As SAPbouiCOM.ComboBox
                '                oItem = oForm.Items.Item("IDH_CHKMNT")
                '                oItem.DisplayDesc = True
                '                oCombo = oItem.Specific
                '                oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodDay(), com.idh.bridge.lookups.FixedValues.getPeriodDay())
                '                oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodMonth(), com.idh.bridge.lookups.FixedValues.getPeriodMonth())
                '                oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodYear(), com.idh.bridge.lookups.FixedValues.getPeriodYear())
                '
                '                oItem = oForm.Items.Item("IDH_CHKDAY")
                '                oItem.DisplayDesc = True
                '                oCombo = oItem.Specific
                '                oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodMin(), com.idh.bridge.lookups.FixedValues.getPeriodMin())
                '                oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodHour(), com.idh.bridge.lookups.FixedValues.getPeriodHour())
                '                oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodDay(), com.idh.bridge.lookups.FixedValues.getPeriodDay())
                '                oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodMonth(), com.idh.bridge.lookups.FixedValues.getPeriodMonth())
                '                oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodYear(), com.idh.bridge.lookups.FixedValues.getPeriodYear())
                '
                '                oItem = oForm.Items.Item("IDH_CONTYP")
                '                oItem.DisplayDesc = True
                '                oCombo = oItem.Specific
                '                oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getContTpSerialNo(), com.idh.bridge.lookups.FixedValues.getContTpSerialNo())
                '                oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getContTpCustomer(), com.idh.bridge.lookups.FixedValues.getContTpCustomer())
                '                oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getContTpItemGroup(), com.idh.bridge.lookups.FixedValues.getContTpItemGroup())
                '
                '                oItem = oForm.Items.Item("IDH_STAT")
                '                oItem.DisplayDesc = True
                '                oCombo = oItem.Specific
                '                oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getStatusDraft(), com.idh.bridge.lookups.FixedValues.getStatusDraft())
                '                oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getStatusApproved(), com.idh.bridge.lookups.FixedValues.getStatusApproved())
                '                oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getStatusOnHold(), com.idh.bridge.lookups.FixedValues.getStatusOnHold())
                '                oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getStatusTerminated(), com.idh.bridge.lookups.FixedValues.getStatusTerminated())
                '
                oItem = oForm.Items.Item("IDH_PAYTRM")
                oItem.DisplayDesc = True

                oItem = oForm.Items.Item("IDH_SALEMP")
                oItem.DisplayDesc = True

                'DO FIX THE BUTTON IMAGES
                oForm.SupportedModes = -1
                oForm.AutoManaged = False

                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE

                oForm.Items.Item("IDH_TABGEN").AffectsFormMode = False
                oForm.Items.Item("IDH_TABDET").AffectsFormMode = False
                oForm.Items.Item("IDH_TABREM").AffectsFormMode = False
                oForm.Items.Item("IDH_TABATT").AffectsFormMode = False

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            Dim oCombo As SAPbouiCOM.ComboBox
            oItem = oForm.Items.Item("IDH_CHKMNT")
            'oItem.DisplayDesc = True
            oCombo = oItem.Specific
            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodDay(), com.idh.bridge.lookups.FixedValues.getPeriodDay())
            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodMonth(), com.idh.bridge.lookups.FixedValues.getPeriodMonth())
            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodYear(), com.idh.bridge.lookups.FixedValues.getPeriodYear())

            oItem = oForm.Items.Item("IDH_CHKDAY")
            'oItem.DisplayDesc = True
            oCombo = oItem.Specific
            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodMin(), com.idh.bridge.lookups.FixedValues.getPeriodMin())
            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodHour(), com.idh.bridge.lookups.FixedValues.getPeriodHour())
            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodDay(), com.idh.bridge.lookups.FixedValues.getPeriodDay())
            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodMonth(), com.idh.bridge.lookups.FixedValues.getPeriodMonth())
            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodYear(), com.idh.bridge.lookups.FixedValues.getPeriodYear())

            oItem = oForm.Items.Item("IDH_CONTYP")
            'oItem.DisplayDesc = True
            oCombo = oItem.Specific
            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getContTpSerialNo(), com.idh.bridge.lookups.FixedValues.getContTpSerialNo())
            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getContTpCustomer(), com.idh.bridge.lookups.FixedValues.getContTpCustomer())
            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getContTpItemGroup(), com.idh.bridge.lookups.FixedValues.getContTpItemGroup())

            doFillStatus(oForm)
            doFillPaymentTern(oForm)
            doFillSalesEmployee(oForm)
            doAddWF(oForm, "WrkContractNumber", 0)
            ''##MA Start Issue#439
            FillCombos.TransactionCodeCombo(FillCombos.getCombo(oForm, "uBC_TRNCd"), Nothing, Nothing, "WO")
            ''##MA End Issue#439
            oForm.Items.Item("IDH_TABGEN").Click()
            oForm.PaneLevel = 1

            If getHasSharedData(oForm) Then
                Dim sCode As String = getParentSharedData(oForm, "IDH_CONTRN")
                If Not sCode Is Nothing AndAlso sCode.Length > 0 Then
                    setFormDFValue(oForm, "Code", sCode, True)
                    doFind(oForm)
                End If
            End If
        End Sub

        Public Sub doFillPaymentTern(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_PAYTRM", "OCTG", "GroupNum", "PymntGroup", Nothing, Nothing, True)
        End Sub

        Public Sub doFillSalesEmployee(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_SALEMP", "OSLP", "SlpCode", "SlpName", Nothing, Nothing, True)
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            Dim sContract As String = getFormDFValue(oForm, "Code", True)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            If sContract.Length = 0 Then
                Dim lContract As Long = getWFValue(oForm, "WrkContractNumber")
                If lContract < 0 Then
                    sContract = com.idh.utils.Conversions.ToString(lContract)
                Else
                    sContract = "-999"
                End If
            End If
            oGridN.setRequiredFilter("U_CntrNo = '" & sContract & "'")
            oGridN.doReloadData()

            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                doSwitchToFind(oForm)
            ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                doSwitchToAdd(oForm)
            ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                doSwitchToAdd(oForm)
            End If
        End Sub

        Public Sub doFillStatus(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            Dim oCombo As SAPbouiCOM.ComboBox

            oItem = oForm.Items.Item("IDH_STAT")
            'oItem.DisplayDesc = True
            oCombo = oItem.Specific
            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getStatusDraft(), com.idh.bridge.lookups.FixedValues.getStatusDraft())
            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getStatusApproved(), com.idh.bridge.lookups.FixedValues.getStatusApproved())
            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getStatusOnHold(), com.idh.bridge.lookups.FixedValues.getStatusOnHold())
            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getStatusTerminated(), com.idh.bridge.lookups.FixedValues.getStatusTerminated())
        End Sub

        '*** Validate the Header
        Private Function doValidateHeader(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim sCust As String = getFormDFValue(oForm, "U_Cust")

            Dim sError As String = ""
            If sCust.Length = 0 Then sError = sError + "Customer, "

            If sError.Length > 0 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("The following fields must be set: " & sError)
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("The following fields must be set: " & sError, "ERUSGENS", {com.idh.bridge.Translation.getTranslatedWord("The following fields: ") + com.idh.bridge.Translation.getTranslatedWord(sError)})
                Return False
            End If
            Return True
        End Function

        Protected Sub doSwitchToFind(ByVal oForm As SAPbouiCOM.Form)
            Dim oExclude() As String = {"IDH_CUST", "IDH_CUSTNM", "IDH_CONTRN", _
            "IDH_ADDRES", "IDH_CITY", "IDH_ZIP", "IDH_STAT", "IDH_STREET"}
            doSetFocus(oForm, "IDH_CUST")
            DisableAllEditItems(oForm, oExclude)
        End Sub

        Protected Sub doSwitchToAdd(ByVal oForm As SAPbouiCOM.Form)
            Dim oExclude() As String = {"IDH_CONTRN"}
            doSetFocus(oForm, "IDH_CUST")
            EnableAllEditItems(oForm, oExclude)
        End Sub

        Private Function doUpdateHeader(ByVal oForm As SAPbouiCOM.Form, ByVal bIsAdd As Boolean) As Boolean
            Dim iwResult As Integer = 0
            Dim swResult As String = Nothing

            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_WASTCONT")
            Try
                With oForm.DataSources.DBDataSources.Item("@IDH_WASTCONT")
                    Dim sCode As String = .GetValue("Code", .Offset).Trim()
                    If sCode.Length = 0 OrElse sCode.Chars(0) = "-" Then
                        'Dim iCode As Integer = DataHandler.INSTANCE.getNextNumber("CONNO")
                        'sCode = com.idh.utils.Conversions.ToString(iCode)
                        sCode = DataHandler.INSTANCE.getSingleCode("CONNO")
                    End If

                    If oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) Then
                        If bIsAdd Then
                            'com.idh.bridge.DataHandler.INSTANCE.doError("A Duplicate entries are not allowed")
                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Duplicate entries are not allowed", "ERUSDBDU", {Nothing})
                            Return False
                        End If
                    Else
                        oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
                        oAuditObj.setName(sCode)
                        setFormDFValue(oForm, "Code", sCode)
                    End If

                    oAuditObj.setFieldValue("U_Cust", .GetValue("U_Cust", .Offset).Trim())
                    oAuditObj.setFieldValue("U_CustNm", .GetValue("U_CustNm", .Offset).Trim())
                    oAuditObj.setFieldValue("U_ContNm", .GetValue("U_ContNm", .Offset).Trim())
                    oAuditObj.setFieldValue("U_CusRef", .GetValue("U_CusRef", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SalEmp", .GetValue("U_SalEmp", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Owner", .GetValue("U_Owner", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SpecIn", .GetValue("U_SpecIn", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Addres", .GetValue("U_Addres", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Street", .GetValue("U_Street", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Block", .GetValue("U_Block", .Offset).Trim())
                    oAuditObj.setFieldValue("U_City", .GetValue("U_City", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Zip", .GetValue("U_Zip", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Phone", .GetValue("U_Phone", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Stat", .GetValue("U_Stat", .Offset).Trim())
                    ''##MA Start Issue#439
                    oAuditObj.setFieldValue("U_TRNCd", .GetValue("U_TRNCd", .Offset).Trim())
                    ''##MA End Issue#439
                    oAuditObj.setFieldValue("U_StarDat", com.idh.utils.Dates.doStrToDate(.GetValue("U_StarDat", .Offset).Trim()))
                    oAuditObj.setFieldValue("U_EndDat", com.idh.utils.Dates.doStrToDate(.GetValue("U_EndDat", .Offset).Trim()))
                    oAuditObj.setFieldValue("U_TermDt", com.idh.utils.Dates.doStrToDate(.GetValue("U_TermDt", .Offset).Trim()))
                    oAuditObj.setFieldValue("U_Exp", com.idh.utils.Dates.doStrToDate(.GetValue("U_Exp", .Offset).Trim()))
                    oAuditObj.setFieldValue("U_DurMnt", .GetValue("U_DurMnt", .Offset).Trim())
                    oAuditObj.setFieldValue("U_ChkMnt", .GetValue("U_ChkMnt", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Ren", com.idh.utils.Dates.doStrToDate(.GetValue("U_Ren", .Offset).Trim()))
                    oAuditObj.setFieldValue("U_RemDay", .GetValue("U_RemDay", .Offset).Trim())
                    oAuditObj.setFieldValue("U_ChkDay", .GetValue("U_ChkDay", .Offset).Trim())
                    oAuditObj.setFieldValue("U_ConTyp", .GetValue("U_ConTyp", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SerLev", .GetValue("U_SerLev", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Except", .GetValue("U_Except", .Offset).Trim())
                    oAuditObj.setFieldValue("U_ExcDet", .GetValue("U_ExcDet", .Offset).Trim())
                    oAuditObj.setFieldValue("U_PayTrm", .GetValue("U_PayTrm", .Offset).Trim())
                    oAuditObj.setFieldValue("U_DepReq", .GetValue("U_DepReq", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Reven", .GetValue("U_Reven", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Costs", .GetValue("U_Costs", .Offset).Trim())
                    oAuditObj.setFieldValue("U_GP", .GetValue("U_GP", .Offset).Trim())
                    oAuditObj.setFieldValue("U_GPPrc", .GetValue("U_GPPrc", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Remark", .GetValue("U_Remark", .Offset).Trim())

                    oAuditObj.setFieldValue("U_Analys1", .GetValue("U_Analys1", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Analys2", .GetValue("U_Analys2", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Analys3", .GetValue("U_Analys3", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Analys4", .GetValue("U_Analys4", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Analys5", .GetValue("U_Analys5", .Offset).Trim())
                End With

                iwResult = oAuditObj.Commit()
                If iwResult <> 0 Then
                    goParent.goDICompany.GetLastError(iwResult, swResult)
                    'com.idh.bridge.DataHandler.INSTANCE.doError("System: Error Adding - " + swResult, "Error updating the Header.")
                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Adding - " + swResult + ",Error updating the Header.", "ERSYUHDR", {swResult})
                    Return False
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error updating the Header.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXUHDR", {Nothing})
                Return False
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj)
            End Try
            Return True
        End Function

        Private Function doUpdateLinkedWO(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim sStatus As String
            Dim sContract As String
            sStatus = getDFValue(oForm, "@IDH_WASTCONT", "U_Stat")
            sContract = getDFValue(oForm, "@IDH_WASTCONT", "Code")
            If sStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusApproved()) Then
                sStatus = "1"
            End If

            Dim sQry1 As String
            Dim sAnal As String = ""
            Dim lContract As Long = getWFValue(oForm, "WrkContractNumber")
            Dim bDoUpdateCustRef As Boolean = False
            Dim sCustUpdateStr As String = ""
            Dim sCustRef As String = ""

            Dim sAnal1 As String = getFormDFValue(oForm, "U_Analys1")
            Dim sAnal2 As String = getFormDFValue(oForm, "U_Analys2")
            Dim sAnal3 As String = getFormDFValue(oForm, "U_Analys3")
            Dim sAnal4 As String = getFormDFValue(oForm, "U_Analys4")
            Dim sAnal5 As String = getFormDFValue(oForm, "U_Analys5")

            If sAnal1.Length > 0 Then
                sAnal = sAnal & ", U_Analys1 = '" & sAnal1 & "'"
            End If

            If sAnal2.Length > 0 Then
                sAnal = sAnal & ", U_Analys2 = '" & sAnal2 & "'"
            End If

            If sAnal1.Length > 0 Then
                sAnal = sAnal & ", U_Analys3 = '" & sAnal3 & "'"
            End If

            If sAnal1.Length > 0 Then
                sAnal = sAnal & ", U_Analys4 = '" & sAnal4 & "'"
            End If

            If sAnal1.Length > 0 Then
                sAnal = sAnal & ", U_Analys5 = '" & sAnal5 & "'"
            End If

            '** Update the Linked WO Start
            If lContract < 0 Then
                sQry1 = "UPDATE [@IDH_JOBENTR] SET U_Status = '" & sStatus & "', U_CntrNo = '" & sContract & "' " & sAnal & " WHERE U_CntrNo = '" & lContract & "'"
            Else
                bDoUpdateCustRef = Config.INSTANCE.getParameterAsBool("WCUWOCR", False)
                If bDoUpdateCustRef Then
                    sCustRef = getFormDFValue(oForm, "U_CusRef")
                    If sCustRef.Length > 0 Then
                        sCustUpdateStr = ", U_CustRef = '" & sCustRef & "' "
                    Else
                        bDoUpdateCustRef = False
                    End If
                End If

                sQry1 = "UPDATE [@IDH_JOBENTR] SET U_Status = '" & sStatus & "' " & sCustUpdateStr & sAnal & " WHERE U_CntrNo = '" & sContract & "'"
            End If

            If goParent.goDB.doUpdateQuery(sQry1) = False Then
                Return False
            End If

            setWFValue(oForm, "WrkContractNumber", sContract)
            '** Update the Linked WO End

            If bDoUpdateCustRef Then
                sQry1 = "UPDATE [@IDH_JOBSHD] SET U_CustRef = '" & sCustRef & "'" & sAnal & " FROM [@IDH_JOBENTR] j, [@IDH_JOBSHD] r WHERE r.U_JobNr = j.Code And j.U_CntrNo = '" & sContract & "'"
                If goParent.goDB.doUpdateQuery(sQry1) = False Then
                    Return False
                End If
            End If

            Return True
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Dim bDoNew As Boolean = False
            Dim sU_CONTRN As String
            If pVal.BeforeAction = True Then
                With oForm.DataSources.DBDataSources.Item("@IDH_WASTCONT")
                    sU_CONTRN = .GetValue("Code", .Offset).Trim()
                End With
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                   oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    oForm.Freeze(True)
                    Try
                        If doValidateHeader(oForm) = False Then
                            BubbleEvent = False
                            Exit Sub
                        End If

                        Dim bDoContinue As Boolean = False
                        goParent.goDICompany.StartTransaction()

                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            If doUpdateHeader(oForm, True) = True Then
                                bDoContinue = True
                                bDoNew = True
                            End If
                        Else
                            If doUpdateHeader(oForm, False) = True Then
                                bDoContinue = True
                            End If
                        End If

                        If doUpdateLinkedWO(oForm) = True Then
                            bDoContinue = True
                        End If

                        If bDoContinue Then
                            If goParent.goDICompany.InTransaction Then
                                goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                            End If
                            com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
                            doLoadData(oForm)

                            doRemoveFormRows(oForm.UniqueID)
                            If bDoNew Then
                                If goParent.doCheckModal(oForm.UniqueID) = False Then
                                    doCreateNewEntry(oForm)
                                Else
                                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                                    BubbleEvent = False
                                End If
                            End If
                        Else
                            If goParent.goDICompany.InTransaction Then
                                goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                            End If
                            com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
                            BubbleEvent = False
                            Exit Sub
                        End If

                        oForm.Update()
                    Catch ex As Exception
                        If goParent.goDICompany.InTransaction Then
                            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                        End If
                        com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
                    Finally
                        oForm.Freeze(False)
                    End Try
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    'Set the Modal data and close
                    If goParent.doCheckModal(oForm.UniqueID) = True Then
                        doSetModalData(oForm)
                        BubbleEvent = False
                    End If
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    BubbleEvent = False
                    doFind(oForm)
                End If
            Else
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    oForm.Items.Item("IDH_CONTRN").Enabled = False
                End If
            End If
        End Sub

        'Find the record containing the Customer and/or Order number
        Protected Function doFind(ByVal oForm As SAPbouiCOM.Form) As Boolean
            'oForm.Freeze(True)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing

            'Dim sMode As String
            Try
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    Dim sContract As String = "" 'Contract Number
                    Dim sCustomerCd As String = "" 'Customer Code
                    Dim sCustomerNm As String = "" 'Customer Name
                    Dim sAddress As String = "" 'Address
                    Dim sCity As String = "" 'City
                    Dim sStreet As String = "" 'Street
                    Dim sZipCode As String = "" 'Zip Code
                    Dim sStatus As String = ""

                    sContract = getFormDFValue(oForm, "Code", True) '"IDH_CONTRN")
                    sCustomerCd = getFormDFValue(oForm, "U_Cust", True) '"IDH_CUST")
                    sCustomerNm = getFormDFValue(oForm, "U_CustNm", True) '"IDH_CUSTNM")

                    sAddress = getFormDFValue(oForm, "U_Addres", True) '"IDH_ADDRES")
                    sCity = getFormDFValue(oForm, "U_City", True) '"IDH_CITY")
                    sStreet = getFormDFValue(oForm, "U_Street", True) '"IDH_STREET")

                    sZipCode = getFormDFValue(oForm, "U_Zip", True) '"IDH_ZIP")
                    sStatus = getFormDFValue(oForm, "U_Stat", True) '"IDH_STAT")

                    Dim sQry As String

                    oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    sQry = "SELECT Top 1 Code from [@IDH_WASTCONT] Where Code = Code "
                    If sContract.Length() > 0 Then
                        sQry = sQry & " AND Code = '" & sContract & "'"
                    End If

                    If sCustomerCd.Length() > 0 Then
                        sCustomerCd = sCustomerCd.Replace("*", "%")
                        sQry = sQry & " AND U_Cust LIKE '" & sCustomerCd & "%'"
                    End If

                    If sCustomerNm.Length() > 0 Then
                        sCustomerNm = sCustomerNm.Replace("*", "%")
                        sQry = sQry & " AND U_CustNm LIKE '" & IDHAddOns.idh.data.Base.doFixForSQL(sCustomerNm) & "%'"
                    End If

                    If sAddress.Length > 0 Then
                        sAddress = sAddress.Replace("*", "%")
                        sQry = sQry & " AND U_Addres LIKE '" & IDHAddOns.idh.data.Base.doFixForSQL(sAddress) & "%'"
                    End If

                    If sCity.Length > 0 Then
                        sCity = sCity.Replace("*", "%")
                        sQry = sQry & " AND U_City LIKE '" & IDHAddOns.idh.data.Base.doFixForSQL(sCity) & "%'"
                    End If

                    If sStreet.Length > 0 Then
                        sStreet = sStreet.Replace("*", "%")
                        sQry = sQry & " AND U_Street LIKE '" & IDHAddOns.idh.data.Base.doFixForSQL(sStreet) & "%'"
                    End If

                    If sZipCode.Length > 0 Then
                        sCity = sZipCode.Replace("*", "%")
                        sQry = sQry & " AND U_Zip LIKE '" & sZipCode & "%'"
                    End If

                    If sStatus.Length > 0 Then
                        sStatus = sStatus.Replace("*", "%")
                        sQry = sQry & " AND U_Stat LIKE '" & sStatus & "%'"
                    End If

                    sQry = sQry & " Order by CAST(Name As Numeric) DESC"
                    oRecordSet.DoQuery(sQry)
                    If oRecordSet.RecordCount > 0 Then
                        doSwitchToAdd(oForm)

                        Dim db As SAPbouiCOM.DBDataSource
                        db = oForm.DataSources.DBDataSources.Item("@IDH_WASTCONT")

                        sContract = oRecordSet.Fields.Item("Code").Value.Trim()
                        db.Clear()

                        Dim oConditions As SAPbouiCOM.Conditions = New SAPbouiCOM.Conditions
                        Dim oCondition As SAPbouiCOM.Condition = oConditions.Add
                        oCondition.Alias = "Code"
                        oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                        oCondition.CondVal = sContract

                        db.Query(oConditions)

                        oForm.Update()

                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        doLoadData(oForm)
                        setWFValue(oForm, "WrkContractNumber", getFormDFValue(oForm, "Code"))
                    Else
                        'com.idh.bridge.DataHandler.INSTANCE.doError("User: The Waste Order Contract was not found", "Error finding the Waste Contract - " & sContract & "." & sCustomerCd & "." & sCustomerNm)
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error finding the Waste Contract - " & sContract & "." & sCustomerCd & "." & sCustomerNm, "ERUSFWC", {sContract, sCustomerCd, sCustomerNm})
                        setWFValue(oForm, "WrkContractNumber", 0)
                    End If
                End If
                Return False
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error finding the Waste Order.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFWO", {Nothing})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
                '    oForm.Freeze(False)
            End Try
            Return True
        End Function

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim sItemId As String = pVal.ItemUID
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                Dim bIsInnerEvent As Boolean = pVal.InnerEvent
                If pVal.BeforeAction = False Then
                    If pVal.ItemChanged Then
                        'If wasSetWithCode(oForm, sItemID) = False Then
                        If bIsInnerEvent = False Then
                            If sItemId = "IDH_CUST" Then
                                setItemValue(oForm, "IDH_CUSTNM", "")
                                doChooseCustomer(oForm, True)
                            ElseIf sItemId = "IDH_CUSTNM" Then
                                Dim sName As String = getItemValue(oForm, "IDH_CUSTNM")
                                If sName.StartsWith("*") OrElse sName.EndsWith("*") Then
                                    setItemValue(oForm, "IDH_CUST", "")
                                End If
                                doChooseCustomer(oForm, True)
                            End If
                        End If
                        'End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_TABGEN" Then
                        oForm.PaneLevel = 1
                    ElseIf pVal.ItemUID = "IDH_TABDET" Then
                        oForm.PaneLevel = 2
                    ElseIf pVal.ItemUID = "IDH_TABREM" Then
                        oForm.PaneLevel = 3
                    ElseIf pVal.ItemUID = "IDH_TABATT" Then
                        oForm.PaneLevel = 4
                    ElseIf pVal.ItemUID = "IDH_CUSL" Then
                        doChooseCustomer(oForm, False)
                    ElseIf pVal.ItemUID = "IDH_BADD" Then
                        doChooseBillAdd(oForm)
                    End If
                End If
                '#MA Start 04-04-2017
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_RESIZE Then
                If oForm.Height >= 483 Then
                    oForm.Items.Item("64").Height = oForm.Height - oForm.Items.Item("64").Top - 70
                Else
                    oForm.Items.Item("64").Height = 239
                End If

                If oForm.Width >= 816 Then
                    oForm.Items.Item("64").Width = oForm.Width - 27
                Else
                    oForm.Items.Item("64").Width = 781
                End If
                '#MA End 04-04-2017
            End If
            Return False
        End Function

        Protected Overridable Sub doChooseCustomer(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean)
            'setSharedData(oForm, "IDH_TYPE", "F-C")
            setSharedData(oForm, "IDH_TYPE", "CL")
            setSharedData(oForm, "IDH_BPCOD", getFormDFValue(oForm, "U_Cust", True))
            setSharedData(oForm, "IDH_NAME", getFormDFValue(oForm, "U_CustNm", True))

            setSharedData(oForm, "TRG", "CU")
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If

            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub

        '** Send By Custom Events
        'Return True done
        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_ADD Then
                If pVal.BeforeAction = True Then
                    Dim lContract As Long = getWFValue(oForm, "WrkContractNumber")
                    Dim sCode As String
                    If lContract < 0 Then
                        sCode = lContract.ToString()
                    Else
                        sCode = getFormDFValue(oForm, "Code")
                    End If
                    setSharedData(oForm, "IDH_CONTRN", sCode)
                    setSharedData(oForm, "WOCode", "")
                    setSharedData(oForm, "IDH_CUST", getFormDFValue(oForm, "U_Cust"))
                    setSharedData(oForm, "IDH_CUSTNM", getFormDFValue(oForm, "U_CustNm"))
                    setSharedData(oForm, "IDH_CUSCRF", getFormDFValue(oForm, "U_CusRef"))
                    setSharedData(oForm, "USESHARE", "")
                    Dim sStatus As String
                    sStatus = getFormDFValue(oForm, "U_Stat")
                    If sStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusApproved()) Then
                        'CREATED Status = 1
                        setSharedData(oForm, "IDH_STATUS", "1")
                    Else
                        setSharedData(oForm, "IDH_STATUS", sStatus)
                    End If

                    goParent.doOpenModalForm("IDH_WASTORD", oForm)
                    Return False
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DEL Then
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DEL_MULTI Then
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK Then 'GRID_ROW_VIEW_EDIT Then
                If pVal.ItemUID = "LINESGRID" Then
                    If pVal.BeforeAction = False Then
                        Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                        'Already set
                        'oGridN.setCurrentLine(pVal.Row)

                        Dim sJobEntr As String = oGridN.doGetFieldValue("Code")
                        setSharedData(oForm, "WOCode", sJobEntr)
                        setSharedData(oForm, "USESHARE", "")
                        goParent.doOpenModalForm("IDH_WASTORD", oForm)
                    End If
                End If
            End If
            Return True
        End Function

        '** Create a new Entry if the Add option is selected
        Private Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form)
            'Dim sDate As String = goParent.doDateToStr()
            'Dim sTime As String = goParent.doTimeToNumStr()

            With oForm.DataSources.DBDataSources.Item("@IDH_WASTCONT")
                .InsertRecord(.Offset)
            End With

            'Dim iCode As Integer = goParent.goDB.doNextNumber("CONNO")

            Dim sDateTime As String = Date.Today.ToString("yyMMddHHmmss")
            'Dim lTicks As Long = Date.Today.ToString("yyMMddHHmmss")Ticks * -1
            Dim lTempNumber As Long = goParent.goDB.getTemporayCode()

            setWFValue(oForm, "WrkContractNumber", lTempNumber)
            'Dim sCode As String = Conversion.Str(iCode)
            Dim sCode As String = ""

            Dim sDefaultStatus As String = Config.Parameter("WCDEFST")

            doClearFields(oForm)

            setFormDFValue(oForm, "Code", sCode, True)
            setFormDFValue(oForm, "Name", sCode, True)
            setFormDFValue(oForm, "U_Stat", sDefaultStatus, True)
            setFormDFValue(oForm, "U_StarDat", Date.Now(), True)
            setFormDFValue(oForm, "U_PayTrm", "-1", True)

            '                .SetValue("Code", .Offset, sCode)
            '                .SetValue("Name", .Offset, sCode)
            '                .SetValue("U_Cust", .Offset, "")
            '                .SetValue("U_CustNm", .Offset, "")
            '                .SetValue("U_ContNm", .Offset, "")
            '                .SetValue("U_CusRef", .Offset, "")
            '                .SetValue("U_SalEmp", .Offset, "")
            '                .SetValue("U_Owner", .Offset, "")
            '                .SetValue("U_SpecIn", .Offset, "")
            '                .SetValue("U_Addres", .Offset, "")
            '                .SetValue("U_Street", .Offset, "")
            '                .SetValue("U_Block", .Offset, "")
            '                .SetValue("U_City", .Offset, "")
            '                .SetValue("U_Zip", .Offset, "")
            '                .SetValue("U_Phone", .Offset, "")
            '                .SetValue("U_Stat", .Offset, sDefaultStatus)
            '                .SetValue("U_StarDat", .Offset, Date.Now())
            '                .SetValue("U_EndDat", .Offset, "")
            '                .SetValue("U_TermDt", .Offset, "")
            '                .SetValue("U_Exp", .Offset, "")
            '                .SetValue("U_DurMnt", .Offset, "")
            '                .SetValue("U_ChkMnt", .Offset, "")
            '                .SetValue("U_Ren", .Offset, "")
            '                .SetValue("U_RemDay", .Offset, "")
            '                .SetValue("U_ChkDay", .Offset, "")
            '                .SetValue("U_ConTyp", .Offset, "")
            '                .SetValue("U_SerLev", .Offset, "")
            '                .SetValue("U_Except", .Offset, "")
            '                .SetValue("U_ExcDet", .Offset, "")
            '                .SetValue("U_PayTrm", .Offset, "-1")
            '                .SetValue("U_DepReq", .Offset, 0)
            '                .SetValue("U_Reven", .Offset, 0)
            '                .SetValue("U_Costs", .Offset, 0)
            '                .SetValue("U_GP", .Offset, 0)
            '                .SetValue("U_GPPrc", .Offset, 0)
            '                .SetValue("U_Remark", .Offset, "")

            doRefreshForm(oForm)
            oForm.Items.Item("IDH_TABGEN").Click()
            doLoadData(oForm)

        End Sub

        '** Clears all the fields when going into the find mode.
        Private Sub doClearAll(ByVal oForm As SAPbouiCOM.Form)
            Dim bIsFind As Boolean = False
            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                bIsFind = True
            End If

            doClearFields(oForm)

            doRefreshForm(oForm)
            oForm.Items.Item("IDH_TABGEN").Click()
            doLoadData(oForm)
        End Sub

        Private Sub doClearFields(ByVal oForm As SAPbouiCOM.Form)
            setFormDFValue(oForm, "Code", "", True)
            setFormDFValue(oForm, "Name", "")
            setFormDFValue(oForm, "U_Cust", "", True)
            setFormDFValue(oForm, "U_CustNm", "", True)
            setFormDFValue(oForm, "U_ContNm", "", True)
            setFormDFValue(oForm, "U_CusRef", "")
            setFormDFValue(oForm, "U_SalEmp", "")
            setFormDFValue(oForm, "U_Owner", "")
            setFormDFValue(oForm, "U_SpecIn", "")
            setFormDFValue(oForm, "U_Addres", "", True)
            setFormDFValue(oForm, "U_Street", "")
            setFormDFValue(oForm, "U_Block", "")
            setFormDFValue(oForm, "U_City", "", True)
            setFormDFValue(oForm, "U_Zip", "", True)
            setFormDFValue(oForm, "U_Phone", "")
            setFormDFValue(oForm, "U_Stat", "", True)
            ''##MA Start Issue#439
            setFormDFValue(oForm, "U_TRNCd", "", True)
            ''##MA End Issue#439
            setFormDFValue(oForm, "U_StarDat", "")
            setFormDFValue(oForm, "U_EndDat", "")
            setFormDFValue(oForm, "U_TermDt", "")
            setFormDFValue(oForm, "U_Exp", "")
            setFormDFValue(oForm, "U_DurMnt", "")
            setFormDFValue(oForm, "U_ChkMnt", "")
            setFormDFValue(oForm, "U_Ren", "")
            setFormDFValue(oForm, "U_RemDay", "")
            setFormDFValue(oForm, "U_ChkDay", "")
            setFormDFValue(oForm, "U_ConTyp", "")
            setFormDFValue(oForm, "U_SerLev", "")
            setFormDFValue(oForm, "U_Except", "")
            setFormDFValue(oForm, "U_ExcDet", "")
            setFormDFValue(oForm, "U_PayTrm", "")
            setFormDFValue(oForm, "U_DepReq", 0)
            setFormDFValue(oForm, "U_Reven", 0)
            setFormDFValue(oForm, "U_Costs", 0)
            setFormDFValue(oForm, "U_GP", 0)
            setFormDFValue(oForm, "U_GPPrc", 0)
            setFormDFValue(oForm, "U_Remark", "")

            setFormDFValue(oForm, "U_Analys1", "")
            setFormDFValue(oForm, "U_Analys2", "")
            setFormDFValue(oForm, "U_Analys3", "")
            setFormDFValue(oForm, "U_Analys4", "")
            setFormDFValue(oForm, "U_Analys5", "")
        End Sub

        '** Print the Form ***
        Public Sub doPrintForm(ByVal oForm As SAPbouiCOM.Form)
            Dim sCode As String = getDFValue(oForm, "@IDH_WASTCONT", "Code")
            Dim sReportFile As String = Config.Parameter("WCREP")
            Dim oParams As New Hashtable
            oParams.Add("CONumber", sCode)
            If Config.INSTANCE.getParameterAsBool("UNEWRPTV", False) Then
                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
            Else
                IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType) ', Config.INSTANCE.doGetBranch(goParent))
            End If
        End Sub

        '** The Menu Event handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                If pVal.MenuUID = Config.NAV_ADD Then
                    oForm.Freeze(True)
                    Try
                        doCreateNewEntry(oForm)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString, "Error processing the Menu Event - " & oForm.UniqueID & "." & pVal.MenuUID)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try

                ElseIf pVal.MenuUID = Config.NAV_FIND OrElse
                       pVal.MenuUID = Config.NAV_FIRST OrElse
                       pVal.MenuUID = Config.NAV_LAST OrElse
                       pVal.MenuUID = Config.NAV_NEXT OrElse
                       pVal.MenuUID = Config.NAV_PREV Then
                    oForm.Freeze(True)
                    Try
                        If pVal.MenuUID = Config.NAV_FIND Then
                            doClearAll(oForm)
                        Else
                            setWFValue(oForm, "WrkContractNumber", 0)
                            doLoadData(oForm)
                        End If
                    Catch ex As Exception
                    Finally
                        oForm.Freeze(False)
                    End Try
                End If
            Else
                If pVal.MenuUID = Config.NAV_ADD OrElse
                        pVal.MenuUID = Config.NAV_FIND OrElse
                        pVal.MenuUID = Config.NAV_FIRST OrElse
                        pVal.MenuUID = Config.NAV_LAST OrElse
                        pVal.MenuUID = Config.NAV_NEXT OrElse
                        pVal.MenuUID = Config.NAV_PREV Then
                    If doCancelLinkedDocs(oForm) = False Then
                        BubbleEvent = False
                    End If
                ElseIf pVal.MenuUID = Config.NAV_PRINT Then
                    doPrintForm(oForm)
                    BubbleEvent = False
                    Return True
                End If
            End If
            Return True
        End Function

        Public Overrides Sub doButtonID2(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If doCancelLinkedDocs(oForm) = False Then
                BubbleEvent = False
            End If
        End Sub

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            If doCancelLinkedDocs(oForm) = True Then
                MyBase.doCloseForm(oForm, BubbleEvent)
            Else
                BubbleEvent = False
            End If
        End Sub

        Public Overrides Sub doClose()
        End Sub

        Public Function doCancelLinkedDocs(ByVal oForm As SAPbouiCOM.Form) As Boolean
            If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                Return True
            End If

            Dim iContract As Long = getWFValue(oForm, "WrkContractNumber")
            If iContract < 0 Then
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                If oGridN.getRowCount > 1 Then
                    If Messages.INSTANCE.doResourceMessageYNAsk("WCNADLO", Nothing, 1) = 1 Then
                        idh.forms.orders.Waste.doDeleteLinkedOrders(goParent, "@IDH_JOBENTR", "@IDH_JOBSHD", iContract.ToString())
                        setWFValue(oForm, "WrkContractNumber", 0)
                        Return True
                    Else
                        Return False
                    End If
                Else
                    Return True
                End If
            Else
                Return True
            End If
        End Function

        Protected Sub doChooseBillAdd(ByVal oForm As SAPbouiCOM.Form)
            Dim sCardCode As String = getFormDFValue(oForm, "U_Cust")
            If sCardCode.Length > 0 Then
                setSharedData(oForm, "TRG", "SA")
                setSharedData(oForm, "IDH_CUSCOD", sCardCode)
                setSharedData(oForm, "IDH_ADRTYP", "B")
                goParent.doOpenModalForm("IDHASRCH", oForm)
            End If
        End Sub

        Protected Overridable Function doSetChoosenCustomer(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoBPRules As Boolean = True) As Boolean
            Dim val As String
            Dim val2 As String
            Dim val3 As String
            Dim val4 As String
            Dim dCreditLimit As Decimal = 0
            Dim dCreditRemain As Decimal = 0
            Dim dDeviationPrc As Decimal = 0
            Dim dTBalance As Decimal = 0
            'Dim sFrozen As String = "N"
            'Dim sFrozenComm As String = Nothing
            Dim sIgnoreCheck As String = "N"
            Dim sObligated As String
            Dim sSalesPerson As String
            'Dim dFrozenFrom As Date
            'Dim dFrozenTo As Date
            Dim sPayTerm As String

            Try
                val = getSharedData(oForm, "CARDCODE")
                val2 = getSharedData(oForm, "PHONE1")
                val3 = getSharedData(oForm, "CNTCTPRSN")
                val4 = getSharedData(oForm, "CARDNAME")

                dCreditLimit = getSharedData(oForm, "CREDITLINE")
                dCreditRemain = getSharedData(oForm, "CRREMAIN")
                dDeviationPrc = getSharedData(oForm, "DEVIATION")
                dTBalance = getSharedData(oForm, "TBALANCE")
                'sFrozen = getSharedData(oForm, "FROZEN")
                'sFrozenComm = getSharedData(oForm, "FROZENC")
                sIgnoreCheck = getSharedData(oForm, "IDHICL")
                sObligated = getSharedData(oForm, "IDHOBLGT")
                'dFrozenFrom = getSharedData(oForm, "FROZENF")
                'dFrozenTo = getSharedData(oForm, "FROZENT")
                sSalesPerson = getSharedData(oForm, "SALESP")
                sPayTerm = getSharedData(oForm, "GROUPNUM")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the Chosen Customer.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCCUS", {Nothing})
                Return False
            End Try

            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                setFormDFValueFromUser(oForm, "U_Cust", val, True)
                setFormDFValueFromUser(oForm, "U_CustNm", val4, True)
                Return True
            End If

            If Not (val Is Nothing) AndAlso val.Length > 0 Then
                If Not sIgnoreCheck = "Y" Then
                    Dim bBlockOnFail As Boolean = Config.INSTANCE.getParameterAsBool("WOBCCL", True)
                    'If Config.INSTANCE.doCheckCredit2(goParent, val, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc, 0, sFrozen, sFrozenComm, False, bBlockOnFail, Nothing, dFrozenFrom, dFrozenTo) = False Then
                    ' Return False
                    'End If
                    If Config.INSTANCE.doCheckCredit2(val, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc, 0, False, bBlockOnFail, Nothing) = False Then
                        Return False
                    End If
                End If

                setFormDFValueFromUser(oForm, "U_Cust", val)
                setFormDFValueFromUser(oForm, "U_Phone", val2)
                setFormDFValueFromUser(oForm, "U_ContNm", val3)
                setFormDFValueFromUser(oForm, "U_CustNm", val4)
                setFormDFValueFromUser(oForm, "U_SalEmp", sSalesPerson)
                setFormDFValueFromUser(oForm, "U_PayTrm", sPayTerm)

                If val.Length > 0 Then
                    Dim oData As ArrayList
                    oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, val, "B")
                    If Not oData Is Nothing Then
                        setFormDFValueFromUser(oForm, "U_Addres", oData.Item(0))
                        setFormDFValueFromUser(oForm, "U_Street", oData.Item(1))
                        setFormDFValueFromUser(oForm, "U_Block", oData.Item(2))
                        setFormDFValueFromUser(oForm, "U_Zip", oData.Item(3))
                        setFormDFValueFromUser(oForm, "U_City", oData.Item(4))
                    Else
                        setFormDFValueFromUser(oForm, "U_Addres", "")
                        setFormDFValueFromUser(oForm, "U_Street", "")
                        setFormDFValueFromUser(oForm, "U_Block", "")
                        setFormDFValueFromUser(oForm, "U_Zip", "")
                        setFormDFValueFromUser(oForm, "U_City", "")
                    End If
                End If

                If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    doSetUpdate(oForm)
                End If
                Return True
            End If
            Return False
        End Function


        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType = "IDHCSRCH" Then
                    Dim sTarget As String = getSharedData(oForm, "TRG")
                    Dim val As String = getSharedData(oForm, "CARDCODE")

                    If Not (val Is Nothing) AndAlso val.Length > 0 Then
                        If sTarget = "CU" Then
                            If doSetChoosenCustomer(oForm) = False Then
                                setFormDFValue(oForm, "U_CardCd", "", True)
                                setFormDFValue(oForm, "U_CustNm", "", True)
                            End If
                        End If
                    End If
                ElseIf sModalFormType = "IDHASRCH" Then
                    setFormDFValue(oForm, "U_Addres", getSharedData(oForm, "ADDRESS"))
                    setFormDFValue(oForm, "U_Street", getSharedData(oForm, "STREET"))
                    setFormDFValue(oForm, "U_Block", getSharedData(oForm, "BLOCK"))
                    setFormDFValue(oForm, "U_City", getSharedData(oForm, "CITY"))
                    setFormDFValue(oForm, "U_Zip", getSharedData(oForm, "ZIPCODE"))
                    setFormDFValue(oForm, "U_Phone", getSharedData(oForm, "TEL1"))
                    doSetUpdate(oForm)
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal Result - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub

        '** This method is called by the returning modal form to set the data before closing
        Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                'If sModalFormType = "IDH_WASTORD" Then
                '    Dim lContract As Long = getWFValue(oForm, "WrkContractNumber")
                '    If lContract < 0 Then
                '        Dim sContract As String = getFormDFValue(oForm, "Code")
                '        lContract = Long.Parse(sContract)
                '        setWFValue(oForm, "WrkContractNumber", lContract)
                '    End If
                doLoadData(oForm)
                'End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal Result.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {String.Empty})
            End Try
        End Sub

        ''MODAL DIALOG
        ''*** Set the return data when called as a modal dialog 
        Private Sub doSetModalData(ByVal oForm As SAPbouiCOM.Form)
            doReturnFromModalShared(oForm, True)
        End Sub

    End Class
End Namespace
