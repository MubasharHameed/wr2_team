Imports System.Collections
Imports com.idh.bridge
Imports com.idh.bridge.lookups
Imports System

Imports com.idh.utils.Conversions
Imports System.Threading
Imports com.idh.bridge.resources

Namespace idh.const
    Public Class Lookup_
        Inherits com.idh.bridge.lookups.Config

        Public Shared Shadows INSTANCE As idh.const.Lookup_

        Public moConfig As Config
        'Shared mhBPTypes As Hashtable = Nothing
        'Shared mhBPNames As Hashtable = Nothing
        'Shared mhUSEBPWGT As Hashtable = New Hashtable
        'Shared gsPotalCong As String = "SE1"
        'Shared gsWCDescription As String = Nothing

        Public Sub New(ByVal sConfigTable As String)
            MyBase.New("[@" & sConfigTable & "]")
            INSTANCE = Me

            'moConfig = New Config(IDHAddOns.idh.addon.Base.PARENT.goDICompany, "[@" & sConfigTable & "]")
            'doLocalReadConfig()
        End Sub

        Protected Overloads Sub doReadConfig()
            MyBase.doReadConfig()
            'doLocalReadConfig()

            ''Now clear the remote connections
            'DirectCast(IDHAddOns.idh.addon.Base.PARENT, idh.main.MainForm).doReleaseRemoteCompanies(Nothing, True)
        End Sub

        'Private Sub doLocalReadConfig()
        'Try
        '    Dim sDoAuditTrail As String
        '    'sDoAuditTrail = mhParams.Item("AUDITT")
        '    sDoAuditTrail = getParameterInternal("AUDITT", True)
        '    If Not sDoAuditTrail Is Nothing Then
        '        If sDoAuditTrail.ToUpper().Equals("TRUE") Then
        '            com.idh.bridge.lookups.Base.DOAUDITTRAIL = True
        '        Else
        '            com.idh.bridge.lookups.Base.DOAUDITTRAIL = False
        '        End If
        '    End If
        'Catch ex As Exception
        'End Try

        ''If mhParams.ContainsKey("GEMAXROW") Then
        ''Dim sMaxRows As String = mhParams.Item("GEMAXROW")
        'Dim sMaxRows As String = getParameterInternal("GEMAXROW", True)
        'If Not sMaxRows Is Nothing AndAlso sMaxRows.Length > 0 Then
        '    Try
        '        Dim iMaxRows As Integer = Convert.ToInt16(sMaxRows)
        '        DBOGridControl.DEFAULTMAXROWS = iMaxRows
        '    Catch ex As Exception
        '    End Try
        'End If
        ''End If
        'End Sub

        'Public Overloads Sub doResetAll()
        '    MyBase.doResetAll()
        '    doLocalResetAll()

        '    moConfig.doResetAll()
        'End Sub

        'Private Sub doLocalResetAll()
        'If Not mhBPTypes Is Nothing Then
        '    mhBPTypes.Clear()
        'End If
        ''If Not mhWeightFactors Is Nothing Then
        ''mhWeightFactors.Clear()
        ''End If

        'If Not mhBranch Is Nothing Then
        '    mhBranch.Clear()
        'End If

        'If Not mhBPNames Is Nothing Then
        '    mhBPNames.Clear()
        'End If

        ''If Not mhCustRef Is Nothing Then
        ''mhCustRef.Clear()
        ''End If

        ''If Not mhUOM Is Nothing Then
        ''mhUOM.Clear()
        ''End If
        'If Not mhUSEBPWGT Is Nothing Then
        '    mhUSEBPWGT.Clear()
        'End If

        'Try
        '    Dim sDoAuditTrail As String
        '    sDoAuditTrail = getParameterInternal("AUDITT", True)
        '    If Not sDoAuditTrail Is Nothing Then
        '        If sDoAuditTrail.ToUpper().Equals("TRUE") Then
        '            com.idh.bridge.lookups.Base.DOAUDITTRAIL = True
        '        Else
        '            com.idh.bridge.lookups.Base.DOAUDITTRAIL = False
        '        End If
        '    End If
        'Catch ex As Exception
        'End Try

        'Dim sMaxRows As String = getParameterInternal("GEMAXROW", True)
        'If Not sMaxRows Is Nothing AndAlso sMaxRows.Length > 0 Then
        '    Try
        '        Dim iMaxRows As Integer = Convert.ToInt16(sMaxRows)
        '        DBOGridControl.DEFAULTMAXROWS = iMaxRows
        '    Catch ex As Exception
        '    End Try
        'End If

        'Dim sMOPAFC As String = getParameterInternal("MOPAFC", True)
        'If Not sMOPAFC Is Nothing AndAlso sMOPAFC.Length > 0 Then
        '    IDHAddOns.idh.addon.Base.PARENT.gsModalParentSwitch = sMOPAFC
        'Else
        '    IDHAddOns.idh.addon.Base.PARENT.gsModalParentSwitch = "NO"
        'End If

        '''Now colear the remote connections
        'DirectCast(IDHAddOns.idh.addon.Base.PARENT, idh.main.MainForm).doReleaseRemoteCompanies(Nothing, True)
        'End Sub

        'Public Overloads Function doGetDOCReportLevel1a(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sType As String, ByVal sCardCode As String, ByVal sWasteCode As String, Optional ByVal bFromManager As Boolean = False) As ArrayList

        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '    Dim sReportFile As String = ""
        '    Dim oArrayList As ArrayList = New ArrayList()
        '    oArrayList.Add("")
        '    oArrayList.Add(False)
        '    Try
        '        Dim sQry As String
        '        Dim sReportDoc As String = ""

        '        sQry = "Select U_WTDOCUM from OITM where ItemCode = '" & sWasteCode & "'"
        '        oRecordSet = oParent.goDB.doSelectQuery(sQry)
        '        If Not oRecordSet Is Nothing AndAlso oRecordSet.RecordCount > 0 Then
        '            sReportDoc = oRecordSet.Fields.Item("U_WTDOCUM").Value
        '            If Not sReportDoc Is Nothing AndAlso sReportDoc.Length > 0 Then
        '                If sReportDoc.Equals("WDOCCON") Then
        '                    oArrayList.Item(1) = True
        '                End If
        '                sReportFile = com.idh.bridge.lookups.Config.Parameter(sReportDoc)
        '            End If
        '        End If
        '        If sReportFile Is Nothing OrElse sReportFile.Length = 0 Then
        '            sReportFile = Config.INSTANCE.doGetDOCReportLevel2(sType, sCardCode, sWasteCode)
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    Finally
        '        IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
        '    End Try
        '    oArrayList.Item(0) = sReportFile
        '    Return oArrayList 'sReportFile
        'End Function

        'Public Class LinkedBP
        '    Public CardCode As String
        '    Public CardName As String
        '    Public UOM As String
        '    Public Phone1 As String
        '    Public ContactPerson As String
        '    Public CardForeignName As String
        'End Class

        'Public Function doGetLinkedBP(ByVal oParent As IDHAddOns.idh.addon.Base, ByRef sCardCode As String) As LinkedBP
        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '    Dim oLinkedBP As LinkedBP = Nothing
        '    Try
        '        Dim sQry As String = "SELECT b1.U_IDHLBPC, b1.U_IDHLBPN, b2.U_IDHUOM, b1.Phone1, b1.CntctPrsn,b2.CardFName FROM OCRD b1, OCRD b2 WHERE b1.CardCode = '" & sCardCode & "' AND b2.CardCode = b1.U_IDHLBPC"
        '        oRecordSet = oParent.goDB.doSelectQuery(sQry)
        '        If Not oRecordSet Is Nothing AndAlso
        '         oRecordSet.RecordCount > 0 Then
        '            Dim sCode As String = oRecordSet.Fields.Item(0).Value

        '            If Not sCode Is Nothing AndAlso sCode.Length > 0 Then
        '                oLinkedBP = New LinkedBP()
        '                oLinkedBP.CardCode = sCode
        '                oLinkedBP.CardName = oRecordSet.Fields.Item(1).Value
        '                oLinkedBP.UOM = oRecordSet.Fields.Item(2).Value
        '                oLinkedBP.Phone1 = oRecordSet.Fields.Item(3).Value
        '                oLinkedBP.ContactPerson = oRecordSet.Fields.Item(4).Value
        '                oLinkedBP.CardForeignName = oRecordSet.Fields.Item(5).Value
        '            End If
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    Finally
        '        IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
        '    End Try
        '    Return oLinkedBP
        'End Function

        'Public Function getOrigins(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal CardCode As String) As ArrayList
        '    Dim retValue As New ArrayList
        '    Dim k As Integer
        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '    Try
        '        Dim sQry As String
        '        sQry = "SELECT Address,Street,ZipCode,City FROM [CRD1] WHERE CardCode='" + CardCode + "'" + " and AdresType='S' order by LineNum"
        '        oRecordSet = oParent.goDB.doSelectQuery(sQry)

        '        If oRecordSet.RecordCount = 0 Then
        '            IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
        '            Return Nothing
        '        End If

        '        While (oRecordSet.EoF = False)
        '            Dim singleValue(5) As String
        '            For k = 0 To 3
        '                singleValue.SetValue(oRecordSet.Fields.Item(k).Value, k + 1)
        '            Next
        '            retValue.Add(singleValue)
        '            oRecordSet.MoveNext()
        '        End While
        '        Return retValue
        '    Catch ex As Exception

        '    Finally
        '        IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
        '    End Try
        '    Return Nothing
        'End Function

        'Public Function doCheckBillableTipping(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sItemGrp As String, ByVal sJob As String) As Boolean
        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '    Try
        '        Dim sQry As String
        '        sQry = "select U_JobTp " &
        '                " from [@IDH_JOBTYPE] " &
        '                " where U_Billa = 'Y' " &
        '                "     AND U_ItemGrp = '" & sItemGrp & "'" &
        '                "     AND U_JobTp = '" & IDHAddOns.idh.data.Base.doFixForSQL(sJob) & "'"
        '        oRecordSet = oParent.goDB.doSelectQuery(sQry)
        '        If oRecordSet.RecordCount > 0 Then
        '            Return True
        '        End If
        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRBF", {Nothing})
        '    Finally
        '        oRecordSet = Nothing
        '    End Try
        '    Return False
        'End Function

        'Public Function doCheckBillableHaulage(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sItemGrp As String, ByVal sJob As String) As Boolean
        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '    Try
        '        Dim sQry As String
        '        sQry = "select U_JobTp " &
        '                " from [@IDH_JOBTYPE] " &
        '                " where U_HBilla = 'Y' " &
        '                "     AND U_ItemGrp = '" & sItemGrp & "'" &
        '                "     AND jt.U_JobTp = '" & IDHAddOns.idh.data.Base.doFixForSQL(sJob) & "'"

        '        oRecordSet = oParent.goDB.doSelectQuery(sQry)
        '        If oRecordSet.RecordCount > 0 Then
        '            Return True
        '        End If
        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCHBF", {Nothing})
        '    Finally
        '        oRecordSet = Nothing
        '    End Try
        '    Return False
        'End Function

        'Public Function doCheckedBilledBefore(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sLicNr As String, ByVal sJobNr As String, ByVal sItemGrp As String) As Boolean
        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '    Try
        '        Dim sQry As String
        '        sQry = "select Code from [@IDH_JOBSHD] " &
        '            " where U_SLicNr = '" & sLicNr & "'" &
        '         "   AND U_JobNr = '" & sJobNr & "'" &
        '         "   AND U_JobTp in ( " &
        '         "          Select U_JobTp " &
        '         "          from [@IDH_JOBTYPE] " &
        '         "          where U_Billa = 'Y' " &
        '         "             AND U_ItemGrp = '" & sItemGrp & "')"
        '        oRecordSet = oParent.goDB.doSelectQuery(sQry)
        '        If oRecordSet.RecordCount > 0 Then
        '            Return True
        '        End If
        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCBBF", {sJobNr})
        '    Finally
        '        oRecordSet = Nothing
        '    End Try
        '    Return False
        'End Function

        'Public Function doGetTimeWindow(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sCardCode As String) As String()
        '    Dim oResult(2) As String

        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '    Try
        '        Dim sQry As String
        '        sQry = "select U_IDH_RTF, U_IDH_RTT from OCRD " &
        '            " where CardCode = '" & sCardCode & "'"
        '        oRecordSet = oParent.goDB.doSelectQuery(sQry)
        '        If oRecordSet.RecordCount > 0 Then
        '            oResult(0) = oRecordSet.Fields.Item(0).Value
        '            oResult(1) = oRecordSet.Fields.Item(1).Value
        '            Return oResult
        '        End If
        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXBPTW", {sCardCode})
        '    Finally
        '        IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
        '    End Try
        '    Return Nothing
        'End Function

        'Public Function doGetContainerJobTypes(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sItemGrp As String, ByVal sCurrentJobType As String) As ArrayList
        '    Dim oData As New ArrayList
        '    If sItemGrp Is Nothing Then
        '        Return oData
        '    End If
        '    Dim sQry As String
        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '    Try
        '        sQry = "select " &
        '               "   U_JobTp, " &
        '               "   U_Seq " &
        '               "from [@IDH_JOBTYPE] jt " &
        '               "where " &
        '               "   U_ItemGrp = '" & sItemGrp & "'" &
        '               "   AND U_OrdCat = 'Waste Order' " &
        '               "   AND U_Seq > ( select U_Seq " &
        '               "       from [@IDH_JOBTYPE] " &
        '               "       where U_ItemGrp = '" & sItemGrp & "' " &
        '               "           AND U_OrdCat = 'Waste Order' " &
        '               "           AND U_JobTp = '" & sCurrentJobType & "' " &
        '               "       ) " &
        '               " ORDER BY U_Seq"
        '        oRecordSet = oParent.goDB.doSelectQuery(sQry)
        '        If Not oRecordSet Is Nothing Then
        '            Dim sJobType As String
        '            While oRecordSet.EoF = False
        '                sJobType = oRecordSet.Fields.Item(0).Value
        '                oData.Add(sJobType)
        '                oRecordSet.MoveNext()
        '            End While
        '            If sCurrentJobType.ToUpper().Equals("EXCHANGE") = True Then
        '                oData.Add(sCurrentJobType)
        '            End If
        '        End If
        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRCNJT", {sItemGrp})
        '    Finally
        '        IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
        '    End Try
        '    Return oData
        'End Function

        'Public Function doGetPriceListName(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal lPIndex As Long) As String
        '    Dim oPList As SAPbobsCOM.PriceLists
        '    Dim sListName As String

        '    If lPIndex = -1 Then
        '        sListName = "Last Purcahse Price"
        '    Else
        '        oPList = oParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPriceLists)
        '        If oPList.GetByKey(lPIndex) Then
        '            sListName = oPList.PriceListName
        '        Else
        '            sListName = "DontKnow"
        '        End If
        '        IDHAddOns.idh.data.Base.doReleaseObject(oPList)
        '    End If

        '    Return sListName
        'End Function

        'Public Function doGetLorryTareWeights(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sVehicleReg As String) As Object()
        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '    'Dim dValues() As Double = {0, 0, }
        '    Dim dValues() As Object = {CDbl(0.0), CDbl(0.0), "", ""}
        '    'Dim dValues As New Collection

        '    If sVehicleReg Is Nothing AndAlso sVehicleReg.Length = 0 Then
        '        Return dValues
        '    End If
        '    Try
        '        Dim sQry As String
        '        If Config.ParameterAsBool("VMUSENEW", False) Then
        '            sQry = "SELECT U_Tarre, U_TRWTE1 FROM [@IDH_VEHMAS2] WITH(NOLOCK) WHERE U_VehReg = '" & sVehicleReg & "'"
        '            oRecordSet = oParent.goDB.doSelectQuery(sQry)
        '            If oRecordSet.RecordCount > 0 Then
        '                dValues(0) = (oRecordSet.Fields.Item(0).Value)

        '                Dim dtDate As DateTime = oRecordSet.Fields.Item(1).Value
        '                If dtDate.Year > 1900 Then
        '                    dValues(2) = com.idh.utils.Dates.doDateToSBODateStr(oRecordSet.Fields.Item(1).Value)
        '                End If
        '            End If
        '        Else
        '            sQry = "SELECT U_Tarre, U_TRLTar,U_TRWTE1 ,U_TRWTE2 FROM [@IDH_VEHMAS] WITH(NOLOCK) WHERE U_VehReg = '" & sVehicleReg & "'"
        '            oRecordSet = oParent.goDB.doSelectQuery(sQry)
        '            If oRecordSet.RecordCount > 0 Then
        '                dValues(0) = (oRecordSet.Fields.Item(0).Value)
        '                dValues(1) = (oRecordSet.Fields.Item(1).Value)

        '                Dim dtDate As DateTime = oRecordSet.Fields.Item(2).Value
        '                If dtDate.Year > 1900 Then
        '                    dValues(2) = com.idh.utils.Dates.doDateToSBODateStr(oRecordSet.Fields.Item(2).Value)
        '                End If
        '                dtDate = oRecordSet.Fields.Item(3).Value
        '                If dtDate.Year > 1900 Then
        '                    dValues(3) = com.idh.utils.Dates.doDateToSBODateStr(oRecordSet.Fields.Item(3).Value)
        '                End If
        '            End If
        '        End If
        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXLTRW", {sVehicleReg})
        '    Finally
        '        IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
        '    End Try
        '    Return dValues
        'End Function

        'Public Function doGetPOLimitsDOWO(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sWR1OrdType As String, ByVal sCardCode As String, ByVal sCustAddr As String, ByVal sCustRef As String, ByVal dValue As Double, Optional ByVal bDoAlert As Boolean = True) As Boolean
        '    Dim sDoCheck As String = com.idh.bridge.lookups.Config.Parameter(sWR1OrdType & "PODO")
        '    If Not sDoCheck Is Nothing AndAlso sDoCheck.ToUpper().Equals("FALSE") = True Then
        '        Return True
        '    End If

        '    If sCardCode Is Nothing OrElse sCardCode.Length = 0 Then
        '        Return True
        '    End If

        '    If sCustAddr Is Nothing OrElse sCustAddr.Length = 0 Then
        '        Return True
        '    Else
        '        sCustAddr = IDHAddOns.idh.data.Base.doFixForSQL(sCustAddr)
        '    End If

        '    If sCustRef Is Nothing OrElse sCustRef.Length = 0 Then
        '        Return True
        '    End If

        '    Dim sQry As String
        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '    Dim dQty As Double = 0
        '    Dim dVal As Double = 0
        '    Dim dAQty As Double = 0
        '    Dim dAVal As Double = 0
        '    'Dim sMessage As String = ""
        '    Dim sAlertMessage As String = ""
        '    Dim sResourceMessage As String
        '    Try
        '        sQry = "select " &
        '             "    a.U_ISBCUSTRN, " &
        '             "    a.Address, " &
        '             "    case when a.U_IDHPOQT Is NULL then 0 else a.U_IDHPOQT end As U_IDHPOQT, " &
        '             "    case when a.U_IDHPOVA Is NULL then 0 else a.U_IDHPOVA end As U_IDHPOVA " &
        '             "from CRD1 a " &
        '             "where " &
        '             "    a.Address = '" & IDHAddOns.idh.data.Base.doFixForSQL(sCustAddr) & "' " &
        '             "    AND a.CardCode = '" & sCardCode & "' " &
        '             "    AND a.U_ISBCUSTRN = '" & sCustRef & "' " &
        '             "    AND a.AdresType = 'S' "

        '        oRecordSet = oParent.goDB.doSelectQuery(sQry)
        '        If oRecordSet.RecordCount > 0 Then
        '            dAQty = oRecordSet.Fields.Item(2).Value
        '            dAVal = oRecordSet.Fields.Item(3).Value
        '        Else
        '            Return True
        '        End If

        '        sQry = "select " &
        '             "    a.U_ISBCUSTRN, " &
        '             "    o.U_CardCd, " &
        '             "    a.Address, " &
        '             "    COUNT(r.Code) AS QTY, " &
        '             "    SUM(r.U_Total) AS VALUE, " &
        '             "    case when a.U_IDHPOQT Is NULL then 0 else a.U_IDHPOQT end As U_IDHPOQT, " &
        '             "    case when a.U_IDHPOVA Is NULL then 0 else a.U_IDHPOVA end As U_IDHPOVA " &
        '             "from IDH_VROWS r, IDH_VORDERS o, CRD1 a " &
        '             "where " &
        '             "    r.U_JobNr = o.Code " &
        '             "    AND r.U_Status not in ('" & com.idh.bridge.lookups.FixedValues.getStatusFoc() & "', '" & com.idh.bridge.lookups.FixedValues.getDoFocStatus() & "', '" & com.idh.bridge.lookups.FixedValues.getReqFocStatus() & "') " &
        '             "    AND o.U_CardCd = '" & sCardCode & "' " &
        '             "    AND a.Address = '" & IDHAddOns.idh.data.Base.doFixForSQL(sCustAddr) & "' " &
        '             "    AND a.CardCode = o.U_CardCd " &
        '             "    AND a.U_ISBCUSTRN = '" & sCustRef & "' " &
        '             "    AND a.U_ISBCUSTRN =  o.U_CustRef " &
        '             "    AND a.AdresType = 'S' " &
        '             "Group By " &
        '             "    o.U_CardCd, " &
        '             "    a.U_ISBCUSTRN, " &
        '             "    a.Address, " &
        '             "    a.U_IDHPOVA, " &
        '             "    a.U_IDHPOQT"

        '        oRecordSet = oParent.goDB.doSelectQuery(sQry)
        '        If oRecordSet.RecordCount > 0 Then
        '            dQty = oRecordSet.Fields.Item(3).Value
        '            dVal = oRecordSet.Fields.Item(4).Value
        '        End If
        '        IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)

        '        If dAQty > 0 Then
        '            dQty = dQty + 1
        '            If dQty = dAQty Then
        '                '                        sMessage = sMessage & _
        '                '                        		getTranslatedWord("WARNING:  Maximum number of Movements has been reached") & ToChar(10) & _
        '                '                               getTranslatedWord("Maximum Defined  Movements") & " = " & dAQty & ToChar(10) & ToChar(10)
        '                '                        sMessageId = "GENMXM"
        '                Dim saParams As String() = {sCustRef, dAQty.ToString()}
        '                sResourceMessage = Messages.INSTANCE.getMessage("GENMXM", saParams)
        '                If oParent.doMessage(sResourceMessage, 1, "Ok", "Cancel", Nothing, True) = 2 Then
        '                    Return False
        '                End If
        '                sAlertMessage = sAlertMessage & ToChar(10) & ToChar(10) & sResourceMessage
        '            ElseIf dQty > dAQty Then
        '                '                        sMessage = sMessage & _
        '                '                        			getTranslatedWord("WARNING:  Maximum number of Movements has been exceeded") & ToChar(10) & _
        '                '                                    getTranslatedWord("Maximum Defined  Movements") & " = " & dAQty & ToChar(10) & _
        '                '                                    getTranslatedWord("Current Movemets") & " = " & dQty & ToChar(10) & ToChar(10)
        '                Dim saParams As String() = {sCustRef, dAQty.ToString(), dQty.ToString()}
        '                sResourceMessage = Messages.INSTANCE.getMessage("GENMXME", saParams)
        '                If oParent.doMessage(sResourceMessage, 1, "Ok", "Cancel", Nothing, True) = 2 Then
        '                    Return False
        '                End If
        '                sAlertMessage = sAlertMessage & ToChar(10) & ToChar(10) & sResourceMessage
        '            End If
        '        End If

        '        If dAVal > 0 Then
        '            If dValue > 0 Then
        '                dVal = dVal + dValue
        '            End If
        '            If dVal = dAVal Then
        '                '                        sMessage = sMessage & _
        '                '                        			getTranslatedWord("WARNING:  Maximum Order Value has been reached") & ToChar(10) & _
        '                '                                    getTranslatedWord("Maximum Defined Value") & " = " & oParent.doDoubleToMoney(dAVal) & ToChar(10) & ToChar(10)
        '                Dim saParams As String() = {sCustRef, oParent.doDoubleToMoney(dAVal).ToString()}
        '                sResourceMessage = Messages.INSTANCE.getMessage("GENMXOV", saParams)
        '                If oParent.doMessage(sResourceMessage, 1, "Ok", "Cancel", Nothing, True) = 2 Then
        '                    Return False
        '                End If
        '                sAlertMessage = sAlertMessage & ToChar(10) & ToChar(10) & sResourceMessage
        '            ElseIf dVal > dAVal Then
        '                '                        sMessage = sMessage & _
        '                '                        			getTranslatedWord("WARNING:  Maximum Order Value has been exceeded") & ToChar(10) & _
        '                '                                    getTranslatedWord("Maximum Defined Value") & " = " & oParent.doDoubleToMoney(dAVal) & ToChar(10) & _
        '                '                                    getTranslatedWord("Current Values") & " = " & oParent.doDoubleToMoney(dVal) & ToChar(10) & ToChar(10)
        '                Dim saParams As String() = {sCustRef, oParent.doDoubleToMoney(dAVal), oParent.doDoubleToMoney(dVal)}
        '                sResourceMessage = Messages.INSTANCE.getMessage("GENMXOVE", saParams)
        '                If oParent.doMessage(sResourceMessage, 1, "Ok", "Cancel", Nothing, True) = 2 Then
        '                    Return False
        '                End If
        '                sAlertMessage = sAlertMessage & ToChar(10) & ToChar(10) & sResourceMessage
        '            End If
        '        End If
        '    Catch ex As Exception
        '        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error retrieving Purchase Order Limits - Customer: " & sCardCode & " - CustAddress: " & sCustAddr)
        '        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRPOL", {sCardCode, sCustAddr})
        '    Finally
        '        IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)

        '        If bDoAlert = True Then
        '            If Not sAlertMessage Is Nothing AndAlso sAlertMessage.Length > 0 Then
        '                Dim sAlertUsers As String = com.idh.bridge.lookups.Config.Parameter("MDCRAL")
        '                If Not sAlertUsers Is Nothing AndAlso sAlertUsers.Length > 0 Then
        '                    Dim oUsers() As String = sAlertUsers.Split(",")
        '                    oParent.doSendAlert(oUsers, Translation.getTranslatedWord("Cust Ref Limit") & " : " & sCustRef, sAlertMessage, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner, sCardCode, True)
        '                End If
        '            End If
        '        End If
        '    End Try
        '    Return True
        'End Function

        'Public Function doDecodeCCNumber(ByVal sCC As String) As String
        '    Return sCC
        'End Function

        'Public Function doEnCodeCCNumber(ByVal sCC As String) As String
        '    Return sCC
        'End Function

        'Public Function getCCDetails(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sCustomer As String, ByVal sCCNum As String, ByVal sCCTypeCode As String) As Hashtable
        '    Dim oData As New Hashtable
        '    Dim sQry As String
        '    sQry = "SELECT c1.CardCode, c1.CrCardNum, ct.CreditCard, ct.CardName, c1.CardValid, c1.AvrageLate, ct.CompanyId, ct.Phone, ct.AcctCode " &
        '           "      FROM OCRD c1, OCRC ct " &
        '           "      WHERE c1.CreditCard = ct.CreditCard " &
        '           "          AND c1.CrCardNum = '" & sCCNum & "' " &
        '           "          AND ct.CreditCard = '" & sCCTypeCode & "' " &
        '           "          AND c1.CardCode = '" & sCustomer & "' " &
        '           "UNION " &
        '           "SELECT c2.U_CardCd, c2.U_CCNum, ct.CreditCard, ct.CardName, c2.U_CCExp, c2.U_CCAvDe, ct.CompanyId, ct.Phone, ct.AcctCode  " &
        '           "      FROM [@IDH_CCDTL] c2, OCRC ct " &
        '           "      WHERE c2.U_CCType = ct.CreditCard " &
        '           "          AND c2.U_CCNum = '" & sCCNum & "' " &
        '           "          AND ct.CreditCard = '" & sCCTypeCode & "' " &
        '           "          AND c2.U_CardCd = '" & sCustomer & "'"
        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '    Try
        '        oRecordSet = oParent.goDB.doSelectQuery(sQry)
        '        If Not oRecordSet Is Nothing AndAlso oRecordSet.RecordCount > 0 Then
        '            oData.Add("CardCode", oRecordSet.Fields.Item(0).Value)
        '            oData.Add("CCNum", oRecordSet.Fields.Item(1).Value)
        '            oData.Add("CCType", oRecordSet.Fields.Item(2).Value)
        '            oData.Add("CCTypeName", oRecordSet.Fields.Item(3).Value)
        '            oData.Add("CCExpiry", oRecordSet.Fields.Item(4).Value)
        '            oData.Add("CCDelay", oRecordSet.Fields.Item(5).Value)
        '            oData.Add("CompID", oRecordSet.Fields.Item(6).Value)
        '            oData.Add("Phone", oRecordSet.Fields.Item(7).Value)
        '            oData.Add("GLAcct", oRecordSet.Fields.Item(8).Value)
        '        End If
        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRCCD", {Nothing})
        '    Finally
        '        IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
        '    End Try
        '    Return oData
        'End Function
    End Class
End Namespace
