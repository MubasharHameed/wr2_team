'****
'**** CHANGE THE WF - ZIPCODE AND ADDRESS TO WORK WITH THE NEWLY ADDED FORM ITEMS
'****

Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports IDHAddOns.idh.addon

Imports com.idh.bridge.reports
Imports com.idh.bridge
Imports System
Imports com.idh.utils.Conversions
Imports com.idh.bridge.lookups
Imports WR1_Grids.idh.controls.grid
'Imports WR.idh.const
Imports com.idh.dbObjects.User
Imports com.idh.dbObjects.Shared.User
Imports com.idh.bridge.data
Imports com.idh.utils
Imports com.uBC.utils
Imports com.uBC.data
Imports com.idh.dbObjects
Imports com.idh.dbObjects.numbers
Imports com.idh.bridge.resources

'************************************************************************************************
'** BUFFERS
'************************************************************************************************
' INPUT BUFFER (ADD - NEW)
' 0 -  Extra (Normally the action, NoUpdate - Caller will Add)
' 1 -  Entry Code (Ignored)
' 2 -  JobNumber (Waste Order)
' 3 -  Item Group
' 4 -  Requested Date
' 5 -  Requested Time
' 6 -  Skip Lic Number
' 7 -  Skip Lic Expiry
' 8 -  Skip Lic Charge
' 9 -  Skip Lic Supplier
' 10 - CardCode (Customer Code)
' 11 - CardName (Customer Name)
' 12 - Booking Date
' 13 - Booking Time
' 14 - Contract Number
'
' INPUT BUFFER (CHANGE - MODIFY)
' 0 -  Extra (Normally the action, NoUpdate - Caller will Update, DoUpdate - Save if changed)
' 1 -  Entry Code
' 2 -  JobNumber (Waste Order)
' 3 -  JobType
' 4 -  Requested Date
' 5 -  Requested Time
' 6 -  Action Start Date
' 7 -  Action Start Time
' 8 -  Action End Date
' 9 -  Action End Time
' 10 - Item Code
' 11 - Item Description
' 12 - Item Group
' 13 - Lorry
' 14 - Driver
' 15 - Document Number
' 16 - Tipping Supplier
' 17 - Tipping Weight
' 18 - Tipping Cost
' 19 - Tipping Total
' 20 - Customer Tipping Weight
' 21 - Customer Tipping Charge
' 22 - Customer Tipping Total
' 23 - Congestion Charge
' 24 - Price
' 25 - Discount
' 26 - Total
' 27 - Skip Lic Number
' 28 - Skip Lic Expiry
' 29 - Skip Lic Charge
' 30 - Skip Lic Supplier
' 31 - Container Serial Number
' 32 - Status
' 33 - CardCode (Customer Code)
' 34 - CardName (Customer Name)
' 35 - Booking Date
' 36 - Booking Time
' 37 - Vehicle Type
'
' 38 - Discount Amount
' 39 - Tax Amount
' 40 - Supplier Ref No.
' 41 - WasteCode
' 42 - Waste Description
' 43 - Contract Number
'
' OUTPUT BUFFER
'  0 - Mode ( Add, Change.... )
'  1 - State (MustCalc,Late,Busy,Done,Waiting,No Request)
'  2 - Entry Code
'  3 - JobNumber (Waste Order Number)
'  4 - JobType
'  5 - Requested Date
'  6 - Requested Time
'  7 - Actual Start Date
'  8 - Actual Start Time
'  9 - Actual End Date
' 10 - Actual End Time
' 11 - Item Code
' 12 - Item Description
' 13 - Item group
' 14 - Lorry
' 15 - Driver
' 16 - Document Number
' 17 - Tipping Supplier
' 18 - Tipping Weight
' 19 - Tipping Cost
' 20 - Tipping Total
' 21 - Customer Tipping Weight
' 22 - Customer Tipping Charge
' 23 - Customer Tipping Total
' 24 - Congestion Charge
' 25 - Price
' 26 - Discount
' 27 - Total
' 28 - Skip Lic Number
' 29 - Skip Lic Expiry
' 30 - Skip Lic Charge
' 31 - Skip Lic Supplier
' 32 - Container Serial number
' 33 - Status
' 34 - Vehicle Type
'
' 35 - Discount Amount
' 36 - Tax Amount
' 37 - Supplier Ref No.
' 38 - Waste Code
' 39 - Waste Description
' 40 - ScheduleData

'*****************************************************************************************

Namespace idh.forms
    Public Class OrderRow
        Inherits IDHAddOns.idh.forms.Base

        Private miPriceLookupSkipBackCol As Integer = 14930874
        Private miDefaultBackColor As Integer = -99
        Public Function DefaultBackColor(ByRef oForm As SAPbouiCOM.Form) As Integer
            If miDefaultBackColor = -99 Then
                Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("IDH_ITMCOD")
                miDefaultBackColor = oItem.BackColor
            End If
            Return miDefaultBackColor
        End Function


        Private ghLastRow As New Hashtable
        Private Shared msDefaultAUOM As String = "ea"

        Shared msMainTable As String = "@IDH_JOBSHD"

        'KA -- START -- 20121025

        Shared moJobEndEnabled() As String = {
            "IDH_OBLED", "IDH_CUSREF", "IDH_ONCS", "IDH_CARRIE", "IDH_CARNAM",
            "IDH_LORRY", "IDH_VEHTP", "IDH_DRIVER", "IDH_STARDT", "IDH_STARTM",
            "IDH_ENDDT", "IDH_ENDTM", "IDH_COMM", "IDH_COMM2",
            "IDH_TUOMQT", "IDH_TRDWGT", "IDH_TIPWEI", "IDH_TIPCOS", "uBC_TIPCOS", "IDH_ORDQTY", "IDH_ORDCOS", "uBC_ORDCOS",
            "IDH_USERE", "IDH_USEAU",
            "IDH_UOM", "IDH_PUOM", "IDH_CUSWEI", "IDH_RDWGT", "IDH_CUSCM", "IDH_CUSCHG", "uBC_CUSCHG",
            "IDH_CUSQTY", "IDH_DSCPRC", "IDH_DISCNT",
            "IDH_CUSCHR", "uBC_CUSCHR", "IDH_CASHMT",
            "IDH_CUSTRE", "IDH_DOORD", "IDH_REBATE", "IDH_DOARI", "IDH_DOPO",
            "IDH_FOC", "IDH_HAULAC",
            "IDH_WASCL1", "IDH_WASMAT", "IDH_ALTITM",
            "IDH_DOJRN", "IDH_DIST",
            "IDH_CBICNT", "IDH_CBIQTY", "IDH_CBIUOM",
            "IDH_EXTCM1", "IDH_EXTCM2", "IDH_MAXINM", "IDH_OTCOM",
            "IDH_ANYLS1", "IDH_ANYLS2", "IDH_ANYLS3", "IDH_ANYLS4", "IDH_ANYLS5",
            "IDH_JOBTYPE", "IDH_ITMCOD", "IDH_DESC", "IDH_SERNR", "IDH_TIPPIN", "IDH_TIPNM", "IDH_SITADD", "IDH_SUPREF", "IDH_SUPCOD", "IDH_SUPNAM", "IDH_SUPRFN"
        }
        'KA -- END -- 20121025
        '' ''## MA Start 20-10-2014

        Dim moLinkedWODisabled() As String = {
                "IDH_TUOMQT", "IDH_TRDWGT", "IDH_TIPWEI", "IDH_PUOM", "IDH_ORDQTY", "IDH_RDWGT",
                "IDH_UOM", "IDH_CUSQTY", "IDH_CUSWEI", "IDH_HLSQTY",
                "IDH_PURWGT", "IDH_SUPWGT", "IDH_PURUOM", "IDH_CARWGT"
        }
        '' ''## MA End 20-10-2014

        Shared moSOMarketingCheckBoxes() As String = {"IDH_DOPO", "IDH_CUSTRE", "IDH_DOORD", "IDH_REBATE", "IDH_DOARI", "IDH_FOC", "IDH_DOJRN"}

        Shared moMarketingCheckboxes() As String = {
            "IDH_AINV", "IDH_FOC", "IDH_DOORD", "IDH_DOARI", "IDH_DOARIP", "IDH_BOOKIN", "IDH_DOPO", "IDH_CUSTRE", "IDH_DOJRN", "IDH_CARRRE", "IDH_REBATE"
        }

        '' ''## MA Start Issue#407
        '' Added IDH_RTCD field name in list below 
        '' ''## MA End Issue#407
        Shared moJobOpenEnabled() As String = {
            "IDH_STARDT", "IDH_STARTM", "IDH_LORRY", "IDH_TIPPIN",
            "IDH_TUOMQT", "IDH_TRDWGT", "IDH_TIPWEI", "IDH_TIPCOS", "uBC_TIPCOS", "IDH_CUSWEI", "IDH_DRIVER", "IDH_ENDDT", "IDH_ENDTM", "IDH_DOCNR",
            "IDH_CUSCHG", "uBC_CUSCHG", "IDH_DISCNT", "IDH_CONCHG", "IDH_SERNR", "IDH_DESC", "IDH_ITMCOD", "IDH_DOORD", "IDH_REBATE",
            "IDH_SKPLIC", "IDH_LICEXP", "IDH_LICCHR", "IDH_LICSUP", "IDH_WASMAT", "IDH_WASCL1", "IDH_ALTITM",
            "IDH_REQDAT", "IDH_REQTIM", "IDH_SUPREF", "IDH_DSCPRC", "IDH_VEHTP", "IDH_2RD", "IDH_2RW",
            "IDH_2RM", "IDH_2RY", "IDH_2FRE", "IDH_2FTU", "IDH_2FWE", "IDH_2FTH", "IDH_2FFR", "IDH_2FSA",
            "IDH_2FSU", "IDH_2FMO", "IDH_2RRA", "IDH_2RRB", "IDH_2RRVA", "IDH_2RRS", "IDH_2RRVB", "IDH_2RO",
            "IDH_TIPNM", "IDH_CARRIE", "IDH_CARNAM", "IDH_DIST", "IDH_SLNM", "IDH_ORDQTY", "IDH_ORDCOS", "uBC_ORDCOS",
            "IDH_DOPO", "IDH_REQTIT", "IDH_CUSQTY", "IDH_CUSCHR", "uBC_CUSCHR",
            "IDH_DOARI", "IDH_FOC", "IDH_CUSREF", "IDH_RDWGT", "IDH_COMM", "IDH_CUSCM", "IDH_REMPT", "IDH_REMPQ",
            "IDH_REMNOT", "IDH_JOBRMD", "IDH_JOBRMT", "IDH_BRANCH", "IDH_OBLED", "IDH_COMM2", "IDH_ONCS",
            "IDH_HAZCN", "IDH_SCHED", "IDH_USCHED", "IDH_JOBTPE", "IDH_HAULAC", "IDH_SUPCOD", "IDH_SUPNAM",
            "IDH_SUPRFN", "IDH_CARREF", "IDH_EXPLDW", "IDH_ISTRL", "IDH_PUOM", "IDH_UOM", "IDH_USEAU", "IDH_USERE",
            "IDH_SITADD", "IDH_DOJRN", "IDH_CBICNT", "IDH_CBIQTY", "IDH_CBIUOM", "IDH_EXTCM1", "IDH_EXTCM2", "IDH_OTCOM",
            "IDH_MAXINM", "IDH_TFSREQ", "IDH_TFSNUM",
            "IDH_EVNREQ", "IDH_EVNNUM",
            "IDH_ANYLS1", "IDH_ANYLS2", "IDH_ANYLS3", "IDH_ANYLS4", "IDH_ANYLS5",
            "IDH_ITCF", "IDH_SRF", "IDH_CSCF", "IDH_WASCF",
            "IDH_TIPCF", "IDH_SITAL", "IDH_SUPCF", "IDH_CCCF", "IDH_VCRCF", "IDH_RTCD", "IDH_SAMPRQ"
        }
        '', "IDH_SKPQTY", "IDH_SKPCOS", "IDH_SKPTOT"
        ''## MA End 28-08-2014 Issue#406 Added filed names IDH_JBDNDT, IDH_JBDNTM

        '** Cost fields
        '** Tipping PO -  ' DONT KNOW - , "IDH_SITAL" -- 'ALWAYS DISABLED - "IDH_TIPTOT", "IDH_DCOCUR"
        Shared moTIPPO_SupportItems() As String = {"IDH_TIPPIN", "IDH_TIPNM", "IDH_TIPCF", "IDH_SITADD", "IDH_SUPREF"}
        Shared moTIPPO_WeightItems() As String = {"IDH_TUOMQT", "IDH_TRDWGT", "IDH_TIPWEI", "IDH_PUOM"}
        Shared moTIPPO_PriceItems() As String = {"IDH_TIPCOS", "uBC_TIPCOS", "IDH_SIP"}

        '** Producer PO -- ALWAYS DISABLED - , "380", "381", "IDH_PURTOT", "IDH_PCOCUR"
        Shared moProPO_SupportItems() As String = {"IDH_SUPCOD", "IDH_SUPNAM", "IDH_SUPCF", "IDH_SUPRFN"}
        Shared moProPO_WeightItems() As String = {"IDH_PUOMQT", "IDH_PURWGT", "IDH_SUPWGT", "IDH_PURUOM"}
        Shared moProPO_PriceItems() As String = {"IDH_PRCOST", "uBC_PRCOST", "IDH_PSIP"}

        '** Haulage PO -- ALWAYS DISABLED - , "382", "383" , "388", "IDH_CARWGT", "IDH_ORDTOT", "IDH_CCOCUR"
        Shared moJOBPO_SupportItems() As String = {"IDH_CARRIE", "IDH_CARNAM", "IDH_CCCF", "IDH_CARREF"}
        Shared moJOBPO_WeightItems() As String = {"IDH_ORDQTY"}
        Shared moJOBPO_PriceItems() As String = {"IDH_ORDCOS", "uBC_ORDCOS", "IDH_WSIP"}

        Shared moSIPCIPButtons() As String = {"IDH_CIP", "IDH_SIP", "IDH_PSIP", "IDH_WSIP"}

        '** Charge Fields
        '** Tip SO - ALWAYS DISABLED - "IDH_CUSTOT", "IDH_DCGCUR"
        Shared moTIPSO_SupportItems() As String = {}
        Shared moTIPSO_WeightItems() As String = {"IDH_EXPLDW", "IDH_USEAU", "IDH_CUSCM", "IDH_USERE", "IDH_RDWGT", "IDH_CUSWEI", "IDH_UOM"}
        Shared moTIPSO_PriceItems() As String = {"IDH_CUSCHG", "uBC_CUSCHG", "IDH_CIP"}

        '** Haulage SO - ALWAYS DISABLED - "379", "IDH_HLSQTY", "IDH_JOBPRC", "IDH_HCGCUR"
        Shared moJOBSO_SupportItems() As String = {}
        Shared moJOBSO_WeightItems() As String = {"IDH_HAULAC", "IDH_CUSQTY", "IDH_HLSQTY"}
        Shared moJOBSO_PriceItems() As String = {"IDH_CUSCHR", "uBC_CUSCHR", "IDH_CIP"}

        '** Total Fields
        Shared moSO_Totalstems() As String = {"IDH_CUSTOT", "IDH_JOBPRC", "IDH_ADDCHR", "IDH_TBD", "IDH_SUBTOT", "IDH_TAX", "IDH_TOTAL", "IDH_ADDCH", "IDH_ADDCHV", "IDH_TADDCH"}
        Shared moPO_Totalstems() As String = {"IDH_PURTOT", "IDH_TIPTOT", "IDH_ORDTOT", "IDH_ADDCOS", "IDH_PVAT", "IDH_TOTCOS", "IDH_PROFIT", "IDH_ADDCO", "IDH_ADDCOV", "IDH_TADDCO"}

        Shared moOrdredDisabled() As String = {
                "IDH_CONCHG", "IDH_DISCNT", "IDH_CASHMT", "IDH_DSCPRC",
                "IDH_LICCHR", "IDH_ITMCOD", "IDH_DESC", "IDH_ITCF",
                "IDH_WASCL1", "IDH_WASMAT", "IDH_ALTITM", "IDH_WASCF",
                "IDH_REQDAT", "IDH_REQTIM", "IDH_REQTIT",
                "IDH_STARDT", "IDH_STARTM",
                "IDH_ENDDT", "IDH_ENDTM",
                "IDH_TFSNUM", "IDH_TFSSC",
                "IDH_EVNNUM", "IDH_EVNSC",
                "IDH_JOBTPE"
        }

        'Shared moOrdredDisabled() As String = { _
        '        "IDH_USERE", "IDH_USEAU", _
        '        "IDH_CUSWEI", "IDH_HLSQTY", "IDH_RDWGT", "IDH_CUSCM", "IDH_CUSCHG", "uBC_CUSCHG", "IDH_CUSTOT", _
        '        "IDH_SUPWGT", _
        '        "IDH_CONCHG", "IDH_JOBPRC", "IDH_DISCNT", "IDH_TOTAL", "IDH_CASHMT", _
        '        "IDH_LICCHR", "IDH_ITMCOD", "IDH_WASCL1", "IDH_WASMAT", "IDH_ALTITM", _
        '        "IDH_CUSQTY", "IDH_CUSCHR", "uBC_CUSCHR", _
        '        "IDH_REQDAT", "IDH_REQTIM", "IDH_REQTIT", "IDH_STARDT", "IDH_STARTM", _
        '        "IDH_ENDDT", "IDH_ENDTM", "IDH_DESC", "IDH_ITCF", "IDH_WASCF", _
        '        "IDH_UOM", "IDH_DSCPRC", "IDH_TFSNUM", "IDH_TFSSC", _
        '        "IDH_DOORD", "IDH_FOC", "IDH_DOARI", "IDH_JOBTPE", _
        '        "IDH_CARWGT" _
        '}

        ''Dim oItems1() As String = {"IDH_WASCL1", "IDH_WASMAT", "IDH_WASCF"}

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            'OnTime [#Ico000????] USA
            'WR Config based WO Row form title
            'MyBase.New(oParent, "IDHJOBS", Nothing, -1, "OrderRow.srf", True, True, False, "Waste Order Row Details", load_Types.idh_LOAD_NORMAL)
            'MyBase.New(oParent, "IDHJOBS", "IDHORD", iMenuPosition, "OrderRow.srf", True, True, False, Config.Parameter("WORNAME"), IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_PRE)
            MyBase.New(oParent, "IDHJOBS", "IDHORD", iMenuPosition, Config.ParameterWithDefault("WORFORM", "OrderRow.srf"), True, True, False, Config.Parameter("WORNAME"), IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL)

            doEnableHistory(msMainTable)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                '            Dim oItem As SAPbouiCOM.Item
                '            Dim oOpt As SAPbouiCOM.OptionBtn

                '            'oItem = doAddItem(oForm, "PRKITM", SAPbouiCOM.BoFormItemTypes.it_EDIT, 0, 0, 0, 0, 0)

                '            doAddUFCheck(oForm, "IDH_DOORD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                '            doAddUFCheck(oForm, "IDH_FOC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                '            doAddUFCheck(oForm, "IDH_DOARI", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                '            doAddUFCheck(oForm, "IDH_AVDOC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
                '            doAddUFCheck(oForm, "IDH_DOPO", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")

                '            oForm.DataSources.UserDataSources.Add("U_BTime", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 10)
                '            oForm.DataSources.UserDataSources.Add("U_ItmGrN", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 30)
                '            oForm.DataSources.UserDataSources.Add("U_VehNam", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 30)
                '            oForm.DataSources.UserDataSources.Add("U_BefDis", SAPbouiCOM.BoDataType.dt_PRICE, 10)
                '            oForm.DataSources.UserDataSources.Add("U_SubTot", SAPbouiCOM.BoDataType.dt_PRICE, 10)

                '            'USER DEFINED FIELDS FOR RECURING JOBS
                '            'RADIO BUTTONS
                '            oForm.DataSources.UserDataSources.Add("U_2RD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                '            oForm.DataSources.UserDataSources.Add("U_2RW", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                '            oForm.DataSources.UserDataSources.Add("U_2RM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                '            oForm.DataSources.UserDataSources.Add("U_2RY", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                '            oForm.DataSources.UserDataSources.Add("U_2RO", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)

                '            oForm.DataSources.UserDataSources.Add("U_2RRA", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                '            oForm.DataSources.UserDataSources.Add("U_2RRB", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)

                '            'CHECKBOX
                '            oForm.DataSources.UserDataSources.Add("U_2FMO", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                '            oForm.DataSources.UserDataSources.Add("U_2FTU", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                '            oForm.DataSources.UserDataSources.Add("U_2FWE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                '            oForm.DataSources.UserDataSources.Add("U_2FTH", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                '            oForm.DataSources.UserDataSources.Add("U_2FFR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                '            oForm.DataSources.UserDataSources.Add("U_2FSA", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                '            oForm.DataSources.UserDataSources.Add("U_2FSU", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)

                '            oForm.DataSources.UserDataSources.Add("U_2FRE", SAPbouiCOM.BoDataType.dt_LONG_NUMBER, 5)
                '            oForm.DataSources.UserDataSources.Add("U_2RRS", SAPbouiCOM.BoDataType.dt_DATE, 10)
                '            oForm.DataSources.UserDataSources.Add("U_2RRVA", SAPbouiCOM.BoDataType.dt_LONG_NUMBER, 5)
                '            oForm.DataSources.UserDataSources.Add("U_2RRVB", SAPbouiCOM.BoDataType.dt_DATE, 10)

                '            oItem = oForm.Items.Add("IDH_VtPrc", SAPbouiCOM.BoFormItemTypes.it_EDIT)

                '            oItem.Visible = False
                '            doSetEnabled(oItem, False)

                '            'oItem.Specific.DataBind.SetBound(True, "", "U_VatPrc")

                '            oOpt = oForm.Items.Item("IDH_USERE").Specific
                '            oOpt.ValOn = "1"
                '            oOpt.ValOff = "0"
                '            oOpt.Selected = True
                '            doAddFormDF(oForm, "IDH_USERE", "U_UseWgt")

                '            oOpt = oForm.Items.Item("IDH_USEAU").Specific
                '            oOpt.ValOn = "2"
                '            oOpt.GroupWith("IDH_USERE")

                '            doAddFormDF(oForm, "IDH_JOBSCH", "Code")
                '            doAddFormDF(oForm, "IDH_JOBENT", "U_JobNr")
                '            'doAddFormDF(oForm, "IDH_JOBTYP", "U_JobTp")
                '            doAddFormDF(oForm, "IDH_JOBTPE", "U_JobTp")
                '            doAddFormDF(oForm, "IDH_REQDAT", "U_RDate")
                '            doAddFormDF(oForm, "IDH_REQTIM", "U_RTime")
                '            doAddFormDF(oForm, "IDH_REQTIT", "U_RTimeT")
                '            doAddFormDF(oForm, "IDH_STARDT", "U_ASDate")
                '            doAddFormDF(oForm, "IDH_STARTM", "U_ASTime")
                '            doAddFormDF(oForm, "IDH_ENDDT", "U_AEDate")
                '            doAddFormDF(oForm, "IDH_ENDTM", "U_AETime")




                '            doAddFormDF(oForm, "IDH_ITMCOD", "U_ItemCd")
                '            doAddFormDF(oForm, "IDH_DESC", "U_ItemDsc")
                '            doAddFormDF(oForm, "IDH_WASCL1", "U_WasCd")
                '            doAddFormDF(oForm, "IDH_WASMAT", "U_WasDsc")
                '            doAddFormDF(oForm, "IDH_ITMGRP", "U_ItmGrp")
                '            doAddFormDF(oForm, "IDH_VEHTP", "U_VehTyp")
                '            doAddFormDF(oForm, "IDH_LORRY", "U_Lorry")
                '            doAddFormDF(oForm, "IDH_DRIVER", "U_Driver")
                '            doAddFormDF(oForm, "IDH_DOCNR", "U_DocNum")
                '            doAddFormDF(oForm, "IDH_TRDWGT", "U_TRdWgt")
                '            doAddFormDF(oForm, "IDH_TIPWEI", "U_TipWgt")
                '            doAddFormDF(oForm, "IDH_TIPCOS", "U_TipCost")
                '            doAddFormDF(oForm, "IDH_TIPTOT", "U_TipTot")
                '            doAddFormDF(oForm, "IDH_CUSWEI", "U_CstWgt")
                '            doAddFormDF(oForm, "IDH_RDWGT", "U_RdWgt")
                '            doAddFormDF(oForm, "IDH_EXPLDW", "U_ExpLdWgt")
                '            doAddFormDF(oForm, "IDH_CUSCM", "U_AUOMQt")
                '            doAddFormDF(oForm, "IDH_CUSCHG", "U_TCharge")
                '            doAddFormDF(oForm, "IDH_CUSTOT", "U_TCTotal")
                '            doAddFormDF(oForm, "IDH_CONCHG", "U_CongCh")
                '            doAddFormDF(oForm, "IDH_JOBPRC", "U_Price")
                '            doAddFormDF(oForm, "IDH_DISCNT", "U_Discnt")
                '            doAddFormDF(oForm, "IDH_DSCPRC", "U_DisAmt")
                '            doAddFormDF(oForm, "IDH_TAX", "U_TaxAmt")
                '            doAddFormDF(oForm, "IDH_PVAT", "U_VtCostAmt")
                '            doAddFormDF(oForm, "IDH_TOTAL", "U_Total")
                '            doAddFormDF(oForm, "IDH_SKPLIC", "U_SLicNr")
                '            doAddFormDF(oForm, "IDH_LICEXP", "U_SLicExp")
                '            doAddFormDF(oForm, "IDH_LICCHR", "U_SLicCh")
                '            doAddFormDF(oForm, "IDH_BRANCH", "U_Branch")
                '            doAddFormDF(oForm, "IDH_USER", "U_User")
                '            doAddFormDF(oForm, "IDH_SKPQTY", "U_SLicCQt")
                '            doAddFormDF(oForm, "IDH_SKPCOS", "U_SLicCst")
                '            doAddFormDF(oForm, "IDH_SKPTOT", "U_SLicCTo")
                '            doAddFormDF(oForm, "IDH_TOTCOS", "U_JCost")
                '            doAddFormDF(oForm, "IDH_SERNR", "U_Serial")
                '            doAddFormDF(oForm, "IDH_STATUS", "U_Status")
                '            doAddFormDF(oForm, "IDH_ROWSTA", "U_RowSta")

                '            doAddFormDF(oForm, "IDH_CUSREF", "U_CustRef")
                '            doAddFormDF(oForm, "IDH_SUPREF", "U_SiteRef")
                '            doAddFormDF(oForm, "IDH_SUPRFN", "U_ProRef")
                '            doAddFormDF(oForm, "IDH_CARREF", "U_SupRef")

                '            doAddFormDF(oForm, "IDH_BOOKDT", "U_BDate")
                '            doAddFormDF(oForm, "IDH_BOOKTM", "U_BTime")
                '            doAddFormDF(oForm, "IDH_DIST", "U_Dista")

                '            'BP Fields Bound
                '            doAddFormDF(oForm, "IDH_CUST", "U_CustCd")
                '            doAddFormDF(oForm, "IDH_NAME", "U_CustNm")
                '            doAddFormDF(oForm, "IDH_TIPPIN", "U_Tip")
                '            doAddFormDF(oForm, "IDH_TIPNM", "U_TipNm")
                '            doAddFormDF(oForm, "IDH_SITADD", IDH_JOBSHD._SAddress)
                '            doAddFormDF(oForm, "IDH_CARRIE", "U_CarrCd")
                '            doAddFormDF(oForm, "IDH_CARNAM", "U_CarrNm")
                '            doAddFormDF(oForm, "IDH_SUPCOD", "U_ProCd")
                '            doAddFormDF(oForm, "IDH_SUPNAM", "U_ProNm")

                '            doAddFormDF(oForm, "IDH_LICSUP", "U_SLicSp")
                '            doAddFormDF(oForm, "IDH_SLNM", "U_SLicNm")
                '            doAddFormDF(oForm, "IDH_PSTAT", "U_PStat")
                '            doAddFormDF(oForm, "IDH_ORDQTY", "U_OrdWgt")
                '            doAddFormDF(oForm, "IDH_ORDCOS", "U_OrdCost")
                '            doAddFormDF(oForm, "IDH_ORDTOT", "U_OrdTot")
                '            doAddFormDF(oForm, "IDH_WRORD", "U_WROrd")
                '            doAddFormDF(oForm, "IDH_WRROW", "U_WRRow")
                '            doAddFormDF(oForm, "IDH_CUSQTY", "U_CusQty")
                '            doAddFormDF(oForm, "IDH_HLSQTY", "U_HlSQty")
                '            doAddFormDF(oForm, "IDH_HAULAC", "U_HaulAC")
                '            doAddFormDF(oForm, "IDH_CUSCHR", "U_CusChr")
                '            doAddFormDF(oForm, "IDH_COMM", "U_Comment")
                '            doAddFormDF(oForm, "IDH_COMM2", "U_IntComm")
                '            doAddFormDF(oForm, "IDH_UOM", "U_UOM")
                '            doAddFormDF(oForm, "IDH_PUOM", "U_PUOM")
                '            doAddFormDF(oForm, "IDH_CASHMT", "U_PayMeth")
                '            doAddFormDF(oForm, "IDH_JOBRMD", "U_JobRmD")
                '            doAddFormDF(oForm, "IDH_JOBRMT", "U_JobRmT")
                '            doAddFormDF(oForm, "IDH_REMNOT", "U_RemNot")
                '            doAddFormDF(oForm, "IDH_OBLED", "U_Obligated")
                '            doAddFormDF(oForm, "IDH_ORIGIN", "U_Origin")
                '            doAddFormDF(oForm, "IDH_ONCS", "U_CustCs")
                '            ''Mansoor 03-04-2014 Start
                '            doAddFormDF(oForm, "IDH_ADDCOS", "U_AddCost") ' Total Add charge WO VAT
                '            doAddFormDF(oForm, "IDH_ADDCHR", "U_AddCharge") ' Total Add cost WO VAT

                '            doAddFormDF(oForm, "IDH_ADDCOV", "U_TAddCsVat") ' Total Add charge VAT
                '            doAddFormDF(oForm, "IDH_ADDCHV", "U_TAddChVat") ' Total Add cost VAT

                '            doAddFormDF(oForm, "IDH_TADDCH", "U_TAddChrg") ' Total Add charge with VAT
                '            doAddFormDF(oForm, "IDH_TADDCO", "U_TAddCost") ' Total Add cost with VAT
                '            ''End 03-04-2014

                '            doAddFormDF(oForm, "IDH_CARRRE", "U_CarrReb")
                '            doAddFormDF(oForm, "IDH_CUSTRE", "U_CustReb")
                '            doAddFormDF(oForm, "IDH_PBI", "U_LnkPBI")
                '            doAddFormDF(oForm, "IDH_HAZCN", "U_ConNum")

                '            doAddFormDF(oForm, "IDH_TUOMQT", IDH_JOBSHD._TAUOMQt)
                '            doAddFormDF(oForm, "IDH_PUOMQT", "U_PAUOMQt")
                '            doAddFormDF(oForm, "IDH_PURWGT", "U_PRdWgt")
                '            doAddFormDF(oForm, "IDH_SUPWGT", "U_ProWgt")
                '            doAddFormDF(oForm, "IDH_PURUOM", "U_ProUOM")
                '            doAddFormDF(oForm, "IDH_PRCOST", "U_PCost")
                '            doAddFormDF(oForm, "IDH_PURTOT", "U_PCTotal")

                '            doAddFormDF(oForm, "IDH_VALDED", IDH_JOBSHD._ValDed)

                '            doAddFormDF(oForm, "IDH_SCHED", "U_Sched")
                '            doAddFormDF(oForm, "IDH_USCHED", "U_USched")
                '            doAddFormDF(oForm, "IDH_REBATE", IDH_JOBSHD._Rebate)

                '            oForm.Items.Item("IDH_TBD").Specific.DataBind.SetBound(True, "", "U_BefDis")
                '            oForm.Items.Item("IDH_SUBTOT").Specific.DataBind.SetBound(True, "", "U_SubTot")
                '            oForm.Items.Item("IDH_ITMGRN").Specific.DataBind.SetBound(True, "", "U_ItmGrN")

                '            doAddUF(oForm, "IDH_CONVA", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 254, False, False)
                '            doAddUF(oForm, "IDH_SITETL", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, False, False)
                '            doAddUF(oForm, "IDH_SteId", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
                '            doAddUF(oForm, "IDH_ADDR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 200, False, False)
                '            doAddUF(oForm, "IDH_ZP", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 200, False, False)
                '            doAddUF(oForm, "IDH_PROFIT", SAPbouiCOM.BoDataType.dt_PRICE, 10, False, False)
                '            doAddUF(oForm, "IDH_REMPQ", SAPbouiCOM.BoDataType.dt_SHORT_NUMBER, 2, False, False)
                ''Mansoor 03-04-2014 Start
                '            doAddUF(oForm, "IDH_ADDCO", SAPbouiCOM.BoDataType.dt_PRICE, 5, False, False)
                '            doAddUF(oForm, "IDH_ADDCH", SAPbouiCOM.BoDataType.dt_PRICE, 5, False, False)
                '            ''Mansoor 03-04-2014 End     

                '            doAddUF(oForm, "IDH_PAYTRM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100, False, False)

                '            oItem = doAddUF(oForm, "IDH_REMPT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, False, False)
                '            oItem.DisplayDesc = True
                '            Dim oCombo As SAPbouiCOM.ComboBox
                '            oCombo = oItem.Specific
                '            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodHour(), com.idh.bridge.lookups.FixedValues.getPeriodHour())
                '            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodDay(), com.idh.bridge.lookups.FixedValues.getPeriodDay())
                '            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodMonth(), com.idh.bridge.lookups.FixedValues.getPeriodMonth())
                '            oCombo.ValidValues.Add(com.idh.bridge.lookups.FixedValues.getPeriodYear(), com.idh.bridge.lookups.FixedValues.getPeriodYear())

                '            'LINK RECURING FIELDS

                '            'RADIO
                '            Dim oRadio As SAPbouiCOM.OptionBtn
                '            oRadio = oForm.Items.Item("IDH_2RD").Specific
                '            oRadio.DataBind.SetBound(True, "", "U_2RD")
                '            oRadio.ValOff = "N"
                '            oRadio.ValOn = "Y"

                '            oRadio = oForm.Items.Item("IDH_2RW").Specific
                '            oRadio.DataBind.SetBound(True, "", "U_2RW")
                '            oRadio.ValOff = "N"
                '            oRadio.ValOn = "Y"
                '            oRadio.GroupWith("IDH_2RD")

                '            oRadio = oForm.Items.Item("IDH_2RM").Specific
                '            oRadio.DataBind.SetBound(True, "", "U_2RM")
                '            oRadio.ValOff = "N"
                '            oRadio.ValOn = "Y"
                '            oRadio.GroupWith("IDH_2RW")

                '            oRadio = oForm.Items.Item("IDH_2RY").Specific
                '            oRadio.DataBind.SetBound(True, "", "U_2RY")
                '            oRadio.ValOff = "N"
                '            oRadio.ValOn = "Y"
                '            oRadio.GroupWith("IDH_2RM")

                '            oRadio = oForm.Items.Item("IDH_2RO").Specific
                '            oRadio.DataBind.SetBound(True, "", "U_2RO")
                '            oRadio.ValOff = "N"
                '            oRadio.ValOn = "Y"
                '            oRadio.GroupWith("IDH_2RY")
                '            oForm.DataSources.UserDataSources.Item("U_2RD").ValueEx = "5"

                '            oRadio = oForm.Items.Item("IDH_2RRA").Specific
                '            oRadio.DataBind.SetBound(True, "", "U_2RRA")
                '            oRadio.ValOff = "N"
                '            oRadio.ValOn = "Y"

                '            oRadio = oForm.Items.Item("IDH_2RRB").Specific
                '            oRadio.DataBind.SetBound(True, "", "U_2RRB")
                '            oRadio.ValOff = "N"
                '            oRadio.ValOn = "Y"
                '            oRadio.GroupWith("IDH_2RRA")

                '            'CHECKBOX
                '            Dim oCheck As SAPbouiCOM.CheckBox
                '            doAddFormDF(oForm, "IDH_ISTRL", "U_IsTrl")
                '            oCheck = oForm.Items.Item("IDH_ISTRL").Specific
                '            oCheck.ValOff = "N"
                '            oCheck.ValOn = "Y"

                '            oCheck = oForm.Items.Item("IDH_2FMO").Specific
                '            oCheck.DataBind.SetBound(True, "", "U_2FMO")
                '            oCheck.ValOff = "N"
                '            oCheck.ValOn = "Y"

                '            oCheck = oForm.Items.Item("IDH_2FTU").Specific
                '            oCheck.DataBind.SetBound(True, "", "U_2FTU")
                '            oCheck.ValOff = "N"
                '            oCheck.ValOn = "Y"

                '            oCheck = oForm.Items.Item("IDH_2FWE").Specific
                '            oCheck.DataBind.SetBound(True, "", "U_2FWE")
                '            oCheck.ValOff = "N"
                '            oCheck.ValOn = "Y"

                '            oCheck = oForm.Items.Item("IDH_2FTH").Specific
                '            oCheck.DataBind.SetBound(True, "", "U_2FTH")
                '            oCheck.ValOff = "N"
                '            oCheck.ValOn = "Y"

                '            oCheck = oForm.Items.Item("IDH_2FFR").Specific
                '            oCheck.DataBind.SetBound(True, "", "U_2FFR")
                '            oCheck.ValOff = "N"
                '            oCheck.ValOn = "Y"

                '            oCheck = oForm.Items.Item("IDH_2FSA").Specific
                '            oCheck.DataBind.SetBound(True, "", "U_2FSA")
                '            oCheck.ValOff = "N"
                '            oCheck.ValOn = "Y"

                '            oCheck = oForm.Items.Item("IDH_2FSU").Specific
                '            oCheck.DataBind.SetBound(True, "", "U_2FSU")
                '            oCheck.ValOff = "N"
                '            oCheck.ValOn = "Y"

                '            'Bound to DBField
                '            oCheck = oForm.Items.Item("IDH_CUSTRE").Specific
                '            oCheck.ValOn = "Y"
                '            oCheck.ValOff = "N"

                '            'Bound to DBField
                '            oCheck = oForm.Items.Item("IDH_CARRRE").Specific
                '            oCheck.ValOn = "Y"
                '            oCheck.ValOff = "N"

                '            'Bound to DBField
                '            oCheck = oForm.Items.Item("IDH_SCHED").Specific
                '            oCheck.ValOn = "Y"
                '            oCheck.ValOff = "N"

                '            'Bound to DBField
                '            oCheck = oForm.Items.Item("IDH_USCHED").Specific
                '            oCheck.ValOn = "Y"
                '            oCheck.ValOff = "N"

                '            'Bound to DBField
                '            oCheck = oForm.Items.Item("IDH_HAULAC").Specific
                '            oCheck.ValOn = "Y"
                '            oCheck.ValOff = "N"

                '            'Bound to DBField
                '            oCheck = oForm.Items.Item("IDH_REBATE").Specific
                '            oCheck.ValOn = "Y"
                '            oCheck.ValOff = "N"

                '            Dim oEdit As SAPbouiCOM.EditText
                '            oEdit = oForm.Items.Item("IDH_2FRE").Specific
                '            oEdit.DataBind.SetBound(True, "", "U_2FRE")
                '            oEdit.Value = 0

                '            oEdit = oForm.Items.Item("IDH_2RRS").Specific
                '            oEdit.DataBind.SetBound(True, "", "U_2RRS")
                '            oForm.DataSources.UserDataSources.Item("U_2RRS").ValueEx = goParent.doDateToStr()

                '            oEdit = oForm.Items.Item("IDH_2RRVA").Specific
                '            oEdit.DataBind.SetBound(True, "", "U_2RRVA")
                '            oEdit.Value = 0

                '            oEdit = oForm.Items.Item("IDH_2RRVB").Specific
                '            oEdit.DataBind.SetBound(True, "", "U_2RRVB")
                '            oForm.DataSources.UserDataSources.Item("U_2RRVB").ValueEx = goParent.doDateToStr()

                '            doAddFormDF(oForm, "IDH_SODOC", "U_SAORD")
                '            doAddFormDF(oForm, "IDH_DPODOC", "U_TIPPO")
                '            doAddFormDF(oForm, "IDH_HPODOC", "U_JOBPO")
                '            doAddFormDF(oForm, "IDH_PPODOC", "U_ProPO")
                '            doAddFormDF(oForm, "IDH_SGCUDO", "U_GRIn")
                '            doAddFormDF(oForm, "IDH_PGPODO", "U_ProGRPO")
                '            doAddFormDF(oForm, "IDH_SODLVN", "U_SoDlvNot")

                '            If Config.INSTANCE.getParameterAsBool("USAREL", False) Then
                '                doAddFormDF(oForm, "IDH_JRNDOC", "U_Jrnl")
                '                doAddFormDF(oForm, "IDH_STAJRN", "U_JStat")

                '            End If

                '            oForm.DataBrowser.BrowseBy = "IDH_JOBSCH" 'IDH_JOBENT"

                '            'LINK BUTTONS
                '            Dim oLink As SAPbouiCOM.LinkedButton
                '            oLink = oForm.Items.Item("IDH_TIPL").Specific
                '            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

                '            oLink = oForm.Items.Item("IDH_SCLK").Specific
                '            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

                '            oLink = oForm.Items.Item("IDH_ITLK").Specific
                '            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_Items

                '            oLink = oForm.Items.Item("IDH_WASLK").Specific
                '            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_Items

                '            oLink = oForm.Items.Item("IDH_LCLK").Specific
                '            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

                '            oLink = oForm.Items.Item("IDH_CCLK").Specific
                '            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

                '            oLink = oForm.Items.Item("IDH_CSLK").Specific
                '            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

                '            oLink = oForm.Items.Item("IDH_SOLK").Specific
                '            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_Order

                '            oLink = oForm.Items.Item("IDH_DPOLK").Specific
                '            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_PurchaseOrder

                '            oLink = oForm.Items.Item("IDH_HPOLK").Specific
                '            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_PurchaseOrder

                '            oLink = oForm.Items.Item("IDH_PPOLK").Specific
                '            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_PurchaseOrder

                '            oLink = oForm.Items.Item("IDH_PGPOLK").Specific
                '            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_GoodsReceiptPO

                '            oLink = oForm.Items.Item("IDH_SGCULK").Specific
                '            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_GoodsReceipt

                '            oLink = oForm.Items.Item("IDH_SDLVNL").Specific
                '            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_DeliveryNotes

                '            If Config.INSTANCE.getParameterAsBool("USAREL", False) Then
                '                oLink = oForm.Items.Item("IDH_JRNLK").Specific
                '                oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_JournalPosting

                '            End If

                '            oForm.Items.Item("IDH_TABCOS").AffectsFormMode = False
                '            oForm.Items.Item("IDH_TABCOV").AffectsFormMode = False
                '            oForm.Items.Item("IDH_TABCOM").AffectsFormMode = False
                '            oForm.Items.Item("IDH_REMFLD").AffectsFormMode = False
                '            oForm.Items.Item("IDH_TABADD").AffectsFormMode = False

                '            'OnTime 32355: WR1_NewDev_Buying&Trading_Add functionality to log Activity with a WOR
                '            oForm.Items.Item("IDH_TABACT").AffectsFormMode = False 'Activity TAB
                '            oForm.Items.Item("IDH_TABACC").AffectsFormMode = False 'Accounting TAB

                '            ''OnTime 32355: WR1_NewDev_Buying&Trading_Add functionality to log Activity with a WOR
                '            ''Add new grid
                '            'oForm.Items.Item("IDH_TABACT").AffectsFormMode = False
                '            'oForm.Items.Item("IDH_TABACC").AffectsFormMode = False


                '            oForm.Items.Item("IDH_TABCOS").Click()

                '            'Set visible to false as per discussion with HN and CW and
                '            'moved Sch/UnSch checkboxes to Comments TAB
                '            oForm.Items.Item("IDH_TABCOV").Visible = False
                '            oForm.Items.Item("IDH_REMFLD").Visible = False

                '            oItem = oForm.Items.Item("IDH_ADDGRD")

                '            oForm.Items.Item("261").Visible = False
                '            oForm.Items.Item("IDH_PBI").Visible = False
                '            oForm.Items.Item("IDH_PBILK").Visible = False

                '            Dim owItem As SAPbouiCOM.Item
                '            owItem = WR1_Grids.idh.controls.grid.AdditionalExpenses.doAddToForm( _
                '                Me, oForm, "ADDGRID", _
                '                oItem.Left, _
                '                oItem.Top, _
                '                oItem.Width, _
                '                oItem.Height, _

                '                102, _
                '                102).getSBOItem()

                '            ''Add Activity Grid and Fill
                '            oItem = oForm.Items.Add("ACTGRID", SAPbouiCOM.BoFormItemTypes.it_GRID)
                '            'set grid position and from/to panes etc
                '            oItem.Left = owItem.Left
                '            oItem.Top = owItem.Top
                '            oItem.Width = owItem.Width
                '            oItem.Height = owItem.Height 'owItem.Height + 50
                '            oItem.FromPane = 103
                '            oItem.ToPane = 103

                '            Dim oGrid As SAPbouiCOM.Grid
                '            oGrid = oItem.Specific
                '            oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single
                '            oForm.DataSources.DataTables.Add("dtActivity")

                '            'KA -- START -- 20121025
                '            'New Comments field with extended length
                '            doAddFormDF(oForm, "IDH_EXTCM1", "U_ExtComm1")
                '            doAddFormDF(oForm, "IDH_EXTCM2", "U_ExtComm2")
                '            'KA -- END -- 20121025

                '            'TFS Required Field
                '            doAddFormDF(oForm, "IDH_TFSREQ", "U_TFSReq")
                '            doAddFormDF(oForm, "IDH_TFSNUM", "U_TFSNum")

                '            'Mitie Dev - 20130121
                '            doAddFormDF(oForm, "IDH_MAXINM", "U_MaximoNum")

                'LPV - BEGIN - 20150706 - Uncommented for the CBI release
                'USA Release
                'Set Documents button caption on Job Managers
                Dim oItm As SAPbouiCOM.Item
                Dim oBtn As SAPbouiCOM.Button
                'Dim oChk As SAPbouiCOM.CheckBox

                If Config.INSTANCE.getParameterAsBool("USAREL", False) Then
                    'Set Documents button label for USA release
                    Try
                        oItm = oForm.Items.Item("IDH_DOCS")
                        If oItm IsNot Nothing Then
                            oBtn = oItm.Specific
                            oBtn.Caption = Config.ParameterWithDefault("BTDOCCAP", "Documents")

                        End If
                    Catch ex As Exception

                    End Try

                    'Add Do Journals Checkbox to reset DoJournal status on WOR
                    'doAddUFCheck(oForm, "IDH_DOJRN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")

                    ''KA -- START -- 20121022
                    ''Add Containers, Manifest Qty and manifest UOM fields
                    'doAddFormDF(oForm, "IDH_CBICNT", "U_CBICont")
                    'doAddFormDF(oForm, "IDH_CBIQTY", "U_CBIManQty")
                    'doAddFormDF(oForm, "IDH_CBIUOM", "U_CBIManUOM")
                    ''KA -- END -- 20121022
                Else
                    oForm.Items.Item("IDH_DOJRN").Enabled = False
                    oForm.Items.Item("IDH_STAJRN").Visible = False
                    'KA -- START -- 20121022
                    'Labels
                    oForm.Items.Item("339").Visible = False 'Containers
                    oForm.Items.Item("341").Visible = False 'Manifest Qty
                    oForm.Items.Item("343").Visible = False 'Manifest UOM

                    'Controls
                    oForm.Items.Item("IDH_CBICNT").Visible = False
                    oForm.Items.Item("IDH_CBIQTY").Visible = False
                    oForm.Items.Item("IDH_CBIUOM").Visible = False
                    'KA -- END -- 20121022
                End If
                'LPV - END - 20150706

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
                BubbleEvent = False
            End Try
        End Sub

        '** Do the final form steps to show after loaddata
        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            ''Temp Code to get Disabled Items
            'Dim sEnabled As String = ""
            'For Each oItem As SAPbouiCOM.Item In oForm.Items
            '    If oItem.Enabled = True AndAlso Array.IndexOf(IDHAddOns.idh.lookups.Base.EDITITEMS, oItem.Type) >= 0 Then
            '        sEnabled = sEnabled & """" & oItem.UniqueID & ""","
            '    End If
            'Next

            MyBase.doFinalizeShow(oForm)
        End Sub

        Private Sub doEnableCoverage(ByVal oForm As SAPbouiCOM.Form, ByVal bEnable As Boolean)
            Dim sRemDate As String = getFormDFValue(oForm, "U_JobRmD")
            Dim sRemTime As String = getFormDFValue(oForm, "U_JobRmT")

            If sRemDate.Length > 0 Then 'OrElse sRemTime.Length > 0 OrElse sRemTime.Equals("00:00") Then
                Dim oItems() As String = {"IDH_2RD", "IDH_2RW", "IDH_2RM", "IDH_2RY", "IDH_2RO",
                                      "IDH_2RRA", "IDH_2RRB", "IDH_2FMO", "IDH_2FTU",
                                      "IDH_2FWE", "IDH_2FTH", "IDH_2FFR", "IDH_2FSA",
                                      "IDH_2FSU", "IDH_2FRE", "IDH_2RRS", "IDH_2RRVA",
                "IDH_2RRVB"}
                setEnableItems(oForm, bEnable, oItems)
            Else
                Dim oItems() As String = {"IDH_2RD", "IDH_2RW", "IDH_2RM", "IDH_2RY", "IDH_2RO",
                                          "IDH_2RRA", "IDH_2RRB", "IDH_2FMO", "IDH_2FTU",
                                          "IDH_2FWE", "IDH_2FTH", "IDH_2FFR", "IDH_2FSA",
                                          "IDH_2FSU", "IDH_2FRE", "IDH_2RRS", "IDH_2RRVA",
                "IDH_2RRVB", "IDH_REMPQ", "IDH_REMPT"}
                setEnableItems(oForm, bEnable, oItems)
            End If
        End Sub

        Protected Overridable Function thisWOR(ByVal oForm As SAPbouiCOM.Form) As IDH_JOBSHD
            Dim oWOR As IDH_JOBSHD = getWFValue(oForm, "WOR")
            If oWOR Is Nothing Then
                oWOR = createWOR(oForm)
            End If
            Return oWOR
        End Function
        Private Function createWOR(ByVal oForm As SAPbouiCOM.Form) As IDH_JOBSHD
            Return createWOR(oForm, Nothing)
        End Function
        Private Function createWOR(ByVal oForm As SAPbouiCOM.Form, ByVal oWO As IDH_JOBENTR) As IDH_JOBSHD
            Dim oWOR As IDH_JOBSHD = New IDH_JOBSHD(Nothing, oForm, oWO)
            oWOR.MustLoadChildren = False
            oWOR.SkipPriceLookup = Config.INSTANCE.SkipPriceLookup

            If oWOR.AdditionalExpenses IsNot Nothing Then
                oWOR.AdditionalExpenses.SBOFormGUIOnly = oForm
            End If

            oWOR.Handler_ChargeTotalsCalculated = New IDH_JOBSHD.et_ExternalTrigger(AddressOf doHandleChargeTotalsCalculated)

            oWOR.Handler_ChargePricesChanged = New IDH_JOBSHD.et_ExternalTrigger(AddressOf doHandleChargePricesChanged)
            oWOR.Handler_CostTippingPricesChanged = New IDH_JOBSHD.et_ExternalTrigger(AddressOf doHandleCostTippingPricesChanged)
            oWOR.Handler_CostHaulagePricesChanged = New IDH_JOBSHD.et_ExternalTrigger(AddressOf doHandleCostHaulagePricesChanged)
            oWOR.Handler_CostProducerPricesChanged = New IDH_JOBSHD.et_ExternalTrigger(AddressOf doHandleCostProducerPricesChanged)

            oWOR.Handler_CostTotalsCalculated = New IDH_JOBSHD.et_ExternalTrigger(AddressOf doHandleCostTotalsCalculated)
            oWOR.Handler_ExternalYNOption = New IDH_JOBSHD.et_ExternalYNOption(AddressOf doHandlerYNOption)

            oWOR.Handler_ExternalTriggerWithData_ShowMessage = New IDH_JOBSHD.et_ExternalTriggerWithData(AddressOf doHandlerWithData_ShowMessage)

            oWOR.Handler_ExternalAdditionalExpensesRebateWeightUpdater = New IDH_JOBSHD.et_ExternalTrigger(AddressOf doHandlerExternalAdditionalExpensesRebateWeightUpdater)
            oWOR.Handler_ExternalAdditionalExpensesRebateUOMUpdater = New IDH_JOBSHD.et_ExternalTrigger(AddressOf doHandlerExternalAdditionalExpensesRebateUOMUpdater)

            oWOR.Handler_ChargeTipPriceLookupSkipped = New IDH_JOBSHD.et_ExternalTriggerWithData(AddressOf doHandleChargeTipPriceLookupSkipped)
            oWOR.Handler_ChargeHaulagePriceLookupSkipped = New IDH_JOBSHD.et_ExternalTriggerWithData(AddressOf doHandleChargeHaulagePriceLookupSkipped)
            oWOR.Handler_CostTipPriceLookupSkipped = New IDH_JOBSHD.et_ExternalTriggerWithData(AddressOf doHandleCostTipPriceLookupSkipped)
            oWOR.Handler_CostHaulagePriceLookupSkipped = New IDH_JOBSHD.et_ExternalTriggerWithData(AddressOf doHandleCostHaulagePriceLookupSkipped)
            oWOR.Handler_CostProducerPriceLookupSkipped = New IDH_JOBSHD.et_ExternalTriggerWithData(AddressOf doHandleCostProducerPriceLookupSkipped)

            If oWOR.SkipPriceLookup Then
                setVisible(oForm, "IDH_CHRPL", True)
                setVisible(oForm, "IDH_CSTPL", True)
            End If

            doAddWF(oForm, "WOR", oWOR)
            Return oWOR
        End Function

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            'Dim oItem As SAPbouiCOM.Item
            Dim oOpt As SAPbouiCOM.OptionBtn

            oOpt = oForm.Items.Item("IDH_USERE").Specific
            oOpt.ValOn = "1"
            oOpt.ValOff = "0"
            oOpt.Selected = True
            doAddFormDF(oForm, "IDH_USERE", "U_UseWgt")

            oOpt = oForm.Items.Item("IDH_USEAU").Specific
            oOpt.ValOn = "2"
            oOpt.GroupWith("IDH_USERE")

            doResetCoverage(oForm)

            doUOMCombos(oForm)
            doBranchCombo(oForm)
            doFillObligatedCombo(oForm)

            FillCombos.TransactionCodeCombo(FillCombos.getCombo(oForm, "uBC_TRNCd"), Nothing, Nothing, "WO")

            '*** START LOOKING AT THE NEW OBJECTS ***
            'doAddWF(oForm, "DBOBJROW", New IDH_JOBSHD(

            '            doAddWF(oForm, "IDH_LastModePO", "DontKnow")

            'The Price Add Update Form
            'Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption = New WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption(Nothing, oForm, Nothing)
            'oOOForm.KeepInMemory = True
            'oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogOkReturn(AddressOf doAddUpdatePrices)
            'oOOForm.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogCancelReturn(AddressOf doCancelAddUpdatePrices)
            'doAddWF(oForm, "PRCUPD", oOOForm)

            Dim oWOR As IDH_JOBSHD = Nothing

            'Dim sTest As String = oRow.Code
            'Dim sCustomer As String = oRow.U_CustCd
            'Dim sWasteCode As String = oRow.U_WasCd

            doAddWF(oForm, "PREVUOM", Nothing)

            doAddWF(oForm, "STADDR", "")
            doAddWF(oForm, "ZIPCODE", "")
            doAddWF(oForm, "AUTOCOLLECT", 0)
            doAddWF(oForm, "DEFWEI", 0)
            doAddWF(oForm, "LastVehAct", "DOSEARCH")
            'doAddWF(oForm, "VehicleType", "S")
            doAddWF(oForm, "OpenFrom", "DN")
            doAddWF(oForm, "DOADDLIC", False)
            doAddWF(oForm, "FORMACTION", "DN")
            doAddWF(oForm, "LOCKCNT", False)
            doAddWF(oForm, "CIPUOM", Nothing)
            doAddWF(oForm, "BUYFROM", Config.INSTANCE.doGetDoBuyingAndTrade())
            doAddWF(oForm, "INSERTADDEXP", False)
            oForm.PaneLevel = 101

            If Config.INSTANCE.getParameterAsBool("PBIENABL", True) Then
                oForm.Items.Item("IDH_PBIDTL").Visible = True
            Else
                oForm.Items.Item("IDH_PBIDTL").Visible = False
            End If

            Dim sCode As String = Nothing
            setWFValue(oForm, "ISNEW", False)
            Dim sExtra As String = Nothing
            Dim sContract As String = ""
            Dim sRowSta As String

            If ghOldDialogParams.Contains(oForm.UniqueID) Then
                Dim oData As ArrayList = ghOldDialogParams.Item(oForm.UniqueID)
                If Not (oData Is Nothing) Then

                    With getFormMainDataSource(oForm)
                        If oData.Count > 1 Then
                            sExtra = oData.Item(0)
                            setWFValue(oForm, "FORMACTION", sExtra)

                            '**************************************************************
                            '** Use this if a Grid Object is passed through as a parameter
                            '** USED FROM:
                            '** idh.forms.orders.Waste -> doCustomItemEvent (+- 888)
                            '** idh.forms.orders.Waste -> doCustomItemEvent (+- 920)
                            '** idh.forms.orders.Waste -> doDuplicateWORow (+- 1056)
                            '**
                            '** PARAMETERS SENT:
                            '** 0 -> PARAMETER SWITCH - here it should be IDHGRID
                            '** 1 -> Selected Grid Row Number
                            '** 2 -> IDH Grid Object
                            '** 3 -> Hashtable containing Extra Parameters
                            '**      (From idh.forms.orders.Waste)  -> 	0 - U_BDate
                            '**											1 -> U_BTime
                            '**											2 -> U_ZpCd
                            '**											3 -> U_Address
                            '**											4 -> U_SiteTl
                            '**											5 -> RowData
                            '**											6 -> U_SteId
                            '**											7 -> U_PZpCd
                            '**											8 -> U_PAddress
                            '**											9 -> U_PPhone1
                            '**											10 -> U_FirstBP
                            '** 4 -> Action Switch [DOADD, DOCHANGE]
                            '**************************************************************
                            If sExtra = "IDHGRID" Then
                                oForm.Items.Item("IDH_TABCOV").AffectsFormMode = False
                                Dim iRow As Integer = oData.Item(1)
                                Dim oGridN As OrderRowGrid = oData.Item(2)
                                Dim oHead As Hashtable = oData.Item(3)
                                Dim sDoAdd As String = oData.Item(4)
                                If sDoAdd.Equals("DOADD") Then
                                    setWFValue(oForm, "ISNEW", True)
                                End If

                                oWOR = createWOR(oForm, oHead.Item("WasteOrder"))

                                oGridN.setCurrentDataRowIndex(iRow)
                                Dim sWCode As String = oGridN.doGetFieldValue("U_JobNr")
                                doAddWF(oForm, "JOBENTRIES", oGridN.JobEntries)
                                oWOR.BlockChangeNotif = True
                                Try
                                    .SetValue("U_JobNr", .Offset, sWCode) 'oGridN.doGetFieldValue("U_JobNr"))
                                    Dim sJob As String = oWOR.U_JobNr

                                    .SetValue(IDH_JOBSHD._LckPrc, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._LckPrc))

                                    .SetValue("Code", .Offset, oGridN.doGetFieldValue("Code"))
                                    .SetValue("Name", .Offset, oGridN.doGetFieldValue("Name"))

                                    '.SetValue("U_Status", .Offset, oGridN.doGetFieldValue("U_Status"))
                                    '.SetValue("U_RowSta", .Offset, oGridN.doGetFieldValue("U_RowSta"))
                                    '.SetValue("U_PStat", .Offset, oGridN.doGetFieldValue("U_PStat"))
                                    '.SetValue("U_JobTp", .Offset, oGridN.doGetFieldValue("U_JobTp"))

                                    .SetValue("U_WROrd", .Offset, oGridN.doGetFieldValue("U_WROrd"))
                                    .SetValue("U_WRRow", .Offset, oGridN.doGetFieldValue("U_WRRow"))

                                    Dim oVal As Object
                                    oVal = goParent.doDateToStr(oGridN.doGetFieldValue("U_RDate"), True)
                                    .SetValue("U_RDate", .Offset, oVal)

                                    oVal = oGridN.doGetFieldValue("U_RTime")
                                    .SetValue("U_RTime", .Offset, oVal)

                                    oVal = oGridN.doGetFieldValue("U_RTimeT")
                                    .SetValue("U_RTimeT", .Offset, oVal)

                                    oVal = goParent.doDateToStr(oGridN.doGetFieldValue("U_ASDate"), True)
                                    .SetValue("U_ASDate", .Offset, oVal)

                                    oVal = oGridN.doGetFieldValue("U_ASTime")
                                    .SetValue("U_ASTime", .Offset, oVal)

                                    oVal = goParent.doDateToStr(oGridN.doGetFieldValue("U_AEDate"), True)
                                    .SetValue("U_AEDate", .Offset, oVal)

                                    oVal = oGridN.doGetFieldValue("U_AETime")
                                    .SetValue("U_AETime", .Offset, oVal)

                                    oVal = goParent.doDateToStr(oGridN.doGetFieldValue("U_SLicExp"), True)
                                    .SetValue("U_SLicExp", .Offset, oVal)

                                    oVal = goParent.doDateToStr(oGridN.doGetFieldValue("U_JobRmD"), True)
                                    .SetValue("U_JobRmD", .Offset, oVal)

                                    .SetValue("U_JobRmT", .Offset, oGridN.doGetFieldValue("U_JobRmT"))
                                    .SetValue("U_RemNot", .Offset, oGridN.doGetFieldValue("U_RemNot"))
                                    .SetValue("U_RemCnt", .Offset, oGridN.doGetFieldValue("U_RemCnt"))

                                    .SetValue("U_ItmGrp", .Offset, oGridN.doGetFieldValue("U_ItmGrp"))
                                    .SetValue("U_ItemCd", .Offset, oGridN.doGetFieldValue("U_ItemCd"))
                                    .SetValue("U_ItemDsc", .Offset, oGridN.doGetFieldValue("U_ItemDsc"))

                                    'BP Information
                                    .SetValue("U_Tip", .Offset, oGridN.doGetFieldValue("U_Tip"))
                                    .SetValue(IDH_JOBSHD._SAddress, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._SAddress))
                                    .SetValue(IDH_JOBSHD._SAddrsLN, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._SAddrsLN))
                                    .SetValue("U_CustCd", .Offset, oGridN.doGetFieldValue("U_CustCd"))
                                    .SetValue("U_CarrCd", .Offset, oGridN.doGetFieldValue("U_CarrCd"))
                                    .SetValue("U_ProCd", .Offset, oGridN.doGetFieldValue("U_ProCd"))

                                    .SetValue("U_TipNm", .Offset, oGridN.doGetFieldValue("U_TipNm"))
                                    .SetValue("U_CustNm", .Offset, oGridN.doGetFieldValue("U_CustNm"))
                                    .SetValue("U_CarrNm", .Offset, oGridN.doGetFieldValue("U_CarrNm"))
                                    .SetValue("U_ProNm", .Offset, oGridN.doGetFieldValue("U_ProNm"))

                                    .SetValue("U_Serial", .Offset, oGridN.doGetFieldValue("U_Serial"))
                                    .SetValue("U_SLicNr", .Offset, oGridN.doGetFieldValue("U_SLicNr"))
                                    .SetValue("U_Lorry", .Offset, oGridN.doGetFieldValue("U_Lorry"))
                                    .SetValue("U_Driver", .Offset, oGridN.doGetFieldValue("U_Driver"))

                                    .SetValue(IDH_JOBSHD._TRdWgt, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._TRdWgt))
                                    .SetValue(IDH_JOBSHD._TipWgt, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._TipWgt))

                                    .SetValue("U_RdWgt", .Offset, oGridN.doGetFieldValue("U_RdWgt"))
                                    .SetValue("U_CstWgt", .Offset, oGridN.doGetFieldValue("U_CstWgt"))

                                    .SetValue("U_ExpLdWgt", .Offset, oGridN.doGetFieldValue("U_ExpLdWgt"))
                                    '## Start 17-09-2013
                                    '.SetValue("U_UseWgt", .Offset, oGridN.doGetFieldValue("U_UseWgt"))
                                    '## End
                                    .SetValue("U_AUOMQt", .Offset, oGridN.doGetFieldValue("U_AUOMQt"))
                                    .SetValue("U_AUOM", .Offset, oGridN.doGetFieldValue("U_AUOM"))
                                    '.SetValue("U_JobNr", .Offset, oGridN.doGetFieldValue("U_JobNr"))
                                    .SetValue("U_TipCost", .Offset, oGridN.doGetFieldValue("U_TipCost"))
                                    .SetValue("U_TipTot", .Offset, oGridN.doGetFieldValue("U_TipTot"))
                                    ''MA 03-04-2014 Start
                                    .SetValue("U_TAddCost", .Offset, oGridN.doGetFieldValue("U_TAddCost"))
                                    .SetValue("U_TAddChrg", .Offset, oGridN.doGetFieldValue("U_TAddChrg"))

                                    .SetValue("U_TAddChVat", .Offset, oGridN.doGetFieldValue("U_TAddChVat"))
                                    .SetValue("U_TAddCsVat", .Offset, oGridN.doGetFieldValue("U_TAddCsVat"))

                                    .SetValue("U_AddCharge", .Offset, oGridN.doGetFieldValue("U_AddCharge"))
                                    .SetValue("U_AddCost", .Offset, oGridN.doGetFieldValue("U_AddCost"))
                                    ''MA 03-04-2014 End
                                    .SetValue(IDH_JOBSHD._WgtDed, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._WgtDed))
                                    .SetValue(IDH_JOBSHD._ValDed, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._ValDed))
                                    .SetValue("U_TCharge", .Offset, oGridN.doGetFieldValue("U_TCharge"))
                                    .SetValue("U_TCTotal", .Offset, oGridN.doGetFieldValue("U_TCTotal"))
                                    .SetValue("U_CongCh", .Offset, oGridN.doGetFieldValue("U_CongCh"))
                                    .SetValue("U_DocNum", .Offset, oGridN.doGetFieldValue("U_DocNum"))
                                    .SetValue("U_Price", .Offset, oGridN.doGetFieldValue("U_Price"))
                                    .SetValue("U_Discnt", .Offset, oGridN.doGetFieldValue("U_Discnt"))
                                    .SetValue("U_Total", .Offset, oGridN.doGetFieldValue("U_Total"))
                                    .SetValue("U_SLicCh", .Offset, oGridN.doGetFieldValue("U_SLicCh"))
                                    .SetValue("U_SLicSp", .Offset, oGridN.doGetFieldValue("U_SLicSp"))
                                    .SetValue("U_VehTyp", .Offset, oGridN.doGetFieldValue("U_VehTyp"))
                                    .SetValue("U_DisAmt", .Offset, oGridN.doGetFieldValue("U_DisAmt"))

                                    .SetValue("U_TAUOMQt", .Offset, oGridN.doGetFieldValue("U_TAUOMQt"))
                                    .SetValue("U_PAUOMQt", .Offset, oGridN.doGetFieldValue("U_PAUOMQt"))
                                    .SetValue("U_PRdWgt", .Offset, oGridN.doGetFieldValue("U_PRdWgt"))
                                    .SetValue("U_ProWgt", .Offset, oGridN.doGetFieldValue("U_ProWgt"))
                                    .SetValue("U_ProUOM", .Offset, oGridN.doGetFieldValue("U_ProUOM"))
                                    .SetValue("U_PCost", .Offset, oGridN.doGetFieldValue("U_PCost"))
                                    .SetValue("U_PCTotal", .Offset, oGridN.doGetFieldValue("U_PCTotal"))

                                    .SetValue("U_WasCd", .Offset, oGridN.doGetFieldValue("U_WasCd"))
                                    ''MA Start 10-09-2015 Issue#909; it was also a bug in WOR, that it was not loading the saved alternate description
                                    .SetValue(IDH_JOBSHD._AltWasDsc, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._AltWasDsc))
                                    ''MA End 10-09-2015 Issue#909
                                    .SetValue(IDH_JOBSHD._BookIn, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._BookIn))
                                    'Fix
                                    'This was setting the value of field to false
                                    '.SetValue("U_WasDsc", .Offset, oGridN.doGetFieldValue("U_WasDsc"))
                                    If oGridN.doGetFieldValue("U_WasDsc").ToString().ToLower() = "false" Then
                                        .SetValue("U_WasDsc", .Offset, "")
                                    Else
                                        .SetValue("U_WasDsc", .Offset, oGridN.doGetFieldValue("U_WasDsc"))
                                    End If

                                    .SetValue("U_Weight", .Offset, oGridN.doGetFieldValue("U_Weight"))
                                    .SetValue("U_Quantity", .Offset, oGridN.doGetFieldValue("U_Quantity"))
                                    .SetValue("U_JCost", .Offset, oGridN.doGetFieldValue("U_JCost"))
                                    .SetValue("U_SAINV", .Offset, oGridN.doGetFieldValue("U_SAINV"))
                                    .SetValue("U_SAORD", .Offset, oGridN.doGetFieldValue("U_SAORD"))
                                    .SetValue("U_TIPPO", .Offset, oGridN.doGetFieldValue("U_TIPPO"))
                                    .SetValue("U_JOBPO", .Offset, oGridN.doGetFieldValue("U_JOBPO"))
                                    .SetValue("U_ProPO", .Offset, oGridN.doGetFieldValue("U_ProPO"))
                                    .SetValue("U_GRIn", .Offset, oGridN.doGetFieldValue("U_GRIn"))
                                    .SetValue("U_ProGRPO", .Offset, oGridN.doGetFieldValue("U_ProGRPO"))
                                    .SetValue("U_SODlvNot", .Offset, oGridN.doGetFieldValue("U_SODlvNot"))
                                    .SetValue(IDH_DISPROW._WHTRNF, .Offset, oGridN.doGetFieldValue(IDH_DISPROW._WHTRNF))

                                    'CR 20150601: The DO Journals functionality build for USA Clients can now be used for all under new WR Config Key "NEWDOJRN" 
                                    'If Config.INSTANCE.getParameterAsBool("USAREL", False) Then
                                    If Config.INSTANCE.getParameterAsBool("NEWDOJRN", False) Then
                                        .SetValue("U_Jrnl", .Offset, oGridN.doGetFieldValue("U_Jrnl"))
                                        .SetValue("U_JStat", .Offset, oGridN.doGetFieldValue("U_JStat"))
                                    End If

                                    .SetValue("U_TPCN", .Offset, oGridN.doGetFieldValue("U_TPCN"))
                                    .SetValue("U_TCCN", .Offset, oGridN.doGetFieldValue("U_TCCN"))

                                    .SetValue("U_Comment", .Offset, oGridN.doGetFieldValue("U_Comment"))
                                    .SetValue("U_IntComm", .Offset, oGridN.doGetFieldValue("U_IntComm"))
                                    .SetValue("U_ExtComm1", .Offset, oGridN.doGetFieldValue("U_ExtComm1"))
                                    .SetValue("U_ExtComm2", .Offset, oGridN.doGetFieldValue("U_ExtComm2"))
                                    .SetValue("U_OTComments", .Offset, oGridN.doGetFieldValue("U_OTComments"))

                                    .SetValue("U_MaximoNum", .Offset, oGridN.doGetFieldValue("U_MaximoNum"))

                                    'Analysis Code - 20150408
                                    .SetValue("U_Analys1", .Offset, oGridN.doGetFieldValue("U_Analys1"))
                                    .SetValue("U_Analys2", .Offset, oGridN.doGetFieldValue("U_Analys2"))
                                    .SetValue("U_Analys3", .Offset, oGridN.doGetFieldValue("U_Analys3"))
                                    .SetValue("U_Analys4", .Offset, oGridN.doGetFieldValue("U_Analys4"))
                                    .SetValue("U_Analys5", .Offset, oGridN.doGetFieldValue("U_Analys5"))

                                    'Multi Currency Pricing 
                                    .SetValue(IDH_JOBSHD._DispCgCc, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._DispCgCc))
                                    .SetValue(IDH_JOBSHD._CarrCgCc, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._CarrCgCc))
                                    .SetValue(IDH_JOBSHD._PurcCoCc, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._PurcCoCc))
                                    .SetValue(IDH_JOBSHD._DispCoCc, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._DispCoCc))
                                    .SetValue(IDH_JOBSHD._CarrCoCc, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._CarrCoCc))
                                    .SetValue(IDH_JOBSHD._LiscCoCc, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._LiscCoCc))

                                    .SetValue("U_TFSReq", .Offset, oGridN.doGetFieldValue("U_TFSReq"))
                                    .SetValue("U_TFSNum", .Offset, oGridN.doGetFieldValue("U_TFSNum"))

                                    .SetValue("U_EVNReq", .Offset, oGridN.doGetFieldValue("U_EVNReq"))
                                    .SetValue("U_EVNNum", .Offset, oGridN.doGetFieldValue("U_EVNNum"))

                                    Dim sCustRef As String
                                    sCustRef = oGridN.doGetFieldValue("U_CustRef")
                                    .SetValue("U_CustRef", .Offset, sCustRef)
                                    If Not String.IsNullOrEmpty(sCustRef) AndAlso Config.Parameter("MDREAE").ToUpper().Equals("FALSE") Then
                                        doSetEnabled(oForm.Items.Item("IDH_CUSREF"), False)
                                    Else
                                        doSetEnabled(oForm.Items.Item("IDH_CUSREF"), True)
                                    End If

                                    .SetValue("U_CongCd", .Offset, oGridN.doGetFieldValue("U_CongCd"))
                                    .SetValue("U_SLicNm", .Offset, oGridN.doGetFieldValue("U_SLicNm"))
                                    .SetValue("U_SCngNm", .Offset, oGridN.doGetFieldValue("U_SCngNm"))
                                    .SetValue("U_Dista", .Offset, oGridN.doGetFieldValue("U_Dista"))

                                    .SetValue("U_SupRef", .Offset, oGridN.doGetFieldValue("U_SupRef"))
                                    .SetValue("U_ProRef", .Offset, oGridN.doGetFieldValue("U_ProRef"))
                                    .SetValue("U_SiteRef", .Offset, oGridN.doGetFieldValue("U_SiteRef"))

                                    .SetValue("U_OrdWgt", .Offset, oGridN.doGetFieldValue("U_OrdWgt"))
                                    .SetValue(IDH_JOBSHD._CarWgt, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._CarWgt))
                                    .SetValue("U_OrdCost", .Offset, oGridN.doGetFieldValue("U_OrdCost"))
                                    .SetValue("U_OrdTot", .Offset, oGridN.doGetFieldValue("U_OrdTot"))

                                    Dim sVal As String
                                    sVal = oGridN.doGetFieldValue("U_WROrd")
                                    .SetValue("U_WROrd", .Offset, sVal)
                                    sVal = oGridN.doGetFieldValue("U_WRRow")
                                    .SetValue("U_WRRow", .Offset, sVal)

                                    .SetValue("U_CusQty", .Offset, oGridN.doGetFieldValue("U_CusQty"))
                                    .SetValue("U_HlSQty", .Offset, oGridN.doGetFieldValue("U_HlSQty"))
                                    .SetValue("U_CusChr", .Offset, oGridN.doGetFieldValue("U_CusChr"))
                                    .SetValue("U_UOM", .Offset, oGridN.doGetFieldValue("U_UOM"))
                                    '## Start 17-09-2013
                                    If Config.INSTANCE.doGetBaseWeightFactors(oGridN.doGetFieldValue("U_UOM")) < 0 Then
                                        .SetValue("U_UseWgt", .Offset, 2) 'Set Alternative UOM Radio Button if UOM Factor is <0 (or is -1)
                                    Else
                                        .SetValue("U_UseWgt", .Offset, oGridN.doGetFieldValue("U_UseWgt")) 'Set Radion Button whatever received from WOH
                                    End If
                                    '## End
                                    .SetValue("U_PUOM", .Offset, oGridN.doGetFieldValue("U_PUOM"))

                                    Dim dLicCost As Double = oGridN.doGetFieldValue("U_SLicCst")
                                    Dim dLicTotal As Double = oGridN.doGetFieldValue("U_SLicCTo")
                                    Dim dLicQty As Double = oGridN.doGetFieldValue("U_SLicCQt")
                                    If dLicQty = 0 Then
                                        dLicQty = 1
                                    End If

                                    .SetValue("U_TaxAmt", .Offset, oGridN.doGetFieldValue("U_TaxAmt"))
                                    .SetValue("U_VtCostAmt", .Offset, oGridN.doGetFieldValue("U_VtCostAmt"))

                                    .SetValue("U_TChrgVtRt", .Offset, oGridN.doGetFieldValue("U_TChrgVtRt"))
                                    .SetValue("U_HChrgVtRt", .Offset, oGridN.doGetFieldValue("U_HChrgVtRt"))
                                    .SetValue("U_TCostVtRt", .Offset, oGridN.doGetFieldValue("U_TCostVtRt"))
                                    .SetValue("U_PCostVtRt", .Offset, oGridN.doGetFieldValue("U_PCostVtRt"))
                                    .SetValue("U_HCostVtRt", .Offset, oGridN.doGetFieldValue("U_HCostVtRt"))

                                    .SetValue("U_TChrgVtGrp", .Offset, oGridN.doGetFieldValue("U_TChrgVtGrp"))
                                    .SetValue("U_HChrgVtGrp", .Offset, oGridN.doGetFieldValue("U_HChrgVtGrp"))
                                    .SetValue("U_TCostVtGrp", .Offset, oGridN.doGetFieldValue("U_TCostVtGrp"))
                                    .SetValue("U_PCostVtGrp", .Offset, oGridN.doGetFieldValue("U_PCostVtGrp"))
                                    .SetValue("U_HCostVtGrp", .Offset, oGridN.doGetFieldValue("U_HCostVtGrp"))

                                    .SetValue(IDH_JOBSHD._LckPrc, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._LckPrc))
                                    If dLicTotal = 0 Then
                                        If dLicCost <> 0 Then
                                            dLicTotal = dLicCost * dLicQty
                                        End If
                                    Else
                                        If dLicTotal <> 0 Then
                                            dLicCost = dLicTotal / dLicQty
                                        End If
                                    End If
                                    .SetValue("U_SLicCQt", .Offset, dLicQty)
                                    .SetValue("U_SLicCst", .Offset, dLicCost)
                                    .SetValue("U_SLicCTo", .Offset, dLicTotal)

                                    oVal = goParent.doDateToStr(oGridN.doGetFieldValue("U_BDate"), True)
                                    .SetValue("U_BDate", .Offset, oVal)

                                    oVal = oGridN.doGetFieldValue("U_BTime")
                                    .SetValue("U_BTime", .Offset, oVal)

                                    oVal = goParent.doDateToStr(oGridN.doGetFieldValue("U_JbToBeDoneDt"), True)
                                    .SetValue("U_JbToBeDoneDt", .Offset, oVal)

                                    oVal = oGridN.doGetFieldValue("U_JbToBeDoneTm")
                                    .SetValue("U_JbToBeDoneTm", .Offset, oVal)

                                    sContract = oGridN.doGetFieldValue("U_CntrNo")
                                    .SetValue("U_CntrNo", .Offset, sContract)

                                    doSetValuesFromHeader(oForm)

                                    'If Config.INSTANCE.doGetDoBuyingAndTrade() AndAlso oWOR.ParentOrder.U_FirstBP.Equals("Producer") Then
                                    '    setUFValue(oForm, "IDH_ZP", oWOR.ParentOrder.U_PZpCd)
                                    '    setUFValue(oForm, "IDH_ADDR", oWOR.ParentOrder.U_PAddress)
                                    '    setUFValue(oForm, "IDH_SITETL", oWOR.ParentOrder.U_PPhone1)
                                    'Else
                                    '    setUFValue(oForm, "IDH_ZP", oWOR.ParentOrder.U_ZpCd)
                                    '    setUFValue(oForm, "IDH_ADDR", oWOR.ParentOrder.U_Address)
                                    '    setUFValue(oForm, "IDH_SITETL", oWOR.ParentOrder.U_SiteTl)
                                    'End If

                                    'setWFValue(oForm, "ZIPCODE", oWOR.ParentOrder.U_ZpCd)
                                    'setWFValue(oForm, "STADDR", oWOR.ParentOrder.U_Address)

                                    'setWFValue(oForm, "FORECS", oHead.Item("FORECS"))

                                    'setUFValue(oForm, "IDH_SteId", oWOR.ParentOrder.U_SteId)
                                    'setUFValue(oForm, "IDH_PAYTRM", getPaymentTerms(oForm, getFormDFValue(oForm, "U_CustCd"))) 'Payment Term

                                    setWFValue(oForm, "JobAdditionalHandler", oHead.Item("JobAdditionalHandler"))

                                    setWFValue(oForm, "CALLEDFROMFC", oHead.Item("CALLEDFROMFC"))
                                    setWFValue(oForm, "FCEXPWGT", oHead.Item("FCEXPWGT"))
                                    setWFValue(oForm, "DOSIP", oHead.Item("DOSIP"))

                                    'setWFValue(oForm, "TIPCHRG", oHead.Item("TIPCHRG"))
                                    'setWFValue(oForm, "TIPCST", oHead.Item("TIPCST"))
                                    'setWFValue(oForm, "HAULCHRG", oHead.Item("HAULCHRG"))
                                    'setWFValue(oForm, "HAULCST", oHead.Item("HAULCST"))
                                    'setWFValue(oForm, "PRODCST", oHead.Item("PRODCST"))

                                    'Set TFS Fields visible status
                                    'WOH Status Codes to set TFS fields enable on WOR. 7 = Approved

                                    'Another clause to set visibility depending if Customer and Disposal Country are same
                                    Dim sWOHStatus As String = oHead.Item("WOHSTATUS")

                                    Dim sTFSReqArray As String = Config.ParameterWithDefault("TFSREQEN", "7")
                                    Dim aTRSplit As ArrayList = New ArrayList(sTFSReqArray.Split(","))

                                    If Config.ParameterWithDefault("TFSMOD", False).ToString() = "TFSMOD_TRUE" AndAlso sWOHStatus.Length > 0 AndAlso
                                    aTRSplit.Contains(sWOHStatus) AndAlso (getFormDFValue(oForm, "U_TFSReq").ToString().Length > 0 OrElse hasDifferentCountry(oForm)) Then
                                        'TFS Module
                                        oForm.Items.Item("IDH_TFSREQ").Visible = True
                                        .SetValue("U_TFSReq", .Offset, "Y")
                                        oForm.Items.Item("352").Visible = True
                                        oForm.Items.Item("IDH_TFSLK").Visible = True
                                        oForm.Items.Item("IDH_TFSNUM").Visible = True
                                        oForm.Items.Item("IDH_TFSSC").Visible = True
                                    Else
                                        oForm.Items.Item("IDH_TFSREQ").Visible = False
                                        oForm.Items.Item("352").Visible = False
                                        oForm.Items.Item("IDH_TFSLK").Visible = False
                                        oForm.Items.Item("IDH_TFSNUM").Visible = False
                                        oForm.Items.Item("IDH_TFSSC").Visible = False
                                    End If

                                    'EVN Requirement 
                                    If getFormDFValue(oForm, "U_EVNReq").ToString().Equals("Y") Then
                                        oForm.Items.Item("IDH_EVNREQ").Visible = True
                                        '.SetValue("U_EVNReq", .Offset, "Y")
                                        oForm.Items.Item("1000018").Visible = True
                                        oForm.Items.Item("IDH_EVNLK").Visible = True
                                        oForm.Items.Item("IDH_EVNNUM").Visible = True
                                        oForm.Items.Item("IDH_EVNSC").Visible = True
                                    Else
                                        oForm.Items.Item("IDH_EVNREQ").Visible = True
                                        oForm.Items.Item("1000018").Visible = False
                                        oForm.Items.Item("IDH_EVNLK").Visible = False
                                        oForm.Items.Item("IDH_EVNNUM").Visible = False
                                        oForm.Items.Item("IDH_EVNSC").Visible = False
                                    End If

                                    .SetValue("U_RowSta", .Offset, oGridN.doGetFieldValue("U_RowSta"))
                                    sRowSta = .GetValue("U_RowSta", .Offset)

                                    Dim oPreselect As PreSelect = oHead.Item("PRESELECT")
                                    'Dim sTrnCode As String = oGridN.doGetFieldValue(IDH_JOBSHD._TrnCode)
                                    If oPreselect IsNot Nothing Then
                                        setWFValue(oForm, "PRESELECT", oHead.Item("PRESELECT"))
                                        '.SetValue(IDH_JOBSHD._TrnCode, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._TrnCode))

                                        'If oWOR.TRNCdDecodeRecord IsNot Nothing Then
                                        '.SetValue(IDH_JOBSHD._JobTp, .Offset, oWOR.TRNCdDecodeRecord.JobType)
                                        'End If
                                        'Else
                                        '    .SetValue("U_Status", .Offset, oGridN.doGetFieldValue("U_Status"))
                                        '    .SetValue("U_PStat", .Offset, oGridN.doGetFieldValue("U_PStat"))

                                        '    .SetValue("U_JobTp", .Offset, oGridN.doGetFieldValue("U_JobTp"))
                                    End If

                                    If sDoAdd.Equals("DOADD") = False Then
                                        oWOR.U_TrnCode = oGridN.doGetFieldValue(IDH_JOBSHD._TrnCode)
                                        oWOR.U_Status = oGridN.doGetFieldValue("U_Status")
                                        oWOR.U_PStat = oGridN.doGetFieldValue("U_PStat")
                                        oWOR.U_JobTp = oGridN.doGetFieldValue("U_JobTp")
                                    End If

                                    Dim sSOStatus As String = .GetValue("U_Status", .Offset)
                                    Dim sPStatus As String = .GetValue("U_PStat", .Offset)
                                    If String.IsNullOrWhiteSpace(sRowSta) Then
                                        .SetValue("U_RowSta", .Offset, com.idh.bridge.lookups.FixedValues.getStatusOpen())
                                    End If

                                    If String.IsNullOrWhiteSpace(sSOStatus) Then
                                        .SetValue("U_Status", .Offset, com.idh.bridge.lookups.FixedValues.getStatusOpen())
                                    End If

                                    If String.IsNullOrWhiteSpace(sPStatus) Then
                                        .SetValue("U_PStat", .Offset, com.idh.bridge.lookups.FixedValues.getStatusOpen())
                                    End If

                                    'Payment Options
                                    .SetValue("U_PayMeth", .Offset, oGridN.doGetFieldValue("U_PayMeth"))
                                    .SetValue("U_PayStat", .Offset, oGridN.doGetFieldValue("U_PayStat"))
                                    .SetValue("U_CCNum", .Offset, oGridN.doGetFieldValue("U_CCNum"))
                                    .SetValue("U_CCType", .Offset, oGridN.doGetFieldValue("U_CCType"))
                                    .SetValue("U_CCStat", .Offset, oGridN.doGetFieldValue("U_CCStat"))

                                    .SetValue("U_Covera", .Offset, oGridN.doGetFieldValue("U_Covera"))
                                    .SetValue("U_CoverHst", .Offset, oGridN.doGetFieldValue("U_CoverHst"))

                                    .SetValue("U_Obligated", .Offset, oGridN.doGetFieldValue("U_Obligated"))
                                    .SetValue("U_Origin", .Offset, oGridN.doGetFieldValue("U_Origin"))
                                    .SetValue("U_User", .Offset, oGridN.doGetFieldValue("U_User"))
                                    .SetValue("U_Branch", .Offset, oGridN.doGetFieldValue("U_Branch"))
                                    .SetValue("U_CustCs", .Offset, oGridN.doGetFieldValue("U_CustCs"))

                                    .SetValue("U_ConNum", .Offset, oGridN.doGetFieldValue("U_ConNum"))
                                    .SetValue("U_MANPRC", .Offset, oGridN.doGetFieldValue("U_MANPRC"))

                                    .SetValue("U_LnkPBI", .Offset, oGridN.doGetFieldValue("U_LnkPBI"))

                                    .SetValue("U_Sched", .Offset, oGridN.doGetFieldValue("U_Sched"))
                                    .SetValue("U_USched", .Offset, oGridN.doGetFieldValue("U_USched"))

                                    .SetValue("U_HaulAC", .Offset, oGridN.doGetFieldValue("U_HaulAC"))
                                    .SetValue("U_IsTrl", .Offset, oGridN.doGetFieldValue("U_IsTrl"))

                                    'doCalcProfit(oForm)
                                    doEnableCoverage(oForm, True)

                                    Dim sRebate As String
                                    sRebate = oGridN.doGetFieldValue("U_CarrReb")
                                    setFormDFValue(oForm, "U_CarrReb", sRebate)
                                    sRebate = oGridN.doGetFieldValue("U_CustReb")
                                    setFormDFValue(oForm, "U_CustReb", sRebate)

                                    If Config.INSTANCE.getParameterAsBool("USAREL", False) Then
                                        .SetValue("U_CBICont", .Offset, oGridN.doGetFieldValue("U_CBICont"))
                                        .SetValue("U_CBIManQty", .Offset, oGridN.doGetFieldValue("U_CBIManQty"))
                                        .SetValue("U_CBIManUOM", .Offset, oGridN.doGetFieldValue("U_CBIManUOM"))
                                    End If

                                    'Set Work Field value for Credit Checking
                                    setWFValue(oForm, "CCTotal", oGridN.doGetFieldValue("U_Total"))
                                    .SetValue(IDH_JOBSHD._RouteCd, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._RouteCd))
                                    .SetValue(IDH_JOBSHD._RtCdCode, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._RtCdCode))
                                    .SetValue(IDH_JOBSHD._Sample, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._Sample))
                                    .SetValue(IDH_JOBSHD._SampleRef, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._SampleRef))
                                    .SetValue(IDH_JOBSHD._SampStatus, .Offset, oGridN.doGetFieldValue(IDH_JOBSHD._SampStatus))

                                Finally
                                    oWOR.BlockChangeNotif = False
                                End Try

                                'This will trigger the BO logic
                                If sDoAdd.Equals("DOADD") Then
                                    oWOR.U_TrnCode = oGridN.doGetFieldValue(IDH_JOBSHD._TrnCode)
                                End If
                            Else
                                sCode = oData.Item(1)

                                If Not String.IsNullOrEmpty(sCode) Then
                                    If oData.Count = 2 Then
                                        '**************************************************************
                                        '** When only the Waste Order Row Code is known, the rest of
                                        '** the data needs to be retrieved using a query for the Row
                                        '** and a query for the Address
                                        '**
                                        '** USED FROM:
                                        '** idh.forms.PreBookInstruction -> doItemEvent (+- 593)
                                        '** idh.forms.SBO.Activity -> doItemEvent (+- 65)
                                        '** idh.forms.SBO.Sales -> doItemEvent (+- 45)
                                        '**
                                        '** PARAMETERS SENT:
                                        '** 0 -> PARAMETER SWITCH - currently iccnored [IDHWO/DoUpdate/DoView]
                                        '** 1 -> Waste Order Row Number
                                        '**************************************************************
                                        doLoadRowFromCode(oForm, sCode)
                                        oWOR = thisWOR(oForm)
                                    ElseIf oData.Count = 15 Then 'NOTE: Keep the counter updated for no. of values added in oData Array in manager.Templ.vb class file--> doCustomerItemEvent(...) 
                                        '*************************************************************************************
                                        '** Called mostly from the Managers
                                        '**
                                        '** USED FROM:
                                        '** WR1_Managers.idh.forms.manager.DeLink -> doGridDoubleClick (+- 173)
                                        '** WR1_Managers.idh.forms.manager.Licences -> doGridDoubleClick (+- 274)
                                        '** WR1_Managers.idh.forms.manager.Licences -> doCustomItemEvent (+- 411)
                                        '** WR1_Managers.idh.forms.manager.Track -> doGridDoubleClick (+- 1988)
                                        '** WR1_Managers.idh.forms.manager.Coverage -> doGridDoubleClick (+- 378)
                                        '** WR1_Managers.idh.forms.manager.Templ -> doGridDoubleClick (+- 774)
                                        '** WR1_Managers.idh.forms.manager.Templ -> doCustomItemEvent (+- 1988)
                                        '** idh.forms.order.Disposal -> doItemEvent (+- 2905)
                                        '**
                                        '** PARAMETERS SENT:
                                        '** 0 -> PARAMETER SWITCH - [DoUpdate, DoAuto]
                                        '** 1 -> Waste Order Row Number
                                        '** 2 -> Booking Date WO.U_BDate
                                        '** 3 -> Booking Time WO.U_BTime
                                        '** 4 -> ZipCode WO.U_ZpCd
                                        '** 5 -> Address WO.U_Address
                                        '** 6 -> Indicator that this row can include coverage instructions [CANNOTDOCOVERAGE, IDHJOBCO]
                                        '** 7 -> SiteId WO.U_SteId
                                        '** 8 -> Used to Add Licences if required, set to [DOADDLIC]
                                        '**			-> Currently only used by WR1_Managers.idh.forms.manager.Licences -> doCustomItemEvent
                                        '** 9 -> PAddress
                                        '** 10 -> PZipCode
                                        '** 11 -> PPhone1
                                        '** 12 -> FirstBP (Customer,Producer)
                                        '*************************************************************************************
                                        oForm.Items.Item("IDH_TABCOV").AffectsFormMode = False
                                        .SetValue("Code", .Offset, sCode)

                                        'Dim oCons As New SAPbouiCOM.Conditions
                                        'Dim oCon As SAPbouiCOM.Condition

                                        'oCon = oCons.Add
                                        'oCon.Alias = "Code"
                                        'oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                                        'oCon.CondVal = sCode
                                        'getFormMainDataSource(oForm).Query(oCons)

                                        doLoadRowFromCode(oForm, sCode)
                                        oWOR = thisWOR(oForm)

                                        'If Config.INSTANCE.doGetDoBuyingAndTrade() AndAlso oWOR.ParentOrder.U_FirstBP.Equals("Producer") Then
                                        '    setUFValue(oForm, "IDH_ZP", oWOR.ParentOrder.U_PZpCd)
                                        '    setUFValue(oForm, "IDH_ADDR", oWOR.ParentOrder.U_PAddress)
                                        '    setUFValue(oForm, "IDH_SITETL", oWOR.ParentOrder.U_PPhone1)
                                        'Else
                                        '    setUFValue(oForm, "IDH_ZP", oWOR.ParentOrder.U_ZpCd)
                                        '    setUFValue(oForm, "IDH_ADDR", oWOR.ParentOrder.U_Address)
                                        '    setUFValue(oForm, "IDH_SITETL", oWOR.ParentOrder.U_SiteTl)
                                        'End If

                                        'setWFValue(oForm, "ZIPCODE", oWOR.ParentOrder.U_ZpCd)
                                        'setWFValue(oForm, "STADDR", oWOR.ParentOrder.U_Address)

                                        ''Get the needed Data
                                        'Dim sQry As String = "SELECT " & IDH_JOBENTR._LckPrc & "," & IDH_JOBENTR._ForeCS & " FROM [@IDH_JOBENTR] WHERE Code = '" & getFormDFValue(oForm, IDH_JOBSHD._JobNr) & "'"
                                        'Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                                        'oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                        'oRecordSet.DoQuery(sQry)
                                        'If oRecordSet.RecordCount > 0 Then
                                        '    'setWFValue(oForm, "LCKPRC", oRecordSet.Fields.Item(0).Value)
                                        '    setWFValue(oForm, "FORECS", oRecordSet.Fields.Item(1).Value)
                                        'End If
                                        'IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)

                                        'setUFValue(oForm, "IDH_STEID", oData(7))
                                        'setUFValue(oForm, "IDH_PAYTRM", getPaymentTerms(oForm, getFormDFValue(oForm, "U_CustCd"))) 'Payment Term

                                        setWFValue(oForm, "OpenFrom", oData(6))

                                        'If sExtra = "DoAuto" Then
                                        'doGetChargePrices(oForm, True)
                                        'oWOR.doGetChargePrices(True)
                                        'End If

                                        If Not oData(8) Is Nothing AndAlso oData(8).Equals("DOADDLIC") Then
                                            setWFValue(oForm, "DOADDLIC", True)
                                        End If

                                        'Set TFS Fields visible status
                                        Dim sWOHStatus As String = oData(13)

                                        Dim sTFSReqArray As String = Config.ParameterWithDefault("TFSREQEN", "7")
                                        Dim aTRSplit As ArrayList = New ArrayList(sTFSReqArray.Split(","))

                                        'WOH Status Codes to set TFS fields enable on WOR. 7 = Approved
                                        If Config.ParameterWithDefault("TFSMOD", False).ToString() = "TFSMOD_TRUE" AndAlso sWOHStatus.Length > 0 AndAlso
                                        aTRSplit.Contains(sWOHStatus) AndAlso (getFormDFValue(oForm, "U_TFSReq").ToString().Length > 0 OrElse hasDifferentCountry(oForm)) Then
                                            'TFS Module
                                            oForm.Items.Item("IDH_TFSREQ").Visible = True
                                            .SetValue("U_TFSReq", .Offset, "Y")
                                            oForm.Items.Item("352").Visible = True
                                            oForm.Items.Item("IDH_TFSLK").Visible = True
                                            oForm.Items.Item("IDH_TFSNUM").Visible = True
                                            oForm.Items.Item("IDH_TFSSC").Visible = True
                                        Else
                                            oForm.Items.Item("IDH_TFSREQ").Visible = False
                                            oForm.Items.Item("352").Visible = False
                                            oForm.Items.Item("IDH_TFSLK").Visible = False
                                            oForm.Items.Item("IDH_TFSNUM").Visible = False
                                            oForm.Items.Item("IDH_TFSSC").Visible = False
                                        End If

                                        'EVN Requirement 
                                        If getFormDFValue(oForm, "U_EVNReq").ToString().Equals("Y") Then
                                            oForm.Items.Item("IDH_EVNREQ").Visible = True
                                            '.SetValue("U_EVNReq", .Offset, "Y")
                                            oForm.Items.Item("1000018").Visible = True
                                            oForm.Items.Item("IDH_EVNLK").Visible = True
                                            oForm.Items.Item("IDH_EVNNUM").Visible = True
                                            oForm.Items.Item("IDH_EVNSC").Visible = True
                                        Else
                                            oForm.Items.Item("IDH_EVNREQ").Visible = True
                                            oForm.Items.Item("1000018").Visible = False
                                            oForm.Items.Item("IDH_EVNLK").Visible = False
                                            oForm.Items.Item("IDH_EVNNUM").Visible = False
                                            oForm.Items.Item("IDH_EVNSC").Visible = False
                                        End If

                                        'Additional Expense rows 
                                        If Not oData(14) Is Nothing AndAlso oData(14).Equals("INSERTADDEXP") Then
                                            setWFValue(oForm, "INSERTADDEXP", True)
                                        End If
                                    Else
                                        '**************************************************
                                        '** idh.forms.SkipExpiryReport
                                        '**************************************************
                                        .SetValue("Code", .Offset, sCode)
                                        doLoadRowFromCode(oForm, sCode)

                                        'Dim oCons As New SAPbouiCOM.Conditions
                                        'Dim oCon As SAPbouiCOM.Condition

                                        'oCon = oCons.Add
                                        'oCon.Alias = "Code"
                                        'oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                                        'oCon.CondVal = sCode
                                        'getFormMainDataSource(oForm).Query(oCons)
                                    End If
                                End If
                            End If
                        End If
                    End With


                    '					Dim dExpLdWeight As Double = getFormDFValue(oForm, "U_ExpLdWgt")
                    '					If dExpLdWeight > 0 Then
                    '						setEnableItem(oForm, TRUE, "IDH_USEEXP")
                    '					End If

                    If getWFValue(oForm, "ISNEW") = False Then
                        oForm.SupportedModes = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                    Else
                        oForm.SupportedModes = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                        '                        setWFValue(oForm, "IDH_LastModePO", False)
                    End If

                    If Config.INSTANCE.getParameterAsBool("WOLCKCNT", True) AndAlso
                     getWFValue(oForm, "ISNEW") = False Then

                        Dim sContainerCd As String = getFormDFValue(oForm, "U_ItemCd")
                        If String.IsNullOrEmpty(sContainerCd) Then
                            setWFValue(oForm, "LOCKCNT", False)
                        Else
                            setWFValue(oForm, "LOCKCNT", True)
                        End If
                    End If

                    If getWFValue(oForm, "DOADDLIC") = True Then
                        doSetFocus(oForm, "IDH_SKPLIC")
                    Else
                        '                        If oForm.Items.Item("IDH_JOBTYP").Enabled = True Then
                        '                            doSetFocus(oForm, "IDH_JOBTYP")
                        '                        Else
                        '                            doSetFocus(oForm, "IDH_DOCNR")
                        '                        End If
                        doParkEdit(oForm)
                        'If oForm.Items.Item("IDH_JOBTPE").Enabled = True Then
                        '    doSetFocus(oForm, "IDH_JOBTPE")
                        'Else
                        '    doSetFocus(oForm, "IDH_DOCNR")
                        'End If
                    End If

                    Dim oItems() As String = {"IDH_JOBENT", "IDH_JOBSCH", "IDH_ITMGRP", "IDH_ITMGRN"}
                    setEnableItems(oForm, False, oItems)

                    Dim sForeCs1 As String = getWFValue(oForm, "FORECS")
                    If Not String.IsNullOrEmpty(sForeCs1) Then
                        Dim sFCBPType As String = Config.INSTANCE.getForcastBPType(sForeCs1)
                        setWFValue(oForm, "FORECSBPT", sFCBPType)
                        If Config.ParameterAsBool("LKBFCWCD", False) AndAlso getFormDFValue(oForm, "U_WasCd") IsNot Nothing AndAlso getFormDFValue(oForm, "U_WasCd").ToString.Trim <> "" Then
                            setEnableItem(oForm, False, "IDH_WASCL1")
                            setEnableItem(oForm, False, "IDH_WASCF")
                            setEnableItem(oForm, False, "IDH_WASMAT")
                        End If
                        If Not String.IsNullOrEmpty(sFCBPType) Then
                            If sFCBPType = "C" Then
                                setEnableItem(oForm, False, "IDH_CUST")
                                setEnableItem(oForm, False, "IDH_NAME")
                            ElseIf sFCBPType = "S" Then
                                setEnableItem(oForm, False, "IDH_SUPCOD")
                                setEnableItem(oForm, False, "IDH_SUPNAM")
                            End If
                        End If
                    End If

                    oForm.EnableMenu(Config.NAV_FIND, False)
                    oForm.EnableMenu(Config.NAV_ADD, False)
                    oForm.EnableMenu(Config.NAV_FIRST, False)
                    oForm.EnableMenu(Config.NAV_LAST, False)
                    oForm.EnableMenu(Config.NAV_PREV, False)
                    oForm.EnableMenu(Config.NAV_NEXT, False)
                End If
            Else
                oForm.SupportedModes = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or SAPbouiCOM.BoFormMode.fm_EDIT_MODE Or SAPbouiCOM.BoFormMode.fm_FIND_MODE Or SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                oForm.AutoManaged = False
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                doClearAll(oForm)

                oForm.EnableMenu(Config.NAV_FIND, True)
                oForm.EnableMenu(Config.NAV_ADD, False)
                oForm.EnableMenu(Config.NAV_FIRST, True)
                oForm.EnableMenu(Config.NAV_LAST, True)
                oForm.EnableMenu(Config.NAV_PREV, True)
                oForm.EnableMenu(Config.NAV_NEXT, True)
            End If

            doFillJobTypeCombo(oForm)

            'KA -- DIMECA
            doFillAlternateItemDescription(oForm, getFormDFValue(oForm, "U_AltWasDsc"))

            'Set Work Field value for Credit Checking
            Dim sOrdTotal As String = getWFValue(oForm, "CCTotal")
            If sOrdTotal Is Nothing OrElse sOrdTotal.Length < 0 Then
                setWFValue(oForm, "CCTotal", getFormDFValue(oForm, "U_Total"))
            End If

            'Dim dCharge As Double = getFormDFValue(oForm, "U_CusChr")

            '** The Additional Expenses
            Dim oAddGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            If oAddGrid Is Nothing Then
                oAddGrid = New WR1_Grids.idh.controls.grid.AdditionalExpenses(Me, oForm, "ADDGRID")
            End If
            Dim sJobNr As String = getFormDFValue(oForm, "U_JobNr")
            Dim sRowNr As String = getFormDFValue(oForm, "Code")

            sRowSta = getFormDFValue(oForm, "U_RowSta")
            Dim bCanEdit As Boolean = True
            If sRowSta.Equals(com.idh.bridge.lookups.FixedValues.getStatusDeleted()) Then
                bCanEdit = False
            Else
                Dim sStatusS As String = getFormDFValue(oForm, "U_Status")
                If sStatusS.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusOrdered()) = True OrElse
                 sStatusS.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusInvoiced()) = True OrElse
                 sStatusS.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusFoc()) = True Then
                    bCanEdit = False
                End If
            End If

            Dim dDocDate As Date = com.idh.utils.Dates.doStrToDate(getFormDFValue(oForm, "U_AEDate"))
            ''MA Start 26-01-2015
            'oAddGrid.doSetGridParameters("WO", sJobNr, sRowNr, bCanEdit, dDocDate)
            setWFValue(oForm, "CanEdit", bCanEdit)
            doTheGridLayout(oAddGrid, sJobNr, sRowNr, bCanEdit, dDocDate)
            ''MA End 26-01-2015

            Dim oAdde As WR1_Data.idh.data.AdditionalExpenses = getWFValue(oForm, "JobAdditionalHandler")
            If Not oAdde Is Nothing Then
                oAddGrid.doSetDataHandler(oAdde)
            End If

            If oWOR Is Nothing Then
                'If the IDH_JOMENTR Object is Nothing it will be looked up by the backeend when needed
                oWOR = thisWOR(oForm)
            End If

            'SET THE ADDITIONAL TOTALS ON THE TAB
            setUFValue(oForm, "IDH_ADDCO", oWOR.U_AddCost)
            setUFValue(oForm, "IDH_ADDCH", oWOR.U_AddCharge)

            setUFValue(oForm, "IDH_ADDCOV", oWOR.U_TAddCsVat)
            setUFValue(oForm, "IDH_ADDCHV", oWOR.U_TAddChVat)

            setUFValue(oForm, "IDH_TADDCO", oWOR.U_TAddCost)
            setUFValue(oForm, "IDH_TADDCH", oWOR.U_TAddChrg)

            doActivityGrid(oForm)

            If String.IsNullOrWhiteSpace(oWOR.U_TrnCode) = False Then
                setEnableItems(oForm, False, moMarketingCheckboxes)
            End If
        End Sub
        Private Sub doTheGridLayout(ByVal oGridN As WR1_Grids.idh.controls.grid.AdditionalExpenses, sJobNr As String, sRowNr As String, bCanEdit As Boolean, dDocDate As DateTime)
            If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", False) Then
                Dim sFormTypeId As String = oGridN.getSBOForm().TypeEx
                Dim oform As SAPbouiCOM.Form = oGridN.getSBOForm
                Dim oFormSettings As IDH_FORMSET = getWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId)
                If oFormSettings Is Nothing Then
                    oFormSettings = New IDH_FORMSET()
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, "")
                    setWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId, oFormSettings)

                    oFormSettings.doAddFieldToGrid(oGridN)

                    oGridN.doSetGridParameters("WO", sJobNr, sRowNr, True, dDocDate, True)

                    oFormSettings.doSyncDB(oGridN)

                Else
                    If oFormSettings.SkipFormSettings Then
                        oGridN.doSetGridParameters("WO", sJobNr, sRowNr, bCanEdit, dDocDate, True)
                    Else
                        oGridN.doSetGridParameters("WO", sJobNr, sRowNr, bCanEdit, dDocDate, False)
                        oFormSettings.doAddFieldToGrid(oGridN)
                    End If
                End If
            Else
                oGridN.doSetGridParameters("WO", sJobNr, sRowNr, bCanEdit, dDocDate, True)
            End If
        End Sub
        Private Function doLoadRowFromCode(ByVal oForm As SAPbouiCOM.Form, ByVal sCode As String) As Boolean
            oForm.Items.Item("IDH_TABCOV").AffectsFormMode = False
            setFormDFValue(oForm, "Code", sCode)

            Dim oCons As New SAPbouiCOM.Conditions
            Dim oCon As SAPbouiCOM.Condition
            Dim oDataSrc As SAPbouiCOM.DBDataSource

            oCon = oCons.Add
            oCon.Alias = "Code"
            oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
            oCon.CondVal = sCode
            oDataSrc = getFormMainDataSource(oForm)
            oDataSrc.Query(oCons)

            If oDataSrc.Size = 0 Then
                'DataHandler.INSTANCE.doError(DataHandler.ERRORLEVEL_USER + ": No record was found for Row: " & sCode)
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("No record was found for Row: " & sCode, "ERUSNRFR", {sCode})
                Return False
            End If

            'If the IDH_JOMENTR Object is Nothing it will be looked up by the backeend when needed
            createWOR(oForm)
            doSetValuesFromHeader(oForm)

            setWFValue(oForm, "OpenFrom", "CANNOTDOCOVERAGE")
            doEnableCoverage(oForm, False)
            Return True
            'Return doLoadHeaderFromRowCode(oForm, sCode)
        End Function

        Private Sub doSetValuesFromHeader(ByVal oForm As SAPbouiCOM.Form)
            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)
            Dim sFirstBP As String = oWOR.ParentOrder.U_FirstBP

            If Config.INSTANCE.doGetDoBuyingAndTrade() AndAlso Not sFirstBP Is Nothing AndAlso sFirstBP.Equals("Producer") Then
                setUFValue(oForm, "IDH_ZP", oWOR.ParentOrder.U_PZpCd)
                setUFValue(oForm, "IDH_ADDR", oWOR.ParentOrder.U_PAddress)
                setUFValue(oForm, "IDH_SITETL", oWOR.ParentOrder.U_PPhone1)
            Else
                setUFValue(oForm, "IDH_ZP", oWOR.ParentOrder.U_ZpCd)
                setUFValue(oForm, "IDH_ADDR", oWOR.ParentOrder.U_Address)
                setUFValue(oForm, "IDH_SITETL", oWOR.ParentOrder.U_SiteTl)
            End If

            setWFValue(oForm, "ZIPCODE", oWOR.ParentOrder.U_ZpCd)
            setWFValue(oForm, "STADDR", oWOR.ParentOrder.U_Address)

            setWFValue(oForm, "FORECS", oWOR.ParentOrder.U_ForeCS)

            '            oHead.Add("LCKPRC", getFormDFValue(oForm, IDH_JOBENTR._LckPrc))

            setUFValue(oForm, "IDH_STEID", oWOR.ParentOrder.U_SteId)
            setUFValue(oForm, "IDH_PAYTRM", getPaymentTerms(oForm, oWOR.F_CustomerCode)) 'Payment Term
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            doLoadDataF(oForm, False)
        End Sub

        '** The Initializer
        Protected Sub doLoadDataF(ByVal oForm As SAPbouiCOM.Form, ByVal bDoFindFields As Boolean)
            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)
            oForm.Freeze(True)
            Try
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    'doSetFocus(oForm, "IDH_JOBSCH")
                    Dim oItem As SAPbouiCOM.Item
                    oItem = oForm.Items.Item("IDH_JOBSCH")
                    oItem.Enabled = True
                    oItem.Click()

                    Dim oExclude() As String = {"IDH_JOBSCH"}
                    DisableAllEditItems(oForm, oExclude)
                    Exit Sub
                End If

                doSwitchSkip(oForm)
                doApplyStatuses(oForm)

                setUFValue(oForm, "IDH_AVDOC", "N")
                doGetItemGrpName(oForm)

                Dim sJobNr As String = oWOR.U_JobNr
                Dim sRowNr As String = oWOR.Code
                Dim oGridAdd As AdditionalExpenses = AdditionalExpenses.getInstance(oForm, "ADDGRID")

                If bDoFindFields = True Then
                    Dim dDocDate As Date = oWOR.U_AEDate
                    ''MA Start 26-01-2015
                    doTheGridLayout(oGridAdd, sJobNr, sRowNr, False, dDocDate)
                    ' oGridAdd.doSetGridParameters("WO", sJobNr, sRowNr, False, dDocDate)
                    ''MA End 26-01-2015
                End If

                oGridAdd.doReloadDataFH(sRowNr)
                doAdditionalExpGridUOMCombo(oForm)
                If bDoFindFields = False AndAlso com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", False) Then
                    Dim bCanEdit As Boolean = getWFValue(oForm, "CanEdit")
                    oGridAdd.doUpdateColumns(bCanEdit)
                End If

                If getWFValue(oForm, "ISNEW") = True Then
                    ''SKIP
                    Dim nSkipCost As Double = oWOR.U_SLicCst
                    Dim sSuppCd As String = oWOR.U_SLicSp
                    If sSuppCd.Length > 0 AndAlso
                     nSkipCost <> 0 Then
                        doAddSkip(oForm)
                    End If

                    ''CONGESTION
                    Dim nCongestionCost As Double = oWOR.U_CongCh
                    If nCongestionCost <> 0 Then
                        doAddCongestion(oForm)
                    End If

                    setWFValue(oForm, "ISNEW", False)

                    oWOR.doCalculateTotalsWithOther()
                ElseIf getWFValue(oForm, "DOADDLIC") = True Then
                    ''SKIP
                    Dim nSkipCost As Double = oWOR.U_SLicCst
                    Dim sSuppCd As String = oWOR.U_SLicSp
                    If sSuppCd.Length > 0 AndAlso
                     nSkipCost <> 0 Then
                        doAddSkip(oForm)
                    End If
                    setWFValue(oForm, "DOADDLIC", False)

                    oWOR.doCalculateLicinseCostTotals(IDH_JOBSHD.TOTAL_CALCULATE)
                ElseIf getWFValue(oForm, "INSERTADDEXP") = True Then
                    ''20160223 - ECW: On OSM, when user right click a generate a new WOR of different Job Type 
                    ''the additional charges dont get added. 
                    ''Trigger Additional Expense grid row addition (By BP Add. Charges and By Pricing) 
                    ''START 

                    'UPDATE THE ADDITIONAL GRID FROM HERE FOR NOW - IN FUTURE THE DBOBJECT WILL BE USED
                    doRefreshAdditionalPrices(oForm)

                    doAddAutoAdditionalCodes(oForm)
                    'oWOR.doGetAllUOM()
                    'oWOR.doGetAllPrices()

                    If Config.INSTANCE.getParameterAsBool("USEBPACH", False) Then
                        'Remove any existing AutoAdd rows 
                        'This is we dont know any items being added for this BP previously 
                        removeAutoAddBPAddCharges(oForm)
                        'Now add any new AddCharges row 
                        getBPAdditionalCharges(oForm)
                    End If

                    setWFValue(oForm, "INSERTADDEXP", False)
                    oWOR.doGetAllUOM()
                    oWOR.doGetAllPrices()

                    ''END 
                End If

                Dim sOrd As String = oWOR.U_WROrd
                If sOrd.Length > 0 Then
                    setEnableItem(oForm, True, "IDH_ORDLK")
                End If

                doVatGroupsCombo(oForm)

                Dim sPBI As String = getFormDFValue(oForm, "U_LnkPBI")
                If Not sPBI Is Nothing AndAlso sPBI.Length > 0 Then
                    oForm.Items.Item("261").Visible = True
                    oForm.Items.Item("IDH_PBI").Visible = True
                    oForm.Items.Item("IDH_PBILK").Visible = True
                Else
                    oForm.Items.Item("261").Visible = False
                    oForm.Items.Item("IDH_PBI").Visible = False
                    oForm.Items.Item("IDH_PBILK").Visible = False
                End If

                doDescribeCoverage(oForm)

                'doCalcAdditionalTotals(oForm)

                setUFValue(oForm, "U_BefDis", oWOR.SubBeforeDiscount)
                setUFValue(oForm, "U_SubTot", oWOR.SubAfterDiscount)
                setUFValue(oForm, "IDH_ADDCO", oWOR.U_AddCost)
                setUFValue(oForm, "IDH_ADDCH", oWOR.U_AddCharge)
                doCalculateProfit(oWOR)

                If bDoFindFields = True Then
                    oForm.Items.Item("IDH_TABCOS").Click()
                    doParkEdit(oForm)

                    DisableAllEditItems(oForm)
                End If
                If Config.INSTANCE.getParameterAsBool("WODSPSP", False) Then
                    oForm.Items.Item("IDH_TIPCF").Visible = False
                    oForm.Items.Item("IDH_SITAL").Visible = False
                    oForm.Items.Item("IDH_GETSIT").Visible = True
                    oForm.Items.Item("IDH_ALTSIT").Visible = True
                End If

                ''Disabled because currencies will be set by CIP/SIP DBObjects now 
                ' ''Set All BPs currency symbols 
                'doSetCurrencies(oForm)

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Loading DataF.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXLDBD", {Nothing})
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        Private Sub doApplyStatuses(ByVal oForm As SAPbouiCOM.Form)
            ' 20150219 - LPV - BEGIN - Move Items to groups
            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)

            Dim sStatus As String = oWOR.U_Status
            Dim sPStatus As String = oWOR.U_PStat
            Dim sDONr As String = oWOR.U_WROrd
            Dim sDORow As String = oWOR.U_WRRow
            Dim sJournalStatus As String = oWOR.U_JStat

            Dim sCarrierCN As String = oWOR.U_TPCN
            Dim sCustomerCN As String = oWOR.U_TCCN
            Dim sAEDate As String = getFormDFValue(oForm, "U_AEDate")
            Dim bKeepOpen As Boolean = False
            If String.IsNullOrWhiteSpace(sAEDate) AndAlso Config.INSTANCE.getParameterAsBool("MDPOBGI", False) = True Then
                bKeepOpen = True
            End If

            If oWOR.doCheckHaveMarketingDocuments(False) AndAlso bKeepOpen = False Then
                oForm.SupportedModes = SAPbouiCOM.BoFormMode.fm_OK_MODE
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE

                'If Not sAEDate Is Nothing AndAlso sAEDate.Length > 0 Then
                '    DisableAllEditItems(oForm, moJobEndEnabled)
                'Else
                DisableAllEditItems(oForm, moJobOpenEnabled)

                doSwitchSOFields(oForm)
                doSwitchPOFields(oForm)
                doSwitchProducerFields(oForm)

                doSetOrdered(oForm)
                'End If
            Else
                Dim oAddGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                oAddGrid.doEnabled(True)

                'If Not sAEDate Is Nothing AndAlso sAEDate.Length > 0 Then
                '    DisableAllEditItems(oForm, moJobEndEnabled)
                'Else
                DisableAllEditItems(oForm, moJobOpenEnabled)
                'End If
                doSwitchSOFields(oForm)
                doSwitchPOFields(oForm)
                doSwitchProducerFields(oForm)

                If getWFValue(oForm, "LOCKCNT") = True Then
                    setEnableItem(oForm, False, "IDH_ITMCOD")
                    setEnableItem(oForm, False, "IDH_DESC")
                    setEnableItem(oForm, False, "IDH_ITCF")
                End If
            End If

            ''##MA Start 17-09-2014 Issue#427
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_WOR_REQUESTDATE) = False Then '8192
                setEnableItem(oForm, False, "IDH_REQDAT")
                setEnableItem(oForm, False, "IDH_REQTIM")
                setEnableItem(oForm, False, "IDH_REQTIT")
            End If
            ''##MA End 17-09-2014 Issue#427
            ''##MA Start 08-09-2015 Issue#913
            Dim sForeCs1 As String = getWFValue(oForm, "FORECS")
            If Not sForeCs1 Is Nothing AndAlso sForeCs1.Length > 0 Then
                If Config.ParameterAsBool("LKBFCWCD", False) AndAlso getFormDFValue(oForm, "U_WasCd") IsNot Nothing AndAlso getFormDFValue(oForm, "U_WasCd").ToString.Trim <> "" Then
                    setEnableItem(oForm, False, "IDH_WASCL1")
                    setEnableItem(oForm, False, "IDH_WASCF")
                    setEnableItem(oForm, False, "IDH_WASMAT")
                End If
            End If
            ''##MA End 08-09-2015 Issue#913

            Dim bIsLinked As Boolean = False
            If sDONr IsNot Nothing AndAlso sDONr.Length > 0 AndAlso sDORow IsNot Nothing AndAlso sDORow.Length > 0 Then
                bIsLinked = True
            End If

            If bIsLinked AndAlso Config.INSTANCE.getParameterAsBool("WODWONL", True) Then
                doSetLinkedWODODisable(oForm)
            End If

            If com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus) = True Then
                doSwitchCheckBox(oForm, "IDH_DOARI", True)
            ElseIf com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) = True Then
                doSwitchCheckBox(oForm, "IDH_DOORD", True)
            ElseIf com.idh.bridge.lookups.FixedValues.isWaitingForFoc(sStatus) = True Then
                doSwitchCheckBox(oForm, "IDH_FOC", True)
            ElseIf com.idh.bridge.lookups.FixedValues.isWaitingForRebate(sStatus) = True Then
                oWOR.U_CustReb = "Y"
                If String.IsNullOrEmpty(oWOR.U_TrnCode) Then
                    doSwitchCheckBox(oForm, "IDH_CUSTRE", True)
                End If
            Else
                doSwitchCheckBox(oForm, "IDH", False)
            End If

            If sPStatus.Trim().Length = 0 OrElse sPStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusOpen()) Then
                If oForm.Items.Item("IDH_DOPO").Enabled Then
                    setUFValue(oForm, "IDH_DOPO", "N")
                End If
            ElseIf sPStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusOrdered()) _
                    OrElse (Not sCarrierCN Is Nothing AndAlso sCarrierCN.Length > 0) Then
                If oForm.Items.Item("IDH_DOPO").Enabled Then
                    doSwitchCheckBox(oForm, "IDH_DOPO", True)
                End If
            End If

            If com.idh.bridge.lookups.FixedValues.isWaitingForRebate(sPStatus) = True Then
                oWOR.U_CarrReb = "Y"
                doSwitchCheckBox(oForm, "IDH_CARRRE", True)
            ElseIf com.idh.bridge.lookups.FixedValues.isSetForAction(sPStatus) Then
                doSwitchCheckBox(oForm, "IDH_DOPO", True)
            End If

            'Do The Rebate statuses
            Dim bDoCRebate As Boolean = Config.ParameterAsBool("WOCREB", False)
            If bDoCRebate = True Then
                Dim sWastCd As String = oWOR.U_WasCd
                Dim bIsRebateItem As Boolean = Config.INSTANCE.doCheckIsCarrierRebate(sWastCd)

                If sCarrierCN Is Nothing OrElse sCarrierCN.Length = 0 Then
                    If String.IsNullOrEmpty(oWOR.U_TrnCode) Then
                        oForm.Items.Item("IDH_CARRRE").Enabled = True
                    End If

                    If getWFValue(oForm, "ISNEW") = True Then
                        If bIsRebateItem = True Then
                            oWOR.U_CarrReb = "Y"
                            doSwitchCheckBox(oForm, "IDH_CARRRE", True)
                        End If
                    End If
                Else
                    oForm.Items.Item("IDH_CARRRE").Enabled = False
                End If

                Dim sTCCN As String = getFormDFValue(oForm, "U_TCCN")
                If sTCCN Is Nothing OrElse sTCCN.Length = 0 Then
                    If String.IsNullOrEmpty(oWOR.U_TrnCode) Then
                        oForm.Items.Item("IDH_CUSTRE").Enabled = True
                    End If

                    If getWFValue(oForm, "ISNEW") = True Then
                        If bIsRebateItem = True Then
                            oWOR.U_CustReb = "Y"
                            doSwitchCheckBox(oForm, "IDH_CUSTRE", True)
                        End If
                    End If
                Else
                    oForm.Items.Item("IDH_CUSTRE").Enabled = False
                End If
            Else
                oForm.Items.Item("IDH_CARRRE").Enabled = False
                oForm.Items.Item("IDH_CUSTRE").Enabled = False
            End If

            'Set Journal Status
            If FixedValues.isJournaled(sJournalStatus) Then
                setEnableItem(oForm, False, "IDH_DOJRN")
            End If

            If com.idh.bridge.lookups.FixedValues.isWaitingForJournal(sJournalStatus) = True Then
                doSwitchCheckBox(oForm, "IDH_DOJRN", True)
            End If

            doSwitchPriceFields(oForm)

            doCheckMarketingCheckBoxesAuthorization(oForm)

            ' 20150219 - LPV - END 
        End Sub

        Private Sub doBlankOutItems(ByVal oForm As SAPbouiCOM.Form, ByVal oItemsList As String())
            Dim oItem As SAPbouiCOM.Item
            Dim iBlankColor As Integer = 14930874
            For Each sItem As String In oItemsList
                oItem = oForm.Items.Item(sItem)
                oItem.Enabled = False
                oItem.ForeColor = iBlankColor
                oItem.BackColor = iBlankColor
            Next
        End Sub

        Private Sub doSwitchPriceFields(ByVal oForm As SAPbouiCOM.Form)
            ''20150219 - LPV - BEGIN - work on the grouped items
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) = False OrElse Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) = False Then
                doBlankOutItems(oForm, moJOBPO_PriceItems)
                doBlankOutItems(oForm, moTIPPO_PriceItems)
                doBlankOutItems(oForm, moProPO_PriceItems)

                doBlankOutItems(oForm, moJOBSO_PriceItems)
                doBlankOutItems(oForm, moTIPSO_PriceItems)

                doBlankOutItems(oForm, moPO_Totalstems)
                doBlankOutItems(oForm, moSO_Totalstems)
            ElseIf Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES) = False OrElse Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Cost_PRICES) = False Then
                setEnableItems(oForm, False, moJOBPO_PriceItems)
                setEnableItems(oForm, False, moTIPPO_PriceItems)
                setEnableItems(oForm, False, moProPO_PriceItems)

                setEnableItems(oForm, False, moJOBSO_PriceItems)
                setEnableItems(oForm, False, moTIPSO_PriceItems)

                setEnableItems(oForm, False, moPO_Totalstems)
                setEnableItems(oForm, False, moSO_Totalstems)
            Else
                setEnableItems(oForm, True, moSIPCIPButtons)
            End If
            ''20150219 - LPV - END - work on the grouped items

            ' ''MA Start 28-10-2014 Issue#417 Issue#418
            'Dim oItems_Cost(), oItems_Charge() As String
            'If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) = False OrElse Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) = False Then
            '    'oItems = New String() {"IDH_TIPCOS", "uBC_TIPCOS", "IDH_ORDCOS", "uBC_ORDCOS", "IDH_PRCOST", "uBC_PRCOST", "IDH_CUSCHG", "uBC_CUSCHG", "IDH_CUSCHR", "uBC_CUSCHR", _
            '    '         "IDH_PROFIT", "IDH_TIPTOT", "IDH_ORDTOT", "IDH_PURTOT", "IDH_PVAT", _
            '    '         "IDH_ADDCOS", "IDH_TOTCOS", "IDH_CUSTOT", "IDH_JOBPRC", "IDH_TBD", _
            '    '         "IDH_DSCPRC", "IDH_SUBTOT", "IDH_TAX", "IDH_ADDCHR", "IDH_TOTAL"} ', _
            '    ''"IDH_ADDCO", "IDH_ADDCH"}
            '    oItems_Cost = New String() {"IDH_PRCOST", "uBC_PRCOST", "IDH_TIPCOS", "uBC_TIPCOS", "IDH_ORDCOS", "uBC_ORDCOS", _
            '              "IDH_PURTOT", "IDH_TIPTOT", "IDH_ORDTOT", "IDH_ADDCOS", "IDH_PVAT", "IDH_TOTCOS", "IDH_PROFIT",
            '                                "IDH_ADDCO", "IDH_ADDCOV", "IDH_TADDCO"} ''last 3 items are from Additinal Tab
            '    ', _
            '    '"IDH_ADDCO", "IDH_ADDCH"}
            '    setEnableItems(oForm, False, oItems_Cost)

            '    Dim oItem As SAPbouiCOM.Item
            '    Dim iBlankColor As Integer = 14930874
            '    For Each sItem As String In oItems_Cost
            '        oItem = oForm.Items.Item(sItem)
            '        oItem.ForeColor = iBlankColor
            '        oItem.BackColor = iBlankColor
            '    Next

            '    ''oItems = New String() {"IDH_CIP", "IDH_SIP", "IDH_WSIP", "IDH_PSIP"}
            '    oItems_Cost = New String() {"IDH_SIP", "IDH_WSIP", "IDH_PSIP"}
            '    setEnableItems(oForm, False, oItems_Cost)
            'ElseIf Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES) = False OrElse Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Cost_PRICES) = False Then
            '    'oItems = New String() {"IDH_TIPCOS", "uBC_TIPCOS", "IDH_ORDCOS", "uBC_ORDCOS", "IDH_PRCOST", "uBC_PRCOST", "IDH_CUSCHG", "uBC_CUSCHG", "IDH_CUSCHR", "uBC_CUSCHR", "IDH_CIP", "IDH_SIP", "IDH_WSIP", "IDH_PSIP"}
            '    oItems_Cost = New String() {"IDH_TIPCOS", "uBC_TIPCOS", "IDH_ORDCOS", "uBC_ORDCOS", "IDH_PRCOST", "uBC_PRCOST", "IDH_SIP", "IDH_WSIP", "IDH_PSIP"}
            '    setEnableItems(oForm, False, oItems_Cost)
            'End If
            'If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) = False OrElse Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) = False Then
            '    'oItems = New String() {"IDH_TIPCOS", "uBC_TIPCOS", "IDH_ORDCOS", "uBC_ORDCOS", "IDH_PRCOST", "uBC_PRCOST", "IDH_CUSCHG", "uBC_CUSCHG", "IDH_CUSCHR", "uBC_CUSCHR", _
            '    '         "IDH_PROFIT", "IDH_TIPTOT", "IDH_ORDTOT", "IDH_PURTOT", "IDH_PVAT", _
            '    '         "IDH_ADDCOS", "IDH_TOTCOS", "IDH_CUSTOT", "IDH_JOBPRC", "IDH_TBD", _
            '    '         "IDH_DSCPRC", "IDH_SUBTOT", "IDH_TAX", "IDH_ADDCHR", "IDH_TOTAL"} ', _
            '    oItems_Charge = New String() {"IDH_CUSCHG", "uBC_CUSCHG", "IDH_CUSCHR", "uBC_CUSCHR", _
            '             "IDH_CUSTOT", "IDH_JOBPRC", "IDH_TBD", _
            '             "IDH_DSCPRC", "IDH_SUBTOT", "IDH_TAX", "IDH_ADDCHR", "IDH_TOTAL",
            '                "IDH_ADDCH", "IDH_ADDCHV", "IDH_TADDCH"} ''last three from Additinal Tab
            '    ', _
            '    setEnableItems(oForm, False, oItems_Charge)

            '    Dim oItem As SAPbouiCOM.Item
            '    Dim iBlankColor As Integer = 14930874
            '    For Each sItem As String In oItems_Charge
            '        oItem = oForm.Items.Item(sItem)
            '        oItem.ForeColor = iBlankColor
            '        oItem.BackColor = iBlankColor
            '    Next

            '    ''oItems = New String() {"IDH_CIP", "IDH_SIP", "IDH_WSIP", "IDH_PSIP"}
            '    oItems_Charge = New String() {"IDH_CIP"}
            '    setEnableItems(oForm, False, oItems_Charge)
            'ElseIf Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES) = False OrElse Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Charge_PRICES) = False Then
            '    'oItems = New String() {"IDH_TIPCOS", "uBC_TIPCOS", "IDH_ORDCOS", "uBC_ORDCOS", "IDH_PRCOST", "uBC_PRCOST", "IDH_CUSCHG", "uBC_CUSCHG", "IDH_CUSCHR", "uBC_CUSCHR", "IDH_CIP", "IDH_SIP", "IDH_WSIP", "IDH_PSIP"}
            '    oItems_Charge = New String() {"IDH_CUSCHG", "uBC_CUSCHG", "IDH_CUSCHR", "uBC_CUSCHR", "IDH_CIP"}
            '    setEnableItems(oForm, False, oItems_Charge)
            'End If
            ' ''MA End 28-10-2014 Issue#417 Issue#418
        End Sub

        Private Sub doCheckMarketingCheckBoxesAuthorization(ByVal oForm As SAPbouiCOM.Form)
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_ALLOW_MARKEDOC_SWITCHES) = False Then
                setEnableItems(oForm, False, moSOMarketingCheckBoxes)
            End If
        End Sub

        'Private Sub doSetBillOrdDisable(ByVal oForm As SAPbouiCOM.Form)
        '    setEnableItems(oForm, False, moOrdredDisabled)
        'End Sub

        Private Sub doSetOrdered(ByVal oForm As SAPbouiCOM.Form)
            setEnableItems(oForm, False, moOrdredDisabled)

            Dim oAddGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            oAddGrid.doEnabled(False)
        End Sub

        Private Sub doSetLinkedWODODisable(ByVal oForm As SAPbouiCOM.Form)
            setEnableItems(oForm, False, moLinkedWODisabled)
        End Sub

        Private Sub doSwitchSOFields(ByVal oForm As SAPbouiCOM.Form)
            '20150219 - LPV - BEGIn - GROUP ITEMS
            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)
            Dim bPurchaseItem As Boolean = Config.INSTANCE.doCheckCanPurchaseWB(oWOR.U_WasCd)

            If oWOR.isMarketingModeSOAswell() Then
                If oWOR.doCheckHasMarketingDoc_SalesInvoice() OrElse
                    oWOR.doCheckHasMarketingDoc_SalesOrder() Then

                    setEnableItems(oForm, False, moTIPSO_PriceItems)
                    setEnableItems(oForm, False, moTIPSO_WeightItems)
                    setEnableItems(oForm, False, moTIPSO_SupportItems)

                    setEnableItems(oForm, False, moJOBSO_PriceItems)
                    setEnableItems(oForm, False, moJOBSO_WeightItems)
                    setEnableItems(oForm, False, moJOBSO_SupportItems)

                    setEnableItems(oForm, False, moSOMarketingCheckBoxes)

                    doSetFocus(oForm, "IDH_VEHTP")

                    Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                    oGrid.doEnabled(False)
                Else
                    If Config.INSTANCE.getParameterAsBool("WODMDOL", False) = False Then
                        setEnableItem(oForm, False, "IDH_CUSWEI")
                    End If

                    If String.IsNullOrWhiteSpace(oWOR.U_TrnCode) = False Then
                        setEnableItems(oForm, False, moMarketingCheckboxes)
                    End If
                End If
            Else
                setEnableItems(oForm, False, moTIPSO_PriceItems)
                setEnableItems(oForm, False, moTIPSO_WeightItems)
                setEnableItems(oForm, False, moTIPSO_SupportItems)

                setEnableItems(oForm, False, moJOBSO_PriceItems)
                setEnableItems(oForm, False, moJOBSO_WeightItems)
                setEnableItems(oForm, False, moJOBSO_SupportItems)

                setEnableItems(oForm, False, moSOMarketingCheckBoxes)
            End If
            '20150219 - LPV - END
        End Sub

        Private Sub doSwitchPOFields(ByVal oForm As SAPbouiCOM.Form)
            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)

            If oWOR.doCheckHasMarketingDoc_DisposalPO() Then
                setEnableItems(oForm, False, moTIPPO_PriceItems)
                setEnableItems(oForm, False, moTIPPO_WeightItems)
                setEnableItems(oForm, False, moTIPPO_SupportItems)
            End If

            If oWOR.doCheckHasMarketingDoc_HaulagePO() Then
                setEnableItems(oForm, False, moJOBPO_PriceItems)
                setEnableItems(oForm, False, moJOBPO_WeightItems)
                setEnableItems(oForm, False, moJOBPO_SupportItems)
            End If

            'doSwitchProducerFields(oForm)

            If oWOR.doCheckHasMarketingDoc_ProducerPO() AndAlso
                oWOR.doCheckHasMarketingDoc_HaulagePO() AndAlso
                oWOR.doCheckHasMarketingDoc_DisposalPO() Then

                doSetEnabled(oForm.Items.Item("IDH_DOPO"), False)
            Else
                doSetEnabled(oForm.Items.Item("IDH_DOPO"), True)
            End If

            If String.IsNullOrWhiteSpace(oWOR.U_TrnCode) = False Then
                setEnableItems(oForm, False, moMarketingCheckboxes)
            End If
        End Sub

        Private Sub doSwitchProducerFields(ByVal oForm As SAPbouiCOM.Form)
            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)

            Dim bPurchaseItem As Boolean = Config.INSTANCE.doCheckCanPurchaseWB(oWOR.U_WasCd)

            If oWOR.isMarketingModePOAswell() Then
                If bPurchaseItem = False OrElse
                    oWOR.doCheckHasMarketingDoc_ProducerPO() OrElse
                    oWOR.doCheckHasMarketingDoc_ProducerInvoice() Then

                    setEnableItems(oForm, False, moProPO_PriceItems)
                    setEnableItems(oForm, False, moProPO_WeightItems)
                    setEnableItems(oForm, False, moProPO_SupportItems)

                    If oWOR.doCheckHasMarketingDoc_ProducerPO() OrElse
                        oWOR.doCheckHasMarketingDoc_ProducerInvoice() Then
                        Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                        oGrid.doEnabled(False)
                    End If
                Else
                    If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES) = False OrElse
                            Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) = False OrElse
                            Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Cost_PRICES) = False OrElse
                            Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) = False Then
                        setEnableItems(oForm, True, moProPO_WeightItems)
                        setEnableItems(oForm, True, moProPO_SupportItems)
                    Else
                        setEnableItems(oForm, True, moProPO_PriceItems)
                        setEnableItems(oForm, True, moProPO_WeightItems)
                        setEnableItems(oForm, True, moProPO_SupportItems)
                    End If
                    If Config.INSTANCE.getParameterAsBool("WODMDOL", False) = False Then
                        setEnableItem(oForm, False, "IDH_SUPWGT")
                    End If
                End If
            Else
                setEnableItems(oForm, False, moProPO_PriceItems)
                setEnableItems(oForm, False, moProPO_WeightItems)
                setEnableItems(oForm, False, moProPO_SupportItems)
            End If

            If String.IsNullOrWhiteSpace(oWOR.U_TrnCode) = False Then
                setEnableItems(oForm, False, moMarketingCheckboxes)
            End If
        End Sub

        Private Sub doSetCurrencies(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bFromModalForm As Boolean = False, Optional ByVal sModalFormType As String = "")
            Dim sCustCd As String = getFormDFValue(oForm, "U_CustCd")
            Dim sProdCd As String = getFormDFValue(oForm, "U_ProCd")
            Dim sDispCd As String = getFormDFValue(oForm, "U_Tip")
            Dim sCarrCd As String = getFormDFValue(oForm, "U_CarrCd")
            'Dim sLiceCd As String = getFormDFValue(oForm, "")
            Dim sQry As String = ""

            Dim sCCur As String
            Dim sPCur As String
            Dim sDCur As String
            Dim sCrCur As String

            sQry = "select  " +
                " IsNull((select IsNull(Currency, '') from OCRD where CardCode = '" + sCustCd + "'), '') As Customer, " +
                " IsNull((select IsNull(Currency, '') from OCRD where CardCode = '" + sProdCd + "'), '') As Producer, " +
                " IsNull((select IsNull(Currency, '') from OCRD where CardCode = '" + sDispCd + "'), '') As Disposal, " +
                " IsNull((select IsNull(Currency, '') from OCRD where CardCode = '" + sCarrCd + "'), '') As Carrier "


            'If sCustCd IsNot Nothing AndAlso sCustCd.Length > 0 Then
            '    sQry = sQry + " Select CardCode, Currency from OCRD where CardCode = '" + sCustCd + "'"
            'End If
            Dim oAdminInfo As SAPbobsCOM.AdminInfo = goParent.goDICompany.GetCompanyService.GetAdminInfo()
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                oRecordSet.DoQuery(sQry)
                If oRecordSet.RecordCount > 0 Then
                    sCCur = oRecordSet.Fields.Item("Customer").Value.ToString()
                    sPCur = oRecordSet.Fields.Item("Producer").Value.ToString()
                    sDCur = oRecordSet.Fields.Item("Disposal").Value.ToString()
                    sCrCur = oRecordSet.Fields.Item("Carrier").Value.ToString()

                    If sCCur.Length < 1 Then
                        sCCur = oAdminInfo.LocalCurrency
                    End If
                    If sPCur.Length < 1 Then
                        sPCur = oAdminInfo.LocalCurrency
                    End If
                    If sDCur.Length < 1 Then
                        sDCur = oAdminInfo.LocalCurrency
                    End If
                    If sCrCur.Length < 1 Then
                        sCrCur = oAdminInfo.LocalCurrency
                    End If

                    'setUFValue(oForm, "IDH_DCGCUR", sCCur)  'Disposal Charge from Customer Ccy 
                    'setUFValue(oForm, "IDH_HCGCUR", sCCur)  'Haulier Charge from Customer Ccy

                    'setUFValue(oForm, "IDH_PCOCUR", sPCur)  'Producer/Purchase
                    'setUFValue(oForm, "IDH_DCOCUR", sDCur)  'Disposal 
                    'setUFValue(oForm, "IDH_CCOCUR", sCrCur) 'Carrier 
                    ''setUFValue(oForm, "IDH_LCOCUR", sCCur)  'License

                    'Multi Currency Pricing 
                    If bFromModalForm Then
                        'setFormDFValue(oForm, IDH_JOBSHD._IDHRTCD, getSharedData(oForm, "IDH_RTCODE"))

                        setFormDFValue(oForm, IDH_JOBSHD._DispCgCc, sCCur)  'Disposal Charge from Customer Ccy 
                        setFormDFValue(oForm, IDH_JOBSHD._CarrCgCc, sCCur)  'Haulier Charge from Customer Ccy

                        setFormDFValue(oForm, IDH_JOBSHD._PurcCoCc, sPCur)  'Producer/Purchase
                        setFormDFValue(oForm, IDH_JOBSHD._DispCoCc, sDCur)  'Disposal 
                        setFormDFValue(oForm, IDH_JOBSHD._CarrCoCc, sCrCur) 'Carrier 
                        setFormDFValue(oForm, IDH_JOBSHD._LiscCoCc, sCCur)  'License
                    Else
                        'setUFValue(oForm, "IDH_DCGCUR", sCCur)  'Disposal Charge from Customer Ccy 
                        'setUFValue(oForm, "IDH_HCHCUR", sCCur)  'Haulier Charge from Customer Ccy

                        'setUFValue(oForm, "IDH_PCOCUR", sPCur)  'Producer/Purchase
                        'setUFValue(oForm, "IDH_DCOCUR", sDCur)  'Disposal 
                        'setUFValue(oForm, "IDH_CCOCUR", sCrCur) 'Carrier 
                        'setUFValue(oForm, "IDH_LCOCUR", sCCur)  'License

                        setFormDFValue(oForm, IDH_JOBSHD._DispCgCc, sCCur)  'Disposal Charge from Customer Ccy 
                        setFormDFValue(oForm, IDH_JOBSHD._CarrCgCc, sCCur)  'Haulier Charge from Customer Ccy

                        setFormDFValue(oForm, IDH_JOBSHD._PurcCoCc, sPCur)  'Producer/Purchase
                        setFormDFValue(oForm, IDH_JOBSHD._DispCoCc, sDCur)  'Disposal 
                        setFormDFValue(oForm, IDH_JOBSHD._CarrCoCc, sCrCur) 'Carrier 
                        setFormDFValue(oForm, IDH_JOBSHD._LiscCoCc, sCCur)  'License
                    End If

                End If
            Catch ex As Exception
            Finally
                oRecordSet = Nothing
            End Try
        End Sub

        Private Sub doGetItemGrpName(ByVal oForm As SAPbouiCOM.Form)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                Dim sGrpCode As String
                With getFormMainDataSource(oForm)
                    sGrpCode = .GetValue("U_ItmGrp", .Offset).Trim()
                End With
                If sGrpCode.Length > 0 Then
                    oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oRecordSet.DoQuery("select ItmsGrpNam from OITB where ItmsGrpCod = " & sGrpCode)
                    If oRecordSet.RecordCount > 0 Then
                        oForm.DataSources.UserDataSources.Item("U_ItmGrN").ValueEx = oRecordSet.Fields.Item(0).Value
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error retrieving the Item group name.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRIGN", {Nothing})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
                oRecordSet = Nothing
            End Try
        End Sub

        Private Sub doAddSkip(ByVal oForm As SAPbouiCOM.Form)
            Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")

            Dim sItemCd As String = Config.INSTANCE.doGetSkipLicCode()
            If Not sItemCd Is Nothing AndAlso sItemCd.Length > 0 Then
                Dim sJobNr As String = getFormDFValue(oForm, "U_JobNr")
                Dim sRowNr As String = getFormDFValue(oForm, "Code")

                Dim nItmCost As Double = getFormDFValue(oForm, "U_SLicCst")
                Dim sCustCd As String = getFormDFValue(oForm, "U_CustCd")
                Dim sCustNm As String = getFormDFValue(oForm, "U_CustNm")
                Dim sCustCurr As String = getFormDFValue(oForm, "U_DispCgCc") 'Config.INSTANCE.getValueFromBP(sSuppCd, "Currency")
                Dim sSuppCd As String = getFormDFValue(oForm, "U_SLicSp")
                Dim sSuppNm As String = getFormDFValue(oForm, "U_SLicNm")
                Dim sSuppCurr As String = Config.INSTANCE.getValueFromBP(sSuppCd, "Currency")
                Dim sEmpId As String = ""
                Dim sEmpNm As String = ""
                Dim sItemNm As String = "Skip Licence - Ref - " & getFormDFValue(oForm, "U_SLicNr") & " Exp " + getFormDFValue(oForm, "U_SLicExp")
                Dim nItmChrg As Double = nItmCost
                Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(sCustCd, sItemCd)
                Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(sSuppCd, sItemCd)

                oGrid.doAddExpense(sJobNr, sRowNr,
                    sCustCd, sCustNm, sCustCurr,
                    sSuppCd, sSuppNm, sSuppCurr,
                    sEmpId, sEmpNm,
                    sItemCd, sItemNm,
                    "ea", 1,
                    nItmCost, nItmChrg,
                    sCostVatGrp,
                    sChrgVatGrp,
                    True,
                    True,
                    True,
                    False,
                    "VARIABLE", "VARIABLE")
            End If
        End Sub

        Private Sub doAddCongestion(ByVal oForm As SAPbouiCOM.Form)
            Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")

            Dim sItemCd As String = Config.INSTANCE.doGetCongestionCode()
            If Not sItemCd Is Nothing AndAlso sItemCd.Length > 0 Then
                Dim sJobNr As String = getFormDFValue(oForm, "U_JobNr")
                Dim sRowNr As String = getFormDFValue(oForm, "Code")

                Dim nItmCost As Double = getFormDFValue(oForm, "U_CongCh")
                Dim sCustCd As String = getFormDFValue(oForm, "U_CustCd")
                Dim sCustNm As String = getFormDFValue(oForm, "U_CustNm")
                Dim sCustCurr As String = getFormDFValue(oForm, "U_DispCgCc") 'Config.INSTANCE.getValueFromBP(sSuppCd, "Currency")
                Dim sSuppCd As String = getFormDFValue(oForm, "U_CongCd")
                Dim sSuppNm As String = getFormDFValue(oForm, "U_SCngNm")
                Dim sSuppCurr As String = Config.INSTANCE.getValueFromBP(sSuppCd, "Currency")
                Dim sEmpId As String = ""
                Dim sEmpNm As String = ""
                Dim sItemNm As String = "Congestion Charge - " & getWFValue(oForm, "ZIPCODE")
                Dim nItmChrg As Double = nItmCost
                Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(sCustCd, sItemCd)
                Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(sSuppCd, sItemCd)

                oGrid.doAddExpense(sJobNr, sRowNr,
                    sCustCd, sCustNm, sCustCurr,
                    sSuppCd, sSuppNm, sSuppCurr,
                    sEmpId, sEmpNm,
                    sItemCd, sItemNm,
                    "ea", 1,
                    nItmCost, nItmChrg,
                    sCostVatGrp,
                    sChrgVatGrp,
                    True,
                    True,
                    True,
                    False,
                    "VARIABLE", "VARIABLE")
            End If
        End Sub


        'KA -- START -- 20121022
        Private Sub doAddFuelSurcharge(ByVal oForm As SAPbouiCOM.Form, ByVal sQty As String, ByVal dItmCharge As Double, ByRef sFinalMsg As String)
            'Bug Fix for Date format and comparison in query above
            Dim sActualEndDt As String = getFormDFValue(oForm, "U_AEDate").ToString()
            Dim oRS As SAPbobsCOM.Recordset

            Dim sFSQry As String = " SELECT U_VendorCd, U_VendorNm, U_Rate, (U_CostTrf * U_Rate / 100) as U_CostTrf, (U_ChargeTrf * U_Rate / 100) as U_ChargeTrf FROM [@ISB_FUELSCHG] " &
                                    " WHERE '" + sActualEndDt + "' BETWEEN U_StartDt AND U_EndDt "
            oRS = goParent.goDB.doSelectQuery(sFSQry)

            If oRS.RecordCount > 0 Then
                Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")

                Dim sItemCd As String = Config.Parameter("FUELSITM")
                If Not sItemCd Is Nothing AndAlso sItemCd.Length > 0 Then
                    Dim sJobNr As String = getFormDFValue(oForm, "U_JobNr")
                    Dim sRowNr As String = getFormDFValue(oForm, "Code")
                    Dim sItemNm As String = sItemCd

                    Dim sCustCd As String = getFormDFValue(oForm, "U_CustCd")
                    Dim sCustNm As String = getFormDFValue(oForm, "U_CustNm")
                    Dim sCustCurr As String = getFormDFValue(oForm, "U_DispCgCc") 'Config.INSTANCE.getValueFromBP(sSuppCd, "Currency")

                    Dim sSuppCd As String = oRS.Fields.Item(0).Value.ToString()
                    Dim sSuppNm As String = oRS.Fields.Item(1).Value.ToString()
                    Dim sSuppCurr As String = Config.INSTANCE.getValueFromBP(sSuppCd, "Currency")
                    Dim sEmpId As String = ""
                    Dim sEmpNm As String = ""
                    Dim sRate As String = oRS.Fields.Item("U_Rate").Value.ToString()

                    Dim nItmCost As Double = 0 'oRS.Fields.Item(3).Value
                    Dim nItmChrg As Double = Convert.ToDouble(sQty) * dItmCharge * (Convert.ToDouble(sRate) / 100) 'oRS.Fields.Item(4).Value
                    Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(sCustCd, sItemCd)
                    Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(sSuppCd, sItemCd)

                    oGrid.doAddExpense(sJobNr, sRowNr,
                        sCustCd, sCustNm, sCustCurr,
                        sSuppCd, sSuppNm, sSuppCurr,
                        sEmpId, sEmpNm,
                        sItemCd, sItemNm,
                        "ea", 1,
                        nItmCost, nItmChrg,
                        sCostVatGrp,
                        sChrgVatGrp,
                        True,
                        True,
                        True,
                        False,
                        "VARIABLE", "VARIABLE")

                    If sFinalMsg IsNot Nothing AndAlso sFinalMsg.Length > 0 Then
                        sFinalMsg = sFinalMsg + ", Fuel Surcharge"
                    Else
                        sFinalMsg = sFinalMsg + "Fuel Surcharge"
                    End If
                End If
            End If
            IDHAddOns.idh.data.Base.doReleaseObject(oRS)
            oRS = Nothing
        End Sub
        'KA -- END -- 20121022

        'Private Sub doAddCountyCharge(ByVal oForm As SAPbouiCOM.Form, ByRef sCustCd As String, ByRef sCustNm As String, ByRef sFinalMsg As String)
        '    Dim sJobNr As String = getFormDFValue(oForm, "U_JobNr")
        '    Dim sWasteCd As String = getFormDFValue(oForm, "U_ItemCd")

        '    Dim oRS As SAPbobsCOM.Recordset
        '    oRS = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

        '    '** NOTE **'
        '    'The query below needs to be updated once LV has updated the table
        '    'structure of Row table to store Customer Address
        '    'Currently the customer address is store in the WOH table
        '    Dim sCQry As String

        '    ''Old Query
        '    'sCQry = " select CADDR.CardCode, CADDR.CardName, CADDR.Address, CTRF.U_CntyCd, CTRF.U_WstGpCd, CTRF.U_VendorCd, CTRF.U_VendorNm,  " & _
        '    '" CTRF.U_CostTrf, CTRF.U_ChargeTrf from [@ISB_CNTYTARIFF] CTRF " & _
        '    '" inner join  " & _
        '    '" ( " & _
        '    '" 	select a.CardCode as CardCode, adr.CardName, a.Address as Address, a.U_USACOUNTY as County from CRD1 a inner join   " & _
        '    '" 	( " & _
        '    '" 		select U_CardCd as CardCode, U_CardNm as CardName, U_Address as Adrs from [@IDH_JOBENTR] where Code = '" + sJobNr + "'" & _
        '    '" 		UNION  " & _
        '    '" 		select U_SCardCd as CardCode, U_SCardNm as CardName, U_SAddress as Adrs from [@IDH_JOBENTR] where Code = '" + sJobNr + "'" & _
        '    '" 	) adr ON a.CardCode = adr.CardCode AND a.Address = adr.Adrs  " & _
        '    '" 	where a.AdresType = 'S' " & _
        '    '" ) CADDR ON CTRF.U_CntyCd = CADDR.County "

        '    ''New Query
        'sCQry = " select CADDR.CardCode, CADDR.CardName, CADDR.Address, CTRF.U_CntyCd, CTRF.U_WstGpCd, CTRF.U_VendorCd, CTRF.U_VendorNm,  " & _
        '            " CTRF.U_CostTrf, CTRF.U_ChargeTrf, CTRF.U_ItemCd from [@ISB_CNTYTARIFF] CTRF " & _
        '" inner join  " & _
        '" ( " & _
        '" 	select a.CardCode as CardCode, adr.CardName, a.Address as Address, a.U_USACOUNTY as County from CRD1 a inner join   " & _
        '" 	( " & _
        '" 		select U_CardCd as CardCode, U_CardNm as CardName, U_Address as Adrs from [@IDH_JOBENTR] where Code = '" + sJobNr + "'" & _
        '" 		UNION  " & _
        '" 		select U_SCardCd as CardCode, U_SCardNm as CardName, U_SAddress as Adrs from [@IDH_JOBENTR] where Code = '" + sJobNr + "'" & _
        '" 	) adr ON a.CardCode = adr.CardCode AND a.Address = adr.Adrs  " & _
        '" 	where a.AdresType = 'S' " & _
        '            " ) CADDR ON CTRF.U_CntyCd = CADDR.County AND CTRF.U_ItemCd = '" + sWasteCd + "' "
        '    'start from here
        '    oRS.DoQuery(sCQry)

        '    If oRS.RecordCount > 0 Then
        '        Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
        '        Dim sItemCd As String = Config.Parameter("CNTYCITM")
        '        If sItemCd IsNot Nothing AndAlso sItemCd.Length > 0 Then
        '            For index As Integer = 0 To oRS.RecordCount - 1
        '                'Dim sJobNr As String = getFormDFValue(oForm, "U_JobNr")
        '                Dim sRowNr As String = getFormDFValue(oForm, "Code")
        '                Dim sItemNm As String = sItemCd

        '                'Dim sCustCd As String = oRS.Fields.Item(0).Value.ToString()
        '                'Dim sCustNm As String = oRS.Fields.Item(1).Value.ToString()

        '                Dim sSuppCd As String = oRS.Fields.Item(5).Value.ToString()
        '                Dim sSuppNm As String = oRS.Fields.Item(6).Value.ToString()
        '                Dim sEmpId As String = ""
        '                Dim sEmpNm As String = ""

        '                Dim nItmCost As Double = oRS.Fields.Item(7).Value
        '                Dim nItmChrg As Double = oRS.Fields.Item(8).Value
        '                Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(goParent, sCustCd, sItemCd)
        '                Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(goParent, sSuppCd, sItemCd)

        '                oGrid.doAddExpense(sJobNr, sRowNr, _
        '                    sCustCd, sCustNm, _
        '                    sSuppCd, sSuppNm, _
        '                    sEmpId, sEmpNm, _
        '                    sItemCd, sItemNm, _
        '                    "ea", 1, _
        '                    nItmCost, nItmChrg, _
        '                    sCostVatGrp, _
        '                    sChrgVatGrp, _
        '                    True, _
        '                    True)
        '                oRS.MoveNext()
        '            Next

        '            If sFinalMsg IsNot Nothing AndAlso sFinalMsg.Length > 0 Then
        '                sFinalMsg = sFinalMsg + ", County Tariff(" + oRS.RecordCount.ToString() + ")"
        '            Else
        '                sFinalMsg = sFinalMsg + "County Tariff(" + oRS.RecordCount.ToString() + ")"
        '            End If

        '        Else
        '            'show warrning message
        '        End If

        '    End If
        '    IDHAddOns.idh.data.Base.doReleaseObject(oRS)
        '    oRS = Nothing
        'End Sub

        'KA -- START -- 20121022

        '*** 20121115: County Charge logic changed
        'Private Sub doAddCountyCharge(ByVal oForm As SAPbouiCOM.Form, ByRef sCustCd As String, ByRef sCustNm As String, ByRef sFinalMsg As String)
        '    Dim sJobNr As String = getFormDFValue(oForm, "U_JobNr")
        '    Dim sRowNr As String = getFormDFValue(oForm, "Code")
        '    Dim sWasteCd As String = getFormDFValue(oForm, "U_WasCd")

        '    Dim sItemCd As String = Config.Parameter("CNTYCITM")
        '    Dim sItemNm As String = sItemCd

        '    Dim oRS As SAPbobsCOM.Recordset
        '    oRS = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

        '    'KA -- START -- 20121025
        '    '** NOTE **'
        '    'The query below needs to be updated once LV has updated the table
        '    'structure of Row table to store Customer Address
        '    'Currently the customer address is store in the WOH table

        '    '20121024: Have updated the query below
        '    'Getting the Customer Address details from the WOH and
        '    'Supplier Address details from WOR
        '    Dim sCQry As New System.Text.StringBuilder
        '    sCQry.Append("SELECT a.cardcode    AS CardCode, ")
        '    sCQry.Append("       adr.cardname, ")
        '    sCQry.Append("       a.address     AS Address, ")
        '    sCQry.Append("       a.u_usacounty AS County, ")
        '    sCQry.Append("       CntyTarif.u_cntycd, ")
        '    sCQry.Append("       CntyTarif.u_wstgpcd, ")
        '    sCQry.Append("       CntyTarif.u_vendorcd, ")
        '    sCQry.Append("       CntyTarif.u_vendornm, ")
        '    sCQry.Append("       CntyTarif.u_costtrf, ")
        '    sCQry.Append("       CntyTarif.u_chargetrf, ")
        '    sCQry.Append("       CntyTarif.u_itemcd, ")
        '    sCQry.Append("       CntyTarif.u_itemnm ")
        '    sCQry.Append("FROM   crd1 a ")
        '    sCQry.Append("       INNER JOIN (SELECT u_cardcd  AS CardCode, ")
        '    sCQry.Append("                          u_cardnm  AS CardName, ")
        '    sCQry.Append("                          u_address AS Adrs ")
        '    sCQry.Append("                   FROM   [@idh_jobentr] ")
        '    sCQry.Append("                   WHERE  code = '" & sJobNr & "' ")
        '    sCQry.Append("                   UNION ")
        '    sCQry.Append("                   SELECT U_Tip  AS CardCode, ")
        '    sCQry.Append("                          U_TipNm  AS CardName, ")
        '    sCQry.Append("                          u_saddress AS Adrs ")
        '    sCQry.Append("                   FROM   [@idh_jobshd] ")
        '    sCQry.Append("                   WHERE  code = '" & sRowNr & "') adr ")
        '    sCQry.Append("               ON a.cardcode = adr.cardcode ")
        '    sCQry.Append("                  AND a.address = adr.adrs ")
        '    sCQry.Append("       INNER JOIN (SELECT Trf.u_cntycd, ")
        '    sCQry.Append("                          Trf.u_wstgpcd, ")
        '    sCQry.Append("                          Trf.u_vendorcd, ")
        '    sCQry.Append("                          Trf.u_vendornm, ")
        '    sCQry.Append("                          Trf.u_costtrf, ")
        '    sCQry.Append("                          Trf.u_chargetrf, ")
        '    sCQry.Append("                          WC.u_itemcd, ")
        '    sCQry.Append("                          WC.u_itemnm ")
        '    sCQry.Append("                   FROM   [@isb_cntytariff] AS Trf ")
        '    sCQry.Append("                          INNER JOIN [@idh_wgpcnty] AS WC ")
        '    sCQry.Append("                                  ON Trf.u_cntycd = WC.u_cntycd ")
        '    sCQry.Append("                                     AND Trf.u_wstgpcd = WC.u_wstgpcd ")
        '    sCQry.Append("                   where Trf.U_Include = 'Y' AND WC.U_ItemCd = '" & sWasteCd & "') CntyTarif ")
        '    sCQry.Append("               ON a.u_usacounty = CntyTarif.u_cntycd ")
        '    sCQry.Append("WHERE  a.adrestype = 'S' ")

        '    oRS.DoQuery(sCQry.ToString())
        '    'KA -- END -- 20121025

        '    If oRS.RecordCount > 0 Then
        '        Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
        '        If sItemCd IsNot Nothing AndAlso sItemCd.Length > 0 Then
        '            For index As Integer = 0 To oRS.RecordCount - 1
        '                Dim sSuppCd As String = oRS.Fields.Item("U_VendorCd").Value.ToString()
        '                Dim sSuppNm As String = oRS.Fields.Item("U_VendorNm").Value.ToString()
        '                Dim sEmpId As String = ""
        '                Dim sEmpNm As String = ""

        '                Dim nItmCost As Double = oRS.Fields.Item("U_CostTrf").Value
        '                Dim nItmChrg As Double = oRS.Fields.Item("U_ChargeTrf").Value
        '                Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(goParent, sCustCd, sItemCd)
        '                Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(goParent, sSuppCd, sItemCd)

        '                oGrid.doAddExpense(sJobNr, sRowNr, _
        '                    sCustCd, sCustNm, _
        '                    sSuppCd, sSuppNm, _
        '                    sEmpId, sEmpNm, _
        '                    sItemCd, sItemNm, _
        '                    "ea", 1, _
        '                    nItmCost, nItmChrg, _
        '                    sCostVatGrp, _
        '                    sChrgVatGrp, _
        '                    True, _
        '                    True)
        '                oRS.MoveNext()
        '            Next

        '            If sFinalMsg IsNot Nothing AndAlso sFinalMsg.Length > 0 Then
        '                sFinalMsg = sFinalMsg + ", County Tariff(" + oRS.RecordCount.ToString() + ")"
        '            Else
        '                sFinalMsg = sFinalMsg + "County Tariff(" + oRS.RecordCount.ToString() + ")"
        '            End If

        '        Else
        '            'show warrning message for no County Item in WR Config
        '            If sFinalMsg IsNot Nothing AndAlso sFinalMsg.Length > 0 Then
        '                sFinalMsg = sFinalMsg + ", County Tariff(Item not found in WRConfig: CNTYCITM)"
        '            Else
        '                sFinalMsg = sFinalMsg + "County Tariff(Item not found in WRConfig: CNTYCITM)"
        '            End If
        '        End If

        '    End If
        '    IDHAddOns.idh.data.Base.doReleaseObject(oRS)
        '    oRS = Nothing
        'End Sub
        'KA -- END -- 20121022

        Private Sub doAddCountyCharge(ByVal oForm As SAPbouiCOM.Form, ByRef sCustCd As String, ByRef sCustNm As String, ByRef sFinalMsg As String)
            Dim sJobNr As String = getFormDFValue(oForm, "U_JobNr")
            Dim sRowNr As String = getFormDFValue(oForm, "Code")
            Dim sWasteCd As String = getFormDFValue(oForm, "U_WasCd")

            'Dim sCustCd As String = getFormDFValue(oForm, "U_CustCd")
            Dim sCustCurr As String = getFormDFValue(oForm, "U_DispCgCc") 'Config.INSTANCE.getValueFromBP(sSuppCd, "Currency")
            Dim sCustAdr As String = getUFValue(oForm, "IDH_ADDR").ToString()
            Dim sDspSiteCd As String = getFormDFValue(oForm, "U_Tip")
            Dim sDspSiteAdr As String = getFormDFValue(oForm, "U_SAddress")
            Dim dManQty As Double = Convert.ToDouble(getFormDFValue(oForm, "U_CBIManQty"))
            Dim sManUOM As String = getFormDFValue(oForm, "U_CBIManUOM")
            Dim sWstProfileCd As String = getFormDFValue(oForm, "U_WasCd")

            Dim sItemCd As String = Config.Parameter("CNTYCITM")
            Dim sItemNm As String = sItemCd
            Dim sQry As String
            Dim oRSCountyCharge As SAPbobsCOM.Recordset

            'sQry = ""
            'sQry = sQry & "SELECT Adr.cardcode    AS CardCode, "
            'sQry = sQry & "       Adr.address     AS Address, "
            'sQry = sQry & "       Adr.u_usacounty AS County, "
            'sQry = sQry & "       CTR.u_cntycd, "
            'sQry = sQry & "       CTR.u_wstgpcd, "
            'sQry = sQry & "       CTR.u_vendorcd, "
            'sQry = sQry & "       CTR.u_vendornm, "
            'sQry = sQry & "       CTR.u_costtrf, "
            'sQry = sQry & "       CTR.u_chargetrf, "
            'sQry = sQry & "       CTR.u_uom "
            'sQry = sQry & "FROM   [@isb_cntytariff] CTR "
            'sQry = sQry & "       INNER JOIN crd1 Adr "
            'sQry = sQry & "               ON CTR.u_cntycd = Adr.u_usacounty "
            'sQry = sQry & "                  AND Adr.cardcode = '" + sCustCd + "' "
            'sQry = sQry & "                  AND Adr.address = '" + sCustAdr + "' "
            'sQry = sQry & "UNION "
            'sQry = sQry & "SELECT Adr.cardcode    AS CardCode, "
            'sQry = sQry & "       Adr.address     AS Address, "
            'sQry = sQry & "       Adr.u_usacounty AS County, "
            'sQry = sQry & "       CTR.u_cntycd, "
            'sQry = sQry & "       CTR.u_wstgpcd, "
            'sQry = sQry & "       CTR.u_vendorcd, "
            'sQry = sQry & "       CTR.u_vendornm, "
            'sQry = sQry & "       CTR.u_costtrf, "
            'sQry = sQry & "       CTR.u_chargetrf, "
            'sQry = sQry & "       CTR.u_uom "
            'sQry = sQry & "FROM   [@isb_cntytariff] CTR "
            'sQry = sQry & "       INNER JOIN crd1 Adr "
            'sQry = sQry & "               ON CTR.u_cntycd = Adr.u_usacounty "
            'sQry = sQry & "                  AND Adr.cardcode = '" + sDspSiteCd + "' "
            'sQry = sQry & "                  AND Adr.address = '" + sDspSiteAdr + "' "

            sQry = ""
            sQry = sQry & "SELECT Adr.cardcode, "
            sQry = sQry & "       Adr.address, "
            sQry = sQry & "       Adr.u_usacounty AS County, "
            sQry = sQry & "       CTR.u_cntycd, "
            sQry = sQry & "       CTR.u_wstgpcd, "
            sQry = sQry & "       CTR.u_vendorcd, "
            sQry = sQry & "       CTR.u_vendornm, "
            sQry = sQry & "       CTR.u_costtrf, "
            sQry = sQry & "       CTR.u_chargetrf, "
            sQry = sQry & "       CTR.u_uom "
            sQry = sQry & "FROM   dbo.oitm "
            sQry = sQry & "       INNER JOIN dbo.[@idh_wgpcnty] "
            sQry = sQry & "               ON dbo.oitm.itemcode = dbo.[@idh_wgpcnty].u_itemcd "
            sQry = sQry & "       INNER JOIN dbo.[@isb_cntytariff] AS CTR "
            sQry = sQry & "                  INNER JOIN dbo.crd1 AS Adr "
            sQry = sQry & "               ON CTR.u_cntycd = Adr.u_usacounty "
            sQry = sQry & "                  AND Adr.cardcode = '" + sCustCd + "' "
            sQry = sQry & "                  AND Adr.address = '" + sCustAdr + "' "
            sQry = sQry & "               ON dbo.[@idh_wgpcnty].u_wstgpcd = CTR.u_wstgpcd "
            sQry = sQry & "WHERE  ( dbo.oitm.itemcode = '" + sWstProfileCd + "' ) "
            sQry = sQry & "UNION "
            sQry = sQry & "SELECT Adr.cardcode, "
            sQry = sQry & "       Adr.address, "
            sQry = sQry & "       Adr.u_usacounty AS County, "
            sQry = sQry & "       CTR.u_cntycd, "
            sQry = sQry & "       CTR.u_wstgpcd, "
            sQry = sQry & "       CTR.u_vendorcd, "
            sQry = sQry & "       CTR.u_vendornm, "
            sQry = sQry & "       CTR.u_costtrf, "
            sQry = sQry & "       CTR.u_chargetrf, "
            sQry = sQry & "       CTR.u_uom "
            sQry = sQry & "FROM   dbo.oitm "
            sQry = sQry & "       INNER JOIN dbo.[@idh_wgpcnty] "
            sQry = sQry & "               ON dbo.oitm.itemcode = dbo.[@idh_wgpcnty].u_itemcd "
            sQry = sQry & "       INNER JOIN dbo.[@isb_cntytariff] AS CTR "
            sQry = sQry & "                  INNER JOIN dbo.crd1 AS Adr "
            sQry = sQry & "               ON CTR.u_cntycd = Adr.u_usacounty "
            sQry = sQry & "                  AND Adr.cardcode = '" + sDspSiteCd + "' "
            sQry = sQry & "                  AND Adr.address = '" + sDspSiteAdr + "' "
            sQry = sQry & "               ON dbo.[@idh_wgpcnty].u_wstgpcd = CTR.u_wstgpcd "
            sQry = sQry & "WHERE  ( dbo.oitm.itemcode = '" + sWstProfileCd + "' ) "

            oRSCountyCharge = goParent.goDB.doSelectQuery(sQry)

            If oRSCountyCharge.RecordCount > 0 Then
                Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                If sItemCd IsNot Nothing AndAlso sItemCd.Length > 0 Then
                    'Get distinct County list 
                    Dim htCounty As Hashtable = New Hashtable
                    For ctr As Integer = 0 To oRSCountyCharge.RecordCount - 1
                        If Not htCounty.Contains(oRSCountyCharge.Fields.Item("County").Value) Then
                            htCounty.Add(oRSCountyCharge.Fields.Item("County").Value, ctr.ToString())
                        Else
                            Exit For
                        End If
                        oRSCountyCharge.MoveNext()
                    Next
                    oRSCountyCharge.MoveFirst()

                    For index As Integer = 0 To htCounty.Count - 1
                        Dim sSuppCd As String = oRSCountyCharge.Fields.Item("U_VendorCd").Value.ToString()
                        Dim sSuppNm As String = oRSCountyCharge.Fields.Item("U_VendorNm").Value.ToString()
                        Dim sSuppCurr As String = Config.INSTANCE.getValueFromBP(sSuppCd, "Currency")
                        Dim sEmpId As String = ""
                        Dim sEmpNm As String = ""

                        Dim sTariffUOM As String = oRSCountyCharge.Fields.Item("U_UOM").Value
                        Dim sTariffCounty As String = oRSCountyCharge.Fields.Item("County").Value
                        Dim nItmCost As Double = oRSCountyCharge.Fields.Item("U_CostTrf").Value
                        Dim nItmChrg As Double = oRSCountyCharge.Fields.Item("U_ChargeTrf").Value

                        Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(sCustCd, sItemCd)
                        Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(sSuppCd, sItemCd)
                        Dim bDOAddExp As Boolean = True

                        If Not sTariffUOM.Equals(sManUOM) Then
                            'get conversion factor from Manifest UOM -->> Tariff UOM
                            Dim oFactorRS As SAPbobsCOM.Recordset
                            Dim sFacQry As String
                            sFacQry = "select U_Factor from [@IDH_CNTYTRFFACTOR] where U_UOMFrom = '" + sManUOM + "' AND U_UOMTo = '" + sTariffUOM + "' AND U_County = '" + sTariffCounty + "'"
                            oFactorRS = goParent.goDB.doSelectQuery(sFacQry)

                            If oFactorRS.RecordCount > 0 Then
                                Dim nFactor As Double = oFactorRS.Fields.Item("U_Factor").Value
                                'nItmChrg = nItmChrg * nFactor * dManQty
                                nItmChrg = nItmChrg * nFactor
                            Else
                                'Message: conversion factor not found
                                If sFinalMsg IsNot Nothing AndAlso sFinalMsg.Length > 0 Then
                                    sFinalMsg = sFinalMsg + ", County Tariff(Conversion factor not found for ManifestUOM-" + sManUOM + " to TariffUOM-" + sTariffUOM + ")"
                                Else
                                    sFinalMsg = sFinalMsg + "County Tariff(Conversion factor not found for ManifestUOM-" + sManUOM + " to TariffUOM-" + sTariffUOM + ")"
                                End If
                                bDOAddExp = False
                            End If
                            IDHAddOns.idh.data.Base.doReleaseObject(oFactorRS)
                        Else
                            'nItmChrg = nItmChrg * dManQty
                        End If

                        Dim sUOMForAddItemRow As String = If(sManUOM IsNot Nothing AndAlso sManUOM.Length > 0, sManUOM, sTariffUOM)

                        If bDOAddExp Then
                            oGrid.doAddExpense(sJobNr, sRowNr,
                                sCustCd, sCustNm, sCustCurr,
                                sSuppCd, sSuppNm, sSuppCurr,
                                sEmpId, sEmpNm,
                                sItemCd, sItemNm,
                                sUOMForAddItemRow, dManQty,
                                nItmCost, nItmChrg,
                                sCostVatGrp,
                                sChrgVatGrp,
                                True,
                                True,
                                True,
                                False,
                                "VARIABLE", "VARIABLE")

                        End If

                        oRSCountyCharge.MoveNext()
                    Next
                Else
                    'show warrning message for no County Item in WR Config
                    If sFinalMsg IsNot Nothing AndAlso sFinalMsg.Length > 0 Then
                        sFinalMsg = sFinalMsg + ", County Tariff(Item not found in WRConfig: CNTYCITM)"
                    Else
                        sFinalMsg = sFinalMsg + "County Tariff(Item not found in WRConfig: CNTYCITM)"
                    End If
                End If

            End If

            IDHAddOns.idh.data.Base.doReleaseObject(oRSCountyCharge)

        End Sub

        Private Sub doAddPortCharge(ByVal oForm As SAPbouiCOM.Form, ByRef sFinalMsg As String)
            Dim sCustAddress As String = getUFValue(oForm, "STADDR")
            Dim dManQty As Double = Convert.ToDouble(getFormDFValue(oForm, "U_CBIManQty"))

            Dim oRS As SAPbobsCOM.Recordset
            oRS = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim sPCQry As String = " select U_SiteAdd, U_VendorCd, U_VendorNm, U_UOM, U_CostTrf, U_ChargeTrf FROM [@ISB_PORTMASTR] " &
                                    " WHERE U_SiteAdd = '" + sCustAddress + "' "
            oRS.DoQuery(sPCQry)
            If oRS.RecordCount > 0 Then
                Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")

                Dim sItemCd As String = Config.Parameter("PORTCITM")
                If Not sItemCd Is Nothing AndAlso sItemCd.Length > 0 Then
                    Dim sJobNr As String = getFormDFValue(oForm, "U_JobNr")
                    Dim sRowNr As String = getFormDFValue(oForm, "Code")
                    Dim sItemNm As String = sItemCd

                    Dim sCustCd As String = getFormDFValue(oForm, "U_CustCd")
                    Dim sCustNm As String = getFormDFValue(oForm, "U_CustNm")
                    Dim sCustCurr As String = getFormDFValue(oForm, "U_DispCgCc")

                    Dim sSuppCd As String = oRS.Fields.Item(1).Value.ToString()
                    Dim sSuppNm As String = oRS.Fields.Item(2).Value.ToString()
                    Dim sUOM As String = oRS.Fields.Item(3).Value.ToString()
                    Dim sSuppCurr As String = Config.INSTANCE.getValueFromBP(sSuppCd, "Currency")
                    Dim sEmpId As String = ""
                    Dim sEmpNm As String = ""

                    Dim nItmCost As Double = oRS.Fields.Item(4).Value
                    Dim nItmChrg As Double = oRS.Fields.Item(5).Value
                    Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(sCustCd, sItemCd)
                    Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(sSuppCd, sItemCd)

                    oGrid.doAddExpense(sJobNr, sRowNr,
                        sCustCd, sCustNm, sCustCurr,
                        sSuppCd, sSuppNm, sSuppCurr,
                        sEmpId, sEmpNm,
                        sItemCd, sItemNm,
                        sUOM, dManQty,
                        nItmCost, nItmChrg,
                        sCostVatGrp,
                        sChrgVatGrp,
                        True,
                        True,
                        True,
                        False,
                        "VARIABLE", "VARIABLE")

                    If sFinalMsg IsNot Nothing AndAlso sFinalMsg.Length > 0 Then
                        sFinalMsg = sFinalMsg + ", Port Charge"
                    Else
                        sFinalMsg = sFinalMsg + "Port Charge"
                    End If
                End If
            End If
            IDHAddOns.idh.data.Base.doReleaseObject(oRS)
            oRS = Nothing
        End Sub

        Private Sub doRemoveConsignmentNoteFee(ByVal oForm As SAPbouiCOM.Form)
            Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            If Not oGrid Is Nothing Then
                Dim sAddItemCd As String = Config.INSTANCE.doGetConsignmentItem()
                If Not sAddItemCd Is Nothing AndAlso sAddItemCd.Length > 0 Then
                    Dim iIndex As Integer = oGrid.doIndexOfItem(sAddItemCd)
                    If iIndex > -1 Then
                        oGrid.doRemoveRow(iIndex)
                    End If
                End If
            End If
        End Sub

        Private Sub doAddConsignmentNoteFee(ByVal oForm As SAPbouiCOM.Form)
            Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            If Not oGrid Is Nothing Then
                Dim sAddItemCd As String = Config.INSTANCE.doGetConsignmentItem()
                Dim sSuppCd As String = Config.INSTANCE.doGetConsignmentSupplier()
                If Not sAddItemCd Is Nothing AndAlso sAddItemCd.Length > 0 AndAlso
                 Not sSuppCd Is Nothing AndAlso sSuppCd.Length > 0 Then

                    If oGrid.doIndexOfItem(sAddItemCd) > -1 Then
                        Exit Sub
                    End If

                    Dim sAddItemNm As String = com.idh.bridge.lookups.Config.INSTANCE.doGetItemDescription(sAddItemCd)
                    Dim sSuppNm As String = com.idh.bridge.lookups.Config.INSTANCE.doGetBPName(sSuppCd)

                    Dim sJobNr As String = getFormDFValue(oForm, "U_JobNr")
                    Dim sRowNr As String = getFormDFValue(oForm, "Code")
                    Dim sCustCd As String = getFormDFValue(oForm, "U_CustCd")
                    Dim sCustNm As String = getFormDFValue(oForm, "U_CustNm")
                    Dim sCustCurr As String = getFormDFValue(oForm, "U_DispCgCc")

                    Dim sSuppCurr As String = Config.INSTANCE.getValueFromBP(sSuppCd, "Currency")
                    Dim sEmpId As String = ""
                    Dim sEmpNm As String = ""

                    Dim sContainerCode As String = getFormDFValue(oForm, "U_ItemCd")
                    Dim sContainerGrp As String = getFormDFValue(oForm, "U_ItmGrp")
                    Dim sWastCd As String = getFormDFValue(oForm, "U_WasCd")
                    Dim sZipCode As String = getWFValue(oForm, "ZIPCODE")
                    Dim sAddress As String = getWFValue(oForm, "STADDR")
                    Dim sBranch As String = getFormDFValue(oForm, "U_Branch")
                    Dim sJobType As String = getFormDFValue(oForm, "U_JobTp")

                    Dim sDocDate As String = getFormDFValue(oForm, "U_RDate")
                    Dim dDocDate As Date = com.idh.utils.Dates.doStrToDate(sDocDate)

                    oGrid.doAddExpenseDoPriceLookup(sJobNr, sRowNr,
                        sCustCd, sCustNm, sCustCurr,
                        sSuppCd, sSuppNm, sSuppCurr,
                        sEmpId, sEmpNm,
                        sContainerCode, sContainerGrp,
                        sAddItemCd, sAddItemNm,
                        sWastCd, sAddress, sBranch, sJobType,
                        dDocDate, sZipCode,
                        "ea", 1,
                        True, True)
                End If
            End If
        End Sub

        '*** Return True If it is a Skip Group
        Private Function doSwitchSkip(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim sItemGrp As String
            With getFormMainDataSource(oForm)
                sItemGrp = .GetValue("U_ItmGrp", .Offset).Trim()
            End With

            Dim bVis As Boolean
            If com.idh.bridge.lookups.Config.INSTANCE.doGetSkipGroup() = sItemGrp Then
                bVis = True
            Else
                bVis = False
                Dim sLicContainers As String = Config.Parameter("IGR-LCON")
                If Not sLicContainers Is Nothing AndAlso sLicContainers.Length > 0 Then
                    Dim oList() As String = sLicContainers.Split(",")

                    Dim iCount As Integer
                    Dim sGroup As String
                    For iCount = 0 To oList.Length() - 1
                        sGroup = oList(iCount).Trim()
                        If sGroup.Length > 0 Then
                            If sGroup = sItemGrp Then
                                bVis = True
                                Exit For
                            End If
                        End If
                    Next
                End If
            End If
            ''MA Start Pt#791 15-06-2015

            'oForm.Items.Item("IDH_SKPLIC").Visible = bVis
            'oForm.Items.Item("IDH_LICEXP").Visible = bVis
            ''oForm.Items.Item("IDH_LICCHR").Visible = bVis
            oForm.Items.Item("IDH_LICSUP").Visible = bVis
            oForm.Items.Item("IDH_SLNM").Visible = bVis
            oForm.Items.Item("IDH_LCCF").Visible = bVis
            oForm.Items.Item("IDH_LCLK").Visible = bVis
            'oForm.Items.Item("IDH_SKPTOT").Visible = bVis
            'oForm.Items.Item("63").Visible = bVis
            'oForm.Items.Item("65").Visible = bVis
            ''oForm.Items.Item("68").Visible = bVis
            'oForm.Items.Item("83").Visible = bVis
            'oForm.Items.Item("152").Visible = bVis
            'oForm.Items.Item("169").Visible = bVis
            ''MA End Pt#791 15-06-2015
            Return False
        End Function

        'KA -- START -- 20121022
        Private Sub doUOMCombos(ByVal oForm As SAPbouiCOM.Form)
            FillCombos.UOMCombo(FillCombos.getCombo(oForm, "IDH_PUOM"))
            FillCombos.UOMCombo(FillCombos.getCombo(oForm, "IDH_PURUOM"))
            FillCombos.UOMCombo(FillCombos.getCombo(oForm, "IDH_UOM"))

            'doFillCombo(oForm, "IDH_PUOM", "OWGT", "UnitDisply", "UnitName")
            'doFillCombo(oForm, "IDH_PURUOM", "OWGT", "UnitDisply", "UnitName")
            'doFillCombo(oForm, "IDH_UOM", "OWGT", "UnitDisply", "UnitName")

            If Config.INSTANCE.getParameterAsBool("USAREL", False) Then
                'doFillCombo(oForm, "IDH_CBIUOM", "OWGT", "UnitDisply", "UnitName")
                FillCombos.UOMCombo(FillCombos.getCombo(oForm, "IDH_CBIUOM"))
            End If
        End Sub
        'KA -- END -- 20121022

        Private Sub doBranchCombo(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_BRANCH", "OUBR", "Code", "Remarks")
        End Sub

        Private Sub doVatGroupsCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            Dim oCombo1 As SAPbouiCOM.ComboBoxColumn
            Dim oCombo2 As SAPbouiCOM.ComboBoxColumn
            Dim iSize As Integer = oGridN.getRowCount()
            If iSize > 0 Then
                Dim oColumns As SAPbouiCOM.GridColumns = oGridN.getSBOGrid().Columns

                Dim iIndex As Integer = oGridN.doIndexFieldWC("U_CostVatGrp")
                oCombo1 = oColumns.Item(iIndex)
                oCombo1.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
                oCombo2 = oColumns.Item(oGridN.doIndexFieldWC("U_ChrgVatGrp"))
                oCombo2.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

                doClearValidValues(oCombo1.ValidValues)
                doClearValidValues(oCombo2.ValidValues)
                Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                'Dim sCategory As String
                Try
                    oRecordSet = goParent.goDB.doSelectQuery("SELECT Code, Name, Category from OVTG")
                    While Not oRecordSet.EoF
                        Try
                            'sCategory = oRecordSet.Fields.Item(2).Value
                            'If sCategory = "I" Then
                            oCombo1.ValidValues.Add(oRecordSet.Fields.Item(0).Value, oRecordSet.Fields.Item(1).Value)
                            'ElseIf sCategory = "O" Then
                            oCombo2.ValidValues.Add(oRecordSet.Fields.Item(0).Value, oRecordSet.Fields.Item(1).Value)
                            'End If
                        Catch ex As Exception
                        End Try
                        oRecordSet.MoveNext()
                    End While
                Catch ex As Exception
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error populating the Vat group combo.")
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Vat Group")})
                Finally
                    IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
                End Try
            End If

        End Sub

        Private Sub doLoadSkip(ByVal oForm As SAPbouiCOM.Form, ByVal sLicNr As String, ByVal sLicExpr As String, ByVal dLicCh As Double, ByVal sLicSp As String)
            Dim sJobEntry As String
            Dim sItemGrp As String
            Dim bHasOrderedEntry As Boolean

            With getFormMainDataSource(oForm)
                sJobEntry = .GetValue("U_JobNr", .Offset).Trim()
                sItemGrp = .GetValue("U_ItmGrp", .Offset).Trim()
            End With
            If Not com.idh.bridge.lookups.Config.INSTANCE.doGetSkipGroup() = sItemGrp Then
                Exit Sub
            End If

            bHasOrderedEntry = Config.INSTANCE.doCheckedBilledBefore(sLicNr, sJobEntry, sItemGrp)

            With getFormMainDataSource(oForm)
                Dim sIGrp1 As String = .GetValue("U_ItmGrp", .Offset).Trim()
                Dim sIGrp2 As String = com.idh.bridge.lookups.Config.INSTANCE.doGetSkipGroup()
                If bHasOrderedEntry = False AndAlso sIGrp1 = sIGrp2 Then

                    If Not (sLicNr Is Nothing) Then
                        .SetValue("U_SLicNR", .Offset, sLicNr)
                    End If
                    If Not (sLicExpr Is Nothing) Then
                        .SetValue("U_SLicExp", .Offset, sLicExpr)
                    End If
                    .SetValue("U_SLicCh", .Offset, dLicCh)
                    .SetValue("U_SLicSp", .Offset, sLicSp)
                Else
                    .SetValue("U_SLicNR", .Offset, "")
                    .SetValue("U_SLicExp", .Offset, "")
                    .SetValue("U_SLicCh", .Offset, 0)
                    .SetValue("U_SLicSp", .Offset, "")
                End If
            End With
        End Sub

        Private Function doCalcState(ByVal sReqDate As String, ByVal sStartDate As String, ByVal sEndDate As String) As String
            Dim sStat As String = "MustCalc"

            Dim dReqDate As Date = com.idh.utils.Dates.doStrToDate(sReqDate)
            Dim dStartDate As Date = com.idh.utils.Dates.doStrToDate(sStartDate)
            Dim dEndDate As Date = com.idh.utils.Dates.doStrToDate(sEndDate)
            Dim dCurrDate As Date = Date.Now

            ' Late: ReqDate < Now And StartDate Is NULL
            If dStartDate = Nothing AndAlso dReqDate < dCurrDate Then
                Return com.idh.bridge.lookups.FixedValues.getProgressLate() 'Config.INSTANCE.doGetLateLbl(goParent) '"Late"
            End If
            ' Busy: StartDate Not NULL And EndDate Is NULL
            If Not (dStartDate = Nothing) AndAlso dEndDate = Nothing Then
                Return com.idh.bridge.lookups.FixedValues.getProgressBusy() 'Config.INSTANCE.doGetBusyLbl(goParent) '"Busy"
            End If
            ' Done: StartDate Not NULL And EndDate Is Not NULL
            If Not (dEndDate = Nothing) Then
                Return com.idh.bridge.lookups.FixedValues.getProgressDone() 'Config.INSTANCE.doGetDoneLbl(goParent) '"Done"
            End If
            ' Waiting: ReqDate > Now And StartDate Is NULL
            If dStartDate = Nothing AndAlso dReqDate > dCurrDate Then
                Return com.idh.bridge.lookups.FixedValues.getProgressWaiting() 'Config.INSTANCE.doGetWaitingLbl(goParent) '"Waiting"
            End If

            ' No Request: ReqDate Is NULL
            If dReqDate = Nothing Then
                Return com.idh.bridge.lookups.FixedValues.getProgressNoRequest() 'Config.INSTANCE.doGetNoRequestLbl(goParent) '"No Request"
            End If

            Return sStat
        End Function

        '*** Validate the Header
        Private Function doValidateHeader(ByVal oForm As SAPbouiCOM.Form) As Boolean
            ''Site
            Dim sCardCode As String = getFormDFValue(oForm, "U_TIP")
            Dim sAddress As String = getFormDFValue(oForm, "U_SAddress")
            Dim sAddressLineNum As String = ""
            If sCardCode.Trim <> "" AndAlso sAddress.Trim <> "" Then
                sAddressLineNum = com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPShipToAddress(sCardCode, sAddress, "LineNum")
                setFormDFValue(oForm, IDH_JOBSHD._SAddrsLN, sAddressLineNum)
            Else
                setFormDFValue(oForm, IDH_JOBSHD._SAddrsLN, sAddressLineNum)
            End If
            Return True
        End Function

        Private Sub doDescribeCoverageHist(ByVal oForm As SAPbouiCOM.Form)
            ''Dim sConvStr As String = getFormDFValue( oForm, "U_Covera")
        End Sub

        Private Sub doDescribeCoverage(ByVal oForm As SAPbouiCOM.Form)
            Dim sPreBook As String = getFormDFValue(oForm, "U_Covera")
            Dim sPreBookDesc As String
            sPreBookDesc = getCoverageDescription(sPreBook)
            doSetCoverageItems(oForm, sPreBook)
            setUFValue(oForm, "IDH_CONVA", sPreBookDesc)
        End Sub

        Private Sub doSetCoverageItems(ByVal oForm As SAPbouiCOM.Form, ByVal sConvStr As String)
            Dim sFrom As String = getWFValue(oForm, "OpenFrom")
            Dim bReDo As Boolean = False
            If sFrom.Equals("IDHJOBCO") = False Then
                Exit Sub
            End If

            If Not sConvStr Is Nothing AndAlso sConvStr.Length() > 0 Then
                While sConvStr.Chars(0) = "_"
                    sConvStr = sConvStr.Substring(1)
                End While

                If Not (sConvStr.Substring(1, 3) = "000" OrElse sConvStr.EndsWith("00000000") = True) AndAlso
          sConvStr.StartsWith("Created From:") = False Then
                    Select Case sConvStr.Chars(0)
                        Case "D"
                            setUFValue(oForm, "U_2RD", "1")
                        Case "W"
                            setUFValue(oForm, "U_2RD", "2")
                        Case "M"
                            setUFValue(oForm, "U_2RD", "3")
                        Case "Y"
                            setUFValue(oForm, "U_2RD", "4")
                        Case Else
                            setUFValue(oForm, "U_2RD", "5")
                    End Select

                    Dim sFreq As String = sConvStr.Substring(1, 3)
                    setUFValue(oForm, "U_2FRE", sFreq)

                    If sConvStr.Chars(4) = "Y" Then
                        setUFValue(oForm, "U_2FSU", "Y")
                    End If
                    If sConvStr.Chars(5) = "Y" Then
                        setUFValue(oForm, "U_2FMO", "Y")
                    End If
                    If sConvStr.Chars(6) = "Y" Then
                        setUFValue(oForm, "U_2FTU", "Y")
                    End If
                    If sConvStr.Chars(7) = "Y" Then
                        setUFValue(oForm, "U_2FWE", "Y")
                    End If
                    If sConvStr.Chars(8) = "Y" Then
                        setUFValue(oForm, "U_2FTH", "Y")
                    End If
                    If sConvStr.Chars(9) = "Y" Then
                        setUFValue(oForm, "U_2FFR", "Y")
                    End If
                    If sConvStr.Chars(10) = "Y" Then
                        setUFValue(oForm, "U_2FSA", "Y")
                    End If

                    Dim iWrkVal As Integer
                    '                    Dim sStartDate As String = sConvStr.Substring(11, 8)
                    '                    Dim dStartDate As Date
                    '                    iWrkVal = Val(sStartDate)
                    '                    If iWrkVal > 0 Then
                    '                       	setUFValue(oForm,"U_2RRS",sStartDate)
                    '                    End If

                    Dim sEndDate As String = sConvStr.Substring(19, 8)
                    Dim dEndDate As Date
                    iWrkVal = ToInt(sEndDate)
                    If iWrkVal > 20060000 Then
                        dEndDate = com.idh.utils.Dates.doStrToDate(sEndDate)
                        '                       	setUFValue(oForm,"U_2RRVB",sEndDate)
                        setUFValue(oForm, "U_2RRA", "2")
                    Else
                        '                    	setUFValue(oForm,"U_2RRVA",iWrkVal.ToString())
                        setUFValue(oForm, "U_2RRA", "1")
                    End If
                End If
            End If
        End Sub

        Private Function getCoverageDescription(ByVal sConvStr As String) As String
            Dim sResult As String = ""
            Dim bFromPBI As Boolean = False
            If sConvStr.StartsWith("Created From:") = True Then
                sResult = sConvStr
            ElseIf Not sConvStr Is Nothing AndAlso sConvStr.Length() > 0 Then
                If sConvStr.StartsWith("PBI-") = True Then
                    sResult = "PBI Entry: " & ToChar(10)
                    sConvStr = sConvStr.Substring(4)
                    bFromPBI = True
                Else
                    While sConvStr.Chars(0) = "_"
                        sConvStr = sConvStr.Substring(1)
                    End While
                End If
                '            	If sConvStr.Chars(0) = "_" Then
                '                	sConvStr = sConvStr.Substring(1)
                '                End If

                If sConvStr.Substring(1, 3).Equals("000") = True AndAlso
                 sConvStr.Chars(0) <> "D" AndAlso
                 sConvStr.Chars(0) <> "W" AndAlso
                 sConvStr.Chars(0) <> "M" AndAlso
                 sConvStr.Chars(0) <> "Y" Then
                    sResult = "No Coverage"
                ElseIf sConvStr.EndsWith("00000000") = True Then
                    sResult = "No Coverage"
                ElseIf sConvStr.StartsWith("Created From:") = True Then
                    sResult = sConvStr
                Else
                    If sConvStr.Length > 27 AndAlso bFromPBI = False Then
                        Dim sSrc As String = sConvStr.Substring(28)
                        sResult = "[Created From: " & sSrc & "]   ...... " & ToChar(10)
                    End If

                    'The coverage was set with a recurrence pattern of Other,  to be done  every 001 Week(s) on Monday, Friday. Starting from 20070310 to 20070410
                    sResult = sResult & "The PreBook was set with a recurrence pattern of "
                    Select Case sConvStr.Chars(0)
                        Case "D"
                            sResult = sResult & "Daily"
                        Case "W"
                            sResult = sResult & "Weekly"
                        Case "M"
                            sResult = sResult & "Monthly"
                        Case "Y"
                            sResult = sResult & "Yearly"
                        Case Else
                            sResult = sResult & "Other"
                    End Select

                    Dim sFreq As String = sConvStr.Substring(1, 3)
                    Dim iFreq As Integer = ToInt(sFreq)
                    If iFreq > 0 Then
                        sResult = sResult & ", to be done every " & ToInt(sFreq) & " Week(s) "
                    End If

                    Dim sDays As String = ""
                    If sConvStr.Chars(4) = "Y" Then
                        sDays = sDays & "Sunday"
                    End If
                    If sConvStr.Chars(5) = "Y" Then
                        If sDays.Length() > 0 Then
                            sDays = sDays & ", "
                        End If
                        sDays = sDays & "Monday"
                    End If
                    If sConvStr.Chars(6) = "Y" Then
                        If sDays.Length() > 0 Then
                            sDays = sDays & ", "
                        End If
                        sDays = sDays & "Tuesday"
                    End If
                    If sConvStr.Chars(7) = "Y" Then
                        If sDays.Length() > 0 Then
                            sDays = sDays & ", "
                        End If
                        sDays = sDays & "Wednesday"
                    End If
                    If sConvStr.Chars(8) = "Y" Then
                        If sDays.Length() > 0 Then
                            sDays = sDays & ", "
                        End If
                        sDays = sDays & "Thursday"
                    End If
                    If sConvStr.Chars(9) = "Y" Then
                        If sDays.Length() > 0 Then
                            sDays = sDays & ", "
                        End If
                        sDays = sDays & "Friday"
                    End If
                    If sConvStr.Chars(10) = "Y" Then
                        If sDays.Length() > 0 Then
                            sDays = sDays & ", "
                        End If
                        sDays = sDays & "Saturday"
                    End If
                    If sDays.Length() > 0 Then
                        sResult = sResult & " on " & sDays & "."
                    End If

                    Dim sStartDate As String = sConvStr.Substring(11, 8)
                    Dim dStartDate As Date
                    Dim iWrkVal As Integer = ToInt(sStartDate)
                    If iWrkVal > 0 Then
                        dStartDate = com.idh.utils.Dates.doStrToDate(sStartDate)
                        sResult = sResult & " Starting from " & dStartDate
                    End If

                    Dim sEndDate As String = sConvStr.Substring(19, 8)
                    Dim dEndDate As Date
                    iWrkVal = ToInt(sEndDate)
                    If iWrkVal > 20060000 Then
                        dEndDate = com.idh.utils.Dates.doStrToDate(sEndDate)
                        sResult = sResult & " to " & dEndDate
                    Else
                        sResult = sResult & " recurring " & iWrkVal & " time."
                    End If
                End If
            Else
                sResult = "No Coverage"
            End If
            Return sResult
        End Function

        '----U_2SCHDT---
        '0     - The Occurance D - Daily,W - Weekly,M - Monthly,Y - Yearly,O - Other
        '1,2,3 - Frequency 0 - 999
        '4     - Sunday - N/Y
        '5     - Monday - N/Y
        '6     - Tuesday - N/Y
        '7     - Wednesday - N/Y
        '8     - Thursday - N/Y
        '9     - Friday - N/Y
        '10    - Saterday - N/Y
        '11-18 - Date from - 20071230 / 00000000
        '19-26 - Date to or Occurences - 20071230 / 00000000
        Public Sub doCreateRecureString(ByVal oForm As SAPbouiCOM.Form)
            Dim sResult As String = ""
            Dim sWrk As String = ""
            sWrk = oForm.DataSources.UserDataSources.Item("U_2RD").ValueEx
            If sWrk = "1" Then
                sResult = "D"
            ElseIf sWrk = "2" Then
                sResult = "W"
            ElseIf sWrk = "3" Then
                sResult = "M"
            ElseIf sWrk = "4" Then
                sResult = "Y"
            Else
                sResult = "O"
            End If

            sWrk = oForm.DataSources.UserDataSources.Item("U_2FRE").ValueEx
            If sWrk.Length = 0 Then
                sWrk = "000"
            Else
                sWrk = sWrk.PadLeft(3, "0")
            End If
            sResult = sResult & sWrk

            sWrk = oForm.DataSources.UserDataSources.Item("U_2FSU").ValueEx
            If sWrk.Length > 0 Then
                sResult = sResult & sWrk
            Else
                sResult = sResult & "N"
            End If

            sWrk = oForm.DataSources.UserDataSources.Item("U_2FMO").ValueEx
            If sWrk.Length > 0 Then
                sResult = sResult & sWrk
            Else
                sResult = sResult & "N"
            End If

            sWrk = oForm.DataSources.UserDataSources.Item("U_2FTU").ValueEx
            If sWrk.Length > 0 Then
                sResult = sResult & sWrk
            Else
                sResult = sResult & "N"
            End If

            sWrk = oForm.DataSources.UserDataSources.Item("U_2FWE").ValueEx
            If sWrk.Length > 0 Then
                sResult = sResult & sWrk
            Else
                sResult = sResult & "N"
            End If

            sWrk = oForm.DataSources.UserDataSources.Item("U_2FTH").ValueEx
            If sWrk.Length > 0 Then
                sResult = sResult & sWrk
            Else
                sResult = sResult & "N"
            End If

            sWrk = oForm.DataSources.UserDataSources.Item("U_2FFR").ValueEx
            If sWrk.Length > 0 Then
                sResult = sResult & sWrk
            Else
                sResult = sResult & "N"
            End If

            sWrk = oForm.DataSources.UserDataSources.Item("U_2FSA").ValueEx
            If sWrk.Length > 0 Then
                sResult = sResult & sWrk
            Else
                sResult = sResult & "N"
            End If

            sWrk = oForm.DataSources.UserDataSources.Item("U_2RRS").ValueEx
            If sWrk.Length = 0 Then
                sWrk = goParent.doDateToStr() '"00000000"
            End If
            sResult = sResult & sWrk

            sWrk = oForm.DataSources.UserDataSources.Item("U_2RRA").ValueEx
            If sWrk = "1" Then
                sWrk = oForm.DataSources.UserDataSources.Item("U_2RRVA").ValueEx
                If sWrk.Length > 0 Then
                    sWrk = sWrk.PadLeft(8, "0")
                Else
                    sWrk = "00000000"
                End If
                sResult = sResult & sWrk
            ElseIf sWrk = "2" Then
                sWrk = oForm.DataSources.UserDataSources.Item("U_2RRVB").ValueEx
                If sWrk.Length = 0 Then
                    sWrk = "00000000"
                End If
                sResult = sResult & sWrk
            Else
                sResult = sResult & "00000000"
            End If

            If sResult.StartsWith("O000NNNNNNN") = False Then
                Dim sCurrentCover As String = getFormDFValue(oForm, "U_Covera")
                Dim sCoverHist As String = getFormDFValue(oForm, "U_CoverHst")
                sCoverHist = sResult & "|" & sCoverHist
                setFormDFValue(oForm, "U_CoverHst", sCoverHist)
                setFormDFValue(oForm, "U_Covera", sResult)
                doDescribeCoverage(oForm)

                Dim sCode As String = getFormDFValue(oForm, "Code")
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse sCode.Length = 0 Then
                    setFormDFValue(oForm, "U_Sched", "Y")
                    setFormDFValue(oForm, "U_USched", "N")
                End If
            End If
        End Sub

        Private Sub doResetCoverage(ByVal oForm As SAPbouiCOM.Form)
            setUFValue(oForm, "U_2RD", "O")
            setUFValue(oForm, "U_2FRE", "000")
            setUFValue(oForm, "U_2FSU", "N")
            setUFValue(oForm, "U_2FMO", "N")
            setUFValue(oForm, "U_2FTU", "N")
            setUFValue(oForm, "U_2FWE", "N")
            setUFValue(oForm, "U_2FTH", "N")
            setUFValue(oForm, "U_2FFR", "N")
            setUFValue(oForm, "U_2FSA", "N")
            setUFValue(oForm, "U_2RRS", "00000000")
            setUFValue(oForm, "U_2RRVA", "00000000")
            setUFValue(oForm, "U_2RRA", "00000000")
        End Sub

        Private Shared Function doRemove(ByVal sCode As String) As Boolean
            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(IDHAddOns.idh.addon.Base.PARENT, msMainTable)
            Try
                Dim iwResult As Integer
                Dim swResult As String = Nothing

                oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_REMOVE)
                iwResult = oAuditObj.Remove()
                If iwResult <> 0 Then
                    IDHAddOns.idh.addon.Base.PARENT.goDICompany.GetLastError(iwResult, swResult)
                    'com.idh.bridge.DataHandler.INSTANCE.doError("System: Error Remove - " + swResult, "Error removing - " & sCode)
                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Remove - " + swResult + ", error removing - " + sCode, "ERSYDBRR", {swResult, sCode})
                    Return False
                Else
                    Return True
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error removing the DO - " & sCode)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRDO", {sCode})
                Return False
            End Try
            Return True
        End Function

        Public Overrides Sub doButtonID2(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            doRemoveThisEntry(oForm)
            doReturnFromCanceled(oForm, BubbleEvent)
        End Sub

        Public Sub doRemoveThisEntry(ByVal oForm As SAPbouiCOM.Form)
            Dim sFormAction As String = getWFValue(oForm, "FORMACTION")
            If sFormAction.Equals("DoAuto") Then
                Dim sMsg As String = com.idh.bridge.Translation.getTranslatedWord("You have selected Cancel button. This will delete the WOR. Please choose you want to...?")
                Dim iSelection As Integer = goParent.doMessage(sMsg, 1, "Save+Cont.", "Delete+Cont.")
                If iSelection = 2 Then
                    Dim sRow As String = getFormDFValue(oForm, "Code")
                    Dim oAdde As WR1_Data.idh.data.AdditionalExpenses = getWFValue(oForm, "JobAdditionalHandler")
                    If Not oAdde Is Nothing Then
                        oAdde.doRemoveRowEntries(sRow)
                    End If
                    doRemove(sRow)
                End If
            ElseIf sFormAction.Equals("IDHGRID") Then
                Dim oData As ArrayList
                If ghOldDialogParams.Contains(oForm.UniqueID) Then
                    oData = ghOldDialogParams.Item(oForm.UniqueID)
                    If Not (oData Is Nothing) AndAlso oData.Count > 2 Then
                        Dim iRow As Integer = oData.Item(1)
                        Dim oGridN As FilterGrid = oData.Item(2)
                        Dim sKey As String = oGridN.doRowKeyLookup(iRow)
                        Dim oAdde As WR1_Data.idh.data.AdditionalExpenses = getWFValue(oForm, "JobAdditionalHandler")
                        'If oGridN.checkRowNew(iRow) Then
                        If oGridN.IsAddedRow(iRow) Then
                            ''Remove all the enries for the above row
                            If Not oAdde Is Nothing Then
                                oAdde.doRemoveRowEntries(sKey)
                            End If
                            oGridN.doRemoveRow(iRow, False)
                        End If
                    End If
                End If
            End If
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Try
                Dim bDoNew As Boolean = False
                If pVal.BeforeAction = True Then
                    'Check If the Prices needs to be looked up
                    Dim oWOR As IDH_JOBSHD = thisWOR(oForm)

                    'doRequestCIPSIPUpdateOnOK(oForm)
                    'If doRequestCIPSIPUpdateOnOK(oForm) Then
                    '    BubbleEvent = False
                    '    Exit Sub
                    'End If

                    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse
                       oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then

                        'Check If the Prices needs to be looked up
                        oWOR.doDelayedPrices()

                        doRequestCIPSIPUpdateOnOK(oForm)

                        Dim sU_JOBSCH As String
                        Dim sU_JOBENTR As String

                        sU_JOBSCH = getFormDFValue(oForm, "Code").Trim()
                        sU_JOBENTR = getFormDFValue(oForm, "U_JobNr").Trim()

                        Dim dCustW As Double = getFormDFValue(oForm, "U_CstWgt")
                        If dCustW > 0 Then
                            Dim sCheckAccountFlags As String = Config.Parameter("WOWNAF")
                            If sCheckAccountFlags.ToUpper.Equals("TRUE") Then
                                If Not (getUFValue(oForm, "IDH_FOC").Equals("Y") OrElse
                                        getUFValue(oForm, "IDH_DOORD").Equals("Y") OrElse
                                        getUFValue(oForm, "IDH_DOARI").Equals("Y")) Then
                                    'If goParent.goApplication.M_essageBox("No Accounting flag was selected. " & Chr(10) & "Do you want to continue?", 1, "Yes", "No") = 2 Then
                                    If com.idh.bridge.res.Messages.INSTANCE.doResourceMessageYNAsk("JBNAFS", Nothing, 1) = 2 Then
                                        BubbleEvent = False
                                        Exit Sub
                                    End If
                                End If
                            End If
                        End If

                        '*** Check the Job Type
                        Dim sOrderType As String
                        sOrderType = getFormDFValue(oForm, "U_JobTp")
                        If sOrderType.Length = 0 Then
                            'com.idh.bridge.DataHandler.INSTANCE.doError("Order Type must be set")
                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Order Type must be set", "ERUSGENS", {com.idh.bridge.Translation.getTranslatedWord("Order Type")})
                            'doSetFocus(oForm, "IDH_JOBTYP")
                            'doSetFocus(oForm, "IDH_JOBTPE")
                            doParkEdit(oForm)
                            BubbleEvent = False
                            Exit Sub
                        End If


                        '*** Check the Container Type
                        Dim sContainer As String
                        sContainer = getFormDFValue(oForm, IDH_JOBSHD._ItemCd)
                        If sContainer.Length = 0 Then
                            'com.idh.bridge.DataHandler.INSTANCE.doError("Container must be set")
                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Container must be set", "ERUSGENS", {com.idh.bridge.Translation.getTranslatedWord("Container")})
                            doSetFocus(oForm, "IDH_ITMCOD")
                            BubbleEvent = False
                            Exit Sub
                        Else
                            If (Config.INSTANCE.getParameterAsBool("VLGRTRCD", True) AndAlso getFormDFValue(oForm, IDH_JOBSHD._TrnCode) IsNot Nothing AndAlso getFormDFValue(oForm, IDH_JOBSHD._TrnCode) = "29") Then
                                Dim sLinkedItemCode As String = Config.INSTANCE.getValueFromOITM(sContainer, OITM._WTITMLN).ToString()
                                If (String.IsNullOrEmpty(sLinkedItemCode)) Then
                                    com.idh.bridge.DataHandler.INSTANCE.doError("Container does not have a linked stock item required for Transaction code Goods Receipt.")
                                    'return "Container does not have a linked stock item required for Transaction code Goods Receipt.";
                                    BubbleEvent = False
                                    Exit Sub
                                End If
                                If (Not Config.INSTANCE.isStockItem(sLinkedItemCode)) Then
                                    com.idh.bridge.DataHandler.INSTANCE.doError("Container does not have a linked stock item required for Transaction code Goods Receipt.")
                                    'return "Container does not have a linked stock item required for Transaction code Goods Receipt.";
                                    BubbleEvent = False
                                    Exit Sub
                                End If
                            End If
                        End If

                        '*** Check for a valid wastecode
                        If Config.INSTANCE.getParameterAsBool("WOCWC", True) = True Then
                            Dim sWastCd As String = getFormDFValue(oForm, "U_WasCd")
                            If Config.INSTANCE.doCheckIsValidItem(sWastCd) = False Then
                                Dim saParam As String() = {sWastCd}
                                doResError("Incorrect Waste Code entered: " & sWastCd, "ERRICWC", saParam)
                                doSetFocus(oForm, "IDH_WASCL1")
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If
                        '*** Check for a valid UOM
                        'OnTime 22323: UOM validation
                        'Note: validate UOM right in the begining of the price calculation
                        Dim sUOM As String = getFormDFValue(oForm, "U_UOM")
                        'if it is empty or 'CM' dont validated
                        'Note: 'CM' UOM is coming from business logic: so it doesn't exist in the OWGT table
                        If (Not (sUOM = String.Empty)) And
                               (Not (sUOM.ToUpper() = "cm".ToUpper())) Then
                            If Not isValidUOM(oForm, sUOM) Then
                                Dim saParam As String() = {sUOM}
                                doSetFocus(oForm, "IDH_UOM")
                                doResError("Incorrect UOM entered: " & sUOM, "INVLDUOM", saParam)
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If

                        '*** now validate PUOM
                        'OnTime 22323: UOM validation
                        Dim sPUOM As String = getFormDFValue(oForm, "U_PUOM")
                        'if it is empty or 'CM' dont validate
                        'Note: 'CM' UOM is coming from business logic: so it doesn't exist in the OWGT table
                        If (Not (sPUOM = String.Empty)) And
                               (Not (sPUOM.ToUpper() = "cm".ToUpper())) Then
                            If Not isValidUOM(oForm, sPUOM) Then
                                Dim saParam As String() = {sPUOM}
                                doSetFocus(oForm, "IDH_PUOM")
                                doResError("Incorrect PUOM entered: " & sPUOM, "INVLDUOM", saParam)
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If

                        ''##MA Start 03-09-2014 Issue#423
                        '*** Check for a vehicle if start date is provided
                        If Config.INSTANCE.getParameterAsBool("VALIDVEH", True) = True Then
                            Dim sASDate As String = getFormDFValue(oForm, IDH_JOBSHD._ASDate)
                            Dim sVehicle As String = getFormDFValue(oForm, IDH_JOBSHD._Lorry)
                            ' Dim sVehicleCd As String = getFormDFValue(oForm, IDH_JOBSHD._LorryCd)
                            If sASDate.Trim <> "" AndAlso sVehicle.Trim = "" Then
                                'com.idh.bridge.DataHandler.INSTANCE.doError("Vehicle must be enter to start WOR")
                                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Vehicle must be entered to start WOR", "ERUSEVEH", {Nothing})
                                doSetFocus(oForm, "IDH_LORRY")
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If
                        ''##MA End 03-09-2014 Issue#423

                        'Dimeca - Required validation for Selected Order Type as per Order Type Setup 
                        If IsOrderTypeCommentsRequired(getFormDFValue(oForm, IDH_JOBSHD._JobTp)) Then
                            Dim sOrderTypeComm As String = getFormDFValue(oForm, IDH_JOBSHD._OTComments)
                            If sOrderTypeComm.Length < 1 Then
                                'com.idh.bridge.DataHandler.INSTANCE.doError("The selected Order Type requires comments. Please provide reason in Comments Tab--> Order Type Comments field.")
                                com.idh.bridge.DataHandler.INSTANCE.doResUserError("The selected Order Type requires comments. Please provide reason in Comments Tab--> Order Type Comments field.", "ERUSCOMM", {Nothing})
                                doSetFocus(oForm, "IDH_OTCOM")
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If

                        Dim bFoc As String = getUFValue(oForm, "IDH_FOC")
                        Dim sCustomer As String = getFormDFValue(oForm, "U_CustCd")
                        Dim sStatus As String = getFormDFValue(oForm, "U_Status")
                        Dim dChargeTotal As Double = 0
                        If bFoc.Equals("Y") Then
                            'If goParent.goApplication.M_essageBox("The Disposal order will not create a Marketing Document. " & Chr(10) & "Do you want to continue?", 1, "Yes", "No") = 2 Then
                            If com.idh.bridge.res.Messages.INSTANCE.doResourceMessageYNAsk("WORNMD", Nothing, 1) = 2 Then
                                BubbleEvent = False
                                Exit Sub
                            End If
                            '                        ElseIf com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) OrElse _
                            '                        	com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus) Then

                            'CR 20150601: The Credit Check build for USA Clients will now be used for all under new WR Config Key "NEWCRCHK" 
                            'ElseIf com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) AndAlso Config.INSTANCE.getParameterAsBool("USAREL", False) = False Then
                        ElseIf com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) AndAlso Config.INSTANCE.getParameterAsBool("NEWCRCHK", False) = False Then
                            dChargeTotal = getUnCommittedRowTotals(oForm)
                            'Non-USA Release Credit Check
                            If Config.INSTANCE.doCheckCredit(sCustomer, dChargeTotal, True) = False Then
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If

                        'Credit chek for USA Release
                        'OnTime [#Ico000????] USA
                        'START
                        'USA Release

                        '*** NOTE: This Credit Check triggers when order is saved
                        dChargeTotal = getUnCommittedRowTotals(oForm)
                        'Dim bCChkPass As Boolean = False

                        'For Credit Check: If ADD mode do as it is
                        'For Credit Check: If UPDATE mode do credit check only if order value has been changed
                        Dim bOrderValueChanged As Boolean = False

                        'If oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            doWarnInActiveBPnSites(oForm)
                            Dim sOldOrderTotal As String
                            Dim sNewOrderTotal As String

                            sNewOrderTotal = getFormDFValue(oForm, "U_Total")
                            sOldOrderTotal = getWFValue(oForm, "CCTotal")

                            If Not sOldOrderTotal.Equals(sNewOrderTotal) Then
                                bOrderValueChanged = True
                            End If
                        End If
                        'CR 20150601: The Credit Check build for USA Clients will now be used for all under new WR Config Key "NEWCRCHK" 
                        'If (Config.INSTANCE.getParameterAsBool("USAREL", False) = True) AndAlso bOrderValueChanged Then
                        If (Config.INSTANCE.getParameterAsBool("NEWCRCHK", False) = True) AndAlso bOrderValueChanged Then
                            Dim sHeaderMsg As String = String.Empty
                            Dim oParentForm As SAPbouiCOM.Form = goParent.doGetParentForm(oForm.UniqueID)
                            'Company level Credit Check
                            If Config.INSTANCE.getParameterAsBool("MDCRDO", False) Then
                                'Load information from Credit Check View
                                Dim oResult As DataRecords
                                Dim sQry As String = "SELECT bp.validFor, bp.validFrom, bp.validTo, " &
                                    " v.CardCode, v.CreditLine,  v.Balance, v.OrdersBal, " &
                                    " v.DNotesBal, v.TBalance, v.WRBalance, v.RemainBal, v.Deviation, " &
                                    " v.frozenFor, v.FrozenComm, v.IDHICL, v.frozenFrom, v.frozenTo, " &
                                    " bp.GroupNum " &
                                    " FROM IDH_VWR1BAL v, OCRD bp " &
                                    " WHERE v.CardCode = bp.CardCode AND v.CardCode = '" + sCustomer + "'"
                                oResult = DataHandler.INSTANCE.doBufferedSelectQuery("doCheckCredit", sQry)
                                Dim sIgnoreCheck As String
                                Dim sPaymentTermCode As String
                                Dim dTBalance As Double
                                Dim dWROrders As Double
                                Dim sActive As String
                                Dim sActiveFrom As String
                                Dim sActiveTo As String
                                Dim sFrozen As String
                                Dim sFrozenComm As String


                                Dim sFrozenFrom As String
                                Dim sFrozenTo As String
                                Dim dCreditLimit As Double
                                Dim dCreditRemain As Double
                                Dim dDeviationPrc As Double


                                If oResult.RecordCount > 0 Then
                                    sIgnoreCheck = oResult.getValueAsStringNotNull("IDHICL")
                                    sPaymentTermCode = oResult.getValueAsStringNotNull("GroupNum")
                                    dTBalance = oResult.getValueAsDouble("TBalance")
                                    dWROrders = oResult.getValueAsDouble("WRBalance")

                                    sActive = oResult.getValueAsString("validFor")
                                    sActiveFrom = oResult.getValueAsString("validFrom")
                                    sActiveTo = oResult.getValueAsString("validTo")

                                    sFrozen = oResult.getValueAsString("frozenFor")
                                    sFrozenComm = oResult.getValueAsString("FrozenComm")
                                    sFrozenFrom = oResult.getValueAsString("frozenFrom")
                                    sFrozenTo = oResult.getValueAsString("frozenTo")
                                    dCreditLimit = oResult.getValueAsDouble("CreditLine")
                                    dCreditRemain = oResult.getValueAsDouble("RemainBal")
                                    dDeviationPrc = oResult.getValueAsDouble("Deviation")

                                    'Release oResult object
                                    oResult = Nothing
                                    'Calculate total balance including this WOR charge amount
                                    dTBalance = dTBalance + dChargeTotal

                                    'See if the WOH status is not 'Quote'
                                    'For a Quote WOH there is NO Credit Check
                                    Dim oWOHRS As SAPbobsCOM.Recordset
                                    Dim sHeaderStatus As String = "-1"
                                    oWOHRS = goParent.goDB.doSelectQuery("select U_Status from [@IDH_JOBENTR] where code = " + sU_JOBENTR)
                                    If oWOHRS.RecordCount > 0 Then
                                        sHeaderStatus = oWOHRS.Fields.Item("U_Status").Value
                                    End If
                                    IDHAddOns.idh.data.Base.doReleaseObject(oWOHRS)

                                    'BP exempt from credit check
                                    If sIgnoreCheck <> "Y" AndAlso Not sHeaderStatus.Equals(Config.ParameterWithDefault("MDQUOTCD", "8")) Then
                                        'BP is Cash Customer or Other

                                        If Config.ParameterWithDefault("MDCPTC", "0") = sPaymentTermCode Then
                                            goParent.doMessage("Customer is CIA")
                                            'cash customer
                                            If Config.INSTANCE.doCheckCreditCashCustomer(sCustomer, dTBalance, dWROrders) = False Then
                                                'Set Order to DRAFT
                                                'Update WOH with status = "DRAFT"
                                                Dim sUpdateQry = "Update [@IDH_JOBENTR] set U_Status = '6' where Code = '" & sU_JOBENTR & "'"
                                                DataHandler.INSTANCE.doUpdateQuery(sUpdateQry)

                                                setSharedData(oParentForm, "WOTODRAFT", Config.ParameterWithDefault("MDDRFTCD", "6"))
                                                'setFormDFValueFromUser(oForm, "U_Status", Config.ParameterWithDefault("MDDRFTCD", "6"), True)
                                                sHeaderMsg = "Over Credit Limit"
                                                doSendCreditCheckAlertAndMessage(oForm, sCustomer, sHeaderMsg, "CCCASHAL", "CCCALERT")
                                            Else
                                                setSharedData(oParentForm, "WOTODRAFT", "-1")
                                            End If
                                            'If Config.INSTANCE.doCheckCreditCashCustomer(goParent, sCustomer, dTBalance, dWROrders) = False Then
                                            '    'Update WOH with status = "DRAFT"
                                            '    'Changing as on 20120831: change the status to Draft
                                            '    sQry = "Update [@IDH_JOBENTR] set U_Status = '6' where Code = '" & sU_JOBENTR & "'"
                                            '    DataHandler.INSTANCE.doUpdateQuery(sQry)

                                            '    'Do alert the super users
                                            '    'OLD

                                            '    'Dim sAlertusers As String = Config.ParameterWithDefault("MDCDAL", "")
                                            '    'If sAlertusers IsNot Nothing AndAlso sAlertusers.Length > 0 Then
                                            '    '    Dim sAlertMsg As String = Config.ParameterWithDefault("CCHKAMSG", "Credit check alert!")
                                            '    '    goParent.doSendAlert(sAlertusers.Split(","), "Credit Limit Alert for: " & sCustomer, sAlertMsg, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner, sCustomer, True)
                                            '    'End If
                                            '    'Alert Users

                                            '    Dim sCCAlertUsers As String = Config.ParameterWithDefault("CCCASHAL", "")
                                            '    If sCCAlertUsers IsNot Nothing AndAlso sCCAlertUsers.Length > 0 Then
                                            '        Dim sAlertMsg As String = Config.ParameterWithDefault("CCCALERT", "CIA Credit check alert!")
                                            '        sAlertMsg = sAlertMsg + Chr(13)
                                            '        sAlertMsg = sAlertMsg + "Work Order: " + getFormDFValue(oForm, "Code") + Chr(13)
                                            '        sAlertMsg = sAlertMsg + "From User: " + goParent.gsUserName + Chr(13)
                                            '        sAlertMsg = sAlertMsg + "Cc: " + sCCAlertUsers
                                            '        If Not sCCAlertUsers.Contains(goParent.gsUserName) Then
                                            '            sCCAlertUsers = sCCAlertUsers + "," + goParent.gsUserName
                                            '        End If
                                            '        goParent.doSendAlert(sCCAlertUsers.Split(","), "Over Credit Limit: " & sCustomer, sAlertMsg, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner, sCustomer, True)
                                            '    End If
                                            '    'Changing as on 20120831: change the status to Draft
                                            '    bCChkPass = False
                                            'Else
                                            '    bCChkPass = True
                                            'End If
                                        Else
                                            'account customer
                                            Dim bPayTermLimitPass As Boolean = False
                                            Dim bCreditCheckPass As Boolean = False
                                            'Dim sMessage As String
                                            Dim bDoAlert As Boolean = True
                                            Dim bDoDraft As Boolean = False
                                            Dim sMessageType As String = Nothing


                                            Dim oActiveFrom As Date = com.idh.utils.Dates.doStrToDate(sActiveFrom)
                                            Dim oActiveTo As Date = com.idh.utils.Dates.doStrToDate(sActiveTo)
                                            Dim oFrozenFrom As Date = com.idh.utils.Dates.doStrToDate(sFrozenFrom)
                                            Dim oFrozenTo As Date = com.idh.utils.Dates.doStrToDate(sFrozenTo)

                                            If com.idh.bridge.lookups.Config.INSTANCE.doCheckBPActive(sActive, oActiveFrom, oActiveTo, sFrozen, sFrozenComm, oFrozenFrom, oFrozenTo, DateTime.Now(), True) = True Then
                                                'if BP is not frozen
                                                If Config.INSTANCE.getParameterAsBool("DPTLCHK", False) Then 'DoPaymentTermLimitCheck
                                                    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                                                    Dim sINVQry As String = ""
                                                    sINVQry = sINVQry & "SELECT DocDueDate + " + Config.ParameterWithDefault("PTGRCPRD", "0") + " AS BufferDocDueDate, "
                                                    sINVQry = sINVQry & " DocEntry, "
                                                    sINVQry = sINVQry & " CardCode "
                                                    sINVQry = sINVQry & "FROM   OINV "
                                                    sINVQry = sINVQry & "WHERE  CardCode = '" + sCustomer + "' "
                                                    sINVQry = sINVQry & "       AND DocDueDate + " + Config.ParameterWithDefault("PTGRCPRD", "0") + " <= GetDate() "
                                                    sINVQry = sINVQry & "       AND DocStatus = 'O' "

                                                    oRecordSet = goParent.goDB.doSelectQuery(sINVQry)
                                                    If oRecordSet.RecordCount > 0 Then
                                                        bPayTermLimitPass = False
                                                    Else
                                                        bPayTermLimitPass = True
                                                    End If
                                                    IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
                                                Else
                                                    bPayTermLimitPass = True
                                                End If
                                                'If Config.INSTANCE.getParameterAsBool("DPTLCHK", False) Then 'DoPaymentTermLimitCheck
                                                '    'bPayTermLimitPass = Config.INSTANCE.doCheckCreditAdvance( _
                                                '    '    goParent, sCustomer, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc, _
                                                '    '    0, bDoAlert, False, Nothing, bDoDraft)
                                                '    bPayTermLimitPass = True
                                                'Else
                                                '    bPayTermLimitPass = True
                                                'End If

                                                If bPayTermLimitPass Then
                                                    If Config.INSTANCE.getParameterAsBool("DCRLCHK", False) Then 'DoCreditLimitCheck
                                                        Config.INSTANCE.doCheckCreditAdvance(
                                                            sCustomer, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc,
                                                            0, bDoAlert, False, Nothing, bDoDraft, sMessageType)

                                                        If bDoDraft Then
                                                            'Update WOH with status = "DRAFT"
                                                            Dim sUpdateQry = "Update [@IDH_JOBENTR] set U_Status = '6' where Code = '" & sU_JOBENTR & "'"
                                                            DataHandler.INSTANCE.doUpdateQuery(sUpdateQry)

                                                            setSharedData(oParentForm, "WOTODRAFT", Config.ParameterWithDefault("MDDRFTCD", "6"))
                                                            'setFormDFValueFromUser(oForm, "U_Status", Config.ParameterWithDefault("MDDRFTCD", "6"), True)
                                                        Else
                                                            setSharedData(oParentForm, "WOTODRAFT", "-1")
                                                        End If

                                                        If sMessageType = "NEARING" Then
                                                            sHeaderMsg = "Customer NEARING Credit Limit"
                                                        ElseIf sMessageType = "OVER" Then
                                                            sHeaderMsg = "Customer OVER Credit Limit"
                                                        ElseIf sMessageType = "NONE" Then
                                                            sHeaderMsg = String.Empty
                                                        End If

                                                        If sHeaderMsg.Length > 0 Then
                                                            doSendCreditCheckAlertAndMessage(oForm, sCustomer, sHeaderMsg, "MDCDAL", "CCHKAMSG")
                                                        End If
                                                    End If

                                                Else
                                                    'Update WOH with status = "DRAFT"
                                                    Dim sUpdateQry = "Update [@IDH_JOBENTR] set U_Status = '6' where Code = '" & sU_JOBENTR & "'"
                                                    DataHandler.INSTANCE.doUpdateQuery(sUpdateQry)

                                                    setSharedData(oParentForm, "WOTODRAFT", Config.ParameterWithDefault("MDDRFTCD", "6"))
                                                    'setFormDFValueFromUser(oForm, "U_Status", Config.ParameterWithDefault("MDDRFTCD", "6"), True)
                                                    sHeaderMsg = "Over Credit Terms"
                                                    doSendCreditCheckAlertAndMessage(oForm, sCustomer, sHeaderMsg, "MDCDAL", "CCHKAMSG")
                                                End If
                                                'If Config.INSTANCE.getParameterAsBool("DCRLCHK", False) Then 'DoCreditLimitCheck
                                                '    bCreditCheckPass = Config.INSTANCE.doCheckCreditAdvance( _
                                                '        goParent, sCustomer, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc, _
                                                '        0, bDoAlert, False, Nothing, bDoDraft)
                                                'End If

                                                ''For both Payment Terms OR Credit Check failure
                                                ''If any of the checks fail create the order as draft
                                                'If (Not bCreditCheckPass) Or (Not bPayTermLimitPass) Then
                                                '    'Update WOH with status = "DRAFT"
                                                '    sQry = "Update [@IDH_JOBENTR] set U_Status = '6' where Code = '" & sU_JOBENTR & "'"
                                                '    DataHandler.INSTANCE.doUpdateQuery(sQry)

                                                '    'Do alert the super users
                                                '    If bDoAlert Then

                                                '        Dim sAlertusers As String = Config.ParameterWithDefault("MDCDAL", "")
                                                '        If sAlertusers IsNot Nothing AndAlso sAlertusers.Length > 0 Then

                                                '            Dim sAlertMsg As String = Config.ParameterWithDefault("CCHKAMSG", "Credit check alert!")
                                                '            sAlertMsg = sAlertMsg + Chr(13)
                                                '            sAlertMsg = sAlertMsg + "Work Order: " + getFormDFValue(oForm, "Code") + Chr(13)
                                                '            sAlertMsg = sAlertMsg + "From User: " + goParent.gsUserName + Chr(13)
                                                '            sAlertMsg = sAlertMsg + "Cc: " + sAlertusers
                                                '            If Not sAlertusers.Contains(goParent.gsUserName) Then
                                                '                sAlertusers = sAlertusers + "," + goParent.gsUserName
                                                '            End If
                                                '            goParent.doSendAlert(sAlertusers.Split(","), "Credit Limit Alert for: " & sCustomer, sAlertMsg, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner, sCustomer, True)
                                                '        End If
                                                '    End If

                                                'Else
                                                '    bCChkPass = True
                                                'End If
                                            Else
                                                'As BP is frozen do nothing
                                                setSharedData(oParentForm, "WOTODRAFT", "-1")
                                            End If
                                        End If
                                    Else
                                        'when bp is ignored for credit check
                                        '***DO NOTHING
                                        'bCChkPass = True
                                    End If
                                Else
                                    'when there is no record in Credit Check View
                                    '***DO NOTHING
                                    'bCChkPass = True
                                End If
                                'oResult = Nothing
                            Else
                                'there is no company level credit check
                                'bCChkPass = True
                            End If

                            'Dim oParentForm As SAPbouiCOM.Form = goParent.doGetParentForm(oForm.UniqueID)
                            'If bCChkPass = False Then
                            '    setSharedData(oParentForm, "WOTODRAFT", Config.ParameterWithDefault("MDDRFTCD", "6"))
                            '    'BubbleEvent = False
                            '    'Exit Sub
                            'Else
                            '    setSharedData(oParentForm, "WOTODRAFT", "-1")
                            'End If
                        End If
                        'END
                        'OnTime [#Ico000????] USA

                        Dim sAddress As String = getWFValue(oForm, "STADDR")
                        Dim sCustRef As String = getFormDFValue(oForm, "U_CustRef")
                        If Config.INSTANCE.doGetPOLimitsDOWO("WO", sCustomer, sAddress, sCustRef, dChargeTotal) = False Then
                            BubbleEvent = False
                            Exit Sub
                        End If

                        'Check if the Customer ref is required
                        If sCustRef Is Nothing OrElse sCustRef.Length = 0 Then
                            'If Config.INSTANCE.doGetCustRefRequired(goParent, sCustomer, sAddress) = True Then
                            If Config.INSTANCE.doGetCustRefRequired(sCustomer, sAddress) = True Then
                                'Dim sMessage As String = goParent.goMessages.getMessage("WORCRR", Nothing)
                                doResError("Error doing Button 1.", "WORCRR", Nothing)
                                doSetFocus(oForm, "IDH_CUSREF")
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If

                        'Dim sCustCs As String = getFormDFValue( oForm, "U_CustCs")

                        '20151201: DIMECA Validation likewise in Disposal Order 
                        'Start 
                        Dim sTransCd As String = getFormDFValue(oForm, IDH_JOBSHD._TrnCode) 'getSharedData(oForm, "TRNCODE")
                        Dim sTransDsc As String = String.Empty
                        If String.IsNullOrWhiteSpace(sTransCd) = False Then
                            Dim oTransCode As IDH_TRANCD = New IDH_TRANCD()
                            If oTransCode.getByKey(sTransCd) Then
                                sTransDsc = oTransCode.U_Process
                            End If

                            'Check if Waste Code is 'Purchase Over Weighbridge ' 
                            Dim sIDHCWBB As String = Config.INSTANCE.getValueFromOITM(getFormDFValue(oForm, IDH_JOBSHD._WasCd), "U_IDHCWBB").ToString
                            Dim bWastePurOverWB As Boolean = sIDHCWBB.Equals("Y")

                            'Determine Transaction Code by attributes 
                            Dim sTCNature As String = String.Empty 'SELLINGWO or BUYINGWO or INTERNALTRANSWO

                            'If Config.ParameterAsBool("DIMWOSO", False) Then
                            '    If (oTransCode.U_MarkDoc1 = "17" OrElse oTransCode.U_MarkDoc2 = "17" _
                            '            OrElse oTransCode.U_MarkDoc3 = "17" OrElse oTransCode.U_MarkDoc4 = "17") _
                            '            AndAlso Config.INSTANCE.isOutgoingJob(Config.ParameterWithDefault("WOSODGRP", Config.INSTANCE.doGetDOContainerGroup()), oTransCode.U_OrdType) Then
                            '        'Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), oTransCode.U_OrdType)
                            '        'OR
                            '        'Config.INSTANCE.isOutgoingJob("201", oTransCode.U_OrdType) 
                            '        sTCNature = "SELLINGWO"
                            '    End If
                            'End If

                            If Config.ParameterAsBool("DIMWOBUY", False) Then
                                If (oTransCode.U_MarkDoc1 = "22" OrElse oTransCode.U_MarkDoc2 = "22" _
                                        OrElse oTransCode.U_MarkDoc3 = "22" OrElse oTransCode.U_MarkDoc4 = "22") _
                                        AndAlso Config.INSTANCE.isIncomingJob(Config.ParameterWithDefault("WOBUYDGP", Config.INSTANCE.doGetDOContainerGroup()), oTransCode.U_OrdType) Then

                                    sTCNature = "BUYINGWO"
                                End If
                            End If

                            'If Config.ParameterAsBool("DIMWOST", False) Then
                            '    If oTransCode.U_MarkDoc1 = "" AndAlso oTransCode.U_MarkDoc2 = "" _
                            '            AndAlso oTransCode.U_MarkDoc3 = "" AndAlso oTransCode.U_MarkDoc4 = "" _
                            '            AndAlso oTransCode.U_Stock = "67" _
                            '            AndAlso Config.INSTANCE.isOutgoingJob(Config.ParameterWithDefault("WOSTDGRP", Config.INSTANCE.doGetDOContainerGroup()), oTransCode.U_OrdType) Then

                            '        sTCNature = "INTERNALTRANSWO"
                            '    End If
                            'End If

                            ''Sales WO 
                            'If Config.ParameterAsBool("DIMWOSO", False) Then
                            '    'Rule: Outgoing DO + must have WOH and WOR + Selling Transaction Code 
                            '    If sTCNature = "SELLINGWO" Then
                            '        If getFormDFValue(oForm, IDH_JOBSHD._WROrd).ToString().Length <= 0 _
                            '                OrElse getFormDFValue(oForm, IDH_JOBSHD._WRRow).ToString().Length <= 0 _
                            '                OrElse Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), getFormDFValue(oForm, IDH_JOBSHD._JobTp)) = False Then
                            '            doResError("Config (DIMWOSO) Validation rule failed.", "EREWOSO", Nothing)
                            '            BubbleEvent = False
                            '            Exit Sub
                            '        End If
                            '    End If
                            'End If

                            'Buying WO
                            If Config.ParameterAsBool("DIMWOBUY", False) Then
                                'Rule: Buying Transaction Code + Must have Price 
                                If sTCNature = "BUYINGWO" Then
                                    'Config.INSTANCE.isIncomingJob(Config.ParameterWithDefault("WOBUYDGP", Config.INSTANCE.doGetDOContainerGroup()), oTransCode.U_OrdType) 
                                    'If Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), getFormDFValue(oForm, IDH_JOBSHD._JobTp)) = False _
                                    '        OrElse bWastePurOverWB = False _
                                    '        OrElse getFormDFValue(oForm, IDH_JOBSHD._PCost) <= 0 Then
                                    '    doResError("Config (DIMWOBUY) Validation rule failed.", "EREWOBUY", Nothing)
                                    '    BubbleEvent = False
                                    '    Exit Sub
                                    'End If

                                    '20160113: Had testing together with KA & HG and commented above block 
                                    'Decided there is no need to check the job type 
                                    'and look for PurOverWB and the PCost only 
                                    If bWastePurOverWB = True _
                                            AndAlso getFormDFValue(oForm, IDH_JOBSHD._PCost) <= 0 Then
                                        doResError("Config (DIMWOBUY) Validation rule failed.", "EREWOBUY", Nothing)
                                        BubbleEvent = False
                                        Exit Sub
                                    End If
                                End If
                            End If

                            ''Internal Stock Transfer WO 
                            'If Config.ParameterAsBool("DIMWOST", False) Then
                            '    'Rule: Outgoing WO + must have WOH and WOR + Internal Transfer Transaction Code 
                            '    If sTCNature = "INTERNALTRANSWO" Then
                            '        If getFormDFValue(oForm, IDH_JOBSHD._WROrd).ToString().Length <= 0 _
                            '                OrElse getFormDFValue(oForm, IDH_JOBSHD._WRRow).ToString().Length <= 0 _
                            '                OrElse Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), getFormDFValue(oForm, IDH_JOBSHD._JobTp)) = False Then
                            '            doResError("Config (DIMWOST) Validation rule failed.", "EREWOST", Nothing)
                            '            BubbleEvent = False
                            '            Exit Sub
                            '        End If
                            '    End If
                            'End If
                        End If
                        'End 

                        Dim sDoAdd As String = ""
                        If ghOldDialogParams.Contains(oForm.UniqueID) Then
                            Dim oData As ArrayList = ghOldDialogParams.Item(oForm.UniqueID)
                            If Not (oData Is Nothing) AndAlso oData.Count > 1 Then
                                sDoAdd = oData.Item(0)
                            End If
                        End If

                        '*** IF NO UPDATE SET SET MODE TO OK
                        '*** DATA WILL BE UPDATED BY CALLER
                        doCreateRecureString(oForm)

                        If sDoAdd = "NoUpdate" OrElse sDoAdd = "IDHGRID" Then
                            If doValidateHeader(oForm) = False Then
                                BubbleEvent = False
                                Exit Sub
                            End If

                            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                bDoNew = True
                            End If
                            doAfterBtn1(oForm, bDoNew, False)

                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                            BubbleEvent = False
                        Else
                            oForm.Freeze(True)
                            Try
                                If doValidateHeader(oForm) = False Then
                                    BubbleEvent = False
                                    Exit Sub
                                End If

                                Dim bDoContinue As Boolean = False
                                goParent.goDICompany.StartTransaction()

                                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    bDoNew = True
                                End If
                                bDoContinue = doUpdateRow(oForm)

                                If bDoContinue = True Then
                                    If goParent.goDICompany.InTransaction Then
                                        goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                                    End If
                                    com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()

                                    'Dim oWrkWOR As com.isb.dbObjects.User.IDH_JOBSHD = New com.isb.dbObjects.User.IDH_JOBSHD()
                                    'If String.IsNullOrWhiteSpace(sU_JOBSCH) = False Then
                                    '    oWrkWOR.SBOFormGUIOnly = oForm
                                    '    If oWrkWOR.getByKey(sU_JOBSCH) Then
                                    '        oWrkWOR.doSaveSampleTask()
                                    '        oWrkWOR.doUpdateFinalBatchQty()
                                    '        oWrkWOR.doSentNCRAlert()
                                    '        oWrkWOR.doMarketingDocuments()
                                    '    End If
                                    'End If

                                    doAfterBtn1(oForm, bDoNew, True)
                                    If bDoNew = True Then
                                        If goParent.doCheckModal(oForm.UniqueID) = False Then
                                            doCreateNewEntry(oForm)
                                        Else
                                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                                            BubbleEvent = False
                                        End If
                                    End If

                                    'If goParent.goDICompany.InTransaction Then
                                    '    goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                                    'End If
                                    'com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
                                    'doAfterBtn1(oForm, bDoNew, True)
                                    ''doCreateRecuringJobs(oForm)
                                Else
                                    If goParent.goDICompany.InTransaction Then
                                        goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                    End If
                                    com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
                                End If


                                'oForm.Update()
                            Catch ex As Exception
                                If goParent.goDICompany.InTransaction Then
                                    goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)

                                End If
                                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error doing Button 1.")
                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXBNTO", {Nothing})
                                com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()

                            Finally
                                oForm.Freeze(False)

                            End Try
                        End If
                        'ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        '    oForm.Freeze(True)
                        '    Try
                        '        Dim sCode As String = getFormDFValue(oForm, "Code")
                        '        doLoadRowFromCode(oForm, sCode)

                        '        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        '        doLoadData(oForm)

                        '        'oForm.Update()
                        '        BubbleEvent = False
                        '    Catch ex As Exception
                        '        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing button 1.")
                        '    End Try
                        '    oForm.Freeze(False)
                    ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        If goParent.doCheckModal(oForm.UniqueID) = True Then
                            doSetRowData(oForm)
                            BubbleEvent = False
                        End If
                    ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        oForm.Freeze(True)
                        Try
                            Dim sCode As String = getItemValue(oForm, "IDH_JOBSCH")
                            If doLoadRowFromCode(oForm, sCode) = True Then
                                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                                doLoadDataF(oForm, True)
                            End If
                            BubbleEvent = False
                        Catch ex As Exception
                            'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing button 1.")
                            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXBNTO", {Nothing})
                        End Try
                        oForm.Freeze(False)
                    End If
                Else
                    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        doLoadData(oForm)
                    ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        oForm.Freeze(True)
                        Try
                            Dim sCode As String = getFormDFValue(oForm, "Code")
                            If doLoadRowFromCode(oForm, sCode) = True Then
                                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                                doLoadDataF(oForm, True)
                            End If
                            'oForm.Update()
                            BubbleEvent = False
                        Catch ex As Exception
                            'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing button 1.")
                            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXBNTO", {Nothing})
                        End Try
                        oForm.Freeze(False)
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing button 1.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXBNTO", {Nothing})
            End Try
        End Sub

        Private Sub doWarnInActiveBPnSites(ByVal oForm As SAPbouiCOM.Form)
            Try
                If Config.ParameterAsBool("SHWINACW", False) = False Then
                    Exit Sub
                End If
                Dim sValue As String = ""

                Dim sBPCodes As String() '= {""}
                Dim sAddresses As String() '= {""}

                sBPCodes = {"Customer", "IDH_CUST", "Supplier", "IDH_SUPCOD", "Carrier", "IDH_CARRIE",
                                              "Disposal Site", "IDH_TIPPIN"}

                sAddresses = {"Customer Address", "IDH_CUST", "IDH_ADDR",
                                        "Disposal Site Address", "IDH_TIPPIN", "IDH_SITADD"}
                Dim sLabel, sBPField, sBPCode, sAddressField As String
                Dim oBPParam() As String = {"", ""}
                Dim oAddressParam() As String = {"", ""}
                For iitem As Int32 = 0 To sBPCodes.Count - 1 Step 2
                    sLabel = sBPCodes(iitem)
                    sBPField = sBPCodes(iitem + 1)
                    sValue = getItemValue(oForm, sBPField)
                    If sValue IsNot Nothing AndAlso sValue.Trim <> "" AndAlso Not Config.INSTANCE.doCheckBPActive(sValue, DateTime.Now) Then
                        oBPParam(0) = com.idh.bridge.Translation.getTranslatedWord(sLabel)
                        oBPParam(1) = sValue
                        doWarnMess(com.idh.bridge.res.Messages.getGMessage("WRINACBP", oBPParam), SAPbouiCOM.BoMessageTime.bmt_Short)
                    End If
                Next
                For iitem As Int32 = 0 To sAddresses.Count - 1 Step 3
                    sLabel = sAddresses(iitem)
                    sBPField = sAddresses(iitem + 1)
                    sAddressField = sAddresses(iitem + 2)

                    sValue = getItemValue(oForm, sAddressField)
                    sBPCode = getItemValue(oForm, sBPField)
                    If sValue IsNot Nothing AndAlso sValue.Trim <> "" AndAlso Not Config.INSTANCE.doCheckBPAddressActive(sBPCode, sValue) Then
                        oAddressParam(0) = Translation.getTranslatedWord(sLabel)
                        oAddressParam(1) = sValue
                        'oAddressParam(2) = sBPField


                        doWarnMess(com.idh.bridge.res.Messages.getGMessage("WRINACAD", oAddressParam), SAPbouiCOM.BoMessageTime.bmt_Short)
                    End If
                Next
                'Next
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "doWarnInActiveBPnSites", {Nothing})
            End Try
        End Sub
        Public Function getUnCommittedRowTotals(ByVal oForm As SAPbouiCOM.Form) As Double
            Dim sRow As String = getFormDFValue(oForm, "Code")
            Dim dChargeTotal As Double = getFormDFValue(oForm, "U_Total")

            If ghOldDialogParams.Contains(oForm.UniqueID) Then
                Dim oPForm As SAPbouiCOM.Form = goParent.doGetParentForm(oForm)
                If oPForm.TypeEx.Equals("IDH_WASTORD") Then
                    Dim oWOForm As idh.forms.orders.Waste = getIDHForm(oPForm)
                    dChargeTotal = oWOForm.getUnCommittedRowTotals(oPForm, sRow, dChargeTotal)
                End If
            End If
            Return dChargeTotal
        End Function

        Public Overridable Function doAfterBtn1(ByVal oForm As SAPbouiCOM.Form, ByVal bDoNew As Boolean, ByVal bMustDoUpdates As Boolean) As Boolean
            'Dim oWOR As IDH_JOBSHD = thisWOR(oForm)

            '** Check for the auto collect jobs
            If bDoNew Then
                Dim iAutoCollectDays As Integer = 0
                Dim sAutoCollectDays As String = Config.Parameter("WOACDA")
                If Not sAutoCollectDays Is Nothing AndAlso sAutoCollectDays.Length > 0 Then
                    Try
                        iAutoCollectDays = ToInt(sAutoCollectDays)
                    Catch ex As Exception
                        iAutoCollectDays = 0
                    End Try
                    If iAutoCollectDays > 0 Then

                        Dim sJobs As String = Config.Parameter("WOACJS")
                        If Not sJobs Is Nothing AndAlso sJobs.Length > 0 Then
                            Dim sOrderType As String
                            Dim saJobs() As String = sJobs.Split(",")
                            sOrderType = getFormDFValue(oForm, "U_JobTp")
                            If System.Array.IndexOf(saJobs, sOrderType) > -1 Then
                                'If goParent.goApplication.M_essageBox("Do you want to create a collection job.", 1, "Ok", "No") = 2 Then
                                If com.idh.bridge.res.Messages.INSTANCE.doResourceMessageYN("WORCCJB", Nothing, 1) = 2 Then
                                    setWFValue(oForm, "AUTOCOLLECT", 0)
                                    Return False
                                Else
                                    setWFValue(oForm, "AUTOCOLLECT", iAutoCollectDays)
                                End If
                            End If
                        End If
                    End If
                End If
            End If

            Dim bDoDOC As String = getUFValue(oForm, "IDH_AVDOC")
            Dim sReportFile As String

            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then

                If bDoDOC.Equals("Y") Then
                    Dim sRowID As String = getFormDFValue(oForm, "Code")
                    Dim sCode As String = getFormDFValue(oForm, "Code").Trim()
                    If sRowID.Trim().Length > 0 Then
                        Dim sCardCode As String = getFormDFValue(oForm, "U_CustCd")
                        Dim sWasteCode As String = getFormDFValue(oForm, "U_WasCd")

                        Dim oData As ArrayList = Config.INSTANCE.doGetDOCReportLevel1a("WO", sCardCode, sWasteCode)
                        sReportFile = oData.Item(0)

                        If oData.Item(1) = True Then
                            Dim sConNo As String = getFormDFValue(oForm, "U_ConNum")
                            If sConNo Is Nothing OrElse sConNo.Length = 0 Then
                                'Dim iConNum As Integer = DataHandler.INSTANCE.getNextNumber("CONNUM")
                                Dim sConNum As String = DataHandler.INSTANCE.getSingleCode("CONNUM")
                                Dim sPremCode As String = ""
                                Dim sAddr As String = getWFValue(oForm, "STADDR")
                                Dim oAddData As ArrayList = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sCardCode, "S", sAddr)
                                If Not oAddData Is Nothing AndAlso oData.Count > 0 Then
                                    sPremCode = oAddData.Item(18)
                                End If
                                If sPremCode Is Nothing OrElse sPremCode.Length = 0 Then
                                    sConNo = com.idh.bridge.utils.General.CONNUM_EXEPTION_PREFIX & "/" & sConNum
                                Else
                                    sConNo = sPremCode & "/" & sConNum
                                End If

                                goParent.goDB.doUpdateQuery("UPDATE [@IDH_JOBSHD] Set U_ConNum = '" & sConNo & "' WHERE Code = '" & sRowID & "'")
                                setFormDFValue(oForm, "U_ConNum", sConNo)
                            End If
                        End If


                        '                        Dim sDoPrintDialog As String = Config.Parameter( "MDSWPD")
                        '                        If sDoPrintDialog Is Nothing Then
                        '                            sDoPrintDialog = "TRUE"
                        '                        Else
                        '                            sDoPrintDialog = sDoPrintDialog.ToUpper()
                        '                        End If

                        Dim sShowReport As String = Config.Parameter("MDSWDR")
                        If sShowReport Is Nothing Then
                            sShowReport = "TRUE"
                        Else
                            sShowReport = sShowReport.ToUpper()
                        End If


                        Dim sCopies As String = Config.Parameter("MDPCD")
                        If sCopies Is Nothing Then
                            sCopies = "1"
                        End If

                        Dim oParams As New Hashtable
                        oParams.Add("OrdNum", sCode)
                        oParams.Add("RowNum", sRowID)

                        'idh.report.Base.doCallReport(goParent, oForm, sReportFile, sShowReport, "TRUE", sDoPrintDialog, sCopies, oParams)
                        If (Config.INSTANCE.useNewReportViewer()) Then
                            IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                            IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                            IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTSHOW", sShowReport)
                            IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPRINT", "TRUE")
                            IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
                        Else
                            IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, sShowReport, "TRUE", sCopies, oParams, gsType)
                        End If
                    End If
                End If
            ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
            End If

            doCheckAndAddVehicle(oForm)

            If bMustDoUpdates = True Then
                Try
                    'PBI's is now updated through the PBI interfaces
                    'Dim sScheduleData As String = getFormDFValue(oForm, IDH_JOBSHD._Covera)
                    'Dim sRowCode As String = getFormDFValue(oForm, IDH_JOBSHD._Code)
                    'Dim sCardCode As String = getFormDFValue(oForm, IDH_JOBSHD._CustCd)
                    ''doCreateRecuringJobs(oForm)

                    'doCreateRecuringJobs(goParent, sRowCode, sCardCode, sScheduleData)
                Catch ex As Exception
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing Button 1.")
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXBNTO", {Nothing})
                End Try
            End If
            Return True
        End Function

        Public Sub doCheckAndAddVehicle(ByVal oForm As SAPbouiCOM.Form)
            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)
            oWOR.TareWeight = 0
            oWOR.TrailerTareWeight = 0
            oWOR.doCheckAndAddVehicle()

            '    'Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_VEHMAS")
            '    'Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            '    'Dim sQry As String

            '    'Dim sReg As String = getFormDFValue(oForm, "U_Lorry")
            '    'If sReg Is Nothing OrElse sReg.Length = 0 Then
            '    '    Return
            '    'End If

            '    'Try
            '    '    Dim sVehCode As String = getFormDFValue(oForm, "U_LorryCd")
            '    '    oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            '    '    If Not (sVehCode Is Nothing) AndAlso sVehCode.Length() > 0 Then
            '    '        sQry = "SELECT Code FROM [@IDH_VEHMAS] WHERE Code = '" & sVehCode & "'"
            '    '    Else
            '    '        Dim sCustCd As String = getFormDFValue(oForm, "U_CustCd")
            '    '        Dim sCarrCd As String = getFormDFValue(oForm, "U_CarrCd")
            '    '        sQry = "SELECT Code FROM [@IDH_VEHMAS] WHERE U_VehReg = '" & goParent.goDB.doFixForSQL(sReg) & "' " & _
            '    '                  " And U_CCrdCd = '" & sCustCd & "' " & _
            '    '                  " And U_CSCrdCd = '" & sCarrCd & "' " & _
            '    '                  " AND (U_WR1ORD IS NULL OR U_WR1ORD='' OR U_WR1ORD LIKE 'WO%')"
            '    '    End If
            '    '    oRecordSet.DoQuery(sQry)
            '    '    If oRecordSet.RecordCount = 0 Then

            '    '        Dim sDoASave As String = Config.Parameter("WOSVCR")
            '    '        If sDoASave.ToUpper().Equals("TRUE") Then
            '    '            Dim sCode As String
            '    '            sCode = DataHandler.doGenerateCode(IDH_VEHMAS.AUTONUMPREFIX, "VEHMAST")

            '    '            oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
            '    '            oAuditObj.setName(sCode)

            '    '            setFormDFValue(oForm, "U_LorryCd", sCode)

            '    '            oAuditObj.setFieldValue("U_VehReg", sReg)
            '    '            oAuditObj.setFieldValue("U_VehDesc", getFormDFValue(oForm, "U_VehTyp"))
            '    '            oAuditObj.setFieldValue("U_Driver", getFormDFValue(oForm, "U_Driver"))
            '    '            oAuditObj.setFieldValue("U_TRLReg", getFormDFValue(oForm, "U_TRLReg"))
            '    '            oAuditObj.setFieldValue("U_TRLNM", getFormDFValue(oForm, "U_TRLNM"))
            '    '            oAuditObj.setFieldValue("U_ItmGrp", Config.Parameter("IGR-LOR"))


            '    '            oAuditObj.setFieldValue("U_VehType", getWFValue(oForm, "VehicleType"))
            '    '            oAuditObj.setFieldValue("U_WR1ORD", "WO")
            '    '            oAuditObj.setFieldValue("U_WFStat", "ReqAdd")
            '    '        End If
            '    '    Else

            '    '        Dim sDoSave As String = Config.Parameter("WOSVUP")
            '    '        If sDoSave.ToUpper().Equals("TRUE") Then
            '    '            Dim sCode As String
            '    '            sCode = oRecordSet.Fields.Item(0).Value
            '    '            IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
            '    '            oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE)
            '    '            oAuditObj.setFieldValue("U_WFStat", "ReqUpdate")
            '    '        End If
            '    '    End If

            '    '    If oAuditObj.getAction() = IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD OrElse _
            '    '        oAuditObj.getAction() = IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE Then

            '    '        ''CUSTOMER
            '    '        oAuditObj.setFieldValue("U_CCrdCd", getFormDFValue(oForm, "U_CustCd"))
            '    '        oAuditObj.setFieldValue("U_CName", getFormDFValue(oForm, "U_CustNm"))

            '    '        ''WASTE CARRIER
            '    '        oAuditObj.setFieldValue("U_CSCrdCd", getFormDFValue(oForm, "U_CarrCd"))
            '    '        oAuditObj.setFieldValue("U_CSName", getFormDFValue(oForm, "U_CarrNm"))

            '    '        ''DISPOSAL SITE
            '    '        oAuditObj.setFieldValue("U_SCrdCd", getFormDFValue(oForm, "U_Tip"))
            '    '        oAuditObj.setFieldValue("U_SName", getFormDFValue(oForm, "U_TipNm"))
            '    '        oAuditObj.setFieldValue(IDH_JOBSHD._SAddress, getFormDFValue(oForm, IDH_JOBSHD._SAddress))

            '    '        ''ITEM CODE
            '    '        oAuditObj.setFieldValue("U_ItemCd", getFormDFValue(oForm, "U_ItemCd"))
            '    '        oAuditObj.setFieldValue("U_ItemDsc", getFormDFValue(oForm, "U_ItemDsc"))

            '    '        ''WASTE CODE
            '    '        oAuditObj.setFieldValue("U_WasCd", getFormDFValue(oForm, "U_WasCd"))
            '    '        oAuditObj.setFieldValue("U_WasDsc", getFormDFValue(oForm, "U_WasDsc"))

            '    '        '*** The tare weights
            '    '        oAuditObj.setFieldValue("U_Tarre", 0)
            '    '        oAuditObj.setFieldValue("U_TRLTar", 0)

            '    '        'ONLY UPDATE IF THE 2nd WEIGHT CAPTURED
            '    '        Dim dWeight As Double
            '    '        dWeight = ToDouble(getFormDFValue(oForm, "U_CstWgt"))
            '    '        If dWeight <> 0 Then
            '    '            Dim sMarkAct As String
            '    '            If getUFValue(oForm, "IDH_FOC").Equals("Y") Then
            '    '                sMarkAct = "DOFOC"
            '    '            ElseIf getUFValue(oForm, "IDH_DOORD").Equals("Y") Then
            '    '                sMarkAct = "DOORD"
            '    '            ElseIf getUFValue(oForm, "IDH_DOARI").Equals("Y") Then
            '    '                sMarkAct = "DOARI"
            '    '                'ElseIf getUFValue(oForm, "IDH_AINV").Equals("Y") Then
            '    '                '    sMarkAct = "DOAINV"
            '    '                'ElseIf getUFValue(oForm, "IDH_DOARIP").Equals("Y") Then
            '    '                '    sMarkAct = "DOARIP"
            '    '            Else
            '    '                sMarkAct = ""
            '    '            End If
            '    '            oAuditObj.setFieldValue("U_MarkAc", sMarkAct)
            '    '        End If

            '    '        Dim iResult As Integer
            '    '        iResult = oAuditObj.Commit()

            '    '        If iResult <> 0 Then
            '    '            Dim sResult As String = Nothing
            '    '            goParent.goDICompany.GetLastError(iResult, sResult)
            '    '            com.idh.bridge.DataHandler.INSTANCE.doError("System: Error Adding/Update Vehicle - " + sResult, "Error checking and adding vehicle.")
            '    '        End If
            '    '    End If
            '    'Catch ex As Exception
            '    '    com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error checking and adding the vehicle.")
            '    'Finally
            '    '    IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
            '    '    IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj)
            '    'End Try
        End Sub

        Public Shared Function doDeleteRow(ByVal sRowCode As String, ByVal sComment As String) As Boolean
            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(IDHAddOns.idh.addon.Base.PARENT, msMainTable)
            Dim iwResult As Integer
            Dim swResult As String = Nothing
            If oAuditObj.setKeyPair(sRowCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_REMOVE) Then
                oAuditObj.setComment(sComment)
                iwResult = oAuditObj.Commit()

                If iwResult <> 0 Then
                    IDHAddOns.idh.addon.Base.PARENT.goDICompany.GetLastError(iwResult, swResult)
                    'com.idh.bridge.DataHandler.INSTANCE.doError("System: Error Removing - " + swResult, "Error Removing: " + swResult)
                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Removing - " + swResult + ",Error Removing: " + swResult, "ERSYDBRR", {swResult, swResult})
                    Return False
                End If
            End If
            Return True
        End Function

        Private Function doUpdateRow(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim iwResult As Integer = -1
            Dim swResult As String = Nothing
            Dim bIsAdd As Boolean = False
            Dim sEDate As String = Nothing
            Dim oEDate As Date = Nothing
            Dim sJobNr As String = Nothing
            Dim sCode As String = Nothing

            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)

            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(goParent, getFormMainTable())
            Try
                With getFormMainDataSource(oForm)
                    sCode = .GetValue("Code", .Offset).Trim()
                    sJobNr = .GetValue("U_JobNr", .Offset).Trim()
                    If oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) = False Then
                        oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
                        bIsAdd = True
                    End If

                    oAuditObj.setName(sCode)

                    Dim ii As Integer
                    Dim sField As String = Nothing
                    Dim oVal As Object = Nothing

                    Dim sSOStatus As String = .GetValue("U_Status", .Offset)
                    Dim sPOStatus As String = .GetValue("U_PStat", .Offset)
                    For ii = 0 To .Fields.Count - 1
                        Try
                            sField = .Fields.Item(ii).Name
                            oVal = .GetValue(ii, .Offset)
                            If oVal.GetType Is GetType(String) AndAlso Not oVal Is Nothing Then
                                oVal = oVal.Trim()
                            End If

                            If sField.Equals("Code") Then
                            ElseIf sField.Equals("Name") Then
                                If oAuditObj.getAction() = IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD AndAlso Not (oVal Is Nothing) Then
                                    oAuditObj.setName(oVal)
                                End If
                            ElseIf sField.Equals("ArchiveDate") Then

                            ElseIf sField.Equals("U_ProUOM") Then
                                oAuditObj.setFieldValue(sField, oVal)
                            Else
                                If sField.Equals("U_PStat") Then
                                    If oVal.Length = 0 Then
                                        oVal = com.idh.bridge.lookups.FixedValues.getStatusOpen()
                                    End If

                                    sPOStatus = oVal
                                ElseIf sField.Equals("U_Status") Then
                                    If oVal.Length = 0 Then
                                        oVal = com.idh.bridge.lookups.FixedValues.getStatusOpen()
                                    End If

                                    sSOStatus = oVal
                                End If

                                oAuditObj.setFieldValue(sField, oVal)
                            End If
                        Catch ex As Exception
                            'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the Field values - " & sField & " - " & oVal)
                            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSVS", {sField, oVal.ToString()})
                        End Try
                    Next

                    iwResult = oAuditObj.Commit()

                    If iwResult <> 0 Then
                        goParent.goDICompany.GetLastError(iwResult, swResult)
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Error Adding: " + swResult)
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Adding: " + swResult, "ERSYADD", {swResult})
                        Return False
                    End If

                    Dim oAddGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                    oAddGrid.doProcessData()

                    Dim sJob As String = getFormDFValue(oForm, "U_JobNr")






                    'If Config.INSTANCE.getParameterAsBool("MDAGEN", False) Then
                    '    If sSOStatus.StartsWith(FixedValues.getDoOrderStatus()) OrElse _
                    '     sPOStatus.StartsWith(FixedValues.getDoOrderStatus()) Then
                    '        com.idh.bridge.action.MarketingDocs.doOrders("@IDH_JOBENTR", getFormMainTable(), "WO", sJob, sCode)

                    '    End If

                    '    If sSOStatus.StartsWith(FixedValues.getDoInvoiceStatus()) OrElse _
                    '     sSOStatus.StartsWith(FixedValues.getDoPInvoiceStatus()) Then
                    '        com.idh.bridge.action.MarketingDocs.doInvoicesAR("@IDH_JOBENTR", getFormMainTable(), "WO", sJob, sCode)

                    '    End If

                    '    If sSOStatus.StartsWith(FixedValues.getDoRebateStatus()) OrElse _
                    '     sPOStatus.StartsWith(FixedValues.getDoRebateStatus()) Then
                    '        com.idh.bridge.action.MarketingDocs.doRebates("@IDH_JOBENTR", getFormMainTable(), "WO", sJob, sCode)
                    '    End If

                    '    If sSOStatus.StartsWith(FixedValues.getDoFocStatus()) Then
                    '        com.idh.bridge.action.MarketingDocs.doFoc("@IDH_JOBENTR", getFormMainTable(), "WO", sJob, sCode)


                    '    End If
                    'End If

                    Dim sOrderType As String = getFormDFValue(oForm, "U_JobTp").Trim()

                    Dim sLicJob As String = Config.ParameterWithDefault("WOJBLRN", "")
                    sEDate = getFormDFValue(oForm, "U_AEDate")

                    If sOrderType.Equals(sLicJob) Then
                        If bIsAdd OrElse
                         wasSetByUser(oForm, "IDH_ENDDT") Then
                            Dim sExpDate As String = sEDate
                            Dim dLicCost As Double = getFormDFValue(oForm, "U_CusChr")

                            Dim sWO As String = getFormDFValue(oForm, "U_JobNr")
                            idh.forms.orders.Waste.doUpdateSkipLicExpiry(sWO, ToDateTime(sExpDate), dLicCost)
                        End If
                    End If
                End With

                oEDate = com.idh.utils.Dates.doStrToDate(sEDate)
                If com.idh.utils.Dates.isValidDate(oEDate) AndAlso Not sJobNr Is Nothing AndAlso Not sCode Is Nothing Then
                    Dim oWOH As IDH_JOBENTR = New IDH_JOBENTR()
                    oWOH.doCheckAndCloseWO(sJobNr, sCode)
                    IDHAddOns.idh.data.Base.doReleaseObject(oWOH)
                End If

                com.idh.dbObjects.User.IDH_BPFORECST.doUpdateForecast(sJobNr)

                Return True
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Updating the Waste Order Row.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXUGOR", {com.idh.bridge.Translation.getTranslatedWord("Waste")})
                Return False
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj)
            End Try
        End Function

        Private Sub doUSAEndDate(ByVal oForm As SAPbouiCOM.Form, ByVal sDateE As String)
            'KA -- START -- 20121022
            'OnTime [#Ico000????] USA
            'START
            'Add Additional Expense Row when Actual End Date is entered
            If Config.INSTANCE.getParameterAsBool("USAREL", False) Then
                If Not sDateE Is Nothing AndAlso sDateE.Length > 0 Then
                    'Check if Customer's eligible for Fuel Surcharge
                    Dim sCardCode As String = getFormDFValue(oForm, "U_CustCd")
                    Dim sCardName As String = getFormDFValue(oForm, "U_CustNm")
                    Dim sWOCode As String = getFormDFValue(oForm, "U_JobNr")
                    Dim sRowNr As String = getFormDFValue(oForm, "Code")
                    Dim oRecordset As SAPbobsCOM.Recordset

                    Dim sFuelsurcharge As String = Nothing
                    Dim sCountyCharge As String = Nothing
                    Dim sPortCharge As String = Nothing

                    oRecordset = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    Dim sQry As String = "select U_FULSCHG, U_COUNTAR, U_PORTARF from OCRD where CardCode = '" + sCardCode + "'"
                    oRecordset.DoQuery(sQry)

                    If oRecordset.RecordCount > 0 Then
                        sFuelsurcharge = oRecordset.Fields.Item(0).Value
                        sCountyCharge = oRecordset.Fields.Item(1).Value
                        sPortCharge = oRecordset.Fields.Item(2).Value
                    End If
                    IDHAddOns.idh.data.Base.doReleaseObject(oRecordset)

                    Dim sFinalMsg As String = Nothing
                    'Fuel Surcharge
                    If sFuelsurcharge IsNot Nothing AndAlso sFuelsurcharge.Length > 0 AndAlso sFuelsurcharge.ToUpper() = "Y" Then
                        'Now check if there is an existing Fuel Surcharge Row in WO
                        'Dim sWOCode As String = getFormDFValue(oForm, "U_JobNr")

                        ' => KA - 20121009 START
                        Dim oFuelRS As SAPbobsCOM.Recordset

                        'sQry = "select * from [@IDH_WOADDEXP] where U_ItemCd = '" + Config.ParameterWithDefault("FUELSITM", "") + "' AND U_JobNr = '" + sWOCode + "'"
                        'sQry = " select AddExp.U_ItemCd, AddExp.U_Quantity, AddExp.U_ItmCost, U_ItmChrg, U_UOM, " + _
                        '        " * from [@IDH_WOADDEXP] AddExp " + _
                        '        " INNER JOIN oitm on AddExp.U_ItemCd = OITM.ItemCode  " + _
                        '        " AND OITM.ItmsGrpCod = (select U_Val from [@IDH_WRCONFIG] where Code = 'IGR-LOR') " + _
                        '        " where U_JobNr = '" + sWOCode + "' AND U_RowNr = '" + sRowNr + "'"

                        Dim sVehGrp As String = Config.ParameterWithDefault("FSVEHGRP", "-1")
                        sQry = " select AddExp.U_ItemCd, AddExp.U_Quantity, AddExp.U_ItmCost, U_ItmChrg, U_UOM, " +
                                " * from [@IDH_WOADDEXP] AddExp " +
                                " INNER JOIN oitm on AddExp.U_ItemCd = OITM.ItemCode  " +
                                " AND OITM.ItmsGrpCod IN (" + sVehGrp + ") " +
                                " where U_JobNr = '" + sWOCode + "' AND U_RowNr = '" + sRowNr + "'"
                        oFuelRS = goParent.goDB.doSelectQuery(sQry)

                        Dim sQty As String
                        Dim sItmCharge As String
                        Dim sIC As String
                        Dim sUOM As String
                        Dim dItmCharge As Double

                        While Not oFuelRS.EoF
                            sQty = oFuelRS.Fields.Item("U_Quantity").Value.ToString()
                            sItmCharge = oFuelRS.Fields.Item("U_ItmChrg").Value.ToString()
                            sIC = oFuelRS.Fields.Item("U_ItemCd").Value.ToString()
                            sUOM = oFuelRS.Fields.Item("U_UOM").Value.ToString()
                            dItmCharge = 0.0

                            If sItmCharge IsNot Nothing AndAlso sItmCharge.Length > 0 AndAlso Convert.ToDouble(sItmCharge) > 0 Then
                                dItmCharge = Convert.ToDouble(sItmCharge)
                            Else
                                'Get Item Charge from CIP
                                'sQry = "SELECT Code, Name, U_CustCd, U_AItmCd, U_UOM, U_AItmPr from [@IDH_CSITPR] " + _
                                '        " where U_LinkNr != 0 AND U_AItmCd = '" + sIC + "' AND U_UOM = '" + sUOM + "'"

                                sQry = "SELECT Code, Name, U_CustCd, U_AItmCd, U_UOM, U_AItmPr from [@IDH_CSITPR] " +
                                        " where U_AItmCd = '" + sIC + "' AND U_UOM = '" + sUOM + "'" +
                                        " AND '" + getFormDFValue(oForm, "U_RDate").ToString() + "' BETWEEN U_StDate AND U_EnDate "

                                'NOTE 20160309: 
                                'After long discussion with HH, HN, Uval and KA, it was decided to include the order by 
                                'clause and another filter 
                                'to get FALLBACK for the Additional Item 
                                'No other possible scenario identified so far where this will fail 
                                'or not provide expected results 
                                sQry = sQry + " AND IsNull(U_CustCd, '') = '' "
                                Dim sOrderByClause As String = Config.INSTANCE.getParameterWithDefault("PRADIORD", "")
                                sQry = sQry + " " + sOrderByClause

                                Dim oRSTemp As SAPbobsCOM.Recordset
                                oRSTemp = goParent.goDB.doSelectQuery(sQry)
                                If oRSTemp.RecordCount > 0 Then
                                    sItmCharge = oRSTemp.Fields.Item("U_AItmPr").Value.ToString()
                                    If sItmCharge IsNot Nothing AndAlso sItmCharge.Length > 0 AndAlso Convert.ToDouble(sItmCharge) > 0 Then
                                        dItmCharge = Convert.ToDouble(sItmCharge)
                                        IDHAddOns.idh.data.Base.doReleaseObject(oRSTemp)
                                    End If
                                End If
                            End If
                            If dItmCharge > 0 Then
                                doAddFuelSurcharge(oForm, sQty, dItmCharge, sFinalMsg)
                            Else
                                'Update FinalMessage for Item Charge value = ZERO
                                If sFinalMsg IsNot Nothing AndAlso sFinalMsg.Length > 0 Then
                                    sFinalMsg = sFinalMsg + ", Fuel Surcharge(Item charge value not found!)"
                                Else
                                    sFinalMsg = sFinalMsg + "Fuel Surcharge(Item charge value not found!)"
                                End If
                            End If
                            oFuelRS.MoveNext()
                        End While
                        IDHAddOns.idh.data.Base.doReleaseObject(oFuelRS)
                        ' => KA - 20121009 END
                    End If

                    'County Charge
                    If sCountyCharge IsNot Nothing AndAlso sCountyCharge.Length > 0 AndAlso sCountyCharge.ToUpper() = "Y" Then
                        'Now check if there is an existing County Chanrge Row in WO
                        'Dim sWOCode As String = getFormDFValue(oForm, "U_JobNr")
                        'Dim oCountyRS As SAPbobsCOM.Recordset
                        'sQry = "select * from [@IDH_WOADDEXP] where U_ItemCd = '" + Config.ParameterWithDefault("CNTYCITM", "") + "' AND U_JobNr = '" + sWOCode + "'"
                        'oCountyRS = goParent.goDB.doSelectQuery(sQry)
                        'if there is no Add. Exp. for 'County Charge' item code add row
                        'If Not oCountyRS.RecordCount > 0 Then
                        '    doAddCountyCharge(oForm, sCardCode, sCardName, sFinalMsg)
                        'End If
                        'IDHAddOns.idh.data.Base.doReleaseObject(oCountyRS)

                        doAddCountyCharge(oForm, sCardCode, sCardName, sFinalMsg)
                    End If

                    'Port Charge
                    If sPortCharge IsNot Nothing AndAlso sPortCharge.Length > 0 AndAlso sPortCharge.ToUpper() = "Y" Then
                        'Now check if there is an existing Port Chanrge Row in WO
                        'Dim sWOCode As String = getFormDFValue(oForm, "U_JobNr")
                        Dim oPortRS As SAPbobsCOM.Recordset
                        sQry = "select * from [@IDH_WOADDEXP] where U_ItemCd = '" + Config.ParameterWithDefault("PORTCITM", "") + "' AND U_JobNr = '" + sWOCode + "'"
                        oPortRS = goParent.goDB.doSelectQuery(sQry)
                        'if there is no Add. Exp. for 'Port Charge' item code add row
                        If Not oPortRS.RecordCount > 0 Then
                            doAddPortCharge(oForm, sFinalMsg)
                        End If
                        IDHAddOns.idh.data.Base.doReleaseObject(oPortRS)
                    End If

                    If sFinalMsg IsNot Nothing AndAlso sFinalMsg.Length > 0 Then
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                        doWarnMess("Following Additional Items were added: " + sFinalMsg + ". Please review Additional Tab and update Ordered Quantity.", SAPbouiCOM.BoMessageTime.bmt_Short)
                    End If
                ElseIf sDateE Is Nothing OrElse sDateE.Equals(String.Empty) Then
                    'remove the surcharge rows from the grid
                    Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                    Dim iGridRowCnt As Integer = oGrid.getRowCount

                    For index As Integer = 1 To iGridRowCnt - 1
                        Dim iSearchIndx As Integer
                        'Remove Fuel Surcharge
                        iSearchIndx = oGrid.doIndexOfItem(Config.Parameter("FUELSITM"))
                        If iSearchIndx > -1 Then
                            oGrid.doRemoveRow(iSearchIndx)
                        End If

                        'Remove County Surcharge
                        iSearchIndx = oGrid.doIndexOfItem(Config.Parameter("CNTYCITM"))
                        If iSearchIndx > -1 Then
                            oGrid.doRemoveRow(iSearchIndx)
                        End If

                        'Remove Port Surcharge
                        iSearchIndx = oGrid.doIndexOfItem(Config.Parameter("PORTCITM"))
                        If iSearchIndx > -1 Then
                            oGrid.doRemoveRow(iSearchIndx)
                        End If
                    Next
                    Dim oWOR As IDH_JOBSHD = thisWOR(oForm)
                    '## MA 07-04-2014 Start
                    Dim dCostTWOVAT As Double = oGrid.doCalculateCostTotalsExcVAT()

                    setUFValue(oForm, "IDH_ADDCO", dCostTWOVAT)
                    Dim dCostVAT As Double = oGrid.doCalculateCostVAT()
                    setUFValue(oForm, "IDH_ADDCOV", dCostVAT)

                    setUFValue(oForm, "IDH_TADDCO", dCostTWOVAT + dCostVAT)

                    oWOR.U_TAddCsVat = dCostVAT
                    oWOR.U_AddCost = dCostTWOVAT
                    'oWOR.U_TAddCost = dCostTWOVAT + dCostVAT

                    'LP Viljoen - This will automatically be called when the above values are set - oWOR.doCalculateCostTotals()

                    'Dim dChrgT As Double = oGrid.doCalculateChargeTotals()
                    'setUFValue(oForm, "IDH_TADDCH", dChrgT)
                    'oWOR.U_TAddChrg = dChrgT
                    Dim dChgTWOVAT As Double = oGrid.doCalculateChargeTotalsExcVAT()
                    setUFValue(oForm, "IDH_ADDCHR", dChgTWOVAT)
                    Dim dChargeVAT As Double = oGrid.doCalculateChargeVAT()
                    setUFValue(oForm, "IDH_ADDCHV", dChargeVAT)

                    setUFValue(oForm, "IDH_TADDCH", dChargeVAT + dChgTWOVAT)

                    oWOR.U_TAddChVat = dChargeVAT
                    oWOR.U_AddCharge = dChgTWOVAT
                    'oWOR.U_TAddChrg = dChargeVAT + dChgTWOVAT

                    'incorrect Code must stick to the new Core flow (LP Viljoen) and the calculations will automatically be called by setting the above values 
                    '- oWOR.doCalculateChargeTotal(True)
                    '## MA 07-04-2014 End


                End If
            End If
            'END
            'OnTime [#Ico000????] USA
            'KA -- END -- 20121022
        End Sub

        Private Function doValidationEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)
            Dim sItemId As String = pVal.ItemUID

            If pVal.BeforeAction = True Then
                If pVal.ItemChanged Then
                    If sItemId = "IDH_REQDAT" Then
                        Dim dDate As Date = oWOR.U_RDate
                        If Config.INSTANCE.doReqDateCheck(dDate, DateTime.Now()) = False Then
                            BubbleEvent = False
                            Return False
                        End If
                        oWOR.doGetAllUOM()
                        oWOR.doGetAllPrices()
                    End If
                End If
            ElseIf pVal.BeforeAction = False Then
                If pVal.ItemChanged Then
                    If sItemId = "IDH_STARDT" Then
                        Dim sDateS As String = getFormDFValue(oForm, "U_ASDate")
                        Dim sDateE As String = getFormDFValue(oForm, "U_AEDate")

                        Dim sPBI As String = oWOR.U_LnkPBI
                        If Not sPBI Is Nothing AndAlso sPBI.Length > 0 _
                            AndAlso Not sDateS Is Nothing AndAlso sDateS.Length > 0 Then
                            '20150227 - LPV - BEGIN - UPDATE the PBI Detail linked to this WOR
                            'Dim oPBIDetail As IDH_PBIDTL = New IDH_PBIDTL()
                            'If oPBIDetail.getData(IDH_PBIDTL._IDHWOR & " = '" & oWOR.Code & "'", Nothing) = 1 Then
                            '    oPBIDetail.U_IDHJDATE_AsString(sDateS)
                            '    oPBIDetail.doProcessData()
                            'Else
                            '    'Check the PBI's Date if it is not the same as the set date display a error message
                            '    'indicating the PBI's Date.
                            '    Dim sPBIDate As String = doCheckPBIDate(oWOR.Code, "U_IDHJDATE", sDateS)
                            '    If Not sPBIDate Is Nothing Then
                            '        com.idh.bridge.DataHandler.INSTANCE.doError("The Start Date is not the same as the PBI Start Date and will now be set to the PBI Date. (" & sPBIDate & " - " & sDateS & ")")
                            '        sDateS = sPBIDate
                            '        setFormDFValue(oForm, "U_ASDate", sDateS)
                            '    End If
                            'End If
                            '20150227 - LPV - BEGIN - UPDATE the PBI Detail linked to this WOR
                        End If

                        Config.INSTANCE.doCheckDatesMonths(sDateS, sDateE)
                    ElseIf sItemId = "IDH_ENDDT" Then
                        Dim sDateS As String = getFormDFValue(oForm, "U_ASDate")
                        Dim sDateE As String = getFormDFValue(oForm, "U_AEDate")

                        '20150227 - LPV - BEGIN - UPDATE the PBI Detail linked to this WOR
                        'Dim sPBI As String = oWOR.U_LnkPBI
                        'If Not sPBI Is Nothing AndAlso sPBI.Length > 0 _
                        ' AndAlso Not sDateE Is Nothing AndAlso sDateE.Length > 0 Then
                        '    'Check the PBI's Date if it is not the same as the set date display a error message
                        '    'indicating the PBI's Date.
                        '    Dim sPBIDate As String = doCheckPBIDate(oWOR.Code, "U_IDHJDATE", sDateE)
                        '    If Not sPBIDate Is Nothing Then
                        '        com.idh.bridge.DataHandler.INSTANCE.doError("The End Date is not the same as the PBI End Date and will now be set to the PBI Date. (" & sPBIDate & " - " & sDateE & ")")
                        '        sDateE = sPBIDate
                        '        setFormDFValue(oForm, "U_AEDate", sDateE)
                        '    End If
                        'End If
                        '20150227 - LPV - END - UPDATE the PBI Detail linked to this WOR

                        Config.INSTANCE.doCheckDatesMonths(sDateS, sDateE)


                        Dim sLicJob As String = Config.ParameterWithDefault("WOJBLRN", "")
                        If oWOR.U_JobTp.Equals(sLicJob) Then
                            Dim sExpDate As String = getFormDFValue(oForm, "U_AEDate")
                            setFormDFValue(oForm, "U_SLicExp", sDateE)
                        End If

                        doUSAEndDate(oForm, sDateE)
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._AEDate)

                        'If String.IsNullOrEmpty(sDateE) Then
                        'If Config.INSTANCE.getParameterAsBool("USEBPACH", False) Then
                        'Remove any existing AutoAdd rows 
                        'This is we dont know any items being added for this BP previously 
                        'removeAutoAddBPAddCharges(oForm)
                        'End If
                        'Else
                        doUpdateAutoAddBPAddCharges(oForm, sDateE)
                        'End If
                    ElseIf sItemId = "IDH_ONCS" Then
                        Dim sCs As String = getItemValue(oForm, "IDH_ONCS")
                        If sCs.StartsWith("*") OrElse sCs.EndsWith("*") Then
                            setItemValue(oForm, "IDH_ONCS", "")
                            doChooseComplianceScheme(oForm, True)
                        End If
                    ElseIf sItemId = "IDH_2RRVA" Then
                        oForm.Items.Item("IDH_2RRA").Specific.Selected = True
                    ElseIf sItemId = "IDH_2RRVB" Then
                        oForm.Items.Item("IDH_2RRB").Specific.Selected = True
                    ElseIf pVal.ItemUID = "IDH_2FRE" Then
                        oForm.DataSources.UserDataSources.Item("U_2RD").ValueEx = "5"
                    ElseIf sItemId = "IDH_ITMGRP" Then
                        doGetItemGrpName(oForm)
                    ElseIf sItemId = "IDH_TIPWEI" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._TipWgt)
                    ElseIf sItemId = "IDH_TIPCOS" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._TipCost)

                        'Ask To Save to SIP
                        doRequestCIPSIPUpdate(oForm, "ASKSIP", WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.DispCst)
                    ElseIf sItemId = "IDH_SUPWGT" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._ProWgt)
                    ElseIf sItemId = "IDH_PRCOST" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._PCost)

                        'Ask To Save to SIP
                        doRequestCIPSIPUpdate(oForm, "ASKSIP", WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.ProCst)
                    ElseIf sItemId = "IDH_ORDQTY" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._CarWgt)
                        'oWOR.doMarkUserUpdate(IDH_JOBSHD._OrdWgt)
                    ElseIf sItemId = "IDH_ORDCOS" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._OrdCost)

                        'Ask To Save to SIP
                        doRequestCIPSIPUpdate(oForm, "ASKSIP", WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.HaulCst)
                    ElseIf sItemId = "IDH_CUSCHG" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._TCharge)

                        doRequestCIPSIPUpdate(oForm, "ASKCIP", WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TChrg)
                    ElseIf sItemId = "IDH_CUSCHR" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._CusChr)

                        'Ask To Save to CIP
                        doRequestCIPSIPUpdate(oForm, "ASKCIP", WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.HChrg)
                    ElseIf sItemId = "IDH_CUSQTY" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._CusQty)
                        ''oWOR.doCalculateHaulageChargeTotals()
                    ElseIf sItemId = "IDH_CUSWEI" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._CstWgt)
                    ElseIf sItemId = "IDH_CUSCM" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._AUOMQt)
                    ElseIf sItemId = "IDH_PUOMQT" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._PAUOMQt)
                    ElseIf sItemId = "IDH_TUOMQT" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._TAUOMQt)
                    ElseIf sItemId = "IDH_SKPQTY" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._SLicCQt)
                    ElseIf sItemId = "IDH_SKPCOS" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._SLicCst)
                    ElseIf sItemId = "IDH_SKPTOT" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._SLicCTo)
                    ElseIf sItemId = "IDH_LICCHR" OrElse
                            sItemId = "IDH_CONCHG" OrElse
                            sItemId = "IDH_JOBPRC" Then
                        'incorrect Code must stick to the new Core flow (LP Viljoen) - oWOR.doCalculateChargeTotal(True)
                        oWOR.doCalculateChargeVat("R")
                    ElseIf sItemId = "IDH_DISCNT" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._Discnt)
                    ElseIf sItemId = "IDH_DSCPRC" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._DisAmt)
                    ElseIf sItemId = "IDH_REQDAT" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._RDate)
                    ElseIf sItemId = "IDH_PURWGT" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._PRdWgt)
                    ElseIf sItemId = "IDH_RDWGT" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._RdWgt)
                        If Config.INSTANCE.getParameterAsBool("USEBPACH", False) Then
                            'Remove any existing AutoAdd rows 
                            'This is we dont know any items being added for this BP previously 
                            If oWOR.U_RdWgt = 0 Then
                                removeAutoAddBPAddCharges(oForm)
                            End If
                        End If
                    ElseIf sItemId = "IDH_TRDWGT" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._TRdWgt, True)
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._TRdWgt)
                        'oWOR.doPreMarkUserUpdate(IDH_JOBSHD._TRdWgt)
                    ElseIf sItemId = "IDH_LICSUP" Then
                        doSkipChanged(oForm)
                    ElseIf sItemId = "IDH_SLNM" Then
                        doSkipChanged(oForm)
                    ElseIf sItemId = "IDH_SKPLIC" Then
                        doSkipChanged(oForm)
                    ElseIf sItemId = "IDH_LICEXP" Then
                        doSkipChanged(oForm)
                    ElseIf sItemId = "IDH_TFSNUM" Then
                        Dim sASDate As String = getFormDFValue(oForm, "U_ASDate")
                        If sASDate Is Nothing OrElse sASDate.Length = 0 Then
                            oWOR.U_ASDate = DateTime.Now
                        End If
                    End If

                    If pVal.InnerEvent = False AndAlso (sItemId = "IDH_ITMCOD" OrElse sItemId = "IDH_DESC") Then
                        doChooseContainer(oForm, sItemId)
                        ''MA Start 25-11-2014
                    ElseIf pVal.InnerEvent = False AndAlso (sItemId = "IDH_CUSREF") Then
                        Dim sCustRef As String = ""
                        sCustRef = getFormDFValue(oForm, IDH_JOBSHD._CustRef) '.GetValue("U_ItemCd", .Offset).Trim()
                        If sCustRef IsNot Nothing AndAlso (sCustRef.Trim.IndexOf("*") > -1) Then
                            doChooseCustomerRef(oForm)
                        End If
                        ''MA End 25-11-2014
                        '## Start 27-11-2013
                    ElseIf sItemId = "IDH_WASCL1" OrElse sItemId = "IDH_WASMAT" Then
                        Dim sValue As String = getItemValue(oForm, sItemId)
                        If (pVal.ItemChanged AndAlso wasSetByUser(oForm, sItemId) = True) OrElse sValue.Contains("*") Then
                            doChooseWasteCode(oForm, sItemId)
                        End If
                        'ElseIf sItemId = "IDH_WASMAT" Then
                        '    Dim sDesc As String = getItemValue(oForm, "IDH_WASMAT")
                        '    If doNeedSearch(sDesc) Then
                        '        doChooseWasteCode(oForm)
                        '    End If
                        '## End 27-11-2013
                        'ElseIf pVal.InnerEvent = False AndAlso (sItemId = "IDH_SUPCOD" OrElse sItemId = "IDH_SUPNAM") Then
                        '    doChooseSupplier(oForm, True, sItemId)
                    ElseIf sItemId = "IDH_SUPCOD" Then 'OrElse sItemId = "IDH_SUPNAM") Then
                        If wasSetByUser(oForm, sItemId) Then
                            setItemValue(oForm, "IDH_SUPNAM", "")
                            doChooseSupplier(oForm, True, pVal.ItemUID)
                        End If
                    ElseIf (sItemId = "IDH_SUPNAM") Then
                        If wasSetByUser(oForm, sItemId) Then
                            setItemValue(oForm, "IDH_SUPCOD", "")
                            doChooseSupplier(oForm, True, pVal.ItemUID)
                        End If
                    ElseIf sItemId = "IDH_CARRIE" Then 'OrElse pVal.ItemUID = "IDH_CARNAM" Then
                        If wasSetByUser(oForm, sItemId) Then
                            setItemValue(oForm, "IDH_CARNAM", "")
                            doChooseCarrier(oForm, True, pVal.ItemUID)
                        End If
                    ElseIf sItemId = "IDH_CARNAM" Then 'OrElse pVal.ItemUID = "IDH_CARNAM" Then
                        If wasSetByUser(oForm, sItemId) Then
                            setItemValue(oForm, "IDH_CARRIE", "")
                            doChooseCarrier(oForm, True, pVal.ItemUID)
                        End If
                    ElseIf sItemId = "IDH_TIPPIN" Then
                        If wasSetByUser(oForm, sItemId) Then
                            setItemValue(oForm, "IDH_TIPNM", "")
                            doChooseSite(oForm, True, pVal.ItemUID)
                        End If
                    ElseIf pVal.ItemUID = "IDH_TIPNM" Then
                        If wasSetByUser(oForm, sItemId) Then
                            setItemValue(oForm, "IDH_TIPPIN", "")
                            doChooseSite(oForm, True, pVal.ItemUID)
                        End If
                    ElseIf sItemId = "IDH_LICSUP" Then 'OrElse pVal.ItemUID = "IDH_SLNM" Then
                        If wasSetByUser(oForm, sItemId) Then
                            setItemValue(oForm, "IDH_SLNM", "")
                            doChooseLic(oForm, True, pVal.ItemUID)
                        End If
                    ElseIf sItemId = "IDH_SLNM" Then 'OrElse pVal.ItemUID = "IDH_SLNM" Then
                        If wasSetByUser(oForm, sItemId) Then
                            setItemValue(oForm, "IDH_LICSUP", "")
                            doChooseLic(oForm, True, pVal.ItemUID)
                        End If
                    ElseIf sItemId = "IDH_LORRY" Then
                        Dim sAct As String = getWFValue(oForm, "LastVehAct")
                        If sAct <> "CREATE" AndAlso wasSetByUser(oForm, sItemId) Then 'sAct = "DOSEARCH" 
                            'setItemValue(oForm, "IDH_LICSUP", "")
                            'doChooseLic(oForm, True, pVal.ItemUID)
                            doCallLorrySearchEB(oForm)
                        End If
                    End If
                Else
                    If pVal.InnerEvent = False Then
                        If (sItemId = "IDH_SUPCOD" OrElse sItemId = "IDH_SUPNAM") Then
                            Dim sVal As String = getItemValue(oForm, sItemId)
                            If sVal.IndexOf("*") > -1 Then
                                doChooseSupplier(oForm, True, sItemId)
                            End If
                        ElseIf sItemId = "IDH_TIPPIN" Then
                            Dim sCode As String = getItemValue(oForm, "IDH_TIPPIN")
                            If sCode.IndexOf("*") > -1 Then
                                setItemValue(oForm, "IDH_TIPNM", "")
                                doChooseSite(oForm, True, pVal.ItemUID)
                            End If
                        ElseIf pVal.ItemUID = "IDH_TIPNM" Then
                            Dim sName As String = getItemValue(oForm, "IDH_TIPNM")
                            If sName.IndexOf("*") > -1 Then
                                setItemValue(oForm, "IDH_TIPPIN", "")
                                doChooseSite(oForm, True, pVal.ItemUID)
                            End If
                        ElseIf sItemId = "IDH_CARRIE" Then 'OrElse pVal.ItemUID = "IDH_CARNAM" Then
                            Dim sCode As String = getItemValue(oForm, "IDH_CARRIE")
                            If sCode.IndexOf("*") > -1 Then
                                setItemValue(oForm, "IDH_CARNAM", "")
                                doChooseCarrier(oForm, True, pVal.ItemUID)
                            End If
                        ElseIf sItemId = "IDH_CARNAM" Then 'OrElse pVal.ItemUID = "IDH_CARNAM" Then
                            Dim sName As String = getItemValue(oForm, "IDH_CARNAM")
                            If sName.IndexOf("*") > -1 Then
                                setItemValue(oForm, "IDH_CARRIE", "")
                                doChooseCarrier(oForm, True, pVal.ItemUID)
                            End If
                        ElseIf sItemId = "IDH_LICSUP" Then 'OrElse pVal.ItemUID = "IDH_SLNM" Then
                            Dim sCode As String = getItemValue(oForm, "IDH_LICSUP")
                            If sCode.IndexOf("*") > -1 Then
                                setItemValue(oForm, "IDH_SLNM", "")
                                doChooseLic(oForm, True, pVal.ItemUID)
                            End If
                        ElseIf sItemId = "IDH_SLNM" Then 'OrElse pVal.ItemUID = "IDH_SLNM" Then
                            Dim sName As String = getItemValue(oForm, "IDH_SLNM")
                            If sName.IndexOf("*") > -1 Then
                                setItemValue(oForm, "IDH_LICSUP", "")
                                doChooseLic(oForm, True, pVal.ItemUID)
                            End If
                        ElseIf (sItemId = "IDH_WASCL1" OrElse sItemId = "IDH_WASMAT" OrElse sItemId = "IDH_WASMAT") AndAlso pVal.ItemChanged Then
                            doChooseWasteCode(oForm, sItemId)
                        ElseIf sItemId = "IDH_LORRY" Then
                            Dim sName As String = getItemValue(oForm, "IDH_LORRY")
                            If sName.IndexOf("*") > -1 Then
                                ' setItemValue(oForm, "IDH_LORRY", "")
                                doCallLorrySearchEB(oForm)
                            End If
                        End If
                    End If
                End If
            End If
            Return True
        End Function

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)
            Dim sItemId As String = pVal.ItemUID

            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST Then
                Dim oCFLEvento As SAPbouiCOM.IChooseFromListEvent
                oCFLEvento = pVal

                Dim sCFL_ID As String
                sCFL_ID = oCFLEvento.ChooseFromListUID
                If oCFLEvento.BeforeAction = False Then
                    Dim oCFL As SAPbouiCOM.ChooseFromList
                    oCFL = oForm.ChooseFromLists.Item(sCFL_ID)

                    Dim oDataTable As SAPbouiCOM.DataTable
                    oDataTable = oCFLEvento.SelectedObjects
                    Dim val As String = Nothing
                    Dim val1 As String = Nothing
                    Try
                        val = oDataTable.GetValue("CardCode", 0)
                        val1 = oDataTable.GetValue("CardName", 0)
                    Catch ex As Exception

                    End Try
                    If (sItemId = "IDH_TIPPIN") Or (pVal.ItemUID = "IDH_TIPCF") Then
                        With getFormMainDataSource(oForm)
                            .SetValue("U_Tip", .Offset, val)
                            .SetValue("U_TipNm", .Offset, val1)
                        End With
                        oForm.Items.Item("IDH_TIPPIN").Update()
                        oForm.Items.Item("IDH_TIPNM").Update()
                        'doGetTipCostPrices(oForm)
                        oWOR.doGetTipCostPrice(True)
                    ElseIf (sItemId = "IDH_SUPCOD") Or (pVal.ItemUID = "IDH_SUPCF") Then
                        With getFormMainDataSource(oForm)
                            .SetValue("U_ProCd", .Offset, val)
                            .SetValue("U_ProNm", .Offset, val1)
                        End With
                        oForm.Items.Item("IDH_SUPCOD").Update()
                        oForm.Items.Item("IDH_SUPNAM").Update()
                        If Config.INSTANCE.doGetDoBuyingAndTrade() Then
                            oWOR.doGetProducerCostPrice(True)
                        End If
                    ElseIf (sItemId = "IDH_LICSUP") Or (pVal.ItemUID = "IDH_LCCF") Then
                        With getFormMainDataSource(oForm)
                            .SetValue("U_SLicSp", .Offset, val)
                            .SetValue("U_SLicNm", .Offset, val1)
                        End With
                        oForm.Items.Item("IDH_LICSUP").Update()
                        oForm.Items.Item("IDH_SLNM").Update()
                    ElseIf (sItemId = "IDH_CARRIE") Or (pVal.ItemUID = "IDH_CCCF") Then
                        With getFormMainDataSource(oForm)
                            .SetValue("U_CarrCd", .Offset, val)
                            .SetValue("U_CarrNm", .Offset, val1)
                        End With
                        oForm.Items.Item("IDH_CARRIE").Update()
                        oForm.Items.Item("IDH_CARNAM").Update()
                        oWOR.doGetHaulageCostPrice(True)
                    End If
                    doSetUpdate(oForm)
                End If
                'OnTime 32355: WR1_NewDev_Buying&Trading_Add functionality to log Activity with a WOR
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE Then
                'On Activate reloading the Activity Grid
                If pVal.BeforeAction = False Then
                    doReloadActivity(oForm)
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If pVal.BeforeAction = True Then
                    If pVal.CharPressed = 13 Then
                        'To avoid supressing the ENTER in Comments fields
                        If Not (pVal.ItemUID = "IDH_COMM" Or pVal.ItemUID = "IDH_COMM2" Or pVal.ItemUID = "IDH_EXTCM1" Or pVal.ItemUID = "IDH_EXTCM2") Then
                            goParent.goApplication.SendKeys("{TAB}")
                            BubbleEvent = False
                        End If
                    End If
                Else
                    'If doCheckSearchKey(pVal) = True Then
                    '    'IDH_LICSUP, IDH_TIPPIN, IDH_CARRIE
                    '    If sItemId = "IDH_CARRIE" OrElse pVal.ItemUID = "IDH_CARNAM" Then
                    '        Dim sCode As String = getItemValue(oForm, "IDH_CARRIE")
                    '        Dim sName As String = getItemValue(oForm, "IDH_CARNAM")
                    '        If sCode.Length > 0 AndAlso (sCode.StartsWith("*") OrElse sCode.EndsWith("*")) Then
                    '            setItemValue(oForm, "IDH_CARNAM", "")
                    '        ElseIf sName.Length > 0 AndAlso (sName.StartsWith("*") OrElse sName.EndsWith("*")) Then
                    '            setItemValue(oForm, "IDH_CARRIE", "")
                    '        End If
                    '        doChooseCarrier(oForm, True, pVal.ItemUID)
                    '        'ElseIf sItemId = "IDH_TIPPIN" Then
                    '        '    Dim sCode As String = getItemValue(oForm, "IDH_TIPPIN")
                    '        '    '  Dim sName As String = getItemValue(oForm, "IDH_TIPNM")
                    '        '    If sCode.Length = 0 OrElse sCode.IndexOf("*") > -1 OrElse pVal.ItemChanged Then
                    '        '        setItemValue(oForm, "IDH_TIPNM", "")
                    '        '        doChooseSite(oForm, True, pVal.ItemUID)
                    '        '        'ElseIf sName.Length > 0 AndAlso (sName.StartsWith("*") OrElse sName.EndsWith("*")) Then
                    '        '        '    setItemValue(oForm, "IDH_TIPPIN", "")
                    '        '    End If

                    '        'ElseIf pVal.ItemUID = "IDH_TIPNM" Then
                    '        '    ' Dim sCode As String = getItemValue(oForm, "IDH_TIPPIN")
                    '        '    Dim sName As String = getItemValue(oForm, "IDH_TIPNM")
                    '        '    If (sName.Length > 0 AndAlso sName.IndexOf("*")) OrElse pVal.ItemChanged Then
                    '        '        setItemValue(oForm, "IDH_TIPPIN", "")
                    '        '        doChooseSite(oForm, True, pVal.ItemUID)
                    '        '    End If

                    '        'ElseIf sItemId = "IDH_LICSUP" OrElse pVal.ItemUID = "IDH_SLNM" Then
                    '        '    Dim sCode As String = getItemValue(oForm, "IDH_LICSUP")
                    '        '    Dim sName As String = getItemValue(oForm, "IDH_SLNM")
                    '        '    If sCode.Length > 0 AndAlso (sCode.StartsWith("*") OrElse sCode.EndsWith("*")) Then
                    '        '        setItemValue(oForm, "IDH_SLNM", "")
                    '        '    ElseIf sName.Length > 0 AndAlso (sName.StartsWith("*") OrElse sName.EndsWith("*")) Then
                    '        '        setItemValue(oForm, "IDH_LICSUP", "")
                    '        '    End If
                    '        '    doChooseLic(oForm, True, pVal.ItemUID)
                    '    ElseIf (sItemId = "IDH_SUPCOD" OrElse sItemId = "IDH_SUPNAM") Then
                    '        doChooseSupplier(oForm, True, sItemId)
                    '    End If
                    'End If

                    If pVal.CharPressed = 9 Then
                        ''## MA Start 18-11-2014 
                        'If sItemId = "IDH_ITMCOD" OrElse sItemId = "IDH_DESC" Then
                        'Dim sItem As String
                        'Dim sGrp As String
                        'Dim sContName As String = String.Empty

                        'With getFormMainDataSource(oForm)
                        '    sItem = .GetValue("U_ItemCd", .Offset).Trim()
                        '    sGrp = .GetValue("U_ItmGrp", .Offset).Trim()
                        '    sContName = .GetValue("U_ItemDsc", .Offset).Trim()
                        'End With

                        'setSharedData(oForm, "TRG", "PROD")
                        'setSharedData(oForm, "IDH_ITMCOD", sItem)
                        'setSharedData(oForm, "IDH_GRPCOD", sGrp)
                        'setSharedData(oForm, "IDH_ITMNAM", sContName)
                        'setSharedData(oForm, "SILENT", "SHOWMULTI")
                        'goParent.doOpenModalForm("IDHISRC", oForm)

                        'Else
                        ''## MA End 18-11-2014 

                        ''MA Start 27-11-2014
                        'If sItemId = "IDH_WASCL1" Then
                        '    '## Start 01-10-2013
                        '    doChooseWasteCode(oForm)
                        '    '## End
                        'ElseIf sItemId = "IDH_WASMAT" Then
                        '    Dim sDesc As String = getItemValue(oForm, "IDH_WASMAT")
                        '    '## Start 01-10-2013
                        '    If doNeedSearch(sDesc) Then
                        '        doChooseWasteCode(oForm)
                        '    End If
                        ''Dim sGrp As String = Config.INSTANCE.doWasteMaterialGroup(goParent)
                        ''Dim sItem As String
                        ''With getFormMainDataSource(oForm)
                        ''    sItem = .GetValue("U_WasCd", .Offset).Trim()
                        ''End With

                        ''If doNeedSearch(sDesc) Then
                        ''    setSharedData(oForm, "TP", "WAST")
                        ''    setSharedData(oForm, "IDH_ITMCOD", sItem)
                        ''    setSharedData(oForm, "IDH_GRPCOD", sGrp)
                        ''    setSharedData(oForm, "IDH_CRDCD", getFormDFValue(oForm, "U_CustCd"))
                        ''    setSharedData(oForm, "IDH_ITMNAM", sDesc)
                        ''    setSharedData(oForm, "SILENT", "SHOWMULTI")
                        ''    If Config.INSTANCE.getParameterAsBool("USAREL", False) = True Then
                        ''        setSharedData(oForm, "IDH_INVENT", "")
                        ''        setSharedData(oForm, "IDH_BPADDR", getUFValue(oForm, "IDH_ADDR").ToString()) 'Filter for Address
                        ''        goParent.doOpenModalForm("IDHWPWISRC", oForm)
                        ''    Else
                        ''        setSharedData(oForm, "IDH_INVENT", "") '"N")
                        ''        setSharedData(oForm, "IDH_JOBTP", getFormDFValue(oForm, IDH_JOBSHD._JobTp))
                        ''        goParent.doOpenModalForm("IDHWISRC", oForm)
                        ''    End If
                        ''End If
                        ''## End
                        ''ElseIf sItemId = "IDH_SERNR" Then
                        ''Dim oData As New ArrayList
                        ''With getFormMainDataSource(oForm)
                        ''    oData.Add("SER")
                        ''    oData.Add(.GetValue("U_ItmGrp", .Offset).Trim())
                        ''    oData.Add(.GetValue("U_Serial", .Offset).Trim())
                        ''    oData.Add(.GetValue("U_ItemCd", .Offset).Trim())
                        ''End With
                        ''goParent.doOpenModalForm("IDHSSRCH", oForm, oData)
                        'Else
                        'If sItemId = "IDH_LORRY" Then
                        '    ''MA End 27-11-2014
                        '    Dim sAct As String = getWFValue(oForm, "LastVehAct")
                        '    If sAct = "DOSEARCH" Then
                        '        doCallLorrySearchEB(oForm)
                        '        ''With getFormMainDataSource(oForm)
                        '        'setSharedData(oForm, "IDH_VEHREG", getFormDFValue(oForm, "U_Lorry"))

                        '        'If Config.INSTANCE.getParameterAsBool("WOVESEBC", False) = True Then
                        '        '    setSharedData(oForm, "IDH_VEHCUS", "")
                        '        'Else
                        '        '    setSharedData(oForm, "IDH_VEHCUS", getFormDFValue(oForm, "U_CustCd"))
                        '        'End If

                        '        'If Config.INSTANCE.getParameterAsBool("WOVESEAS", False) = True Then
                        '        '    setSharedData(oForm, "IDH_VEHTP", "A")
                        '        'Else
                        '        '    If getFormDFValue(oForm, "U_CarrCd").Equals(goParent.goDICompany.CompanyName) Then
                        '        '        setSharedData(oForm, "IDH_VEHTP", "A")
                        '        '    End If
                        '        'End If

                        '        'setSharedData(oForm, "IDH_WR1OT", "^WO")
                        '        'setSharedData(oForm, "SHOWCREATE", "TRUE")
                        '        'setSharedData(oForm, "SHOWACTIVITY", Config.Parameter("WOSVAC"))
                        '        'goParent.doOpenModalForm("IDHLOSCH", oForm)
                        '        ''End With
                        '    End If
                        If sItemId = "ADDGRID" Then
                            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, pVal.ItemUID)
                            If oGridN.doCheckIsSameCol(pVal.ColUID, "U_EmpId") Then
                                'Dim sValue As String = oGridN.doGetFieldValue("U_EmpId")
                                ''If sValue = "*" Then
                                ''    Dim sItemCd As String = oGridN.doGetFieldValue("U_ItemCd")
                                ''    setSharedData(oForm, "IDH_EMPRID", sItemCd)
                                ''    goParent.doOpenModalForm("IDHEMPSCR", oForm)
                                ''End If
                                'Dim sItemCd As String = oGridN.doGetFieldValue("U_ItemCd")
                                'setSharedData(oForm, "IDH_EMPRID", sItemCd)
                                'goParent.doOpenModalForm("IDHEMPSCR", oForm)
                            ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "U_SuppCd") Then
                                '                                Dim sValue As String = oGridN.doGetFieldValue("U_SuppCd")
                                '                                setSharedData(oForm, "TRG", "SUP")
                                '                                setSharedData(oForm, "IDH_TYPE", "%F-S")
                                '                                goParent.doOpenModalForm("IDHCSRCH", oForm)
                                '                                'ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "U_ItemCd") Then
                                '                                '    Dim sValue As String = oGridN.doGetFieldValue("U_ItemCd")
                                '                                '    Dim sAddGrp As String = Config.INSTANCE.doGetAdditionalExpGroup()
                                '                                '    setSharedData(oForm, "TRG", "ADD")
                                '                                '    setSharedData(oForm, "IDH_GRPCOD", sAddGrp)

                                '                                '    setSharedData(oForm, "NOT_GRPCOD", Config.ParameterWithDefault("IGNWSTGP", ""))
                                '                                '    goParent.doOpenModalForm("IDHISRC", oForm)
                            End If
                            oGridN = Nothing
                            ''## MA STart Issue#407
                        ElseIf sItemId = "IDH_RTCD" Then
                            setSharedData(oForm, "IDH_RTCODE", getItemValue(oForm, "IDH_RTCD"))
                            setSharedData(oForm, "SILENT", "SHOWMULTI")
                            goParent.doOpenModalForm("IDHROSRC", oForm)
                            ''## MA End Issue#407
                        End If
                    Else
                        If sItemId = "IDH_VEHREG" Then ' AndAlso pVal.ItemChanged Then
                            setWFValue(oForm, "LastVehAct", "DOSEARCH")
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                If pVal.BeforeAction = True Then
                    Dim sUOM As String
                    If sItemId = "IDH_PUOM" Then
                        sUOM = getFormDFValue(oForm, IDH_JOBSHD._PUOM)
                        setWFValue(oForm, "PREVUOM", sUOM)
                    ElseIf sItemId = "IDH_PURUOM" Then
                        sUOM = getFormDFValue(oForm, IDH_JOBSHD._ProUOM)
                        setWFValue(oForm, "PREVUOM", sUOM)
                    ElseIf sItemId = "IDH_UOM" Then
                        sUOM = getFormDFValue(oForm, IDH_JOBSHD._UOM)
                        setWFValue(oForm, "PREVUOM", sUOM)


                    End If
                Else
                    If sItemId = "uBC_CUSCHG" Then
                        oWOR.doCalculateTipChargeTotals(com.idh.dbObjects.User.Shared.OrderRow.TOTAL_CALCULATE)
                    ElseIf sItemId = "uBC_CUSCHR" Then
                        oWOR.doCalculateHaulageChargeTotals(com.idh.dbObjects.User.Shared.OrderRow.TOTAL_CALCULATE)
                    ElseIf sItemId = "uBC_PRCOST" Then
                        oWOR.doCalculateProducerCostTotals(com.idh.dbObjects.User.Shared.OrderRow.TOTAL_CALCULATE)
                    ElseIf sItemId = "uBC_TIPCOS" Then
                        oWOR.doCalculateTipCostTotals(com.idh.dbObjects.User.Shared.OrderRow.TOTAL_CALCULATE)
                    ElseIf sItemId = "uBC_ORDCOS" Then
                        oWOR.doCalculateHaulageCostTotals(com.idh.dbObjects.User.Shared.OrderRow.TOTAL_CALCULATE)
                    ElseIf sItemId = "IDH_JOBTPE" Then
                        'UPDATE THE ADDITIONAL GRID FROM HERE FOR NOW - IN FUTURE THE DBOBJECT WILL BE USED
                        doRefreshAdditionalPrices(oForm)

                        doAddAutoAdditionalCodes(oForm)
                        oWOR.doGetAllUOM()
                        oWOR.doGetAllPrices()

                        If Config.INSTANCE.getParameterAsBool("USEBPACH", False) Then
                            'Remove any existing AutoAdd rows 
                            'This is we dont know any items being added for this BP previously 
                            removeAutoAddBPAddCharges(oForm)
                            'Now add any new AddCharges row 
                            getBPAdditionalCharges(oForm)
                        End If
                    ElseIf sItemId = "IDH_PUOM" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._PUOM)

                        'Ask To Save to SIP
                        doRequestCIPSIPUpdate(oForm, "ASKSIP", WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.PUOM)
                    ElseIf sItemId = "IDH_PURUOM" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._ProUOM)

                        'Ask To Save to SIP
                        doRequestCIPSIPUpdate(oForm, "ASKSIP", WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.PRUOM)
                    ElseIf sItemId = "IDH_UOM" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._UOM)

                        'UPDATE THE ADDITIONAL GRID FROM HERE FOR NOW - IN FUTURE THE DBOBJECT WILL BE USED
                        doRefreshAdditionalPrices(oForm)

                        doAddAutoAdditionalCodes(oForm)


                        'Ask To Save to CIP
                        doRequestCIPSIPUpdate(oForm, "ASKCIP", WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TUOM)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                doValidationEvent(oForm, pVal, BubbleEvent)
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If sItemId = "IDH_CHRPL" Then
                        'Check If the Prices needs to be looked up
                        oWOR.doDelayedPrices(True, False, True)
                    ElseIf sItemId = "IDH_CSTPL" Then
                        'Check If the Prices needs to be looked up
                        oWOR.doDelayedPrices(False, True, True)
                    ElseIf sItemId = "IDH_USEEXP" Then
                        Dim dExpWeight As Double = getFormDFValue(oForm, IDH_JOBSHD._ExpLdWgt)
                        If isItemEnabled(oForm, "IDH_RDWGT") Then
                            setFormDFValue(oForm, IDH_JOBSHD._RdWgt, dExpWeight)
                            setFormDFValue(oForm, IDH_JOBSHD._CstWgt, dExpWeight)

                            ''20150617: WOSYWE was setting values for both Disposal and Purchase sides 
                            ''have to split the same config so user can set value based on config key 
                            ''Have kept the old logic as is and in its ELSE block add the code for new config keys 
                            Dim sDoSynch As String = Config.Parameter("WOSYWE")
                            If sDoSynch.ToUpper.Equals("TRUE") Then
                                Dim dTipWeight As Double '= getFormDFValue(oForm, IDH_JOBSHD._TRdWgt)
                                Dim sUOM As String = getFormDFValue(oForm, IDH_JOBSHD._UOM)
                                Dim sPUOM As String = getFormDFValue(oForm, IDH_JOBSHD._PUOM)
                                dTipWeight = Config.INSTANCE.doConvertWeights(sUOM, sPUOM, dExpWeight)

                                setFormDFValue(oForm, IDH_JOBSHD._TRdWgt, dTipWeight)
                                setFormDFValue(oForm, IDH_JOBSHD._TipWgt, dTipWeight)
                                oWOR.doGetTipCostPrice(True)
                            Else
                                Dim sDispTonSynch As String = Config.Parameter("WOSYWEDQ")
                                If sDispTonSynch.ToUpper.Equals("TRUE") Then
                                    Dim dTipWeight As Double '= getFormDFValue(oForm, IDH_JOBSHD._TRdWgt)
                                    Dim sUOM As String = getFormDFValue(oForm, IDH_JOBSHD._UOM)
                                    Dim sPUOM As String = getFormDFValue(oForm, IDH_JOBSHD._PUOM)
                                    dTipWeight = Config.INSTANCE.doConvertWeights(sUOM, sPUOM, dExpWeight)

                                    'Wrong settings
                                    'setFormDFValue(oForm, IDH_JOBSHD._TipWgt, dTipWeight) 'Disposal Quantity 
                                    setFormDFValue(oForm, IDH_JOBSHD._TRdWgt, dTipWeight) 'Disposal Quantity 
                                    setFormDFValue(oForm, IDH_JOBSHD._TipWgt, dTipWeight) 'Disposal Quantity 
                                    oWOR.doGetTipCostPrice(True)
                                End If

                                Dim sPurcWgtSynch As String = Config.Parameter("WOSYWEPQ")
                                If sPurcWgtSynch.ToUpper.Equals("TRUE") Then
                                    Dim dTipWeight As Double '= getFormDFValue(oForm, IDH_JOBSHD._TRdWgt)
                                    Dim sUOM As String = getFormDFValue(oForm, IDH_JOBSHD._UOM)
                                    Dim sPUOM As String = getFormDFValue(oForm, IDH_JOBSHD._PUOM)
                                    dTipWeight = Config.INSTANCE.doConvertWeights(sUOM, sPUOM, dExpWeight)

                                    'Wrong settings 
                                    'setFormDFValue(oForm, IDH_JOBSHD._TRdWgt, dTipWeight) 'Purchased Quanity 
                                    setFormDFValue(oForm, IDH_JOBSHD._PRdWgt, dTipWeight) 'Purchased Quanity 
                                    setFormDFValue(oForm, IDH_JOBSHD._ProWgt, dTipWeight) 'Purchased Quanity 
                                    oWOR.doGetProducerCostPrice(True)
                                End If
                            End If

                            oWOR.doGetChargePrices(True)
                        End If

                        If isItemEnabled(oForm, "IDH_PURWGT") Then
                            setFormDFValue(oForm, IDH_JOBSHD._PRdWgt, dExpWeight)
                            setFormDFValue(oForm, IDH_JOBSHD._ProWgt, dExpWeight)
                            oWOR.doGetProducerCostPrice(True)
                        End If
                    ElseIf sItemId = "IDH_USERE" OrElse sItemId = "IDH_USEAU" Then
                        oWOR.setUserChanged(Config.MASK_TIPCSTWGT, False)
                        oWOR.setUserChanged(Config.MASK_PRODUCERCSTWGT, False)
                        oWOR.setUserChanged(Config.MASK_TIPCHRGWGT, False)

                        oWOR.doMarkUserUpdate(IDH_DISPROW._UseWgt)

                        ''oWOR.doCalculateTipChargeWeight()
                        'oWOR.doGetCustomerUOM()

                        'If isItemEnabled(oForm, "IDH_PURWGT") Then
                        '    oWOR.doGetProducerCostPrice(True)
                        'End If
                    ElseIf sItemId.Equals("IDHCSTREF") Then
                        doChooseCustomerRef(oForm)
                    ElseIf sItemId = "IDH_BINCOL" Then
                        setSharedData(oForm, "IDH_WOR", getFormDFValue(oForm, "Code"))
                        setSharedData(oForm, "IDH_ACT", "C")
                        setSharedData(oForm, "IDH_CNT", getFormDFValue(oForm, "U_ItemCd"))
                        goParent.doOpenModalForm("IDH_WOCNTLST", oForm)
                    ElseIf sItemId = "IDH_BINDEL" Then
                        setSharedData(oForm, "IDH_WOR", getFormDFValue(oForm, "Code"))
                        setSharedData(oForm, "IDH_ACT", "D")
                        setSharedData(oForm, "IDH_CNT", getFormDFValue(oForm, "U_ItemCd"))
                        goParent.doOpenModalForm("IDH_WOCNTLST", oForm)
                    ElseIf sItemId = "IDH_HAULAC" Then
                        oWOR.doCalculateHaulageChargeTotals("M")
                    ElseIf sItemId = "IDH_TIPCF" Then
                        doChooseSite(oForm, False, "")
                    ElseIf sItemId = "IDH_LCCF" Then
                        doChooseLic(oForm, False, "")
                    ElseIf sItemId = "IDH_HAZARR" Then
                        Dim sConNum As String = getFormDFValue(oForm, "U_ConNum")
                        setSharedData(oForm, "IDH_HAZCN", sConNum)
                        goParent.doOpenModalForm("IDHCONMWO", oForm)
                    ElseIf sItemId = "IDH_CCCF" Then
                        doChooseCarrier(oForm, False, "")
                    ElseIf sItemId = "IDH_CSCF" Then
                        doChooseComplianceScheme(oForm, False)
                    ElseIf sItemId = "IDH_SUPCF" Then
                        doChooseSupplier(oForm, False, "")
                    ElseIf sItemId = "IDH_2RD" OrElse
                                        pVal.ItemUID = "IDH_2RW" OrElse
                                        pVal.ItemUID = "IDH_2RM" OrElse
                                        pVal.ItemUID = "IDH_2RY" Then
                        oForm.DataSources.UserDataSources.Item("U_2FMO").ValueEx = "N"
                        oForm.DataSources.UserDataSources.Item("U_2FTU").ValueEx = "N"
                        oForm.DataSources.UserDataSources.Item("U_2FWE").ValueEx = "N"
                        oForm.DataSources.UserDataSources.Item("U_2FTH").ValueEx = "N"
                        oForm.DataSources.UserDataSources.Item("U_2FFR").ValueEx = "N"
                        oForm.DataSources.UserDataSources.Item("U_2FSA").ValueEx = "N"
                        oForm.DataSources.UserDataSources.Item("U_2FSU").ValueEx = "N"
                        oForm.DataSources.UserDataSources.Item("U_2FRE").ValueEx = "0"
                    ElseIf sItemId = "IDH_DOC" Then
                        doDocReport(oForm)
                    ElseIf sItemId = "IDH_PBILK" Then
                        Dim sPBICode As String = getFormDFValue(oForm, "U_LnkPBI")
                        setSharedData(oForm, "PBICode", sPBICode)
                        setSharedData(oForm, "TRG", "IDH_PBI")
                        goParent.doOpenModalForm("IDH_PBI", oForm)
                    ElseIf sItemId = "IDH_PBIDTL" Then
                        setSharedData(oForm, "IDH_WOR", getFormDFValue(oForm, "Code"))
                        goParent.doOpenModalForm("IDHPBID", oForm)

                        'OnTime 32355: WR1_NewDev_Buying&Trading_Add functionality to log Activity with a WOR
                        'Event handling of the Activity button 'IDH_ACTVTY'
                    ElseIf sItemId = "IDH_ACTVTY" Then
                        If oForm.PaneLevel <> 103 Then
                            oForm.PaneLevel = 103
                        End If
                        setSharedData(oForm, "IDH_WOH", getFormDFValue(oForm, "U_JobNr"))
                        setSharedData(oForm, "IDH_WOR", getFormDFValue(oForm, "Code"))
                        setSharedData(oForm, "IDH_CUST", getFormDFValue(oForm, "U_CustCd"))
                        setSharedData(oForm, "sFMode", SAPbouiCOM.BoFormMode.fm_ADD_MODE.ToString())
                        goParent.doOpenModalForm("651", oForm)
                    ElseIf sItemId = "IDH_ORDLK" Then
                        Dim sOrd As String = getFormDFValue(oForm, "U_WROrd")
                        Dim sRow As String = getFormDFValue(oForm, "U_WRRow")
                        If sOrd.Length > 0 Then
                            Dim oData As New ArrayList
                            oData.Add("DoUpdate")
                            oData.Add(sOrd)
                            setSharedData(oForm, "ROWCODE", sRow)
                            goParent.doOpenModalForm("IDH_DISPORD", oForm, oData)
                        End If
                    ElseIf sItemId = "IDH_CIP" Then
                        Dim sCardCode As String = getFormDFValue(oForm, IDH_JOBSHD._CustCd)
                        Dim sItemCode As String = getFormDFValue(oForm, IDH_JOBSHD._ItemCd)
                        Dim sItemGrp As String = getFormDFValue(oForm, IDH_JOBSHD._ItmGrp)
                        Dim sWastCd As String = getFormDFValue(oForm, IDH_JOBSHD._WasCd)
                        ' Dim sContainerCd As String = getFormDFValue(oForm, IDH_JOBSHD._ContNr)

                        Dim sAddress As String = getWFValue(oForm, "STADDR")
                        Dim sZipCode As String = getWFValue(oForm, "ZIPCODE")

                        Dim sBranch As String = getFormDFValue(oForm, IDH_JOBSHD._Branch)
                        Dim sJobType As String = getFormDFValue(oForm, IDH_JOBSHD._JobTp)
                        Dim sDocDate As String = getFormDFValue(oForm, IDH_JOBSHD._RDate)

                        Dim sUOM As String = getFormDFValue(oForm, IDH_JOBSHD._UOM)
                        Dim dWeight As Double = getFormDFValue(oForm, IDH_JOBSHD._CstWgt)

                        Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.prices.CIP = New WR1_FR2Forms.idh.forms.fr2.prices.CIP(Nothing, oForm.UniqueID, Nothing)
                        oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doHandleCIPReturn)

                        oOOForm.CustomerCode = sCardCode
                        If Config.ParamaterWithDefault("WCPCSADR", "DEFAULT") = "DEFAULT" Then
                            oOOForm.Address = sAddress
                            oOOForm.ZipCode = sZipCode
                        End If
                        If Config.ParamaterWithDefault("WCPBRNCH", "ALL/BLANK") = "DEFAULT" Then
                            oOOForm.BrancheCode = sBranch
                        End If
                        If Config.ParamaterWithDefault("WCPCNCOD", "DEFAULT") = "DEFAULT" Then
                            oOOForm.ContainerCode = sItemCode
                        End If
                        If Config.ParamaterWithDefault("WCPORDTP", "ALL/BLANK") = "DEFAULT" Then
                            oOOForm.JobType = sJobType
                        End If
                        If Config.ParamaterWithDefault("WCPWSTCD", "DEFAULT") = "DEFAULT" Then
                            oOOForm.WasteCode = sWastCd
                        End If
                        If Config.ParamaterWithDefault("WCPWR1TP", "DEFAULT") = "DEFAULT" Then
                            oOOForm.WR1OrderType = "WO"
                        End If

                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            'oOOForm.ContainerCode = sItemCode
                            'oOOForm.WasteCode = sWastCd

                            'Dim bUseAny As String = Config.INSTANCE.getParameterAsBool("PRCDWRT", False)
                            'If bUseAny Then
                            '    oOOForm.WR1OrderType = ""
                            'Else
                            '    oOOForm.WR1OrderType = "WO"
                            'End If
                            oOOForm.FilterSet = WR1_FR2Forms.idh.forms.fr2.prices.CIP.WORADD
                        Else
                            oOOForm.FilterSet = WR1_FR2Forms.idh.forms.fr2.prices.CIP.WOROK
                        End If

                        oOOForm.FilterMode = 1
                        oOOForm.doShowModal()
                    ElseIf sItemId = "IDH_SIP" Then
                        Dim sDispSite As String = getFormDFValue(oForm, IDH_JOBSHD._Tip)
                        Dim sItemCode As String = getFormDFValue(oForm, IDH_JOBSHD._ItemCd)
                        Dim sItemGrp As String = getFormDFValue(oForm, IDH_JOBSHD._ItmGrp)
                        Dim sWastCd As String = getFormDFValue(oForm, IDH_JOBSHD._WasCd)
                        Dim sSuppAddress As String = getFormDFValue(oForm, IDH_JOBSHD._SAddress)

                        Dim sAddress As String = getWFValue(oForm, "STADDR")
                        Dim sZipCode As String = getWFValue(oForm, "ZIPCODE")

                        Dim sBranch As String = getFormDFValue(oForm, IDH_JOBSHD._Branch)
                        Dim sCustomer As String = getFormDFValue(oForm, IDH_JOBSHD._CustCd)
                        Dim sJobType As String = getFormDFValue(oForm, IDH_JOBSHD._JobTp)
                        Dim sDocDate As String = getFormDFValue(oForm, IDH_JOBSHD._RDate)

                        Dim dWeight As Double = getFormDFValue(oForm, IDH_JOBSHD._TipWgt)
                        Dim sPUOM As String = getFormDFValue(oForm, IDH_JOBSHD._PUOM)

                        Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.prices.SIP = New WR1_FR2Forms.idh.forms.fr2.prices.SIP(Nothing, oForm.UniqueID, Nothing)
                        oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doHandleDisposalSIPReturn)
                        'oOOForm.DelayedLoad = True

                        oOOForm.CustomerCode = sCustomer
                        oOOForm.SupplierCode = sDispSite
                        oOOForm.ItemGroup = sItemGrp

                        If Config.ParamaterWithDefault("WSPCNCOD", "DEFAULT") = "DEFAULT" Then
                            oOOForm.ContainerCode = sItemCode
                        End If
                        If Config.ParamaterWithDefault("WSPWSTCD", "DEFAULT") = "DEFAULT" Then
                            oOOForm.WasteCode = sWastCd
                        End If
                        If Config.ParamaterWithDefault("WSPWR1TP", "DEFAULT") = "DEFAULT" Then
                            oOOForm.WR1OrderType = "WO"
                        End If
                        If Config.ParamaterWithDefault("WSPCSADR", "ALL/BLANK") = "DEFAULT" Then
                            oOOForm.Address = sAddress
                        End If
                        If Config.ParamaterWithDefault("WSPPRADR", "ALL/BLANK") = "DEFAULT" Then
                            oOOForm.SupplierAddress = sSuppAddress
                        End If
                        If Config.ParamaterWithDefault("WSPBRNCH", "ALL/BLANK") = "DEFAULT" Then
                            oOOForm.BrancheCode = sBranch
                        End If
                        If Config.ParamaterWithDefault("WSPORDTP", "ALL/BLANK") = "DEFAULT" Then
                            oOOForm.JobType = sJobType
                        End If

                        oOOForm.FilterMode = 1

                        oOOForm.doShowModal()
                    ElseIf sItemId = "IDH_PSIP" Then
                        Dim sProdCode As String = getFormDFValue(oForm, IDH_JOBSHD._ProCd)
                        Dim sItemCode As String = getFormDFValue(oForm, IDH_JOBSHD._ItemCd)
                        Dim sItemGrp As String = getFormDFValue(oForm, IDH_JOBSHD._ItmGrp)
                        Dim sWastCd As String = getFormDFValue(oForm, IDH_JOBSHD._WasCd)

                        'Dim sPAddress As String = getFormDFValue(oForm, IDH_JOBSHD._p)

                        Dim sAddress As String = getWFValue(oForm, "STADDR")
                        Dim sZipCode As String = getWFValue(oForm, "ZIPCODE")

                        Dim sBranch As String = getFormDFValue(oForm, IDH_JOBSHD._Branch)
                        Dim sCustomer As String = getFormDFValue(oForm, IDH_JOBSHD._CustCd)
                        Dim sJobType As String = getFormDFValue(oForm, IDH_JOBSHD._JobTp)
                        Dim sDocDate As String = getFormDFValue(oForm, IDH_JOBSHD._RDate)

                        Dim dWeight As Double = getFormDFValue(oForm, IDH_JOBSHD._ProWgt)
                        Dim sPUOM As String = getFormDFValue(oForm, IDH_JOBSHD._ProUOM)

                        Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.prices.SIP = New WR1_FR2Forms.idh.forms.fr2.prices.SIP(Nothing, oForm.UniqueID, Nothing)
                        oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doHandleProducerSIPReturn)
                        'oOOForm.DelayedLoad = True

                        oOOForm.CustomerCode = sCustomer
                        oOOForm.SupplierCode = sProdCode
                        oOOForm.ItemGroup = sItemGrp

                        'oOOForm.ContainerCode = sItemCode
                        'oOOForm.WasteCode = sWastCd
                        'oOOForm.WR1OrderType = "WO"
                        If Config.ParamaterWithDefault("WSPCNCOD", "DEFAULT") = "DEFAULT" Then
                            oOOForm.ContainerCode = sItemCode
                        End If
                        If Config.ParamaterWithDefault("WSPWSTCD", "DEFAULT") = "DEFAULT" Then
                            oOOForm.WasteCode = sWastCd
                        End If
                        If Config.ParamaterWithDefault("WSPWR1TP", "DEFAULT") = "DEFAULT" Then
                            oOOForm.WR1OrderType = "WO"
                        End If
                        If Config.ParamaterWithDefault("WSPCSADR", "ALL/BLANK") = "DEFAULT" Then
                            oOOForm.Address = sAddress
                        End If
                        'If Config.ParamaterWithDefault("WSPPRADR", "ALL/BLANK") = "DEFAULT" Then
                        '    oOOForm.SupplierAddress = sSuppAddress
                        'End If
                        If Config.ParamaterWithDefault("WSPBRNCH", "ALL/BLANK") = "DEFAULT" Then
                            oOOForm.BrancheCode = sBranch
                        End If
                        If Config.ParamaterWithDefault("WSPORDTP", "ALL/BLANK") = "DEFAULT" Then
                            oOOForm.JobType = sJobType
                        End If

                        'oOOForm.JobType = sJobType
                        'oOOForm.DocDate = sDocDate
                        'oOOForm.Address = sAddress
                        'oOOForm.ZipCode = sZipCode
                        'oOOForm.BrancheCode = sBranch
                        'oOOForm.UOM = sPUOM
                        'oOOForm.Weight = dWeight

                        oOOForm.FilterMode = 1

                        oOOForm.doShowModal()

                        'oOOForm.CustomerCode = sCustomer
                        'oOOForm.SupplierCode = sProdCode
                        'oOOForm.ContainerCode = sItemCode
                        'oOOForm.WasteCode = sWastCd
                        'oOOForm.JobType = sJobType
                        'oOOForm.WR1OrderType = "WO"
                        'oOOForm.DocDate = sDocDate
                        'oOOForm.Address = sAddress
                        'oOOForm.ZipCode = sZipCode
                        'oOOForm.BrancheCode = sBranch
                        'oOOForm.UOM = sPUOM
                        'oOOForm.Weight = dWeight

                        'oOOForm.doLoadNow(2)

                    ElseIf sItemId = "IDH_WSIP" Then
                        Dim sWasteCarrier As String = getFormDFValue(oForm, IDH_JOBSHD._CarrCd)
                        Dim sItemCode As String = getFormDFValue(oForm, IDH_JOBSHD._ItemCd)
                        Dim sItemGrp As String = getFormDFValue(oForm, IDH_JOBSHD._ItmGrp)
                        Dim sWastCd As String = getFormDFValue(oForm, IDH_JOBSHD._WasCd)

                        Dim sAddress As String = getWFValue(oForm, "STADDR")
                        Dim sZipCode As String = getWFValue(oForm, "ZIPCODE")

                        Dim sBranch As String = getFormDFValue(oForm, IDH_JOBSHD._Branch)
                        Dim sCustomer As String = getFormDFValue(oForm, IDH_JOBSHD._CustCd)
                        Dim sJobType As String = getFormDFValue(oForm, IDH_JOBSHD._JobTp)
                        Dim sDocDate As String = getFormDFValue(oForm, IDH_JOBSHD._RDate)

                        Dim dWeight As Double = getFormDFValue(oForm, IDH_JOBSHD._OrdWgt)
                        Dim sPUOM As String = getFormDFValue(oForm, IDH_JOBSHD._PUOM)

                        Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.prices.SIP = New WR1_FR2Forms.idh.forms.fr2.prices.SIP(Nothing, oForm.UniqueID, Nothing)
                        oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doHandleCarrierSIPReturn)
                        'oOOForm.DelayedLoad = True

                        oOOForm.CustomerCode = sCustomer
                        oOOForm.SupplierCode = sWasteCarrier
                        oOOForm.ItemGroup = sItemGrp

                        'oOOForm.ContainerCode = sItemCode
                        'oOOForm.WasteCode = sWastCd
                        'oOOForm.WR1OrderType = "WO"
                        If Config.ParamaterWithDefault("WSPCNCOD", "DEFAULT") = "DEFAULT" Then
                            oOOForm.ContainerCode = sItemCode
                        End If
                        If Config.ParamaterWithDefault("WSPWSTCD", "DEFAULT") = "DEFAULT" Then
                            oOOForm.WasteCode = sWastCd
                        End If
                        If Config.ParamaterWithDefault("WSPWR1TP", "DEFAULT") = "DEFAULT" Then
                            oOOForm.WR1OrderType = "WO"
                        End If
                        If Config.ParamaterWithDefault("WSPCSADR", "ALL/BLANK") = "DEFAULT" Then
                            oOOForm.Address = sAddress
                        End If
                        'If Config.ParamaterWithDefault("WSPPRADR", "ALL/BLANK") = "DEFAULT" Then
                        '    oOOForm.SupplierAddress = sSuppAddress
                        'End If
                        If Config.ParamaterWithDefault("WSPBRNCH", "ALL/BLANK") = "DEFAULT" Then
                            oOOForm.BrancheCode = sBranch
                        End If
                        If Config.ParamaterWithDefault("WSPORDTP", "ALL/BLANK") = "DEFAULT" Then
                            oOOForm.JobType = sJobType
                        End If
                        'oOOForm.JobType = sJobType
                        'oOOForm.DocDate = sDocDate
                        'oOOForm.Address = sAddress
                        'oOOForm.ZipCode = sZipCode
                        'oOOForm.BrancheCode = sBranch
                        'oOOForm.UOM = sPUOM
                        'oOOForm.Weight = dWeight

                        oOOForm.FilterMode = 1

                        oOOForm.doShowModal()

                        'oOOForm.CustomerCode = sCustomer
                        'oOOForm.SupplierCode = sWasteCarrier
                        'oOOForm.ContainerCode = sItemCode
                        'oOOForm.WasteCode = sWastCd
                        'oOOForm.JobType = sJobType
                        'oOOForm.WR1OrderType = "WO"
                        'oOOForm.DocDate = sDocDate
                        'oOOForm.Address = sAddress
                        'oOOForm.ZipCode = sZipCode
                        'oOOForm.BrancheCode = sBranch
                        'oOOForm.UOM = sPUOM
                        'oOOForm.Weight = dWeight

                        'oOOForm.doLoadNow(2)

                    ElseIf sItemId = "IDH_GSIP" Then
                        Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.prices.SIP = New WR1_FR2Forms.idh.forms.fr2.prices.SIP(Nothing, oForm.UniqueID, Nothing)
                        oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doHandleAllSIPReturn)
                        'oOOForm.DelayedLoad = True

                        oOOForm.SupplierCode = ""
                        oOOForm.ContainerCode = ""
                        oOOForm.WasteCode = ""
                        oOOForm.JobType = "" 'sJobType
                        oOOForm.WR1OrderType = "" '"WO"
                        oOOForm.Address = ""
                        oOOForm.ZipCode = ""
                        oOOForm.BrancheCode = ""
                        oOOForm.ItemGroup = ""
                        oOOForm.FilterMode = 1

                        oOOForm.doShowModal()

                        'oOOForm.SupplierCode = ""
                        'oOOForm.ContainerCode = ""
                        'oOOForm.WasteCode = ""
                        'oOOForm.JobType = "" 'sJobType
                        'oOOForm.WR1OrderType = "" '"WO"
                        'oOOForm.Address = ""
                        'oOOForm.ZipCode = ""
                        'oOOForm.BrancheCode = ""

                        'oOOForm.doLoadNow(1)

                    ElseIf sItemId = "IDH_WABT" Then
                        Dim sCardCode As String = getFormDFValue(oForm, "U_CustCd")
                        Dim sAddress As String = getWFValue(oForm, "STADDR")

                        If sCardCode.Trim().Length > 0 Then
                            Dim sReportFile As String

                            sReportFile = Config.Parameter("WOCWAB")

                            Dim oParams As New Hashtable
                            oParams.Add("Customer", sCardCode)
                            oParams.Add("SiteAddr", sAddress)

                            If (Config.INSTANCE.useNewReportViewer()) Then
                                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                                IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
                            Else
                                IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
                            End If
                        End If
                    ElseIf sItemId = "IDH_2FMO" OrElse
                        pVal.ItemUID = "IDH_2FTU" OrElse
                           pVal.ItemUID = "IDH_2FWE" OrElse
                           pVal.ItemUID = "IDH_2FTH" OrElse
                           pVal.ItemUID = "IDH_2FFR" OrElse
                           pVal.ItemUID = "IDH_2FSA" OrElse
                           pVal.ItemUID = "IDH_2FSU" Then
                        oForm.DataSources.UserDataSources.Item("U_2RD").ValueEx = "5"
                    ElseIf sItemId = "IDH_TABCOS" Then
                        oForm.PaneLevel = 100
                    ElseIf sItemId = "IDH_TABSTD" Then
                        oForm.PaneLevel = 101
                    ElseIf sItemId = "IDH_TABADD" Then
                        oForm.PaneLevel = 102
                    ElseIf sItemId = "IDH_TABCOV" Then
                        oForm.PaneLevel = 2
                    ElseIf sItemId = "IDH_TABCOM" Then
                        oForm.PaneLevel = 3
                    ElseIf sItemId = "IDH_REMFLD" Then
                        oForm.PaneLevel = 5
                        'OnTime 32355: WR1_NewDev_Buying&Trading_Add functionality to log Activity with a WOR
                    ElseIf sItemId = "IDH_TABACT" Then
                        oForm.PaneLevel = 103
                    ElseIf sItemId = "IDH_TABACC" Then
                        oForm.PaneLevel = 104

                        '                    ElseIf sItemId = "IDH_JTC" Then
                        '                        Dim sGrp As String = getFormDFValue(oForm, "U_ItmGrp")
                        '                        setSharedData(oForm, "IDH_ITMGRP", sGrp)
                        '                        goParent.doOpenModalForm("IDHJTSRC", oForm)
                    ElseIf sItemId = "IDH_ITCF" Then
                        doChooseContainer(oForm, sItemId)
                    ElseIf sItemId = "IDH_WASCF" Then
                        'OnTime [#Ico000????] USA
                        'START
                        '## Start 01-10-2013
                        doChooseWasteCode(oForm, sItemId)
                        'setSharedData(oForm, "TP", "WAST")
                        'setSharedData(oForm, "IDH_ITMCOD", "")
                        'setSharedData(oForm, "IDH_CRDCD", getFormDFValue(oForm, "U_CustCd"))
                        'If Config.INSTANCE.getParameterAsBool("USAREL", False) = True Then
                        '    'USA Release
                        '    setSharedData(oForm, "IDH_INVENT", "")
                        '    setSharedData(oForm, "IDH_BPADDR", getUFValue(oForm, "IDH_ADDR").ToString()) 'Filter for Address
                        '    goParent.doOpenModalForm("IDHWPWISRC", oForm)
                        'Else
                        '    'Non-USA release
                        '    setSharedData(oForm, "IDH_INVENT", "") '"N")
                        '    setSharedData(oForm, "IDH_GRPCOD", Config.INSTANCE.doWasteMaterialGroup(goParent))
                        '    setSharedData(oForm, "IDH_JOBTP", getFormDFValue(oForm, IDH_JOBSHD._JobTp))
                        '    goParent.doOpenModalForm("IDHWISRC", oForm)
                        'End If
                        '## End
                        'END
                        'OnTime [#Ico000????] USA
                    ElseIf sItemId = "IDH_TFSSC" Then
                        setSharedData(oForm, "IDHTFSN", getFormDFValue(oForm, "U_TFSNUM"))
                        'EWC Code filter
                        'Get EWC Code from selected waste profile
                        Dim sWPItem As String = getFormDFValue(oForm, "U_WasCd")
                        If sWPItem IsNot Nothing AndAlso sWPItem.Length > 0 Then
                            setSharedData(oForm, "IDHEWCCD", getWPEWCCode(getFormDFValue(oForm, "U_WasCd")))
                        End If
                        setSharedData(oForm, "IDHTFSSTA", "01,05,06")
                        goParent.doOpenModalForm("IDHTFSNSR", oForm)
                    ElseIf sItemId = "IDH_TFSLK" Then
                        Dim sTFSNum As String = getFormDFValue(oForm, "U_TFSNUM")

                        If sTFSNum IsNot Nothing AndAlso sTFSNum.Length > 0 Then
                            'Open the TFS Mgmt. Console form and populate WP details as per 1st WOR
                            setSharedData(oForm, "TFSNUM", sTFSNum)
                            setSharedData(oForm, "WORNO", getFormDFValue(oForm, "Code"))
                            setSharedData(oForm, "WOHNO", getFormDFValue(oForm, "U_JobNr"))
                            goParent.doOpenForm("IDH_TFSMAST", oForm, False)
                        End If
                    ElseIf sItemId = "IDH_EVNSC" Then
                        setSharedData(oForm, "IDHEVNN", getFormDFValue(oForm, "U_EVNNum"))
                        'EWC Code filter
                        'Get EWC Code from selected waste profile
                        Dim sWPItem As String = getFormDFValue(oForm, "U_WasCd")
                        If sWPItem IsNot Nothing AndAlso sWPItem.Length > 0 Then
                            setSharedData(oForm, "IDHEWCCD", getWPEWCCode(getFormDFValue(oForm, "U_WasCd")))
                        End If
                        setSharedData(oForm, "IDHTFSSTA", "01,05,06")
                        goParent.doOpenModalForm("IDHEVNNSR", oForm)
                    ElseIf sItemId = "IDH_EVNLK" Then
                        Dim sEVNNum As String = getFormDFValue(oForm, "U_EVNNum")

                        If sEVNNum IsNot Nothing AndAlso sEVNNum.Length > 0 Then
                            'Open the EVN form 
                            setSharedData(oForm, "EVNNUM", sEVNNum)
                            goParent.doOpenForm("IDHEVNMAS", oForm, False)
                        End If
                    ElseIf sItemId = "IDH_EVNREQ" Then
                        Dim sEVNReq As String = getFormDFValue(oForm, "U_EVNReq")
                        If sEVNReq.Equals("Y") Then
                            setVisible(oForm, "1000018", True)
                            setVisible(oForm, "IDH_EVNLK", True)
                            setVisible(oForm, "IDH_EVNNUM", True)
                            setVisible(oForm, "IDH_EVNSC", True)
                        Else
                            setVisible(oForm, "1000018", False)
                            setVisible(oForm, "IDH_EVNLK", False)
                            setVisible(oForm, "IDH_EVNNUM", False)
                            setVisible(oForm, "IDH_EVNSC", False)
                        End If
                    ElseIf sItemId = "IDH_SRCF" Then
                        'SERIAL NUMBER SEARCH
                        Dim oData As New ArrayList
                        With getFormMainDataSource(oForm)
                            oData.Add("SER")
                            oData.Add(.GetValue("U_ItmGrp", .Offset).Trim())
                            oData.Add("")
                            oData.Add(.GetValue("U_ItemCd", .Offset).Trim())
                        End With
                        goParent.doOpenModalForm("IDHSSRCH", oForm, oData)
                    ElseIf sItemId = "IDH_VCRCF" Then
                        doCallLorrySearchCFL(oForm)
                        ''LORRY SEARCH
                        'With getFormMainDataSource(oForm)
                        '    setSharedData(oForm, "IDH_VEHREG", .GetValue("U_Lorry", .Offset))

                        '    If Config.INSTANCE.getParameterAsBool("WOVESEBC", False) = True Then
                        '        setSharedData(oForm, "IDH_VEHCUS", "")
                        '    Else
                        '        setSharedData(oForm, "IDH_VEHCUS", getFormDFValue(oForm, "U_CustCd"))
                        '    End If

                        '    If Config.INSTANCE.getParameterAsBool("WOVESEAS", False) = True Then
                        '        setSharedData(oForm, "IDH_VEHTP", "A")
                        '    Else
                        '        If getFormDFValue(oForm, "U_CarrCd").Equals(goParent.goDICompany.CompanyName) Then
                        '            setSharedData(oForm, "IDH_VEHTP", "A")
                        '        End If
                        '    End If

                        '    'setSharedData(oForm, "IDH_VEHCUS", getFormDFValue( oForm, "U_CustCd"))
                        '    'If getFormDFValue( oForm, "U_CarrCd").Equals(goParent.goDICompany.CompanyName) Then
                        '    '    setSharedData(oForm, "IDH_VEHTP", "A")
                        '    'End If

                        '    setSharedData(oForm, "IDH_WR1OT", "^WO")
                        '    setSharedData(oForm, "SHOWACTIVITY", Config.Parameter("WOSVAC"))
                        '    setSharedData(oForm, "SHOWCREATE", "TRUE")
                        '    goParent.doOpenModalForm("IDHLOSCH", oForm)
                        'End With
                    ElseIf sItemId = "IDH_SITAL" Then
                        Dim sCardCode As String = Nothing
                        sCardCode = getFormDFValue(oForm, "U_TIP")

                        setSharedData(oForm, "TRG", sItemId)
                        setSharedData(oForm, "IDH_CUSCOD", sCardCode)
                        setSharedData(oForm, "IDH_ADRTYP", "S")
                        ''##MA Start 18-09-2014 Issue#403
                        setSharedData(oForm, "IDH_APPLYFLTR", Config.INSTANCE.getParameterAsBool("FLTRADDR", False).ToString)
                        ''##MA End 18-09-2014 Issue#403
                        goParent.doOpenModalForm("IDHASRCH", oForm)
                    ElseIf sItemId = "IDH_REBATE" Then
                        oWOR.doCalculateRebate()
                    ElseIf sItemId = "IDH_DOORD" OrElse
                            sItemId = "IDH_DOARI" OrElse
                            sItemId = "IDH_FOC" OrElse
                            sItemId = "IDH_DOPO" OrElse
                            sItemId = "IDH_CUSTRE" OrElse
                            sItemId = "IDH_CARRRE" Then
                        Dim bDoMDDocs As Boolean
                        'Dim sPreFix As String
                        Dim bValueSet As Boolean = False

                        bDoMDDocs = Config.INSTANCE.getParameterAsBool("MDAGEN", False)
                        'If sDoMDDocs.ToUpper().Equals("TRUE") Then
                        '    sPreFix = Config.INSTANCE.getStatusPreFixDo()
                        'Else
                        '    sPreFix = Config.INSTANCE.getStatusPreFixReq()
                        'End If

                        Dim sStatus As String
                        Dim sAction As String
                        If bDoMDDocs Then
                            sAction = "D" 'Do
                        Else
                            sAction = "R" 'Request
                        End If

                        If sItemId = "IDH_DOARI" Then
                            If getUFValue(oForm, "IDH_DOARI") = "Y" Then
                                setSharedData(oForm, "ACTION", sAction)
                                goParent.doOpenModalForm("IDHPAYMET", oForm)
                                bValueSet = True
                            End If
                        ElseIf sItemId = "IDH_DOORD" Then
                            If getUFValue(oForm, "IDH_DOORD") = "Y" Then
                                If sAction = "D" Then
                                    sStatus = com.idh.bridge.lookups.FixedValues.getDoOrderStatus()
                                Else
                                    sStatus = com.idh.bridge.lookups.FixedValues.getReqOrderStatus()
                                End If

                                setFormDFValue(oForm, "U_Status", sStatus) 'sPreFix & com.idh.bridge.lookups.FixedValues.getStatusOrder())
                                setFormDFValue(oForm, "U_PayMeth", com.idh.bridge.lookups.FixedValues.getPayMethAccounts())
                                setFormDFValue(oForm, "U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusUnPaid())
                                setFormDFValue(oForm, "U_CCNum", "")
                                setFormDFValue(oForm, "U_CCType", "0")
                                setFormDFValue(oForm, "U_CCStat", "")
                                bValueSet = True
                            End If
                        ElseIf sItemId = "IDH_FOC" Then
                            If getUFValue(oForm, "IDH_FOC") = "Y" Then
                                If sAction = "D" Then
                                    sStatus = com.idh.bridge.lookups.FixedValues.getDoFocStatus()
                                Else
                                    sStatus = com.idh.bridge.lookups.FixedValues.getReqFocStatus()
                                End If

                                setFormDFValue(oForm, "U_Status", sStatus) 'sPreFix & com.idh.bridge.lookups.FixedValues.getStatusFoc())
                                setFormDFValue(oForm, "U_PayMeth", com.idh.bridge.lookups.FixedValues.getPayMethFoc())
                                setFormDFValue(oForm, "U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusFree())
                                setFormDFValue(oForm, "U_CCNum", "")
                                setFormDFValue(oForm, "U_CCType", "0")
                                setFormDFValue(oForm, "U_CCStat", "")
                                bValueSet = True
                            End If
                        ElseIf sItemId = "IDH_CUSTRE" Then
                            If getFormDFValue(oForm, "U_CustReb") = "Y" Then
                                If sAction = "D" Then
                                    sStatus = com.idh.bridge.lookups.FixedValues.getDoRebateStatus()
                                Else
                                    sStatus = com.idh.bridge.lookups.FixedValues.getReqRebateStatus()
                                End If

                                setFormDFValue(oForm, "U_Status", sStatus)
                                setFormDFValue(oForm, "U_PayMeth", com.idh.bridge.lookups.FixedValues.getPayMethFoc())
                                setFormDFValue(oForm, "U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusFree())
                                setFormDFValue(oForm, "U_CCNum", "")
                                setFormDFValue(oForm, "U_CCType", "0")
                                setFormDFValue(oForm, "U_CCStat", "")
                                bValueSet = True
                            End If
                        ElseIf sItemId = "IDH_DOPO" Then
                            If getUFValue(oForm, "IDH_DOPO") = "Y" Then
                                If sAction = "D" Then
                                    sStatus = com.idh.bridge.lookups.FixedValues.getDoOrderStatus()
                                Else
                                    sStatus = com.idh.bridge.lookups.FixedValues.getReqOrderStatus()
                                End If

                                setFormDFValue(oForm, "U_PStat", sStatus) 'sPreFix & com.idh.bridge.lookups.FixedValues.getStatusOrder())
                            Else
                                setFormDFValue(oForm, "U_PStat", com.idh.bridge.lookups.FixedValues.getStatusOpen())
                            End If
                            bValueSet = True
                        ElseIf sItemId = "IDH_CARRRE" Then
                            If getFormDFValue(oForm, "U_CarrReb") = "Y" Then
                                If sAction = "D" Then
                                    sStatus = com.idh.bridge.lookups.FixedValues.getDoRebateStatus()
                                Else
                                    sStatus = com.idh.bridge.lookups.FixedValues.getReqRebateStatus()
                                End If

                                setFormDFValue(oForm, "U_PStat", sStatus)
                            Else
                                setFormDFValue(oForm, "U_PStat", com.idh.bridge.lookups.FixedValues.getStatusOpen())
                            End If
                            bValueSet = True
                        End If

                        If bValueSet = False Then
                            setFormDFValue(oForm, "U_Status", com.idh.bridge.lookups.FixedValues.getStatusOpen())
                            setFormDFValue(oForm, "U_PayMeth", "")
                            setFormDFValue(oForm, "U_PayStat", "")
                            setFormDFValue(oForm, "U_CCNum", "")
                            setFormDFValue(oForm, "U_CCType", "0")
                            setFormDFValue(oForm, "U_CCStat", "")
                        End If
                    ElseIf sItemId = "IDH_DOJRN" Then
                        If getUFValue(oForm, "IDH_DOJRN") = "Y" Then
                            setFormDFValue(oForm, "U_JStat", com.idh.bridge.lookups.FixedValues.getDoJrnlStatus())
                        Else
                            setFormDFValue(oForm, "U_JStat", "")
                        End If
                    ElseIf sItemId = "IDH_DOCS" Then
                        doDocumentsReport(oForm)
                    ElseIf sItemId = "IDH_GETSIT" Then
                        'Get all sites from @IDH_SUITPR
                        Dim sCustPostCd As String = getUFValue(oForm, "IDH_ZP").ToString()
                        If sCustPostCd IsNot Nothing AndAlso sCustPostCd.Length > 0 Then
                            Dim sContCd As String
                            Dim sMaterialCd As String
                            With getFormMainDataSource(oForm)
                                sContCd = .GetValue("U_ItemCd", .Offset).Trim()
                                sMaterialCd = .GetValue("U_WasCd", .Offset).Trim()
                            End With

                            'Dim sQry As String = "select U_CardCd, U_SupZipCd, U_WasteCd, U_WasteDs, U_ItemCd, U_ItemDs, * from [@IDH_SUITPR] " + _
                            '    " where IsNull(U_WasteCd, '') = '" + sMaterialCd + "' AND IsNull(U_ItemCd, '') = '" + sContCd + "' "

                            Dim sQry As String = "select U_CardCd, U_SupZipCd, U_WasteCd, U_WasteDs, U_ItemCd, U_ItemDs, * from [@IDH_SUITPR] " +
                                                " where IsNull(U_WasteCd, '') = '" + sMaterialCd + "' " +
                                                " AND ( U_ItemCd = '" + sContCd + "' OR U_ItemCd = '' OR U_ItemCd IS NULL )"

                            Dim sPostCodeList As New ArrayList()
                            Dim oRecordset As SAPbobsCOM.Recordset
                            oRecordset = goParent.goDB.doSelectQuery(sQry)
                            If oRecordset.RecordCount > 0 Then
                                While Not oRecordset.EoF
                                    If oRecordset.Fields.Item("U_SupZipCd").Value.ToString().Length > 0 Then
                                        sPostCodeList.Add(oRecordset.Fields.Item("U_SupZipCd").Value)
                                    End If
                                    oRecordset.MoveNext()
                                End While
                            End If
                            IDHAddOns.idh.data.Base.doReleaseObject(oRecordset)

                            'validate customer & site post codes in mileage 
                            If sPostCodeList.Count > 0 Then
                                Dim iRecCtr As Integer = 0
                                For Each sSupPostCd As String In sPostCodeList
                                    sQry = String.Empty
                                    sQry = "select code from [@IDH_MILEAGES] " +
                                    " where U_CustZip = '" + sCustPostCd + "' and U_SupZip = '" + sSupPostCd + "'"
                                    Dim oValidateRS As SAPbobsCOM.Recordset
                                    oValidateRS = goParent.goDB.doSelectQuery(sQry)
                                    If oValidateRS.RecordCount = 0 Then
                                        'Calculate Distance 
                                        Dim sDurDis As String = doCalculateSiteDistances(sCustPostCd, sSupPostCd)
                                        'Insert in Mileage table 
                                        If Not sDurDis = "-1" Then

                                            Dim sDistanceKm As String = Convert.ToDouble(sDurDis.Split("##")(2)) / 1000
                                            Dim sDurationHrs As String = Convert.ToDouble(sDurDis.Split("##")(0)) / 3600 'sDurDis.Split("##")(0) 

                                            Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, "GENSEQ")
                                            Dim sInsertQry As New System.Text.StringBuilder
                                            sInsertQry.Append("INSERT INTO [@IDH_MILEAGES]")
                                            sInsertQry.Append(" ([Code], [Name], [U_SupZip], [U_CustZip], [U_Distance], [U_Duration]) ")
                                            sInsertQry.Append(" VALUES ")
                                            sInsertQry.Append(" ('" + oNumbers.CodeCode + "', '" + oNumbers.NameCode + "', '" + sSupPostCd + "', '" + sCustPostCd + "', '")
                                            sInsertQry.Append(" " + sDistanceKm + "', '" + sDurationHrs + "') ")
                                            Try
                                                goParent.goDB.doUpdateQuery(sInsertQry.ToString())
                                                iRecCtr = iRecCtr + 1
                                            Catch ex As Exception
                                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBIR", {com.idh.bridge.Translation.getTranslatedWord("the mileage record"), "@IDH_MILEAGES"})
                                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBIR", {"the mileage record", "@IDH_MILEAGES"})
                                            End Try
                                        Else
                                            'com.idh.bridge.DataHandler.INSTANCE.doError("Error calculating distance between Customer('" + sCustPostCd + "') and Supplier('" + sSupPostCd + "') postcodes!")
                                            com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error calculating distance between Customer('" + sCustPostCd + "') and Supplier('" + sSupPostCd + "') postcodes!", "ERSYCDCS", {sCustPostCd, sSupPostCd})
                                        End If
                                    End If
                                    IDHAddOns.idh.data.Base.doReleaseObject(oValidateRS)
                                Next
                                If iRecCtr > 0 Then
                                    doWarnMess(iRecCtr.ToString() + " Customer and Supplier combinations calculated and inserted in mileage table.")
                                End If
                            Else
                                doWarnMess("No pricing information found to calculate distances. Please check container and material code.")
                            End If
                        Else
                            doWarnMess("Customer Post Code is missing!")
                        End If
                        'goParent.doOpenModalForm("IDHCSRCH", oForm)
                        doChooseSite(oForm, False, "")
                    ElseIf sItemId = "IDH_ALTSIT" Then
                        doChooseAlternateSite(oForm, False)
                    ElseIf sItemId = "IDH_ROTCFL" Then ''## MA STart Issue#407
                        setSharedData(oForm, "IDH_RTCODE", getItemValue(oForm, "IDH_RTCD"))
                        setSharedData(oForm, "SILENT", "SHOWMULTI")

                        goParent.doOpenModalForm("IDHROSRC", oForm)
                        ''## MA End Issue#407
                    ElseIf sItemId = "IDH_SAMPRQ" Then
                        oWOR.doMarkUserUpdate(IDH_JOBSHD._Sample)
                    End If
                Else
                    Dim oItem As SAPbouiCOM.Item = oForm.Items.Item(pVal.ItemUID)
                    If oItem.Type = SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX Then
                        doSwitchCheckBox(oForm, pVal.ItemUID, False)
                    End If
                End If
            End If
            Return False
        End Function

        Public Function doHandleCIPReturn(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Dim oWOR As IDH_JOBSHD = thisWOR(oOForm.SBOParentForm)
            If oWOR.doCheckHasCustomerMarketingDoc() = False Then
                thisWOR(oOForm.SBOParentForm).doGetChargePrices(True)
                doSetUpdate(oOForm.SBOParentForm)
            End If

            'Dim sStatus As String = getFormDFValue(oOForm.SBOParentForm, "U_Status")
            'If Config.INSTANCE.doCheckCanChange(goParent, sStatus) = True Then
            '    thisWOR(oOForm.SBOParentForm).doGetChargePrices(True)
            '    doSetUpdate(oOForm.SBOParentForm)
            'End If
            Return True
        End Function

        Protected Overridable Sub doChooseCustomerRef(ByVal oForm As SAPbouiCOM.Form)
            Dim sCardCode As String = getFormDFValue(oForm, IDH_JOBSHD._CustCd)
            Dim sSite As String = getWFValue(oForm, "STADDR")

            setSharedData(oForm, "TRG", "CUSREF")
            setSharedData(oForm, "VAL1", sCardCode)
            setSharedData(oForm, "VAL2", sSite)
            setSharedData(oForm, "SILENT", "SHOWMULTI")

            goParent.doOpenModalForm("IDHCREF", oForm)
        End Sub

        Public Function doHandleDisposalSIPReturn(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Dim oWOR As IDH_JOBSHD = thisWOR(oOForm.SBOParentForm)
            If oWOR.doCheckHasDisposalMarketingDoc() = False Then
                oWOR.doGetTipCostPrice(True)

                If oWOR.U_Tip = oWOR.U_CarrCd Then
                    oWOR.doGetHaulageCostPrice(True)
                End If
                If oWOR.U_Tip = oWOR.U_ProCd Then
                    oWOR.doGetProducerCostPrice(True)
                End If

                doSetUpdate(oOForm.SBOParentForm)
            End If

            'Dim sStatus As String = getFormDFValue(oOForm.SBOParentForm, "U_Status")
            'If Config.INSTANCE.doCheckCanChange(goParent, sStatus) = True Then
            '    thisWOR(oOForm.SBOParentForm).doGetTipCostPrice(True)
            '    doSetUpdate(oOForm.SBOParentForm)
            'End If
            Return True
        End Function

        Public Function doHandleCarrierSIPReturn(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Dim oWOR As IDH_JOBSHD = thisWOR(oOForm.SBOParentForm)
            If oWOR.doCheckHasHaulierMarketingDoc() = False Then
                oWOR.doGetHaulageCostPrice(True)

                If oWOR.U_CarrCd = oWOR.U_Tip Then
                    oWOR.doGetTipCostPrice(True)
                End If
                If oWOR.U_CarrCd = oWOR.U_ProCd Then
                    oWOR.doGetProducerCostPrice(True)
                End If

                doSetUpdate(oOForm.SBOParentForm)
            End If

            'Dim sStatus As String = getFormDFValue(oOForm.SBOParentForm, "U_Status")
            'If Config.INSTANCE.doCheckCanChange(goParent, sStatus) = True Then
            '    thisWOR(oOForm.SBOParentForm).doGetHaulageCostPrice(True)
            '    doSetUpdate(oOForm.SBOParentForm)
            'End If
            Return True
        End Function

        Public Function doHandleProducerSIPReturn(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Dim oWOR As IDH_JOBSHD = thisWOR(oOForm.SBOParentForm)
            If oWOR.doCheckHasProducerMarketingDoc() = False Then
                oWOR.doGetProducerCostPrice(True)

                If oWOR.U_ProCd = oWOR.U_Tip Then
                    oWOR.doGetTipCostPrice(True)
                End If
                If oWOR.U_ProCd = oWOR.U_CarrCd Then
                    oWOR.doGetHaulageCostPrice(True)
                End If

                doSetUpdate(oOForm.SBOParentForm)
            End If

            'Dim sStatus As String = getFormDFValue(oOForm.SBOParentForm, "U_Status")
            'If Config.INSTANCE.doCheckCanChange(goParent, sStatus) = True Then
            '    thisWOR(oOForm.SBOParentForm).doGetProducerCostPrice(True)
            '    doSetUpdate(oOForm.SBOParentForm)
            'End If
            Return True
        End Function

        Public Function doHandleAllSIPReturn(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            'Dim sStatus As String = getFormDFValue(oOForm.SBOParentForm, "U_Status")
            'If Config.INSTANCE.doCheckCanChange(goParent, sStatus) = True Then

            Dim oWOR As IDH_JOBSHD = thisWOR(oOForm.SBOParentForm)

            If oWOR.doCheckHasDisposalMarketingDoc() = False Then
                oWOR.doGetTipCostPrice(True)
            End If

            If Config.INSTANCE.doGetDoBuyingAndTrade() Then
                If oWOR.doCheckHasProducerMarketingDoc() = False Then
                    oWOR.doGetProducerCostPrice(True)
                End If
            End If

            If oWOR.doCheckHasHaulierMarketingDoc() = False Then
                oWOR.doGetHaulageCostPrice(True)
            End If

            doSetUpdate(oOForm.SBOParentForm)

            'End If
            Return True
        End Function

        Private Sub doRequestCIPSIPUpdate(ByVal oForm As SAPbouiCOM.Form, ByVal sMessId As String, ByVal oPriceType As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes)
            'Skip this if the User can not update the SIP or CIP
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_CIP_SIP_FROM_DO_WO) Then
                If Config.INSTANCE.getParameterAsBool("ITMPUOK", True) Then
                    Dim oUpdateList As Hashtable = getWFValue(oForm, "PRICEUPDATELIST")
                    If oUpdateList Is Nothing Then
                        oUpdateList = New Hashtable()
                        oUpdateList.Add(oPriceType, sMessId)
                        setWFValue(oForm, "PRICEUPDATELIST", oUpdateList)
                    Else
                        If oUpdateList.Contains(oPriceType) = False Then
                            oUpdateList.Add(oPriceType, sMessId)
                        End If
                    End If
                Else
                    doActualCIPSIPUpdateRequest(oForm, sMessId, oPriceType)
                End If
            End If
        End Sub

        Private Function doRequestCIPSIPUpdateOnOK(ByVal oForm As SAPbouiCOM.Form) As Boolean
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_CIP_SIP_FROM_DO_WO) Then
                If Config.INSTANCE.getParameterAsBool("ITMPUOK", True) Then
                    Dim oUpdateList As Hashtable = getWFValue(oForm, "PRICEUPDATELIST")
                    'Dim sMessId As String
                    Dim oaPriceTypeList As New ArrayList()
                    If Not oUpdateList Is Nothing Then
                        For Each oPriceType As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes In oUpdateList.Keys
                            oaPriceTypeList.Add(oPriceType)
                        Next
                        If oaPriceTypeList.Count > 0 Then
                            doCombinedCIPSIPUpdateRequest(oForm, oaPriceTypeList)
                        End If
                        oUpdateList.Clear()
                        setWFValue(oForm, "PRICEUPDATELIST", Nothing)
                        Return True
                    End If
                End If
            End If
            Return False
        End Function

        Private Sub doActualCIPSIPUpdateRequest(ByVal oForm As SAPbouiCOM.Form, ByVal sMessId As String, ByVal oPriceType As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes)
            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)

            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_CIP_SIP_FROM_DO_WO) Then
                If com.idh.bridge.res.Messages.INSTANCE.doResourceMessageYN(sMessId, New String() {WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.getPriceTypeName(oPriceType)}, 1) = 1 Then
                    'Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption = getWFValue(oForm, "PRCUPD")

                    Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption = doCreatePriceUpdateOptionForm(oForm)
                    oOOForm.PriceType = oPriceType

                    Dim oPriceLUP As Object = Nothing
                    If oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TChrg OrElse
                        oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.HChrg OrElse
                        oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TUOM Then
                        oPriceLUP = oWOR.CIP 'getWFValue(oForm, "CIP")
                    ElseIf oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.DispCst OrElse
                        oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TUOM Then
                        oPriceLUP = oWOR.SIPT 'getWFValue(oForm, "SIPT")
                    ElseIf oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.HaulCst Then
                        oPriceLUP = oWOR.SIPH 'getWFValue(oForm, "SIPH")
                    ElseIf oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.ProCst OrElse
                        oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.PRUOM Then
                        oPriceLUP = oWOR.SIPP 'getWFValue(oForm, "SIPP")
                    End If

                    Dim sToDate As String = Config.Parameter("IPDFED")
                    If sToDate Is Nothing OrElse sToDate.Length = 0 Then
                        sToDate = com.idh.utils.Dates.doDateToSBODateStr(DateTime.Now)
                    End If

                    Dim sFromDate As String = Config.Parameter("IPDFST")
                    If sFromDate Is Nothing OrElse sFromDate.Length = 0 OrElse sFromDate.ToUpper().Equals("NOW") Then
                        sFromDate = com.idh.utils.Dates.doDateToSBODateStr(DateTime.Now)
                    ElseIf sFromDate.ToUpper().Equals("DOC") Then
                        sFromDate = getFormDFValue(oForm, "U_RDate")
                    End If

                    'oOOForm.doShowModal(oForm)

                    If oPriceLUP.Count > 0 AndAlso Not oPriceLUP.Prices Is Nothing AndAlso oPriceLUP.Prices.Found Then
                        oOOForm.FromDate = sFromDate 'com.idh.utils.dates.doDateToSBODateStr(DateTime.Now) 'oPriceLUP.U_StDate_AsString
                        oOOForm.ToDate = sToDate 'oPriceLUP.U_EnDate_AsString

                        If oPriceLUP.U_StAddr.Length > 0 Then
                            oOOForm.AllSites = "N"
                        Else
                            oOOForm.AllSites = "Y"
                        End If
                    Else
                        oOOForm.FromDate = com.idh.utils.Dates.doDateToSBODateStr(DateTime.Now)
                        oOOForm.ToDate = sToDate
                        oOOForm.AllSites = "Y"
                    End If

                    oOOForm.doShowModal(oForm)
                End If
            End If
        End Sub

        Private Sub doCombinedCIPSIPUpdateRequest(ByVal oForm As SAPbouiCOM.Form, ByVal oaPriceTypeList As ArrayList)
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_CIP_SIP_FROM_DO_WO) Then
                Dim sMessId As String = "ASKMULTI"
                If com.idh.bridge.res.Messages.INSTANCE.doResourceMessageYN(sMessId, Nothing, 1) = 1 Then
                    'Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption = getWFValue(oForm, "PRCUPD")

                    Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption = doCreatePriceUpdateOptionForm(oForm)
                    oOOForm.PriceTypes = oaPriceTypeList

                    Dim sToDate As String = Config.Parameter("IPDFED")
                    If sToDate Is Nothing OrElse sToDate.Length = 0 Then
                        sToDate = com.idh.utils.Dates.doDateToSBODateStr(DateTime.Now)
                    End If

                    Dim sFromDate As String = Config.Parameter("IPDFST")
                    If sFromDate Is Nothing OrElse sFromDate.Length = 0 OrElse sFromDate.ToUpper().Equals("NOW") Then
                        sFromDate = com.idh.utils.Dates.doDateToSBODateStr(DateTime.Now)
                    ElseIf sFromDate.ToUpper().Equals("DOC") Then
                        sFromDate = getFormDFValue(oForm, "U_RDate")
                    End If

                    oOOForm.FromDate = sFromDate
                    oOOForm.ToDate = sToDate
                    oOOForm.AllSites = "Y"

                    oOOForm.doShowModal(oForm)
                End If
            End If
        End Sub

        Public Function doAddUpdatePrices(ByVal oDialogForm As com.idh.forms.oo.Form) As Boolean
            Dim oPriceForm As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption = oDialogForm
            Dim oParentForm As SAPbouiCOM.Form = oPriceForm.SBOParentForm
            Dim oWOR As IDH_JOBSHD = thisWOR(oParentForm)

            Dim sFromDate As String = oPriceForm.FromDate
            Dim sToDate As String = oPriceForm.ToDate
            Dim sAllSites As String = oPriceForm.AllSites

            Dim dFromDate As Date = com.idh.utils.Dates.doStrToDate(sFromDate)
            Dim dToDate As Date = com.idh.utils.Dates.doStrToDate(sToDate)

            Dim sBranch As String = ""
            Dim sJobType As String = ""
            Dim sZip As String = ""
            Dim bResult As Boolean = True
            Dim bIsCombined As Boolean = False
            Dim bDoUpdate As Boolean = False

            Dim sMessId As String = Nothing
            Dim oPrintType As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes = oPriceForm.PriceType
            Dim oPriceTypes As ArrayList = oPriceForm.PriceTypes
            If oPriceTypes.Count = 0 Then
                oPriceTypes.Add(oPrintType)
            Else
                bIsCombined = True
                sMessId = "ASKMULOV"
            End If

            If bIsCombined = False OrElse oPriceForm.IDH_TUOM OrElse oPriceForm.IDH_TChrg OrElse oPriceForm.IDH_HChrg Then
                If bIsCombined = False Then
                    sMessId = "ASKCIPOV"
                End If

                Dim oCIP As IDH_CSITPR = oWOR.CIP
                Dim dTPrice As Double = oWOR.U_TCharge
                Dim dHPrice As Double = oWOR.U_CusChr
                Dim sUOM As String = oWOR.U_UOM
                Dim iSrc As Integer

                If oCIP.LookedUp = False Then
                    oWOR.doLookupChargePrices()
                End If

                If oCIP.Count > 0 AndAlso Not oCIP.Prices Is Nothing AndAlso oCIP.Prices.Found Then
                    If bDoUpdate OrElse Messages.INSTANCE.doResourceMessageOC(sMessId, New String() {WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.getPriceTypeName(oPrintType)}, 1) = 1 Then
                        oCIP.U_EnDate = dFromDate.AddDays(-1)
                        bDoUpdate = True
                    Else
                        If bIsCombined Then
                            Return bResult
                        Else
                            Return False
                        End If
                    End If
                End If
                iSrc = oCIP.CurrentRowIndex


                Dim sDocDate As String = getFormDFValue(oParentForm, IDH_JOBSHD._RDate)
                Dim dDocDate As Date = com.idh.utils.Dates.doStrToDate(sDocDate)

                If oCIP.U_LinkNr <= 0 Then
                    oCIP.doAddEmptyRow(True, True, False)

                    oCIP.doApplyDefaults()

                    oCIP.Code = ""
                    oCIP.Name = ""
                    If Config.ParameterAsBool("CIPCYCUS", True) Then
                        oCIP.U_CustCd = oWOR.F_CustomerCode
                    Else
                        oCIP.U_CustCd = ""
                    End If
                    If Config.ParameterAsBool("PRCCYCNT", True) Then
                        oCIP.U_ItemCd = oWOR.F_ContainerCode
                        oCIP.U_ItemDs = oWOR.F_ContainerDescription
                    Else
                        oCIP.U_ItemCd = ""
                        oCIP.U_ItemDs = ""
                    End If


                    oCIP.U_WasteCd = oWOR.F_WasteCode
                    oCIP.U_WasteDs = oWOR.F_WasteDescription

                    If sAllSites = "Y" Then
                        oCIP.U_StAddr = ""
                    Else
                        oCIP.U_StAddr = oWOR.F_CustomerAddress
                    End If
                    If Config.ParameterAsBool("PRCCYOTP", False) Then ' Order Type
                        oCIP.U_JobTp = oWOR.U_JobTp 'sJobType
                    Else
                        oCIP.U_JobTp = "" 'sJobType
                    End If
                    If Config.ParameterAsBool("PRCCYWR1", True) Then
                        oCIP.U_WR1ORD = "WO"
                    Else
                        oCIP.U_WR1ORD = ""
                    End If
                    If Config.ParameterAsBool("PRCCYBRN", False) Then
                        oCIP.U_Branch = oWOR.U_Branch 'sBranch
                    Else
                        oCIP.U_Branch = "" 'sBranch
                    End If
                    oCIP.U_ZpCd = "" 'sZip
                    If Config.ParameterAsBool("CIPCYPRD", False) Then
                        oCIP.U_BP2CD = oWOR.U_ProCd
                    Else
                        oCIP.U_BP2CD = ""
                    End If
                Else
                    Dim iLink As Integer = oCIP.U_LinkNr
                    oCIP.U_LinkNr = iLink * -1
                    oCIP.doDuplicate(iSrc)
                    oCIP.Name = oCIP.Code
                    oCIP.U_LinkNr = iLink

                    ''Issue #59 did not apply CR59 based on linked CIP
                    ''chat with HH on 05-01-2016 11:00AM
                    'If Config.ParameterAsBool("CIPCYCUS", True) Then
                    '    oCIP.U_CustCd = oWOR.F_CustomerCode
                    'Else
                    '    oCIP.U_CustCd = ""
                    'End If
                    'If Config.ParameterAsBool("PRCCYCNT", True) Then
                    '    oCIP.U_ItemCd = oWOR.F_ContainerCode
                    '    oCIP.U_ItemDs = oWOR.F_ContainerDescription
                    'Else
                    '    oCIP.U_ItemCd = ""
                    '    oCIP.U_ItemDs = ""
                    'End If
                    'If Config.ParameterAsBool("PRCCYOTP", False) Then ' Order Type
                    '    oCIP.U_JobTp = oWOR.U_JobTp 'sJobType
                    'Else
                    '    oCIP.U_JobTp = "" 'sJobType
                    'End If
                    'If Config.ParameterAsBool("PRCCYWR1", True) Then
                    '    oCIP.U_WR1ORD = "WO"
                    'Else
                    '    oCIP.U_WR1ORD = ""
                    'End If
                    'If Config.ParameterAsBool("PRCCYBRN", False) Then
                    '    oCIP.U_Branch = "" 'sBranch
                    'Else
                    '    oCIP.U_Branch = oWOR.U_Branch 'sBranch
                    'End If
                    'If Config.ParameterAsBool("CIPCYPRD", False) Then
                    '    oCIP.U_BP2CD = oWOR.U_ProCd
                    'End If
                End If

                oCIP.U_StDate = dFromDate
                oCIP.U_EnDate = dToDate

                If bIsCombined = False OrElse oPriceForm.IDH_TChrg Then
                    oCIP.U_TipTon = dTPrice
                End If

                If bIsCombined = False OrElse oPriceForm.IDH_HChrg Then
                    oCIP.U_Haulge = dHPrice
                End If

                If bIsCombined = False OrElse oPriceForm.IDH_TUOM Then
                    oCIP.U_UOM = sUOM
                End If

                If oCIP.doProcessData() Then
                    Dim iPrcSet As Integer = getFormDFValue(oParentForm, "U_MANPRC")
                    oWOR.setUserChanged(Config.MASK_TIPCHRG, False)
                    oWOR.setUserChanged(Config.MASK_HAULCHRG, False)
                End If
            End If

            If bIsCombined = False OrElse oPriceForm.IDH_DispCst OrElse oPriceForm.IDH_PUOM Then
                If bIsCombined = False Then
                    sMessId = "ASKSIPOV"
                End If

                Dim oSIPT As IDH_SUITPR = oWOR.SIPT 'getWFValue(oParentForm, "SIPT")
                Dim dTPrice As Double = -9999
                Dim dHPrice As Double = -9999
                Dim sUOM As String = Nothing

                If bIsCombined = False OrElse oPriceForm.IDH_DispCst Then
                    dTPrice = getFormDFValue(oParentForm, IDH_JOBSHD._TipCost)
                End If

                If bIsCombined = False OrElse oPriceForm.IDH_PUOM Then
                    sUOM = getFormDFValue(oParentForm, IDH_JOBSHD._PUOM)
                End If

                If sUOM Is Nothing OrElse sUOM.Length = 0 Then
                    sUOM = oWOR.U_UOM
                End If

                If oSIPT.LookedUp = False Then
                    oWOR.doLookupChargePrices() 'doLookupTipCostPrices(oParentForm)
                End If

                If oSIPT.Count > 0 AndAlso Not oSIPT.Prices Is Nothing AndAlso oSIPT.Prices.Found Then
                    If bDoUpdate OrElse Messages.INSTANCE.doResourceMessageOC(sMessId, New String() {WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.getPriceTypeName(oPrintType)}, 1) = 1 Then
                        oSIPT.U_EnDate = dFromDate.AddDays(-1)
                        bDoUpdate = True
                    Else
                        If bIsCombined Then
                            Return bResult
                        Else
                            Return False
                        End If
                    End If
                End If
                Dim sSuppCd As String = getFormDFValue(oParentForm, IDH_JOBSHD._Tip)
                If doUpdateSIPDataBuffer(oParentForm, sSuppCd,
                                     sFromDate, sToDate, sAllSites,
                                     oSIPT, dTPrice, dHPrice, sUOM) Then
                    oWOR.setUserChanged(Config.MASK_TIPCST, False)
                End If
            End If

            If bIsCombined = False OrElse oPriceForm.IDH_HaulCst Then
                If bIsCombined = False Then
                    sMessId = "ASKSIPOV"
                End If

                Dim oSIPH As IDH_SUITPR = oWOR.SIPH 'getWFValue(oParentForm, "SIPH")
                Dim dTPrice As Double = -9999
                Dim dHPrice As Double = -9999
                Dim sUOM As String = Nothing

                If bIsCombined = False OrElse oPriceForm.IDH_HaulCst Then
                    dHPrice = getFormDFValue(oParentForm, IDH_JOBSHD._OrdCost)
                End If

                'If bIsCombined = False OrElse oPriceForm.IDH_PUOM Then
                sUOM = getFormDFValue(oParentForm, IDH_JOBSHD._PUOM)
                'End If

                If oSIPH.LookedUp = False Then
                    oWOR.doLookupHaulageCostPrice() 'doLookupHaulageCostPrices(oParentForm)
                End If

                If oSIPH.Count > 0 AndAlso Not oSIPH.Prices Is Nothing AndAlso oSIPH.Prices.Found Then
                    If bDoUpdate OrElse Messages.INSTANCE.doResourceMessageOC(sMessId, New String() {WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.getPriceTypeName(oPrintType)}, 1) = 1 Then
                        oSIPH.U_EnDate = dFromDate.AddDays(-1)
                        bDoUpdate = True
                    Else
                        If bIsCombined Then
                            Return bResult
                        Else
                            Return False
                        End If
                    End If
                End If
                Dim sSuppCd As String = getFormDFValue(oParentForm, IDH_JOBSHD._CarrCd)
                If doUpdateSIPDataBuffer(oParentForm, sSuppCd,
                                     sFromDate, sToDate, sAllSites,
                                     oSIPH, dTPrice, dHPrice, sUOM) Then
                    oWOR.setUserChanged(Config.MASK_HAULCST, False)
                End If
            End If

            If bIsCombined = False OrElse oPriceForm.IDH_ProCst OrElse oPriceForm.IDH_PRUOM Then
                If bIsCombined = False Then
                    sMessId = "ASKSIPOV"
                End If

                Dim oSIPP As IDH_SUITPR = oWOR.SIPP 'getWFValue(oParentForm, "SIPP")
                'Dim dPPrice As Double = getFormDFValue(oParentForm, IDH_JOBSHD._PCost)
                'Dim sUOM As String = getFormDFValue(oParentForm, IDH_JOBSHD._ProUOM)

                Dim dPPrice As Double = -9999
                Dim dHPrice As Double = -9999
                Dim sUOM As String = Nothing

                If bIsCombined = False OrElse oPriceForm.IDH_ProCst Then
                    dPPrice = getFormDFValue(oParentForm, IDH_JOBSHD._PCost)
                End If

                'If bIsCombined = False OrElse oPriceForm.IDH_PRUOM Then
                sUOM = getFormDFValue(oParentForm, IDH_JOBSHD._ProUOM)
                'End If

                If oSIPP.LookedUp = False Then
                    oWOR.doLookupProducerCostPrices() 'doLookupProducerCostPrices(oParentForm)
                End If

                If oSIPP.Count > 0 AndAlso Not oSIPP.Prices Is Nothing AndAlso oSIPP.Prices.Found Then
                    If bDoUpdate OrElse Messages.INSTANCE.doResourceMessageOC(sMessId, New String() {WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.getPriceTypeName(oPrintType)}, 1) = 1 Then
                        oSIPP.U_EnDate = dFromDate.AddDays(-1)
                        bDoUpdate = True
                    Else
                        If bIsCombined Then
                            Return bResult
                        Else
                            Return False
                        End If
                    End If
                End If

                Dim sSuppCd As String = getFormDFValue(oParentForm, IDH_JOBSHD._ProCd)
                If doUpdateSIPDataBuffer(oParentForm, sSuppCd,
                                     sFromDate, sToDate, sAllSites,
                                     oSIPP, dPPrice, dHPrice, sUOM) Then
                    oWOR.setUserChanged(Config.MASK_PRODUCERCST, False)
                End If
            End If
            Return bResult
        End Function

        Private Function doUpdateSIPDataBuffer(ByVal oForm As SAPbouiCOM.Form,
                                         ByVal sSuppCd As String,
                                         ByVal sFromDate As String, ByVal sToDate As String, ByVal sAllSites As String,
                                         ByVal oSIP As IDH_SUITPR, ByVal dTPrice As Double, ByVal dHPrice As Double,
                                         ByVal sUOM As String) As Boolean

            If sUOM Is Nothing AndAlso dTPrice = -9999 AndAlso dHPrice = -9999 Then
                Return True
            End If

            Dim sBranch As String = ""
            Dim sJobType As String = ""
            Dim sZip As String = ""

            If oSIP.Count > 0 AndAlso Not oSIP.Prices Is Nothing AndAlso oSIP.Prices.Found Then
                sBranch = oSIP.U_Branch
                sJobType = oSIP.U_JobTp
                sZip = oSIP.U_ZpCd
                If sUOM Is Nothing OrElse sUOM.Length = 0 Then
                    sUOM = oSIP.U_UOM
                End If
            End If

            Dim dFromDate As Date = com.idh.utils.Dates.doStrToDate(sFromDate)
            Dim dToDate As Date = com.idh.utils.Dates.doStrToDate(sToDate)
            Dim iSrc As Integer = oSIP.CurrentRowIndex

            If oSIP.U_LinkNr <= 0 Then
                oSIP.doAddEmptyRow(True, True, False)

                Dim sCustCd As String = getFormDFValue(oForm, IDH_JOBSHD._CustCd)
                Dim sCardName As String = getFormDFValue(oForm, IDH_JOBSHD._CustNm)
                Dim sItemCode As String = getFormDFValue(oForm, IDH_JOBSHD._ItemCd)
                Dim sItemName As String = getFormDFValue(oForm, IDH_JOBSHD._ItemDsc)
                Dim sItemGrp As String = getFormDFValue(oForm, IDH_JOBSHD._ItmGrp)
                Dim sWastCd As String = getFormDFValue(oForm, IDH_JOBSHD._WasCd)
                Dim sWastName As String = getFormDFValue(oForm, IDH_JOBSHD._WasDsc)
                Dim sDocDate As String = getFormDFValue(oForm, IDH_JOBSHD._RDate)
                'Dim sUOM As String = getFormDFValue(oForm, IDH_JOBSHD._PUOM)
                Dim dDocDate As Date = com.idh.utils.Dates.doStrToDate(sDocDate)

                Dim sAddress As String = getWFValue(oForm, "STADDR")

                sBranch = getFormDFValue(oForm, IDH_JOBSHD._Branch)
                sJobType = getFormDFValue(oForm, IDH_JOBSHD._JobTp)
                'sZip = getWFValue(oForm, "ZIPCODE")

                oSIP.doApplyDefaults()

                oSIP.Code = ""
                oSIP.Name = ""
                If Config.ParameterAsBool("SIPCYPRD", True) Then
                    oSIP.U_CardCd = sSuppCd
                Else
                    oSIP.U_CardCd = ""
                End If
                If Config.ParameterAsBool("SIPCYCUS", True) Then
                    oSIP.U_BP2CD = sCustCd
                Else
                    oSIP.U_BP2CD = ""
                End If
                If Config.ParameterAsBool("PRCCYCNT", True) Then
                    oSIP.U_ItemCd = sItemCode
                    oSIP.U_ItemDs = sItemName
                Else
                    oSIP.U_ItemCd = ""
                    oSIP.U_ItemDs = ""
                End If

                'USE DEFAULTS
                'oSIP.U_FTon = 0
                'oSIP.U_TTon = 500
                'oSIP.U_ChrgCal =
                'oSIP.U_AItmPr =
                'oSIP.U_AItmCd =
                'oSIP.U_AItmDs =
                'oSIP.U_FrmBsWe =
                'oSIP.U_ToBsWe =
                'oSIP.U_BP2CD =
                'oSIP.U_HaulCal =
                'oSIP.U_AAddExp =
                'oSIP.U_ItmPLs = ""
                'oSIP.U_ItemPr = ""
                'oSIP.U_ItemTPr = ""

                oSIP.U_WasteCd = sWastCd
                oSIP.U_WasteDs = sWastName

                If sAllSites = "Y" Then
                    oSIP.U_StAddr = ""
                Else
                    oSIP.U_StAddr = sAddress
                End If
                If Config.ParameterAsBool("PRCCYOTP", False) Then ' Order Type
                    oSIP.U_JobTp = sJobType 'sJobType
                Else
                    oSIP.U_JobTp = "" 'sJobType
                End If
                If Config.ParameterAsBool("PRCCYWR1", True) Then
                    oSIP.U_WR1ORD = "WO"
                Else
                    oSIP.U_WR1ORD = ""
                End If
                If Config.ParameterAsBool("PRCCYBRN", False) Then
                    oSIP.U_Branch = sBranch
                Else
                    oSIP.U_Branch = "" 'sBranch
                End If
                oSIP.U_ZpCd = "" 'sZip
            Else
                Dim iLink As Integer = oSIP.U_LinkNr
                oSIP.U_LinkNr = iLink * -1
                oSIP.doDuplicate(iSrc)
                oSIP.Name = oSIP.Code
                oSIP.U_LinkNr = iLink
            End If
            oSIP.U_StDate = dFromDate
            oSIP.U_EnDate = dToDate

            oSIP.U_UOM = sUOM

            If dTPrice <> -9999 Then
                oSIP.U_TipTon = dTPrice
            End If

            If dHPrice <> -9999 Then
                oSIP.U_Haulge = dHPrice
            End If

            'oSIP.doAddData()
            Return oSIP.doProcessData()
        End Function

        Public Function doCancelAddUpdatePrices(ByVal oDeialogForm As com.idh.forms.oo.Form) As Boolean
            Dim oPForm As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption = oDeialogForm
            Dim oPriceType As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes = oPForm.PriceType

            If oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TChrg OrElse
                oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.HChrg OrElse
                oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TUOM Then
                thisWOR(oPForm.SBOParentForm).doGetTipCostPrice(True)
            ElseIf oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.DispCst OrElse
                oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TUOM Then
                thisWOR(oPForm.SBOParentForm).doGetTipCostPrice(True)
            ElseIf oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.HaulCst Then
                thisWOR(oPForm.SBOParentForm).doGetHaulageCostPrice(True)
            ElseIf oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.ProCst OrElse
                oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.PRUOM Then
                thisWOR(oPForm.SBOParentForm).doGetProducerCostPrice(True)
            End If
            Return True
            Return True
        End Function

        'Check to see if the date for the PBI Detail are the same as the entered Date
        Private Function doCheckPBIDate(ByVal sRowNr As String, ByVal sDateCol As String, ByVal sEnteredDate As String) As String
            Dim sResult As String = Nothing
            'Select TOP 1 U_IDHPRDSD, U_IDHPRDED FROM [@IDH_PBIDTL] WHERE U_IDHWOR = '44557'
            Dim sQry As String = "Select TOP 1 " & sDateCol & " FROM [@IDH_PBIDTL] WHERE U_IDHWOR = '" & sRowNr & "'"

            Dim oRecordset As SAPbobsCOM.Recordset = Nothing
            oRecordset = goParent.goDB.doSelectQuery(sQry)

            If Not oRecordset Is Nothing AndAlso oRecordset.RecordCount > 0 Then
                Dim dPBIDate As DateTime = oRecordset.Fields.Item(0).Value
                If com.idh.utils.Dates.isValidDate(dPBIDate) Then
                    Dim sPBIDate As String = com.idh.utils.Dates.doDateToSBODateStr(dPBIDate)
                    If sPBIDate.Equals(sEnteredDate) = False Then
                        sResult = sPBIDate
                    End If
                End If
            End If
            IDHAddOns.idh.data.Base.doReleaseObject(oRecordset)
            Return sResult
        End Function

        Protected Overridable Sub doChooseSite(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, sItemID As String)
            setSharedData(oForm, "TRG", "ST")
            If Config.INSTANCE.getParameterAsBool("WOBPFLT", True) = False Then
                'setSharedData(oForm, "IDH_BPCOD", "")
                'setSharedData(oForm, "IDH_NAME", "")
                setSharedData(oForm, "IDH_TYPE", "")
                setSharedData(oForm, "IDH_GROUP", "")
                setSharedData(oForm, "IDH_BRANCH", "")
            Else
                setSharedData(oForm, "IDH_TYPE", Config.ParameterWithDefault("DSPBPTYP", "F-S"))
                setSharedData(oForm, "IDH_GROUP", Config.Parameter("BGSDispo"))

            End If
            setSharedData(oForm, "IDH_BPCOD", getItemValue(oForm, "IDH_TIPPIN"))
            setSharedData(oForm, "IDH_NAME", getItemValue(oForm, "IDH_TIPNM"))
            setSharedData(oForm, "CALLEDITEM", sItemID)
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If

            If Config.INSTANCE.getParameterAsBool("WODSPSP", False) Then
                Dim sWasteCode As String = getFormDFValue(oForm, IDH_JOBSHD._WasCd)
                Dim sZipCode As String = getWFValue(oForm, "ZIPCODE")
                Dim sContainerCd As String = getFormDFValue(oForm, IDH_JOBSHD._ItemCd)
                'setSharedData(oForm, "IDH_ZIPCOD", sZipCode)
                setSharedData(oForm, "IDH_CUSTZP", sZipCode)
                setSharedData(oForm, "IDH_WSTCD", sWasteCode)

                '20140728 - Fix for OVDs: Un-remarked below line, when "Get Sites" button clicked for the Customer Search
                'User DONT want to filter ONLY on Container Code. Want to see both with this Container Cd and the empty ones
                'e.g.: --AND (s.U_ItemCd = '20TE_TIPPER' OR IsNull(s.U_ItemCd, '') = '') 
                'To acheieve have added above clause in the sRequired clause in BPSearch for DOSIP 
                'setSharedData(oForm, "IDH_CONTCD", sContainerCd)

                setSharedData(oForm, "DOSIP", "TRUE")
            Else
                setSharedData(oForm, "DOSIP", "FALSE")
            End If

            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub

        Protected Overridable Sub doChooseAlternateSite(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean)
            setSharedData(oForm, "TRG", "ST")
            If Config.INSTANCE.getParameterAsBool("WOBPFLT", True) = False Then
                'setSharedData(oForm, "IDH_BPCOD", "")
                'setSharedData(oForm, "IDH_NAME", "")
                setSharedData(oForm, "IDH_TYPE", "")
                setSharedData(oForm, "IDH_GROUP", "")
                setSharedData(oForm, "IDH_BRANCH", "")
            Else
                setSharedData(oForm, "IDH_TYPE", Config.ParameterWithDefault("DSPBPTYP", "F-S"))
                setSharedData(oForm, "IDH_GROUP", Config.Parameter("BGSDispo"))

            End If
            setSharedData(oForm, "IDH_BPCOD", getItemValue(oForm, "IDH_TIPPIN"))
            setSharedData(oForm, "IDH_NAME", getItemValue(oForm, "IDH_TIPNM"))

            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If

            If Config.INSTANCE.getParameterAsBool("WODSPSP", False) Then
                Dim sWasteCode As String = getFormDFValue(oForm, IDH_JOBSHD._WasCd)
                Dim sZipCode As String = getWFValue(oForm, "ZIPCODE")

                'setSharedData(oForm, "IDH_ZIPCOD", sZipCode)
                setSharedData(oForm, "IDH_CUSTZP", sZipCode)
                setSharedData(oForm, "IDH_WSTCD", sWasteCode)
                'setSharedData(oForm, "DOSIP", "TRUE")
            Else
                'setSharedData(oForm, "DOSIP", "FALSE")
            End If

            goParent.doOpenModalForm("BPALTSCH", oForm)
        End Sub

        Protected Sub doSetChoosenSite(ByVal oForm As SAPbouiCOM.Form)
            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)

            Dim sCardCode As String = getSharedData(oForm, "CARDCODE")
            Dim sCardName As String = getSharedData(oForm, "CARDNAME")

            oWOR.BlockChangeNotif = True
            Try
                oWOR.U_Tip = sCardCode
                oWOR.U_TipNm = sCardName

                If getSharedData(oForm, "DOSIP") = "TRUE" Then
                    setWFValue(oForm, "DOSIP", "Y")
                    setEnableItem(oForm, False, "IDH_SITAL")

                    oWOR.U_SAddress = getSharedData(oForm, "SUPADDR")
                    oWOR.U_LckPrc = "Y"
                    oWOR.BlockChangeNotif = True
                    oWOR.U_WasCd = getSharedData(oForm, "WASTCD")

                    oWOR.U_UOM = getSharedData(oForm, "UOM")
                    oWOR.setUserChanged(Config.MASK_CUSTUOM, True)

                    Dim dWeightFactor As Double = Config.INSTANCE.doGetBaseWeightFactors(oWOR.U_UOM)
                    If dWeightFactor <= 0 Then
                        oWOR.U_UseWgt = "2"
                    Else
                        oWOR.U_UseWgt = "1"
                    End If

                    oWOR.U_PUOM = getSharedData(oForm, "UOM")
                    oWOR.setUserChanged(Config.MASK_TIPUOM, True)
                    oWOR.BlockChangeNotif = False

                    'Set values to '1' for DOSIP when Get Site is choosen 
                    If Config.INSTANCE.doGetBaseWeightFactors(oWOR.U_PUOM) < 0 Then
                        'Cost side 
                        oWOR.U_TAUOMQt = 1
                        oWOR.U_TipWgt = 1
                        oWOR.U_OrdWgt = 1
                        oWOR.U_CarWgt = 1
                    End If
                    If Config.INSTANCE.doGetBaseWeightFactors(oWOR.U_UOM) < 0 Then
                        'Charge 
                        oWOR.U_AUOMQt = 1
                        oWOR.U_CstWgt = 1
                        oWOR.U_CusQty = 1
                        oWOR.U_HlSQty = 1
                    End If

                    Dim dDisposalCost As Double = getSharedData(oForm, "TIP")
                    oWOR.U_TipCost = dDisposalCost
                    oWOR.setUserChanged(Config.MASK_TIPCST, True)
                    oWOR.doCalculateTipCostTotals("S")

                    oWOR.U_TCharge = dDisposalCost
                    oWOR.setUserChanged(Config.MASK_TIPCHRG, True)
                    oWOR.doCalculateTipChargeTotals("S")

                    Dim dDistance As Double = getSharedData(oForm, "DISTANCE")
                    Dim dHaulageCst As Double
                    Dim oHaulCst As IDH_HAULCST = New IDH_HAULCST()
                    dHaulageCst = oHaulCst.getChrgPriceForDistance(dDistance)
                    If dHaulageCst < 0 Then
                        dHaulageCst = getSharedData(oForm, "HAULAGECHRG")
                    End If
                    oWOR.U_OrdCost = dHaulageCst
                    oWOR.setUserChanged(Config.MASK_HAULCST, True)
                    oWOR.doCalculateHaulageCostTotals(com.idh.dbObjects.User.Shared.OrderRow.TOTAL_CALCULATE)

                    Dim dHualChrg As Double = getSharedData(oForm, "HAULAGECST")
                    oWOR.U_CusChr = dHualChrg
                    oWOR.setUserChanged(Config.MASK_HAULCHRG, True)
                    oWOR.doCalculateHaulageChargeTotals(com.idh.dbObjects.User.Shared.OrderRow.TOTAL_CALCULATE)

                    'When DOSIP from Get Sites button: Set 'Apply Haulage By Weight' checkbox ticked 
                    If oWOR.U_UOM = Config.ParameterWithDefault("GEDEFUOM", "t") Then
                        oWOR.U_HaulAC = "Y"
                    End If

                    'setFormDFValue(oForm, IDH_JOBENTR._LckPrc, "Y")
                    'setWFValue(oForm, "DOSIP", getSharedData(oForm, "DOSIP"))
                    'setWFValue(oForm, "WASTCD", getSharedData(oForm, "WASTCD"))
                    'setWFValue(oForm, "TIPCST", 0)
                    'setWFValue(oForm, "PRODCST", 0)
                    'setWFValue(oForm, "HAULCST", getSharedData(oForm, "HAULAGE"))

                    'setWFValue(oForm, "TIPCHRG", 0)
                    'setWFValue(oForm, "HAULCHRG", 0)
                Else
                    setEnableItem(oForm, True, "IDH_SITAL")
                    oWOR.U_SAddress = ""
                    oWOR.U_SAddrsLN = ""
                    'doGetTipCostPrices(oForm)
                    oWOR.doGetTipCostPrice(True)
                End If
            Catch e As Exception
                Throw (e)
            End Try
            oWOR.BlockChangeNotif = False
        End Sub

        Protected Sub doSetAlternateChoosenSite(ByVal oForm As SAPbouiCOM.Form)
            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)

            Dim sCardCode As String = getSharedData(oForm, "CARDCODE")
            Dim sCardName As String = getSharedData(oForm, "CARDNAME")
            Dim sAddress As String = getSharedData(oForm, "ADDRESS")

            oWOR.BlockChangeNotif = True
            Try
                oWOR.U_Tip = sCardCode
                oWOR.U_TipNm = sCardName
                oWOR.U_SAddress = sAddress

                'If getSharedData(oForm, "DOSIP") = "TRUE" Then
                '    setEnableItem(oForm, False, "IDH_SITAL")
                '    oWOR.U_SAddress = getSharedData(oForm, "SUPADDR")

                '    oWOR.U_LckPrc = "Y"

                '    oWOR.BlockChangeNotif = True
                '    oWOR.U_WasCd = getSharedData(oForm, "WASTCD")

                '    oWOR.U_UOM = getSharedData(oForm, "UOM")
                '    oWOR.setUserChanged(Config.MASK_CUSTUOM, True)

                '    Dim dWeightFactor As Double = Config.INSTANCE.doGetBaseWeightFactors(oWOR.U_UOM)
                '    If dWeightFactor <= 0 Then
                '        oWOR.U_UseWgt = "2"
                '    Else
                '        oWOR.U_UseWgt = "1"
                '    End If

                '    oWOR.U_PUOM = getSharedData(oForm, "UOM")
                '    oWOR.setUserChanged(Config.MASK_TIPUOM, True)
                '    oWOR.BlockChangeNotif = False

                '    Dim dDisposalCost As Double = getSharedData(oForm, "TIP")
                '    oWOR.U_TipCost = dDisposalCost
                '    oWOR.setUserChanged(Config.MASK_TIPCST, True)
                '    oWOR.doCalculateTipCostTotals("S")

                '    oWOR.U_TCharge = dDisposalCost
                '    oWOR.setUserChanged(Config.MASK_TIPCHRG, True)
                '    oWOR.doCalculateTipChargeTotals("S")

                '    Dim dDistance As Double = getSharedData(oForm, "DISTANCE")
                '    Dim dHaulageCst As Double
                '    Dim oHaulCst As IDH_HAULCST = New IDH_HAULCST()
                '    dHaulageCst = oHaulCst.getChrgPriceForDistance(dDistance)
                '    If dHaulageCst < 0 Then
                '        dHaulageCst = getSharedData(oForm, "HAULAGECHRG")
                '    End If
                '    oWOR.U_OrdCost = dHaulageCst
                '    oWOR.setUserChanged(Config.MASK_HAULCST, True)
                '    oWOR.doCalculateHaulageCostTotals()

                '    Dim dHualChrg As Double = getSharedData(oForm, "HAULAGECST")
                '    oWOR.U_CusChr = dHualChrg
                '    oWOR.setUserChanged(Config.MASK_HAULCHRG, True)
                '    oWOR.doCalculateHaulageChargeTotals()

                '    'setFormDFValue(oForm, IDH_JOBENTR._LckPrc, "Y")
                '    'setWFValue(oForm, "DOSIP", getSharedData(oForm, "DOSIP"))
                '    'setWFValue(oForm, "WASTCD", getSharedData(oForm, "WASTCD"))
                '    'setWFValue(oForm, "TIPCST", 0)
                '    'setWFValue(oForm, "PRODCST", 0)
                '    'setWFValue(oForm, "HAULCST", getSharedData(oForm, "HAULAGE"))

                '    'setWFValue(oForm, "TIPCHRG", 0)
                '    'setWFValue(oForm, "HAULCHRG", 0)
                'Else
                '    setEnableItem(oForm, True, "IDH_SITAL")
                '    oWOR.U_SAddress = ""
                '    'doGetTipCostPrices(oForm)
                '    oWOR.doGetTipCostPrice(True)
                'End If
            Catch e As Exception
                Throw (e)
            End Try
            oWOR.BlockChangeNotif = False
        End Sub

        '## Start 01-10-2013
        Private Sub doChooseWasteCode(ByVal oForm As SAPbouiCOM.Form, sItemID As String)
            ''## MA Start 18-11-2014 
            'sItemId = "IDH_WASCL1" OrElse sItemId = "IDH_WASMAT" 
            If wasSetWithCode(oForm, sItemID) Then
                Return
            End If
            Dim sWItem As String = ""
            Dim sWItemName As String = ""
            Dim sGrp As String = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup()
            If sItemID <> "IDH_WASCF" Then 'if called from Waste code/desc text box
                With getFormMainDataSource(oForm)
                    sWItem = .GetValue("U_WasCd", .Offset).Trim()
                    sWItemName = .GetValue("U_WasDsc", .Offset).Trim()
                    If sItemID = "IDH_WASCL1" AndAlso sWItemName.Trim.Length > 0 Then 'if nothing in code then neglect CFL
                        '.SetValue("U_WasDsc", .Offset, "")
                        sWItemName = ""
                        doFillAlternateItemDescription(oForm, "", True)
                    ElseIf sItemID = "IDH_WASMAT" AndAlso sWItem.Trim.Length > 0 Then 'if nothing in code then neglect CFL
                        '.SetValue("U_WasCd", .Offset, "")
                        sWItem = ""
                        doFillAlternateItemDescription(oForm, "", True)
                    End If

                End With
                If sWItem.Trim.Length > 0 OrElse sWItemName.Trim.Length > 0 Then
                    setSharedData(oForm, "ACTIVEITM", sItemID)
                    doClearSharedData(oForm)
                    setSharedData(oForm, "TP", "WAST")
                    setSharedData(oForm, "IDH_ITMCOD", sWItem)
                    setSharedData(oForm, "IDH_GRPCOD", sGrp)
                    setSharedData(oForm, "IDH_CRDCD", getFormDFValue(oForm, "U_CustCd"))
                    setSharedData(oForm, "IDH_ITMNAM", sWItemName)
                    setSharedData(oForm, "SILENT", "SHOWMULTI")
                    If Config.INSTANCE.getParameterAsBool("USAREL", False) = True Then
                        'USA Release
                        setSharedData(oForm, "IDH_INVENT", "")
                        setSharedData(oForm, "IDH_BPADDR", getUFValue(oForm, "IDH_ADDR").ToString()) 'Filter for Address
                        goParent.doOpenModalForm("IDHWPWISRC", oForm)
                    Else
                        'Non-USA Release
                        setSharedData(oForm, "IDH_INVENT", "") '"N")
                        setSharedData(oForm, "IDH_JOBTP", getFormDFValue(oForm, IDH_JOBSHD._JobTp))
                        If getSharedData(oForm, "IDH_ITMSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_ITMSRCHOPEND") = "" Then
                            goParent.doOpenModalForm("IDHWISRC", oForm)
                        End If
                    End If
                Else
                    With getFormMainDataSource(oForm)
                        .SetValue("U_WasDsc", .Offset, "")
                        .SetValue("U_WasCd", .Offset, "")
                    End With
                End If

            Else 'if called by CFL button
                With getFormMainDataSource(oForm)
                    sWItem = .GetValue("U_WasCd", .Offset).Trim()
                    sWItemName = .GetValue("U_WasDsc", .Offset).Trim()
                End With
                setSharedData(oForm, "ACTIVEITM", sItemID)
                doClearSharedData(oForm)
                setSharedData(oForm, "TP", "WAST")
                setSharedData(oForm, "IDH_ITMCOD", sWItem)
                setSharedData(oForm, "IDH_GRPCOD", sGrp)
                setSharedData(oForm, "IDH_CRDCD", getFormDFValue(oForm, "U_CustCd"))
                setSharedData(oForm, "IDH_ITMNAM", sWItemName)
                setSharedData(oForm, "SILENT", "")
                If Config.INSTANCE.getParameterAsBool("USAREL", False) = True Then
                    'USA Release
                    setSharedData(oForm, "IDH_INVENT", "")
                    setSharedData(oForm, "IDH_BPADDR", getUFValue(oForm, "IDH_ADDR").ToString()) 'Filter for Address
                    goParent.doOpenModalForm("IDHWPWISRC", oForm)
                Else
                    'Non-USA Release
                    setSharedData(oForm, "IDH_INVENT", "") '"N")
                    setSharedData(oForm, "IDH_JOBTP", getFormDFValue(oForm, IDH_JOBSHD._JobTp))
                    If getSharedData(oForm, "IDH_ITMSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_ITMSRCHOPEND") = "" Then
                        goParent.doOpenModalForm("IDHWISRC", oForm)
                    End If
                End If
            End If
            ''## MA End 18-11-2014 
            ' '''
            'Dim sItem As String
            'Dim sGrp As String = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup()
            'With getFormMainDataSource(oForm)
            '    sItem = .GetValue("U_WasCd", .Offset).Trim()
            'End With

            'doClearSharedData(oForm)
            'setSharedData(oForm, "TP", "WAST")
            'setSharedData(oForm, "IDH_ITMCOD", sItem)
            'setSharedData(oForm, "IDH_GRPCOD", sGrp)
            'setSharedData(oForm, "IDH_CRDCD", getFormDFValue(oForm, "U_CustCd"))
            'setSharedData(oForm, "IDH_ITMNAM", "")
            ''setSharedData(oForm, "SILENT", "SHOWMULTI")
            'If Config.INSTANCE.getParameterAsBool("USAREL", False) = True Then
            '    'USA Release
            '    setSharedData(oForm, "IDH_INVENT", "")
            '    setSharedData(oForm, "IDH_BPADDR", getUFValue(oForm, "IDH_ADDR").ToString()) 'Filter for Address
            '    goParent.doOpenModalForm("IDHWPWISRC", oForm)
            'Else
            '    'Non-USA Release
            '    setSharedData(oForm, "IDH_INVENT", "") '"N")
            '    setSharedData(oForm, "IDH_JOBTP", getFormDFValue(oForm, IDH_JOBSHD._JobTp))
            '    If getSharedData(oForm, "IDH_ITMSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_ITMSRCHOPEND") = "" Then
            '        goParent.doOpenModalForm("IDHWISRC", oForm)
            '    End If
            'End If
        End Sub
        '## End
        Protected Overridable Sub doChooseSupplier(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, sItemID As String)
            setSharedData(oForm, "TRG", "SP")
            setSharedData(oForm, "IDH_TYPE", "S")
            setSharedData(oForm, "IDH_BPCOD", getItemValue(oForm, "IDH_SUPCOD"))
            setSharedData(oForm, "IDH_NAME", getItemValue(oForm, "IDH_SUPNAM"))
            setSharedData(oForm, "CALLEDITEM", sItemID)
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            'goParent.doOpenModalForm("IDHCSRCH", oForm)
            If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
                goParent.doOpenModalForm("IDHCSRCH", oForm) 'Actual BP Search Form 
            Else
                If (getSharedData(oForm, "IDH_BPSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_BPSRCHOPEND").ToString.Trim = "") Then
                    goParent.doOpenModalForm("IDHNCSRCH", oForm) 'NEW BP Search Form 
                End If
            End If
        End Sub

        Private Sub doSkipChanged(ByVal oForm As SAPbouiCOM.Form)
            Dim sLicSupCd As String = getFormDFValue(oForm, "U_SLicSp")
            Dim sLicSupNM As String = getFormDFValue(oForm, "U_SLicNm")
            Dim sLicSupNo As String = getFormDFValue(oForm, "U_SLicNr")
            Dim sLicSupEx As String = getFormDFValue(oForm, "U_SLicExp")

            Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            Dim sSkipCd As String = com.idh.bridge.lookups.Config.INSTANCE.doGetSkipLicCode()
            If Not oGrid Is Nothing Then
                Dim sItemNm As String = "Skip Licence - Ref - " & sLicSupNo & " Exp " + sLicSupEx

                oGrid.doUpdateExpense(sSkipCd, Nothing, Nothing, sLicSupCd, sLicSupNM, Nothing, sItemNm)
            End If

        End Sub

        Private Sub doChooseComplianceScheme(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean)
            setSharedData(oForm, "TRG", "CS")
            If Config.INSTANCE.getParameterAsBool("WOBPFLT", True) = False Then
                'setSharedData(oForm, "IDH_BPCOD", "")
                setSharedData(oForm, "IDH_NAME", "")
                setSharedData(oForm, "IDH_TYPE", "")
                setSharedData(oForm, "IDH_GROUP", "")
                setSharedData(oForm, "IDH_BRANCH", "")
            Else
                'setSharedData(oForm, "IDH_NAME", sBPName)
                'setSharedData(oForm, "IDH_TYPE", "F-S")
                'setSharedData(oForm, "IDH_TYPE", sType)
                'setSharedData(oForm, "IDH_GROUP", "")
            End If
            setSharedData(oForm, "IDH_CS", "Y")
            setSharedData(oForm, "IDH_BPCOD", "")
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
                goParent.doOpenModalForm("IDHCSRCH", oForm) 'Actual BP Search Form 
            Else
                If (getSharedData(oForm, "IDH_BPSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_BPSRCHOPEND").ToString.Trim = "") Then
                    goParent.doOpenModalForm("IDHNCSRCH", oForm) 'NEW BP Search Form 
                End If
            End If
        End Sub
        Private Sub doChooseContainer(oForm As SAPbouiCOM.Form, sItemID As String)
            ''## MA Start 18-11-2014 
            If wasSetWithCode(oForm, sItemID) Then
                Return
            End If
            Dim sContCd As String = ""
            Dim sGrp As String
            Dim sContName As String = String.Empty
            If sItemID <> "IDH_ITCF" Then 'if called from container code/desc text box
                setSharedData(oForm, "CALLEDITEM", sItemID)
                With getFormMainDataSource(oForm)
                    sContCd = .GetValue("U_ItemCd", .Offset).Trim()
                    sGrp = .GetValue("U_ItmGrp", .Offset).Trim()
                    sContName = .GetValue("U_ItemDsc", .Offset).Trim()
                    If sItemID = "IDH_ITMCOD" AndAlso sContName.Trim.Length > 0 Then 'if nothing in code then neglect CFL
                        .SetValue("U_ItemDsc", .Offset, "")
                        sContName = ""
                    ElseIf sItemID = "IDH_DESC" AndAlso sContCd.Trim.Length > 0 Then 'if nothing in code then neglect CFL
                        .SetValue("U_ItemCd", .Offset, "")
                        sContCd = ""
                    End If
                End With
                If sContCd.Trim.Length > 0 OrElse sContName.Trim.Length > 0 Then
                    setSharedData(oForm, "ACTIVEITM", sItemID)
                    setSharedData(oForm, "TRG", "PROD")
                    setSharedData(oForm, "IDH_ITMCOD", sContCd)
                    setSharedData(oForm, "IDH_GRPCOD", sGrp)
                    setSharedData(oForm, "IDH_ITMNAM", sContName)
                    setSharedData(oForm, "SILENT", "SHOWMULTI")
                    goParent.doOpenModalForm("IDHISRC", oForm)
                End If

            Else 'if called by CFL button
                With getFormMainDataSource(oForm)
                    sGrp = .GetValue("U_ItmGrp", .Offset).Trim()
                    sContCd = .GetValue("U_ItemCd", .Offset).Trim()
                    sContName = .GetValue("U_ItemDsc", .Offset).Trim()
                End With
                setSharedData(oForm, "ACTIVEITM", sItemID)
                setSharedData(oForm, "TRG", "PROD")
                setSharedData(oForm, "IDH_ITMCOD", sContCd)
                setSharedData(oForm, "IDH_ITMNAM", sContName)
                setSharedData(oForm, "IDH_GRPCOD", sGrp)
                goParent.doOpenModalForm("IDHISRC", oForm)
            End If
            ''## MA End 18-11-2014 
        End Sub
        Protected Overridable Sub doChooseCarrier(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, ByVal sItemID As String)
            setSharedData(oForm, "TRG", "WC")
            If Config.INSTANCE.getParameterAsBool("WOBPFLT", True) = False Then
                'setSharedData(oForm, "IDH_BPCOD", "")
                'setSharedData(oForm, "IDH_NAME", "")
                setSharedData(oForm, "IDH_TYPE", "")
                setSharedData(oForm, "IDH_GROUP", "")
                setSharedData(oForm, "IDH_BRANCH", "")
            Else
                'setSharedData(oForm, "IDH_TYPE", "F-S")
                setSharedData(oForm, "IDH_TYPE", "S")
                setSharedData(oForm, "IDH_GROUP", Config.Parameter("BGSWCarr"))
            End If
            setSharedData(oForm, "IDH_BPCOD", getItemValue(oForm, "IDH_CARRIE"))
            setSharedData(oForm, "IDH_NAME", getItemValue(oForm, "IDH_CARNAM"))
            setSharedData(oForm, "CALLEDITEM", sItemID)
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
                goParent.doOpenModalForm("IDHCSRCH", oForm) 'Actual BP Search Form 
            Else
                If (getSharedData(oForm, "IDH_BPSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_BPSRCHOPEND").ToString.Trim = "") Then
                    goParent.doOpenModalForm("IDHNCSRCH", oForm) 'NEW BP Search Form 
                End If
            End If
        End Sub

        Protected Overridable Sub doChooseLic(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, sItemID As String)
            setSharedData(oForm, "TRG", "LC")
            If Config.INSTANCE.getParameterAsBool("WOBPFLT", True) = False Then
                'setSharedData(oForm, "IDH_BPCOD", "")
                'setSharedData(oForm, "IDH_NAME", "")
                setSharedData(oForm, "IDH_TYPE", "")
                setSharedData(oForm, "IDH_GROUP", "")
                setSharedData(oForm, "IDH_BRANCH", "")
            Else
                setSharedData(oForm, "IDH_TYPE", "F-S")
                setSharedData(oForm, "IDH_GROUP", Config.Parameter("BGSLicen"))
            End If
            setSharedData(oForm, "IDH_BPCOD", getItemValue(oForm, "IDH_LICSUP"))
            setSharedData(oForm, "IDH_NAME", getItemValue(oForm, "IDH_SLNM"))
            setSharedData(oForm, "CALLEDITEM", sItemID)
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
                goParent.doOpenModalForm("IDHCSRCH", oForm) 'Actual BP Search Form 
            Else
                If (getSharedData(oForm, "IDH_BPSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_BPSRCHOPEND").ToString.Trim = "") Then
                    goParent.doOpenModalForm("IDHNCSRCH", oForm) 'NEW BP Search Form 
                End If
            End If
        End Sub

        Protected Overridable Sub doDocReport(ByVal oForm As SAPbouiCOM.Form)
            Dim sRowCode As String = getFormDFValue(oForm, "Code")

            'OnTime Ticket 25926 - DOC Report crahses application
            'if report starts with http display PDF report in web browser control
            Dim sReportFile As String

            sReportFile = Config.Parameter("WDOCDOC")
            If sReportFile.StartsWith("http://") Then
                'Start ISB Code
                Dim oParams As New Hashtable
                oParams.Add("RowNum", sRowCode)

                'implementation of doCallReportDefaults
                doCallHTTPRpts(goParent, oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType, Config.INSTANCE.doGetBranch(goParent.gsUserName))
                'End ISB Code
            Else
                Dim oReport As DocCon = New DocCon(Me, oForm, "WO", gsType, True)
                Dim bNewConsignment As Boolean = oReport.doDocConReport(sRowCode)
                doAfterConsignmentReport(oForm, oReport)
                oReport = Nothing
            End If
        End Sub

        Protected Overridable Sub doDocumentsReport(ByVal oForm As SAPbouiCOM.Form)
            Dim sRCodes As New ArrayList
            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                sRCodes.Add(getFormDFValue(oForm, "Code").ToString())
            Else
                'com.idh.bridge.DataHandler.INSTANCE.doError("Save the Waste Order Row and try again.")
                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Save the Waste Order Row and try again.", "ERSYSWOR", {Nothing})
                Exit Sub
            End If
            'Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            'Dim oRows As ArrayList = oGridN.doPrepareListFromSelection("r.Code")
            'If oRows.Count() = 0 Then
            '    com.idh.bridge.DataHandler.INSTANCE.doError("Select a waste order row.")
            '    Exit Sub
            'End If



            Dim sReportFile As String

            sReportFile = Config.Parameter("WDOCUM")



            'Dim oList As New ArrayList
            'oList = oGridN.doPrepareListFromSelection("r.Code")
            'oParams.Add("PARAM2", oRows)
            If (Config.INSTANCE.useNewReportViewer()) Then
                Dim oParams As New Hashtable
                oParams.Add("RowNum", sRCodes)
                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
            Else
                Dim oParams As New Hashtable
                oParams.Add("sRowNums", sRCodes)
                IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
            End If
        End Sub

        Public Sub doAfterConsignmentReport(ByVal oForm As SAPbouiCOM.Form, ByVal oReport As DocCon)
            'Prompt for the JJK Number if it is an USA version
            Dim bISUSARelease As Boolean = Config.INSTANCE.getParameterAsBool("USAREL", False)
            If bISUSARelease = True Then
                If Not oReport.ConNumbers Is Nothing AndAlso oReport.ConNumbers.Count > 0 Then
                    For Each sConNum As String In oReport.ConNumbers
                        Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.JJKNumber = New WR1_FR2Forms.idh.forms.fr2.JJKNumber(Nothing, oForm, Nothing)
                        oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doReturnOkFromJJK)
                        oOOForm.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doReturnCancelFromJJK)
                        'oOOForm.RowCodes = oReport.ConRowsToUpdate
                        oOOForm.TempConNum = sConNum 'oReport.LastConsignmentNumber
                        oOOForm.RowTable = "@IDH_JOBSHD"
                        oOOForm.doShowModal()
                    Next
                End If
            Else
                If oReport.HasNewConsignmentNote Then
                    setFormDFValue(oForm, "U_ConNum", oReport.LastConsignmentNumber)
                End If
            End If
        End Sub

        Public Function doReturnOkFromJJK(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Dim oJJKForm As WR1_FR2Forms.idh.forms.fr2.JJKNumber = oOForm
            setFormDFValue(oOForm.SBOParentForm, "U_ConNum", oJJKForm.JJKNumber)
            Return True
        End Function

        Public Function doReturnCancelFromJJK(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Dim oJJKForm As WR1_FR2Forms.idh.forms.fr2.JJKNumber = oOForm
            setFormDFValue(oOForm.SBOParentForm, "U_ConNum", oJJKForm.TempConNum)
            Return True
        End Function


        '        Protected Overridable Sub doDocReport(ByVal oForm As SAPbouiCOM.Form)
        '            Dim sCode As String = getFormDFValue( oForm, "U_JobNr")
        '            Dim sRowID As String = getFormDFValue( oForm, "Code")
        '
        '            If sRowID.Trim().Length > 0 Then
        '                Dim sReportFile As String
        '
        '                Dim sCardCode As String = getFormDFValue( oForm, "U_CustCd")
        '                Dim sWasteCode As String = getFormDFValue( oForm, "U_WasCd")
        '
        '                Dim oData As ArrayList = Config.INSTANCE.doGetDOCReportLevel1a(goParent, "WO", sCardCode, sWasteCode)
        '                sReportFile = oData.Item(0)
        '
        '        		If oData.Item(1) = True Then
        '        			Dim sConNo As String = getFormDFValue( oForm, "U_ConNum")
        '					If sConNo Is Nothing OrElse sConNo.Length = 0 Then
        '						Dim iConNum As Integer = goParent.goDB.doNextNumber("CONNUM")
        '						Dim sPremCode As String = ""
        '                 		Dim sAddr As String = getWFValue(oForm, "STADDR")
        '						Dim oAddData As ArrayList = SRCH.idh.forms.search.AddressSearch.doGetAddress(goParent, sCardCode, "S", sAddr)
        '                        If Not oAddData Is Nothing AndAlso oAddData.Count > 0 Then
        '                        	sPremCode = oAddData.Item(18)
        '                        End If
        '						If sPremCode Is Nothing OrElse sPremCode.Length = 0 Then
        '                			sConNo = "EXEXXX" & "/" & iConNum
        '                		Else
        '                			sConNo = sPremCode & "/" & iConNum
        '                		End If
        '
        '						goParent.goDB.doUpdateQuery("UPDATE [@IDH_JOBSHD] Set U_ConNum = '" & sConNo & "' WHERE Code = '" & sRowID & "'")
        '						setFormDFValue( oForm, "U_ConNum", sConNo)
        '						'sRowID = sConNo
        '					End If
        '					sRowID = sConNo
        '				End If
        '
        '                Dim oParams As New Hashtable
        '                'oParams.Add("OrdNum", sCode)
        '                oParams.Add("RowNum", sRowID)
        '                idh.report.Base.doCallReportDefaults( oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType, Config.INSTANCE.doGetBranch(goParent))
        '            End If
        '        End Sub

        Protected Sub doSwitchCheckBox(ByVal oForm As SAPbouiCOM.Form, ByVal sID As String, ByVal bDoSetValue As Boolean)
            If sID.Equals("IDH_DOPO") Then
                If bDoSetValue = True Then
                    setUFValue(oForm, "IDH_DOPO", "Y")
                End If
                'setUFValue(oForm, sID, "Y")
                setFormDFValue(oForm, "U_CarrReb", "N")
            ElseIf sID.Equals("IDH_CARRRE") Then
                setUFValue(oForm, "IDH_DOPO", "N")
            ElseIf sID.Equals("IDH_DOORD") OrElse
                           sID.Equals("IDH_DOARI") OrElse
                           sID.Equals("IDH_FOC") OrElse
                           sID.Equals("IDH") OrElse
                           sID.Equals("IDH_CUSTRE") OrElse
                           sID.Equals("IDH_DOJRN") Then

                If sID.Equals("IDH_FOC") = False Then
                    setUFValue(oForm, "IDH_FOC", "N")
                End If

                If sID.Equals("IDH_DOORD") = False Then
                    setUFValue(oForm, "IDH_DOORD", "N")
                End If

                If sID.Equals("IDH_DOARI") = False Then
                    setUFValue(oForm, "IDH_DOARI", "N")
                End If

                If sID.Equals("IDH_CUSTRE") = False Then
                    setFormDFValue(oForm, "U_CustReb", "N")
                End If

                If sID.Equals("IDH_DOJRN") = False Then
                    setUFValue(oForm, "IDH_DOJRN", "N")
                End If

                If bDoSetValue = True AndAlso sID.Equals("IDH") = False Then
                    Try
                        setUFValue(oForm, sID, "Y")
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error switching the checkboxes.")
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSCHK", {Nothing})
                    End Try
                End If
            End If
        End Sub

        '** Clear All
        Private Sub doClearAll(ByVal oForm As SAPbouiCOM.Form)
            doClearAll(oForm, False, True)
        End Sub

        '** Clear All
        Private Sub doClearAll(ByVal oForm As SAPbouiCOM.Form, ByVal bDoFindFields As Boolean, ByVal bDoLoadRest As Boolean)
            doClearAllFormValues(oForm)

            setFormDFValue(oForm, IDH_JOBSHD._User, goParent.gsUserName)
            setFormDFValue(oForm, IDH_JOBSHD._Branch, Config.INSTANCE.doGetCurrentBranch())
            setFormDFValue(oForm, IDH_JOBSHD._ItemCd, Config.INSTANCE.getDefaultWOContainer())
            setFormDFValue(oForm, IDH_JOBSHD._UOM, Config.INSTANCE.getDefaultUOM()) ''Kg
            setFormDFValue(oForm, IDH_JOBSHD._PUOM, Config.INSTANCE.getDefaultUOM()) ''Kg
            setFormDFValue(oForm, IDH_JOBSHD._ProUOM, Config.INSTANCE.getDefaultUOM())

            setWFValue(oForm, "DEFWEI", 0)

            doResetCoverage(oForm)
            doRefreshForm(oForm)

            If bDoLoadRest Then
                doLoadDataF(oForm, bDoFindFields)
            End If
        End Sub

        '** Create a new Entry
        Public Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form)
            With getFormMainDataSource(oForm)
                .InsertRecord(.Offset)

                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(IDH_JOBSHD.AUTONUMPREFIX, "JOBSCHE")
                .SetValue("Code", .Offset, oNumbers.CodeCode)
                .SetValue("Name", .Offset, oNumbers.NameCode)
                .SetValue("U_JobNr", .Offset, "")
                .SetValue("U_JobTp", .Offset, "")
                .SetValue("U_RDate", .Offset, "")

                .SetValue("U_RTime", .Offset, "")
                .SetValue("U_RTimeT", .Offset, "")

                .SetValue("U_ASDate", .Offset, "")
                .SetValue("U_ASTime", .Offset, "")
                .SetValue("U_AEDate", .Offset, "")
                .SetValue("U_AETime", .Offset, "")
                .SetValue("U_VehTyp", .Offset, "")
                .SetValue("U_User", .Offset, goParent.gsUserName)
                '.SetValue("U_CustCs", .Offset, "")
                .SetValue("U_Branch", .Offset, Config.INSTANCE.doGetCurrentBranch())
                .SetValue("U_Lorry", .Offset, "")
                .SetValue("U_Driver", .Offset, "")
                .SetValue("U_CongCh", .Offset, "")
                .SetValue("U_ItemCd", .Offset, "")
                .SetValue("U_ItemDsc", .Offset, "")
                .SetValue("U_ItmGrp", .Offset, "")
                .SetValue("U_DocNum", .Offset, "")
                .SetValue("U_Tip", .Offset, 0)
                .SetValue(IDH_JOBSHD._SAddress, .Offset, "")
                .SetValue("U_SupRef", .Offset, "")

                '.SetValue("U_ProRef", .Offset, "")
                '.SetValue("U_SiteRef", .Offset, "")

                .SetValue(IDH_JOBSHD._TRdWgt, .Offset, 0)
                .SetValue("U_TipWgt", .Offset, 0)
                .SetValue("U_TipCost", .Offset, 0)
                .SetValue("U_TipTot", .Offset, 0)
                .SetValue("U_TAddCost", .Offset, 0)
                .SetValue("U_TAddChrg", .Offset, 0)
                ''MA Start 07-04-2014
                .SetValue("U_TAddChVat", .Offset, 0)
                .SetValue("U_TAddCsVat", .Offset, 0)
                .SetValue("U_AddCharge", .Offset, 0)
                .SetValue("U_AddCost", .Offset, 0)
                ''MA End 07-04-2014
                .SetValue("U_CstWgt", .Offset, 0)
                .SetValue("U_RdWgt", .Offset, 0)
                .SetValue("U_UseWgt", .Offset, "1")
                .SetValue("U_AUOMQt", .Offset, Config.INSTANCE.getDefaultAUOM())
                .SetValue("U_TCharge", .Offset, 0)
                .SetValue("U_TCTotal", .Offset, 0)
                .SetValue("U_Price", .Offset, 0)
                .SetValue("U_Discnt", .Offset, 0)
                .SetValue("U_DisAmt", .Offset, 0)
                .SetValue("U_TaxAmt", .Offset, 0)
                .SetValue("U_SLicNr", .Offset, "")
                .SetValue("U_SLicExp", .Offset, "")
                .SetValue("U_SLicCh", .Offset, 0)
                .SetValue("U_SLicSp", .Offset, "")
                .SetValue("U_Serial", .Offset, "")
                .SetValue("U_Status", .Offset, com.idh.bridge.lookups.FixedValues.getStatusOpen())
                .SetValue("U_PStat", .Offset, com.idh.bridge.lookups.FixedValues.getStatusOpen())
                .SetValue("U_Comment", .Offset, "")
                .SetValue("U_IntComm", .Offset, "")
                .SetValue("U_AUOM", .Offset, Config.INSTANCE.getDefaultAUOM())
                .SetValue("U_UOM", .Offset, Config.INSTANCE.getDefaultUOM()) ''"Kg")
                .SetValue("U_PUOM", .Offset, Config.INSTANCE.getDefaultUOM()) ''"Kg")

                'Fix to avoid puting 'false in U_WasDesc when MDDEWA is not set

                Dim sDefWC As String = Config.ParameterWithDefault("MDDEWA", "")
                If sDefWC.Length > 0 Then
                    Dim sDefWCDesc As String = Config.INSTANCE.doGetItemDescription(sDefWC)
                    .SetValue("U_WasCd", .Offset, sDefWC)
                    .SetValue("U_WasDsc", .Offset, sDefWCDesc)
                Else
                    .SetValue("U_WasCd", .Offset, "")
                    .SetValue("U_WasDsc", .Offset, "")
                End If

                .SetValue("U_CusQty", .Offset, 1)
                .SetValue("U_HlSQty", .Offset, 1)
                .SetValue("U_CusChr", .Offset, 0)

                .SetValue("U_TAUOMQt", .Offset, 0)
                .SetValue("U_PAUOMQt", .Offset, 0)
                .SetValue("U_PRdWgt", .Offset, 0)
                .SetValue("U_ProWgt", .Offset, 0)
                .SetValue("U_ProUOM", .Offset, Config.INSTANCE.getDefaultUOM())
                .SetValue("U_PCost", .Offset, 0)
                .SetValue("U_PCTotal", .Offset, 0)

                'KA -- START -- 20121022
                'CBI Fields
                .SetValue("U_CBICont", .Offset, 0)
                .SetValue("U_CBIManQty", .Offset, 0)
                .SetValue("U_CBIManUOM", .Offset, 0)
                'KA -- END -- 20121022

                'KA -- START -- 20121025
                'New Comments field with extended length
                .SetValue("U_ExtComm1", .Offset, "")
                .SetValue("U_ExtComm2", .Offset, "")
                'KA -- END -- 20121025
                .SetValue("U_AltWasDsc", .Offset, "")
                .SetValue("U_OTComments", .Offset, "")

                'Multi Currency Pricing 
                .SetValue(IDH_JOBSHD._DispCgCc, .Offset, "")
                .SetValue(IDH_JOBSHD._CarrCgCc, .Offset, "")
                .SetValue(IDH_JOBSHD._PurcCoCc, .Offset, "")
                .SetValue(IDH_JOBSHD._DispCoCc, .Offset, "")
                .SetValue(IDH_JOBSHD._CarrCoCc, .Offset, "")
                .SetValue(IDH_JOBSHD._LiscCoCc, .Offset, "")

                .SetValue(IDH_JOBSHD._RtCdCode, .Offset, "")
                .SetValue(IDH_JOBSHD._RouteCd, .Offset, "")
                .SetValue(IDH_JOBSHD._PCostVtRt, .Offset, "0")
                .SetValue(IDH_JOBSHD._PCostVtGrp, .Offset, "")
                .SetValue(IDH_JOBSHD._Sample, .Offset, "")
                .SetValue(IDH_JOBSHD._SampleRef, .Offset, "")
                .SetValue(IDH_JOBSHD._SampStatus, .Offset, "")


            End With
            setWFValue(oForm, "DEFWEI", 0)
            doResetCoverage(oForm)

            doRefreshForm(oForm)
        End Sub

        '** The Menu Event handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                If pVal.MenuUID = Config.NAV_FIRST OrElse
                   pVal.MenuUID = Config.NAV_LAST OrElse
                   pVal.MenuUID = Config.NAV_NEXT OrElse
                   pVal.MenuUID = Config.NAV_PREV Then
                    oForm.Freeze(True)
                    Try
                        doClearFormUFValues(oForm)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error handeling the Menu event.")
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHMEN", {Nothing})
                    Finally
                        oForm.Freeze(False)
                    End Try
                End If
            ElseIf pVal.BeforeAction = False Then
                If pVal.MenuUID = Config.NAV_ADD Then
                    oForm.Freeze(True)
                    Try
                        doCreateNewEntry(oForm)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing menu event - " & oForm.UniqueID & "." & pVal.MenuUID)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try
                ElseIf pVal.MenuUID = Config.NAV_FIND OrElse
                       pVal.MenuUID = Config.NAV_FIRST OrElse
                       pVal.MenuUID = Config.NAV_LAST OrElse
                       pVal.MenuUID = Config.NAV_NEXT OrElse
                       pVal.MenuUID = Config.NAV_PREV Then
                    oForm.Freeze(True)
                    Try
                        If pVal.MenuUID = Config.NAV_FIND Then
                            doClearAll(oForm, True, False)
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                        Else
                            doSetValuesFromHeader(oForm)
                            'Dim sCode As String = getFormDFValue(oForm, IDH_JOBSHD._Code)
                            'doLoadHeaderFromRowCode(oForm, sCode)
                        End If
                        doLoadDataF(oForm, True)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error handeling the Menu event.")
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHMEN", {Nothing})
                    Finally
                        oForm.Freeze(False)
                    End Try
                End If
            End If
            Return True
        End Function

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            '            If getReturnButton(oForm).Equals("1") = False Then
            '                If goParent.doCheckModal(oForm.UniqueID) = True Then
            '                    Dim sMode As String = ""
            '                    Dim oData As ArrayList
            '                    If ghOldDialogParams.Contains(oForm.UniqueID) Then
            '                        oData = ghOldDialogParams.Item(oForm.UniqueID)
            '                        If Not (oData Is Nothing) AndAlso oData.Count > 2 Then
            '                            sMode = oData.Item(0)
            '                            If sMode = "IDHGRID" Then
            '                                Dim iRow As Integer = oData.Item(1)
            '                                Dim oGridN As FilterGrid = oData.Item(2)
            '                                Dim sKey As String = oGridN.doRowKeyLookup(iRow)
            '
            '                                Dim oAdde As WR1_Data.idh.data.AdditionalExpenses = getWFValue(oForm,  "JobAdditionalHandler")
            '                                If oGridN.checkRowNew(iRow) Then
            '                                	''Remove all the enries for the above row
            '     	  							If Not oAdde Is Nothing Then
            '										oAdde.doRemoveRowEntries(sKey)
            '            						End If
            '									oGridN.doRemoveRow(iRow, False)
            '								Else
            ''                               	 	If Not oAdde Is Nothing Then
            ''                                	End If
            '                                End If
            '
            ''                                If sKey.Length > 1 AndAlso sKey.Chars(0) = "-" Then
            ''                                	oGridN.doRemoveRow(iRow, False)
            ''                                End If
            ''                                'If sKey.Equals(oGridN.TMPROWID) Then
            ''                                '    oGridN.doRemoveRow(iRow, False)
            ''                                'End If
            '                            End If
            '                        End If
            '                    End If
            '                End If
            '            End If

            If getReturnButton(oForm).Equals("1") = False Then
                '            	If goParent.doCheckModal(oForm.UniqueID) = True Then
                '					doRemoveThisEntry(oForm)
                '				End If
            End If

            MyBase.doCloseForm(oForm, BubbleEvent)
            ghLastRow.Remove(oForm.UniqueID)

            doClearParentSharedData(oForm)

            'Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption = getWFValue(oForm, "PRCUPD")
            'oOOForm.KeepInMemory = False
            'oOOForm.doCloseForm(True)
        End Sub

        Public Overrides Sub doClose()
        End Sub

        Private Sub doSetGridValues(ByVal oForm As SAPbouiCOM.Form, ByRef oData As ArrayList)
            Dim iRow As Integer = oData.Item(1)
            Dim oGridN As FilterGrid = oData.Item(2)
            Dim oExtra As Hashtable = oData.Item(3)

            oGridN.setCurrentDataRowIndex(iRow)
            oGridN.getSBOForm.Freeze(True)
            Dim sField As String = ""
            Dim oVal As Object = ""
            Try
                With getFormMainDataSource(oForm)
                    Dim sSchedule As String = getFormDFValue(oForm, "U_Covera")
                    oExtra.Add("U_2SCHDT", sSchedule)
                    oExtra.Add("AUTOCOLLECT", getWFValue(oForm, "AUTOCOLLECT"))
                    oExtra.Add("REMPT", getUFValue(oForm, "IDH_REMPT"))
                    oExtra.Add("REMPQ", getUFValue(oForm, "IDH_REMPQ"))

                    Dim ii As Integer
                    For ii = 0 To .Fields.Count - 1
                        sField = .Fields.Item(ii).Name
                        oVal = .GetValue(sField, .Offset)
                        If (sField = "U_RDate" OrElse
                            sField = "U_ASDate" OrElse
                            sField = "U_AEDate" OrElse
                            sField = "U_SLicExp" OrElse
                            sField = "U_JobRmD" OrElse
                            sField = "U_CCExp" OrElse
                            sField = "U_JbToBeDoneDt" OrElse
                            sField = "U_BDate") Then  'AndAlso oVal.Trim().Length > 0 Then
                            If Not oVal Is Nothing AndAlso oVal.Length = 0 Then
                                oVal = Nothing
                            End If
                            oVal = com.idh.utils.Dates.doStrToDate(oVal)
                        ElseIf sField = "U_RTime" OrElse
                            sField = "U_RTimeT" OrElse
                            sField = "U_ASTime" OrElse
                            sField = "U_AETime" OrElse
                            sField = "U_JobRmT" OrElse
                            sField = "U_JbToBeDoneTm" OrElse
                            sField = "U_BTime" Then
                            'oVal = goParent.doTimeStrToInt(oVal)
                            'oVal = goParent.doStrToTimeStr(oVal)
                        ElseIf sField = "U_Tip" OrElse sField = "U_SAddress" OrElse sField = "U_SAddrsLN" Then
                            oGridN.doSetFieldValue(sField, oVal)
                        End If
                        oGridN.doSetFieldValue(sField, oVal)
                    Next

                    Dim sState As String = doCalcState(
                        .GetValue("U_RDate", .Offset),
                        .GetValue("U_ASDate", .Offset),
                        .GetValue("U_AEDate", .Offset))
                    oGridN.doSetFieldValue("Progress", sState)
                End With

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Setting Field: " & sField & " - Val: " & oVal)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSVS", {sField, oVal})
            Finally
                oGridN.getSBOForm.Freeze(False)
            End Try
        End Sub

        'MODAL DIALOG
        ''*** Get the selected rows
        Private Sub doSetRowData(ByVal oForm As SAPbouiCOM.Form)
            Dim sMode As String = ""
            Dim oData As ArrayList = Nothing
            If ghOldDialogParams.Contains(oForm.UniqueID) Then
                oData = ghOldDialogParams.Item(oForm.UniqueID)
                If Not (oData Is Nothing) AndAlso oData.Count > 2 Then
                    sMode = oData.Item(0)
                End If
            End If

            If sMode = "IDHGRID" Then
                doSetGridValues(oForm, oData)

                Dim oAddGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                oAddGrid.doProcessData()
            End If
            doReturnFromModalBuffered(oForm, oData)
        End Sub
        '## Start 01-10-2013
        Protected Overrides Sub doHandleModalCanceled(ByVal oParentForm As SAPbouiCOM.Form, ByVal sModalFormType As String)
            MyBase.doHandleModalCanceled(oParentForm, sModalFormType)
            If sModalFormType.Equals("IDHWPWISRC") OrElse sModalFormType.Equals("IDHWISRC") Then 'Waste Item
                ''## MA Start 27-11-2014
                Dim sitemid = getSharedData(oParentForm, "ACTIVEITM")
                If sitemid Is Nothing Then
                    sitemid = "IDH_WASCL1"
                End If
                If sitemid = "IDH_WASCF" Then
                    doSetFocus(oParentForm, "IDH_WASCL1")
                    setItemValue(oParentForm, "IDH_WASCL1", getItemValue(oParentForm, "IDH_WASCL1"))
                Else
                    doSetFocus(oParentForm, "IDH_WASCL1")
                    setItemValue(oParentForm, "IDH_WASCL1", "")
                    setItemValue(oParentForm, "IDH_WASMAT", "")
                    doFillAlternateItemDescription(oParentForm, "", True)
                End If
                'If Not oParentForm.ActiveItem.Equals("IDH_WASCL1") Then
                '    doSetFocus(oParentForm, "IDH_WASCL1")
                '    setItemValue(oParentForm, "IDH_WASMAT", "")
                '    doFillAlternateItemDescription(oParentForm, "", True)
                'End If
                ''## MA End 27-11-2014
                ''## MA Start 26-08-2014 Issue#407
            ElseIf sModalFormType.Equals("IDHROSRC") Then
                If Not oParentForm.ActiveItem.Equals("IDH_RTCD") Then
                    doSetFocus(oParentForm, "IDH_RTCD")
                    setItemValue(oParentForm, "IDH_RTCD", "")
                End If
                ''## MA End 26-08-2014 Issue#407
                ''## MA Start 18-11-2014 
            ElseIf sModalFormType.Equals("IDHISRC") Then
                Dim sTarget = getSharedData(oParentForm, "TRG")
                If sTarget IsNot Nothing AndAlso sTarget = "PROD" Then
                    Dim sitemid = getSharedData(oParentForm, "ACTIVEITM")
                    If sitemid Is Nothing Then
                        sitemid = "IDH_ITMCOD"
                    End If
                    If sitemid = "IDH_ITCF" Then
                        doSetFocus(oParentForm, "IDH_ITMCOD")
                        setItemValue(oParentForm, "IDH_ITMCOD", getItemValue(oParentForm, "IDH_ITMCOD"))
                    Else 'If Not oParentForm.ActiveItem.Equals(sitemid) Then
                        doSetFocus(oParentForm, "IDH_ITMCOD")
                        setItemValue(oParentForm, "IDH_ITMCOD", "")
                        setItemValue(oParentForm, "IDH_DESC", "")
                    End If
                End If
                ''## MA End 18-11-2014 

                ''## MA Start 25-11-2014 
            ElseIf sModalFormType.Equals("IDHCREF") Then
                Dim cCustRef As String = getFormDFValue(oParentForm, IDH_JOBSHD._CustRef)
                If cCustRef IsNot Nothing AndAlso cCustRef.IndexOf("*") > -1 Then
                    setFormDFValue(oParentForm, IDH_JOBSHD._CustRef, "")
                End If
                ''## MA End 25-11-2014 
            ElseIf sModalFormType = "IDHCSRCH" OrElse sModalFormType = "IDHNCSRCH" Then
                Dim sTarget As String = getSharedData(oParentForm, "TRG")
                If sTarget = "ST" Then
                    doSetChoosenSite(oParentForm)
                ElseIf sTarget = "WC" Then
                    Dim oWOR As IDH_JOBSHD = thisWOR(oParentForm)
                    Dim sOldLock As String = oWOR.U_LckPrc
                    Dim sDoSip As String = getWFValue(oParentForm, "DOSIP")
                    If sDoSip = "Y" Then
                        oWOR.U_LckPrc = "N"
                    End If

                    oWOR.U_CarrCd = ""
                    oWOR.U_CarrNm = ""

                    oWOR.doGetHaulageCostPrice(True)
                    oWOR.U_LckPrc = sOldLock
                End If
            ElseIf sModalFormType = "IDHLOSCH" Then
                setFormDFValue(oParentForm, IDH_JOBSHD._Lorry, "")
                doSetFocus(oParentForm, "IDH_LORRY")
            End If
            If getSharedData(oParentForm, "CALLEDITEM") IsNot Nothing AndAlso getSharedData(oParentForm, "CALLEDITEM").Trim <> "" Then
                doSetFocus(oParentForm, getSharedData(oParentForm, "CALLEDITEM"))

            End If
        End Sub
        '## End


        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)
            Try
                'OnTime [#Ico000????] USA
                'Adding IDHWPWISRC search filter below
                'to handle results if it is a USA release
                If sModalFormType = "IDHCREF" Then
                    Dim sVal As String = getSharedData(oForm, "CUSTREF")
                    setFormDFValue(oForm, IDH_JOBSHD._CustRef, sVal, False, True)
                ElseIf sModalFormType = "IDHWISRC" Or sModalFormType = "IDHWPWISRC" Then
                    oForm.Freeze(True)
                    Dim sWastCd As String = getSharedData(oForm, "ITEMCODE")

                    'setFormDFValue(oForm, "U_WasCd", sWastCd, False, True)
                    'setFormDFValue(oForm, "U_WasDsc", getSharedData(oForm, "ITEMNAME"))
                    oWOR.U_WasCd = sWastCd
                    oWOR.U_WasDsc = getSharedData(oForm, "ITEMNAME")

                    Dim sWOHDispSite As String = getFormDFValue(oForm, "U_Tip")
                    If sWOHDispSite Is Nothing OrElse sWOHDispSite.Length = 0 Then
                        'KA -- START -- 20121025
                        'Do set Disposal Site to the selected waste profile's disposal site in OITM
                        'Start
                        Dim sWPItemDisposalSite As String = getSharedData(oForm, "WPDSPFAC")
                        Dim sWPsDispSiteBPCD As String = getSharedData(oForm, "WPDFCD")
                        Dim sWPsDispSiteBPNM As String = getSharedData(oForm, "WPDSPFAC")
                        Dim sWPsDispSiteAdd As String = getSharedData(oForm, "WPDFADD")

                        'WP Item Search has got no Disposal Site BP Code, Name and also Site Address
                        'The fields are now added on the WP Item Search screen
                        'These fields are comming from the WP Item
                        'Therefore we dont need to reteriece the Site Address seperately

                        'New rule from CBI
                        'If disposal site from WP is blank DONOT overwrite the existing
                        If Not sWPsDispSiteBPCD Is Nothing AndAlso sWPsDispSiteBPCD.Length > 0 Then
                            setFormDFValue(oForm, "U_Tip", sWPsDispSiteBPCD, False, True)
                            setFormDFValue(oForm, "U_TipNm", sWPsDispSiteBPNM, False, True)
                            If sWPsDispSiteAdd IsNot Nothing AndAlso sWPsDispSiteAdd.Length > 0 Then
                                setFormDFValue(oForm, "U_Saddress", sWPsDispSiteAdd, False, True)
                            Else
                                Dim oRecordset As SAPbobsCOM.Recordset = Nothing
                                Dim sQry As String = "select Address, CardCode from CRD1 where AdresType = 'S' AND CardCode ='" + sWPsDispSiteBPCD + "'"
                                oRecordset = PARENT.goDB.doSelectQuery(sQry)
                                If Not oRecordset Is Nothing AndAlso oRecordset.RecordCount > 0 Then
                                    sWPsDispSiteAdd = oRecordset.Fields.Item("Address").Value.Trim()

                                    setFormDFValue(oForm, "U_Saddress", sWPsDispSiteAdd, False, True)
                                    'updating all prices as the Tipping Site was changed
                                    'oWOR.doGetAllUOM()
                                    oWOR.doGetAllPrices()
                                End If
                                IDHAddOns.idh.data.Base.doReleaseObject(oRecordset)
                            End If
                            Dim sCardCode As String = getFormDFValue(oForm, "U_TIP")
                            Dim sAddress As String = getFormDFValue(oForm, "U_SAddress")
                            Dim sAddressLineNum As String = ""
                            If sCardCode.Trim <> "" AndAlso sAddress.Trim <> "" Then
                                sAddressLineNum = com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPShipToAddress(sCardCode, sAddress, "LineNum")
                                setFormDFValue(oForm, IDH_JOBSHD._SAddrsLN, sAddressLineNum)
                            Else
                                setFormDFValue(oForm, IDH_JOBSHD._SAddrsLN, sAddressLineNum)
                            End If
                        End If
                        'End
                        'KA -- END -- 20121025
                    End If

                    Dim sCWBB As String = getSharedData(oForm, "CWBB")
                    Dim bCanPurchase As Boolean = False
                    If (sCWBB = "Y" OrElse sCWBB = "y") Then
                        bCanPurchase = True
                    End If

                    doSwitchProducerFields(oForm)

                    Dim sUseDefWeight As String
                    sUseDefWeight = getSharedData(oForm, "USEDEFWEI")
                    If Not sUseDefWeight Is Nothing AndAlso sUseDefWeight.ToUpper.Equals("TRUE") Then
                        Dim dDefWeight As Double
                        Dim dWeight As Double = 0
                        dDefWeight = getSharedData(oForm, "DEFWEI")
                        setWFValue(oForm, "DEFWEI", dDefWeight)
                    End If

                    Dim bDoCRebate As Boolean = Config.ParameterAsBool("WOCREB", False)
                    Dim sRebateItem As String = getSharedData(oForm, "CREB")

                    'Carrier Rebate
                    Dim sTPCN As String = getFormDFValue(oForm, "U_TPCN")
                    If Not sTPCN Is Nothing AndAlso sTPCN.Length > 0 Then
                        oForm.Items.Item("IDH_CARRRE").Enabled = False
                    Else
                        If bDoCRebate = True Then
                            If String.IsNullOrEmpty(oWOR.U_TrnCode) Then
                                oForm.Items.Item("IDH_CARRRE").Enabled = True
                            End If
                            setFormDFValue(oForm, "U_CarrReb", sRebateItem)
                        Else
                            oForm.Items.Item("IDH_CARRRE").Enabled = False
                            setFormDFValue(oForm, "U_CarrReb", "N")
                        End If
                    End If

                    'Customer Rebate
                    Dim sTCCN As String = getFormDFValue(oForm, "U_TCCN")
                    If Not sTCCN Is Nothing AndAlso sTCCN.Length > 0 Then
                        oForm.Items.Item("IDH_CUSTRE").Enabled = False
                    Else
                        If bDoCRebate = True Then
                            If String.IsNullOrEmpty(oWOR.U_TrnCode) Then
                                oForm.Items.Item("IDH_CUSTRE").Enabled = True
                            End If
                            setFormDFValue(oForm, "U_CustReb", sRebateItem)
                        Else
                            oForm.Items.Item("IDH_CUSTRE").Enabled = False
                            setFormDFValue(oForm, "U_CustReb", "N")
                        End If
                    End If

                    'Check for HazMaterial
                    If com.idh.bridge.lookups.Config.INSTANCE.isHazardousItem(sWastCd) Then
                        If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("WOACONS", True) Then
                            doAddConsignmentNoteFee(oForm)
                        End If
                    Else
                        If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("WOACONS", True) Then
                            doRemoveConsignmentNoteFee(oForm)
                        End If
                    End If

                    ''UPDATE THE ADDITIONAL GRID FROM HERE FOR NOW - IN FUTURE THE DBOBJECT WILL BE USED
                    'doRefreshAdditionalPrices(oForm)

                    'getAutoAdditionalCodes(oForm)

                    'oWOR.BlockChangeNotif = True
                    Try
                        'Add this to force the Price
                        If sModalFormType = "IDHWPWISRC" Then
                            Dim sS_UOM As String = getSharedData(oForm, "UOM")
                            Dim sP_UOM As String = getSharedData(oForm, "PUOM")

                            If sS_UOM.Length = 0 Then
                                oWOR.doGetCustomerUOM(True)
                            Else
                                oWOR.U_UOM = sS_UOM
                                oWOR.doMarkUserUpdate(IDH_JOBSHD._UOM)
                            End If

                            If sP_UOM.Length = 0 Then
                                oWOR.doGetDisposalSiteUOM(True)
                                oWOR.doGetProducerUOM(True)
                            Else
                                oWOR.U_PUOM = sP_UOM
                                oWOR.doMarkUserUpdate(IDH_JOBSHD._PUOM)

                                oWOR.U_ProUOM = sP_UOM
                                oWOR.doMarkUserUpdate(IDH_JOBSHD._ProUOM)
                            End If
                        Else
                            oWOR.doGetAllUOM(True)
                        End If
                    Catch e As Exception
                    End Try
                    'oWOR.BlockChangeNotif = False

                    'UPDATE THE ADDITIONAL GRID FROM HERE FOR NOW - IN FUTURE THE DBOBJECT WILL BE USED
                    doRefreshAdditionalPrices(oForm)

                    doAddAutoAdditionalCodes(oForm)

                    oWOR.doGetChargePrices(True)
                    oWOR.doGetCostPrices(True, True, True, True)

                    'oWOR.doGetAllPrices()

                    If oWOR.U_WasCd.Length > 0 Then
                        doFillAlternateItemDescription(oForm)
                    End If
                ElseIf sModalFormType = "IDHJTSRC" Then
                    setFormDFValue(oForm, "U_JobTp", getSharedData(oForm, "ORDTYPE"), False, True)

                    doSetUpdate(oForm)

                    'UPDATE THE ADDITIONAL GRID FROM HERE FOR NOW - IN FUTURE THE DBOBJECT WILL BE USED
                    doRefreshAdditionalPrices(oForm)

                    doAddAutoAdditionalCodes(oForm)

                    oWOR.doGetAllPrices()
                ElseIf sModalFormType = "IDHLOSCH" Then
                    doHandleVehicleSearchResult(oForm, sLastButton)
                ElseIf sModalFormType = "IDHISRC" Then
                    If getSharedData(oForm, "TRG") = "PROD" Then
                        oWOR.U_ItemCd = getSharedData(oForm, "ITEMCODE")
                        oWOR.U_ItemDsc = getSharedData(oForm, "ITEMNAME")

                        'oWOR.doGetAllUOM(True)

                        'UPDATE THE ADDITIONAL GRID FROM HERE FOR NOW - IN FUTURE THE DBOBJECT WILL BE USED
                        doRefreshAdditionalPrices(oForm)

                        doAddAutoAdditionalCodes(oForm)

                        ''oWOR.doGetAllPrices()
                        'KA -- START -- 20121025
                        doSetUpdate(oForm)
                        'KA -- END -- 20121025
                    ElseIf getSharedData(oForm, "TRG") = "ADD" Then
                        Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                        If Not oGrid Is Nothing Then
                            Dim iRow As Integer = oGrid.getCurrentDataRowIndex()

                            Dim sCardCode As String = getFormDFValue(oForm, "U_CustCd")
                            Dim sItemCode As String = getFormDFValue(oForm, "U_ItemCd")
                            Dim sItemGrp As String = getFormDFValue(oForm, "U_ItmGrp")
                            Dim sWastCd As String = getFormDFValue(oForm, "U_WasCd")
                            Dim sZipCode As String = getWFValue(oForm, "ZIPCODE")
                            Dim sAddress As String = getWFValue(oForm, "STADDR")
                            Dim sBranch As String = getFormDFValue(oForm, "U_Branch")
                            Dim sJobType As String = getFormDFValue(oForm, "U_JobTp")

                            Dim sDocDate As String = getFormDFValue(oForm, "U_RDate")
                            Dim dDocDate As Date = com.idh.utils.Dates.doStrToDate(sDocDate)

                            Dim sUOM As String
                            Dim dWeight As Double

                            sUOM = getFormDFValue(oForm, "U_UOM")
                            dWeight = getFormDFValue(oForm, "U_CstWgt")

                            oGrid.doCalculateLineChargePrices(sCardCode, sItemCode, sItemGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dWeight, sUOM, iRow, sZipCode)
                            oGrid.doCalculateLineCostPrices(sCardCode, sItemCode, sItemGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dWeight, sUOM, iRow, sZipCode)
                        End If
                    End If
                ElseIf sModalFormType = "IDHCSRCH" OrElse sModalFormType = "IDHNCSRCH" Then
                    Dim sTarget As String = getSharedData(oForm, "TRG")
                    Dim sCardCode As String = getSharedData(oForm, "CARDCODE")
                    Dim sCardName As String = getSharedData(oForm, "CARDNAME")
                    If Not (sCardCode Is Nothing) AndAlso sCardCode.Length > 0 Then
                        If sTarget = "WC" Then
                            'setFormDFValue(oForm, "U_CarrCd", sCardCode)
                            'setFormDFValue(oForm, "U_CarrNm", sCardName)

                            Dim sOldLock As String = oWOR.U_LckPrc
                            Dim sDoSip As String = getWFValue(oForm, "DOSIP")
                            If sDoSip = "Y" Then
                                oWOR.U_LckPrc = "N"
                            End If

                            oWOR.U_CarrCd = sCardCode
                            oWOR.U_CarrNm = sCardName

                            oWOR.doGetHaulageCostPrice(True)
                            oWOR.U_LckPrc = sOldLock

                        ElseIf sTarget = "ST" Then
                            doSetChoosenSite(oForm)
                        ElseIf sTarget = "SP" Then
                            oWOR.U_ProCd = sCardCode
                            oWOR.U_ProNm = sCardName
                            oWOR.U_ProRef = ""
                            'doGetTipCostPrices(oForm)
                        ElseIf sTarget = "PT" Then
                            oWOR.U_ProCd = sCardCode
                            oWOR.U_ProNm = sCardName
                            'If Config.INSTANCE.doGetDoBuyingAndTrade() Then
                            '    oWOR.doGetProducerCostPrice(True)
                            'End If
                        ElseIf sTarget = "LC" Then
                            oWOR.U_SLicSp = sCardCode
                            oWOR.U_SLicNm = sCardName
                        ElseIf sTarget = "CS" Then
                            oWOR.U_CustCs = sCardCode
                        ElseIf sTarget = "SUP" Then
                            'Dim oGrid As UpdateGrid = IDHGrid.getInstance(oForm, "ADDGRID")
                            Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")

                            If Not oGrid Is Nothing Then
                                Dim iRow As Integer = oGrid.getCurrentDataRowIndex()

                                'oGrid.doSetFieldValue("U_SuppCd", sCardCode)
                                'oGrid.doSetFieldValue("U_SuppNm", sCardName)

                                Dim sCustCode As String = getFormDFValue(oForm, "U_CustCd")
                                Dim sItemCode As String = getFormDFValue(oForm, "U_ItemCd")
                                Dim sItemGrp As String = getFormDFValue(oForm, "U_ItmGrp")
                                Dim sWastCd As String = getFormDFValue(oForm, "U_WasCd")
                                Dim sZipCode As String = getWFValue(oForm, "ZIPCODE")
                                Dim sAddress As String = getWFValue(oForm, "STADDR")
                                Dim sBranch As String = getFormDFValue(oForm, "U_Branch")
                                Dim sJobType As String = getFormDFValue(oForm, "U_JobTp")

                                Dim sDocDate As String = getFormDFValue(oForm, "U_RDate")
                                Dim dDocDate As Date = com.idh.utils.Dates.doStrToDate(sDocDate)

                                Dim sUOM As String
                                Dim dWeight As Double

                                sUOM = getFormDFValue(oForm, "U_UOM")
                                dWeight = getFormDFValue(oForm, "U_CstWgt")

                                oGrid.doCalculateLineCostPrices(sCustCode, sItemCode, sItemGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dWeight, sUOM, iRow, sZipCode)
                            End If

                            ''???????????
                            ''oGrid.doProcessData()
                            ''oGrid = Nothing
                        End If

                        doSetUpdate(oForm)
                    End If
                ElseIf sModalFormType = "BPALTSCH" Then
                    Dim sTarget As String = getSharedData(oForm, "TRG")
                    Dim sCardCode As String = getSharedData(oForm, "CARDCODE")
                    Dim sCardName As String = getSharedData(oForm, "CARDNAME")
                    doSetAlternateChoosenSite(oForm)
                ElseIf sModalFormType = "IDHPAYMET" Then
                    Dim sPayMeth As String = getSharedData(oForm, "METHOD")
                    Dim sStatus As String = getSharedData(oForm, "STATUS")
                    setFormDFValue(oForm, "U_Status", sStatus)
                    setFormDFValue(oForm, "U_PayMeth", sPayMeth)
                    setFormDFValue(oForm, "U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusUnPaid())
                    setFormDFValue(oForm, "U_CCNum", "")
                    setFormDFValue(oForm, "U_CCType", "0")
                    setFormDFValue(oForm, "U_CCStat", "")

                    If sPayMeth.Equals(Config.INSTANCE.getCreditCardName()) Then
                        Dim sDoAdd As String = ""
                        If ghOldDialogParams.Contains(oForm.UniqueID) Then
                            Dim oData As ArrayList = ghOldDialogParams.Item(oForm.UniqueID)
                            If Not (oData Is Nothing) AndAlso oData.Count > 1 Then
                                sDoAdd = oData.Item(0)
                            End If
                        End If

                        If Not (sDoAdd = "NoUpdate" OrElse sDoAdd = "IDHGRID") OrElse
                         Config.INSTANCE.getParameterAsBool("CCONROW", False) Then
                            Dim dRowTotal As Double = getFormDFValue(oForm, "U_Total")
                            Dim sCustCd As String = getFormDFValue(oForm, "U_CustCd")
                            Dim sCustNm As String = getFormDFValue(oForm, "U_CustNm")

                            setSharedData(oForm, "CUSTCD", sCustCd)
                            setSharedData(oForm, "CUSTNM", sCustNm)
                            setSharedData(oForm, "AMOUNT", dRowTotal)
                            goParent.doOpenModalForm("IDHCCPAY", oForm)
                        End If
                    ElseIf sStatus.Equals(com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus()) AndAlso (sPayMeth.Equals("Cash") OrElse sPayMeth.Equals("Check") OrElse sPayMeth.Equals("Cheque")) Then
                        setFormDFValue(oForm, "U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusPaid())
                    ElseIf sStatus.Equals(com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus()) AndAlso sPayMeth.Equals(com.idh.bridge.lookups.FixedValues.getPayMethFoc()) Then
                        setFormDFValue(oForm, "U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusFree())
                    End If
                ElseIf sModalFormType = "IDHCCPAY" Then
                    Dim sCCStatPre As String
                    Dim sPStat As String
                    Dim sCCNum As String
                    Dim sCCType As String
                    Dim sCCIs As String
                    Dim sCCSec As String
                    Dim sCCHNm As String
                    Dim sCCPCd As String
                    Dim sCCExp As String

                    sCCNum = getSharedData(oForm, "CCNUM")
                    sPStat = getSharedData(oForm, "PSTAT")
                    sCCType = getSharedData(oForm, "CCTYPE")
                    sCCIs = getSharedData(oForm, "CCIs")
                    sCCSec = getSharedData(oForm, "CCSec")
                    sCCHNm = getSharedData(oForm, "CCHNm")
                    sCCPCd = getSharedData(oForm, "CCPCd")
                    sCCExp = getSharedData(oForm, "CCEXP")
                    If sPStat.Equals(com.idh.bridge.lookups.FixedValues.getStatusPaid()) Then
                        sCCStatPre = "APPROVED: "
                    Else
                        sCCStatPre = "DECLINED: "
                    End If
                    setFormDFValue(oForm, "U_CCNum", sCCNum)
                    setFormDFValue(oForm, "U_CCType", sCCType)
                    setFormDFValue(oForm, "U_CCStat", sCCStatPre & getSharedData(oForm, "CCSTAT"))
                    setFormDFValue(oForm, "U_PayStat", sPStat)
                    setFormDFValue(oForm, "U_CCExp", sCCExp)
                    setFormDFValue(oForm, "U_CCIs", sCCIs)
                    setFormDFValue(oForm, "U_CCSec", sCCSec)
                    setFormDFValue(oForm, "U_CCHNum", sCCHNm)
                    setFormDFValue(oForm, "U_CCPCd", sCCPCd)
                    'KA -- START -- 20121022
                ElseIf sModalFormType = "IDHEMPSCR" Then
                    ''Dim oGrid As UpdateGrid = IDHGrid.getInstance(oForm, "ADDGRID")
                    'Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")

                    'If Not oGrid Is Nothing Then
                    '    Dim iRow As Integer = oGrid.getCurrentLine()
                    '    Dim sEmpID As String = getSharedData(oForm, "EMPID")
                    '    Dim sEmpFullNM As String = getSharedData(oForm, "FULLNM")

                    '    oGrid.doSetFieldValue("U_EmpId", sEmpID)
                    '    oGrid.doSetFieldValue("U_EmpNm", sEmpFullNM)
                    'End If
                    'oGrid.doProcessData()
                    'oGrid = Nothing
                    'doClearSharedData(oForm)
                    'doSetUpdate(oForm)
                    ''KA -- END -- 20121022
                ElseIf sModalFormType = "IDHASRCH" Then
                    Dim sTrg As String = getSharedData(oForm, "TRG")
                    If sTrg = "IDH_SITAL" Then
                        'setFormDFValue(oForm, IDH_JOBSHD._SAddress, getSharedData(oForm, "ADDRESS"))
                        'setFormDFValue(oForm, IDH_JOBSHD._SAddrsLN, getSharedData(oForm, "LINENUM"))
                        oWOR.U_SAddress = getSharedData(oForm, "ADDRESS")
                        oWOR.U_SAddrsLN = getSharedData(oForm, "LINENUM")
                        doSetUpdate(oForm)
                    End If
                ElseIf sModalFormType.Equals("IDHTFSNSR") Then
                    Dim sTFSNum As String = getSharedData(oForm, "IDH_TFSNUM")
                    If sTFSNum IsNot Nothing AndAlso sTFSNum.Length > 0 Then
                        'Open the TFS Mgmt. Console form and populate WP details as per 1st WOR
                        setSharedData(oForm, "TFSNUM", sTFSNum)
                        setSharedData(oForm, "WORNO", getFormDFValue(oForm, "Code"))
                        setSharedData(oForm, "WOHNO", getFormDFValue(oForm, "U_JobNr"))
                        'Check weather TFS has status 5=Acknowledge or 6=Consented
                        If doCheckTFSStatus(sTFSNum) Then
                            'Update TFS Movement rows with WOH and WOR details
                            doUpdateTFSMovementRows(sTFSNum, getFormDFValue(oForm, "U_JobNr"), getFormDFValue(oForm, "Code"))

                            setFormDFValue(oForm, "U_TFSNum", sTFSNum)
                            Dim sASDate As String = getFormDFValue(oForm, "U_ASDate")
                            If sASDate Is Nothing OrElse sASDate.Length = 0 Then
                                setFormDFValue(oForm, "U_ASDate", goParent.doDateToStr())
                            End If
                            doSetUpdate(oForm)

                            goParent.doOpenForm("IDH_TFSMAST", oForm, False)
                        Else
                            doWarnMess("The selected TFS Status has not been Acknowledged or Consented. Kindly update TFS to stamp WOR's on movements and reselect TFS Number again.")
                        End If
                    End If
                ElseIf sModalFormType.Equals("IDHEVNNSR") Then
                    Dim sEVNNum As String = getSharedData(oForm, "IDH_EVNNUM")
                    If sEVNNum IsNot Nothing AndAlso sEVNNum.Length > 0 Then
                        'Open the EVN form 
                        setFormDFValue(oForm, "U_EVNNum", sEVNNum)
                        doSetUpdate(oForm)
                        'goParent.doOpenForm("IDHEVNMAS", oForm, False)
                    End If
                    ''## MA STart Issue#407
                ElseIf sModalFormType = "IDHROSRC" Then
                    setFormDFValue(oForm, IDH_JOBSHD._IDHRTCD, getSharedData(oForm, "IDH_RTCODE"))
                    doSetUpdate(oForm)
                    ''## MA End Issue#407
                End If

                ''Disabled because currencies will be set by CIP/SIP DBObjects now 
                'If sModalFormType = "IDHCSRCH" OrElse sModalFormType = "IDHLNKWO" OrElse sModalFormType = "IDHLOSCH" Then
                '    doSetCurrencies(oForm, True, sModalFormType)
                'End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error handeling Modal results - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            Finally
                oForm.Freeze(False)
            End Try
            doClearSharedData(oForm)
        End Sub

        Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            If oData Is Nothing Then Exit Sub
            Dim oArr As ArrayList = oData
            If oArr.Count = 0 Then
                Return
            End If
            If sModalFormType = "IDHSSRCH" Then
                Dim ssType As String = oArr.Item(0)
                If ssType = "SER" Then
                    setFormDFValue(oForm, "U_Serial", oArr.Item(1), False, True)
                End If
                doSetUpdate(oForm)
            ElseIf sModalFormType = "IDH_DISPORD" Then
                If oArr.Contains("LinkUpdated") Then
                    Dim sCode As String = getItemValue(oForm, "IDH_JOBSCH")
                    If doLoadRowFromCode(oForm, sCode) = True Then
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        doLoadDataF(oForm, False)
                    End If
                End If
            End If
        End Sub

        'Public Shared Function doUpdateWOFromDO(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sDOOrder As String, ByVal sDOOrderRow As String) As Boolean
        '    Dim oDispRow As com.idh.dbObjects.User.IDH_DISPROW = New com.idh.dbObjects.User.IDH_DISPROW()
        '    If oDispRow.doUpdateLinkedWO(sDOOrder, sDOOrderRow) Then
        '        Dim bDoMDDocs As Boolean = Config.INSTANCE.getParameterAsBool("MDAGEN")
        '        If bDoMDDocs Then
        '            Dim sHeadTable As String = "@IDH_JOBENTR"
        '            Dim sRowTable As String = "@IDH_JOBSHD"

        '            com.idh.bridge.action.MarketingDocs.doOrders(sHeadTable, sRowTable, "WO", oDispRow.U_WROrd, oDispRow.U_WRRow)
        '            com.idh.bridge.action.MarketingDocs.doInvoicesAR(sHeadTable, sRowTable, "WO", oDispRow.U_WROrd, oDispRow.U_WRRow)
        '            com.idh.bridge.action.MarketingDocs.doRebates(sHeadTable, sRowTable, "WO", oDispRow.U_WROrd, oDispRow.U_WRRow)
        '            com.idh.bridge.action.MarketingDocs.doFoc(sHeadTable, sRowTable, "WO", oDispRow.U_WROrd, oDispRow.U_WRRow)

        '        End If
        '        Return True

        '    End If
        '    Return False
        'End Function

        Public Shared Function doAutoCreateNewRowFromLastJobRow_(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sJob As String, ByVal bCopyComments As Boolean, ByVal bCopyPrice As Boolean, ByVal bCopyQty As Boolean) As String
            Dim sNewCode As String = Nothing

            'Get the last row from the WO
            Dim sQry As String = "SELECT r.Code, r.U_JobNr, e.U_CardCd, r.U_ItmGrp, r.U_JobTp FROM [@IDH_JOBSHD] r, [@IDH_JOBENTR] e WHERE r.Code = ( SELECT MAX( CAST( Code As Numeric) ) As Row FROM [@IDH_JOBSHD] WHERE U_JobNr = '" & sJob & "' ) AND e.Code = r.U_JobNr"
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = oParent.goDB.doSelectQuery(sQry)
                If Not oRecordSet Is Nothing AndAlso oRecordSet.RecordCount > 0 Then
                    Dim sRowCode As String = oRecordSet.Fields.Item("Code").Value
                    Dim sWOCode As String = oRecordSet.Fields.Item("U_JobNr").Value
                    Dim sCustomer As String = oRecordSet.Fields.Item("U_CardCd").Value
                    Dim sItemGrp As String = oRecordSet.Fields.Item("U_ItmGrp").Value
                    Dim sJobType As String = oRecordSet.Fields.Item("U_JobTp").Value

                    If Config.INSTANCE.doCheckWOIsJobAvail(sWOCode, sItemGrp, sJobType) = True Then
                        Dim sAdditionalMessage As String = Translation.getTranslatedWord("Creating a Job for Customer") &
                           "  - " & sCustomer & " " &
                           Translation.getTranslatedWord("from row") & " - " & sRowCode
                        If Config.INSTANCE.doCheckCredit(sCustomer, 0, True, True, sAdditionalMessage) = True Then
                            'sNewCode = doDuplicateRow_(oParent, sRowCode, bCopyComments, bCopyPrice, bCopyQty, sJobType, True, True)
                            sNewCode = IDH_JOBSHD.doDuplicateRow(sRowCode, bCopyComments, bCopyPrice, bCopyQty, sJobType, True, True)
                        End If
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Auto Create From WO: " & sJob)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXACWO", {sJob})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
            End Try
            Return sNewCode
        End Function

        'Public Shared Function doDuplicateRow_(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sRowCode As String, ByVal bCopyComments As Boolean, ByVal bCopyPrice As Boolean, ByVal bCopyQty As Boolean, ByVal bCopyAddItems As Boolean) As String
        '    'Return doDuplicateRow_(oParent, sRowCode, bCopyComments, bCopyPrice, Nothing, False, True, bCopyAddItems)
        '    Return doDuplicateRow_(oParent, sRowCode, bCopyComments, bCopyPrice, bCopyQty, Nothing, False, bCopyAddItems)
        'End Function

        'Public Shared Function doDuplicateRow2(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sRowCode As String, ByVal bCopyComments As Boolean, ByVal bCopyPrice As Boolean, ByVal bCopyQty As Boolean, ByVal sJob As String, ByVal bCopyAddItems As Boolean) As String
        '    Return doDuplicateRow_(oParent, sRowCode, bCopyComments, bCopyPrice, bCopyQty, sJob, False, False, bCopyAddItems, True, True, "")
        'End Function

        'Public Shared Function doDuplicateRow_(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sRowCode As String, ByVal bCopyComments As Boolean, ByVal bCopyPrice As Boolean, ByVal bCopyQty As Boolean, ByVal sJob As String, ByVal bClearHaulage As Boolean, ByVal bCopyAddItems As Boolean) As String
        '    Return doDuplicateRow_(oParent, sRowCode, bCopyComments, bCopyPrice, bCopyQty, sJob, bClearHaulage, False, bCopyAddItems, True, True, "")
        'End Function

        'Public Shared Function doDuplicateRow2(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sRowCode As String, ByVal bCopyComments As Boolean, ByVal bCopyPrice As Boolean, ByVal bCopyQty As Boolean, ByVal sJob As String, ByVal bCopyAddItems As Boolean, ByVal bCopyVehReg As Boolean, ByVal bCopyDriver As Boolean, ByVal sReqDate As String) As String
        '    Return doDuplicateRow_(oParent, sRowCode, bCopyComments, bCopyPrice, bCopyQty, sJob, False, False, bCopyAddItems, bCopyVehReg, bCopyDriver, sReqDate)
        'End Function

        ''** Duplicate the Row outside of the Grid
        'Public Shared Function doDuplicateRow_(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sRowCode As String, ByVal bCopyComments As Boolean, ByVal bCopyPrice As Boolean, ByVal bCopyQty As Boolean, ByVal sJob As String, ByVal bClearHaulage As Boolean, ByVal bDoStart As Boolean, ByVal bCopyAdditems As Boolean, ByVal bCopyVehReg As Boolean, ByVal bCopyDriver As Boolean, ByVal sReqDate As String) As String
        '    Return IDH_JOBSHD.doDuplicateRow(sRowCode, bCopyComments, bCopyPrice, bCopyQty, sJob, bClearHaulage, bDoStart, bCopyAdditems, bCopyVehReg, bCopyDriver, sReqDate)

        '    'Dim oTableSrc As SAPbobsCOM.UserTable = Nothing
        '    'Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(oParent, msMainTable)
        '    'Dim sNewCode As String = Nothing

        '    'Try
        '    '    'Step through the selected rows and update
        '    '    Dim oField As SAPbobsCOM.Field
        '    '    Dim sFieldName As String

        '    '    '                Dim sRow As String
        '    '    Dim iwResult As Integer
        '    '    Dim swResult As String = Nothing
        '    '    oTableSrc = oParent.goDICompany.UserTables.Item("IDH_JOBSHD")

        '    '    Dim sNowDate As String = oParent.doDateToStr()
        '    '    Dim sNowTime As String = com.idh.utils.dates.doTimeToNumStr()

        '    '    If oTableSrc.GetByKey(sRowCode) Then
        '    '        sNewCode = com.idh.utils.Conversions.ToString(oParent.goDB.doNextNumber("JOBSCHE"))

        '    '        oAuditObj.setKeyPair(sNewCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
        '    '        oAuditObj.setName(sNewCode)


        '    '        'First Copy all the values to clear or reset it later

        '    '        '*******************************************************
        '    '        'The Date validation and formatter changed for 8.81:
        '    '        '      The code was moved into the framework
        '    '        '      to apply it to all DataSource Setters otherwise we
        '    '        '      we would have had to change all the setters
        '    '        '*******************************************************
        '    '        '*******************************************************
        '    '        'The Time Type changed from Integer to ShortNumber:
        '    '        '      The code was moved into the framework
        '    '        '      to apply it to all DataSource Setters otherwise we
        '    '        '      we would have had to change all the setters
        '    '        '*******************************************************
        '    '        Dim iFields As Integer = oTableSrc.UserFields.Fields.Count
        '    '        For iCol As Integer = 0 To iFields - 1
        '    '            oField = oTableSrc.UserFields.Fields.Item(iCol)
        '    '            sFieldName = oField.Name
        '    '            oAuditObj.setFieldValue(sFieldName, oField.Value)

        '    '        Next

        '    '        If Config.INSTANCE.getParameterAsBool("WOCLRSPO", False) Then
        '    '            Dim sCustRef As String = oAuditObj.getFieldValue("U_CustRef")
        '    '            If Not sCustRef Is Nothing AndAlso sCustRef.Length() > 0 Then
        '    '                Dim sJobNumber As String = oAuditObj.getFieldValue("U_JobNr")
        '    '                ''Check the site Customer Ref
        '    '                Dim sCustDRef As String = Config.INSTANCE.doGetCustRef(sJobNumber) ''Config.INSTANCE.doGetCustRef(oParent, sJobNumber)
        '    '                If sCustDRef.Length = 0 OrElse sCustDRef.Equals(sCustRef) = False Then
        '    '                    oAuditObj.setFieldValue("U_CustRef", "")



        '    '                End If
        '    '            End If
        '    '        End If

        '    '        oAuditObj.setFieldValue("U_Ser1", "")
        '    '        oAuditObj.setFieldValue("U_Ser2", "")
        '    '        oAuditObj.setFieldValue("U_WDt1", "")
        '    '        oAuditObj.setFieldValue("U_WDt2", "")

        '    '        If bCopyQty = False Then

        '    '            'Shared
        '    '            oAuditObj.setFieldValue("U_Wei1", 0)
        '    '            oAuditObj.setFieldValue("U_Wei2", 0)


        '    '            'Cost
        '    '            oAuditObj.setFieldValue(IDH_JOBSHD._TRdWgt, 0)
        '    '            oAuditObj.setFieldValue("U_TipWgt", 0)
        '    '            If bClearHaulage = True Then
        '    '                'oAuditObj.setFieldValue("U_OrdWgt", 0)

        '    '            End If
        '    '            oAuditObj.setFieldValue("U_OrdWgt", 0)

        '    '            Dim sVal As String = oAuditObj.getFieldValue("U_UseWgt")
        '    '            oAuditObj.setFieldValue("U_TAUOMQt", 0)
        '    '            oAuditObj.setFieldValue("U_PAUOMQt", 0)
        '    '            oAuditObj.setFieldValue("U_PRdWgt", 0)
        '    '            oAuditObj.setFieldValue("U_ProWgt", 0)


        '    '            'Charge
        '    '            oAuditObj.setFieldValue("U_AUOMQt", 0)
        '    '            oAuditObj.setFieldValue("U_RdWgt", 0)
        '    '            oAuditObj.setFieldValue("U_CstWgt", 0)
        '    '            If bClearHaulage = True Then
        '    '                'oAuditObj.setFieldValue("U_CusQty", 0)
        '    '                'oAuditObj.setFieldValue("U_HlSQty", 0)

        '    '            End If
        '    '            oAuditObj.setFieldValue("U_CusQty", 0)
        '    '            oAuditObj.setFieldValue("U_HlSQty", 0)

        '    '        End If

        '    '        If bCopyPrice = False OrElse bCopyQty = False Then

        '    '            If bCopyPrice = False Then

        '    '                'Cost
        '    '                oAuditObj.setFieldValue("U_TipCost", 0)
        '    '                oAuditObj.setFieldValue("U_OrdCost", 0)
        '    '                oAuditObj.setFieldValue("U_PCost", 0)


        '    '                'Charge
        '    '                oAuditObj.setFieldValue("U_TCharge", 0)
        '    '                oAuditObj.setFieldValue("U_CusChr", 0)

        '    '            End If


        '    '            'Cost
        '    '            oAuditObj.setFieldValue("U_TipTot", 0)
        '    '            oAuditObj.setFieldValue("U_OrdTot", 0)
        '    '            oAuditObj.setFieldValue("U_PCTotal", 0)
        '    '            oAuditObj.setFieldValue("U_VtCostAmt", 0)
        '    '            'oAuditObj.setFieldValue("U_TAddCost", 0)

        '    '            'Change 20130927: The Total Add Item Cost was commented above. Have uncommented after an issue raised by CBI 
        '    '            oAuditObj.setFieldValue("U_TAddCost", 0)

        '    '            oAuditObj.setFieldValue("U_JCost", 0)

        '    '            'oAuditObj.setFieldValue("U_UseWgt", "1")


        '    '            'Charge
        '    '            oAuditObj.setFieldValue("U_SLicCTo", 0)
        '    '            oAuditObj.setFieldValue("U_SLicCh", 0)
        '    '            oAuditObj.setFieldValue("U_CongCh", 0)

        '    '            oAuditObj.setFieldValue("U_TCTotal", 0)

        '    '            oAuditObj.setFieldValue("U_Price", 0)

        '    '            oAuditObj.setFieldValue("U_Discnt", 0)
        '    '            oAuditObj.setFieldValue("U_DisAmt", 0)

        '    '            oAuditObj.setFieldValue("U_TaxAmt", 0)
        '    '            oAuditObj.setFieldValue("U_TAddChrg", 0)
        '    '            oAuditObj.setFieldValue("U_Total", 0)
        '    '        ElseIf bClearHaulage Then
        '    '            oAuditObj.setFieldValue("U_OrdTot", 0)
        '    '            oAuditObj.setFieldValue("U_Price", 0)

        '    '        End If

        '    '        Dim sLastTAddCost As String = ""
        '    '        Dim sLastTAddCharge As String = ""
        '    '        'Manage Additional Items Cost/Charge 
        '    '        If bCopyAdditems = False Then
        '    '            sLastTAddCost = oAuditObj.getFieldValue("U_TAddCost")
        '    '            sLastTAddCharge = oAuditObj.getFieldValue("U_TAddChrg")

        '    '            oAuditObj.setFieldValue("U_TAddCost", 0)
        '    '            oAuditObj.setFieldValue("U_TAddChrg", 0)

        '    '        End If

        '    '        oAuditObj.setFieldValue("U_JobRmD", "")
        '    '        oAuditObj.setFieldValue("U_JobRmT", 0)
        '    '        oAuditObj.setFieldValue("U_RemNot", "")
        '    '        oAuditObj.setFieldValue("U_RemCnt", 0)

        '    '        oAuditObj.setFieldValue("U_ConNum", "")
        '    '        oAuditObj.setFieldValue("U_LoadSht", "")

        '    '        'oAuditObj.setFieldValue("U_ExpLdWgt", 0)

        '    '        Dim sWO As String = oTableSrc.UserFields.Fields.Item("U_JobNr").Value
        '    '        Dim sSource As String = sWO & "." & sRowCode

        '    '        'OnTime [#Ico000????] USA

        '    '        'START

        '    '        '** This bit is already covered now in the Duplicate Options Form
        '    '        'If bCopyComments = False Then
        '    '        '    Dim sComment As String = oAuditObj.getFieldValue("U_Comment")
        '    '        '    Dim sCommentInt As String = oAuditObj.getFieldValue("U_IntComm")
        '    '        '    If sComment.Length > 0 OrElse sCommentInt.Length > 0 Then
        '    '        '        If oParent.doResourceMessageYN("WORDPCM", Nothing, 1) = 1 Then
        '    '        '            bCopyComments = True



        '    '        '        End If
        '    '        '    End If
        '    '        'End If


        '    '        'END
        '    '        'OnTime [#Ico000????] USA

        '    '        If bCopyComments = False Then
        '    '            oAuditObj.setFieldValue("U_Comment", "")
        '    '            oAuditObj.setFieldValue("U_IntComm", "")
        '    '            oAuditObj.setFieldValue("U_ExtComm1", "")
        '    '            oAuditObj.setFieldValue("U_ExtComm2", "")

        '    '        Else
        '    '            If Config.INSTANCE.getParameterAsBool("MDDPRC", True) = True Then
        '    '                oAuditObj.setFieldValue("U_Comment", "")


        '    '            End If
        '    '        End If

        '    '        'Copy Route and Sequence when duplicating
        '    '        'KASHIF's Change 20121219 Start
        '    '        'If Config.INSTANCE.getParameterAsBool("CLRRCSQ", True) = True Then
        '    '        '    oAuditObj.setFieldValue("U_IDHRTCD", "")
        '    '        '    oAuditObj.setFieldValue("U_IDHSEQ", "")

        '    '        'End If
        '    '        If Config.INSTANCE.getParameterAsBool("CLRRCSQ", True) = True Then
        '    '            oAuditObj.setFieldValue("U_IDHRTCD", "")
        '    '            oAuditObj.setFieldValue("U_IDHRTDT", "")
        '    '            oAuditObj.setFieldValue("U_IDHSEQ", "")

        '    '        End If
        '    '        'KASHIF's Change 20121219 End
        '    '        'Copy Route and Sequence when duplicating

        '    '        oAuditObj.setFieldValue("U_Covera", "")
        '    '        oAuditObj.setFieldValue("U_CoverHst", "")

        '    '        oAuditObj.setFieldValue("U_User", oParent.gsUserName)
        '    '        Dim sCurrentJob As String = oTableSrc.UserFields.Fields.Item("U_JobTp").Value
        '    '        If sJob Is Nothing Then
        '    '            sJob = sCurrentJob

        '    '        Else
        '    '            '                    	If sJob.Equals(sCurrentJob) False Then



        '    '            '
        '    '            '                    	End If
        '    '        End If
        '    '        oAuditObj.setFieldValue("U_JobTp", sJob)

        '    '        Dim dWeightTotal As Double = 0

        '    '        Dim sItemCode As String = oTableSrc.UserFields.Fields.Item("U_ItemCd").Value


        '    '        '    ''Shared
        '    '        '                    Dim sCardCode As String = oTableSrc.UserFields.Fields.Item("U_CustCd").Value
        '    '        '                    Dim dVatRate As Double = Config.INSTANCE.doGetItemVatRate(oParent, sItemCode)
        '    '        '                    Dim dCustChr As Double = oTableSrc.UserFields.Fields.Item("U_CusChr").Value



        '    '        '                    Dim dTotal As Double
        '    '        '                    Dim dVatAmt As Double
        '    '        '                    If dVatRate > 0 Then
        '    '        '                        dVatAmt = dCustChr * dVatRate / 100




        '    '        '                    Else
        '    '        '                        dVatAmt = 0
        '    '        '                    End If
        '    '        '
        '    '        '                    dTotal = dCustChr + dVatAmt
        '    '        '                    oAuditObj.setFieldValue("U_TaxAmt", dVatAmt)
        '    '        '                    oAuditObj.setFieldValue("U_Total", dTotal)


        '    '        '                    oAuditObj.setFieldValue("U_Total", 0)

        '    '        oAuditObj.setFieldValue("U_Status", com.idh.bridge.lookups.FixedValues.getStatusOpen())
        '    '        oAuditObj.setFieldValue("U_PStat", com.idh.bridge.lookups.FixedValues.getStatusOpen())
        '    '        oAuditObj.setFieldValue("U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusUnPaid())
        '    '        oAuditObj.setFieldValue("U_PayMeth", "")
        '    '        oAuditObj.setFieldValue("U_CCNum", "")
        '    '        oAuditObj.setFieldValue("U_CCType", "0")
        '    '        oAuditObj.setFieldValue("U_CCStat", "")

        '    '        oAuditObj.setFieldValue("U_CCExp", "")
        '    '        oAuditObj.setFieldValue("U_CCIs", "")
        '    '        oAuditObj.setFieldValue("U_CCSec", "")
        '    '        oAuditObj.setFieldValue("U_CCHNum", "")
        '    '        oAuditObj.setFieldValue("U_CCPCd", "")

        '    '        oAuditObj.setFieldValue("U_SAORD", "")
        '    '        oAuditObj.setFieldValue("U_TIPPO", "")
        '    '        oAuditObj.setFieldValue("U_JOBPO", "")
        '    '        oAuditObj.setFieldValue("U_ProPO", "")
        '    '        oAuditObj.setFieldValue("U_ProGRPO", "")
        '    '        oAuditObj.setFieldValue("U_SODlvNot", "")

        '    '        If Config.INSTANCE.getParameterAsBool("USAREL", False) Then
        '    '            oAuditObj.setFieldValue("U_Jrnl", "")
        '    '            oAuditObj.setFieldValue("U_JStat", "")

        '    '            oAuditObj.setFieldValue("U_CBICont", "")
        '    '            oAuditObj.setFieldValue("U_CBIManQty", "")
        '    '            oAuditObj.setFieldValue("U_CBIManUOM", "")

        '    '        End If

        '    '        oAuditObj.setFieldValue("U_GRIn", "")
        '    '        oAuditObj.setFieldValue("U_SAINV", "")
        '    '        oAuditObj.setFieldValue("U_SLPO", "")
        '    '        oAuditObj.setFieldValue("U_TPCN", "")
        '    '        oAuditObj.setFieldValue("U_TCCN", "")
        '    '        oAuditObj.setFieldValue("U_Covera", "")
        '    '        oAuditObj.setFieldValue("U_CoverHst", "")

        '    '        'KA -- START -- 20121025
        '    '        'If bDoStart Then
        '    '        '    oAuditObj.setFieldValue("U_ASDate", sNowDate)
        '    '        '    oAuditObj.setFieldValue("U_ASTime", sNowTime)

        '    '        'Else
        '    '        '    oAuditObj.setFieldValue("U_ASDate", "")
        '    '        '    oAuditObj.setFieldValue("U_ASTime", "")

        '    '        'End If

        '    '        'oAuditObj.setFieldValue("U_AEDate", "")
        '    '        'oAuditObj.setFieldValue("U_AETime", "")
        '    '        'KA -- END -- 20121025

        '    '        oAuditObj.setFieldValue("U_WROrd", "")
        '    '        oAuditObj.setFieldValue("U_WRRow", "")

        '    '        oAuditObj.setFieldValue("U_LnkPBI", "")
        '    '        oAuditObj.setFieldValue("U_Sched", "N")
        '    '        oAuditObj.setFieldValue("U_USched", "Y")

        '    '        oAuditObj.setFieldValue("U_DPRRef", "")

        '    '        oAuditObj.setFieldValue("U_WastTNN", "")
        '    '        oAuditObj.setFieldValue("U_HazWCNN", "")
        '    '        oAuditObj.setFieldValue("U_SiteRef", "") 'Not Sure why this gets cleared
        '    '        oAuditObj.setFieldValue("U_ExtWeig", "")

        '    '        'KA -- START -- 20121022
        '    '        oAuditObj.setFieldValue("U_CBICont", 0)
        '    '        oAuditObj.setFieldValue("U_CBIManQty", 0)
        '    '        oAuditObj.setFieldValue("U_CBIManUOM", "")
        '    '        'KA -- END -- 20121022

        '    '        'Moved the block above for bCopyComments Flag
        '    '        ''KA -- START -- 20121025
        '    '        ''New Comments field with extended length
        '    '        'oAuditObj.setFieldValue("U_ExtComm1", "")
        '    '        'oAuditObj.setFieldValue("U_ExtComm2", "")
        '    '        ''KA -- END -- 20121025

        '    '        oAuditObj.setFieldValue("U_MaximoNum", "")

        '    '        oAuditObj.setFieldValue("U_TFSReq", "")
        '    '        oAuditObj.setFieldValue("U_TFSNum", "")

        '    '        If Config.INSTANCE.getParameterAsBool("MDDPVE", True) Then
        '    '            oAuditObj.setFieldValue("U_Lorry", "")
        '    '            oAuditObj.setFieldValue("U_LorryCd", "")
        '    '            oAuditObj.setFieldValue("U_Driver", "")
        '    '            oAuditObj.setFieldValue("U_TRLReg", "")
        '    '            oAuditObj.setFieldValue("U_TRLNM", "")
        '    '            oAuditObj.setFieldValue("U_VehTyp", "")

        '    '        End If

        '    '        If Config.INSTANCE.getParameterAsBool("WORCLRCA", False) Then
        '    '            oAuditObj.setFieldValue("U_CarrCd", "")
        '    '            oAuditObj.setFieldValue("U_CarrNm", "")

        '    '        End If

        '    '        'KA -- START -- 20121025
        '    '        'Dim sUseTodaysDate As String = Config.Parameter("MDDPTD")
        '    '        'If Not sUseTodaysDate Is Nothing AndAlso sUseTodaysDate.ToUpper.Equals("TRUE") Then
        '    '        '    oAuditObj.setFieldValue("U_RDate", sNowDate) 'oParent.doDateToStr())

        '    '        'End If
        '    '        'oAuditObj.setFieldValue("U_BDate", sNowDate)
        '    '        'oAuditObj.setFieldValue("U_BTime", sNowTime)


        '    '        'START
        '    '        'Setup up Booking, Requested Date & Time
        '    '        'DATE Block
        '    '        Dim sUseTodaysDate As String = Config.Parameter("MDDPTD")
        '    '        If sUseTodaysDate IsNot Nothing AndAlso sUseTodaysDate.ToUpper.Equals("TRUE") Then
        '    '            oAuditObj.setFieldValue("U_BDate", sNowDate)
        '    '            oAuditObj.setFieldValue("U_RDate", sNowDate)

        '    '        Else
        '    '            'will default to source row DATE automatically

        '    '        End If

        '    '        'TIME Block
        '    '        Dim sUseTodaysTime As String = Config.Parameter("MDDPTT")
        '    '        If sUseTodaysTime IsNot Nothing AndAlso sUseTodaysTime.ToUpper.Equals("TRUE") Then
        '    '            oAuditObj.setFieldValue("U_BTime", sNowTime)
        '    '            oAuditObj.setFieldValue("U_RTime", sNowTime)
        '    '            oAuditObj.setFieldValue("U_RTimeT", sNowTime)

        '    '        Else
        '    '            'will default to source row TIME automatically

        '    '        End If

        '    '        'No rule for Actual End DATE & TIME
        '    '        oAuditObj.setFieldValue("U_ASDate", "")
        '    '        oAuditObj.setFieldValue("U_ASTime", 0)

        '    '        oAuditObj.setFieldValue("U_AEDate", "")
        '    '        oAuditObj.setFieldValue("U_AETime", 0)

        '    '        'END
        '    '        'KA -- END -- 20121025

        '    '        If bCopyPrice = False Then
        '    '            doGetCharges(oParent, oAuditObj, oAuditObj)
        '    '            doGetCosts(oParent, oAuditObj, oAuditObj)

        '    '        End If

        '    '        'Recalculate if Additional Items = false 
        '    '        If bCopyAdditems = False Then
        '    '            Dim sJCost As String = oAuditObj.getFieldValue("U_JCost") 'Cost total 
        '    '            Dim sTotal As String = oAuditObj.getFieldValue("U_Total") 'Charge total 

        '    '            Dim dTAddCost As Double = 0
        '    '            Dim dTAddCharge As Double = 0

        '    '            Dim dJCost As Double = 0
        '    '            Dim dTotal As Double = 0

        '    '            If sJCost IsNot Nothing AndAlso sJCost.Length > 0 Then
        '    '                dJCost = Convert.ToDouble(sJCost)

        '    '            End If
        '    '            If sTotal IsNot Nothing AndAlso sTotal.Length > 0 Then
        '    '                dTotal = Convert.ToDouble(sTotal)

        '    '            End If
        '    '            If sLastTAddCost.Length > 0 Then
        '    '                dTAddCost = Convert.ToDouble(sLastTAddCost)

        '    '            End If
        '    '            If sLastTAddCharge.Length > 0 Then
        '    '                dTAddCharge = Convert.ToDouble(sLastTAddCharge)

        '    '            End If

        '    '            oAuditObj.setFieldValue("U_JCost", dJCost - dTAddCost)
        '    '            oAuditObj.setFieldValue("U_Total", dTotal - dTAddCharge)

        '    '        End If

        '    '        iwResult = oAuditObj.Commit()
        '    '        If iwResult <> 0 Then
        '    '            oParent.goDICompany.GetLastError(iwResult, swResult)
        '    '            com.idh.bridge.DataHandler.INSTANCE.doError("Error Duplicating: " + swResult)
        '    '            sNewCode = Nothing

        '    '        Else
        '    '            com.idh.dbObjects.User.IDH_BPFORECST.doUpdateForecast(sWO)


        '    '        End If
        '    '    End If
        '    'Catch ex As Exception
        '    '    com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error duplicating the Row.")
        '    '    sNewCode = Nothing
        '    'Finally
        '    '    IDHAddOns.idh.data.Base.doReleaseObject(oTableSrc)
        '    '    IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj)
        '    'End Try
        '    'Return sNewCode
        'End Function

        'Public Shared Function doDuplicateAdditionalItems(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParentRowCode As String, ByVal sNewRowCode As String, ByVal bCopyPrice As Boolean, ByVal bCopyQty As Boolean) As String
        '    ''Code added by Kashif - which is incorrect as we will loose the audit trail and it also does not cater for the copy Price and Copy Qty Options.... it is also copying over all the marketing document numbers

        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing


        '    Try
        '        Dim oField As SAPbobsCOM.Field
        '        Dim sFieldName As String
        '        Dim sQry As String
        '        Dim sInsertQry As String

        '        Dim sNowDate As String = oParent.doDateToStr()
        '        Dim sNowTime As String = com.idh.utils.dates.doTimeToNumStr()

        '        sQry = "select [Code], [Name], [U_JobNr], [U_RowNr], [U_ItemCd], [U_ItemDsc], [U_CustCd], [U_CustNm], " & _
        '        " [U_SuppCd], [U_SuppNm], [U_UOM], [U_Quantity], [U_ItmCost], [U_ItmChrg], [U_ItmCostVat], [U_ItmChrgVat], [U_ItmTCost], " & _
        '        " [U_ItmTChrg], [U_CostVatGrp], [U_ChrgVatGrp], [U_SONr], [U_PONr], [U_SINVNr], [U_PINVNr], [U_FPCost], [U_FPChrg], " & _
        '        " [U_EmpId], [U_EmpNm] from [@IDH_WOADDEXP] where U_RowNr = '" + sParentRowCode + "'" & _
        '        " AND [U_ItemCd] NOT IN (" & _



        '        "'" + Config.Parameter("FUELSITM") + "', " & _
        '        "'" + Config.Parameter("CNTYCITM") + "', " & _
        '        "'" + Config.Parameter("PORTCITM") + "') "

        '        oRecordSet = oParent.goDB.doSelectQuery(sQry)

        '        If oRecordSet.RecordCount > 0 Then
        '            For cntInsert As Integer = 0 To oRecordSet.RecordCount - 1

        '                Dim sQty As String = oRecordSet.Fields.Item(11).Value.ToString()
        '                Dim sItmCost As String = oRecordSet.Fields.Item(12).Value.ToString()
        '                Dim sItmChrg As String = oRecordSet.Fields.Item(13).Value.ToString()

        '                Dim sItmCostVAT As String = oRecordSet.Fields.Item(14).Value.ToString()
        '                Dim sItmChrgVAT As String = oRecordSet.Fields.Item(15).Value.ToString()

        '                Dim sItmTCost As String = oRecordSet.Fields.Item(16).Value.ToString()
        '                Dim sItmTChrg As String = oRecordSet.Fields.Item(17).Value.ToString()

        '                If bCopyQty = False Then






        '                    sQty = "0"
        '                    sItmCostVAT = "0"
        '                    sItmChrgVAT = "0"
        '                    sItmTCost = "0"
        '                    sItmTChrg = "0"
        '                End If

        '                If bCopyPrice = False Then







        '                    sItmCost = "0"
        '                    sItmChrg = "0"
        '                    sItmCostVAT = "0"
        '                    sItmChrgVAT = "0"
        '                    sItmTCost = "0"
        '                    sItmTChrg = "0"
        '                End If

        '                Dim iNewSeqCode As Integer = oParent.goDB.doNextNumber("ADDEXPKEY")
        '                Dim oInsertRS As SAPbobsCOM.Recordset = Nothing
        '                sInsertQry = String.Empty

        '                sInsertQry = sInsertQry + " INSERT INTO [@IDH_WOADDEXP]([Code],[Name], [U_JobNr], [U_RowNr], [U_ItemCd], [U_ItemDsc], [U_CustCd], [U_CustNm], " '0- 7
        '                sInsertQry = sInsertQry + " [U_SuppCd], [U_SuppNm], [U_UOM], [U_Quantity], [U_ItmCost], [U_ItmChrg], [U_ItmCostVat], [U_ItmChrgVat], [U_ItmTCost], " '8 - 16
        '                'sInsertQry = sInsertQry + " [U_ItmTChrg], [U_CostVatGrp], [U_ChrgVatGrp], [U_SONr], [U_PONr], [U_SINVNr], [U_PINVNr], [U_FPCost], [U_FPChrg], "
        '                sInsertQry = sInsertQry + " [U_ItmTChrg], [U_CostVatGrp], [U_ChrgVatGrp], [U_FPCost], [U_FPChrg], "
        '                sInsertQry = sInsertQry + " [U_EmpId], [U_EmpNm])VALUES( "

        '                sInsertQry = sInsertQry + "'" + iNewSeqCode.ToString() + "', "
        '                sInsertQry = sInsertQry + "'" + iNewSeqCode.ToString() + "', "
        '                'sInsertQry = sInsertQry + "'" + sNowDate + "', "
        '                sInsertQry = sInsertQry + "'" + oRecordSet.Fields.Item(2).Value.ToString() + "', "
        '                sInsertQry = sInsertQry + "'" + sNewRowCode + "', "
        '                sInsertQry = sInsertQry + "'" + oRecordSet.Fields.Item(4).Value.ToString() + "', "
        '                sInsertQry = sInsertQry + "'" + oRecordSet.Fields.Item(5).Value.ToString() + "', "
        '                sInsertQry = sInsertQry + "'" + oRecordSet.Fields.Item(6).Value.ToString() + "', "
        '                sInsertQry = sInsertQry + "'" + oRecordSet.Fields.Item(7).Value.ToString() + "', "
        '                sInsertQry = sInsertQry + "'" + oRecordSet.Fields.Item(8).Value.ToString() + "', "
        '                sInsertQry = sInsertQry + "'" + oRecordSet.Fields.Item(9).Value.ToString() + "', "
        '                sInsertQry = sInsertQry + "'" + oRecordSet.Fields.Item(10).Value.ToString() + "', "

        '                sInsertQry = sInsertQry + "" + sQty + ", "
        '                sInsertQry = sInsertQry + "" + sItmCost + ", "
        '                sInsertQry = sInsertQry + "" + sItmChrg + ", "
        '                sInsertQry = sInsertQry + "" + sItmCostVAT + ", "
        '                sInsertQry = sInsertQry + "" + sItmChrgVAT + ", "
        '                sInsertQry = sInsertQry + "" + sItmTCost + ", "
        '                sInsertQry = sInsertQry + "" + sItmTChrg + ", "

        '                sInsertQry = sInsertQry + "'" + oRecordSet.Fields.Item(18).Value.ToString() + "', "
        '                sInsertQry = sInsertQry + "'" + oRecordSet.Fields.Item(19).Value.ToString() + "', "

        '                'sInsertQry = sInsertQry + "'" + oRecordSet.Fields.Item(20).Value.ToString() + "', "
        '                'sInsertQry = sInsertQry + "'" + oRecordSet.Fields.Item(21).Value.ToString() + "', "
        '                'sInsertQry = sInsertQry + "'" + oRecordSet.Fields.Item(22).Value.ToString() + "', "
        '                'sInsertQry = sInsertQry + "'" + oRecordSet.Fields.Item(23).Value.ToString() + "', "

        '                sInsertQry = sInsertQry + "'" + oRecordSet.Fields.Item(24).Value.ToString() + "', "
        '                sInsertQry = sInsertQry + "'" + oRecordSet.Fields.Item(25).Value.ToString() + "', "
        '                sInsertQry = sInsertQry + "'" + oRecordSet.Fields.Item(26).Value.ToString() + "', "
        '                sInsertQry = sInsertQry + "'" + oRecordSet.Fields.Item(27).Value.ToString() + "' ) "

        '                'oInsertRS = oParent.goDB.doSelectQuery(sInsertQry)
        '                oParent.goDB.doUpdateQuery(sInsertQry)
        '                oRecordSet.MoveNext()

        '                IDHAddOns.idh.data.Base.doReleaseObject(oInsertRS)


        '            Next
        '        End If
        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error duplicating the Additional Items.")
        '    Finally
        '        IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)

        '    End Try
        'End Function

        'We don't do prebooks like this anymore
        'Public Shared Sub doCreateRecuringJobs(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sRowCode As String, ByVal sCardCode As String, ByVal sScheduleData As String)
        '    If Not sScheduleData Is Nothing AndAl so sScheduleData.Length > 0 AndAlso _
        '        Not sScheduleData.Chars(0) = "_" AndAlso _
        '        sScheduleData.StartsWith("Created From:") = False AndAlso _
        '        sScheduleData.StartsWith("PBI-") = False Then
        '        Dim oDates As com.idh.bridge.utils.Dates.ScheduleDates = com.idh.bridge.utils.Dates.doCreateScheduleDates(sCardCode, sScheduleData, DateTime.MinValue)
        '        If oDates.Count > 0 Then
        '            doPreBookRows(oParent, sRowCode, oDates)


        '        End If
        '    End If
        'End Sub

        ''** Do the PreBook outside of the Grid
        'Public Shared Function doPreBookRows(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sRowCode As String, ByVal oDates As com.idh.bridge.utils.Dates.ScheduleDates, Optional ByVal sStartDate As String = Nothing, Optional ByVal sEndDate As String = Nothing) As Boolean
        '    If (oDates Is Nothing OrElse oDates.Count = 0) AndAlso sStartDate Is Nothing AndAlso sEndDate Is Nothing Then
        '        Return True

        '    End If

        '    Dim oTableObj As New IDHAddOns.idh.data.AuditObject(oParent, msMainTable)
        '    Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(oParent, msMainTable)
        '    Dim sNewCode As String = Nothing
        '    Dim oWrkDate As Date

        '    Try
        '        'Step through the selected rows and update
        '        Dim iwResult As Integer
        '        Dim swResult As String = Nothing
        '        Dim iRepeat As Integer

        '        'Update the Base Row
        '        If oTableObj.setKeyPair(sRowCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) Then
        '            oTableObj.setName(sRowCode)

        '            Dim sCoverage As String = oTableObj.getFieldValue("U_Covera")
        '            If sCoverage.StartsWith("PBI-") = True Then


        '                Return True
        '            End If




        '            Dim sCardCode As String = oTableObj.getFieldValue(IDH_JOBSHD._CustCd)









        '            While sCoverage.Chars(0) = "_"
        '                sCoverage = sCoverage.Substring(1)
        '            End While



        '            If oDates Is Nothing Then
        '                sCoverage = sCoverage.Substring(0, 11)
        '                sCoverage = sCoverage & sStartDate & sEndDate
        '                oDates = com.idh.bridge.utils.Dates.doCreateScheduleDates(sCardCode, sCoverage, DateTime.MinValue)
        '                If oDates Is Nothing OrElse oDates.Count = 0 Then
        '                    Return True
        '                End If
        '            End If


        '            Dim sCoverHist As String = oTableObj.getFieldValue("U_CoverHst")
        '            sCoverHist = sCoverage & "|" & sCoverHist



        '            sCoverage = "_" & sCoverage






        '            oTableObj.setFieldValue("U_Covera", sCoverage)
        '            oTableObj.setFieldValue("U_CoverHst", sCoverHist)


        '            iwResult = oTableObj.Update()
        '            If iwResult <> 0 Then
        '                oParent.goDICompany.GetLastError(iwResult, swResult)
        '                com.idh.bridge.DataHandler.INSTANCE.doError("Error Updating the Base: " + swResult)
        '            End If











        '            oTableObj.doStopLocalAudit()

        '            '**
        '            ' RESET THE VALUES
        '            '**
        '            oTableObj.setFieldValue("U_Status", com.idh.bridge.lookups.FixedValues.getStatusOpen())
        '            oTableObj.setFieldValue("U_PStat", com.idh.bridge.lookups.FixedValues.getStatusOpen())
        '            oTableObj.setFieldValue("U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusUnPaid())
        '            oTableObj.setFieldValue("U_PayMeth", "")
        '            oTableObj.setFieldValue("U_CCNum", "")
        '            oTableObj.setFieldValue("U_CCType", "0")
        '            oTableObj.setFieldValue("U_CCStat", "")


        '            oTableObj.setFieldValue("U_SAORD", "")
        '            oTableObj.setFieldValue("U_TIPPO", "")
        '            oTableObj.setFieldValue("U_JOBPO", "")
        '            oTableObj.setFieldValue("U_ProPO", "")
        '            oTableObj.setFieldValue("U_ProGRPO", "")

        '            oTableObj.setFieldValue("U_SODlvNot", "")












        '            If Config.INSTANCE.getParameterAsBool("USAREL", False) Then
        '                oTableObj.setFieldValue("U_Jrnl", "")
        '                oTableObj.setFieldValue("U_JStat", "")
        '            End If

        '            oTableObj.setFieldValue("U_GRIn", "")
        '            oTableObj.setFieldValue("U_SAINV", "")
        '            oTableObj.setFieldValue("U_SLPO", "")
        '            oTableObj.setFieldValue("U_TPCN", "")
        '            oTableObj.setFieldValue("U_TCCN", "")
        '            oTableObj.setFieldValue("U_Covera", "")
        '            oTableObj.setFieldValue("U_CoverHst", "")
        '            oTableObj.setFieldValue("U_ASDate", "")
        '            oTableObj.setFieldValue("U_ASTime", 0)
        '            oTableObj.setFieldValue("U_AEDate", "")
        '            oTableObj.setFieldValue("U_AETime", 0)
        '            oTableObj.setFieldValue("U_WROrd", "")
        '            oTableObj.setFieldValue("U_WRRow", "")






        '            oTableObj.setFieldValue("U_RDate", "")

        '            oTableObj.setFieldValue("U_Wei1", 0)
        '            oTableObj.setFieldValue("U_Wei2", 0)
        '            oTableObj.setFieldValue("U_Ser1", "")
        '            oTableObj.setFieldValue("U_Ser2", "")
        '            oTableObj.setFieldValue("U_WDt1", "")
        '            oTableObj.setFieldValue("U_WDt2", "")


        '            oTableObj.setFieldValue(IDH_JOBSHD._TRdWgt, 0)
        '            oTableObj.setFieldValue("U_TipWgt", 0)
        '            oTableObj.setFieldValue("U_TipCost", 0)
        '            oTableObj.setFieldValue("U_TipTot", 0)
        '            oTableObj.setFieldValue("U_SLicCTo", 0)

        '            oTableObj.setFieldValue("U_UseWgt", "1")
        '            oTableObj.setFieldValue("U_AUOMQt", 0)
        '            oTableObj.setFieldValue("U_RdWgt", 0)
        '            oTableObj.setFieldValue("U_CstWgt", 0)
        '            oTableObj.setFieldValue("U_TCharge", 0)
        '            oTableObj.setFieldValue("U_TCTotal", 0)

        '            oAuditObj.setFieldValue("U_TAUOMQt", 0)
        '            oAuditObj.setFieldValue("U_PAUOMQt", 0)
        '            oAuditObj.setFieldValue("U_PRdWgt", 0)
        '            oAuditObj.setFieldValue("U_ProWgt", 0)
        '            oAuditObj.setFieldValue("U_PCost", 0)
        '            oAuditObj.setFieldValue("U_PCTotal", 0)

        '            oTableObj.setFieldValue("U_Discnt", 0)
        '            oTableObj.setFieldValue("U_DisAmt", 0)
        '            oTableObj.setFieldValue("U_SLicCh", 0)
        '            oTableObj.setFieldValue("U_CongCh", 0)
        '            oTableObj.setFieldValue("U_Total", 0)

        '            oTableObj.setFieldValue("U_JobRmD", "")
        '            oTableObj.setFieldValue("U_JobRmT", 0)
        '            oTableObj.setFieldValue("U_RemNot", "")
        '            oTableObj.setFieldValue("U_RemCnt", 0)



        '            oTableObj.setFieldValue("U_TaxAmt", 0)
        '            oTableObj.setFieldValue("U_VtCostAmt", 0)

        '            oTableObj.setFieldValue("U_TChrgVtRt", 0)
        '            oTableObj.setFieldValue("U_HChrgVtRt", 0)
        '            oTableObj.setFieldValue("U_TCostVtRt", 0)
        '            oTableObj.setFieldValue("U_HCostVtRt", 0)



        '            oTableObj.setFieldValue("U_TChrgVtGrp", "")
        '            oTableObj.setFieldValue("U_HChrgVtGrp", "")
        '            oTableObj.setFieldValue("U_TCostVtGrp", "")
        '            oTableObj.setFieldValue("U_HCostVtGrp", "")

        '            Dim sNowDate As String = oParent.doDateToStr()
        '            Dim sNowTime As String = com.idh.utils.dates.doTimeToNumStr()
        '            oTableObj.setFieldValue("U_BDate", sNowDate)
        '            oTableObj.setFieldValue("U_BTime", sNowTime)


        '            oTableObj.setFieldValue("U_User", oParent.gsUserName)



        '            oTableObj.setFieldValue("U_Comment", "") 'Created From: " & sSource)



        '            oTableObj.setFieldValue("U_IntComm", "")

        '            Dim sSource As String = oTableObj.getFieldValue("U_JobNr") & "." & sRowCode
        '            If sCoverage.Chars(0) = "_" Then
        '                oTableObj.setFieldValue("U_Covera", sCoverage & "-" & sSource)
        '            Else
        '                oTableObj.setFieldValue("U_Covera", "_" & sCoverage & "-" & sSource)
        '            End If




        '            oTableObj.setFieldValue("U_CoverHst", "")


        '            'Now create all the new rows
        '            Dim iCode As Integer = oParent.goDB.doNextNumber("JOBSCHE", oDates.Count())
        '            For iRepeat = 0 To oDates.Count() - 1



        '                sNewCode = com.idh.utils.Conversions.ToString(iCode)


        '                oAuditObj.setKeyPair(sNewCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
        '                oAuditObj.setName(sNewCode)



        '                oWrkDate = oDates.Item(iRepeat)





        '                'Dim sRDate As String = oParent.doDateToStr(oWrkDate)
        '                'oAuditObj.setFieldValue("U_RDate", sRDate)





        '                If iRepeat = 0 Then
        '                    doGetCosts(oParent, oTableObj, oTableObj)
        '                    doGetCharges(oParent, oTableObj, oTableObj)
        '                End If
        '                oTableObj.setFieldValue("U_RDate", oWrkDate)










        '                Dim iFields As Integer = oTableObj.getFieldsCount()
        '                For iCol As Integer = 0 To iFields - 1
        '                    oAuditObj.setFieldValue(iCol, oTableObj.getFieldValue(iCol))
        '                Next

        '                iwResult = oAuditObj.Commit()
        '                If iwResult <> 0 Then
        '                    oParent.goDICompany.GetLastError(iwResult, swResult)
        '                    com.idh.bridge.DataHandler.INSTANCE.doError("Error Creating the PreBook: " + swResult)
        '                    sNewCode = Nothing
        '                End If
        '                iCode = iCode + 1
        '            Next
        '        End If

        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing the PreBook rows.")
        '        sNewCode = False
        '    Finally
        '        IDHAddOns.idh.data.Base.doReleaseObject(oTableObj)
        '        IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj)

        '    End Try
        '    Return True
        'End Function

        Public Sub doRefreshAdditionalPrices(ByVal oForm As SAPbouiCOM.Form)
            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)

            Dim sCardCode As String = oWOR.F_CustomerCode
            Dim sItemCode As String = oWOR.U_ItemCd
            Dim sItemGrp As String = oWOR.U_ItmGrp
            Dim sWastCd As String = oWOR.U_WasCd
            Dim sZipCode As String = oWOR.F_CustomerZipCode
            Dim sAddress As String = oWOR.F_CustomerAddress
            Dim sBranch As String = oWOR.U_Branch
            Dim sJobType As String = oWOR.U_JobTp

            '20150308 - LPV - BEGIN - avoid double conversion
            'Dim sDocDate As String = oWOR.U_BDate
            'Dim dDocDate As Date = com.idh.utils.Dates.doStrToDate(sDocDate)
            Dim dDocDate As Date = oWOR.U_BDate
            '20150308 - LPV - END - avoid double conversion

            Dim sUOM As String = oWOR.U_UOM
            Dim dWeight As Double = oWOR.U_CstWgt

            Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            If Not oGrid Is Nothing Then
                oGrid.doCalculateLinesChargePrices(sCardCode, sItemCode, sItemGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dWeight, sUOM, sZipCode)
                oGrid.doCalculateLinesCostPrices(sCardCode, sItemCode, sItemGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dWeight, sUOM, sZipCode)
            End If
        End Sub

        Public Sub doCalcAdditionalTotals(ByVal oForm As SAPbouiCOM.Form)
            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)
            Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            '## MA 07-04-2014 Start
            Dim dCost As Double = oGrid.doCalculateCostTotalsExcVAT()
            Dim dChrg As Double = oGrid.doCalculateChargeTotalsExcVAT()



            Dim dCostVat As Double = oGrid.doCalculateCostVAT()
            Dim dChrgVat As Double = oGrid.doCalculateChargeVAT()

            oWOR.U_AddCost = dCost
            oWOR.U_TAddCsVat = dCostVat
            'LP Viljoen - not needed as the above will auto calculate this value - oWOR.U_TAddCost = dCost + dCostVat

            oWOR.U_AddCharge = dChrg
            oWOR.U_TAddChVat = dChrgVat
            'LP Viljoen - not needed as the above will auto calculate this value - oWOR.U_TAddChrg = dChrg + dChrgVat



            '## MA 07-04-2014 End
            'oWOR.U_TAddCost = dCostT
            'oWOR.U_TAddChrg = dChrgT

            'LP Viljoen - not needed as the above will auto calculate this value - oWOR.doCalculateTotals()
        End Sub

        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)

            If MyBase.doCustomItemEvent(oForm, pVal) = False Then
                Return False
            End If
            If pVal.ItemUID = "ADDGRID" Then
                Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SEARCH_VALUESET Then
                    If pVal.BeforeAction = False Then
                        If pVal.ColUID = IDH_WOADDEXP._ItemCd OrElse pVal.ColUID = IDH_WOADDEXP._SuppCd Then
                            Dim iRow As Integer = pVal.Row
                            Dim sCardCode As String = getFormDFValue(oForm, "U_CustCd")
                            Dim sItemCode As String = getFormDFValue(oForm, "U_ItemCd")
                            Dim sItemGrp As String = getFormDFValue(oForm, "U_ItmGrp")
                            Dim sWastCd As String = getFormDFValue(oForm, "U_WasCd")
                            Dim sZipCode As String = getWFValue(oForm, "ZIPCODE")
                            Dim sAddress As String = getWFValue(oForm, "STADDR")
                            Dim sBranch As String = getFormDFValue(oForm, "U_Branch")
                            Dim sJobType As String = getFormDFValue(oForm, "U_JobTp")

                            Dim sDocDate As String = getFormDFValue(oForm, "U_RDate")
                            Dim dDocDate As Date = com.idh.utils.Dates.doStrToDate(sDocDate)

                            Dim sUOM As String = oWOR.U_UOM
                            Dim dWeight As Double = oWOR.U_CstWgt

                            'sUOM = getFormDFValue(oForm, "U_UOM")
                            'dWeight = getFormDFValue(oForm, "U_CstWgt")

                            oGrid.doCalculateLineChargePrices(sCardCode, sItemCode, sItemGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dWeight, sUOM, iRow, sZipCode)
                            oGrid.doCalculateLineCostPrices(sCardCode, sItemCode, sItemGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dWeight, sUOM, iRow, sZipCode)
                        End If
                    End If
                ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED Then
                    If pVal.BeforeAction = False Then
                        If pVal.ColUID = "U_CostVatGrp" OrElse
                           pVal.ColUID = "U_ItmCost" Then
                            oGrid.doCalculateCostLineTotals(pVal.Row)
                        ElseIf pVal.ColUID = "U_ChrgVatGrp" OrElse
                            pVal.ColUID = "U_ItmChrg" Then
                            oGrid.doCalculateChargeLineTotals(pVal.Row)
                        ElseIf pVal.ColUID = "U_Quantity" Then
                            'oGrid.doCalculateLineTotals(pVal.Row)

                            Dim sSupplier As String = oGrid.doGetFieldValue("U_SuppCd")
                            'If String.IsNullOrWhiteSpace(sSupplier) OrElse sSupplier = "*" Then
                            'Return True
                            'End If

                            Dim iRow As Integer = pVal.Row
                            Dim sCardCode As String = getFormDFValue(oForm, "U_CustCd")
                            Dim sItemCode As String = getFormDFValue(oForm, "U_ItemCd")
                            Dim sItemGrp As String = getFormDFValue(oForm, "U_ItmGrp")
                            Dim sWastCd As String = getFormDFValue(oForm, "U_WasCd")
                            Dim sZipCode As String = getWFValue(oForm, "ZIPCODE")
                            Dim sAddress As String = getWFValue(oForm, "STADDR")
                            Dim sBranch As String = getFormDFValue(oForm, "U_Branch")
                            Dim sJobType As String = getFormDFValue(oForm, "U_JobTp")

                            Dim sDocDate As String = getFormDFValue(oForm, "U_RDate")
                            Dim dDocDate As Date = com.idh.utils.Dates.doStrToDate(sDocDate)

                            Dim sUOM As String = oWOR.U_UOM 'oGrid.doGetFieldValue(IDH_WOADDEXP._UOM, iRow)
                            Dim dWeight As Double = oGrid.doGetFieldValue(IDH_WOADDEXP._Quantity, iRow) 'oWOR.U_CstWgt

                            oGrid.doCalculateLineChargePrices(sCardCode, sItemCode, sItemGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dWeight, sUOM, iRow, sZipCode)

                            If String.IsNullOrWhiteSpace(sSupplier) = False AndAlso sSupplier <> "*" Then
                                oGrid.doCalculateLineCostPrices(sCardCode, sItemCode, sItemGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dWeight, sUOM, iRow, sZipCode)
                            Else
                                oGrid.doCalculateCostLineTotals(iRow)
                            End If

                        ElseIf pVal.ColUID = "U_ItmTCost" Then
                            ''## MA Start 08-04-2014
                            Dim dCostTWOVAT As Double = oGrid.doCalculateCostTotalsExcVAT()
                            Dim dCostVAT As Double = oGrid.doCalculateCostVAT()
                            If oWOR.U_Rebate.ToLower = "y" Then

                                Dim dChgTWOVAT As Double = dCostTWOVAT + oGrid.doCalculateChargeTotalsExcVAT()
                                setUFValue(oForm, "IDH_ADDCH", dChgTWOVAT)
                                Dim dChargeVAT As Double = dCostVAT + oGrid.doCalculateChargeVAT()
                                setUFValue(oForm, "IDH_ADDCHV", dChargeVAT)


                                setUFValue(oForm, "IDH_TADDCH", dChargeVAT + dChgTWOVAT)
                                oWOR.U_AddCharge = dChgTWOVAT
                                oWOR.U_TAddChVat = dChargeVAT

                                setUFValue(oForm, "IDH_ADDCO", 0)
                                setUFValue(oForm, "IDH_ADDCOV", 0)
                                oWOR.U_AddCost = 0
                                oWOR.U_TAddCsVat = 0
                            Else
                                setUFValue(oForm, "IDH_ADDCO", dCostTWOVAT)
                                setUFValue(oForm, "IDH_ADDCOV", dCostVAT)


                                setUFValue(oForm, "IDH_TADDCO", dCostTWOVAT + dCostVAT)

                                oWOR.U_AddCost = dCostTWOVAT
                                oWOR.U_TAddCsVat = dCostVAT
                            End If


                            'LP Viljoen - This is not needed as it will auto calculate if the above is set
                            'oWOR.U_TAddCost = dCostTWOVAT + dCostVAT
                            ''Dim dCostT As Double = oGrid.doCalculateCostTotals()
                            ''setUFValue(oForm, "IDH_ADDCO", dCostT)
                            ''oWOR.U_TAddCost = dCostT

                            'oWOR.doCalculateCostTotals()
                            ''## MA End 08-04-2014
                        ElseIf pVal.ColUID = "U_ItmTChrg" Then
                            ''## MA Start 08-04-2014
                            Dim dChgTWOVAT As Double = oGrid.doCalculateChargeTotalsExcVAT()
                            Dim dChargeVAT As Double = oGrid.doCalculateChargeVAT()


                            If oWOR.U_Rebate.ToLower = "y" Then

                                setUFValue(oForm, "IDH_ADDCO", 0)
                                setUFValue(oForm, "IDH_ADDCOV", 0)

                                Dim dCostTWOVAT As Double = oGrid.doCalculateCostTotalsExcVAT()
                                Dim dCostVAT As Double = oGrid.doCalculateCostVAT()

                                dChgTWOVAT += dCostTWOVAT
                                dChargeVAT += dChargeVAT

                                setUFValue(oForm, "IDH_ADDCO", 0)
                                setUFValue(oForm, "IDH_ADDCOV", 0)
                                oWOR.U_AddCost = 0
                                oWOR.U_TAddCsVat = 0
                            End If
                            setUFValue(oForm, "IDH_ADDCH", dChgTWOVAT)
                            setUFValue(oForm, "IDH_ADDCHV", dChargeVAT)
                            setUFValue(oForm, "IDH_TADDCH", dChargeVAT + dChgTWOVAT)
                            oWOR.U_AddCharge = dChgTWOVAT
                            oWOR.U_TAddChVat = dChargeVAT


                            'LP Viljoen - This is not needed as it will auto calculate if the above is set
                            'oWOR.U_TAddChrg = dChargeVAT + dChgTWOVAT
                            ''Dim dChrgT As Double = oGrid.doCalculateChargeTotals()
                            ''setUFValue(oForm, "IDH_ADDCH", dChrgT)

                            ''oWOR.U_TAddChrg = dChrgT
                            ''incorrect Code must stick to the new Core flow (LP Viljoen) - oWOR.doCalculateChargeTotal(True)
                            'oWOR.doCalculateChargeVat("R")
                            '## MA 08-04-2014 End
                            '' ElseIf pVal.ColUID = "U_UOM" OrElse pVal.ColUID = "U_SuppCd" Then
                        ElseIf pVal.ColUID = "U_UOM" Then
                            Dim sSupplier As String = oGrid.doGetFieldValue("U_SuppCd")
                            'If sSupplier Is Nothing OrElse sSupplier.Length = 0 OrElse sSupplier = "*" Then
                            'Return True
                            ' End If

                            Dim iRow As Integer = pVal.Row
                            Dim sCardCode As String = getFormDFValue(oForm, "U_CustCd")
                            Dim sItemCode As String = getFormDFValue(oForm, "U_ItemCd")
                            Dim sItemGrp As String = getFormDFValue(oForm, "U_ItmGrp")
                            Dim sWastCd As String = getFormDFValue(oForm, "U_WasCd")
                            Dim sZipCode As String = getWFValue(oForm, "ZIPCODE")
                            Dim sAddress As String = getWFValue(oForm, "STADDR")
                            Dim sBranch As String = getFormDFValue(oForm, "U_Branch")
                            Dim sJobType As String = getFormDFValue(oForm, "U_JobTp")

                            Dim sDocDate As String = getFormDFValue(oForm, "U_RDate")
                            Dim dDocDate As Date = com.idh.utils.Dates.doStrToDate(sDocDate)

                            Dim sUOM As String = oWOR.U_UOM
                            Dim dWeight As Double = oWOR.U_CstWgt

                            'sUOM = getFormDFValue(oForm, "U_UOM")
                            'dWeight = getFormDFValue(oForm, "U_CstWgt")

                            oGrid.doCalculateLineChargePrices(sCardCode, sItemCode, sItemGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dWeight, sUOM, iRow, sZipCode)
                            oGrid.doCalculateLineCostPrices(sCardCode, sItemCode, sItemGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dWeight, sUOM, iRow, sZipCode)
                        End If
                    End If
                ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY Then
                    'If pVal.BeforeAction = False Then
                    Dim iRow As Integer = pVal.Row
                    Dim sCode As String = getFormDFValue(oForm, "U_JobNr")
                    Dim sRowID As String = getFormDFValue(oForm, "Code")
                    Dim sCustCd As String = getFormDFValue(oForm, "U_CustCd")
                    Dim sCustNm As String = getFormDFValue(oForm, "U_CustNm")

                    oGrid.doSetFieldValue("U_JobNr", iRow, sCode)
                    oGrid.doSetFieldValue("U_RowNr", iRow, sRowID)
                    oGrid.doSetFieldValue("U_CustCd", iRow, sCustCd)
                    oGrid.doSetFieldValue("U_CustNm", iRow, sCustNm)

                    oGrid.doSetFieldValue("U_UOM", iRow, "ea")
                    oGrid.doSetFieldValue("U_Quantity", iRow, 1)
                    oGrid.doSetFieldValue("U_ItmCost", iRow, 0)
                    oGrid.doSetFieldValue("U_ItmChrg", iRow, 0)
                    oGrid.doSetFieldValue("U_ItmCostVat", iRow, 0)
                    oGrid.doSetFieldValue("U_ItmChrgVat", iRow, 0)
                    oGrid.doSetFieldValue("U_TAddCost", iRow, 0)
                    oGrid.doSetFieldValue("U_TAddChrg", iRow, 0)

                    'oGrid.doSetFieldValue("U_CostVatGrp", iRow, "O1")
                    'oGrid.doSetFieldValue("U_ChrgVatGrp", iRow, "O1")
                    'End If
                    doSetUpdate(oForm)
                ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
                    If pVal.BeforeAction = False Then
                        If pVal.oData.MenuUID.Equals("DUPLICATE") Then
                            doCalcAdditionalTotals(oForm)
                            setUFValue(oForm, "IDH_ADDCO", oWOR.U_AddCost)
                            setUFValue(oForm, "IDH_ADDCH", oWOR.U_AddCharge)
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                        ElseIf pVal.oData.MenuUID.Equals("DELADDITM") Then
                            doCalcAdditionalTotals(oForm)
                            setUFValue(oForm, "IDH_ADDCO", oWOR.U_AddCost)
                            setUFValue(oForm, "IDH_ADDCH", oWOR.U_AddCharge)
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                        End If
                    End If
                End If
            End If

            Return True
        End Function

        Private Sub doFillJobTypeCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDH_JOBTPE")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = oItem.Specific
            doClearValidValues(oCombo.ValidValues)

            Dim sItemGrp As String = getFormDFValue(oForm, "U_ItmGrp")
            Dim sLastJobType As String
            Dim sJobNumber As String = getFormDFValue(oForm, "U_JobNr")
            Dim dQty As Double = 0

            Dim oJobs As JobEntries = getWFValue(oForm, "JOBENTRIES")
            Dim oJobTypes As ArrayList
            Dim oJobSeq As IDH_JOBTYPE_SEQ = IDH_JOBTYPE_SEQ.getInstance()
            If oJobs Is Nothing Then
                sLastJobType = getFormDFValue(oForm, "U_JobTp")
                oJobTypes = IDH_JOBTYPE_SEQ.getAvailJobs(
                 Config.CAT_WO,
                 sItemGrp,
                 sLastJobType)
            Else
                sItemGrp = getFormDFValue(oForm, "U_ItmGrp")
                sLastJobType = oJobs.LastJobType
                sJobNumber = getFormDFValue(oForm, "U_JobNr")
                dQty = oJobs.getOnSiteQty(sJobNumber)

                If getWFValue(oForm, "ISNEW") = True Then
                    oJobTypes = IDH_JOBTYPE_SEQ.getNextAvailJobs(
                     Config.CAT_WO,
                     sItemGrp,
                     sLastJobType,
                     dQty)
                Else
                    oJobTypes = IDH_JOBTYPE_SEQ.getAvailJobs(
                     Config.CAT_WO,
                     sItemGrp,
                     sLastJobType)
                End If
            End If
            If oJobTypes.Count > 0 Then
                Dim iIndex As Integer
                For iIndex = 0 To oJobTypes.Count() - 1
                    Try
                        oJobSeq.gotoRow(oJobTypes.Item(iIndex))
                        oCombo.ValidValues.Add(oJobSeq.U_JobTp, oJobSeq.U_JobTp)
                    Catch
                    End Try
                Next
            End If
        End Sub

        Private Sub doFillObligatedCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDH_OBLED")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = oItem.Specific
            doClearValidValues(oCombo.ValidValues)

            Dim sWord As String

            sWord = getTranslatedWord("None")
            oCombo.ValidValues.Add("", sWord)


            sWord = Config.Parameter("OBTRUE")
            If sWord Is Nothing OrElse sWord.Length = 0 Then
                sWord = getTranslatedWord("Obligated")
            End If
            oCombo.ValidValues.Add(sWord, sWord)


            sWord = Config.Parameter("OBFALSE")
            If sWord Is Nothing OrElse sWord.Length = 0 Then
                sWord = getTranslatedWord("Non-Obligated")
            End If
            oCombo.ValidValues.Add(sWord, sWord)
        End Sub

        Private Sub doFillAlternateItemDescription(ByVal oForm As SAPbouiCOM.Form, Optional ByVal sAltWasDsc As String = "", Optional ByVal sClearList As Boolean = False)
            If Config.ParamaterWithDefault("FLALTITM", True) = "FALSE" Then
                oForm.Items.Item("401").Visible = False 'Label control 
                oForm.Items.Item("IDH_ALTITM").Visible = False 'Combobox control 
                Exit Sub
            End If

            Dim sItemCode As String = getFormDFValue(oForm, "U_WasCd")
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDH_ALTITM")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = oItem.Specific

            If Not sClearList Then
                doClearValidValues(oCombo.ValidValues)
                doFillCombo(oForm, oCombo, "[@IDH_ITMDESC]", "Code", "U_ItemAltName", "U_ItemCode = '" + sItemCode + "'", Nothing, "", "Select Alternative ItmDescrp.")
                If sAltWasDsc.Length > 0 Then
                    oCombo.Select(sAltWasDsc, SAPbouiCOM.BoSearchKey.psk_ByValue)
                Else
                    oCombo.Select("", SAPbouiCOM.BoSearchKey.psk_ByValue)
                End If
            Else
                doClearValidValues(oCombo.ValidValues)
                oCombo.ValidValues.Add("", "Select Alternative ItmDescrp.")
                oCombo.Select("", SAPbouiCOM.BoSearchKey.psk_ByValue)
            End If
        End Sub

        'OnTime 22323: UOM validation
        'Note: validate UOM right in the begining of the price calculation
        Private Function isValidUOM(ByVal oForm As SAPbouiCOM.Form, ByRef strUOM As String) As Boolean
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Dim bolValidateUOM As Boolean = True

            Try
                oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                oRecordSet.DoQuery("select UnitCode from OWGT")
                If oRecordSet.RecordCount > 0 Then
                    Dim oWeightMeasures As SAPbobsCOM.WeightMeasures = Nothing
                    oWeightMeasures = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oWeightMeasures)
                    oWeightMeasures.Browser.Recordset = oRecordSet
                    While Not oWeightMeasures.Browser.EoF
                        If strUOM.ToUpper() = oWeightMeasures.UnitDisplay.ToUpper() Then
                            bolValidateUOM = True
                            Exit While
                        Else
                            'com.idh.bridge.DataHandler.INSTANCE.doError("Invalid UOM! Please provide a valid UOM.")
                            bolValidateUOM = False
                        End If
                        oWeightMeasures.Browser.MoveNext()
                    End While
                    Return bolValidateUOM
                End If
                Return bolValidateUOM
            Catch ex As Exception
            Finally
                oRecordSet = Nothing
            End Try
            Return bolValidateUOM
        End Function

        'OnTime Ticket 25926 - DOC Report crahses application
        Public Function doCallHTTPRpts(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal oForm As SAPbouiCOM.Form, ByVal sReport As String, ByVal sDoShow As String, ByVal sDoPrint As String, ByVal sCopies As String, ByRef oParams As Hashtable, ByRef sFromForm As String, ByRef sBranch As String) As Boolean
            Dim sDoPrintDialog As String = Config.Parameter("MDSWPD")
            If sDoPrintDialog Is Nothing Then
                sDoPrintDialog = "TRUE"
            Else
                sDoPrintDialog = sDoPrintDialog.ToUpper()
            End If

            Dim sDoAutoPrint As String = Config.Parameter("AUTOPRNT")
            If sDoAutoPrint Is Nothing Then
                sDoAutoPrint = "TRUE"
            Else
                sDoAutoPrint = sDoAutoPrint.ToUpper()
            End If

            'Get the printer from the DB
            'Dim oPrinterSettings As WR1_Data.idh.data.PrintSettings = Nothing
            'oPrinterSettings = Config.INSTANCE.getPrinterSetting(oParent, sReport, sFromForm, sBranch)

            Dim oPrinterSettings As com.idh.bridge.PrintSettings = Nothing
            oPrinterSettings = PrintSettings.getPrinterSetting(sReport, sFromForm, sBranch)

            Dim sPrinterName As String = Nothing
            Dim iWidth As Integer = 0
            Dim iHeight As Integer = 0
            If Not oPrinterSettings Is Nothing Then
                sPrinterName = oPrinterSettings.msPrinterName
                iWidth = oPrinterSettings.miWidth
                iHeight = oPrinterSettings.miHeight
            End If
            Return doCallHTTPRptsContainer(oParent, oForm, sReport, sDoShow, sDoAutoPrint, sDoPrintDialog, sCopies, oParams, sPrinterName, iWidth, iHeight)

        End Function

        Public Function doCallHTTPRptsContainer(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal oForm As SAPbouiCOM.Form, ByVal sReport As String, ByVal sDoShow As String, ByVal sDoPrint As String, ByVal sDoDialog As String, ByVal sCopies As String, ByRef oParams As Hashtable, ByRef sPrinterName As String, ByRef iWidth As Integer, ByRef iHeight As Integer) As Boolean
            'Get The print option
            Dim bDoPrint As Boolean = False
            If (Not sDoPrint Is Nothing) AndAlso sDoPrint.ToUpper().Equals("TRUE") Then
                bDoPrint = True
            End If

            'Get The show the report
            Dim bDoShow As Boolean = True
            If (Not sDoShow Is Nothing) AndAlso sDoShow.ToUpper().Equals("FALSE") Then
                bDoShow = False
                bDoPrint = True
            End If

            'Get The show printdialog option
            Dim bDoDialog As Boolean = True
            If (Not sDoDialog Is Nothing) AndAlso sDoDialog.ToUpper().Equals("FALSE") Then
                bDoDialog = False
            End If

            'Get The number of copies
            Dim iCopies As Integer = 1
            If Not sCopies Is Nothing Then
                Try
                    iCopies = Val(sCopies)
                Catch ex As Exception
                End Try
            End If

            setSharedData(oForm, "DOPRINT", sDoPrint)
            setSharedData(oForm, "DOSHOWPRNDIALOG", sDoDialog)
            setSharedData(oForm, "DOSHOW", sDoShow)
            setSharedData(oForm, "NUMCOPIES", sCopies)
            setSharedData(oForm, "PARAMS", oParams)
            setSharedData(oForm, "PRINTERNAME", sPrinterName)

            setSharedData(oForm, "WIDTH", iWidth)
            setSharedData(oForm, "HEIGHT", iHeight)

            If sReport.StartsWith("http://") Then
                Dim sURL As String = sReport

                If oParams.Count > 0 Then
                    Dim en As IEnumerator = oParams.Keys.GetEnumerator()
                    Dim sKey As String
                    Dim sValue As String

                    While en.MoveNext
                        sKey = en.Current().ToString()
                        If Not sKey Is Nothing Then
                            sValue = oParams.Item(sKey)
                            sURL = sURL & "&" & sKey & "=" & sValue
                        End If
                    End While
                End If

                If bDoShow = False Then
                    'Dim oHTML As idh.report.HTMLViewer
                    'oHTML = idh.report.HTMLViewer.getInstance(oParent)
                    'oHTML.doPrintSilent(sURL, bDoDialog, iCopies)
                Else
                    setSharedData(oForm, "URL", sURL)
                    oParent.doOpenModalForm("IDHHTML", oForm)
                End If
            Else
                'Dim sReportChk As String = idh.report.CrystalViewer.getReportChk(oParent, sReport)
                Dim sReportChk As String = getReportChk(oParent, sReport)
                If Not sReportChk Is Nothing Then
                    setSharedData(oForm, "CHKREPORT", sReportChk)
                    setSharedData(oForm, "REPORT", sReport)
                    setSharedData(oForm, "ZOOM", "69")
                    setSharedData(oForm, "DSPGROUPTREE", "FALSE")
                    setSharedData(oForm, "DSPTOOLBAR", "TRUE")
                    oParent.doOpenModalForm("IDHREPORT", oForm)
                Else
                    Return False
                End If
            End If
            Return True

        End Function

        Public Function getReportChk(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sReportIn As String) As String
            Dim sReport As String = Nothing
            Dim sPath As String = Config.Parameter("REPDIR")
            If Not sPath Is Nothing AndAlso sPath.Trim().Length() > 0 Then
                If sPath.EndsWith("\") = False Then
                    sPath = sPath & "\"
                End If
            Else
                'sPath = Directory.GetCurrentDirectory & "\reports\"
                sPath = IDHAddOns.idh.addon.Base.REPORTPATH
            End If

            sReport = sReportIn.Trim()
            If sReport Is Nothing OrElse sReport.Length() = 0 Then
                Return Nothing
            End If
            sReport = sPath & sReport

            'If oParent.gsAdditionalSRFPath Is Nothing Then
            If IDHAddOns.idh.addon.Base.REPORTPATH Is Nothing Then
                If File.Exists(sReport) = False Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Config: The report file could not be found - " & sReportIn, "The report file could not be found - " & sReportIn)
                    com.idh.bridge.DataHandler.INSTANCE.doResConfigError("The report file could not be found - " & sReportIn, "ERCONREP", {sReportIn})
                    Return Nothing
                Else
                    Return sReport
                End If
            Else
                If File.Exists(sReport) = False Then
                    'sPath = oParent.gsAdditionalSRFPath & "\reports\"
                    sPath = IDHAddOns.idh.addon.Base.REPORTPATH & "\"
                    sReport = sPath & sReportIn.Trim()
                    If File.Exists(sReport) = False Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Config: The report file could not be found - " & sReportIn, "The report file could not be found - " & sReportIn)
                        com.idh.bridge.DataHandler.INSTANCE.doResConfigError("The report file could not be found - " & sReportIn, "ERCONREP", {sReportIn})
                        Return Nothing
                    Else
                        Return sReport
                    End If
                Else
                    Return sReport
                End If
            End If
        End Function

        'OnTime 32355: WR1_NewDev_Buying&Trading_Add functionality to log Activity with a WOR
        Public Sub doActivityGrid(ByVal oForm As SAPbouiCOM.Form)
            'Dim owItem As SAPbouiCOM.Item
            'owItem = oForm.Items.Item("IDH_ADDGRD")

            'Dim oItem As SAPbouiCOM.Item
            'oItem = oForm.Items.Add("ACTGRID", SAPbouiCOM.BoFormItemTypes.it_GRID)
            ''set grid position and from/to panes etc
            'oItem.Left = owItem.Left
            'oItem.Top = owItem.Top
            'oItem.Width = owItem.Width
            'oItem.Height = owItem.Height

            'oItem.FromPane = 103
            'oItem.ToPane = 103

            'Dim oGrid As SAPbouiCOM.Grid
            'oGrid = oItem.Specific
            'oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single

            'oForm.DataSources.DataTables.Add("dtActivity")

            Dim oGrid As SAPbouiCOM.Grid
            oGrid = oForm.Items.Item("ACTGRID").Specific

            oForm.DataSources.DataTables.Item("dtActivity").
                ExecuteQuery("select ClgCode AS 'Activity Code', ReContact AS 'Date', BeginTime AS 'Time', Details AS 'Remarks', " _
                + "Notes AS 'Notes', U_IDHWO AS 'WO Header', U_IDHWOR AS 'WO Row' from OCLG where U_IDHWOR = '" + getFormDFValue(oForm, "Code") + "'")
            oGrid.DataTable = oForm.DataSources.DataTables.Item("dtActivity")

            'Setting Columns attributes
            oGrid.Columns.Item(0).Width = 70
            oGrid.Columns.Item(0).Editable = False
            'Add link button on Activity Code Column
            Dim oLinkCol As SAPbouiCOM.EditTextColumn
            oLinkCol = oGrid.Columns.Item("Activity Code")
            oLinkCol.LinkedObjectType = SAPbouiCOM.BoLinkedObject.lf_ContactWithCustAndVend

            oGrid.Columns.Item(1).Width = 60
            oGrid.Columns.Item(1).Editable = False

            oGrid.Columns.Item(2).Width = 60
            oGrid.Columns.Item(2).Editable = False

            oGrid.Columns.Item(3).Width = 130
            oGrid.Columns.Item(3).Editable = False

            oGrid.Columns.Item(4).Width = 230
            oGrid.Columns.Item(4).Editable = False

            oGrid.Columns.Item(5).Visible = False
            oGrid.Columns.Item(6).Visible = False
        End Sub

        Public Sub doReloadActivity(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim oGrid As SAPbouiCOM.Grid
                oGrid = oForm.Items.Item("ACTGRID").Specific

                oForm.DataSources.DataTables.Item("dtActivity").
                    ExecuteQuery("select ClgCode AS 'Activity Code', ReContact AS 'Date', BeginTime AS 'Time', Details AS 'Remarks', " _
                    + "Notes AS 'Notes', U_IDHWO AS 'WO Header', U_IDHWOR AS 'WO Row' from OCLG where U_IDHWOR = '" + getFormDFValue(oForm, "Code") + "'")
                oGrid.DataTable = oForm.DataSources.DataTables.Item("dtActivity")

                'Setting Columns attributes
                oGrid.Columns.Item(0).Width = 70
                oGrid.Columns.Item(0).Editable = False
                'Add link button on Activity Code Column
                Dim oLinkCol As SAPbouiCOM.EditTextColumn
                oLinkCol = oGrid.Columns.Item("Activity Code")
                oLinkCol.LinkedObjectType = SAPbouiCOM.BoLinkedObject.lf_ContactWithCustAndVend

                oGrid.Columns.Item(1).Width = 60
                oGrid.Columns.Item(1).Editable = False

                oGrid.Columns.Item(2).Width = 60
                oGrid.Columns.Item(2).Editable = False

                oGrid.Columns.Item(3).Width = 130
                oGrid.Columns.Item(3).Editable = False

                oGrid.Columns.Item(4).Width = 230
                oGrid.Columns.Item(4).Editable = False

                oGrid.Columns.Item(5).Visible = False
                oGrid.Columns.Item(6).Visible = False
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "doReloadActivity()")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXACT", {Nothing})
            End Try
        End Sub

        Private Sub doAdditionalExpGridUOMCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "ADDGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_UOM"))
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

                doFillCombo(oForm, oCombo, "OWGT", "UnitDisply", "UnitName", Nothing, Nothing, Nothing, Nothing)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the UOM Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("UOM")})
            End Try
        End Sub

        Private Function getPaymentTerms(ByVal oForm As SAPbouiCOM.Form, ByVal sCardCode As String) As String
            Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            Dim oPT As SAPbobsCOM.PaymentTermsTypes = Nothing
            Dim sPaymentTerms As String = ""
            Try
                oBP = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
                oPT = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPaymentTermsTypes)

                If oBP.GetByKey(sCardCode) Then
                    If oPT.GetByKey(oBP.PayTermsGrpCode) Then
                        sPaymentTerms = oPT.PaymentTermsGroupName
                    End If
                End If
            Catch ex As Exception
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oPT)
                IDHAddOns.idh.data.Base.doReleaseObject(oBP)
            End Try
            Return sPaymentTerms
        End Function

        Public Sub doSendCreditCheckAlertAndMessage(ByVal oForm As SAPbouiCOM.Form, ByVal sCardCode As String, ByVal sHeaderMsg As String, ByVal sAlertUsersKey As String, ByVal sAlertMsgKey As String)
            Dim sAlertusers As String = Config.ParameterWithDefault(sAlertUsersKey, "")
            If sAlertusers IsNot Nothing AndAlso sAlertusers.Length > 0 Then
                Dim sAlertMsg As String = Config.ParameterWithDefault(sAlertMsgKey, "Credit check alert!")
                sAlertMsg = sAlertMsg + Chr(13)
                sAlertMsg = sAlertMsg + sHeaderMsg + Chr(13)
                sAlertMsg = sAlertMsg + "Work Order: " + getFormDFValue(oForm, "Code") + Chr(13)
                sAlertMsg = sAlertMsg + "From User: " + goParent.gsUserName + Chr(13)
                sAlertMsg = sAlertMsg + "Cc: " + sAlertusers
                If Not sAlertusers.Contains(goParent.gsUserName) Then
                    sAlertusers = sAlertusers + "," + goParent.gsUserName
                End If
                goParent.doMessage(sHeaderMsg)
                goParent.doSendAlert(sAlertusers.Split(","), sHeaderMsg & ": " & sCardCode, sAlertMsg, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner, sCardCode, True)
            End If
        End Sub

        Public Function doCheckTFSStatus(ByVal sTFSNum As String) As Boolean
            Dim sChkQry As String
            sChkQry = "SELECT U_Status FROM [@IDH_TFSMAST] where U_TFSNum = '" + sTFSNum + "'"
            Dim oChkRS As SAPbobsCOM.Recordset = goParent.goDB.doSelectQuery(sChkQry)
            If oChkRS IsNot Nothing AndAlso oChkRS.RecordCount > 0 Then
                Dim sTFSStatus As String = oChkRS.Fields.Item("U_Status").Value.ToString()
                IDHAddOns.idh.data.Base.doReleaseObject(oChkRS)
                If sTFSStatus.Equals("05") OrElse sTFSStatus.Equals("06") Then
                    Return True
                Else
                    Return False
                End If
            End If
            Return False
        End Function

        Public Sub doUpdateTFSMovementRows(ByVal sTFSNum As String, ByVal sWOHeader As String, ByVal sWORow As String)
            Dim sQry As String
            'sQry = "UPDATE [@IDH_TFSMOVE] SET U_WRRow = CASE WHEN LEN(U_WRRow) > 0 THEN U_WRRow + '," + sWORow + "' ELSE '" + sWORow + "' END " + _
            '" WHERE U_TFSNo = '" + sTFSNum + "' AND U_Status = '01' "

            sQry = "UPDATE [@IDH_TFSMOVE] SET U_WRRow = CASE WHEN LEN(U_WRRow) > 0 THEN U_WRRow + '," + sWORow + "' ELSE '" + sWORow + "' END " +
            " WHERE U_TFSNo = '" + sTFSNum + "' AND U_Status = '01' " +
            " AND Code = (SELECT TOP 1 Code from [@IDH_TFSMOVE] where U_TFSNo = '" + sTFSNum + "' AND U_Status = '01'  AND (IsNull(U_WRRow, '') NOT LIKE '%" + sWORow + "%')   )"


            Dim oRS As SAPbobsCOM.Recordset = goParent.goDB.doSelectQuery(sQry)
            IDHAddOns.idh.data.Base.doReleaseObject(oRS)
        End Sub

        Public Function getWPEWCCode(ByVal sItemCode As String) As String
            Dim oRS As SAPbobsCOM.Recordset
            oRS = goParent.goDB.doSelectQuery("select U_EWC from OITM where ItemCode = '" + sItemCode + "'")
            If oRS.RecordCount > 0 Then
                'Return "%" + oRS.Fields.Item("U_EWC").Value + "%"
                Return oRS.Fields.Item("U_EWC").Value
            Else
                Return ""
            End If
        End Function

        Public Function IsOrderTypeCommentsRequired(ByVal sOrderType As String) As String
            Dim oRS As SAPbobsCOM.Recordset
            oRS = goParent.goDB.doSelectQuery("select IsNull(U_RemarksReq, '') as U_RemarksReq from [@IDH_JOBTYPE] where U_JobTp = '" + sOrderType + "' AND IsNull(U_RemarksReq, '') <> '' ")
            If oRS.RecordCount > 0 Then
                Dim sRemarksReq As String = oRS.Fields.Item("U_RemarksReq").Value
                If sRemarksReq.Length > 0 AndAlso sRemarksReq.Equals("Y") Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        End Function

        Public Function hasDifferentCountry(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim sCustCountry As String
            Dim sDisposalCountry As String

            Dim oCustAdd As ArrayList = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, getFormDFValue(oForm, "U_CustCd"), "S", getUFValue(oForm, "IDH_ADDR"))
            Dim oDisposalAdd As ArrayList = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, getFormDFValue(oForm, "U_Tip"), "S", getFormDFValue(oForm, "U_SAddress"))

            sCustCountry = If((oCustAdd IsNot Nothing AndAlso oCustAdd.Count > 0), oCustAdd(5), "")
            sDisposalCountry = If((oDisposalAdd IsNot Nothing AndAlso oDisposalAdd.Count > 0), oDisposalAdd(5), "")

            If sCustCountry.Length < 1 Or sDisposalCountry.Length < 1 Then
                Return False
            Else
                Return Not sCustCountry.Equals(sDisposalCountry)
            End If
        End Function

        Private Sub doAddAutoAdditionalCodes(ByVal oForm As SAPbouiCOM.Form)
            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)
            Dim oItem As com.idh.dbObjects.User.Item

            Dim oCodes As AutoAdditinalCodes
            oCodes = oWOR.getAutoAdditionalCodesCIPSIP()

            If oCodes IsNot Nothing Then
                Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")

                'Customer
                If Not oCodes.moCustomerList Is Nothing AndAlso oCodes.moCustomerList.Count > 0 Then
                    For Each oItem In oCodes.moCustomerList
                        If Not oItem.msItemCode Is Nothing AndAlso oItem.msItemCode.Length > 0 Then
                            'Check if the item is allready in the Grid and only adds it if it does not
                            If oGrid.doIndexOfItem(oItem.msItemCode) < 0 Then
                                'Dim sJobNr As String = getFormDFValue(oForm, "Code", True)
                                'Dim sRowNr As String = getDFValue(oForm, msRowTable, "Code")

                                Dim sCustCd As String = oWOR.F_CustomerCode
                                Dim sCustNm As String = oWOR.F_CustomerName
                                Dim sCustCurr As String = Config.INSTANCE.getValueFromBP(sCustCd, "Currency")
                                Dim sSuppCd As String = "" 'getDFValue(oForm, msRowTable, "U_CongCd")
                                Dim sSuppNm As String = "" 'getDFValue(oForm, msRowTable, "U_SCngNm")
                                Dim sSuppCurr As String = ""
                                Dim sEmpId As String = ""
                                Dim sEmpNm As String = ""
                                Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(sCustCd, oItem.msItemCode)
                                Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(sSuppCd, oItem.msItemCode)

                                oGrid.doAddExpense(oWOR.U_JobNr, oWOR.Code,
                                    sCustCd, sCustNm, sCustCurr,
                                    sSuppCd, sSuppNm, sSuppCurr,
                                    sEmpId, sEmpNm,
                                    oItem.msItemCode, oItem.msItemName,
                                    oItem.msUOM, 1,
                                    oItem.mdItemCost, oItem.mdItemChrg,
                                    sCostVatGrp,
                                    sChrgVatGrp,
                                    True,
                                    True,
                                    True,
                                    False,
                                    oItem.msChrgCalc, "VARIABLE")

                            End If
                        End If
                    Next
                End If

                'Supplier
                If Not oCodes.moSupplierList Is Nothing AndAlso oCodes.moSupplierList.Count > 0 Then
                    For Each oItem In oCodes.moSupplierList
                        If Not oItem.msItemCode Is Nothing AndAlso oItem.msItemCode.Length > 0 Then
                            'Check if the item is allready in the Grid and only adds it if it does not
                            If oGrid.doIndexOfItem(oItem.msItemCode) < 0 Then
                                'Dim sJobNr As String = getFormDFValue(oForm, "Code", True)
                                'Dim sRowNr As String = getDFValue(oForm, msRowTable, "Code")

                                Dim sCustCd As String = oWOR.F_CustomerCode
                                Dim sCustNm As String = oWOR.F_CustomerName
                                Dim sCustCurr As String = Config.INSTANCE.getValueFromBP(sCustCd, "Currency")
                                Dim sSuppCd As String = oWOR.U_ProCd
                                Dim sSuppNm As String = oWOR.U_ProNm
                                Dim sSuppCurr As String = oWOR.U_PurcCoCc
                                Dim sEmpId As String = ""
                                Dim sEmpNm As String = ""
                                Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(sCustCd, oItem.msItemCode)
                                Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(sSuppCd, oItem.msItemCode)

                                oGrid.doAddExpense(oWOR.U_JobNr, oWOR.Code,
                                    sCustCd, sCustNm, sCustCurr,
                                    sSuppCd, sSuppNm, sSuppCurr,
                                    sEmpId, sEmpNm,
                                    oItem.msItemCode, oItem.msItemName,
                                    oItem.msUOM, 1,
                                    oItem.mdItemCost, oItem.mdItemChrg,
                                    sCostVatGrp,
                                    sChrgVatGrp,
                                    True,
                                    True,
                                    True,
                                    False,
                                    "VARIABLE", oItem.msChrgCalc)

                            End If
                        End If
                    Next
                End If
            End If
        End Sub

#Region "PriceFunctions"
        Private Shared Sub doGetCosts(ByVal oParent As IDHAddOns.idh.addon.Base, ByRef oRowInObj As IDHAddOns.idh.data.AuditObject, ByRef oRowOutObj As IDHAddOns.idh.data.AuditObject)
            Try
                Dim sTipCode As String = oRowInObj.getFieldValue("U_Tip")
                Dim sCarrierCode As String = oRowInObj.getFieldValue("U_CarrCd")
                Dim sItemCode As String = oRowInObj.getFieldValue("U_ItemCd")
                Dim sItemGrp As String = oRowInObj.getFieldValue("U_ItmGrp")
                Dim sJobType As String = oRowInObj.getFieldValue("U_JobTp")
                Dim dTipWeight As Double = oRowInObj.getFieldValue(IDH_JOBSHD._TRdWgt) 'oRowInObj.getFieldValue("U_TipWgt")
                Dim dHaulWeight As Double = oRowInObj.getFieldValue("U_OrdWgt")
                Dim sWastCd As String = oRowInObj.getFieldValue("U_WasCd")
                Dim sCustCd As String = oRowInObj.getFieldValue("U_CustCd")
                'Dim sAddress As String = Nothing 'oRowInObj.getFieldValue("STADDR")
                Dim sPUOM As String = oRowInObj.getFieldValue("U_PUOM")
                'Dim sDocDate As String = oRowInObj.getFieldValue("U_RDate")

                '** Get Customer Address
                Dim sAddress As String = Nothing
                Dim sZipCode As String = Nothing
                Dim sBranch As String

                Dim sJobNr As String = oRowInObj.getFieldValue("U_JobNr")
                Dim sQry As String = "Select U_ADDRESS, U_ZpCd FROM [@IDH_JOBENTR] WHERE CODE = '" & sJobNr & "'"

                Dim iPrcSet As Integer = oRowOutObj.getFieldValue("U_MANPRC")

                Dim oRecordset As SAPbobsCOM.Recordset = Nothing
                oRecordset = oParent.goDB.doSelectQuery(sQry)
                If Not oRecordset Is Nothing AndAlso oRecordset.RecordCount > 0 Then
                    sAddress = oRecordset.Fields.Item(0).Value.Trim()
                    sZipCode = oRecordset.Fields.Item(1).Value.Trim()
                End If
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordset)

                sBranch = oRowInObj.getFieldValue("U_Branch")

                'Dim dDocDate As Date = com.idh.utils.dates.doStrToDate(sDocDate)
                Dim dDocDate As Date = oRowInObj.getFieldValue("U_RDate")

                Dim dTipCost As Double
                Dim dHaulCost As Double
                Dim dTipTotal As Double = 0
                Dim dTipOffset As Double = 0
                Dim dHaulTotal As Double = 0
                Dim sCalcType As String = ""
                Dim sHCalcType As String = ""
                'Dim dOrdTotal As Double = 0
                Dim dLicTotal As Double = 0

                Dim dTipVatRate As Double = 0
                Dim dHaulageVatRate As Double = 0

                Dim oPrices As com.idh.dbObjects.User.Prices 'ArrayList
                If sTipCode.Length > 0 AndAlso sItemCode.Length > 0 Then
                    Dim oSIPT As IDH_SUITPR = New IDH_SUITPR()
                    oPrices = oSIPT.doGetJobCostPrice("Waste Order", sJobType, sItemGrp, sItemCode, sTipCode, dTipWeight, sPUOM, sWastCd, sCustCd, sAddress, dDocDate, sBranch, sZipCode)

                    dTipCost = oPrices.TipPrice
                    sCalcType = oPrices.TipChargeCalc.ToUpper()
                    dTipOffset = oPrices.TipOffsetWeight
                    dTipVatRate = oPrices.TipVat

                    oRowOutObj.setFieldValue("U_TCostVtRt", dTipVatRate)
                    oRowOutObj.setFieldValue("U_TCostVtGrp", oPrices.TipVatGroup)
                Else
                    dTipCost = 0
                    sCalcType = "VARIABLE"

                    oRowOutObj.setFieldValue("U_TCostVtRt", 0)
                    oRowOutObj.setFieldValue("U_TCostVtGrp", "")
                End If

                If sCalcType.Equals("OFFSET") Then
                    If dTipOffset < dTipWeight Then
                        dTipWeight = dTipWeight - dTipOffset
                    Else
                        dTipWeight = 0
                    End If

                    oRowInObj.setFieldValue("U_TipWgt", dTipWeight)

                    dTipTotal = dTipCost * dTipWeight
                End If

                If sCalcType.Equals("FIXED") Then
                    If dTipWeight = 0 Then
                        dTipTotal = 0
                    Else
                        dTipTotal = dTipCost
                    End If
                Else
                    dTipTotal = dTipCost * dTipWeight
                End If

                If sCarrierCode.Length > 0 AndAlso sItemCode.Length > 0 Then
                    Dim oSIPH As IDH_SUITPR = New IDH_SUITPR()
                    oPrices = oSIPH.doGetJobCostPrice("Waste Order", sJobType, sItemGrp, sItemCode, sCarrierCode, dHaulWeight, sPUOM, sWastCd, sCustCd, sAddress, dDocDate, sBranch, sZipCode)

                    dHaulCost = oPrices.HaulPrice
                    sHCalcType = oPrices.HaulageChargeCalc

                    dHaulageVatRate = oPrices.HaulageVat
                    oRowOutObj.setFieldValue("U_HCostVtRt", dHaulageVatRate)
                    oRowOutObj.setFieldValue("U_HCostVtGrp", oPrices.HaulageVatGroup)
                Else
                    dHaulCost = 0
                    sHCalcType = "STANDARD"

                    oRowOutObj.setFieldValue("U_HCostVtRt", 0)
                    oRowOutObj.setFieldValue("U_HCostVtGrp", "")
                End If

                If sHCalcType.ToUpper().Equals("FIXED") Then
                    If dHaulWeight = 0 Then
                        dHaulTotal = 0
                    Else
                        dHaulTotal = dHaulCost
                    End If
                Else
                    dHaulTotal = dHaulCost * dHaulWeight
                End If

                Dim dTAddCost As Double = oRowOutObj.getFieldValue("U_TAddCost")
                Dim dTipVat As Double = dTipTotal * (dTipVatRate / 100)
                Dim dHaulageVat As Double = dHaulTotal * (dHaulageVatRate / 100)

                oRowOutObj.setFieldValue("U_TipCost", dTipCost)
                iPrcSet = iPrcSet And (Not Config.MASK_TIPCST)

                oRowOutObj.setFieldValue("U_OrdCost", dHaulCost)
                iPrcSet = iPrcSet And (Not Config.MASK_HAULCST)

                oRowOutObj.setFieldValue("U_MANPRC", iPrcSet)

                oRowOutObj.setFieldValue("U_TipTot", dTipTotal)
                oRowOutObj.setFieldValue("U_OrdTot", dHaulTotal)
                oRowOutObj.setFieldValue("U_JCost", (dTipTotal + dHaulTotal + dLicTotal + dTAddCost + dTipVat + dHaulageVat))

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error get the costs.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGCST", {Nothing})
            End Try
        End Sub

        Private Shared Sub doGetCharges(ByVal oParent As IDHAddOns.idh.addon.Base, ByRef oRowInObj As IDHAddOns.idh.data.AuditObject, ByRef oRowOutObj As IDHAddOns.idh.data.AuditObject)
            Try
                Dim sCardCode As String = oRowInObj.getFieldValue("U_CustCd")
                Dim sCarrierCode As String = oRowInObj.getFieldValue("U_CarrCd")
                Dim sItemCode As String = oRowInObj.getFieldValue("U_ItemCd")
                Dim sItemGrp As String = oRowInObj.getFieldValue("U_ItmGrp")
                Dim sWastCd As String = oRowInObj.getFieldValue("U_WasCd")
                ''Dim sAddress As String = Nothing 'getWFValue(oForm, "STADDR")
                Dim sJobType As String = oRowInObj.getFieldValue("U_JobTp")
                Dim sDocDate As String = oRowInObj.getFieldValue("U_RDate")
                Dim dDocDate As Date = oRowInObj.getFieldValue("U_RDate") 'com.idh.utils.dates.doStrToDate(sDocDate)

                '** Get Customer Address
                Dim sAddress As String = Nothing
                Dim sZipCode As String = Nothing
                Dim sBranch As String = oRowInObj.getFieldValue("U_Branch") 'com.idh.utils.dates.doStrToDate(sDocDate)
                Dim sJobNr As String = oRowInObj.getFieldValue("U_JobNr")
                Dim sQry As String = "Select U_ADDRESS, U_ZpCd FROM [@IDH_JOBENTR] WHERE CODE = '" & sJobNr & "'"

                Dim oRecordset As SAPbobsCOM.Recordset = Nothing
                oRecordset = oParent.goDB.doSelectQuery(sQry)
                If Not oRecordset Is Nothing AndAlso oRecordset.RecordCount > 0 Then
                    sAddress = oRecordset.Fields.Item(0).Value.Trim()
                    sZipCode = oRecordset.Fields.Item(1).Value.Trim()
                End If
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordset)

                'Dim sCustUOM As String = Config.INSTANCE.getBPUOM(oParent, sCardCode)
                'Dim sDefUOM As String = Config.INSTANCE.getDefaultUOM(oParent)
                Dim sCIPUom As String = oRowInObj.getFieldValue("U_UOM")
                'Dim sUOM As String
                Dim sUseWgt As String = oRowInObj.getFieldValue("U_UseWgt")

                If sCIPUom Is Nothing OrElse sCIPUom.Length = 0 Then
                    If sUseWgt = "1" Then
                        sCIPUom = Config.INSTANCE.getCIPWeightUOM("Waste Order", sJobType, sItemGrp, sItemCode, sCardCode, sWastCd, sAddress, dDocDate, sBranch, sZipCode)
                    Else
                        sCIPUom = Config.INSTANCE.getCIPUnitUOM("Waste Order", sJobType, sItemGrp, sItemCode, sCardCode, sWastCd, sAddress, dDocDate, sBranch, sZipCode)
                    End If

                    If sCIPUom Is Nothing OrElse sCIPUom.Length = 0 Then
                        sCIPUom = Config.INSTANCE.getCIPAnyUOM("Waste Order", sJobType, sItemGrp, sItemCode, sCardCode, sWastCd, sAddress, dDocDate, sBranch, sZipCode)

                        If sCIPUom Is Nothing OrElse sCIPUom.Length = 0 Then
                            sCIPUom = Config.INSTANCE.doGetItemSalesUOM(sCardCode, sWastCd)
                            'sCIPUom = Config.INSTANCE.getDefaultUOM(oParent)
                        End If

                        If sCIPUom IsNot Nothing AndAlso sCIPUom.Length > 0 Then
                            Dim dWeightFactor As Double = Config.INSTANCE.doGetBaseWeightFactors(sCIPUom)
                            If dWeightFactor <= 0 Then
                                oRowInObj.setFieldValue("U_UseWgt", "2")
                                sUseWgt = "2"
                            Else
                                oRowInObj.setFieldValue("U_UseWgt", "1")
                                sUseWgt = "1"
                            End If
                        End If
                    End If

                    If sCIPUom IsNot Nothing Then
                        oRowInObj.setFieldValue("U_UOM", sCIPUom)
                        '    sUOM = sCIPUom
                        'Else
                        '   sUOM = oRowInObj.getFieldValue("U_UOM")
                    End If
                End If

                Dim dWrkWgt As Double = 0
                If sUseWgt = "2" Then
                    dWrkWgt = oRowInObj.getFieldValue("U_AUOMQt")
                Else
                    dWrkWgt = oRowInObj.getFieldValue("U_RdWgt")
                End If
                'If dReadWeight = 0 Then
                'dReadWeight = oRowInObj.getFieldValue("U_RdWgt")
                'End If

                Dim iPrcSet As Integer = oRowOutObj.getFieldValue("U_MANPRC")
                Dim dHaulChrg As Double
                Dim dTipCharge As Double
                Dim sTipChargCalcType As String
                Dim sHaulageChargCalcType As String
                Dim dTipOffset As Double
                Dim dHaulageOffset As Double

                Dim dTipVatRate As Double = 0
                Dim sTipVatGrp As String = ""
                Dim dHaulageVatRate As Double = 0
                Dim sHaulageVatGrp As String = ""
                '## MA Start 08-04-2014
                Dim dAddCharges As Double = oRowInObj.getFieldValue("U_AddCharge") 'oRowInObj.getFieldValue("U_TAddChrg")

                Dim dAddChargesVAT As Double = oRowInObj.getFieldValue("U_TAddChVat") 'oRowInObj.getFieldValue("U_TAddChrg")
                '## MA End 08-04-2014
                If sCardCode.Length > 0 AndAlso sItemCode.Length > 0 Then
                    Dim oPrices As com.idh.dbObjects.User.Prices 'ArrayList
                    Dim oCIP As IDH_CSITPR = New IDH_CSITPR()

                    oPrices = oCIP.doGetJobChargePrices("Waste Order", sJobType, sItemGrp, sItemCode, sCardCode, dWrkWgt, sCIPUom, sWastCd, sCarrierCode, sAddress, dDocDate, sBranch, sZipCode)

                    dHaulChrg = oPrices.HaulPrice
                    dTipCharge = oPrices.TipPrice
                    dTipVatRate = oPrices.TipVat
                    sTipVatGrp = oPrices.TipVatGroup

                    dHaulageVatRate = oPrices.HaulageVat
                    sHaulageVatGrp = oPrices.HaulageVatGroup

                    sTipChargCalcType = oPrices.TipChargeCalc
                    sHaulageChargCalcType = oPrices.HaulageChargeCalc
                    dTipOffset = oPrices.TipOffsetWeight
                    dHaulageOffset = oPrices.HaulageOffsetWeight
                Else
                    dHaulChrg = 0
                    dTipCharge = 0

                    sTipChargCalcType = "VARIABLE"
                    sHaulageChargCalcType = "STANDARD"
                    dTipOffset = 0
                    dHaulageOffset = 0
                End If

                Dim dTotal As Double = 0
                Dim dCustWeight As Double
                Dim dTipCustTotal As Double
                Dim dPrice As Double
                Dim dCusQty As Double
                Dim dBefDiscTotal As Double

                Dim dDiscountAmt As Double
                Dim dDiscnt As Double
                Dim dVatAmnt As Double
                Dim dDefWeight As Double = 0
                Dim sHaulAC As String

                If sHaulageChargCalcType.Equals("WEIGHT") Then
                    sHaulAC = "Y"
                Else
                    sHaulAC = "N"
                End If
                oRowOutObj.setFieldValue("U_HaulAC", sHaulAC)

                dDiscnt = oRowInObj.getFieldValue("U_Discnt")
                dDiscountAmt = oRowInObj.getFieldValue("U_DisAmt")

                If dWrkWgt > 0 Then
                    If sTipChargCalcType.ToUpper().Equals("OFFSET") Then
                        If dTipOffset < dWrkWgt Then
                            dCustWeight = dWrkWgt - dTipOffset
                        Else
                            dCustWeight = 0
                        End If
                    Else
                        dCustWeight = dWrkWgt
                    End If
                ElseIf dWrkWgt < 0 Then
                    dCustWeight = dWrkWgt
                Else
                    dCustWeight = 0
                End If

                If sTipChargCalcType.ToUpper().Equals("FIXED") Then
                    If dCustWeight = 0 Then
                        dTipCustTotal = 0
                    Else
                        dTipCustTotal = dTipCharge
                    End If
                Else
                    dTipCustTotal = dTipCharge * dCustWeight
                End If

                'Haulage
                If sHaulAC = "Y" Then
                    dCusQty = oRowInObj.getFieldValue("U_RdWgt")
                Else
                    dCusQty = oRowInObj.getFieldValue("U_CusQty")
                End If
                oRowOutObj.setFieldValue("U_HlSQty", dCusQty)

                dPrice = dCusQty * dHaulChrg
                dBefDiscTotal = dTipCustTotal + dPrice

                If dDiscnt = 0 AndAlso dDiscountAmt <> 0 Then
                    dDiscnt = dDiscountAmt / dBefDiscTotal * 100
                ElseIf dDiscnt <> 0 Then
                    dDiscountAmt = (dDiscnt / 100) * dBefDiscTotal
                End If

                Dim TSub As Double = dTipCustTotal - (dTipCustTotal * (dDiscnt / 100))
                Dim HSub As Double = dPrice - (dPrice * (dDiscnt / 100))
                Dim TVat As Double = TSub * (dTipVatRate / 100)
                Dim HVat As Double = HSub * (dHaulageVatRate / 100)

                dTotal = TSub + HSub
                '##MA Start 08-04-2014
                dVatAmnt = TVat + HVat + dAddChargesVAT
                '##MA End 08-04-2014

                dTotal = dTotal + dVatAmnt + dAddCharges

                oRowOutObj.setFieldValue("U_CstWgt", dCustWeight)
                oRowOutObj.setFieldValue("U_Price", dPrice)
                oRowOutObj.setFieldValue("U_Discnt", CStr(dDiscnt))
                oRowOutObj.setFieldValue("U_DisAmt", CStr(dDiscountAmt))
                oRowOutObj.setFieldValue("U_TaxAmt", dVatAmnt)
                oRowOutObj.setFieldValue("U_TCTotal", CStr(dTipCustTotal))
                oRowOutObj.setFieldValue("U_Total", CStr(dTotal))

                oRowOutObj.setFieldValue("U_CusChr", dHaulChrg)
                iPrcSet = iPrcSet And (Not Config.MASK_HAULCHRG)

                oRowOutObj.setFieldValue("U_TCharge", dTipCharge)

                iPrcSet = iPrcSet And (Not Config.MASK_TIPCHRG)
                oRowOutObj.setFieldValue("U_MANPRC", iPrcSet)
                oRowOutObj.setFieldValue("U_TChrgVtRt", dTipVatRate)
                oRowOutObj.setFieldValue("U_TChrgVtGrp", sTipVatGrp)

                oRowOutObj.setFieldValue("U_HChrgVtRt", dHaulageVatRate)
                oRowOutObj.setFieldValue("U_HChrgVtGrp", sHaulageVatGrp)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error getting the charges.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGCHG", {Nothing})
            End Try
        End Sub

        Private Function doCreatePriceUpdateOptionForm(ByVal oForm As SAPbouiCOM.Form) As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption
            Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption = New WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption(Nothing, oForm, Nothing)
            oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doAddUpdatePrices)
            oOOForm.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doCancelAddUpdatePrices)
            Return oOOForm
        End Function

        'Private Sub doGetAdditionalChargePrices(ByVal oForm As SAPbouiCOM.Form)
        '    Dim oWOR As IDH_JOBSHD = thisWOR(oForm)

        '    Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
        '    If Not oGrid Is Nothing Then
        '        oGrid.doCalculateLinesPrices(oWOR.F_CustomerCode, oWOR.F_ContainerCode, oWOR.F_ContainerGroup, oWOR.F_WasteCode, oWOR.F_CustomerAddress, oWOR.U_Branch, oWOR.U_JobTp, oWOR.U_RDate, oWOR.U_CstWgt, oWOR.U_UOM, oWOR.F_CustomerZipCode)
        '        Dim dChrgT As Double = oGrid.doCalculateChargeTotals()

        '        setUFValue(oForm, "IDH_ADDCH", dChrgT)
        '        oWOR.U_TAddChrg = dChrgT
        '    End If

        '    oWOR.doCalculateChargeTotal()
        'End Sub
#End Region

#Region "OtherPriceQtyCalculations"
        Public Sub doCalculateProfit(ByVal oRow As IDH_JOBSHD)
            Dim oForm As SAPbouiCOM.Form = oRow.SBOForm
            Dim dProfit = oRow.doCalculateProfit()

            Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            'Dim dAddProf As Double
            If Not oGrid Is Nothing Then
                dProfit = dProfit + oGrid.doCalculateProfit()
            End If

            setUFValue(oForm, "IDH_PROFIT", dProfit)
        End Sub
#End Region

        Public Function doCalculateSiteDistances(ByVal sOrigin As String, ByVal sDestination As String) As String
            Dim sURL As String = "http://maps.googleapis.com/maps/api/distancematrix/xml?"
            sURL = sURL + "origins=" + sOrigin + "&"
            sURL = sURL + "destinations=" + sDestination + "&"
            sURL = sURL + "sensor=false" + "&"
            sURL = sURL + "language=en-GB" + "&"
            sURL = sURL + "units=imperial"

            Try
                'Make request to WebService and Get response 
                Dim oWebRequest As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create(sURL)
                Dim oWebResponse As System.Net.HttpWebResponse = oWebRequest.GetResponse()

                'Read data stream from response and parse in XML 
                Dim oStream As Stream = oWebResponse.GetResponseStream()

                Dim oReader As New StreamReader(oStream)
                Dim sResponseReader As String = oReader.ReadToEnd()
                oWebResponse.Close()
                oReader.Close()

                Dim xmlDoc As New Xml.XmlDocument
                xmlDoc.LoadXml(sResponseReader)

                Dim nodeDuration As Xml.XmlNodeList
                Dim nodeDistance As Xml.XmlNodeList
                Dim sDuration As String
                Dim sDistance As String

                If xmlDoc.GetElementsByTagName("status").Item(0).ChildNodes.Item(0).InnerText = "OK" Then
                    nodeDuration = xmlDoc.GetElementsByTagName("duration")
                    nodeDistance = xmlDoc.GetElementsByTagName("distance")

                    sDuration = nodeDuration.Item(0).ChildNodes.Item(0).InnerText
                    sDistance = nodeDistance.Item(0).ChildNodes.Item(0).InnerText
                    Return sDuration + "##" + sDistance
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doCalculateSiteDistances().")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCSD", {Nothing})
            End Try
            Return "-1"
        End Function

#Region "Handlers"
        Public Sub doHandleChargeTotalsCalculated(ByVal oCaller As IDH_JOBSHD)
            Dim oForm As SAPbouiCOM.Form = oCaller.SBOForm

            setUFValue(oForm, "U_BefDis", oCaller.SubBeforeDiscount)
            setUFValue(oForm, "U_SubTot", oCaller.SubAfterDiscount)

            doCalculateProfit(oCaller)
        End Sub


        ''IDH_CUSCHG  @IDH_JOBSHD U_TCharge 680,230,50 Pane 100-101
        ''IDH_CUSCHR @IDH_JOBSHD U_CusChr 680,245,50 
        ''
        ''IDH_PRCOST @IDH_JOBSHD U_PCost 680,405,50
        ''IDH_TIPCOS @IDH_JOBSHD U_TipCost 680,420,50
        ''IDH_ORDCOS @IDH_JOBSHD U_OrdCost 680,435,50
        ''
        ''# uBC_CUSCHG  @IDH_JOBSHD U_TCharge 680,230,50
        ''# uBC_CUSCHR @IDH_JOBSHD U_CusChr 680,245,50 
        ''
        ''# uBC_PRCOST @IDH_JOBSHD U_PCost 680,405,50
        ''# uBC_TIPCOS @IDH_JOBSHD U_TipCost 680,420,50
        ''# uBC_ORDCOS @IDH_JOBSHD U_OrdCost 680,435,50 

        Public Sub doHandleChargeTipPriceLookupSkipped(ByVal oCaller As DBBase, ByVal oData As Object)
            Dim iBackColor As Integer

            If oData = False Then
                iBackColor = miPriceLookupSkipBackCol
            Else
                iBackColor = DefaultBackColor(oCaller.SBOForm)
            End If

            Dim oItem As SAPbouiCOM.Item

            oItem = oCaller.SBOForm.Items.Item("IDH_CUSWEI")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If

            oItem = oCaller.SBOForm.Items.Item("IDH_UOM")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If

            oItem = oCaller.SBOForm.Items.Item("IDH_CUSCHG")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If
        End Sub

        Public Sub doHandleChargeHaulagePriceLookupSkipped(ByVal oCaller As DBBase, ByVal oData As Object)
            Dim iBackColor As Integer

            If oData = False Then
                iBackColor = miPriceLookupSkipBackCol
            Else
                iBackColor = DefaultBackColor(oCaller.SBOForm)
            End If

            Dim oItem As SAPbouiCOM.Item

            oItem = oCaller.SBOForm.Items.Item("IDH_CUSCHR")
            If oItem.Enabled Then
                oItem.BackColor = iBackColor
            End If
        End Sub

        Public Sub doHandleCostTipPriceLookupSkipped(ByVal oCaller As DBBase, ByVal oData As Object)
            Dim iBackColor As Integer

            If oData = False Then
                iBackColor = miPriceLookupSkipBackCol
            Else
                iBackColor = DefaultBackColor(oCaller.SBOForm)
            End If

            Dim oItem As SAPbouiCOM.Item

            oItem = oCaller.SBOForm.Items.Item("IDH_TIPWEI")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If

            oItem = oCaller.SBOForm.Items.Item("IDH_PUOM")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If

            oItem = oCaller.SBOForm.Items.Item("IDH_TIPCOS")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If
        End Sub

        Public Sub doHandleCostHaulagePriceLookupSkipped(ByVal oCaller As DBBase, ByVal oData As Object)
            Dim iBackColor As Integer

            If oData = False Then
                iBackColor = miPriceLookupSkipBackCol
            Else
                iBackColor = DefaultBackColor(oCaller.SBOForm)
            End If

            Dim oItem As SAPbouiCOM.Item

            oItem = oCaller.SBOForm.Items.Item("IDH_ORDCOS")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If
        End Sub

        Public Sub doHandleCostProducerPriceLookupSkipped(ByVal oCaller As DBBase, ByVal oData As Object)
            Dim iBackColor As Integer

            If oData = False Then
                iBackColor = miPriceLookupSkipBackCol
            Else
                iBackColor = DefaultBackColor(oCaller.SBOForm)
            End If

            Dim oItem As SAPbouiCOM.Item

            oItem = oCaller.SBOForm.Items.Item("IDH_PURWGT")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If

            oItem = oCaller.SBOForm.Items.Item("IDH_PURUOM")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If

            oItem = oCaller.SBOForm.Items.Item("IDH_PRCOST")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If
        End Sub

        Public Sub doHandleChargePricesChanged(ByVal oCaller As IDH_JOBSHD)
            Dim oForm As SAPbouiCOM.Form = oCaller.SBOForm
            If oCaller.CIP.Prices Is Nothing OrElse oCaller.CIP.Prices.Count < 2 Then
                oForm.Items.Item("uBC_CUSCHG").Visible = False
                oForm.Items.Item("IDH_CUSCHG").Visible = True

                oForm.Items.Item("uBC_CUSCHR").Visible = False
                oForm.Items.Item("IDH_CUSCHR").Visible = True
            Else
                Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("uBC_CUSCHG")
                FillCombos.TippingPriceCombo(oItem.Specific, oCaller.CIP.Prices)

                oItem.Visible = True
                oForm.Items.Item("IDH_CUSCHG").Visible = False

                oItem = oForm.Items.Item("uBC_CUSCHR")
                FillCombos.HaulagePriceCombo(oItem.Specific, oCaller.CIP.Prices)

                oItem.Visible = True
                oForm.Items.Item("IDH_CUSCHR").Visible = False
            End If
        End Sub

        Public Sub doHandleCostTippingPricesChanged(ByVal oCaller As IDH_JOBSHD)
            Dim oForm As SAPbouiCOM.Form = oCaller.SBOForm
            If oCaller.SIPT.Prices Is Nothing OrElse oCaller.SIPT.Prices.Count < 2 Then
                oForm.Items.Item("uBC_TIPCOS").Visible = False
                oForm.Items.Item("IDH_TIPCOS").Visible = True
            Else
                Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("uBC_TIPCOS")
                FillCombos.TippingPriceCombo(oItem.Specific, oCaller.SIPT.Prices)

                oItem.Visible = True
                oForm.Items.Item("IDH_TIPCOS").Visible = False
            End If
        End Sub

        Public Sub doHandleCostHaulagePricesChanged(ByVal oCaller As IDH_JOBSHD)
            Dim oForm As SAPbouiCOM.Form = oCaller.SBOForm
            If oCaller.SIPH.Prices Is Nothing OrElse oCaller.SIPH.Prices.Count < 2 Then
                oForm.Items.Item("uBC_ORDCOS").Visible = False
                oForm.Items.Item("IDH_ORDCOS").Visible = True
            Else
                Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("uBC_ORDCOS")
                FillCombos.HaulagePriceCombo(oItem.Specific, oCaller.SIPH.Prices)

                oItem.Visible = True
                oForm.Items.Item("IDH_ORDCOS").Visible = False
            End If
        End Sub

        Public Sub doHandleCostProducerPricesChanged(ByVal oCaller As IDH_JOBSHD)
            Dim oForm As SAPbouiCOM.Form = oCaller.SBOForm
            If oCaller.SIPP.Prices Is Nothing OrElse oCaller.SIPP.Prices.Count < 2 Then
                oForm.Items.Item("uBC_PRCOST").Visible = False
                oForm.Items.Item("IDH_PRCOST").Visible = True
            Else
                Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("uBC_PRCOST")
                FillCombos.TippingPriceCombo(oItem.Specific, oCaller.SIPP.Prices)

                oItem.Visible = True
                oForm.Items.Item("IDH_PRCOST").Visible = False
            End If
        End Sub


        Public Sub doHandleCostTotalsCalculated(ByVal oCaller As IDH_JOBSHD)
            Dim oForm As SAPbouiCOM.Form = oCaller.SBOForm

            'setUFValue(oForm, "U_BefDis", oCaller.SubBeforeDiscount)
            'setUFValue(oForm, "U_SubTot", oCaller.SubAfterDiscount)

            doCalculateProfit(oCaller)
        End Sub
        Public Sub doHandlerWithData_ShowMessage(ByVal oCaller As IDH_JOBSHD, sMessageId As Object)
            com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage(sMessageId.ToString())
        End Sub

        Public Function doHandlerYNOption(ByVal oCaller As IDH_JOBSHD, ByVal sMessageId As String, ByVal sParams As String(), ByVal iDefButton As Integer) As Integer
            Return Messages.INSTANCE.doResourceMessageYN(sMessageId, sParams, iDefButton)
        End Function

        Public Sub doHandlerExternalAdditionalExpensesRebateUOMUpdater(ByVal oCaller As IDH_JOBSHD)
            Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oCaller.SBOForm, "ADDGRID")
            oGrid.doUpdateRebateLineUOM(oCaller.U_CarrCd, oCaller.U_WasCd, oCaller.U_ProUOM)
        End Sub

        Public Sub doHandlerExternalAdditionalExpensesRebateWeightUpdater(ByVal oCaller As IDH_JOBSHD)
            Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oCaller.SBOForm, "ADDGRID")
            oGrid.doUpdateRebateLineWeight(oCaller.U_CarrCd, oCaller.U_WasCd, oCaller.U_ProWgt, oCaller.U_ProUOM)
        End Sub

#End Region

#Region "Buffer Zone"
        Private Sub doCallLorrySearchEB(ByVal oForm As SAPbouiCOM.Form)
            'If Config.ParameterAsBool("VMUSENEW", False) = False Then
            ''With getFormMainDataSource(oForm)
            setSharedData(oForm, "IDH_VEHREG", getFormDFValue(oForm, "U_Lorry"))

            If Config.INSTANCE.getParameterAsBool("WOVESEBC", False) = True Then
                setSharedData(oForm, "IDH_VEHCUS", "")
            Else
                setSharedData(oForm, "IDH_VEHCUS", getFormDFValue(oForm, "U_CustCd"))
            End If
            ''MA Start 19-01-2014 Issue#142 Skype chat Hannah 19-01-2014
            'If Config.INSTANCE.getParameterAsBool("WOVESEAS", False) = True Then
            '    setSharedData(oForm, "IDH_VEHTP", "A")
            'Else
            '    If getFormDFValue(oForm, "U_CarrCd").Equals(goParent.goDICompany.CompanyName) Then
            '        setSharedData(oForm, "IDH_VEHTP", "A")
            '    End If
            'End If
            setSharedData(oForm, "IDH_VEHTP", Config.INSTANCE.getParameterWithDefault("DFLSRTYE", ""))
            ''MA End 19-01-2014 Issue#142 Skype chat Hannah 19-01-2014

            setSharedData(oForm, "IDH_WR1OT", "^WO")
            setSharedData(oForm, "SHOWCREATE", "TRUE")
            setSharedData(oForm, "SHOWACTIVITY", Config.Parameter("WOSVAC"))
            goParent.doOpenModalForm("IDHLOSCH", oForm)
            ''End With            
            'Else
            'doCreateLorrySearch(oForm)
            'End If
        End Sub

        Private Sub doCallLorrySearchCFL(ByVal oForm As SAPbouiCOM.Form)
            ''If Config.ParameterAsBool("VMUSENEW", False) = False Then
            'LORRY SEARCH
            With getFormMainDataSource(oForm)
                setSharedData(oForm, "IDH_VEHREG", .GetValue("U_Lorry", .Offset))

                If Config.INSTANCE.getParameterAsBool("WOVESEBC", False) = True Then
                    setSharedData(oForm, "IDH_VEHCUS", "")
                Else
                    setSharedData(oForm, "IDH_VEHCUS", getFormDFValue(oForm, "U_CustCd"))
                End If

                ''MA Start 19-01-2014 Issue#142 Skype chat Hannah 19-01-2014
                'If Config.INSTANCE.getParameterAsBool("WOVESEAS", False) = True Then
                '    setSharedData(oForm, "IDH_VEHTP", "A")
                'Else
                '    If getFormDFValue(oForm, "U_CarrCd").Equals(goParent.goDICompany.CompanyName) Then
                '        setSharedData(oForm, "IDH_VEHTP", "A")
                '    End If
                'End If
                setSharedData(oForm, "IDH_VEHTP", Config.INSTANCE.getParameterWithDefault("DFLSRTYE", ""))
                ''MA End 19-01-2014 Issue#142 Skype chat Hannah 19-01-2014


                'setSharedData(oForm, "IDH_VEHCUS", getFormDFValue( oForm, "U_CustCd"))
                'If getFormDFValue( oForm, "U_CarrCd").Equals(goParent.goDICompany.CompanyName) Then
                '    setSharedData(oForm, "IDH_VEHTP", "A")
                'End If

                setSharedData(oForm, "IDH_WR1OT", "^WO")
                setSharedData(oForm, "SHOWACTIVITY", Config.Parameter("WOSVAC"))
                setSharedData(oForm, "SHOWCREATE", "TRUE")
                goParent.doOpenModalForm("IDHLOSCH", oForm)
            End With
            ''Else
            ''doCreateLorrySearch(oForm)
            ''End If
        End Sub

        Private Sub doHandleVehicleSearchResult(ByVal oForm As SAPbouiCOM.Form, ByVal sLastButton As String)
            Dim oWOR As IDH_JOBSHD = thisWOR(oForm)

            Dim sDesc As String = getSharedData(oForm, "VEHDESC")
            If Not sDesc Is Nothing AndAlso sDesc.Length > 0 Then
                setFormDFValue(oForm, "U_VehTyp", sDesc)
            End If

            Dim sDriver As String = getSharedData(oForm, "DRIVER")
            If Not sDriver Is Nothing AndAlso sDriver.Length > 0 Then
                setFormDFValue(oForm, "U_Driver", sDriver)
            End If

            Dim sVType As String = getSharedData(oForm, "VTYPE")
            '                    If sVType.Length = 0 Then
            '                    	sVType = "S"
            '                    End If
            ''MA 15-01-2015
            ''20150323 - LPV - BEGIN - FIXED incorrect Lookup ID
            Dim sV_WR1ORD As String = getSharedData(oForm, "WR1TYPE") '"V_WR1ORD")
            If sV_WR1ORD Is Nothing OrElse sV_WR1ORD = "^DO" OrElse sV_WR1ORD = "DO" Then
                sV_WR1ORD = ""
            ElseIf sV_WR1ORD = "^WO" Then
                sV_WR1ORD = "WO"
            End If
            ''20150323 - LPV - END - FIXED incorrect Lookup ID
            ''MA 15-01-2015

            If oWOR.TRNCdDecodeRecord Is Nothing Then
                Dim sMarkAct As String = getSharedData(oForm, "MARKAC")
                If sMarkAct Is Nothing Then
                    sMarkAct = com.idh.bridge.lookups.FixedValues.getReqFocStatus()
                End If

                If sMarkAct.Equals("DOFOC") OrElse sMarkAct.Equals(com.idh.bridge.lookups.FixedValues.getStatusFoc()) Then
                    doSwitchCheckBox(oForm, "IDH_FOC", True)
                ElseIf sMarkAct.Equals("DOORD") Then
                    doSwitchCheckBox(oForm, "IDH_DOORD", True)
                ElseIf sMarkAct.Equals("DOARI") Then
                    doSwitchCheckBox(oForm, "IDH_DOARI", True)
                ElseIf sMarkAct.Equals("DOARIP") Then
                    doSwitchCheckBox(oForm, "IDH_DOARIP", True)
                ElseIf sMarkAct.Equals("DOAINV") Then
                    doSwitchCheckBox(oForm, "IDH_AINV", True)
                Else
                    'doSwitchCheckBox(oForm, "IDH", False)
                End If
            End If

            If sVType.Length = 0 AndAlso Config.Parameter("DFVEHTYP").Length > 0 Then
                sVType = Config.Parameter("DFVEHTYP")
            End If
            If sLastButton.Equals("IDH_CREATE") Then
                Try
                    Dim sReg As String

                    Dim sJob As String = getFormDFValue(oForm, "U_JobTp", True)

                    sReg = getSharedData(oForm, "REG")
                    If sReg Is Nothing OrElse sReg.Length = 0 Then
                        sReg = getFormDFValue(oForm, "U_Lorry", True)
                    End If

                    sReg = com.idh.bridge.utils.General.doFixReg(sReg)

                    setFormDFValue(oForm, "U_Lorry", sReg, False, True)
                    setFormDFValue(oForm, "U_LorryCd", "", False, True)
                    doSetFocus(oForm, "IDH_VEHREG")
                    setWFValue(oForm, "LastVehAct", "CREATE")
                    'setWFValue(oForm, "VehicleType", sVType)
                    oWOR.VehicleType = sVType
                    ''MA 15-01-2015
                    oWOR.VehicleWR1ORD = sV_WR1ORD
                    ''MA 15-01-2015
                Catch ex As Exception
                End Try
            Else
                setFormDFValue(oForm, "U_Lorry", getSharedData(oForm, "REG"), False, True)
                setFormDFValue(oForm, "U_LorryCd", getSharedData(oForm, "VEHCODE"), False, True)
                setWFValue(oForm, "LastVehAct", "SELECT")
            End If

            'Dim sMessage As String = ""
            Dim sMessageId As String = ""
            Dim sCarrierCd As String = getSharedData(oForm, "WCSCD")
            Dim bDoCarrier As Boolean = False
            Dim sSiteCd As String = getSharedData(oForm, "SUPCD")
            Dim bDoSite As Boolean = False
            Dim sCurrCarr As String = getFormDFValue(oForm, "U_CarrCd")
            Dim sCurrSite As String = getFormDFValue(oForm, "U_Tip")

            If sCurrCarr.Length = 0 Then
                bDoCarrier = True
            ElseIf Not sCarrierCd Is Nothing _
                AndAlso sCarrierCd.Length > 0 _
                AndAlso sCurrCarr.Equals(sCarrierCd) = False Then
                'sMessage = sMessage & "The Carrier will be changed." & Chr(10)
                sMessageId = "WORCC"
                bDoCarrier = True
            End If

            If sCurrSite.Length = 0 Then
                bDoSite = True
            ElseIf Not sSiteCd Is Nothing _
                AndAlso sSiteCd.Length > 0 _
                AndAlso sCurrSite.Equals(sSiteCd) = False Then
                'sMessage = sMessage & "The Disposal Site will be changed." & Chr(10)
                If sMessageId.Length > 0 Then
                    sMessageId = "WORCSC"
                Else
                    sMessageId = "WORSC"
                End If
                bDoSite = True
            End If

            '                    If sMessage.Length > 0 Then
            '                        If goParent.goApplication.M_essageBox(sMessage & Chr(10) & "Do you want to continue with the update." & Chr(10), 1, "Yes", "No") = 2 Then
            '                            bDoCarrier = False
            '                            bDoSite = False
            '                        End If
            '                    End If
            If sMessageId.Length > 0 Then
                If Messages.INSTANCE.doResourceMessageYNAsk(sMessageId, Nothing, 1) = 2 Then
                    bDoCarrier = False
                    bDoSite = False
                End If
            End If

            If bDoCarrier Then
                setFormDFValue(oForm, "U_CarrCd", sCarrierCd)
                setFormDFValue(oForm, "U_CarrNm", getSharedData(oForm, "WCSNAM"))
                '## Start 12-09-2013
                Dim oData As ArrayList = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sCarrierCd, "S")
                If oData IsNot Nothing AndAlso oData.Count > 12 Then
                    setFormDFValue(oForm, "U_SupRef", oData(12))
                End If
                '## End
            End If

            If bDoSite Then
                setFormDFValue(oForm, "U_Tip", sSiteCd)
                setFormDFValue(oForm, "U_TipNm", getSharedData(oForm, "SUPPLIER"))

            End If

            If bDoCarrier OrElse bDoSite Then
                oWOR.doGetAllPrices()
            End If

            doSetUpdate(oForm)
        End Sub
#End Region

#Region "New Lorry Search"
        'Private Sub doCreateLorrySearch(ByVal oForm As SAPbouiCOM.Form)
        '    Dim oOOForm As com.uBC.forms.fr3.VehicleM = New com.uBC.forms.fr3.VehicleM(Nothing, oForm.UniqueID, Nothing)
        '    oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doLorrySelection)
        '    oOOForm.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doLorryCancel)

        '    oOOForm.doShowModal(oForm)
        'End Sub

        'Public Function doLorrySelection(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
        '    Dim oForm As SAPbouiCOM.Form = oOForm.SBOParentForm
        '    Dim oOOForm As com.uBC.forms.fr3.VehicleM = oOForm
        '    Dim oVehMast As IDH_VEHMAS2 = oOOForm.VehicleMaster

        '    Dim oWOR As IDH_JOBSHD = thisWOR(oForm)

        '    Dim sDesc As String = oVehMast.U_VehDesc
        '    If Not sDesc Is Nothing AndAlso sDesc.Length > 0 Then
        '        setFormDFValue(oForm, "U_VehTyp", sDesc)
        '    End If

        '    Dim sDriver As String = oVehMast.U_Driver
        '    If Not sDriver Is Nothing AndAlso sDriver.Length > 0 Then
        '        setFormDFValue(oForm, "U_Driver", sDriver)
        '    End If

        '    Dim sVType As String = oVehMast.U_VehT
        '    If sVType.Length = 0 AndAlso Config.Parameter("DFVEHTYP").Length > 0 Then
        '        sVType = Config.Parameter("DFVEHTYP")
        '    End If
        '    'If sLastButton.Equals("IDH_CREATE") Then
        '    '    Try
        '    '        Dim sReg As String

        '    '        Dim sJob As String = getFormDFValue(oForm, "U_JobTp", True)

        '    '        sReg = getSharedData(oForm, "REG")
        '    '        If sReg Is Nothing OrElse sReg.Length = 0 Then
        '    '            sReg = getFormDFValue(oForm, "U_Lorry", True)
        '    '        End If

        '    '        sReg = com.idh.bridge.utils.General.doFixReg(sReg)

        '    '        setFormDFValue(oForm, "U_Lorry", sReg, False, True)
        '    '        setFormDFValue(oForm, "U_LorryCd", "", False, True)
        '    '        doSetFocus(oForm, "IDH_VEHREG")
        '    '        setWFValue(oForm, "LastVehAct", "CREATE")
        '    '        setWFValue(oForm, "VehicleType", sVType)
        '    '    Catch ex As Exception
        '    '    End Try
        '    'Else
        '    setFormDFValue(oForm, "U_Lorry", oVehMast.U_VehReg, False, True)
        '    setFormDFValue(oForm, "U_LorryCd", oVehMast.Code, False, True)
        '    setWFValue(oForm, "LastVehAct", "SELECT")
        '    'End If

        '    Dim sMessageId As String = ""
        '    Dim sCarrierCd As String = oVehMast.U_CCCrdCd
        '    Dim bDoCarrier As Boolean = False
        '    Dim sSiteCd As String = ""
        '    Dim bDoSite As Boolean = False
        '    Dim sCurrCarr As String = getFormDFValue(oForm, "U_CarrCd")
        '    Dim sCurrSite As String = getFormDFValue(oForm, "U_Tip")

        '    If sCurrCarr.Length = 0 Then
        '        bDoCarrier = True
        '    ElseIf Not sCarrierCd Is Nothing _
        '        AndAlso sCarrierCd.Length > 0 _
        '        AndAlso sCurrCarr.Equals(sCarrierCd) = False Then
        '        sMessageId = "WORCC"
        '        bDoCarrier = True
        '    End If

        '    If sCurrSite.Length = 0 Then
        '        bDoSite = True
        '    ElseIf Not sSiteCd Is Nothing _
        '        AndAlso sSiteCd.Length > 0 _
        '        AndAlso sCurrSite.Equals(sSiteCd) = False Then
        '        If sMessageId.Length > 0 Then
        '            sMessageId = "WORCSC"
        '        Else
        '            sMessageId = "WORSC"
        '        End If
        '        bDoSite = True
        '    End If

        '    If sMessageId.Length > 0 Then
        '        If goParent.doResourceMessageYNAsk(sMessageId, Nothing, 1) = 2 Then
        '            bDoCarrier = False
        '            bDoSite = False
        '        End If
        '    End If

        '    If bDoCarrier Then
        '        setFormDFValue(oForm, "U_CarrCd", sCarrierCd)
        '        setFormDFValue(oForm, "U_CarrNm", Config.INSTANCE.doGetBPName(sCarrierCd))
        '        '## Start 12-09-2013
        '        Dim oData As ArrayList = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sCarrierCd, "S")
        '        If oData IsNot Nothing AndAlso oData.Count > 12 Then
        '            setFormDFValue(oForm, "U_SupRef", oData(12))
        '        End If
        '        '## End
        '    End If

        '    If bDoSite Then
        '        setFormDFValue(oForm, "U_Tip", sSiteCd)
        '        setFormDFValue(oForm, "U_TipNm", getSharedData(oForm, "SUPPLIER"))
        '    End If

        '    If bDoCarrier OrElse bDoSite Then
        '        oWOR.doGetAllPrices()
        '    End If

        '    doSetUpdate(oForm)

        '    Return True
        'End Function

        'Public Function doLorryCancel(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
        '    Return True
        'End Function
#End Region

#Region "BP Additional Charges"
        Public Sub getBPAdditionalCharges(ByVal oForm As SAPbouiCOM.Form)
            If Config.INSTANCE.getParameterAsBool("USEBPACH", False) Then
                Dim oWOR As IDH_JOBSHD = thisWOR(oForm)
                Dim sBPCode As String = oWOR.U_CustCd
                Dim oBPAC As IDH_BPADCHG
                Dim oList As ArrayList
                oBPAC = New IDH_BPADCHG()
                oList = oBPAC.getBPAdditionalCharges(sBPCode, oWOR.U_JobTp)

                If Not oList Is Nothing AndAlso oList.Count > 0 Then
                    Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                    If oGrid IsNot Nothing Then
                        '20151123: New Logic covering both Mitie and ECW and any other client 
                        'have 2 configs, each for EMPTY and MATCH 
                        'Discuss with HG and HN
                        'Start - New Logic 
                        For Each oBPAC In oList
                            If oBPAC.U_JobTp.Length = 0 Then
                                'check flag 
                                If Config.INSTANCE.getParameterAsBool("BPACDFJT", True) Then
                                    'Check if the item is allready in the Grid and only adds it if it does not
                                    If oGrid.doIndexOfItem(oBPAC.U_ItemCode) < 0 Then
                                        Dim sCustCd As String = oWOR.F_CustomerCode
                                        Dim sCustNm As String = oWOR.F_CustomerName
                                        Dim sCustCurr As String = Config.INSTANCE.getValueFromBP(sCustCd, "Currency")

                                        Dim sSalesUOM As String '= oBPAC.U_UOM.ToString()
                                        If oBPAC.U_UOM.Equals(String.Empty) Then
                                            sSalesUOM = Config.INSTANCE.getParameterWithDefault("BPACDUOM", "ea")
                                        Else
                                            sSalesUOM = oBPAC.U_UOM
                                        End If

                                        Dim sSuppCd As String = "" 'getDFValue(oForm, msRowTable, "U_CongCd")
                                        Dim sSuppNm As String = "" 'getDFValue(oForm, msRowTable, "U_SCngNm")
                                        Dim sSuppCurr As String = ""
                                        Dim sEmpId As String = oBPAC.U_EmpId
                                        Dim sEmpNm As String = oBPAC.U_EmpNm ' Config.LookupTableField("OSLP", "SlpName", "SlpCode = '" + oBPAC.U_SlpCode.ToString() + "'").ToString()
                                        Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(sCustCd, oBPAC.U_ItemCode)
                                        Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(sSuppCd, oBPAC.U_ItemCode)
                                        Dim dCost As Double = 0

                                        oGrid.doAddExpense(oWOR.U_JobNr, oWOR.Code,
                                            sCustCd, sCustNm, sCustCurr,
                                            sSuppCd, sSuppNm, sSuppCurr,
                                            sEmpId, sEmpNm,
                                            oBPAC.U_ItemCode, oBPAC.U_ItemName,
                                            sSalesUOM, 1,
                                            dCost, 0,
                                            sCostVatGrp,
                                            sChrgVatGrp,
                                            False,
                                            False,
                                            False,
                                            True,
                                            "VARIABLE", "VARIABLE")
                                    End If
                                End If
                            Else
                                'check flag 
                                If Config.INSTANCE.getParameterAsBool("BPACMTJT", True) Then
                                    'Check if the item is allready in the Grid and only adds it if it does not
                                    If oGrid.doIndexOfItem(oBPAC.U_ItemCode) < 0 Then
                                        Dim sCustCd As String = oWOR.F_CustomerCode
                                        Dim sCustNm As String = oWOR.F_CustomerName
                                        Dim sCustCurr As String = Config.INSTANCE.getValueFromBP(sCustCd, "Currency")

                                        Dim sSalesUOM As String '= oBPAC.U_UOM.ToString()
                                        If oBPAC.U_UOM.Equals(String.Empty) Then
                                            sSalesUOM = Config.INSTANCE.getParameterWithDefault("BPACDUOM", "ea")
                                        Else
                                            sSalesUOM = oBPAC.U_UOM
                                        End If


                                        Dim sSuppCd As String = "" 'getDFValue(oForm, msRowTable, "U_CongCd")
                                        Dim sSuppNm As String = "" 'getDFValue(oForm, msRowTable, "U_SCngNm")
                                        Dim sSuppCurr As String = ""
                                        Dim sEmpId As String = oBPAC.U_EmpId
                                        Dim sEmpNm As String = oBPAC.U_EmpNm ' Config.LookupTableField("OSLP", "SlpName", "SlpCode = '" + oBPAC.U_SlpCode.ToString() + "'").ToString()
                                        Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(sCustCd, oBPAC.U_ItemCode)
                                        Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(sSuppCd, oBPAC.U_ItemCode)
                                        Dim dCost As Double = 0

                                        oGrid.doAddExpense(oWOR.U_JobNr, oWOR.Code,
                                            sCustCd, sCustNm, sCustCurr,
                                            sSuppCd, sSuppNm, sSuppCurr,
                                            sEmpId, sEmpNm,
                                            oBPAC.U_ItemCode, oBPAC.U_ItemName,
                                            sSalesUOM, 1,
                                            dCost, 0,
                                            sCostVatGrp,
                                            sChrgVatGrp,
                                            False,
                                            False,
                                            False,
                                            True,
                                            "VARIABLE", "VARIABLE")
                                    End If
                                End If
                            End If
                        Next
                        'End - new Logic 
                        If oList.Count > 0 Then
                            doWarnMess("BPs Additional charges added. Please review Additional Tab and update Ordered Quantity.", SAPbouiCOM.BoMessageTime.bmt_Short)
                        End If
                    End If
                End If
                oBPAC = Nothing
                oList = Nothing
                oWOR = Nothing
            End If

        End Sub

        Public Sub removeAutoAddBPAddCharges(ByVal oForm As SAPbouiCOM.Form)
            If Config.INSTANCE.getParameterAsBool("USEBPACH", False) Then
                Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                If oGrid IsNot Nothing Then
                    If oGrid.getRowCount() > 1 Then
                        ''20151118
                        ''Commented block as it was not deleting old rows for BP Additional Charges, 
                        ''added automatically by selection of a Job Type 
                        'Dim oWOR As IDH_JOBSHD = thisWOR(oForm)
                        'Dim sBPCode As String = oWOR.U_CustCd
                        'Dim oBPAC As IDH_BPADCHG
                        'Dim oList As ArrayList
                        'oBPAC = New IDH_BPADCHG()
                        'oList = oBPAC.getBPAdditionalCharges(sBPCode, oWOR.U_JobTp)

                        'If Not oList Is Nothing AndAlso oList.Count > 0 Then
                        '    For Each oBPAC In oList
                        '        If Not oBPAC.U_ItemCode Is Nothing AndAlso oBPAC.U_ItemCode.Length > 0 Then
                        '            'Remove Row
                        '            Dim iSearchIndx As Integer
                        '            iSearchIndx = oGrid.doIndexOfItem(oBPAC.U_ItemCode)
                        '            If iSearchIndx > -1 Then
                        '                oGrid.doRemoveRow(iSearchIndx)
                        '            End If
                        '        End If
                        '    Next
                        'End If
                        'oBPAC = Nothing
                        'oList = Nothing
                        'oWOR = Nothing

                        Dim iIndex As Integer = 0
                        Do
                            If oGrid.doGetFieldValue(IDH_WOADDEXP._BPAutoAdd, iIndex).Equals("Y") Then
                                oGrid.doRemoveRow(iIndex)
                            Else
                                iIndex = iIndex + 1
                            End If
                        Loop While iIndex < oGrid.getRowCount
                    End If
                    oGrid = Nothing
                End If
            End If
        End Sub

        Public Sub doUpdateAutoAddBPAddCharges(ByVal oForm As SAPbouiCOM.Form, ByVal sDateE As String)
            If Config.INSTANCE.getParameterAsBool("USEBPACH", False) Then
                Try
                    Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                    If oGrid IsNot Nothing Then
                        If Config.INSTANCE.getParameterAsBool("USEBPACH", False) AndAlso oGrid.getRowCount() > 1 Then
                            If Not sDateE Is Nothing AndAlso sDateE.Length > 0 Then
                                Dim sBPCode As String = getFormDFValue(oForm, "U_CustCd")
                                Dim sJobType As String = getFormDFValue(oForm, "U_JobTp")
                                Dim dTCharge As Double = getFormDFValue(oForm, "U_TCharge")

                                Dim oBPAC As IDH_BPADCHG
                                Dim oList As ArrayList
                                oBPAC = New IDH_BPADCHG()
                                oList = oBPAC.getBPAdditionalCharges(sBPCode, sJobType)

                                If Not oList Is Nothing AndAlso oList.Count > 0 Then
                                    For Each oBPAC In oList
                                        If Not oBPAC.U_ItemCode Is Nothing AndAlso oBPAC.U_ItemCode.Length > 0 Then
                                            Try
                                                Dim iSearchIndx As Integer
                                                iSearchIndx = oGrid.doIndexOfItem(oBPAC.U_ItemCode)
                                                If iSearchIndx > -1 AndAlso oGrid.doGetFieldValue("U_BPAutoAdd", iSearchIndx) = "Y" Then
                                                    oGrid.doSetFieldValue("U_ItmChrg", iSearchIndx, oBPAC.doCalculateAdditionalCharge(oBPAC.U_FrmParse, thisWOR(oForm)))
                                                End If
                                            Catch ex As Exception
                                                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "doUpdateAutoAddBPAddCharges: Error evaluating the additional charges expression!")
                                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXACE", {Nothing})
                                            End Try
                                        End If

                                    Next
                                End If
                                oBPAC = Nothing
                                oList = Nothing
                            End If
                        End If
                        oGrid = Nothing
                    End If
                Catch ex As Exception
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "doUpdateAutoAddBPAddCharges: Error updating additional item row charges.")
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXUAIR", {Nothing})
                End Try
            End If

        End Sub

#End Region
    End Class
End Namespace
