Imports System.IO
Imports System.Collections
Imports System

Namespace idh.forms
    Public Class SerialNrSearch
        Inherits IDHAddOns.idh.forms.Base

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHSSRCH", Nothing, -1, "Serial Nr Search.srf", False, True, False, "Serial Nr Search", load_Types.idh_LOAD_NORMAL )
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            oForm.Freeze(True)
            Dim oGrid As SAPbouiCOM.Grid
            Dim oEdit As SAPbouiCOM.EditText
            Dim oItem As SAPbouiCOM.Item

            Try
                oForm.DataSources.UserDataSources.Add("GRPCOD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20)
                oForm.DataSources.UserDataSources.Add("GRPNAM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30)
                oForm.DataSources.UserDataSources.Add("SERNR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30)
                oForm.DataSources.UserDataSources.Add("ITMCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30)
                oForm.Items.Item("IDH_GRPCOD").Specific.DataBind.SetBound(True, "", "GRPCOD")
                oForm.Items.Item("IDH_GRPNAM").Specific.DataBind.SetBound(True, "", "GRPNAM")
                oForm.Items.Item("IDH_SERNR").Specific.DataBind.SetBound(True, "", "SERNR")
                oForm.Items.Item("IDH_ITMCD").Specific.DataBind.SetBound(True, "", "ITMCD")

                If ghOldDialogParams.Contains(oForm.UniqueID) Then
                    Dim oData As ArrayList = ghOldDialogParams.Item(oForm.UniqueID)
                    If Not (oData Is Nothing) Then
                        If oData.Count > 1 AndAlso Not (oData.Item(1) Is Nothing) Then
                            oForm.DataSources.UserDataSources.Item("GRPCOD").Value = oData.Item(1)
                            Dim sGroup As String = oData.Item(1)
                            If sGroup.Length > 0 Then
                                oForm.Items.Item("IDH_LBLGC").Visible = False
                                oForm.Items.Item("IDH_LBLGN").Visible = False
                                oForm.Items.Item("IDH_GRPCOD").Visible = False
                                oForm.Items.Item("IDH_GRPNAM").Visible = False
                            End If
                        End If

                        If oData.Count > 2 AndAlso Not (oData.Item(2) Is Nothing) Then
                            oForm.DataSources.UserDataSources.Item("SERNR").Value = oData.Item(2)
                        End If
                        If oData.Count > 3 AndAlso Not (oData.Item(3) Is Nothing) Then
                            Dim sItemCode As String = oData.Item(3)
                            oForm.DataSources.UserDataSources.Item("ITMCD").Value = sItemCode
                            If sItemCode.Length > 0 Then
                                oForm.Items.Item("IDH_LBLGC").Visible = False
                                oForm.Items.Item("IDH_LBLGN").Visible = False
                                oForm.Items.Item("IDH_GRPCOD").Visible = False
                                oForm.Items.Item("IDH_GRPNAM").Visible = False
                            End If
                        End If
                    End If
                End If

                doSetAffectsMode(oForm)

                oForm.AutoManaged = True
                oForm.SupportedModes = SAPbouiCOM.BoFormMode.fm_OK_MODE
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE

                oForm.Items.Item("1").Specific.Caption = getTranslatedWord("Choose")

                oItem = oForm.Items.Add("ColExp", SAPbouiCOM.BoFormItemTypes.it_EDIT)
                oItem.Left = 0
                oItem.Top = 0
                oItem.Width = 0
                oItem.Height = 0

                oForm.DataSources.UserDataSources.Add("COLEXP", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                oItem.Enabled = False
                oEdit = oItem.Specific
                oEdit.DataBind.SetBound(True, "", "COLEXP")
                oForm.DataSources.UserDataSources.Item("COLEXP").ValueEx = "E"

                ' Add a Grid item to the form
                oItem = oForm.Items.Add("SearchGrid", SAPbouiCOM.BoFormItemTypes.it_GRID)
                ' Set the grid dimensions and position
                oItem.Left = 5
                oItem.Top = 40
                oItem.Width = 515
                oItem.Height = 350

                oItem.AffectsFormMode = False

                ' Set the grid data
                oGrid = oItem.Specific
                oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single

                oForm.DataSources.DataTables.Add("SearchData")
                oGrid.DataTable = oForm.DataSources.DataTables.Item("SearchData")

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
            oForm.Freeze(False)
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            Try
                Dim oGrid As SAPbouiCOM.Grid

                Dim sGRPCOD As String = oForm.Items.Item("IDH_GRPCOD").Specific.Value
                Dim sGRPNAM As String = oForm.Items.Item("IDH_GRPNAM").Specific.Value
                Dim sSERNR As String = oForm.Items.Item("IDH_SERNR").Specific.Value
                Dim sITMCD As String = oForm.Items.Item("IDH_ITMCD").Specific.Value

                Dim sQryFilter As String = ""

                If sGRPCOD.Length > 0 Then
                    sQryFilter = ""
                End If

                If sGRPCOD.Length > 0 Then
                    If sGRPCOD.EndsWith("*") OrElse sGRPCOD.EndsWith("%") Then sGRPCOD = sGRPCOD.Substring(0, sGRPCOD.Length - 1)
                    If sGRPCOD.Length > 0 AndAlso sGRPCOD.Chars(0) = "*" Then sGRPCOD = "%" & sGRPCOD.Remove(0, 1)
                    sQryFilter = sQryFilter & " AND i.ItmsGrpCod LIKE '" & sGRPCOD & "%'"
                End If

                If sGRPNAM.Length > 0 Then
                    If sGRPNAM.EndsWith("*") OrElse sGRPNAM.EndsWith("%") Then sGRPNAM = sGRPNAM.Substring(0, sGRPNAM.Length - 1)
                    If sGRPNAM.Length > 0 AndAlso sGRPNAM.Chars(0) = "*" Then sGRPNAM = "%" & sGRPNAM.Remove(0, 1)
                    sQryFilter = sQryFilter & " AND ig.ItmsGrpNam LIKE '" & sGRPNAM & "%'"
                End If

                If sSERNR.Length > 0 Then
                    If sSERNR.EndsWith("*") OrElse sSERNR.EndsWith("%") Then sSERNR = sSERNR.Substring(0, sSERNR.Length - 1)
                    If sSERNR.Length > 0 AndAlso sSERNR.Chars(0) = "*" Then sSERNR = "%" & sSERNR.Remove(0, 1)
                    sQryFilter = sQryFilter & " AND a.U_SerNum LIKE '" & sSERNR & "%'"
                End If

                If sITMCD.Length > 0 Then
                    If sITMCD.EndsWith("*") OrElse sITMCD.EndsWith("%") Then sITMCD = sITMCD.Substring(0, sITMCD.Length - 1)
                    If sITMCD.Length > 0 AndAlso sITMCD.Chars(0) = "*" Then sITMCD = "%" & sITMCD.Remove(0, 1)
                    sQryFilter = sQryFilter & " AND U_ItemCode like 'Z-" & sITMCD & "'"
                End If

                oGrid = oForm.Items.Item("SearchGrid").Specific
                Dim sQry As String = "select  " & _
                                     " ig.ItmsGrpNam,   a.U_Desc,   a.U_SerNum  " & _
                                     " from OITM i, OITB ig, [@BA_OAMD] a  " & _
                                     "where 	i.U_BA_IsFA = 'Y' " & _
                                  "  AND i.ItmsGrpCod = ig.ItmsGrpCod   " & _
                                  "  AND a.U_ItemCode = i.ItemCode " & _
                                  sQryFilter

                oGrid.DataTable.ExecuteQuery(sQry)
                If oGrid.DataTable.Rows.Count > 0 Then

                    oGrid.Columns.Item(0).Editable = False
                    oGrid.Columns.Item(0).TitleObject.Caption = getTranslatedWord("Group Name")
                    oGrid.Columns.Item(0).Width = 100

                    oGrid.Columns.Item(1).Editable = False
                    oGrid.Columns.Item(1).TitleObject.Caption = getTranslatedWord("Asset Name")
                    oGrid.Columns.Item(1).Width = 225

                    oGrid.Columns.Item(2).Editable = False
                    oGrid.Columns.Item(2).TitleObject.Caption = getTranslatedWord("Serial Nr")
                    oGrid.Columns.Item(2).Width = 145

                    Dim sColExpVal As String = oForm.DataSources.UserDataSources.Item("COLEXP").ValueEx
                    If sColExpVal = "E" Then
                        oGrid.Rows.ExpandAll()
                    Else
                        oGrid.Rows.CollapseAll()
                    End If

                    oGrid.CollapseLevel = 1

                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error loading Data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXLDBD", {Nothing})
            End Try
            oForm.Freeze(False)
        End Sub

        '*** Get the selected rows
        Private Sub doPrepareModalData(ByVal oForm As SAPbouiCOM.Form)
            Dim oGrid As SAPbouiCOM.Grid
            Dim iIndex As Integer
            Dim iSelected As Integer
            
            'Dim oCells As SAPbouiCOM.DataCells
            Dim oSelectedRows As SAPbouiCOM.SelectedRows
            Dim oData As New ArrayList

            oGrid = oForm.Items.Item("SearchGrid").Specific
            oSelectedRows = oGrid.Rows.SelectedRows

            For iIndex = 0 To oGrid.Rows.SelectedRows.Count - 1
                iSelected = oGrid.GetDataTableRowIndex(oSelectedRows.Item(iIndex, SAPbouiCOM.BoOrderType.ot_SelectionOrder))

                If iSelected > -1 Then
                    Dim sSearchMode As String = ""
                    If ghOldDialogParams.Contains(oForm.UniqueID) Then
                        Dim oData1 As ArrayList = ghOldDialogParams.Item(oForm.UniqueID)
                        If Not (oData1 Is Nothing) AndAlso oData1.Count > 1 Then
                            sSearchMode = oData1.Item(0)
                        End If
                    End If
                    oData.Add(sSearchMode) '0: Search Type
                    oData.Add(oGrid.DataTable.Columns.Item(2).Cells.Item(iSelected).Value) '1: Serial Number
                    oData.Add(oGrid.DataTable.Columns.Item(1).Cells.Item(iSelected).Value) '2: Description
                    Exit For
                End If
            Next
            doReturnFromModalBuffered(oForm, oData)
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If goParent.doCheckModal(oForm.UniqueID) = True Then
                    doPrepareModalData(oForm)
                End If
            End If
        End Sub

        Public Overrides Sub doButtonID2(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            doReturnFromCanceled(oForm, BubbleEvent)
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_GRPCOD" OrElse _
                       pVal.ItemUID = "IDH_GRPNAM" OrElse _
                       pVal.ItemUID = "IDH_SERNR" OrElse _
                       pVal.ItemUID = "IDH_ITMCD" Then
                        If pVal.ItemChanged Then
                            'doFilterData(oForm)
                            doLoadData(oForm)
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "SearchGrid" Then
                        If goParent.doCheckModal(oForm.UniqueID) = True Then
                            Dim oGrid As SAPbouiCOM.Grid
                            oGrid = oForm.Items.Item("SearchGrid").Specific
                            If Not (pVal.ColUID = "RowsHeader") Then
                                oGrid.Rows.SelectedRows.Add(pVal.Row)
                            End If
                            doPrepareModalData(oForm)
                        End If
                        BubbleEvent = False
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_CLICK Then
                If pVal.BeforeAction = True Then
                    If pVal.ItemUID = "SearchGrid" Then
                        If goParent.doCheckModal(oForm.UniqueID) = True Then
                            Dim oGrid As SAPbouiCOM.Grid
                            oGrid = oForm.Items.Item("SearchGrid").Specific
                            If Not (pVal.ColUID = "RowsHeader") Then
                                oGrid.Rows.SelectedRows.Add(pVal.Row)
                            End If
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = True Then
                    If pVal.ItemUID = "SearchGrid" Then
                        If pVal.ColUID = "RowsHeader" AndAlso pVal.Row = -1 Then
                            Dim oGrid As SAPbouiCOM.Grid
                            Dim sColExp As String = oForm.DataSources.UserDataSources.Item("COLEXP").ValueEx
                            oGrid = oForm.Items.Item("SearchGrid").Specific
                            If sColExp = "C" Then
                                oForm.DataSources.UserDataSources.Item("COLEXP").ValueEx = "E"
                                oGrid.Rows.ExpandAll()
                            Else
                                oForm.DataSources.UserDataSources.Item("COLEXP").ValueEx = "C"
                                oGrid.Rows.CollapseAll()
                            End If
                        End If
                        BubbleEvent = False
                    End If
                Else
                End If
            End If
            Return True
        End Function

        Public Overrides Sub doClose()
        End Sub

    End Class
End Namespace
