Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Namespace idh.forms
    Public Class WasteProposal
        Inherits IDHAddOns.idh.forms.Base

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_WASPRO", iMenuPosition, "Waste Proposal.srf", False, True, False)
        End Sub


        '*** Create Sub-Menu
        Protected Overrides Sub doCreateSubMenu()
        	doCreateFormMenu("IDHORD", "Waste Proposal")
        	
'            Dim oMenus As SAPbouiCOM.Menus
'            Dim oMenuItem As SAPbouiCOM.MenuItem
'            Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
'
'            oMenuItem = goParent.goApplication.Menus.Item("IDHORD")
'
'            oMenus = oMenuItem.SubMenus
'            oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
'            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING
'            oCreationPackage.UniqueID = gsType
'            oCreationPackage.String = "Waste Proposal"
'            oCreationPackage.Position = giMenuPosition
'
'            Try
'                oMenus.AddEx(oCreationPackage)
'            Catch ex As Exception
'            End Try
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
        End Sub

        '** Do the final actions to show the form
        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            oForm.Visible = True
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                oForm.Items.Item("IDH_TABGEN").AffectsFormMode = False
                oForm.Items.Item("IDH_TABDET").AffectsFormMode = False
                oForm.Items.Item("IDH_TABREM").AffectsFormMode = False
                oForm.Items.Item("IDH_TABATT").AffectsFormMode = False
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)

        End Sub

        '*** Validate the Header
        Private Function doValidateHeader(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Return True
        End Function

        Private Function doUpdateHeader(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Return True
        End Function

        Private Function doAddHeader(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Return True
        End Function

        Private Function doUpdateLines(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Return True
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Return True
        End Function

        '** Create a new Entry if the Add option is selected
        Private Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        '** Clears all the fields when going into the find mode.
        Private Sub doClearAll(ByVal oForm As SAPbouiCOM.Form)
        End Sub


        '** The Menu Event handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            Return True
        End Function

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCloseForm(oForm, BubbleEvent)
        End Sub

        Public Overrides Sub doClose()
        End Sub

        '** This method is called by the returning modal form to set the data before closing
        Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error handeling Modal Result - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub

        ''MODAL DIALOG
        ''*** Set the return data when called as a modal dialog 
        Private Sub doSetModalData(ByVal oForm As SAPbouiCOM.Form)
        End Sub
    End Class
End Namespace
