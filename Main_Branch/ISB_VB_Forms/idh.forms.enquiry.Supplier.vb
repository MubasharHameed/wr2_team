Imports System.IO
Imports System.Collections
Imports System

Namespace idh.forms.enquiry
    Public Class Supplier
        Inherits idh.forms.enquiry.Templ

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHSENQ", iMenuPosition, "Waste Supplier Enquiry")
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Waste Supplier Enquiry"
'        End Function

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.UpdateGrid)
            oGridN.doAddListField("State", "Progress", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Status", "Status", False, -1, Nothing, Nothing)

            oGridN.doAddListField("r.U_Tip", "Supplier", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Driver", "Driver", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_SupRef", "Supplier Ref No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("''", "PO No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_JobNr", "Order No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.Code", "Row No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_JobTp", "Order Type", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_RDate", "Req. Start", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ASDate", "Act. Start", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_AEDate", "Act EDate", False, -1, Nothing, Nothing)

            oGridN.doAddListField("r.U_ItmGrp", "Container Grp", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItemCd", "Container Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItemDsc", "Container Desc.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicNr", "Skip Lic. No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicExp", "Skip Exp.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicCh", "SLicCh", False, -1, Nothing, Nothing)

            oGridN.doAddListField("r.U_Lorry", "Vehicle Reg.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Driver", "Driver", False, -1, Nothing, Nothing)

            oGridN.doAddListField("r.U_Tip", "Supplier", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipWgt", "Sup. Wgt", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipCost", "TipCost", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CstWgt", "Cust. Wgt", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCharge", "TCharge", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CongCh", "CongCh", False, -1, Nothing, Nothing)

            oGridN.doAddListField("e.U_CardCd", "Customer Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_CardNM", "Customer Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_ZpCd", "Post Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_CntrNo", "Contract No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicSp", "SLicSp", False, 0, Nothing, Nothing)
            '..
            oGridN.doAddListField("r.U_RTime", "Req. Time From", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_RTimeT", "Req. Time To", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_ASTime", "Act. Time", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_AETime", "Act ETime", False, 0, "TIME", Nothing)
            '..
            oGridN.doAddListField("r.U_Price", "Price", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Discnt", "Discnt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TaxAmt", "Sales Vat Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_VtCostAmt", "Purchase Vat Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Serial", "Item Serial No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Total", "Total", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_VehTyp", "Vehicle", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BDate", "Booking Date", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BTime", "Booking Time", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipTot", "TipTot", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCTotal", "TCTotal", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DocNum", "DocNum", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DisAmt", "Discount Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WasCd", "Waste Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WasDsc", "Waste Description", False, 0, Nothing, Nothing)
        End Sub

        '*** This is where the search fields are set
        Protected Overrides Sub doSetFilterLayout(ByVal oForm As SAPbouiCOM.Form)
'            Dim iCount As Integer

            oForm.Freeze(True)
            Try
                oForm.Items.Item("IDH_LA1").Left = 170
                oForm.Items.Item("IDH_LA2").Left = 170
                oForm.Items.Item("IDH_REQSTF").Left = 250
                oForm.Items.Item("IDH_ACTSTF").Left = 250

                oForm.Items.Item("IDH_LB1").Left = 340
                oForm.Items.Item("IDH_LB2").Left = 340
                oForm.Items.Item("IDH_REQSTT").Left = 385
                oForm.Items.Item("IDH_ACTSTT").Left = 385

                oForm.Items.Item("IDH_LC1").Left = 5
                oForm.Items.Item("IDH_LC2").Left = 5
                oForm.Items.Item("IDH_CUST").Left = 80
                oForm.Items.Item("IDH_NAME").Left = 80

                oForm.Items.Item("IDH_CUST").Click()
            Catch ex As Exception
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace
