Imports System.IO
Imports System.Collections
Imports System

Imports IDHAddOns.idh.controls
Imports com.bridge
Imports com.idh.bridge
Imports com.idh.bridge.reports
Imports com.idh.utils.Conversions
Imports com.idh.utils
Imports com.idh.bridge.lookups
Imports WR1_Grids.idh.controls.grid
Imports com.idh.dbObjects
Imports com.idh.dbObjects.User
Imports com.idh.controls

Imports com.uBC.utils
Imports com.uBC.data
Imports com.idh.bridge.action
Imports com.idh.bridge.data
Imports com.idh.dbObjects.numbers
Imports com.idh.bridge.resources

Namespace idh.forms.orders
    Public Class Disposal
        Inherits idh.forms.orders.Tmpl
        Public miOldBackColor As Integer = -1
        Public miOldFrontColor As Integer = -1
        Public miBlankColor As Integer = 14930874

        Private miPriceLookupSkipBackCol As Integer = 14930874
        Private miDefaultBackColor As Integer = -99
        Public Function DefaultBackColor(ByRef oForm As SAPbouiCOM.Form) As Integer
            If miDefaultBackColor = -99 Then
                Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("IDH_WEI1")
                miDefaultBackColor = oItem.BackColor
            End If
            Return miDefaultBackColor
        End Function

        Enum cf_KeepData 'Clear Filter
            KEEP_NONE = 0
            KEEP_VEHICLE = 1
            KEEP_MDATA1 = 100
            KEEP_PRESELECT = 200
        End Enum

#Region "The Filed Arrays"
        Public Shared moAlwaysDisabledFields As String() = {
                  "IDH_BOOREF", "IDH_STATUS", "IDH_ITMGRC", "IDH_ITMGRN",
                  "IDH_CUSTOT", "IDH_SUBTOT", "IDH_TOTCOS",
                  "IDH_TAX", "IDH_ADDCHR",
                  "IDH_TOTAL", "IDH_SER1", "IDH_WDT1", "IDH_SER2", "IDH_WDT2",
                  "IDH_WEIB", "IDH_SERB", "IDH_WDTB",
                  "IDH_RSTAT", "IDH_CASHMT", "IDH_ROW", "IDH_PSTAT", "IDH_USER",
                  "IDH_TIPTOT", "IDH_ORDTOT", "IDH_PURTOT", "IDH_TAXO", "IDH_VALDED", "IDH_BOOKIN",
                  "IDH_ADDCOV", "IDH_ADDCHV", "IDH_ADDCO", "IDH_ADDCH", "IDH_TADDCH", "IDH_ADDCOS", "IDH_TADDCO",
                  "IDH_DCGCUR", "IDH_HCHCUR", "IDH_PCOCUR", "IDH_DCOCUR", "IDH_CCOCUR", "IDH_LCOCUR", "IDH_SAMREF", "IDH_SAMSTA"
        }

        Public Shared moBPFields As String() = {
            "IDH_CUST", "IDH_CUSTNM",
            "IDH_CARRIE", "IDH_CARNAM",
            "IDH_WPRODU", "IDH_WNAM",
            "IDH_DISSIT", "IDH_DISNAM"
        }
        Public Shared moLinkedWOFields As String() = {
            "IDH_WRORD", "IDH_WRROW", "IDH_CHKCA"
        }

        Public Shared moSearchFields As String() = {
            "IDH_JOBTTP", "IDH_VEHREG", "IDH_BOOREF", "IDH_WRORD", "IDH_WRROW", "IDH_WOCF"
        }

        'Public Shared moSearchFields As String() = { _
        '   "IDH_JOBTTP", "IDH_VEHREG", "IDH_CUST", "IDH_BOOREF", "IDH_WRORD", "IDH_WRROW", "IDH_WOCF", "IDH_CUSL" _
        '}

        '"IDH_WMLK"
        Shared moCostTippingIncommingItems() As String = {
            "IDH_PROCD", "IDH_PRONM", "IDH_PROCF",
            "IDH_PURWGT", "IDH_PURUOM", "IDH_PRCOST"
        }
        '"IDH_NOVAT" _

        Shared moCostTippingOutgoingItems() As String = {
            "IDH_TIPWEI", "IDH_PUOM", "IDH_TIPCOS"
         }

        Shared moDisposalSiteItems() As String = {
            "IDH_DISPCD", "IDH_DISPNM", "IDH_DISPCF",
            "IDH_SITAL", "IDH_SITAL2", "IDH_DISPAD",
            "IDH_DISSIT", "IDH_DISNAM", "IDH_SITL", "IDH_SITCON",
            "IDH_SITCRF", "IDH_SITADD", "IDH_SITAL",
            "IDH_SITSTR", "IDH_SITBLO", "IDH_SITCIT",
            "IDH_SITSTA", "IDH_SITPOS", "IDH_SITPHO"
            }

        Shared moChargeTippingIncomingItems() As String = {
            "IDH_CUST", "IDH_CUSTNM"
        }

        Shared moCostHaulageItems() As String = {
            "IDH_ORDQTY", "IDH_ORDCOS",
            "IDH_ADDEX", "IDH_ADDCOS"
        }

        Shared moSalesItems() As String = {
         "IDH_CUSWEI", "IDH_UOM", "IDH_CUSCHG", "IDH_DISCNT",
         "IDH_DSCPRC", "IDH_DOORD", "IDH_DOARI", "IDH_DOARIP", "IDH_AINV", "IDH_FOC"
        }

        Shared moFirstWeightItems() As String = {
         "IDH_WEI1", "IDH_ACC1", "IDH_BUF1", "IDH_TAR1"
        }

        Shared moSecondWeightItems() As String = {
         "IDH_WEI2", "IDH_ACC2", "IDH_BUF2", "IDH_TAR2"
        }

        Shared moReadWeightItems() As String = {
         "IDH_RDWGT"
        }

        Shared moAllWeightItems() As String = {
            "IDH_WEI1", "IDH_ACC1", "IDH_BUF1", "IDH_TAR1",
            "IDH_WEI2", "IDH_ACC2", "IDH_BUF2", "IDH_TAR2",
            "IDH_RDWGT", "IDH_USERE", "IDH_USEAU", "IDH_CUSCM", "IDH_WGTDED", "IDH_ADJWGT", "IDH_BPWGT"
        }

        Shared moMarketingCheckboxes() As String = {
            "IDH_AINV", "IDH_FOC", "IDH_DOORD", "IDH_DOARI", "IDH_DOARIP", "IDH_BOOKIN", "IDH_DOPO", "IDH_JRNL"
        }
#End Region

        Private msLastWeighBridgeId As String = "-1"
        Private Shared msDefaultAUOM As String = "ea"

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_DISPORD", "Disposal Order.srf", "DO", "@IDH_DISPORD", "@IDH_DISPROW", "Disposal Order", iMenuPosition, "Disposal Order")
            msNextNum = "DISPORD"
            'msNextNumPrefix = IDH_DISPORD.AUTONUMPREFIX

            msNextRowNum = "DISPROW"
            'msNextRowNumPrefix = IDH_DISPROW.AUTONUMPREFIX
        End Sub

        Protected Overrides Function getNextNumPrefix() As String
            Return IDH_DISPORD.AUTONUMPREFIX
        End Function

        Protected Overrides Function getNextRowNumPrefix() As String
            Return IDH_DISPROW.AUTONUMPREFIX
        End Function

        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
        End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            Dim sField As String

            sField = Config.INSTANCE.doGetProgressQueryStr("")
            oGridN.doAddListField(sField, "Progress", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Status", "Status", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_PStat", "Purchase", False, -1, Nothing, Nothing)
            oGridN.doAddListField("Code", "LineID", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_JobNr", "Job Entry", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_DISPROW._JobTp, "Order Type", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_RDate", "Req. Start", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_RTime", "Req. Time From", False, 0, "TIME", Nothing)
            oGridN.doAddListField("U_BDate", "Row Booking Date", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_BTime", "Row Booking Time", False, 0, "TIME", Nothing)
            oGridN.doAddListField("U_RTimeT", "Req. Time To", False, 0, "TIME", Nothing)
            oGridN.doAddListField("U_ASDate", "Act. Start", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ASTime", "Act. S-Time", False, -1, "TIME", Nothing)
            oGridN.doAddListField("U_AEDate", "Act End", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_AETime", "Act E-Time", False, -1, "TIME", Nothing)
            ''## MA Start 16-10-2014 
            oGridN.doAddListField("U_BPWgt", "BP Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_UseBPWgt", "Use BP Weight", False, 0, Nothing, Nothing)
            ''## MA End 16-10-2014 

            ''## MA Start 02-09-2014 Issue#406
            'oGridN.doAddListField("U_JbToBeDoneDt", "Job To Be Done Date", False, -1, Nothing, Nothing)
            'oGridN.doAddListField("U_JbToBeDoneTm", "Job To Be Done Time", False, -1, "TIME", Nothing)

            'oGridN.doAddListField("U_DrivrOnSitDt", "Driver On Site Date", False, 0, Nothing, Nothing)
            'oGridN.doAddListField("U_DrivrOnSitTm", "Driver On Site Time", False, 0, "TIME", Nothing)

            'oGridN.doAddListField("U_DrivrOfSitDt", "Driver Off Site Date", False, 0, Nothing, Nothing)
            'oGridN.doAddListField("U_DrivrOfSitTm", "Driver Off Site Time", False, 0, "TIME", Nothing)
            ''## MA Start 02-09-2014 Issue#406
            oGridN.doAddListField("U_ItmGrp", "Item Grp", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ItemCd", "Item Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ItemDsc", "Item Desc.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Serial", "Item Serial No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_SLicNr", "Skip Lic. No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_SLicExp", "Skip Exp.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Lorry", "Vehicle", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Driver", "Driver", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_TRLReg", "2nd Tare Id", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_TRLNM", "2nd Tare Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Tip", "Supplier", False, -1, Nothing, Nothing)

            ''Duplicate column added
            oGridN.doAddListField(IDH_DISPROW._SAddress, "Disp Address", False, 0, Nothing, Nothing)

            oGridN.doAddListField(IDH_DISPROW._SAddrsLN, "Disp Site Address LineNum", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ProWgt", "Producer Wgt", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_CstWgt", "Cust. Wgt", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_RdWgt", "Read Wgt", False, -1, Nothing, Nothing)
            oGridN.doAddListField(IDH_DISPROW._TRdWgt, "Sup. Read Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_PRdWgt", "Producer Read Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SAORD", "Sales Order", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_TIPPO", "Tipping PO", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Wei1", "Weighbridge Weigh 1", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Wei2", "Weighbridge Weigh 2", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_PCost", "Producer Cost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_PCTotal", "TipTot", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TCharge", "TCharge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TCTotal", "TCTotal", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CongCh", "CongCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_DocNum", "DocNum", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Price", "Price", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Discnt", "Discnt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Total", "Total", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SLicCh", "SLicCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SLicSp", "SLicSp", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_VehTyp", "Vehicle Type", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_DisAmt", "Discount Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TaxAmt", "Tax Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SupRef", "Supplier Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_WasCd", "Waste Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_WasDsc", "Waste Description", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Weight", "Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Quantity", "Order Quantity", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_JCost", "Order Cost", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_SAINV", "Sales Invoice", False, 0, Nothing, Nothing)
            'oGridN.doAddListField("U_SAORD", "Sales Order", False, 0, Nothing, Nothing)
            'oGridN.doAddListField("U_TIPPO", "Tipping PO", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TPCN", "Disposal Purchase Credit Note", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TCCN", "Disposal Purchase Credit Note", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_JOBPO", "Job PO", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CustCd", "Customer Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CarrCd", "CarrierCode", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CongCd", "Congestion Supplier Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TipNm", "Dispoal Site Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CustNm", "Customer Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CarrNm", "Carrier Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SLicNm", "Skip License Supplier Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SCngNm", "Congestion Supplier Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Dista", "Distance", False, 0, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Ser1", "Flash Serial 1", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Ser2", "Flash Serial 2", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_WDt1", "Weigh Date 1", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_WDt2", "Weigh Date 2", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_AddEx", "Additional Expences", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_CusQty", "Customer Qty", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_HlSQty", "Customer Qty", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CusChr", "Customer Charge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_UOM", "Sales UOM", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ProUOM", "Producer UOM", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_TipWgt", "Disposal Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_PUOM", "Purchase UOM", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TipCost", "Tipping Cost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TipTot", "Tipping Total", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_OrdWgt", "Order Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_DISPROW._CarWgt, "Order Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_OrdCost", "Order Cost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_OrdTot", "Order Total", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_WROrd", "WR Link Order", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_WRRow", "WR Link Order Row", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_LoadSht", "Load Sheet Number", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ExpLdWgt", "Expected Load Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_WASLIC", "Waste Lic. Reg. No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CntrNo", "Contract Number", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_PayMeth", "Payment Method", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_PayStat", "Payment Status", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_CCNum", "Credit Card", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_CCType", "CreditCard Type", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CCStat", "Credit Card Status", False, -1, Nothing, Nothing)

            oGridN.doAddListField("U_CCExp", "CCard Expiry", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CCIs", "CCard Issue #", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CCSec", "CCard Security Number", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CCHNum", "CCard House Number", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CCPCd", "CCard PostCode", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_CustRef", "Customer Ref No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_LorryCd", "Vehicle Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SLPO", "Purchase Order", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Covera", "Coverage String", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Comment", "Comments", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_IntComm", "Internal Comments", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_UseWgt", "Weight to Use", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_AUOMQt", "Overriding Qty", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_PAUOMQt", "Producer Overriding Qty", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_AUOM", "Overriding UOM", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_JobRmD", "Reminder Date", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_JobRmt", "Reminder Time", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_RemNot", "Reminder Message", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_RemCnt", "Reminder Count", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CoverHst", "Coverage Hist.", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_ProPO", "Producer Order.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ProCd", "Producer Code.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ProNm", "Producer Name.", False, 20, Nothing, Nothing)
            oGridN.doAddListField("U_ProGRPO", "Producer GR PO.", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_GRIn", "Goods Receipt In.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SODlvNot", "SO Delivery note", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_DISPROW._WHTRNF, "Stock Transfer number", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_User", "User Name.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Branch", "Branch.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TZone", "Tip Zone", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ContNr", "Container Number", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SealNr", "Seal Number", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ConNum", "Hazardous Waste Consignment Note Number", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_TAddCost", "Additional Cost incl. VAT.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TAddChrg", "Additional Charge incl. VAT.", False, 0, Nothing, Nothing)
            ''## MA Start 15-04-2014
            oGridN.doAddListField("U_TAddChVat", "Additional Cost VAT", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TAddCsVat", "Additional Charge VAT", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_AddCharge", "Additional Cost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_AddCost", "Additional Charge", False, 0, Nothing, Nothing)

            ''## MA End 15-04-2014
            oGridN.doAddListField("U_Obligated", "Obligated.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Origin", "Origin.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CustCs", "Compliancs Scheme", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_DISPROW._FTOrig, "Free Text Origin", False, 0, Nothing, Nothing)

            ''Duplicate column added
            ''oGridN.doAddListField(IDH_DISPROW._SAddress, "Disp Site Address", False, 0, Nothing, Nothing)


            oGridN.doAddListField("U_WastTNN", "Waste Transfer Note Number", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SiteRef", "Site Reference Number", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ExtWeig", "External Weighbridge Number", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_TChrgVtRt", "Tipping Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_HChrgVtRt", "Haulage Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TCostVtRt", "Tipping Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_PCostVtRt", "Supplier Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_HCostVtRt", "Haulage Vat Purchase Rate", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_TChrgVtGrp", "Tipping Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_HChrgVtGrp", "Haulage Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TCostVtGrp", "Tipping Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_PCostVtGrp", "Supplier Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_HCostVtGrp", "Haulage Vat Purchase Group", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_VtCostAmt", "Purchase Vat Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CarrReb", "Carrier Rebate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CustReb", "Customer Rebate", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_MANPRC", "Manual Prices Set", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_LnkPBI", "PBI Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_DPRRef", "Daily Profitability Report", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_MDChngd", "Changed Marketing Document", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_Sched", "Scheduled Job", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_USched", "Un-Scheduled", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_RowSta", "Row Status", False, -1, Nothing, Nothing)

            oGridN.doAddListField("U_WgtDed", "Weight Deduction", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_AdjWgt", "Adjusted Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ValDed", "Value Deduction", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_BookIn", "Book into Stock", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_IsTrl", "Trail Load", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_DISPROW._LckPrc, "Price Lock", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_DISPROW._TrnCode, "Transaction Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_DISPROW._AltWasDsc, "Alternative Waste Description", False, 0, Nothing, Nothing)

            'Multi Currency Pricing 
            oGridN.doAddListField(IDH_DISPROW._DispCgCc, "Disp. Charge Currency", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_DISPROW._CarrCgCc, "Carr. Charge Currency", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_DISPROW._PurcCoCc, "Purc. Cost Currency", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_DISPROW._DispCoCc, "Disp. Cost Currency", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_DISPROW._CarrCoCc, "Carr. Cost Currency", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_DISPROW._LiscCoCc, "Lisc. Cost Currency", False, 0, Nothing, Nothing)

            'WRRow2 (was missing to save/update prior 1021)
            oGridN.doAddListField(IDH_DISPROW._WRRow2, "WR Row2", False, 0, Nothing, Nothing)

            oGridN.doAddListField(IDH_DISPROW._Sample, "Sample", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_DISPROW._SampleRef, "Sample Ref", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_DISPROW._SampStatus, "Sample Status", False, 0, Nothing, Nothing)

        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
            'Dim oItem As SAPbouiCOM.Item
            'Dim oOpt As SAPbouiCOM.OptionBtn = Nothing
            'doAddUFCheck(oForm, "IDH_FOC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
            'doAddUFCheck(oForm, "IDH_DOORD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
            'doAddUFCheck(oForm, "IDH_DOARI", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
            'doAddUFCheck(oForm, "IDH_AINV", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
            'doAddUFCheck(oForm, "IDH_AVCONV", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
            'doAddUFCheck(oForm, "IDH_AVDOC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
            'doAddUFCheck(oForm, "IDH_DOPO", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
            'doAddUFCheck(oForm, "IDH_NOVAT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
            'oItem = doAddUFCheck(oForm, "IDH_DOARIP", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
            'doSetEnabled(oItem, False)
            'Dim oData As SAPbouiCOM.DBDataSource
            'oData = oForm.DataSources.DBDataSources.Add(msRowTable)
            'oData.InsertRecord(0)

            'doAddDF(oForm, msRowTable, "IDH_ROW", "Code")
            'doAddDF(oForm, msRowTable, "IDH_VEHREG", "U_Lorry")
            'doAddDF(oForm, msRowTable, "IDH_VEH", "U_VehTyp")
            'doAddDF(oForm, msRowTable, "IDH_DRIVER", "U_Driver")
            'doAddDF(oForm, msRowTable, "IDH_TRLReg", "U_TRLReg")

            'doAddDF(oForm, msRowTable, "IDH_BRANCH", "U_Branch")
            'doAddDF(oForm, msRowTable, "IDH_TZONE", "U_TZone")
            'doAddDF(oForm, msRowTable, "IDH_CONTNR", "U_ContNr")
            'doAddDF(oForm, msRowTable, "IDH_SEALNR", "U_SealNr")
            'doAddDF(oForm, msRowTable, "IDH_USER", "U_User")
            'doAddDF(oForm, msRowTable, "IDH_ONCS", "U_CustCs")

            'doAddDF(oForm, msRowTable, "IDH_TRLNM", "U_TRLNM")
            'doAddDF(oForm, msRowTable, "IDH_JOBTTP", IDH_DISPROW._JobTp)
            'doAddDF(oForm, msRowTable, "IDH_ITMCOD", "U_ItemCd")
            'doAddDF(oForm, msRowTable, "IDH_DESC", "U_ItemDsc")
            'doAddDF(oForm, msRowTable, "IDH_WASCL1", "U_WasCd")
            'doAddDF(oForm, msRowTable, "IDH_WASMAT", "U_WasDsc")
            'doAddDF(oForm, msRowTable, "IDH_ITMGRC", "U_ItmGrp")

            'doAddDF(oForm, msRowTable, "IDH_WEI1", "U_Wei1")
            'doAddDF(oForm, msRowTable, "IDH_WEI2", "U_Wei2")
            'doAddDF(oForm, msRowTable, "IDH_WDT1", "U_WDt1")
            'doAddDF(oForm, msRowTable, "IDH_WDT2", "U_WDt2")
            'doAddDF(oForm, msRowTable, "IDH_SER1", "U_Ser1")
            'doAddDF(oForm, msRowTable, "IDH_SER2", "U_Ser2")
            'doAddDF(oForm, msRowTable, "IDH_ADDEX", "U_AddEx")

            ''Mansoor 14-04-2014 Start
            ''doAddDF(oForm, msRowTable, "IDH_ADDCHR", "U_TAddChrg")
            ''doAddDF(oForm, msRowTable, "IDH_ADDCOS", "U_TAddCost")
            'doAddDF(oForm, msRowTable, "IDH_ADDCOS", "U_AddCost") ' Total Add charge WO VAT
            'doAddDF(oForm, msRowTable, "IDH_ADDCHR", "U_AddCharge") ' Total Add cost WO VAT

            'doAddDF(oForm, msRowTable, "IDH_ADDCOV", "U_TAddCsVat") ' Total Add charge VAT
            'doAddDF(oForm, msRowTable, "IDH_ADDCHV", "U_TAddChVat") ' Total Add cost VAT
            'doAddDF(oForm, msRowTable, "IDH_TADDCH", "U_TAddChrg") ' Total Add charge with VAT
            'doAddDF(oForm, msRowTable, "IDH_TADDCO", "U_TAddCost") ' Total Add cost with VAT
            ''End 14-04-2014            
            'doAddDF(oForm, msRowTable, "IDH_WRORD", "U_WROrd")
            'doAddDF(oForm, msRowTable, "IDH_WRROW", "U_WRRow")
            'doAddDF(oForm, msRowTable, "IDH_LOADSH", "U_LoadSht")
            'doAddDF(oForm, msRowTable, "IDH_EXPLDW", IDH_DISPROW._ExpLdWgt)

            ''Sales Fields
            'doAddDF(oForm, msRowTable, "IDH_UOM", "U_UOM")
            'doAddDF(oForm, msRowTable, "IDH_CUSCM", "U_AUOMQt")
            'doAddDF(oForm, msRowTable, "IDH_RSTAT", "U_Status")
            'doAddDF(oForm, msRowTable, "IDH_CASHMT", "U_PayMeth")
            'doAddDF(oForm, msRowTable, "IDH_WGTDED", "U_WgtDed")
            'doAddDF(oForm, msRowTable, "IDH_ADJWGT", "U_AdjWgt")
            'doAddDF(oForm, msRowTable, "IDH_VALDED", "U_ValDed")
            'doAddDF(oForm, msRowTable, "IDH_BOKSTA", "U_BookIn")
            'doAddDF(oForm, msRowTable, "IDH_TWGTDE", "U_WgtDed")
            'doAddDF(oForm, msRowTable, "IDH_TVALDE", "U_ValDed")
            'doAddDF(oForm, msRowTable, "IDH_CUSWEI", "U_CstWgt")
            'doAddDF(oForm, msRowTable, "IDH_RDWGT", "U_RdWgt")
            'doAddDF(oForm, msRowTable, "IDH_CUSCHG", "U_TCharge")
            'doAddDF(oForm, msRowTable, "IDH_CUSTOT", "U_TCTotal")
            'doAddDF(oForm, msRowTable, "IDH_DISCNT", "U_Discnt")
            'doAddDF(oForm, msRowTable, "IDH_DSCPRC", "U_DisAmt")
            'doAddDF(oForm, msRowTable, "IDH_TAX", "U_TaxAmt")
            'doAddDF(oForm, msRowTable, "IDH_TAXO", "U_VtCostAmt")
            'doAddDF(oForm, msRowTable, "IDH_TOTAL", "U_Total")
            ''Purchase Fields
            'doAddDF(oForm, msRowTable, "IDH_PSTAT", "U_PStat")
            'doAddDF(oForm, msRowTable, "IDH_PURUOM", "U_ProUOM")
            'doAddDF(oForm, msRowTable, "IDH_PURWGT", "U_ProWgt")
            'doAddDF(oForm, msRowTable, "IDH_PRCOST", "U_PCost")
            'doAddDF(oForm, msRowTable, "IDH_PURTOT", "U_PCTotal")
            'doAddDF(oForm, msRowTable, "IDH_TOTCOS", "U_JCost")
            'doAddDF(oForm, msRowTable, "IDH_TIPWEI", "U_TipWgt")
            'doAddDF(oForm, msRowTable, "IDH_PUOM", "U_PUOM")
            'doAddDF(oForm, msRowTable, "IDH_TIPCOS", "U_TipCost")
            'doAddDF(oForm, msRowTable, "IDH_TIPTOT", "U_TipTot")
            'doAddDF(oForm, msRowTable, "IDH_ORDQTY", "U_OrdWgt")
            'doAddDF(oForm, msRowTable, "IDH_ORDCOS", "U_OrdCost")
            'doAddDF(oForm, msRowTable, "IDH_ORDTOT", "U_OrdTot")
            ''## MA Start 08-08-2014 Issue#: 313
            ''doAddDF(oForm, msRowTable, "IDH_JOBDAT", "U_BDate")
            ''## MA End 08-08-2014 Issue#: 313
            'doAddUFCheck(oForm, "IDH_BOOKIN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")

            ''CHECKBOX
            'doAddDF(oForm, msRowTable, "IDH_ISTRL", "U_IsTrl")

            'Dim oCheck As SAPbouiCOM.CheckBox
            'oCheck = oForm.Items.Item("IDH_ISTRL").Specific
            'oCheck.ValOff = "N"
            'oCheck.ValOn = "Y"
            ''doAddDBCheck(oForm,

            ''Additional Producer Field

            ''doAddDF(oForm, msRowTable, "IDH_PROCD", "U_ProCd")
            ''doAddDF(oForm, msRowTable, "IDH_PRONM", "U_ProNm")
            'doAddDF(oForm, "@IDH_DISPORD", "IDH_PROCD", "U_PCardCd")

            'doAddDF(oForm, "@IDH_DISPORD", "IDH_PRONM", "U_PCardNM")

            ''Additional Disposal Site Field
            'doAddDF(oForm, "@IDH_DISPORD", "IDH_DISPCD", "U_SCardCd")
            'doAddDF(oForm, "@IDH_DISPORD", "IDH_DISPNM", "U_SCardNM")
            'doAddDF(oForm, msRowTable, "IDH_DISPAD", IDH_DISPROW._SAddress)

            'doAddDF(oForm, msRowTable, "IDH_RORIGI", "U_Origin")
            'doAddDF(oForm, msRowTable, "IDH_OBLED", "U_Obligated")
            'doAddDF(oForm, msRowTable, "IDH_CORIGI", "U_Origin")

            'doAddDF(oForm, msRowTable, "IDH_WASTTN", "U_WastTNN")
            ''doAddDF(oForm, msRowTable, "IDH_HAZCN", "U_HazWCNN")
            'doAddDF(oForm, msRowTable, "IDH_HAZCN", "U_ConNum")
            'doAddDF(oForm, msRowTable, "IDH_SITREF", "U_SiteRef")

            'doAddDF(oForm, msRowTable, "IDH_EXTWEI", "U_ExtWeig")

            'doAddUF(oForm, "IDH_TRLTar", SAPbouiCOM.BoDataType.dt_MEASURE, 10) ', "VM.IDH_TRLTar
            'doAddUF(oForm, "IDH_TARWEI", SAPbouiCOM.BoDataType.dt_MEASURE, 10) ', "VM.U_Tarre
            'doAddUF(oForm, "IDH_SUBTOT", SAPbouiCOM.BoDataType.dt_PRICE, 10)
            'doAddUF(oForm, "IDH_ITMGRN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30)

            'doAddUF(oForm, "IDH_WEIB", SAPbouiCOM.BoDataType.dt_MEASURE, 10)
            'doAddUF(oForm, "IDH_SERB", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30)
            'doAddUF(oForm, "IDH_WDTB", SAPbouiCOM.BoDataType.dt_DATE, 10)
            'doAddUF(oForm, "IDH_ADDCO", SAPbouiCOM.BoDataType.dt_PRICE, 10, False, False)
            'doAddUF(oForm, "IDH_ADDCH", SAPbouiCOM.BoDataType.dt_PRICE, 10, False, False)

            'oItem = doAddUF(oForm, "IDH_WEIG", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 10)
            'oItem.Specific.FontSize = 25

            'oItem = doAddUF(oForm, "IDH_WEIBRG", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, False, False)
            'oItem.DisplayDesc = True

            'oForm.Items.Item("IDH_WEIGH").AffectsFormMode = False

            ''oForm.Items.Item("IDH_CIP").Visible = False

            ''OnTime 32756: WR1_NewDev_Planning_BOOKING IN STOCK
            ''setting tab mode of forms
            'oForm.Items.Item("IDH_TABDED").AffectsFormMode = False 'Deduction TAB
            'oForm.Items.Item("IDH_TABACT").AffectsFormMode = False 'Activity TAB

            'oForm.Items.Item("IDH_ITLK").Specific().LinkedObject = SAPbouiCOM.BoLinkedObject.lf_Items
            'oForm.Items.Item("IDH_WMLK").Specific().LinkedObject = SAPbouiCOM.BoLinkedObject.lf_Items
            'oForm.Items.Item("IDH_PROLN").Specific().LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner
            'oForm.Items.Item("IDH_CSLK").Specific().LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner
            'oForm.Items.Item("IDH_DISPLN").Specific.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

            'oItem = oForm.Items.Item("IDH_ADDGRD")
            'Dim oAddGrid As UpdateGrid = WR1_Grids.idh.controls.grid.AdditionalExpenses.doAddToForm(Me, oForm, "ADDGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, 11, 11)
            'oAddGrid.getSBOItem().AffectsFormMode = False

            ''oItem = oForm.Items.Item("IDH_DEDGRD")
            ''idh.controls.grid.Deductions.doAddToForm(Me, oForm, "DEDGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, 12, 12)
            'Dim oDeductions As IDH_DODEDUCT = New IDH_DODEDUCT(Me, oForm)
            'Dim oDedGrid As DBOGrid = New DBOGrid(Me, oForm, "IDH_DEDGRD", "DEDGRID", oDeductions)
            'oDedGrid.getSBOItem().AffectsFormMode = False
            'oDedGrid.doSetDeleteActive(True)
            'setWFValue(oForm, "DEDGRID", oDedGrid)

            ''Add Activity Grid

            'Try
            '    doADDActivity(oForm)
            'Catch ex As Exception
            '    com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "doBeforeLoadData")

            'End Try

            Dim oOpt As SAPbouiCOM.OptionBtn = Nothing
            oOpt = oForm.Items.Item("IDH_USERE").Specific
            oOpt.ValOn = "1"
            oOpt.ValOff = "0"
            oOpt.Selected = True
            doAddDF(oForm, msRowTable, "IDH_USERE", "U_UseWgt")

            oOpt = oForm.Items.Item("IDH_USEAU").Specific
            oOpt.ValOn = "2"
            oOpt.GroupWith("IDH_USERE")
        End Sub

        Private Sub doFillObligatedCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDH_OBLED")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = oItem.Specific
            doClearValidValues(oCombo.ValidValues)

            Dim sWord As String

            sWord = getTranslatedWord("None")
            oCombo.ValidValues.Add("", sWord)


            sWord = Config.Parameter("OBTRUE")
            If sWord Is Nothing OrElse sWord.Length = 0 Then
                sWord = getTranslatedWord("Obligated")
            End If
            oCombo.ValidValues.Add(sWord, sWord)


            sWord = Config.Parameter("OBFALSE")
            If sWord Is Nothing OrElse sWord.Length = 0 Then
                sWord = getTranslatedWord("Non-Obligated")
            End If
            oCombo.ValidValues.Add(sWord, sWord)
        End Sub

        Private Sub doFillAlternateItemDescription(ByVal oForm As SAPbouiCOM.Form, Optional ByVal sAltWasDsc As String = "", Optional ByVal sClearList As Boolean = False)
            If Config.ParamaterWithDefault("FLALTITM", True) = "FALSE" Then
                oForm.Items.Item("423").Visible = False 'Label control 
                oForm.Items.Item("IDH_ALTITM").Visible = False 'Combobox control 
                Exit Sub
            End If

            Dim sItemCode As String = getDFValue(oForm, msRowTable, "U_WasCd")
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDH_ALTITM")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = oItem.Specific

            If Not sClearList Then
                doFillCombo(oForm, oCombo, "[@IDH_ITMDESC]", "Code", "U_ItemAltName", "U_ItemCode = '" + sItemCode + "'", Nothing, "", "Select Alternative ItmDescrp.")
                If sAltWasDsc.Length > 0 Then
                    oCombo.Select(sAltWasDsc, SAPbouiCOM.BoSearchKey.psk_ByValue)
                Else
                    oCombo.Select("", SAPbouiCOM.BoSearchKey.psk_ByValue)
                End If
            Else
                doClearValidValues(oCombo.ValidValues)
                oCombo.ValidValues.Add("", "Select Alternative ItmDescrp.")
                oCombo.Select("", SAPbouiCOM.BoSearchKey.psk_ByValue)
            End If
        End Sub

        '** Do the final form steps to show after loaddata
        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            If (oDOR IsNot Nothing AndAlso oDOR.MarketingMode = Config.MarketingMode.PO) Then
                'If getWFValue(oForm, "FormMode") = "PO" Then
                oForm.Items.Item("IDH_FOC").Visible = False
            Else
                Dim sShowFreeCharge As String

                sShowFreeCharge = Config.Parameter("MASFCD")
                setVisible(oForm, "IDH_FOC", sShowFreeCharge.ToUpper.Equals("TRUE"))
            End If

            'Setting Show Row Hitory button on Disposal Order Screen
            'depending if the logged in user is a 'SuperUser' or not
            'Dim oItem As SAPbouiCOM.Item
            'oItem = oForm.Items.Item("IDH_ROWHTR")
            'oItem.Visible = Config.INSTANCE.doGetIsSupperUser()
            '## MA Start 11-07-2014 ' Issue no 299
            'setVisible(oForm, "IDH_ROWHTR", False)
            setVisible(oForm, "IDH_ROWHTR", Config.INSTANCE.doGetIsSupperUser())
            '## MA End 11-07-2014 ' Issue no 299
            '## MA Start 18-09-2014
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_MANUALDOWEIGHTENTERY) = False Then
                'moAlwaysDisabledFields = General.doAppendToArray(moAlwaysDisabledFields, "IDH_WEI1")
                'Dim oWeightItems(1) As String '= {"IDH_WEI1", "IDH_WEI2"}
                Dim sWeightItemListToDiable As String = Config.INSTANCE.getParameter("DISBLCTR")
                If sWeightItemListToDiable IsNot Nothing Then
                    If System.Array.IndexOf(moAlwaysDisabledFields, "IDH_WEI1") = -1 AndAlso sWeightItemListToDiable.IndexOf("IDH_WEI1") > -1 Then
                        moAlwaysDisabledFields = General.doAppendToArray(moAlwaysDisabledFields, "IDH_WEI1")
                    End If
                    If System.Array.IndexOf(moAlwaysDisabledFields, "IDH_WEI2") = -1 AndAlso sWeightItemListToDiable.IndexOf("IDH_WEI2") > -1 Then
                        moAlwaysDisabledFields = General.doAppendToArray(moAlwaysDisabledFields, "IDH_WEI2")
                    End If

                    If System.Array.IndexOf(moAlwaysDisabledFields, "IDH_TARWEI") = -1 AndAlso sWeightItemListToDiable.IndexOf("IDH_TARWEI") > -1 Then
                        moAlwaysDisabledFields = General.doAppendToArray(moAlwaysDisabledFields, "IDH_TARWEI")
                    End If
                    If System.Array.IndexOf(moAlwaysDisabledFields, "IDH_TRLTar") = -1 AndAlso sWeightItemListToDiable.IndexOf("IDH_TRLTar") > -1 Then

                        moAlwaysDisabledFields = General.doAppendToArray(moAlwaysDisabledFields, "IDH_TRLTar")
                    End If
                    ''MA Start 04-02-2015 Issue#554
                    If System.Array.IndexOf(moAlwaysDisabledFields, "IDH_TRWTE1") = -1 AndAlso sWeightItemListToDiable.IndexOf("IDH_TRWTE1") > -1 Then
                        moAlwaysDisabledFields = General.doAppendToArray(moAlwaysDisabledFields, "IDH_TRWTE1")
                    End If
                    If System.Array.IndexOf(moAlwaysDisabledFields, "IDH_TRWTE2") = -1 AndAlso sWeightItemListToDiable.IndexOf("IDH_TRWTE2") > -1 Then
                        moAlwaysDisabledFields = General.doAppendToArray(moAlwaysDisabledFields, "IDH_TRWTE2")
                    End If
                    ''MA End 04-02-2015 Issue#554
                    If System.Array.IndexOf(moAlwaysDisabledFields, "IDH_RDWGT") = -1 AndAlso sWeightItemListToDiable.IndexOf("IDH_RDWGT") > -1 Then
                        moAlwaysDisabledFields = General.doAppendToArray(moAlwaysDisabledFields, "IDH_RDWGT")
                    End If
                    If System.Array.IndexOf(moAlwaysDisabledFields, "IDH_CUSCM") = -1 AndAlso sWeightItemListToDiable.IndexOf("IDH_CUSCM") > -1 Then
                        moAlwaysDisabledFields = General.doAppendToArray(moAlwaysDisabledFields, "IDH_CUSCM")
                    End If
                    If System.Array.IndexOf(moAlwaysDisabledFields, "IDH_WGTDED") = -1 AndAlso sWeightItemListToDiable.IndexOf("IDH_WGTDED") > -1 Then
                        moAlwaysDisabledFields = General.doAppendToArray(moAlwaysDisabledFields, "IDH_WGTDED")
                    End If
                    If System.Array.IndexOf(moAlwaysDisabledFields, "IDH_ADJWGT") = -1 AndAlso sWeightItemListToDiable.IndexOf("IDH_ADJWGT") > -1 Then
                        moAlwaysDisabledFields = General.doAppendToArray(moAlwaysDisabledFields, "IDH_ADJWGT")
                    End If
                    If System.Array.IndexOf(moAlwaysDisabledFields, "IDH_CUSWEI") = -1 AndAlso sWeightItemListToDiable.IndexOf("IDH_CUSWEI") > -1 Then
                        moAlwaysDisabledFields = General.doAppendToArray(moAlwaysDisabledFields, "IDH_CUSWEI")
                    End If
                    If System.Array.IndexOf(moAlwaysDisabledFields, "IDH_BPWGT") = -1 AndAlso sWeightItemListToDiable.IndexOf("IDH_BPWGT") > -1 Then
                        moAlwaysDisabledFields = General.doAppendToArray(moAlwaysDisabledFields, "IDH_BPWGT")
                    End If
                End If
            End If
            '## MA End 18-09-2014

            MyBase.doFinalizeShow(oForm)
        End Sub

        Private Sub doFillJobTypes(ByVal oForm As SAPbouiCOM.Form)
            ''GET THE JOBTYPES
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Dim sQry As String
            Dim oItem As SAPbouiCOM.Item
            Dim oJobType As SAPbouiCOM.ComboBox
            Dim sGrp As String = getDFValue(oForm, msRowTable, "U_ItmGrp")

            Try
                If sGrp Is Nothing OrElse sGrp.Length = 0 Then
                    doSetItemGroup(oForm)
                    sGrp = getDFValue(oForm, msRowTable, "U_ItmGrp")
                End If
                If sGrp Is Nothing OrElse sGrp.Length = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("User: Item Group must be set.", "Item Group must be set.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Item Group must be set.", "ERUSGENS", {com.idh.bridge.Translation.getTranslatedWord("Item Group")})
                End If

                sQry = "select U_ItemGrp, U_JobTp, U_Seq,  U_Billa " &
                        " from [@IDH_JOBTYPE] " &
                        " where U_ItemGrp = '" & sGrp & "'" &
                        " ORDER BY U_ItemGrp, U_JobTp"

                oRecordSet = goParent.goDB.doSelectQuery(sQry)
                If oRecordSet.RecordCount > 0 Then
                    oItem = oForm.Items.Item("IDH_JOBTTP")
                    oItem.DisplayDesc = True
                    oJobType = oItem.Specific

                    doClearValidValues(oJobType.ValidValues)

                    Dim iCount As Integer
                    Dim oCheck As ArrayList = New ArrayList
                    Dim sKey As String
                    Dim sValue As String
                    For iCount = 0 To oRecordSet.RecordCount - 1
                        sKey = oRecordSet.Fields.Item(1).Value
                        sValue = oRecordSet.Fields.Item(1).Value
                        Try
                            oJobType.ValidValues.Add(sKey, sValue)
                        Catch ex As Exception
                        End Try
                        oRecordSet.MoveNext()
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the job types.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Job Types")})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
            End Try
        End Sub

        Private Sub doUOMCombos(ByVal oForm As SAPbouiCOM.Form)
            FillCombos.UOMCombo(FillCombos.getCombo(oForm, "IDH_PUOM"))
            FillCombos.UOMCombo(FillCombos.getCombo(oForm, "IDH_PURUOM"))
            FillCombos.UOMCombo(FillCombos.getCombo(oForm, "IDH_UOM"))

            'doFillCombo(oForm, "IDH_PUOM", "OWGT", "UnitDisply", "UnitName")
            'doFillCombo(oForm, "IDH_PURUOM", "OWGT", "UnitDisply", "UnitName")
            'doFillCombo(oForm, "IDH_UOM", "OWGT", "UnitDisply", "UnitName")
        End Sub

        Protected Function thisDOR(ByVal oForm As SAPbouiCOM.Form) As IDH_DISPROW
            Dim oDOR As IDH_DISPROW = getWFValue(oForm, "DOR")
            If oDOR Is Nothing Then
                oDOR = createDOR(oForm)
            End If
            Return oDOR
        End Function

        'Private Function thisDOR(ByVal oForm As SAPbouiCOM.Form) As IDH_DISPROW
        '    Return getWFValue(oForm, "DOR")
        'End Function
        Private Function createDOR(ByVal oForm As SAPbouiCOM.Form) As IDH_DISPROW
            Dim oDOR As IDH_DISPROW = createDOR(oForm, thisDO(oForm))
            oDOR.MustLoadChildren = False
            Return oDOR
        End Function

        Private Function createDOR(ByVal oForm As SAPbouiCOM.Form, ByVal oDO As IDH_DISPORD) As IDH_DISPROW
            'If oDO Is Nothing Then
            'oDO = createDO(oForm)
            'End If

            Dim oDOR As IDH_DISPROW = New IDH_DISPROW(Nothing, oForm, oDO)
            oDOR.MustLoadChildren = False
            oDOR.SkipPriceLookup = Config.INSTANCE.SkipPriceLookup

            If oDOR.AdditionalExpenses IsNot Nothing Then
                oDOR.AdditionalExpenses.SBOFormGUIOnly = oForm
            End If

            ''Use this to block the duplicate Additional Items Lookup
            oDOR.DoAdditionalCharges = False

            oDOR.Handler_ChargeTotalsCalculated = New IDH_DISPROW.et_ExternalTrigger(AddressOf doHandleChargeTotalsCalculated)
            oDOR.Handler_CostTotalsCalculated = New IDH_DISPROW.et_ExternalTrigger(AddressOf doHandleCostTotalsCalculated)

            oDOR.Handler_ChargePricesChanged = New IDH_DISPROW.et_ExternalTrigger(AddressOf doHandleChargePricesChanged)
            oDOR.Handler_CostTippingPricesChanged = New IDH_DISPROW.et_ExternalTrigger(AddressOf doHandleCostTippingPricesChanged)
            oDOR.Handler_CostHaulagePricesChanged = New IDH_DISPROW.et_ExternalTrigger(AddressOf doHandleCostHaulagePricesChanged)
            oDOR.Handler_CostProducerPricesChanged = New IDH_DISPROW.et_ExternalTrigger(AddressOf doHandleCostProducerPricesChanged)

            oDOR.Handler_ExternalYNOption = New IDH_DISPROW.et_ExternalYNOption(AddressOf doHandlerYNOption)
            'oDOR.Handler_GetChequeNumber = New IDH_DISPROW.et_SetChequeNumberTrigger(AddressOf doGetChequeNumber)

            oDOR.Handler_CheckPaymentReportTrigger = New IDH_DISPROW.et_ExternalTrigger(AddressOf doHandleCheckReport)

            oDOR.Handler_ChargeTipPriceLookupSkipped = New IDH_DISPROW.et_ExternalTriggerWithData(AddressOf doHandleChargeTipPriceLookupSkipped)
            oDOR.Handler_ChargeHaulagePriceLookupSkipped = New IDH_DISPROW.et_ExternalTriggerWithData(AddressOf doHandleChargeHaulagePriceLookupSkipped)
            oDOR.Handler_CostTipPriceLookupSkipped = New IDH_DISPROW.et_ExternalTriggerWithData(AddressOf doHandleCostTipPriceLookupSkipped)
            oDOR.Handler_CostHaulagePriceLookupSkipped = New IDH_DISPROW.et_ExternalTriggerWithData(AddressOf doHandleCostHaulagePriceLookupSkipped)
            oDOR.Handler_CostProducerPriceLookupSkipped = New IDH_DISPROW.et_ExternalTriggerWithData(AddressOf doHandleCostProducerPriceLookupSkipped)

            If oDOR.SkipPriceLookup Then
                setVisible(oForm, "IDH_CHRPL", True)
                setVisible(oForm, "IDH_CSTPL", True)
            End If

            doAddWF(oForm, "DOR", oDOR)
            Return oDOR
        End Function

        Protected Function thisDO(ByVal oForm As SAPbouiCOM.Form) As IDH_DISPORD
            Dim oDO As IDH_DISPORD = getWFValue(oForm, "DO")
            If oDO Is Nothing Then
                oDO = createDO(oForm)
            End If
            If oDO IsNot Nothing Then
                Dim sCode As String = oDO.Code
            End If
            Return oDO
        End Function

        Private Function createDO(ByVal oForm As SAPbouiCOM.Form) As IDH_DISPORD
            Dim oDO As IDH_DISPORD = New IDH_DISPORD(Nothing, oForm)
            oDO.MustLoadChildren = False
            doAddWF(oForm, "DO", oDO)
            Return oDO
        End Function

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            'doResetLookupSkippedCol(oForm)

            Dim oDO As IDH_DISPORD = thisDO(oForm)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

            Dim oItem As SAPbouiCOM.Item
            Dim oOpt As SAPbouiCOM.OptionBtn

            oOpt = oForm.Items.Item("IDH_USERE").Specific
            oOpt.ValOn = "1"

            oOpt.ValOff = "0"
            oOpt.Selected = True
            doAddDF(oForm, msRowTable, "IDH_USERE", "U_UseWgt")

            oOpt = oForm.Items.Item("IDH_USEAU").Specific
            oOpt.ValOn = "2"
            oOpt.GroupWith("IDH_USERE")

            'createDO(oForm)

            Dim oTab As SAPbouiCOM.Folder
            Dim oLabel As SAPbouiCOM.StaticText

            oItem = oForm.Items.Item("IDH_TABDED")
            oTab = oItem.Specific
            oTab.Caption = "Deductions (" & com.idh.bridge.lookups.Config.INSTANCE.getWeighBridgeUOM() & ")"

            oItem = oForm.Items.Item("IDH_DEDLBL")
            oLabel = oItem.Specific()
            oLabel.Caption = "Total Weight Deduction (" & com.idh.bridge.lookups.Config.INSTANCE.getWeighBridgeUOM() & ")"

            ''The Price Add Update Form
            'Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption = New WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption(Nothing, oForm, Nothing)
            'oOOForm.KeepInMemory = True
            'oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogOkReturn(AddressOf doAddUpdatePrices)
            'oOOForm.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogCancelReturn(AddressOf doCancelAddUpdatePrices)

            'doAddWF(oForm, "PRCUPD", oOOForm)

            'The Working Variables for the form
            'doAddWF(oForm, "IDH_VatPrc", 0)
            doAddWF(oForm, "IDH_CCTChr", "")
            doAddWF(oForm, "IDH_CCHChr", "")
            doAddWF(oForm, "IDH_LoadSecondWeigh", False)
            'doAddWF(oForm, "IDH_LastModePO", "DontKnow")
            doAddWF(oForm, "IDH_LastMode", "DontKnow")
            doAddWF(oForm, "IDH_TOffset", 0)
            doAddWF(oForm, "IDH_HOffset", 0)
            doAddWF(oForm, "IDH_TCostOffset", 0)
            doAddWF(oForm, "IDH_HCostOffset", 0)
            doAddWF(oForm, "LastVehAct", "DOSEARCH")
            'doAddWF(oForm, "VehicleType", "S")
            doAddWF(oForm, "CreateNewVechicle", False)
            doAddWF(oForm, "TargetRow", "DN")
            'doAddWF(oForm, "FormMode", "DN")
            doAddWF(oForm, "FindBranchDisposalSite", True)
            'doAddWF(oForm, "IsWBWeight", True)
            doAddWF(oForm, "WBridge", Nothing)
            doAddWF(oForm, "LASTLINKEDWO", Nothing)
            doAddWF(oForm, "COSTPRICESBLANKED", False)
            doAddWF(oForm, "CHARGEPRICESBLANKED", False)
            doAddWF(oForm, "CANEDIT", True)
            'doSwitchToSO(oForm)

            If Config.Parameter("MDFFBP").ToUpper().Equals("TRUE") Then
                mbFolderFollowDB = True
            Else
                mbFolderFollowDB = False
            End If

            doGetBridges(oForm)
            doSetWeighBridgeLogo(oForm)

            MyBase.doBeforeLoadData(oForm)
            doSetPreselectData(oForm)

            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
            oGridN.msWR1OrdType = "DO"

            Dim sRowCode As String = getParentSharedData(oForm, "ROWCODE")
            setParentSharedData(oForm, "ROWCODE", "")
            If Not sRowCode Is Nothing AndAlso sRowCode.Length > 0 Then
                setWFValue(oForm, "TargetRow", sRowCode)
            End If

            doFillJobTypes(oForm)
            doBranchCombo(oForm)
            doTZoneCombo(oForm)
            doFillObligatedCombo(oForm)
            doUOMCombos(oForm)

            FillCombos.TransactionCodeCombo(FillCombos.getCombo(oForm, "uBC_TRNCd"), Nothing, Nothing, "DO")

            'KA -- DIMECA
            'doFillAlternateItemDescription(oForm, "")

            If ghOldDialogParams.Contains(oForm.UniqueID) = False Then
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            End If

            setUFValue(oForm, "IDH_WEIB", "0")
            setUFValue(oForm, "IDH_SERB", "")
            setUFValue(oForm, "IDH_WDTB", "")

            doSetVisible(oForm, "IDH_WOCFL", False, 1000)

            '** The Additional Expenses
            ''LPVTEMP START
            Dim oAddGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            If oAddGrid Is Nothing Then
                oAddGrid = New WR1_Grids.idh.controls.grid.AdditionalExpenses(Me, oForm, "ADDGRID")
                oAddGrid.getSBOItem().AffectsFormMode = False
            End If
            'oAddGrid.getSBOItem().AffectsFormMode = False

            Dim oAdde As WR1_Data.idh.data.AdditionalExpenses = getWFValue(oForm, "JobAdditionalHandler")
            If Not oAdde Is Nothing Then
                oAddGrid.doSetDataHandler(oAdde)
            End If
            'LPVTEMP END

            'LPVTEMP START
            Dim oDedGrid As DBOGrid = getWFValue(oForm, "DEDGRID")
            Dim oDeductions As IDH_DODEDUCT = Nothing  'oDOR.Deductions
            If oDedGrid Is Nothing Then
                oDedGrid = DBOGrid.getInstance(oForm, "DEDGRID")
                If oDedGrid Is Nothing Then
                    If oDeductions Is Nothing Then
                        oDeductions = New IDH_DODEDUCT(Me, oForm)
                        oDeductions.OrderRow = oDOR
                    End If

                    oDedGrid = New DBOGrid(Me, oForm, "IDH_DEDGRD", "DEDGRID", oDeductions)
                    oDedGrid.getSBOItem().AffectsFormMode = False
                    oDedGrid.doSetDeleteActive(True)
                Else
                    oDeductions = oDedGrid.DBObject()
                End If
                setWFValue(oForm, "DEDGRID", oDedGrid)
            Else
                oDeductions = oDedGrid.DBObject()
            End If

            If oDeductions.Handler_TotalWeightChanged Is Nothing Then
                oDeductions.Handler_TotalWeightChanged = New IDH_DODEDUCT.ev_DBObject_Event(AddressOf handleDeductionTotalWeightChanged)
            End If

            If oDeductions.Handler_TotalValueChanged Is Nothing Then
                oDeductions.Handler_TotalValueChanged = New IDH_DODEDUCT.ev_DBObject_Event(AddressOf handleDeductionTotalValueChanged)
            End If

            oDedGrid.doAddListField(IDH_DODEDUCT._Code, "Code", False, 5, "IGNORE", Nothing)
            oDedGrid.doAddListField(IDH_DODEDUCT._Name, "Name", False, 0, "IGNORE", Nothing)
            oDedGrid.doAddListField(IDH_DODEDUCT._JobNr, "Order Number", False, 0, "IGNORE", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_UserDefinedObject)
            oDedGrid.doAddListField(IDH_DODEDUCT._RowNr, "Row Order Number", False, 0, "IGNORE", Nothing)
            oDedGrid.doAddListField(IDH_DODEDUCT._ItemCd, "Item Code", True, -1, "SRC:IDHISRC(DED)[IDH_ITMCOD][ITEMCODE;U_ItemDsc=ITEMNAME;U_ItmWgt=DEFWEI;U_UOM=SUOM]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oDedGrid.doAddListField(IDH_DODEDUCT._ItemDsc, "Item Description", True, -1, "IGNORE", Nothing)
            oDedGrid.doAddListField(IDH_DODEDUCT._Qty, "Quantity", True, 10, "IGNORE", Nothing)
            oDedGrid.doAddListField(IDH_DODEDUCT._ItmWgt, "Item Weight", True, -1, "IGNORE", Nothing)
            oDedGrid.doAddListField(IDH_DODEDUCT._UOM, "Unit Of Measure", True, -1, "COMBOBOX", Nothing)
            oDedGrid.doAddListField(IDH_DODEDUCT._WgtDed, "Weight Deduction", True, -1, "IGNORE", Nothing)
            oDedGrid.doAddListField(IDH_DODEDUCT._ValDed, "Value Deduction", True, -1, "IGNORE", Nothing)
            oDedGrid.doSynchFields()

            'SET THE ADDITIONAL TOTALS ON THE TAB
            setUFValue(oForm, "IDH_ADDCO", 0)
            setUFValue(oForm, "IDH_ADDCH", 0)
        End Sub

        Public Sub handleDeductionTotalWeightChanged(ByVal oDeduction As IDH_DODEDUCT)
            Dim oForm As SAPbouiCOM.Form = oDeduction.SBOForm
            Dim dTotal As Double = oDeduction.WeightTotal

            'Update the Deducted Weight
            thisDOR(oForm).U_WgtDed = dTotal
        End Sub

        Public Sub handleDeductionTotalValueChanged(ByVal oDeduction As IDH_DODEDUCT)
            Dim oForm As SAPbouiCOM.Form = oDeduction.SBOForm
            Dim dTotal As Double = oDeduction.ValueTotal

            'Update the Deducted value
            thisDOR(oForm).U_ValDed = dTotal
        End Sub

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            doLoadDataLocal(oForm, True)
            doFillAlternateItemDescription(oForm, getDFValue(oForm, msRowTable, "U_AltWasDsc"))
            ''Disabled because currencies will be set by CIP/SIP DBObjects now 
            'doSetCurrencies(oForm)
        End Sub

        'LPVTEMP START
        Protected Sub doLoadDataLocal(ByVal oForm As SAPbouiCOM.Form, ByVal bDoSetAddDefaults As Boolean)
            'Dim oWinForm As com.idh.form.SBO.ChequeNumber = New com.idh.form.SBO.ChequeNumber()
            'oWinForm.doShowInSBO()

            ' ''If oWinForm.doShowDialog() = System.Windows.Forms.DialogResult.OK Then
            ' ''    Dim iChequeNumber As Integer = oWinForm.Che
            ' ''End If

            oForm.Freeze(True)
            Try
                Dim oDO As IDH_DISPORD = thisDO(oForm)
                Dim oDOR As IDH_DISPROW = thisDOR(oForm)

                Dim sCode As String = getFormDFValue(oForm, "Code", True)
                'setWFValue(oForm, "FormMode", "DN")

                oForm.PaneLevel = 10

                MyBase.doLoadData(oForm)

                setEnableItem(oForm, False, "IDH_Amend")

                '********************************************************************
                '*** Additional Expenses Data handler
                '*** This will create the Datahandler for the Additional Charges Grid
                '*** The Grid will get it's data from the handler this handler can
                '*** be passed between the Disposal Header and the row
                '********************************************************************
                Dim oAdde As WR1_Data.idh.data.AdditionalExpenses = getWFValue(oForm, "JobAdditionalHandler")
                If oAdde Is Nothing Then
                    oAdde = New WR1_Data.idh.data.AdditionalExpenses(goParent, "[@IDH_DOADDEXP]")
                    setWFValue(oForm, "JobAdditionalHandler", oAdde)
                End If
                'oAdde.GetByJob(sWrkCode)

                '********************************************************************
                '*** Deductions Data handler
                '*** This will create the Datahandler for the Deductions Grid
                '*** The Grid will get it's data from the handler this handler can
                '*** be passed between the Disposal Header and the row
                '********************************************************************
                'LPVTEMP START
                'Dim oDed As WR1_Data.idh.data.Deductions = getWFValue(oForm, "JobDeductionHandler")
                'If oDed Is Nothing Then
                '    oDed = New WR1_Data.idh.data.Deductions(goParent, "[@IDH_DODEDUCT]")
                '    setWFValue(oForm, "JobDeductionHandler", oDed)
                'End If
                'oDed.GetByJob(sWrkCode)
                'LPVTEMP END

                Dim iMode As SAPbouiCOM.BoFormMode = oForm.Mode
                Dim bDoSecondWeigh As Boolean
                bDoSecondWeigh = getWFValue(oForm, "IDH_LoadSecondWeigh")

                setUFValue(oForm, "IDH_AVCONV", "N")
                setUFValue(oForm, "IDH_AVDOC", "N")
                oForm.Title = getTranslatedCaption("#TITLE", "Disposal Order")

                setUFIValue(oForm, "IDH_WEIBRG", msLastWeighBridgeId)
                doConfigWeighBridge(oForm, msLastWeighBridgeId)

                If bDoSecondWeigh = True Then
                    doShowFirstSecondWeigh(oForm)
                Else
                    sCode = getFormDFValue(oForm, "Code", True)

                    If sCode.Length() = 0 Then
                        doFillRowData(oForm, -1)
                    Else
                        Dim sRowID As String = getWFValue(oForm, "TargetRow")
                        If sRowID.Equals("DN") = False Then
                            doFillRowData(oForm, sRowID)
                        Else
                            doFillRowData(oForm, 0)
                        End If

                        If oDOR.IsLinkedToOrder() Then
                            FillCombos.TransactionCodeCombo(FillCombos.getCombo(oForm, "uBC_TRNCd"), Nothing, Nothing, Nothing)
                        End If
                    End If
                End If
                'oForm.Mode = iMode

                'Dim oDO As IDH_DISPORD = thisDO(oForm)
                'Dim oDOR As IDH_DISPROW = oDO.getRowDB()
                'If oDOR IsNot Nothing Then
                '    oDOR.doClearBuffers()
                'End If

                setWFValue(oForm, "TargetRow", "DN")
                setWFValue(oForm, "WRROWChanged", False)
                '
                If Config.INSTANCE.getParameterAsBool("DOORLU", False) = False Then
                    Dim sOrigin As String
                    sOrigin = getDFValue(oForm, "@IDH_DISPORD", "U_Origin")
                    If sOrigin Is Nothing OrElse sOrigin.Length = 0 Then
                        setDFValue(oForm, "@IDH_DISPORD", "U_Origin", getDFValue(oForm, "@IDH_DISPORD", "U_PCity"))
                    End If
                End If

                'Check if DO is linked to WOR then disable Transaction Code field 
                Dim sWOR As String = getDFValue(oForm, msRowTable, "U_WRRow", True)
                If sWOR IsNot Nothing AndAlso sWOR.Length > 0 Then
                    setEnableItem(oForm, False, "uBC_TRNCd")
                End If
                '## MA Start 08-09-2015 Issue#913:
                doBPForecast(oForm)
                '## MA End 08-09-2015 Issue#913:



                '## MA Start 13-08-2014 Issue#147: Skype HH: Disable Add Tab if DO is Linked with WOR
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    If bDoSetAddDefaults = True Then
                        doSetAddDefaults(oForm)
                        setEnableItem(oForm, True, "IDH_TABADD")
                        ''## MA Start 15-08-2014 Issue#401 
                        ''setEnableItem(oForm, False, "IDH_WEI1")
                        ''## MA Start 13-08-2014 Issue#401
                    End If
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    doSetFindDefaults(oForm)
                    setEnableItem(oForm, True, "IDH_TABADD")
                Else
                    Dim sWORowID As String = getDFValue(oForm, msRowTable, "U_WRRow", True)
                    Dim sWOCode As String = getDFValue(oForm, msRowTable, "U_WROrd", True)
                    If sWORowID.Trim <> "" OrElse sWOCode.Trim <> "" Then
                        setEnableItem(oForm, False, "IDH_TABADD") 'ADDGRID")
                    Else
                        setEnableItem(oForm, True, "IDH_TABADD")
                    End If
                    '## MA End 13-08-2014 Issue#147: Skype HH: Disable Add Tab if DO is Linked with WOR
                End If


                'OnTime 32756: WR1_NewDev_Buying&Trading_Add functionality to log Activity with a WOR
                'Add Activity Grid and Fill
                Try
                    If Not sCode Is Nothing AndAlso sCode.Length > 0 Then
                        doLoadActivity(oForm, sCode, "-1")
                    End If
                Catch ex As Exception
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "idh.forms.orders.Tmpl: doActivityGrid()")
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDAG", {"idh.forms.orders.Tmpl"})
                End Try

                'Add BP Additional Charges if form is comming from PreSelet 
                Try
                    Dim oPreData As PreSelect = getWFValue(oForm, "PRESELECT")
                    If oPreData IsNot Nothing Then
                        If oPreData.BPCode.Length > 0 Then
                            'Remove and Add BP Ad. Charge rows 
                            removeAutoAddBPAddCharges(oForm)
                            getBPAdditionalCharges(oForm)

                            'Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)
                            'If Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) Then
                            '    doHideLoadSheet(oForm)
                            '    If Config.INSTANCE.doCheckCanPurchaseWB(getDFValue(oForm, msRowTable, "U_WasCd")) Then
                            '        doSwitchToPO(oForm, True)
                            '    Else
                            '        doSwitchToSO(oForm)
                            '    End If
                            'Else
                            '    doSwitchToSO(oForm)
                            '    doShowLoadSheet(oForm)
                            'End If

                        End If
                    End If
                    If oDO.U_CardCd <> "" Then
                        setEnableItem(oForm, True, "IDH_CUSADD")
                    End If
                    If oDOR.U_CarrCd <> "" Then
                        setEnableItem(oForm, True, "IDH_ADDRES")
                    End If
                    If oDOR.U_ProCd <> "" Then
                        setEnableItem(oForm, True, "IDH_PRDADD")
                    End If
                    If oDO.U_SCardCd <> "" AndAlso isItemEnabled(oForm, "IDH_DISSIT") Then
                        setEnableItem(oForm, True, "IDH_SITADD")
                    End If

                Catch ex As Exception
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDAG", {"idh.forms.orders.Disposal:doLoadDataLocal"})
                End Try

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error loading local data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXLLD", {Nothing})
            End Try
            oForm.Freeze(False)
        End Sub

        ' ''## MA Start 16-10-2014
        'Private Sub doUpdateBPWeightFlag(oForm As SAPbouiCOM.Form)
        '    Try
        '        Dim sUseBPWeight As String = ""
        '        If getFormDFValue(oForm, "U_CardCd") IsNot Nothing AndAlso getFormDFValue(oForm, "U_CardCd").ToString <> "" Then
        '            sUseBPWeight = Config.INSTANCE.getBP_UseBPWeight(goParent, getFormDFValue(oForm, "U_CardCd"))
        '            If Not sUseBPWeight Is Nothing Then 'AndAlso sUseBPWeight.Length > 0
        '                setFormDFValue(oForm, "U_UseBPWgt", sUseBPWeight, True)
        '            End If
        '        Else
        '            setFormDFValue(oForm, "U_UseBPWgt", "", True)
        '        End If
        '        If sUseBPWeight.Trim.ToUpper = "Y" AndAlso System.Array.IndexOf(moAlwaysDisabledFields, "IDH_BPWGT") = -1 Then
        '            'If System.Array.IndexOf(moAlwaysDisabledFields, "IDH_BPWGT") = -1 Then
        '            setEnableItem(oForm, True, "IDH_BPWGT")
        '            'End If
        '        Else
        '            setEnableItem(oForm, False, "IDH_BPWGT")
        '        End If
        '        setDFValue(oForm, msRowTable, IDH_DISPROW._BPWgt, 0)
        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Update BP Weight Flag.")
        '    End Try
        'End Sub
        'Public Overrides Sub doCreateFindEntry(oForm As SAPbouiCOM.Form)
        '    MyBase.doCreateFindEntry(oForm)
        '    setFormDFValue(oForm, "U_UseBPWgt", "", True)
        'End Sub
        'Public Overrides Sub doCreateNewEntry(oForm As SAPbouiCOM.Form)
        '    MyBase.doCreateNewEntry(oForm)
        '    setFormDFValue(oForm, "U_UseBPWgt", "", True)
        'End Sub
        ' ''## MA End 16-10-2014

        'Protected Sub doLoadDataLocal(ByVal oForm As SAPbouiCOM.Form, ByVal bDoSetAddDefaults As Boolean)
        '    oForm.Freeze(True)
        '    Try
        '        Dim sCode As String = getFormDFValue(oForm, "Code", True)
        '        setWFValue(oForm, "FormMode", "DN")

        '        oForm.PaneLevel = 10

        '        MyBase.doLoadData(oForm)

        '        Dim sWrkCode As String = "51284" 'sCode
        '        '********************************************************************
        '        '*** Additional Expenses Data handler
        '        '*** This will create the Datahandler for the Additional Charges Grid
        '        '*** The Grid will get it's data from the handler this handler can
        '        '*** be passed between the Disposal Header and the row
        '        '********************************************************************
        '        Dim oAdde As WR1_Data.idh.data.AdditionalExpenses = getWFValue(oForm, "JobAdditionalHandler")
        '        If oAdde Is Nothing Then
        '            oAdde = New WR1_Data.idh.data.AdditionalExpenses(goParent, "[@IDH_DOADDEXP]")
        '            setWFValue(oForm, "JobAdditionalHandler", oAdde)
        '        End If
        '        'oAdde.GetByJob(sWrkCode)

        '        '********************************************************************
        '        '*** Deductions Data handler
        '        '*** This will create the Datahandler for the Deductions Grid
        '        '*** The Grid will get it's data from the handler this handler can
        '        '*** be passed between the Disposal Header and the row
        '        '********************************************************************
        '        'LPVTEMP START
        '        'Dim oDed As WR1_Data.idh.data.Deductions = getWFValue(oForm, "JobDeductionHandler")
        '        'If oDed Is Nothing Then
        '        '    oDed = New WR1_Data.idh.data.Deductions(goParent, "[@IDH_DODEDUCT]")
        '        '    setWFValue(oForm, "JobDeductionHandler", oDed)
        '        'End If
        '        'oDed.GetByJob(sWrkCode)
        '        'LPVTEMP END

        '        Dim iMode As SAPbouiCOM.BoFormMode = oForm.Mode
        '        Dim bDoSecondWeigh As Boolean
        '        bDoSecondWeigh = getWFValue(oForm, "IDH_LoadSecondWeigh")

        '        setUFValue(oForm, "IDH_AVCONV", "N")
        '        setUFValue(oForm, "IDH_AVDOC", "N")
        '        oForm.Title = getTranslatedCaption("#TITLE", "Disposal Order")

        '        setUFIValue(oForm, "IDH_WEIBRG", msLastWeighBridgeId)
        '        doConfigWeighBridge(oForm, msLastWeighBridgeId)

        '        If bDoSecondWeigh = True Then
        '            doShowFirstSecondWeigh(oForm)
        '        Else
        '            sCode = getFormDFValue(oForm, "Code", True)

        '            If sCode.Length() = 0 Then
        '                doFillRowData(oForm, -1)
        '            Else
        '                Dim sRowID As String = getWFValue(oForm, "TargetRow")
        '                If sRowID.Equals("DN") = False Then
        '                    doFillRowData(oForm, sRowID)
        '                Else
        '                    doFillRowData(oForm, 0)
        '                End If
        '            End If
        '        End If
        '        oForm.Mode = iMode

        '        setWFValue(oForm, "TargetRow", "DN")
        '        setWFValue(oForm, "WRROWChanged", False)
        '        '
        '        If Config.INSTANCE.getParameterAsBool("DOORLU", False) = False Then
        '            Dim sOrigin As String
        '            sOrigin = getDFValue(oForm, "@IDH_DISPORD", "U_Origin")
        '            If sOrigin Is Nothing OrElse sOrigin.Length = 0 Then
        '                setDFValue(oForm, "@IDH_DISPORD", "U_Origin", getDFValue(oForm, "@IDH_DISPORD", "U_PCity"))
        '            End If
        '        End If

        '        setWFValue(oForm, "IDH_LoadSecondWeigh", False)

        '        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
        '            If bDoSetAddDefaults = True Then
        '                doSetAddDefaults(oForm)
        '            End If
        '        ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
        '            doSetFindDefaults(oForm)
        '        End If

        '        'OnTime 32756: WR1_NewDev_Buying&Trading_Add functionality to log Activity with a WOR
        '        'Add Activity Grid and Fill
        '        Try
        '            If Not sCode Is Nothing AndAlso sCode.Length > 0 Then
        '                doLoadActivity(oForm, sCode, "-1")
        '            End If
        '        Catch ex As Exception
        '            com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "idh.forms.orders.Tmpl: doActivityGrid()")
        '        End Try

        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error loading local data.")
        '    End Try
        '    oForm.Freeze(False)
        'End Sub
        'LPVTEMP END

        Public Sub doSetItemGroup(ByVal oForm As SAPbouiCOM.Form)
            Dim oGrpCmb As SAPbouiCOM.ComboBox
            Dim oItem As SAPbouiCOM.Item
            Dim oVal As SAPbouiCOM.ValidValues
            Dim sVal As String

            oItem = oForm.Items.Item("IDH_IGRP")
            doSetEnabled(oItem, True)
            oGrpCmb = oItem.Specific

            oVal = oGrpCmb.ValidValues
            If Not (oVal Is Nothing) AndAlso oVal.Count > 0 Then
                sVal = oVal.Item(0).Value
                setUFValue(oForm, "IDH_ITMGRN", oVal.Item(0).Description)
                setDFValue(oForm, msRowTable, "U_ItmGrp", oVal.Item(0).Value)
                setDFValue(oForm, "@IDH_DISPORD", "U_ItemGrp", oVal.Item(0).Value)
            End If
            doSetEnabled(oItem, False)
        End Sub

        Protected Sub doGetBridges(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item

            'GET THE AVAILABLE WEIGH BRIDGES
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                Dim oWBRGCmb As SAPbouiCOM.ComboBox
                oRecordSet = goParent.goDB.doSelectQuery("select U_Val, U_Desc from [@" & goParent.goConfigTable & "] WHERE Code LIKE 'WBID%'")
                If oRecordSet.RecordCount > 0 Then
                    oItem = oForm.Items.Item("IDH_WEIBRG")
                    oWBRGCmb = oItem.Specific
                    Dim oVal1 As String
                    Dim oVal2 As String

                    doClearValidValues(oWBRGCmb.ValidValues)

                    Dim sPort As String
                    Dim sID As String

                    Dim oWBridge As Weighbridge = getWFValue(oForm, "WBridge")

                    For iCount As Integer = 0 To oRecordSet.RecordCount - 1
                        sID = oRecordSet.Fields.Item(0).Value


                        sPort = Config.Parameter("WBPORT" & sID)
                        If Weighbridge.IsPortAvailable(Int32.Parse(sPort)) Then
                            If oWBridge Is Nothing AndAlso iCount = 0 Then
                                doConfigWeighBridge(oForm, sID)
                            End If

                            oVal1 = oRecordSet.Fields.Item(0).Value
                            oVal2 = oRecordSet.Fields.Item(1).Value
                            Try
                                oWBRGCmb.ValidValues.Add(oVal1, oVal2)
                            Catch ex As Exception
                            End Try
                        End If
                        oRecordSet.MoveNext()
                    Next
                Else
                    doConfigWeighBridge(oForm, "-1")
                End If
            Catch ex As Exception
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
            Finally
            End Try
        End Sub

        Private Sub doSetWeighBridgeLogo(ByVal oForm As SAPbouiCOM.Form)
            Dim oWBridge As Weighbridge = getWFValue(oForm, "WBridge")
            If oWBridge Is Nothing Then
                Exit Sub
            End If

            Dim sPort As String = Config.Parameter("WBPORT" & oWBridge.getWBId())
            Dim sLogo As String = Config.Parameter("WBLOGO" & oWBridge.getWBId())
            If Not sLogo Is Nothing Then
                If sLogo.ToUpper().Equals("TRUE") Then
                    oForm.Items.Item("IDH_M").Visible = True
                Else
                    oForm.Items.Item("IDH_M").Visible = False
                End If
            End If
        End Sub

        Protected Sub doSwitchGenButton(ByVal oForm As SAPbouiCOM.Form)
            Dim sConNum As String = getDFValue(oForm, msRowTable, "U_ConNum")
            Dim bDoEnable As Boolean = False
            If sConNum Is Nothing OrElse sConNum.Length = 0 Then
                Dim sWasteCode As String = getDFValue(oForm, msRowTable, "U_WasCd")
                If com.idh.bridge.lookups.Config.INSTANCE.isHazardousItem(sWasteCode) = True Then
                    bDoEnable = True
                End If
            End If
            setEnableItem(oForm, bDoEnable, "IDH_CONGEN")

            If bDoEnable = True Then
                Dim bAllowHazChange As Boolean = com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("DOCACCN", False)
                setEnableItem(oForm, bAllowHazChange, "IDH_HAZCN")
            Else
                setEnableItem(oForm, False, "IDH_HAZCN")
            End If
        End Sub

        Protected Sub doSwitchToUpdate(ByVal oForm As SAPbouiCOM.Form)
            setWFValue(oForm, "CANEDIT", True)

            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            Dim bIsRedo As Boolean = False

            Dim sLastMode As String = getWFValue(oForm, "IDH_LastMode")
            If sLastMode.Equals("Add") Then
                doSetCheckBoxes(oForm)
                Exit Sub
            Else
                setWFValue(oForm, "IDH_LastMode", "Update")
            End If

            Dim sAllowChanging As String
            sAllowChanging = Config.Parameter("DOACBP")

            Dim bDoCreate As Boolean = getWFValue(oForm, "CreateNewVechicle")
            Dim oExclude() As String
            If sAllowChanging.ToUpper().Equals("TRUE") OrElse bDoCreate Then
                oExclude = moAlwaysDisabledFields
            Else
                oExclude = General.doCombineArrays(moAlwaysDisabledFields, moBPFields)
            End If

            'If String.IsNullOrWhiteSpace(oDOR.U_TrnCode) = False Then
            '    oExclude = General.doCombineArrays(oExclude, moMarketingCheckboxes)
            'End If

            Dim oAddGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            oAddGrid.doEnabled(True)

            Dim bDoWB As Boolean = True
            Dim dFirstWeight As Double = getDFValue(oForm, msRowTable, IDH_DISPROW._Wei1)
            Dim dSecondWeight As Double = getDFValue(oForm, msRowTable, IDH_DISPROW._Wei2)
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WEIGHTS) = False Then
                Dim dReadWeight As Double = getDFValue(oForm, msRowTable, IDH_DISPROW._RdWgt)

                If dReadWeight <> 0 Then
                    oExclude = General.doCombineArrays(oExclude, moFirstWeightItems)
                    oExclude = General.doCombineArrays(oExclude, moSecondWeightItems)
                    oExclude = General.doCombineArrays(oExclude, moReadWeightItems)

                    setEnableItem(oForm, False, "IDH_TAR1")
                    setEnableItem(oForm, False, "IDH_ACC1")
                    setEnableItem(oForm, False, "IDH_BUF1")

                    setEnableItem(oForm, False, "IDH_TAR2")
                    setEnableItem(oForm, False, "IDH_ACC2")
                    setEnableItem(oForm, False, "IDH_BUF2")

                    setEnableItem(oForm, False, "IDH_REF")
                    setEnableItem(oForm, False, "IDH_ACCB")
                    bDoWB = False
                Else
                    If dFirstWeight <> 0 Then
                        oExclude = General.doCombineArrays(oExclude, moFirstWeightItems)
                        setEnableItem(oForm, False, "IDH_TAR1")
                        setEnableItem(oForm, False, "IDH_ACC1")
                        setEnableItem(oForm, False, "IDH_BUF1")
                    End If

                    If dSecondWeight <> 0 Then
                        oExclude = General.doCombineArrays(oExclude, moSecondWeightItems)
                        setEnableItem(oForm, False, "IDH_TAR2")
                        setEnableItem(oForm, False, "IDH_ACC2")
                        setEnableItem(oForm, False, "IDH_BUF2")
                    End If
                End If
            End If

            If oDOR.U_Status = FixedValues.getReDoOrderStatus OrElse
                oDOR.U_Status = FixedValues.getReDoInvoiceStatus OrElse
                oDOR.U_PStat = FixedValues.getReDoOrderStatus OrElse
                oDOR.U_PStat = FixedValues.getReDoInvoiceStatus Then
                oExclude = General.doCombineArrays(oExclude, moAllWeightItems)
                bIsRedo = True
            End If

            Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)
            Dim bIsOutgoing As Boolean = Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType)

            If (oDOR IsNot Nothing AndAlso oDOR.MarketingMode = Config.MarketingMode.PO) Then
                'If getWFValue(oForm, "FormMode") = "PO" Then
                oExclude = General.doCombineArrays(oExclude, moCostTippingOutgoingItems)
                oExclude = General.doCombineArrays(oExclude, moCostHaulageItems)
                oExclude = General.doCombineArrays(oExclude, moSalesItems)
            Else
                oExclude = General.doCombineArrays(oExclude, moCostTippingIncommingItems)
                oExclude = General.doCombineArrays(oExclude, moCostHaulageItems)


                If bIsOutgoing Then
                    oExclude = General.doCombineArrays(oExclude, moCostTippingOutgoingItems)
                    'Else
                    '    If moLookup.getParameterAsBool("DODCEDI", False) Then
                    '        oExclude = General.doCombineArrays(oExclude, moDisposalSiteItems)
                    '    End If
                End If
            End If

            If bIsOutgoing = False OrElse Config.ParameterAsBool("DODCEDI", False) Then
                oExclude = General.doCombineArrays(oExclude, moDisposalSiteItems)
            End If

            EnableAllEditItems(oForm, oExclude)
            'setEnableItem(oForm, False, "IDH_CUSL")
            'setEnableItem(oForm, False, "IDH_CARL")
            setEnableItem(oForm, True, "IDH_CUSL")
            setEnableItem(oForm, True, "IDH_CARL")
            setEnableItem(oForm, True, "IDH_NOVAT")
            setEnableItem(oForm, True, "IDH_TAR1")
            setEnableItem(oForm, True, "IDH_TAR1")

            Dim sLoadSht As String = getDFValue(oForm, msRowTable, "U_LoadSht")
            If sLoadSht.Length > 0 Then
                setEnableItem(oForm, False, "IDH_CUSWEI")
                setEnableItem(oForm, False, "IDH_UOM")
            End If

            'If Config.INSTANCE.getParameterAsBool( "DOCACCN", False) = True Then
            '	setEnableItem(oForm, True, "IDH_HAZCN")
            'Else
            '	setEnableItem(oForm, False, "IDH_HAZCN")
            'End If

            doSetCheckBoxes(oForm)

            If bIsRedo = False Then
                If bDoWB AndAlso oForm.Items.Item("IDH_WEIBRG").Specific.ValidValues.Count > 0 Then
                    setEnableItems(oForm, (dFirstWeight = 0), moFirstWeightItems)
                    setEnableItems(oForm, (dSecondWeight = 0), moSecondWeightItems)

                    'Dim oItems() As String = {"IDH_REF", "IDH_ACC1", "IDH_ACC2", "IDH_BUF1", "IDH_BUF2"}
                    'setEnableItems(oForm, True, oItems)
                End If

                Dim sOrder As String = getDFValue(oForm, msRowTable, "U_WROrd")
                Dim bFocusSet As Boolean = False
                If sOrder.Length = 0 Then
                    setEnableItem(oForm, True, "IDH_WRORD")
                Else
                    ''## MA Start 16-09-2014 Issue#401
                    If isItemEnabled(oForm, "IDH_WEI1") Then
                        doSetFocus(oForm, "IDH_WEI1")
                    End If
                    ''## MA End 16-09-2014 Issue#401
                    bFocusSet = True
                    setEnableItem(oForm, False, "IDH_WRORD")
                End If

                Dim sRow As String = getDFValue(oForm, msRowTable, "U_WRRow")
                If sRow.Length = 0 Then
                    setEnableItem(oForm, True, "IDH_WRROW")
                Else
                    If bFocusSet = False AndAlso isItemEnabled(oForm, "IDH_WEI1") Then
                        doSetFocus(oForm, "IDH_WEI1")
                    End If
                    setEnableItem(oForm, False, "IDH_WRROW")
                End If
            End If
            doSwitchGenButton(oForm)

            If oDOR.SkipPriceLookup Then
                setEnableItem(oForm, True, "IDH_CHRPL")
                setEnableItem(oForm, True, "IDH_CSTPL")
            End If
        End Sub

        Protected Overrides Sub doSwitchToAdd(ByVal oForm As SAPbouiCOM.Form)

            setWFValue(oForm, "CANEDIT", True)

            'com.idh.bridge.utils.Combobox2.doSelectFirstRow(FillCombos.getCombo(oForm, "uBC_TRNCd"))

            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

            Dim sLastMode As String = getWFValue(oForm, "IDH_LastMode")
            If sLastMode.Equals("Add") Then
                doSetCheckBoxes(oForm)
                Exit Sub
            Else
                setWFValue(oForm, "IDH_LastMode", "Add")
            End If

            Dim sAllowChanging As String
            sAllowChanging = Config.Parameter("DOACBP")

            Dim bDoCreate As Boolean = getWFValue(oForm, "CreateNewVechicle")
            Dim oExclude() As String
            If sAllowChanging.ToUpper().Equals("TRUE") OrElse bDoCreate Then
                oExclude = moAlwaysDisabledFields
            Else
                oExclude = General.doCombineArrays(moAlwaysDisabledFields, moBPFields)
                oExclude = General.doCombineArrays(oExclude, moBPFields)
                oExclude = General.doAppendToArray(oExclude, "IDH_CUSWEI")
            End If

            Dim oAddGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            oAddGrid.doEnabled(True)


            Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)
            Dim bIsOutgoing As Boolean = Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType)

            If (oDOR IsNot Nothing AndAlso oDOR.MarketingMode = Config.MarketingMode.PO) Then
                'If getWFValue(oForm, "FormMode") = "PO" Then
                oExclude = General.doCombineArrays(oExclude, moCostTippingOutgoingItems)
                oExclude = General.doCombineArrays(oExclude, moCostHaulageItems)
                oExclude = General.doCombineArrays(oExclude, moSalesItems)

                If (String.IsNullOrWhiteSpace(oDOR.U_TrnCode) = False) Then
                    oExclude = General.doCombineArrays(oExclude, moChargeTippingIncomingItems)
                End If
            Else
                oExclude = General.doCombineArrays(oExclude, moCostTippingIncommingItems)
                oExclude = General.doCombineArrays(oExclude, moCostHaulageItems)

                'Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)
                If bIsOutgoing Then
                    oExclude = General.doCombineArrays(oExclude, moCostTippingOutgoingItems)
                    'Else
                    '    If moLookup.getParameterAsBool("DODCEDI", False) Then
                    '        oExclude = General.doCombineArrays(oExclude, moDisposalSiteItems)
                    '    End If
                End If
            End If

            If bIsOutgoing = False OrElse Config.ParameterAsBool("DODCEDI", False) Then
                oExclude = General.doCombineArrays(oExclude, moDisposalSiteItems)
            End If
            oExclude = General.doCombineArrays(oExclude, New String() {"IDH_CUSADD", "IDH_ADDRES", "IDH_PRDADD", "IDH_SITADD"})

            'If (String.IsNullOrWhiteSpace(oDOR.U_TrnCode) = False) Then
            '    oExclude = General.doCombineArrays(oExclude, moMarketingCheckboxes)
            'End If

            EnableAllEditItems(oForm, oExclude)
            setEnableItem(oForm, True, "IDH_CUSL")
            setEnableItem(oForm, True, "IDH_CARL")
            setEnableItem(oForm, True, "IDH_NOVAT")
            setEnableItem(oForm, True, "IDH_TAR1")
            setEnableItem(oForm, True, "IDH_TAR2")

            doSetCheckBoxes(oForm)

            If oForm.Items.Item("IDH_WEIBRG").Specific.ValidValues.Count > 0 Then
                Dim oItems() As String = {"IDH_REF", "IDH_ACC1", "IDH_ACC2", "IDH_BUF1", "IDH_BUF2"}
                setEnableItems(oForm, True, oItems)
            End If

            doSwitchGenButton(oForm)

            If oDOR.SkipPriceLookup Then
                setEnableItem(oForm, True, "IDH_CHRPL")
                setEnableItem(oForm, True, "IDH_CSTPL")
            End If
        End Sub
        Protected Overrides Sub doSetFocusInAddMode(oForm As SAPbouiCOM.Form)
            If Config.INSTANCE.getParameterWithDefault("DOSTFCAD", "") <> "" Then
                doSetFocus(oForm, Config.INSTANCE.getParameterWithDefault("DOSTFCAD", ""))
            End If
        End Sub
        Protected Overrides Sub doSetFocusInFindMode(oForm As SAPbouiCOM.Form)
            If Config.INSTANCE.getParameterWithDefault("DOSTFCFD", "") <> "" Then
                doSetFocus(oForm, Config.INSTANCE.getParameterWithDefault("DOSTFCFD", ""))
            End If
        End Sub
        Protected Overrides Sub doSwitchToFind(ByVal oForm As SAPbouiCOM.Form)
            setWFValue(oForm, "CANEDIT", False)

            Dim sLastMode As String = getWFValue(oForm, "IDH_LastMode")
            If sLastMode.Equals("Find") Then
                Exit Sub
            Else
                setWFValue(oForm, "IDH_LastMode", "Find")
            End If

            'Dim oExclude() As String = {"IDH_JOBTTP", "IDH_VEHREG", "IDH_CUST", "IDH_BOOREF", "IDH_WRORD", "IDH_WRROW", "IDH_WOCF"}

            oForm.Items.Item("IDH_VEHREG").Click(SAPbouiCOM.BoCellClickType.ct_Regular)

            DisableAllEditItems(oForm, moSearchFields)
            'setEnableItem(oForm, True, "IDH_CUSL")
            setEnableItem(oForm, True, "IDH_CARL")

            Dim oItems() As String = {"IDH_REF", "IDH_ACC1", "IDH_ACC2", "IDH_BUF1", "IDH_BUF2", "IDH_CONGEN"}
            setEnableItems(oForm, False, oItems)
        End Sub

        Protected Sub doSwitchToAddWeigh(ByVal oForm As SAPbouiCOM.Form)
            Dim sLastMode As String = getWFValue(oForm, "IDH_LastMode")
            If sLastMode.Equals("Add") Then
                Exit Sub
            Else
                setWFValue(oForm, "IDH_LastMode", "Add")
            End If

            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

            '"IDH_CUSWEI",
            '"IDH_USERE", "IDH_USEAE", _
            '## MA Start 13-10-2014
            Dim oItems() As String = {
                        "IDH_WRORD", "IDH_WRROW",
                        "IDH_WEIBRG",
                        "IDH_USERE", "IDH_USEAU",
                        "IDH_UOM",
                        "IDH_TAR1", "IDH_TAR2",
                        "IDH_WEIBRG",
                        "IDH_ITMCOD", "IDH_DESC",
                        "IDH_WASCL1", "IDH_WASMAT", "IDH_ALTITM",
                        "IDH_RORIGI", "IDH_CUSCHG",
                        "IDH_DISCNT", "IDH_DSCPRC",
                        "IDH_ADDEX"
                        }


            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_MANUALDOWEIGHTENTERY) Then
                ''MA Start 04-02-2015 Issue#554 added IDH_TRWTE1 and IDH_TRWTE2 to list
                Dim oWeightItems() As String = {"IDH_WEI1", "IDH_WEI2", "IDH_TARWEI", "IDH_TRLTar", "IDH_RDWGT", "IDH_CUSCM", "IDH_CUSWEI", "IDH_TRWTE1", "IDH_TRWTE2"}
                ''MA End 04-02-2015
                oItems = General.doCombineArrays(oItems, oWeightItems)
            End If
            '## MA End 13-10-2014

            If String.IsNullOrWhiteSpace(oDOR.U_TrnCode) Then
                oItems = General.doCombineArrays(oItems, moMarketingCheckboxes)
            End If

            doSetFocus(oForm, "IDH_WEIGH")
            setEnableItems(oForm, True, oItems)

            If oForm.Items.Item("IDH_WEIBRG").Specific.ValidValues.Count > 0 Then
                Dim oItems1() As String = {"IDH_REF", "IDH_ACC1", "IDH_ACC2", "IDH_BUF1", "IDH_BUF2"}
                setEnableItems(oForm, True, oItems1)
            End If
        End Sub

        'LPV 20150925 START - Change Mode switch
        'Private Sub doSwitchToOrdered(ByVal oForm As SAPbouiCOM.Form, ByVal sStatus As String)
        '    setWFValue(oForm, "CANEDIT", False)

        '    Dim sLastMode As String = getWFValue(oForm, "IDH_LastMode")
        '    If sLastMode.Equals(com.idh.bridge.lookups.FixedValues.getStatusOrdered()) _
        '     OrElse sLastMode.Equals(com.idh.bridge.lookups.FixedValues.getStatusFoc()) _
        '     OrElse sLastMode.Equals(com.idh.bridge.lookups.FixedValues.getStatusInvoiced()) Then
        '        Exit Sub
        '    Else
        '        setWFValue(oForm, "IDH_LastMode", sStatus)
        '    End If

        Private Sub doSwitchToOrdered(ByVal oForm As SAPbouiCOM.Form)
            setWFValue(oForm, "CANEDIT", False)

            Dim sLastMode As String = getWFValue(oForm, "IDH_LastMode")
            If sLastMode.Equals("Ordered") Then
                Exit Sub
            Else
                setWFValue(oForm, "IDH_LastMode", "Ordered")
            End If

            'LPV 20150925 END - Change Mode Switch

            doParkEdit(oForm)
            '## MA Start 07-08-2014 Issue#:299; Add IDH_ROWHTR into exclude list to prevent from getting disable
            Dim oExclude() As String = {"1", "2", "IDH_ACTVTY", "IDH_DOC", "IDH_CON", "IDH_CONV", "IDH_AVCONV", "IDH_AVDOC", "IDH_ORDLK", "IDH_ROWLK", "IDH_ROWHTR", "IDH_TXDT", "IDH_SAMREF", "IDH_SAMSTA"}
            '## MA End 07-08-2014
            DisableAllEditAndButtonItems(oForm, oExclude)

            Dim oAddGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            oAddGrid.doEnabled(False)

            If Config.ParameterAsBool("DOSHWAMD", False) = True Then
                setEnableItem(oForm, True, "IDH_Amend")
            End If
        End Sub

        Private Sub doGetItemGrpName(ByVal oForm As SAPbouiCOM.Form)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                Dim sGrpCode As String = getDFValue(oForm, msRowTable, "U_ItmGrp")
                If sGrpCode.Length > 0 Then
                    oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oRecordSet.DoQuery("select ItmsGrpNam from OITB where ItmsGrpCod = " & sGrpCode)
                    If oRecordSet.RecordCount > 0 Then
                        setUFValue(oForm, "IDH_ITMGRN", oRecordSet.Fields.Item(0).Value)
                    End If
                End If
            Catch ex As Exception
            Finally
                oRecordSet = Nothing
            End Try
        End Sub

        'OnTime Ticket 23675 - Disposal Order Booking Date
        'Disposal order booking date to be updated from the linked Waste Order's Actual Start Date
        'The data update depends on WR Config Key
        Private Sub doGetLinkWOASDate(ByVal oForm As SAPbouiCOM.Form)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                Dim sWRRowID As String = getDFValue(oForm, msRowTable, "U_WRRow", True).Trim()
                'getDFValue(oForm, msRowTable, "U_ItmGrp")
                If sWRRowID.Length > 0 Then
                    oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oRecordSet.DoQuery("select U_ASDate, U_ASTime from [@IDH_JOBSHD]  where Code = " & sWRRowID)
                    If oRecordSet.RecordCount > 0 Then
                        Dim oBTime As String = oRecordSet.Fields.Item(1).Value.ToString()
                        Dim oBDate As Date = oRecordSet.Fields.Item(0).Value.ToString()

                        If com.idh.utils.Dates.isValidDate(oBDate) Then
                            '                            Dim myBDate As String
                            '                            Dim myYear As String = Year(oBDate).ToString()
                            '                            Dim myMonth As String = ""
                            '
                            '                            If (Month(oBDate).ToString().Length < 2) Then
                            '                                myMonth = "0" + Month(oBDate).ToString()
                            '                            Else
                            '                                myMonth = Month(oBDate).ToString()
                            '                            End If
                            '                            Dim myDay As String = ""
                            '                            If (Day(oBDate).ToString().Length < 2) Then
                            '                                myDay = "0" + Day(oBDate).ToString()
                            '                            Else
                            '                                myDay = Day(oBDate).ToString()
                            '                            End If
                            '
                            '                            myBDate = myYear + myMonth + myDay
                            '
                            '                            setDFValue(oForm, "@IDH_DISPORD", "U_BDate", myBDate)
                            setDFValue(oForm, "@IDH_DISPORD", "U_BTime", oBTime)

                            setDFValue(oForm, "@IDH_DISPORD", "U_BDate", com.idh.utils.Dates.doDateToSBODateStr(oBDate))
                        End If
                    End If
                End If
            Catch ex As Exception
            Finally
                oRecordSet = Nothing
            End Try
        End Sub

        Public Overrides Function doAfterBtn1(ByVal oForm As SAPbouiCOM.Form, ByVal bDoNew As Boolean) As Boolean
            Dim oMode As SAPbouiCOM.BoFormMode = oForm.Mode
            If oMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse
                   oMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then

                If goParent.doCheckModal(oForm.UniqueID) = True Then
                    doSetModalData(oForm)
                Else
                    If doAutoPrint(oForm, oMode) Then
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                        setWFValue(oForm, "IDH_LoadSecondWeigh", False)
                        setFormDFValue(oForm, "Code", "")
                        doLoadData(oForm)
                        doSetFocusInFindMode(oForm)
                    Else
                        'oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                    End If
                End If
            ElseIf oMode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                setWFValue(oForm, "IDH_LoadSecondWeigh", False)
                setFormDFValue(oForm, "Code", "")
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                doLoadData(oForm)
            End If

            Dim oDO As IDH_DISPORD = thisDO(oForm)
            oDO = Nothing
            setWFValue(oForm, "DO", Nothing)

            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            oDOR = Nothing
            setWFValue(oForm, "DOR", Nothing)

            Return True
        End Function

        Protected Overrides Sub doSetModalData(ByVal oForm As SAPbouiCOM.Form)
            Dim oMode As SAPbouiCOM.BoFormMode = oForm.Mode
            If oMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse
                   oMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                doAutoPrint(oForm, oMode)
            End If
            MyBase.doSetModalData(oForm)
        End Sub

        'Return true if the Data needs to be reloaded
        Private Function doAutoPrint(ByVal oForm As SAPbouiCOM.Form, ByRef oMode As SAPbouiCOM.BoFormMode) As Boolean
            Dim bDoConv As String = getUFValue(oForm, "IDH_AVCONV")
            Dim bDoDOC As String = getUFValue(oForm, "IDH_AVDOC")
            Dim sReportFile As String

            If oMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse
                oMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then

                Dim bDoReloadNow As Boolean = True
                If bDoDOC.Equals("Y") Then
                    Dim sRowID As String = getDFValue(oForm, msRowTable, "Code")
                    Dim sCode As String = getDFValue(oForm, "@IDH_DISPORD", "Code").Trim()

                    If sRowID.Trim().Length > 0 Then

                        sReportFile = Config.Parameter("MDDREP")
                        If sReportFile Is Nothing Then
                            sReportFile = "AW Duty of Care v1.3 DO.rpt"
                        End If

                        Dim sShowReport As String = Config.ParamaterWithDefault("MDSWDR", "TRUE")
                        'If sShowReport Is Nothing Then
                        '    sShowReport = "TRUE"
                        'Else
                        '    sShowReport = sShowReport.ToUpper()
                        'End If


                        Dim sCopies As String = Config.Parameter("MDPCD")
                        If sCopies Is Nothing Then
                            sCopies = "1"
                        End If

                        Dim oParams As New Hashtable
                        oParams.Add("OrdNum", sCode)
                        oParams.Add("RowNum", sRowID)

                        setSharedData(oForm, "DORELOAD", "TRUE")

                        If (Config.INSTANCE.useNewReportViewer()) Then
                            IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                            IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                            IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
                        Else
                            IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, sShowReport, "TRUE", sCopies, oParams, gsType)
                        End If
                        bDoReloadNow = False
                    End If
                End If

                If bDoConv.Equals("Y") Then
                    Dim sDoPrintDialog As String = Config.Parameter("MDSWCR")
                    If sDoPrintDialog Is Nothing Then
                        sDoPrintDialog = "TRUE"
                    Else
                        sDoPrintDialog = sDoPrintDialog.ToUpper()
                    End If

                    Dim sRowID As String = getDFValue(oForm, msRowTable, "Code")
                    Dim sCode As String = getDFValue(oForm, "@IDH_DISPORD", "Code").Trim()

                    If sRowID.Trim().Length > 0 Then
                        'Check for in or out
                        If Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)) Then

                            sReportFile = Config.Parameter("MDCREPI")
                            If sReportFile Is Nothing Then
                                sReportFile = "WasteTransferNoteGoodsINv1.1.rpt"
                            End If
                        Else

                            sReportFile = Config.Parameter("MDCREPO")
                            If sReportFile Is Nothing Then
                                sReportFile = "WasteTransferNoteGoodsOUTv1.0.rpt"
                            End If
                        End If


                        Dim sCopies As String = Config.Parameter("MDPCC")
                        If sCopies Is Nothing Then
                            sCopies = "1"
                        End If

                        Dim sShowReport As String = Config.Parameter("MDSWCR")
                        If sShowReport Is Nothing Then
                            sShowReport = "TRUE"
                        Else
                            sShowReport = sShowReport.ToUpper()
                        End If

                        Dim oParams As New Hashtable
                        oParams.Add("OrdNum", sCode)
                        oParams.Add("RowNum", sRowID)

                        If bDoReloadNow = True Then
                            If sShowReport = "TRUE" OrElse sReportFile.StartsWith("http://") = False Then
                                setSharedData(oForm, "DORELOAD", "TRUE")
                            End If
                        End If

                        If (Config.INSTANCE.useNewReportViewer()) Then
                            IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                            IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                            IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTSHOW", sShowReport)
                            IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPRINT", "TRUE")
                            IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
                        Else
                            IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, sShowReport, "TRUE", sCopies, oParams, gsType)
                        End If

                        If bDoReloadNow = True Then
                            If sReportFile.StartsWith("http://") AndAlso sShowReport = "FALSE" Then
                                bDoReloadNow = True
                            Else
                                bDoReloadNow = False
                            End If
                        End If
                    End If
                End If

                setUFValue(oForm, "IDH_WEIB", "0")
                setUFValue(oForm, "IDH_SERB", "")
                setUFValue(oForm, "IDH_WDTB", "")

                Return bDoReloadNow
                'If bDoReloadNow Then
                '    oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                '    setFormDFValue(oForm, "Code", "")
                '    doLoadData(oForm)
                'Else
                '	oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                'End If
            End If
            Return False
        End Function

        'Private Function doLinkDOToWO(ByVal sHead As String, ByVal sRow As String) As Boolean
        '    Return MarketingDocs.doLinkDoToWO(sHead, sRow, msOrderType)
        'End Function

        Protected Overrides Function doOtherMarketingProcesses(ByVal oForm As SAPbouiCOM.Form, ByVal oChangedAddedRows As ArrayList) As Boolean
            Dim oUpdateGrid As UpdateGrid
            oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")

            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            'If oDOR.isLinkedToOrder() Then
            '    Return oDOR.doUpdateLinkedWO()
            'End If

            ''Step through the Rows and call the updater
            'Dim oDO As IDH_DISPORD = thisDO(oForm)
            ' ''For now while we are not 100% on DB Objects
            'oDO.doLoadChildren()

            'oDO.SBOForm = oDOR.SBOForm
            'oDO.CalledFromGUI = True
            'oDO.Handler_CheckPaymentReportTrigger = oDOR.Handler_CheckPaymentReportTrigger
            'Return oDO.doMarketingDocuments()

            'Dim oWrkDOR As IDH_DISPROW = New IDH_DISPROW()
            'oWrkDOR.SBOForm = oDOR.SBOForm
            'oWrkDOR.CalledFromGUI = True
            'oWrkDOR.Handler_CheckPaymentReportTrigger = oDOR.Handler_CheckPaymentReportTrigger
            'If oWrkDOR.getByKey(oDOR.Code) Then
            '    Return oWrkDOR.doMarketingDocuments()
            'End If
            'Return True

            Dim oWrkDOR As IDH_DISPROW = New IDH_DISPROW()
            oWrkDOR.SBOForm = oDOR.SBOForm
            'oWrkDOR.CalledFromGUI = True
            oWrkDOR.Handler_CheckPaymentReportTrigger = oDOR.Handler_CheckPaymentReportTrigger

            For iRows As Integer = 0 To oUpdateGrid.getRowCount - 1
                oUpdateGrid.setCurrentDataRowIndex(iRows)
                Dim sRowCode As String = oUpdateGrid.doGetFieldValue(IDH_DISPROW._Code)
                If sRowCode IsNot Nothing AndAlso sRowCode.Trim.Length > 0 AndAlso sRowCode.Equals("-1") = False Then
                    If oWrkDOR.getByKey(sRowCode) Then
                        oWrkDOR.doLinkedOrdersAndMarketingDocuments()
                    End If
                End If
            Next

            Return True
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then

                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse
                      oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then

                    oForm.Freeze(True)
                    Try
                        Dim oDOR As IDH_DISPROW = thisDOR(oForm)
                        oDOR.doDelayedPrices()

                        Dim oUpdateGrid As UpdateGrid
                        oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                        If doSetGridRowData(oForm, oUpdateGrid) = True Then
                            ''## MA Start 12-08-2014 Issue#313
                            Dim oDO As IDH_DISPORD = thisDO(oForm)

                            '' Updated By LPV Start 2015-06-03
                            ''If oDO.doUpdateAllROWsBookingDate(False) Then
                            ''## MA End 12-08-2014 Issue#313
                            MyBase.doButtonID1(oForm, pVal, BubbleEvent)
                            ''End If
                            '' Updated By LPV End 2015-06-03
                        Else
                            BubbleEvent = False
                        End If
                    Catch ex As Exception
                        Throw (ex)
                    Finally
                        oForm.Freeze(False)
                    End Try
                Else
                    MyBase.doButtonID1(oForm, pVal, BubbleEvent)
                End If
            End If
        End Sub

        '** This is where you will do checks before you continue with the Button1 Process
        Public Overrides Function doBeforeBtn1(ByVal oForm As SAPbouiCOM.Form) As Boolean
            doSetFocus(oForm, "IDH_VEHREG")

            If doRowValidation(oForm) = False Then
                Return False
            End If

            If doRequestCIPSIPUpdateOnOK(oForm) Then
                Return False
            End If

            Return MyBase.doBeforeBtn1(oForm)
        End Function

        Private Function doRowValidation(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

            'This should in future be the only Validation
            If oDOR.doPreAddValidation() = False Then
                Dim sResult As String = oDOR.LastValidationError
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("General Validation error.", "ERUSGENS", {com.idh.bridge.Translation.getTranslatedWord(sResult)})
                Return False
            End If

            Dim sHazProducer As String
            Dim sReg As String = getDFValue(oForm, msRowTable, "U_Lorry")
            If sReg.Length() = 0 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("The registration number must be set")
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("The registration number must be set", "ERUSGENS", {com.idh.bridge.Translation.getTranslatedWord("The registration number")})
                Return False
            End If
            Dim sCustomer As String = ""
            If Config.ParameterAsBool("ORCUSTRQ", True) Then
                sCustomer = getDFValue(oForm, msRowTable, IDH_DISPROW._CustCd) 'getFormDFValue(oForm, "U_CardCd")
                If sCustomer.Length() = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("The Customer must be set.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("The Customer must be set.", "ERUSGENS", {com.idh.bridge.Translation.getTranslatedWord("The Customer")})
                    Return False
                End If

                Dim sCustomerNm As String = getFormDFValue(oForm, "U_CardNm")
                If sCustomerNm.Length() = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("The Customer name must be set.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("The Customer name must be set.", "ERUSGENS", {com.idh.bridge.Translation.getTranslatedWord("The Customer name")})
                    Return False
                End If
            End If

            If Config.INSTANCE.getParameterAsBool("DOORIGRQ", False) Then
                Dim sOrigin As String = getDFValue(oForm, msRowTable, "U_Origin")
                If sOrigin.Length() = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("The Origin Waste must be set.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("The Origin Waste must be set.", "ERUSOWC", {Nothing})
                    Return False
                End If
            End If

            If Config.ParameterAsBool("DOCKWA", True) Then
                Dim sWasteCode As String
                sWasteCode = getDFValue(oForm, msRowTable, "U_WasCd")
                If sWasteCode Is Nothing OrElse sWasteCode.Length = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("The waste type is compulsory.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("The waste type is compulsory.", "ERUSWTC", {Nothing})
                    doSetFocus(oForm, "IDH_WASCL1")
                    Return False
                End If
            End If

            '*** Check for a valid Wastecode
            Dim sWastCd As String = getDFValue(oForm, msRowTable, "U_WasCd")
            Dim sWastDesc As String = Nothing
            If Config.ParameterAsBool("DOCWC", False) = True Then
                sWastDesc = Config.INSTANCE.doGetItemDescription(sWastCd)

                If sWastDesc.Length = 0 Then
                    'Config.INSTANCE.doCheckIsValidItem( sWastCd ) = False Then
                    Dim saParam As String() = {sWastCd}
                    doResError("Incorrect Waste Code entered: " & sWastCd, "ERRICWC", saParam)
                    doSetFocus(oForm, "IDH_WASCL1")
                    Return False
                End If
            End If

            '*** Check for a valid Waste Desccription
            If Config.ParameterAsBool("DOCWD", False) = True Then
                Dim sWastDsc As String = getDFValue(oForm, msRowTable, "U_WasDsc")
                If sWastDesc Is Nothing Then
                    sWastDesc = Config.INSTANCE.doGetItemDescription(sWastCd)
                End If

                If sWastDesc.Equals(sWastDsc) = False Then
                    Dim saParam As String() = {sWastDsc}
                    doResError("Incorrect Waste Description entered: " & sWastDsc, "ERRICWD", saParam)
                    doSetFocus(oForm, "IDH_WASMAT")
                    Return False
                End If
            End If

            '*** Check for a Valid Container
            If Config.ParameterAsBool("DOCCO", False) = True Then
                Dim sContainer As String
                sContainer = getDFValue(oForm, msRowTable, IDH_DISPROW._ItemCd)
                If sContainer Is Nothing OrElse sContainer.Length = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("The Container is compulsory.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("The Container is compulsory.", "ERUSCONC", {Nothing})
                    doSetFocus(oForm, "IDH_ITMCOD")
                    Return False
                End If
            End If

            If (oDOR.MarketingMode = Config.MarketingMode.PO) Then
                'If getWFValue(oForm, "FormMode") = "PO" Then
                sHazProducer = getFormDFValue(oForm, "U_PCardNm")
                '*** now validate PUOM
                'OnTime 22323: UOM validation
                'Dim sPUOM As String = getFormDFValue(oForm, "U_ProUOM")
                Dim sPUOM As String = getDFValue(oForm, msRowTable, "U_ProUOM")
                'if it is empty or 'CM' dont validate
                'Note: 'CM' UOM is coming from business logic: so it doesn't exist in the OWGT table
                If (Not (sPUOM = String.Empty)) And
                   (Not (sPUOM.ToUpper() = "cm".ToUpper())) Then
                    If Not Config.INSTANCE.isValidUOM(sPUOM) Then
                        Dim saParam As String() = {sPUOM}
                        doSetFocus(oForm, "IDH_PURUOM")
                        doResError("Incorrect PURUOM entered: " & sPUOM, "INVLDUOM", saParam)
                        Return False
                    End If
                End If
            Else
                'sHazProducer = sCustomerNm
                sHazProducer = getFormDFValue(oForm, "U_PCardNm")

                '*** Check for a valid UOM
                'OnTime 22323: UOM validation
                'Note: validate UOM right in the begining of the price calculation
                Dim sUOM As String = getDFValue(oForm, msRowTable, "U_UOM")
                'if it is empty or 'CM' dont validated
                'Note: 'CM' UOM is coming from business logic: so it doesn't exist in the OWGT table
                If (Not (sUOM = String.Empty)) And
                   (Not (sUOM.ToUpper() = "cm".ToUpper())) Then
                    If Not Config.INSTANCE.isValidUOM(sUOM) Then
                        Dim saParam As String() = {sUOM}
                        doSetFocus(oForm, "IDH_UOM")
                        doResError("Incorrect UOM entered: " & sUOM, "INVLDUOM", saParam)
                        Return False
                    End If
                End If
            End If

            Dim dW1 As Double = getDFValue(oForm, msRowTable, "U_Wei1")
            Dim dCustW As Double = getDFValue(oForm, msRowTable, "U_CstWgt")
            Dim dPurchWgt As Double = getDFValue(oForm, msRowTable, "U_ProWgt")
            Dim sCheckWeight As String = Config.Parameter("DOCKWE")
            If sCheckWeight.ToUpper.Equals("TRUE") Then
                If (oDOR.MarketingMode = Config.MarketingMode.SO) Then
                    'If getWFValue(oForm, "FormMode") = "SO" Then
                    If dW1 = 0 AndAlso dCustW = 0 Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError("There is no valid weight, the row could not be saved.")
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("There is no valid weight, the row could not be saved.", "ERUSWRNS", {Nothing})
                        Return False
                    End If
                Else
                    If dW1 = 0 AndAlso dPurchWgt = 0 Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError("There is no valid weight, the row could not be saved.")
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("There is no valid weight, the row could not be saved.", "ERUSWRNS", {Nothing})
                        Return False
                    End If
                End If
            End If

            If dCustW <> 0 Then
                Dim sCheckAccountFlags As String = Config.Parameter("DOWNAF")
                If sCheckAccountFlags.ToUpper.Equals("TRUE") Then
                    If Not (getUFValue(oForm, "IDH_FOC").Equals("Y") OrElse
                            getUFValue(oForm, "IDH_DOORD").Equals("Y") OrElse
                            getUFValue(oForm, "IDH_DOARI").Equals("Y") OrElse
                            getUFValue(oForm, "IDH_AINV").Equals("Y")
                            ) Then

                        If Messages.INSTANCE.doResourceMessageYNAsk("JBNAFS", Nothing, 1) = 2 Then
                            Return False
                        End If
                    End If
                End If
            End If

            Dim dW2 As Double = getDFValue(oForm, msRowTable, "U_Wei2")
            If Config.INSTANCE.getParameterAsBool("DOWEWA", True) = True Then

                'If getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp).Equals(Config.INSTANCE.doGetJopTypeIncoming()) Then
                If Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)) Then
                    If dW2 > dW1 Then
                        'If goParent.goApplication.M_essageBox("Please note the the second Weight is greater that the first weight." & Chr(10) & "Do you want to continue?", 1, "Yes", "No") = 2 Then
                        If Messages.INSTANCE.doResourceMessageYNAsk("DOSWBFW", Nothing, 1) = 2 Then
                            Return False
                        End If
                    End If
                Else
                    If dW1 > dW2 Then
                        'If goParent.goApplication.M_essageBox("Please note the the first Weight is greater that the second weight." & Chr(10) & "Do you want to continue?", 1, "Yes", "No") = 2 Then
                        If Messages.INSTANCE.doResourceMessageYNAsk("DOFWBSW", Nothing, 1) = 2 Then
                            Return False
                        End If
                    End If
                End If
            End If

            Dim bContinue As Boolean = True
            Dim bFoc As String = getUFValue(oForm, "IDH_FOC")
            Dim sPStatus As String = getDFValue(oForm, msRowTable, "U_PStat")
            Dim sStatus As String = getDFValue(oForm, msRowTable, "U_Status")

            'If Not (sPStatus.StartsWith(Config.INSTANCE.getStatusPreFixDo()) OrElse _
            '	sPStatus.StartsWith(Config.INSTANCE.getStatusPreFixReq())) Then
            If com.idh.bridge.lookups.FixedValues.isSetForAction(sPStatus) = False Then
                If bFoc.Equals("Y") Then
                    If Config.INSTANCE.getParameterAsBool("DOFOWA", True) = True Then
                        'If goParent.goApplication.M_essageBox("The Disposal order will not create a Marketing Document. " & Chr(10) & "Do you want to continue?", 1, "Yes", "No") <> 1 Then
                        If Messages.INSTANCE.doResourceMessageYNAsk("DONMD", Nothing, 1) = 2 Then
                            Return False
                        End If
                    End If
                    '            	ElseIf com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) OrElse _
                    '                    com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus) Then
                ElseIf com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) Then
                    Dim dChargeTotal As Double = 0
                    dChargeTotal = getUnCommittedRowTotals(oForm, getDFValue(oForm, msRowTable, "Code"), getDFValue(oForm, msRowTable, "U_Total"))
                    If Config.INSTANCE.doCheckCredit(sCustomer, dChargeTotal, True) = False Then
                        Return False
                    End If
                End If
            End If

            Dim bISUSARelease As Boolean = Config.INSTANCE.getParameterAsBool("USAREL", False)
            If bISUSARelease = False Then
                Dim sConNum As String = getDFValue(oForm, msRowTable, "U_ConNum")
                '			Dim dReadWgt As Double 	= getDFValue(oForm, msRowTable, "U_Wei2")
                '			Dim dAUOMQt As Double 	= getDFValue(oForm, msRowTable, "U_AUOMQt")
                '			If ((dW1 > 0 AndAlso dW2 > 0) OrElse dReadWgt > 0 OrElse dAUOMQt > 0 ) AndAlso _
                '                  (Not sWastCd Is Nothing AndAlso sWastCd.Length > 0) Then
                If Not sWastCd Is Nothing AndAlso sWastCd.Length > 0 Then
                    'Dim sConNum As String = getDFValue(oForm, msRowTable, "U_ConNum")
                    If com.idh.bridge.lookups.Config.INSTANCE.isHazardousItem(sWastCd) = True Then
                        If sConNum Is Nothing OrElse sConNum.Length = 0 Then
                            Dim saParam As String() = {sWastCd}
                            doResError("A Consignment Number must be entered for Waste Code: " & sWastCd, "ERRCNREQ", saParam)
                            doSetFocus(oForm, "IDH_HAZCD")
                            Return False
                        End If
                    End If

                    If sConNum.Length > 0 Then
                        If com.idh.bridge.utils.General.doCheckConNumber(sConNum, sHazProducer) = False Then
                            Dim saParam As String() = {sConNum}
                            doResError("A Consignment Number Format Error: " & sConNum, "ERRCNFE", saParam)
                            doSetFocus(oForm, "IDH_HAZCD")
                            Return False
                        End If
                    End If
                End If

                If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("DOCHKLDS") = True Then
                    Dim sLoadSheet As String = getDFValue(oForm, msRowTable, com.idh.dbObjects.User.IDH_DISPROW._LoadSht)
                    If sLoadSheet Is Nothing OrElse sLoadSheet.Length = 0 Then
                        If com.idh.bridge.lookups.Config.INSTANCE.isBatchtem(sWastCd) Then
                            doResError("The Loadsheet must be selected.", "DOLDSHT", Nothing)
                            doSetFocus(oForm, "IDH_LOADSH")
                            'com.idh.bridge.DataHandler.INSTANCE.doError("The Loadsheet must be selected.")
                            Return False
                        End If
                    End If
                End If
            End If

            '20150920: Dimeca DO Validations 

            Dim sTransCd As String = getDFValue(oForm, msRowTable, IDH_DISPROW._TrnCode) 'getSharedData(oForm, "TRNCODE")
            Dim sTransDsc As String = String.Empty
            If String.IsNullOrWhiteSpace(sTransCd) = False AndAlso dW2 > 0 Then
                Dim oTransCode As IDH_TRANCD = New IDH_TRANCD()
                If oTransCode.getByKey(sTransCd) Then
                    sTransDsc = oTransCode.U_Process
                End If

                'Check if Waste Code is 'Purchase Over Weighbridge ' 
                Dim sIDHCWBB As String = Config.INSTANCE.getValueFromOITM(sWastCd, "U_IDHCWBB").ToString
                Dim bWastePurOverWB As Boolean = sIDHCWBB.Equals("Y")

                'Determine Transaction Code by attributes 
                Dim sTCNature As String = String.Empty 'SELLINGDO or BUYINGDO or INTERNALTRANSDO

                If Config.ParameterAsBool("DIMDOSO", False) Then
                    If (oTransCode.U_MarkDoc1 = "17" OrElse oTransCode.U_MarkDoc2 = "17" _
                            OrElse oTransCode.U_MarkDoc3 = "17" OrElse oTransCode.U_MarkDoc4 = "17") _
                            AndAlso Config.INSTANCE.isOutgoingJob(Config.ParameterWithDefault("DOSODGRP", Config.INSTANCE.doGetDOContainerGroup()), oTransCode.U_OrdType) Then
                        'Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), oTransCode.U_OrdType)
                        'OR
                        'Config.INSTANCE.isOutgoingJob("201", oTransCode.U_OrdType) 
                        sTCNature = "SELLINGDO"
                    End If
                End If

                If Config.ParameterAsBool("DIMDOBUY", False) Then
                    If (oTransCode.U_MarkDoc1 = "22" OrElse oTransCode.U_MarkDoc2 = "22" _
                            OrElse oTransCode.U_MarkDoc3 = "22" OrElse oTransCode.U_MarkDoc4 = "22") _
                            AndAlso Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), oTransCode.U_OrdType) Then

                        sTCNature = "BUYINGDO"
                    End If
                End If

                If Config.ParameterAsBool("DIMDOST", False) Then
                    If oTransCode.U_MarkDoc1 = "" AndAlso oTransCode.U_MarkDoc2 = "" _
                            AndAlso oTransCode.U_MarkDoc3 = "" AndAlso oTransCode.U_MarkDoc4 = "" _
                            AndAlso oTransCode.U_Stock = "67" _
                            AndAlso Config.INSTANCE.isOutgoingJob(Config.ParameterWithDefault("DOSTDGRP", Config.INSTANCE.doGetDOContainerGroup()), oTransCode.U_OrdType) Then

                        sTCNature = "INTERNALTRANSDO"
                    End If
                End If

                'Sales DO 
                If Config.ParameterAsBool("DIMDOSO", False) Then
                    'Rule: Outgoing DO + must have WOH and WOR + Selling Transaction Code 
                    If sTCNature = "SELLINGDO" Then
                        If getDFValue(oForm, msRowTable, IDH_DISPROW._WROrd).ToString().Length <= 0 _
                                OrElse getDFValue(oForm, msRowTable, IDH_DISPROW._WRRow).ToString().Length <= 0 _
                                OrElse Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)) = False Then
                            doResError("Config (DIMDOSO) Validation rule failed.", "EREDOSO", Nothing)
                            Return False
                        End If
                    End If
                End If

                'Buying DO 
                If Config.ParameterAsBool("DIMDOBUY", False) Then
                    'Rule: Buying Transaction Code + Must have Price 
                    If sTCNature = "BUYINGDO" Then
                        If Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)) = False _
                                OrElse bWastePurOverWB = False _
                                OrElse getDFValue(oForm, msRowTable, IDH_DISPROW._PCost) <= 0 Then
                            doResError("Config (DIMDOBUY) Validation rule failed.", "EREDOBUY", Nothing)
                            Return False
                        End If
                    End If
                End If

                'Internal Stock Transfer DO 
                If Config.ParameterAsBool("DIMDOST", False) Then
                    'Rule: Outgoing DO + must have WOH and WOR + Internal Transfer Transaction Code 
                    If sTCNature = "INTERNALTRANSDO" Then
                        If getDFValue(oForm, msRowTable, IDH_DISPROW._WROrd).ToString().Length <= 0 _
                                OrElse getDFValue(oForm, msRowTable, IDH_DISPROW._WRRow).ToString().Length <= 0 _
                                OrElse Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)) = False Then
                            doResError("Config (DIMDOST) Validation rule failed.", "EREDOST", Nothing)
                            Return False
                        End If
                    End If
                End If
            End If

            Return True
        End Function

        '     Private Function doCalcWBWeightTotal(ByVal oForm As SAPbouiCOM.Form) As Boolean
        '         Dim dW1 As Double
        '         Dim dW2 As Double
        '         Dim dWTotal As Double = 0
        '         Try
        '             dW1 = getDFValue(oForm, msRowTable, "U_Wei1")
        '             dW2 = getDFValue(oForm, msRowTable, "U_Wei2")
        '             If dW1 > 0 AndAlso dW2 > 0 Then
        '                 dWTotal = dW2 - dW1
        '                 If dWTotal < 0 Then
        '                     dWTotal = dWTotal * -1
        '                 End If
        '             End If
        '         Catch ex As Exception
        '             com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error calculating the WB Weight.")
        '         End Try

        '         If dWTotal <> 0 Then
        '             Dim sUOM As String = getCurrentUOM(oForm)
        '             Config.INSTANCE.doCheckMaxWeight(goParent, "DO", dWTotal, Config.ParameterWithDefault("WBUOM", "kg"))
        '         End If
        '         setDFValue(oForm, msRowTable, IDH_DISPROW._RdWgt, dWTotal)
        'setDFValue(oForm, msRowTable, IDH_DISPROW._TRdWgt, dWTotal)
        'setDFValue(oForm, msRowTable, IDH_DISPROW._PRdWgt, dWTotal)

        '         'Update the Adjusted Weight
        '         If dWTotal <> 0 Then
        '             Dim dDed As Double = getDFValue(oForm, msRowTable, "U_WgtDed")
        '             dWTotal = dWTotal - dDed

        '             setDFValue(oForm, msRowTable, "U_AdjWgt", dWTotal)
        '         End If

        '         If dWTotal <> 0 Then
        '             doItAll(oForm)
        '         End If
        '     End Function

        'Private Function getCurrentUOM(ByVal oForm As SAPbouiCOM.Form) As String
        '    Dim sUOM As String
        '    If getWFValue(oForm, "FormMode") = "SO" Then
        '        sUOM = getDFValue(oForm, msRowTable, "U_UOM")
        '    Else
        '        sUOM = getDFValue(oForm, msRowTable, "U_ProUOM")
        '    End If
        '    Return sUOM
        'End Function

        ''OnTime 22323: UOM validation
        ''Note: validate UOM right in the begining of the price calculation
        'Private Function isValidUOM(ByVal oForm As SAPbouiCOM.Form, ByRef strUOM As String) As Boolean
        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '    Dim bolValidateUOM As Boolean = True

        '    Try
        '        oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        '        oRecordSet.DoQuery("select UnitCode from OWGT")
        '        If oRecordSet.RecordCount > 0 Then
        '            Dim oWeightMeasures As SAPbobsCOM.WeightMeasures = Nothing
        '            oWeightMeasures = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oWeightMeasures)
        '            oWeightMeasures.Browser.Recordset = oRecordSet
        '            While Not oWeightMeasures.Browser.EoF
        '                If strUOM.ToUpper() = oWeightMeasures.UnitDisplay.ToUpper() Then
        '                    bolValidateUOM = True
        '                    Exit While
        '                Else
        '                    'com.idh.bridge.DataHandler.INSTANCE.doError("Invalid UOM! Please provide a valid UOM.")
        '                    bolValidateUOM = False
        '                End If
        '                oWeightMeasures.Browser.MoveNext()
        '            End While
        '            Return bolValidateUOM
        '        End If
        '        Return bolValidateUOM
        '    Catch ex As Exception
        '    Finally
        '        oRecordSet = Nothing
        '    End Try
        'End Function

        'Public Function isLinkToWO(ByVal oForm As SAPbouiCOM.Form) As Boolean
        '    Dim sLinkedWOR As String = getDFValue(oForm, msRowTable, IDH_DISPROW._WRRow)
        '    If Not sLinkedWOR Is Nothing AndAlso sLinkedWOR.Length > 0 Then
        '        Return True
        '    End If
        '    Return False
        'End Function

        'Private Sub doItAllNew(ByVal oForm As SAPbouiCOM.Form)
        '    Dim oDOR As IDH_DISPROW = thisDOR(oForm)

        '    doSetCheckBoxes(oForm)
        'End Sub

        Protected Sub doSetCheckBoxes(ByVal oForm As SAPbouiCOM.Form)
            Dim dW1 As Double
            Dim dW2 As Double
            Dim dAlternative As Double
            Dim dReadWeight As Double
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            Try
                dW1 = getDFValue(oForm, msRowTable, "U_Wei1")
                dW2 = getDFValue(oForm, msRowTable, "U_Wei2")

                Dim sCardCode As String = getDFValue(oForm, "@IDH_DISPORD", "U_CardCd")
                Dim sCustUOM As String = Config.INSTANCE.getBPUOM(sCardCode)
                Dim sDefUOM As String = Config.INSTANCE.getDefaultUOM()
                If sCustUOM.Equals(sDefUOM) = False Then
                    dAlternative = getDFValue(oForm, msRowTable, "U_AUOMQt")
                Else
                    dAlternative = 0
                End If
                dReadWeight = getDFValue(oForm, msRowTable, "U_RdWgt")

            Catch ex As Exception
            End Try
            'Now we ll call this method on change of values in weight fields so if it qualify to tick the checkbox so to do it.
            ''Note: 20150508 - As per discussion with HN of MA on this
            ''the tick/untick of the checkbox is solely based on the conditions below. 
            'Dim sAVDOC As String = Config.Parameter("MDDAPD")
            'If Not (sAVDOC Is Nothing) AndAlso sAVDOC.ToUpper().Equals("TRUE") Then
            '    setUFValue(oForm, "IDH_AVDOC", "Y")
            'Else
            '    setUFValue(oForm, "IDH_AVDOC", "N")
            'End If
            'Dim sAVCONV As String = Config.Parameter("MDDAPC")
            'If Not (sAVCONV Is Nothing) AndAlso sAVCONV.ToUpper().Equals("TRUE") Then
            '    setUFValue(oForm, "IDH_AVCONV", "Y")
            'Else
            '    setUFValue(oForm, "IDH_AVCONV", "N")
            'End If
            Dim sAVDOC As String = Config.Parameter("MDDAPD")
            Dim sAVCONV As String = Config.Parameter("MDDAPC")
            If (dW1 > 0 AndAlso dW2 > 0) OrElse dAlternative <> 0 OrElse dReadWeight <> 0 Then

                If String.IsNullOrWhiteSpace(oDOR.U_TrnCode) Then
                    'Dim oItems() As String = {"IDH_FOC", "IDH_AVDOC", "IDH_AVCONV", "IDH_DOORD", "IDH_AINV", "IDH_DOARI", "IDH_BOOKIN"}
                    setEnableItems(oForm, True, moMarketingCheckboxes)

                    doSetEnabled(oForm.Items.Item("IDH_DOARIP"), False)
                End If

                Dim sWastCd As String = getDFValue(oForm, msRowTable, "U_WasCd")
                Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)
                'If  sJobType.Equals(Config.INSTANCE.doGetJopTypeOutgoing()) AndAlso _
                If Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) AndAlso
                 com.idh.bridge.lookups.Config.INSTANCE.isHazardousItem(sWastCd) = True Then
                    setUFValue(oForm, "IDH_AVDOC", "Y")
                    setUFValue(oForm, "IDH_AVCONV", "Y")
                Else
                    If Not (sAVDOC Is Nothing) AndAlso sAVDOC.ToUpper().Equals("TRUE") Then
                        setUFValue(oForm, "IDH_AVDOC", "Y")
                    Else
                        setUFValue(oForm, "IDH_AVDOC", "N")
                    End If
                    If Not (sAVCONV Is Nothing) AndAlso sAVCONV.ToUpper().Equals("TRUE") Then
                        setUFValue(oForm, "IDH_AVCONV", "Y")
                    Else
                        setUFValue(oForm, "IDH_AVCONV", "N")
                    End If
                End If
            Else
                setUFValue(oForm, "IDH_DOARIP", "N")
                doSetEnabled(oForm.Items.Item("IDH_DOARIP"), False)
                setUFValue(oForm, "IDH_AVCONV", "N")
                setUFValue(oForm, "IDH_AVDOC", "N")
            End If
        End Sub


        Private Sub doApplyStatuses(ByVal oForm As SAPbouiCOM.Form)
            ' 20150219 - LPV - BEGIN - Move Items to groups
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

            Dim sStatus As String = oDOR.U_Status
            Dim sPStatus As String = oDOR.U_PStat
            Dim sJournalStatus As String = oDOR.U_JStat

            Dim sCarrierCN As String = oDOR.U_TPCN
            Dim sCustomerCN As String = oDOR.U_TCCN
            'Dim sAEDate As String = getFormDFValue(oForm, "U_AEDate")

            'If oDOR.doCheckHaveMarketingDocuments(False) Then
            '    oForm.SupportedModes = SAPbouiCOM.BoFormMode.fm_OK_MODE
            '    oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE

            ''If Not sAEDate Is Nothing AndAlso sAEDate.Length > 0 Then
            ''    DisableAllEditItems(oForm, moJobEndEnabled)
            ''Else
            'DisableAllEditItems(oForm, moJobOpenEnabled)

            'doSwitchSOFields(oForm)
            'doSwitchPOFields(oForm)
            'doSwitchProducerFields(oForm)

            'doSetOrdered(oForm)
            ''End If
            'Else
            'Dim oAddGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            'oAddGrid.doEnabled(True)

            ''If Not sAEDate Is Nothing AndAlso sAEDate.Length > 0 Then
            ''    DisableAllEditItems(oForm, moJobEndEnabled)
            ''Else
            'DisableAllEditItems(oForm, moJobOpenEnabled)
            ''End If
            'doSwitchSOFields(oForm)
            'doSwitchPOFields(oForm)
            'doSwitchProducerFields(oForm)

            If getWFValue(oForm, "LOCKCNT") = True Then
                setEnableItem(oForm, False, "IDH_ITMCOD")
                setEnableItem(oForm, False, "IDH_DESC")
                setEnableItem(oForm, False, "IDH_ITCF")
            End If
            'End If

            ' ''##MA Start 17-09-2014 Issue#427
            'If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_WOR_REQUESTDATE) = False Then '8192
            '    setEnableItem(oForm, False, "IDH_REQDAT")
            '    setEnableItem(oForm, False, "IDH_REQTIM")
            '    setEnableItem(oForm, False, "IDH_REQTIT")
            'End If
            ' ''##MA End 17-09-2014 Issue#427
            If com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus) = True Then
                Dim sPayMeth As String = oDOR.U_PayMeth
                If Not sPayMeth Is Nothing AndAlso sPayMeth.Equals(com.idh.bridge.lookups.FixedValues.getPayMethPrePaid()) Then
                    doSwitchCheckBox(oForm, "IDH_AINV", "Y", False)
                Else
                    doSwitchCheckBox(oForm, "IDH_DOARI", "Y", False)
                End If

                'If com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus) = True Then
                '    doSwitchCheckBox(oForm, "IDH_DOARI", True, False)
            ElseIf com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) = True Then
                doSwitchCheckBox(oForm, "IDH_DOORD", "Y", False)
            ElseIf com.idh.bridge.lookups.FixedValues.isWaitingForFoc(sStatus) = True Then
                doSwitchCheckBox(oForm, "IDH_FOC", "Y", False)
            ElseIf com.idh.bridge.lookups.FixedValues.isWaitingForRebate(sStatus) = True Then
                oDOR.U_CustReb = "Y"
                doSwitchCheckBox(oForm, "IDH_CUSTRE", "Y", False)
            Else
                doSwitchCheckBox(oForm, "IDH_FOC", "Y", False)
            End If

            If com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sPStatus) = True Then
                doSwitchCheckBox(oForm, "IDH_DOPO", "Y", False)
            Else
                doSwitchCheckBox(oForm, "IDH_DOPO", "N", False)
            End If

            If oDOR.U_BookIn = "B" OrElse oDOR.U_BookIn = "D" Then
                doSwitchCheckBox(oForm, "IDH_BOOKIN", "Y", False)
            Else
                doSwitchCheckBox(oForm, "IDH_BOOKIN", "N", False)
            End If

            'If sPStatus.Trim().Length = 0 OrElse sPStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusOpen()) Then
            '    If oForm.Items.Item("IDH_DOPO").Enabled Then
            '        setUFValue(oForm, "IDH_DOPO", "N")
            '    End If
            'ElseIf sPStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusOrdered()) _
            '        OrElse (Not sCarrierCN Is Nothing AndAlso sCarrierCN.Length > 0) Then
            '    If oForm.Items.Item("IDH_DOPO").Enabled Then
            '        doSwitchCheckBox(oForm, "IDH_DOPO", True, False)
            '    End If
            'End If

            'If com.idh.bridge.lookups.FixedValues.isWaitingForRebate(sPStatus) = True Then
            '    oDOR.U_CarrReb = "Y"
            '    doSwitchCheckBox(oForm, "IDH_CARRRE", True, False)
            'ElseIf com.idh.bridge.lookups.FixedValues.isSetForAction(sPStatus) Then
            '    doSwitchCheckBox(oForm, "IDH_DOPO", True, False)
            'End If

            ''Do The Rebate statuses
            'Dim bDoCRebate As Boolean = Config.ParameterAsBool("WOCREB", False)
            'If bDoCRebate = True Then
            '    Dim sWastCd As String = oDOR.U_WasCd
            '    Dim bIsRebateItem As Boolean = Config.INSTANCE.doCheckIsCarrierRebate(sWastCd)

            '    If sCarrierCN Is Nothing OrElse sCarrierCN.Length = 0 Then
            '        oForm.Items.Item("IDH_CARRRE").Enabled = True

            '        If getWFValue(oForm, "ISNEW") = True Then
            '            If bIsRebateItem = True Then
            '                oDOR.U_CarrReb = "Y"
            '                doSwitchCheckBox(oForm, "IDH_CARRRE", True, False)
            '            End If
            '        End If
            '    Else
            '        oForm.Items.Item("IDH_CARRRE").Enabled = False
            '    End If

            '    Dim sTCCN As String = getFormDFValue(oForm, "U_TCCN")
            '    If sTCCN Is Nothing OrElse sTCCN.Length = 0 Then
            '        oForm.Items.Item("IDH_CUSTRE").Enabled = True

            '        If getWFValue(oForm, "ISNEW") = True Then
            '            If bIsRebateItem = True Then
            '                oDOR.U_CustReb = "Y"
            '                doSwitchCheckBox(oForm, "IDH_CUSTRE", True, False)
            '            End If
            '        End If
            '    Else
            '        oForm.Items.Item("IDH_CUSTRE").Enabled = False
            '    End If
            'Else
            '    oForm.Items.Item("IDH_CARRRE").Enabled = False
            '    oForm.Items.Item("IDH_CUSTRE").Enabled = False
            'End If

            'Set Journal Status
            If FixedValues.isJournaled(sJournalStatus) Then
                setEnableItem(oForm, False, "IDH_DOJRN")
            End If

            If com.idh.bridge.lookups.FixedValues.isWaitingForJournal(sJournalStatus) = True Then
                doSwitchCheckBox(oForm, "IDH_DOJRN", True, False)
            End If

            'doSwitchPriceFields(oForm)

            'doCheckMarketingCheckBoxesAuthorization(oForm)

            ' 20150219 - LPV - END 
        End Sub

        ' Y - set checkbox to ticked
        ' N - set checkbox to not ticked
        ' S - no to change tick
        Protected Sub doSwitchCheckBox(ByVal oForm As SAPbouiCOM.Form, ByVal sID As String, ByVal sCheckboxSwitch As String, ByVal bDoSetStatusValue As Boolean)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            'If (oDOR.TRNCdDecodeRecord IsNot Nothing AndAlso bDoSetStatusValue = True) Then
            '    Exit Sub
            'End If

            If sID Is Nothing Then
                Exit Sub
            End If

            Dim sAction As String = ""
            Dim sPStatus As String
            Dim sStatus As String

            If bDoSetStatusValue = True Then
                Dim sDoMDDocs As String = Config.Parameter("MDAGED")
                If sDoMDDocs.ToUpper().Equals("TRUE") Then
                    sAction = "D" 'Do      'Config.INSTANCE.getStatusPreFixDo()
                Else
                    sAction = "R" 'Request 'Config.INSTANCE.getStatusPreFixReq()
                End If
            End If

            If sID.Equals("IDH_BOOKIN") Then
                Try
                    If sCheckboxSwitch <> "S" Then
                        setUFValue(oForm, sID, sCheckboxSwitch)
                    End If
                Catch ex As Exception
                End Try

                If bDoSetStatusValue Then
                    Dim sBStatus As String = getDFValue(oForm, msRowTable, "U_BookIn")
                    If sBStatus = "D" Then
                        setEnableItem(oForm, False, "IDH_BOOKIN")
                    Else
                        If getUFValue(oForm, "IDH_BOOKIN") = "Y" Then
                            setDFValue(oForm, msRowTable, "U_BookIn", "B")
                        Else
                            setDFValue(oForm, msRowTable, "U_BookIn", "N")
                        End If
                    End If
                End If
            ElseIf sID.Equals("IDH_DOPO") Then

                Try
                    If sCheckboxSwitch <> "S" Then
                        setUFValue(oForm, sID, sCheckboxSwitch)
                    End If
                Catch ex As Exception
                End Try

                If bDoSetStatusValue = True Then
                    If getUFValue(oForm, "IDH_DOPO") = "Y" Then
                        If sAction = "D" Then
                            sPStatus = com.idh.bridge.lookups.FixedValues.getDoOrderStatus()
                            'sStatus = com.idh.bridge.lookups.FixedValues.getDoFocStatus()
                        Else
                            sPStatus = com.idh.bridge.lookups.FixedValues.getReqOrderStatus()
                            'sStatus = com.idh.bridge.lookups.FixedValues.getReqFocStatus()
                        End If

                        setDFValue(oForm, msRowTable, "U_PStat", sPStatus) 'sPreFix & com.idh.bridge.lookups.FixedValues.getStatusOrder())
                        'setDFValue(oForm, msRowTable, "U_Status", sStatus) 'sPreFix & com.idh.bridge.lookups.FixedValues.getStatusFoc())
                        'setDFValue(oForm, msRowTable, "U_PayMeth", com.idh.bridge.lookups.FixedValues.getPayMethFoc())
                        'setDFValue(oForm, msRowTable, "U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusFree())
                        'setDFValue(oForm, msRowTable, "U_CCNum", "")
                        'setDFValue(oForm, msRowTable, "U_CCType", "0")
                        'setDFValue(oForm, msRowTable, "U_CCStat", "")
                    Else
                        setDFValue(oForm, msRowTable, "U_PStat", com.idh.bridge.lookups.FixedValues.getStatusOpen())
                    End If
                    'Else
                    '    setUFValue(oForm, "IDH_FOC", "Y")
                End If

            ElseIf sID.Equals("IDH_DOORD") OrElse
               sID.Equals("IDH_DOARI") OrElse
               sID.Equals("IDH_AINV") OrElse
               sID.Equals("IDH_DOARIP") OrElse
               sID.Equals("IDH_FOC") OrElse
               sID.Equals("IDH_JRNL") OrElse
               sID.Equals("IDH") Then

                'setUFValue(oForm, "IDH_DOPO", "N")

                If sID.Equals("IDH_FOC") = False Then
                    setUFValue(oForm, "IDH_FOC", "N")
                End If

                If sID.Equals("IDH_DOORD") = False Then
                    setUFValue(oForm, "IDH_DOORD", "N")
                End If

                If sID.Equals("IDH_DOARI") = False Then
                    setUFValue(oForm, "IDH_DOARI", "N")
                End If

                If sID.Equals("IDH_DOARIP") = False Then
                    setUFValue(oForm, "IDH_DOARIP", "N")
                End If

                If sID.Equals("IDH_AINV") = False Then
                    setUFValue(oForm, "IDH_AINV", "N")
                End If

                If sID.Equals("IDH_JRNL") = False Then
                    setUFValue(oForm, "IDH_JRNL", "N")
                End If

                If sID.StartsWith("IDH") = True AndAlso sID.Equals("IDH") = False Then
                    Try
                        If sCheckboxSwitch <> "S" Then
                            setUFValue(oForm, sID, sCheckboxSwitch)
                        End If
                    Catch ex As Exception
                    End Try

                    'Dim sStatus As String
                    If bDoSetStatusValue = True Then
                        If getUFValue(oForm, sID) = "Y" Then
                            If sID.Equals("IDH_DOARI") Then
                                'If getUFValue(oForm, "IDH_DOARI") = "Y" Then
                                'setSharedData(oForm, "PREFIX", sPreFix)
                                setSharedData(oForm, "ACTION", sAction)
                                goParent.doOpenModalForm("IDHPAYMET", oForm)
                                'End If
                            ElseIf sID.Equals("IDH_AINV") Then
                                'If getUFValue(oForm, "IDH_AINV") = "Y" Then
                                If sAction = "D" Then
                                    sStatus = com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus()
                                Else
                                    sStatus = com.idh.bridge.lookups.FixedValues.getReqInvoiceStatus()
                                End If

                                setDFValue(oForm, msRowTable, "U_Status", sStatus) 'sPreFix & com.idh.bridge.lookups.FixedValues.getStatusInvoice())
                                setDFValue(oForm, msRowTable, "U_PayMeth", com.idh.bridge.lookups.FixedValues.getPayMethPrePaid())
                                setDFValue(oForm, msRowTable, "U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusPaid())
                                setDFValue(oForm, msRowTable, "U_CCNum", "")
                                setDFValue(oForm, msRowTable, "U_CCType", "0")
                                setDFValue(oForm, msRowTable, "U_CCStat", "")
                                'End If
                            ElseIf sID.Equals("IDH_DOARIP") Then
                                'If getUFValue(oForm, "IDH_AINV") = "Y" Then
                                If sAction = "D" Then
                                    sStatus = com.idh.bridge.lookups.FixedValues.getDoPInvoiceStatus()
                                Else
                                    sStatus = com.idh.bridge.lookups.FixedValues.getReqPInvoiceStatus()
                                End If

                                setDFValue(oForm, msRowTable, "U_Status", sStatus) 'sPreFix & Config.INSTANCE.getStatusPInvoice())
                                setDFValue(oForm, msRowTable, "U_PayMeth", "Cash")
                                setDFValue(oForm, msRowTable, "U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusUnPaid())
                                setDFValue(oForm, msRowTable, "U_CCNum", "")
                                setDFValue(oForm, msRowTable, "U_CCType", "0")
                                setDFValue(oForm, msRowTable, "U_CCStat", "")
                                'End If
                            ElseIf sID.Equals("IDH_DOORD") Then
                                'If getUFValue(oForm, "IDH_DOORD") = "Y" Then
                                If sAction = "D" Then
                                    sStatus = com.idh.bridge.lookups.FixedValues.getDoOrderStatus()
                                Else
                                    sStatus = com.idh.bridge.lookups.FixedValues.getReqOrderStatus()
                                End If

                                setDFValue(oForm, msRowTable, "U_Status", sStatus) 'sPreFix & com.idh.bridge.lookups.FixedValues.getStatusOrder())
                                setDFValue(oForm, msRowTable, "U_PayMeth", "Accounts")
                                setDFValue(oForm, msRowTable, "U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusUnPaid())
                                setDFValue(oForm, msRowTable, "U_CCNum", "")
                                setDFValue(oForm, msRowTable, "U_CCType", "0")
                                setDFValue(oForm, msRowTable, "U_CCStat", "")
                                'End If
                            ElseIf sID.Equals("IDH_JRNL") Then
                                If sAction = "D" Then
                                    sStatus = com.idh.bridge.lookups.FixedValues.getDoJrnlStatus()
                                Else
                                    sStatus = com.idh.bridge.lookups.FixedValues.getReqJrnlStatus()
                                End If

                                setDFValue(oForm, msRowTable, "U_Status", sStatus) 'sPreFix & com.idh.bridge.lookups.FixedValues.getStatusOrder())
                                setDFValue(oForm, msRowTable, "U_PayMeth", "Accounts")
                                setDFValue(oForm, msRowTable, "U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusUnPaid())
                                setDFValue(oForm, msRowTable, "U_CCNum", "")
                                setDFValue(oForm, msRowTable, "U_CCType", "0")
                                setDFValue(oForm, msRowTable, "U_CCStat", "")
                            ElseIf sID.Equals("IDH_FOC") Then
                                'If getUFValue(oForm, "IDH_FOC") = "Y" Then
                                If sAction = "D" Then
                                    sStatus = com.idh.bridge.lookups.FixedValues.getDoFocStatus()
                                Else
                                    sStatus = com.idh.bridge.lookups.FixedValues.getReqFocStatus()
                                End If

                                setDFValue(oForm, msRowTable, "U_Status", sStatus) 'sPreFix & com.idh.bridge.lookups.FixedValues.getStatusFoc())
                                setDFValue(oForm, msRowTable, "U_PayMeth", com.idh.bridge.lookups.FixedValues.getPayMethFoc())
                                setDFValue(oForm, msRowTable, "U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusFree())
                                setDFValue(oForm, msRowTable, "U_CCNum", "")
                                setDFValue(oForm, msRowTable, "U_CCType", "0")
                                setDFValue(oForm, msRowTable, "U_CCStat", "")
                                'End If
                            Else
                                setDFValue(oForm, msRowTable, IDH_DISPROW._Status, FixedValues.getStatusOpen())
                                setDFValue(oForm, msRowTable, IDH_DISPROW._PStat, FixedValues.getStatusOpen())
                                setDFValue(oForm, msRowTable, "U_PayMeth", "")
                                setDFValue(oForm, msRowTable, "U_PayStat", "")
                                setDFValue(oForm, msRowTable, "U_CCNum", "")
                                setDFValue(oForm, msRowTable, "U_CCType", "0")
                                setDFValue(oForm, msRowTable, "U_CCStat", "")
                            End If
                        Else
                            setDFValue(oForm, msRowTable, IDH_DISPROW._Status, FixedValues.getStatusOpen())
                            setDFValue(oForm, msRowTable, IDH_DISPROW._PStat, FixedValues.getStatusOpen())
                            setDFValue(oForm, msRowTable, "U_PayMeth", "")
                            setDFValue(oForm, msRowTable, "U_PayStat", "")
                            setDFValue(oForm, msRowTable, "U_CCNum", "")
                            setDFValue(oForm, msRowTable, "U_CCType", "0")
                            setDFValue(oForm, msRowTable, "U_CCStat", "")
                        End If
                    End If
                Else
                    'If bDoSetStatusValue = True Then
                    '    setDFValue(oForm, msRowTable, IDH_DISPROW._Status, com.idh.bridge.lookups.FixedValues.getStatusOpen())
                    '    setDFValue(oForm, msRowTable, IDH_DISPROW._PStat, com.idh.bridge.lookups.FixedValues.getStatusOpen())
                    '    setDFValue(oForm, msRowTable, "U_PayMeth", "")
                    '    setDFValue(oForm, msRowTable, "U_PayStat", "")
                    '    setDFValue(oForm, msRowTable, "U_CCNum", "")
                    '    setDFValue(oForm, msRowTable, "U_CCType", "0")
                    '    setDFValue(oForm, msRowTable, "U_CCStat", "")
                    'End If
                End If
            End If
        End Sub

        '        Protected Overrides Function doUpdateGridRows(ByVal oForm As SAPbouiCOM.Form) As Boolean
        '            Return MyBase.doUpdateGridRows(oForm)
        '        End Function

        Protected Sub doLoadGridForRow(ByVal oForm As SAPbouiCOM.Form)
            Dim sDORow As String = getDFValue(oForm, msRowTable, "Code")
            Dim oAddGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            oAddGrid.doReloadDataFH(sDORow)

            ''LPVTEMP START
            'Dim oDedGrid As WR1_Grids.idh.controls.grid.Deductions = WR1_Grids.idh.controls.grid.Deductions.getInstance(oForm, "DEDGRID")
            'oDedGrid.doReloadDataFH(sDORow)
            Dim oDedGrid As DBOGrid = getWFValue(oForm, "DEDGRID")
            Dim oData As IDH_DODEDUCT = oDedGrid.DBObject
            oData.getByOrderRow(sDORow)
            oDedGrid.doPostReloadData(True)
            ''LPVTEMP END
        End Sub

        Protected Sub doUpdateGridRowDORCodes(ByVal oForm As SAPbouiCOM.Form)
            Dim sRowCode As String = getDFValue(oForm, msRowTable, "Code")
            Dim oAData As WR1_Data.idh.data.AdditionalExpenses = Nothing
            oAData = getWFValue(oForm, "JobAdditionalHandler")
            If Not oAData Is Nothing Then
                oAData.doSetRowCode(sRowCode)
            End If

            'LPVTEMP START
            'Dim oDData As WR1_Data.idh.data.Deductions = Nothing
            'oDData = getWFValue(oForm, "JobDeductionHandler")
            'If Not oDData Is Nothing Then
            '    oDData.doSetRowCode(sRowCode)
            'End If
            Dim oDedGrid As DBOGrid = getWFValue(oForm, "DEDGRID")
            Dim oData As IDH_DODEDUCT = oDedGrid.DBObject
            oData.doSetRowCode(sRowCode)
            'LPVTEMP END
        End Sub

        Protected Overrides Function doUpdateGridRows(ByVal oForm As SAPbouiCOM.Form) As Boolean
            If MyBase.doUpdateGridRows(oForm) Then
                Dim sJobCode As String = getFormDFValue(oForm, "Code")
                Dim sRowCode As String = getDFValue(oForm, msRowTable, IDH_DISPROW._Code)

                'LPVTEMP START
                Dim oAddGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                oAddGrid.doSetRowCode(sJobCode, sRowCode)
                oAddGrid.doProcessData()

                'Dim oAData As WR1_Data.idh.data.AdditionalExpenses = Nothing
                'oAData = getWFValue(oForm, "JobAdditionalHandler")
                'If Not oAData Is Nothing Then
                'Dim sRowCode As String = getDFValue(oForm, msRowTable, "Code")
                'oAData.doSetRowCode(sRowCode)
                'oAData.doProcessData()
                'End If

                'Dim oDData As WR1_Data.idh.data.Deductions = Nothing
                'oDData = getWFValue(oForm, "JobDeductionHandler")
                'If Not oDData Is Nothing Then
                '    Dim sRowCode As String = getDFValue(oForm, msRowTable, "Code")
                '    oDData.doSetRowCode(sRowCode)
                '    oDData.doProcessData()
                'End If
                Dim oDedGrid As DBOGrid = getWFValue(oForm, "DEDGRID")
                Dim oData As IDH_DODEDUCT = oDedGrid.DBObject
                oData.doSetRowCode(sRowCode)
                oDedGrid.doProcessData()
                'LPVTEMP END
                Return True
            Else
                Return False
            End If
        End Function

        Protected Function doSetGridRowData(ByVal oForm As SAPbouiCOM.Form, ByRef oUpdateGrid As UpdateGrid) As Boolean
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            Dim oDO As IDH_DISPORD = thisDO(oForm)
            ''MA Start 11-02-2015
            'If doCheckAndAddVehicle(oForm) = False Then
            '    Return False
            'End If
            ''MA Start 11-02-2015

            Dim sRowID As String
            'Dim iLastRow As Integer
            Dim sJobNr As String = getDFValue(oForm, "@IDH_DISPORD", "Code")
            Dim dRowTotal As Double = getDFValue(oForm, msRowTable, "U_Total")
            sRowID = getDFValue(oForm, msRowTable, "Code").Trim()

            Dim sCustomer As String = oDO.U_CardCd
            Dim sCustomerName As String = oDO.U_CardNM

            Dim sAddress As String = oDO.U_Address
            Dim sCustRef As String = oDO.U_CustRef

            Dim bFoc As String = getUFValue(oForm, "IDH_FOC")
            Dim dChargeTotal As Double = 0

            If bFoc.Equals("Y") = False Then
                dChargeTotal = getUnCommittedRowTotals(oForm, sRowID, dRowTotal)
            End If
            If Config.INSTANCE.doGetPOLimitsDOWO("DO", sCustomer, sAddress, sCustRef, dChargeTotal) = False Then
                Return False
            End If

            If sRowID.Length = 0 OrElse sRowID.Equals("-1") Then
                sRowID = doCheckAndAssignRowNumber(oForm)
                '                sRowID = com.idh.utils.Conversions.ToString(goParent.goDB.doNextNumber("DISPROW"))
                '                oUpdateGrid.setCurrentLine(oUpdateGrid.getLastRowIndex())
                '                oUpdateGrid.doSetFieldValue("Code", sRowID)
                '                oUpdateGrid.doSetFieldValue("Name", sRowID)
                '                setDFValue(oForm, msRowTable, "Code", sRowID)
            Else
                ''Find the row containing the RowId
                Dim sKey As String
                sKey = oUpdateGrid.doRowKeyLookup(oUpdateGrid.getCurrentDataRowIndex()).Trim()
                If sKey.Equals(sRowID) = False Then
                    Dim iRow As Integer
                    For iRow = 0 To oUpdateGrid.getLastRowIndex()
                        oUpdateGrid.setCurrentDataRowIndex(iRow)
                        sKey = oUpdateGrid.doRowKeyLookup(oUpdateGrid.getCurrentDataRowIndex()).Trim()
                        If sKey.Equals(sRowID) Then
                            Exit For
                        End If
                    Next
                End If
            End If
            ''
            ''SV 20/03/2015 #587
            ''
            'thisDOR(oForm).OrderRow = sRowID
            If doCheckAndAddVehicle(oForm) = False Then
                Return False
            End If
            ''End

            Dim sLoadSheet As String = getDFValue(oForm, msRowTable, "U_LoadSht")
            If Not sLoadSheet Is Nothing AndAlso sLoadSheet.Length > 0 Then
                Dim oLoadHead As User.IDH_MANBALD = New User.IDH_MANBALD()
                If oLoadHead.getByKey(sLoadSheet) Then
                    oLoadHead.U_DOR = sRowID 'getDFValue(oForm, msRowTable, "Code")
                    oLoadHead.doUpdateDataRow()
                End If
                com.idh.bridge.DataHandler.INSTANCE.doReleaseObject(oLoadHead)
            End If

            oUpdateGrid.doSetFieldValue("U_JobNr", sJobNr)
            oUpdateGrid.doSetFieldValue("U_Lorry", getDFValue(oForm, msRowTable, "U_Lorry"))
            oUpdateGrid.doSetFieldValue("U_LorryCd", getDFValue(oForm, msRowTable, "U_LorryCd"))
            oUpdateGrid.doSetFieldValue("U_VehTyp", getDFValue(oForm, msRowTable, "U_VehTyp"))
            oUpdateGrid.doSetFieldValue("U_TRLReg", getDFValue(oForm, msRowTable, "U_TRLReg"))
            oUpdateGrid.doSetFieldValue("U_TRLNM", getDFValue(oForm, msRowTable, "U_TRLNM"))
            oUpdateGrid.doSetFieldValue("U_Driver", getDFValue(oForm, msRowTable, "U_Driver"))
            oUpdateGrid.doSetFieldValue(IDH_DISPROW._JobTp, getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp))
            oUpdateGrid.doSetFieldValue("U_ItemCd", getDFValue(oForm, msRowTable, "U_ItemCd"))
            oUpdateGrid.doSetFieldValue("U_ItemDsc", getDFValue(oForm, msRowTable, "U_ItemDsc"))
            oUpdateGrid.doSetFieldValue("U_WasCd", getDFValue(oForm, msRowTable, "U_WasCd"))
            oUpdateGrid.doSetFieldValue("U_WasDsc", getDFValue(oForm, msRowTable, "U_WasDsc"))
            oUpdateGrid.doSetFieldValue("U_ItmGrp", getDFValue(oForm, msRowTable, "U_ItmGrp"))

            'KA - DIMECA - Alternate Item Description 
            oUpdateGrid.doSetFieldValue("U_AltWasDsc", getDFValue(oForm, msRowTable, "U_AltWasDsc"))

            'Multi Currency Pricing 
            oUpdateGrid.doSetFieldValue(IDH_DISPROW._DispCgCc, getDFValue(oForm, msRowTable, IDH_DISPROW._DispCgCc))
            oUpdateGrid.doSetFieldValue(IDH_DISPROW._CarrCgCc, getDFValue(oForm, msRowTable, IDH_DISPROW._CarrCgCc))
            oUpdateGrid.doSetFieldValue(IDH_DISPROW._PurcCoCc, getDFValue(oForm, msRowTable, IDH_DISPROW._PurcCoCc))
            oUpdateGrid.doSetFieldValue(IDH_DISPROW._DispCoCc, getDFValue(oForm, msRowTable, IDH_DISPROW._DispCoCc))
            oUpdateGrid.doSetFieldValue(IDH_DISPROW._CarrCoCc, getDFValue(oForm, msRowTable, IDH_DISPROW._CarrCoCc))
            oUpdateGrid.doSetFieldValue(IDH_DISPROW._LiscCoCc, getDFValue(oForm, msRowTable, IDH_DISPROW._LiscCoCc))


            oUpdateGrid.doSetFieldValue("U_MANPRC", getDFValue(oForm, msRowTable, "U_MANPRC"))

            oUpdateGrid.doSetFieldValue("U_TCharge", getDFValue(oForm, msRowTable, "U_TCharge"))
            oUpdateGrid.doSetFieldValue("U_TCTotal", getDFValue(oForm, msRowTable, "U_TCTotal"))
            oUpdateGrid.doSetFieldValue("U_Discnt", getDFValue(oForm, msRowTable, "U_Discnt"))
            oUpdateGrid.doSetFieldValue("U_DisAmt", getDFValue(oForm, msRowTable, "U_DisAmt"))
            oUpdateGrid.doSetFieldValue("U_Total", dRowTotal)

            oUpdateGrid.doSetFieldValue("U_User", getDFValue(oForm, msRowTable, "U_User"))
            oUpdateGrid.doSetFieldValue("U_Branch", getDFValue(oForm, msRowTable, "U_Branch"))
            oUpdateGrid.doSetFieldValue("U_TZone", getDFValue(oForm, msRowTable, "U_TZone"))
            oUpdateGrid.doSetFieldValue("U_ContNr", getDFValue(oForm, msRowTable, "U_ContNr"))
            oUpdateGrid.doSetFieldValue("U_SealNr", getDFValue(oForm, msRowTable, "U_SealNr"))
            oUpdateGrid.doSetFieldValue("U_Origin", getDFValue(oForm, msRowTable, "U_Origin"))
            oUpdateGrid.doSetFieldValue("U_CustCs", getDFValue(oForm, msRowTable, "U_CustCs"))
            oUpdateGrid.doSetFieldValue("U_Obligated", getDFValue(oForm, msRowTable, "U_Obligated"))
            oUpdateGrid.doSetFieldValue(IDH_DISPROW._FTOrig, getDFValue(oForm, msRowTable, IDH_DISPROW._FTOrig))

            ''Duplicate column added 
            'oUpdateGrid.doSetFieldValue(IDH_DISPROW._SAddress, getDFValue(oForm, msRowTable, IDH_DISPROW._SAddress))
            'oUpdateGrid.doSetFieldValue(IDH_DISPROW._TrnCode, getFormDFValue(oForm, IDH_DISPORD._TRNCd))

            oUpdateGrid.doSetFieldValue("U_ConNum", getDFValue(oForm, msRowTable, "U_ConNum"))

            oUpdateGrid.doSetFieldValue("U_WastTNN", getDFValue(oForm, msRowTable, "U_WastTNN"))
            'oUpdateGrid.doSetFieldValue("U_HazWCNN", getDFValue(oForm, msRowTable, "U_HazWCNN"))
            oUpdateGrid.doSetFieldValue("U_SiteRef", getDFValue(oForm, msRowTable, "U_SiteRef"))
            oUpdateGrid.doSetFieldValue("U_ExtWeig", getDFValue(oForm, msRowTable, "U_ExtWeig"))
            ''MA Start 07-04-2016 Issue#1111
            oUpdateGrid.doSetFieldValue(IDH_DISPROW._ExpLdWgt, getDFValue(oForm, msRowTable, IDH_DISPROW._ExpLdWgt))
            ''MA End 07-04-2016 Issue#1111

            'Dim sQty As String
            Dim sCstQty As String
            'sCstQty = getDFValue(oForm, msRowTable, "U_CusQty")
            sCstQty = getDFValue(oForm, msRowTable, "U_HlSQty")
            If sCstQty.Length = 0 Then
                sCstQty = "0"
            End If
            oUpdateGrid.doSetFieldValue("U_CusQty", sCstQty)
            oUpdateGrid.doSetFieldValue("U_HlSQty", sCstQty)

            oUpdateGrid.doSetFieldValue("U_CusChr", getDFValue(oForm, msRowTable, "U_CusChr"))

            oUpdateGrid.doSetFieldValue(IDH_DISPROW._CustCd, sCustomer)
            oUpdateGrid.doSetFieldValue(IDH_DISPROW._CustNm, sCustomerName)

            oUpdateGrid.doSetFieldValue("U_Tip", oDO.U_SCardCd)
            oUpdateGrid.doSetFieldValue(IDH_DISPROW._SAddress, getDFValue(oForm, msRowTable, IDH_DISPROW._SAddress))

            oUpdateGrid.doSetFieldValue(IDH_DISPROW._SAddrsLN, getDFValue(oForm, msRowTable, IDH_DISPROW._SAddrsLN))

            oUpdateGrid.doSetFieldValue("U_CarrCd", getDFValue(oForm, "@IDH_DISPORD", "U_CCardCd"))

            oUpdateGrid.doSetFieldValue("U_CustRef", getDFValue(oForm, "@IDH_DISPORD", "U_CustRef"))
            oUpdateGrid.doSetFieldValue("U_SupRef", getDFValue(oForm, "@IDH_DISPORD", "U_SupRef"))

            'Was commented out not sure why
            oUpdateGrid.doSetFieldValue(IDH_DISPROW._ProCd, getDFValue(oForm, msRowTable, "U_ProCd"))
            oUpdateGrid.doSetFieldValue(IDH_DISPROW._ProNm, getDFValue(oForm, msRowTable, "U_ProNm"))

            Dim dW1 As Double = 0
            Dim dW2 As Double = 0
            Dim dReadWeight As Double = 0
            Dim sWTime As String = ""
            Dim dWDate As String = ""

            dReadWeight = getDFValue(oForm, msRowTable, IDH_DISPROW._RdWgt)
            Try
                dW1 = getDFValue(oForm, msRowTable, "U_Wei1")

                sWTime = oUpdateGrid.doGetFieldValue("U_ASTime")
                dWDate = oUpdateGrid.doGetFieldValue("U_ASDate")
                'If dW1 > 0 AndAlso (dWDate Is Nothing) Then
                If (dW1 > 0 OrElse (dW2 = 0 AndAlso dW1 = 0 AndAlso dReadWeight > 0)) AndAlso (dWDate Is Nothing) Then


                    Dim sTime As String = com.idh.utils.Dates.doTimeToNumStr()
                    oUpdateGrid.doSetFieldValue("U_ASDate", Date.Now())
                    oUpdateGrid.doSetFieldValue("U_ASTime", sTime)
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception(W1) : " & ex.ToString, "Error setting the Grid row data - " & dWDate & " - " & sWTime)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGRD", {dWDate, sWTime})
            End Try

            Try
                dW2 = getDFValue(oForm, msRowTable, IDH_DISPROW._Wei2)
                oDOR.setEndDate()

                sWTime = oUpdateGrid.doGetFieldValue("U_AETime")
                dWDate = oUpdateGrid.doGetFieldValue("U_AEDate")

                oUpdateGrid.doSetFieldValue("U_AEDate", oDOR.U_AEDate)
                oUpdateGrid.doSetFieldValue("U_AETime", oDOR.U_AETime)

                ''If dW2 > 0 AndAlso dW1 > 0 AndAlso (dWDate Is Nothing) Then
                'If ((dW2 > 0 AndAlso dW1 > 0) OrElse (dW2 = 0 AndAlso dW1 = 0 AndAlso dReadWeight > 0)) AndAlso (dWDate Is Nothing) Then
                '    Dim sTime As String = com.idh.utils.Dates.doTimeToNumStr()
                '    oUpdateGrid.doSetFieldValue("U_AEDate", Date.Now())
                '    oUpdateGrid.doSetFieldValue("U_AETime", sTime)

                'End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception(W2) : " & ex.ToString, "Error setting the Grid row data - " & dWDate & " - " & sWTime)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGRD", {dWDate, sWTime})
            End Try

            oUpdateGrid.doSetFieldValue("U_Wei1", dW1)

            oUpdateGrid.doSetFieldValue("U_Wei2", dW2)

            oUpdateGrid.doSetFieldValue("U_WDt1", com.idh.utils.Dates.doStrToDate(getDFValue(oForm, msRowTable, "U_WDt1")))
            oUpdateGrid.doSetFieldValue("U_WDt2", com.idh.utils.Dates.doStrToDate(getDFValue(oForm, msRowTable, "U_WDt2")))
            oUpdateGrid.doSetFieldValue("U_Ser1", getDFValue(oForm, msRowTable, "U_Ser1"))
            oUpdateGrid.doSetFieldValue("U_Ser2", getDFValue(oForm, msRowTable, "U_Ser2"))
            oUpdateGrid.doSetFieldValue("U_AddEx", getDFValue(oForm, msRowTable, "U_AddEx"))

            oUpdateGrid.doSetFieldValue("U_WgtDed", getDFValue(oForm, msRowTable, "U_WgtDed"))
            oUpdateGrid.doSetFieldValue("U_AdjWgt", getDFValue(oForm, msRowTable, "U_AdjWgt"))
            oUpdateGrid.doSetFieldValue("U_ValDed", getDFValue(oForm, msRowTable, "U_ValDed"))
            oUpdateGrid.doSetFieldValue("U_BookIn", getDFValue(oForm, msRowTable, "U_BookIn"))
            oUpdateGrid.doSetFieldValue("U_IsTrl", getDFValue(oForm, msRowTable, "U_IsTrl"))

            oUpdateGrid.doSetFieldValue("U_TAddChrg", getDFValue(oForm, msRowTable, "U_TAddChrg"))
            oUpdateGrid.doSetFieldValue("U_TAddCost", getDFValue(oForm, msRowTable, "U_TAddCost"))

            ''## MA Start 15-04-2014
            oUpdateGrid.doSetFieldValue("U_TAddChVat", getDFValue(oForm, msRowTable, "U_TAddChVat"))
            oUpdateGrid.doSetFieldValue("U_TAddCsVat", getDFValue(oForm, msRowTable, "U_TAddCsVat"))

            oUpdateGrid.doSetFieldValue("U_AddCharge", getDFValue(oForm, msRowTable, "U_AddCharge"))
            oUpdateGrid.doSetFieldValue("U_AddCost", getDFValue(oForm, msRowTable, "U_AddCost"))

            ''## MA End 15-04-2014

            Dim sUOM As String = getDFValue(oForm, msRowTable, "U_UOM")
            oUpdateGrid.doSetFieldValue("U_WASLIC", getUFValue(oForm, "IDH_LICREG"))

            oUpdateGrid.doSetFieldValue("U_WROrd", getDFValue(oForm, msRowTable, "U_WROrd"))
            oUpdateGrid.doSetFieldValue("U_WRRow", getDFValue(oForm, msRowTable, "U_WRRow"))
            oUpdateGrid.doSetFieldValue("U_LoadSht", getDFValue(oForm, msRowTable, "U_LoadSht"))

            oUpdateGrid.doSetFieldValue(IDH_DISPROW._TrnCode, oDOR.U_TrnCode)

            Dim dProdWei As Double = 0
            Dim dCustWei As Double = 0
            If (oDOR.MarketingMode = Config.MarketingMode.PO) Then
                'If getWFValue(oForm, "FormMode") = "PO" Then
                dProdWei = getDFValue(oForm, msRowTable, "U_ProWgt")
            Else
                dCustWei = getDFValue(oForm, msRowTable, "U_CstWgt")
            End If
            oUpdateGrid.doSetFieldValue("U_UseWgt", getDFValue(oForm, msRowTable, "U_UseWgt"))
            oUpdateGrid.doSetFieldValue("U_ProWgt", dProdWei)
            oUpdateGrid.doSetFieldValue("U_CstWgt", dCustWei)
            oUpdateGrid.doSetFieldValue("U_PAUOMQt", getDFValue(oForm, msRowTable, "U_AUOMQt"))
            oUpdateGrid.doSetFieldValue("U_TRdWgt", getDFValue(oForm, msRowTable, "U_TRdWgt"))
            oUpdateGrid.doSetFieldValue("U_PRdWgt", getDFValue(oForm, msRowTable, "U_PRdWgt"))
            oUpdateGrid.doSetFieldValue("U_AUOMQt", getDFValue(oForm, msRowTable, "U_AUOMQt"))
            oUpdateGrid.doSetFieldValue("U_RdWgt", getDFValue(oForm, msRowTable, "U_RdWgt"))

            oUpdateGrid.doSetFieldValue("U_ProUOM", getDFValue(oForm, msRowTable, "U_ProUOM"))
            oUpdateGrid.doSetFieldValue("U_UOM", sUOM)
            oUpdateGrid.doSetFieldValue("U_AUOM", getDFValue(oForm, msRowTable, "U_AUOM"))

            oUpdateGrid.doSetFieldValue("U_PCost", getDFValue(oForm, msRowTable, "U_PCost"))
            oUpdateGrid.doSetFieldValue("U_PCTotal", getDFValue(oForm, msRowTable, "U_PCTotal"))
            oUpdateGrid.doSetFieldValue("U_JCost", getDFValue(oForm, msRowTable, "U_JCost"))

            oUpdateGrid.doSetFieldValue("U_TipWgt", getDFValue(oForm, msRowTable, "U_TipWgt"))
            oUpdateGrid.doSetFieldValue("U_PUOM", getDFValue(oForm, msRowTable, "U_PUOM"))
            oUpdateGrid.doSetFieldValue("U_TipCost", getDFValue(oForm, msRowTable, "U_TipCost"))
            oUpdateGrid.doSetFieldValue("U_TipTot", getDFValue(oForm, msRowTable, "U_TipTot"))

            oUpdateGrid.doSetFieldValue("U_OrdWgt", getDFValue(oForm, msRowTable, "U_OrdWgt"))
            Dim sOrdQty As String
            sOrdQty = getDFValue(oForm, msRowTable, IDH_DISPROW._CarWgt)
            If sOrdQty.Length = 0 Then
                oUpdateGrid.doSetFieldValue(IDH_DISPROW._CarWgt, getDFValue(oForm, msRowTable, IDH_DISPROW._OrdWgt))
            Else
                oUpdateGrid.doSetFieldValue(IDH_DISPROW._CarWgt, sOrdQty)
            End If

            oUpdateGrid.doSetFieldValue("U_OrdCost", getDFValue(oForm, msRowTable, "U_OrdCost"))
            oUpdateGrid.doSetFieldValue("U_OrdTot", getDFValue(oForm, msRowTable, "U_OrdTot"))

            'Try
            '    sWTime = oUpdateGrid.doGetFieldValue("U_AETime")
            '    dWDate = oUpdateGrid.doGetFieldValue("U_AEDate")

            '    'If (dCustWei <> 0 OrElse dProdWei <> 0) AndAlso (dWDate Is Nothing) Then
            '    If (dCustWei > 0 OrElse dProdWei > 0) AndAlso (dWDate Is Nothing) Then
            '        'Dim sTime As String = goParent.doStrToTimeStr(gcom.idh.utils.dates.doTimeToNumStr())
            '        Dim sTime As String = com.idh.utils.dates.doTimeToNumStr()
            '        oUpdateGrid.doSetFieldValue("U_AEDate", Date.Now())
            '        oUpdateGrid.doSetFieldValue("U_AETime", sTime)
            '    End If
            'Catch ex As Exception
            '    com.idh.bridge.DataHandler.INSTANCE.doError("Exception(W2) : " & ex.ToString, "Error setting the Grid row data - " & dWDate & " - " & sWTime)
            'End Try

            Dim sPOStatus As String = getDFValue(oForm, msRowTable, "U_PStat")
            Dim sSOStatus As String = getDFValue(oForm, msRowTable, "U_Status")

            If sPOStatus Is Nothing OrElse sPOStatus.Length = 0 Then
                sPOStatus = com.idh.bridge.lookups.FixedValues.getStatusOpen()
            End If
            If sSOStatus Is Nothing OrElse sSOStatus.Length = 0 Then
                sSOStatus = com.idh.bridge.lookups.FixedValues.getStatusOpen()
            End If

            oUpdateGrid.doSetFieldValue("U_PayMeth", getDFValue(oForm, msRowTable, "U_PayMeth"))
            oUpdateGrid.doSetFieldValue("U_PayStat", getDFValue(oForm, msRowTable, "U_PayStat"))
            oUpdateGrid.doSetFieldValue("U_CCNum", getDFValue(oForm, msRowTable, "U_CCNum"))
            oUpdateGrid.doSetFieldValue("U_CCType", getDFValue(oForm, msRowTable, "U_CCType"))
            oUpdateGrid.doSetFieldValue("U_CCStat", getDFValue(oForm, msRowTable, "U_CCStat"))
            oUpdateGrid.doSetFieldValue("U_Status", sSOStatus)
            oUpdateGrid.doSetFieldValue("U_PStat", sPOStatus)

            Dim sRowSta As String = oUpdateGrid.doGetFieldValue("U_RowSta") 'getDFValue(oForm, msRowTable, "U_RowSta")
            'oUpdateGrid.doSetFieldValue("U_RowSta", sRowSta)

            oUpdateGrid.doSetFieldValue("U_TaxAmt", getDFValue(oForm, msRowTable, "U_TaxAmt"))
            oUpdateGrid.doSetFieldValue("U_TChrgVtRt", getDFValue(oForm, msRowTable, "U_TChrgVtRt"))
            oUpdateGrid.doSetFieldValue("U_HChrgVtRt", getDFValue(oForm, msRowTable, "U_HChrgVtRt"))
            oUpdateGrid.doSetFieldValue("U_TCostVtRt", getDFValue(oForm, msRowTable, "U_TCostVtRt"))
            oUpdateGrid.doSetFieldValue("U_PCostVtRt", getDFValue(oForm, msRowTable, "U_PCostVtRt"))
            oUpdateGrid.doSetFieldValue("U_HCostVtRt", getDFValue(oForm, msRowTable, "U_HCostVtRt"))
            oUpdateGrid.doSetFieldValue("U_TChrgVtGrp", getDFValue(oForm, msRowTable, "U_TChrgVtGrp"))
            oUpdateGrid.doSetFieldValue("U_HChrgVtGrp", getDFValue(oForm, msRowTable, "U_HChrgVtGrp"))
            oUpdateGrid.doSetFieldValue("U_TCostVtGrp", getDFValue(oForm, msRowTable, "U_TCostVtGrp"))
            oUpdateGrid.doSetFieldValue("U_PCostVtGrp", getDFValue(oForm, msRowTable, "U_PCostVtGrp"))
            oUpdateGrid.doSetFieldValue("U_HCostVtGrp", getDFValue(oForm, msRowTable, "U_HCostVtGrp"))
            oUpdateGrid.doSetFieldValue("U_VtCostAmt", getDFValue(oForm, msRowTable, "U_VtCostAmt"))

            Dim sBDate As String = getDFValue(oForm, msRowTable, "U_BDate")
            Dim sBTime As String = getDFValue(oForm, msRowTable, "U_BTime")
            oUpdateGrid.doSetFieldValue("U_BDate", com.idh.utils.Dates.doStrToDate(sBDate))
            oUpdateGrid.doSetFieldValue("U_BTime", sBTime)

            Dim sRDate As String = getDFValue(oForm, msRowTable, "U_RDate")
            Dim sRTime As String = getDFValue(oForm, msRowTable, "U_RTime")
            oUpdateGrid.doSetFieldValue("U_RDate", com.idh.utils.Dates.doStrToDate(sRDate))
            oUpdateGrid.doSetFieldValue("U_RTime", sBTime)

            ''## MA Start 16-10-2014
            oUpdateGrid.doSetFieldValue("U_BPWgt", getDFValue(oForm, msRowTable, "U_BPWgt"))
            oUpdateGrid.doSetFieldValue("U_UseBPWgt", getDFValue(oForm, msRowTable, "U_UseBPWgt"))
            ''## MA End 16-10-2014
            '## MA Start 11-09-2014 Issue#406
            'Dim sJobToBeDoneDt As String = getDFValue(oForm, msRowTable, "U_JbToBeDoneDt")
            'Dim sJobTobeDoneTm As String = getDFValue(oForm, msRowTable, "U_JbToBeDoneTm")
            'oUpdateGrid.doSetFieldValue("U_JbToBeDoneDt", com.idh.utils.dates.doStrToDate(sJobToBeDoneDt))
            'oUpdateGrid.doSetFieldValue("U_JbToBeDoneTm", sJobTobeDoneTm)
            '## MA End 11-09-2014 Issue#406

            oUpdateGrid.doSetFieldValue("U_LnkPBI", getDFValue(oForm, msRowTable, "U_LnkPBI"))

            oUpdateGrid.doSetFieldValue("U_Sched", getDFValue(oForm, msRowTable, "U_Sched"))
            oUpdateGrid.doSetFieldValue("U_USched", getDFValue(oForm, msRowTable, "U_USched"))

            'WRRow2 (was missing to save/update prior 1021)
            oUpdateGrid.doSetFieldValue(IDH_DISPROW._WRRow2, getDFValue(oForm, msRowTable, IDH_DISPROW._WRRow2))


            Dim sBranch As String = getDFValue(oForm, msRowTable, IDH_DISPROW._Branch)
            oUpdateGrid.doSetFieldValue(IDH_DISPROW._Branch, sBranch)

            'Start from here - Sample fields on DO
            oUpdateGrid.doSetFieldValue(IDH_DISPROW._Sample, getDFValue(oForm, msRowTable, IDH_DISPROW._Sample))
            oUpdateGrid.doSetFieldValue(IDH_DISPROW._SampleRef, getDFValue(oForm, msRowTable, IDH_DISPROW._SampleRef))
            oUpdateGrid.doSetFieldValue(IDH_DISPROW._SampStatus, getDFValue(oForm, msRowTable, IDH_DISPROW._SampStatus))

            ''LPVTEMP START
            'Dim oAddGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            ''oAddGrid.doSetRowCode(sRowID)
            'oAddGrid.doProcessData()

            ' ''LPVTEMP START
            ''Dim oDedGrid As WR1_Grids.idh.controls.grid.Deductions = WR1_Grids.idh.controls.grid.Deductions.getInstance(oForm, "DEDGRID")
            ''oDedGrid.doProcessData()
            'Dim oDedGrid As DBOGrid = getWFValue(oForm, "DEDGRID")
            'oDedGrid.doProcessData()
            ' ''LPVTEMP END
            ''LPVTEMP END
            Return True
        End Function

        Public Function doGetLinkedWORs(ByVal oForm As SAPbouiCOM.Form) As String
            Dim sList As String = ""
            Dim sCurrWOR As String = getDFValue(oForm, msRowTable, "U_WRRow", True)

            ''Find the row containing the RowId
            Dim sLinkedWOR As String
            Dim iRow As Integer
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            For iRow = 0 To oGridN.getLastRowIndex()
                oGridN.setCurrentDataRowIndex(iRow)
                sLinkedWOR = oGridN.doGetFieldValue("U_WRRow").Trim()
                If Not sLinkedWOR Is Nothing AndAlso sLinkedWOR.Length > 0 AndAlso sLinkedWOR.Equals(sCurrWOR) = False Then
                    If sList.Length > 0 Then
                        sList = sList & ",'" & sLinkedWOR & "'"
                    Else
                        sList = "'" & sLinkedWOR & "'"
                    End If
                End If
            Next
            Return sList
        End Function

        Public Function doCheckAndAddVehicle(ByVal oForm As SAPbouiCOM.Form) As Boolean
            ''MA Start 10-02-2015
            doCheckAndAddVehicle = True
            'If Config.ParameterAsBool("VALTRWGT", False) Then
            '    If getUFValue(oForm, "IDH_TRWTE1") IsNot Nothing AndAlso getUFValue(oForm, "IDH_TRWTE1").ToString <> "" AndAlso com.idh.utils.Dates.doStrToDate(getUFValue(oForm, "IDH_TRWTE1")).AddMonths(6) < Today Then
            '        com.idh.bridge.DataHandler.INSTANCE.doError("Vahicle Tare weight has been expired. Please update the tare weight.")
            '        Return False
            '    End If
            '    If getUFValue(oForm, "IDH_TRWTE2") IsNot Nothing AndAlso getUFValue(oForm, "IDH_TRWTE2").ToString <> "" AndAlso com.idh.utils.Dates.doStrToDate(getUFValue(oForm, "IDH_TRWTE2")).AddMonths(6) < Today Then
            '        com.idh.bridge.DataHandler.INSTANCE.doError("Vahicle 2nd Tare weight has been expired. Please update the 2nd tare weight.")
            '        Return False
            '    End If
            'End If
            ''MA End 10-02-2015
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            oDOR.TareWeight = Conversions.ToDouble(getUFValue(oForm, "IDH_TARWEI"))
            oDOR.TrailerTareWeight = Conversions.ToDouble(getUFValue(oForm, "IDH_TRLTar"))
            ''MA Start 06-02-2015 Issue#554
            'oDOR.TareWeightDate = getUFValue(oForm, "IDH_TRWTE1")
            If getUFValue(oForm, "IDH_TRWTE1") IsNot Nothing AndAlso getUFValue(oForm, "IDH_TRWTE1").ToString <> "" Then
                oDOR.TareWeightDate = com.idh.utils.Dates.doStrToDate(getUFValue(oForm, "IDH_TRWTE1"))
            End If
            If getUFValue(oForm, "IDH_TRWTE2") IsNot Nothing AndAlso getUFValue(oForm, "IDH_TRWTE2").ToString <> "" Then
                oDOR.SecondTareWeightDate = com.idh.utils.Dates.doStrToDate(getUFValue(oForm, "IDH_TRWTE2"))
            End If
            If oDOR.doCheckAndAddVehicle() = False Then
                Return False
            End If
            Return True
            ''MA End 06-02-2015 Issue#554

            ''        	Dim sWONR As String
            ''        	Dim sWORow As String
            ''        	sWONR = getDFValue(oForm, msRowTable, "U_WROrd", True)
            ''            sWORow = getDFValue(oForm, msRowTable, "U_WRRow", True)
            ''			If sWONR.Length > 0 AndAlso sWORow.Length > 0 Then
            ''				Exit Sub
            ''			End If

            'Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            'Dim sQry As String

            'Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_VEHMAS")
            'Try
            '    Dim sCustCd As String = getDFValue(oForm, "@IDH_DISPORD", "U_CardCd")
            '    Dim sCarrCd As String = getDFValue(oForm, "@IDH_DISPORD", "U_CCardCd")
            '    Dim sReg As String = getDFValue(oForm, msRowTable, "U_Lorry")
            '    Dim sVehCode As String = getDFValue(oForm, msRowTable, "U_LorryCd")
            '    oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            '    If Not (sVehCode Is Nothing) AndAlso sVehCode.Length() > 0 Then
            '        sQry = "Select Code FROM [@IDH_VEHMAS] WHERE Code = '" & sVehCode & "' "
            '        If Config.INSTANCE.getParameterAsBool("DOVMUC", True) Then
            '            sQry = sQry & " And U_CCrdCd = '" & sCustCd & "' "
            '        End If
            '    Else
            '        sQry = "SELECT Code FROM [@IDH_VEHMAS] WHERE U_VehReg = '" & goParent.goDB.doFixForSQL(sReg) & "' " & _
            '                  " And U_CCCrdCd = '" & sCarrCd & "' " & _
            '                  " And (U_WR1ORD Is NULL Or U_WR1ORD='' OR U_WR1ORD LIKE 'DO%')"

            '        If Config.INSTANCE.getParameterAsBool("DOVMUC", True) Then
            '            sQry = sQry & " And U_CCrdCd = '" & sCustCd & "' "
            '        End If
            '    End If
            '    oRecordSet.DoQuery(sQry)
            '    If oRecordSet.RecordCount = 0 Then

            '        Dim sDoASave As String = Config.Parameter("MASVCR")
            '        If sDoASave.ToUpper().Equals("TRUE") Then
            '            Dim sCode As String
            '            sCode = DataHandler.doGenerateCode(IDH_VEHMAS.AUTONUMPREFIX, "VEHMAST")

            '            oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
            '            oAuditObj.setName(sCode)

            '            setDFValue(oForm, msRowTable, "U_LorryCd", sCode)

            '            oAuditObj.setFieldValue("U_VehReg", sReg)
            '            oAuditObj.setFieldValue("U_VehDesc", getDFValue(oForm, msRowTable, "U_VehTyp"))
            '            oAuditObj.setFieldValue("U_Driver", getDFValue(oForm, msRowTable, "U_Driver"))
            '            oAuditObj.setFieldValue("U_TRLReg", getDFValue(oForm, msRowTable, "U_TRLReg"))
            '            oAuditObj.setFieldValue("U_TRLNM", getDFValue(oForm, msRowTable, "U_TRLNM"))
            '            oAuditObj.setFieldValue("U_ItmGrp", Config.Parameter("IGR-LOR"))
            '            oAuditObj.setFieldValue("U_VehType", getWFValue(oForm, "VehicleType"))
            '            oAuditObj.setFieldValue("U_WR1ORD", "DO")
            '            oAuditObj.setFieldValue("U_WFStat", "ReqAdd")

            '        End If
            '    Else

            '        Dim sDoSave As String = Config.Parameter("MASVUP")
            '        If sDoSave.ToUpper().Equals("TRUE") Then
            '            Dim sCode As String
            '            sCode = oRecordSet.Fields.Item(0).Value
            '            IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)

            '            oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE)

            '            oAuditObj.setFieldValue("U_WFStat", "ReqUpdate")
            '        End If
            '    End If

            '    If oAuditObj.getAction() = IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD OrElse _
            '        oAuditObj.getAction() = IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE Then

            '        ''CUSTOMER
            '        '                    oAuditObj.setFieldValue("U_CCrdCd", getDFValue(oForm, "@IDH_DISPORD", "U_CardCd"))
            '        oAuditObj.setFieldValue("U_CCrdCd", sCustCd)
            '        oAuditObj.setFieldValue("U_CName", getDFValue(oForm, "@IDH_DISPORD", "U_CardNM"))
            '        oAuditObj.setFieldValue("U_CAddress", getDFValue(oForm, "@IDH_DISPORD", "U_Address"))


            '        ''INCOMMING DISPOSAL
            '        'If getUFValue(oForm, "IDH_JOBTTP").Equals(Config.INSTANCE.doGetJopTypeIncomming()) Then
            '        '    ''CARRIER
            '        '    '---> WC.Cus - WASTE CARRIER (Cus)
            '        '    '---> WC.Sup - WASTE CARRIER (Sup)
            '        '                    oAuditObj.setFieldValue("U_CCCrdCd", getDFValue(oForm, "@IDH_DISPORD", "U_CCardCd"))
            '        oAuditObj.setFieldValue("U_CCCrdCd", sCarrCd)
            '        oAuditObj.setFieldValue("U_CCName", getDFValue(oForm, "@IDH_DISPORD", "U_CCardNM"))

            '        'Else
            '        '    '---> WC.Cus - WASTE CARRIER (Cus)
            '        '    '---> WC.Sup - WASTE CARRIER (Sup)
            '        '    utTable.UserFields().Fields().Item("U_CCCrdCd").Value = getDFValue(oForm, "@IDH_DISPORD", "U_SCardCd")
            '        '    utTable.UserFields().Fields().Item("U_CCName").Value = getDFValue(oForm, "@IDH_DISPORD", "U_SCardNm")
            '        'End If

            '        'Disposal Site
            '        oAuditObj.setFieldValue("U_SCrdCd", getDFValue(oForm, "@IDH_DISPORD", "U_SCardCd"))
            '        oAuditObj.setFieldValue("U_SName", getDFValue(oForm, "@IDH_DISPORD", "U_SCardNM"))
            '        oAuditObj.setFieldValue(IDH_VEHMAS._SAddress, getDFValue(oForm, "@IDH_DISPORD", IDH_DISPORD._SAddress))

            '        oAuditObj.setFieldValue("U_Origin", getDFValue(oForm, "@IDH_DISPORD", "U_Origin"))

            '        oAuditObj.setFieldValue("U_ItemCd", getDFValue(oForm, msRowTable, "U_ItemCd"))
            '        oAuditObj.setFieldValue("U_ItemDsc", getDFValue(oForm, msRowTable, "U_ItemDsc"))
            '        oAuditObj.setFieldValue("U_WasCd", getDFValue(oForm, msRowTable, "U_WasCd"))
            '        oAuditObj.setFieldValue("U_WasDsc", getDFValue(oForm, msRowTable, "U_WasDsc"))

            '        '*** The tare weights
            '        oAuditObj.setFieldValue("U_Tarre", getUFValue(oForm, "IDH_TARWEI"))
            '        oAuditObj.setFieldValue("U_TRLTar", getUFValue(oForm, "IDH_TRLTar"))

            '        'ONLY UPDATE IF THE 2nd WEIGHT CAPTURED
            '        Dim dWei2 As Double
            '        dWei2 = ToDouble(getDFValue(oForm, msRowTable, "U_Wei2"))
            '        If dWei2 > 0 Then
            '            Dim sMarkAct As String
            '            If getUFValue(oForm, "IDH_FOC").Equals("Y") Then
            '                sMarkAct = "DOFOC"
            '            ElseIf getUFValue(oForm, "IDH_DOORD").Equals("Y") Then
            '                sMarkAct = "DOORD"
            '            ElseIf getUFValue(oForm, "IDH_DOARI").Equals("Y") Then
            '                sMarkAct = "DOARI"
            '            ElseIf getUFValue(oForm, "IDH_AINV").Equals("Y") Then
            '                sMarkAct = "DOAINV"
            '            ElseIf getUFValue(oForm, "IDH_DOARIP").Equals("Y") Then
            '                sMarkAct = "DOARIP"
            '            Else
            '                sMarkAct = ""
            '            End If
            '            oAuditObj.setFieldValue("U_MarkAc", sMarkAct)
            '        End If

            '        Dim iResult As Integer
            '        iResult = oAuditObj.Commit()

            '        If iResult <> 0 Then
            '            Dim sResult As String = Nothing
            '            goParent.goDICompany.GetLastError(iResult, sResult)
            '            com.idh.bridge.DataHandler.INSTANCE.doError("Error Adding/Update Vehicle: " + sResult)
            '        End If
            '    End If
            'Catch ex As Exception
            '    com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error checking and adding the vehicle.")
            'Finally
            '    IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
            '    IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj)
            'End Try
        End Function

        'Handle the Validation events
        Private Function doValidationEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            ''## Updated By LPV Start 2014-08-13
            Dim oDO As IDH_DISPORD = thisDO(oForm)
            ''## Updated By LPV Start 2014-08-13
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

            Dim sItemId As String = pVal.ItemUID

            Dim bIsInnerEvent As Boolean = pVal.InnerEvent
            If pVal.BeforeAction = False Then
                If pVal.ItemChanged Then
                    If wasSetWithCode(oForm, sItemId) = False Then
                        If bIsInnerEvent = False Then
                            If sItemId = "IDH_CUST" OrElse sItemId = "IDH_CUSTNM" Then
                                'removeAutoAddBPAddCharges(oForm, True)
                                'getBPAdditionalCharges(oForm)
                            ElseIf sItemId = "IDH_PRONM" Then
                                Dim sName As String = getItemValue(oForm, "IDH_PRONM")
                                If sName.IndexOf("*") > -1 Then
                                    setItemValue(oForm, "IDH_PROCD", "")
                                End If
                                doChooseProducer(oForm, True, sItemId)
                            ElseIf sItemId = "IDH_ONCS" Then
                                Dim sCs As String = getItemValue(oForm, "IDH_ONCS")
                                If sCs.IndexOf("*") > -1 Then
                                    setItemValue(oForm, "IDH_ONCS", "")
                                    doChooseComplianceScheme(oForm, True)
                                End If
                                'ElseIf sItemId = "IDH_WASMAT" Then
                                '    Dim sDesc As String = getItemValue(oForm, "IDH_WASMAT")
                                '    If sDesc.StartsWith("*") OrElse sDesc.EndsWith("*") Then
                                '        setSharedData(oForm, "TP", "WAST")
                                '        setSharedData(oForm, "IDH_GRPCOD", Config.INSTANCE.doWasteMaterialGroup())
                                '        setSharedData(oForm, "IDH_CRDCD", getDFValue(oForm, "@IDH_DISPORD", "U_CardCd"))
                                '        setSharedData(oForm, "IDH_ITMNAM", sDesc)
                                '        setSharedData(oForm, "IDH_INVENT", "") '"N")
                                '        setSharedData(oForm, "IDH_JOBTP", getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp))


                                '        If getSharedData(oForm, "IDH_ITMSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_ITMSRCHOPEND") = "" Then
                                '            goParent.doOpenModalForm("IDHWISRC", oForm)
                                '        End If
                                '    End If
                                ''MA Start 19-11-2014
                                'ElseIf sItemId = "IDH_DESC" Then
                                '    Dim sDesc As String = getDFValue(oForm, msRowTable, "U_ItemDsc")
                                '    If sDesc.StartsWith("*") OrElse sDesc.EndsWith("*") Then
                                '        setSharedData(oForm, "TRG", "PROD")
                                '        setSharedData(oForm, "IDH_ITMCOD", "")
                                '        setSharedData(oForm, "IDH_GRPCOD", Config.Parameter("MDDDIG"))
                                '        setSharedData(oForm, "IDH_ITMNAM", sDesc)
                                '        goParent.doOpenModalForm("IDHISRC", oForm)
                                '    End If
                                ''MA End 19-11-2014
                            ElseIf sItemId = "IDH_VEHREG" Then
                                Dim sAct As String = getWFValue(oForm, "LastVehAct")
                                If sAct = "DOSEARCH" Then
                                    doCallLorrySearchEB(oForm)

                                    '    Dim sReg As String = getDFValue(oForm, msRowTable, "U_Lorry", True)
                                    '    Dim bDoSetReg As Boolean = False
                                    '    If sReg.StartsWith("!") Then
                                    '        setSharedData(oForm, "SHOWONSITE", "TRUE")
                                    '        sReg = sReg.Substring(1)
                                    '        bDoSetReg = True
                                    '    Else
                                    '        setSharedData(oForm, "SHOWACTIVITY", "FALSE")
                                    '        setSharedData(oForm, "SHOWONSITE", "FALSE")
                                    '    End If
                                    '    Dim sRegChanged As String = com.idh.bridge.utils.General.doFixReg(sReg)
                                    '    If sRegChanged.Equals(sReg) = False OrElse bDoSetReg = True Then
                                    '        setDFValue(oForm, msRowTable, "U_Lorry", sRegChanged, True)
                                    '    End If
                                    '    If Config.INSTANCE.getParameterAsBool("DOVMSUC", True) Then
                                    '        Dim sCustomer As String
                                    '        sCustomer = getDFValue(oForm, "@IDH_DISPORD", "U_CardCd", True)
                                    '        setSharedData(oForm, "IDH_VEHCUS", sCustomer)
                                    '    Else
                                    '        setSharedData(oForm, "IDH_VEHCUS", "")
                                    '    End If

                                    '    Dim sJobTp As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, True)
                                    '    setSharedData(oForm, "JOBTYPE", sJobTp)

                                    '    getDFValue(oForm, msRowTable, "U_Lorry", True)
                                    '    setSharedData(oForm, "IDH_VEHREG", sRegChanged)
                                    '    setSharedData(oForm, "SHOWCREATE", "TRUE")
                                    '    'setSharedData(oForm, "IDH_WR1OT", "^DO")
                                    '    setSharedData(oForm, "IDH_WR1OT", "")
                                    '    setSharedData(oForm, "SILENT", "SHOWMULTI")
                                    '    goParent.doOpenModalForm("IDHLOSCH", oForm)
                                End If
                            End If
                        End If

                        If sItemId = "IDH_WEI1" Then
                            oDOR.U_WDt1 = DateTime.Now
                            oDOR.U_Ser1 = "NONE"
                            oDOR.doMarkUserUpdate(IDH_DISPROW._Wei1)
                            doSetCheckBoxes(oForm)
                        ElseIf sItemId = "IDH_WEI2" Then
                            oDOR.U_WDt2 = DateTime.Now
                            oDOR.U_Ser2 = "NONE"
                            oDOR.doMarkUserUpdate(IDH_DISPROW._Wei2)
                            'Validation rule to trigger adding new rows 
                            If oDOR.U_Wei1 > 0 AndAlso oDOR.U_WRRow = "" Then
                                'Update BP Additional Charge rows when 2nd Weight is changed 
                                doUpdateAutoAddBPAddCharges(oForm, Date.Now())
                            End If
                            doSetCheckBoxes(oForm)
                        ElseIf sItemId = "IDH_ADJWGT" Then
                            oDOR.doMarkUserUpdate(IDH_DISPROW._AdjWgt)
                        ElseIf sItemId = "IDH_BPWGT" Then
                            oDOR.doMarkUserUpdate(IDH_DISPROW._BPWgt)
                        ElseIf sItemId = "IDH_WGTDED" Then
                            oDOR.doMarkUserUpdate(IDH_DISPROW._WgtDed)
                        ElseIf sItemId = "IDH_TARWEI" Then
                            oDOR.doMarkUserUpdate(IDH_JOBSHD._TipWgt)
                        ElseIf sItemId = "IDH_TRLTar" Then
                            Dim dQty As Double = getUFValue(oForm, "IDH_TRLTar")
                            If dQty > 0 Then
                                Dim sUOM As String = oDOR.ActiveUOM
                                Config.INSTANCE.doCheckMaxWeight("DO", dQty, Config.ParameterWithDefault("WBUOM", "kg"))
                            End If
                        ElseIf sItemId = "IDH_WRORD" Then
                            setWFValue(oForm, "WRROWChanged", True)
                            setDFValue(oForm, msRowTable, "U_WRRow", "")
                        ElseIf sItemId = "IDH_ADDEX" Then
                            oDOR.doMarkUserUpdate(IDH_JOBSHD._TAddChrg)
                        ElseIf sItemId = "IDH_PURWGT" Then
                            oDOR.doMarkUserUpdate(IDH_JOBSHD._ProWgt)
                        ElseIf sItemId = "IDH_TIPWEI" Then
                            oDOR.doMarkUserUpdate(IDH_JOBSHD._TipWgt)
                        ElseIf sItemId = "IDH_TIPCOS" Then
                            oDOR.doMarkUserUpdate(IDH_JOBSHD._TipCost)

                            'Ask To Save to SIP
                            doRequestCIPSIPUpdate(oForm, "ASKSIP", WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.DispCst)
                        ElseIf sItemId = "IDH_ORDQTY" Then
                            oDOR.doMarkUserUpdate(IDH_JOBSHD._CarWgt)
                            'oDOR.doMarkUserUpdate(IDH_JOBSHD._OrdWgt)
                        ElseIf sItemId = "IDH_ORDCOS" Then
                            oDOR.doMarkUserUpdate(IDH_JOBSHD._OrdCost)

                            'Ask To Save to SIP
                            doRequestCIPSIPUpdate(oForm, "ASKSIP", WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.HaulCst)
                        ElseIf sItemId = "IDH_PRCOST" Then
                            oDOR.doMarkUserUpdate(IDH_JOBSHD._PCost)

                            'Ask To Save to SIP
                            doRequestCIPSIPUpdate(oForm, "ASKSIP", WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.ProCst)
                        ElseIf sItemId = "IDH_RDWGT" Then
                            oDOR.doMarkUserUpdate(IDH_JOBSHD._RdWgt)
                            doSetCheckBoxes(oForm)
                        ElseIf sItemId = "IDH_LOADSH" Then
                            Dim sLoadSheet As String = getDFValue(oForm, msRowTable, "U_LoadSht")
                            Dim oLoadHead As User.IDH_MANBALD = New User.IDH_MANBALD
                            If oLoadHead.getLoadSheet(sLoadSheet) Then
                                oDOR.U_WasCd = oLoadHead.U_ItemCd
                                oDOR.U_WasDsc = Config.INSTANCE.doGetItemDescription(oLoadHead.U_ItemCd)

                                ''NOT Sure about the unit used in the Batch
                                'Dim dReadWeight As Double = Config.INSTANCE.doConvertWeights( goParent,"t", "kg", oLoadHead.getTotalWeights())
                                'setDFValue(oForm, msRowTable, "U_RdWgt", dReadWeight)

                                oDOR.U_UOM = "t"

                                Dim dLoadWeight As Double = oLoadHead.getTotalWeights()
                                oDOR.U_CstWgt = dLoadWeight

                                setEnableItem(oForm, False, "IDH_CUSWEI")
                                setEnableItem(oForm, False, "IDH_UOM")
                            End If
                            com.idh.bridge.DataHandler.INSTANCE.doReleaseObject(oLoadHead)
                        ElseIf sItemId = "IDH_BOOKDT" Then
                            ''## MA Start 11-08-2014 Issue#313
                            ''## Updated By LPV Start 2014-08-13
                            oDOR.U_BDate = oDO.U_BDate
                            ''com.idh.utils.dates.doStrToDate(getFormDFValue(oForm, "U_BDate")) ' getDFValue(oForm, "@IDH_DISPORD", "U_BDate")
                            'oDOR.doMarkUserUpdate(IDH_JOBSHD._BDate)
                            ''## Updated By LPV Start 2014-08-13
                            ''## MA End 11-08-2014 Issue#313
                            If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                'doItAll(oForm)
                            End If
                            ''    ''## MA Start 08-08-2014 Issue#313
                            ''ElseIf sItemId = "IDH_JOBDAT" Then
                            ''    oDOR.doMarkUserUpdate(IDH_JOBSHD._BDate)
                            ''    ''## MA End 08-08-2014 Issue#313
                        ElseIf sItemId = "IDH_REQDAT" Then
                            oDOR.doMarkUserUpdate(IDH_JOBSHD._RDate)
                        ElseIf sItemId = "IDH_CUSCM" Then
                            oDOR.doMarkUserUpdate(IDH_JOBSHD._AUOMQt)
                            doSetCheckBoxes(oForm)
                        ElseIf sItemId = "IDH_CUSWEI" Then
                            oDOR.doMarkUserUpdate(IDH_JOBSHD._CstWgt)
                            doRefreshAdditionalPrices(oForm)
                        ElseIf sItemId = "IDH_CUSCHG" Then
                            oDOR.doMarkUserUpdate(IDH_JOBSHD._TCharge)
                            doRequestCIPSIPUpdate(oForm, "ASKCIP", WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TChrg)

                            ''Update BP Additional Charge rows when 2nd Weight is changed 
                            doUpdateAutoAddBPAddCharges(oForm, Date.Now())
                        ElseIf sItemId = "IDH_DISCNT" Then
                            oDOR.doMarkUserUpdate(IDH_JOBSHD._Discnt)
                        ElseIf sItemId = "IDH_DSCPRC" Then
                            oDOR.doMarkUserUpdate(IDH_JOBSHD._DisAmt)
                        ElseIf sItemId = "IDH_PRDCIT" Then
                            If Config.INSTANCE.getParameterAsBool("DOORLU", False) = False Then
                                Dim sOrigin As String
                                sOrigin = getDFValue(oForm, msRowTable, "U_Origin")
                                If sOrigin Is Nothing OrElse sOrigin.Length = 0 Then
                                    setDFValue(oForm, msRowTable, "U_Origin", getDFValue(oForm, "@IDH_DISPORD", "U_PCity"))
                                End If
                            End If
                        ElseIf sItemId = "IDH_RORIGI" Then
                            If Config.INSTANCE.getParameterAsBool("DOORLU", False) = False Then
                                Dim sOrigin As String
                                sOrigin = getDFValue(oForm, msRowTable, "U_Origin")
                                setDFValue(oForm, "@IDH_DISPORD", "U_PCity", sOrigin)
                            End If
                        End If
                    Else
                        '                    		If sItemID = "IDH_PROCD" Then
                        '                            	doChooseRowProducer(oForm, True)
                        '                            End If
                    End If

                    ''ALWAYS DO THESE VALIDATIONS
                    ''Even if it was set by code
                    If bIsInnerEvent = False Then
                        If sItemId = "IDH_ITMCOD" Then '
                            setDFValue(oForm, msRowTable, "U_ItemDsc", "")
                            doChooseContainer(oForm, sItemId)
                        ElseIf sItemId = "IDH_DESC" Then
                            setDFValue(oForm, msRowTable, "U_ItemCd", "")
                            doChooseContainer(oForm, sItemId)
                            'If sItemId = "IDH_ITMCOD" Then
                            '    Dim sItem As String = getDFValue(oForm, msRowTable, "U_ItemCd")

                            '    setSharedData(oForm, "TRG", "PROD")
                            '    setSharedData(oForm, "IDH_ITMCOD", sItem)
                            '    setSharedData(oForm, "IDH_GRPCOD", "")
                            '    setSharedData(oForm, "SILENT", "SHOWMULTI")
                            '    goParent.doOpenModalForm("IDHISRC", oForm)
                        ElseIf sItemId = "IDH_PROCD" Then
                            setDFValue(oForm, "@IDH_DISPORD", "U_PCardNM", "")
                            doChooseProducer(oForm, True, sItemId)
                        ElseIf sItemId = "IDH_RORIGI" OrElse sItemId = "IDH_CORIGI" Then
                            Dim sOrigin As String = getDFValue(oForm, msRowTable, "U_Origin")
                            doChooseOrigin(oForm, False, sOrigin, sItemId)
                            '## Start 27-11-2013
                        ElseIf sItemId = "IDH_WASCL1" Then ' OrElse sItemId = "IDH_WASMAT" Then
                            'setDFValue(oForm, msRowTable, "U_WasDsc", "") '.SetValue("U_WasDsc", .Offset, "")
                            doChooseWasteCode(oForm, sItemId)
                            'setSharedData(oForm, "TP", "WAST")
                            'setSharedData(oForm, "IDH_GRPCOD", Config.INSTANCE.doWasteMaterialGroup())
                            'setSharedData(oForm, "IDH_CRDCD", getDFValue(oForm, "@IDH_DISPORD", "U_CardCd"))
                            'setSharedData(oForm, "IDH_ITMCOD", getDFValue(oForm, msRowTable, "U_WasCd"))
                            'setSharedData(oForm, "IDH_INVENT", "") '"N")
                            'setSharedData(oForm, "SILENT", "SHOWMULTI")
                            'setSharedData(oForm, "IDH_JOBTP", getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp))

                            'If getSharedData(oForm, "IDH_ITMSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_ITMSRCHOPEND") = "" Then
                            '    goParent.doOpenModalForm("IDHWISRC", oForm)
                            'End If
                            '## End 27-11-2013
                        ElseIf sItemId = "IDH_WASMAT" Then
                            'setDFValue(oForm, msRowTable, IDH_DISPROW._WasCd, "")
                            doChooseWasteCode(oForm, sItemId)
                        ElseIf sItemId = "IDH_WRROW" Then
                            ''Dim bWRRowChanged As Boolean = getWFValue(oForm,"WRROWChanged")
                            ''If pVal.ItemChanged OrElse bWRRowChanged Then
                            ''If pVal.ItemChanged Then
                            If oForm.Items.Item("IDH_WRORD").Enabled = True Then
                                If wasSetWithCode(oForm, "IDH_WRORD") = True OrElse wasSetWithCode(oForm, "IDH_WRROW") = True Then
                                    Dim sReg As String = getDFValue(oForm, msRowTable, "U_Lorry", True)
                                    Dim sRowID As String = getDFValue(oForm, msRowTable, "U_WRRow", True).Trim()
                                    Dim sCode As String = getDFValue(oForm, msRowTable, "U_WROrd", True).Trim()
                                    If sRowID.Length > 0 OrElse sCode.Length > 0 Then
                                        Dim sDORowID As String = getDFValue(oForm, msRowTable, "Code").Trim()
                                        Dim sDOCode As String = getDFValue(oForm, "@IDH_DISPORD", "Code").Trim()

                                        ''##MA Start 03-09-2014 Issue#422
                                        If Config.ParamaterWithDefault("SRLNKVEH", "FALSE") Then
                                            setSharedData(oForm, "IDH_VEHREG", sReg)
                                        Else
                                            setSharedData(oForm, "IDH_VEHREG", "^" & sReg)
                                        End If
                                        ''##MA End 03-09-2014 Issue#422
                                        setSharedData(oForm, "DISPORD", sDOCode)
                                        setSharedData(oForm, "DISPROW", sDORowID)
                                        setSharedData(oForm, "IDH_WOHD", sCode)
                                        setSharedData(oForm, "IDH_WOROW", sRowID)
                                        setSharedData(oForm, "SILENT", "SHOWMULTI")
                                        setSharedData(oForm, "USEDROWS", doGetLinkedWORs(oForm))

                                        goParent.doOpenModalForm("IDHLNKWO", oForm)

                                        'OnTime Ticket 23675 - Disposal Order Booking Date
                                        'Disposal order booking date to be updated from the linked Waste Order's Actual Start Date
                                        'The data update depends on WR Config Key
                                        Dim sUpdateDOBDate As String

                                        sUpdateDOBDate = Config.Parameter("DOBDWOAD")
                                        If sUpdateDOBDate.ToUpper().Equals("TRUE") Then
                                            doGetLinkWOASDate(oForm)
                                        End If
                                    End If
                                End If
                            End If
                            setWFValue(oForm, "WRROWChanged", False)
                            ''## MA Start 16-10-2014
                        ElseIf sItemId = "IDH_BPWGT" Then
                            oDOR.doMarkUserUpdate(IDH_JOBSHD._BPWgt)
                            ''If getFormDFValue(oForm, IDH_DISPORD._UseBPWgt) IsNot Nothing AndAlso _
                            ''    getFormDFValue(oForm, IDH_DISPORD._UseBPWgt) = "Y" Then
                            ''    If oDOR.getMarketingMode() = Config.MarketingMode.PO Then
                            ''        setDFValue(oForm, msRowTable, "U_ProWgt", getDFValue(oForm, msRowTable, IDH_DISPROW._BPWgt))
                            ''        oDOR.doMarkUserUpdate(IDH_JOBSHD._ProWgt)
                            ''    Else
                            ''        setDFValue(oForm, msRowTable, "U_CstWgt", getDFValue(oForm, msRowTable, IDH_DISPROW._BPWgt))
                            ''        oDOR.doMarkUserUpdate(IDH_JOBSHD._CstWgt)
                            ''    End If
                            ''    ''If getWFValue(oForm, "FormMode") = "PO" Then
                            ''    ''Else
                            ''    ''End If
                            ''End If
                            '' ''## MA End 16-10-2014
                        End If
                    End If
                Else
                    If bIsInnerEvent = False Then
                        '' MA Start 19-11-2014
                        'If sItemId = "IDH_ITMCOD" Then
                        '    Dim sItem As String = getDFValue(oForm, msRowTable, "U_ItemCd")
                        '    If sItem.Length = 0 Then
                        '        setSharedData(oForm, "TRG", "PROD")
                        '        setSharedData(oForm, "IDH_ITMCOD", sItem)
                        '        setSharedData(oForm, "IDH_GRPCOD", "")
                        '        setSharedData(oForm, "SILENT", "SHOWMULTI")
                        '        goParent.doOpenModalForm("IDHISRC", oForm)
                        '    End If
                        'Else
                        '' MA End 19-11-2014
                        'If sItemId = "IDH_WASCL1" Then
                        '    Dim sWasteCode As String = getDFValue(oForm, msRowTable, "U_WasCd")
                        '    If sWasteCode.Length = 0 OrElse sWasteCode.IndexOf("*") > -1 Then
                        '        setSharedData(oForm, "TP", "WAST")
                        '        setSharedData(oForm, "IDH_GRPCOD", Config.INSTANCE.doWasteMaterialGroup())
                        '        setSharedData(oForm, "IDH_CRDCD", getDFValue(oForm, "@IDH_DISPORD", "U_CardCd"))
                        '        setSharedData(oForm, "IDH_ITMCOD", sWasteCode)
                        '        setSharedData(oForm, "IDH_INVENT", "") '"N")
                        '        setSharedData(oForm, "SILENT", "SHOWMULTI")
                        '        setSharedData(oForm, "IDH_JOBTP", getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp))

                        '        If getSharedData(oForm, "IDH_ITMSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_ITMSRCHOPEND") = "" Then
                        '            goParent.doOpenModalForm("IDHWISRC", oForm)
                        '        End If
                        '    End If
                        'Elseif
                        If sItemId = "IDH_PROCD" Then
                            Dim sProdCd As String = getDFValue(oForm, "@IDH_DISPORD", "U_PCardCd")
                            If sProdCd.IndexOf("*") > -1 Then
                                doChooseProducer(oForm, True, sItemId) 'RowProducer(oForm, True)
                            End If
                        ElseIf sItemId = "IDH_PRONM" Then
                            Dim sProdNM As String = getDFValue(oForm, "@IDH_DISPORD", "U_PCardNM")
                            If sProdNM.IndexOf("*") > -1 Then
                                doChooseProducer(oForm, True, sItemId) 'RowProducer(oForm, True)
                            End If
                        ElseIf sItemId = "IDH_WASCL1" OrElse sItemId = "IDH_WASMAT" Then
                            doChooseWasteCode(oForm, sItemId)
                            'ElseIf sItemId = "IDH_CUST" Then
                            '    Dim sCustCd As String = getDFValue(oForm, "@IDH_DISPORD", "U_CardCd")
                            '    If sCustCd.Length = 0 OrElse sCustCd.IndexOf("*") > -1 Then
                            '        doChooseCustomer(oForm, True, sItemId) 'RowProducer(oForm, True)
                            '    End If
                            'ElseIf sItemId = "IDH_CUSTNM" Then
                            '    Dim sCustNM As String = getDFValue(oForm, "@IDH_DISPORD", "U_CardNM")
                            '    If sCustNM.Length = 0 OrElse sCustNM.IndexOf("*") > -1 Then
                            '        doChooseCustomer(oForm, True, sItemId) 'RowProducer(oForm, True)
                            '    End If
                        ElseIf sItemId = "IDH_WRROW" Then
                            Dim bWRRowChanged As Boolean = getWFValue(oForm, "WRROWChanged")
                            If bWRRowChanged Then
                                If oForm.Items.Item("IDH_WRORD").Enabled = True Then
                                    Dim sReg As String = getDFValue(oForm, msRowTable, "U_Lorry", True)
                                    Dim sRowID As String = getDFValue(oForm, msRowTable, "U_WRRow", True).Trim()
                                    Dim sCode As String = getDFValue(oForm, msRowTable, "U_WROrd", True).Trim()
                                    If sRowID.Length > 0 OrElse sCode.Length > 0 Then
                                        Dim sDORowID As String = getDFValue(oForm, msRowTable, "Code").Trim()
                                        Dim sDOCode As String = getDFValue(oForm, "@IDH_DISPORD", "Code").Trim()

                                        ''##MA Start 03-09-2014 Issue#422
                                        If Config.ParamaterWithDefault("SRLNKVEH", "FALSE") Then
                                            setSharedData(oForm, "IDH_VEHREG", sReg)
                                        Else
                                            setSharedData(oForm, "IDH_VEHREG", "^" & sReg)
                                        End If
                                        ''##MA End 03-09-2014 Issue#422                                        
                                        setSharedData(oForm, "DISPORD", sDOCode)
                                        setSharedData(oForm, "DISPROW", sDORowID)
                                        setSharedData(oForm, "IDH_WOHD", sCode)
                                        setSharedData(oForm, "IDH_WOROW", sRowID)
                                        setSharedData(oForm, "SILENT", "SHOWMULTI")
                                        setSharedData(oForm, "USEDROWS", doGetLinkedWORs(oForm))

                                        goParent.doOpenModalForm("IDHLNKWO", oForm)

                                        'OnTime Ticket 23675 - Disposal Order Booking Date
                                        'Disposal order booking date to be updated from the linked Waste Order's Actual Start Date
                                        'The data update depends on WR Config Key
                                        Dim sUpdateDOBDate As String

                                        sUpdateDOBDate = Config.Parameter("DOBDWOAD")
                                        If sUpdateDOBDate.ToUpper().Equals("TRUE") Then
                                            doGetLinkWOASDate(oForm)
                                        End If
                                    End If
                                End If
                                setWFValue(oForm, "WRROWChanged", False)
                            End If
                        End If
                    End If
                End If
                'If pVal.InnerEvent = False And pVal.ItemChanged Then
                '    If sItemId = "IDH_ITMCOD" OrElse sItemId = "IDH_DESC" Then
                '        doChooseContainer(oForm, sItemId)
                '    End If
                'End If
            End If
            Return True
        End Function

        '** The ItemSubEvent handler
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            '			oForm.Freeze(True)
            '			Try
            MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            Dim oDO As IDH_DISPORD = thisDO(oForm)
            Dim sItemID As String = pVal.ItemUID

            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If pVal.BeforeAction = True Then
                    Dim iKey As Integer = pVal.CharPressed()
                    If iKey = 13 AndAlso sItemID.Equals("IDH_SPECIN") = False Then
                        IDHAddOns.idh.addon.Base.APPLICATION.SendKeys("{TAB}")
                        BubbleEvent = False
                    End If
                    Return False
                Else
                    If doCheckSearchKey(pVal) Then
                    Else
                        If sItemID = "IDH_VEHREG" Then ' AndAlso pVal.ItemChanged Then
                            setWFValue(oForm, "LastVehAct", "DOSEARCH")
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                doValidationEvent(oForm, pVal, BubbleEvent)
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                If pVal.BeforeAction = False Then
                    If sItemID = "uBC_CUSCHG" Then
                        oDOR.doCalculateTipChargeTotals(IDH_DISPROW.TOTAL_CALCULATE)
                    ElseIf sItemID = "uBC_CUSCHR" Then
                        oDOR.doCalculateHaulageChargeTotals(IDH_DISPROW.TOTAL_CALCULATE)
                    ElseIf sItemID = "uBC_PRCOST" Then
                        oDOR.doCalculateProducerCostTotals(IDH_DISPROW.TOTAL_CALCULATE)
                    ElseIf sItemID = "uBC_TIPCOS" Then
                        oDOR.doCalculateTipCostTotals(IDH_DISPROW.TOTAL_CALCULATE)
                    ElseIf sItemID = "uBC_ORDCOS" Then
                        oDOR.doCalculateHaulageCostTotals(IDH_DISPROW.TOTAL_CALCULATE)
                    ElseIf sItemID = "IDH_WEIBRG" Then
                        doConfigWeighBridge(oForm, getItemValue(oForm, "IDH_WEIBRG"))
                    ElseIf sItemID = "IDH_JOBTTP" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            doSetDOAddDefaults(oForm, False)
                        End If

                        Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)
                        If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            ''21-06-2016 Start Issue#
                            If oDOR.U_Lorry.Trim() <> String.Empty AndAlso oDOR.IsLinkedToOrder() = False AndAlso getWFValue(oForm, "CreateNewVechicle") = True Then 'sWOLink Is Nothing OrElse sWOLink.Length = 0 Then
                                Dim sMarkAct As String = Config.INSTANCE.getValueFromVehicleMasterByRegistration(oDOR.U_Lorry.Trim(), "U_MarkAc")
                                If sMarkAct Is Nothing Then
                                    sMarkAct = com.idh.bridge.lookups.FixedValues.getReqFocStatus()
                                End If


                                If sMarkAct.Equals("DOFOC") OrElse sMarkAct.Equals(com.idh.bridge.lookups.FixedValues.getStatusFoc()) Then
                                    doSwitchCheckBox(oForm, "IDH_FOC", "Y", True)
                                ElseIf sMarkAct.Equals("DOORD") Then
                                    doSwitchCheckBox(oForm, "IDH_DOORD", "Y", True)
                                ElseIf sMarkAct.Equals("DOARI") Then
                                    doSwitchCheckBox(oForm, "IDH_DOARI", "Y", True)
                                ElseIf sMarkAct.Equals("DOARIP") Then
                                    doSwitchCheckBox(oForm, "IDH_DOARIP", "Y", True)
                                ElseIf sMarkAct.Equals("DOAINV") Then
                                    doSwitchCheckBox(oForm, "IDH_AINV", "Y", True)
                                Else

                                    doSwitchCheckBox(oForm, "IDH", "N", True)
                                End If
                            Else
                                doSwitchCheckBox(oForm, "IDH_FOC", "Y", True)
                                doSwitchCheckBox(oForm, "IDH_DOPO", "N", True)
                            End If
                            ''21-06-2016 End Issue#
                            If Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) Then
                                doHideLoadSheet(oForm)
                                If Config.INSTANCE.doCheckCanPurchaseWB(getDFValue(oForm, msRowTable, "U_WasCd")) Then
                                    doSwitchToPO(oForm, True)
                                Else
                                    doSwitchToSO(oForm)
                                End If
                            Else
                                doSwitchToSO(oForm)
                                doShowLoadSheet(oForm)
                            End If
                            oDOR.doMarkUserUpdate(IDH_DISPROW._JobTp)


                            'oDOR.doGetAllUOM()
                            'getAutoAdditionalCodes(oForm)

                            If oDOR.U_UseWgt = "1" Then
                                oDOR.BlockChangeNotif = True
                                Try
                                    oDOR.doGetAllUOM(True)
                                Catch e As Exception
                                End Try
                                oDOR.BlockChangeNotif = False
                            End If

                            'UPDATE THE ADDITIONAL GRID FROM HERE FOR NOW - IN FUTURE THE DBOBJECT WILL BE USED
                            doRefreshAdditionalPrices(oForm)

                            doAddAutoAdditionalCodes(oForm)

                            oDOR.doGetChargePrices(True)
                            oDOR.doGetCostPrices(True, True, True, True)

                            If Config.INSTANCE.getParameterAsBool("USEBPACH", False) Then
                                'Remove any existing AutoAdd rows 
                                'This is we dont know any items being added for this BP previously 
                                removeAutoAddBPAddCharges(oForm)
                                'Now add any new AddCharges row 
                                getBPAdditionalCharges(oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDH_BRANCH" Then
                        If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            doTZoneCombo(oForm)
                            setDFValue(oForm, msRowTable, "U_TZone", "")
                            doChooseBranchSite(oForm)
                            oDOR.doGetAllUOM()

                            'UPDATE THE ADDITIONAL GRID FROM HERE FOR NOW - IN FUTURE THE DBOBJECT WILL BE USED
                            doRefreshAdditionalPrices(oForm)

                            doAddAutoAdditionalCodes(oForm)

                        End If
                    ElseIf sItemID = "IDH_PUOM" Then
                        If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            oDOR.doMarkUserUpdate(IDH_DISPROW._PUOM)

                            'Ask To Save to SIP
                            doRequestCIPSIPUpdate(oForm, "ASKSIP", WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.PUOM)
                        End If

                    ElseIf sItemID = "IDH_PURUOM" Then
                        If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            oDOR.doMarkUserUpdate(IDH_DISPROW._ProUOM)

                            'Ask To Save to SIP
                            doRequestCIPSIPUpdate(oForm, "ASKSIP", WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.PRUOM)
                        End If
                    ElseIf sItemID = "IDH_UOM" Then
                        If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            oDOR.doMarkUserUpdate(IDH_DISPROW._UOM)

                            'UPDATE THE ADDITIONAL GRID FROM HERE FOR NOW - IN FUTURE THE DBOBJECT WILL BE USED
                            doRefreshAdditionalPrices(oForm)

                            doAddAutoAdditionalCodes(oForm)

                            'Ask To Save to CIP
                            doRequestCIPSIPUpdate(oForm, "ASKCIP", WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TUOM)
                        End If
                    ElseIf sItemID = "uBC_TRNCd" Then
                        oDOR.doMarkUserUpdate(IDH_DISPROW._TrnCode)
                        oDO.U_TRNCd = oDOR.U_TrnCode

                        If Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), oDOR.U_JobTp) Then
                            doHideLoadSheet(oForm)
                            If Config.INSTANCE.doCheckCanPurchaseWB(oDOR.U_WasCd) Then
                                doSwitchToPO(oForm, True)
                            Else
                                doSwitchToSO(oForm)
                            End If
                        Else
                            doSwitchToSO(oForm)
                            doShowLoadSheet(oForm)
                        End If

                        doApplyStatuses(oForm)

                        If String.IsNullOrWhiteSpace(oDOR.U_TrnCode) = False Then
                            setEnableItems(oForm, False, moMarketingCheckboxes)
                        End If

                        If oDOR.U_Status = FixedValues.getDoInvoiceStatus() Then
                            Dim sDoMDDocs As String = Config.Parameter("MDAGED")
                            Dim sAction As String
                            If sDoMDDocs.ToUpper().Equals("TRUE") Then
                                sAction = "D" 'Do      'Config.INSTANCE.getStatusPreFixDo()
                            Else
                                sAction = "R" 'Request 'Config.INSTANCE.getStatusPreFixReq()
                            End If
                            setSharedData(oForm, "ACTION", sAction)
                            goParent.doOpenModalForm("IDHPAYMET", oForm)
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If sItemID = "IDH_CHRPL" Then
                    'Check If the Prices needs to be looked up
                    oDOR.doDelayedPrices(True, False, True)
                ElseIf sItemID = "IDH_CSTPL" Then
                    'Check If the Prices needs to be looked up
                    oDOR.doDelayedPrices(False, True, True)
                ElseIf pVal.BeforeAction = False Then
                    If sItemID = "IDH_USERE" OrElse sItemID = "IDH_USEAU" Then
                        oDOR.setUserChanged(Config.MASK_TIPCSTWGT, False)
                        oDOR.setUserChanged(Config.MASK_PRODUCERCSTWGT, False)
                        oDOR.setUserChanged(Config.MASK_TIPCHRGWGT, False)

                        oDOR.doMarkUserUpdate(IDH_DISPROW._UseWgt)

                        'oDOR.doGetCustomerUOM()
                        'oDOR.doGetDisposalSiteUOM()
                        'If oDOR.getMarketingMode() = Config.MarketingMode.PO Then
                        '    oDOR.doGetProducerUOM()

                        'End If
                    ElseIf sItemID = "IDH_PROCF" Then
                        'doChooseRowProducer(oForm, False)
                        doChooseProducer(oForm, False, sItemID)
                    ElseIf sItemID = "IDH_HAZARR" Then
                        Dim sConNum As String = getDFValue(oForm, msRowTable, "U_ConNum")
                        setSharedData(oForm, "IDH_HAZCN", sConNum)
                        goParent.doOpenModalForm("IDHCONMDO", oForm)
                    ElseIf sItemID = "IDH_CONGEN" Then
                        Dim sConNum As String = ""
                        Dim sBPCd As String = ""
                        Dim sBPNM As String = ""
                        Dim sBPAddress As String = ""

                        sBPCd = getDFValue(oForm, "@IDH_DISPORD", "U_PCardCd").Trim()
                        sBPNM = getDFValue(oForm, "@IDH_DISPORD", "U_PCardNM").Trim()
                        sBPAddress = getDFValue(oForm, "@IDH_DISPORD", "U_PAddress").Trim()

                        sConNum = com.idh.bridge.utils.General.doCreateConNumber(sBPCd, sBPNM, sBPAddress)

                        setDFValue(oForm, msRowTable, "U_ConNum", sConNum)
                    ElseIf sItemID = "IDH_CSCF" Then
                        doChooseComplianceScheme(oForm, False)
                        'ElseIf sItemID = "IDH_SITAL" Then
                        '    Dim sCardCode As String = Nothing
                        '    sCardCode = getFormDFValue(oForm, "U_TIP")

                        '    setSharedData(oForm, "TRG", sItemID)
                        '    setSharedData(oForm, "IDH_CUSCOD", sCardCode)
                        '    setSharedData(oForm, "IDH_ADRTYP", "S")
                        '    ''##MA Start 18-09-2014 Issue#403
                        '    setSharedData(oForm, "IDH_APPLYFLTR", Config.INSTANCE.getParameterAsBool("FLTRADDR", False).ToString)
                        '    ''##MA End 18-09-2014 Issue#403
                        '    goParent.doOpenModalForm("IDHASRCH", oForm)
                    ElseIf sItemID = "IDH_ORDLK" Then
                        Dim sWROrd As String = getDFValue(oForm, msRowTable, "U_WROrd")
                        If sWROrd.Length > 0 Then
                            Dim oData As New ArrayList
                            oData.Add("DoUpdate")
                            oData.Add(sWROrd)
                            goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)
                        End If
                    ElseIf sItemID = "IDH_LOADSL" Then
                        Dim sLoadSheet As String = getDFValue(oForm, msRowTable, "U_LoadSht")
                        setSharedData(oForm, "IDH_LOADSHT", sLoadSheet)

                        Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.manager.LoadForm = New WR1_FR2Forms.idh.forms.fr2.manager.LoadForm(Nothing, oForm.UniqueID, Nothing)
                        oOOForm.doShowModal(oForm)
                    ElseIf sItemID = "IDH_Amend" Then
                        Dim oOOForm As com.uBC.forms.fr3.popup.DOAmend = New com.uBC.forms.fr3.popup.DOAmend(Nothing, oForm.UniqueID) ', oForm)
                        oOOForm.DORCode = oDOR.Code
                        oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doReturnFromAmend)
                        'oOOForm.Handler_GetChequeNumber = New IDH_DISPROW.et_SetChequeNumberTrigger(AddressOf doGetChequeNumber)
                        oOOForm.doShowModal(oForm)
                    ElseIf sItemID = "IDH_ROWLK" Then
                        Dim sWRRow As String = getDFValue(oForm, msRowTable, "U_WRRow")
                        If sWRRow.Length > 0 Then
                            Dim oData As New ArrayList
                            oData.Add("DoUpdate")
                            If sWRRow.Length > 0 Then

                                oData.Add(sWRRow)
                                oData.Add(com.idh.utils.Dates.doStrToDate(getFormDFValue(oForm, "U_BDate")))
                                oData.Add(goParent.doTimeStrToStr(getFormDFValue(oForm, "U_BTime")))
                                oData.Add(getFormDFValue(oForm, "U_ZpCd"))
                                oData.Add(getFormDFValue(oForm, "U_Address"))
                                oData.Add("CANNOTDOCOVERAGE")
                                oData.Add(getFormDFValue(oForm, "U_SteId"))

                                'Added to handle the BP Switching
                                oData.Add(Nothing)
                                oData.Add(getFormDFValue(oForm, "U_PAddress"))
                                oData.Add(getFormDFValue(oForm, "U_PZpCd"))
                                oData.Add(getFormDFValue(oForm, "U_PPhone1"))
                                oData.Add(getFormDFValue(oForm, "U_FirstBP"))



                                oData.Add(getFormDFValue(oForm, "U_AddrssLN"))
                                oData.Add(getFormDFValue(oForm, "U_PAddrsLN"))

                                goParent.doOpenModalForm("IDHJOBS", oForm, oData)
                            End If
                        End If
                    ElseIf sItemID = "IDH_NEWWEI" Then
                        If doRowValidation(oForm) Then
                            Dim oUpdateGrid As UpdateGrid
                            oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                            doSetGridRowData(oForm, oUpdateGrid)
                            doUpdateGridRowDORCodes(oForm)

                            'oUpdateGrid.doAddNewLine()
                            oUpdateGrid.doAddEditLine()
                            doNewRowAskAboutVehicle(oForm)
                        End If
                    ElseIf sItemID = "IDH_TABADD" Then
                        oForm.PaneLevel = 11
                    ElseIf sItemID = "IDH_WEIGH" Then
                        oForm.PaneLevel = 10
                    ElseIf sItemID = "IDH_TABACT" Then
                        'OnTime 32756: WR1_NewDev_Planning_BOOKING IN STOCK
                        'setting PaneLevel for both Activity and Deductions TAB
                        oForm.PaneLevel = 13
                    ElseIf sItemID = "IDH_TABDED" Then
                        oForm.PaneLevel = 12
                    ElseIf sItemID = "IDH_DOC" Then
                        doDocReport(oForm)
                    ElseIf sItemID = "IDH_CON" Then
                        doConvReport(oForm)
                    ElseIf sItemID = "IDH_ACTVTY" Then
                        'OnTime 32756: WR1_NewDev_Planning_BOOKING IN STOCK
                        'Event handling when Activity button is clicked
                        If oForm.PaneLevel <> 13 Then
                            oForm.PaneLevel = 13
                        End If

                        doCheckAndAssignRowNumber(oForm)

                        setSharedData(oForm, "IDH_DOH", getDFValue(oForm, "@IDH_DISPORD", "Code"))
                        setSharedData(oForm, "IDH_DOR", getDFValue(oForm, msRowTable, "Code"))
                        setSharedData(oForm, "IDH_CUST", getDFValue(oForm, msRowTable, IDH_DISPROW._CustCd)) 'getDFValue(oForm, "@IDH_DISPORD", "U_CardCd"))
                        setSharedData(oForm, "sFMode", SAPbouiCOM.BoFormMode.fm_ADD_MODE.ToString())
                        goParent.doOpenModalForm("651", oForm)
                    ElseIf sItemID = "IDH_BUF1" Then
                        Dim dWei As Double = getUFValue(oForm, "IDH_WEIB")
                        Dim sSer As String = getUFValue(oForm, "IDH_SERB")
                        Dim sDate As String = getUFValue(oForm, "IDH_WDTB")

                        If dWei > 0 Then
                            oDOR.U_Ser1 = sSer
                            oDOR.U_WDt1_AsString(sDate)
                            oDOR.U_Wei1 = dWei

                            doSetUpdate(oForm)
                        End If
                    ElseIf sItemID = "IDH_BUF2" Then
                        Dim dWei As Double = getUFValue(oForm, "IDH_WEIB")
                        Dim sSer As String = getUFValue(oForm, "IDH_SERB")
                        Dim sDate As String = getUFValue(oForm, "IDH_WDTB")

                        If dWei > 0 Then
                            oDOR.U_Ser2 = sSer
                            oDOR.U_WDt2_AsString(sDate)
                            oDOR.U_Wei2 = dWei

                            doSetUpdate(oForm)
                        End If
                    ElseIf sItemID = "IDH_TAR1" Then
                        Dim dVehTar As Double = getUFValue(oForm, "IDH_TARWEI")
                        Dim dTrlTar As Double = getUFValue(oForm, "IDH_TRLTar")

                        oDOR.U_Ser1 = "NONE"
                        oDOR.U_Wei1 = dVehTar + dTrlTar
                    ElseIf sItemID = "IDH_TAR2" Then
                        Dim dVehTar As Double = getUFValue(oForm, "IDH_TARWEI")
                        Dim dTrlTar As Double = getUFValue(oForm, "IDH_TRLTar")

                        oDOR.U_Ser2 = "NONE"
                        oDOR.U_Wei2 = dVehTar + dTrlTar
                    ElseIf sItemID = "IDH_ACC1" Then
                        doReadWeight(oForm, "ACC1")
                    ElseIf sItemID = "IDH_ACC2" Then
                        doReadWeight(oForm, "ACC2")
                    ElseIf sItemID = "IDH_ACCB" Then
                        doReadWeight(oForm, "ACCB")
                    ElseIf sItemID = "IDH_REF" Then
                        doReadWeight(oForm, "CR")
                    ElseIf sItemID = "IDH_WOCF" OrElse sItemID = "IDH_WOCFL" Then
                        If oForm.Items.Item("IDH_WRORD").Enabled = True Then
                            Dim sWORowID As String = getDFValue(oForm, msRowTable, "U_WRRow", True)
                            Dim sWOCode As String = getDFValue(oForm, msRowTable, "U_WROrd", True)
                            Dim sReg As String = getDFValue(oForm, msRowTable, "U_Lorry", True)
                            Dim sDORowID As String = getDFValue(oForm, msRowTable, "Code")
                            Dim sDOCode As String = getDFValue(oForm, "@IDH_DISPORD", "Code")


                            If sWOCode.Length = 0 AndAlso sWORowID.Length = 0 AndAlso sItemID = "IDH_WOCFL" Then
                                Dim sWrkCode As String = getWFValue(oForm, "LASTLINKEDWO")
                                If Not sWrkCode Is Nothing AndAlso sWrkCode.Length > 0 Then
                                    sWOCode = sWrkCode
                                    setDFValue(oForm, msRowTable, "U_WROrd", sWOCode)
                                End If
                            End If

                            ''##MA Start 03-09-2014 Issue#422
                            If Config.ParamaterWithDefault("SRLNKVEH", "FALSE") Then
                                setSharedData(oForm, "IDH_VEHREG", sReg)
                            Else
                                setSharedData(oForm, "IDH_VEHREG", "^" & sReg)
                            End If
                            ''##MA End 03-09-2014 Issue#422


                            setSharedData(oForm, "DISPORD", sDOCode)
                            setSharedData(oForm, "DISPROW", sDORowID)
                            setSharedData(oForm, "IDH_WOHD", sWOCode)
                            setSharedData(oForm, "IDH_WOROW", sWORowID)
                            setSharedData(oForm, "SILENT", "SHOWMULTI")
                            setSharedData(oForm, "USEDROWS", doGetLinkedWORs(oForm))

                            'MDJ Bug List Fix #2
                            setSharedData(oForm, "CUSTREF", getDFValue(oForm, "@IDH_DISPORD", "U_CustRef"))

                            goParent.doOpenModalForm("IDHLNKWO", oForm)

                            'OnTime Ticket 23675 - Disposal Order Booking Date
                            'Disposal order booking date to be updated from the linked Waste Order's Actual Start Date
                            'The data update depends on WR Config Key
                            Dim sUpdateDOBDate As String

                            sUpdateDOBDate = Config.Parameter("DOBDWOAD")
                            If sUpdateDOBDate.ToUpper().Equals("TRUE") Then
                                doGetLinkWOASDate(oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDH_ORLK" Then
                        Dim sOrigin As String = getDFValue(oForm, msRowTable, "U_Origin")
                        doChooseOrigin(oForm, False, sOrigin, "")
                    ElseIf sItemID = "IDH_ITCF" Then
                        '' MA 19-11-2014
                        'setSharedData(oForm, "TRG", "PROD")
                        'setSharedData(oForm, "IDH_ITMCOD", "")
                        'setSharedData(oForm, "IDH_GRPCOD", Config.Parameter("MDDDIG"))
                        'goParent.doOpenModalForm("IDHISRC", oForm)
                        doChooseContainer(oForm, sItemID)
                        '' MA 19-11-2014
                    ElseIf sItemID = "IDH_WASCF" Then
                        '##Start 19-11-2013
                        doChooseWasteCode(oForm, sItemID)
                        'setSharedData(oForm, "TP", "WAST")
                        'Dim sWasteName As String = getDFValue(oForm, msRowTable, "U_WasDsc")
                        'Dim sWasteCode As String = getDFValue(oForm, msRowTable, "U_WasCd")
                        'setSharedData(oForm, "IDH_ITMCOD", sWasteCode)
                        'setSharedData(oForm, "IDH_ITMNAM", sWasteName)
                        'setSharedData(oForm, "IDH_GRPCOD", Config.INSTANCE.doWasteMaterialGroup())
                        'setSharedData(oForm, "IDH_CRDCD", getDFValue(oForm, "@IDH_DISPORD", "U_CardCd"))
                        'setSharedData(oForm, "IDH_INVENT", "") '"N")
                        'setSharedData(oForm, "IDH_JOBTP", getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp))

                        'If getSharedData(oForm, "IDH_ITMSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_ITMSRCHOPEND") = "" Then
                        '    goParent.doOpenModalForm("IDHWISRC", oForm)
                        'End If
                        '##End 19-11-2013
                    ElseIf sItemID = "IDH_OS" Then
                        doCallLorrySearchOnSite(oForm)

                        'Dim sReg As String '= getDFValue(oForm, msRowTable, "U_Lorry", True)
                        'sReg = getDFValue(oForm, msRowTable, "U_Lorry", True)
                        'Dim sRegChanged As String = com.idh.bridge.utils.General.doFixReg(sReg)
                        'setSharedData(oForm, "JOBTYPE", "")
                        'If sRegChanged.Equals(sReg) = False Then
                        '    setDFValue(oForm, msRowTable, "U_Lorry", sRegChanged, True)
                        'End If
                        'If Config.INSTANCE.getParameterAsBool("DOVMSUC", True) Then
                        '    Dim sCustomer As String
                        '    sCustomer = getDFValue(oForm, "@IDH_DISPORD", "U_CardCd", True)
                        '    setSharedData(oForm, "IDH_VEHCUS", sCustomer)
                        'Else
                        '    setSharedData(oForm, "IDH_VEHCUS", "")
                        'End If
                        'setSharedData(oForm, "IDH_VEHREG", sRegChanged)
                        'setSharedData(oForm, "SHOWCREATE", "TRUE")
                        ''setSharedData(oForm, "IDH_WR1OT", "^DO")
                        'setSharedData(oForm, "IDH_WR1OT", "")
                        'setSharedData(oForm, "SHOWONSITE", "TRUE")
                        'goParent.doOpenModalForm("IDHLOSCH", oForm)
                    ElseIf sItemID = "IDH_REGL" Then
                        doCallLorrySearchCFL(oForm)

                        'Dim sReg As String '= getDFValue(oForm, msRowTable, "U_Lorry", True)
                        'sReg = getDFValue(oForm, msRowTable, "U_Lorry", True)

                        'Dim sRegChanged As String = com.idh.bridge.utils.General.doFixReg(sReg)

                        'If sRegChanged.Equals(sReg) = False Then
                        '    setDFValue(oForm, msRowTable, "U_Lorry", sRegChanged, True)
                        'End If
                        'If Config.INSTANCE.getParameterAsBool("DOVMSUC", True) Then
                        '    Dim sCustomer As String
                        '    sCustomer = getDFValue(oForm, "@IDH_DISPORD", "U_CardCd", True)
                        '    setSharedData(oForm, "IDH_VEHCUS", sCustomer)
                        'Else
                        '    setSharedData(oForm, "IDH_VEHCUS", "")
                        'End If
                        'setSharedData(oForm, "IDH_VEHREG", sRegChanged)
                        'setSharedData(oForm, "SHOWCREATE", "TRUE")
                        ''setSharedData(oForm, "IDH_WR1OT", "^DO")
                        'setSharedData(oForm, "IDH_WR1OT", "")
                        'setSharedData(oForm, "SHOWONSITE", "FALSE")
                        'setSharedData(oForm, "SHOWACTIVITY", "FALSE")
                        'goParent.doOpenModalForm("IDHLOSCH", oForm)
                    ElseIf sItemID = "IDH_TXDT" Then
                        setSharedData(oForm, "IDHDISPROW", thisDOR(oForm))
                        Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                        Dim sAddItems As Collections.Hashtable = Nothing
                        If Not oGrid Is Nothing Then
                            sAddItems = New Collections.Hashtable
                            For i As Int32 = 0 To oGrid.getRowCount - 1
                                Dim sAddItem(2) As String
                                If oGrid.doGetFieldValue("U_ItemCd", i).ToString.Trim = "" Then
                                    Continue For
                                End If
                                sAddItem(0) = oGrid.doGetFieldValue("U_ItemCd", i)
                                sAddItem(1) = CDbl(oGrid.doGetFieldValue("U_ItmChrg", i) * oGrid.doGetFieldValue("U_Quantity", i)).ToString("##0.000")
                                sAddItem(2) = oGrid.doGetFieldValue("U_ItemDsc", i)
                                sAddItems.Add(i, sAddItem)
                            Next
                        End If
                        setSharedData(oForm, "ADDITEMS", sAddItems)
                        goParent.doOpenModalForm("IDH_DOTXDT", oForm)
                    End If
                Else
                    If sItemID = "IDH_NOVAT" Then
                        doSwitchVatGroups(oForm)
                    Else
                        Dim oItem As SAPbouiCOM.Item = oForm.Items.Item(sItemID)
                        If oItem.Type = SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX Then

                            'Check if Generate Sample Checkbox is checked
                            'Generate Sample record in system 
                            'If sItemID = "IDH_SAMCHK" Then
                            'If pVal.FormMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                            '    If getDFValue(oForm, msRowTable, IDH_DISPROW._SampleRef).ToString().Length > 0 AndAlso getDFValue(oForm, msRowTable, IDH_DISPROW._Sample).ToString() = "N" Then
                            '        setDFValue(oForm, msRowTable, IDH_DISPROW._Sample, "Y")
                            '        IDHAddOns.idh.addon.Base.PARENT.doMessage("Cannot untick the checkbox. There is a Lab Task linked with the DO.")
                            '        Return False
                            '    Else
                            '        Dim sResult As String = String.Empty
                            '        sResult = doGenerateLabTask(oForm)
                            '        If Not sResult.Equals("-1") Then
                            '            setDFValue(oForm, msRowTable, IDH_DISPROW._Sample, "Y")
                            '            setDFValue(oForm, msRowTable, IDH_DISPROW._SampleRef, sResult)
                            '            setDFValue(oForm, msRowTable, IDH_DISPROW._SampStatus, "Pending")
                            '        Else
                            '            setDFValue(oForm, msRowTable, IDH_DISPROW._Sample, "N")
                            '            setDFValue(oForm, msRowTable, IDH_DISPROW._SampleRef, String.Empty)
                            '            setDFValue(oForm, msRowTable, IDH_DISPROW._SampStatus, String.Empty)
                            '            IDHAddOns.idh.addon.Base.PARENT.doMessage("System could not generate the Sample Task successfully!")
                            '        End If
                            '    End If
                            'Else
                            '    IDHAddOns.idh.addon.Base.PARENT.doMessage("Can NOT generate sample in ADD mode!")
                            '    setDFValue(oForm, msRowTable, IDH_DISPROW._Sample, "N")
                            'End If
                            'Else
                            doSwitchCheckBox(oForm, sItemID, "S", True)
                            'End If
                            'If sItemID = "IDH_DOORD" OrElse _
                            '    sItemID = "IDH_DOARI" OrElse _
                            '    sItemID = "IDH_FOC" OrElse _
                            '    sItemID = "IDH_AINV" OrElse _
                            '    sItemID = "IDH_DOARIP" OrElse _
                            '    sItemID = "IDH_DOPO" Then
                            ''doSwitchCheckBox(oForm, sItemID, "S", True)
                            'End If
                        End If
                    End If
                End If
                'OnTime 32756: WR1_NewDev_Buying&Trading_Add functionality to log Activity with a WOR
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE Then
                'On Activate reloading the Activity Grid
                If pVal.BeforeAction = False Then
                    doLoadActivity(oForm, getDFValue(oForm, "@IDH_DISPORD", "Code"), getDFValue(oForm, msRowTable, "Code"))
                End If
            End If
            '            Catch ex As Exception
            '                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing the Item event.")
            '            Finally
            '                oForm.Freeze(False)
            '            End Try

            Return False
        End Function

        'Public Function doAddUpdateChequeNumber(ByVal oDialogForm As com.idh.forms.oo.Form) As Boolean

        'Public Sub doGetChequeNumber(ByVal oCaller As IDH_DISPROW, ByVal oPay As SAPbobsCOM.Payments)
        '    Dim oForm As com.idh.form.SBO.ChequeNumber = New com.idh.form.SBO.ChequeNumber()
        '    oForm.doShowInSBO()

        '    'Dim oForm As SAPbouiCOM.Form = oCaller.SBOForm
        '    'Dim oOOForm As com.uBC.forms.fr3.popup.ChequeNumber = New com.uBC.forms.fr3.popup.ChequeNumber(Nothing, oForm.UniqueID, Nothing)
        '    'oOOForm.DOR = oCaller
        '    'oOOForm.Payment = oPay
        '    ''oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doAddUpdateChequeNumber)
        '    ''oOOForm.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doCancelAddUpdateChequeNumber)
        '    'oOOForm.doShowModal(oForm)
        'End Sub

        Private Sub doRequestCIPSIPUpdate(ByVal oForm As SAPbouiCOM.Form, ByVal sMessId As String, ByVal oPriceType As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            If Not oDOR.IsLinkedToOrder() Then
                'Skip this if the User can not update the SIP or CIP
                If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_CIP_SIP_FROM_DO_WO) Then
                    If Config.INSTANCE.getParameterAsBool("ITMPUOK", True) Then
                        Dim oUpdateList As Hashtable = getWFValue(oForm, "PRICEUPDATELIST")
                        If oUpdateList Is Nothing Then
                            oUpdateList = New Hashtable()
                            oUpdateList.Add(oPriceType, sMessId)
                            setWFValue(oForm, "PRICEUPDATELIST", oUpdateList)
                        Else
                            If oUpdateList.Contains(oPriceType) = False Then
                                oUpdateList.Add(oPriceType, sMessId)
                            End If
                        End If
                    Else
                        doActualCIPSIPUpdateRequest(oForm, sMessId, oPriceType)
                    End If
                End If
            End If
        End Sub

        Private Function doRequestCIPSIPUpdateOnOK(ByVal oForm As SAPbouiCOM.Form) As Boolean
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_CIP_SIP_FROM_DO_WO) Then
                If Config.INSTANCE.getParameterAsBool("ITMPUOK", True) Then
                    Dim oUpdateList As Hashtable = getWFValue(oForm, "PRICEUPDATELIST")
                    Dim sMessId As String
                    If Not oUpdateList Is Nothing AndAlso oUpdateList.Count > 0 Then
                        For Each oPriceType As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes In oUpdateList.Keys
                            sMessId = oUpdateList.Item(oPriceType)
                            doActualCIPSIPUpdateRequest(oForm, sMessId, oPriceType)
                        Next
                        oUpdateList.Clear()
                        setWFValue(oForm, "PRICEUPDATELIST", Nothing)
                        Return True
                    End If
                End If
            End If
            Return False
        End Function

        Private Sub doActualCIPSIPUpdateRequest(ByVal oForm As SAPbouiCOM.Form, ByVal sMessId As String, ByVal oPriceType As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

            'Skip this if the User can not update the SIP or CIP
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_CIP_SIP_FROM_DO_WO) Then
                If Messages.INSTANCE.doResourceMessageYN(sMessId, New String() {WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.getPriceTypeName(oPriceType)}, 1) = 1 Then
                    'Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption = getWFValue(oForm, "PRCUPD")

                    Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption = doCreatePriceUpdateOptionForm(oForm)

                    'Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption = New WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption(Nothing, oForm, Nothing)
                    'oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogOkReturn(AddressOf doAddUpdatePrices)
                    'oOOForm.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogCancelReturn(AddressOf doCancelAddUpdatePrices)

                    'getWFValue(oForm, "PRCUPD")
                    oOOForm.PriceType = oPriceType
                    Dim oPriceLUP As Object = Nothing
                    If oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TChrg OrElse
                        oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.HChrg OrElse
                        oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TUOM Then
                        oPriceLUP = oDOR.CIP
                    ElseIf oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.DispCst OrElse
                        oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TUOM Then
                        oPriceLUP = oDOR.SIPT
                    ElseIf oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.HaulCst Then
                        oPriceLUP = oDOR.SIPH
                    ElseIf oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.ProCst OrElse
                        oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.PRUOM Then
                        oPriceLUP = oDOR.SIPP
                    End If

                    Dim sToDate As String = Config.Parameter("IPDFED")
                    If sToDate Is Nothing OrElse sToDate.Length = 0 OrElse sToDate.ToUpper().Equals("NOW") Then
                        sToDate = com.idh.utils.Dates.doDateToSBODateStr(DateTime.Now)
                    End If

                    'oOOForm.doShowModal(oForm)
                    Dim sFromDate As String = Config.Parameter("IPDFST")
                    If sFromDate Is Nothing OrElse sFromDate.Length = 0 OrElse sFromDate.ToUpper().Equals("NOW") Then
                        sFromDate = com.idh.utils.Dates.doDateToSBODateStr(DateTime.Now)
                    ElseIf sFromDate.ToUpper().Equals("DOC") Then
                        sFromDate = getDFValue(oForm, "@IDH_DISPORD", "U_BDate")
                    End If

                    If oPriceLUP.Count > 0 AndAlso Not oPriceLUP.Prices Is Nothing AndAlso oPriceLUP.Prices.Found Then
                        oOOForm.FromDate = sFromDate 'oPriceLUP.U_StDate_AsString
                        oOOForm.ToDate = sToDate 'oPriceLUP.U_EnDate_AsString

                        If oPriceLUP.U_StAddr.Length > 0 Then
                            oOOForm.AllSites = "N"
                        Else
                            oOOForm.AllSites = "Y"
                        End If
                    Else
                        oOOForm.FromDate = sFromDate
                        oOOForm.ToDate = sToDate
                        oOOForm.AllSites = "Y"
                    End If

                    oOOForm.doShowModal(oForm)
                End If
            End If
        End Sub

        Private Function doCreatePriceUpdateOptionForm(ByVal oForm As SAPbouiCOM.Form) As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption
            Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption = New WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption(Nothing, oForm, Nothing)
            oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doAddUpdatePrices)
            oOOForm.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doCancelAddUpdatePrices)
            Return oOOForm
        End Function

        Public Function doReturnFromAmend(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Dim oAmendForm As com.uBC.forms.fr3.popup.DOAmend = oOForm
            'If oAmendForm.DOR IsNot Nothing Then
            '    If oAmendForm.DOR.U_Status.StartsWith(FixedValues.getStatusPreFixReDo()) OrElse _
            '       oAmendForm.DOR.U_PStat.StartsWith(FixedValues.getStatusPreFixReDo()) Then


            '        Dim oDOR As IDH_DISPROW = thisDOR(oOForm.SBOForm)
            '        oDOR.setValue(IDH_DISPROW._Status, oAmendForm.DOR.U_Status, False)
            '        oDOR.setValue(IDH_DISPROW._PStat, oAmendForm.DOR.U_PStat, False)

            '        doSwitchToUpdate(oOForm.SBOForm)

            '    End If
            'End If

            'doLoadDataLocal(oOForm.SBOForm, False)
            setEnableItem(oOForm.SBOParentForm, False, "IDH_Amend")
            doLoadData(oOForm.SBOParentForm)
            'doSwitchToUpdate(oOForm.SBOForm)
            Return True
        End Function

        Public Function doAddUpdatePrices(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Dim oDOR As IDH_DISPROW = thisDOR(oOForm.SBOParentForm)

            Dim oPForm As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption = oOForm
            Dim sFromDate As String = oPForm.FromDate
            Dim sToDate As String = oPForm.ToDate
            Dim sAllSites As String = oPForm.AllSites

            Dim dFromDate As Date = com.idh.utils.Dates.doStrToDate(sFromDate)
            Dim dToDate As Date = com.idh.utils.Dates.doStrToDate(sToDate)

            Dim sBranch As String = ""
            Dim sJobType As String = ""
            Dim sZip As String = ""
            Dim sProCd As String = ""
            If oPForm.PriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TChrg OrElse
                oPForm.PriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.HChrg OrElse
                oPForm.PriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TUOM Then

                Dim oCIP As IDH_CSITPR = oDOR.CIP
                Dim dTPrice As Double = getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._TCharge)
                Dim dHPrice As Double = getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._CusChr)
                Dim sUOM As String = getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._UOM)

                If oCIP.LookedUp = False Then
                    oDOR.doLookupChargePrices()
                End If

                If oCIP.Count > 0 AndAlso Not oCIP.Prices Is Nothing AndAlso oCIP.Prices.Found Then
                    If Messages.INSTANCE.doResourceMessageOC("ASKCIPOV", New String() {WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.getPriceTypeName(oPForm.PriceType)}, 1) = 2 Then
                        Return False
                    End If
                    oCIP.U_EnDate = dFromDate.AddDays(-1)
                End If

                oCIP.doAddEmptyRow(True, True, False)

                'Dim sCardCode As String = getFormDFValue(oOForm.SBOParentForm, IDH_DISPORD._CardCd)
                'Dim sCardName As String = getFormDFValue(oOForm.SBOParentForm, IDH_DISPORD._CardNM)
                Dim sItemCode As String = getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._ItemCd)
                Dim sItemName As String = getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._ItemDsc)
                Dim sItemGrp As String = getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._ItmGrp)
                Dim sWastCd As String = getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._WasCd)
                Dim sWastName As String = getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._WasDsc)
                Dim sAddress As String = getWFValue(oOForm.SBOParentForm, "STADDR")

                sProCd = oDOR.U_ProCd 'getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._ProCd)
                sJobType = oDOR.U_JobTp 'getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._JobTp)
                sBranch = oDOR.U_Branch 'getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._Branch)
                'Dim sDocDate As String = getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._RDate)
                'Dim dDocDate As Date = com.idh.utils.dates.doStrToDate(sDocDate)

                oCIP.doApplyDefaults()

                oCIP.Code = ""
                oCIP.Name = ""
                If Config.ParameterAsBool("CIPCYCUS", True) Then
                    oCIP.U_CustCd = oDOR.U_CustCd
                Else
                    oCIP.U_CustCd = ""
                End If
                If Config.ParameterAsBool("PRCCYCNT", True) Then
                    oCIP.U_ItemCd = sItemCode
                    oCIP.U_ItemDs = sItemName
                Else
                    oCIP.U_ItemCd = ""
                    oCIP.U_ItemDs = ""
                End If




                oCIP.U_StDate = dFromDate
                oCIP.U_EnDate = dToDate

                oCIP.U_TipTon = dTPrice
                oCIP.U_Haulge = dHPrice

                oCIP.U_WasteCd = sWastCd
                oCIP.U_WasteDs = sWastName

                If sAllSites = "Y" Then
                    oCIP.U_StAddr = ""
                Else
                    oCIP.U_StAddr = sAddress
                End If
                If Config.ParameterAsBool("PRCCYOTP", False) Then ' Order Type
                    oCIP.U_JobTp = sJobType 'sJobType
                Else
                    oCIP.U_JobTp = "" 'sJobType
                End If

                Dim sWOOrd As String = getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._WROrd)
                Dim sWORow As String = getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._WRRow)
                If Not sWOOrd Is Nothing AndAlso sWOOrd.Length > 0 AndAlso
                    Not sWORow Is Nothing AndAlso sWORow.Length > 0 Then
                    oCIP.U_WR1ORD = "" '"WO"
                Else
                    If Config.ParameterAsBool("PRCCYWR1", True) Then
                        oCIP.U_WR1ORD = "DO"
                    Else
                        oCIP.U_WR1ORD = ""
                    End If
                End If

                oCIP.U_UOM = sUOM
                If Config.ParameterAsBool("PRCCYBRN", False) Then
                    oCIP.U_Branch = sBranch
                Else
                    oCIP.U_Branch = "" 'sBranch
                End If
                If Config.ParameterAsBool("CIPCYPRD", False) Then
                    oCIP.U_BP2CD = sProCd
                Else
                    oCIP.U_BP2CD = ""
                End If
                oCIP.U_ZpCd = "" 'sZip

                oCIP.doProcessData()
            ElseIf oPForm.PriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.DispCst OrElse
                oPForm.PriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TUOM Then

                Dim oSIPT As IDH_SUITPR = oDOR.SIPT
                Dim dTPrice As Double = getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._TipCost)
                Dim sUOM As String = getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._PUOM)

                If oSIPT.LookedUp = False Then
                    oDOR.doLookupTipCostPrices()
                End If

                If oSIPT.Count > 0 AndAlso Not oSIPT.Prices Is Nothing AndAlso oSIPT.Prices.Found Then
                    If Messages.INSTANCE.doResourceMessageOC("ASKSIPOV", New String() {WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.getPriceTypeName(oPForm.PriceType)}, 1) = 2 Then
                        Return False
                    End If
                    oSIPT.U_EnDate = dFromDate.AddDays(-1)

                End If
                Dim sSuppCd As String = getFormDFValue(oOForm.SBOParentForm, IDH_DISPORD._SCardCd)
                doUpdateSIPDataBuffer(oOForm.SBOParentForm, sSuppCd,
                                     sFromDate, sToDate, sAllSites,
                                     oSIPT, dTPrice, 0, sUOM)

            ElseIf oPForm.PriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.HaulCst Then

                Dim oSIPH As IDH_SUITPR = oDOR.SIPH
                Dim dHPrice As Double = getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._OrdCost)
                Dim sUOM As String = getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._PUOM)

                If oSIPH.LookedUp = False Then
                    oDOR.doLookupHaulageCostPrice()
                End If

                If oSIPH.Count > 0 AndAlso Not oSIPH.Prices Is Nothing AndAlso oSIPH.Prices.Found Then
                    If Messages.INSTANCE.doResourceMessageOC("ASKSIPOV", New String() {WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.getPriceTypeName(oPForm.PriceType)}, 1) = 2 Then
                        Return False
                    End If
                    oSIPH.U_EnDate = dFromDate.AddDays(-1)
                End If
                Dim sSuppCd As String = getFormDFValue(oOForm.SBOParentForm, IDH_DISPORD._CCardCd)
                doUpdateSIPDataBuffer(oOForm.SBOParentForm, sSuppCd,
                                     sFromDate, sToDate, sAllSites,
                                     oSIPH, 0, dHPrice, sUOM)
            ElseIf oPForm.PriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.ProCst OrElse
                oPForm.PriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.PRUOM Then

                Dim oSIPP As IDH_SUITPR = oDOR.SIPP
                Dim dPPrice As Double = getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._PCost)
                Dim sUOM As String = getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._ProUOM)

                If oSIPP.LookedUp = False Then
                    oDOR.doLookupProducerCostPrices()
                End If

                If oSIPP.Count > 0 AndAlso Not oSIPP.Prices Is Nothing AndAlso oSIPP.Prices.Found Then
                    If Messages.INSTANCE.doResourceMessageOC("ASKSIPOV", New String() {WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.getPriceTypeName(oPForm.PriceType)}, 1) = 2 Then
                        Return False
                    End If
                    oSIPP.U_EnDate = dFromDate.AddDays(-1)
                End If
                Dim sSuppCd As String = getDFValue(oOForm.SBOParentForm, msRowTable, IDH_DISPROW._ProCd)
                doUpdateSIPDataBuffer(oOForm.SBOParentForm, sSuppCd,
                                     sFromDate, sToDate, sAllSites,
                                     oSIPP, dPPrice, 0, sUOM)
            End If
            Return True
        End Function

        Private Sub doUpdateSIPDataBuffer(ByVal oForm As SAPbouiCOM.Form,
                                         ByVal sSuppCd As String,
                                         ByVal sFromDate As String, ByVal sToDate As String, ByVal sAllSites As String,
                                         ByVal oSIP As IDH_SUITPR, ByVal dTPrice As Double, ByVal dHPrice As Double,
                                         ByVal sUOM As String)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

            Dim sBranch As String = ""
            Dim sJobType As String = ""
            Dim sZip As String = ""

            If oSIP.Count > 0 AndAlso Not oSIP.Prices Is Nothing AndAlso oSIP.Prices.Found Then
                sBranch = oSIP.U_Branch
                sJobType = oSIP.U_JobTp
                sZip = oSIP.U_ZpCd
                If sUOM Is Nothing OrElse sUOM.Length = 0 Then
                    sUOM = oSIP.U_UOM
                End If
            End If

            oSIP.doAddEmptyRow(True, True, False)

            'Dim sCustCd As String = getDFValue(oForm, msRowTable, IDH_DISPROW._CustCd)
            'Dim sCardName As String = getDFValue(oForm, msRowTable, IDH_DISPROW._CustNm)
            Dim sItemCode As String = getDFValue(oForm, msRowTable, IDH_DISPROW._ItemCd)
            Dim sItemName As String = getDFValue(oForm, msRowTable, IDH_DISPROW._ItemDsc)
            Dim sItemGrp As String = getDFValue(oForm, msRowTable, IDH_DISPROW._ItmGrp)
            Dim sWastCd As String = getDFValue(oForm, msRowTable, IDH_DISPROW._WasCd)
            Dim sWastName As String = getDFValue(oForm, msRowTable, IDH_DISPROW._WasDsc)
            'Dim sDocDate As String = getDFValue(oForm, msRowTable, IDH_DISPROW._RDate)
            'Dim dDocDate As Date = com.idh.utils.dates.doStrToDate(sDocDate)
            Dim dFromDate As Date = com.idh.utils.Dates.doStrToDate(sFromDate)
            Dim dToDate As Date = com.idh.utils.Dates.doStrToDate(sToDate)
            Dim sAddress As String = getWFValue(oForm, "STADDR")

            sJobType = oDOR.U_JobTp ' getDFValue(oForm.SBOParentForm, msRowTable, IDH_DISPROW._JobTp)
            sBranch = oDOR.U_Branch 'getDFValue(oForm.SBOParentForm, msRowTable, IDH_DISPROW._Branch)

            oSIP.doApplyDefaults()

            oSIP.Code = ""
            oSIP.Name = ""
            If Config.ParameterAsBool("SIPCYPRD", True) Then
                oSIP.U_CardCd = sSuppCd
            Else
                oSIP.U_CardCd = ""
            End If
            If Config.ParameterAsBool("SIPCYCUS", True) Then
                oSIP.U_BP2CD = oDOR.U_CustCd
            Else
                oSIP.U_BP2CD = ""
            End If
            If Config.ParameterAsBool("PRCCYCNT", True) Then
                oSIP.U_ItemCd = sItemCode
                oSIP.U_ItemDs = sItemName
            Else
                oSIP.U_ItemCd = ""
                oSIP.U_ItemDs = ""
            End If

            oSIP.U_StDate = dFromDate
            oSIP.U_EnDate = dToDate
            oSIP.U_TipTon = dTPrice
            oSIP.U_Haulge = dHPrice

            oSIP.U_WasteCd = sWastCd
            oSIP.U_WasteDs = sWastName

            If sAllSites = "Y" Then
                oSIP.U_StAddr = ""
            Else
                oSIP.U_StAddr = sAddress
            End If

            If Config.ParameterAsBool("PRCCYOTP", False) Then ' Order Type
                oSIP.U_JobTp = sJobType
            Else
                oSIP.U_JobTp = "" 'sJobType
            End If
            If Config.ParameterAsBool("PRCCYWR1", True) Then
                oSIP.U_WR1ORD = "WO"
            Else
                oSIP.U_WR1ORD = ""
            End If

            oSIP.U_UOM = sUOM
            If Config.ParameterAsBool("PRCCYBRN", False) Then
                oSIP.U_Branch = sBranch
            Else
                oSIP.U_Branch = "" 'sBranch
            End If

            oSIP.U_ZpCd = "" 'sZip

            oSIP.doProcessData()
        End Sub

        Public Function doCancelAddUpdatePrices(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Dim oDOR As IDH_DISPROW = thisDOR(oOForm.SBOForm)
            If Not oDOR.IsLinkedToOrder() Then
                Dim oPForm As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption = oOForm
                Dim oPriceType As WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes = oPForm.PriceType
                If oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TChrg OrElse
                    oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.HChrg OrElse
                    oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TUOM Then
                    thisDOR(oPForm.SBOParentForm).doGetTipCostPrice(True)
                ElseIf oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.DispCst OrElse
                    oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TUOM Then
                    thisDOR(oPForm.SBOParentForm).doGetTipCostPrice(True)
                ElseIf oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.HaulCst Then
                    thisDOR(oPForm.SBOParentForm).doGetHaulageCostPrice(True)
                ElseIf oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.ProCst OrElse
                    oPriceType = WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption.en_PriceTypes.PRUOM Then
                    thisDOR(oPForm.SBOParentForm).doGetProducerCostPrice(True)
                End If
            End If
            Return True
        End Function

        Private Function doCheckAndAssignRowNumber(ByVal oForm As SAPbouiCOM.Form) As String
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            'Dim sRowID As String = getDFValue(oForm, msRowTable, "Code")
            Dim sRowID As String = oDOR.Code 'getDFValue(oForm, msRowTable, "Code")

            If sRowID = "-1" Then
                Dim oUpdateGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")

                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(IDH_DISPROW.AUTONUMPREFIX, "DISPROW")
                oUpdateGrid.setCurrentDataRowIndex(oUpdateGrid.getLastRowIndex())
                oUpdateGrid.doSetFieldValue("Code", oNumbers.CodeCode)
                oUpdateGrid.doSetFieldValue("Name", oNumbers.NameCode)
                'setDFValue(oForm, msRowTable, "Code", oNumbers.CodeCode)
                oDOR.Code = oNumbers.CodeCode
                oDOR.Name = oNumbers.NameCode
                ''SV 20/03/2015 #587
                sRowID = oNumbers.CodeCode
                ''End
            End If
            Return sRowID
        End Function

        Private Sub doChooseComplianceScheme(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean)
            setSharedData(oForm, "TRG", "CS")

            If Config.INSTANCE.getParameterAsBool(msOrderType & "BPFLT", True) = False Then
                'setSharedData(oForm, "IDH_BPCOD", "")
                setSharedData(oForm, "IDH_NAME", "")
                setSharedData(oForm, "IDH_TYPE", "")
                setSharedData(oForm, "IDH_GROUP", "")
                setSharedData(oForm, "IDH_BRANCH", "")
            Else
                'setSharedData(oForm, "IDH_NAME", sBPName)
                'setSharedData(oForm, "IDH_TYPE", "F-S")
                'setSharedData(oForm, "IDH_TYPE", sType)
                'setSharedData(oForm, "IDH_GROUP", "")
            End If
            setSharedData(oForm, "IDH_CS", "Y")
            setSharedData(oForm, "IDH_BPCOD", "")
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub

        '       	Protected Overridable Sub doChooseRowProducer(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean)
        '            setSharedData(oForm, "TRG", "RPRO")
        '
        '            If Config.INSTANCE.getParameterAsBool( msOrderType & "BPFLT", True) = False Then
        '	            'setSharedData(oForm, "IDH_BPCOD", "")
        '	            'setSharedData(oForm, "IDH_NAME", "")
        '    	        setSharedData(oForm, "IDH_TYPE", "")
        '        	    setSharedData(oForm, "IDH_GROUP", "")
        '        	    setSharedData(oForm, "IDH_BRANCH", "")
        '        	Else
        '            	setSharedData(oForm, "IDH_TYPE", "F-S")
        '            End If
        '            setSharedData(oForm, "IDH_NAME", getDFValue(oForm, msRowTable, "U_ProNm"))
        '            setSharedData(oForm, "IDH_BPCOD", getDFValue(oForm, msRowTable, "U_ProCd"))
        '            If bCanDoSilent = True Then
        '                setSharedData(oForm, "SILENT", "SHOWMULTI")
        '            End If
        '            goParent.doOpenModalForm("IDHCSRCH", oForm)
        '        End Sub

        Protected Overridable Sub doConvReport(ByVal oForm As SAPbouiCOM.Form)
            Dim sRowID As String = getDFValue(oForm, msRowTable, "Code").Trim()
            Dim sCode As String = getDFValue(oForm, "@IDH_DISPORD", "Code").Trim()

            If sRowID.Trim().Length > 0 Then
                'Check for in or out
                Dim sReportFile As String

                If Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)) Then
                    'If getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp).Equals(Config.INSTANCE.doGetJopTypeIncoming()) Then

                    sReportFile = Config.Parameter("MDCREPI")
                    If sReportFile Is Nothing Then
                        sReportFile = "WasteTransferNoteGoodsINv1.1.rpt"
                    End If
                Else

                    sReportFile = Config.Parameter("MDCREPO")
                    If sReportFile Is Nothing Then
                        sReportFile = "WasteTransferNoteGoodsOUTv1.0.rpt"
                    End If
                End If

                Dim oParams As New Hashtable
                oParams.Add("OrdNum", sCode)
                oParams.Add("RowNum", sRowID)

                setSharedData(oForm, "DORELOAD", "FALSE")

                If (Config.INSTANCE.useNewReportViewer()) Then
                    IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                    IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                    IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
                Else
                    IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
                End If
            End If
        End Sub

        Protected Overridable Sub doDocReport(ByVal oForm As SAPbouiCOM.Form)
            Dim sRowCode As String = getDFValue(oForm, msRowTable, "Code")

            '            Dim oReport As idh.report.DocCon = New idh.report.DocCon(goParent, oForm, "DO", gsType, True)
            '			If oReport.doDocReport(sRowCode) = True AndAlso oReport.getLastConsignmentNumber().Length > 0 Then
            '				setDFValue(oForm, msRowTable, "U_ConNum", oReport.getLastConsignmentNumber())
            '			End If

            'Dim oReport As DocCon = New DocCon(oForm, "DO", gsType, True )
            'If oReport.doDocReport(sRowCode) = True AndAlso oReport.LastConsignmentNumber.Length > 0 Then
            '	setDFValue(oForm, msRowTable, "U_ConNum", oReport.LastConsignmentNumber)
            'End If

            'OnTime Ticket 25926 - DOC Report crahses application
            'if report starts with http display PDF report in web browser control
            Dim sReportFile As String

            sReportFile = Config.Parameter("WDOCDOC")
            If sReportFile.StartsWith("http://") Then
                'Start ISB Code
                Dim oParams As New Hashtable
                oParams.Add("RowNum", sRowCode)

                'implementation of doCallReportDefaults
                doCallHTTPRpts(goParent, oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType, Config.INSTANCE.doGetBranch(goParent.gsUserName))
                'End ISB Code
            Else
                Dim oReport As DocCon = New DocCon(Me, oForm, "DO", gsType, True)
                oReport.doDocConReport(sRowCode)
                doAfterConsignmentReport(oForm, oReport)
                oReport = Nothing
            End If

        End Sub

        Public Sub doAfterConsignmentReport(ByVal oForm As SAPbouiCOM.Form, ByVal oReport As DocCon)
            'Prompt for the JJK Number if it is an USA version
            Dim bISUSARelease As Boolean = Config.INSTANCE.getParameterAsBool("USAREL", False)
            If bISUSARelease = True Then
                If Not oReport.ConNumbers Is Nothing AndAlso oReport.ConNumbers.Count > 0 Then
                    For Each sConNum As String In oReport.ConNumbers
                        Dim oOOForm As WR1_FR2Forms.idh.forms.fr2.JJKNumber = New WR1_FR2Forms.idh.forms.fr2.JJKNumber(Nothing, oForm, Nothing)
                        oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doReturnOKFromJJK)
                        oOOForm.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doReturnCancelFromJJK)
                        'oOOForm.RowCodes = oReport.ConRowsToUpdate
                        oOOForm.TempConNum = sConNum 'oReport.LastConsignmentNumber
                        oOOForm.RowTable = "@IDH_DISPROW"
                        oOOForm.doShowModal()
                    Next
                End If
            Else
                If oReport.HasNewConsignmentNote AndAlso oReport.LastConsignmentNumber.Length > 0 Then
                    setDFValue(oForm, msRowTable, "U_ConNum", oReport.LastConsignmentNumber)
                End If
            End If
        End Sub

        Public Function doReturnOKFromJJK(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Dim oJJKForm As WR1_FR2Forms.idh.forms.fr2.JJKNumber = oOForm
            setDFValue(oOForm.SBOParentForm, msRowTable, "U_ConNum", oJJKForm.JJKNumber)
            Return True
        End Function

        Public Function doReturnCancelFromJJK(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Dim oJJKForm As WR1_FR2Forms.idh.forms.fr2.JJKNumber = oOForm
            setDFValue(oOForm.SBOParentForm, msRowTable, "U_ConNum", oJJKForm.TempConNum)
            Return True
        End Function

        '        Protected Overridable Sub doDocReport(ByVal oForm As SAPbouiCOM.Form)
        '            Dim sRowID As String = getDFValue(oForm, msRowTable, "Code")
        '            Dim sCode As String = getDFValue(oForm, "@IDH_DISPORD", "Code")
        '
        '            If sRowID.Trim().Length > 0 Then
        '                Dim sReportFile As String
        '                'Dim sCardCode As String = getDFValue(oForm, msRowTable, "U_CustCd")
        '                Dim sCardCode As String = getDFValue(oForm, "@IDH_DISPORD", "U_CardCd")
        '                Dim sWasteCode As String = getDFValue(oForm, msRowTable, "U_WasCd")
        '
        '                Dim oData As ArrayList = Config.INSTANCE.doGetDOCReportLevel1a(goParent, "DO", sCardCode, sWasteCode)
        '                sReportFile = oData.Item(0)
        '
        '        		If oData.Item(1) = True Then
        '        			Dim sConNo As String = getDFValue(oForm, msRowTable, "U_ConNum")
        '					If sConNo Is Nothing OrElse sConNo.Length = 0 Then
        '                		Dim sPremCode As String = ""
        '						Dim iConNum As Integer = goParent.goDB.doNextNumber("CONNUM")
        '						Dim sAddr As String
        '						Dim oAddData As ArrayList
        '
        '						Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)
        '						If sJobType.Equals(Config.INSTANCE.doGetJopTypeOutgoing()) Then
        '			                Dim sProducerCode As String = getDFValue(oForm, "@IDH_DISPORD", "U_PCardCd")
        '			                sAddr = getDFValue(oForm, "@IDH_DISPORD", "U_PAddress")
        '							oAddData = SRCH.idh.forms.search.AddressSearch.doGetAddress(goParent, sProducerCode, "S", sAddr)
        '            			Else
        '							sAddr = getDFValue(oForm, "@IDH_DISPORD", "U_Address")
        '							oAddData = SRCH.idh.forms.search.AddressSearch.doGetAddress(goParent, sCardCode, "S", sAddr)
        '						End If
        '
        '                		If Not oAddData Is Nothing AndAlso oAddData.Count > 0 Then
        '                			sPremCode = oAddData.Item(18)
        '                		End If
        '						If sPremCode Is Nothing OrElse sPremCode.Length = 0 Then
        '                			sConNo = "EXEXXX" & "/" & iConNum
        '                		Else
        '                			sConNo = sPremCode & "/" & iConNum
        '                		End If
        '
        '						goParent.goDB.doUpdateQuery("UPDATE [@IDH_DISPROW] Set U_ConNum = '" & sConNo & "' WHERE Code = '" & sRowID & "'")
        '						setDFValue(oForm, msRowTable, "U_ConNum", sConNo)
        '						'sRowID = sConNo
        '					End If
        '					sRowID = sConNo
        '				End If
        '
        '                Dim oParams As New Hashtable
        '               'oParams.Add("OrdNum", sCode)
        '                oParams.Add("RowNum", sRowID)
        '                setSharedData(oForm, "DORELOAD", "FALSE")
        '                'idh.report.Base.doCallReport(goParent, oForm, sReportFile, "TRUE", "FALSE", "TRUE", "1", oParams)
        '                idh.report.Base.doCallReportDefaults( oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType, Config.INSTANCE.doGetBranch(goParent))
        '            End If
        '        End Sub

        Protected Overrides Function doFind(ByVal oForm As SAPbouiCOM.Form, ByRef sSwitch As String) As Boolean
            Try
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    Dim sOrder As String = getFormDFValue(oForm, "Code", True)
                    Dim sCustomer As String = getFormDFValue(oForm, "U_CardCd", True)
                    Dim sReg As String = getDFValue(oForm, msRowTable, "U_Lorry", True)
                    Dim sVehCode As String = getDFValue(oForm, msRowTable, "U_LorryCd")
                    Dim sJbTp As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, True)
                    Return doFindInternal(oForm, sSwitch, sOrder, sCustomer, sReg, sVehCode, sJbTp)
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error looking up the DO.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXLUDO", {Nothing})
            End Try
            Return False
        End Function

        'Find the record containing the Reg, Jobtype and Order number that does not have a second weigh
        'If no record found go into a add mode
        'Returns True if it is a second weigh Job else False
        Protected Function doFindInternal(ByVal oForm As SAPbouiCOM.Form,
          ByRef sSwitch As String,
          ByVal sOrder As String,
          ByVal sCustomer As String,
          ByVal sReg As String,
          ByVal sVehCode As String,
          ByVal sJbTp As String) As Boolean
            oForm.Freeze(True)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing

            Try
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    Dim sQry As String

                    If sReg.Length = 0 AndAlso sOrder.Length = 0 AndAlso sCustomer.Length = 0 Then
                        Return False
                    End If

                    sQry = "SELECT Top 1 l.U_JobNr, l.Code from [@IDH_DISPROW] l, [@IDH_DISPORD] h Where h.Code = l.U_JobNr And l.U_RowSta<>'Deleted' "
                    If sOrder.Length() > 0 Then
                        sQry = sQry & " AND l.U_JobNr = '" & sOrder & "'"
                    Else
                        If sVehCode.Length() > 0 Then
                            sQry = sQry & " AND l.U_LorryCd = '" & sVehCode & "'"
                        ElseIf sReg.Length() > 0 Then
                            sQry = sQry & " AND l.U_Lorry = '" & IDHAddOns.idh.data.Base.doFixForSQL(sReg) & "'"
                        Else
                            Return False
                        End If

                        sQry = sQry & " AND ((l.U_Wei2 = 0 AND l.U_Wei1 != 0) Or (l.U_Wei1 = 0 AND l.U_Wei2 != 0)) AND " &
                            " U_CstWgt = 0 AND l.U_Status != '" & com.idh.bridge.lookups.FixedValues.getStatusOrdered() & "' AND " &
                            " l.U_Status != '" & com.idh.bridge.lookups.FixedValues.getStatusInvoiced() & "' AND " &
                            " l.U_Status != '" & com.idh.bridge.lookups.FixedValues.getStatusFoc() & "' "

                        If Config.INSTANCE.getParameterAsBool("DOFVPJ") = True Then
                            If sJbTp.Length() > 0 Then
                                sQry = sQry & " AND l.U_JobTp = '" & sJbTp & "'"
                            End If
                        End If

                        If sCustomer.Length() > 0 Then
                            sCustomer = sCustomer.Replace("*", "%")
                            sQry = sQry & " AND h.U_CardCd LIKE '" & sCustomer & "%'"
                        End If
                    End If

                    sQry = sQry & " Order by CAST(l.Name As Numeric) DESC"
                    oRecordSet = goParent.goDB.doSelectQuery(sQry)
                    If oRecordSet.RecordCount > 0 Then
                        'This indicates a second weigh and will load the first weigh job
                        sOrder = oRecordSet.Fields.Item("U_JobNr").Value.Trim()
                        Dim sRow As String = oRecordSet.Fields.Item("Code").Value.Trim()
                        doLoad2ndWeigh(oForm, sOrder, sRow)
                        Return True
                    Else
                        If sOrder.Length() = 0 Then
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                            setFormDFValue(oForm, "Code", "")
                            doLoadDataLocal(oForm, False)

                            setDFValue(oForm, msRowTable, IDH_DISPROW._Lorry, sReg)
                            setDFValue(oForm, msRowTable, "U_LorryCd", sVehCode)
                            setDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, sJbTp)

                            ''MA Start 04-02-2015 Issue#554
                            Dim dTares() As Object = Config.INSTANCE.doGetLorryTareWeightsByRegistration(sReg)
                            setUFValue(oForm, "IDH_TARWEI", dTares(0))
                            setUFValue(oForm, "IDH_TRLTar", dTares(1))
                            setUFValue(oForm, "IDH_TRWTE1", dTares(2))
                            setUFValue(oForm, "IDH_TRWTE2", dTares(3))
                            ''MA Start 04-02-2015 Issue#554
                            oForm.Title = getTranslatedCaption("#TITLE", "Disposal Order") & " - " & getTranslatedWord("First Weigh")
                            doSetEnabled(oForm.Items.Item("IDH_BOOREF"), False)
                        Else
                            'com.idh.bridge.DataHandler.INSTANCE.doError("No Records were found: " & sOrder)
                            com.idh.bridge.DataHandler.INSTANCE.doResSystemError("No Records were found: " & sOrder, "ERSYDBNF", {sOrder})
                        End If
                        Return False
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error finding DO.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "ERSYFDO", {Nothing})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
                oForm.Freeze(False)
            End Try
            Return False
        End Function

        Private Sub doConfigWeighBridge(ByVal oForm As SAPbouiCOM.Form, ByVal sId As String)
            msLastWeighBridgeId = sId

            Dim oWBridge As Weighbridge = getWFValue(oForm, "WBridge")
            If sId.Equals("-1") = True Then
                If Not oWBridge Is Nothing Then
                    oWBridge.doClose()
                    oWBridge = Nothing
                End If

                setWFValue(oForm, "WBridge", Nothing)
                Exit Sub
            End If

            Dim bDoConfigure As Boolean = False
            If oWBridge Is Nothing Then
                oWBridge = New Weighbridge(sId)

                setWFValue(oForm, "WBridge", oWBridge)

                bDoConfigure = True
            ElseIf oWBridge.getWBId().Equals(sId) = False Then
                oWBridge.doClose()
                oWBridge.setWBId(sId)

                bDoConfigure = True
            End If

            doSetWeighBridgeLogo(oForm)

            If bDoConfigure = True Then
                Dim sReadOnceCmd As String = Config.Parameter("WBFR" & sId)
                Dim sReadContinuesCmd As String = Config.Parameter("WBCR" & sId)

                Dim iWBFRSS As Integer = ToInt(Config.Parameter("WBFRSS" & sId))
                Dim iWBFRSL As Integer = ToInt(Config.Parameter("WBFRSL" & sId))

                Dim iWBFRWS As Integer = ToInt(Config.Parameter("WBFRWS" & sId))
                Dim iWBFRWL As Integer = ToInt(Config.Parameter("WBFRWL" & sId))

                Dim iWBCRWS As Integer = ToInt(Config.Parameter("WBCRWS" & sId))
                Dim iWBCRWL As Integer = ToInt(Config.Parameter("WBCRWL" & sId))

                Dim sPort As String = Config.Parameter("WBPORT" & sId)
                Dim sBaud As String = Config.Parameter("WBBAUD" & sId)
                Dim sDataBit As String = Config.Parameter("WBDBIT" & sId)

                'N-None, O-Odd, E-Even, M-Mark
                Dim sParity As String = Config.Parameter("WBPARI" & sId)
                Dim sStop As String = Config.Parameter("WBSTOP" & sId)
                Dim sFlowControl As String = Config.Parameter("WBFLOW" & sId)
                Dim sInMode As String = Config.Parameter("WBIMOD" & sId)
                Dim sWBDelay As String = Config.Parameter("WBDELA" & sId)

                If sParity Is Nothing OrElse sParity.Length = 0 Then
                    sParity = "N"
                End If

                Dim iWBDELA As Integer
                If sWBDelay Is Nothing OrElse sWBDelay.Length = 0 Then
                    iWBDELA = 0
                Else
                    iWBDELA = Int32.Parse(sWBDelay)
                End If

                oWBridge.doConfigPort(Int32.Parse(sPort), Int32.Parse(sBaud), Int32.Parse(sDataBit), sParity.Chars(0), Int32.Parse(sStop), sFlowControl)
                oWBridge.setInMode(sInMode)
                oWBridge.setReadDelay(iWBDELA)
                oWBridge.doConfigReadCointinues(sReadContinuesCmd, iWBCRWS, iWBCRWL)
                oWBridge.doConfigReadOnce(sReadOnceCmd, iWBFRWS, iWBFRWL, iWBFRSS, iWBFRSL)
            End If
        End Sub

        Private Sub doReadWeight(ByVal oForm As SAPbouiCOM.Form, ByVal sReadType As String)
            Dim sWeight As String
            Dim oWBridge As Weighbridge = getWFValue(oForm, "WBridge")
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

            If oWBridge Is Nothing Then
                If msLastWeighBridgeId.Equals("-1") = True Then
                    Exit Sub
                End If

                doConfigWeighBridge(oForm, msLastWeighBridgeId)
                If oWBridge Is Nothing Then
                    Exit Sub
                End If
            End If

            Try
                oWBridge.doOpen()

                If sReadType = "CR" Then
                    If oWBridge.doReadContinues() = False Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError(sReadType & ": " & oWBridge.getError()) 'oWBridge.getErrorCode())
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError(sReadType & ": " & oWBridge.getError(), "ERSYGEN", {com.idh.bridge.Translation.getTranslatedWord(sReadType) &
                                                     ": " & com.idh.bridge.Translation.getTranslatedWord(oWBridge.getError())})
                    End If
                    sWeight = oWBridge.getWeight().ToString()
                    setUFValue(oForm, "IDH_WEIG", sWeight)
                ElseIf sReadType = "ACC1" Then
                    If oWBridge.doReadOnce() = False Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError(sReadType & ": " & oWBridge.getError())
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError(sReadType & ": " & oWBridge.getError(), "ERSYGEN", {com.idh.bridge.Translation.getTranslatedWord(sReadType) &
                                                                              ": " & com.idh.bridge.Translation.getTranslatedWord(oWBridge.getError())})
                    End If
                    sWeight = oWBridge.getWeight().ToString()
                    setUFValue(oForm, "IDH_WEIG", sWeight)

                    oDOR.U_Ser1 = oWBridge.getSerial()
                    oDOR.U_WDt1 = DateTime.Now
                    oDOR.U_Wei1 = oWBridge.getWeight()

                    doSetUpdate(oForm)
                ElseIf sReadType = "ACC2" Then
                    If oWBridge.doReadOnce() = False Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError(sReadType & ": " & oWBridge.getError())
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError(sReadType & ": " & oWBridge.getError(), "ERSYGEN", {com.idh.bridge.Translation.getTranslatedWord(sReadType) &
                                                                             ": " & com.idh.bridge.Translation.getTranslatedWord(oWBridge.getError())})
                    End If
                    sWeight = oWBridge.getWeight().ToString()
                    setUFValue(oForm, "IDH_WEIG", sWeight)

                    oDOR.U_Ser2 = oWBridge.getSerial()
                    oDOR.U_WDt2 = DateTime.Now
                    oDOR.U_Wei2 = oWBridge.getWeight()

                    doSetUpdate(oForm)
                ElseIf sReadType = "ACCB" Then
                    If oWBridge.doReadOnce() = False Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError(sReadType & ": " & oWBridge.getError())
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError(sReadType & ": " & oWBridge.getError(), "ERSYGEN", {com.idh.bridge.Translation.getTranslatedWord(sReadType) &
                                                                             ": " & com.idh.bridge.Translation.getTranslatedWord(oWBridge.getError())})
                    End If
                    sWeight = oWBridge.getWeight().ToString()
                    setUFValue(oForm, "IDH_SERB", oWBridge.getSerial())

                    setUFValue(oForm, "IDH_WEIG", sWeight)
                    setUFValue(oForm, "IDH_WEIB", sWeight)

                    setUFValue(oForm, "IDH_WDTB", goParent.doDateToStr())
                End If
            Catch Ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & Ex.ToString, "Error reading weight.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EREXRWGT", {Nothing})
                oWBridge.doClose()
                Exit Sub
            Finally
                oWBridge.doClose()
            End Try
        End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            oForm.Freeze(True)

            Try
                MyBase.doHandleModalResultShared(oForm, sModalFormType, sLastButton)

                If sModalFormType = "IDHASRCH" Then
                    oDOR.doGetAllUOM()
                ElseIf sModalFormType = "IDHCSRCH" Then
                    Dim sTarget As String = getSharedData(oForm, "TRG")
                    Dim val As String = getSharedData(oForm, "CARDCODE")
                    If sTarget = "CS" Then
                        setDFValue(oForm, msRowTable, "U_CustCs", val)
                    End If
                ElseIf sModalFormType = "IDHWISRC" Then
                    setDFValueFromUser(oForm, msRowTable, "U_WasCd", getSharedData(oForm, "ITEMCODE"))
                    setDFValueFromUser(oForm, msRowTable, "U_WasDsc", getSharedData(oForm, "ITEMNAME"), False, True)
                    setDFValueFromUser(oForm, msRowTable, "U_AUOM", getSharedData(oForm, "UOM"))

                    Dim sCWBB As String = getSharedData(oForm, "CWBB")
                    If (sCWBB = "Y" OrElse sCWBB = "y") _
                     AndAlso Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, True)) Then
                        'AndAlso getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, True).Equals(Config.INSTANCE.doGetJopTypeIncoming()) Then
                        doSwitchToPO(oForm, True)

                        Dim sPUOM As String = getDFValue(oForm, msRowTable, "U_ProUOM")
                        If sPUOM.Length = 0 Then
                            sPUOM = getSharedData(oForm, "PUOM")
                            If Not sPUOM Is Nothing AndAlso sPUOM.Length > 0 Then
                                setDFValueFromUser(oForm, msRowTable, "U_ProUOM", sPUOM)
                            End If
                        End If
                    Else
                        doSwitchToSO(oForm)
                    End If
                    ''MA Start 07-04-2016 Issue#1111
                    oDOR.doCheckAndSetExpectedLoadWeight()
                    ''MA End 07-04-2016 Issue#1111

                    'If oDOR.U_UseWgt = "1" Then
                    oDOR.BlockChangeNotif = True

                    Try
                        oDOR.doGetAllUOM(True)
                    Catch e As Exception

                    End Try
                    oDOR.BlockChangeNotif = False
                    'End If

                    'UPDATE THE ADDITIONAL GRID FROM HERE FOR NOW - IN FUTURE THE DBOBJECT WILL BE USED
                    doRefreshAdditionalPrices(oForm)

                    doAddAutoAdditionalCodes(oForm)

                    oDOR.doGetChargePrices(True)
                    oDOR.doGetCostPrices(True, True, True, True)

                    If oDOR.U_WasCd.Length > 0 Then
                        doFillAlternateItemDescription(oForm)
                    End If

                    'getAutoAdditionalCodes(oForm)
                    'getAutoDeductioCodes(oForm)
                    doSwitchGenButton(oForm)
                    doSetUpdate(oForm)
                ElseIf sModalFormType = "IDHISRC" Then
                    If getSharedData(oForm, "TRG") = "PROD" Then
                        oDOR.U_ItemCd = getSharedData(oForm, "ITEMCODE")
                        oDOR.U_ItemDsc = getSharedData(oForm, "ITEMNAME")

                        Dim sItemGroupCode As String = getSharedData(oForm, "ITMSGRPCOD")
                        setDFValue(oForm, "@IDH_DISPORD", "U_ItemGrp", sItemGroupCode)
                        oDOR.U_ItmGrp = sItemGroupCode

                        'setDFValueFromUser(oForm, msRowTable, "U_ItemCd", getSharedData(oForm, "ITEMCODE"), False, True)
                        'setDFValueFromUser(oForm, msRowTable, "U_ItemDsc", getSharedData(oForm, "ITEMNAME"))

                        'oDOR.doGetAllUOM()

                        'getAutoAdditionalCodes(oForm)

                        'UPDATE THE ADDITIONAL GRID FROM HERE FOR NOW - IN FUTURE THE DBOBJECT WILL BE USED
                        doRefreshAdditionalPrices(oForm)

                        doAddAutoAdditionalCodes(oForm)

                    End If
                ElseIf sModalFormType = "IDHLNKWO" Then
                    Dim bContinue As Boolean = True

                    'This is now done in the Search form
                    '                    If sLastButton.Equals("IDH_CREATE") Then
                    '                    	Dim sWOH As String = getDFValue(oForm, msRowTable, "U_WROrd", True).Trim()
                    '                    	Dim sWOR As String = getDFValue(oForm, msRowTable, "U_WRRow", True).Trim()

                    '
                    '						sWOR = idh.forms.OrderRow.doAutoCreateNewRowFromLastJobRow( goParent, sWOH, True )
                    '                    	If Not sWOR Is Nothing AndAlso sWOR.Length > 0 Then
                    '                    		setSharedData(oForm, "ORDNO", sWOH)
                    '            				setSharedData(oForm, "ROWNO", sWOR )




                    '                    	Else
                    '                    		bContinue = False
                    '            			End If
                    '                    End If

                    If bContinue Then
                        doLinkDOAndWOFromShare(oForm)

                        'OnTime Ticket 23675 - Disposal Order Booking Date
                        'Disposal order booking date to be updated from the linked Waste Order's Actual Start Date
                        'The data update depends on WR Config Key
                        Dim sUpdateDOBDate As String

                        sUpdateDOBDate = Config.Parameter("DOBDWOAD")
                        If sUpdateDOBDate.ToUpper().Equals("TRUE") Then
                            doGetLinkWOASDate(oForm)
                        End If
                        '## MA Start 19-08-2014 Issue#147: Skype HH: Disable Add Tab if DO is Linked with WOR
                        Dim sWORowID As String = getDFValue(oForm, msRowTable, "U_WRRow", True)
                        Dim sWOCode As String = getDFValue(oForm, msRowTable, "U_WROrd", True)
                        If sWORowID.Trim <> "" OrElse sWOCode.Trim <> "" Then
                            setEnableItem(oForm, False, "IDH_TABADD") 'ADDGRID")
                        Else
                            setEnableItem(oForm, True, "IDH_TABADD")
                        End If
                        '## MA End 19-08-2014 Issue#147: Skype HH: Disable Add Tab if DO is Linked with WOR
                    End If

                    'ElseIf sModalFormType = "IDHEAORSR" Then
                    '	setOrigin(oForm, getSharedData(oForm, "IDH_ORCODE"))
                ElseIf sModalFormType = "IDHLOSCH" Then
                    doHandleVehicleSearchResult(oForm, sLastButton)

                    'Dim sCustomer As String
                    'Dim sCarrier As String
                    'Dim sPayType As String

                    'sCustomer = getSharedData(oForm, "CUSCD")
                    'sCarrier = getSharedData(oForm, "WCCD")
                    ''                    sPayType = getSharedData(oForm, "MARKAC")
                    ''					If Not sPayType Is Nothing AndAlso ( sPayType.Equals("DOORD") OrElse sPayType.Equals("REQORD") ) Then
                    'If Not sCustomer Is Nothing AndAlso sCustomer.Length > 0 Then
                    '    '		                    If Config.INSTANCE.getParameterAsBool( "DOVMUC", True ) OrElse _

                    '    '		                    	( sCustomer = sCarrier ) Then
                    '    Dim dChargeTotal As Double = getUnCommittedRowTotals(oForm, Nothing, 0)

                    '    'If com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) Then
                    '    Dim bBlockOnFail As Boolean = Config.INSTANCE.getParameterAsBool("DOBCCL", True)
                    '    If Config.INSTANCE.doCheckCredit(goParent, sCustomer, dChargeTotal, False, bBlockOnFail) = False Then

                    '        Exit Sub
                    '    End If
                    'End If

                    'Dim sVType As String = getSharedData(oForm, "VTYPE")
                    'If sVType.Length = 0 AndAlso Config.Parameter("DFVEHTYP") = "" Then
                    '    sVType = "S"
                    'ElseIf sVType.Length = 0 AndAlso Config.Parameter("DFVEHTYP").Length > 0 Then

                    '    sVType = Config.Parameter("DFVEHTYP")
                    'End If
                    'If sLastButton.Equals("IDH_CREATE") Then
                    '    setWFValue(oForm, "CreateNewVechicle", True)
                    '    Try
                    '        Dim sReg As String
                    '        Dim sJob As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, True)

                    '        sReg = getSharedData(oForm, "REG")
                    '        If sReg Is Nothing OrElse sReg.Length = 0 Then
                    '            sReg = getDFValue(oForm, msRowTable, "U_Lorry", True)

                    '        End If

                    '        sReg = com.idh.bridge.utils.General.doFixReg(sReg)

                    '        setDFValue(oForm, msRowTable, "U_LorryCd", "", False, True)
                    '        If sJob.Length > 0 Then
                    '            setFormDFValue(oForm, "Code", "")
                    '            oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                    '            doLoadDataLocal(oForm, False)

                    '            doSetVehicleReg(oForm, sReg, "", sJob, False, False)
                    '            doSetEnabled(oForm.Items.Item("IDH_TARWEI"), True)
                    '            doSetEnabled(oForm.Items.Item("IDH_TRLTar"), True)

                    '        End If
                    '        doSetFocus(oForm, "IDH_VEHREG")
                    '        setWFValue(oForm, "LastVehAct", "CREATE")
                    '        setWFValue(oForm, "VehicleType", sVType)
                    '    Catch ex As Exception
                    '    End Try
                    '    setWFValue(oForm, "CreateNewVechicle", False)
                    'Else
                    '    Dim sJob As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, True)
                    '    If sJob Is Nothing OrElse sJob.Length = 0 Then
                    '        com.idh.bridge.DataHandler.INSTANCE.doError("The Job Type must be selected.")
                    '        Exit Sub
                    '    End If

                    '    Dim iMode As SAPbouiCOM.BoFormMode = oForm.Mode
                    '    Dim sReg As String = getSharedData(oForm, "REG")
                    '    Dim sVehCode As String = getSharedData(oForm, "VEHCODE")
                    '    Dim sJobNr As String = Nothing
                    '    Dim sJobRow As String = Nothing
                    '    Dim sDoShowOnSite As String = getSharedData(oForm, "SHOWONSITE")

                    '    '** The onsite search **'
                    '    If sDoShowOnSite = "TRUE" Then
                    '        sJobNr = getSharedData(oForm, "DONR")
                    '        If sJobNr.Length = 0 Then

                    '            sJobNr = Nothing
                    '        End If

                    '        sJobRow = getSharedData(oForm, "DOROW")
                    '        If sJobRow.Length = 0 Then
                    '            sJobRow = Nothing
                    '        End If
                    '    End If

                    '    If Not sJobNr Is Nothing AndAlso Not sJobRow Is Nothing Then
                    '        doLoad2ndWeigh(oForm, sJobNr, sJobRow)
                    '    Else
                    '        doSetVehicleReg(oForm, sReg, sVehCode, sJob, True, True)
                    '    End If

                    '    If iMode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    '        doSetUpdate(oForm)
                    '    End If
                    '    setWFValue(oForm, "LastVehAct", "SELECT")
                    'End If

                    'doGetPrices(oForm)
                ElseIf sModalFormType = "IDHREPORT" OrElse sModalFormType = "IDHHTML" Then
                    If getSharedData(oForm, "DORELOAD") = "TRUE" Then
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE

                        setFormDFValue(oForm, "Code", "", False, True)
                        doLoadData(oForm)
                    End If

                ElseIf sModalFormType = "IDHPAYMET" Then
                    Dim sPayMeth As String = getSharedData(oForm, "METHOD")
                    Dim sStatus As String = getSharedData(oForm, "STATUS")
                    setDFValue(oForm, msRowTable, "U_Status", sStatus)
                    setDFValue(oForm, msRowTable, "U_PayMeth", sPayMeth)
                    setDFValue(oForm, msRowTable, "U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusUnPaid())
                    setDFValue(oForm, msRowTable, "U_CCNum", "")
                    setDFValue(oForm, msRowTable, "U_CCType", "0")
                    setDFValue(oForm, msRowTable, "U_CCStat", "")

                    If sPayMeth.Equals(Config.INSTANCE.getCreditCardName()) Then

                        If Config.INSTANCE.getParameterAsBool("CCONROW", False) Then
                            setSharedData(oForm, "CUSTCD", oDOR.U_CustCd)
                            setSharedData(oForm, "CUSTNM", oDOR.U_CustNm)
                            setSharedData(oForm, "AMOUNT", oDOR.U_Total)
                            goParent.doOpenModalForm("IDHCCPAY", oForm)
                        End If
                    ElseIf sStatus.Equals(com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus()) AndAlso (sPayMeth.Equals("Cash") OrElse sPayMeth.Equals("Check") OrElse sPayMeth.Equals("Cheque")) Then
                        setDFValue(oForm, msRowTable, "U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusPaid())
                    ElseIf sStatus.Equals(com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus()) AndAlso sPayMeth.Equals(com.idh.bridge.lookups.FixedValues.getPayMethFoc()) Then
                        setDFValue(oForm, msRowTable, "U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusFree())
                    End If

                    doSwitchCheckBox(oForm, "IDH_DOARI", "Y", False)
                ElseIf sModalFormType = "IDHCCPAY" Then

                    If Config.INSTANCE.getParameterAsBool("CCONROW", False) Then
                        Dim sCCStatPre As String
                        Dim sPStat As String
                        Dim sCCNum As String
                        Dim sCCType As String
                        Dim sCCIs As String
                        Dim sCCSec As String
                        Dim sCCHNm As String
                        Dim sCCPCd As String
                        Dim sCCExp As String

                        sCCNum = getSharedData(oForm, "CCNUM")
                        sPStat = getSharedData(oForm, "PSTAT")
                        sCCType = getSharedData(oForm, "CCTYPE")
                        sCCIs = getSharedData(oForm, "CCIs")
                        sCCSec = getSharedData(oForm, "CCSec")
                        sCCHNm = getSharedData(oForm, "CCHNm")
                        sCCPCd = getSharedData(oForm, "CCPCd")
                        sCCExp = getSharedData(oForm, "CCEXP")
                        If sPStat.Equals(com.idh.bridge.lookups.FixedValues.getStatusPaid()) Then
                            sCCStatPre = "APPROVED: "

                        Else
                            sCCStatPre = "DECLINED: "

                        End If
                        setDFValue(oForm, msRowTable, "U_CCNum", sCCNum)
                        setDFValue(oForm, msRowTable, "U_CCType", sCCType)
                        setDFValue(oForm, msRowTable, "U_CCStat", sCCStatPre & getSharedData(oForm, "CCSTAT"))
                        setDFValue(oForm, msRowTable, "U_PayStat", sPStat)
                        setDFValue(oForm, msRowTable, "U_CCExp", sCCExp)
                        setDFValue(oForm, msRowTable, "U_CCIs", sCCIs)
                        setDFValue(oForm, msRowTable, "U_CCSec", sCCSec)
                        setDFValue(oForm, msRowTable, "U_CCHNum", sCCHNm)
                        setDFValue(oForm, msRowTable, "U_CCPCd", sCCPCd)
                    ElseIf sModalFormType = "IDHASRCH" Then
                        Dim sTrg As String = getSharedData(oForm, "TRG")
                        If sTrg = "IDH_SITAL" OrElse sTrg = "IDH_SITAL2" Then
                            setFormDFValue(oForm, IDH_DISPROW._SAddress, getSharedData(oForm, "ADDRESS"))
                            doSetUpdate(oForm)
                        End If
                    End If
                End If

                ''Disabled because currencies will be set by CIP/SIP DBObjects now 
                'If sModalFormType = "IDHCSRCH" OrElse sModalFormType = "IDHNCSRCH" OrElse sModalFormType = "IDHLNKWO" OrElse sModalFormType = "IDHLOSCH" Then
                '    doSetCurrencies(oForm, True)
                'End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error handeling modal result.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHMR", {Nothing})
            End Try
            oForm.Freeze(False)

        End Sub

        Protected Overrides Sub doHandleModalCanceled(ByVal oParentForm As SAPbouiCOM.Form, ByVal sModalFormType As String)
            'MyBase.doHandleModalCanceled(oParentForm, sModalFormType)
            'MA Start 19-11-2014
            If sModalFormType.Equals("IDHISRC") Then
                Dim sTarget = getSharedData(oParentForm, "TRG")
                If sTarget IsNot Nothing AndAlso sTarget = "PROD" Then
                    Dim sitemid = getSharedData(oParentForm, "ACTIVEITM")
                    If sitemid Is Nothing Then
                        sitemid = "IDH_ITMCOD"
                    End If
                    If sitemid = "IDH_ITCF" Then
                        doSetFocus(oParentForm, "IDH_ITMCOD")
                        setItemValue(oParentForm, "IDH_ITMCOD", getItemValue(oParentForm, "IDH_ITMCOD"))
                    Else 'If Not oParentForm.ActiveItem.Equals(sitemid) Then
                        doSetFocus(oParentForm, "IDH_ITMCOD")
                        setItemValue(oParentForm, "IDH_ITMCOD", "")
                        setItemValue(oParentForm, "IDH_DESC", "")
                    End If
                End If
                'MA End 19-11-2014
            ElseIf sModalFormType.Equals("IDHWPWISRC") OrElse sModalFormType.Equals("IDHWISRC") Then 'Customer
                If Not oParentForm.ActiveItem.Equals("IDH_WASCL1") Then
                    doFillAlternateItemDescription(oParentForm, "", True)
                End If
            End If
            If getParentSharedData(oParentForm, "CALLEDITEM") IsNot Nothing AndAlso getParentSharedData(oParentForm, "CALLEDITEM").Trim <> "" Then
                doSetFocus(oParentForm, getParentSharedData(oParentForm, "CALLEDITEM"))

            End If
        End Sub

        Public Overrides Sub setEnableItem(ByVal oForm As SAPbouiCOM.Form, ByVal bIsEnabled As Boolean, ByVal sItem As String)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            'Dim sMode As String = getWFValue(oForm, "FormMode")
            If bIsEnabled Then
                If (oDOR IsNot Nothing AndAlso oDOR.MarketingMode = Config.MarketingMode.PO) Then
                    If Array.IndexOf(moCostTippingOutgoingItems, sItem) <> -1 Then
                        Exit Sub
                    ElseIf Array.IndexOf(moSalesItems, sItem) <> -1 Then
                        Exit Sub
                    End If
                Else
                    Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)
                    If Array.IndexOf(moCostTippingIncommingItems, sItem) <> -1 Then
                        Exit Sub
                    ElseIf Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) Then
                        If Array.IndexOf(moCostTippingOutgoingItems, sItem) <> -1 Then
                            Exit Sub
                        End If
                        'If Array.IndexOf(moDisposalSiteItems, sItem) <> -1 Then
                        '    If moLookup.getParameterAsBool("DODCEDI", False) = False Then
                        '        Exit Sub
                        '    End If
                        'End If
                    End If
                End If
            End If
            MyBase.doSetEnabled(oForm.Items.Item(sItem), bIsEnabled)
        End Sub

        Private Sub doSwitchToPO(ByVal oForm As SAPbouiCOM.Form, ByVal bReplaceProd As Boolean, Optional ByVal bDoSetAddress As Boolean = True)
            If getWFValue(oForm, "CANEDIT") = False Then
                Exit Sub
            End If

            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

            doSwitchCheckBox(oForm, "IDH_FOC", "Y", True)

            'If String.IsNullOrWhiteSpace(oDOR.U_TrnCode) = False Then
            'If oDOR.TRNCdDecodeRecord.PurcahseOrder Then
            ' If oDOR.isLinkedToOrder() = False Then
            'doSwitchCheckBox(oForm, "IDH_DOPO", "Y", True)
            'End If
            'End If
            'Else
            If com.idh.bridge.lookups.FixedValues.isOpen(oDOR.U_PStat) AndAlso oDOR.IsLinkedToOrder() = False Then
                If oDOR.TRNCdDecodeRecord Is Nothing OrElse oDOR.TRNCdDecodeRecord.PurcahseOrder Then
                    doSwitchCheckBox(oForm, "IDH_DOPO", "Y", True)
                End If
            End If
            'End If

            setEnableItems(oForm, Config.ParameterAsBool("DODCEDI", False), moDisposalSiteItems)
            setEnableItems(oForm, False, moCostTippingOutgoingItems)
            setEnableItems(oForm, True, moCostTippingIncommingItems)
            setEnableItems(oForm, True, moCostHaulageItems)
            setEnableItems(oForm, False, moSalesItems)
            setEnableItem(oForm, True, "IDH_NOVAT")

            doSetPOVatCheckbox(oForm)

            'setDFValue(oForm, msRowTable, IDH_DISPROW._RdWgt, 0)
            setDFValue(oForm, msRowTable, IDH_DISPROW._TRdWgt, 0)
            'setDFValue(oForm, msRowTable, IDH_DISPROW._PRdWgt, 0)
            setDFValue(oForm, msRowTable, IDH_DISPROW._CstWgt, 0)
            setDFValue(oForm, msRowTable, IDH_DISPROW._TCharge, 0)

            oDOR.doCalculateChargeTotals(IDH_DISPROW.TOTAL_CALCULATE)

            Dim oLinkedBP As LinkedBP = Config.INSTANCE.doGetLinkedBP(getDFValue(oForm, msRowTable, IDH_DISPROW._CustCd)) 'getDFValue(oForm, "@IDH_DISPORD", "U_CardCd"))
            If bReplaceProd AndAlso Not oLinkedBP Is Nothing Then
                doSetChoosenProducerFromBuffer(oForm, oLinkedBP, bDoSetAddress)
                setWasSetFromUser(oForm, "IDH_PROCD")
            End If

            doSwitchPriceFields(oForm)
            doEnableDisableOtherItems(oForm)

            If String.IsNullOrWhiteSpace(oDOR.U_TrnCode) = False Then
                setEnableItems(oForm, False, moMarketingCheckboxes)
            End If
        End Sub

        Private Sub doSwitchToSO(ByVal oForm As SAPbouiCOM.Form)
            If getWFValue(oForm, "CANEDIT") = False Then
                Exit Sub
            End If

            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

            doSwitchCheckBox(oForm, "IDH_DOPO", "N", True)

            If com.idh.bridge.lookups.FixedValues.isOpen(oDOR.U_Status) AndAlso oDOR.IsLinkedToOrder() = False Then
                If oDOR.TRNCdDecodeRecord Is Nothing OrElse oDOR.TRNCdDecodeRecord.SalesOrder Then
                    doSwitchCheckBox(oForm, "IDH_DOORD", "Y", True)
                End If
            End If

            'Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)
            Dim bIsOutgoingJob As Boolean = Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), oDOR.U_JobTp)

            'If getWFValue(oForm, "FormMode") = "SO" Then
            'Exit Sub
            'End If
            'setWFValue(oForm, "FormMode", "SO")

            setEnableItems(oForm, False, moCostTippingIncommingItems)
            setEnableItems(oForm, bIsOutgoingJob, moCostTippingOutgoingItems)
            setEnableItems(oForm, bIsOutgoingJob OrElse Config.ParameterAsBool("DODCEDI", False), moDisposalSiteItems)
            setEnableItems(oForm, bIsOutgoingJob, moCostHaulageItems)
            '## MA Start 13-10-2014
            If System.Array.IndexOf(moAlwaysDisabledFields, "IDH_CUSWEI") > -1 Then
                Dim oExclude() As String
                oExclude = {"IDH_CUSWEI"}
                'EnableAllItems(oForm, moSalesItems, oExclude)
                EnableAllEditItems(oForm, oExclude)
            Else
                setEnableItems(oForm, True, moSalesItems)
            End If
            '## MA Start 13-10-2014

            setEnableItem(oForm, True, "IDH_NOVAT")
            setDFValue(oForm, msRowTable, "U_ProWgt", 0)
            setDFValue(oForm, msRowTable, "U_PCost", 0)

            'setDFValue(oForm, msRowTable, "U_TipWgt", 0)
            'setDFValue(oForm, msRowTable, "U_TipCost", 0)

            doSetSOVatCheckbox(oForm)
            doSwitchPriceFields(oForm)
            doEnableDisableOtherItems(oForm)

            If String.IsNullOrWhiteSpace(oDOR.U_TrnCode) = False Then
                setEnableItems(oForm, False, moMarketingCheckboxes)
            End If
        End Sub

        Private Sub doEnableDisableOtherItems(ByVal oForm As SAPbouiCOM.Form)
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_ALLOW_AUOMSWITCH) = False Then
                setEnableItem(oForm, False, "IDH_USERE")
                setEnableItem(oForm, False, "IDH_USEAU")
            End If
            '    'Arrays for Items to Disable at certain stages
            '    'Sales Ordered
            '    'Purchase Ordered
            '    'Purchace Mode
            '    'Purchase Mode Update
            '    'Sales Mode Add
            '    'Sales Mode Update
            '    'Find Mode
        End Sub

        Private Sub doSwitchVatGroups(ByVal oForm As SAPbouiCOM.Form)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            Dim sDocDate As String = getDFValue(oForm, "@IDH_DISPORD", "U_BDate")
            Dim dDocDate As Date = com.idh.utils.Dates.doStrToDate(sDocDate)

            If getUFValue(oForm, "IDH_NOVAT") = "Y" Then
                oDOR.DoChargeVat = False
                If (oDOR.MarketingMode = Config.MarketingMode.PO) Then
                    'If getWFValue(oForm, "FormMode") = "PO" Then
                    oDOR.U_TCostVtGrp = Config.Parameter("MDVTEXGI")
                    oDOR.U_TCostVtRt = 0

                    oDOR.U_PCostVtGrp = Config.Parameter("MDVTEXGI")
                    oDOR.U_PCostVtRt = 0
                Else
                    oDOR.U_TChrgVtGrp = Config.Parameter("MDVTEXGO")
                    oDOR.U_TChrgVtRt = 0
                End If

                Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                oGrid.DoVat = False
            Else
                oDOR.DoChargeVat = True

                Dim sSupCardCode As String = oDOR.F_ProducerCode
                Dim sCardCode As String = oDOR.F_CustomerCode
                Dim sWasteCode As String = oDOR.F_WasteCode
                If (oDOR.MarketingMode = Config.MarketingMode.PO) Then
                    'If getWFValue(oForm, "FormMode") = "PO" Then
                    Dim sTVatGroup As String = Config.INSTANCE.doGetPurchaceVatGroup(sCardCode, sWasteCode)
                    Dim dTVatRate As Double = Config.INSTANCE.doGetVatRate(sTVatGroup, dDocDate)

                    Dim sPVatGroup As String = Config.INSTANCE.doGetPurchaceVatGroup(sSupCardCode, sWasteCode)
                    Dim dPVatRate As Double = Config.INSTANCE.doGetVatRate(sPVatGroup, dDocDate)

                    oDOR.U_TCostVtGrp = sTVatGroup
                    oDOR.U_TCostVtRt = dTVatRate

                    oDOR.U_PCostVtGrp = sPVatGroup
                    oDOR.U_PCostVtRt = dPVatRate
                Else
                    Dim sVatGroup As String = Config.INSTANCE.doGetSalesVatGroup(sCardCode, sWasteCode)
                    Dim dVatRate As Double = Config.INSTANCE.doGetVatRate(sVatGroup, dDocDate)

                    oDOR.U_TChrgVtGrp = sVatGroup
                    oDOR.U_TChrgVtRt = dVatRate
                End If

                Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                oGrid.DoVat = True
            End If
            doCalcAdditionalTotals(oForm)

            'doCalcMarketingTotals(oForm)

            oDOR.doCalculateCostVat("A")
            oDOR.doCalculateChargeVat("A")
        End Sub

        Private Sub doSetVatCheckbox(ByVal oForm As SAPbouiCOM.Form)
            If getWFValue(oForm, "FormMode") = "PO" Then
                doSetPOVatCheckbox(oForm)
            ElseIf getWFValue(oForm, "FormMode") = "SO" Then
                doSetSOVatCheckbox(oForm)
            End If

        End Sub

        Private Sub doSetPOVatCheckbox(ByVal oForm As SAPbouiCOM.Form)
            Dim sVatGroup As String = getDFValue(oForm, msRowTable, "U_TCostVtGrp")
            If Not sVatGroup Is Nothing AndAlso sVatGroup.Equals(Config.Parameter("MDVTEXGI")) Then
                setUFValue(oForm, "IDH_NOVAT", "Y")
            Else
                setUFValue(oForm, "IDH_NOVAT", "N")
            End If
        End Sub

        Private Sub doSetSOVatCheckbox(ByVal oForm As SAPbouiCOM.Form)
            Dim sVatGroup As String = getDFValue(oForm, msRowTable, "U_TChrgVtGrp")
            If Not sVatGroup Is Nothing AndAlso sVatGroup.Equals(Config.Parameter("MDVTEXGO")) Then
                setUFValue(oForm, "IDH_NOVAT", "Y")
            Else
                setUFValue(oForm, "IDH_NOVAT", "N")
            End If
        End Sub

        Private Sub doSetVisible(ByVal oForm As SAPbouiCOM.Form, ByRef oItems() As String, ByVal bIsVisible As Boolean, ByVal iPane As Integer)
            oForm.Freeze(True)
            Try
                Dim iCount As Integer
                'Dim oItem As SAPbouiCOM.Item
                For iCount = 0 To oItems.GetLength(0) - 1
                    doSetVisible(oForm, oItems(iCount), bIsVisible, iPane)
                    '                    oItem = oForm.Items.Item(oItems(iCount))
                    '                    oItem.Visible = bIsVisible
                    '                   	oItem.FromPane = iPane
                    '                   	oItem.ToPane = iPane
                Next
            Catch ex As Exception
                'doError("doSetVisible", ex.ToString(), "")
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        Private Sub doSetVisible(ByVal oForm As SAPbouiCOM.Form, ByRef sItem As String, ByVal bIsVisible As Boolean, ByVal iPane As Integer)
            oForm.Freeze(True)
            Try
                Dim oItem As SAPbouiCOM.Item
                oItem = oForm.Items.Item(sItem)
                oItem.Visible = bIsVisible
                oItem.FromPane = iPane
                oItem.ToPane = iPane
            Catch ex As Exception
                'doError("doSetVisible", ex.ToString(), "")
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        '**Lookup and Link
        Private Sub doLinkDOAndWOFromShare(ByVal oForm As SAPbouiCOM.Form)
            Dim sOrd As String = getSharedData(oForm, "ORDNO")
            Dim sRow As String = getSharedData(oForm, "ROWNO")
            Dim sCode As String = ""
            Dim sRowNr As String = ""
            Dim sJobType As String = getDFValue(oForm, IDH_DISPROW.TableName, IDH_DISPROW._JobTp, True)

            If sOrd.Length = 0 OrElse sRow.Length = 0 Then
                setWFValue(oForm, "LastVehAct", "DOSEARCH")
                setDFValue(oForm, msRowTable, "U_WROrd", "", True)
                setDFValue(oForm, msRowTable, "U_WRRow", "", True)
                doSetFocus(oForm, "IDH_VEHREG")
                Exit Sub
            End If

            'If sOrd.Length > 0 Then
            oForm.Items.Item("IDH_ORDLK").Visible = True
            'Else
            '    oForm.Items.Item("IDH_ORDLK").Visible = False
            'End If

            'If sRow.Length > 0 Then
            oForm.Items.Item("IDH_ROWLK").Visible = True
            'Else
            '    oForm.Items.Item("IDH_ROWLK").Visible = False
            'End If

            setWFValue(oForm, "LASTLINKEDWO", sOrd)
            doSetVisible(oForm, "IDH_WOCFL", True, 0)

            '** First try and get a DO linked to this WO-ROW
            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                If (sOrd.Length > 0 AndAlso sRow.Length > 0) Then

                    Dim sWR1JobType As String = getDFValue(oForm, IDH_DISPROW.TableName, IDH_DISPROW._WROrd, True)

                    Dim sQry As String = "Select Code, U_JobNr FROM [@IDH_DISPROW] WHERE U_WROrd = '" & sOrd & "' AND U_WRRow = '" & sRow & "'"

                    Dim oRecordset As SAPbobsCOM.Recordset = Nothing
                    oRecordset = goParent.goDB.doSelectQuery(sQry)
                    If Not oRecordset Is Nothing AndAlso oRecordset.RecordCount > 0 Then
                        sRowNr = oRecordset.Fields.Item(0).Value.Trim()
                        sCode = oRecordset.Fields.Item(1).Value.Trim()
                    End If
                    IDHAddOns.idh.data.Base.doReleaseObject(oRecordset)

                    If sCode.Length > 0 Then
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                        If sCode.Length > 0 Then
                            Dim cond As SAPbouiCOM.Condition
                            Dim conds As SAPbouiCOM.Conditions

                            conds = New SAPbouiCOM.Conditions
                            cond = conds.Add()
                            cond.Alias = "Code" 'FIELD NAME
                            cond.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                            cond.CondVal = sCode

                            getFormMainDataSource(oForm).Query(conds)

                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                            doSetEnabled(oForm.Items.Item("IDH_BOOREF"), False)
                        End If

                        setWFValue(oForm, "TargetRow", sRowNr)
                        doLoadData(oForm)

                        setDFValue(oForm, IDH_DISPROW.TableName, IDH_DISPROW._WROrd, sWR1JobType, True)

                        '** Check the Weights
                        Dim dWei1 As Double = getDFValue(oForm, msRowTable, "U_Wei1")
                        Dim dWei2 As Double = getDFValue(oForm, msRowTable, "U_Wei2")
                        If dWei1 = 0 Then
                            ''## MA Start 16-09-2014 Issue#401
                            If isItemEnabled(oForm, "IDH_WEI1") Then
                                doSetFocus(oForm, "IDH_WEI1")
                            End If
                            ''## MA End 16-09-2014 Issue#401
                            oForm.Title = getTranslatedCaption("#TITLE", "Disposal Order") & " - " & getTranslatedWord("First Weigh")
                        ElseIf dWei2 = 0 Then
                            ''## MA Start 16-09-2014 Issue#401
                            If isItemEnabled(oForm, "IDH_WEI2") Then
                                doSetFocus(oForm, "IDH_WEI2")
                            End If
                            ''## MA End 16-09-2014 Issue#401
                            oForm.Title = "Disposal Order - Second Weigh"
                        End If
                        Exit Sub
                    End If
                End If
            End If

            If (sOrd.Length > 0 AndAlso sRow.Length > 0) Then
                If Config.INSTANCE.getParameterAsBool("DOLKWA", False) = True Then
                    '                	Dim sMessage As String
                    '                	sMessage = "By linking the Disposal and the Waste Order the Waste Order will be updated with values from the Disposal Order."
                    '                	If goParent.goApplication.M_essageBox(sMessage & Chr(10) & "Do you want to continue." & Chr(10), 1, "Yes", "No") = 2 Then
                    '                    	Exit Sub
                    '                	End If

                    If Messages.INSTANCE.doResourceMessageYNAsk("DODLWUD", Nothing, 1) = 2 Then
                        Exit Sub
                    End If
                End If

                'If (sOrd.Length > 0 AndAlso sRow.Length > 0) Then
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                    setFormDFValue(oForm, "Code", sCode)
                    doLoadData(oForm)
                End If

                Dim oDO As IDH_DISPORD = thisDO(oForm)
                Dim oDOR As IDH_DISPROW = thisDOR(oForm)

                '20150308 - LPV - BEGIN - Do Not Clear tare as per request
                'setUFValue(oForm, "IDH_TARWEI", 0)
                'setUFValue(oForm, "IDH_TRLTar", 0)
                ''MA Start 05-02-2015 Issue#554
                'setUFValue(oForm, "IDH_TRWTE1", "")
                'setUFValue(oForm, "IDH_TRWTE2", "")
                ''MA End 05-02-2015 Issue#554
                '20150308 - LPV - END - Do Not Clear tare as per request

                'oForm.Title = "Disposal Order - First Weigh"

                'If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                '    doSwitchToAdd(oForm)
                '    oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                'End If

                Dim sContainerType As String = getSharedData(oForm, "CONTP")
                Dim sContainerCode As String = getSharedData(oForm, "CONCD")
                Dim sContainerDesc As String = getSharedData(oForm, "CONDSC")
                Dim sWasteCode As String = getSharedData(oForm, "WASTCD")
                Dim sWasteDesc As String = getSharedData(oForm, "WASTDE")
                ''MA 10-09-2015 ALTWSTDE was not passing from WO Search form
                Dim sAltWasteDesc As String = getSharedData(oForm, "ALTWSTDE")

                Dim sCustomerCode As String = getSharedData(oForm, "CUSTCD")
                Dim sCustomerName As String = getSharedData(oForm, "CUSTNM")
                Dim sCarrCode As String = getSharedData(oForm, "CARRCD")
                Dim sHeadCarrCode As String = getSharedData(oForm, "HDCARRCD")
                Dim sCarrName As String = getSharedData(oForm, "CARRNM")
                Dim sPCode As String = getSharedData(oForm, "PCD")
                Dim sPName As String = getSharedData(oForm, "PNM")
                Dim sSCode As String = getSharedData(oForm, "SCD")
                Dim sSName As String = getSharedData(oForm, "SNM")
                Dim sOrigin As String = getSharedData(oForm, "ORIGIN")

                Dim sWastTNN As String = getSharedData(oForm, "WASTTNN")
                'Dim sHazWCNN As String = getSharedData(oForm, "HAZWCNN")
                Dim sSiteRef As String = getSharedData(oForm, "SITEREF")
                Dim sExtWeig As String = getSharedData(oForm, "EXTWEIG")
                Dim sComNum As String = getSharedData(oForm, "CONNUM")

                Dim sLorrDesc As String = getSharedData(oForm, "LORRYDESC")
                Dim sDriver As String = getSharedData(oForm, "DRIVER")
                Dim sObligated As String = getSharedData(oForm, "OBLIGATED")
                Dim sCompliance As String = getSharedData(oForm, "COMSCHEME")
                Dim sCustRef As String = getSharedData(oForm, "CUSTREF")

                Dim sWORJOPTP As String = getSharedData(oForm, "JOBTP")
                Dim dExpLdWgt As Double = getSharedData(oForm, "EXPLdWGT")

                doSetUpdate(oForm)

                Try
                    oDOR.BlockChangeNotif = True
                    If Not sCustomerCode Is Nothing AndAlso sCustomerCode.Length > 0 Then
                        Dim dChargeTotal As Double = getUnCommittedRowTotals(oForm, Nothing, 0)
                        If Config.INSTANCE.doCheckCredit(sCustomerCode, dChargeTotal, False) = False Then
                            Exit Sub
                        End If

                        If sCompliance Is Nothing OrElse sCompliance.Length = 0 Then
                            sCompliance = Config.INSTANCE.getBPCs(sCustomerCode)
                        End If
                        setDFValue(oForm, msRowTable, "U_CustCs", sCompliance)
                        'oDOR.U_CustCs = sCompliance
                    End If

                    Dim sLorry As String = getSharedData(oForm, "LORRY")
                    'Dim sLorrTest As String = getDFValue(oForm, msRowTable, "U_Lorry")
                    Dim sLorrTest As String = oDOR.U_Lorry
                    Dim sLorryCode As String = getSharedData(oForm, "LORRYCD")
                    If sLorrTest.Length = 0 AndAlso sLorry.Length > 0 Then
                        setDFValue(oForm, msRowTable, "U_Lorry", sLorry, False, True)
                        setDFValue(oForm, msRowTable, "U_LorryCd", sLorryCode, False, True)
                        'oDOR.U_Lorry = sLorry
                        'oDOR.U_LorryCd = sLorryCode
                    End If

                    setDFValue(oForm, msRowTable, "U_VehTyp", sLorrDesc)
                    setDFValue(oForm, msRowTable, "U_Driver", sDriver)
                    setDFValue(oForm, msRowTable, "U_WROrd", sOrd)
                    setDFValue(oForm, msRowTable, "U_WRRow", sRow, False, True)
                    'oDOR.U_VehTyp = sLorrDesc
                    'oDOR.U_Driver = sDriver
                    'oDOR.U_WROrd = sOrd
                    'oDOR.U_WRRow = sRow

                    '*** CUSTOMER
                    If String.IsNullOrWhiteSpace(sCustomerName) = False Then
                        setFormDFValue(oForm, IDH_DISPORD._CardCd, sCustomerCode)
                        setFormDFValue(oForm, IDH_DISPORD._CardNM, sCustomerName)

                        ''setDFValue(oForm, msRowTable, IDH_DISPROW._CustCd, sCustomerCode, False, True)
                        ''setDFValue(oForm, msRowTable, IDH_DISPROW._CustNm, sCustomerName, False, True)

                        ' ''## MA Start 16-10-2014
                        ''doUpdateBPWeightFlag(oForm)
                        ' ''## MA End 16-10-2014

                        setFormDFValue(oForm, "U_Contact", getSharedData(oForm, "CONTACT"))
                        setFormDFValue(oForm, "U_Address", getSharedData(oForm, "ADDRESS"))
                        setFormDFValue(oForm, "U_Street", getSharedData(oForm, "STREET"))
                        setFormDFValue(oForm, "U_Block", getSharedData(oForm, "BLOCK"))
                        setFormDFValue(oForm, "U_County", getSharedData(oForm, "COUNTY"))
                        setFormDFValue(oForm, "U_City", getSharedData(oForm, "CITY"))
                        setFormDFValue(oForm, "U_ZpCd", getSharedData(oForm, "ZPCD"))
                        setFormDFValue(oForm, "U_Phone1", getSharedData(oForm, "PHONE1"))
                        setFormDFValue(oForm, "U_SiteTl", getSharedData(oForm, "SITETL"))
                        setFormDFValue(oForm, "U_SteId", getSharedData(oForm, "STEID"))
                        setFormDFValue(oForm, "U_Route", getSharedData(oForm, "ROUTE"))
                        setFormDFValue(oForm, "U_Seq", getSharedData(oForm, "SEQ"))

                        setFormDFValue(oForm, "U_PremCd", getSharedData(oForm, "PRECD"))
                        setFormDFValue(oForm, "U_SiteLic", getSharedData(oForm, "SITELIC"))

                        ''Was removed bringing it back as per Hanna sheet
                        setFormDFValue(oForm, "U_CustRef", getSharedData(oForm, "CUSTREF"))

                        'oDO.U_CardCd = sCustomerCode
                        'oDO.U_CardNM = sCustomerName
                        'oDO.U_Contact = getSharedData(oForm, "CONTACT")
                        'oDO.U_Address = getSharedData(oForm, "ADDRESS")
                        'oDO.U_Street = getSharedData(oForm, "STREET")
                        'oDO.U_Block = getSharedData(oForm, "BLOCK")
                        'oDO.U_County = getSharedData(oForm, "COUNTY")
                        'oDO.U_City = getSharedData(oForm, "CITY")
                        'oDO.U_ZpCd = getSharedData(oForm, "ZPCD")
                        'oDO.U_Phone1 = getSharedData(oForm, "PHONE1")
                        'oDO.U_SiteTl = getSharedData(oForm, "SITETL")
                        'oDO.U_SteId = getSharedData(oForm, "STEID")
                        'oDO.U_Route = getSharedData(oForm, "ROUTE")
                        'oDO.U_Seq = getSharedData(oForm, "SEQ")
                        'oDO.U_PremCd = getSharedData(oForm, "PRECD")
                        'oDO.U_SiteLic = getSharedData(oForm, "SITELIC")

                        'Was removed bringing it back as per Hanna sheet
                        'oDO.U_CustRef = getSharedData(oForm, "CUSTREF")

                        doSetEnabled(oForm.Items.Item("IDH_CUST"), False)
                        doSetEnabled(oForm.Items.Item("IDH_CUSTNM"), False)
                    End If

                    '*** CARRIER
                    If String.IsNullOrWhiteSpace(sCarrCode) = False Then
                        ''setDFValue(oForm, msRowTable, "U_CarrCd", sCarrCode, False, True)
                        ''setDFValue(oForm, msRowTable, "U_CarrNm", sCarrName, False, True)

                        setFormDFValue(oForm, "U_CCardCd", sCarrCode, False, True)
                        setFormDFValue(oForm, "U_CCardNM", sCarrName, False, True)

                        'oDO.U_CCardCd = sCarrCode
                        'oDO.U_CCardNM = sCarrName

                        ''Update all the linked rows values
                        Dim oParams As Hashtable = New Hashtable()
                        oParams.Add("U_CarrCd", sCarrCode)
                        oParams.Add("U_CarrNm", sCarrName)
                        setAllRowsValues(oForm, oParams)

                        If sHeadCarrCode.Equals(sCarrCode) = False Then
                            Dim oAddressData As ArrayList
                            oAddressData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sCarrCode, "S")
                            doSetCarrierAddress(oForm, oAddressData)
                        Else
                            setFormDFValue(oForm, "U_CContact", getSharedData(oForm, "CCONTACT"))
                            setFormDFValue(oForm, "U_CAddress", getSharedData(oForm, "CADDRESS"))
                            setFormDFValue(oForm, "U_CStreet", getSharedData(oForm, "CSTREET"))
                            setFormDFValue(oForm, "U_CBlock", getSharedData(oForm, "CBLOCK"))
                            setFormDFValue(oForm, "U_CCity", getSharedData(oForm, "CCITY"))
                            setFormDFValue(oForm, "U_CZpCd", getSharedData(oForm, "CZPCD"))
                            setFormDFValue(oForm, "U_CPhone1", getSharedData(oForm, "CPHONE1"))

                            'oDO.U_CContact = getSharedData(oForm, "CCONTACT")
                            'oDO.U_CAddress = getSharedData(oForm, "CADDRESS")
                            'oDO.U_CStreet = getSharedData(oForm, "CSTREET")
                            'oDO.U_CBlock = getSharedData(oForm, "CBLOCK")
                            'oDO.U_CCity = getSharedData(oForm, "CCITY")
                            'oDO.U_CZpCd = getSharedData(oForm, "CZPCD")
                            'oDO.U_CPhone1 = getSharedData(oForm, "CPHONE1")
                        End If
                    End If
                    setFormDFValue(oForm, "U_SupRef", getSharedData(oForm, "CREF"))

                    'CODE CHANGED BY KASHIF - START 20121126
                    ''*** Site
                    'If sSCode.Length > 0 Then
                    '    setDFValue(oForm, msRowTable, "U_Tip", sSCode)
                    '    setDFValue(oForm, msRowTable, "U_TipNm", sSName)

                    '    setFormDFValue(oForm, "U_SCardCd", sSCode)
                    '    setFormDFValue(oForm, "U_SCardNM", sSName)
                    '    setFormDFValue(oForm, "U_SContact", getSharedData(oForm, "SCONTACT"))
                    '    setFormDFValue(oForm, "U_SAddress", getSharedData(oForm, "SADDRESS"))
                    '    setFormDFValue(oForm, "U_SStreet", getSharedData(oForm, "SSTREET"))
                    '    setFormDFValue(oForm, "U_SBlock", getSharedData(oForm, "SBLOCK"))
                    '    setFormDFValue(oForm, "U_SCity", getSharedData(oForm, "SCITY"))
                    '    setFormDFValue(oForm, "U_SZpCd", getSharedData(oForm, "SZPCD"))
                    '    setFormDFValue(oForm, "U_SPhone1", getSharedData(oForm, "SPHONE1"))
                    'End If

                    'New Logic: as Disposal BP Code, Name and Address are now saved on the WOR
                    'we need to pull the information from the WOR
                    Dim sRowSiteAddress As String = getSharedData(oForm, "RSADDRESS")
                    Dim sQuery As String = "select * from CRD1 where CardCode = '" + sSCode + "' AND Address = '" + sRowSiteAddress + "'"
                    Dim oRS As SAPbobsCOM.Recordset
                    oRS = goParent.goDB.doSelectQuery(sQuery)
                    If oRS IsNot Nothing AndAlso oRS.RecordCount > 0 Then
                        ''Set WOR's disposal site details
                        setDFValue(oForm, msRowTable, "U_Tip", sSCode)
                        setDFValue(oForm, msRowTable, "U_TipNm", sSName)

                        setFormDFValue(oForm, "U_SCardCd", sSCode)
                        setFormDFValue(oForm, "U_SCardNM", sSName)
                        setFormDFValue(oForm, "U_SContact", oRS.Fields.Item("U_IDHACN").Value)
                        setFormDFValue(oForm, "U_SAddress", oRS.Fields.Item("Address").Value)
                        setFormDFValue(oForm, "U_SStreet", oRS.Fields.Item("Street").Value)
                        setFormDFValue(oForm, "U_SBlock", oRS.Fields.Item("Block").Value)
                        setFormDFValue(oForm, "U_SCity", oRS.Fields.Item("City").Value)
                        setFormDFValue(oForm, "U_SZpCd", oRS.Fields.Item("ZipCode").Value)
                        setFormDFValue(oForm, "U_SPhone1", oRS.Fields.Item("U_Tel1").Value)

                        'Set WOR's disposal site details
                        'oDO.U_SCardCd = sSCode
                        'oDO.U_SCardNM = sSName
                        'oDO.U_SContact = oRS.Fields.Item("U_IDHACN").Value
                        'oDO.U_SAddress = oRS.Fields.Item("Address").Value
                        'oDO.U_SStreet = oRS.Fields.Item("Street").Value
                        'oDO.U_SBlock = oRS.Fields.Item("Block").Value
                        'oDO.U_SCity = oRS.Fields.Item("City").Value
                        'oDO.U_SZpCd = oRS.Fields.Item("ZipCode").Value
                        'oDO.U_SPhone1 = oRS.Fields.Item("U_Tel1").Value
                    End If
                    IDHAddOns.idh.data.Base.doReleaseObject(oRS)

                    setDFValue(oForm, msRowTable, IDH_DISPROW._SAddress, sRowSiteAddress)
                    'oDOR.U_SAddress = sRowSiteAddress
                    'CODE CHANGED BY KASHIF - END 20121126

                    setDFValue(oForm, msRowTable, "U_ItemCd", sContainerCode, False, True)
                    setDFValue(oForm, msRowTable, "U_ItemDsc", sContainerDesc, False, True)
                    setDFValue(oForm, msRowTable, "U_WasCd", sWasteCode, False, True)
                    setDFValue(oForm, msRowTable, "U_WasDsc", sWasteDesc, False, True)
                    setDFValue(oForm, msRowTable, "U_ItmGrp", sContainerType, False, True)
                    ''MA Start 10-09-2015 Here alternate waste Description was not passing from Search form' Have update the search form to return it
                    ''KA - DIMECA - ALternate Waste Description 
                    setDFValue(oForm, msRowTable, "U_AltWasDsc", sAltWasteDesc, False, True)
                    ''MA End 10-09-2015 Here alternate waste Description was not passing from Search form' Have update the search form to return it

                    setDFValue(oForm, msRowTable, "U_Obligated", sObligated)

                    setDFValue(oForm, msRowTable, "U_WastTNN", sWastTNN)
                    ''setDFValue(oForm, msRowTable, "U_HazWCNN", sHazWCNN)
                    setDFValue(oForm, msRowTable, "U_SiteRef", sSiteRef)
                    setDFValue(oForm, msRowTable, "U_ExtWeig", sExtWeig)

                    ''Dim sOldConNum As String = getDFValue(oForm, msRowTable, "U_ConNum")
                    ''If sOldConNum.Length = 0 Then
                    setDFValue(oForm, msRowTable, "U_ConNum", sComNum)
                    ''End If

                    'oDOR.U_ItemCd = sContainerCode
                    'oDOR.U_ItemDsc = sContainerDesc
                    'oDOR.U_WasCd = sWasteCode
                    'oDOR.U_WasDsc = sWasteDesc
                    'oDOR.U_ItmGrp = sContainerType

                    'KA - DIMECA - ALternate Waste Description 
                    'oDOR.U_AltWasDsc = sAltWasteDesc

                    'oDOR.U_Obligated = sObligated
                    'oDOR.U_WastTNN = sWastTNN
                    'setDFValue(oForm, msRowTable, "U_HazWCNN", sHazWCNN)
                    'oDOR.U_SiteRef = sSiteRef
                    'oDOR.U_ExtWeig = sExtWeig

                    'Dim sOldConNum As String = getDFValue(oForm, msRowTable, "U_ConNum")
                    'If sOldConNum.Length = 0 Then
                    'oDOR.U_ConNum = sComNum
                    'End If

                    setDFValue(oForm, msRowTable, "U_Origin", sOrigin, False, True)
                    'setOrigin(oForm, sOrigin)
                    'oDO.U_Origin = sOrigin

                    doSwitchCheckBox(oForm, "IDH_FOC", "Y", True)
                    doSwitchCheckBox(oForm, "IDH_DOPO", "N", True)

                    setDFValue(oForm, msRowTable, IDH_DISPROW._BookIn, "N")
                    'oDOR.U_BookIn = "N"

                    '*** Producer
                    If String.IsNullOrWhiteSpace(sPCode) = False Then
                        setFormDFValue(oForm, "U_PCardCd", sPCode)
                        setFormDFValue(oForm, "U_PCardNM", sPName)

                        setDFValue(oForm, msRowTable, "U_ProCd", sPCode)
                        setDFValue(oForm, msRowTable, "U_ProNm", sPName)

                        setFormDFValue(oForm, "U_PContact", getSharedData(oForm, "PCONTACT"))
                        setFormDFValue(oForm, "U_PAddress", getSharedData(oForm, "PADDRESS"))
                        setFormDFValue(oForm, "U_PStreet", getSharedData(oForm, "PSTREET"))
                        setFormDFValue(oForm, "U_PBlock", getSharedData(oForm, "PBLOCK"))
                        setFormDFValue(oForm, "U_PCity", getSharedData(oForm, "PCITY"))
                        setFormDFValue(oForm, "U_PZpCd", getSharedData(oForm, "PZPCD"))
                        setFormDFValue(oForm, "U_PPhone1", getSharedData(oForm, "PPHONE1"))

                        'oDO.U_PCardCd = sPCode
                        'oDO.U_PCardNM = sPName

                        'oDO.U_PContact = getSharedData(oForm, "PCONTACT")
                        'oDO.U_PAddress = getSharedData(oForm, "PADDRESS")
                        'oDO.U_PStreet = getSharedData(oForm, "PSTREET")
                        'oDO.U_PBlock = getSharedData(oForm, "PBLOCK")
                        'oDO.U_PCity = getSharedData(oForm, "PCITY")
                        'oDO.U_PZpCd = getSharedData(oForm, "PZPCD")
                        'oDO.U_PPhone1 = getSharedData(oForm, "PPHONE1")
                    End If

                    'Setting Transaction code from the Linked WOR 
                    Dim sTrnCode As String = getSharedData(oForm, "TRNCODE")
                    Dim sTrnDescp As String = String.Empty
                    If String.IsNullOrWhiteSpace(sTrnCode) = False Then
                        'setEnableItems(oForm, False, moMarketingCheckboxes)

                        setDFValue(oForm, msRowTable, IDH_DISPROW._TrnCode, sTrnCode)
                        'oDOR.U_TrnCode = sTrnCode

                        Dim oTransCode As IDH_TRANCD = New IDH_TRANCD()
                        If oTransCode.getByKey(sTrnCode) Then
                            sTrnDescp = oTransCode.U_Process
                        End If

                        Dim sSpInst As String = getFormDFValue(oForm, "U_SpInst")
                        'Dim sSpInst As String = oDO.U_SpInst
                        If String.IsNullOrWhiteSpace(sSpInst) = False Then
                            If sSpInst.Contains("#") Then
                                sSpInst = sSpInst.Replace(sSpInst.Substring(sSpInst.IndexOf("#"), sSpInst.Length - sSpInst.IndexOf("#")), String.Empty)
                                sSpInst = sSpInst.Trim().Trim("-").Trim()
                            End If
                            sSpInst = If(sSpInst.Length > 0, sSpInst + " - ", sSpInst) + "#WOR Transaction Code/Desp: " + sTrnCode + "-" + sTrnDescp + "#"
                        Else
                            sSpInst = "#WOR Transaction Code/Desp: " + sTrnCode + "-" + sTrnDescp + "#"
                        End If
                        setFormDFValue(oForm, "U_SpInst", sSpInst)
                        'oDO.U_SpInst = sSpInst
                    End If

                    If String.IsNullOrWhiteSpace(sWORJOPTP) = False Then
                        Dim sNewJopbType As String = ""
                        '20150218 - LPV - BEGIN - Change order to always fall back on incoming
                        If Config.INSTANCE.isOutgoingJob(sContainerType, sWORJOPTP) Then
                            'setDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, Config.INSTANCE.doGetJopTypeOutgoing())
                            If Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) = False Then
                                sJobType = Config.INSTANCE.doGetJopTypeOutgoing()
                            End If
                        Else
                            'setDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, Config.INSTANCE.doGetJopTypeIncoming())
                            If Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) = False Then
                                sJobType = Config.INSTANCE.doGetJopTypeIncoming()
                            End If
                        End If
                        '20150218 - LPV - END
                    End If
                    oDOR.U_JobTp = sJobType

                    'If Config.INSTANCE.doCheckCanPurchaseWB(getDFValue(oForm, msRowTable, "U_WasCd")) _
                    'AndAlso Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)) Then
                    If Config.INSTANCE.doCheckCanPurchaseWB(oDOR.U_WasCd) _
                        AndAlso Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), oDOR.U_JobTp) Then
                        doSwitchToPO(oForm, True, False)
                    Else
                        doSwitchToSO(oForm)
                    End If

                    FillCombos.TransactionCodeCombo(FillCombos.getCombo(oForm, "uBC_TRNCd"), Nothing, Nothing, Nothing)
                    setEnableItem(oForm, False, "uBC_TRNCd")
                    doSwitchGenButton(oForm)

                    'setFormDFValue(oForm, "U_RTIMEF", getSharedData(oForm, "RTIMEF"))
                    'setFormDFValue(oForm, "U_RTIMET", getSharedData(oForm, "RTIMET"))

                    ' ''Rather use the Default value
                    ' ''setDFValue(oForm, msRowTable, "U_BookIn", getSharedData(oForm, "BOOKIN"))

                    'setDFValue(oForm, msRowTable, "U_IsTrl", getSharedData(oForm, "ISTRL"))

                    oDO.U_RTIMEF = getSharedData(oForm, "RTIMEF")
                    oDOR.U_RTimeT = getSharedData(oForm, "RTIMET")
                    oDOR.U_IsTrl = getSharedData(oForm, "ISTRL")

                    'Dim sCustRef As String = getSharedData(oForm, "CUSTREF")
                    If Not sCustRef Is Nothing AndAlso sCustRef.Length() > 0 AndAlso Config.Parameter("MDREAE").ToUpper().Equals("FALSE") Then
                        doSetEnabled(oForm.Items.Item("IDH_CUSCRF"), False)
                    Else
                        doSetEnabled(oForm.Items.Item("IDH_CUSCRF"), True)
                    End If

                    ''setDFValue(oForm, "@IDH_DISPORD", "U_CustRef", sCustRef)
                    'setDFValue(oForm, msRowTable, IDH_DISPROW._ExpLdWgt, dExpLdWgt)
                    oDOR.U_ExpLdWgt = dExpLdWgt

                    Dim sMANPRC As String = getSharedData(oForm, "MANPRC")
                    If String.IsNullOrWhiteSpace(sMANPRC) = False Then
                        oDOR.U_MANPRC = sMANPRC
                    End If

                    'Set the UOM's
                    Dim sUOM As String = getSharedData(oForm, "UOM")
                    Dim sPUOM As String = getSharedData(oForm, "PUOM")
                    Dim sProUOM As String = getSharedData(oForm, "PROUOM")

                    'setDFValue(oForm, msRowTable, IDH_DISPROW._UOM, sUOM, False, True)
                    'setDFValue(oForm, msRowTable, IDH_DISPROW._PUOM, sPUOM, False, True)
                    'setDFValue(oForm, msRowTable, IDH_DISPROW._ProUOM, sProUOM, False, True)

                    oDOR.U_UOM = sUOM
                    oDOR.U_PUOM = sPUOM
                    oDOR.U_ProUOM = sProUOM

                    Dim sUseWeight As String = getSharedData(oForm, "USEWGT")
                    If sUseWeight Is Nothing OrElse sUseWeight.Length = 0 Then
                        Dim dFactor As Double
                        If getWFValue(oForm, "FormMode") = "PO" Then
                            dFactor = Config.INSTANCE.doGetBaseWeightFactors(sProUOM)
                        Else
                            dFactor = Config.INSTANCE.doGetBaseWeightFactors(sUOM)

                        End If

                        If dFactor < 0 Then
                            'setDFValue(oForm, msRowTable, IDH_DISPROW._UseWgt, "2")
                            oDOR.U_UseWgt = "2"
                        Else
                            'setDFValue(oForm, msRowTable, IDH_DISPROW._UseWgt, "1")
                            oDOR.U_UseWgt = "1"
                        End If
                    Else
                        'setDFValue(oForm, msRowTable, IDH_DISPROW._UseWgt, sUseWeight)
                        oDOR.U_UseWgt = sUseWeight
                    End If

                    ''Set The Additional UOM Weights
                    Dim dTAUOMQT As Double = getSharedData(oForm, "TAUOMQT")
                    Dim dPAUOMQT As Double = getSharedData(oForm, "PAUOMQT")
                    Dim dAUOMQT As Double = getSharedData(oForm, "AUOMQT")
                    'setDFValue(oForm, msRowTable, IDH_DISPROW._TAUOMQt, dTAUOMQT, False, True)
                    'setDFValue(oForm, msRowTable, IDH_DISPROW._PAUOMQt, dPAUOMQT, False, True)
                    'setDFValue(oForm, msRowTable, IDH_DISPROW._AUOMQt, dAUOMQT, False, True)
                    oDOR.U_TAUOMQt = dTAUOMQT
                    oDOR.U_PAUOMQt = dPAUOMQT
                    oDOR.U_AUOMQt = dAUOMQT

                    'Set The Weights
                    Dim dTipWgt As Double = getSharedData(oForm, "TIPWGT")
                    Dim dProWgt As Double = getSharedData(oForm, "PROWGT")
                    Dim dCstWgt As Double = getSharedData(oForm, "QTY")
                    'setDFValue(oForm, msRowTable, IDH_DISPROW._TipWgt, dTipWgt, False, True)
                    'setDFValue(oForm, msRowTable, IDH_DISPROW._ProWgt, dProWgt, False, True)
                    'setDFValue(oForm, msRowTable, IDH_DISPROW._CstWgt, dCstWgt, False, True)
                    oDOR.U_TipWgt = dTipWgt
                    oDOR.U_ProWgt = dProWgt
                    oDOR.U_CstWgt = dCstWgt

                    Dim dDisposalCharge As Double = getSharedData(oForm, "DISPCHRG")
                    Dim dDisposalCost As Double = getSharedData(oForm, "DISPCST")
                    Dim dHaulCost As Double = getSharedData(oForm, "HAULCST")
                    Dim dProCost As Double = getSharedData(oForm, "PRODCST")
                    'setDFValue(oForm, msRowTable, IDH_DISPROW._TCharge, dDisposalCharge)
                    'setDFValue(oForm, msRowTable, IDH_DISPROW._TipCost, dDisposalCost)
                    'setDFValue(oForm, msRowTable, IDH_DISPROW._OrdCost, dHaulCost)
                    'setDFValue(oForm, msRowTable, IDH_DISPROW._PCost, dProCost)
                    oDOR.U_TCharge = dDisposalCharge
                    oDOR.U_TipCost = dDisposalCost
                    oDOR.U_OrdCost = dHaulCost
                    oDOR.U_PCost = dProCost

                    'If sJobType IsNot Nothing AndAlso sJobType.Length > 0 Then
                    '    Dim sNewJopbType As String = ""
                    '    '20150218 - LPV - BEGIN - Change order to always fall back on incoming
                    '    If Config.INSTANCE.isOutgoingJob(sContainerType, sJobType) Then
                    '        setDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, Config.INSTANCE.doGetJopTypeOutgoing())
                    '    Else
                    '        setDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, Config.INSTANCE.doGetJopTypeIncoming())
                    '    End If

                    '    'If Config.INSTANCE.isIncomingJob(sContainerType, sJobType) Then
                    '    '    setDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, Config.INSTANCE.doGetJopTypeIncoming())
                    '    'Else
                    '    '    setDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, Config.INSTANCE.doGetJopTypeOutgoing())
                    '    'End If
                    '    '20150218 - LPV - END
                    'End If

                    '##Start 20-06-2014' change doCalulateTotal position and changed getAutoAdditionalCodes
                    'thisDOR(oForm).doCalculateTotals()

                    'getAutoAdditionalCodes(oForm, sRow)
                    thisDOR(oForm).doCalculateTotals(IDH_DISPROW.TOTAL_CALCULATE)
                    '##End 20-06-2014
                    doAddAutoAdditionalCodes(oForm)

                    '## MA Start 08-09-2015 Issue#913:
                    If oDOR.U_WasCd.Length > 0 Then '' this was the bug anb fixed during working on CR 909: Alternate Description was not refreshing 
                        doFillAlternateItemDescription(oForm, oDOR.U_AltWasDsc)
                    End If
                    doBPForecast(oForm)
                    '## MA End 08-09-2015 Issue#913:
                    'End If
                Catch ex As Exception
                    Throw ex
                Finally
                    oDOR.BlockChangeNotif = False
                End Try
            End If
        End Sub

        Protected Overrides Sub doReturnCanceled(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String)
            Try
                If sModalFormType = "IDHLOSCH" Then
                    setWFValue(oForm, "LastVehAct", "DOSEARCH")
                    'setWFValue(oForm, "LastVehAct", "CANCELED")
                    setDFValue(oForm, msRowTable, "U_LorryCd", "")
                    setDFValue(oForm, msRowTable, "U_Lorry", "")

                    doSetFocus(oForm, "IDH_VEHREG")
                ElseIf sModalFormType = "IDHLNKWO" Then
                    setWFValue(oForm, "LastVehAct", "DOSEARCH")
                    setDFValue(oForm, msRowTable, "U_WROrd", "", True)
                    setDFValue(oForm, msRowTable, "U_WRRow", "", True)
                    doSetFocus(oForm, "IDH_VEHREG")
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error handeling the cancel.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHCAN", {Nothing})
            End Try
        End Sub

        Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            If oData Is Nothing Then Exit Sub
            Dim oArr As ArrayList = oData
            If oArr.Count = 0 Then
                Return
            End If
            oForm.Update()
        End Sub

        Private Sub doUseTarreWeight(ByVal oForm As SAPbouiCOM.Form)
        End Sub


        Private Sub doLoad2ndWeigh(ByVal oForm As SAPbouiCOM.Form,
           ByVal sJobNr As String,
           ByVal sJobRow As String)
            setFormDFValue(oForm, "Code", sJobNr)
            setDFValue(oForm, msRowTable, "Code", sJobRow)

            setWFValue(oForm, "IDH_LoadSecondWeigh", True)
            oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE

            ''Now Refresh the form data
            Dim db As SAPbouiCOM.DBDataSource
            Dim oConditions As SAPbouiCOM.Conditions = New SAPbouiCOM.Conditions
            Dim oCondition As SAPbouiCOM.Condition = oConditions.Add

            db = oForm.DataSources.DBDataSources.Item("@IDH_DISPORD")
            db.Clear()
            oCondition.Alias = "Code"
            oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
            oCondition.CondVal = sJobNr
            db.Query(oConditions)

            doLoadData(oForm)

            '            Dim sObligated As String = getDFValue(goParent, msRowTable, "U_Obligated")
            '            Dim sCardCode As String = getDFValue(goParent, msMDHead, "U_CardCd")
            '            If sObligated Is Nothing Then
            '            	sObligated = Config.INSTANCE.getBPOligated(goParent, sCardCode )
            '            End If
            '            setDFValue(goParent, msRowTable, "U_Obligated", sObligated)

            'Find a way to switch to the fisrt 0 second weigh
            oForm.Title = getTranslatedCaption("#TITLE", "Disposal Order") & " - " & getTranslatedWord("Second Weigh")
            ''## MA Start 16-09-2014 Issue#401
            If isItemEnabled(oForm, "IDH_WEI2") Then
                doSetFocus(oForm, "IDH_WEI2")
            End If
            ''## MA End 16-09-2014 Issue#401
        End Sub

        Private Sub doSetVehicleReg(ByVal oForm As SAPbouiCOM.Form,
         ByVal sReg As String,
         ByVal sVehCode As String,
         ByVal sJob As String,
         Optional ByVal bDoReadShared As Boolean = True,
         Optional ByVal bDoFind As Boolean = True)

            Dim oDO As IDH_DISPORD = thisDO(oForm)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

            Dim sOrigin As String = Nothing

            '** Check if linked to WO
            'Dim sWORD As String = getDFValue(oForm, msRowTable, "U_WROrd")
            'Dim sWRow As String = getDFValue(oForm, msRowTable, "U_WRRow")
            Dim sWORD As String = oDOR.U_WROrd
            Dim sWRow As String = oDOR.U_WRRow

            Dim bDoSetData As Boolean = True
            If String.IsNullOrWhiteSpace(sWORD) = False Then
                bDoSetData = False
                setDFValue(oForm, msRowTable, "U_Lorry", sReg, True)
                'setDFValue(oForm, msRowTable, "U_LorryCd", sVehCode)
                oDOR.U_LorryCd = sVehCode
                Exit Sub
            End If

            '            sJob = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)
            If String.IsNullOrWhiteSpace(sJob) Then
                Exit Sub
            End If

            Dim sCustomerCode As String = ""
            Dim sOrder As String = String.Empty
            Dim sJbTp As String = String.Empty
            If bDoFind = True Then
                sOrder = getDFValue(oForm, "@IDH_DISPORD", "Code", True)
                sJbTp = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, True)

                'sCustomerCode = getFormDFValue(oForm, IDH_DISPORD._CardCd)
                sCustomerCode = oDO.U_CardCd

                Dim bResult As Boolean = doFindInternal(oForm, "VEH", sOrder, sCustomerCode, sReg, sVehCode, sJbTp)
                If bResult Then
                    oDOR.BlockChangeNotif = True
                    Try
                        oDOR.doGetAllUOM()
                    Catch e As Exception

                    End Try
                    oDOR.BlockChangeNotif = False
                    oDOR.doGetChargePrices(False)
                    oDOR.doGetCostPrices(True, True, True, False)
                    doAddAutoAdditionalCodes(oForm)

                    '201511: Code added to auto-add BP Additional Charges 
                    'Remove and Add BP Ad. Charge rows 
                    If Config.INSTANCE.getParameterAsBool("USEBPACH", False) Then
                        'Remove any existing AutoAdd rows 
                        'This is we dont know any items being added for this BP previously 
                        removeAutoAddBPAddCharges(oForm)
                        'Now add any new AddCharges row 
                        getBPAdditionalCharges(oForm)
                    End If
                    Exit Sub
                End If
            Else
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                    'setFormDFValue(oForm, "Code", "")
                    oDO.Code = ""
                    doLoadData(oForm)
                End If

                setUFValue(oForm, "IDH_TARWEI", 0)
                setUFValue(oForm, "IDH_TRLTar", 0)
                ''MA Start 05-02-2015 Issue#554
                setUFValue(oForm, "IDH_TRWTE1", "")
                setUFValue(oForm, "IDH_TRWTE2", "")
                ''MA End 05-02-2015 Issue#554
                oForm.Title = getTranslatedCaption("#TITLE", "Disposal Order") & " - " & getTranslatedWord("First Weigh")
            End If

            If Not sJob Is Nothing AndAlso sJob.Length > 0 Then
                setDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, sJob, True)

                If Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), sJob) Then
                    doShowLoadSheet(oForm)
                Else
                    doHideLoadSheet(oForm)
                End If
            End If

            setDFValue(oForm, msRowTable, "U_Lorry", sReg, True)
            setDFValue(oForm, msRowTable, "U_LorryCd", sVehCode)

            'oDOR.U_LorryCd = sVehCode

            Dim sWasteCarrierCode As String = ""
            Dim sWasteCarrierName As String = ""
            Dim sCustomerName As String = ""
            Dim sDisposalSiteCode As String = ""
            Dim sDisposalSiteName As String = ""

            Dim sCustomerAddress As String = Nothing
            Dim sDisposalAddress As String = Nothing

            Dim bSetCustomerAddress As Boolean = True
            Dim bSetDisposalAddress As Boolean = True
            Dim bSetProducerAddress As Boolean = True
            'Dim bDoCustomer As Boolean = True
            Dim sWasteCode As String = Nothing

            If bDoReadShared = True Then
                Try
                    oDOR.BlockChangeNotif = True
                    sWasteCode = getSharedData(oForm, "WASCD")

                    setWFValue(oForm, "VEHTYPE", getSharedData(oForm, "VTYPE"))

                    'setDFValue(oForm, msRowTable, "U_VehTyp", getSharedData(oForm, "VEHDESC"))
                    'setDFValue(oForm, msRowTable, "U_Driver", getSharedData(oForm, "DRIVER"))
                    oDOR.U_VehTyp = getSharedData(oForm, "VEHDESC")
                    oDOR.U_Driver = getSharedData(oForm, "DRIVER")

                    setUFValue(oForm, "IDH_TARWEI", getSharedData(oForm, "TARRE"))
                    setUFValue(oForm, "IDH_TRLTar", getSharedData(oForm, "TRLTAR"))

                    ''MA Start 05-02-2015 Issue#554
                    Dim dtDatetemp = getSharedData(oForm, "TRWTE1")
                    If dtDatetemp IsNot Nothing Then
                        setUFValue(oForm, "IDH_TRWTE1", com.idh.utils.Dates.doDateToSBODateStr(getSharedData(oForm, "TRWTE1")))
                    Else
                        setUFValue(oForm, "IDH_TRWTE1", "")
                    End If
                    dtDatetemp = getSharedData(oForm, "TRWTE2")

                    If dtDatetemp IsNot Nothing Then
                        setUFValue(oForm, "IDH_TRWTE2", com.idh.utils.Dates.doDateToSBODateStr(getSharedData(oForm, "TRWTE2")))
                    Else
                        setUFValue(oForm, "IDH_TRWTE2", "")
                    End If
                    ''MA End 05-02-2015 Issue#554

                    Dim sContainer As String = getSharedData(oForm, "ITEMCD")
                    If String.IsNullOrWhiteSpace(sContainer) = False Then
                        'setDFValue(oForm, msRowTable, "U_ItemCd", sContainer)
                        'setDFValue(oForm, msRowTable, "U_ItemDsc", getSharedData(oForm, "ITEMDSC"))
                        oDOR.U_ItemCd = sContainer
                        oDOR.U_ItemDsc = getSharedData(oForm, "ITEMDSC")
                    End If

                    'setDFValue(oForm, msRowTable, "U_WasCd", getSharedData(oForm, "WASCD"))
                    If String.IsNullOrWhiteSpace(sWasteCode) = False Then
                        'setDFValue(oForm, msRowTable, "U_WasCd", sWasteCode)
                        'setDFValue(oForm, msRowTable, "U_WasDsc", getSharedData(oForm, "WASDSC"))
                        oDOR.U_WasCd = sWasteCode
                        oDOR.U_WasDsc = getSharedData(oForm, "WASDSC")
                    End If

                    'setDFValue(oForm, msRowTable, "U_TRLReg", getSharedData(oForm, "TRLREG"))
                    'setDFValue(oForm, msRowTable, "U_TRLNM", getSharedData(oForm, "TRLNM"))
                    oDOR.U_TRLReg = getSharedData(oForm, "TRLREG")
                    oDOR.U_TRLNM = getSharedData(oForm, "TRLNM")

                    'Dim sWOLink As String = getDFValue(oForm, msRowTable, "U_WROrd")
                    If oDOR.TRNCdDecodeRecord IsNot Nothing Then
                        If oDOR.IsLinkedToOrder() = False Then 'sWOLink Is Nothing OrElse sWOLink.Length = 0 Then
                            'Dim sMarkAct As String = getSharedData(oForm, "MARKAC")
                            'If sMarkAct Is Nothing Then
                            '    sMarkAct = com.idh.bridge.lookups.FixedValues.getReqFocStatus()
                            'End If


                            'If sMarkAct.Equals("DOFOC") OrElse sMarkAct.Equals(com.idh.bridge.lookups.FixedValues.getStatusFoc()) Then
                            '    doSwitchCheckBox(oForm, "IDH_FOC", "Y", True)
                            'ElseIf sMarkAct.Equals("DOORD") Then
                            '    doSwitchCheckBox(oForm, "IDH_DOORD", "Y", True)
                            'ElseIf sMarkAct.Equals("DOARI") Then
                            '    doSwitchCheckBox(oForm, "IDH_DOARI", "Y", True)
                            'ElseIf sMarkAct.Equals("DOARIP") Then
                            '    doSwitchCheckBox(oForm, "IDH_DOARIP", "Y", True)
                            'ElseIf sMarkAct.Equals("DOAINV") Then
                            '    doSwitchCheckBox(oForm, "IDH_AINV", "Y", True)
                            'Else

                            '    doSwitchCheckBox(oForm, "IDH", "N", True)
                            'End If
                        Else
                            doSwitchCheckBox(oForm, "IDH_FOC", "Y", True)
                            doSwitchCheckBox(oForm, "IDH_DOPO", "N", True)

                        End If
                    Else
                        ''11-05-2015 Start Issue#777
                        If oDOR.IsLinkedToOrder() = False Then 'sWOLink Is Nothing OrElse sWOLink.Length = 0 Then
                            Dim sMarkAct As String = getSharedData(oForm, "MARKAC")
                            If sMarkAct Is Nothing Then
                                sMarkAct = com.idh.bridge.lookups.FixedValues.getReqFocStatus()
                            End If


                            If sMarkAct.Equals("DOFOC") OrElse sMarkAct.Equals(com.idh.bridge.lookups.FixedValues.getStatusFoc()) Then
                                doSwitchCheckBox(oForm, "IDH_FOC", "Y", True)
                            ElseIf sMarkAct.Equals("DOORD") Then
                                doSwitchCheckBox(oForm, "IDH_DOORD", "Y", True)
                            ElseIf sMarkAct.Equals("DOARI") Then
                                doSwitchCheckBox(oForm, "IDH_DOARI", "Y", True)
                            ElseIf sMarkAct.Equals("DOARIP") Then
                                doSwitchCheckBox(oForm, "IDH_DOARIP", "Y", True)
                            ElseIf sMarkAct.Equals("DOAINV") Then
                                doSwitchCheckBox(oForm, "IDH_AINV", "Y", True)
                            Else

                                doSwitchCheckBox(oForm, "IDH", "N", True)
                            End If
                        Else
                            doSwitchCheckBox(oForm, "IDH_FOC", "Y", True)
                            doSwitchCheckBox(oForm, "IDH_DOPO", "N", True)
                        End If
                        ''11-05-2015 End Issue#777
                    End If

                    sCustomerCode = getSharedData(oForm, "CUSCD")
                    sCustomerName = getSharedData(oForm, "CUSTOMER")

                    sWasteCarrierCode = getSharedData(oForm, "WCCD")
                    sWasteCarrierName = getSharedData(oForm, "WCNAM")

                    If String.IsNullOrWhiteSpace(sWasteCarrierCode) = True Then
                        sWasteCarrierCode = getSharedData(oForm, "WCCD")
                        sWasteCarrierName = getSharedData(oForm, "WCNAM")
                    End If

                    If String.IsNullOrWhiteSpace(sWasteCarrierCode) = False Then
                        If Config.ParameterAsBool("VMUSENEW", False) Then
                            Dim oBP As LinkedBP = Config.INSTANCE.doGetLinkedBP(sWasteCarrierCode)
                            If (oBP IsNot Nothing) Then
                                sCustomerCode = oBP.LinkedBPCardCode
                            Else
                                sCustomerCode = ""
                                sCustomerName = ""
                            End If
                        End If
                    End If

                    If String.IsNullOrWhiteSpace(sCustomerCode) Then
                        sCustomerCode = getSharedData(oForm, "WCCD")
                    End If

                    'Override with newly looked up values
                    If Not String.IsNullOrWhiteSpace(sWasteCarrierCode) Then
                        sWasteCarrierName = Config.INSTANCE.doGetBPName(sWasteCarrierCode)
                    Else
                        sWasteCarrierName = ""
                    End If

                    'Override with newly looked up values
                    If Not String.IsNullOrWhiteSpace(sCustomerCode) Then
                        sCustomerName = Config.INSTANCE.doGetBPName(sCustomerCode)
                    Else
                        sCustomerName = ""
                    End If

                    Dim sVehDesc As String = getSharedData(oForm, "VEHDIS")
                    If String.IsNullOrWhiteSpace(sVehDesc) Then
                        sVehDesc = getSharedData(oForm, "VEHDESC")
                    End If
                    'setDFValue(oForm, IDH_DISPROW.TableName, IDH_DISPROW._VehTyp, sVehDesc)
                    oDOR.U_VehTyp = sVehDesc
                    ''End

                    sDisposalSiteCode = getSharedData(oForm, "SUPCD")
                    sDisposalSiteName = getSharedData(oForm, "SUPPLIER")

                    sCustomerAddress = getSharedData(oForm, "CADDRESS")
                    sDisposalAddress = getSharedData(oForm, "SADDRESS")

                    sOrigin = getSharedData(oForm, "ORIGIN")

                    '				If Config.INSTANCE.getParameterAsBool( "DOVMUC", True) Then
                    '					If sCustomerCode.Equals(sWasteCarrierCode) = False Then
                    '						bDoCustomer = False
                    '						setDFValue(oForm, "@IDH_DISPORD", "U_CardCd", "")
                    '                    	setDFValue(oForm, "@IDH_DISPORD", "U_CardNM", "")
                    '					End If
                    '				End If

                    'If bDoCustomer AndAlso Not sCustomerCode Is Nothing AndAlso sCustomerCode.Length > 0 Then
                    If String.IsNullOrWhiteSpace(sCustomerCode) = False Then
                        'setFormDFValue(oForm, IDH_DISPORD._CardCd, sCustomerCode)
                        'setFormDFValue(oForm, IDH_DISPORD._CardNM, sCustomerName)

                        'setDFValue(oForm, msRowTable, IDH_DISPROW._CustCd, sCustomerCode)
                        'setDFValue(oForm, msRowTable, IDH_DISPROW._CustNm, sCustomerName)
                        oDO.U_CardCd = sCustomerCode
                        oDO.U_CardNM = sCustomerName

                        Dim sUOM As String = Config.INSTANCE.getBPUOM(sCustomerCode)
                        If String.IsNullOrWhiteSpace(sUOM) = False Then
                            'setDFValue(oForm, msRowTable, "U_UOM", sUOM, True)
                            oDOR.U_UOM = sUOM
                        End If
                        ''## MA Start 16-10-2014
                        'doUpdateBPWeightFlag(oForm)
                        ''## MA End 16-10-2014

                        Dim sCs As String = Config.INSTANCE.getBPCs(sCustomerCode)
                        If String.IsNullOrWhiteSpace(sCs) = False Then
                            'setDFValue(oForm, msRowTable, "U_CustCs", sCs, True)
                            oDOR.U_CustCs = sCs
                        End If

                        '## Start 23-06-2014
                        Dim sCustWasLicNo As String = Config.INSTANCE.getValueFromBP(sCustomerCode, "U_WasLic")
                        'setFormDFValue(oForm, "U_WasLic", sCustWasLicNo)
                        oDO.U_WasLic = sCustWasLicNo
                        '## End 23-06-2014

                        Dim sObligated As String
                        sObligated = Config.INSTANCE.getBPOligated(sCustomerCode)
                        'setDFValue(oForm, msRowTable, "U_Obligated", sObligated)
                        oDOR.U_Obligated = sObligated

                        Dim oData As ArrayList = Nothing
                        If String.IsNullOrWhiteSpace(sCustomerAddress) = False Then
                            Dim sPh1 As String = ""
                            'sPh1 = getDFValue(oForm, "@IDH_DISPORD", "U_Phone1")

                            oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sCustomerCode, "S", sCustomerAddress)
                            FilterRestrictedAddress(oData, sCustomerCode)
                            If Not oData Is Nothing AndAlso oData.Count > 0 Then
                                doSetCustomerAddress(oForm, oData, sCustomerCode, sPh1)
                                bSetCustomerAddress = False
                                'If sOrigin Is Nothing OrElse sOrigin.Length = 0 Then
                                '
                                'End If
                            End If
                        End If

                        If Config.INSTANCE.getParameterAsBool("DOVMSCAP", True) = True Then
                            'setFormDFValue(oForm, "U_PCardCd", sCustomerCode)
                            'setFormDFValue(oForm, "U_PCardNM", sCustomerName)

                            'setDFValue(oForm, msRowTable, "U_ProCd", sCustomerCode)
                            'setDFValue(oForm, msRowTable, "U_ProNm", sCustomerName)

                            oDO.U_PCardCd = sCustomerCode
                            oDO.U_PCardNM = sCustomerName

                            If Not oData Is Nothing AndAlso oData.Count > 0 Then
                                doSetProducerAddress(oForm, oData, False)
                                bSetProducerAddress = False
                            End If
                        End If
                    End If

                    If String.IsNullOrWhiteSpace(sWasteCarrierCode) = False Then
                        'setFormDFValue(oForm, "U_CCardCd", sWasteCarrierCode)
                        'setFormDFValue(oForm, "U_CCardNM", sWasteCarrierName)

                        oDO.U_CCardCd = sWasteCarrierCode
                        oDO.U_CCardNM = sWasteCarrierName

                        ''Update all the linked rows values
                        Dim oParams As Hashtable = New Hashtable()
                        oParams.Add("U_CarrCd", sWasteCarrierCode)
                        oParams.Add("U_CarrNm", sWasteCarrierName)
                        '## Start 23-06-2014
                        Dim sCarrWasLicNo As String = Config.INSTANCE.getValueFromBP(sWasteCarrierCode, "U_WasLic")
                        'setFormDFValue(oForm, "U_CWasLic", sCarrWasLicNo)
                        oDO.U_CWasLic = sCarrWasLicNo
                        '## End 23-06-2014
                        setAllRowsValues(oForm, oParams)
                    End If

                    If String.IsNullOrWhiteSpace(sDisposalSiteCode) = False Then
                        'setFormDFValue(oForm, "U_SCardCd", sDisposalSiteCode)
                        'setFormDFValue(oForm, "U_SCardNM", sDisposalSiteName)

                        oDO.U_SCardCd = sDisposalSiteCode
                        oDO.U_SCardNM = sDisposalSiteName

                        If String.IsNullOrWhiteSpace(sDisposalAddress) = False Then
                            Dim oData As ArrayList
                            oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sDisposalSiteCode, "S", sDisposalAddress)
                            ''##MA Start 23-09-2014 Issue#403
                            setSharedData(oForm, "IDH_APPLYFLTR", Config.INSTANCE.getParameterAsBool("FLTRADDR", False).ToString)
                            ''##MA End 23-09-2014 Issue#403
                            If Not oData Is Nothing AndAlso oData.Count > 0 Then
                                doSetSiteAddress(oForm, oData)
                                bSetDisposalAddress = False
                            End If
                        End If
                    End If

                    Dim sTstWasteCode As String = getDFValue(oForm, msRowTable, "U_WasCd")
                    Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)
                    If Config.INSTANCE.doCheckCanPurchaseWB(sTstWasteCode) _
                         AndAlso Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) Then
                        'AndAlso getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp).Equals(Config.INSTANCE.doGetJopTypeIncoming()) Then
                        doSwitchToPO(oForm, True)
                        bSetProducerAddress = False
                    Else
                        doSwitchToSO(oForm)
                    End If
                Catch ex As Exception
                    Throw ex
                Finally
                    oDOR.BlockChangeNotif = False
                End Try

                doSwitchGenButton(oForm)

                doAddAutoAdditionalCodes(oForm)
                If Config.INSTANCE.getParameterAsBool("USEBPACH", False) Then
                    'Remove and Add BP Ad. Charge rows 
                    removeAutoAddBPAddCharges(oForm)
                    getBPAdditionalCharges(oForm)
                End If
            End If

            doSetDOAddDefaults(oForm, False, bSetCustomerAddress, True, bSetDisposalAddress, bSetProducerAddress)
            Dim bDoEnableWOLink As Boolean = True
            Dim oItems() As String = {"IDH_WRORD", "IDH_WRROW"}
            setEnableItems(oForm, bDoEnableWOLink, oItems)

            Dim sWasCd As String = getDFValue(oForm, msRowTable, "U_WasCd")
            If sWasCd IsNot Nothing AndAlso sWasCd.Length > 0 Then
                'Trigger doMarkUserUpdate 
                oDOR.doMarkUserUpdate(IDH_DISPROW._WasCd)
            End If

            If Not sOrigin Is Nothing AndAlso sOrigin.Length > 0 Then
                'setOrigin(oForm, sOrigin)
                oDO.U_Origin = sOrigin
            End If

            If sWasteCode IsNot Nothing AndAlso sWasteCode.Length > 0 Then
                doFillAlternateItemDescription(oForm)
            End If
        End Sub

        Private Function doUsePrices(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim sLinkedWOR As String = getDFValue(oForm, msRowTable, "U_WRRow")
            If Not sLinkedWOR Is Nothing AndAlso sLinkedWOR.Length > 0 Then
                Return Config.INSTANCE.getParameterAsBool("DOPLLNK", True)
            End If
            Return True
        End Function

        Protected Overrides Sub doClickFirstFocus(ByVal oForm As SAPbouiCOM.Form)
            doSetFocusInFindMode(oForm)
            'doSetFocus(oForm, "IDH_WEIGH")
        End Sub

        Private Sub doClearRowKeepVehicle(ByVal oForm As SAPbouiCOM.Form)
            doClearRow(oForm, cf_KeepData.KEEP_VEHICLE)
        End Sub

        Private Sub doClearRowKeepMultiRow1(ByVal oForm As SAPbouiCOM.Form)
            doClearRow(oForm, cf_KeepData.KEEP_MDATA1)
        End Sub

        Private Sub doClearRow(ByVal oForm As SAPbouiCOM.Form, Optional ByVal cfKeepVehicle As cf_KeepData = cf_KeepData.KEEP_NONE)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            oDOR.doClearBuffers()

            Dim sJobType As String = Nothing
            Dim sLorry As String = ""
            Dim sVehCode As String = ""
            Dim sVehType As String = Nothing
            Dim sTRLReg As String = Nothing
            Dim sTRLNM As String = Nothing
            Dim sDriver As String = Nothing
            Dim dTarWei As Double
            Dim dTRLTar As Double
            Dim dtTRWTE1 As String = Nothing 'tare 1 date
            Dim dtTRWTE2 As String = Nothing 'tare 2 date

            Dim sCustCs As String = ""
            Dim sWastTNN As String = ""
            Dim sConNum As String = ""
            Dim sSiteRef As String = ""
            Dim sExtWeig As String = ""
            Dim sContNr As String = ""
            Dim sSealNr As String = ""
            Dim sObligated As String = ""
            Dim sBranch As String = ""
            Dim sTZone As String = ""


            Dim sSAddress As String = ""
            Dim sSAddressLN As String = ""
            Dim sTranCode As String = ""

            If wasSetByUser(oForm, "IDH_BRANCH") Then
                sBranch = getDFValue(oForm, msRowTable, "U_Branch", True)
            Else
                sBranch = Config.INSTANCE.doGetCurrentBranch()
            End If

            If cfKeepVehicle = cf_KeepData.KEEP_VEHICLE Then
                sLorry = getDFValue(oForm, msRowTable, "U_Lorry", True)
                sVehCode = getDFValue(oForm, msRowTable, "U_LorryCd")

                sJobType = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, True)
                sVehType = getDFValue(oForm, msRowTable, "U_VehTyp")
                sTRLReg = getDFValue(oForm, msRowTable, "U_TRLReg")
                sTRLNM = getDFValue(oForm, msRowTable, "U_TRLNM")
                sDriver = getDFValue(oForm, msRowTable, "U_Driver")
                dTarWei = getUFValue(oForm, "IDH_TARWEI")
                dTRLTar = getUFValue(oForm, "IDH_TRLTar")
                ''MA Start 05-02-2015 Issue#554
                dtTRWTE1 = getUFValue(oForm, "IDH_TRWTE1")
                dtTRWTE2 = getUFValue(oForm, "IDH_TRWTE2")
                ''MA End 05-02-2015 Issue#554

                sSAddress = getDFValue(oForm, msRowTable, IDH_DISPROW._SAddress)
                sSAddressLN = getDFValue(oForm, msRowTable, IDH_DISPROW._SAddrsLN)
                sTranCode = getDFValue(oForm, msRowTable, IDH_DISPROW._TrnCode)
            ElseIf cfKeepVehicle = cf_KeepData.KEEP_MDATA1 Then
                sLorry = getDFValue(oForm, msRowTable, "U_Lorry", True)
                sVehCode = getDFValue(oForm, msRowTable, "U_LorryCd")

                sJobType = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, True)
                sVehType = getDFValue(oForm, msRowTable, "U_VehTyp")
                sTRLReg = getDFValue(oForm, msRowTable, "U_TRLReg")
                sTRLNM = getDFValue(oForm, msRowTable, "U_TRLNM")
                sDriver = getDFValue(oForm, msRowTable, "U_Driver")
                dTarWei = getUFValue(oForm, "IDH_TARWEI")
                dTRLTar = getUFValue(oForm, "IDH_TRLTar")
                ''MA Start 05-02-2015 Issue#554
                dtTRWTE1 = getUFValue(oForm, "IDH_TRWTE1")
                dtTRWTE2 = getUFValue(oForm, "IDH_TRWTE2")
                ''MA End 05-02-2015 Issue#554

                sCustCs = getDFValue(oForm, msRowTable, "U_CustCs", True)
                sWastTNN = getDFValue(oForm, msRowTable, "U_WastTNN", True)
                'sConNum = getDFValue(oForm, msRowTable, "U_ConNum", True)
                sSiteRef = getDFValue(oForm, msRowTable, "U_SiteRef", True)
                sExtWeig = getDFValue(oForm, msRowTable, "U_ExtWeig", True)
                sContNr = getDFValue(oForm, msRowTable, "U_ContNr", True)
                sSealNr = getDFValue(oForm, msRowTable, "U_SealNr", True)
                sObligated = getDFValue(oForm, msRowTable, "U_Obligated", True)

                sTZone = getDFValue(oForm, msRowTable, "U_TZone", True)

                sSAddress = getDFValue(oForm, msRowTable, IDH_DISPROW._SAddress)
                sSAddressLN = getDFValue(oForm, msRowTable, IDH_DISPROW._SAddrsLN)
                sTranCode = getDFValue(oForm, msRowTable, IDH_DISPROW._TrnCode)
            Else
                setWFValue(oForm, "LastVehAct", "DOSEARCH")
            End If

            Dim oData As SAPbouiCOM.DBDataSource
            oData = oForm.DataSources.DBDataSources.Add(msRowTable)
            oData.Clear()
            oData.InsertRecord(0)

            Dim oPreSelect As PreSelect = getWFValue(oForm, "PRESELECT")
            If oPreSelect IsNot Nothing Then
                If oPreSelect.WasteCode IsNot Nothing AndAlso oPreSelect.WasteCode.Length > 0 Then
                    setDFValue(oForm, msRowTable, IDH_DISPROW._WasCd, oPreSelect.WasteCode)
                    setDFValue(oForm, msRowTable, IDH_DISPROW._WasDsc, oPreSelect.WasteDescription)
                Else
                    Dim sDefDOWC As String = Config.ParameterWithDefault("DODEFWCD", "")
                    If sDefDOWC.Length > 0 Then
                        Dim sDefDOWCDesc As String = Config.INSTANCE.doGetItemDescription(sDefDOWC)
                        setDFValue(oForm, msRowTable, "U_WasCd", sDefDOWC)
                        setDFValue(oForm, msRowTable, "U_WasDsc", sDefDOWCDesc)
                    Else
                        setDFValue(oForm, msRowTable, "U_WasCd", "")
                        setDFValue(oForm, msRowTable, "U_WasDsc", "")
                    End If
                End If

                oDOR.U_TrnCode = oPreSelect.TransactionCode
                'setDFValue(oForm, msRowTable, IDH_DISPROW._TrnCode, oPreSelect.TransactionCode)
                oDOR.doCheckAndSetExpectedLoadWeight()
                If oPreSelect.VehReg IsNot Nothing AndAlso oPreSelect.VehReg.Length > 0 Then
                    Dim oVehicle As IDH_VEHMAS2 = New IDH_VEHMAS2()
                    If oVehicle.getData(IDH_VEHMAS2._VehReg & " = '" & oPreSelect.VehReg & "'", Nothing) > 0 Then
                        setDFValue(oForm, msRowTable, IDH_DISPROW._LorryCd, oVehicle.Code)
                        setDFValue(oForm, msRowTable, IDH_DISPROW._Lorry, oVehicle.U_VehReg)

                        setDFValue(oForm, msRowTable, IDH_DISPROW._VehTyp, oVehicle.U_VehT)
                        ''setDFValue(oForm, msRowTable, IDH_DISPROW._TRLReg, oVehicle.U_)
                        ''setDFValue(oForm, msRowTable, IDH_DISPROW._TRLNM)
                        setDFValue(oForm, msRowTable, IDH_DISPROW._Driver, oVehicle.U_Driver)

                        setUFValue(oForm, "IDH_TARWEI", oVehicle.U_Tarre)
                        ''MA Start 05-02-2015 Issue#554
                        setUFValue(oForm, "IDH_TRWTE1", oVehicle.U_TRWTE1)
                        ''MA End 05-02-2015 Issue#554
                        ''setUFValue(oForm, "IDH_TRLTar")
                    Else
                        setDFValue(oForm, msRowTable, IDH_DISPROW._Lorry, oPreSelect.VehReg)
                    End If
                End If

                If oDOR.TRNCdDecodeRecord.JobType IsNot Nothing AndAlso oDOR.TRNCdDecodeRecord.JobType.Length > 0 Then
                    sJobType = oDOR.TRNCdDecodeRecord.JobType
                    setDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, sJobType, True)
                End If

                doApplyStatuses(oForm)
            Else
                doSwitchCheckBox(oForm, "IDH", "N", True)

                If String.IsNullOrWhiteSpace(sTranCode) Then
                    oDOR.U_TrnCode = sTranCode
                End If

                'Set default Material Code for Disposal Order. 
                Dim sDefDOWC As String = Config.ParameterWithDefault("DODEFWCD", "")
                If sDefDOWC.Length > 0 Then
                    Dim sDefDOWCDesc As String = Config.INSTANCE.doGetItemDescription(sDefDOWC)
                    setDFValue(oForm, msRowTable, "U_WasCd", sDefDOWC)
                    setDFValue(oForm, msRowTable, "U_WasDsc", sDefDOWCDesc)
                Else
                    setDFValue(oForm, msRowTable, "U_WasCd", "")
                    setDFValue(oForm, msRowTable, "U_WasDsc", "")
                End If
                oDOR.doCheckAndSetExpectedLoadWeight()
                If cfKeepVehicle = cf_KeepData.KEEP_VEHICLE Then
                    setDFValue(oForm, msRowTable, "U_Lorry", sLorry, True)
                    setDFValue(oForm, msRowTable, "U_LorryCd", sVehCode)

                    setDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, sJobType, True)
                    setDFValue(oForm, msRowTable, "U_VehTyp", sVehType)
                    setDFValue(oForm, msRowTable, "U_TRLReg", sTRLReg)
                    setDFValue(oForm, msRowTable, "U_TRLNM", sTRLNM)
                    setDFValue(oForm, msRowTable, "U_Driver", sDriver)
                    setUFValue(oForm, "IDH_TARWEI", dTarWei)
                    setUFValue(oForm, "IDH_TRLTar", dTRLTar)
                    ''MA Start 05-02-2015 Issue#554
                    setUFValue(oForm, "IDH_TRWTE1", dtTRWTE1)
                    setUFValue(oForm, "IDH_TRWTE2", dtTRWTE2)
                    ''MA End 05-02-2015 Issue#554
                ElseIf cfKeepVehicle = cf_KeepData.KEEP_MDATA1 Then
                    setDFValue(oForm, msRowTable, "U_Lorry", sLorry, True)
                    setDFValue(oForm, msRowTable, "U_LorryCd", sVehCode)

                    setDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, sJobType, True)
                    setDFValue(oForm, msRowTable, "U_VehTyp", sVehType)
                    setDFValue(oForm, msRowTable, "U_TRLReg", sTRLReg)
                    setDFValue(oForm, msRowTable, "U_TRLNM", sTRLNM)
                    setDFValue(oForm, msRowTable, "U_Driver", sDriver)
                    setUFValue(oForm, "IDH_TARWEI", dTarWei)
                    setUFValue(oForm, "IDH_TRLTar", dTRLTar)
                    ''MA Start 05-02-2015 Issue#554
                    setUFValue(oForm, "IDH_TRWTE1", dtTRWTE1)
                    setUFValue(oForm, "IDH_TRWTE2", dtTRWTE2)
                    ''MA End 05-02-2015 Issue#554
                Else
                    setDFValue(oForm, msRowTable, "U_Lorry", "", True)
                    setDFValue(oForm, msRowTable, "U_LorryCd", "", True)
                    ''MA Start 10-02-2015'clear Veh tare weight/date after adding DO discuss with Hannah skype 10-02-2015
                    setUFValue(oForm, "IDH_TARWEI", 0)
                    setUFValue(oForm, "IDH_TRLTar", 0)

                    setUFValue(oForm, "IDH_TRWTE1", "")
                    setUFValue(oForm, "IDH_TRWTE2", "")
                    ''MA Start 10-02-2015
                End If
            End If

            setDFValue(oForm, msRowTable, "U_Branch", sBranch)

            '            setDFValue(oForm, msRowTable, "U_Lorry", "", True)
            '            setDFValue(oForm, msRowTable, "U_LorryCd", "", True)

            setDFValue(oForm, msRowTable, "U_CustCs", sCustCs)
            setDFValue(oForm, msRowTable, "U_WastTNN", sWastTNN)
            setDFValue(oForm, msRowTable, "U_ConNum", sConNum)
            setDFValue(oForm, msRowTable, "U_SiteRef", sSiteRef)
            setDFValue(oForm, msRowTable, "U_ExtWeig", sExtWeig)
            setDFValue(oForm, msRowTable, "U_ContNr", sContNr)
            setDFValue(oForm, msRowTable, "U_SealNr", sSealNr)
            setDFValue(oForm, msRowTable, "U_Obligated", sObligated)

            setDFValue(oForm, msRowTable, "U_User", goParent.gsUserName)
            setDFValue(oForm, msRowTable, "U_TZone", sTZone)
            setDFValue(oForm, msRowTable, "U_Origin", getDFValue(oForm, "@IDH_DISPORD", "U_Origin"))
            setDFValue(oForm, msRowTable, IDH_DISPROW._FTOrig, "")
            setDFValue(oForm, msRowTable, IDH_DISPROW._SAddress, sSAddress)

            setDFValue(oForm, msRowTable, IDH_DISPROW._SAddrsLN, sSAddressLN)
            ''setDFValue(oForm, msRowTable, IDH_DISPROW._TrnCode, "")
            setDFValue(oForm, msRowTable, "U_UseWgt", "1", True)
            setDFValue(oForm, msRowTable, "U_Wei1", "0", True)
            setDFValue(oForm, msRowTable, "U_Wei2", "0", True)
            setDFValue(oForm, msRowTable, "U_MANPRC", "0", True)
            'setDFValue(oForm, msRowTable, "U_Status", com.idh.bridge.lookups.FixedValues.getStatusOpen())
            setDFValue(oForm, msRowTable, "U_TCharge", "0", True)
            setDFValue(oForm, msRowTable, "U_AUOMQt", "0", True)

            setDFValue(oForm, msRowTable, "U_RowSta", com.idh.bridge.lookups.FixedValues.getStatusOpen())

            setDFValue(oForm, msRowTable, "U_ProWgt", "0", True)
            setDFValue(oForm, msRowTable, "U_PCost", "0", True)
            setDFValue(oForm, msRowTable, "U_PCTotal", "0", True)
            setDFValue(oForm, msRowTable, "U_JCost", "0", True)

            setDFValue(oForm, msRowTable, "U_WROrd", "", True)
            setDFValue(oForm, msRowTable, "U_WRRow", "", True)
            setDFValue(oForm, msRowTable, "U_LoadSht", "", True)

            setDFValue(oForm, msRowTable, "U_MDChngd", "", True)
            ''MA Start 02-12-2015 Issue#1007
            ''setDFValue(oForm, msRowTable, IDH_DISPROW._CarWgt, "0", True)
            ''MA End 02-12-2015 Issue#1007
            setUFValue(oForm, "IDH_AVDOC", "N")
            setUFValue(oForm, "IDH_AVCONV", "N")
            setUFValue(oForm, "IDH_SUBTOT", "0")
            setUFValue(oForm, "IDH_NOVAT", "N")

            ''## MA Start 15-04-2014
            setUFValue(oForm, "IDH_ADDCO", "0")
            setUFValue(oForm, "IDH_ADDCH", "0")
            ''## MA End 15-04-2014
            '** Set the Stock Movement Flag
            If oPreSelect Is Nothing Then
                If String.IsNullOrWhiteSpace(sTranCode) = False Then
                    oDOR.U_TrnCode = sTranCode
                Else
                    Dim bDoStock As Boolean = Config.INSTANCE.getParameterAsBool("DOAUTSM")
                    If bDoStock Then
                        'setUFValue(oForm, "IDH_BOOKIN", "Y")
                        setDFValue(oForm, msRowTable, "U_BookIn", "B", True)
                        doSwitchCheckBox(oForm, "IDH_BOOKIN", "Y", False)
                    Else
                        'setUFValue(oForm, "IDH_BOOKIN", "N")
                        setDFValue(oForm, msRowTable, "U_BookIn", "N", True)
                        doSwitchCheckBox(oForm, "IDH_BOOKIN", "N", False)
                    End If
                End If
            End If

            setDFValue(oForm, msRowTable, "U_IsTrl", "N", True)

            'setDFValue(oForm, msRowTable, "U_BookIn", "", True)

            setWFValue(oForm, "IDH_CCTChr", "VARIABLE")
            setDFValue(oForm, msRowTable, "U_AUOM", msDefaultAUOM)

            Dim sUOM As String = Config.INSTANCE.getDefaultUOM()
            setDFValue(oForm, msRowTable, "U_UOM", sUOM) ''"Kg")
            setDFValue(oForm, msRowTable, "U_PUOM", sUOM) ''"Kg")
            setDFValue(oForm, msRowTable, "U_ProUOM", sUOM) ''"Kg")

            'setDFValue(oForm, msRowTable, "Code", "")
            setDFValue(oForm, msRowTable, "Code", "-1")

            'setWFValue(oForm, "IDH_VatPrc", 0)
            setDFValue(oForm, msRowTable, IDH_DISPROW._RdWgt, 0)
            setDFValue(oForm, msRowTable, IDH_DISPROW._TRdWgt, 0)
            setDFValue(oForm, msRowTable, IDH_DISPROW._PRdWgt, 0)
            setDFValue(oForm, msRowTable, "U_AdjWgt", 0)
            setDFValue(oForm, msRowTable, "U_ValDed", 0)
            setDFValue(oForm, msRowTable, "U_WgtDed", 0)
            setDFValue(oForm, msRowTable, "U_IsTrl", "N")

            setDFValue(oForm, msRowTable, "U_TChrgVtRt", 0)
            setDFValue(oForm, msRowTable, "U_HChrgVtRt", 0)
            setDFValue(oForm, msRowTable, "U_TChrgVtGrp", "")
            setDFValue(oForm, msRowTable, "U_HChrgVtGrp", "")

            setDFValue(oForm, msRowTable, "U_TCostVtRt", 0)
            setDFValue(oForm, msRowTable, "U_PCostVtRt", 0)
            setDFValue(oForm, msRowTable, "U_HCostVtRt", 0)
            setDFValue(oForm, msRowTable, "U_TCostVtGrp", "")
            setDFValue(oForm, msRowTable, "U_PCostVtGrp", "")
            setDFValue(oForm, msRowTable, "U_HCostVtGrp", "")

            setDFValue(oForm, msRowTable, "U_Covera", "")
            setDFValue(oForm, msRowTable, "U_CoverHst", "")
            ''## MA Start 16-10-2014
            setDFValue(oForm, msRowTable, "U_BPWgt", "0")
            setDFValue(oForm, msRowTable, "U_UseBPWgt", "")
            ''## MA End 16-10-2014

            ''			setDFValue(oForm, msRowTable, "U_LoadSht", "")

            setUFValue(oForm, "IDH_WEIG", "")

            doSetItemGroup(oForm)

            '** Set the Default Container to DISPOSAL
            Dim sContainer As String = Config.Parameter("MDDODE")
            setDFValue(oForm, msRowTable, "U_ItemCd", sContainer)

            '** get the Item Description
            If (Not sContainer Is Nothing AndAlso sContainer.Length > 0) Then
                Dim sDesc As String = Config.INSTANCE.doGetItemDescription(sContainer)
                setDFValue(oForm, msRowTable, "U_ItemDsc", sDesc)

                Dim sItemGroupCode As String = Config.LookupTableField("OITM", "ItmsGrpCod", "ItemCode = '" + sContainer + "'").ToString()
                setUFValue(oForm, "IDH_IGRP", sItemGroupCode)
                setDFValue(oForm, msRowTable, "U_ItmGrp", sItemGroupCode)
            End If

            'Dim sRSDate As String = getFormDFValue(oForm, "U_RSDate")
            'Dim oRSDate As DateTime = com.idh.utils.Dates.doStrToDate(sRSDate)
            'If com.idh.utils.Dates.isValidDate(oRSDate) Then
            'setDFValue(oForm, msRowTable, "U_RDate", oRSDate)
            'Else
            'setDFValue(oForm, msRowTable, "U_RDate", goParent.doDateToStr(Date.Now()))
            'End If

            Dim sDate As String = goParent.doDateToStr(Date.Now())
            Dim sTime As String = com.idh.utils.Dates.doTimeToNumStr(Nothing)
            setDFValue(oForm, msRowTable, "U_BDate", sDate) 'goParent.doDateToStr(Date.Now()))
            setDFValue(oForm, msRowTable, "U_BTime", sTime) 'com.idh.utils.Dates.doTimeToNumStr(Nothing))

            setDFValue(oForm, msRowTable, "U_RDate", sDate) 'goParent.doDateToStr(Date.Now()))
            setDFValue(oForm, msRowTable, "U_RTime", sTime) 'com.idh.utils.Dates.doTimeToNumStr(Nothing))

            'WRRow2 (was missing to save/update prior 1021)
            setDFValue(oForm, msRowTable, IDH_DISPROW._WRRow2, "")

            setDFValue(oForm, msRowTable, IDH_DISPROW._Sample, "")
            setDFValue(oForm, msRowTable, IDH_DISPROW._SampleRef, "")
            setDFValue(oForm, msRowTable, IDH_DISPROW._SampStatus, "")

            ''## MA Start 11-09-2014 Issue#406
            'If Config.INSTANCE.getParameter("CTOFJBHR") <> "" AndAlso Config.INSTANCE.getParameter("CTOFJBHR") <> "0" _
            '    AndAlso Config.INSTANCE.getParamaterWithDefault("MAXJOBHR", "0") <> "0" Then
            '    Dim cuttoffhrs As DateTime = Today.ToString("yyyy/MM/dd") & " " & Config.INSTANCE.getParameter("CTOFJBHR")
            '    Dim BTime As DateTime = DateTime.Now 'goParent.doStrToTime(oGridN.doGetFieldValue("U_BTime"))
            '    If BTime.TimeOfDay <= cuttoffhrs.TimeOfDay Then
            '        'MAXJOBHR 
            '        'Dim BDate As DateTime = Today 'goParent.doStrToDate(goParent.doDateToStr(oGridN.doGetFieldValue("U_BDate"), True))
            '        'BTime = BDate.Add(BTime.TimeOfDay)

            '        BTime = BTime.AddHours(CInt(Config.INSTANCE.getParamater("MAXJOBHR")))
            '        setDFValue(oForm, msRowTable, "U_JbToBeDoneTm", goParent.doTimeToNumStr(BTime))
            '        setDFValue(oForm, msRowTable, "U_JbToBeDoneDt", goParent.doDateToStr(BTime))
            '    Else
            '        BTime = "08:00" 'if BDate is beyond the cuttof time then set to 8AM of next Day( Hayden to be confirm from Mike)
            '        setDFValue(oForm, msRowTable, "U_JbToBeDoneTm", goParent.doTimeStrToInt(BTime))
            '        Dim BDate As DateTime = Today.AddDays(1) 'goParent.doStrToDate(goParent.doDateToStr(oGridN.doGetFieldValue("U_BDate"), True)).AddDays(1)
            '        setDFValue(oForm, msRowTable, "U_JbToBeDoneDt", goParent.doDateToStr(BDate))
            '    End If
            '    'Else
            'End If
            ''## MA End 11-09-2014 Issue#406
        End Sub

        '** set the PreSelect Data if it was set
        Protected Sub doSetPreselectData(ByVal oForm As SAPbouiCOM.Form)

            Dim oPreData As PreSelect = getWFValue(oForm, "PRESELECT")
            If oPreData IsNot Nothing Then
                doClearRow(oForm)

                'setFormDFValue(oForm, IDH_DISPORD._TRNCd, oPreData.TransactionCode)
                Dim oDO As IDH_DISPORD = thisDO(oForm)
                Dim oDOR As IDH_DISPROW = thisDOR(oForm)
                oDO.U_TRNCd = oPreData.TransactionCode

                Dim sBPCode As String = oPreData.BPCode
                If Not String.IsNullOrWhiteSpace(sBPCode) Then
                    If Config.INSTANCE.doCheckIsVendor(sBPCode) Then
                        'setFormDFValue(oForm, IDH_DISPORD._PCardCd, oPreData.BPCode)
                        'setFormDFValue(oForm, IDH_DISPORD._PCardNM, oPreData.BPName)
                        oDO.U_PCardCd = oPreData.BPCode
                        oDO.U_PCardNM = oPreData.BPName
                        doChooseProducer(oForm, True, "")
                    Else
                        'setFormDFValue(oForm, IDH_DISPORD._CardCd, oPreData.BPCode)
                        'setFormDFValue(oForm, IDH_DISPORD._CardNM, oPreData.BPName)
                        oDO.U_CardCd = oPreData.BPCode
                        oDO.U_CardNM = oPreData.BPName
                        doChooseCustomer(oForm, True, "")
                    End If
                End If
                ''SV 24/09/2014
                Dim sCarrier As String = oPreData.sCarrier
                If Not String.IsNullOrWhiteSpace(sCarrier) Then
                    'setFormDFValue(oForm, IDH_DISPORD._CCardCd, sCarrier)
                    oDO.U_CCardCd = sCarrier
                End If
                Dim sVehDesc As String = oPreData.sVehicleDescription
                If Not String.IsNullOrWhiteSpace(sVehDesc) Then
                    'setDFValue(oForm, msRowTable, IDH_DISPROW._VehTyp, sVehDesc)
                    oDOR.U_VehTyp = sVehDesc
                End If
                Dim sDriver As String = oPreData.sDriver
                If Not String.IsNullOrWhiteSpace(sDriver) Then
                    'setUFValue(oForm, IDH_DISPROW._Driver, sDriver)
                    oDOR.U_Driver = sDriver
                End If

                Dim sCustomer As String = oPreData.Customer
                If Not String.IsNullOrWhiteSpace(sCustomer) Then
                    'setUFValue(oForm, IDH_DISPROW._CustCd, sCustomer)
                    oDOR.U_CustCd = sCustomer
                End If

                Dim sBranch As String = oPreData.Branch
                If Not String.IsNullOrWhiteSpace(sBranch) Then
                    'setUFValue(oForm, IDH_DISPROW._Branch, sBranch)
                    oDOR.U_Branch = sBranch
                End If
                ''End
            End If
        End Sub

        Private Sub doNewRowAskAboutVehicle(ByVal oForm As SAPbouiCOM.Form)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

            Dim sCustCode As String = oDOR.U_CustCd
            Dim sSuppCode As String = oDOR.U_ProCd
            Dim sBPCodeToValidate As String = "-1"

            'Deciding Buying/Selling/Trading Disposal Order 
            If (sCustCode IsNot Nothing AndAlso sCustCode.Length > 0) And (sSuppCode = Nothing OrElse sSuppCode.Length = 0) Then
                'this is Selling DO 
                sBPCodeToValidate = sCustCode
            ElseIf (sSuppCode IsNot Nothing AndAlso sSuppCode.Length > 0) And (sCustCode = Nothing OrElse sCustCode.Length = 0) Then
                'this is Buying DO 
                sBPCodeToValidate = sSuppCode
            ElseIf (sCustCode IsNot Nothing AndAlso sCustCode.Length > 0) And (sSuppCode IsNot Nothing AndAlso sSuppCode.Length > 0) Then
                'this is Trading DO 
                sBPCodeToValidate = sCustCode
            Else
                'No validation required for Active/InActive BP 
            End If

            If sBPCodeToValidate <> "-1" Then
                Dim oCheckDate As DateTime = Date.Now() 'oDOR.U_BDate

                If Config.INSTANCE.doCheckBPActive(sBPCodeToValidate, oCheckDate) = False Then
                    'com.idh.bridge.DataHandler.INSTANCE.doUserError("The Requested date is in an Inactive Date Range: " + oCheckDate)
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("The Requested date is in an Inactive Date Range: " + oCheckDate, "ERUSIAD", {oCheckDate.ToString()})
                    Exit Sub
                End If
            End If

            Dim sTranCode As String = oDOR.U_TrnCode

            Dim sVehReg As String
            sVehReg = oDOR.U_Lorry 'getDFValue(oForm, msRowTable, "U_Lorry")
            If sVehReg.Length > 0 Then
                'If goParent.goApplication.M_essageBox("Is this another transaction on the same vehicle?", 1, "Yes", "No") = 1 Then

                '## Ma Start 02-07-2014; commencted the code and use Yes answer as option, with refrence to No 147 from Sheet
                'If goParent.doResourceMessageYN("DOVTC", Nothing, 1) = 1 Then
                Dim sMWFilter As String = Config.ParameterWithDefault("DOMCFLT", "0")
                If sMWFilter.Equals("1") Then
                    doClearRowKeepMultiRow1(oForm)
                Else
                    doClearRowKeepVehicle(oForm)
                End If
                'Else
                '   doClearRow(oForm)
                'End If
                '## Ma End 02-07-2014
            Else
                doClearRow(oForm)
            End If
            doSwitchToAddWeigh(oForm)
            doLoadGridForRow(oForm)
        End Sub

        Public Sub doRefreshAdditionalPrices(ByVal oForm As SAPbouiCOM.Form)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

            Dim sCardCode As String = oDOR.F_CustomerCode
            Dim sItemCode As String = oDOR.U_ItemCd
            Dim sItemGrp As String = oDOR.U_ItmGrp
            Dim sWastCd As String = oDOR.U_WasCd
            Dim sZipCode As String = oDOR.F_CustomerZipCode
            Dim sAddress As String = oDOR.F_CustomerAddress
            Dim sBranch As String = oDOR.U_Branch
            Dim sJobType As String = oDOR.U_JobTp

            '20150308 - LPV - BEGIN - avoid double conversion
            'Dim sDocDate As String = oDOR.U_BDate
            'Dim dDocDate As Date = com.idh.utils.Dates.doStrToDate(sDocDate)
            Dim dDocDate As Date = oDOR.U_BDate
            '20150308 - LPV - END - avoid double conversion

            Dim sUOM As String = oDOR.U_UOM
            Dim dWeight As Double = oDOR.U_CstWgt

            Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            If Not oGrid Is Nothing Then
                oGrid.doCalculateLinesChargePrices(sCardCode, sItemCode, sItemGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dWeight, sUOM, sZipCode)
                oGrid.doCalculateLinesCostPrices(sCardCode, sItemCode, sItemGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dWeight, sUOM, sZipCode)
            End If

            'Update
            doUpdateAutoAddBPAddCharges(oForm, Date.Now())
        End Sub

        '** Send By Custom Events
        'Return True done
        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            Dim oDO As IDH_DISPORD = thisDO(oForm)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

            If MyBase.doCustomItemEvent(oForm, pVal) = False Then
                Return False
            End If

            If pVal.ItemUID = "DEDGRID" Then
                If pVal.BeforeAction = True Then

                Else
                    ''LPVTEMP START
                    'Dim oGrid As WR1_Grids.idh.controls.grid.Deductions = WR1_Grids.idh.controls.grid.Deductions.getInstance(oForm, "DEDGRID")
                    ''LPVTEMP END

                    Dim oGrid As DBOGrid = getWFValue(oForm, "DEDGRID")
                    If Not oGrid Is Nothing Then
                        If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_ADD_EMPTY Then
                            'If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY Then
                            Dim iRow As Integer = pVal.Row
                            Dim sCode As String = getFormDFValue(oForm, "Code")
                            Dim sRowID As String = getDFValue(oForm, msRowTable, "Code")

                            oGrid.DBObject.doSetDefaultValue("U_JobNr", sCode)
                            oGrid.DBObject.doSetDefaultValue("U_RowNr", sRowID)

                            'oGrid.doSetFieldValue("U_JobNr", iRow, sCode)
                            'oGrid.doSetFieldValue("U_RowNr", iRow, sRowID)

                            ''LPVTEMP START
                            'oGrid.doSetFieldValue("U_Qty", iRow, 1)
                            'oGrid.doSetFieldValue("U_WgtDed", iRow, 0)
                            'oGrid.doSetFieldValue("U_ValDed", iRow, 0)
                            ''LPVTEMP END
                        ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED Then
                            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE _
                                    OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                doSetUpdate(oForm)
                            End If
                        ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SEARCH Then
                            Dim sCode As String = getFormDFValue(oForm, "Code")
                            Dim bIsNew As Boolean = oGrid.IsAddedRow(pVal.Row)
                            Dim iLast As Integer = oGrid.getLastRowIndex()

                            If bIsNew AndAlso iLast = pVal.Row AndAlso pVal.ColUID = IDH_DODEDUCT._ItemCd AndAlso pVal.oData = lookups.Base.en_DialogResults.CANCELED Then
                                oGrid.doSetFieldValue("Code", pVal.Row, "")
                                oGrid.doRemoveAddedRowFlag(pVal.Row)
                                Return False
                            End If
                        End If
                    End If
                End If
            ElseIf pVal.ItemUID = "ADDGRID" AndAlso pVal.BeforeAction = False Then
                Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                If Not oGrid Is Nothing Then
                    If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED Then
                        If pVal.BeforeAction = False Then
                            If pVal.ColUID = "U_CostVatGrp" OrElse
                               pVal.ColUID = "U_ItmCost" Then
                                oGrid.doCalculateCostLineTotals(pVal.Row)
                            ElseIf pVal.ColUID = "U_ChrgVatGrp" OrElse
                                    pVal.ColUID = "U_ItmChrg" Then
                                oGrid.doCalculateChargeLineTotals(pVal.Row)
                            ElseIf pVal.ColUID = "U_Quantity" Then
                                'oGrid.doCalculateLineTotals(pVal.Row)

                                Dim sSupplier As String = oGrid.doGetFieldValue("U_SuppCd")
                                'If String.IsNullOrWhiteSpace(sSupplier) OrElse sSupplier = "*" Then
                                'Return True
                                'End If

                                Dim iRow As Integer = pVal.Row
                                Dim sCardCode As String = oDOR.U_CustCd
                                Dim sItemCode As String = oDOR.U_ItemCd
                                Dim sItemGrp As String = oDOR.U_ItmGrp
                                Dim sWastCd As String = oDOR.U_WasCd
                                Dim sZipCode As String = getWFValue(oForm, "ZIPCODE")
                                Dim sAddress As String = getWFValue(oForm, "STADDR")
                                Dim sBranch As String = oDOR.U_Branch
                                Dim sJobType As String = oDOR.U_JobTp

                                Dim sDocDate As String = oDOR.U_RDate
                                Dim dDocDate As Date = com.idh.utils.Dates.doStrToDate(sDocDate)

                                Dim sUOM As String = oDOR.U_UOM 'oGrid.doGetFieldValue(IDH_WOADDEXP._UOM, iRow)
                                Dim dWeight As Double = oGrid.doGetFieldValue(IDH_DOADDEXP._Quantity, iRow) 'oWOR.U_CstWgt

                                oGrid.doCalculateLineChargePrices(sCardCode, sItemCode, sItemGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dWeight, sUOM, iRow, sZipCode)

                                If String.IsNullOrWhiteSpace(sSupplier) = False AndAlso sSupplier <> "*" Then
                                    oGrid.doCalculateLineCostPrices(sCardCode, sItemCode, sItemGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dWeight, sUOM, iRow, sZipCode)
                                End If
                            ElseIf pVal.ColUID = "U_ItmTCost" Then
                                ''## MA Start 14-04-2014
                                'Dim dCostT As Double = oGrid.doCalculateCostTotals()
                                'setUFValue(oForm, "IDH_ADDCO", dCostT)

                                'oDOR.U_TAddCost = dCostT
                                'oDOR.doCalculateCostTotals()

                                Dim dCostTWOVAT As Double = oGrid.doCalculateCostTotalsExcVAT()
                                setUFValue(oForm, "IDH_ADDCO", dCostTWOVAT)
                                Dim dCostVAT As Double = oGrid.doCalculateCostVAT()
                                setUFValue(oForm, "IDH_ADDCOV", dCostVAT)
                                setUFValue(oForm, "IDH_TADDCO", dCostTWOVAT + dCostVAT)
                                oDOR.U_AddCost = dCostTWOVAT
                                oDOR.U_TAddCsVat = dCostVAT
                                'LP Viljoen - not needed as the above will auto calculate this value - oDOR.U_TAddCost = dCostTWOVAT + dCostVAT
                                'LP Viljoen - not needed as the above will auto calculate this value - oDOR.doCalculateCostTotals()
                                ''## MA End 14-04-2014
                            ElseIf pVal.ColUID = "U_ItmTChrg" Then
                                ''## MA Start 14-04-2014
                                'Dim dChrgT As Double = oGrid.doCalculateChargeTotals()
                                'setUFValue(oForm, "IDH_ADDCH", dChrgT)

                                'oDOR.U_TAddChrg = dChrgT
                                'oDOR.doCalculateChargeTotal(True)
                                Dim dChgTWOVAT As Double = oGrid.doCalculateChargeTotalsExcVAT()
                                setUFValue(oForm, "IDH_ADDCH", dChgTWOVAT)
                                Dim dChargeVAT As Double = oGrid.doCalculateChargeVAT()
                                setUFValue(oForm, "IDH_ADDCHV", dChargeVAT)
                                setUFValue(oForm, "IDH_TADDCH", dChargeVAT + dChgTWOVAT)
                                oDOR.U_AddCharge = dChgTWOVAT
                                oDOR.U_TAddChVat = dChargeVAT
                                'LP Viljoen - not needed as the above will auto calculate this value - oDOR.U_TAddChrg = dChargeVAT + dChgTWOVAT
                                'incorrect Code must stick to the new Core flow (LP Viljoen) - oDOR.doCalculateChargeTotal(True)
                                'oDOR.doCalculateChargeVat("R")
                                '## MA 14-04-2014 End
                            ElseIf pVal.ColUID = "U_ItemCd" Then
                                If oGrid.SetFromSearch Then
                                    Dim iRow As Integer = pVal.Row
                                    Dim sCardCode As String = getDFValue(oForm, msRowTable, IDH_DISPROW._CustCd)
                                    Dim sItemCode As String = getDFValue(oForm, msRowTable, "U_ItemCd")
                                    Dim sItemGrp As String = getDFValue(oForm, msRowTable, "U_ItmGrp")
                                    Dim sWastCd As String = getDFValue(oForm, msRowTable, "U_WasCd")
                                    Dim sZipCode As String = getFormDFValue(oForm, "U_ZpCd")
                                    Dim sAddress As String = getFormDFValue(oForm, "U_Address")
                                    Dim sBranch As String = getDFValue(oForm, msRowTable, "U_Branch")
                                    Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)

                                    Dim sDocDate As String = getDFValue(oForm, msRowTable, "U_BDate")
                                    Dim dDocDate As Date = com.idh.utils.Dates.doStrToDate(sDocDate)

                                    Dim sUOM As String
                                    Dim dWeight As Double

                                    sUOM = getDFValue(oForm, msRowTable, "U_UOM")
                                    dWeight = getDFValue(oForm, msRowTable, "U_CstWgt")
                                    oGrid.doCalculateLineChargePrices(sCardCode, sItemCode, sItemGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dWeight, sUOM, iRow, sZipCode)
                                    oGrid.doCalculateLineCostPrices(sCardCode, sItemCode, sItemGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dWeight, sUOM, iRow, sZipCode)
                                End If
                            ElseIf pVal.ColUID = "U_SuppCd" Then
                                If oGrid.SetFromSearch Then
                                    Dim iRow As Integer = pVal.Row
                                    Dim sCardCode As String = getDFValue(oForm, msRowTable, IDH_DISPROW._CustCd)
                                    Dim sItemCode As String = getDFValue(oForm, msRowTable, "U_ItemCd")
                                    Dim sItemGrp As String = getDFValue(oForm, msRowTable, "U_ItmGrp")
                                    Dim sWastCd As String = getDFValue(oForm, msRowTable, "U_WasCd")
                                    Dim sZipCode As String = getFormDFValue(oForm, "U_ZpCd")
                                    Dim sAddress As String = getFormDFValue(oForm, "U_Address")
                                    Dim sBranch As String = getDFValue(oForm, msRowTable, "U_Branch")
                                    Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)

                                    Dim sDocDate As String = getDFValue(oForm, msRowTable, "U_BDate")
                                    Dim dDocDate As Date = com.idh.utils.Dates.doStrToDate(sDocDate)

                                    Dim sUOM As String
                                    Dim dWeight As Double

                                    sUOM = getDFValue(oForm, msRowTable, "U_UOM")
                                    dWeight = getDFValue(oForm, msRowTable, "U_CstWgt")

                                    oGrid.doCalculateLineCostPrices(sCardCode, sItemCode, sItemGrp, sWastCd, sAddress, sBranch, sJobType, dDocDate, dWeight, sUOM, iRow, sZipCode)
                                End If
                            End If

                            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE _
                                OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                doSetUpdate(oForm)
                            End If
                        ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SEARCH_VALUESET Then
                            If pVal.ColUID = IDH_DOADDEXP._ItemCd OrElse pVal.ColUID = IDH_DOADDEXP._SuppCd Then

                            End If
                        End If
                    ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY Then
                        Dim iRow As Integer = pVal.Row
                        'Dim sCode As String = getFormDFValue(oForm, "Code")
                        'Dim sRowID As String = getDFValue(oForm, msRowTable, "Code")
                        Dim sCustCd As String = getFormDFValue(oForm, IDH_DISPORD._CardCd)
                        Dim sCustNm As String = getFormDFValue(oForm, IDH_DISPORD._CardNM)

                        oGrid.doSetFieldValue("U_JobNr", iRow, oDO.Code) 'sCode)
                        oGrid.doSetFieldValue("U_RowNr", iRow, oDOR.Code) 'sRowID)
                        oGrid.doSetFieldValue("U_CustCd", iRow, oDO.U_CardCd) 'sCustCd)
                        oGrid.doSetFieldValue("U_CustNm", iRow, oDO.U_CardNM) 'sCustNm)

                        oGrid.doSetFieldValue("U_UOM", iRow, "ea")
                        oGrid.doSetFieldValue("U_Quantity", iRow, 1)
                        oGrid.doSetFieldValue("U_ItmCost", iRow, 0)
                        oGrid.doSetFieldValue("U_ItmChrg", iRow, 0)
                        oGrid.doSetFieldValue("U_ItmCostVat", iRow, 0)
                        oGrid.doSetFieldValue("U_ItmChrgVat", iRow, 0)
                        oGrid.doSetFieldValue("U_TAddCost", iRow, 0)
                        oGrid.doSetFieldValue("U_TAddChrg", iRow, 0)
                    ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
                        If pVal.BeforeAction = False Then
                            If pVal.oData.MenuUID.Equals("DUPLICATE") Then
                                doCalcAdditionalTotals(oForm)
                                setUFValue(oForm, "IDH_ADDCO", oDOR.U_AddCost)
                                setUFValue(oForm, "IDH_ADDCH", oDOR.U_AddCharge)
                                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                            ElseIf pVal.oData.MenuUID.Equals("DELADDITM") Then
                                doCalcAdditionalTotals(oForm)
                                setUFValue(oForm, "IDH_ADDCO", oDOR.U_AddCost)
                                setUFValue(oForm, "IDH_ADDCH", oDOR.U_AddCharge)
                                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                            End If
                        End If
                    End If
                    'If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE _
                    ' OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    '    doSetUpdate(oForm)
                    'End If
                End If
            ElseIf pVal.ItemUID = "LINESGRID" Then
                'Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                If Not oGridN Is Nothing Then
                    If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_ADD Then
                        If pVal.BeforeAction = True Then
                            oGridN.setCurrentDataRowIndex(oGridN.getLastRowIndex)
                            doNewRowAskAboutVehicle(oForm)
                        End If
                    ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DEL Then
                        'Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                        oGridN.doCommentDeletion()
                        Return False
                    ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DEL_MULTI Then
                        'Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                        oGridN.doCommentDeletion()
                        Return False
                    ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK Then 'GRID_ROW_VIEW_EDIT Then
                        If pVal.BeforeAction = False Then
                            doFillRowData(oForm, pVal.Row)
                            doSetFocus(oForm, "IDH_WEIGH")
                        End If
                    End If
                End If
            End If
            Return True
        End Function

        Public Sub doCalcAdditionalTotals(ByVal oForm As SAPbouiCOM.Form)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            '## MA 14-04-2014 Start
            'Dim dCostT As Double = oGrid.doCalculateCostTotals()
            'Dim dChrgT As Double = oGrid.doCalculateChargeTotals()

            'oDOR.U_TAddCost = dCostT
            'oDOR.U_TAddChrg = dChrgT

            'oDOR.doCalculateTotals()
            Dim dCost As Double = oGrid.doCalculateCostTotalsExcVAT()
            Dim dChrg As Double = oGrid.doCalculateChargeTotalsExcVAT()

            Dim dCostVat As Double = oGrid.doCalculateCostVAT()
            Dim dChrgVat As Double = oGrid.doCalculateChargeVAT()

            oDOR.U_AddCost = dCost
            oDOR.U_TAddCsVat = dCostVat
            'LP Viljoen - not needed as the above will auto calculate this value - oDOR.U_TAddCost = dCost + dCostVat

            oDOR.U_AddCharge = dChrg
            oDOR.U_TAddChVat = dChrgVat
            'LP Viljoen - not needed as the above will auto calculate this value - oDOR.U_TAddChrg = dChrg + dChrgVat

            'LP Viljoen - not needed as the above will auto calculate this value - oDOR.doCalculateTotals()
            '## MA 07-04-2014 End
        End Sub

        'Seeks for the first record in the grid without a second weigh
        Private Sub doShowFirstSecondWeigh(ByVal oForm As SAPbouiCOM.Form)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            Dim oDO As IDH_DISPORD = thisDO(oForm)

            Dim sHeadCode As String = oDO.Code
            Dim sRowCode As String = oDOR.Code

            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            For iRow As Integer = 0 To oGridN.getLastRowIndex()
                oGridN.setCurrentDataRowIndex(iRow)
                If oGridN.doGetFieldValue("Code").Trim().Length() > 0 AndAlso oGridN.doGetFieldValue("U_Wei2") = 0 Then
                    doFillRowData(oForm, iRow, False)
                    Exit Sub
                End If
            Next

            doFillRowData(oForm, 0)

            sHeadCode = oDO.Code
            sRowCode = oDOR.Code
        End Sub

        'Find the row index for this RowCode
        Private Sub doFillRowData(ByVal oForm As SAPbouiCOM.Form, ByVal sRowCode As String)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            Dim iFieldIndex As Integer = oGridN.doIndexField("Code")

            Dim iRow As Integer = 0
            For iRow = 0 To oGridN.getRowCount - 1
                oGridN.setCurrentDataRowIndex(iRow)
                If oGridN.doGetFieldValue(iFieldIndex).Equals(sRowCode) Then
                    Exit For
                End If
            Next
            doFillRowData(oForm, iRow)
        End Sub

        Private Sub doBranchCombo(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_BRANCH", "OUBR", "Code", "Remarks")
        End Sub

        Private Sub doTZoneCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim sBranch As String = getDFValue(oForm, msRowTable, "U_Branch")
            doFillCombo(oForm, "IDH_TZONE", "[@IDH_TPZONES]", "Code", "U_Desc", " U_Branch = '" & sBranch & "'", "U_Desc")
        End Sub

        Private Sub doFillRowData(ByVal oForm As SAPbouiCOM.Form, ByVal iRow As Integer)
            doFillRowData(oForm, iRow, False)
        End Sub

        Private Sub doFillRowData(ByVal oForm As SAPbouiCOM.Form, ByVal iRow As Integer, ByVal bReplaceBranch As Boolean)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            Dim bCompleteFillDone As Boolean = False
            Try
                Dim oDOR As IDH_DISPROW = thisDOR(oForm)

                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    Dim oPreData As PreSelect = getWFValue(oForm, "PRESELECT")
                    If oPreData Is Nothing Then
                        doClearRow(oForm)
                    End If
                    Exit Sub
                ElseIf iRow >= 0 AndAlso iRow < oGridN.getDataTable.Rows.Count Then
                    oGridN.setCurrentDataRowIndex(iRow)
                    oDOR.doClearBuffers()
                Else
                    Dim iRows As Integer
                    iRows = oGridN.getRowCount
                    If iRows > 1 Then
                        oGridN.setCurrentDataRowIndex(iRows - 2)
                        oDOR.doClearBuffers()
                    Else
                        doClearRow(oForm)
                        Exit Sub
                    End If
                End If

                setDFValue(oForm, msRowTable, IDH_DISPROW._RDate, oGridN.doGetFieldValue(IDH_DISPROW._RDate))
                setDFValue(oForm, msRowTable, "Code", oGridN.doGetFieldValue("Code"))
                setDFValue(oForm, msRowTable, "U_Lorry", oGridN.doGetFieldValue("U_Lorry"))
                setDFValue(oForm, msRowTable, "U_LorryCd", oGridN.doGetFieldValue("U_LorryCd"))
                setDFValue(oForm, msRowTable, "U_VehTyp", oGridN.doGetFieldValue("U_VehTyp"))
                setDFValue(oForm, msRowTable, "U_CustRef", oGridN.doGetFieldValue("U_CustRef"))
                setDFValue(oForm, msRowTable, IDH_DISPROW._BDate, oGridN.doGetFieldValue(IDH_DISPROW._BDate))

                setDFValue(oForm, msRowTable, "U_User", oGridN.doGetFieldValue("U_User"))

                'If bReplaceBranch Then 'AndAlso Not wasSetByUser(oForm, "IDH_BRANCH") Then
                setDFValue(oForm, msRowTable, "U_Branch", oGridN.doGetFieldValue("U_Branch"))
                'End If

                setDFValue(oForm, msRowTable, "U_TZone", oGridN.doGetFieldValue("U_TZone"))
                setDFValue(oForm, msRowTable, "U_ContNr", oGridN.doGetFieldValue("U_ContNr"))
                setDFValue(oForm, msRowTable, "U_SealNr", oGridN.doGetFieldValue("U_SealNr"))
                setDFValue(oForm, msRowTable, "U_Origin", oGridN.doGetFieldValue("U_Origin"))
                setDFValue(oForm, msRowTable, "U_CustCs", oGridN.doGetFieldValue("U_CustCs"))
                setDFValue(oForm, msRowTable, IDH_DISPROW._FTOrig, oGridN.doGetFieldValue(IDH_DISPROW._FTOrig))

                setDFValue(oForm, msRowTable, IDH_DISPROW._SAddress, oGridN.doGetFieldValue(IDH_DISPROW._SAddress))

                setDFValue(oForm, msRowTable, IDH_DISPROW._SAddrsLN, oGridN.doGetFieldValue(IDH_DISPROW._SAddrsLN))

                Dim sObligated As String = oGridN.doGetFieldValue("U_Obligated")
                setDFValue(oForm, msRowTable, "U_Obligated", sObligated)

                setDFValue(oForm, msRowTable, "U_WastTNN", oGridN.doGetFieldValue("U_WastTNN"))
                'setDFValue(oForm, msRowTable, "U_HazWCNN", oGridN.doGetFieldValue("U_HazWCNN"))
                setDFValue(oForm, msRowTable, "U_SiteRef", oGridN.doGetFieldValue("U_SiteRef"))
                setDFValue(oForm, msRowTable, "U_ExtWeig", oGridN.doGetFieldValue("U_ExtWeig"))

                setDFValue(oForm, msRowTable, "U_ConNum", oGridN.doGetFieldValue("U_ConNum"))

                setDFValue(oForm, msRowTable, "U_TRLReg", oGridN.doGetFieldValue("U_TRLReg"))
                setDFValue(oForm, msRowTable, "U_TRLNM", oGridN.doGetFieldValue("U_TRLNM"))
                setDFValue(oForm, msRowTable, "U_Driver", oGridN.doGetFieldValue("U_Driver"))
                setDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, oGridN.doGetFieldValue(IDH_DISPROW._JobTp))
                setDFValue(oForm, msRowTable, "U_ItemCd", oGridN.doGetFieldValue("U_ItemCd"))
                setDFValue(oForm, msRowTable, "U_ItemDsc", oGridN.doGetFieldValue("U_ItemDsc"))
                setDFValue(oForm, msRowTable, "U_WasCd", oGridN.doGetFieldValue("U_WasCd"))
                setDFValue(oForm, msRowTable, "U_WasDsc", oGridN.doGetFieldValue("U_WasDsc"))
                setDFValue(oForm, msRowTable, "U_AltWasDsc", oGridN.doGetFieldValue("U_AltWasDsc"))

                'Multi Currency Pricing 
                setDFValue(oForm, msRowTable, IDH_DISPROW._DispCgCc, oGridN.doGetFieldValue(IDH_DISPROW._DispCgCc))
                setDFValue(oForm, msRowTable, IDH_DISPROW._CarrCgCc, oGridN.doGetFieldValue(IDH_DISPROW._CarrCgCc))
                setDFValue(oForm, msRowTable, IDH_DISPROW._PurcCoCc, oGridN.doGetFieldValue(IDH_DISPROW._PurcCoCc))
                setDFValue(oForm, msRowTable, IDH_DISPROW._DispCoCc, oGridN.doGetFieldValue(IDH_DISPROW._DispCoCc))
                setDFValue(oForm, msRowTable, IDH_DISPROW._CarrCoCc, oGridN.doGetFieldValue(IDH_DISPROW._CarrCoCc))
                setDFValue(oForm, msRowTable, IDH_DISPROW._LiscCoCc, oGridN.doGetFieldValue(IDH_DISPROW._LiscCoCc))


                setDFValue(oForm, msRowTable, "U_MANPRC", oGridN.doGetFieldValue("U_MANPRC"))

                setDFValue(oForm, msRowTable, IDH_DISPROW._ExpLdWgt, oGridN.doGetFieldValue(IDH_DISPROW._ExpLdWgt))
                setDFValue(oForm, msRowTable, "U_TCharge", oGridN.doGetFieldValue("U_TCharge"))
                setDFValue(oForm, msRowTable, "U_TCTotal", oGridN.doGetFieldValue("U_TCTotal"))
                setDFValue(oForm, msRowTable, "U_Discnt", oGridN.doGetFieldValue("U_Discnt"))
                setDFValue(oForm, msRowTable, "U_DisAmt", oGridN.doGetFieldValue("U_DisAmt"))
                setDFValue(oForm, msRowTable, "U_Total", oGridN.doGetFieldValue("U_Total"))
                setDFValue(oForm, msRowTable, "U_Wei1", oGridN.doGetFieldValue("U_Wei1"))
                setDFValue(oForm, msRowTable, "U_Wei2", oGridN.doGetFieldValue("U_Wei2"))
                setDFValue(oForm, msRowTable, "U_WDt1", goParent.doDateToStr(oGridN.doGetFieldValue("U_WDt1")))
                setDFValue(oForm, msRowTable, "U_WDt2", goParent.doDateToStr(oGridN.doGetFieldValue("U_WDt2")))
                setDFValue(oForm, msRowTable, "U_Ser1", oGridN.doGetFieldValue("U_Ser1"))
                setDFValue(oForm, msRowTable, "U_Ser2", oGridN.doGetFieldValue("U_Ser2"))

                setDFValue(oForm, msRowTable, "U_WgtDed", oGridN.doGetFieldValue("U_WgtDed"))
                setDFValue(oForm, msRowTable, "U_AdjWgt", oGridN.doGetFieldValue("U_AdjWgt"))
                setDFValue(oForm, msRowTable, "U_ValDed", oGridN.doGetFieldValue("U_ValDed"))
                setDFValue(oForm, msRowTable, "U_BookIn", oGridN.doGetFieldValue("U_BookIn"))
                setDFValue(oForm, msRowTable, "U_IsTrl", oGridN.doGetFieldValue("U_IsTrl"))

                setDFValue(oForm, msRowTable, "U_TAddCost", oGridN.doGetFieldValue("U_TAddCost"))
                setDFValue(oForm, msRowTable, "U_TAddChrg", oGridN.doGetFieldValue("U_TAddChrg"))
                '## MA Start 15-04-2014
                setDFValue(oForm, msRowTable, "U_TAddChVat", oGridN.doGetFieldValue("U_TAddChVat"))
                setDFValue(oForm, msRowTable, "U_TAddCsVat", oGridN.doGetFieldValue("U_TAddCsVat"))

                setDFValue(oForm, msRowTable, "U_AddCharge", oGridN.doGetFieldValue("U_AddCharge"))
                setDFValue(oForm, msRowTable, "U_AddCost", oGridN.doGetFieldValue("U_AddCost"))
                '## MA End 15-04-2014


                setUFValue(oForm, "IDH_LICREG", oGridN.doGetFieldValue("U_WASLIC"))

                setDFValue(oForm, msRowTable, "U_TaxAmt", oGridN.doGetFieldValue("U_TaxAmt"))
                setDFValue(oForm, msRowTable, "U_VtCostAmt", oGridN.doGetFieldValue("U_VtCostAmt"))

                setDFValue(oForm, msRowTable, "U_TChrgVtRt", oGridN.doGetFieldValue("U_TChrgVtRt"))
                setDFValue(oForm, msRowTable, "U_HChrgVtRt", oGridN.doGetFieldValue("U_HChrgVtRt"))
                setDFValue(oForm, msRowTable, "U_TCostVtRt", oGridN.doGetFieldValue("U_TCostVtRt"))
                setDFValue(oForm, msRowTable, "U_PCostVtRt", oGridN.doGetFieldValue("U_PCostVtRt"))
                setDFValue(oForm, msRowTable, "U_HCostVtRt", oGridN.doGetFieldValue("U_HCostVtRt"))

                setDFValue(oForm, msRowTable, "U_TChrgVtGrp", oGridN.doGetFieldValue("U_TChrgVtGrp"))
                setDFValue(oForm, msRowTable, "U_HChrgVtGrp", oGridN.doGetFieldValue("U_HChrgVtGrp"))
                setDFValue(oForm, msRowTable, "U_TCostVtGrp", oGridN.doGetFieldValue("U_TCostVtGrp"))
                setDFValue(oForm, msRowTable, "U_PCostVtGrp", oGridN.doGetFieldValue("U_PCostVtGrp"))
                setDFValue(oForm, msRowTable, "U_HCostVtGrp", oGridN.doGetFieldValue("U_HCostVtGrp"))

                Dim sOrd As String = oGridN.doGetFieldValue("U_WROrd")
                setDFValue(oForm, msRowTable, "U_WROrd", sOrd)
                Dim sOrdRow As String = oGridN.doGetFieldValue("U_WRRow")
                setDFValue(oForm, msRowTable, "U_WRRow", sOrdRow)

                Dim sLoadSht As String = oGridN.doGetFieldValue("U_LoadSht")
                setDFValue(oForm, msRowTable, "U_LoadSht", sLoadSht)
                If sLoadSht.Length > 0 Then
                    setEnableItem(oForm, False, "IDH_CUSWEI")
                    setEnableItem(oForm, False, "IDH_UOM")
                    setEnableItem(oForm, False, "IDH_USERE")
                    setEnableItem(oForm, False, "IDH_USEAU")
                Else
                    '## MA Start 13-10-2014
                    If System.Array.IndexOf(moAlwaysDisabledFields, "IDH_CUSWEI") = -1 Then
                        setEnableItem(oForm, True, "IDH_CUSWEI")
                    End If
                    '## MA Start 13-10-2014

                    setEnableItem(oForm, True, "IDH_UOM")
                    setEnableItem(oForm, True, "IDH_USERE")
                    setEnableItem(oForm, True, "IDH_USEAU")
                End If

                setDFValue(oForm, msRowTable, "U_Covera", oGridN.doGetFieldValue("U_Covera"))
                setDFValue(oForm, msRowTable, "U_CoverHst", oGridN.doGetFieldValue("U_CoverHst"))

                setDFValue(oForm, msRowTable, "U_UseWgt", oGridN.doGetFieldValue("U_UseWgt"))
                setDFValue(oForm, msRowTable, "U_UOM", oGridN.doGetFieldValue("U_UOM"))
                setDFValue(oForm, msRowTable, "U_AUOM", oGridN.doGetFieldValue("U_AUOM"))
                setDFValue(oForm, msRowTable, "U_AUOMQt", oGridN.doGetFieldValue("U_AUOMQt"))
                setDFValue(oForm, msRowTable, "U_CstWgt", oGridN.doGetFieldValue("U_CstWgt"))

                Dim dReadWeight As Double = oGridN.doGetFieldValue("U_RdWgt")
                setDFValue(oForm, msRowTable, "U_RdWgt", dReadWeight)

                setDFValue(oForm, msRowTable, IDH_DISPROW._TRdWgt, oGridN.doGetFieldValue(IDH_DISPROW._TRdWgt))
                setDFValue(oForm, msRowTable, "U_TipWgt", oGridN.doGetFieldValue("U_TipWgt"))
                setDFValue(oForm, msRowTable, "U_PUOM", oGridN.doGetFieldValue("U_PUOM"))
                setDFValue(oForm, msRowTable, "U_TipCost", oGridN.doGetFieldValue("U_TipCost"))
                setDFValue(oForm, msRowTable, "U_TipTot", oGridN.doGetFieldValue("U_TipTot"))
                setDFValue(oForm, msRowTable, "U_OrdWgt", oGridN.doGetFieldValue("U_OrdWgt"))
                setDFValue(oForm, msRowTable, IDH_DISPROW._CarWgt, oGridN.doGetFieldValue(IDH_DISPROW._CarWgt))
                setDFValue(oForm, msRowTable, "U_OrdCost", oGridN.doGetFieldValue("U_OrdCost"))
                setDFValue(oForm, msRowTable, "U_OrdTot", oGridN.doGetFieldValue("U_OrdTot"))

                setDFValue(oForm, msRowTable, IDH_DISPROW._PRdWgt, oGridN.doGetFieldValue(IDH_DISPROW._PRdWgt))
                setDFValue(oForm, msRowTable, "U_ProWgt", oGridN.doGetFieldValue("U_ProWgt"))
                setDFValue(oForm, msRowTable, "U_ProUOM", oGridN.doGetFieldValue("U_ProUOM"))
                setDFValue(oForm, msRowTable, "U_PCost", oGridN.doGetFieldValue("U_PCost"))
                setDFValue(oForm, msRowTable, "U_PCTotal", oGridN.doGetFieldValue("U_PCTotal"))
                setDFValue(oForm, msRowTable, "U_JCost", oGridN.doGetFieldValue("U_JCost"))
                setDFValue(oForm, msRowTable, "U_AddEx", oGridN.doGetFieldValue("U_AddEx"))
                ''## MA Start 16-10-2014
                setDFValue(oForm, msRowTable, IDH_DISPROW._BPWgt, oGridN.doGetFieldValue(IDH_DISPROW._BPWgt))
                ''## MA End 16-10-2014
                ''## MA Start 12-09-2014 Issue#:406
                'setDFValue(oForm, msRowTable, IDH_DISPROW._JbToBeDoneDt, oGridN.doGetFieldValue(IDH_DISPROW._JbToBeDoneDt))
                'setDFValue(oForm, msRowTable, IDH_DISPROW._JbToBeDoneTm, oGridN.doGetFieldValue(IDH_DISPROW._JbToBeDoneTm))
                ''## MA End 12-09-2014 Issue#:406

                Dim sTransactionCode As String = oGridN.doGetFieldValue(IDH_DISPROW._TrnCode)
                setDFValue(oForm, msRowTable, IDH_DISPROW._TrnCode, sTransactionCode)

                'WRRow2 (was missing to save/update prior 1021)
                setDFValue(oForm, msRowTable, IDH_DISPROW._WRRow2, oGridN.doGetFieldValue(IDH_DISPROW._WRRow2))

                setDFValue(oForm, msRowTable, IDH_DISPROW._Sample, oGridN.doGetFieldValue(IDH_DISPROW._Sample))
                setDFValue(oForm, msRowTable, IDH_DISPROW._SampleRef, oGridN.doGetFieldValue(IDH_DISPROW._SampleRef))
                setDFValue(oForm, msRowTable, IDH_DISPROW._SampStatus, oGridN.doGetFieldValue(IDH_DISPROW._SampStatus))

                ''## MA Start 08-08-2014 Issue#:313
                ''Dim sBDate As String = oGridN.doGetFieldValue("U_BDate") 'getDFValue(oForm, msRowTable, "U_BDate")
                ''setDFValue(oForm, msRowTable, "U_BDate", com.idh.utils.dates.doDateToSBODateStr(sBDate)) ' com.idh.utils.dates.doStrToDate(sBDate))
                ''## MA End 08-08-2014 Issue#:313
                '				setDFValue(oForm, msRowTable, "U_ProCd", oGridN.doGetFieldValue("U_ProCd"))
                '                setDFValue(oForm, msRowTable, "U_ProNm", oGridN.doGetFieldValue("U_ProNm"))

                Dim sPPorder As String = oGridN.doGetFieldValue("U_ProPO")
                setDFValue(oForm, msRowTable, "U_ProPO", sPPorder)

                Dim sPGPorder As String = oGridN.doGetFieldValue("U_ProGRPO")
                setDFValue(oForm, msRowTable, "U_ProGRPO", sPGPorder)

                Dim sGRIn As String = oGridN.doGetFieldValue("U_GRIn")
                setDFValue(oForm, msRowTable, "U_GRIn", sGRIn)

                Dim sSODlvNot As String = oGridN.doGetFieldValue(IDH_DISPROW._SODlvNot)
                setDFValue(oForm, msRowTable, IDH_DISPROW._SODlvNot, sSODlvNot)

                Dim sTransferNo As String = oGridN.doGetFieldValue(IDH_DISPROW._WHTRNF)
                setDFValue(oForm, msRowTable, IDH_DISPROW._WHTRNF, sTransferNo)

                'If Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)) Then
                '    doHideLoadSheet(oForm)
                '    If Config.INSTANCE.doCheckCanPurchaseWB(getDFValue(oForm, msRowTable, "U_WasCd")) Then
                '        'AndAlso getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp).Equals(Config.INSTANCE.doGetJopTypeIncoming()) Then
                '        doSwitchToPO(oForm, False)
                '    Else
                '        doSwitchToSO(oForm)
                '    End If
                'Else
                '    'doShowLoadSheet(oForm)
                '    doSwitchToSO(oForm)
                '    doShowLoadSheet(oForm)
                'End If

                'Dim sStatus As String = oGridN.doGetFieldValue("U_Status")
                'Dim sPStatus As String = oGridN.doGetFieldValue("U_PStat")
                'Dim sPayMeth As String = oGridN.doGetFieldValue("U_PayMeth")
                'Dim sBStatus As String = oGridN.doGetFieldValue("U_BookIn")

                oDOR.U_Status = oGridN.doGetFieldValue("U_Status")
                oDOR.U_PStat = oGridN.doGetFieldValue("U_PStat")
                oDOR.U_PayMeth = oGridN.doGetFieldValue("U_PayMeth")
                oDOR.U_BookIn = oGridN.doGetFieldValue("U_BookIn")

                'If sBStatus Is Nothing OrElse sBStatus.Length = 0 OrElse sBStatus.Equals("D") = False Then
                '    setEnableItem(oForm, True, "IDH_BOOKIN")
                'Else
                '    setEnableItem(oForm, False, "IDH_BOOKIN")
                'End If

                'Dim bDoSkipStatusSet As Boolean = False
                'If sStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusOrdered()) Then
                '    doSwitchCheckBox(oForm, "IDH", False, False)
                'ElseIf sStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusFoc()) Then
                '    doSwitchCheckBox(oForm, "IDH", False, False)
                'ElseIf sStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusInvoiced()) Then
                '    doSwitchCheckBox(oForm, "IDH", False, False)
                'ElseIf sStatus.Length() > 0 Then
                '    If com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus) = True Then
                '        If Not sPayMeth Is Nothing AndAlso sPayMeth.Equals(com.idh.bridge.lookups.FixedValues.getPayMethPrePaid()) Then
                '            doSwitchCheckBox(oForm, "IDH_AINV", True, False)
                '        Else
                '            doSwitchCheckBox(oForm, "IDH_DOARI", True, False)
                '        End If
                '    ElseIf com.idh.bridge.lookups.FixedValues.isWaitingForPInvoice(sStatus) = True Then
                '        doSwitchCheckBox(oForm, "IDH_DOARIP", True, False)
                '    ElseIf com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) = True Then
                '        doSwitchCheckBox(oForm, "IDH_DOORD", True, False)
                '    ElseIf com.idh.bridge.lookups.FixedValues.isWaitingForFoc(sStatus) = True Then
                '        doSwitchCheckBox(oForm, "IDH_FOC", True, False)
                '    Else
                '        If sOrd.Length > 0 AndAlso sOrdRow.Length > 0 Then
                '            doSwitchCheckBox(oForm, "IDH_FOC", True, True)
                '            doSetUpdate(oForm)
                '            bDoSkipStatusSet = True
                '        Else
                '            sStatus = com.idh.bridge.lookups.FixedValues.getStatusOpen()
                '            doSwitchCheckBox(oForm, "IDH", False, False)
                '        End If
                '    End If
                'Else
                '    sStatus = com.idh.bridge.lookups.FixedValues.getStatusOpen()
                '    doSwitchCheckBox(oForm, "IDH", False, False)
                'End If

                'If sPStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusOrdered()) Then
                '    doSwitchCheckBox(oForm, "IDH", False, False)
                '    ''ElseIf sPStatus.Length() > 4 AndAlso sPStatus.Chars(sPStatus.Length-4) = "1" Then
                '    'ElseIf sPStatus.StartsWith(Config.INSTANCE.getStatusPreFixDo()) OrElse _
                '    '	sPStatus.StartsWith(Config.INSTANCE.getStatusPreFixReq()) Then
                'ElseIf com.idh.bridge.lookups.FixedValues.isSetForAction(sPStatus) Then
                '    doSwitchCheckBox(oForm, "IDH_DOPO", True, False)
                'End If

                doApplyStatuses(oForm)

                'If bDoSkipStatusSet = False Then
                '    setDFValue(oForm, msRowTable, "U_PStat", sPStatus)
                '    setDFValue(oForm, msRowTable, "U_Status", sStatus)
                '    setDFValue(oForm, msRowTable, "U_PayMeth", sPayMeth)
                '    setDFValue(oForm, msRowTable, "U_PayStat", oGridN.doGetFieldValue("U_PayStat"))
                '    setDFValue(oForm, msRowTable, "U_CCNum", oGridN.doGetFieldValue("U_CCNum"))
                '    setDFValue(oForm, msRowTable, "U_CCType", oGridN.doGetFieldValue("U_CCType"))
                '    setDFValue(oForm, msRowTable, "U_CCStat", oGridN.doGetFieldValue("U_CCStat"))
                'End If

                'SET THE ADDITIONAL TOTALS ON THE TAB
                ''## MA 14-04-2014 Start
                setUFValue(oForm, "IDH_ADDCO", oDOR.U_AddCost)
                setUFValue(oForm, "IDH_ADDCOV", oDOR.U_TAddCsVat)
                oDOR.U_TAddCost = oDOR.U_AddCost + oDOR.U_TAddCsVat


                setUFValue(oForm, "IDH_ADDCH", oDOR.U_AddCharge)
                setUFValue(oForm, "IDH_ADDCHV", oDOR.U_TAddChVat)
                oDOR.U_TAddChrg = oDOR.U_AddCharge + oDOR.U_TAddChVat

                ''## MA 14-04-2014 End
                setUFValue(oForm, "IDH_TADDCO", oDOR.U_TAddCost)
                setUFValue(oForm, "IDH_TADDCH", oDOR.U_TAddChrg)

                setWFValue(oForm, "LastVehAct", "SELECT")

                Dim sReg As String = getDFValue(oForm, msRowTable, "U_Lorry")
                If sReg.Length > 0 Then
                    ''MA Start 04-02-2015 Issue#554
                    Dim dTares() As Object = Config.INSTANCE.doGetLorryTareWeightsByRegistration(sReg)
                    setUFValue(oForm, "IDH_TARWEI", dTares(0))
                    setUFValue(oForm, "IDH_TRLTar", dTares(1))
                    setUFValue(oForm, "IDH_TRWTE1", dTares(2))
                    setUFValue(oForm, "IDH_TRWTE2", dTares(3))
                Else
                    setUFValue(oForm, "IDH_TARWEI", "0")
                    setUFValue(oForm, "IDH_TRLTar", "0")
                    setUFValue(oForm, "IDH_TRWTE1", "")
                    setUFValue(oForm, "IDH_TRWTE2", "")
                    ''MA End 04-02-2015 Issue#554
                End If
                If getDFValue(oForm, msRowTable, "U_Ser1").Length = 0 Then
                    setDFValue(oForm, msRowTable, "U_Ser1", "NONE")
                End If
                If getDFValue(oForm, msRowTable, "U_Ser2").Length = 0 Then
                    setDFValue(oForm, msRowTable, "U_Ser2", "NONE")
                End If
                doCompleteRowFill(oForm)
                bCompleteFillDone = True

                If getWFValue(oForm, "CANEDIT") = True Then
                    If Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)) Then
                        doHideLoadSheet(oForm)
                        If Config.INSTANCE.doCheckCanPurchaseWB(getDFValue(oForm, msRowTable, "U_WasCd")) Then
                            'AndAlso getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp).Equals(Config.INSTANCE.doGetJopTypeIncoming()) Then
                            doSwitchToPO(oForm, False)
                        Else
                            doSwitchToSO(oForm)
                        End If
                    Else
                        'doShowLoadSheet(oForm)
                        doSwitchToSO(oForm)
                        doShowLoadSheet(oForm)
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling row data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGFRD", {Nothing})
            Finally
                'Dim sReg As String = getDFValue(oForm, msRowTable, "U_Lorry")
                'If sReg.Length > 0 Then
                '    Dim dTares() As Double = Config.INSTANCE.doGetLorryTareWeights(goParent, sReg)
                '    setUFValue(oForm, "IDH_TARWEI", dTares(0).ToString())
                '    setUFValue(oForm, "IDH_TRLTar", dTares(1).ToString())
                'Else
                '    setUFValue(oForm, "IDH_TARWEI", "0")
                '    setUFValue(oForm, "IDH_TRLTar", "0")
                'End If
                'If getDFValue(oForm, msRowTable, "U_Ser1").Length = 0 Then
                '    setDFValue(oForm, msRowTable, "U_Ser1", "NONE")
                'End If
                'If getDFValue(oForm, msRowTable, "U_Ser2").Length = 0 Then
                '    setDFValue(oForm, msRowTable, "U_Ser2", "NONE")
                'End If

                If bCompleteFillDone = False Then
                    doCompleteRowFill(oForm)
                Else
                    oForm.Update()
                End If
            End Try

            '            doCalcMarketingTotals(oForm, "R")
        End Sub

        Private Sub doSwitchPriceFields(ByVal oForm As SAPbouiCOM.Form)
            ''MA Start 29-10-2014 Issue#417 Issue#418
            Dim oItems_Cost() As String, oItems_Charge() As String

            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) = False OrElse Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) = False Then
                'oItems = New String() {"IDH_TIPCOS", "IDH_ORDCOS", "IDH_PRCOST", "IDH_CUSCHG", _
                '          "IDH_TIPTOT", "IDH_ORDTOT", "IDH_PURTOT", "IDH_TAXO", _
                '          "IDH_ADDCOS", "IDH_TOTCOS", "IDH_CUSTOT", _
                '          "IDH_DSCPRC", "IDH_SUBTOT", "IDH_TAX", "IDH_ADDCHR", "IDH_VALDED", "IDH_TOTAL", _
                '"IDH_ADDCO", "IDH_ADDCH"}
                oItems_Cost = New String() {"IDH_TIPCOS", "IDH_ORDCOS", "IDH_PRCOST", "IDH_TIPTOT",
                                            "IDH_ORDTOT", "IDH_PURTOT", "IDH_ADDCOS", "IDH_TAXO", "IDH_TOTCOS",
                                            "IDH_ADDCO", "IDH_ADDCOV", "IDH_TADDCO"} ''Last 3 items on this row are from Additional tab
                setEnableItems(oForm, False, oItems_Cost)

                Dim oItem As SAPbouiCOM.Item

                For Each sItem As String In oItems_Cost
                    oItem = oForm.Items.Item(sItem)
                    If miOldBackColor = -1 Then
                        miOldBackColor = oItem.BackColor
                    End If
                    If miOldFrontColor = -1 Then
                        miOldFrontColor = oItem.ForeColor
                    End If

                    oItem.ForeColor = miBlankColor
                    oItem.BackColor = miBlankColor
                Next
            ElseIf Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES) = False OrElse Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Cost_PRICES) = False Then
                'oItems = New String() {"IDH_TIPCOS", "IDH_ORDCOS", "IDH_PRCOST", "IDH_CUSCHG"}
                oItems_Cost = New String() {"IDH_TIPCOS", "IDH_ORDCOS", "IDH_PRCOST"}
                setEnableItems(oForm, False, oItems_Cost)
            ElseIf doUsePrices(oForm) = False Then
                'oItems = New String() {"IDH_TIPCOS", "IDH_ORDCOS", "IDH_PRCOST", "IDH_CUSCHG"}
                oItems_Cost = New String() {"IDH_TIPCOS", "IDH_ORDCOS", "IDH_PRCOST"}
                setEnableItems(oForm, False, oItems_Cost)
                setWFValue(oForm, "COSTPRICESBLANKED", True)
            ElseIf getWFValue(oForm, "COSTPRICESBLANKED") Then
                Dim sStatus As String = getDFValue(oForm, msRowTable, "U_Status")
                If com.idh.bridge.lookups.FixedValues.canUpdate(sStatus) Then
                    'oItems = New String() {"IDH_TIPCOS", "IDH_ORDCOS", "IDH_PRCOST", "IDH_CUSCHG"}

                    oItems_Cost = New String() {"IDH_TIPCOS", "IDH_ORDCOS", "IDH_PRCOST"}
                    setWFValue(oForm, "COSTPRICESBLANKED", False)
                End If
            End If
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) = False OrElse Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) = False Then
                'oItems = New String() {"IDH_TIPCOS", "IDH_ORDCOS", "IDH_PRCOST", "IDH_CUSCHG", _
                '          "IDH_TIPTOT", "IDH_ORDTOT", "IDH_PURTOT", "IDH_TAXO", _
                '          "IDH_ADDCOS", "IDH_TOTCOS", "IDH_CUSTOT", _
                '          "IDH_DSCPRC", "IDH_SUBTOT", "IDH_TAX", "IDH_ADDCHR", "IDH_VALDED", "IDH_TOTAL", _
                '"IDH_ADDCO", "IDH_ADDCH"}
                oItems_Charge = New String() {"IDH_CUSCHG", "IDH_CUSTOT", "IDH_DSCPRC", "IDH_SUBTOT", "IDH_ADDCHR",
                                      "IDH_TAX", "IDH_VALDED", "IDH_TOTAL",
                                     "IDH_ADDCH", "IDH_ADDCHV", "IDH_TADDCH"} ''Last 3 items on this row are from Additional tab
                setEnableItems(oForm, False, oItems_Charge)

                Dim oItem As SAPbouiCOM.Item

                For Each sItem As String In oItems_Charge
                    oItem = oForm.Items.Item(sItem)
                    If miOldBackColor = -1 Then
                        miOldBackColor = oItem.BackColor
                    End If
                    If miOldFrontColor = -1 Then
                        miOldFrontColor = oItem.ForeColor
                    End If

                    oItem.ForeColor = miBlankColor
                    oItem.BackColor = miBlankColor
                Next
            ElseIf Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES) = False OrElse Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Charge_PRICES) = False Then
                'oItems = New String() {"IDH_TIPCOS", "IDH_ORDCOS", "IDH_PRCOST", "IDH_CUSCHG"}
                oItems_Charge = New String() {"IDH_CUSCHG"}
                setEnableItems(oForm, False, oItems_Charge)
            ElseIf doUsePrices(oForm) = False Then
                'oItems = New String() {"IDH_TIPCOS", "IDH_ORDCOS", "IDH_PRCOST", "IDH_CUSCHG"}
                oItems_Charge = New String() {"IDH_CUSCHG"}
                setEnableItems(oForm, False, oItems_Charge)
                setWFValue(oForm, "CHARGEPRICESBLANKED", True)
            ElseIf getWFValue(oForm, "CHARGEPRICESBLANKED") Then
                Dim sStatus As String = getDFValue(oForm, msRowTable, "U_Status")
                If com.idh.bridge.lookups.FixedValues.canUpdate(sStatus) Then
                    'oItems = New String() {"IDH_TIPCOS", "IDH_ORDCOS", "IDH_PRCOST", "IDH_CUSCHG"}
                    oItems_Charge = New String() {"IDH_CUSCHG"}
                    setWFValue(oForm, "CHARGEPRICESBLANKED", False)
                End If
            End If
            ''MA End 29-10-2014 Issue#417 Issue#418
        End Sub

        Private Sub doCompleteRowFill(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim oDOR As IDH_DISPROW = thisDOR(oForm)

                '            	setWFValue(oForm, "TargetRow", "DN")
                '            	setWFValue(oForm,"WRROWChanged", False)

                '*** Do remainder of row load
                'Dim bCanEdit As Boolean = True
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    doSwitchToFind(oForm)
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    doSwitchToAdd(oForm)
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    'LPV 20150925 START - Change Mode switch
                    If oDOR.doCheckCanEdit(False) Then
                        doSwitchToUpdate(oForm)
                    Else
                        doSwitchToOrdered(oForm)
                    End If

                    'Dim sPONr As String = getDFValue(oForm, msRowTable, "U_ProPO")
                    'If Not sPONr Is Nothing AndAlso sPONr.Length > 0 AndAlso sPONr.Equals("ERROR") = False Then
                    '    doSwitchToOrdered(oForm, com.idh.bridge.lookups.FixedValues.getStatusOrdered())
                    'Else
                    '    Dim sStatus As String = getDFValue(oForm, msRowTable, "U_Status")
                    '    If com.idh.bridge.lookups.FixedValues.canUpdate(sStatus) Then
                    '        doSwitchToUpdate(oForm)
                    '    Else
                    '        doSwitchToOrdered(oForm, sStatus)
                    '    End If
                    'End If
                    'LPV 20150925 END - Change Mode switch
                End If

                If getWFValue(oForm, "CANEDIT") = True Then
                    doSwitchPriceFields(oForm)
                End If

                Dim sOrder As String = getDFValue(oForm, msRowTable, "U_WROrd")
                If sOrder.Length > 0 Then
                    oForm.Items.Item("IDH_ORDLK").Visible = True
                Else
                    oForm.Items.Item("IDH_ORDLK").Visible = False
                End If

                Dim sRow As String = getDFValue(oForm, msRowTable, "U_WRRow")
                If sRow.Length > 0 Then
                    oForm.Items.Item("IDH_ROWLK").Visible = True
                Else
                    oForm.Items.Item("IDH_ROWLK").Visible = False
                End If

                Dim sRowSta As String = getDFValue(oForm, msRowTable, "U_RowSta")
                Dim sDOOrd As String = getDFValue(oForm, "@IDH_DISPORD", "Code")
                Dim sDORow As String = getDFValue(oForm, msRowTable, "Code")
                If sRowSta.Equals(com.idh.bridge.lookups.FixedValues.getStatusDeleted()) Then
                    setWFValue(oForm, "CANEDIT", False)
                End If

                '** The Additional Expenses
                Dim dDocDate As Date = com.idh.utils.Dates.doStrToDate(getDFValue(oForm, msRowTable, "U_BDate"))
                Dim oAddGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                ''LPVTEMP START
                'If oAddGrid Is Nothing Then
                '    oAddGrid = New WR1_Grids.idh.controls.grid.AdditionalExpenses(Me, oForm, "ADDGRID")
                '    oAddGrid.doSetGridParameters("DO", sDOOrd, sDORow, bCanEdit, dDocDate)
                '    oAddGrid.getSBOItem().AffectsFormMode = False
                'Else
                '    oAddGrid.doSetGridParameters("DO", sDOOrd, sDORow, bCanEdit, dDocDate, False)
                'End If
                ''MA Start 27-01-2015
                'oAddGrid.doSetGridParameters("DO", sDOOrd, sDORow, getWFValue(oForm, "CANEDIT"), dDocDate)
                doTheGridLayout(oAddGrid, sDOOrd, sDORow, getWFValue(oForm, "CANEDIT"), dDocDate)
                ''MA End 27-01-2015


                'Dim oAdde As WR1_Data.idh.data.AdditionalExpenses = getWFValue(oForm, "JobAdditionalHandler")
                'If Not oAdde Is Nothing Then
                '    oAddGrid.doSetDataHandler(oAdde)
                'End If
                ''LPVTEMP END
                oAddGrid.doReloadDataFH(sDORow)
                doGridUOMCombo(oAddGrid)

                ''MA Start 27-01-2015
                If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", False) Then
                    oAddGrid.doUpdateColumns(getWFValue(oForm, "CANEDIT"))
                End If
                ''MA End 27-01-2015
                '** The Deductions
                ''LPVTEMP START
                'Dim oDedGrid As WR1_Grids.idh.controls.grid.Deductions = WR1_Grids.idh.controls.grid.Deductions.getInstance(oForm, "DEDGRID")
                'If oDedGrid Is Nothing Then
                '    oDedGrid = New WR1_Grids.idh.controls.grid.Deductions(Me, oForm, "DEDGRID")
                '    oDedGrid.doSetGridParameters("DO", sDOOrd, sDORow, bCanEdit, dDocDate)
                '    oDedGrid.getSBOItem().AffectsFormMode = False
                'Else
                '    oDedGrid.doSetGridParameters("DO", sDOOrd, sDORow, bCanEdit, dDocDate, False)
                'End If

                'Dim oDed As WR1_Data.idh.data.Deductions = Nothing
                'oDed = getWFValue(oForm, "JobDeductionHandler")
                'If Not oDed Is Nothing Then
                '    oDedGrid.doSetDataHandler(oDed)
                'End If
                'oDedGrid.doReloadDataFH(sDORow)

                Dim oDedGrid As DBOGrid = getWFValue(oForm, "DEDGRID")
                oDedGrid.doEnabled(getWFValue(oForm, "CANEDIT"))
                Dim oData As IDH_DODEDUCT = oDedGrid.DBObject

                'Dim oDOR As IDH_DISPROW = thisDOR(oForm)
                oDOR.BlockChangeNotif = True
                Try
                    oData.getByOrderRow(sDORow)
                    oDedGrid.doPostReloadData(True)
                Catch ex As Exception
                    Throw ex
                Finally
                    oDOR.BlockChangeNotif = False
                End Try

                doGridUOMCombo(oDedGrid)
                ''LPVTEMP END

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the row fill.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGFRD", {Nothing})
            End Try
        End Sub
        Private Sub doTheGridLayout(ByVal oGridN As WR1_Grids.idh.controls.grid.AdditionalExpenses, ByVal sDOOrd As String, ByVal sDORow As String, ByVal bCanEdit As Boolean, ByVal dDocDate As DateTime)
            If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", False) Then
                Dim sFormTypeId As String = oGridN.getSBOForm().TypeEx
                Dim oform As SAPbouiCOM.Form = oGridN.getSBOForm
                Dim oFormSettings As IDH_FORMSET = getWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId)
                If oFormSettings Is Nothing Then
                    oFormSettings = New IDH_FORMSET()
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, "")
                    setWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId, oFormSettings)

                    oFormSettings.doAddFieldToGrid(oGridN)

                    oGridN.doSetGridParameters("DO", sDOOrd, sDORow, True, dDocDate, True)

                    oFormSettings.doSyncDB(oGridN)

                Else
                    If oFormSettings.SkipFormSettings Then
                        oGridN.doSetGridParameters("DO", sDOOrd, sDORow, bCanEdit, dDocDate, True)
                    Else
                        oGridN.doSetGridParameters("DO", sDOOrd, sDORow, bCanEdit, dDocDate, False)
                        oFormSettings.doAddFieldToGrid(oGridN)

                    End If
                End If
            Else
                oGridN.doSetGridParameters("DO", sDOOrd, sDORow, bCanEdit, dDocDate, True)
            End If
        End Sub

        Private Sub doGridUOMCombo(ByVal oGridN As IDHAddOns.idh.controls.IDHGrid)
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_UOM"))
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

                'doFillCombo(oGridN.getSBOForm(), oCombo, "OWGT", "UnitDisply", "UnitName", Nothing, Nothing, Nothing, Nothing)
                FillCombos.UOMCombo(oCombo)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the UOM Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("UOM")})
            End Try
        End Sub

        'Protected Overrides Sub setOrigin(ByVal oForm As SAPbouiCOM.Form, ByVal sOriginCd As String)
        '    setFormDFValue(oForm, "U_Origin", sOriginCd)
        '    setDFValue(oForm, msRowTable, "U_Origin", sOriginCd)
        'End Sub

        Protected Overrides Sub doSetChoosenCarrier(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoBPRules As Boolean = True)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            Dim oDO As IDH_DISPORD = thisDO(oForm)

            Dim Val As String = getSharedData(oForm, "CARDCODE")
            Dim val2 As String = getSharedData(oForm, "PHONE1")
            Dim val3 As String = getSharedData(oForm, "CNTCTPRSN")
            Dim val4 As String = getSharedData(oForm, "CARDNAME")

            oDO.U_CCardCd = Val
            oDO.U_CCardNM = val4

            'oDOR.U_CarrCd = Val
            'oDOR.U_CarrNm = val4

            oDO.U_CPhone1 = val2
            oDO.U_CContact = val3

            'Update all the linked rows values
            Dim oParams As Hashtable = New Hashtable()
            oParams.Add("U_CarrCd", oDO.U_CCardCd)
            oParams.Add("U_CarrNm", oDO.U_CCardNM)
            setAllRowsValues(oForm, oParams)

            Dim sWOLink As String = oDOR.U_WROrd
            If Not sWOLink Is Nothing AndAlso sWOLink.Length > 0 Then
                Dim oData As ArrayList
                oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, Val, "S")
                doSetCarrierAddress(oForm, oData)
            Else
                If bDoBPRules Then
                    doBusinessPartnerRules(oForm, False, True, False, False)
                End If
            End If

            '## Start 18-06-2014' following function was not setting the field value, so used setFormDFValue
            'setUFValue(oForm, "IDH_LICREG", getSharedData(oForm, "WASLIC"))
            oDO.U_CWasLic = getSharedData(oForm, "WASLIC")
            '## End 18-06-2014
            setEnableItem(oForm, True, "IDH_ADDRES")
            'Dim sJobType As String = oDOR.U_JobTp
            'If Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) Then
            '    thisDOR(oForm).doGetHaulageCostPrice(True)
            'End If
            doSetUpdate(oForm)
        End Sub

        'Protected Overrides Sub doSetChoosenCarrier(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoBPRules As Boolean = True)
        '    Dim Val As String = getSharedData(oForm, "CARDCODE")
        '    Dim val2 As String = getSharedData(oForm, "PHONE1")
        '    Dim val3 As String = getSharedData(oForm, "CNTCTPRSN")
        '    Dim val4 As String = getSharedData(oForm, "CARDNAME")

        '    setFormDFValue(oForm, "U_CCardCd", Val, True)
        '    setFormDFValue(oForm, "U_CCardNM", val4, True)
        '    setFormDFValue(oForm, "U_CPhone1", val2, True)
        '    setFormDFValue(oForm, "U_CContact", val3, True)

        '    'Update all the linked rows values
        '    Dim oParams As Hashtable = New Hashtable()
        '    oParams.Add("U_CarrCd", getFormDFValue(oForm, "U_CCardCd"))
        '    oParams.Add("U_CarrNm", getFormDFValue(oForm, "U_CCardNM"))
        '    setAllRowsValues(oForm, oParams)

        '    Dim sWOLink As String = getDFValue(oForm, msRowTable, "U_WROrd")
        '    If Not sWOLink Is Nothing AndAlso sWOLink.Length > 0 Then
        '        Dim oData As ArrayList
        '        oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, Val, "S")
        '        doSetCarrierAddress(oForm, oData)
        '    Else
        '        If bDoBPRules Then
        '            doBusinessPartnerRules(oForm, False, True, False, False)
        '        End If
        '    End If

        '    '## Start 18-06-2014' following function was not setting the field value, so used setFormDFValue
        '    'setUFValue(oForm, "IDH_LICREG", getSharedData(oForm, "WASLIC"))
        '    setFormDFValue(oForm, "U_CWasLic", getSharedData(oForm, "WASLIC"))
        '    '## End 18-06-2014

        '    Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)
        '    If Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) Then
        '        thisDOR(oForm).doGetHaulageCostPrice(True)
        '    End If
        '    doSetUpdate(oForm)
        'End Sub

        Protected Overrides Sub doSetChoosenSite(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoBPRules As Boolean = True)
            Dim oDO As IDH_DISPORD = thisDO(oForm)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

            Dim Val As String = getSharedData(oForm, "CARDCODE")
            oDO.U_SCardCd = Val
            oDO.U_SCardNM = getSharedData(oForm, "CARDNAME")

            'oDOR.U_Tip = Val
            'oDOR.U_TipNm = getSharedData(oForm, "CARDNAME")

            oDO.U_SPhone1 = getSharedData(oForm, "PHONE1")
            oDO.U_SContact = getSharedData(oForm, "CNTCTPRSN")

            Dim sWOLink As String = oDOR.U_WROrd
            If Not sWOLink Is Nothing AndAlso sWOLink.Length > 0 Then
                Dim oData As ArrayList
                oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, Val, "S")
                If (oData IsNot Nothing AndAlso oData.Count > 0) Then
                    doSetSiteAddress(oForm, oData)
                End If
            Else
                If bDoBPRules Then
                    setWFValue(oForm, "FindBranchDisposalSite", False)
                    doBusinessPartnerRules(oForm, False, False, True, False)
                End If
            End If

            'Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)
            'If Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) Then

            '    Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            '    oDOR.BlockChangeNotif = True
            '    Try
            '        oDOR.doGetDisposalSiteUOM(True)
            '    Catch e As Exception
            '    End Try
            '    oDOR.BlockChangeNotif = False
            '    oDOR.doGetTipCostPrice(True)
            'End If
            If isItemEnabled(oForm, "IDH_DISSIT") Then
                setEnableItem(oForm, True, "IDH_SITADD")
            End If

            doSetUpdate(oForm)
        End Sub

        'Protected Overrides Sub doSetChoosenSite(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoBPRules As Boolean = True)
        '    Dim Val As String = getSharedData(oForm, "CARDCODE")
        '    setFormDFValueFromUser(oForm, "U_SCardCd", Val)
        '    setFormDFValueFromUser(oForm, "U_SCardNM", getSharedData(oForm, "CARDNAME"))
        '    setFormDFValue(oForm, "U_SPhone1", getSharedData(oForm, "PHONE1"))
        '    setFormDFValue(oForm, "U_SContact", getSharedData(oForm, "CNTCTPRSN"))

        '    Dim sWOLink As String = getDFValue(oForm, msRowTable, "U_WROrd")
        '    If Not sWOLink Is Nothing AndAlso sWOLink.Length > 0 Then
        '        Dim oData As ArrayList
        '        oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, Val, "S")
        '        doSetSiteAddress(oForm, oData)
        '    Else
        '        If bDoBPRules Then
        '            setWFValue(oForm, "FindBranchDisposalSite", False)
        '            doBusinessPartnerRules(oForm, False, False, True, False)
        '        End If
        '    End If

        '    Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)
        '    If Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) Then

        '        Dim oDOR As IDH_DISPROW = thisDOR(oForm)
        '        oDOR.BlockChangeNotif = True
        '        Try
        '            oDOR.doGetDisposalSiteUOM(True)
        '        Catch e As Exception
        '        End Try
        '        oDOR.BlockChangeNotif = False
        '        oDOR.doGetTipCostPrice(True)
        '    End If
        '    doSetUpdate(oForm)
        'End Sub

        Protected Overrides Sub doSetChoosenProducer(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoBPRules As Boolean = True, Optional ByVal bDoLinkCustomerAndProducer As Boolean = False)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            Dim oDO As IDH_DISPORD = thisDO(oForm)

            Dim Val As String = getSharedData(oForm, "CARDCODE")
            oDO.U_PCardCd = Val
            oDO.U_PCardNM = getSharedData(oForm, "CARDNAME")

            'oDOR.U_ProCd = Val
            'oDOR.U_ProNm = getSharedData(oForm, "CARDNAME")

            oDO.U_PPhone1 = getSharedData(oForm, "PHONE1")
            oDO.U_PContact = getSharedData(oForm, "CNTCTPRSN")

            Dim sWOLink As String = oDOR.U_WROrd
            If Not sWOLink Is Nothing AndAlso sWOLink.Length > 0 Then
                Dim oData As ArrayList
                oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, Val, "S")
                doSetProducerAddress(oForm, oData, True)
            Else
                If bDoBPRules Then
                    doBusinessPartnerRules(oForm, False, False, False, True)
                End If
            End If

            'If oDOR.getMarketingMode() = Config.MarketingMode.PO Then 'getWFValue(oForm, "FormMode") = "PO" Then
            '    oDOR.U_ProUOM = getSharedData(oForm, "IDHUOM") 'Config.INSTANCE.getBPUOM(goParent, Val)
            '    oDOR.doGetProducerCostPrice(True)
            'End If
            setEnableItem(oForm, True, "IDH_PRDADD")

            doSetUpdate(oForm)

            Dim sFirstBP As String = oDO.U_FirstBP
            If sFirstBP.Equals("None") Then
                oDO.U_FirstBP = "Producer"
            End If

            'UPDATE THE ADDITIONAL GRID FROM HERE FOR NOW - IN FUTURE THE DBOBJECT WILL BE USED
            doRefreshAdditionalPrices(oForm)
            doAddAutoAdditionalCodes(oForm)
        End Sub

        'Protected Overrides Sub doSetChoosenProducer(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoBPRules As Boolean = True, Optional ByVal bDoLinkCustomerAndProducer As Boolean = False)
        '    Dim Val As String = getSharedData(oForm, "CARDCODE")
        '    setFormDFValueFromUser(oForm, "U_PCardCd", Val)
        '    setFormDFValueFromUser(oForm, "U_PCardNM", getSharedData(oForm, "CARDNAME"))

        '    setDFValue(oForm, msRowTable, "U_ProCd", Val)
        '    setDFValue(oForm, msRowTable, "U_ProNm", getSharedData(oForm, "CARDNAME"))

        '    setFormDFValue(oForm, "U_PPhone1", getSharedData(oForm, "PHONE1"))
        '    setFormDFValue(oForm, "U_PContact", getSharedData(oForm, "CNTCTPRSN"))

        '    Dim sWOLink As String = getDFValue(oForm, msRowTable, "U_WROrd")
        '    If Not sWOLink Is Nothing AndAlso sWOLink.Length > 0 Then
        '        Dim oData As ArrayList
        '        oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, Val, "S")
        '        doSetProducerAddress(oForm, oData, True)
        '    Else
        '        If bDoBPRules Then
        '            doBusinessPartnerRules(oForm, False, False, False, True)
        '        End If
        '    End If

        '    If getWFValue(oForm, "FormMode") = "PO" Then
        '        Dim sSalesUOM As String = getSharedData(oForm, "IDHUOM") 'Config.INSTANCE.getBPUOM(goParent, Val)
        '        setDFValue(oForm, msRowTable, "U_ProUOM", sSalesUOM)

        '        Dim oDOR As IDH_DISPROW = thisDOR(oForm)
        '        oDOR.BlockChangeNotif = True
        '        Try
        '            oDOR.doGetProducerUOM(True)
        '        Catch e As Exception
        '        End Try
        '        oDOR.BlockChangeNotif = False
        '        oDOR.doGetProducerCostPrice(True)
        '    End If

        '    'doItAll(oForm)
        '    doSetUpdate(oForm)

        '    Dim sFirstBP As String = getFormDFValue(oForm, "U_FirstBP")
        '    If sFirstBP.Equals("None") Then
        '        setFormDFValue(oForm, "U_FirstBP", "Producer")
        '    End If
        'End Sub

        '        Protected Function doSetChoosenRowProducerFromShared(ByVal oForm As SAPbouiCOM.Form) As Boolean
        '        	Dim sLinkedBPCode As String = getSharedData(oForm, "CARDCODE")
        '            Dim sLinkedBPName As String = getSharedData(oForm, "CARDNAME")
        '
        '            If sLinkedBPCode.Length > 0 AndAlso getWFValue(oForm, "FormMode") = "PO" Then
        '            	setDFValue(oForm, msRowTable, "U_ProCd", sLinkedBPCode, True)
        '               	setDFValue(oForm, msRowTable, "U_ProNM", sLinkedBPName, True)
        '            End If
        '        End Function


        Protected Function doSetChoosenProducerFromBuffer(ByVal oForm As SAPbouiCOM.Form, ByRef oLinkedBP As LinkedBP, Optional ByVal bDoSetAddress As Boolean = True) As Boolean
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            Dim oDO As IDH_DISPORD = thisDO(oForm)

            If Not oLinkedBP Is Nothing Then
                If oLinkedBP.LinkedBPCardCode.Length > 0 AndAlso oDOR.MarketingMode = Config.MarketingMode.PO Then 'getWFValue(oForm, "FormMode") = "PO" Then
                    oDO.U_PCardCd = oLinkedBP.LinkedBPCardCode
                    oDO.U_PCardNM = oLinkedBP.LinkedBPName

                    'oDOR.U_ProCd = oLinkedBP.CardCode
                    'oDOR.U_ProNm = oLinkedBP.CardName
                    setEnableItem(oForm, True, "IDH_PRDADD")
                    If bDoSetAddress Then
                        Dim oAddressData As ArrayList
                        oAddressData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, oLinkedBP.LinkedBPCardCode, "S")
                        doSetProducerAddress(oForm, oAddressData, False)
                    End If
                    oDOR.U_ProUOM = oLinkedBP.UOM
                End If

                doSetUpdate(oForm)

                Dim sFirstBP As String = oDO.U_FirstBP
                If sFirstBP.Equals("None") Then
                    oDO.U_FirstBP = "Producer"
                End If
            End If

            'UPDATE THE ADDITIONAL GRID FROM HERE FOR NOW - IN FUTURE THE DBOBJECT WILL BE USED
            doRefreshAdditionalPrices(oForm)
            doAddAutoAdditionalCodes(oForm)
            Return True
        End Function

        'Protected Function doSetChoosenProducerFromBuffer(ByVal oForm As SAPbouiCOM.Form, ByRef oLinkedBP As idh.const.Lookup.LinkedBP, Optional ByVal bDoSetAddress As Boolean = True) As Boolean
        '    If Not oLinkedBP Is Nothing Then

        '        If oLinkedBP.CardCode.Length > 0 AndAlso getWFValue(oForm, "FormMode") = "PO" Then
        '            setFormDFValue(oForm, "U_PCardCd", oLinkedBP.CardCode, True)
        '            setFormDFValue(oForm, "U_PCardNM", oLinkedBP.CardName, True)

        '            setDFValue(oForm, msRowTable, "U_ProCd", oLinkedBP.CardCode, True)
        '            setDFValue(oForm, msRowTable, "U_ProNm", oLinkedBP.CardName, True)

        '            If bDoSetAddress Then
        '                Dim oAddressData As ArrayList
        '                oAddressData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, oLinkedBP.CardCode, "S")
        '                doSetProducerAddress(oForm, oAddressData, False)
        '            End If
        '        End If

        '        If getWFValue(oForm, "FormMode") = "PO" Then
        '            thisDOR(oForm).U_ProUOM = oLinkedBP.UOM
        '        End If

        '        'doItAll(oForm)
        '        doSetUpdate(oForm)

        '        Dim sFirstBP As String = getFormDFValue(oForm, "U_FirstBP")
        '        If sFirstBP.Equals("None") Then
        '            setFormDFValue(oForm, "U_FirstBP", "Producer")
        '        End If
        '    End If
        'End Function

        Protected Overrides Function doSetChoosenCustomerFromShared(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoBPRules As Boolean = True, Optional ByVal bDoBuyAndTrade As Boolean = False) As Boolean
            Dim val As String = Nothing
            Dim val2 As String
            Dim val3 As String
            Dim val4 As String
            Dim dCreditLimit As Decimal = 0
            Dim dCreditRemain As Decimal = 0
            Dim dDeviationPrc As Decimal = 0
            Dim dTBalance As Decimal = 0
            Dim sUOM As String
            Dim sCustCs As String
            Dim sObligated As String
            Dim sIgnoreCheck As String
            Dim WasteLicRegNo As String = ""

            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            Dim oDO As IDH_DISPORD = thisDO(oForm)

            Try
                val = getSharedData(oForm, "CARDCODE")
                val2 = getSharedData(oForm, "PHONE1")
                val3 = getSharedData(oForm, "CNTCTPRSN")
                val4 = getSharedData(oForm, "CARDNAME")
                dCreditLimit = getSharedData(oForm, "CREDITLINE")
                dCreditRemain = getSharedData(oForm, "CRREMAIN")
                dDeviationPrc = getSharedData(oForm, "DEVIATION")
                dTBalance = getSharedData(oForm, "TBALANCE")
                sUOM = getSharedData(oForm, "IDHUOM")
                sCustCs = getSharedData(oForm, "IDHONCS")
                sObligated = getSharedData(oForm, "IDHOBLGT")
                sIgnoreCheck = getSharedData(oForm, "IDHICL")
                WasteLicRegNo = getSharedData(oForm, "WASLIC")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the chosen Customer.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSVS", {com.idh.bridge.Translation.getTranslatedWord("Chosen Customer"), val.ToString()})
                Return False
            End Try

            If Not (val Is Nothing) AndAlso val.Length > 0 Then
                If Not sIgnoreCheck = "Y" Then
                    Dim dChargeTotal As Double = getUnCommittedRowTotals(oForm, getDFValue(oForm, msRowTable, "Code"), getDFValue(oForm, msRowTable, "U_Total"))
                    Dim bBlockOnFail As Boolean = Config.INSTANCE.getParameterAsBool("DOBCCL", True)
                    If Config.INSTANCE.doCheckCredit2(val, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc, dChargeTotal, False, bBlockOnFail, Nothing) = False Then
                        Return False
                    End If
                End If

                'If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                '    oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                '    setFormDFValue(oForm, "Code", "")

                '    Dim sJbTp As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, True)

                '    Dim bResult As Boolean = doFindInternal(oForm, "VEH", "", val, "", "", sJbTp)
                '    If bResult Then
                '        'Dim oDOR As com.idh.dbObjects.User.IDH_DISPROW = thisDOR(oForm)
                '        oDOR.BlockChangeNotif = True
                '        Try
                '            oDOR.doGetAllUOM()
                '        Catch e As Exception

                '        End Try
                '        oDOR.BlockChangeNotif = False
                '        oDOR.doGetChargePrices(False)
                '        oDOR.doGetCostPrices(True, True, True, False)
                '        doAddAutoAdditionalCodes(oForm)
                '        Return True
                '    End If

                '    'doLoadDataLocal(oForm, False)
                'End If

                oDO.U_CardCd = val
                oDO.U_CardNM = val4
                oDO.U_Contact = val3

                'oDOR.U_CustCd = val
                'oDOR.U_CustNm = val4
                oDOR.U_Obligated = sObligated

                If sCustCs.Length > 0 AndAlso (oDOR.U_CustCd.Length = 0 OrElse wasSetWithCode(oForm, "IDH_ONCS")) Then
                    oDOR.U_CustCs = sCustCs
                End If

                '## Start 18-06-2014
                oDO.U_WasLic = WasteLicRegNo
                '## End 18-06-2014

                Dim sWOLink As String = oDOR.U_WROrd
                If Not sWOLink Is Nothing AndAlso sWOLink.Length > 0 Then
                    Dim oData As ArrayList
                    oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, val, "S")
                    doSetCustomerAddress(oForm, oData, val, val2)
                Else
                    If bDoBPRules = True Then
                        doBusinessPartnerRules(oForm, True, False, False, False)
                    End If
                End If
                setEnableItem(oForm, True, "IDH_CUSADD")
                Dim sLinkedBPCode As String = getSharedData(oForm, "IDHLBPC")
                Dim sLinkedBPName As String = getSharedData(oForm, "IDHLBPN")

                If sLinkedBPCode.Length > 0 AndAlso oDOR.MarketingMode = Config.MarketingMode.PO Then 'getWFValue(oForm, "FormMode") = "PO" Then
                    oDO.U_PCardCd = sLinkedBPCode
                    oDO.U_PCardNM = sLinkedBPName
                    setEnableItem(oForm, True, "IDH_PRDADD")
                    'oDOR.U_ProCd = sLinkedBPCode
                    'oDOR.U_ProNm = sLinkedBPName
                End If

                Dim sFirstBP As String = oDO.U_FirstBP
                If sFirstBP.Equals("None") Then
                    oDO.U_FirstBP = "Customer"
                End If

                'UPDATE THE ADDITIONAL GRID FROM HERE FOR NOW - IN FUTURE THE DBOBJECT WILL BE USED
                'If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then

                'Else
                doSetUpdate(oForm)

                'doRefreshAdditionalPrices(oForm)
                'doAddAutoAdditionalCodes(oForm)

                'UPDATE THE ADDITIONAL GRID FROM HERE FOR NOW - IN FUTURE THE DBOBJECT WILL BE USED
                'doRefreshAdditionalPrices(oForm)
                If Config.INSTANCE.getParameterAsBool("USEBPACH", False) Then
                    removeAutoAddBPAddCharges(oForm)
                    getBPAdditionalCharges(oForm)
                End If
                doAddAutoAdditionalCodes(oForm)

                Return True
                'End If
            End If
            Return False
        End Function

        'Protected Overrides Function doSetChoosenCustomerFromShared(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoBPRules As Boolean = True, Optional ByVal bDoBuyAndTrade As Boolean = False) As Boolean
        '    Dim oDOR As IDH_DISPROW = thisDOR(oForm)

        '    Dim val As String
        '    Dim val2 As String
        '    Dim val3 As String
        '    Dim val4 As String
        '    Dim dCreditLimit As Decimal = 0
        '    Dim dCreditRemain As Decimal = 0
        '    Dim dDeviationPrc As Decimal = 0
        '    Dim dTBalance As Decimal = 0
        '    Dim sUOM As String
        '    Dim sCustCs As String
        '    Dim sObligated As String
        '    Dim sIgnoreCheck As String
        '    Dim WasteLicRegNo As String = ""

        '    Try
        '        val = getSharedData(oForm, "CARDCODE")
        '        val2 = getSharedData(oForm, "PHONE1")
        '        val3 = getSharedData(oForm, "CNTCTPRSN")
        '        val4 = getSharedData(oForm, "CARDNAME")
        '        dCreditLimit = getSharedData(oForm, "CREDITLINE")
        '        dCreditRemain = getSharedData(oForm, "CRREMAIN")
        '        dDeviationPrc = getSharedData(oForm, "DEVIATION")
        '        dTBalance = getSharedData(oForm, "TBALANCE")
        '        sUOM = getSharedData(oForm, "IDHUOM")
        '        sCustCs = getSharedData(oForm, "IDHONCS")
        '        sObligated = getSharedData(oForm, "IDHOBLGT")
        '        sIgnoreCheck = getSharedData(oForm, "IDHICL")
        '        WasteLicRegNo = getSharedData(oForm, "WASLIC")
        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the chosen Customer.")
        '        Return False
        '    End Try

        '    If Not (val Is Nothing) AndAlso val.Length > 0 Then
        '        If Not sIgnoreCheck = "Y" Then
        '            Dim dChargeTotal As Double = getUnCommittedRowTotals(oForm, getDFValue(oForm, msRowTable, "Code"), getDFValue(oForm, msRowTable, "U_Total"))
        '            Dim bBlockOnFail As Boolean = Config.INSTANCE.getParameterAsBool("DOBCCL", True)
        '            If Config.INSTANCE.doCheckCredit2(val, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc, dChargeTotal, False, bBlockOnFail, Nothing) = False Then
        '                Return False
        '            End If
        '        End If

        '        setFormDFValue(oForm, "U_CardCd", val, True)
        '        setFormDFValue(oForm, "U_CardNM", val4, True)
        '        setFormDFValue(oForm, "U_Contact", val3, True)
        '        setDFValue(oForm, msRowTable, "U_Obligated", sObligated, True)

        '        ''## MA Start 16-10-2014
        '        'doUpdateBPWeightFlag(oForm)
        '        ''## MA End 16-10-2014

        '        If sCustCs.Length > 0 AndAlso (getDFValue(oForm, msRowTable, "U_CustCd").Length = 0 OrElse wasSetWithCode(oForm, "IDH_ONCS")) Then
        '            setDFValue(oForm, msRowTable, "U_CustCs", sCustCs, True)
        '        End If
        '        '## Start 18-06-2014
        '        setFormDFValue(oForm, "U_WasLic", WasteLicRegNo)
        '        '## End 18-06-2014

        '        Dim sWOLink As String = getDFValue(oForm, msRowTable, "U_WROrd")
        '        If Not sWOLink Is Nothing AndAlso sWOLink.Length > 0 Then
        '            Dim oData As ArrayList
        '            oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, val, "S")
        '            doSetCustomerAddress(oForm, oData, val, val2)
        '        Else
        '            If bDoBPRules = True Then
        '                doBusinessPartnerRules(oForm, True, False, False, False)
        '            End If
        '        End If

        '        Dim sLinkedBPCode As String = getSharedData(oForm, "IDHLBPC")
        '        Dim sLinkedBPName As String = getSharedData(oForm, "IDHLBPN")

        '        If sLinkedBPCode.Length > 0 AndAlso oDOR.getMarketingMode() = Config.MarketingMode.PO Then 'getWFValue(oForm, "FormMode") = "PO" Then
        '            setFormDFValue(oForm, "U_PCardCd", sLinkedBPCode) ', True)
        '            setFormDFValue(oForm, "U_PCardNM", sLinkedBPName) ', True)

        '            setDFValue(oForm, msRowTable, "U_ProCd", sLinkedBPCode)
        '            setDFValue(oForm, msRowTable, "U_ProNm", sLinkedBPName)
        '        End If

        '        oDOR.BlockChangeNotif = True
        '        Try
        '            oDOR.doGetCustomerUOM(True)
        '        Catch e As Exception
        '        End Try
        '        oDOR.BlockChangeNotif = False
        '        oDOR.doGetChargePrices(True)

        '        doSetUpdate(oForm)

        '        Dim sFirstBP As String = getFormDFValue(oForm, "U_FirstBP")
        '        If sFirstBP.Equals("None") Then
        '            setFormDFValue(oForm, "U_FirstBP", "Customer")
        '        End If

        '        Return True
        '    End If
        '    Return False
        'End Function


        Protected Overrides Sub doChooseSite(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, sItemID As String)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            Dim oDO As IDH_DISPORD = thisDO(oForm)

            Dim sCardCode As String = getFormDFValue(oForm, "U_SCardCd", True)
            Dim sCardName As String = getFormDFValue(oForm, "U_SCardNM", True)

            Dim sSiteOption As String = Config.Parameter("DODEDII")
            Dim sBranch As String = ""
            Dim sGroup As String = Config.Parameter("BGCDispo")
            Dim sType As String = "C"

            Dim bCanEditSite As Boolean = False
            Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)

            If (Not sSiteOption Is Nothing AndAlso sSiteOption.Equals("USER")) Then
                bCanEditSite = True
                'Else If sJobType.Equals(Config.INSTANCE.doGetJopTypeOutgoing()) Then
            ElseIf Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) Then
                bCanEditSite = True
            Else
                bCanEditSite = Config.INSTANCE.getParameterAsBool("DODCEDI", False)
                If bCanEditSite = False Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Can only change the Disposal site for outgoing Jobs")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Can only change the Disposal site for outgoing Jobs", "ERUSCDS", {Nothing})
                End If
            End If

            If bCanEditSite Then
                'If sJobType.Equals(Config.INSTANCE.doGetJopTypeIncoming()) Then
                If Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) Then
                    'If (Not sSiteOption Is Nothing AndAlso sSiteOption.Equals("USER")) Then
                    sBranch = getDFValue(oForm, msRowTable, "U_Branch")

                    sGroup = Config.Parameter("BGSDispo")
                    sType = "S"
                    'End If
                End If

                setSharedData(oForm, "TRG", "ST")
                'With oForm.DataSources.DBDataSources.Item("@IDH_DISPORD")
                If sCardCode.Length > 0 OrElse
                 sCardName.Length > 0 OrElse
                 Config.INSTANCE.getParameterAsBool(msOrderType & "BPFLT", True) = False Then
                    'setSharedData(oForm, "IDH_BPCOD", "")
                    'setSharedData(oForm, "IDH_NAME", "")
                    setSharedData(oForm, "IDH_TYPE", "")
                    setSharedData(oForm, "IDH_GROUP", "")
                    setSharedData(oForm, "IDH_BRANCH", "")
                Else
                    setSharedData(oForm, "IDH_TYPE", sType)
                    setSharedData(oForm, "IDH_GROUP", sGroup)
                    setSharedData(oForm, "IDH_BRANCH", sBranch)
                End If
                setSharedData(oForm, "IDH_BPCOD", sCardCode)
                setSharedData(oForm, "IDH_NAME", sCardName)
                'End With
                setSharedData(oForm, "CALLEDITEM", sItemID)
                If bCanDoSilent = True Then
                    setSharedData(oForm, "SILENT", "SHOWMULTI")
                End If
                goParent.doOpenModalForm("IDHCSRCH", oForm)
            End If
        End Sub

        'Protected Overrides Sub doChooseSite(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean)
        '    'Dim sCardCode As String = getItemValue( oForm, "IDH_DISSIT")
        '    'Dim sCardName As String = getItemValue( oForm, "IDH_DISNAM")
        '    Dim sCardCode As String = getFormDFValue(oForm, "U_SCardCd", True)
        '    Dim sCardName As String = getFormDFValue(oForm, "U_SCardNM", True)

        '    Dim sSiteOption As String = Config.Parameter("DODEDII")
        '    Dim sBranch As String = ""
        '    Dim sGroup As String = Config.Parameter("BGCDispo")
        '    Dim sType As String = "C"

        '    Dim bCanEditSite As Boolean = False
        '    Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)

        '    If (Not sSiteOption Is Nothing AndAlso sSiteOption.Equals("USER")) Then
        '        bCanEditSite = True
        '        'Else If sJobType.Equals(Config.INSTANCE.doGetJopTypeOutgoing()) Then
        '    ElseIf Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) Then
        '        bCanEditSite = True
        '    Else
        '        bCanEditSite = Config.INSTANCE.getParameterAsBool("DODCEDI", False)
        '        If bCanEditSite = False Then
        '            com.idh.bridge.DataHandler.INSTANCE.doError("Can only change the Disposal site for outgoing Jobs")
        '        End If
        '    End If

        '    If bCanEditSite Then
        '        'If sJobType.Equals(Config.INSTANCE.doGetJopTypeIncoming()) Then
        '        If Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) Then
        '            'If (Not sSiteOption Is Nothing AndAlso sSiteOption.Equals("USER")) Then
        '            sBranch = getDFValue(oForm, msRowTable, "U_Branch")

        '            sGroup = Config.Parameter("BGSDispo")
        '            sType = "S"
        '            'End If
        '        End If

        '        setSharedData(oForm, "TRG", "ST")
        '        'With oForm.DataSources.DBDataSources.Item("@IDH_DISPORD")
        '        If sCardCode.Length > 0 OrElse _
        '         sCardName.Length > 0 OrElse _
        '         Config.INSTANCE.getParameterAsBool(msOrderType & "BPFLT", True) = False Then
        '            'setSharedData(oForm, "IDH_BPCOD", "")
        '            'setSharedData(oForm, "IDH_NAME", "")
        '            setSharedData(oForm, "IDH_TYPE", "")
        '            setSharedData(oForm, "IDH_GROUP", "")
        '            setSharedData(oForm, "IDH_BRANCH", "")
        '        Else
        '            setSharedData(oForm, "IDH_TYPE", sType)
        '            setSharedData(oForm, "IDH_GROUP", sGroup)
        '            setSharedData(oForm, "IDH_BRANCH", sBranch)
        '        End If
        '        setSharedData(oForm, "IDH_BPCOD", sCardCode)
        '        setSharedData(oForm, "IDH_NAME", sCardName)
        '        'End With

        '        If bCanDoSilent = True Then
        '            setSharedData(oForm, "SILENT", "SHOWMULTI")
        '        End If
        '        goParent.doOpenModalForm("IDHCSRCH", oForm)
        '    End If
        'End Sub

        Private Sub doChooseContainer(ByVal oForm As SAPbouiCOM.Form, ByVal sItemID As String)
            ''## MA Start 19-11-2014 
            If wasSetWithCode(oForm, sItemID) Then
                Return
            End If
            Dim sContCd As String = String.Empty

            Dim sContName As String = String.Empty
            If sItemID <> "IDH_ITCF" Then 'if called from container code/desc text box
                sContCd = getDFValue(oForm, msRowTable, "U_ItemCd")
                sContName = getDFValue(oForm, msRowTable, "U_ItemDsc")
                If sItemID = "IDH_ITMCOD" AndAlso (sContCd.Trim.Length = 0 OrElse sContCd.IndexOf("*") > -1) AndAlso sContName.Trim.Length > 0 Then 'if nothing in code then neglect CFL
                    setDFValue(oForm, msRowTable, "U_ItemDsc", "")
                    sContName = ""
                ElseIf sItemID = "IDH_DESC" AndAlso (sContName.Trim.Length = 0 OrElse sContName.IndexOf("*") > -1) AndAlso sContCd.Trim.Length > 0 Then 'if nothing in code then neglect CFL
                    setDFValue(oForm, msRowTable, "U_ItemCd", "")
                    sContCd = ""
                End If
                If sContCd.Trim.Length > 0 OrElse sContName.Trim.Length > 0 Then
                    setSharedData(oForm, "ACTIVEITM", sItemID)
                    setSharedData(oForm, "TRG", "PROD")
                    setSharedData(oForm, "IDH_ITMCOD", sContCd)
                    setSharedData(oForm, "IDH_GRPCOD", Config.Parameter("MDDDIG"))
                    setSharedData(oForm, "IDH_ITMNAM", sContName)
                    setSharedData(oForm, "SILENT", "SHOWMULTI")
                    setSharedData(oForm, "CALLEDITEM", sItemID)
                    goParent.doOpenModalForm("IDHISRC", oForm)

                End If

            Else 'if called by CFL button
                setSharedData(oForm, "ACTIVEITM", sItemID)
                setSharedData(oForm, "IDH_ITMNAM", sContName)
                setSharedData(oForm, "IDH_ITMCOD", sContCd)
                setSharedData(oForm, "TRG", "PROD")
                setSharedData(oForm, "IDH_GRPCOD", Config.Parameter("MDDDIG"))
                goParent.doOpenModalForm("IDHISRC", oForm)
            End If
            ''## MA End 19-11-2014 
        End Sub
        Protected Sub doChooseBranchSite(ByVal oForm As SAPbouiCOM.Form)
            Dim sSiteOption As String = Config.Parameter("DODEDII")
            Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)

            If (Not sSiteOption Is Nothing AndAlso sSiteOption.Equals("USER")) AndAlso
             Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) Then
                'sJobType.Equals(Config.INSTANCE.doGetJopTypeIncoming())  Then
                Dim sBranch As String = getDFValue(oForm, msRowTable, "U_Branch")
                Dim sGroup As String = Config.Parameter("BGSDispo")

                setSharedData(oForm, "TRG", "ST")
                If Config.INSTANCE.getParameterAsBool(msOrderType & "BPFLT", True) = False Then
                    setSharedData(oForm, "IDH_BPCOD", "")
                    setSharedData(oForm, "IDH_NAME", "")
                    setSharedData(oForm, "IDH_TYPE", "")
                    setSharedData(oForm, "IDH_GROUP", "")
                    setSharedData(oForm, "IDH_BRANCH", "")
                Else
                    setSharedData(oForm, "IDH_BPCOD", "")
                    setSharedData(oForm, "IDH_TYPE", "S")
                    setSharedData(oForm, "IDH_GROUP", sGroup)
                    setSharedData(oForm, "IDH_BRANCH", sBranch)
                End If
                'setSharedData(oForm, "CALLEDITEM", sItemID)
                setSharedData(oForm, "SILENT", "SHOWMULTI")
                goParent.doOpenModalForm("IDHCSRCH", oForm)
            End If

            thisDOR(oForm).doGetAllUOM()
            doSetUpdate(oForm)
        End Sub

        Protected Overrides Function doSetProducerAddress(ByVal oForm As SAPbouiCOM.Form, ByRef oData As ArrayList, ByVal bFromUser As Boolean, Optional ByVal bUpdateCustomer As Boolean = True _
                                                                , Optional ByVal bDisableProducerNewAddressButton As Boolean = True) As Boolean
            Dim bResult As Boolean = MyBase.doSetProducerAddress(oForm, oData, bFromUser, bUpdateCustomer)
            Dim sOrigin As String
            If Config.INSTANCE.getParameterAsBool("DOORLU", False) = False Then
                'setUFValue(oForm, "IDH_Origin", getDFValue(oForm, "@IDH_DISPORD", "U_PCity"))
                sOrigin = getDFValue(oForm, msRowTable, "U_Origin")
                If sOrigin Is Nothing OrElse sOrigin.Length = 0 Then
                    setDFValue(oForm, msRowTable, "U_Origin", getDFValue(oForm, "@IDH_DISPORD", "U_PCity"))
                End If
            End If
            Return bResult
        End Function

        Protected Overrides Sub doChooseLic(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, sItemID As String)
            MyBase.doChooseLic(oForm, bCanDoSilent, sItemID)
        End Sub

        Protected Overrides Sub doChooseCarrier(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, sItemID As String)
            'Dim sCardCode As String = getItemValue(oForm, "IDH_CARRIE")
            'Dim sCardName As String = getItemValue(oForm, "IDH_CARNAM")
            Dim sCardCode As String = getFormDFValue(oForm, "U_CCardCd", True)
            Dim sCardName As String = getFormDFValue(oForm, "U_CCardNM", True)

            setSharedData(oForm, "TRG", "WC")
            If sCardCode.Length > 0 OrElse
             sCardName.Length > 0 OrElse
             Config.INSTANCE.getParameterAsBool(msOrderType & "BPFLT", True) = False Then
                setSharedData(oForm, "IDH_TYPE", "")
                setSharedData(oForm, "IDH_GROUP", "")
                setSharedData(oForm, "IDH_BRANCH", "")
            Else
                setSharedData(oForm, "IDH_TYPE", "C")
                setSharedData(oForm, "IDH_GROUP", Config.Parameter("BGCWCarr"))
            End If

            setSharedData(oForm, "IDH_BPCOD", sCardCode)
            setSharedData(oForm, "IDH_NAME", sCardName)

            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            setSharedData(oForm, "CALLEDITEM", sItemID)
            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub
        '## Start 01-10-2013
        Private Sub doChooseWasteCode(ByVal oForm As SAPbouiCOM.Form, ByVal sItemID As String)
            ''## MA Start 18-11-2014 
            'sItemId = "IDH_WASCL1" OrElse sItemId = "IDH_WASMAT" 
            If wasSetWithCode(oForm, sItemID) Then
                Return
            End If
            Dim sWItem As String = ""
            Dim sWItemName As String = ""
            Dim sGrp As String = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup()
            If sItemID <> "IDH_WASCF" Then 'if called from Waste code/desc text box
                'With getFormMainDataSource(oForm)
                sWItem = getDFValue(oForm, msRowTable, "U_WasCd")
                sWItemName = getDFValue(oForm, msRowTable, "U_WasDsc") '.GetValue("U_WasDsc", .Offset).Trim()
                If sItemID = "IDH_WASCL1" Then 'AndAlso sWItem.Trim.Length = 0 AndAlso sWItemName.Trim.Length > 0 Then 'if nothing in code then neglect CFL
                    setDFValue(oForm, msRowTable, "U_WasDsc", "") '.SetValue("U_WasDsc", .Offset, "")
                    sWItemName = ""
                    doFillAlternateItemDescription(oForm, "", True)
                ElseIf sItemID = "IDH_WASMAT" Then 'AndAlso sWItemName.Trim.Length = 0 AndAlso sWItem.Trim.Length > 0 Then 'if nothing in code then neglect CFL
                    setDFValue(oForm, msRowTable, "U_WasCd", "") '.SetValue("U_WasDsc", .Offset, "")
                    sWItem = ""
                    doFillAlternateItemDescription(oForm, "", True)
                End If
                'End With
                If sWItem.Trim.Length > 0 OrElse sWItemName.Trim.Length > 0 Then
                    setSharedData(oForm, "ACTIVEITM", sItemID)
                    doClearSharedData(oForm)
                    setSharedData(oForm, "TP", "WAST")
                    setSharedData(oForm, "IDH_GRPCOD", Config.INSTANCE.doWasteMaterialGroup())
                    setSharedData(oForm, "IDH_CRDCD", getDFValue(oForm, msRowTable, IDH_DISPROW._CustCd))
                    setSharedData(oForm, "IDH_ITMCOD", sWItem)
                    setSharedData(oForm, "IDH_ITMNAM", sWItemName)
                    setSharedData(oForm, "IDH_INVENT", "") '"N")
                    setSharedData(oForm, "SILENT", "SHOWMULTI")
                    setSharedData(oForm, "IDH_JOBTP", getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp))
                    setSharedData(oForm, "CALLEDITEM", sItemID)
                    If getSharedData(oForm, "IDH_ITMSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_ITMSRCHOPEND") = "" Then
                        goParent.doOpenModalForm("IDHWISRC", oForm)
                    End If
                End If

            Else 'if called by CFL button

                sWItem = getDFValue(oForm, msRowTable, "U_WasCd")
                sWItemName = getDFValue(oForm, msRowTable, "U_WasDsc") '.GetValue("U_WasDsc", .Offset).Trim()
                setSharedData(oForm, "ACTIVEITM", sItemID)
                doClearSharedData(oForm)
                setSharedData(oForm, "TP", "WAST")
                setSharedData(oForm, "IDH_GRPCOD", Config.INSTANCE.doWasteMaterialGroup())
                setSharedData(oForm, "IDH_CRDCD", getDFValue(oForm, msRowTable, IDH_DISPROW._CustCd))
                setSharedData(oForm, "IDH_ITMCOD", sWItem)
                setSharedData(oForm, "IDH_ITMNAM", sWItemName)
                setSharedData(oForm, "IDH_INVENT", "") '"N")
                setSharedData(oForm, "SILENT", "")
                setSharedData(oForm, "IDH_JOBTP", getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp))

                If getSharedData(oForm, "IDH_ITMSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_ITMSRCHOPEND") = "" Then
                    goParent.doOpenModalForm("IDHWISRC", oForm)
                End If
            End If
            ''## MA End 18-11-2014 
            ' '''
        End Sub
        '## End
        Protected Overrides Sub doChooseProducer(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, sItemID As String)
            'Dim sCardCode As String = getItemValue(oForm, "IDH_WPRODU")
            'Dim sCardName As String = getItemValue(oForm, "IDH_WNAM")
            Dim sCardCode As String = getFormDFValue(oForm, "U_PCardCd", True)
            Dim sCardName As String = getFormDFValue(oForm, "U_PCardNM", True)

            setSharedData(oForm, "TRG", "PR")
            If sCardCode.Length > 0 OrElse
                sCardName.Length > 0 OrElse
                Config.INSTANCE.getParameterAsBool(msOrderType & "BPFLT", True) = False Then
                setSharedData(oForm, "IDH_TYPE", "")
                setSharedData(oForm, "IDH_GROUP", "")
                setSharedData(oForm, "IDH_BRANCH", "")
            Else
                setSharedData(oForm, "IDH_TYPE", "S")
                setSharedData(oForm, "IDH_GROUP", "")
            End If

            setSharedData(oForm, "IDH_BPCOD", sCardCode)
            setSharedData(oForm, "IDH_NAME", sCardName)
            setSharedData(oForm, "CALLEDITEM", sItemID)
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub

        Private Sub doCheckAsAdd(ByVal oForm As SAPbouiCOM.Form)
            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                doSetDOAddDefaults(oForm, False)
            End If
        End Sub

        Public Overrides Sub doSetAddDefaults(ByVal oForm As SAPbouiCOM.Form)
            doSetDOAddDefaults(oForm, True, True, True, True, True)
        End Sub

        Public Sub doSetDOAddDefaults(ByVal oForm As SAPbouiCOM.Form,
         ByVal bSetJobType As Boolean,
         Optional ByVal bSetCustomerAddress As Boolean = True,
         Optional ByVal bSetCarrierAddress As Boolean = True,
         Optional ByVal bSetDisposalAddress As Boolean = True,
         Optional ByVal bSetProducerAddress As Boolean = True)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            Dim oDO As IDH_DISPORD = thisDO(oForm)
            Dim sJobType As String

            If bSetJobType = True Then
                Dim oPreSelect As PreSelect = getWFValue(oForm, "PRESELECT")
                If oPreSelect Is Nothing Then
                    sJobType = Config.INSTANCE.doGetJopTypeIncoming()
                Else
                    sJobType = oDOR.TRNCdDecodeRecord.JobType
                End If

                'sJobType = Config.INSTANCE.doGetJopTypeIncoming()
                setDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, sJobType, True)
                doHideLoadSheet(oForm)
                'setEnableItems(oForm, False, moCostOutgoingItems)
            Else
                sJobType = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, True)
                doShowLoadSheet(oForm)
                'setEnableItems(oForm, True, moCostOutgoingItems)
            End If

            If oForm.PaneLevel <> 10 Then
                doSetFocus(oForm, "IDH_WEIGH")
            End If
            doSetFocus(oForm, "IDH_VEHREG")

            'doBusinessPartnerRules(oForm, False, False, False, False)
            doBusinessPartnerRules(oForm, bSetCustomerAddress, bSetCarrierAddress, bSetDisposalAddress, bSetProducerAddress)
            If oDO.U_CardCd <> "" Then
                setEnableItem(oForm, True, "IDH_CUSADD")
            End If
            If oDOR.U_CarrCd <> "" Then
                setEnableItem(oForm, True, "IDH_ADDRES")
            End If
            If oDOR.U_ProCd <> "" Then
                setEnableItem(oForm, True, "IDH_PRDADD")
            End If
            If oDO.U_SCardCd <> "" AndAlso isItemEnabled(oForm, "IDH_DISSIT") Then
                setEnableItem(oForm, True, "IDH_SITADD")
            End If

        End Sub

        Public Overrides Sub doSetFindDefaults(ByVal oForm As SAPbouiCOM.Form)
            Dim sJobType As String = Config.INSTANCE.doGetJopTypeIncoming()
            setDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, sJobType, True)
            doHideLoadSheet(oForm)

            If oForm.PaneLevel <> 10 Then
                doSetFocus(oForm, "IDH_WEIGH")
            End If

            doSetFocus(oForm, "IDH_VEHREG")
        End Sub

        Public Sub doBusinessPartnerRules(ByVal oForm As SAPbouiCOM.Form,
                ByVal bDoCustAddr As Boolean,
                ByVal bDoCarrAddr As Boolean,
                ByVal bDoSiteAddr As Boolean,
                ByVal bDoProdAddr As Boolean)

            'IGNORE THIS IF THE DO IS LINKED TO A WO
            '            Dim sWOLink As String = getDFValue(oForm, msRowTable, "U_WROrd")
            '            If Not sWOLink Is Nothing AndAlso sWOLink.Length > 0 Then
            '            	Exit Sub
            '            End If
            Dim oDO As IDH_DISPORD = thisDO(oForm)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

            Dim oAddressData As ArrayList
            Dim sVType As String = getWFValue(oForm, "VEHTYPE")
            If sVType Is Nothing OrElse sVType.Length = 0 Then
                sVType = "A"
            End If

            Dim sCustomerCode As String = oDO.U_CardCd 'getFormDFValue(oForm, "U_CardCd")
            Dim sCustomerName As String = oDO.U_CardNM 'getFormDFValue(oForm, "U_CardNM")
            Dim sPh1 As String = oDO.U_Phone1 '(oForm, "U_Phone1")

            Dim sWasteCarrierCode As String = oDO.U_CCardCd 'getFormDFValue(oForm, "U_CCardCd")
            Dim sWasteCarrierName As String = oDO.U_CCardNM 'getFormDFValue(oForm, "U_CCardNM")

            Dim sDisposalSiteCode As String = oDO.U_SCardCd 'getFormDFValue(oForm, "U_SCardCd")
            Dim sDisposalSiteOld As String = sDisposalSiteCode
            Dim sDisposalSiteName As String = oDO.U_SCardNM 'getFormDFValue(oForm, "U_SCardNM")

            Dim sProducerCode As String = oDO.U_PCardCd 'getFormDFValue(oForm, "U_PCardCd")
            Dim sProducerName As String = oDO.U_PCardNM 'getFormDFValue(oForm, "U_PCardNM")

            Dim sCustomerOption As String
            Dim sCarrierOption As String
            Dim sProducerOption As String
            Dim sSiteOption As String
            Dim bJobIsIn As Boolean = Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp))
            'Dim bJobIsIn As Boolean = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp).Equals(Config.INSTANCE.doGetJopTypeIncoming())

            If bJobIsIn Then
                sCustomerOption = Config.ParameterWithDefault("DODECUI", "NONE") ' (BLANK, DB, NONE, CARDCODE, CARRIER)
                sCarrierOption = Config.ParameterWithDefault("DODEWCI", "NONE") ' (BLANK, DB, NONE, CARDCODE, CUSTOMER, LBPCUST)
                sProducerOption = Config.ParameterWithDefault("DODEPRI", "NONE") ' (BLANK, DB, NONE, CARDCODE, CUSTOMER)
                sSiteOption = Config.ParameterWithDefault("DODEDII", "NONE") ' (BLANK, DB, NONE, CARDCODE, USER, USERWH)
            Else
                sCustomerOption = Config.ParameterWithDefault("DODECUO", "NONE") ' (BLANK, DB, NONE, CARDCODE)
                sCarrierOption = Config.ParameterWithDefault("DODEWCO", "NONE") ' (BLANK, DB, NONE, CARDCODE, LBPCUST)
                sProducerOption = Config.ParameterWithDefault("DODEPRO", "NONE") ' (BLANK, DB, NONE, CARDCODE)
                sSiteOption = Config.ParameterWithDefault("DODEDIO", "NONE") ' (BLANK, DB, NONE, CARDCODE, CUSTOMER, USERWH)
            End If

            Dim sCompanyName As String = goParent.goDICompany.CompanyName
            If Not sSiteOption Is Nothing AndAlso sSiteOption.Equals("USER") Then
                If getWFValue(oForm, "FindBranchDisposalSite") = True Then
                    'AndAlso wasSetWithCode(oForm, "IDH_DISSIT" ) Then
                    sDisposalSiteCode = ""
                End If
            End If

            Dim sPrevCustomer As String = sCustomerCode
            Dim sPrevCarrier As String = sWasteCarrierCode
            Dim sPrevProducer As String = sProducerCode
            Dim sPrevSite As String = sDisposalSiteCode

            setWFValue(oForm, "FindBranchDisposalSite", True)

            Dim bCustChanged As Boolean = False
            Dim bCarrierChanged As Boolean = False
            Dim bDisposalSiteChanged As Boolean = False
            Dim bProducerChanged As Boolean = False

            For iTry As Integer = 0 To 1
                bCustChanged = False
                bCarrierChanged = False
                bCustChanged = False
                bCustChanged = False

                ''***********************************************************
                ''  CUSTOMER
                ''***********************************************************
                If sCustomerOption.Equals("BLANK") Then
                    'setFormDFValue(oForm, "U_CardCd", "")
                    'setFormDFValue(oForm, "U_CardNM", "")

                    sCustomerCode = ""
                    sCustomerName = ""
                Else
                    If sCustomerCode.Length = 0 AndAlso sCustomerOption.Equals("IGNORE") = False Then
                        If sCustomerOption.Length = 0 OrElse sCustomerOption.Equals("NONE") Then
                            If bJobIsIn Then
                                If sWasteCarrierCode.Length() > 0 AndAlso sVType.ToUpper().Equals("S") Then
                                    sCustomerCode = sWasteCarrierCode
                                    sCustomerName = sWasteCarrierName
                                End If
                            End If
                        ElseIf sCustomerOption.Equals("DB") Then
                            sCustomerCode = sCompanyName
                            sCustomerName = sCompanyName
                        ElseIf sCustomerOption.Equals("CARRIER") Then
                            If sWasteCarrierCode.Length() > 0 Then
                                sCustomerCode = sWasteCarrierCode
                                sCustomerName = sWasteCarrierName
                            End If
                        ElseIf sCustomerOption.Equals("PRODUCER") Then
                            If sProducerCode.Length() > 0 Then
                                sCustomerCode = sProducerCode
                                sCustomerName = sProducerName
                            End If
                        ElseIf sCustomerOption.Equals("SITE") Then
                            If sDisposalSiteCode.Length() > 0 Then
                                sCustomerCode = sDisposalSiteCode
                                sCustomerName = sDisposalSiteName
                            End If
                        Else
                            sCustomerCode = sCustomerOption
                            If WR1_Search.idh.forms.search.BPSearch.doGetBPInfo(goParent, oForm, sCustomerCode) Then
                                doSetChoosenCustomerFromShared(oForm, False)
                                sCustomerName = oDO.U_CardNM 'getDFValue(oForm, "@IDH_DISPORD", "U_CardNM")
                            End If
                        End If
                    End If

                    'If sCustomerCode.Length > 0 Then
                    If sPrevCustomer.Equals(sCustomerCode) = False Then
                        bCustChanged = True

                        'setFormDFValue(oForm, "U_CardCd", sCustomerCode)
                        'setFormDFValue(oForm, "U_CardNM", sCustomerName)

                        'setDFValue(oForm, msRowTable, IDH_DISPROW._CustCd, sCustomerCode)
                        'setDFValue(oForm, msRowTable, IDH_DISPROW._CustNm, sCustomerName)

                        oDO.U_CardCd = sCustomerCode
                        oDO.U_CardNM = sCustomerName

                        '	                    If sPrevCustomer.Equals(sCustomerCode) = False Then
                        '	                        bDoCustAddr = True
                        '	                    Else
                        '	                        bDoCustAddr = False
                        '	                    End If
                        sPrevCustomer = sCustomerCode
                    End If

                    If bDoCustAddr OrElse bCustChanged Then
                        If sCustomerCode.Equals(sCompanyName) = True Then
                            oAddressData = WR1_Search.idh.forms.search.AddressSearch.doGetCompanyAddress(goParent)
                        Else
                            oAddressData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sCustomerCode, "S")
                            FilterRestrictedAddress(oAddressData, sCustomerCode)
                        End If

                        If doSetCustomerAddress(oForm, oAddressData, sCustomerCode, sPh1) = False Then
                            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                'setDFValue(oForm, msRowTable, "U_Lorry", "", True)
                                'setDFValue(oForm, msRowTable, "U_LorryCd", "")
                                'setDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, "", True)

                                oDOR.U_Lorry = String.Empty
                                oDOR.U_LorryCd = String.Empty
                                oDOR.U_JobTp = String.Empty

                                setUFValue(oForm, "IDH_TARWEI", 0)
                                setUFValue(oForm, "IDH_TRLTar", 0)
                                ''MA Start 05-02-2015 Issue#554
                                setUFValue(oForm, "IDH_TRWTE1", "")
                                setUFValue(oForm, "IDH_TRWTE2", "")
                                ''MA End 05-02-2015 Issue#554

                                oForm.Title = getTranslatedCaption("#TITLE", "Disposal Order")
                            End If
                        End If
                    End If
                End If

                ''***********************************************************
                ''  CARRIER
                ''***********************************************************
                If sCarrierOption.Equals("BLANK") Then
                    'setDFValue(oForm, "@IDH_DISPORD", "U_CCardCd", "")
                    'setDFValue(oForm, "@IDH_DISPORD", "U_CCardNM", "")

                    sWasteCarrierCode = ""
                    sWasteCarrierName = ""
                Else
                    If sWasteCarrierCode.Length = 0 AndAlso sCarrierOption.Equals("IGNORE") = False Then
                        If sCarrierOption.Length = 0 OrElse sCarrierOption.Equals("NONE") Then
                            If sVType.ToUpper().Equals("S") Then
                                If bJobIsIn Then
                                    sWasteCarrierCode = sCompanyName
                                    sWasteCarrierName = sCompanyName
                                Else
                                    If sDisposalSiteCode.Length > 0 Then
                                        sWasteCarrierCode = sDisposalSiteCode
                                        sWasteCarrierName = sDisposalSiteName
                                    End If
                                End If
                            End If
                        ElseIf sCarrierOption.Equals("DB") Then
                            sWasteCarrierCode = sCompanyName
                            sWasteCarrierName = sCompanyName
                        ElseIf sCarrierOption.Equals("PRODUCER") Then
                            If sProducerCode.Length() > 0 Then
                                sWasteCarrierCode = sProducerCode
                                sWasteCarrierName = sProducerName
                            End If
                        ElseIf sCarrierOption.Equals("SITE") Then
                            If sDisposalSiteCode.Length() > 0 Then
                                sWasteCarrierCode = sDisposalSiteCode
                                sWasteCarrierName = sDisposalSiteName
                            End If
                        ElseIf sCarrierOption.Equals("CUSTOMER") Then
                            If sCustomerCode.Length() > 0 Then
                                sWasteCarrierCode = sCustomerCode
                                sWasteCarrierName = sCustomerName
                            End If
                        ElseIf sCarrierOption.Equals("LBPCUST") Then
                            If sCustomerCode.Length() > 0 Then
                                Dim oLnkBP As LinkedBP = New LinkedBP()
                                If oLnkBP.doGetLinkedBP(sCustomerCode) Then
                                    sWasteCarrierCode = oLnkBP.LinkedBPCardCode
                                    sWasteCarrierName = oLnkBP.LinkedBPName
                                End If
                            End If
                        Else
                            sWasteCarrierCode = sCarrierOption
                            If WR1_Search.idh.forms.search.BPSearch.doGetBPInfo(goParent, oForm, sWasteCarrierCode) Then
                                doSetChoosenCarrier(oForm, False)
                                'sWasteCarrierName = getDFValue(oForm, "@IDH_DISPORD", "U_CCardNM")
                                sWasteCarrierName = oDO.U_CCardNM
                            End If
                        End If
                    End If

                    'If sWasteCarrierCode.Length > 0 Then
                    If sPrevCarrier.Equals(sWasteCarrierCode) = False Then
                        bCarrierChanged = True

                        'setDFValue(oForm, "@IDH_DISPORD", "U_CCardCd", sWasteCarrierCode)
                        'setDFValue(oForm, "@IDH_DISPORD", "U_CCardNM", sWasteCarrierName)
                        oDO.U_CCardCd = sWasteCarrierCode
                        oDO.U_CCardNM = sWasteCarrierName

                        '## Start 24-06-2014
                        Dim sCarrWasLicNo As String = Config.INSTANCE.getValueFromBP(sWasteCarrierCode, "U_WasLic")
                        'setFormDFValue(oForm, "U_CWasLic", sCarrWasLicNo)
                        oDO.U_CWasLic = sCarrWasLicNo
                        '## End 24-06-2014

                        ''Update all the linked rows values
                        Dim oParams As Hashtable = New Hashtable()
                        oParams.Add("U_CarrCd", sWasteCarrierCode)
                        oParams.Add("U_CarrNm", sWasteCarrierName)
                        setAllRowsValues(oForm, oParams)

                        'If sPrevCarrier.Equals(sWasteCarrierCode) = False Then
                        '    bDoCarrAddr = True
                        'Else
                        '    bDoCarrAddr = False
                        'End If
                        sPrevCarrier = sWasteCarrierCode
                    End If

                    If bCarrierChanged OrElse bDoCarrAddr Then
                        If sWasteCarrierCode.Equals(sCompanyName) = True Then
                            oAddressData = WR1_Search.idh.forms.search.AddressSearch.doGetCompanyAddress(goParent)
                        Else
                            oAddressData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sWasteCarrierCode, "S")
                        End If
                        doSetCarrierAddress(oForm, oAddressData)
                    End If
                End If

                ''***********************************************************
                ''  DISPOSAL SITE
                ''***********************************************************
                If sSiteOption.Equals("BLANK") Then
                    'setDFValue(oForm, "@IDH_DISPORD", "U_SCardCd", "")
                    'setDFValue(oForm, "@IDH_DISPORD", "U_SCardNM", "")

                    sDisposalSiteCode = ""
                    sDisposalSiteName = ""
                Else
                    If sDisposalSiteCode.Length = 0 AndAlso sDisposalSiteCode.Equals("IGNORE") = False Then
                        If sSiteOption.Length = 0 OrElse sSiteOption.Equals("NONE") Then

                        ElseIf sSiteOption.Equals("DB") Then 'OrElse sSiteOption.Equals("USER") Then
                            sDisposalSiteCode = sCompanyName
                            sDisposalSiteName = sCompanyName
                        ElseIf sSiteOption.Equals("USER") Then
                            'try to select the disposal site linked to the users branch
                            'Dim sBranch As String = getDFValue(oForm, msRowTable, "U_Branch")
                            Dim sBranch As String = oDOR.U_Branch

                            Dim sGroup As String = Config.Parameter("BGSDispo")
                            Dim sQuery As String = "Select CardCode, CardName FROM OCRD WHERE U_IDHBRAN = '" & sBranch & "' And CardType='S' And GroupCode in (" & sGroup & ")"
                            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                            Try
                                oRecordSet = goParent.goDB.doSelectQuery(sQuery)
                                If Not oRecordSet Is Nothing AndAlso oRecordSet.RecordCount > 0 Then
                                    sDisposalSiteCode = oRecordSet.Fields.Item(0).Value
                                    sDisposalSiteName = oRecordSet.Fields.Item(1).Value
                                Else
                                    If Not sDisposalSiteOld Is Nothing AndAlso sDisposalSiteOld.Length > 0 Then
                                        sDisposalSiteCode = sDisposalSiteOld
                                    Else
                                        sDisposalSiteCode = sCompanyName
                                        sDisposalSiteName = sCompanyName
                                    End If
                                End If
                            Catch ex As Exception
                                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error applying BP rules.")
                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXABPR", {Nothing})
                            Finally
                                IDHAddOns.idh.data.Base.doReleaseObject(Nothing)
                            End Try

                        ElseIf sSiteOption.Equals("CARRIER") Then
                            If sWasteCarrierCode.Length() > 0 Then
                                sDisposalSiteCode = sWasteCarrierCode
                                sDisposalSiteName = sWasteCarrierName
                            End If
                        ElseIf sSiteOption.Equals("PRODUCER") Then
                            If sProducerCode.Length() > 0 Then
                                sDisposalSiteCode = sProducerCode
                                sDisposalSiteName = sProducerName
                            End If
                        ElseIf sSiteOption.Equals("CUSTOMER") Then
                            If sCustomerCode.Length() > 0 Then
                                sDisposalSiteCode = sCustomerCode
                                sDisposalSiteName = sCustomerName
                            End If
                        ElseIf sSiteOption.Equals("USERWH") Then
                            sDisposalSiteCode = Config.INSTANCE.doGetUserWarehouseDisposalBPCode()
                            If sDisposalSiteCode IsNot Nothing AndAlso sDisposalSiteCode.Length > 0 Then
                                sDisposalSiteName = Config.INSTANCE.doGetBPName(sDisposalSiteCode)
                            End If
                        Else
                            sDisposalSiteCode = sSiteOption
                            If WR1_Search.idh.forms.search.BPSearch.doGetBPInfo(goParent, oForm, sDisposalSiteCode) Then
                                doSetChoosenSite(oForm, False)
                                'sDisposalSiteName = getDFValue(oForm, "@IDH_DISPORD", "U_SCardNM")
                                sDisposalSiteName = oDO.U_SCardNM
                            End If
                        End If
                    End If

                    'If sDisposalSiteCode.Length > 0 Then
                    If sPrevSite.Equals(sDisposalSiteCode) = False Then
                        bDisposalSiteChanged = True

                        'setDFValue(oForm, "@IDH_DISPORD", "U_SCardCd", sDisposalSiteCode)
                        'setDFValue(oForm, "@IDH_DISPORD", "U_SCardNM", sDisposalSiteName)
                        oDO.U_SCardCd = sDisposalSiteCode
                        oDO.U_SCardNM = sDisposalSiteName

                        'If sPrevSite.Equals(sDisposalSiteCode) = False Then
                        '    bDoSiteAddr = True
                        'Else
                        '    bDoSiteAddr = False
                        'End If
                        sPrevSite = sDisposalSiteCode
                    End If

                    If bDisposalSiteChanged OrElse bDoSiteAddr Then
                        If sSiteOption.Equals("USERWH") Then
                            Dim sAddress As String = Config.INSTANCE.doGetUserWarehouseAddress()
                            If sAddress IsNot Nothing AndAlso sAddress.Length > 0 Then
                                oAddressData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sDisposalSiteCode, "S", sAddress)
                            Else
                                oAddressData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sDisposalSiteCode, "S")
                            End If
                        ElseIf sDisposalSiteCode.Equals(sCompanyName) = True Then
                            oAddressData = WR1_Search.idh.forms.search.AddressSearch.doGetCompanyAddress(goParent)
                        Else
                            oAddressData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sDisposalSiteCode, "S")
                            FilterRestrictedAddress(oAddressData, sDisposalSiteCode)
                        End If

                        If oAddressData IsNot Nothing AndAlso oAddressData.Count > 0 Then
                            doSetSiteAddress(oForm, oAddressData)
                        End If
                    End If
                End If

                ''***********************************************************
                ''  PRODUCER
                ''***********************************************************
                If sProducerOption.Equals("BLANK") Then
                    'setDFValue(oForm, "@IDH_DISPORD", "U_PCardCd", "")
                    'setDFValue(oForm, "@IDH_DISPORD", "U_PCardNM", "")

                    sProducerCode = ""
                    sProducerName = ""
                Else
                    If ((wasSetWithCode(oForm, "IDH_WPRODU") AndAlso wasSetWithCode(oForm, "IDH_PROCD")) OrElse sProducerCode.Length = 0) AndAlso sProducerOption.Equals("IGNORE") = False Then
                        ''If sProducerCode.Length = 0 Then
                        If sProducerOption Is Nothing OrElse sProducerOption.Length = 0 OrElse sProducerOption.Equals("NONE") Then
                            If bJobIsIn Then
                                If sWasteCarrierCode.Length() > 0 AndAlso sVType.ToUpper().Equals("S") Then
                                    ''SET THE EMPTY PRODUCER TO WAST CARRIER
                                    sProducerCode = sWasteCarrierCode
                                    sProducerName = sWasteCarrierName
                                Else
                                    ''PRODUCER
                                    If sCustomerCode.Length > 0 Then
                                        sProducerCode = sCustomerCode
                                        sProducerName = sCustomerName
                                    End If
                                End If
                            End If
                        ElseIf sProducerOption.Equals("DB") Then
                            sProducerCode = sCompanyName
                            sProducerName = sCompanyName
                        ElseIf sProducerOption.Equals("CARRIER") Then
                            If sWasteCarrierCode.Length() > 0 Then
                                sProducerCode = sWasteCarrierCode
                                sProducerName = sWasteCarrierName
                            End If
                        ElseIf sProducerOption.Equals("SITE") Then
                            If sDisposalSiteCode.Length() > 0 Then
                                sProducerCode = sDisposalSiteCode
                                sProducerName = sDisposalSiteName
                            End If
                        ElseIf sProducerOption.Equals("CUSTOMER") Then
                            If sCustomerCode.Length() > 0 Then
                                sProducerCode = sCustomerCode
                                sProducerName = sCustomerName
                            End If
                        ElseIf sProducerOption.Equals("USER") Then
                        Else
                            sProducerCode = sProducerOption
                            If WR1_Search.idh.forms.search.BPSearch.doGetBPInfo(goParent, oForm, sProducerCode) Then
                                doSetChoosenProducer(oForm, False)
                                'sProducerName = getDFValue(oForm, "@IDH_DISPORD", "U_PCardNM")
                                sProducerName = oDO.U_PCardNM
                            End If
                        End If
                    End If

                    'If sProducerCode.Length > 0 AndAlso wasSetWithCode(oForm,"IDH_WPRODU") Then
                    If sPrevProducer.Equals(sProducerCode) = False AndAlso wasSetWithCode(oForm, "IDH_WPRODU") Then
                        bProducerChanged = True

                        'setDFValue(oForm, "@IDH_DISPORD", "U_PCardCd", sProducerCode)
                        'setDFValue(oForm, "@IDH_DISPORD", "U_PCardNM", sProducerName)

                        'setDFValue(oForm, msRowTable, "U_ProCd", sProducerCode)
                        'setDFValue(oForm, msRowTable, "U_ProNm", sProducerName)

                        oDO.U_PCardCd = sProducerCode
                        oDO.U_PCardNM = sProducerName

                        'If sPrevProducer.Equals(sProducerCode) = False Then
                        '    bDoProdAddr = True
                        'Else
                        '    bDoProdAddr = False
                        'End If
                        sPrevProducer = sProducerCode
                    End If

                    If bProducerChanged OrElse bDoProdAddr Then
                        If sProducerCode.Equals(sCompanyName) = True Then
                            oAddressData = WR1_Search.idh.forms.search.AddressSearch.doGetCompanyAddress(goParent)
                        Else
                            oAddressData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sProducerCode, "S")
                            FilterRestrictedAddress(oAddressData, sProducerCode)
                        End If
                        doSetProducerAddress(oForm, oAddressData, False)
                    End If
                    'doSetProducerAddress(oForm, oAddressData, False)
                End If

                If bCustChanged = False AndAlso
                   bCarrierChanged = False AndAlso
                   bDisposalSiteChanged = False AndAlso
                   bProducerChanged = False Then
                    Exit For
                Else
                    bDoCustAddr = False
                    bDoCarrAddr = False
                    bDoSiteAddr = False
                    bDoProdAddr = False
                End If
            Next
        End Sub

        Protected Overrides Function doSetCustomerAddress(ByVal oForm As SAPbouiCOM.Form, ByRef oData As System.Collections.ArrayList, ByVal sCardCode As String, ByVal sPhone1 As String,
                                                          Optional ByVal bUpdateProdAddress As Boolean = True, Optional ByVal bDisableCustomerNewAddressButton As Boolean = True) As Boolean
            If Not (oData Is Nothing) AndAlso oData.Count > 0 Then
                Dim sAddress As String = oData.Item(0)
                Dim sCustRef As String
                sCustRef = oData.Item(12)

                Dim bFoc As String = getUFValue(oForm, "IDH_FOC")
                Dim dChargeTotal As Double = 0
                If bFoc.Equals("Y") = False Then
                    dChargeTotal = getUnCommittedRowTotals(oForm, getDFValue(oForm, msRowTable, "Code"), getDFValue(oForm, msRowTable, "U_Total"))
                End If

                If Config.INSTANCE.doGetPOLimitsDOWO("DO", sCardCode, sAddress, sCustRef, dChargeTotal) = False Then
                    Return False
                End If
            End If
            Return MyBase.doSetCustomerAddress(oForm, oData, sCardCode, sPhone1)

            'UPDATE THE ADDITIONAL GRID FROM HERE FOR NOW - IN FUTURE THE DBOBJECT WILL BE USED
            doRefreshAdditionalPrices(oForm)

        End Function

        Protected Overrides Sub doSetSiteAddress(ByVal oForm As SAPbouiCOM.Form, ByRef oData As ArrayList)
            'Dim sSiteAddr As String = getDFValue(oForm, msRowTable, IDH_DISPROW._SAddress)
            '20150227 - LPV - BEGIN - Update the Row Site Address as well
            'If sSiteAddr Is Nothing OrElse sSiteAddr.Length = 0 Then
            If oData IsNot Nothing AndAlso oData.Count > 0 Then
                setDFValue(oForm, msRowTable, IDH_DISPROW._SAddress, oData.Item(0))
                setDFValue(oForm, msRowTable, IDH_DISPROW._SAddrsLN, oData.Item(30))

                'Else
                'setDFValue(oForm, msRowTable, IDH_DISPROW._SAddress, "")
                'End If
                'End If
                '20150227 - LPV - END - Update the Row Site Address as well
                MyBase.doSetSiteAddress(oForm, oData)
            End If
        End Sub

        'Private Sub getAutoAdditionalCodes(ByVal oForm As SAPbouiCOM.Form, ByVal sWORCode As String)
        '    Dim oDOR As IDH_DISPROW = thisDOR(oForm)
        '    Dim oWORAdditionalItems As New IDH_WOADDEXP()
        '    oWORAdditionalItems.getByOrderRow(sWORCode)

        '    '  Dim oItem As com.idh.dbObjects.User.Item

        '    ''oList = oDOR.getAutoAdditionalCodes()

        '    If oWORAdditionalItems IsNot Nothing AndAlso oWORAdditionalItems.Count > 0 Then
        '        Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
        '        oWORAdditionalItems.first()
        '        While oWORAdditionalItems.next()

        '            If Not oWORAdditionalItems.U_ItemCd Is Nothing AndAlso oWORAdditionalItems.U_ItemCd.Length > 0 Then
        '                'Check if the item is allready in the Grid and only adds it if it does not
        '                If oGrid.doIndexOfItem(oWORAdditionalItems.U_ItemCd) < 0 Then
        '                    Dim sJobNr As String = getFormDFValue(oForm, "Code", True)
        '                    'Dim sRowNr As String = getDFValue(oForm, msRowTable, "Code")

        '                    Dim sCustCd As String = oWORAdditionalItems.U_CustCd
        '                    Dim sCustNm As String = oWORAdditionalItems.U_CustNm
        '                    Dim sCustCurr As String = oWORAdditionalItems.U_CustCurr 'Config.INSTANCE.getValueFromBP(sCustCd, "Currency")

        '                    Dim sSuppCd As String = oWORAdditionalItems.U_SuppCd '"" 'getDFValue(oForm, msRowTable, "U_CongCd")
        '                    Dim sSuppNm As String = oWORAdditionalItems.U_SuppNm '"" 'getDFValue(oForm, msRowTable, "U_SCngNm")
        '                    Dim sSuppCurr As String = Config.INSTANCE.getValueFromBP(sSuppCd, "Currency")
        '                    Dim sEmpId As String = oWORAdditionalItems.U_EmpId
        '                    Dim sEmpNm As String = oWORAdditionalItems.U_EmpNm
        '                    Dim sChrgVatGrp As String = oWORAdditionalItems.U_ChrgVatGrp 'Config.INSTANCE.doGetSalesVatGroup(sCustCd, oWORAdditionalItems.U_ItemCd)
        '                    Dim sCostVatGrp As String = oWORAdditionalItems.U_CostVatGrp 'Config.INSTANCE.doGetPurchaceVatGroup(sSuppCd, oWORAdditionalItems.U_ItemCd)
        '                    '  sChrgVatGrp = Config.INSTANCE.doGetSalesVatGroup(sCustCd, oWORAdditionalItems.U_ItemCd)
        '                    'Dim dCost As Double = 0
        '                    oGrid.doAddExpense(sJobNr, oDOR.Code, _
        '                        sCustCd, sCustNm, sCustCurr, _
        '                        sSuppCd, sSuppNm, sSuppCurr, _
        '                        sEmpId, sEmpNm, _
        '                        oWORAdditionalItems.U_ItemCd, oWORAdditionalItems.U_ItemDsc, _
        '                        oWORAdditionalItems.U_UOM, oWORAdditionalItems.U_Quantity, _
        '                        oWORAdditionalItems.U_ItmCost, oWORAdditionalItems.U_ItmChrg, _
        '                        sCostVatGrp, _
        '                        sChrgVatGrp, _
        '                      IIf(oWORAdditionalItems.U_FPCost = "Y", True, False), IIf(oWORAdditionalItems.U_FPChrg = "Y", True, False), _
        '                         True, False)
        '                End If
        '            End If
        '        End While
        '    End If
        'End Sub

        Private Sub doSetCurrencies(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bFromModalForm As Boolean = False)
            Dim sCustCd As String = getDFValue(oForm, msRowTable, "U_CustCd") 'getFormDFValue(oForm, "U_CustCd")
            Dim sProdCd As String = getDFValue(oForm, msRowTable, "U_ProCd")
            Dim sDispCd As String = getDFValue(oForm, msRowTable, "U_Tip")
            Dim sCarrCd As String = getDFValue(oForm, msRowTable, "U_CarrCd")
            'Dim sLiceCd As String = getDFValue(oForm, msRowTable, "")
            Dim sQry As String = ""

            Dim sCCur As String
            Dim sPCur As String
            Dim sDCur As String
            Dim sCrCur As String

            sQry = "select  " +
                " IsNull((select IsNull(Currency, '') from OCRD where CardCode = '" + sCustCd + "'), '') As Customer, " +
                " IsNull((select IsNull(Currency, '') from OCRD where CardCode = '" + sProdCd + "'), '') As Producer, " +
                " IsNull((select IsNull(Currency, '') from OCRD where CardCode = '" + sDispCd + "'), '') As Disposal, " +
                " IsNull((select IsNull(Currency, '') from OCRD where CardCode = '" + sCarrCd + "'), '') As Carrier "


            'If sCustCd IsNot Nothing AndAlso sCustCd.Length > 0 Then
            '    sQry = sQry + " Select CardCode, Currency from OCRD where CardCode = '" + sCustCd + "'"
            'End If
            Dim oAdminInfo As SAPbobsCOM.AdminInfo = goParent.goDICompany.GetCompanyService.GetAdminInfo()
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                oRecordSet.DoQuery(sQry)
                If oRecordSet.RecordCount > 0 Then
                    sCCur = oRecordSet.Fields.Item("Customer").Value.ToString()
                    sPCur = oRecordSet.Fields.Item("Producer").Value.ToString()
                    sDCur = oRecordSet.Fields.Item("Disposal").Value.ToString()
                    sCrCur = oRecordSet.Fields.Item("Carrier").Value.ToString()

                    If sCCur.Length < 1 OrElse sCCur.Equals("##") Then
                        sCCur = oAdminInfo.LocalCurrency
                    End If
                    If sPCur.Length < 1 OrElse sPCur.Equals("##") Then
                        sPCur = oAdminInfo.LocalCurrency
                    End If
                    If sDCur.Length < 1 OrElse sDCur.Equals("##") Then
                        sDCur = oAdminInfo.LocalCurrency
                    End If
                    If sCrCur.Length < 1 OrElse sCrCur.Equals("##") Then
                        sCrCur = oAdminInfo.LocalCurrency
                    End If

                    '/*
                    ''Dim oItem As SAPbouiCOM.Item

                    ' ''Charge fields
                    ''oItem = oForm.Items.Item("IDH_DCGCUR")
                    ''oItem.Specific.Caption = oItem.Specific.Caption.ToString().Split("(")(0) + ("(" + sDCur + ")") ' Customer Charge
                    ' ''oItem = oForm.Items.Item("IDH_HCGCUR")
                    ' ''oItem.Specific.Caption = oItem.Specific.Caption + ("(" + sDCur + ")") 'To be decided

                    ' ''Cost fields
                    ''oItem = oForm.Items.Item("IDH_DCOCUR")
                    ''oItem.Specific.Caption = oItem.Specific.Caption.ToString().Split("(")(0) + ("(" + sDCur + ")") 'Disposal Cost
                    ''oItem = oForm.Items.Item("IDH_CCOCUR")
                    ''oItem.Specific.Caption = oItem.Specific.Caption.ToString().Split("(")(0) + ("(" + sCrCur + ")") 'Haluage/Carrier Cost
                    ''oItem = oForm.Items.Item("IDH_PCOCUR")
                    ''oItem.Specific.Caption = oItem.Specific.Caption.ToString().Split("(")(0) + ("(" + sPCur + ")") 'Purchase Cost

                    ' ''setUFValue(oForm, "IDH_DCGCUR", sCCur)  'Disposal Charge from Customer Ccy 
                    ' ''setUFValue(oForm, "IDH_HCGCUR", sCCur)  'Haulier Charge from Customer Ccy

                    ' ''setUFValue(oForm, "IDH_PCOCUR", sPCur)  'Producer/Purchase
                    ' ''setUFValue(oForm, "IDH_DCOCUR", sDCur)  'Disposal 
                    ' ''setUFValue(oForm, "IDH_CCOCUR", sCrCur) 'Carrier 
                    '' ''setUFValue(oForm, "IDH_LCOCUR", sCCur)  'License
                    '*/

                    'Multi Currency Pricing 
                    If bFromModalForm Then
                        'setDFValue(oForm, msRowTable, "U_CCPCd", sCCPCd)

                        setDFValue(oForm, msRowTable, IDH_JOBSHD._DispCgCc, sCCur)  'Disposal Charge from Customer Ccy 
                        setDFValue(oForm, msRowTable, IDH_JOBSHD._CarrCgCc, sCCur)  'Haulier Charge from Customer Ccy

                        setDFValue(oForm, msRowTable, IDH_JOBSHD._PurcCoCc, sPCur)  'Producer/Purchase
                        setDFValue(oForm, msRowTable, IDH_JOBSHD._DispCoCc, sDCur)  'Disposal 
                        setDFValue(oForm, msRowTable, IDH_JOBSHD._CarrCoCc, sCrCur) 'Carrier 
                        setDFValue(oForm, msRowTable, IDH_JOBSHD._LiscCoCc, sCCur)  'License
                    Else
                        setUFValue(oForm, "IDH_DCGCUR", sCCur)  'Disposal Charge from Customer Ccy 
                        setUFValue(oForm, "IDH_HCHCUR", sCCur)  'Haulier Charge from Customer Ccy

                        setUFValue(oForm, "IDH_PCOCUR", sPCur)  'Producer/Purchase
                        setUFValue(oForm, "IDH_DCOCUR", sDCur)  'Disposal 
                        setUFValue(oForm, "IDH_CCOCUR", sCrCur) 'Carrier 
                        setUFValue(oForm, "IDH_LCOCUR", sCCur)  'License
                    End If

                End If
            Catch ex As Exception
            Finally
                oRecordSet = Nothing
            End Try
        End Sub
        Private Sub doBPForecast(ByVal oForm As SAPbouiCOM.Form)
            '## MA Start 08-09-2015 Issue#913
            'Dim bEnable As Boolean = True
            Dim sWOCode As String = getDFValue(oForm, msRowTable, "U_WROrd", True)
            If sWOCode IsNot Nothing AndAlso sWOCode.Trim <> "" Then
                Dim sBPForeCastCode As String = com.idh.bridge.lookups.Config.INSTANCE.doLookupTableField(IDH_JOBENTR.TableName, IDH_JOBENTR._ForeCS, IDH_JOBENTR._Code + "='" + sWOCode + "'")
                If sBPForeCastCode IsNot Nothing AndAlso sBPForeCastCode.Trim <> "" _
                    AndAlso Config.ParameterAsBool("LKBFCWCD", False) AndAlso getDFValue(oForm, msRowTable, "U_WasCd") IsNot Nothing AndAlso getDFValue(oForm, msRowTable, "U_WasCd").ToString.Trim <> "" Then
                    setEnableItem(oForm, False, "IDH_WASCL1")
                    setEnableItem(oForm, False, "IDH_WASCF")
                    setEnableItem(oForm, False, "IDH_WASMAT")
                End If
                '## MA Start 08-09-2015 Issue#909
                If getDFValue(oForm, msRowTable, "U_AltWasDsc") Is Nothing OrElse getDFValue(oForm, msRowTable, "U_AltWasDsc").ToString.Trim = "" Then
                    If sBPForeCastCode IsNot Nothing AndAlso sBPForeCastCode.Trim <> "" AndAlso Config.ParameterAsBool("FLALTITM", False) = True Then
                        Dim oBPForecast As IDH_BPFORECST = New IDH_BPFORECST()
                        If oBPForecast.getByKey(sBPForeCastCode) Then
                            setDFValue(oForm, msRowTable, "U_AltWasDsc", oBPForecast.U_AltWDSCd)
                        End If
                    End If
                End If
                '## MA End 08-09-2015 Issue#909
            End If
            'If bEnable Then
            '    setEnableItem(oForm, True, "IDH_WASCL1")
            '    setEnableItem(oForm, True, "IDH_WASCF")
            '    setEnableItem(oForm, True, "IDH_WASMAT")
            'End If
            '## MA End 08-09-2015 Issue#913:
        End Sub
        Private Sub doAddAutoAdditionalCodes(ByVal oForm As SAPbouiCOM.Form)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)
            Dim oItem As com.idh.dbObjects.User.Item

            Dim oCodes As AutoAdditinalCodes
            oCodes = oDOR.getAutoAdditionalCodesCIPSIP()

            If oCodes IsNot Nothing Then
                Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                If oGrid IsNot Nothing Then

                    'Customer
                    If Not oCodes.moCustomerList Is Nothing AndAlso oCodes.moCustomerList.Count > 0 Then
                        For Each oItem In oCodes.moCustomerList
                            If Not oItem.msItemCode Is Nothing AndAlso oItem.msItemCode.Length > 0 Then
                                'Check if the item is allready in the Grid and only adds it if it does not
                                If oGrid.doIndexOfItem(oItem.msItemCode) < 0 Then
                                    'Dim sJobNr As String = getFormDFValue(oForm, "Code", True)
                                    'Dim sRowNr As String = getDFValue(oForm, msRowTable, "Code")

                                    Dim sCustCd As String = oDOR.U_CustCd
                                    Dim sCustNm As String = oDOR.U_CustNm
                                    Dim sCustCurr As String = Config.INSTANCE.getValueFromBP(sCustCd, "Currency")
                                    Dim sSuppCd As String = "" 'getDFValue(oForm, msRowTable, "U_CongCd")
                                    Dim sSuppNm As String = "" 'getDFValue(oForm, msRowTable, "U_SCngNm")
                                    Dim sSuppCurr As String = ""
                                    Dim sEmpId As String = ""
                                    Dim sEmpNm As String = ""
                                    Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(sCustCd, oItem.msItemCode)
                                    Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(sSuppCd, oItem.msItemCode)

                                    oGrid.doAddExpense(oDOR.U_JobNr, oDOR.Code,
                                        sCustCd, sCustNm, sCustCurr,
                                        sSuppCd, sSuppNm, sSuppCurr,
                                        sEmpId, sEmpNm,
                                        oItem.msItemCode, oItem.msItemName,
                                        oItem.msUOM, 1,
                                        oItem.mdItemCost, oItem.mdItemChrg,
                                        sCostVatGrp,
                                        sChrgVatGrp,
                                        True,
                                        True,
                                        True,
                                        False,
                                        oItem.msChrgCalc, "VARIABLE")
                                End If
                            End If
                        Next
                    End If

                    'Supplier
                    If Not oCodes.moSupplierList Is Nothing AndAlso oCodes.moSupplierList.Count > 0 Then
                        For Each oItem In oCodes.moSupplierList
                            If Not oItem.msItemCode Is Nothing AndAlso oItem.msItemCode.Length > 0 Then
                                'Check if the item is allready in the Grid and only adds it if it does not
                                If oGrid.doIndexOfItem(oItem.msItemCode) < 0 Then
                                    'Dim sJobNr As String = getFormDFValue(oForm, "Code", True)
                                    'Dim sRowNr As String = getDFValue(oForm, msRowTable, "Code")

                                    Dim sCustCd As String = oDOR.U_CustCd
                                    Dim sCustNm As String = oDOR.U_CustNm
                                    Dim sCustCurr As String = Config.INSTANCE.getValueFromBP(sCustCd, "Currency")
                                    Dim sSuppCd As String = oDOR.U_ProCd
                                    Dim sSuppNm As String = oDOR.U_ProNm
                                    Dim sSuppCurr As String = oDOR.U_PurcCoCc
                                    Dim sEmpId As String = ""
                                    Dim sEmpNm As String = ""
                                    Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(sCustCd, oItem.msItemCode)
                                    Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(sSuppCd, oItem.msItemCode)

                                    oGrid.doAddExpense(oDOR.U_JobNr, oDOR.Code,
                                        sCustCd, sCustNm, sCustCurr,
                                        sSuppCd, sSuppNm, sSuppCurr,
                                        sEmpId, sEmpNm,
                                        oItem.msItemCode, oItem.msItemName,
                                        oItem.msUOM, 1,
                                        oItem.mdItemCost, oItem.mdItemChrg,
                                        sCostVatGrp,
                                        sChrgVatGrp,
                                        True,
                                        True,
                                        True,
                                        False,
                                        "VARIABLE", oItem.msChrgCalc)

                                End If
                            End If
                        Next
                    End If
                End If
            End If
        End Sub

        'OnTime Ticket 25926 - DOC Report crahses application
        Public Function doCallHTTPRpts(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal oForm As SAPbouiCOM.Form, ByVal sReport As String, ByVal sDoShow As String, ByVal sDoPrint As String, ByVal sCopies As String, ByRef oParams As Hashtable, ByRef sFromForm As String, ByRef sBranch As String) As Boolean
            Dim sDoPrintDialog As String = Config.Parameter("MDSWPD")
            If sDoPrintDialog Is Nothing Then
                sDoPrintDialog = "TRUE"
            Else
                sDoPrintDialog = sDoPrintDialog.ToUpper()
            End If

            Dim sDoAutoPrint As String = Config.Parameter("AUTOPRNT")
            If sDoAutoPrint Is Nothing Then
                sDoAutoPrint = "TRUE"
            Else
                sDoAutoPrint = sDoAutoPrint.ToUpper()
            End If

            'Get the printer from the DB
            'Dim oPrinterSettings As WR1_Data.idh.data.PrintSettings = Nothing
            'oPrinterSettings = Config.INSTANCE.getPrinterSetting(oParent, sReport, sFromForm, sBranch)

            Dim oPrinterSettings As com.idh.bridge.PrintSettings = Nothing
            oPrinterSettings = PrintSettings.getPrinterSetting(sReport, sFromForm, sBranch)

            Dim sPrinterName As String = Nothing
            Dim iWidth As Integer = 0
            Dim iHeight As Integer = 0
            If Not oPrinterSettings Is Nothing Then
                sPrinterName = oPrinterSettings.msPrinterName
                iWidth = oPrinterSettings.miWidth
                iHeight = oPrinterSettings.miHeight
            End If
            Return doCallHTTPRptsContainer(oParent, oForm, sReport, sDoShow, sDoAutoPrint, sDoPrintDialog, sCopies, oParams, sPrinterName, iWidth, iHeight)

        End Function

        Public Function doCallHTTPRptsContainer(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal oForm As SAPbouiCOM.Form, ByVal sReport As String, ByVal sDoShow As String, ByVal sDoPrint As String, ByVal sDoDialog As String, ByVal sCopies As String, ByRef oParams As Hashtable, ByRef sPrinterName As String, ByRef iWidth As Integer, ByRef iHeight As Integer) As Boolean
            'Get The print option
            Dim bDoPrint As Boolean = False
            If (Not sDoPrint Is Nothing) AndAlso sDoPrint.ToUpper().Equals("TRUE") Then
                bDoPrint = True
            End If

            'Get The show the report
            Dim bDoShow As Boolean = True
            If (Not sDoShow Is Nothing) AndAlso sDoShow.ToUpper().Equals("FALSE") Then
                bDoShow = False
                bDoPrint = True
            End If

            'Get The show printdialog option
            Dim bDoDialog As Boolean = True
            If (Not sDoDialog Is Nothing) AndAlso sDoDialog.ToUpper().Equals("FALSE") Then
                bDoDialog = False
            End If

            'Get The number of copies
            Dim iCopies As Integer = 1
            If Not sCopies Is Nothing Then
                Try
                    iCopies = Val(sCopies)
                Catch ex As Exception
                End Try
            End If

            setSharedData(oForm, "DOPRINT", sDoPrint)
            setSharedData(oForm, "DOSHOWPRNDIALOG", sDoDialog)
            setSharedData(oForm, "DOSHOW", sDoShow)
            setSharedData(oForm, "NUMCOPIES", sCopies)
            setSharedData(oForm, "PARAMS", oParams)
            setSharedData(oForm, "PRINTERNAME", sPrinterName)

            setSharedData(oForm, "WIDTH", iWidth)
            setSharedData(oForm, "HEIGHT", iHeight)

            If sReport.StartsWith("http://") Then
                Dim sURL As String = sReport

                If oParams.Count > 0 Then
                    Dim en As IEnumerator = oParams.Keys.GetEnumerator()
                    Dim sKey As String
                    Dim sValue As String

                    While en.MoveNext
                        sKey = en.Current().ToString()
                        If Not sKey Is Nothing Then
                            sValue = oParams.Item(sKey)
                            sURL = sURL & "&" & sKey & "=" & sValue
                        End If
                    End While
                End If

                If bDoShow = False Then
                    'Dim oHTML As idh.report.HTMLViewer
                    'oHTML = idh.report.HTMLViewer.getInstance(oParent)
                    'oHTML.doPrintSilent(sURL, bDoDialog, iCopies)
                Else
                    setSharedData(oForm, "URL", sURL)
                    oParent.doOpenModalForm("IDHHTML", oForm)
                End If
            Else
                'Dim sReportChk As String = idh.report.CrystalViewer.getReportChk(oParent, sReport)
                Dim sReportChk As String = getReportChk(oParent, sReport)
                If Not sReportChk Is Nothing Then
                    setSharedData(oForm, "CHKREPORT", sReportChk)
                    setSharedData(oForm, "REPORT", sReport)
                    setSharedData(oForm, "ZOOM", "69")
                    setSharedData(oForm, "DSPGROUPTREE", "FALSE")
                    setSharedData(oForm, "DSPTOOLBAR", "TRUE")
                    oParent.doOpenModalForm("IDHREPORT", oForm)
                Else
                    Return False
                End If
            End If
            Return True

        End Function

        Public Function getReportChk(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sReportIn As String) As String
            Dim sReport As String = Nothing
            Dim sPath As String = Config.Parameter("REPDIR")
            If Not sPath Is Nothing AndAlso sPath.Trim().Length() > 0 Then
                If sPath.EndsWith("\") = False Then
                    sPath = sPath & "\"
                End If
            Else
                'sPath = Directory.GetCurrentDirectory & "\reports\"
                sPath = IDHAddOns.idh.addon.Base.REPORTPATH
            End If

            sReport = sReportIn.Trim()
            If sReport Is Nothing OrElse sReport.Length() = 0 Then
                Return Nothing
            End If
            sReport = sPath & sReport

            'If oParent.gsAdditionalSRFPath Is Nothing Then
            If IDHAddOns.idh.addon.Base.REPORTPATH Is Nothing Then
                If File.Exists(sReport) = False Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Config: The report file could not be found - " & sReportIn, "The report file could not be found - " & sReportIn)
                    com.idh.bridge.DataHandler.INSTANCE.doResConfigError("The report file could not be found - " & sReportIn, "ERCONREP", {sReportIn})
                    Return Nothing
                Else
                    Return sReport
                End If
            Else
                If File.Exists(sReport) = False Then
                    'sPath = oParent.gsAdditionalSRFPath & "\reports\"
                    sPath = IDHAddOns.idh.addon.Base.REPORTPATH & "\"
                    sReport = sPath & sReportIn.Trim()
                    If File.Exists(sReport) = False Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Config: The report file could not be found - " & sReportIn, "The report file could not be found - " & sReportIn)
                        com.idh.bridge.DataHandler.INSTANCE.doResConfigError("The report file could not be found - " & sReportIn, "ERCONREP", {sReportIn})
                        Return Nothing
                    Else
                        Return sReport
                    End If
                Else
                    Return sReport
                End If
            End If
        End Function

        Private Sub doHideLoadSheet(ByVal oForm As SAPbouiCOM.Form)
            doSetVisible(oForm, "IDH_LOADSH", False, 1000)
            doSetVisible(oForm, "349", False, 1000)
            doSetVisible(oForm, "IDH_LOADSL", False, 1000)
            setDFValue(oForm, msRowTable, "U_LoadSht", "")

            '## MA Start 13-10-2014
            'If System.Array.IndexOf(moAlwaysDisabledFields, "IDH_CUSWEI") = -1 Then
            '    setEnableItem(oForm, True, "IDH_CUSWEI")
            'End If
            '## MA Start 13-10-2014
            If (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso
               Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES)) OrElse
           (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) AndAlso
               Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Charge_PRICES)) Then
                setEnableItem(oForm, True, "IDH_UOM")
            End If
        End Sub

        Private Sub doShowLoadSheet(ByVal oForm As SAPbouiCOM.Form)
            doSetVisible(oForm, "IDH_LOADSH", True, 0)
            doSetVisible(oForm, "349", True, 0)
            doSetVisible(oForm, "IDH_LOADSL", True, 0)

            Dim sLoadSht As String = getDFValue(oForm, msRowTable, "U_LoadSht")
            If sLoadSht.Length > 0 Then
                setEnableItem(oForm, False, "IDH_CUSWEI")
                setEnableItem(oForm, False, "IDH_UOM")
            Else
                '## MA Start 13-10-2014
                If System.Array.IndexOf(moAlwaysDisabledFields, "IDH_CUSWEI") = -1 Then
                    setEnableItem(oForm, True, "IDH_CUSWEI")
                End If
                '## MA Start 13-10-2014

                If (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso
                   Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES)) OrElse
                   (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) AndAlso
                   Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Charge_PRICES)) Then
                    setEnableItem(oForm, True, "IDH_UOM")
                End If
            End If
        End Sub

#Region "OtherPriceQtyCalculations"
        Public Sub doCalculateProfit(ByVal oRow As IDH_DISPROW)
            Dim oForm As SAPbouiCOM.Form = oRow.SBOForm
            Dim dProfit = oRow.doCalculateProfit()

            Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
            'Dim dAddProf As Double
            If Not oGrid Is Nothing Then
                dProfit = dProfit + oGrid.doCalculateProfit()
            End If

            '            setUFValue(oForm, "IDH_PROFIT", dProfit)
        End Sub
#End Region

        Public Overrides Function doAddCustomerShipToAddress(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim oDO As IDH_DISPORD = thisDO(oForm)
            Return BPAddress.doAddCustomerShipToAddress(goParent.goDICompany, oDO)
        End Function

        Public Overrides Function doAddProducerShipToAddress(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim oDO As IDH_DISPORD = thisDO(oForm)
            Return BPAddress.doAddProducerShipToAddress(goParent.goDICompany, oDO)
        End Function

        Public Overrides Sub doValidateAndAddCustomerProducerAddress(ByVal oForm As SAPbouiCOM.Form)
            Dim bCustAddressValid As Boolean = True
            Dim bProdAddressValid As Boolean = True

            If doValidateCustomerAddress(oForm, False) = False Then
                bCustAddressValid = False
            End If

            If Not bCustAddressValid Then
                If doAddCustomerShipToAddress(oForm) = False Then
                    'Return False
                End If
            End If

            If doValidateProducerAddress(oForm, False) = False Then
                bProdAddressValid = False
            End If
            If Not bProdAddressValid Then
                If doAddProducerShipToAddress(oForm) = False Then
                    'Return False
                End If
            End If
        End Sub

#Region "Handlers"
        Public Sub doHandleChargeTotalsCalculated(ByVal oCaller As IDH_DISPROW)
            Dim oForm As SAPbouiCOM.Form = oCaller.SBOForm

            setUFValue(oForm, "U_BefDis", oCaller.SubBeforeDiscount)
            setUFValue(oForm, "U_SubTot", oCaller.SubAfterDiscount)

            doCalculateProfit(oCaller)
        End Sub

        Public Sub doHandleCostTotalsCalculated(ByVal oCaller As IDH_DISPROW)
            Dim oForm As SAPbouiCOM.Form = oCaller.SBOForm

            'setUFValue(oForm, "U_BefDis", oCaller.SubBeforeDiscount)
            'setUFValue(oForm, "U_SubTot", oCaller.SubAfterDiscount)

            doCalculateProfit(oCaller)
        End Sub

        Private Sub doHandleCheckReport(ByVal oCaller As IDH_DISPROW)
            'Now get the Check entry from the Payment record
            Dim sCheckEntry As String = Config.INSTANCE.doLookupTableField("OCHO", "CheckKey", "PmntNum = '" + oCaller.U_ProPay + "'")
            Dim sCheckReport As String = Config.Parameter("CHCKREP")
            If sCheckReport IsNot Nothing AndAlso sCheckEntry.Length > 0 Then
                Dim sShowReport As String = "TRUE" 'Config.ParamaterWithDefault("MDSWDR", "TRUE")
                Dim oParams As New Hashtable

                oParams.Add("RowNum", sCheckEntry)
                IDHAddOns.idh.report.Base.doCallReportDefaults(oCaller.SBOForm, sCheckReport, sShowReport, "FALSE", "1", oParams, gsType)
            End If
        End Sub

        ''IDH_CUSCHG  @IDH_JOBSHD U_TCharge 680,230,50 Pane 100-101
        ''IDH_CUSCHR @IDH_JOBSHD U_CusChr 680,245,50 
        ''
        ''IDH_PRCOST @IDH_JOBSHD U_PCost 680,405,50
        ''IDH_TIPCOS @IDH_JOBSHD U_TipCost 680,420,50
        ''IDH_ORDCOS @IDH_JOBSHD U_OrdCost 680,435,50
        ''
        ''# uBC_CUSCHG  @IDH_JOBSHD U_TCharge 680,230,50
        ''# uBC_CUSCHR @IDH_JOBSHD U_CusChr 680,245,50 
        ''
        ''# uBC_PRCOST @IDH_JOBSHD U_PCost 680,405,50
        ''# uBC_TIPCOS @IDH_JOBSHD U_TipCost 680,420,50
        ''# uBC_ORDCOS @IDH_JOBSHD U_OrdCost 680,435,50 

        Public Sub doHandleChargeTipPriceLookupSkipped(ByVal oCaller As IDH_DISPROW, ByVal oData As Object)
            Dim iBackColor As Integer

            If oData = False Then
                iBackColor = miPriceLookupSkipBackCol
            Else
                iBackColor = DefaultBackColor(oCaller.SBOForm)
            End If

            Dim oItem As SAPbouiCOM.Item

            oItem = oCaller.SBOForm.Items.Item("IDH_CUSWEI")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If

            oItem = oCaller.SBOForm.Items.Item("IDH_UOM")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If

            oItem = oCaller.SBOForm.Items.Item("IDH_CUSCHG")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If
        End Sub

        Public Sub doHandleChargeHaulagePriceLookupSkipped(ByVal oCaller As IDH_DISPROW, ByVal oData As Object)
            Dim iBackColor As Integer

            If oData = False Then
                iBackColor = miPriceLookupSkipBackCol
            Else
                iBackColor = DefaultBackColor(oCaller.SBOForm)
            End If

            Dim oItem As SAPbouiCOM.Item

            'oItem = oCaller.SBOForm.Items.Item("IDH_CUSCHR")
            'If oItem.Enabled Then
            '    oItem.BackColor = iBackColor
            'End If
        End Sub

        Public Sub doHandleCostTipPriceLookupSkipped(ByVal oCaller As IDH_DISPROW, ByVal oData As Object)
            Dim iBackColor As Integer

            If oData = False Then
                iBackColor = miPriceLookupSkipBackCol
            Else
                iBackColor = DefaultBackColor(oCaller.SBOForm)
            End If

            Dim oItem As SAPbouiCOM.Item

            oItem = oCaller.SBOForm.Items.Item("IDH_TIPWEI")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If

            oItem = oCaller.SBOForm.Items.Item("IDH_PUOM")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If

            oItem = oCaller.SBOForm.Items.Item("IDH_TIPCOS")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If
        End Sub

        Public Sub doHandleCostHaulagePriceLookupSkipped(ByVal oCaller As IDH_DISPROW, ByVal oData As Object)
            Dim iBackColor As Integer

            If oData = False Then
                iBackColor = miPriceLookupSkipBackCol
            Else
                iBackColor = DefaultBackColor(oCaller.SBOForm)
            End If

            Dim oItem As SAPbouiCOM.Item

            oItem = oCaller.SBOForm.Items.Item("IDH_ORDCOS")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If
        End Sub

        Public Sub doHandleCostProducerPriceLookupSkipped(ByVal oCaller As IDH_DISPROW, ByVal oData As Object)
            Dim iBackColor As Integer

            If oData = False Then
                iBackColor = miPriceLookupSkipBackCol
            Else
                iBackColor = DefaultBackColor(oCaller.SBOForm)
            End If

            Dim oItem As SAPbouiCOM.Item

            oItem = oCaller.SBOForm.Items.Item("IDH_PURWGT")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If

            oItem = oCaller.SBOForm.Items.Item("IDH_PURUOM")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If

            oItem = oCaller.SBOForm.Items.Item("IDH_PRCOST")
            If oItem.Enabled OrElse oData = True Then
                oItem.BackColor = iBackColor
            End If
        End Sub

        'Public Sub doResetLookupSkippedCol(ByVal oForm As SAPbouiCOM.Form)
        '    Dim oItem As SAPbouiCOM.Item

        '    oItem = oForm.Items.Item("IDH_CUSWEI")
        '    If oItem.Enabled Then
        '        oItem.BackColor = DefaultBackColor(oForm)
        '    End If

        '    oItem = oForm.Items.Item("IDH_UOM")
        '    If oItem.Enabled Then
        '        oItem.BackColor = DefaultBackColor(oForm)
        '    End If

        '    oItem = oForm.Items.Item("IDH_CUSCHG")
        '    If oItem.Enabled Then
        '        oItem.BackColor = DefaultBackColor(oForm)
        '    End If

        '    'oItem = oForm.Items.Item("IDH_CUSWEI")
        '    'If oItem.Enabled Then
        '    '    oItem.BackColor = DefaultBackColor(oForm)
        '    'End If

        '    oItem = oForm.Items.Item("IDH_TIPWEI")
        '    If oItem.Enabled Then
        '        oItem.BackColor = DefaultBackColor(oForm)
        '    End If

        '    oItem = oForm.Items.Item("IDH_PUOM")
        '    If oItem.Enabled Then
        '        oItem.BackColor = DefaultBackColor(oForm)
        '    End If

        '    oItem = oForm.Items.Item("IDH_TIPCOS")
        '    If oItem.Enabled Then
        '        oItem.BackColor = DefaultBackColor(oForm)
        '    End If

        '    oItem = oForm.Items.Item("IDH_ORDCOS")
        '    If oItem.Enabled Then
        '        oItem.BackColor = DefaultBackColor(oForm)
        '    End If

        '    oItem = oForm.Items.Item("IDH_PURWGT")
        '    If oItem.Enabled Then
        '        oItem.BackColor = DefaultBackColor(oForm)
        '    End If

        '    oItem = oForm.Items.Item("IDH_PURUOM")
        '    If oItem.Enabled Then
        '        oItem.BackColor = DefaultBackColor(oForm)
        '    End If

        '    oItem = oForm.Items.Item("IDH_PRCOST")
        '    If oItem.Enabled Then
        '        oItem.BackColor = DefaultBackColor(oForm)
        '    End If
        'End Sub

        Public Sub doHandleChargePricesChanged(ByVal oCaller As IDH_DISPROW)

            Dim oForm As SAPbouiCOM.Form = oCaller.SBOForm
            Dim iCurrentPane As Int16 = oForm.PaneLevel
            Try
                oForm.Freeze(True)
                oForm.PaneLevel = 10
                If oCaller.CIP.Prices Is Nothing OrElse oCaller.CIP.Prices.Count < 2 Then
                    oForm.Items.Item("uBC_CUSCHG").Visible = False
                    'If oForm.Items.Item("IDH_CUSCHG").FromPane >= oForm.PaneLevel AndAlso oForm.Items.Item("IDH_CUSCHG").ToPane <= oForm.PaneLevel Then
                    oForm.Items.Item("IDH_CUSCHG").Visible = True
                    'Else
                    'oForm.Items.Item("IDH_CUSCHG").Visible = True
                    'oForm.Items.Item("IDH_CUSCHG").Visible = False
                    'End If
                Else
                    Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("uBC_CUSCHG")
                    FillCombos.TippingPriceCombo(oItem.Specific, oCaller.CIP.Prices)

                    oItem.Visible = True
                    oForm.Items.Item("IDH_CUSCHG").Visible = False
                End If
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", New String() {"doHandleChargePricesChanged"}) 'new string[] { "Validating Enquiry Items before copying To WOQ." })
            Finally
                oForm.PaneLevel = iCurrentPane
                oForm.Freeze(False)
            End Try
        End Sub

        Public Sub doHandleCostTippingPricesChanged(ByVal oCaller As IDH_DISPROW)
            Dim oForm As SAPbouiCOM.Form = oCaller.SBOForm
            Dim iCurrentPane As Int16 = oForm.PaneLevel
            Try
                oForm.Freeze(True)
                oForm.PaneLevel = 10
                If oCaller.SIPT.Prices Is Nothing OrElse oCaller.SIPT.Prices.Count < 2 Then
                    oForm.Items.Item("uBC_TIPCOS").Visible = False
                    oForm.Items.Item("IDH_TIPCOS").Visible = True
                    'oForm.Items.Item("IDH_TIPCOS").Visible = True
                Else
                    Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("uBC_TIPCOS")
                    FillCombos.TippingPriceCombo(oItem.Specific, oCaller.SIPT.Prices)

                    oItem.Visible = True
                    oForm.Items.Item("IDH_TIPCOS").Visible = False
                End If

            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", New String() {"doHandleCostTippingPricesChanged"}) 'new string[] { "Validating Enquiry Items before copying To WOQ." })
            Finally
                oForm.PaneLevel = iCurrentPane
                oForm.Freeze(False)
            End Try

        End Sub

        Public Sub doHandleCostHaulagePricesChanged(ByVal oCaller As IDH_DISPROW)
            Dim oForm As SAPbouiCOM.Form = oCaller.SBOForm
            Dim iCurrentPane As Int16 = oForm.PaneLevel
            Try
                oForm.Freeze(True)
                oForm.PaneLevel = 10
                If oCaller.SIPH.Prices Is Nothing OrElse oCaller.SIPH.Prices.Count < 2 Then
                    oForm.Items.Item("uBC_ORDCOS").Visible = False
                    oForm.Items.Item("IDH_ORDCOS").Visible = True
                    'oForm.Items.Item("IDH_ORDCOS").Visible = True
                Else
                    Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("uBC_ORDCOS")
                    FillCombos.HaulagePriceCombo(oItem.Specific, oCaller.SIPH.Prices)

                    oItem.Visible = True
                    oForm.Items.Item("IDH_ORDCOS").Visible = False
                End If
                ''MA Start 08-12-2015 Issue#1007
                If (oCaller.U_JobTp IsNot Nothing AndAlso (Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), oCaller.U_JobTp))) Then
                    setEnableItem(oForm, False, "IDH_ORDQTY")
                Else
                    If oCaller.CCHCos = "QTY1" OrElse oCaller.CCHCos = "WEIGHT" Then
                        setEnableItem(oForm, False, "IDH_ORDQTY")
                    Else 'If oCaller.CCHCos = "standard" Then
                        setEnableItem(oForm, True, "IDH_ORDQTY")
                    End If
                End If
                ''MA End 08-12-2015 Issue#1007

            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", New String() {"doHandleCostHaulagePricesChanged"}) 'new string[] { "Validating Enquiry Items before copying To WOQ." })
            Finally
                oForm.PaneLevel = iCurrentPane
                oForm.Freeze(False)
            End Try
        End Sub

        Public Sub doHandleCostProducerPricesChanged(ByVal oCaller As IDH_DISPROW)
            Dim oForm As SAPbouiCOM.Form = oCaller.SBOForm
            Dim iCurrentPane As Int16 = oForm.PaneLevel
            Try
                oForm.Freeze(True)
                oForm.PaneLevel = 10
                If oCaller.SIPP.Prices Is Nothing OrElse oCaller.SIPP.Prices.Count < 2 Then
                    oForm.Items.Item("uBC_PRCOST").Visible = False
                    oForm.Items.Item("IDH_PRCOST").Visible = True
                Else
                    Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("uBC_PRCOST")
                    FillCombos.TippingPriceCombo(oItem.Specific, oCaller.SIPP.Prices)

                    oItem.Visible = True
                    oForm.Items.Item("IDH_PRCOST").Visible = False
                End If

            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", New String() {"doHandleCostProducerPricesChanged"}) 'new string[] { "Validating Enquiry Items before copying To WOQ." })
            Finally
                oForm.PaneLevel = iCurrentPane
                oForm.Freeze(False)
            End Try
        End Sub

        Public Function doHandlerYNOption(ByVal oCaller As IDH_DISPROW, ByVal sMessageId As String, ByVal sParams As String(), ByVal iDefButton As Integer) As Integer
            Return Messages.INSTANCE.doResourceMessageYN(sMessageId, sParams, iDefButton)
        End Function
#End Region

#Region "Buffer Zone"
        Private Sub doCallLorrySearchEB(ByVal oForm As SAPbouiCOM.Form)
            'If Config.ParameterAsBool("VMUSENEW", False) = False Then
            Dim sReg As String = getDFValue(oForm, msRowTable, "U_Lorry", True)
            If sReg.Trim = "" Then
                sReg = getItemValue(oForm, "IDH_VEHREG")
            End If
            Dim bDoSetReg As Boolean = False
            If sReg.StartsWith("!") Then
                setSharedData(oForm, "SHOWONSITE", "True")
                sReg = sReg.Substring(1)
                bDoSetReg = True
            Else
                setSharedData(oForm, "SHOWACTIVITY", "False")
                setSharedData(oForm, "SHOWONSITE", "False")
            End If
            Dim sRegChanged As String = com.idh.bridge.utils.General.doFixReg(sReg)
            If sRegChanged.Equals(sReg) = False OrElse bDoSetReg = True Then
                setDFValue(oForm, msRowTable, "U_Lorry", sRegChanged, True)
            End If
            If Config.INSTANCE.getParameterAsBool("DOVMSUC", True) Then
                Dim sCustomer As String
                sCustomer = getFormDFValue(oForm, IDH_DISPORD._CardCd, True)
                setSharedData(oForm, "IDH_VEHCUS", sCustomer)
            Else
                setSharedData(oForm, "IDH_VEHCUS", "")
            End If

            Dim sJobTp As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, True)
            setSharedData(oForm, "JOBTYPE", sJobTp)

            getDFValue(oForm, msRowTable, "U_Lorry", True)

            ''MA Start 19-01-2014 Issue#142 Skype chat Hannah 19-01-2014
            setSharedData(oForm, "IDH_VEHTP", Config.INSTANCE.getParameterWithDefault("DFLSRTYE", ""))
            ''MA End 19-01-2014 Issue#142 Skype chat Hannah 19-01-2014

            setSharedData(oForm, "IDH_VEHREG", sRegChanged)
            setSharedData(oForm, "SHOWCREATE", "True")
            'setSharedData(oForm, "IDH_WR1OT", "^Do")
            setSharedData(oForm, "IDH_WR1OT", "")
            setSharedData(oForm, "SILENT", "SHOWMULTI")
            goParent.doOpenModalForm("IDHLOSCH", oForm)
            'Else
            'doCreateLorrySearch(oForm)
            'End If
        End Sub

        Private Sub doCallLorrySearchCFL(ByVal oForm As SAPbouiCOM.Form)
            'If Config.ParameterAsBool("VMUSENEW", False) = False Then
            Dim sReg As String '= getDFValue(oForm, msRowTable, "U_Lorry", True)
            sReg = getDFValue(oForm, msRowTable, "U_Lorry", True)
            If sReg.Trim = "" Then
                sReg = getItemValue(oForm, "IDH_VEHREG")
            End If
            Dim sRegChanged As String = com.idh.bridge.utils.General.doFixReg(sReg)

            If sRegChanged.Equals(sReg) = False Then
                setDFValue(oForm, msRowTable, "U_Lorry", sRegChanged, True)
            End If
            If Config.INSTANCE.getParameterAsBool("DOVMSUC", True) Then
                Dim sCustomer As String
                sCustomer = getFormDFValue(oForm, IDH_DISPORD._CardCd, True)
                setSharedData(oForm, "IDH_VEHCUS", sCustomer)
            Else
                setSharedData(oForm, "IDH_VEHCUS", "")
            End If
            setSharedData(oForm, "IDH_VEHREG", sRegChanged)
            ''MA Start 19-01-2014 Issue#142 Skype chat Hannah 19-01-2014
            setSharedData(oForm, "IDH_VEHTP", Config.INSTANCE.getParameterWithDefault("DFLSRTYE", ""))
            ''MA End 19-01-2014 Issue#142 Skype chat Hannah 19-01-2014
            setSharedData(oForm, "SHOWCREATE", "True")
            'setSharedData(oForm, "IDH_WR1OT", "^Do")
            setSharedData(oForm, "IDH_WR1OT", "")
            setSharedData(oForm, "SHOWONSITE", "False")
            setSharedData(oForm, "SHOWACTIVITY", "False")
            goParent.doOpenModalForm("IDHLOSCH", oForm)
            'Else
            'doCreateLorrySearch(oForm)
            'End If
        End Sub

        Private Sub doCallLorrySearchOnSite(ByVal oForm As SAPbouiCOM.Form)
            'If Config.ParameterAsBool("VMUSENEW", False) = False Then
            Dim sReg As String '= getDFValue(oForm, msRowTable, "U_Lorry", True)
            sReg = getDFValue(oForm, msRowTable, "U_Lorry", True)
            Dim sRegChanged As String = com.idh.bridge.utils.General.doFixReg(sReg)
            setSharedData(oForm, "JOBTYPE", "")
            If sRegChanged.Equals(sReg) = False Then
                setDFValue(oForm, msRowTable, "U_Lorry", sRegChanged, True)
            End If
            If Config.INSTANCE.getParameterAsBool("DOVMSUC", True) Then
                Dim sCustomer As String
                sCustomer = getFormDFValue(oForm, IDH_DISPORD._CardCd, True)
                setSharedData(oForm, "IDH_VEHCUS", sCustomer)
            Else
                setSharedData(oForm, "IDH_VEHCUS", "")
            End If
            setSharedData(oForm, "IDH_VEHREG", sRegChanged)
            ''MA Start 19-01-2014 Issue#142 Skype chat Hannah 19-01-2014
            setSharedData(oForm, "IDH_VEHTP", Config.INSTANCE.getParameterWithDefault("DFLSRTYE", ""))
            ''MA End 19-01-2014 Issue#142 Skype chat Hannah 19-01-2014
            setSharedData(oForm, "SHOWCREATE", "True")
            'setSharedData(oForm, "IDH_WR1OT", "^Do")
            setSharedData(oForm, "IDH_WR1OT", "")
            setSharedData(oForm, "SHOWONSITE", "True")
            goParent.doOpenModalForm("IDHLOSCH", oForm)
            'Else
            'doCreateLorrySearch(oForm)
            'End If
        End Sub

        Private Sub doHandleVehicleSearchResult(ByVal oForm As SAPbouiCOM.Form, ByVal sLastButton As String)
            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

            Dim sCustomerCode As String
            'Dim sPayType As String
            'Dim sCarrierCd As String

            sCustomerCode = getSharedData(oForm, "CUSCD")

            '                    sPayType = getSharedData(oForm, "MARKAC")
            '					If Not sPayType Is Nothing AndAlso ( sPayType.Equals("DOORD") OrElse sPayType.Equals("REQORD") ) Then
            If Not sCustomerCode Is Nothing AndAlso sCustomerCode.Length > 0 Then
                '		                    If Config.INSTANCE.getParameterAsBool( "DOVMUC", True ) OrElse _

                '		                    	( sCustomer = sCarrier ) Then
                Dim dChargeTotal As Double = getUnCommittedRowTotals(oForm, Nothing, 0)

                'If com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) Then
                Dim bBlockOnFail As Boolean = Config.INSTANCE.getParameterAsBool("DOBCCL", True)
                If Config.INSTANCE.doCheckCredit(sCustomerCode, dChargeTotal, False, bBlockOnFail) = False Then
                    Exit Sub
                End If
            End If

            Dim sVType As String = getSharedData(oForm, "VTYPE")
            If sVType Is Nothing OrElse sVType.Length = 0 Then
                If Config.Parameter("DFVEHTYP") = "" Then
                    sVType = "S"
                ElseIf Config.Parameter("DFVEHTYP").Length > 0 Then
                    sVType = Config.Parameter("DFVEHTYP")
                End If
            End If

            ''MA 15-01-2015
            Dim sV_WR1ORD As String = getSharedData(oForm, "V_WR1ORD")
            If sV_WR1ORD Is Nothing OrElse sV_WR1ORD = "^WO" OrElse sV_WR1ORD = "WO" Then
                sV_WR1ORD = ""
            ElseIf sV_WR1ORD = "^Do" Then
                sV_WR1ORD = "Do"
            ElseIf sV_WR1ORD.Equals("") Then
                sV_WR1ORD = "Do"
            End If
            ''MA 15-01-2015

            If sLastButton.Equals("IDH_CREATE") Then
                setWFValue(oForm, "CreateNewVechicle", True)
                Try
                    Dim sReg As String
                    Dim sJob As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, True)

                    sReg = getSharedData(oForm, "REG")
                    If sReg Is Nothing OrElse sReg.Length = 0 Then
                        sReg = getDFValue(oForm, msRowTable, "U_Lorry", True)
                    End If

                    sReg = com.idh.bridge.utils.General.doFixReg(sReg)

                    setDFValue(oForm, msRowTable, "U_LorryCd", "", False, True)
                    If sJob.Length > 0 Then
                        setFormDFValue(oForm, "Code", "")
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                        doLoadDataLocal(oForm, False)

                        doSetVehicleReg(oForm, sReg, "", sJob, False, False)
                        doSetEnabled(oForm.Items.Item("IDH_TARWEI"), True)
                        doSetEnabled(oForm.Items.Item("IDH_TRLTar"), True)
                        ''MA Start 05-02-2015 Issue#554
                        doSetEnabled(oForm.Items.Item("IDH_TRWTE1"), True)
                        doSetEnabled(oForm.Items.Item("IDH_TRWTE2"), True)
                        ''MA End 05-02-2015 Issue#554
                    End If
                    doSetFocus(oForm, "IDH_VEHREG")
                    setWFValue(oForm, "LastVehAct", "CREATE")
                    'setWFValue(oForm, "VehicleType", sVType)
                    'SV 19/03/2015 #572,586
                    oDOR.VehicleType = sVType
                    oDOR.VehicleDesc = getSharedData(oForm, "VEHDIS")
                    setDFValue(oForm, IDH_DISPROW.TableName, IDH_DISPROW._VehTyp, getSharedData(oForm, "VEHDIS"))
                    oDOR.CarrierCd = getSharedData(oForm, "WCCD")
                    setDFValue(oForm, IDH_DISPORD.TableName, IDH_DISPORD._CCardCd, getSharedData(oForm, "WCCD"))

                    If Not Config.ParameterAsBool("VMUSENEW", False) Then
                        sCustomerCode = getSharedData(oForm, "IDH_VEHCUS")

                        'setDFValue(oForm, IDH_DISPROW.TableName, IDH_DISPROW._VehTyp, sVType)
                        setDFValue(oForm, IDH_DISPROW.TableName, IDH_DISPROW._Driver, getSharedData(oForm, "DRIVER"))
                        setDFValue(oForm, IDH_DISPROW.TableName, IDH_DISPROW._Branch, getSharedData(oForm, "IDH_BRANCH"))

                        'setFormDFValue(oForm, IDH_DISPORD._CardCd, sCustomerCode)
                        'setDFValue(oForm, IDH_DISPROW.TableName, IDH_DISPROW._CustCd, sCustomerCode)
                        oDOR.U_CarrCd = sCustomerCode
                    Else

                    End If
                    'End
                    ''MA 15-01-2015
                    'oDOR.VehicleWR1ORD = sV_WR1ORD
                    ''MA 15-01-2015

                    Dim sTstWasteCode As String = getDFValue(oForm, msRowTable, "U_WasCd")
                    Dim sJobType As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp)
                    If Config.INSTANCE.doCheckCanPurchaseWB(sTstWasteCode) _
                         AndAlso Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), sJobType) Then
                        doSwitchToPO(oForm, True)
                    Else
                        doSwitchToSO(oForm)
                    End If
                Catch ex As Exception
                End Try
                setWFValue(oForm, "CreateNewVechicle", False)
            Else
                Dim sJob As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, True)
                If sJob Is Nothing OrElse sJob.Length = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("The Job Type must be selected.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("The Job Type must be selected.", "ERUSGENS", {com.idh.bridge.Translation.getTranslatedWord("The Job Type")})
                    Exit Sub
                End If

                Dim iMode As SAPbouiCOM.BoFormMode = oForm.Mode
                Dim sReg As String = getSharedData(oForm, "REG")
                Dim sVehCode As String = getSharedData(oForm, "VEHCODE")
                Dim sJobNr As String = Nothing
                Dim sJobRow As String = Nothing
                Dim sDoShowOnSite As String = getSharedData(oForm, "SHOWONSITE")

                '** The onsite search **'
                If sDoShowOnSite = "True" Then
                    sJobNr = getSharedData(oForm, "DONR")
                    If sJobNr.Length = 0 Then

                        sJobNr = Nothing
                    End If

                    sJobRow = getSharedData(oForm, "DOROW")
                    If sJobRow.Length = 0 Then
                        sJobRow = Nothing
                    End If
                End If

                If Not sJobNr Is Nothing AndAlso Not sJobRow Is Nothing Then
                    doLoad2ndWeigh(oForm, sJobNr, sJobRow)
                Else
                    doSetVehicleReg(oForm, sReg, sVehCode, sJob, True, True)
                End If

                If iMode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    doSetUpdate(oForm)
                End If
                setWFValue(oForm, "LastVehAct", "Select")
            End If

            'com.idh.bridge.utils.Combobox2.doSelectFirstRow(FillCombos.getCombo(oForm, "uBC_TRNCd"))
        End Sub
#End Region

#Region "New Lorry Search"
        'Private Sub doCreateLorrySearch(ByVal oForm As SAPbouiCOM.Form)
        '    Dim oOOForm As com.uBC.forms.fr3.VehicleM = New com.uBC.forms.fr3.VehicleM(Nothing, oForm.UniqueID, Nothing)
        '    oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doLorrySelection)
        '    oOOForm.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doLorryCancel)

        '    oOOForm.doShowModal(oForm)
        'End Sub

        'Public Function doLorrySelection(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
        '    Dim oForm As SAPbouiCOM.Form = oOForm.SBOParentForm
        '    Dim oOOForm As com.uBC.forms.fr3.VehicleM = oOForm
        '    Dim oVehMast As IDH_VEHMAS2 = oOOForm.VehicleMaster

        '    'setDFValue(oForm, msRowTable, IDH_DISPROW._Lorry, oVehMast.U_VehReg, True)

        '    Dim sJob As String = getDFValue(oForm, msRowTable, IDH_DISPROW._JobTp, True)
        '    If sJob Is Nothing OrElse sJob.Length = 0 Then
        '        com.idh.bridge.DataHandler.INSTANCE.doError("The Job Type must be selected.")
        '        Exit Function
        '    End If

        '    Dim iMode As SAPbouiCOM.BoFormMode = oForm.Mode
        '    Dim sJobNr As String = Nothing
        '    Dim sJobRow As String = Nothing
        '    Dim sDoShowOnSite As String = getSharedData(oForm, "SHOWONSITE")

        '    '** The onsite search **'
        '    If sDoShowOnSite = "True" Then
        '        sJobNr = getSharedData(oForm, "DONR")
        '        If sJobNr.Length = 0 Then

        '            sJobNr = Nothing
        '        End If

        '        sJobRow = getSharedData(oForm, "DOROW")
        '        If sJobRow.Length = 0 Then
        '            sJobRow = Nothing
        '        End If
        '    End If

        '    If Not sJobNr Is Nothing AndAlso Not sJobRow Is Nothing Then
        '        doLoad2ndWeigh(oForm, sJobNr, sJobRow)
        '    Else
        '        'doSetVehicleReg(oForm, oVehMast.U_VehReg, sVehCode, sJob, True, True)
        '        doSetVehicleReg(oForm, oVehMast.U_VehReg, oVehMast.Code, sJob, False, True)
        '    End If

        '    If iMode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
        '        doSetUpdate(oForm)
        '    End If
        '    setWFValue(oForm, "LastVehAct", "Select")
        '    Return True
        'End Function

        'Public Function doLorryCancel(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
        '    Return True
        'End Function
#End Region

#Region "BP Additional Charges"
        Public Sub getBPAdditionalCharges(ByVal oForm As SAPbouiCOM.Form)
            If Config.INSTANCE.getParameterAsBool("USEBPACH", False) Then
                Dim oDOR As IDH_DISPROW = thisDOR(oForm)
                'Dim oWOR As IDH_JOBSHD = thisWOR(oForm)
                Dim sCustCd As String = oDOR.U_CustCd
                Dim sCustNm As String = oDOR.U_CustNm
                Dim oBPAC As IDH_BPADCHG
                Dim oList As ArrayList
                oBPAC = New IDH_BPADCHG()
                oList = oBPAC.getBPAdditionalCharges(sCustCd, oDOR.U_JobTp)

                If Not oList Is Nothing AndAlso oList.Count > 0 Then
                    Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                    If oGrid IsNot Nothing Then
                        '20151123: New Logic covering both Mitie and ECW and any other client 
                        'have 2 configs, each for EMPTY and MATCH 
                        'Discuss with HG and HN
                        'Start - New Logic 
                        For Each oBPAC In oList
                            If oBPAC.U_JobTp.Length = 0 Then
                                'check flag 
                                If Config.INSTANCE.getParameterAsBool("BPACDFJT", True) Then
                                    'Check if the item is allready in the Grid and only adds it if it does not
                                    If oGrid.doIndexOfItem(oBPAC.U_ItemCode) < 0 Then
                                        Dim sCustCurr As String = Config.INSTANCE.getValueFromBP(sCustCd, "Currency")

                                        Dim sSalesUOM As String '= oBPAC.U_UOM.ToString()
                                        If oBPAC.U_UOM.Equals(String.Empty) Then
                                            sSalesUOM = Config.INSTANCE.getParameterWithDefault("BPACDUOM", "ea")
                                        Else
                                            sSalesUOM = oBPAC.U_UOM
                                        End If

                                        Dim sSuppCd As String = "" 'getDFValue(oForm, msRowTable, "U_CongCd")
                                        Dim sSuppNm As String = "" 'getDFValue(oForm, msRowTable, "U_SCngNm")
                                        Dim sSuppCurr As String = ""
                                        Dim sEmpId As String = oBPAC.U_EmpId
                                        Dim sEmpNm As String = oBPAC.U_EmpNm 'Config.LookupTableField("OSLP", "SlpName", "SlpCode = '" + oBPAC.U_SlpCode.ToString() + "'").ToString()
                                        Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(sCustCd, oBPAC.U_ItemCode)
                                        Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(sSuppCd, oBPAC.U_ItemCode)
                                        Dim dCost As Double = 0

                                        oGrid.doAddExpense(oDOR.U_JobNr, oDOR.Code, _
                                            sCustCd, sCustNm, sCustCurr, _
                                            sSuppCd, sSuppNm, sSuppCurr, _
                                            sEmpId, sEmpNm, _
                                            oBPAC.U_ItemCode, oBPAC.U_ItemName, _
                                            sSalesUOM, 1, _
                                            dCost, 0, _
                                            sCostVatGrp, _
                                            sChrgVatGrp, _
                                            False, _
                                            False, _
                                            False, _
                                            True,
                                            "VARIABLE", "VARIABLE")
                                    End If
                                End If
                            Else
                                'check flag 
                                If Config.INSTANCE.getParameterAsBool("BPACMTJT", True) Then
                                    'Check if the item is allready in the Grid and only adds it if it does not
                                    If oGrid.doIndexOfItem(oBPAC.U_ItemCode) < 0 Then
                                        Dim sCustCurr As String = Config.INSTANCE.getValueFromBP(sCustCd, "Currency")

                                        Dim sSalesUOM As String '= oBPAC.U_UOM.ToString()
                                        If oBPAC.U_UOM.Equals(String.Empty) Then
                                            sSalesUOM = Config.INSTANCE.getParameterWithDefault("BPACDUOM", "ea")
                                        Else
                                            sSalesUOM = oBPAC.U_UOM
                                        End If

                                        Dim sSuppCd As String = "" 'getDFValue(oForm, msRowTable, "U_CongCd")
                                        Dim sSuppNm As String = "" 'getDFValue(oForm, msRowTable, "U_SCngNm")
                                        Dim sSuppCurr As String = ""
                                        Dim sEmpId As String = oBPAC.U_EmpId
                                        Dim sEmpNm As String = oBPAC.U_EmpNm 'Config.LookupTableField("OSLP", "SlpName", "SlpCode = '" + oBPAC.U_SlpCode.ToString() + "'").ToString()
                                        Dim sChrgVatGrp As String = Config.INSTANCE.doGetSalesVatGroup(sCustCd, oBPAC.U_ItemCode)
                                        Dim sCostVatGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(sSuppCd, oBPAC.U_ItemCode)
                                        Dim dCost As Double = 0

                                        oGrid.doAddExpense(oDOR.U_JobNr, oDOR.Code, _
                                            sCustCd, sCustNm, sCustCurr, _
                                            sSuppCd, sSuppNm, sSuppCurr, _
                                            sEmpId, sEmpNm, _
                                            oBPAC.U_ItemCode, oBPAC.U_ItemName, _
                                            sSalesUOM, 1, _
                                            dCost, 0, _
                                            sCostVatGrp, _
                                            sChrgVatGrp, _
                                            False, _
                                            False, _
                                            False, _
                                            True,
                                            "VARIABLE", "VARIABLE")
                                    End If
                                End If
                            End If
                        Next
                        'End - new Logic 
                        If oList.Count > 0 Then
                            doWarnMess("BPs Additional charges added. Please review Additional Tab and update Ordered Quantity.", SAPbouiCOM.BoMessageTime.bmt_Short)
                        End If
                    End If
                End If
                oBPAC = Nothing
                oList = Nothing
                oDOR = Nothing
            End If
        End Sub

        Public Sub removeAutoAddBPAddCharges(ByVal oForm As SAPbouiCOM.Form)
            If Config.INSTANCE.getParameterAsBool("USEBPACH", False) Then
                Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                If oGrid IsNot Nothing Then
                    If oGrid.getRowCount() > 1 Then
                        ''20151118
                        ''Commented block as it was not deleting old rows for BP Additional Charges, 
                        ''added automatically by selection of a Job Type 
                        'Dim oDOR As IDH_DISPROW = thisDOR(oForm)
                        'Dim sCustCd As String = oDOR.U_CustCd
                        ''Dim sJobType As String = oDOR.U_JobTp 'getFormDFValue(oForm, "U_JobTp")
                        'Dim oBPAC As IDH_BPADCHG
                        'Dim oList As ArrayList
                        'oBPAC = New IDH_BPADCHG()
                        'oList = oBPAC.getBPAdditionalCharges(sCustCd, oDOR.U_JobTp)

                        'If Not oList Is Nothing AndAlso oList.Count > 0 Then
                        '    For Each oBPAC In oList
                        '        If Not oBPAC.U_ItemCode Is Nothing AndAlso oBPAC.U_ItemCode.Length > 0 Then
                        '            'Remove Row
                        '            Dim iSearchIndx As Integer
                        '            iSearchIndx = oGrid.doIndexOfItem(oBPAC.U_ItemCode)
                        '            If iSearchIndx > -1 Then
                        '                oGrid.doRemoveRow(iSearchIndx)
                        '            End If
                        '        End If
                        '    Next
                        'End If
                        'oBPAC = Nothing
                        'oList = Nothing
                        'oDOR = Nothing

                        Dim iIndex As Integer = 0
                        Do
                            If oGrid.doGetFieldValue(IDH_WOADDEXP._BPAutoAdd, iIndex).Equals("Y") Then
                                oGrid.doRemoveRow(iIndex)
                            Else
                                iIndex = iIndex + 1
                            End If
                        Loop While iIndex < oGrid.getRowCount
                    End If
                    oGrid = Nothing
                End If
            End If
        End Sub

        Public Sub removeAutoAddBPAddCharges(ByVal oForm As SAPbouiCOM.Form, ByVal bChangedBP As Boolean)
            If Config.INSTANCE.getParameterAsBool("USEBPACH", False) Then
                Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                If oGrid IsNot Nothing Then
                    Dim iCnt As Integer = oGrid.getRowCount()
                    If iCnt > 1 AndAlso bChangedBP Then
                        For index As Integer = 1 To iCnt - 1
                            oGrid.doRemoveRow(0)
                        Next
                    End If
                    oGrid = Nothing
                End If
            End If
        End Sub

        Public Sub doUpdateAutoAddBPAddCharges(ByVal oForm As SAPbouiCOM.Form, ByVal sDateE As String)
            If Config.INSTANCE.getParameterAsBool("USEBPACH", False) Then
                Try
                    Dim oGrid As WR1_Grids.idh.controls.grid.AdditionalExpenses = WR1_Grids.idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                    If oGrid IsNot Nothing Then
                        If Config.INSTANCE.getParameterAsBool("USEBPACH", False) AndAlso oGrid.getRowCount() > 1 Then
                            Dim oDOR As IDH_DISPROW = thisDOR(oForm)

                            If Not sDateE Is Nothing AndAlso sDateE.Length > 0 Then
                                Dim sBPCode As String = oDOR.F_CustomerCode
                                Dim sJobType As String = oDOR.U_JobTp
                                Dim dTCharge As Double = oDOR.U_TCharge

                                Dim oBPAC As IDH_BPADCHG
                                Dim oList As ArrayList
                                oBPAC = New IDH_BPADCHG()
                                oList = oBPAC.getBPAdditionalCharges(sBPCode, sJobType)

                                If Not oList Is Nothing AndAlso oList.Count > 0 Then
                                    For Each oBPAC In oList
                                        If Not oBPAC.U_ItemCode Is Nothing AndAlso oBPAC.U_ItemCode.Length > 0 Then
                                            Try
                                                Dim iSearchIndx As Integer
                                                iSearchIndx = oGrid.doIndexOfItem(oBPAC.U_ItemCode)
                                                If iSearchIndx > -1 AndAlso oGrid.doGetFieldValue("U_BPAutoAdd", iSearchIndx) = "Y" Then
                                                    oGrid.doSetFieldValue("U_ItmChrg", iSearchIndx, oBPAC.doCalculateAdditionalCharge(oBPAC.U_FrmParse, thisDOR(oForm)))
                                                End If
                                            Catch ex As Exception
                                                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "doUpdateAutoAddBPAddCharges: Error evaluating the additional charges expression!")
                                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXACE", Nothing)
                                            End Try
                                        End If

                                    Next
                                End If
                                oBPAC = Nothing
                                oList = Nothing
                            ElseIf sDateE Is Nothing OrElse sDateE.Equals(String.Empty) Then
                                'TBD: Clear AutoAdded Rows ??
                                'removeAutoAddBPAddCharges(oForm)
                            End If
                            oDOR = Nothing
                        End If
                        oGrid = Nothing
                    End If
                Catch ex As Exception
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "doUpdateAutoAddBPAddCharges: Error updating additional item row charges.")
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXUAIR", Nothing)
                End Try
            End If
        End Sub

        'Public Function doGenerateLabTask(ByVal oForm As SAPbouiCOM.Form) As String
        '    Try
        '        Dim oLabTask As IDH_LABTASK = New IDH_LABTASK()
        '        Dim oNxtNo As NumbersPair = oLabTask.getNewKey()
        '        oLabTask.Code = oNxtNo.CodeCode
        '        oLabTask.Name = oNxtNo.NameCode

        '        oLabTask.U_LabItemRCd = getDFValue(oForm, msRowTable, IDH_DISPROW._WasCd)
        '        oLabTask.U_LabICd = getDFValue(oForm, msRowTable, IDH_DISPROW._WasCd)
        '        oLabTask.U_LabINm = getDFValue(oForm, msRowTable, IDH_DISPROW._WasDsc)

        '        oLabTask.U_BPCode = getFormDFValue(oForm, IDH_DISPORD._CardCd)
        '        oLabTask.U_BPName = getFormDFValue(oForm, IDH_DISPORD._CardNM)

        '        oLabTask.U_RowStatus = "Requested"
        '        oLabTask.U_DtReceived = DateTime.Now

        '        oLabTask.U_AssignedTo = ""
        '        oLabTask.U_AnalysisReq = ""
        '        oLabTask.U_ExtStatus = ""
        '        oLabTask.U_ExtComments = ""
        '        oLabTask.U_ImpResults = ""
        '        oLabTask.U_Decision = "Pending"
        '        oLabTask.U_ObjectCd = getDFValue(oForm, msRowTable, IDH_DISPROW._Code)
        '        oLabTask.U_ObjectType = "DO"
        '        oLabTask.doAddDataRow()

        '        Return oLabTask.Code

        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doError("doCreateLabTask()", "Error adding Lab Task row: " + ex.Message)
        '        Return "-1"
        '    End Try

        'End Function

#End Region


    End Class
End Namespace
