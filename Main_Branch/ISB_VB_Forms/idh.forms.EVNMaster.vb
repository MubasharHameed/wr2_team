﻿Imports System.IO
Imports System.Collections
Imports System
Imports IDHAddOns.idh.controls
Imports com.idh.bridge.data
Imports com.idh.bridge
Imports com.idh.dbObjects.User
Imports com.idh.bridge.lookups
Imports com.idh.controls
Imports com.idh.dbObjects.numbers
Imports SAPbouiCOM
Imports System.Windows.Forms
Imports System.Data
Imports System.Data.SqlClient

Imports com.bridge
Imports com.idh.bridge.utils
Imports com.idh.bridge.resources
Imports com.idh.dbObjects.strct
Imports com.uBC.utils
Imports com.idh.dbObjects

Namespace idh.forms
    Public Class EVNMaster
        Inherits IDHAddOns.idh.forms.Base
        Private moDBObject As IDH_EVNMASTER
        Dim sHeadTable As String = "@IDH_EVNMASTER"
        Dim sDetailTable As String = "@IDH_EVNDETAIL"

#Region "Initialization"

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParentMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHEVNMAS", sParentMenu, iMenuPosition, "EVN Master.srf", False, True, False, "Entsorgungsnachweis", load_Types.idh_LOAD_NORMAL)

            sHeadTable = "@IDH_EVNMASTER"
            sDetailTable = "@IDH_EVNDETAIL"

            doEnableHistory(sHeadTable)

        End Sub

        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)

        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            oForm.Freeze(True)

            Try
                oForm.DataSources.DBDataSources.Add(sHeadTable)

                'Mapp fields with data fields 
                doAddFormDF(oForm, "IDH_CODE", IDH_EVNMASTER._Code)
                doAddFormDF(oForm, "IDH_EVN", IDH_EVNMASTER._EVNNumber)
                doAddFormDF(oForm, "IDH_WASCD", IDH_EVNMASTER._WasCd)
                doAddFormDF(oForm, "IDH_WASNM", IDH_EVNMASTER._WasDsc)
                doAddFormDF(oForm, "IDH_EWC", IDH_EVNMASTER._EWCCd)
                doAddFormDF(oForm, "IDH_DSPCD", IDH_EVNMASTER._DispSiteCd)
                doAddFormDF(oForm, "IDH_DSPNM", IDH_EVNMASTER._DispSiteNm)
                doAddFormDF(oForm, "IDH_DSPAD", IDH_EVNMASTER._DSAddress)
                doAddFormDF(oForm, "IDH_DSPST", IDH_EVNMASTER._DSStreet)
                doAddFormDF(oForm, "IDH_DSPBL", IDH_EVNMASTER._DSBlock)
                doAddFormDF(oForm, "IDH_DSPCT", IDH_EVNMASTER._DSCity)
                doAddFormDF(oForm, "IDH_DSPPC", IDH_EVNMASTER._DSPostCd)

                '##MA Start 14-02-2017

                'Disposal
                doAddFormDF(oForm, "IDH_DSPSAD", IDH_EVNMASTER._DSPSAddr)
                doAddFormDF(oForm, "IDH_DSPSST", IDH_EVNMASTER._DSPSStrt)
                doAddFormDF(oForm, "IDH_DSPSBL", IDH_EVNMASTER._DSPSBlock)
                doAddFormDF(oForm, "IDH_DSPSCT", IDH_EVNMASTER._DSPSCity)
                doAddFormDF(oForm, "IDH_DSPSPC", IDH_EVNMASTER._DSPSPostCd)
                doAddFormDF(oForm, "IDH_DSPSSI", IDH_EVNMASTER._DSPSSID)
                doAddFormDF(oForm, "IDH_Fre", IDH_EVNMASTER._DSPFreistellu)

                'Carrier
                doAddFormDF(oForm, "IDH_CARCD", IDH_EVNMASTER._CarCd)
                doAddFormDF(oForm, "IDH_CARNM", IDH_EVNMASTER._CarNm)
                doAddFormDF(oForm, "IDH_CARAD", IDH_EVNMASTER._CAAddr)
                doAddFormDF(oForm, "IDH_CARST", IDH_EVNMASTER._CAStrt)
                doAddFormDF(oForm, "IDH_CARBL", IDH_EVNMASTER._CABlock)
                doAddFormDF(oForm, "IDH_CARCT", IDH_EVNMASTER._CACity)
                doAddFormDF(oForm, "IDH_CARPC", IDH_EVNMASTER._CAPostCd)
                doAddFormDF(oForm, "IDH_CARSAD", IDH_EVNMASTER._CASAddr)
                doAddFormDF(oForm, "IDH_CARSST", IDH_EVNMASTER._CASStrt)
                doAddFormDF(oForm, "IDH_CARSBL", IDH_EVNMASTER._CASBlock)
                doAddFormDF(oForm, "IDH_CARSCT", IDH_EVNMASTER._CASCity)
                doAddFormDF(oForm, "IDH_CARSPC", IDH_EVNMASTER._CASPostCd)
                doAddFormDF(oForm, "IDH_CARID", IDH_EVNMASTER._CAID)
                doAddFormDF(oForm, "IDH_Ja", IDH_EVNMASTER._RelNum)
                doAddFormDF(oForm, "IDH_Nein", IDH_EVNMASTER._RelNum)

                'Customer
                doAddFormDF(oForm, "IDH_CUSCD", IDH_EVNMASTER._CusCd)
                doAddFormDF(oForm, "IDH_CUSNM", IDH_EVNMASTER._CusNm)
                doAddFormDF(oForm, "IDH_CUSAD", IDH_EVNMASTER._CusAddr)
                doAddFormDF(oForm, "IDH_CUSST", IDH_EVNMASTER._CusStreet)
                doAddFormDF(oForm, "IDH_CUSBL", IDH_EVNMASTER._CusBlock)
                doAddFormDF(oForm, "IDH_CUSCT", IDH_EVNMASTER._CusCity)
                doAddFormDF(oForm, "IDH_CUSPC", IDH_EVNMASTER._CusPostCd)
                doAddFormDF(oForm, "IDH_CUSSAD", IDH_EVNMASTER._CusSAddr)
                doAddFormDF(oForm, "IDH_CUSSST", IDH_EVNMASTER._CusSStrt)
                doAddFormDF(oForm, "IDH_CUSSBL", IDH_EVNMASTER._CusSBlock)
                doAddFormDF(oForm, "IDH_CUSSCT", IDH_EVNMASTER._CusSCity)
                doAddFormDF(oForm, "IDH_CUSSPC", IDH_EVNMASTER._CusSPostCd)
                doAddFormDF(oForm, "IDH_CUSID", IDH_EVNMASTER._CusID)
                '##MA End 14-02-2017

                '##MA Start 21-02-2017

                'General Tab Fields
                doAddFormDF(oForm, "IDH_EN", IDH_EVNMASTER._EnVnSnVs)
                doAddFormDF(oForm, "IDH_VN", IDH_EVNMASTER._EnVnSnVs)
                doAddFormDF(oForm, "IDH_SN", IDH_EVNMASTER._EnVnSnVs)
                doAddFormDF(oForm, "IDH_VS", IDH_EVNMASTER._EnVnSnVs)
                doAddFormDF(oForm, "IDH_RECYCL", "U_Recycle")
                doAddFormDF(oForm, "IDH_DSPOSL", "U_Disposal")
                doAddFormDF(oForm, "IDH_OFICL", "U_Official")
                doAddFormDF(oForm, "IDH_UNOFCL", "U_UnOfficial")
                doAddFormDF(oForm, "IDH_VOLWTD", "U_VolunWithdraw")

                Dim chkRecycle As SAPbouiCOM.CheckBox
                chkRecycle = oForm.Items.Item("IDH_RECYCL").Specific
                chkRecycle.ValOff = "N"
                chkRecycle.ValOn = "Y"

                Dim chkDisposal As SAPbouiCOM.CheckBox
                chkDisposal = oForm.Items.Item("IDH_DSPOSL").Specific
                chkDisposal.ValOff = "N"
                chkDisposal.ValOn = "Y"

                Dim chkOfficial As SAPbouiCOM.CheckBox
                chkOfficial = oForm.Items.Item("IDH_OFICL").Specific
                chkOfficial.ValOff = "N"
                chkOfficial.ValOn = "Y"

                Dim chkUnOfficial As SAPbouiCOM.CheckBox
                chkUnOfficial = oForm.Items.Item("IDH_UNOFCL").Specific
                chkUnOfficial.ValOff = "N"
                chkUnOfficial.ValOn = "Y"

                Dim chkVolWithdraw As SAPbouiCOM.CheckBox
                chkVolWithdraw = oForm.Items.Item("IDH_VOLWTD").Specific
                chkVolWithdraw.ValOff = "N"
                chkVolWithdraw.ValOn = "Y"

                'Material Tab Fields
                doAddFormDF(oForm, "IDH_MatJa1", IDH_EVNMASTER._PreTre)
                doAddFormDF(oForm, "IDH_MatNe1", IDH_EVNMASTER._PreTre)
                doAddFormDF(oForm, "IDH_MatJa2", IDH_EVNMASTER._DecAtt)
                doAddFormDF(oForm, "IDH_MatNe2", IDH_EVNMASTER._DecAtt)

                doAddFormDF(oForm, "IDH_FIXED", "U_Fixed")
                doAddFormDF(oForm, "IDH_STCHFX", "U_StitchFixed")
                doAddFormDF(oForm, "IDH_PASTY", "U_Pasty")
                doAddFormDF(oForm, "IDH_ATOMSD", "U_Atomised")
                doAddFormDF(oForm, "IDH_LIQUID", "U_Liquid")
                doAddFormDF(oForm, "IDH_ODOR", "U_Odor")
                doAddFormDF(oForm, "IDH_COLOR", "U_Color")

                Dim chkFixed As SAPbouiCOM.CheckBox
                chkFixed = oForm.Items.Item("IDH_FIXED").Specific
                chkFixed.ValOff = "N"
                chkFixed.ValOn = "Y"

                Dim chkStitchFixed As SAPbouiCOM.CheckBox
                chkStitchFixed = oForm.Items.Item("IDH_STCHFX").Specific
                chkStitchFixed.ValOff = "N"
                chkStitchFixed.ValOn = "Y"

                Dim chkPasty As SAPbouiCOM.CheckBox
                chkPasty = oForm.Items.Item("IDH_PASTY").Specific
                chkPasty.ValOff = "N"
                chkPasty.ValOn = "Y"

                Dim chkAtomised As SAPbouiCOM.CheckBox
                chkAtomised = oForm.Items.Item("IDH_ATOMSD").Specific
                chkAtomised.ValOff = "N"
                chkAtomised.ValOn = "Y"

                Dim chkLiquid As SAPbouiCOM.CheckBox
                chkLiquid = oForm.Items.Item("IDH_LIQUID").Specific
                chkLiquid.ValOff = "N"
                chkLiquid.ValOn = "Y"

                'Other Fields
                doAddFormDF(oForm, "IDH_Sing", IDH_EVNMASTER._SinMul)
                doAddFormDF(oForm, "IDH_Mul", IDH_EVNMASTER._SinMul)
                doAddFormDF(oForm, "IDH_ENDDT", IDH_EVNMASTER._EndDt)
                doAddFormDF(oForm, "IDH_ORDERS", "U_Orders")

                '##MA End 21-02-2017

                doAddFormDF(oForm, "IDH_REMIND", IDH_EVNMASTER._Activity)
                doAddFormDF(oForm, "IDH_CRDT", IDH_EVNMASTER._CreateDt)
                doAddFormDF(oForm, "IDH_STDT", IDH_EVNMASTER._StartDt)
                doAddFormDF(oForm, "IDH_DUR", IDH_EVNMASTER._Duration)
                doAddFormDF(oForm, "IDH_UOM", IDH_EVNMASTER._UOM)
                doAddFormDF(oForm, "IDH_QTY", IDH_EVNMASTER._Quantity)
                doAddFormDF(oForm, "IDH_YR1", IDH_EVNMASTER._Year1)
                doAddFormDF(oForm, "IDH_YR2", IDH_EVNMASTER._Year2)
                doAddFormDF(oForm, "IDH_YR3", IDH_EVNMASTER._Year3)
                doAddFormDF(oForm, "IDH_YR4", IDH_EVNMASTER._Year4)
                doAddFormDF(oForm, "IDH_YR5", IDH_EVNMASTER._Year5)
                doAddFormDF(oForm, "IDH_ISPRNT", IDH_EVNMASTER._IsPrinted)

                doAddWF(oForm, "CallFromSupplier", False)
                doAddWF(oForm, "CallFromCarrier", False)
                doAddWF(oForm, "CallFromDisposalShipAddress", False)
                doAddWF(oForm, "CallFromDisposalBillAddress", False)
                doAddWF(oForm, "CallFromCarrierShipAddress", False)
                doAddWF(oForm, "CallFromCarrierBillAddress", False)
                doAddWF(oForm, "CallFromCustomerShipAddress", False)
                doAddWF(oForm, "CallFromCustomerBillAddress", False)

                '##MA Start 14-02-2017
                Dim optBtn As SAPbouiCOM.OptionBtn = oForm.Items.Item("IDH_Nein").Specific
                optBtn.GroupWith("IDH_Ja")
                '##MA End 14-02-2017

                '##MA Start 21-02-2017
                Dim optBtnTabMat1 As SAPbouiCOM.OptionBtn = oForm.Items.Item("IDH_MatJa1").Specific
                optBtnTabMat1.GroupWith("IDH_MatNe1")

                Dim optBtnTabMat2 As SAPbouiCOM.OptionBtn = oForm.Items.Item("IDH_MatJa2").Specific
                optBtnTabMat2.GroupWith("IDH_MatNe2")

                Dim optBtnSingMul As SAPbouiCOM.OptionBtn = oForm.Items.Item("IDH_Sing").Specific
                optBtnSingMul.GroupWith("IDH_Mul")

                Dim optBtnEN As SAPbouiCOM.OptionBtn = oForm.Items.Item("IDH_EN").Specific
                Dim optBtnVN As SAPbouiCOM.OptionBtn = oForm.Items.Item("IDH_VN").Specific
                Dim optBtnSN As SAPbouiCOM.OptionBtn = oForm.Items.Item("IDH_SN").Specific
                Dim optBtnVS As SAPbouiCOM.OptionBtn = oForm.Items.Item("IDH_VS").Specific
                optBtnVN.GroupWith("IDH_EN")
                optBtnSN.GroupWith("IDH_VN")
                optBtnVS.GroupWith("IDH_SN")

                'Setting Form Tabs not to affect form mode 
                oForm.Items.Item("IDH_TABATT").AffectsFormMode = False 'Attachments 
                oForm.Items.Item("IDH_TABMAT").AffectsFormMode = False 'Material
                oForm.Items.Item("IDH_TABGEN").AffectsFormMode = False 'General

                Dim oGAttach As UpdateGrid = New UpdateGrid(Me, oForm, "ATTACGRD", 4, 408, 800, 180)

                oGAttach.getSBOItem().AffectsFormMode = False
                oGAttach.getSBOItem().FromPane = 3
                oGAttach.getSBOItem().ToPane = 3

                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                If oGridN Is Nothing Then
                    oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
                End If
                oGridN.getSBOItem().AffectsFormMode = True


                '##MA End 21-02-2017

                'Form level settings 
                oForm.DataBrowser.BrowseBy = "IDH_CODE"

                oForm.SupportedModes = -1
                oForm.AutoManaged = False

                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE

            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
                BubbleEvent = False
            End Try


            oForm.Freeze(False)
        End Sub

#End Region

#Region "Loading Data"

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            Dim sEVNCode As String = getFormDFValue(oForm, IDH_EVNMASTER._Code).ToString()

            If Not sEVNCode Is Nothing AndAlso sEVNCode.Length > 0 Then
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE

                Dim oCons As New SAPbouiCOM.Conditions
                Dim oCon As SAPbouiCOM.Condition

                oCon = oCons.Add
                oCon.Alias = IDH_EVNMASTER._Code
                oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                oCon.CondVal = sEVNCode
                oForm.DataSources.DBDataSources.Item(sHeadTable).Query(oCons)
            Else
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                doCreateNewEntry(oForm)
                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(IDH_EVNMASTER.AUTONUMPREFIX, "EVNMAS")
                setFormDFValue(oForm, IDH_EVNMASTER._Code, oNumbers.CodeCode)
                setFormDFValue(oForm, IDH_EVNMASTER._Name, oNumbers.NameCode)
            End If

            '##MA Start 22-02-2017
            Try
                'Free Text Grid

                Dim oGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                Dim sCode As String = getFormDFValue(oForm, IDH_EVNMASTER._Code)
                oGrid.setRequiredFilter("" & IDH_EVNDETAIL._HDRCode & " = '" & sCode & "'")
                oGrid.doReloadData()
                If getFormDFValue(oForm, IDH_EVNMASTER._EnVnSnVs) = "3" Then
                    oForm.Freeze(True)
                    oForm.Items.Item("LINESGRID").Visible = True
                    Dim grdFreeTextFixedValues As String() = {
                                                              "D", "E", "F", "G",
                                                              "A", "B", "C",
                                                              "L", "M",
                                                              "N", "H", "I", "K", "P",
                                                              "S", "R"
                                                             }
                    If (oGrid.getSBOGrid().Rows.Count <= 17) AndAlso (oGrid.getSBOGrid().Rows.Count > 0) Then 'If there are already some rows added and we need to generate rest of the rows - total rows 16
                        For index = oGrid.getSBOGrid.Rows.Count To 16
                            oGrid.doAddRow(True, False, False)
                        Next

                        For index = 1 To 16
                            oGrid.doSetFieldValue(IDH_EVNDETAIL._BULAND, index - 1, grdFreeTextFixedValues.ElementAt(index - 1))
                        Next
                    End If
                    oForm.Freeze(False)
                Else
                    oForm.Items.Item("LINESGRID").Visible = False
            End If

                'Attachment Grid
                Dim sAttachmentCode As String = getFormDFValue(oForm, IDH_EVNMASTER._AtcEntry).ToString()
                Dim oGAttach As UpdateGrid = UpdateGrid.getInstance(oForm, "ATTACGRD")
                If sAttachmentCode.Length > 0 Then
                    oGAttach.setRequiredFilter(" AbsEntry = " + sAttachmentCode + " ")
                Else
                    oGAttach.setRequiredFilter(" AbsEntry = -1")
            End If
                oGAttach.doReloadData()
                doWORGrid(oForm)
                EnableAllEditItems(oForm, {"IDH_REMIND", "IDH_ISPRNT", "IDH_UCY", "IDH_CYB", "IDH_BR"})
                doSetFocus(oForm, "IDH_WASCD")
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "ERECAGD", {com.idh.bridge.Translation.getTranslatedWord("Attachments")})
            Finally
            End Try
            '##MA End 22-02-2017

            oForm.Items.Item("IDH_TABGEN").Click()
            oForm.PaneLevel = 1
            oForm.Freeze(False)
        End Sub

        Protected Overridable Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)

            MyBase.doBeforeLoadData(oForm)

            If ghOldDialogParams.Contains(oForm.UniqueID) OrElse getHasSharedData(oForm) = True Then
                Dim oData As ArrayList = ghOldDialogParams.Item(oForm.UniqueID)
                Dim sEVNCode As String = getParentSharedData(oForm, "EVNCODE")
                Dim sEVNNum As String = getParentSharedData(oForm, "EVNNUM")

                If Not (oData Is Nothing) Then
                    With getFormMainDataSource(oForm)

                        doCreateFindEntry(oForm)

                        Dim oCons As New SAPbouiCOM.Conditions
                        Dim oCon As SAPbouiCOM.Condition
                        oCon = oCons.Add
                        oCon.Alias = IDH_EVNMASTER._Code
                        oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                        oCon.CondVal = sEVNCode
                        setFormDFValue(oForm, IDH_EVNMASTER._Code, sEVNCode)
                        getFormMainDataSource(oForm).Query(oCons)
                    End With
                ElseIf sEVNCode IsNot Nothing Then
                    setFormDFValue(oForm, IDH_EVNMASTER._Code, sEVNCode)
                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                    doCreateFindEntry(oForm)
                ElseIf sEVNNum IsNot Nothing Then
                    With getFormMainDataSource(oForm)

                        doCreateFindEntry(oForm)

                        Dim oCons As New SAPbouiCOM.Conditions
                        Dim oCon As SAPbouiCOM.Condition
                        oCon = oCons.Add
                        oCon.Alias = IDH_EVNMASTER._EVNNumber
                        oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                        oCon.CondVal = sEVNNum
                        setFormDFValue(oForm, IDH_EVNMASTER._EVNNumber, sEVNNum)
                        getFormMainDataSource(oForm).Query(oCons)
                    End With
                End If
                doClearParentSharedData(oForm)
            Else
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            End If
            doWORGrid(oForm)

            '##MA Start 20-02-2017
            doEVNATTACHGrid(oForm)
            Dim oGAttach As UpdateGrid = UpdateGrid.getInstance(oForm, "ATTACGRD")
            If oGAttach Is Nothing Then
                oGAttach = New UpdateGrid(Me, oForm, "CONGRID")
            End If

            oGAttach.doSetSBOAutoReSize(False)
            oGAttach.doAddGridTable(New GridTable("ATC1", Nothing, "AbsEntry", True, True), True)
            oGAttach.setOrderValue("AbsEntry")
            oGAttach.setRequiredFilter(getListConsoleRequiredStr())
            oGAttach.doSetDoCount(False)
            oGAttach.doSetBlankLine(False)
            doSetListConsoleFields(oGAttach)

            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
            End If
            doSetFilterFields(oGridN)
            doSetListFields(oGridN)

            oGridN.doAddGridTable(New GridTable(sDetailTable, Nothing, IDH_EVNDETAIL._Code, True, True), True)
            oGridN.doSetDoCount(True)
            oGridN.doSetHistory(sDetailTable, IDH_EVNDETAIL._Code)
            '##MA End 20-02-2017

            doFillCombo(oForm, "IDH_UOM", "OWGT", "UnitDisply", "UnitName")
            doFillDuration(oForm)
            doFillWORsCombobox(oForm)

            oForm.Freeze(False)
        End Sub

#End Region

#Region "Unloading Data"

        Public Overrides Sub doClose()
        End Sub

#End Region

#Region "Event Handlers"

        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                If pVal.MenuUID = Config.NAV_ADD Then
                    oForm.Freeze(True)
                    doCreateNewEntry(oForm)
                    doLoadData(oForm)
                    doSetEnabled(oForm.Items.Item("IDH_CFLWCD"), True)
                    doSetEnabled(oForm.Items.Item("IDH_CFLDST"), True)
                    doSetEnabled(oForm.Items.Item("IDH_CFLCAR"), True)
                    doSetEnabled(oForm.Items.Item("IDH_CFLCUS"), True)
                    doSetEnabled(oForm.Items.Item("IDH_CFLADR"), True)
                    doSetEnabled(oForm.Items.Item("IDH_CFLDPS"), True)
                    doSetEnabled(oForm.Items.Item("IDH_CFLCAB"), True)
                    doSetEnabled(oForm.Items.Item("IDH_CFLCAS"), True)
                    doSetEnabled(oForm.Items.Item("IDH_CFLCUB"), True)
                    doSetEnabled(oForm.Items.Item("IDH_CFLCuS"), True)
                    doSetFocus(oForm, "IDH_EVN")
                    oForm.Freeze(False)
                ElseIf pVal.MenuUID = Config.NAV_FIND OrElse
                       pVal.MenuUID = Config.NAV_FIRST OrElse
                       pVal.MenuUID = Config.NAV_LAST OrElse
                       pVal.MenuUID = Config.NAV_NEXT OrElse
                       pVal.MenuUID = Config.NAV_PREV Then
                    oForm.Freeze(True)
                    Try
                        If pVal.MenuUID = Config.NAV_FIND Then
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                            doCreateFindEntry(oForm)
                            doSetFocus(oForm, "IDH_EVN")
                            DisableAllEditItems(oForm, {"IDH_EVN"})
                        Else
                            Dim oExclude() As String = {"IDH_EVN", "IDH_REMIND", "IDH_ISPRNT", "IDH_UCY", "IDH_CYB", "IDH_BR"}
                            doLoadData(oForm)
                            doWORGrid(oForm)
                            EnableAllEditItems(oForm, oExclude)
                            Dim oWORGrid As SAPbouiCOM.Grid
                            oWORGrid = oForm.Items.Item("WORGRID").Specific
                            If Not oWORGrid.DataTable.IsEmpty Then
                                oForm.Items.Item("IDH_EWC").Click(BoCellClickType.ct_Regular)
                                doDisableFieldsForWORCreated(oForm)
                                'Else
                                'EnableAllEditItems(oForm, {"IDH_REMIND", "IDH_ISPRNT", "IDH_UCY", "IDH_CYB", "IDH_BR"})
                                'doSetFocus(oForm, "IDH_WASCD")
                            Else
                                doSetEnabled(oForm.Items.Item("IDH_CFLWCD"), True)
                                doSetEnabled(oForm.Items.Item("IDH_CFLDST"), True)
                                doSetEnabled(oForm.Items.Item("IDH_CFLCAR"), True)
                                doSetEnabled(oForm.Items.Item("IDH_CFLCUS"), True)
                                doSetEnabled(oForm.Items.Item("IDH_CFLADR"), True)
                                doSetEnabled(oForm.Items.Item("IDH_CFLDPS"), True)
                                doSetEnabled(oForm.Items.Item("IDH_CFLCAB"), True)
                                doSetEnabled(oForm.Items.Item("IDH_CFLCAS"), True)
                                doSetEnabled(oForm.Items.Item("IDH_CFLCUB"), True)
                                doSetEnabled(oForm.Items.Item("IDH_CFLCuS"), True)
                            End If
                            oForm.Freeze(True)
                            Dim duration As String = getFormDFValue(oForm, IDH_EVNMASTER._Duration)
                            If Not duration = "" Then
                                For index = Convert.ToInt32(duration) + 1 To 5
                                    oForm.Items.Item("IDH_YR" & index.ToString()).Enabled = False
                                Next
                            End If
                            oForm.Freeze(False)
                        End If
                    Catch ex As Exception
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try
                ElseIf pVal.MenuUID.Equals("NEW") Then
                    Dim errorMessage As String = ""
                    If String.IsNullOrWhiteSpace(getFormDFValue(oForm, IDH_EVNMASTER._CusCd)) Then
                        errorMessage = " Customer Code"
                    End If
                    If String.IsNullOrWhiteSpace(getFormDFValue(oForm, IDH_EVNMASTER._DispSiteCd)) Then
                        errorMessage = errorMessage & " ,Disposal Code"
                    End If
                    If String.IsNullOrWhiteSpace(getFormDFValue(oForm, IDH_EVNMASTER._CarCd)) Then
                        errorMessage = errorMessage & " ,Carrier Code"
                    End If
                    If String.IsNullOrWhiteSpace(getFormDFValue(oForm, IDH_EVNMASTER._WasCd)) Then
                        errorMessage = errorMessage & " ,Waste Code "
                    End If
                    If String.IsNullOrWhiteSpace(getFormDFValue(oForm, IDH_EVNMASTER._EVNNumber)) Then
                        errorMessage = errorMessage & " ,EVN Number "
                    End If

                    If errorMessage.Length = 0 Then
                        setSharedData(oForm, "CusCode", getFormDFValue(oForm, IDH_EVNMASTER._CusCd))
                        setSharedData(oForm, "SupCode", getFormDFValue(oForm, IDH_EVNMASTER._DispSiteCd))
                        setSharedData(oForm, "CarCode", getFormDFValue(oForm, IDH_EVNMASTER._CarCd))
                        setSharedData(oForm, "WasCode", getFormDFValue(oForm, IDH_EVNMASTER._WasCd))
                        setSharedData(oForm, "EVNNumber", getFormDFValue(oForm, IDH_EVNMASTER._EVNNumber))
                        setSharedData(oForm, "CalledFromEVN", True)
                        goParent.doOpenModalForm("IDH_WASTORD", oForm)
                    ElseIf errorMessage.Length > 0 Then
                        com.idh.bridge.DataHandler.INSTANCE.doError("Please Select " & errorMessage & " Before Creating WOH ")
                    End If

                    '##MA End 10-02-2017
                End If
            End If
            Return True
        End Function

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If pVal.BeforeAction = True Then
                    If pVal.CharPressed = 13 Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            oForm.Items.Item("1").Click()
                            Dim oWORGrid As SAPbouiCOM.Grid
                            oWORGrid = oForm.Items.Item("WORGRID").Specific
                            If Not oWORGrid.DataTable.IsEmpty Then
                                oForm.Items.Item("IDH_EWC").Click(BoCellClickType.ct_Regular)
                                doDisableFieldsForWORCreated(oForm)
                            Else
                                EnableAllEditItems(oForm, {"IDH_REMIND", "IDH_ISPRNT", "IDH_UCY", "IDH_CYB", "IDH_BR", "IDH_EVN"})
                                doSetEnabled(oForm.Items.Item("IDH_CFLWCD"), True)
                                doSetEnabled(oForm.Items.Item("IDH_CFLDST"), True)
                                doSetEnabled(oForm.Items.Item("IDH_CFLCAR"), True)
                                doSetEnabled(oForm.Items.Item("IDH_CFLCUS"), True)
                                doSetEnabled(oForm.Items.Item("IDH_CFLADR"), True)
                                doSetEnabled(oForm.Items.Item("IDH_CFLDPS"), True)
                                doSetEnabled(oForm.Items.Item("IDH_CFLCAB"), True)
                                doSetEnabled(oForm.Items.Item("IDH_CFLCAS"), True)
                                doSetEnabled(oForm.Items.Item("IDH_CFLCUB"), True)
                                doSetEnabled(oForm.Items.Item("IDH_CFLCuS"), True)
                                doSetFocus(oForm, "IDH_WASCD")
                            End If
                        Else
                        goParent.goApplication.SendKeys("{TAB}")
                        BubbleEvent = False
                    End If
                    End If
                Else
                    If pVal.CharPressed = 9 Then
                        If pVal.ItemUID = "IDH_WASCD" Then

                            If String.IsNullOrWhiteSpace(getFormDFValue(oForm, IDH_EVNMASTER._WasCd).ToString()) Or getFormDFValue(oForm, IDH_EVNMASTER._WasCd).ToString().Equals("*") Then
                                setFormDFValue(oForm, IDH_EVNMASTER._WasDsc, "")
                            End If
                            setSharedData(oForm, "IDH_ITMCOD", getFormDFValue(oForm, IDH_EVNMASTER._WasCd))
                            setSharedData(oForm, "IDH_ITMNAM", getFormDFValue(oForm, IDH_EVNMASTER._WasDsc))
                            Dim sGrp As String = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup()
                            setSharedData(oForm, "IDH_GRPCOD", sGrp)
                            setSharedData(oForm, "SILENT", "SHOWMULTI")
                            goParent.doOpenModalForm("IDHWISRC", oForm)
                        ElseIf (pVal.ItemUID = "IDH_STDT" Or pVal.ItemUID = "IDH_CRDT" Or pVal.ItemUID = "IDH_ENDDT" Or pVal.ItemUID = "IDH_DUR") Then
                            If String.IsNullOrWhiteSpace(getFormDFValue(oForm, IDH_EVNMASTER._EnVnSnVs).ToString()) Then
                                setFormDFValue(oForm, IDH_EVNMASTER._CreateDt, "")
                                setFormDFValue(oForm, IDH_EVNMASTER._StartDt, "")
                                setFormDFValue(oForm, IDH_EVNMASTER._EndDt, "")
                                setFormDFValue(oForm, IDH_EVNMASTER._Duration, "")
                            End If
                        ElseIf pVal.ItemUID = "IDH_WASNM" Then
                            If String.IsNullOrWhiteSpace(getFormDFValue(oForm, IDH_EVNMASTER._WasDsc).ToString()) Or getFormDFValue(oForm, IDH_EVNMASTER._WasDsc).ToString().Equals("*") Then
                                setFormDFValue(oForm, IDH_EVNMASTER._WasCd, "")
                            End If
                        ElseIf pVal.ItemUID = "IDH_EWC" Then
                            setSharedData(oForm, "IDH_WTWC", getFormDFValue(oForm, IDH_EVNMASTER._EWCCd))
                            goParent.doOpenModalForm("IDH_WTECSR", oForm)
                            '##MA Start 14-02-2017
                        ElseIf pVal.ItemUID = "IDH_DSPCD" OrElse pVal.ItemUID = "IDH_DSPNM" Then
                            setWFValue(oForm, "CallFromSupplier", True)
                            setSharedData(oForm, "TRG", "SUP")
                            setSharedData(oForm, "IDH_BPCOD", getFormDFValue(oForm, IDH_EVNMASTER._DispSiteCd))
                            setSharedData(oForm, "IDH_NAME", getFormDFValue(oForm, IDH_EVNMASTER._DispSiteNm))
                            setSharedData(oForm, "IDH_TYPE", "")
                            setParentSharedData(oForm, "CalledFromEVN", True)
                            goParent.doOpenModalForm("IDHCSRCH", oForm)
                        ElseIf pVal.ItemUID = "IDH_CARCD" OrElse pVal.ItemUID = "IDH_CARNM" Then
                            setWFValue(oForm, "CallFromCarrier", True)
                            setSharedData(oForm, "TRG", "SUP")
                            setSharedData(oForm, "IDH_BPCOD", getFormDFValue(oForm, IDH_EVNMASTER._CarCd))
                            setSharedData(oForm, "IDH_NAME", getFormDFValue(oForm, IDH_EVNMASTER._CarNm))
                            setSharedData(oForm, "IDH_TYPE", "")
                            setParentSharedData(oForm, "CalledFromEVN", True)
                            goParent.doOpenModalForm("IDHCSRCH", oForm)
                        ElseIf pVal.ItemUID = "IDH_CUSCD" OrElse pVal.ItemUID = "IDH_CUSNM" Then
                            setSharedData(oForm, "TRG", "CUS")
                            setSharedData(oForm, "IDH_BPCOD", getFormDFValue(oForm, IDH_EVNMASTER._CusCd))
                            setSharedData(oForm, "IDH_NAME", getFormDFValue(oForm, IDH_EVNMASTER._CusNm))
                            setSharedData(oForm, "IDH_TYPE", "F-C")
                            setParentSharedData(oForm, "CalledFromEVN", False)
                            goParent.doOpenModalForm("IDHCSRCH", oForm)
                            '##MA End 14-02-2017
                        ElseIf pVal.ItemUID = "IDH_DSPAD" Then
                            setWFValue(oForm, "CallFromDisposalBillAddress", True)
                            setSharedData(oForm, "IDH_CUSCOD", getFormDFValue(oForm, IDH_EVNMASTER._DispSiteCd))
                            setSharedData(oForm, "IDH_ADDRES", getFormDFValue(oForm, IDH_EVNMASTER._DSAddress))
                            setSharedData(oForm, "IDH_ADRTYP", "B")
                            goParent.doOpenModalForm("IDHASRCH", oForm)
                            '##MA Start 14-02-2017
                        ElseIf pVal.ItemUID = "IDH_DSPSAD" Then
                            setWFValue(oForm, "CallFromDisposalShipAddress", True)
                            setSharedData(oForm, "IDH_CUSCOD", getFormDFValue(oForm, IDH_EVNMASTER._DispSiteCd))
                            setSharedData(oForm, "IDH_ADDRES", getFormDFValue(oForm, IDH_EVNMASTER._DSPSAddr))
                            setSharedData(oForm, "IDH_ADRTYP", "S")
                            goParent.doOpenModalForm("IDHASRCH", oForm)
                        ElseIf pVal.ItemUID = "IDH_CARAD" Then
                            setWFValue(oForm, "CallFromCarrierBillAddress", True)
                            setSharedData(oForm, "IDH_CUSCOD", getFormDFValue(oForm, IDH_EVNMASTER._CarCd))
                            setSharedData(oForm, "IDH_ADDRES", getFormDFValue(oForm, IDH_EVNMASTER._CAAddr))
                            setSharedData(oForm, "IDH_ADRTYP", "B")
                            goParent.doOpenModalForm("IDHASRCH", oForm)
                        ElseIf pVal.ItemUID = "IDH_CARSAD" Then
                            setWFValue(oForm, "CallFromCarrierShipAddress", True)
                            setSharedData(oForm, "IDH_CUSCOD", getFormDFValue(oForm, IDH_EVNMASTER._CarCd))
                            setSharedData(oForm, "IDH_ADDRES", getFormDFValue(oForm, IDH_EVNMASTER._CASAddr))
                            setSharedData(oForm, "IDH_ADRTYP", "S")
                            goParent.doOpenModalForm("IDHASRCH", oForm)
                        ElseIf pVal.ItemUID = "IDH_CUSAD" Then
                            setWFValue(oForm, "CallFromCustomerBillAddress", True)
                            setSharedData(oForm, "IDH_CUSCOD", getFormDFValue(oForm, IDH_EVNMASTER._CusCd))
                            setSharedData(oForm, "IDH_ADDRES", getFormDFValue(oForm, "U_CusAddr"))
                            setSharedData(oForm, "IDH_ADRTYP", "B")
                            goParent.doOpenModalForm("IDHASRCH", oForm)
                        ElseIf pVal.ItemUID = "IDH_CUSSAD" Then
                            setWFValue(oForm, "CallFromCustomerShipAddress", True)
                            setSharedData(oForm, "IDH_CUSCOD", getFormDFValue(oForm, "U_CusCd"))
                            setSharedData(oForm, "IDH_ADDRES", getFormDFValue(oForm, "U_CusSAddr"))
                            setSharedData(oForm, "IDH_ADRTYP", "S")
                            goParent.doOpenModalForm("IDHASRCH", oForm)
                        ElseIf pVal.ItemUID = "IDH_YR1" Or pVal.ItemUID = "IDH_YR2" Or pVal.ItemUID = "IDH_YR3" Or pVal.ItemUID = "IDH_YR4" Or pVal.ItemUID = "IDH_YR5" Then
                            If getFormDFValue(oForm, "U_EnVnSnVs") = "1" Then
                                setFormDFValue(oForm, "U_Quantity", Convert.ToInt32(If(getFormDFValue(oForm, "U_Year1") = Nothing, "0", getFormDFValue(oForm, "U_Year1"))) + Convert.ToInt32(If(getFormDFValue(oForm, "U_Year2") = Nothing, "0", getFormDFValue(oForm, "U_Year2"))) + Convert.ToInt32(If(getFormDFValue(oForm, "U_Year3") = Nothing, "0", getFormDFValue(oForm, "U_Year3"))) + Convert.ToInt32(If(getFormDFValue(oForm, "U_Year4") = Nothing, "0", getFormDFValue(oForm, "U_Year4"))) + Convert.ToInt32(If(getFormDFValue(oForm, "U_Year5") = Nothing, "0", getFormDFValue(oForm, "U_Year5"))))
                            End If
                            '##MA End 14-02-2017
                        End If
                    End If

                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If Not pVal.BeforeAction Then
                    '##MA Start 20-02-2017
                    If pVal.ItemUID = "btnDecla" Then
                        Try
                            doEVNReportHeader(oForm, "btnDecla")
                        Catch ex As Exception
                            com.idh.bridge.res.Messages.INSTANCE.doMessage(ex.Message & ":---->" & ex.StackTrace)
                        End Try
                    ElseIf pVal.ItemUID = "btnDelete" Then
                        Try
                            'Open file 
                            Dim oGAttach As UpdateGrid = UpdateGrid.getInstance(oForm, "ATTACGRD")
                            Dim oSelected As SAPbouiCOM.SelectedRows = oGAttach.getGrid().Rows.SelectedRows
                            Dim iSelectionCount As Integer = oSelected.Count
                            If iSelectionCount = 1 Then
                                If (goParent.doResourceMessageYN("EVNFDEL", Nothing, 1) = 1) Then
                                    doDeleteAttachFile(oForm, oGAttach)
                                    oGAttach.doReloadData()
                                    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                    End If
                                End If
                            Else
                                doWarnMess("Select 1 row to delete the file.")
                            End If
                        Catch ex As Exception
                            com.idh.bridge.DataHandler.INSTANCE.doUserError("Error occured while system trying to delete selected file.")
                        End Try
                    ElseIf pVal.ItemUID = "btnBrowse" Then
                        Try
                            doCallWinFormOpener(AddressOf doAttachFile, oForm)
                        Catch ex As Exception
                            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
                        End Try
                    ElseIf pVal.ItemUID = "btnDisplay" Then
                        Try
                            'Open file 
                            Dim oGAttach As UpdateGrid = UpdateGrid.getInstance(oForm, "ATTACGRD")
                            Dim oSelected As SAPbouiCOM.SelectedRows = oGAttach.getGrid().Rows.SelectedRows
                            Dim iSelectionCount As Integer = oSelected.Count
                            If iSelectionCount = 1 Then
                                Dim sFileInfo As String = oGAttach.doGetFieldValue("trgtPath") + "\" + oGAttach.doGetFieldValue("FileName") + "." + oGAttach.doGetFieldValue("FileExt")
                                doDisplayFile(sFileInfo)
                            Else
                                doWarnMess("Select 1 row to display the file.")
                            End If
                        Catch ex As Exception
                            com.idh.bridge.DataHandler.INSTANCE.doUserError("Error occured while system trying to load selected file.")
                        End Try
                        '##MA End 20-02-2017

                    ElseIf pVal.ItemUID = "IDH_FINDAD" Then
                        doFindAddress(oForm)
                        '##MA Start 21-02-2017
                    ElseIf pVal.ItemUID = "IDH_SN" Then
                        oForm.Freeze(True)
                        oForm.Items.Item("LINESGRID").Visible = True
                        Dim grdFreeTextFixedValues As String() = {
                                                              "D", "E", "F", "G",
                                                              "A", "B", "C",
                                                              "L", "M",
                                                              "N", "H", "I", "K", "P",
                                                              "S", "R"
                                                             }
                        Dim oGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                        If (oGrid.getSBOGrid().Rows.Count < 17) AndAlso (oGrid.getSBOGrid().Rows.Count > 0) Then
                            For index = 1 To 16
                                oGrid.doAddRow(True, False, False)
                                oGrid.doSetFieldValue("U_BULAND", index - 1, grdFreeTextFixedValues.ElementAt(index - 1))
                            Next
                        End If
                        oForm.Freeze(False)
                    ElseIf pVal.ItemUID = "IDH_EN" Or pVal.ItemUID = "IDH_VN" Or pVal.ItemUID = "IDH_VS" Then
                        doSetFocus(oForm, "IDH_WASCD")
                        oForm.Items.Item("LINESGRID").Visible = False
                    ElseIf pVal.ItemUID = "IDH_TABGEN" Then
                        oForm.PaneLevel = 1
                    ElseIf pVal.ItemUID = "IDH_TABMAT" Then
                        oForm.PaneLevel = 2
                    ElseIf pVal.ItemUID = "IDH_TABATT" Then
                        oForm.PaneLevel = 3
                        '##MA Start 21-02-20
                    ElseIf pVal.ItemUID = "IDH_CFLWCD" Then 'Waste Code 
                        setSharedData(oForm, "IDH_ITMCOD", getFormDFValue(oForm, "U_WasCd"))
                        setSharedData(oForm, "IDH_ITMNAM", getFormDFValue(oForm, "U_WasDsc"))
                        Dim sGrp As String = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup()
                        setSharedData(oForm, "IDH_GRPCOD", sGrp)
                        setSharedData(oForm, "SILENT", "SHOWMULTI")
                        goParent.doOpenModalForm("IDHWISRC", oForm)

                    ElseIf pVal.ItemUID = "IDH_CFLEWC" Then 'EWC 
                        setSharedData(oForm, "IDH_WTWC", getFormDFValue(oForm, "U_EWCCd"))
                        goParent.doOpenModalForm("IDH_WTECSR", oForm)
                        '##MA Start 14-02-2017
                    ElseIf pVal.ItemUID = "IDH_CFLDST" Then 'Disposal Site - BP 
                        setSharedData(oForm, "TRG", "SUP")
                        setWFValue(oForm, "CallFromSupplier", True)
                        setSharedData(oForm, "IDH_BPCOD", getFormDFValue(oForm, "U_DispSiteCd"))
                        setSharedData(oForm, "IDH_NAME", getFormDFValue(oForm, "U_DispSiteNm"))
                        setSharedData(oForm, "IDH_TYPE", "")
                        setParentSharedData(oForm, "CalledFromEVN", True)
                        goParent.doOpenModalForm("IDHCSRCH", oForm)
                    ElseIf pVal.ItemUID = "IDH_CFLCAR" Then 'Carrier - BP 
                        setSharedData(oForm, "TRG", "SUP")
                        setWFValue(oForm, "CallFromCarrier", True)
                        setSharedData(oForm, "IDH_BPCOD", getFormDFValue(oForm, "U_CarCd"))
                        setSharedData(oForm, "IDH_NAME", getFormDFValue(oForm, "U_CarNm"))
                        setSharedData(oForm, "IDH_TYPE", "")
                        setParentSharedData(oForm, "CalledFromEVN", True)
                        goParent.doOpenModalForm("IDHCSRCH", oForm)
                    ElseIf pVal.ItemUID = "IDH_CFLCUS" Then 'Customer - BP 
                        setSharedData(oForm, "TRG", "CUS")
                        setSharedData(oForm, "IDH_BPCOD", getFormDFValue(oForm, "U_CusCd"))
                        setSharedData(oForm, "IDH_NAME", getFormDFValue(oForm, "U_CusNm"))
                        setSharedData(oForm, "IDH_TYPE", "F-C")
                        setParentSharedData(oForm, "CalledFromEVN", False)
                        goParent.doOpenModalForm("IDHCSRCH", oForm)
                        '##MA End 14-02-2017
                    ElseIf pVal.ItemUID = "IDH_CFLADR" Then
                        setWFValue(oForm, "CallFromDisposalBillAddress", True)
                        setSharedData(oForm, "IDH_CUSCOD", getFormDFValue(oForm, "U_DispSiteCd"))
                        setSharedData(oForm, "IDH_ADDRES", getFormDFValue(oForm, "U_DSAddress"))
                        setSharedData(oForm, "IDH_ADRTYP", "B")
                        goParent.doOpenModalForm("IDHASRCH", oForm)
                        '##MA Start 14-02-2017
                    ElseIf pVal.ItemUID = "IDH_CFLDPS" Then
                        setWFValue(oForm, "CallFromDisposalShipAddress", True)
                        setSharedData(oForm, "IDH_CUSCOD", getFormDFValue(oForm, "U_DispSiteCd"))
                        setSharedData(oForm, "IDH_ADDRES", getFormDFValue(oForm, "U_DSPSAddr"))
                        setSharedData(oForm, "IDH_ADRTYP", "S")
                        goParent.doOpenModalForm("IDHASRCH", oForm)
                    ElseIf pVal.ItemUID = "IDH_CFLCAB" Then
                        setWFValue(oForm, "CallFromCarrierBillAddress", True)
                        setSharedData(oForm, "IDH_CUSCOD", getFormDFValue(oForm, "U_CarCd"))
                        setSharedData(oForm, "IDH_ADDRES", getFormDFValue(oForm, "U_CAAddr"))
                        setSharedData(oForm, "IDH_ADRTYP", "B")
                        goParent.doOpenModalForm("IDHASRCH", oForm)
                    ElseIf pVal.ItemUID = "IDH_CFLCAS" Then
                        setWFValue(oForm, "CallFromCarrierShipAddress", True)
                        setSharedData(oForm, "IDH_CUSCOD", getFormDFValue(oForm, "U_CarCd"))
                        setSharedData(oForm, "IDH_ADDRES", getFormDFValue(oForm, "U_CASAddr"))
                        setSharedData(oForm, "IDH_ADRTYP", "S")
                        goParent.doOpenModalForm("IDHASRCH", oForm)
                    ElseIf pVal.ItemUID = "IDH_CFLCUB" Then
                        setWFValue(oForm, "CallFromCustomerBillAddress", True)
                        setSharedData(oForm, "IDH_CUSCOD", getFormDFValue(oForm, "U_CusCd"))
                        setSharedData(oForm, "IDH_ADDRES", getFormDFValue(oForm, "U_CusAddr"))
                        setSharedData(oForm, "IDH_ADRTYP", "B")
                        goParent.doOpenModalForm("IDHASRCH", oForm)
                    ElseIf pVal.ItemUID = "IDH_CFLCuS" Then
                        setWFValue(oForm, "CallFromCustomerShipAddress", True)
                        setSharedData(oForm, "IDH_CUSCOD", getFormDFValue(oForm, "U_CusCd"))
                        setSharedData(oForm, "IDH_ADDRES", getFormDFValue(oForm, "U_CusSAddr"))
                        setSharedData(oForm, "IDH_ADRTYP", "S")
                        goParent.doOpenModalForm("IDHASRCH", oForm)
                        '##MA End 14-02-2017
                    ElseIf pVal.ItemUID = "IDH_ACTVTY" Then
                        If getFormDFValue(oForm, "U_Activity").ToString().Length = 0 Then
                            setSharedData(oForm, "IDH_EVN", getFormDFValue(oForm, "U_EVNNumber"))
                            setSharedData(oForm, "IDH_CUST", getFormDFValue(oForm, "U_DispSiteCd"))
                            setSharedData(oForm, "sFMode", SAPbouiCOM.BoFormMode.fm_ADD_MODE.ToString())
                            goParent.doOpenModalForm("651", oForm)
                        Else
                            oForm.Items.Item("65").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                        End If
                    ElseIf pVal.ItemUID = "IDH_PRINT" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            If doEVNReport(oForm, "IDH_PRINT") Then
                                'update EVN isPrinted flag 
                                setFormDFValue(oForm, "U_IsPrinted", "Y")
                                doUpdateHeader(oForm, False)
                                oForm.Update()
                                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                            End If
                        Else
                            DataHandler.INSTANCE.doErrorToStatusbar(getTranslatedWord("Cannot Print! There is unsaved data on form."), False)
                        End If
                        '##MA Start 14-02-2017
                    ElseIf pVal.ItemUID = "IDH_Ja" Then
                        oForm.Items.Item("IDH_Fre").Visible = True
                        oForm.Items.Item("IDH_Fre").Enabled = False
                        oForm.Items.Item("t_Fre").Visible = True
                    ElseIf pVal.ItemUID = "IDH_Nein" Then
                        oForm.Items.Item("IDH_Fre").Visible = False
                        oForm.Items.Item("t_Fre").Visible = False
                        '##MA Start 14-02-2017
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                Try
                    If Not pVal.BeforeAction AndAlso pVal.ItemUID = "IDH_DUR" Then
                        If getFormDFValue(oForm, IDH_EVNMASTER._EnVnSnVs) = "3" Then
                            Dim sumOfQty As Integer = 0
                            For index = 1 To 5
                                setFormDFValue(oForm, "U_Year" & index, "0")
                                oForm.Items.Item("IDH_YR" & index).Enabled = False
                            Next
                            Dim sDuration As Short = Convert.ToInt32(getFormDFValue(oForm, "U_Duration"))
                            Dim QtyPerYear As String = Config.INSTANCE.doGetEVNQtyPerYear()
                            For index = 1 To sDuration
                                oForm.Items.Item("IDH_YR" & index).Enabled = True
                                setFormDFValue(oForm, "U_Year" & index, QtyPerYear)
                                sumOfQty = sumOfQty + oForm.Items.Item("IDH_YR" & index).Specific.Value
                            Next
                            setFormDFValue(oForm, "U_Quantity", sumOfQty)
                        ElseIf getFormDFValue(oForm, IDH_EVNMASTER._EnVnSnVs) = "1" Then
                            For index = 1 To 5
                                setFormDFValue(oForm, "U_Year" & index, "0")
                                oForm.Items.Item("IDH_YR" & index).Enabled = False
                            Next
                            Dim sDuration As Short = Convert.ToInt32(getFormDFValue(oForm, "U_Duration"))
                            For index = 1 To sDuration
                                oForm.Items.Item("IDH_YR" & index).Enabled = True
                            Next
                            setFormDFValue(oForm, "U_Quantity", "")
                    End If
                    ElseIf Not pVal.BeforeAction AndAlso pVal.ItemUID = "IDH_ORDERS" Then
                        doWORGrid(oForm)
                End If
                Catch ex As Exception
                    com.idh.bridge.DataHandler.INSTANCE.doUserError(ex.Message)
                End Try
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_GOT_FOCUS Then
                If pVal.BeforeAction = False AndAlso (pVal.ItemUID = "IDH_STDT" Or pVal.ItemUID = "IDH_CRDT" Or pVal.ItemUID = "IDH_ENDDT" Or pVal.ItemUID = "IDH_DUR") Then
                    If getFormDFValue(oForm, "U_EnVnSnVs").ToString().Equals("") Or getFormDFValue(oForm, "U_EnVnSnVs").ToString().Equals(Nothing) Then
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Plese select EN, VN, SN or VS before selecting date.")
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_LOST_FOCUS Then
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK Then
                If pVal.ItemUID = "WORGRID" Then
                    If getFormDFValue(oForm, "U_SinMul") = "1" Then 'for multiple
                        Dim oWORGrid As SAPbouiCOM.Grid
                        oWORGrid = oForm.Items.Item("WORGRID").Specific
                        If oWORGrid.DataTable.IsEmpty = False Then
                            oWORGrid.Rows.SelectedRows.Add(pVal.Row)
                            Dim value As String = Convert.ToString(oWORGrid.DataTable.GetValue("WOH", pVal.Row))
                            goParent.doOpenModalForm("IDH_WASTORD", oForm)
                            goParent.goApplication.Forms.ActiveForm.Freeze(True)
                            goParent.goApplication.Forms.ActiveForm.Mode = BoFormMode.fm_FIND_MODE
                            setItemValue(goParent.goApplication.Forms.ActiveForm, "IDH_BOOREF", value, False)
                            goParent.goApplication.Forms.ActiveForm.Items.Item("1").Click(BoCellClickType.ct_Regular)
                            goParent.goApplication.Forms.ActiveForm.Freeze(False)
                        End If
                    End If
                End If
            ElseIf pVal.EventType = BoEventTypes.et_FORM_ACTIVATE Then
                Dim oWORGrid As SAPbouiCOM.Grid
                oWORGrid = oForm.Items.Item("WORGRID").Specific
                If pVal.BeforeAction = False Then
                    oForm.Freeze(True)
                    doWORGrid(oForm)
                    If Not oWORGrid.DataTable.IsEmpty Then
                        oForm.Items.Item("IDH_EWC").Click(BoCellClickType.ct_Regular)
                        doDisableFieldsForWORCreated(oForm)
                    End If
                    oForm.Freeze(False)
                End If
            End If
            Return True
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    If doValidateFields(oForm) Then
                    oForm.Freeze(True)
                    Dim sCode As String = getFormDFValue(oForm, "Code").ToString()
                    If sCode Is Nothing OrElse sCode.Length = 0 Then
                        Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(IDH_EVNMASTER.AUTONUMPREFIX, "EVNMAS")
                        setFormDFValue(oForm, IDH_EVNMASTER._Code, oNumbers.CodeCode)
                        setFormDFValue(oForm, IDH_EVNMASTER._Name, oNumbers.NameCode)
                    End If

                    If doUpdateHeader(oForm, True) Then
                            Dim oGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                            oGrid.doProcessData()
                        oForm.Update()
                            doCreateNewEntry(oForm)
                            doLoadData(oForm)
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                        BubbleEvent = False
                    End If
                    oForm.Freeze(False)
                    End If

                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    oForm.Freeze(True)
                    If doFind(oForm) Then
                        doSetFocus(oForm, "IDH_UCY")
                        EnableAllEditItems(oForm, {"IDH_EVN", "IDH_UCY", "IDH_CYB", "IDH_BR", "IDH_REMIND", "IDH_Fre", "IDH_ISPRNT"})
                        Dim duration As String = getFormDFValue(oForm, "U_Duration")
                        If Not duration = "" Then
                            For index = Convert.ToInt32(duration) + 1 To 5
                                oForm.Items.Item("IDH_YR" & index.ToString()).Enabled = False
                            Next
                        End If
                    End If
                    oForm.Freeze(False)
                    BubbleEvent = False
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    If doValidateFields(oForm) Then
                    oForm.Freeze(True)
                    If doUpdateHeader(oForm, False) Then
                            Dim oGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                            oGrid.doProcessData()
                        oForm.Update()
                            doLoadData(oForm)
                            Dim duration As String = getFormDFValue(oForm, "U_Duration")
                            If Not duration = "" Then
                                For index = Convert.ToInt32(duration) + 1 To 5
                                    oForm.Items.Item("IDH_YR" & index.ToString()).Enabled = False
                                Next
                            End If
                            doSetFocus(oForm, "IDH_WASCD")
                            oForm.Items.Item("IDH_EVN").Enabled = False
                            'doCreateNewEntry(oForm)
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        BubbleEvent = False
                    End If
                    oForm.Freeze(False)
                    Else
                        BubbleEvent = False
                    End If
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    doReturnFromModalShared(oForm, True)
                End If
            Else
            End If

        End Sub

        Public Overrides Function doRightClickEvent(oForm As SAPbouiCOM.Form, ByRef pVal As ContextMenuInfo, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.ItemUID = "WORGRID" Then
                If pVal.BeforeAction = True Then
                    Dim oWORGrid As SAPbouiCOM.Grid
                    oWORGrid = oForm.Items.Item("WORGRID").Specific
                    Dim oMenuItem As SAPbouiCOM.MenuItem
                    Dim sRMenuId As String = IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU
                    oMenuItem = goParent.goApplication.Menus.Item(sRMenuId)
                    Dim oMenus As SAPbouiCOM.Menus
                    Dim iMenuPos As Integer = 1
                    Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
                    oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
                    oCreationPackage.Enabled = True
                    oMenus = oMenuItem.SubMenus
                    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING
                    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                    oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    Dim sQry As String
                    sQry = "Select U_EVNNumber, UsageCurrentYear, CurrentYearBalance, OverAllSum, BalanceRemaining from IDH_VEVNMGR  with (NOLOCK) where U_EVNNumber = '" & getFormDFValue(oForm, "U_EVNNumber") & "'"
                    oRecordSet = goParent.goDB.doSelectQuery(sQry)
                    'If Not oRecordSet Is Nothing AndAlso oRecordSet.RecordCount > 0 Then
                    '    oUCY.Value = oRecordSet.Fields.Item("UsageCurrentYear").Value.ToString()
                    '    oCYB.Value = oRecordSet.Fields.Item("CurrentYearBalance").Value.ToString()
                    '    oBR.Value = oRecordSet.Fields.Item("BalanceRemaining").Value.ToString()
                    'Else
                    '    oUCY.Value = ""
                    '    oCYB.Value = ""
                    '    oBR.Value = ""
                    'End If

                    'Create New WOH only if (1) No row in Grid 
                    ' Or (2) In case of multiple, the current year balance not zero 
                    If (oWORGrid.DataTable.IsEmpty) Or (getItemValue(oForm, "IDH_Mul") = "1" AndAlso Not oRecordSet Is Nothing AndAlso oRecordSet.RecordCount > 0 AndAlso Not oRecordSet.Fields.Item("CurrentYearBalance").Value.ToString() = "0") Then
                        oCreationPackage.UniqueID = "NEW"
                        oCreationPackage.String = getTranslatedWord("&New WOH")
                        oCreationPackage.Position = iMenuPos
                        iMenuPos += 1
                        oMenus.AddEx(oCreationPackage)
                    End If
                Else
                    If goParent.goApplication.Menus.Exists("NEW") Then
                        goParent.goApplication.Menus.RemoveEx("NEW")
                    End If
                End If
            End If
            Return MyBase.doRightClickEvent(oForm, pVal, BubbleEvent)
        End Function

        Public Overrides Function doCustomItemEvent(oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_ADD Then
                Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, "EVNDET")
                oGridN.doSetFieldValue("Code", pVal.Row, oNumbers.CodeCode, True)
                oGridN.doSetFieldValue("Name", pVal.Row, oNumbers.NameCode, True)

                'Using the Form's Main Table
                Dim sCode As String = getFormDFValue(oForm, "Code")
                oGridN.doSetFieldValue("U_HDRCode", pVal.Row, sCode, True)

            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_ADD_EMPTY Then

                Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, "EVNDET")
                oGridN.doSetFieldValue("Code", pVal.Row, oNumbers.CodeCode, True)
                oGridN.doSetFieldValue("Name", pVal.Row, oNumbers.NameCode, True)

                'Using the Form's Main Table
                Dim sCode As String = getFormDFValue(oForm, "Code")
                oGridN.doSetFieldValue("U_HDRCode", pVal.Row, sCode, True)

            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK Then
            End If
            Return MyBase.doCustomItemEvent(oForm, pVal)
        End Function

        Protected Overrides Sub doHandleModalResultShared(oForm As SAPbouiCOM.Form, sModalFormType As String, Optional sLastButton As String = Nothing)
            'IDHEVNNSR

            If sModalFormType = "IDHEVNSRC" Then
                Dim sCode As String = getSharedData(oForm, "IDH_EVNCD")
                setItemValue(oForm, "IDH_EVN", sCode)
            ElseIf sModalFormType = "IDHEVNNSR" Then
                Dim code As String = getSharedData(oForm, "IDH_EVNCD")
                setFormDFValue(oForm, "Code", code)
                doLoadData(oForm)
                Dim duration As String = getFormDFValue(oForm, "U_Duration")
                If Not duration = "" Then
                    For index = Convert.ToInt32(duration) + 1 To 5
                        oForm.Items.Item("IDH_YR" & index.ToString()).Enabled = False
                    Next
                End If
                oForm.Items.Item("IDH_EVN").Enabled = False
            ElseIf sModalFormType = "IDHWISRC" Then 'Waste Code Search 
                setFormDFValue(oForm, "U_WasCd", getSharedData(oForm, "ITEMCODE"))
                setFormDFValue(oForm, "U_WasDsc", getSharedData(oForm, "ITEMNAME"))
            ElseIf sModalFormType = "IDH_WTECSR" Then 'EWC Code 
                setFormDFValue(oForm, "U_EWCCd", getSharedData(oForm, "IDH_WTWCO"))
            ElseIf sModalFormType = "IDHCSRCH" Then 'Disposal Site 

                '##MA Start 14-02-2017
                Dim sTarget, sCardCode, sCardName As String
                sTarget = getSharedData(oForm, "TRG")
                If sTarget.Equals("SUP") Then

                    sCardCode = getSharedData(oForm, "CARDCODE")
                    sCardName = getSharedData(oForm, "CARDNAME")

                    'Get Default address to fetch them
                    Dim sBillToDefault As String = ""
                    Dim sShipToDefault As String = ""
                    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                    oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    Dim sQry As String = "SELECT ShipToDef, BillToDef From OCRD with (NOLOCK) Where cardcode = '" & sCardCode & "'"
                    oRecordSet.DoQuery(sQry)
                    If oRecordSet.RecordCount > 0 Then
                        oRecordSet.MoveFirst()
                        sBillToDefault = oRecordSet.Fields.Item("BillToDef").Value
                        sShipToDefault = oRecordSet.Fields.Item("ShipToDef").Value
                    End If

                    If getWFValue(oForm, "CallFromSupplier") Then
                        'Supplier
                        setWFValue(oForm, "CallFromSupplier", False)

                        setFormDFValue(oForm, "U_DispSiteCd", sCardCode)
                        setFormDFValue(oForm, "U_DispSiteNm", sCardName)

                        'If Supplier Address Config is false, then set address
                        If Config.INSTANCE.doGetEVNDisposalAddress() = False Then
                            Dim oBPAddressShipTo As BPAddress = New BPAddress()
                            oBPAddressShipTo.doGetAddress(sCardCode, "S", False, sShipToDefault)
                            setFormDFValue(oForm, "U_DSPSAddr", oBPAddressShipTo.Address)
                            setFormDFValue(oForm, "U_DSPSStrt", oBPAddressShipTo.Street)
                            setFormDFValue(oForm, "U_DSPSBlock", oBPAddressShipTo.Block)
                            setFormDFValue(oForm, "U_DSPSCity", oBPAddressShipTo.City)
                            setFormDFValue(oForm, "U_DSPSPostCd", oBPAddressShipTo.ZipCode)
                            setFormDFValue(oForm, "U_DSPSSID", oBPAddressShipTo.U_IDHSteId)
                            setFormDFValue(oForm, "U_DSPFreistellu", oBPAddressShipTo.U_Freist)
                            setFormDFValue(oForm, "U_DSPSSID", oBPAddressShipTo.U_ICOEntNr)
                            Dim oBPAddressBillTo As BPAddress = New BPAddress()
                            oBPAddressBillTo.doGetAddress(sCardCode, "B", False, sBillToDefault)
                            setFormDFValue(oForm, "U_DSAddress", oBPAddressBillTo.Address)
                            setFormDFValue(oForm, "U_DSStreet", oBPAddressBillTo.Street)
                            setFormDFValue(oForm, "U_DSBlock", oBPAddressBillTo.Block)
                            setFormDFValue(oForm, "U_DSCity", oBPAddressBillTo.City)
                            setFormDFValue(oForm, "U_DSPostCd", oBPAddressBillTo.ZipCode)
                        End If

                    ElseIf getWFValue(oForm, "CallFromCarrier") Then
                        setWFValue(oForm, "CallFromCarrier", False)

                        'Carrier
                        setFormDFValue(oForm, "U_CarCd", sCardCode)
                        setFormDFValue(oForm, "U_CarNm", sCardName)

                        'If Carrier Address Config is false, then set address
                        If Config.INSTANCE.doGetEVNCarrierAddress() = False Then
                            Dim oBPAddressShipToCarrier As BPAddress = New BPAddress()
                            oBPAddressShipToCarrier.doGetAddress(sCardCode, "S", False, sShipToDefault)
                            setFormDFValue(oForm, "U_CASAddr", oBPAddressShipToCarrier.Address)
                            setFormDFValue(oForm, "U_CASStrt", oBPAddressShipToCarrier.Street)
                            setFormDFValue(oForm, "U_CASBlock", oBPAddressShipToCarrier.Block)
                            setFormDFValue(oForm, "U_CASCity", oBPAddressShipToCarrier.City)
                            setFormDFValue(oForm, "U_CASPostCd", oBPAddressShipToCarrier.ZipCode)
                            setFormDFValue(oForm, "U_CAID", oBPAddressShipToCarrier.U_Bef)

                            Dim oBPAddressBillToCarrier As BPAddress = New BPAddress()
                            oBPAddressBillToCarrier.doGetAddress(sCardCode, "B", False, sBillToDefault)
                            setFormDFValue(oForm, "U_CAAddr", oBPAddressBillToCarrier.Address)
                            setFormDFValue(oForm, "U_CAStrt", oBPAddressBillToCarrier.Street)
                            setFormDFValue(oForm, "U_CABlock", oBPAddressBillToCarrier.Block)
                            setFormDFValue(oForm, "U_CACity", oBPAddressBillToCarrier.City)
                            setFormDFValue(oForm, "U_CAPostCd", oBPAddressBillToCarrier.ZipCode)
                        End If
                    End If

                ElseIf sTarget.Equals("CUS") Then
                    sCardCode = getSharedData(oForm, "CARDCODE")
                    sCardName = getSharedData(oForm, "CARDNAME")

                    'Customer
                    setFormDFValue(oForm, "U_CusCd", sCardCode)
                    setFormDFValue(oForm, "U_CusNm", sCardName)

                    'Get Default address to fetch them
                    Dim sBillToDefault As String = ""
                    Dim sShipToDefault As String = ""
                    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                    oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    Dim sQry As String = "SELECT ShipToDef, BillToDef From OCRD with (NOLOCK) Where cardcode = '" & sCardCode & "'"
                    oRecordSet.DoQuery(sQry)
                    If oRecordSet.RecordCount > 0 Then
                        oRecordSet.MoveFirst()
                        sBillToDefault = oRecordSet.Fields.Item("BillToDef").Value
                        sShipToDefault = oRecordSet.Fields.Item("ShipToDef").Value
                    End If

                    'If Customer Address Config is false, then set address
                    If Config.INSTANCE.doGetEVNCustomerAddress() = False Then
                        Dim oBPAddressShipTo As BPAddress = New BPAddress()
                        oBPAddressShipTo.doGetAddress(sCardCode, "S", False, sShipToDefault)
                        setFormDFValue(oForm, "U_CusSAddr", oBPAddressShipTo.Address)
                        setFormDFValue(oForm, "U_CusSStrt", oBPAddressShipTo.Street)
                        setFormDFValue(oForm, "U_CusSBlock", oBPAddressShipTo.Block)
                        setFormDFValue(oForm, "U_CusSCity", oBPAddressShipTo.City)
                        setFormDFValue(oForm, "U_CusSPostCd", oBPAddressShipTo.ZipCode)
                        setFormDFValue(oForm, "U_CusID", oBPAddressShipTo.U_ICOPRODN)

                        Dim oBPAddressBillTo As BPAddress = New BPAddress()
                        oBPAddressBillTo.doGetAddress(sCardCode, "B", False, sBillToDefault)
                        setFormDFValue(oForm, "U_CusAddr", oBPAddressBillTo.Address)
                        setFormDFValue(oForm, "U_CusStreet", oBPAddressBillTo.Street)
                        setFormDFValue(oForm, "U_CusBlock", oBPAddressBillTo.Block)
                        setFormDFValue(oForm, "U_CusCity", oBPAddressBillTo.City)
                        setFormDFValue(oForm, "U_CusPostCd", oBPAddressBillTo.ZipCode)
                    End If

                End If
                '##MA End 14-02-2017

            ElseIf sModalFormType = "IDHASRCH" Then 'Address Search 

                '##MA Start 14-02-2017
                If getWFValue(oForm, "CallFromDisposalBillAddress") Then
                    setWFValue(oForm, "CallFromDisposalBillAddress", False)
                setFormDFValue(oForm, "U_DSAddress", getSharedData(oForm, "ADDRESS"))
                setFormDFValue(oForm, "U_DSStreet", getSharedData(oForm, "STREET"))
                setFormDFValue(oForm, "U_DSBlock", getSharedData(oForm, "BLOCK"))
                setFormDFValue(oForm, "U_DSCity", getSharedData(oForm, "CITY"))
                setFormDFValue(oForm, "U_DSPostCd", getSharedData(oForm, "ZIPCODE"))
                ElseIf getWFValue(oForm, "CallFromCarrierBillAddress") Then
                    setWFValue(oForm, "CallFromCarrierBillAddress", False)
                    setFormDFValue(oForm, "U_CAAddr", getSharedData(oForm, "ADDRESS"))
                    setFormDFValue(oForm, "U_CAStrt", getSharedData(oForm, "STREET"))
                    setFormDFValue(oForm, "U_CABlock", getSharedData(oForm, "BLOCK"))
                    setFormDFValue(oForm, "U_CACity", getSharedData(oForm, "CITY"))
                    setFormDFValue(oForm, "U_CAPostCd", getSharedData(oForm, "ZIPCODE"))
                ElseIf getWFValue(oForm, "CallFromCustomerBillAddress") Then
                    setWFValue(oForm, "CallFromCustomerBillAddress", False)
                    setFormDFValue(oForm, "U_CusAddr", getSharedData(oForm, "ADDRESS"))
                    setFormDFValue(oForm, "U_CusStreet", getSharedData(oForm, "STREET"))
                    setFormDFValue(oForm, "U_CusBlock", getSharedData(oForm, "BLOCK"))
                    setFormDFValue(oForm, "U_CusCity", getSharedData(oForm, "CITY"))
                    setFormDFValue(oForm, "U_CusPostCd", getSharedData(oForm, "ZIPCODE"))
                ElseIf getWFValue(oForm, "CallFromDisposalShipAddress") Then
                    setWFValue(oForm, "CallFromDisposalShipAddress", False)
                    setFormDFValue(oForm, "U_DSPSAddr", getSharedData(oForm, "ADDRESS"))
                    setFormDFValue(oForm, "U_DSPSStrt", getSharedData(oForm, "STREET"))
                    setFormDFValue(oForm, "U_DSPSBlock", getSharedData(oForm, "BLOCK"))
                    setFormDFValue(oForm, "U_DSPSCity", getSharedData(oForm, "CITY"))
                    setFormDFValue(oForm, "U_DSPSPostCd", getSharedData(oForm, "ZIPCODE"))
                    setFormDFValue(oForm, "U_DSPSSID", getSharedData(oForm, "DISPSID"))
                    setFormDFValue(oForm, "U_DSPFreistellu", getSharedData(oForm, "FREIST"))
                ElseIf getWFValue(oForm, "CallFromCarrierShipAddress") Then
                    setWFValue(oForm, "CallFromCarrierShipAddress", False)
                    setFormDFValue(oForm, "U_CASAddr", getSharedData(oForm, "ADDRESS"))
                    setFormDFValue(oForm, "U_CASStrt", getSharedData(oForm, "STREET"))
                    setFormDFValue(oForm, "U_CASBlock", getSharedData(oForm, "BLOCK"))
                    setFormDFValue(oForm, "U_CASCity", getSharedData(oForm, "CITY"))
                    setFormDFValue(oForm, "U_CASPostCd", getSharedData(oForm, "ZIPCODE"))
                    setFormDFValue(oForm, "U_CAID", getSharedData(oForm, "CARSID"))
                ElseIf getWFValue(oForm, "CallFromCustomerShipAddress") Then
                    setWFValue(oForm, "CallFromCustomerShipAddress", False)
                    setFormDFValue(oForm, "U_CusSAddr", getSharedData(oForm, "ADDRESS"))
                    setFormDFValue(oForm, "U_CusSStrt", getSharedData(oForm, "STREET"))
                    setFormDFValue(oForm, "U_CusSBlock", getSharedData(oForm, "BLOCK"))
                    setFormDFValue(oForm, "U_CusSCity", getSharedData(oForm, "CITY"))
                    setFormDFValue(oForm, "U_CusSPostCd", getSharedData(oForm, "ZIPCODE"))
                    setFormDFValue(oForm, "U_CusID", getSharedData(oForm, "CUSSID"))
                End If
                '##MA End 14-02-2017

            ElseIf sModalFormType = "651" Then
                setFormDFValue(oForm, "U_Activity", getSharedData(oForm, "ACTVTYCD"))
            End If
            doSetUpdate(oForm)
        End Sub

        Protected Overrides Sub doHandleModalCanceled(oParentForm As SAPbouiCOM.Form, sModalFormType As String)
            MyBase.doHandleModalCanceled(oParentForm, sModalFormType)

            '##MA Start 14-02-2017
            If sModalFormType.Equals("IDHCSRCH") Then
                setWFValue(oParentForm, "CallFromCarrier", False)
                setWFValue(oParentForm, "CallFromSupplier", False)
            End If

            If sModalFormType.Equals("IDHASRCH") Then
                setWFValue(oParentForm, "CallFromDisposalShipAddress", False)
                setWFValue(oParentForm, "CallFromDisposalBillAddress", False)
                setWFValue(oParentForm, "CallFromCarrierShipAddress", False)
                setWFValue(oParentForm, "CallFromCarrierBillAddress", False)
                setWFValue(oParentForm, "CallFromCustomerShipAddress", False)
                setWFValue(oParentForm, "CallFromCustomerBillAddress", False)
            End If
            '##MA End 14-02-2017

        End Sub

#End Region

#Region "Custom Functions"

        Protected Function doUpdateHeader(ByVal oForm As SAPbouiCOM.Form, ByVal bIsAdd As Boolean) As Boolean
            Try
            Return doAutoSave(oForm, Nothing, "EVNMAS")
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar(ex.Message)
                Return False
            End Try
        End Function

        Public Overridable Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form)
            Dim sCode As String = ""

            setFormDFValue(oForm, "Code", sCode)
            setFormDFValue(oForm, "U_EVNNumber", "")
            setFormDFValue(oForm, "U_WasCd", "")
            setFormDFValue(oForm, "U_WasDsc", "")
            setFormDFValue(oForm, "U_EWCCd", "")
            setFormDFValue(oForm, "U_DispSiteCd", "")
            setFormDFValue(oForm, "U_DispSiteNm", "")
            setFormDFValue(oForm, "U_DSAddress", "")
            setFormDFValue(oForm, "U_DSStreet", "")
            setFormDFValue(oForm, "U_DSBlock", "")
            setFormDFValue(oForm, "U_DSCity", "")
            setFormDFValue(oForm, "U_DSPostCd", "")
            setFormDFValue(oForm, "U_Activity", "")
            setFormDFValue(oForm, "U_CreateDt", "")
            setFormDFValue(oForm, "U_StartDt", "")
            setFormDFValue(oForm, "U_Duration", "")
            setFormDFValue(oForm, "U_UOM", "")
            setFormDFValue(oForm, "U_Quantity", "")
            setFormDFValue(oForm, "U_Year1", "")
            setFormDFValue(oForm, "U_Year2", "")
            setFormDFValue(oForm, "U_Year3", "")
            setFormDFValue(oForm, "U_Year4", "")
            setFormDFValue(oForm, "U_Year5", "")
            setFormDFValue(oForm, "U_DSPSAddr", "")
            setFormDFValue(oForm, "U_DSPSStrt", "")
            setFormDFValue(oForm, "U_DSPSBlock", "")
            setFormDFValue(oForm, "U_DSPSCity", "")
            setFormDFValue(oForm, "U_DSPSPostCd", "")
            setFormDFValue(oForm, "U_DSPSSID", "")
            setFormDFValue(oForm, "U_CarCd", "")
            setFormDFValue(oForm, "U_CarNm", "")
            setFormDFValue(oForm, "U_CAAddr", "")
            setFormDFValue(oForm, "U_CAStrt", "")
            setFormDFValue(oForm, "U_CABlock", "")
            setFormDFValue(oForm, "U_CACity", "")
            setFormDFValue(oForm, "U_CAPostCd", "")
            setFormDFValue(oForm, "U_CASAddr", "")
            setFormDFValue(oForm, "U_CASStrt", "")
            setFormDFValue(oForm, "U_CASBlock", "")
            setFormDFValue(oForm, "U_CASCity", "")
            setFormDFValue(oForm, "U_CASPostCd", "")
            setFormDFValue(oForm, "U_CAID", "")
            setFormDFValue(oForm, "U_CusCd", "")
            setFormDFValue(oForm, "U_CusNm", "")
            setFormDFValue(oForm, "U_CusAddr", "")
            setFormDFValue(oForm, "U_CusStreet", "")
            setFormDFValue(oForm, "U_CusBlock", "")
            setFormDFValue(oForm, "U_CusCity", "")
            setFormDFValue(oForm, "U_CusPostCd", "")
            setFormDFValue(oForm, "U_CusSAddr", "")
            setFormDFValue(oForm, "U_CusSStrt", "")
            setFormDFValue(oForm, "U_CusSBlock", "")
            setFormDFValue(oForm, "U_CusSCity", "")
            setFormDFValue(oForm, "U_CusSPostCd", "")
            setFormDFValue(oForm, "U_CusID", "")
            setFormDFValue(oForm, "U_EndDt", "")
            setFormDFValue(oForm, "U_AtcEntry", "")
            setFormDFValue(oForm, "U_RelNum", "")
            setFormDFValue(oForm, "U_PreTre", "")
            setFormDFValue(oForm, "U_DecAtt", "")
            setFormDFValue(oForm, "U_SinMul", "")
            setFormDFValue(oForm, "U_EnVnSnVs", "")
            setFormDFValue(oForm, IDH_EVNMASTER._DSPFreistellu, "")

            Dim oEdit As SAPbouiCOM.EditText
            oEdit = oForm.Items.Item("IDH_UCY").Specific
            oEdit.Value = String.Empty

            oEdit = oForm.Items.Item("IDH_CYB").Specific
            oEdit.Value = String.Empty

            oEdit = oForm.Items.Item("IDH_BR").Specific
            oEdit.Value = String.Empty


            doSetFocus(oForm, "IDH_EVN")

            Dim oExclude() As String = {"IDH_REMIND", "IDH_ISPRNT", "IDH_UCY", "IDH_CYB", "IDH_BR"}

            EnableAllEditItems(oForm, oExclude)
            oForm.Update()
        End Sub

        Public Overridable Sub doCreateFindEntry(ByVal oForm As SAPbouiCOM.Form)
            setFormDFValue(oForm, "Code", "", True)
            setFormDFValue(oForm, "U_EVNNumber", "")
            setFormDFValue(oForm, "U_WasCd", "")
            setFormDFValue(oForm, "U_WasDsc", "")
            setFormDFValue(oForm, "U_EWCCd", "")
            setFormDFValue(oForm, "U_DispSiteCd", "")
            setFormDFValue(oForm, "U_DispSiteNm", "")
            setFormDFValue(oForm, "U_DSAddress", "")
            setFormDFValue(oForm, "U_DSStreet", "")
            setFormDFValue(oForm, "U_DSBlock", "")
            setFormDFValue(oForm, "U_DSCity", "")
            setFormDFValue(oForm, "U_DSPostCd", "")
            setFormDFValue(oForm, "U_Activity", "")
            setFormDFValue(oForm, "U_CreateDt", "")
            setFormDFValue(oForm, "U_StartDt", "")
            setFormDFValue(oForm, "U_Duration", "")
            setFormDFValue(oForm, "U_UOM", "")
            setFormDFValue(oForm, "U_Quantity", "")
            setFormDFValue(oForm, "U_Year1", "")
            setFormDFValue(oForm, "U_Year2", "")
            setFormDFValue(oForm, "U_Year3", "")
            setFormDFValue(oForm, "U_Year4", "")
            setFormDFValue(oForm, "U_Year5", "")
            setFormDFValue(oForm, "U_IsPrinted", "")
            setFormDFValue(oForm, "U_DSPSAddr", "")
            setFormDFValue(oForm, "U_DSPSStrt", "")
            setFormDFValue(oForm, "U_DSPSBlock", "")
            setFormDFValue(oForm, "U_DSPSCity", "")
            setFormDFValue(oForm, "U_DSPSPostCd", "")
            setFormDFValue(oForm, "U_DSPSSID", "")
            setFormDFValue(oForm, "U_CarCd", "")
            setFormDFValue(oForm, "U_CarNm", "")
            setFormDFValue(oForm, "U_CAAddr", "")
            setFormDFValue(oForm, "U_CAStrt", "")
            setFormDFValue(oForm, "U_CABlock", "")
            setFormDFValue(oForm, "U_CACity", "")
            setFormDFValue(oForm, "U_CAPostCd", "")
            setFormDFValue(oForm, "U_CASAddr", "")
            setFormDFValue(oForm, "U_CASStrt", "")
            setFormDFValue(oForm, "U_CASBlock", "")
            setFormDFValue(oForm, "U_CASCity", "")
            setFormDFValue(oForm, "U_CASPostCd", "")
            setFormDFValue(oForm, "U_CAID", "")
            setFormDFValue(oForm, "U_CusCd", "")
            setFormDFValue(oForm, "U_CusNm", "")
            setFormDFValue(oForm, "U_CusAddr", "")
            setFormDFValue(oForm, "U_CusStreet", "")
            setFormDFValue(oForm, "U_CusBlock", "")
            setFormDFValue(oForm, "U_CusCity", "")
            setFormDFValue(oForm, "U_CusPostCd", "")
            setFormDFValue(oForm, "U_CusSAddr", "")
            setFormDFValue(oForm, "U_CusSStrt", "")
            setFormDFValue(oForm, "U_CusSBlock", "")
            setFormDFValue(oForm, "U_CusSCity", "")
            setFormDFValue(oForm, "U_CusSPostCd", "")
            setFormDFValue(oForm, "U_CusID", "")
            setFormDFValue(oForm, "U_EndDt", "")
            setFormDFValue(oForm, "U_AtcEntry", "")
            setFormDFValue(oForm, "U_RelNum", "")
            setFormDFValue(oForm, "U_PreTre", "")
            setFormDFValue(oForm, "U_DecAtt", "")
            setFormDFValue(oForm, "U_SinMul", "")
            setFormDFValue(oForm, "U_EnVnSnVs", "")
            setFormDFValue(oForm, IDH_EVNMASTER._DSPFreistellu, "")

            Dim oEdit As SAPbouiCOM.EditText
            oEdit = oForm.Items.Item("IDH_UCY").Specific
            oEdit.Value = String.Empty

            oEdit = oForm.Items.Item("IDH_CYB").Specific
            oEdit.Value = String.Empty

            oEdit = oForm.Items.Item("IDH_BR").Specific
            oEdit.Value = String.Empty

            Dim oExclude() As String = {"IDH_EVN", "IDH_UCY", "IDH_CYB", "IDH_BR"}
            DisableAllEditItems(oForm, oExclude)
            oForm.Update()
        End Sub

        Protected Sub doSetMode(ByVal oForm As SAPbouiCOM.Form, ByVal iMode As SAPbouiCOM.BoFormMode)
            If iMode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                oForm.Items.Item("1").Specific.Caption = getTranslatedWord("Find")
            ElseIf iMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                oForm.Items.Item("1").Specific.Caption = getTranslatedWord("Update")
                doSetUpdate(oForm)
            ElseIf iMode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                oForm.Items.Item("1").Specific.Caption = getTranslatedWord("Ok")
            End If
        End Sub

        Private Sub doFillDuration(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDH_DUR")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = oItem.Specific
            doClearValidValues(oCombo.ValidValues)
            oCombo.ValidValues.Add("1", "1")
            oCombo.ValidValues.Add("2", "2")
            oCombo.ValidValues.Add("3", "3")
            oCombo.ValidValues.Add("4", "4")
            oCombo.ValidValues.Add("5", "5")
        End Sub


        Private Sub doFillWORsCombobox(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDH_ORDERS")
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = oItem.Specific
            doClearValidValues(oCombo.ValidValues)

            For index = Convert.ToInt32(Config.INSTANCE.doEVNWORsIncrement()) To Convert.ToInt32(Config.INSTANCE.doEVNWORsShown())
                If index = Convert.ToInt32(Config.INSTANCE.doEVNWORsShown()) Then
                    oCombo.ValidValues.Add(index, index.ToString() + " Orders")
                    Return
                Else
                    oCombo.ValidValues.Add(index, index.ToString() + " Orders")
                End If

                If Convert.ToInt32(Config.INSTANCE.doEVNWORsShown()) - index < Convert.ToInt32(Config.INSTANCE.doEVNWORsIncrement()) Then
                    index = Convert.ToInt32(Config.INSTANCE.doEVNWORsShown()) - 1
                Else
                    index = index + Convert.ToInt32(Config.INSTANCE.doEVNWORsIncrement()) - 1
                End If
            Next index
        End Sub

        Private Function doFind(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim sEVNCode As String = getItemValue(oForm, "IDH_EVN").Trim()
            If Not sEVNCode Is Nothing AndAlso sEVNCode.Length > 0 Then
                If sEVNCode.Contains("*") Then
                    setSharedData(oForm, "IDHEVNN", sEVNCode.TrimEnd("*"))
                    goParent.doOpenModalForm("IDHEVNNSR", oForm)
                    Return True
                ElseIf Not sEVNCode.Contains("*") Then
                Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    Dim sQry As String = "SELECT Top 1 Code from [@IDH_EVNMASTER] With (NOLOCK) Where U_EVNNumber = '" & sEVNCode & "'"
                oRecordSet.DoQuery(sQry)
                If oRecordSet.RecordCount > 0 Then
                        oRecordSet.MoveFirst()
                        Dim code As String = oRecordSet.Fields.Item("Code").Value
                        setFormDFValue(oForm, "Code", oRecordSet.Fields.Item("Code").Value)
                    doLoadData(oForm)
                    Return True
                    End If
                Else
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("No Records were found: " & sEVNCode, "ERUSNRFR", {sEVNCode})
                    Return False
                End If
            End If
            Return False
        End Function

        Private Sub doManageEmployeeGrids(ByVal oForm As SAPbouiCOM.Form)
            'WOR Grid 
            Dim oWORGrid As DBOGrid = getWFValue(oForm, "WORGR")
            Dim oWOR As IDH_JOBSHD = Nothing
            If oWORGrid Is Nothing Then
                oWORGrid = DBOGrid.getInstance(oForm, "WORGR")
                If oWORGrid Is Nothing Then
                    If oWOR Is Nothing Then
                        oWOR = New IDH_JOBSHD()
                    End If

                    oWORGrid = New DBOGrid(Me, oForm, "WORGR", "WORGR", oWOR)
                    oWORGrid.getSBOItem().AffectsFormMode = True
                    oWORGrid.doSetDeleteActive(True)
                Else
                    oWOR = oWORGrid.DBObject()
                End If
                setWFValue(oForm, "WORGR", oWORGrid)
            Else
                oWOR = oWORGrid.DBObject()
            End If
            oWOR.getData()

            oWORGrid.doAddListField(IDH_RCEMPLOYEE._Code, "Code", False, 0, Nothing, Nothing)
            oWORGrid.doAddListField(IDH_RCEMPLOYEE._Name, "Name", False, 0, Nothing, Nothing)
            oWORGrid.doAddListField(IDH_RCEMPLOYEE._ROUTECL, "RC Code", False, 0, Nothing, Nothing)
            oWORGrid.doAddListField(IDH_RCEMPLOYEE._RCEMP, "Employee", True, -1, "COMBOBOX", Nothing)
            oWORGrid.doAddListField(IDH_RCEMPLOYEE._RCPRIM, "Primary", True, -1, "CHECKBOX", Nothing)
            oWORGrid.doAddListField(IDH_RCEMPLOYEE._RCRTASST, "Route Assistant", True, -1, "CHECKBOX", Nothing)
            oWORGrid.doAddListField(IDH_RCEMPLOYEE._RCCLOCKIN, "Clock In", True, -1, Nothing, Nothing)
            oWORGrid.doAddListField(IDH_RCEMPLOYEE._RCCLOCKOUT, "Clock Out", True, -1, Nothing, Nothing)
            oWORGrid.doAddListField(IDH_RCEMPLOYEE._RCTHOUR1, "Clock Hours", False, -1, Nothing, Nothing)
            oWORGrid.doSynchFields()

        End Sub

        Private Sub doManageLandfillGrids(ByVal oForm As SAPbouiCOM.Form)
            'Employee Grid 
            Dim oLandFillGrid As DBOGrid = getWFValue(oForm, "LANDFGR")
            Dim oRCLandFill As IDH_RCLANDFILL = Nothing
            If oLandFillGrid Is Nothing Then
                oLandFillGrid = DBOGrid.getInstance(oForm, "LANDFGR")
                If oLandFillGrid Is Nothing Then
                    If oRCLandFill Is Nothing Then
                        oRCLandFill = New IDH_RCLANDFILL(Me, oForm)
                    End If

                    oLandFillGrid = New DBOGrid(Me, oForm, "LANDFGR", "LANDFGR", oRCLandFill)
                    oLandFillGrid.getSBOItem().AffectsFormMode = True
                    oLandFillGrid.doSetDeleteActive(True)
                Else
                    oRCLandFill = oLandFillGrid.DBObject()
                End If
                setWFValue(oForm, "LANDFGR", oLandFillGrid)
            Else
                oRCLandFill = oLandFillGrid.DBObject()
            End If
            oRCLandFill.getData()

            oLandFillGrid.doAddListField(IDH_RCLANDFILL._Code, "Code", False, 0, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._Name, "Name", False, 0, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._ROUTECL, "RC Code", False, 0, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._RCONROUTE, "On Route", True, -1, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._RCOFFROUTE, "Off Route", True, -1, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._RCLOADTP, "Load Type", True, -1, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._RCSITE, "Site", True, -1, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._RCTIMEIN, "Time In", True, -1, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._RCTIMEOUT, "Clock Hours", True, -1, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._RCTKTNO, "Ticket No.", True, -1, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._RCTONS, "Tons", True, -1, Nothing, Nothing)
            oLandFillGrid.doAddListField(IDH_RCLANDFILL._RCTCOST, "Total Cost", True, -1, Nothing, Nothing)
            oLandFillGrid.doSynchFields()

        End Sub

        Private Sub doFindAddress(ByVal oForm As SAPbouiCOM.Form)
            If Config.ParamaterWithDefault("PCODEKEY", "").Trim.Equals(String.Empty) Then
                'doResUserError
                com.idh.bridge.DataHandler.INSTANCE.doResConfigError("Address search module is not active. Please contact support.", "ERPCDNAC", {""})
                Return
            End If

            Dim oOOForm As com.uBC.forms.fr3.PostCode = New com.uBC.forms.fr3.PostCode(Nothing, oForm.UniqueID, Nothing)
            Dim sAddress As String = (getFormDFValue(oForm, "U_DSAddress").ToString() + " " +
                                 getFormDFValue(oForm, "U_DSStreet").ToString() + " " +
                                 getFormDFValue(oForm, "U_DSBlock").ToString() + " " +
                                 getFormDFValue(oForm, "U_DSCity").ToString() + " " +
                                 getFormDFValue(oForm, "U_DSPostCd").ToString()).Replace("  ", " ").Trim()

            oOOForm.setUFValue("IDH_ADDRES", sAddress)
            oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doHandlePostCodeOKReturn)
            oOOForm.doShowModal(oForm)
            setSharedData(oForm, "SETEVNIDs", "")
            setSharedData(oForm, "AddressDetail", Nothing)
        End Sub

        Private Function doHandlePostCodeOKReturn(oDialogForm As com.idh.forms.oo.Form) As Boolean
            Try

                Dim oOOForm As com.uBC.forms.fr3.PostCode = CType(oDialogForm, com.uBC.forms.fr3.PostCode)
                setSharedData(oOOForm.SBOParentForm, "SETEVNIDs", "")
                setSharedData(oOOForm.SBOParentForm, "AddressDetail", Nothing)
                Dim Company As String = oOOForm.Company
                Dim SubBuilding As String = oOOForm.SubBuilding
                Dim BuildingNumber As String = oOOForm.BuildingNumber
                Dim BuildingName As String = oOOForm.BuildingName
                Dim SecondaryStreet As String = oOOForm.SecondaryStreet
                Dim Street As String = oOOForm.Street
                Dim Block As String = oOOForm.Block
                ''''Neighbourhood
                Dim District As String = oOOForm.District
                Dim City As String = oOOForm.City
                Dim Line1 As String = oOOForm.Line1
                Dim Line2 As String = oOOForm.Line2
                Dim Line3 As String = oOOForm.Line3
                Dim Line4 As String = oOOForm.Line4
                Dim Line5 As String = oOOForm.Line5
                Dim AdminAreaName As String = oOOForm.AdminAreaName

                ''Province
                Dim ProvinceName As String = oOOForm.ProvinceName
                Dim ProvinceCode As String = oOOForm.ProvinceCode
                Dim PostalCode As String = oOOForm.PostalCode
                Dim CountryName As String = oOOForm.CountryName
                Dim CountryIso2 As String = oOOForm.CountryIso2
                Dim POBoxNumber As String = oOOForm.POBoxNumber

                setFormDFValue(oOOForm.SBOParentForm, "U_DSAddress", (Company & " " & SubBuilding & " " & BuildingNumber & " " & BuildingName).Replace("  ", " "))
                setFormDFValue(oOOForm.SBOParentForm, "U_DSStreet", Street)
                setFormDFValue(oOOForm.SBOParentForm, "U_DSBlock", Block)
                setFormDFValue(oOOForm.SBOParentForm, "U_DSCity", City)
                setFormDFValue(oOOForm.SBOParentForm, "U_DSPostCd", PostalCode)

            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", Nothing)
            End Try
            Return True
        End Function

        Protected Overridable Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_EVNDETAIL._HDRCode, "HDRCode", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_EVNDETAIL._BULAND, "Bundesland", True, 250, Nothing, Nothing)
            oGridN.doAddListField(IDH_EVNDETAIL._KREISKE, "Kreiskennung", True, 250, Nothing, Nothing)
            oGridN.doAddListField(IDH_EVNDETAIL._KREIS, "Kreis", True, 250, Nothing, Nothing)
        End Sub

        Public Sub doWORGrid(ByVal oForm As SAPbouiCOM.Form)

            Dim sEVNNumber As String
            If getFormDFValue(oForm, "U_EVNNumber").ToString() IsNot Nothing AndAlso getFormDFValue(oForm, "U_EVNNumber").ToString().Length > 0 Then
                sEVNNumber = getFormDFValue(oForm, "U_EVNNumber").ToString()
            Else
                sEVNNumber = "-1"
            End If

            doFillWORsCombobox(oForm)

            Dim oGrid As SAPbouiCOM.Grid
            oGrid = oForm.Items.Item("WORGRID").Specific

            Dim sOrders As String = ""
            If getFormDFValue(oForm, "U_Orders") = "" Or getFormDFValue(oForm, "U_Orders") = Nothing Then
                sOrders = " top " + Config.INSTANCE.doEVNWORsShown()
                setFormDFValue(oForm, "U_Orders", Config.INSTANCE.doEVNWORsShown())
            ElseIf Not getFormDFValue(oForm, "U_Orders").ToString() = "" AndAlso getFormDFValue(oForm, "U_Orders").ToString().Length > 0 Then
                sOrders = " top " + getFormDFValue(oForm, "U_Orders")
            End If

            oForm.DataSources.DataTables.Item("dtWOR").
                ExecuteQuery(
                    "select " + sOrders + " r.U_JobNr As 'WOH', r.Code as 'WOR', r.U_BDate as 'Booking Date', r.U_CustCd as 'Customer Cd', r.U_CustNm as 'Customer Nm', r.U_Tip as 'Disposal Site Cd', r.U_TipNm as 'Disposal Site Nm', " _
                    + " r.U_CarrCd as 'Carrier Cd', r.U_CarrNm as 'Carrier Nm', r.U_TRdWgt as Quantity, r.U_CarWgt as 'Carrier Qty', " _
                    + " r.U_WasCd as 'Material Cd', r.U_WasDsc as 'Material Name', r.U_ItemCd as 'Container Cd', r.U_ItemDsc as 'Container Nm' " _
                    + " from [@IDH_JOBSHD] r with (NOLOCK) where r.U_EVNNum = '" + sEVNNumber + "' AND r.U_RowSta <> 'Deleted' order by r.Code desc")
            oGrid.DataTable = oForm.DataSources.DataTables.Item("dtWOR")

            oGrid.Columns.Item(0).Editable = False
            oGrid.Columns.Item(1).Editable = False
            oGrid.Columns.Item(2).Editable = False
            oGrid.Columns.Item(3).Editable = False
            oGrid.Columns.Item(4).Editable = False
            oGrid.Columns.Item(5).Editable = False
            oGrid.Columns.Item(6).Editable = False
            oGrid.Columns.Item(7).Editable = False
            oGrid.Columns.Item(8).Editable = False
            oGrid.Columns.Item(9).Editable = False
            oGrid.Columns.Item(10).Editable = False
            oGrid.Columns.Item(11).Editable = False
            oGrid.Columns.Item(12).Editable = False
            oGrid.Columns.Item(13).Editable = False
            oGrid.Columns.Item(14).Editable = False

            oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single

            Dim oLinkCol As SAPbouiCOM.EditTextColumn
            oLinkCol = oGrid.Columns.Item("Customer Cd")
            oLinkCol.LinkedObjectType = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

            oLinkCol = oGrid.Columns.Item("Disposal Site Cd")
            oLinkCol.LinkedObjectType = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

            oLinkCol = oGrid.Columns.Item("Carrier Cd")
            oLinkCol.LinkedObjectType = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

            oLinkCol = oGrid.Columns.Item("Material Cd")
            oLinkCol.LinkedObjectType = SAPbouiCOM.BoLinkedObject.lf_Items

            oLinkCol = oGrid.Columns.Item("Container Cd")
            oLinkCol.LinkedObjectType = SAPbouiCOM.BoLinkedObject.lf_Items

            Dim oLbl As SAPbouiCOM.StaticText = oForm.Items.Item("IDH_WORCNT").Specific
            Dim oUCY As SAPbouiCOM.EditText = oForm.Items.Item("IDH_UCY").Specific
            Dim oCYB As SAPbouiCOM.EditText = oForm.Items.Item("IDH_CYB").Specific
            Dim oBR As SAPbouiCOM.EditText = oForm.Items.Item("IDH_BR").Specific

            If oGrid.DataTable.Rows.Count > 0 Then
                If oGrid.DataTable.Rows.Count = 1 Then
                    If oGrid.DataTable.GetValue(0, 0).ToString().Length > 0 Then
                        oLbl.Caption = "No. of Linked WORs: " + oGrid.DataTable.Rows.Count.ToString()
                    Else
                        oLbl.Caption = "No. of Linked WORs: 0"
                    End If
                Else
                    oLbl.Caption = "No. of Linked WORs: " + oGrid.DataTable.Rows.Count.ToString()
                End If
            Else
                oLbl.Caption = "No. of Linked WORs: 0"
            End If

            'Set summary 
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                Dim sQry As String
                sQry = "Select U_EVNNumber, UsageCurrentYear, CurrentYearBalance, OverAllSum, BalanceRemaining from IDH_VEVNMGR  with (NOLOCK) where U_EVNNumber = '" & getFormDFValue(oForm, "U_EVNNumber") & "'"
                oRecordSet = goParent.goDB.doSelectQuery(sQry)
                If Not oRecordSet Is Nothing AndAlso oRecordSet.RecordCount > 0 Then
                    oUCY.Value = oRecordSet.Fields.Item("UsageCurrentYear").Value.ToString()
                    oCYB.Value = oRecordSet.Fields.Item("CurrentYearBalance").Value.ToString()
                    oBR.Value = oRecordSet.Fields.Item("BalanceRemaining").Value.ToString()
                Else
                    oUCY.Value = ""
                    oCYB.Value = ""
                    oBR.Value = ""
                End If
            Catch ex As Exception
                Throw ex
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
            End Try

        End Sub

        Public Sub doEVNDETAILGrid(ByVal oForm As SAPbouiCOM.Form)

            Dim sEVNNumber As String
            If getFormDFValue(oForm, "U_EVNNumber").ToString() IsNot Nothing AndAlso getFormDFValue(oForm, "U_EVNNumber").ToString().Length > 0 Then
                sEVNNumber = getFormDFValue(oForm, "U_EVNNumber").ToString()
            Else
                sEVNNumber = "-1"
            End If

            Dim oGrid As SAPbouiCOM.Grid
            oGrid = oForm.Items.Item("LINESGRID").Specific

            oForm.DataSources.DataTables.Item("LINESGRID").
                ExecuteQuery(
                    "select code as 'Code',U_BULAND as 'Bundesland', U_KREISKE as 'Kreiskennung', U_KREIS as 'Kreis'  from [@IDH_EVNDETAIL]  with (NOLOCK) where code = '" + sEVNNumber + "'")
            oGrid.DataTable = oForm.DataSources.DataTables.Item("dtEVNROW")

            oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single

        End Sub

        Public Sub doEVNATTACHGrid(ByVal oForm As SAPbouiCOM.Form)

            Dim sEVNNumber As String
            If getFormDFValue(oForm, "U_EVNNumber").ToString() IsNot Nothing AndAlso getFormDFValue(oForm, "U_EVNNumber").ToString().Length > 0 Then
                sEVNNumber = getFormDFValue(oForm, "U_EVNNumber").ToString()
            Else
                sEVNNumber = "-1"
            End If

            Dim oGrid As SAPbouiCOM.Grid
            oGrid = oForm.Items.Item("ATTACGRD").Specific

            oForm.DataSources.DataTables.Add("dtEVNAttach")
            oForm.DataSources.DataTables.Item("dtEVNAttach").
                ExecuteQuery(
                    "Select AbsEntry, Line, srcPath, trgtPath, FileName, FileExt, Date from [ATC1] with (NOLOCK)")
            oGrid.DataTable = oForm.DataSources.DataTables.Item("dtEVNAttach")

            oGrid.Columns.Item(0).Editable = False
            oGrid.Columns.Item(1).Editable = True
            oGrid.Columns.Item(2).Editable = True
            oGrid.Columns.Item(3).Editable = True
            oGrid.Columns.Item(4).Editable = True
            oGrid.Columns.Item(5).Editable = True
            oGrid.Columns.Item(6).Editable = True
            oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single

        End Sub

        Public Sub doReloadWOR(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim oGrid As SAPbouiCOM.Grid
                oGrid = oForm.Items.Item("WORGRID").Specific

                oForm.DataSources.DataTables.Item("dtWOR").
                    ExecuteQuery("select ClgCode AS 'Activity Code', ReContact AS 'Date', BeginTime AS 'Time', Details AS 'Remarks', " _
                    + "Notes AS 'Notes', U_IDHWO AS 'WO Header', U_IDHWOR AS 'WO Row' from OCLG with (NOLOCK) where U_IDHWOR = '" + getFormDFValue(oForm, "Code") + "'")
                oGrid.DataTable = oForm.DataSources.DataTables.Item("dtWOR")

                'Setting Columns attributes
                oGrid.Columns.Item(0).Width = 70
                oGrid.Columns.Item(0).Editable = False
                'Add link button on Activity Code Column
                Dim oLinkCol As SAPbouiCOM.EditTextColumn
                oLinkCol = oGrid.Columns.Item("Activity Code")
                oLinkCol.LinkedObjectType = SAPbouiCOM.BoLinkedObject.lf_ContactWithCustAndVend

                oGrid.Columns.Item(1).Width = 60
                oGrid.Columns.Item(1).Editable = False

                oGrid.Columns.Item(2).Width = 60
                oGrid.Columns.Item(2).Editable = False

                oGrid.Columns.Item(3).Width = 130
                oGrid.Columns.Item(3).Editable = False

                oGrid.Columns.Item(4).Width = 230
                oGrid.Columns.Item(4).Editable = False

                oGrid.Columns.Item(5).Visible = False
                oGrid.Columns.Item(6).Visible = False
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXACT", {Nothing})
            End Try
        End Sub

        Protected Overridable Function doEVNReport(ByRef oForm As SAPbouiCOM.Form, ByRef oBtnSource As String) As Boolean
            If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update") Then
                DataHandler.INSTANCE.doErrorToStatusbar(getTranslatedWord("Can not print EVN with uncommited data."), False)
                Return False
            End If

            Dim sEVNCode = getFormDFValue(oForm, "U_EVNNumber")
            If sEVNCode = String.Empty Or sEVNCode = "99999999" Then
                DataHandler.INSTANCE.doErrorToStatusbar(getTranslatedWord("Can not print EVN with out a valid EVN Code/Number."), False)
                Return False
            End If

            'Do oGrid validation for a selected row 
            Dim oGrid As SAPbouiCOM.Grid
            oGrid = oForm.Items.Item("WORGRID").Specific
            If oGrid.Rows.SelectedRows.Count = 0 OrElse oGrid.Rows.SelectedRows.Count > 1 Then
                DataHandler.INSTANCE.doErrorToStatusbar(getTranslatedWord("Please select a single WOR to generate EVN form against the selection."), False)
                Return False
            End If

            Dim sWOR As String = oGrid.DataTable.GetValue("WOR", oGrid.Rows.SelectedRows.Item(0, SAPbouiCOM.BoOrderType.ot_SelectionOrder))

            Dim sReportFile As String = String.Empty

            If (oBtnSource = "IDH_PRINT") Then
                sReportFile = Config.Parameter("EVNRPT")
            End If

            If sReportFile.Length > 0 Then
                Dim oParams As New Hashtable
                oParams.Add("RowNum", sWOR)
                'oParams.Add("EVNCode", sEVNCode)
                If (Config.INSTANCE.useNewReportViewer()) Then
                    IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                    IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                    IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
                Else
                    IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
                End If
                Return True
            End If

            Return False
        End Function

        '##MA Start 20-02-2017
        Protected Sub doAttachFile(ByVal oForm As SAPbouiCOM.Form)
            Try

                Dim oFrmOpenFileDialog As WR1Bridge.frmOpenFileDialog = New WR1Bridge.frmOpenFileDialog()

                Dim fileName As String = oFrmOpenFileDialog.FileName
                Dim fileExt As String = oFrmOpenFileDialog.FileExt
                Dim fileDir As String = oFrmOpenFileDialog.FileDirectory
                If Not String.IsNullOrWhiteSpace(fileName) Then

                    Dim oCmpSrv As SAPbobsCOM.CompanyService = goParent.goDICompany.GetCompanyService()
                    Dim sDefaultAttachmentPath As String = oCmpSrv.GetPathAdmin().AttachmentsFolderPath

                    If sDefaultAttachmentPath.Length > 0 Then
                        'Do attachment on a static file 
                        Dim oAtt As SAPbobsCOM.Attachments2 = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oAttachments2)
                        Dim sAttatchmentEntry As String = getFormDFValue(oForm, "U_AtcEntry")
                        If sAttatchmentEntry.Length > 0 AndAlso Not sAttatchmentEntry = "0" Then
                            If (oAtt.GetByKey(Convert.ToInt16(sAttatchmentEntry))) Then
                                oAtt.Lines.Add()
                                oAtt.Lines.FileName = fileName
                                oAtt.Lines.FileExtension = fileExt
                                oAtt.Lines.SourcePath = fileDir
                                oAtt.Lines.Override = SAPbobsCOM.BoYesNoEnum.tYES
                                If oAtt.Update() = 0 Then
                                    'oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                    'load grid again to refresh results 
                                    Dim oGAttach As UpdateGrid = UpdateGrid.getInstance(oForm, "ATTACGRD")
                                    oGAttach.setRequiredFilter(" AbsEntry = " + sAttatchmentEntry + " ")
                                    oGAttach.doReloadData()
                                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                Else
                                    'catch error details 
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription())
                                End If
                            Else
                                'Attachment record not found error 
                                DataHandler.INSTANCE.doError("Error Getting Attachment Record", "Error getting Attachment Record. [" + sAttatchmentEntry + "]")
                            End If
                        Else
                            'Add new 
                            oAtt.Lines.Add()
                            oAtt.Lines.FileName = fileName
                            oAtt.Lines.FileExtension = fileExt
                            oAtt.Lines.SourcePath = fileDir
                            oAtt.Lines.Override = SAPbobsCOM.BoYesNoEnum.tYES

                            Dim iAttEntry As Int16 = -1
                            If (oAtt.Add() = 0) Then
                                iAttEntry = DataHandler.INSTANCE.SBOCompany.GetNewObjectKey()
                                'setUFValue(oForm, "IDH_ATCENT", iAttEntry)
                                setFormDFValue(oForm, "U_AtcEntry", iAttEntry)
                                'load grid again to refresh results 
                                Dim oGAttach As UpdateGrid = UpdateGrid.getInstance(oForm, "ATTACGRD")
                                oGAttach.setRequiredFilter(" AbsEntry = " + iAttEntry.ToString() + " ")
                                oGAttach.doReloadData()
                                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                            End If
                        End If
                    Else
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("doAttachFile(): SBO Company attachment path is not set. Please set under 'General Settings -->>' Path tab.")
                    End If

                End If
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doUserError("doAttachFile(): Error occured  while attaching the file.")
            End Try
        End Sub

        Protected Sub doDeleteAttachFile(ByVal oForm As SAPbouiCOM.Form, ByVal oGAttach As UpdateGrid)
            Try
                Dim oAtt As SAPbobsCOM.Attachments2 = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oAttachments2)
                Dim sAttatchmentEntry As String = getFormDFValue(oForm, "U_AtcEntry")
                If sAttatchmentEntry.Length > 0 Then
                    If (oAtt.GetByKey(Convert.ToInt16(sAttatchmentEntry))) Then
                        Dim sLineNo As String = oGAttach.doGetFieldValue("Line")
                        Dim sQry As String = "delete from ATC1 where AbsEntry = " + sAttatchmentEntry + " and Line = " + sLineNo + "; "
                        Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                        oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        oRecordSet.DoQuery(sQry)
                    Else
                        doWarnMess("System cannot find attachment record.")
                    End If
                End If
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doUserError("doAttachFile(): Error occured  while attaching the file.")
            End Try

        End Sub

        Public Sub doDisplayFile(ByVal sFileInfo As String)
            Try
                Dim oProcess As New System.Diagnostics.Process
                Dim oInfo As New System.Diagnostics.ProcessStartInfo(sFileInfo) '("C:\ISBGlobal\0 - Attachment_upload\SampleUpload1.txt")
                oInfo.UseShellExecute = True
                oInfo.WindowStyle = ProcessWindowStyle.Normal
                oProcess.StartInfo = oInfo
                oProcess.Start()
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doUserError("doDisplayFile(): Error occured while opening the file.")
            End Try
        End Sub

        Protected Overridable Sub doSetListConsoleFields(ByRef oGConsole As IDHAddOns.idh.controls.UpdateGrid)
            oGConsole.doAddListField("AbsEntry", "Abs Entry", False, 8, Nothing, Nothing)
            oGConsole.doAddListField("Line", "Line", False, 8, Nothing, Nothing)
            oGConsole.doAddListField("trgtPath", "Path", False, 8, Nothing, Nothing)
            oGConsole.doAddListField("FileName", "File Name", False, 20, Nothing, Nothing)
            oGConsole.doAddListField("FileExt", "File Extension", False, 20, Nothing, Nothing)
            oGConsole.doAddListField("Date", "Attachment Date", False, 20, Nothing, Nothing)
        End Sub

        Public Overridable Function getListConsoleRequiredStr() As String
            'Dim oForm As SAPbouiCOM.Form
            Dim sField As String = "" '" [U_AnexNo] = '' "
            Return sField
        End Function

        Private Sub doClearAll(ByVal oForm As SAPbouiCOM.Form, ByVal bDoNewCode As Boolean)

            'Using the Form's main table
            setFormDFValue(oForm, "U_CardCode", "")
            setFormDFValue(oForm, "U_CardName", "")
            setFormDFValue(oForm, "U_InvID", "")
            setFormDFValue(oForm, "U_InvDate", "")
            setFormDFValue(oForm, "U_DocTotal", "")

            Dim sCode As String = ""
            If bDoNewCode = True Then
                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(IDH_TRAIN01.AUTONUMPREFIX, "TRN1KEY")

                'Using the Form's main table
                setFormDFValue(oForm, "Code", oNumbers.CodeCode)
                setFormDFValue(oForm, "Name", oNumbers.NameCode)
            Else
                'Using the Form's main table
                setFormDFValue(oForm, "Code", sCode)
                setFormDFValue(oForm, "Name", sCode)
            End If

        End Sub


        Private Function doValidateFields(ByVal oform As SAPbouiCOM.Form) As Boolean
            Dim sError As String = ""

            Try

                Dim sCardCode As String = getFormDFValue(oform, "U_WasCd")
                If sCardCode.Length <= 0 Then
                    sError &= "Waste Code,"
                Else
                    Dim sQryCheckValidItem As String = Nothing
                    sQryCheckValidItem = "Select itemName from OITM with(NOLOCK) where itemcode = '" & sCardCode & "'"
                    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                    oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oRecordSet.DoQuery(sQryCheckValidItem)
                    If Not oRecordSet.RecordCount > 0 Then
                        sError = If(sError.Length > 0, ", - Invalid Waste Code - ", " - Invalid Waste Code - ")
                        'DataHandler.INSTANCE.doErrorToStatusbar(getTranslatedWord("Waste Code invalid."), False)
                        setFormDFValue(oform, "U_WasCd", "")
                        'setFormDFValue(oform, "U_WasDsc", "")
                    End If
                End If

                If getFormDFValue(oform, "U_WasDsc").Length <= 0 Then
                    sError &= "Waste Description,"
                Else
                    Dim sQryCheckValidItem As String = Nothing
                    sQryCheckValidItem = "Select itemName from OITM with(NOLOCK) where itemname = '" & getFormDFValue(oform, "U_WasDsc") & "'"
                    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                    oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oRecordSet.DoQuery(sQryCheckValidItem)
                    If Not oRecordSet.RecordCount > 0 Then
                        sError = If(sError.Length > 0, ", - Invalid Waste description - ", " - Invalid Waste Description - ")
                        'DataHandler.INSTANCE.doErrorToStatusbar(getTranslatedWord("Waste description invalid."), False)
                        'setFormDFValue(oform, "U_WasCd", "")
                        setFormDFValue(oform, "U_WasDsc", "")
                    End If
                End If

                If getFormDFValue(oform, "U_EVNNumber").Length <= 0 Then
                    sError &= "EVN Number,"
                End If

                If getFormDFValue(oform, "U_DispSiteCd").Length <= 0 Then
                    sError &= "Disposal Site,"
                End If

                If getFormDFValue(oform, "U_CarCd").Length <= 0 Then
                    sError &= "Carrier,"
                End If

                If getFormDFValue(oform, "U_CusCd").Length <= 0 Then
                    sError &= "Customer,"
                End If
                If getFormDFValue(oform, "U_SinMul").Length <= 0 Then
                    sError &= "EVN Single or Multiple,"
                End If


                If getFormDFValue(oform, "U_CreateDt").Length <= 0 Then
                    sError &= "Create Date,"
                End If

                If getFormDFValue(oform, "U_StartDt").Length <= 0 Then
                    sError &= "Start Date,"
                End If

                If getFormDFValue(oform, "U_EndDt").Length <= 0 Then
                    sError &= "End Date,"
                End If

                If getFormDFValue(oform, "U_UOM").Length <= 0 Then
                    sError &= "UOM,"
                End If

                If getFormDFValue(oform, "U_Duration").Length <= 0 Then
                    sError &= "Duration,"
                End If

                If getFormDFValue(oform, "U_Quantity").Length <= 0 Then
                    sError &= "Quantity,"
                End If

                If getFormDFValue(oform, "U_EnVnSnVs").Length <= 0 Then
                    sError &= "EN, VN, SN or VS"
                End If

                If getFormDFValue(oform, "U_DecAtt") = "2" Then
                    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                    oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    Dim attachmentEntry As String
                    If getFormDFValue(oform, "U_AtcEntry") = "" Then
                        attachmentEntry = 0
                    Else
                        attachmentEntry = getFormDFValue(oform, "U_AtcEntry")
                    End If
                    Dim sQry As String = "select * from ATC1 where AbsEntry = isnull(" & attachmentEntry & ",0)"
                    oRecordSet.DoQuery(sQry)
                    If oRecordSet.RecordCount > 0 Then
                    Else
                        sError &= "Attachment,"
                    End If
                End If

                If sError.Length > 0 Then
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Mandatory fields not set: The following fields must be set: " & sError, "ERUSGENS", {com.idh.bridge.Translation.getTranslatedWord(sError)})
                    Return False
                Else
                    Return True
                End If

            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBVF", {Nothing})
            End Try
            Return True
        End Function

        Private Sub doDisableFieldsForWORCreated(ByRef oform As SAPbouiCOM.Form)
            doSetEnabled(oform.Items.Item("IDH_CFLWCD"), False)
            doSetEnabled(oform.Items.Item("IDH_CFLDST"), False)
            doSetEnabled(oform.Items.Item("IDH_CFLCAR"), False)
            doSetEnabled(oform.Items.Item("IDH_CFLCUS"), False)
            doSetEnabled(oform.Items.Item("IDH_CFLADR"), False)
            doSetEnabled(oform.Items.Item("IDH_CFLDPS"), False)
            doSetEnabled(oform.Items.Item("IDH_CFLCAB"), False)
            doSetEnabled(oform.Items.Item("IDH_CFLCAS"), False)
            doSetEnabled(oform.Items.Item("IDH_CFLCUB"), False)
            doSetEnabled(oform.Items.Item("IDH_CFLCuS"), False)
            doSetEnabled(oform.Items.Item("IDH_WASCD"), False)
            doSetEnabled(oform.Items.Item("IDH_WASNM"), False)
            doSetEnabled(oform.Items.Item("IDH_DSPCD"), False)
            doSetEnabled(oform.Items.Item("IDH_DSPNM"), False)
            doSetEnabled(oform.Items.Item("IDH_CARCD"), False)
            doSetEnabled(oform.Items.Item("IDH_CARNM"), False)
            doSetEnabled(oform.Items.Item("IDH_CUSCD"), False)
            doSetEnabled(oform.Items.Item("IDH_CUSNM"), False)
            doSetEnabled(oform.Items.Item("IDH_DSPAD"), False)
            doSetEnabled(oform.Items.Item("IDH_DSPSAD"), False)
            doSetEnabled(oform.Items.Item("IDH_CARAD"), False)
            doSetEnabled(oform.Items.Item("IDH_CARSAD"), False)
            doSetEnabled(oform.Items.Item("IDH_CUSAD"), False)
            doSetEnabled(oform.Items.Item("IDH_CUSSAD"), False)
            'Dim oExclude() As String = {"IDH_EVN", "IDH_EWC", "IDH_CRDT", "IDH_STDT", "IDH_ENDDT", "IDH_QTY", "IDH_YR1", "IDH_YR2", "IDH_YR3", "IDH_YR4", "IDH_YR5", "IDH_UOM", "IDH_DUR"}
            'DisableAllEditItems(oform, oExclude)
        End Sub
        '##MA End 20-02-2017
#End Region

        Protected Overridable Function doEVNReportHeader(ByRef oForm As SAPbouiCOM.Form, ByRef oBtnSource As String) As Boolean
            If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update") Or oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Add") Then
                DataHandler.INSTANCE.doErrorToStatusbar(getTranslatedWord("Can not print EVN with uncommited data."), False)
                Return False
            End If

            Dim sEVNCode = getFormDFValue(oForm, "U_EVNNumber")
            If sEVNCode = String.Empty Or sEVNCode = Nothing Then
                DataHandler.INSTANCE.doErrorToStatusbar(getTranslatedWord("Can not print EVN with out a valid EVN Number."), False)
                Return False
            End If

            Dim sReportFile As String = String.Empty

            If (oBtnSource = "btnDecla") Then
                sReportFile = Config.Parameter("EVNFILE")
            End If

            If sReportFile.Length > 0 Then
                Dim oParams As New Hashtable
                oParams.Add("RowNum", sEVNCode)
                If (Config.INSTANCE.useNewReportViewer()) Then
                    IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                    IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                    IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
                Else
                    IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
                End If
                Return True
            End If
            Return False
        End Function

    End Class
End Namespace
