﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Namespace idh.forms
    Public Class Comment
        Inherits IDHAddOns.idh.forms.Base

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDH_COMMFRM", Nothing, -1, "Comment.srf", False, True, False, "Comment", load_Types.idh_LOAD_NORMAL)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                doAddUF(oForm, "IDH_CMMNT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 255, False, False)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub
        
        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
	    End Sub

        '*** Get the selected rows
        Protected Function doPrepareModalData(ByVal oForm As SAPbouiCOM.Form, Optional ByVal iSel As Integer = -1) As Boolean
            Dim sComment As String = CType(getUFValue(oForm, "IDH_CMMNT"), String)
        	If sComment.Length = 0 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("User: A reason must be supplied.", "A reason must be supplied.")
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("A reason must be supplied.", "ERUSREAS", {Nothing})
				Return False
        	Else
	            setParentSharedData(oForm, "CMMNT", sComment)
	            doReturnFromModalShared(oForm, True)       
	            Return True
        	End If
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If goParent.doCheckModal(oForm.UniqueID) = True Then
                    If doPrepareModalData(oForm) = False Then
                    	BubbleEvent = False
                    End If
                End If
            End If
        End Sub

        '** The ItemEvent handler
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Return True
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        End Sub

        Protected Overrides Sub doReturnCanceled(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String)
        End Sub

        Public Overrides Sub doClose()
        End Sub
    End Class
End Namespace
