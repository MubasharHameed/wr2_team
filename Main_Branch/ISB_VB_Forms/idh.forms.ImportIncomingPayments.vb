﻿
Imports com.idh.bridge

Imports com.idh.bridge.lookups

Namespace idh.forms
    Public Class ImportIncomingPayments
        Inherits IDHAddOns.idh.forms.Base
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_INCMPAY", iMenuPosition, "IncomingPayment.srf", False)
        End Sub
        Public Overrides Sub doClose()
        End Sub
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            doAddUF(oForm, "IDH_GLAC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 200, False, False)
            doAddUF(oForm, "IDH_STATUS", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 2000, False, False)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub
        Public Function doImportIncomingPaymentsEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent) As Boolean
            doImportIncomingPayments(oForm)
            Return False
        End Function
        '**
        ' Do the Incoming Payments
        '**
        Private Sub doImportIncomingPayments(ByVal oForm As SAPbouiCOM.Form)
            Dim sFileName As String = "", sSourceFolder As String = "", sArchieveFolder As String = ""
            sSourceFolder = Config.INSTANCE.getParameterWithDefault("ICPSRPTH", "")
            sArchieveFolder = Config.INSTANCE.getParameterWithDefault("ICPDSPTH", "")
            setUFValue(oForm, "IDH_STATUS", "")
            If sSourceFolder.Trim() = "" Then
                setUFValue(oForm, "IDH_STATUS", Translation.getTranslatedMessage("ERSYVIM4", "Source Directory is not setup. Please update it from WR Config") & vbCrLf)
                Return
            End If
            If sArchieveFolder.Trim() = "" Then
                setUFValue(oForm, "IDH_STATUS", Translation.getTranslatedMessage("ERSYVIM3", "Archieve Directory is not setup. Please update it from WR Config") & vbCrLf)
                Return
            End If
            Dim sGLAccount As String = getUFValue(oForm, "IDH_GLAC")
            If sGLAccount Is Nothing OrElse sGLAccount.Trim = "" Then
                setUFValue(oForm, "IDH_STATUS", Translation.getTranslatedMessage("ERSYVIM2", "Invalid GL Account. Please enter a valid GL account") & vbCrLf)
                Return
            End If
            sGLAccount = Config.GetGLAccountCode(sGLAccount)
            If sGLAccount Is Nothing OrElse sGLAccount.Trim = "" Then
                setUFValue(oForm, "IDH_STATUS", Translation.getTranslatedMessage("ERSYVIM2", "Invalid GL Account. Please enter a valid GL account") & vbCrLf)
                Return
            End If
            If Not sSourceFolder.EndsWith("\") Then
                sSourceFolder = sSourceFolder & Convert.ToString("\")
            End If
            If Not sArchieveFolder.EndsWith("\") Then
                sArchieveFolder = sArchieveFolder & Convert.ToString("\")
            End If

            '  System.Windows.Forms.OpenFileDialog oOpenFile = new System.Windows.Forms.OpenFileDialog();

            Try
                Dim files As String() = IO.Directory.GetFiles(sSourceFolder)
                If files Is Nothing OrElse files.Length = 0 Then
                    setUFValue(oForm, "IDH_STATUS", Translation.getTranslatedMessage("ERSYVIM1", "No valid MS excel file available to process") & vbCrLf)
                    'setUFValue(oForm, "IDH_STATUS", "No file available to process." & vbCrLf)
                    Return
                End If
                Dim i As Integer
                For i = 0 To files.Length - 1
                    sFileName = files(i)
                    Dim sExt As String = IO.Path.GetExtension(sFileName)
                    If (sExt.ToLower() = ".xls" OrElse sExt.ToLower() = ".xlsx") Then
                        Exit For
                    End If
                Next
                If i = files.Length Then
                    setUFValue(oForm, "IDH_STATUS", Translation.getTranslatedMessage("ERSYVIM1", "No valid MS excel file available to process") & vbCrLf)
                    Return
                End If

                Dim sBatchName As String = Now.ToString("yyyyMMddHHmmss")
                Dim oImporter As New com.idh.bridge.import.IncomingPayments()

                For i = 0 To files.Length - 1
                    Try
                        com.idh.bridge.DataHandler.INSTANCE.doInfo("Importing the incoming payment file.")
                        sFileName = files(i)
                        Dim sResult As String = ""
                        Dim iTotalRowsDone As Int32 = 0
                        Dim dblTotalInvoiceAmount As Double = 0
                        If oImporter.doReadFile(sFileName, sGLAccount.Trim, sResult, sBatchName, iTotalRowsDone, dblTotalInvoiceAmount) Then
                            com.idh.bridge.DataHandler.INSTANCE.doInfo(sFileName & " import done.")
                            sResult = IO.Path.GetFileName(sFileName) & " processed successfully. Total Rows processed: " & iTotalRowsDone.ToString() & ", Sum of amount processed for invoices: " & dblTotalInvoiceAmount.ToString("##0.00")
                            IO.File.Move(sFileName, sArchieveFolder & IO.Path.GetFileName(sFileName))
                        Else
                            sResult = "Errors found in file " & IO.Path.GetFileName(sFileName) & " are: " & vbCrLf & sResult
                        End If

                        setUFValue(oForm, "IDH_STATUS", getUFValue(oForm, "IDH_STATUS").ToString & vbCrLf & vbCrLf & sResult)

                    Catch ex As Exception
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", New String() {ex.Message})
                    Finally
                        Dim sCurrDirectory As String = IO.Directory.GetCurrentDirectory()
                        IO.Directory.SetCurrentDirectory(IDHAddOns.idh.addon.Base.CURRENTWORKDIR)
                    End Try
                Next
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", New String() {ex.Message})
                Return
            End Try

            'If True Then
            '    'sFileName = oOpenFile.FileName;
            '    Try
            '        com.idh.bridge.DataHandler.INSTANCE.doInfo("Importing the file.")

            '        Dim oImporter As New com.idh.bridge.import.Journal()
            '        If oImporter.doReadFile(sFileName) Then
            '            com.idh.bridge.DataHandler.INSTANCE.doInfo("Import done.")
            '        End If
            '    Catch ex As Exception
            '        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error importing the data.");
            '        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", New String() {String.Empty})
            '    Finally
            '        'goParent.doProgress("EXIMP", 100)

            '        Dim sCurrDirectory As String = IO.Directory.GetCurrentDirectory()
            '        IO.Directory.SetCurrentDirectory(IDHAddOns.idh.addon.Base.CURRENTWORKDIR)
            '    End Try
            'End If

        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                If pVal.ItemUID = "IDH_STRPRC" AndAlso pVal.EventType = SAPbouiCOM.BoEventTypes.et_CLICK Then
                    doImportIncomingPaymentsEvent(oForm, pVal)
                End If
            End If
            Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
        End Function

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)
        End Sub


        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
            Dim GLAct As String = Config.INSTANCE.getParameterWithDefault("GLACCNT1", "")
            setUFValue(oForm, "IDH_GLAC", GLAct)
            '   Dim GLAct As String = Config.INSTANCE.getParameterWithDefault("GLACCNT1", "")
            setUFValue(oForm, "IDH_STATUS", "")

        End Sub

        'Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
        '    MyBase.doFinalizeShow(oForm)
        'End Sub
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            '  doAddEvent(SAPbouiCOM.BoEventTypes.et_GOT_FOCUS)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub



    End Class

End Namespace
