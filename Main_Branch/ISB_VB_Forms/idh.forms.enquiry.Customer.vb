Imports System.IO
Imports System.Collections
Imports System

Namespace idh.forms.enquiry
    Public Class Customer
        Inherits idh.forms.enquiry.Templ

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
	        MyBase.New(oParent, "IDHCENQ", iMenuPosition, "Waste Customer Enquiry")
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Waste Customer Enquiry"
'        End Function

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.UpdateGrid)
            oGridN.doAddFilterField("State", "Progress", False)
            oGridN.doAddFilterField("r.U_Status", "Status", False)

            oGridN.doAddFilterField("e.U_CardCd", "Customer Code", False)
            oGridN.doAddFilterField("e.U_CardNM", "Customer Name", False)
            oGridN.doAddFilterField("e.U_ZpCd", "Post Code", False)
            oGridN.doAddFilterField("e.U_CntrNo", "Contract No.", False)

            oGridN.doAddFilterField("r.U_JobNr", "Order No.", False)
            oGridN.doAddFilterField("r.Code", "Row No.", False)
            oGridN.doAddFilterField("r.U_JobTp", "Order Type", False)
            oGridN.doAddFilterField("r.U_ASDate", "Act. Start", False)
            oGridN.doAddFilterField("r.U_AEDate", "Act EDate", False)
            oGridN.doAddFilterField("r.U_ItmGrp", "Container Grp", False)
            oGridN.doAddFilterField("r.U_ItemCd", "Container Code", False)
            oGridN.doAddFilterField("r.U_ItemDsc", "Container Desc.", False)
            oGridN.doAddFilterField("r.U_CstWgt", "Cust. Wgt", False)
            '..
            '..
            oGridN.doAddFilterField("r.U_RDate", "Req. Start", False, 0)
            oGridN.doAddFilterField("r.U_RTime", "Req. Time From", False, 0, "TIME")
            oGridN.doAddFilterField("r.U_RTimeT", "Req. Time To", False, 0, "TIME")
            oGridN.doAddFilterField("r.U_ASTime", "Act. Time", False, 0, "TIME")
            oGridN.doAddFilterField("r.U_AETime", "Act ETime", False, 0, "TIME")
            oGridN.doAddFilterField("r.U_SLicSp", "SLicSp", False, 0)
            oGridN.doAddFilterField("r.U_SLicNr", "Skip Lic. No.", False, 0)
            oGridN.doAddFilterField("r.U_SLicExp", "Skip Exp.", False, 0)
            oGridN.doAddFilterField("r.U_Lorry", "Vehicle Reg.", False, 0)
            oGridN.doAddFilterField("r.U_Driver", "Driver", False, 0)
            oGridN.doAddFilterField("r.U_Tip", "Supplier", False, 0)
            '..
            oGridN.doAddFilterField("r.U_TipWgt", "Sup. Wgt", False, 0)
            oGridN.doAddFilterField("r.U_TCharge", "TCharge", False, 0)
            oGridN.doAddFilterField("r.U_Price", "Price", False, 0)
            oGridN.doAddFilterField("r.U_Discnt", "Discnt", False, 0)
            oGridN.doAddFilterField("r.U_TaxAmt", "Sales Vat Amount", False, 0)
            oGridN.doAddFilterField("r.U_VtCostAmt", "Purchase Vat Amount", False, 0)
            oGridN.doAddFilterField("r.U_SLicCh", "SLicCh", False, 0)
            oGridN.doAddFilterField("r.U_CongCh", "CongCh", False, 0)

            oGridN.doAddFilterField("r.U_Serial", "Item Serial No.", False, 0)
            oGridN.doAddFilterField("r.U_Total", "Total", False, 0)
            oGridN.doAddFilterField("r.U_VehTyp", "Vehicle", False, 0)
            oGridN.doAddFilterField("e.U_BDate", "Booking Date", False, 0)
            oGridN.doAddFilterField("e.U_BTime", "Booking Time", False, 0)
            oGridN.doAddFilterField("r.U_TipCost", "TipCost", False, 0)
            oGridN.doAddFilterField("r.U_TipTot", "TipTot", False, 0)
            oGridN.doAddFilterField("r.U_TCTotal", "TCTotal", False, 0)
            oGridN.doAddFilterField("r.U_DocNum", "DocNum", False, 0)
            oGridN.doAddFilterField("r.U_DisAmt", "Discount Amount", False, 0)
            oGridN.doAddFilterField("r.U_SupRef", "Supplier Ref No.", False, 0)
            oGridN.doAddFilterField("r.U_WasCd", "Waste Code", False, 0)
            oGridN.doAddFilterField("r.U_WasDsc", "Waste Description", False, 0)
        End Sub

        '*** This is where the search fields are set
        Protected Overrides Sub doSetFilterLayout(ByVal oForm As SAPbouiCOM.Form)
'            Dim iCount As Integer

            oForm.Freeze(True)
            Try
                oForm.Items.Item("IDH_LE1").Visible = False
                oForm.Items.Item("IDH_LE2").Visible = False
                oForm.Items.Item("IDH_VEHREG").Visible = False
                oForm.Items.Item("IDH_DRIVER").Visible = False

                oForm.Items.Item("IDH_LA1").Left = 170
                oForm.Items.Item("IDH_LA2").Left = 170
                oForm.Items.Item("IDH_REQSTF").Left = 250
                oForm.Items.Item("IDH_ACTSTF").Left = 250

                oForm.Items.Item("IDH_LB1").Left = 340
                oForm.Items.Item("IDH_LB2").Left = 340
                oForm.Items.Item("IDH_REQSTT").Left = 385
                oForm.Items.Item("IDH_ACTSTT").Left = 385

                oForm.Items.Item("IDH_LC1").Left = 5
                oForm.Items.Item("IDH_LC2").Left = 5
                oForm.Items.Item("IDH_CUST").Left = 80
                oForm.Items.Item("IDH_NAME").Left = 80

                oForm.Items.Item("IDH_CUST").Click()
            Catch ex As Exception
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

    End Class
End Namespace
