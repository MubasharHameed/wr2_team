Imports System.IO
Imports System.Collections

Namespace idh.forms.enquiry
    Public MustInherit Class Templ
        Inherits WR1_Managers.idh.forms.manager.Templ

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sID As String, ByVal iMenuPosition As Integer, ByVal sTitle As String )
            MyBase.New(oParent, sID, "IDHRE", "Job Manager.srf", iMenuPosition, sTitle)
        End Sub

'        '*** Create Sub-Menu
'        Protected Overrides Sub doCreateSubMenu()
'            Dim oMenus As SAPbouiCOM.Menus
'            Dim oMenuItem As SAPbouiCOM.MenuItem
'            Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
'
'            oMenuItem = goParent.goApplication.Menus.Item("IDHRE")
'            oMenus = oMenuItem.SubMenus
'
'            oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
'            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING
'            oCreationPackage.UniqueID = gsType
'            oCreationPackage.String = getTitle()
'            oCreationPackage.Position = giMenuPosition
'            Try
'                oMenus.AddEx(oCreationPackage)
'            Catch ex As Exception
'            End Try
'        End Sub

    End Class
End Namespace
