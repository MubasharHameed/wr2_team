Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Namespace idh.forms
    Public Class ENGenOption
        Inherits IDHAddOns.idh.forms.Base

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHENGEN", Nothing, -1, "ENGenOption.srf", False, True, False, "Evidence Note Options", load_Types.idh_LOAD_NORMAL)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            oForm.Freeze(True)
            Try
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the Form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
            oForm.Freeze(False)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)
        End Sub

        '*** Get the selected rows
        Protected Sub doPrepareModalData(ByVal oForm As SAPbouiCOM.Form)
            'Dim sPrefix As String = getWFValue(oForm, "PREFIX")
            'Dim sMethod As String = getUFValue(oForm, "IDH_PAYMET")
            'If sPrefix Is Nothing Then
            '    sPrefix = ""
            'End If
            'setParentSharedData(oForm, "STATUS", sPrefix & com.idh.bridge.lookups.FixedValues.getStatusInvoice())
            'setParentSharedData(oForm, "METHOD", sMethod)
            'setParentSharedData(oForm, "ROWS", getWFValue(oForm, "ROWS"))
            doReturnFromModalShared(oForm, True)
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = False Then
                If goParent.doCheckModal(oForm.UniqueID) = True Then
                    doPrepareModalData(oForm)
                End If
            End If
        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = True Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                    If pVal.ItemUID = "IDH_DUPL" OrElse pVal.ItemUID = "IDH_SUBS" Then
                        doPrepareModalData(oForm)
                        'BubbleEvent = False
                    End If
                End If
            End If
            Return True
        End Function

        Public Overrides Sub doClose()
        End Sub

    End Class
End Namespace
