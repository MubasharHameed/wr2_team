Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge
Imports com.idh.dbObjects.User
Imports com.idh.dbObjects.numbers
Imports com.isb.bridge.lookups

Namespace idh.forms.scans
    Public Class Waste
        Inherits IDHAddOns.idh.forms.Base

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_WTWT", sParMenu, iMenuPosition, "Waste Transfer1.srf", False, True, False, "Waste Scanner")
            doEnableHistory("IDH_WTWT")

            'doAddImagesToFix("IDH_WTLURO")
            'doAddImagesToFix("IDH_WTLUCO")
            'doAddImagesToFix("IDH_WTLULC")
            'doAddImagesToFix("IDH_WTLULP")
            'doAddImagesToFix("IDH_WTLUPC")
            'doAddImagesToFix("IDH_WTLUTF")
            'doAddImagesToFix("IDH_WTLUUN")
            'doAddImagesToFix("IDH_WTLUWC")
        End Sub


        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                oForm.DataSources.DBDataSources.Add("@IDH_WTWT")

                doAddDF(oForm, "@IDH_WTWT", "IDH_WTWO", "U_WTWO")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTRO", "U_WTRO")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTTN", "Code")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTSD", "U_WTSD")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTRN", "U_WTRN")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTST", "U_WTST")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTOT", "U_WTOT")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTC1", "U_WTC1")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTAT", "U_WTAT")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTCO", "U_WTCO")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTCT", "U_WTCT")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTCC", "U_WTCC")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTCB", "U_WTCB")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTLT", "U_WTLT")

                doAddDF(oForm, "@IDH_WTWT", "IDH_WTLC", "U_WTLC")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTLP", "U_WTLP")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTPT", "U_WTPT")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTPC", "U_WTPC")

                doAddDF(oForm, "@IDH_WTWT", "IDH_WTWC", "U_WTWC")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTWT", "U_WTWT")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTWD", "U_WTWD")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTWN", "U_WTWN")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTTFS", "U_WTTFS")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTWU", "U_WTWU")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTWQ", "U_WTWQ")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTID", "U_WTID")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTIT", "U_WTIT")
                doAddDF(oForm, "@IDH_WTWT", "IDH_WTIC", "U_WTIC")

                Dim oLink As SAPbouiCOM.LinkedButton
                oLink = oForm.Items.Item("IDH_WTLBCO").Specific
                oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_Items

                oLink = oForm.Items.Item("IDH_WTLBWC").Specific
                oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_Items

                oForm.DataBrowser.BrowseBy = "IDH_WTTN"

                oForm.SupportedModes = -1
                oForm.AutoManaged = True

                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            doGetContainerGroups(oForm)
            doGetContainerLocTypes(oForm)
            doGetWastePositionTypes(oForm)
            doGetActionTypes(oForm)

            'oForm.DataSources.DBDataSources.Item("@IDH_WTWT").Query()

            If getHasSharedData(oForm) Then
                Dim sCode As String = getParentSharedData(oForm, "SCANCODE")

                Dim oCons As New SAPbouiCOM.Conditions
                Dim oCon As SAPbouiCOM.Condition

                oCon = oCons.Add
                oCon.Alias = "Code"
                oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                oCon.CondVal = sCode

                oForm.DataSources.DBDataSources.Item("@IDH_WTWT").Query(oCons)

                oForm.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE
                'oForm.AutoManaged = False
                'DisableAllEditItems(oForm)
            End If
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        'Get the Container Groups
        Public Sub doGetContainerGroups(ByVal oForm As SAPbouiCOM.Form)
            'GET THE ITEM GROUPS
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Dim oItem As SAPbouiCOM.Item
            Try
                Dim oItmGrp As SAPbouiCOM.ComboBox
                '...                Dim sQry As String = "select DISTINCT ItmsGrpCod, ItmsGrpNam from OITB g, [@IDH_JOBTYPE] jt where g.ItmsGrpNam=jt.U_ItemGrp"
                Dim sQry As String = "select DISTINCT ItmsGrpCod, ItmsGrpNam from OITB g With(nolock), [@IDH_JOBTYPE] jt  With(nolock) where g.ItmsGrpCod=jt.U_ItemGrp"
                oRecordSet = goParent.goDB.doSelectQuery(sQry)
                If oRecordSet.RecordCount > 0 Then
                    oItem = oForm.Items.Item("IDH_WTCT")
                    oItem.DisplayDesc = True
                    oItmGrp = oItem.Specific
                    Dim iCount As Integer
                    Dim oVal1 As String
                    Dim oVal2 As String

                    doClearValidValues(oItmGrp.ValidValues)

                    For iCount = 0 To oRecordSet.RecordCount - 1
                        oVal1 = oRecordSet.Fields.Item(0).Value
                        oVal2 = oRecordSet.Fields.Item(1).Value
                        Try
                            oItmGrp.ValidValues.Add(oVal1, oVal2)
                        Catch ex As Exception
                        End Try
                        oRecordSet.MoveNext()
                    Next
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error retrieving the Container Groups.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Container Groups")})
            End Try
            IDHAddOns.idh.data.Base.doReleaseObject(Nothing)
        End Sub

        'Get the Container Location Types
        Public Sub doGetContainerLocTypes(ByVal oForm As SAPbouiCOM.Form)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Dim oItem As SAPbouiCOM.Item
            Try
                Dim oItmGrp As SAPbouiCOM.ComboBox
                Dim sQry As String = "select DISTINCT Code, U_WTTD from [@IDH_WTLOTP]"
                oRecordSet = goParent.goDB.doSelectQuery(sQry)
                If oRecordSet.RecordCount > 0 Then
                    oItem = oForm.Items.Item("IDH_WTLT")
                    oItem.DisplayDesc = True
                    oItmGrp = oItem.Specific
                    Dim iCount As Integer
                    Dim oVal1 As String
                    Dim oVal2 As String

                    doClearValidValues(oItmGrp.ValidValues)

                    For iCount = 0 To oRecordSet.RecordCount - 1
                        oVal1 = oRecordSet.Fields.Item(0).Value
                        oVal2 = oRecordSet.Fields.Item(1).Value
                        Try
                            oItmGrp.ValidValues.Add(oVal1, oVal2)
                        Catch ex As Exception
                        End Try
                        oRecordSet.MoveNext()
                    Next
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error retrieving the Container Location Types.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Container Location Types")})
            End Try
            IDHAddOns.idh.data.Base.doReleaseObject(Nothing)
        End Sub

        'Get the Waste Position
        Public Sub doGetWastePositionTypes(ByVal oForm As SAPbouiCOM.Form)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Dim oItem As SAPbouiCOM.Item
            Try
                Dim oItmGrp As SAPbouiCOM.ComboBox
                Dim sQry As String = "select DISTINCT Code, U_WTTD from [@IDH_WTPOTP]"
                oRecordSet = goParent.goDB.doSelectQuery(sQry)
                If oRecordSet.RecordCount > 0 Then
                    oItem = oForm.Items.Item("IDH_WTPT")
                    oItem.DisplayDesc = True
                    oItmGrp = oItem.Specific
                    Dim iCount As Integer
                    Dim oVal1 As String
                    Dim oVal2 As String

                    doClearValidValues(oItmGrp.ValidValues)

                    For iCount = 0 To oRecordSet.RecordCount - 1
                        oVal1 = oRecordSet.Fields.Item(0).Value
                        oVal2 = oRecordSet.Fields.Item(1).Value
                        Try
                            oItmGrp.ValidValues.Add(oVal1, oVal2)
                        Catch ex As Exception
                        End Try
                        oRecordSet.MoveNext()
                    Next
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error retrieving the Waste Position Types.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Waste Posistion Types")})
            End Try
            IDHAddOns.idh.data.Base.doReleaseObject(Nothing)
        End Sub

        'Get the Action Type
        Public Sub doGetActionTypes(ByVal oForm As SAPbouiCOM.Form)
            Dim oRecordSet As SAPbobsCOM.Recordset
            Dim oItem As SAPbouiCOM.Item
            Try
                Dim oItmGrp As SAPbouiCOM.ComboBox
                Dim sQry As String = "select DISTINCT Code, U_WTAT from [@IDH_WTWF]"
                oRecordSet = goParent.goDB.doSelectQuery(sQry)
                If oRecordSet.RecordCount > 0 Then
                    oItem = oForm.Items.Item("IDH_WTAT")
                    oItem.DisplayDesc = True
                    oItmGrp = oItem.Specific
                    Dim iCount As Integer
                    Dim oVal1 As String
                    Dim oVal2 As String

                    doClearValidValues(oItmGrp.ValidValues)

                    For iCount = 0 To oRecordSet.RecordCount - 1
                        oVal1 = oRecordSet.Fields.Item(0).Value
                        oVal2 = oRecordSet.Fields.Item(1).Value
                        Try
                            oItmGrp.ValidValues.Add(oVal1, oVal2)
                        Catch ex As Exception
                        End Try
                        oRecordSet.MoveNext()
                    Next
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error retrieving the Action Types.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Action Types")})
            End Try
            IDHAddOns.idh.data.Base.doReleaseObject(Nothing)
        End Sub

        '** Do the final form steps to show after loaddata
        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            oForm.Visible = True
        End Sub

        '*** Get the selected rows
        Private Sub doPrepareModalData(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse
                   oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    doUpdate(oForm)

                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                    BubbleEvent = False
                Else
                    If goParent.doCheckModal(oForm.UniqueID) = True Then
                        doPrepareModalData(oForm)
                        BubbleEvent = False
                    End If
                End If
            End If
        End Sub

        Public Overrides Sub doButtonID2(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            doReturnFromCanceled(oForm, BubbleEvent)
            BubbleEvent = False
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_WTLURO" Then
                        'IDHLNKWO
                        goParent.doOpenModalForm("IDHLNKWO", oForm)
                    ElseIf pVal.ItemUID = "IDH_WTLBRO" Then
                        Dim oData As New ArrayList
                        oData.Add("DoUpdate")
                        oData.Add(getDFValue(oForm, "@IDH_WTWT", "U_WTWO"))
                        goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)
                    ElseIf pVal.ItemUID = "IDH_WTLUCO" Then
                        Dim sGrp As String = getDFValue(oForm, "@IDH_WTWT", "U_WTCT")
                        Dim sItem As String = getDFValue(oForm, "@IDH_WTWT", "U_WTCC")

                        Dim oData As New ArrayList
                        oData.Add("PROD")
                        oData.Add(sItem)
                        oData.Add(sGrp)
                        oData.Add("")
                        goParent.doOpenModalForm("IDHISRCH", oForm, oData)
                    ElseIf pVal.ItemUID = "IDH_WTLULC" Then
                    ElseIf pVal.ItemUID = "IDH_WTLULP" Then
                    ElseIf pVal.ItemUID = "IDH_WTLUPC" Then
                    ElseIf pVal.ItemUID = "IDH_WTLUWC" Then
                        Dim sGrp As String = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup()
                        Dim sItem As String = getDFValue(oForm, "@IDH_WTWT", "U_WTWC")

                        doClearSharedData(oForm)
                        setSharedData(oForm, "TP", "WAST")
                        setSharedData(oForm, "IDH_ITMCOD", sItem)
                        setSharedData(oForm, "IDH_GRPCOD", sGrp)
                        setSharedData(oForm, "IDH_CRDCD", "")
                        setSharedData(oForm, "IDH_ITMNAM", "")
                        setSharedData(oForm, "IDH_INVENT", "Y")
                        goParent.doOpenModalForm("IDHWISRC", oForm)
                    End If
                ElseIf pVal.ItemUID = "IDH_WTLUUN" Then
                End If
            End If
            Return True
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            MyBase.doHandleModalResultShared(oForm, sModalFormType, sLastButton)

            Try
                If sModalFormType = "IDHLNKWO" Then
                    setDFValue(oForm, "@IDH_WTWT", "U_WTWO", getSharedData(oForm, "ORDNO"))
                    setDFValue(oForm, "@IDH_WTWT", "U_WTRO", getSharedData(oForm, "ROWNO"))

                    setDFValue(oForm, "@IDH_WTWT", "U_WTOT", getSharedData(oForm, "JOBTP"))
                    setDFValue(oForm, "@IDH_WTWT", "U_WTCT", getSharedData(oForm, "CONTP"))
                    setDFValue(oForm, "@IDH_WTWT", "U_WTCC", getSharedData(oForm, "CONCD"))
                    setDFValue(oForm, "@IDH_WTWT", "U_WTWC", getSharedData(oForm, "WASTCD"))
                    setDFValue(oForm, "@IDH_WTWT", "U_WTWD", getSharedData(oForm, "WASTDE"))
                    setDFValue(oForm, "@IDH_WTWT", "U_WTWQ", getSharedData(oForm, "QTY"))
                ElseIf sModalFormType = "IDHWISRC" Then
                    setDFValue(oForm, "@IDH_WTWT", "U_WTWC", getSharedData(oForm, "ITEMCODE"), False, True)
                    setDFValue(oForm, "@IDH_WTWT", "U_WTWD", getSharedData(oForm, "ITEMNAME"))
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error handeling the Modal results - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub

        Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            If oData Is Nothing Then Exit Sub
            Dim oArr As ArrayList = oData
            If oArr.Count = 0 Then
                Return
            End If
            If sModalFormType = "IDHISRCH" Then
                If oArr.Item(0) = "PROD" Then
                    setDFValue(oForm, "@IDH_WTWT", "U_WTCC", oArr.Item(1), False, True)
                    'setDFValue(oForm, "@IDH_WTWT", "U_ItemDsc", oArr.Item(2))
                End If
            End If
        End Sub

        Private Function doUpdate(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim iwResult As Integer = 0
            Dim swResult As String = Nothing

            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_WTWT")
            Try
                With oForm.DataSources.DBDataSources.Item("@IDH_WTWT")
                    Dim sCode As String = .GetValue("Code", .Offset).Trim()
                    If oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) = False Then
                        oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
                    End If

                    oAuditObj.setName(sCode)

                    Dim ii As Integer
                    Dim sField As String
                    '                    Dim oField As SAPbobsCOM.Field
                    Dim oVal As Object
                    For ii = 0 To .Fields.Count - 1
                        Try
                            sField = .Fields.Item(ii).Name
                            oVal = .GetValue(ii, .Offset)
                            If oVal.GetType Is GetType(String) AndAlso Not oVal Is Nothing Then
                                oVal = oVal.Trim()
                            End If

                            If sField.Equals("Code") Then
                            ElseIf sField.Equals("Name") Then
                                If oAuditObj.getAction() = IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD AndAlso Not (oVal Is Nothing) Then
                                    oAuditObj.setName(oVal)
                                End If
                            Else
                                oAuditObj.setFieldValue(sField, oVal)
                            End If
                        Catch ex As Exception
                        End Try
                    Next

                    iwResult = oAuditObj.Commit()

                    If iwResult <> 0 Then
                        goParent.goDICompany.GetLastError(iwResult, swResult)
                        'com.idh.bridge.DataHandler.INSTANCE.doError("System: Error Adding - " + swResult, "Error updating the Scan.")
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Adding - " + swResult, "ERSYEUAS", {swResult})
                        Return False
                    End If
                End With
                Return True
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error updating the Scan.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXAUSC", {Nothing})
                Return False
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj)
            End Try
        End Function

        '** The Menu Event handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                If pVal.MenuUID = Config.NAV_ADD Then
                    oForm.Freeze(True)
                    Try
                        doCreateNewEntry(oForm)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Menu event - " & oForm.UniqueID & "." & pVal.MenuUID)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try
                ElseIf pVal.MenuUID = Config.NAV_FIND OrElse
                       pVal.MenuUID = Config.NAV_FIRST OrElse
                       pVal.MenuUID = Config.NAV_LAST OrElse
                       pVal.MenuUID = Config.NAV_NEXT OrElse
                       pVal.MenuUID = Config.NAV_PREV Then
                    oForm.Freeze(True)
                    Try
                        If pVal.MenuUID = Config.NAV_FIND Then
                            doClearAll(oForm)
                        Else
                            doLoadData(oForm)
                        End If
                    Catch ex As Exception
                    Finally
                        oForm.Freeze(False)
                    End Try
                End If
            End If
            Return True
        End Function

        Private Sub doClearAll(ByVal oForm As SAPbouiCOM.Form)
            With oForm.DataSources.DBDataSources.Item("@IDH_WTWT")
                .InsertRecord(.Offset)

                .SetValue("Code", .Offset, "")
                .SetValue("Name", .Offset, "")
                .SetValue("U_WTWO", .Offset, "")
                .SetValue("U_WTRO", .Offset, "")
                .SetValue("U_WTSD", .Offset, "")
                .SetValue("U_WTRN", .Offset, "")
                .SetValue("U_WTST", .Offset, "")
                .SetValue("U_WTOT", .Offset, "")
                .SetValue("U_WTC1", .Offset, "")
                .SetValue("U_WTAT", .Offset, "")
                .SetValue("U_WTCO", .Offset, "")
                .SetValue("U_WTCT", .Offset, "")
                .SetValue("U_WTCC", .Offset, "")
                .SetValue("U_WTCB", .Offset, "")
                .SetValue("U_WTLT", .Offset, "")
                .SetValue("U_WTLC", .Offset, "")
                .SetValue("U_WTLP", .Offset, "")
                .SetValue("U_WTPT", .Offset, "")
                .SetValue("U_WTPC", .Offset, "")
                .SetValue("U_WTWC", .Offset, "")
                .SetValue("U_WTWT", .Offset, "")
                .SetValue("U_WTWD", .Offset, "")
                .SetValue("U_WTWN", .Offset, "")
                .SetValue("U_WTTFS", .Offset, "")
                .SetValue("U_WTWU", .Offset, "")
                .SetValue("U_WTWQ", .Offset, "")
                .SetValue("U_WTID", .Offset, "")
                .SetValue("U_WTIT", .Offset, "")
                .SetValue("U_WTIC", .Offset, "")

            End With
        End Sub

        '** Create a new Entry if the Add option is selected
        Private Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form)
            With oForm.DataSources.DBDataSources.Item("@IDH_WTWT")
                .InsertRecord(.Offset)

                'Dim iCode As Integer = goParent.goDB.doNextNumber("TRATRA")
                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, "TRATRA")

                .SetValue("Code", .Offset, oNumbers.CodeCode)
                .SetValue("Name", .Offset, oNumbers.NameCode)
                .SetValue("U_WTWO", .Offset, "")
                .SetValue("U_WTRO", .Offset, "")
                .SetValue("U_WTSD", .Offset, goParent.doDateToStr())
                .SetValue("U_WTRN", .Offset, "")
                .SetValue("U_WTST", .Offset, com.idh.utils.Dates.doTimeToNumStr())
                .SetValue("U_WTOT", .Offset, "")
                .SetValue("U_WTC1", .Offset, "")
                .SetValue("U_WTAT", .Offset, "")
                .SetValue("U_WTCO", .Offset, "")
                .SetValue("U_WTCT", .Offset, "")
                .SetValue("U_WTCC", .Offset, "")
                .SetValue("U_WTCB", .Offset, "")
                .SetValue("U_WTLT", .Offset, "")
                .SetValue("U_WTLC", .Offset, "")
                .SetValue("U_WTLP", .Offset, "")
                .SetValue("U_WTPT", .Offset, "")
                .SetValue("U_WTPC", .Offset, "")
                .SetValue("U_WTWC", .Offset, "")
                .SetValue("U_WTWT", .Offset, "")
                .SetValue("U_WTWD", .Offset, "")
                .SetValue("U_WTWN", .Offset, "")
                .SetValue("U_WTTFS", .Offset, "")
                .SetValue("U_WTWU", .Offset, "")
                .SetValue("U_WTWQ", .Offset, "")
                .SetValue("U_WTID", .Offset, "")
                .SetValue("U_WTIT", .Offset, "")
                .SetValue("U_WTIC", .Offset, "")
            End With
        End Sub


        Public Overrides Sub doClose()
        End Sub

    End Class
End Namespace
