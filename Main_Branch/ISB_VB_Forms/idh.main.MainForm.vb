Imports System.IO
Imports System.Collections
Imports com.idh.bridge
Imports System

Imports com.idh.bridge.lookups
Imports SAPbobsCOM
Imports com.idh.dbObjects.User
Imports com.idh.dbObjects.numbers '

Namespace idh.main
    Public Class MainForm
        Inherits com.isb.core.addon.FormLoader

        Private maFields As ArrayList

        Private mhRemoteCompanies As Hashtable = Nothing
        Private mhRemoteSettings As Hashtable = Nothing

        Private Shared gbIsTest As Boolean = False

        Public Sub New(ByVal sDesc As String, Optional ByVal iDebugLevel As Integer = 0)
            MyBase.New(sDesc, iDebugLevel, 0)
            'MyBase.New( _
            '    ReleaseInfo.AppName, _
            '    ReleaseInfo.ISV, _
            '    sDesc, _
            '    ReleaseInfo.SBOVersion, _
            '    ReleaseInfo.BaseVersion, _
            '    ReleaseInfo.LicSupport, _
            '    ReleaseInfo.DebugLevel, _
            '    ReleaseInfo.ConfigTable, _
            '    ReleaseInfo.DCode, _
            '    0, "srf", "images", "reports") ', "9PL5") '"9PL5") '"8.81-06") "2007.1"
        End Sub

        Protected Overrides Function doSetupDB() As Boolean
            '    'NEWCORE-TEMP-BLOCK--> com.isb.forms.Enquiry.admin.DataStructures.FORCECREATE = False

            '    goDB = New WR1_Data.idh.data.DataStructures(Me, "AW")
            '    If goDB.doSetupDB() Then
            '        'NEWCORE-TEMP-BLOCK--> If gbReleaseDifference = False AndAlso com.isb.forms.Enquiry.admin.DataStructures.FORCECREATE Then
            '        'NEWCORE-TEMP-BLOCK--> com.isb.forms.Enquiry.admin.DataStructures.doSetupUserDefined(DataHandler.INSTANCE.CurrentReleaseCount, goDB)
            '        'NEWCORE-TEMP-BLOCK--> End If
            '        Return True
            '    End If
            '    Return False

            If MyBase.doSetupDB() Then
                'TO BE MOVED
                'com.isb.forms.Enquiry.admin.DataStructures.FORCECREATE = False
                'If gbReleaseDifference = False AndAlso com.isb.forms.Enquiry.admin.DataStructures.FORCECREATE Then
                '    com.isb.forms.Enquiry.admin.DataStructures.doSetupUserDefined(DataHandler.INSTANCE.CurrentReleaseCount, goDB)
                'End If
                Return True
            Else
                Return False
            End If
        End Function

        '*** Instanciate the lookup Object
        Public Overrides Sub doCreateLookup()
            If Config.INSTANCE Is Nothing Then
                com.isb.core.controller.Loader.getConfigInstance("@" & goConfigTable)

                ''Dim oLookup As idh.const.Lookup = New idh.const.Lookup(goConfigTable)
                Config.INSTANCE.doGetUserRole()
            End If
        End Sub

        Protected Overrides Sub doRegisterForms()
            MyBase.doRegisterForms()

            ''TO BE MOVED
            ''If (Config.ParameterWithDefault("ENQMOD", "FALSE") = "ENQMOD_TRUE") Then
            ''com.isb.forms.Enquiry.admin.MainForm.doAddForms(Me)
            ''End If
            'If Config.ParameterAsBool("OSMUSCB", False) Then
            '    com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.admin.OSM), "IDHADM", 67)
            '    com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.FieldChangedView), "IDHADM", 68)
            'End If
        End Sub

        Protected Overrides Sub doCreateExtraMenu()
            MyBase.doCreateExtraMenu()
            'TO BE MOVED
            'If (Config.ParameterWithDefault("ENQMOD", "FALSE") = "ENQMOD_TRUE") Then
            'com.isb.forms.Enquiry.admin.MainForm.doCreateMenu(goMenuID)
            'End If
        End Sub

        ''*** Setup the Resources
        'Protected Overrides Sub doResources()
        '    ''Resources
        '    goMessages = New com.idh.bridge.res.Messages(APPLICATION)
        '    goMessages.doTranslateAll()
        '    DirectCast(goDB, WR1_Data.idh.data.DataStructures).doUpdateUFDescriptions()
        'End Sub

        ''*** Returns the lookup object
        ''Public Overloads Function getLookUp() As WR1_Forms.idh.const.Lookup
        ''    Return goLookup
        ''End Function

        ''*** Returns the message object
        'Public Overloads Function getMessages() As com.idh.bridge.resources.Messages
        '    Return goMessages
        'End Function

        'Protected Overrides Sub doInit()
        '    ''Now clear the remote connections
        '    DirectCast(Me, idh.main.MainForm).doReleaseRemoteCompanies(Nothing, True)
        '    doCloseFloatingForms()
        '    doCreateMenu("IDHISB", goAddOnDesc, "menu1.png")
        '    doCreateExtraMenu()
        '    doRegisterForms()
        '    doCreateDynamicReports(86)

        '    'com.uBC.forms.PopUps.doRegisterHandlerPopUps()
        'End Sub

        'Protected Overridable Sub doRegisterForms()
        '    'Framework Forms
        '    doAddForm(New IDHAddOns.idh.report.GenViewer(Me, -1))
        '    doAddForm(New IDHAddOns.idh.report.HTMLViewer(Me, "IDHRE", 0))

        '    ''com.idh.bridge.DataHandler.INSTANCE.doError("Info: opening WR1", "Info: opening WR1")

        '    'doAddForm(New IDHAddOns.idh.report.GenViewer(Me, -1))
        '    'doAddForm(New IDHAddOns.idh.report.HTMLViewer(Me, "IDHRE", 0))

        '    ''SEARCHES
        '    'doAddForm(New WR1_Search.idh.forms.search.BPSearch(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.DisposalSite(Me))
        '    ''## Start 17-06-2013
        '    'doAddForm(New WR1_Search.idh.forms.search.BPSearch2(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.AddressSearch3(Me))
        '    ''doAddForm(New idh.forms.RptViewr2(Me, -1))
        '    ''## End

        '    'doAddForm(New WR1_Search.idh.forms.search.BPSearchAlternate(Me))

        '    'doAddForm(New WR1_Search.idh.forms.search.AddressSearch(Me))
        '    'If WR1_Forms.Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
        '    '    doAddForm(New WR1_Search.idh.forms.search.WasteItemSearch(Me))
        '    'Else
        '    '    doAddForm(New WR1_Search.idh.forms.search.WasteItemSearch2(Me))
        '    'End If
        '    'doAddForm(New WR1_Search.idh.forms.search.AlternateWasteItemSearch(Me))

        '    'doAddForm(New WR1_Search.idh.forms.search.Route(Me))
        '    'doAddForm(New WR1_Forms.idh.forms.OSMEdit(Me))
        '    'doAddForm(New WR1_Forms.idh.forms.Comment(Me))
        '    ''OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
        '    'doAddForm(New WR1_Forms.idh.forms.ChooseDay(Me))
        '    ''End OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module

        '    ''BP Additional Charges Formula Editor 
        '    'doAddForm(New WR1_Forms.idh.forms.FormulaEditor(Me))

        '    'doAddForm(New WR1_Search.idh.forms.search.LinkWO(Me))
        '    'If WR1_Forms.Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
        '    '    doAddForm(New WR1_Search.idh.forms.search.ItemSearch(Me))
        '    'Else
        '    '    doAddForm(New WR1_Search.idh.forms.search.ItemSearch2(Me))
        '    'End If
        '    ''doAddForm(New WR1_Forms.idh.forms.RptViewr2(Me, -1))

        '    ''doAddForm(New WR1_Search.idh.forms.search.Lorry(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.Lorry(Me))

        '    'doAddForm(New WR1_Search.idh.forms.search.JobType(Me))
        '    'doAddForm(New WR1_Forms.idh.forms.PayMethod(Me))
        '    'doAddForm(New WR1_Forms.idh.forms.CCPay(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.Driver(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.GlAccnt(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.Project(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.ProfitCentre(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.UN(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.EWC(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.HAZ(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.DRCodes(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.Origin(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.Forecast(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.MAddressSearch(Me))
        '    ''doAddForm(New SRCH.idh.forms.search.Subby(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.CustRef(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.Zones(Me))
        '    'doAddForm(New WR1_Admin.idh.forms.lists.Container(Me))

        '    'doAddForm(New WR1_Forms.idh.forms.ENGenOption(Me))

        '    ''OPTIONS
        '    'doAddForm(New WR1_Forms.idh.forms.options.Worksheet(Me))

        '    ''OnTime [#Ico00036727] Transfrontier Shipment Module 
        '    'doAddForm(New WR1_Search.idh.forms.search.TFSNumbers(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.TFSAnnex(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.BPContact(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.HCode(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.YCode(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.TFSCompAuthority(Me))
        '    ''doAddForm(New idh.forms.TFSNewNo(Me, "IDHMAST", 43))
        '    'doAddForm(New WR1_TFS.idh.forms.TFS.PhysicalCharacteristics(Me))
        '    'doAddForm(New WR1_TFS.idh.forms.TFS.PackagingType(Me))
        '    'doAddForm(New WR1_TFS.idh.forms.TFS.TransportationMode(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.BASEL(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.OECD(Me))

        '    ''OnTime [#Ico000????] USA 
        '    ''If Config.INSTANCE.getParameterAsBool("USAREL", False) = True Then
        '    'doAddForm(New WR1_Search.idh.forms.search.ShipName(Me))
        '    ''If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
        '    'doAddForm(New WR1_Search.idh.forms.search.WasteProfile(Me))
        '    ''Else
        '    ''   doAddForm(New SRCH.idh.forms.search.WasteProfile2(Me))
        '    ''End If
        '    'doAddForm(New WR1_Search.idh.forms.search.WPTemplateItemSearch(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.CAS(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.WPWasteItemSearch(Me))
        '    'doAddForm(New WR1_Forms.idh.forms.ShipNameGen(Me, "IDHADM", 61))
        '    'doAddForm(New WR1_Forms.idh.forms.DuplicateOpts(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.EmployeeSearch(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.AddressSearch2(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.USACounty(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.SalesContact(Me))
        '    ''#Changes As On 20120712#
        '    'doAddForm(New WR1_Search.idh.forms.search.EPA(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.ProperShipNm(Me))

        '    ''Draft Document List 
        '    'doAddForm(New WR1_Search.idh.forms.search.DraftSearch(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.DraftSearchSI(Me))
        '    'doAddForm(New WR1_Search.idh.forms.search.Warehouse(Me))

        '    ''EVN Numbers Search 
        '    'doAddForm(New WR1_Search.idh.forms.search.EVNNumbers(Me))

        '    'com.idh.forms.oo.Form.doRegisterForm(GetType(WR1_FR2Forms.idh.forms.fr2.JJKNumber))
        '    ''End If

        '    ''PopUps
        '    'com.idh.forms.oo.Form.doRegisterForm(GetType(WR1_FR2Forms.idh.forms.fr2.popup.AdditionalMarkDocs))
        '    'com.idh.forms.oo.Form.doRegisterForm(GetType(WR1_FR2Forms.idh.forms.fr2.prices.ItemProfileCIPSIP))

        '    'doAddForm(New idh.forms.Attachment(Me))

        '    'Dim sExtra As String = goDB.Data().Extra


        '    ''TEST MENU's

        '    'If gbIsTest Then
        '    '    doAddForm(New WR1_SBOForms.idh.forms.SBO.BusinessPartner(Me))
        '    '    'OnTime 32355: WR1_NewDev_Buying&Trading_Add functionality to log Activity with a WOR
        '    '    'Register Activity wrapper class
        '    '    doAddForm(New WR1_SBOForms.idh.forms.SBO.Activity(Me))
        '    '    'doAddForm(New idh.forms.ProductSearch(Me))
        '    '    doAddForm(New WR1_Forms.idh.forms.SerialNrSearch(Me))
        '    '    'doAddForm(New idh.forms.ItemSearch(Me))
        '    '    'doAddForm(New idh.forms.JobTypeSearch(Me))
        '    '    'doAddForm(New idh.forms.RouteSearch(Me))
        '    '    doAddForm(New WR1_Forms.idh.forms.Map(Me))

        '    '    doAddForm(New WR1_Forms.idh.forms.orders.Disposal(Me, 33))
        '    '    doAddForm(New WR1_Forms.idh.forms.OrderRow(Me, 34))
        '    '    doAddForm(New WR1_Forms.idh.forms.orders.Waste(Me, 32))
        '    '    'If Config.INSTANCE.getParameterAsBool(Me, "WASORDC", True) Then
        '    '    'doAddForm(New idh.forms.orders.Waste_comb(Me, 33))
        '    '    'End If
        '    'Else
        '    '    'GENERAL - SUBS
        '    '    If goDB.Data().Extra.Equals("ECO") Then

        '    '        doAddForm(New WR1_Forms.idh.forms.SerialNrSearch(Me))
        '    '        'doAddForm(New idh.forms.ItemSearch(Me))
        '    '        'doAddForm(New SRCH.idh.forms.search.Lorry(Me)) 
        '    '        'oAddForm(New idh.forms.JobTypeSearch(Me))
        '    '    Else
        '    '        doAddForm(New WR1_SBOForms.idh.forms.SBO.BusinessPartner(Me))
        '    '        'OnTime 32355: WR1_NewDev_Buying&Trading_Add functionality to log Activity with a WOR
        '    '        'Register Activity wrapper class
        '    '        doAddForm(New WR1_SBOForms.idh.forms.SBO.Activity(Me))

        '    '        'doAddForm(New idh.forms.ProductSearch(Me))
        '    '        doAddForm(New WR1_Forms.idh.forms.SerialNrSearch(Me))
        '    '        'doAddForm(New idh.forms.ItemSearch(Me))
        '    '        'doAddForm(New idh.forms.LorrySearch(Me))
        '    '        'doAddForm(New SRCH.idh.forms.search.Lorry(Me))
        '    '        'doAddForm(New idh.forms.JobTypeSearch(Me))
        '    '        'doAddForm(New idh.forms.RouteSearch(Me))
        '    '        doAddForm(New WR1_Forms.idh.forms.Map(Me))
        '    '        doAddForm(New WR1_SBOForms.idh.forms.SBO.ServiceCall(Me))

        '    '        doAddForm(New WR1_SBOForms.idh.forms.SBO.Messages(Me))
        '    '        doAddForm(New WR1_SBOForms.idh.forms.SBO.ApprovalRequest(Me))
        '    '        doAddForm(New WR1_SBOForms.idh.forms.SBO.ApprovalDecisionReport(Me))

        '    '        'Add Goods Receipt/Issue SBO Forms 
        '    '        doAddForm(New WR1_SBOForms.idh.forms.SBO.GoodsReceipt(Me))
        '    '        doAddForm(New WR1_SBOForms.idh.forms.SBO.GoodsIssue(Me))

        '    '    End If
        '    '    doAddForm(New WR1_SBOForms.idh.forms.SBO.ItemMaster(Me))
        '    '    doAddForm(New WR1_SBOForms.idh.forms.SBO.SalesOrder(Me))
        '    '    doAddForm(New WR1_SBOForms.idh.forms.SBO.PurchaseOrder(Me))
        '    '    doAddForm(New WR1_SBOForms.idh.forms.SBO.APInvoice(Me))
        '    '    'doAddForm(New idh.forms.SBO.JournalVoucher (Me))

        '    '    'ADMIN
        '    '    If goDB.Data().Extra.Equals("ECO") Then
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.JobTypes(Me, "IDHADM", 1))
        '    '        'doAddForm(New idh.forms.admin.JobDefaults(Me, "IDHADM", 2))
        '    '    Else
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.JobTypes(Me, "IDHADM", 1))
        '    '        'doAddForm(New idh.forms.admin.JobDefaults(Me, "IDHADM", 2))


        '    '        'doAddForm(New idh.forms.admin.DIRECodes(Me, "IDHADM", 40))
        '    '        'doAddForm(New idh.forms.admin.CLocTypes(Me, "IDHADM", 41))
        '    '        'doAddForm(New idh.forms.admin.CLocCodes(Me, "IDHADM", 42))
        '    '        'doAddForm(New idh.forms.admin.CLocPos(Me, "IDHADM", 43))
        '    '        'doAddForm(New idh.forms.admin.WPosTypes(Me, "IDHADM", 44))
        '    '        'doAddForm(New idh.forms.admin.WPosCodes(Me, "IDHADM", 45))
        '    '        'NEW MENU: doAddForm(New idh.forms.admin.TFS(Me, "IDHADM", 46))
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.EWC(Me, "IDHADM", 47))
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.HAZ(Me, "IDHADM", 48))
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.ENCategories(Me, "IDHADM", 49))
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.WEEE(Me, "IDHADM", 50))
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.Origin(Me, "IDHADM", 51))
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.Printers(Me, "IDHADM", 52))
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.Translate(Me, "IDHADM", 53))
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.SCReason(Me, "IDHADM", 54))
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.SCValidation(Me, "IDHADM", 55))
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.Status(Me, "IDHADM", 56))

        '    '        doAddForm(New WR1_Admin.idh.forms.admin.WasteGroup(Me, "IDHADM", 58)) 'USA Requirement: Waste Group

        '    '        'OnTime [#Ico000????] USA 
        '    '        If WR1_Forms.Config.INSTANCE.getParameterAsBool("USAREL", False) = True Then
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.CountyPlus(Me, "IDHADM", 57)) 'USA Requirement: USA County (from UFD1 table)
        '    '            'doAddForm(New idh.forms.admin.WasteGroup(Me, "IDHADM", 58)) 'USA Requirement: Waste Group 
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.PortMaster(Me, "IDHADM", 59)) 'USA Requirement: Port Master 
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.CountyTariff(Me, "IDHADM", 60)) 'USA Requirement: County Tariff
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.FuelSurcharge(Me, "IDHADM", 61)) 'USA Requirement: Fuel Surcharge
        '    '        End If
        '    '        ''MA Start 13-05-2016 Issue#1072
        '    '        'doAddForm(New WR1_Admin.idh.forms.admin.Distance(Me, "IDHADM", 62))
        '    '        ''MA Start 13-05-2016 Issue#1072
        '    '        'doAddForm(New WR1_Admin.idh.forms.admin.CustRef(Me, "IDHADM", 63))
        '    '        ' ET ADDED HEINZ
        '    '        'doAddForm(New idh.forms.admin.LizenceQuota(Me, "IDHADM", 56))

        '    '        ' ET END
        '    '        com.idh.forms.oo.Form.doRegisterForm(GetType(com.idh.forms.sys.ErrorInfo), "IDHADM")

        '    '        If Config.ParameterAsBool("WFUSETRC", True) = True Then
        '    '            com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.admin.TransactionCodes), "IDHADM", 65)
        '    '            com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.admin.BPTransactionCodesLink), "IDHADM", 66)
        '    '        End If

        '    '        If Config.ParameterAsBool("OSMUSCB", False) Then
        '    '            com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.admin.OSM), "IDHADM", 67)
        '    '            com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.utils.FieldChangedView), "IDHADM", 68)
        '    '        End If

        '    '        'BP Additional Charges
        '    '        If Config.ParameterAsBool("USEBPACH", True) = True Then
        '    '            com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.admin.BPAdditionalCharges), "IDHADM", 69)
        '    '        End If
        '    '    End If

        '    '    'MASTER DATA
        '    '    If goDB.Data().Extra.Equals("ECO") Then
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.VehicleMaster(Me, "IDHMAST", 3))
        '    '        'com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr2.VehicleMaster), "IDHMAST")
        '    '        'com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr2.VehicleM), "IDHMAST", 24)
        '    '    Else
        '    '        ''MA Start 05-11-2014 Issue#411
        '    '        If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) Then
        '    '            com.idh.forms.oo.Form.doRegisterForm(GetType(WR1_FR2Forms.idh.forms.fr2.prices.CIP), "IDHMAST")
        '    '        End If
        '    '        If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) Then
        '    '            com.idh.forms.oo.Form.doRegisterForm(GetType(WR1_FR2Forms.idh.forms.fr2.prices.SIP), "IDHMAST")
        '    '        End If
        '    '        com.idh.forms.oo.Form.doRegisterForm(GetType(WR1_FR2Forms.idh.forms.fr2.PriceUpdateOption))

        '    '        'doAddForm(New idh.forms.price.Customer(Me, 10))
        '    '        'doAddForm(New idh.forms.price.Supplier(Me, 21))
        '    '        com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.admin.PriceGroup), "IDHMAST", 21)

        '    '        doAddForm(New WR1_Admin.idh.forms.admin.MassPrice(Me, "IDHMAST", 22))
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.MassPriceSIP(Me, "IDHMAST", 23))

        '    '        If Config.ParameterAsBool("VMUSENEW", False) = False Then
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.VehicleMaster(Me, "IDHMAST", 24))
        '    '        Else
        '    '            com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.VehicleM), "IDHMAST", 24)
        '    '            com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.VehicleMaster), "IDHMAST")
        '    '            com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.admin.VMReason), "IDHADM")
        '    '        End If

        '    '        com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.admin.Reason), "IDHADM")

        '    '        'OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
        '    '        Dim sRouteparm As String = Config.Parameter("ROUENABL")
        '    '        If sRouteparm <> "TRUE" Then
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.Routes(Me, "IDHMAST", 25))
        '    '        Else
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.Routes2(Me, "IDHMAST", 25))
        '    '        End If

        '    '        'End OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.Zones(Me, "IDHMAST", 26))
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.TipZones(Me, "IDHADM", 27))
        '    '        doAddForm(New WR1_Forms.idh.forms.admin.Config(Me, "IDHADM", 28))
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.GlAccnts(Me, "IDHMAST", 29))
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.BandFactors(Me, "IDHMAST", 28))

        '    '        doAddForm(New WR1_Admin.idh.forms.admin.CCDetails(Me, "IDHMAST", 29))
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.TakeOns(Me, "IDHMAST", 30))

        '    '        doAddForm(New WR1_Admin.idh.forms.admin.WorkFlow(Me, "IDHMAST", 47))
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.SelfBill(Me, "IDHMAST", 48))

        '    '        doAddForm(New WR1_Admin.idh.forms.admin.WasteGrpJobLink(Me, "IDHMAST", 50))
        '    '        '##MA Start 29-08-2014 Issue#403 Issue#421
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.AddressRestriction(Me, "IDHMAST", 51))
        '    '        doAddForm(New WR1_Search.idh.forms.search.Branch(Me))

        '    '        'Moved under Order Menu
        '    '        'doAddForm(New WR1_Admin.idh.forms.admin.BPForecasting(Me, "IDHMAST", 52))
        '    '        '##MA End 29-08-2014 Issue#403 Issue#421
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.AltItemDescription(Me, "IDHMAST", 53))
        '    '        doAddForm(New WR1_Admin.idh.forms.admin.ContainerDetails(Me, "IDHMAST", 54))

        '    '        'com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.RouteClosure), "IDHMAST", 55)
        '    '        doAddForm(New WR1_Forms.idh.forms.RouteClosure(Me, "IDHMAST", 55))


        '    '        '                	doAddForm(New idh.forms.admin.ConChrg(Me, "IDHMAST", 48))

        '    '        '                    doAddForm(New Ico.DVP.DVP(Me, "IDHMAST", 49))
        '    '        'doAddForm(New idh.forms.VehicleForm(Me, 33))
        '    '        'doAddForm(New idh.forms.DriverForm(Me, 34))
        '    '        'doAddForm(New idh.forms.GlAccountForm(Me, 35))

        '    '        'OnTime [#Ico00036727] Transfrontier Shipment Module 
        '    '        'START Master Data Menu Items : 
        '    '        'NEW MENU: 
        '    '        'If Config.Parameter("TFSMOD").ToString() = "TFSMOD_TRUE" Then
        '    '        '    'doAddForm(New idh.forms.admin.TFSNumbers(Me, "IDHMAST", 36))
        '    '        '    doAddForm(New idh.forms.admin.CompAuthority(Me, "IDHMAST", 37))
        '    '        '    doAddForm(New idh.forms.admin.CADocuments(Me, "IDHMAST", 38))
        '    '        '    doAddForm(New idh.forms.admin.CAMatrix(Me, "IDHMAST", 39))
        '    '        '    doAddForm(New idh.forms.TFSConsole(Me, "IDHMAST", 40))
        '    '        '    doAddForm(New idh.forms.TFSAnnex(Me, "IDHMAST", 41))
        '    '        '    doAddForm(New WR1_Managers.idh.forms.manager.TFSSummary(Me, 42))
        '    '        '    'doAddForm(New idh.forms.TFSNewNo(Me, "IDHMAST", 43))

        '    '        '    'doAddForm(New idh.forms.Training01(Me, "IDHMAST", 42))
        '    '        '    'doAddForm(New idh.forms.Test(Me, "IDHMAST", 43))

        '    '        '    'Joachim Code
        '    '        '    doAddForm(New idh.forms.admin.DCodeMaster(Me, "IDHMAST", 56))
        '    '        '    doAddForm(New idh.forms.admin.HCodeMaster(Me, "IDHMAST", 57))
        '    '        '    doAddForm(New idh.forms.admin.RCodeMaster(Me, "IDHMAST", 58))
        '    '        '    doAddForm(New idh.forms.admin.YCodeMaster(Me, "IDHMAST", 59))
        '    '        '    doAddForm(New idh.forms.admin.CarLic(Me, "IDHMAST", 60))
        '    '        '    doAddForm(New idh.forms.admin.ShipStat(Me, "IDHMAST", 61))
        '    '        '    doAddForm(New idh.forms.admin.TFSTyp(Me, "IDHMAST", 62))
        '    '        '    doAddForm(New idh.forms.admin.TFSStat(Me, "IDHMAST", 63))
        '    '        '    doAddForm(New idh.forms.admin.PackIndex(Me, "IDHMAST", 64))
        '    '        '    doAddForm(New idh.forms.admin.MovStat(Me, "IDHMAST", 65))
        '    '        'End If
        '    '        ''OnTime [#Ico00036727] Transfrontier Shipment Module 
        '    '        ''END
        '    '    End If

        '    '    'ORDERS
        '    '    If goDB.Data().Extra.Equals("ECO") Then
        '    '        doAddForm(New WR1_Forms.idh.forms.orders.Disposal(Me, 33))
        '    '    Else
        '    '        'doAddForm(New idh.forms.WasteOrder(Me))
        '    '        'doAddForm(New idh.forms.DisposalOrder(Me))

        '    '        doAddForm(New WR1_Admin.idh.forms.admin.BPForecasting(Me, "IDHORD", 29))

        '    '        doAddForm(New WR1_Forms.idh.forms.EVNMaster(Me, "IDHORD", 30))

        '    '        doAddForm(New WR1_Forms.idh.forms.WasteContract(Me, 31))
        '    '        'doAddForm(New idh.forms.WasteProposal(Me, 31))

        '    '        'If Config.INSTANCE.getParameterAsBool("WFWOPRE", False) = True Then
        '    '        '    com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.WOPreSelect), "IDHORD")
        '    '        '    com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.WO), Nothing)
        '    '        'End If

        '    '        'Dim sVal As String = Config.INSTANCE.getParameter("DOLITEVR")
        '    '        'If String.IsNullOrWhiteSpace(sVal) = False Then
        '    '        '    If sVal = "001" Then
        '    '        '        'com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.DOLite001), "IDHORD")
        '    '        '    End If
        '    '        'End If

        '    '        'doAddForm(New WR1_Forms.idh.forms.orders.Waste(Me, 33))
        '    '        'doAddForm(New WR1_Forms.idh.forms.orders.Disposal(Me, 35))
        '    '        'doAddForm(New WR1_Forms.idh.forms.OrderRow(Me, 36))

        '    '        doAddForm(com.isb.core.controller.Loader.getForm("orders.Waste", New Object() {Me, 33}))
        '    '        doAddForm(com.isb.core.controller.Loader.getForm("orders.Disposal", New Object() {Me, 35}))
        '    '        doAddForm(com.isb.core.controller.Loader.getForm("OrderRow", New Object() {Me, 36}))


        '    '        'If getLookUp().getParameterAsBool("WASORDC", True) Then
        '    '        'doAddForm(New idh.forms.orders.Waste_comb(Me, 33))
        '    '        'End If

        '    '        '                    doAddForm(New idh.forms.scans.Waste(Me, "IDHTTS", 50))
        '    '        '                	doAddForm(New WR1_Managers.idh.forms.manager.Track(Me, 51))
        '    '        If Config.INSTANCE.getParameterAsBool("PBIENABL", True) Then
        '    '            doAddForm(New WR1_Forms.idh.forms.PreBookInstruction(Me, 37))
        '    '        End If
        '    '        'com.idh.forms.oo.Form.doRegisterForm(GetType(idh.forms.fr2.Import), "IDHORD")
        '    '        com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.Import), "IDHORD")
        '    '        doAddForm(New WR1_Forms.idh.forms.ImportIncomingPayments(Me, -1))
        '    '    End If

        '    '    'PERIODIC PROCESSING
        '    '    'If goDB.Data().Extra.Equals("ECO") Then
        '    '    'Else
        '    '    '    doAddForm(New idh.forms.DriverTimesheet(Me, 40))
        '    '    'End If

        '    '    'RECOVERY
        '    '    If goDB.Data().Extra.Equals("ECO") Then
        '    '    Else
        '    '    End If

        '    '    'PLANNING AND SCHEDULING
        '    '    If goDB.Data().Extra.Equals("ECO") Then
        '    '        doAddForm(New WR1_Managers.idh.forms.manager.Disposal(Me, 50, "IDHDOM", "Disposal Manager.srf", "Disposal Order Manager"))
        '    '    Else
        '    '        doAddForm(New WR1_Managers.idh.forms.manager.Order(Me, 50, Nothing, Nothing, Nothing))
        '    '        doAddForm(New WR1_Managers.idh.forms.manager.OrderL2(Me, 51))
        '    '        doAddForm(New WR1_Managers.idh.forms.manager.OrderL3(Me, 52))
        '    '        If Config.INSTANCE.getParameterAsBool("PBIENABL", True) Then
        '    '            doAddForm(New WR1_Managers.idh.forms.manager.OrderL4(Me, 53))
        '    '        End If
        '    '        doAddForm(New WR1_Managers.idh.forms.manager.OrderL5(Me, 55))
        '    '        doAddForm(New WR1_Managers.idh.forms.manager.OrderL6(Me, 53))
        '    '        doAddForm(New WR1_Managers.idh.forms.manager.OrderL7(Me, 54))
        '    '        doAddForm(New WR1_Managers.idh.forms.manager.OrderL8(Me, 55))

        '    '        'If Config.ParameterAsBool("OSMUSCB", False) Then
        '    '        '    com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.managers.OSM), "IDHPS")
        '    '        'End If

        '    '        If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_FORMS1) Then
        '    '            doAddForm(New WR1_Managers.idh.forms.manager.WOBilling(Me, 56))
        '    '            doAddForm(New WR1_Managers.idh.forms.manager.WOBillingL2(Me, 64))
        '    '            doAddForm(New WR1_Managers.idh.forms.manager.WOBillingL3(Me, 67))
        '    '            doAddForm(New WR1_Managers.idh.forms.manager.WOBillingL4(Me, 68))
        '    '            doAddForm(New WR1_Managers.idh.forms.manager.WOBillingL5(Me, 69))
        '    '        End If

        '    '        'doAddForm(New WR1_Managers.idh.forms.manager.Route(Me, 55))
        '    '        'OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
        '    '        If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_FORMS1) Then
        '    '            doAddForm(New WR1_Managers.idh.forms.manager.BPForecast(Me, 69))
        '    '        End If

        '    '        doAddForm(New WR1_Managers.idh.forms.manager.EVN(Me, 69))

        '    '        Dim sRouteparm As String = Config.Parameter("ROUENABL")
        '    '        If sRouteparm = "TRUE" Then
        '    '            doAddForm(New WR1_Managers.idh.forms.manager.RoutePlanner(Me, 70))
        '    '        Else
        '    '            doAddForm(New WR1_Managers.idh.forms.manager.Route(Me, 70))
        '    '        End If
        '    '        'End OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
        '    '        'doAddForm(New WR1_Managers.idh.forms.manager.Vehicle(Me, 71))

        '    '        Dim sVehicleManager As String = Config.ParameterWithDefault("VEHMAN", "IDHVECR2")
        '    '        If sVehicleManager = "IDHVECR2" Then
        '    '            doAddForm(New WR1_Managers.idh.forms.manager.Vehicle2(Me, 72))
        '    '        ElseIf sVehicleManager = "IDHVECR3" Then
        '    '            doAddForm(New WR1_Managers.idh.forms.manager.Vehicle3(Me, 72))
        '    '        End If

        '    '        doAddForm(New WR1_Managers.idh.forms.manager.Disposal(Me, 73, "IDHDOM", "Disposal Manager.srf", "Disposal Order Manager"))
        '    '        'KA: 20130617 
        '    '        'Form menu item resurfaced again. Need to comment this out
        '    '        'OLD Pre-book Manager 
        '    '        'doAddForm(New WR1_Managers.idh.forms.manager.Coverage(Me, 74))

        '    '        If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_FORMS1) Then
        '    '            doAddForm(New WR1_Managers.idh.forms.manager.ConsignmentWO(Me, 75))
        '    '        End If

        '    '        doAddForm(New WR1_Managers.idh.forms.manager.ConsignmentDO(Me, 76))
        '    '        'doAddForm(New WR1_Managers.idh.forms.manager.Compliance(Me, 62))

        '    '        If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_FORMS1) Then
        '    '            doAddForm(New WR1_Managers.idh.forms.manager.DeLink(Me, 78))
        '    '        End If

        '    '        doAddForm(New WR1_Managers.idh.forms.manager.Licence(Me, 80))
        '    '        doAddForm(New WR1_Managers.idh.forms.manager.Updater(Me, 81))

        '    '        If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_FORMS1) Then
        '    '            doAddForm(New WR1_Managers.idh.forms.manager.DOUpdater(Me, 82))
        '    '        End If

        '    '        'doAddForm(New idh.report.DriverToDo(Me, 53))
        '    '        If Config.INSTANCE.getParameterAsBool("PBIENABL", True) Then
        '    '            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_FORMS1) Then
        '    '                doAddForm(New WR1_Managers.idh.forms.manager.PreBook(Me, 83))
        '    '            End If
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.PBIDetails(Me, -1))
        '    '        End If

        '    '        doAddForm(New WR1_Search.idh.forms.search.Subby(Me, "IDHPS", 85))
        '    '        'doAddForm(New WR1_Managers.idh.forms.manager.Contract(Me, -1))

        '    '        doAddForm(New WR1_Managers.idh.forms.manager.APDocWizard(Me, 87))
        '    '        doAddForm(New WR1_Managers.idh.forms.manager.ARDocWizard(Me, 88))

        '    '        com.idh.forms.oo.Form.doRegisterForm(GetType(WR1_FR2Forms.idh.forms.fr2.manager.LoadForm), "IDHPS")
        '    '    End If

        '    '    'ASSET MANAGMENT
        '    '    'If goDB.Data().Extra.Equals("ECO") Then
        '    '    'Else
        '    '    'doAddForm(New idh.forms.ContainerMovements(Me, 60))
        '    '    'End If

        '    '    'TFS Module 
        '    '    If goDB.Data().Extra.Equals("ECO") Then
        '    '    Else

        '    '        If Config.ParameterWithDefault("TFSMOD", "") = "TFSMOD_TRUE" Then
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.TFS(Me, "IDHTFS", 1))
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.CompAuthority(Me, "IDHTFS", 2))
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.CADocuments(Me, "IDHTFS", 3))
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.CAMatrix(Me, "IDHTFS", 4))
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.DCodeMaster(Me, "IDHTFS", 5))
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.HCodeMaster(Me, "IDHTFS", 6))
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.RCodeMaster(Me, "IDHTFS", 7))
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.YCodeMaster(Me, "IDHTFS", 8))
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.BASEL(Me, "IDHTFS", 9))
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.OECD(Me, "IDHTFS", 10))
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.CarLic(Me, "IDHTFS", 11))
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.ShipStat(Me, "IDHTFS", 12))
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.TFSTyp(Me, "IDHTFS", 13))
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.TFSStat(Me, "IDHTFS", 14))
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.PackIndex(Me, "IDHTFS", 15))
        '    '            doAddForm(New WR1_Admin.idh.forms.admin.MovStat(Me, "IDHTFS", 16))

        '    '            'doAddForm(New WR1_Forms.idh.forms.TFSNewNo(Me, "IDHTFS", 17))

        '    '            'doAddForm(New WR1_Forms.idh.forms.TFSConsole(Me, "IDHTFS", 18))
        '    '            'doAddForm(New WR1_Forms.idh.forms.TFSAnnex(Me, "IDHTFS", 19))
        '    '            'doAddForm(New WR1_Managers.idh.forms.manager.TFSSummary(Me, 20))

        '    '            doAddForm(New WR1_TFS.idh.forms.TFS.TFSNewNo(Me, "IDHTFS", 17))
        '    '            doAddForm(New WR1_TFS.idh.forms.TFS.TFSConsole(Me, "IDHTFS", 18))
        '    '            doAddForm(New WR1_TFS.idh.forms.TFS.TFSAnnex(Me, "IDHTFS", 19))
        '    '            doAddForm(New WR1_Managers.idh.forms.manager.TFSSummary(Me, 20))
        '    '        End If
        '    '    End If

        '    '    'FORMS
        '    '    If goDB.Data().Extra.Equals("ECO") Then
        '    '    Else
        '    '        'doAddForm(New idh.forms.VehicleOrders(Me))
        '    '    End If


        '    '    'REPORTING
        '    '    If goDB.Data().Extra.Equals("ECO") Then
        '    '    Else
        '    '        doAddForm(New WR1_Reports.idh.report.OnSite(Me, 10))
        '    '        'doAddForm(New idh.report.DOCDO(Me, 70))
        '    '        'doAddForm(New idh.report.DOCWO(Me, 71))
        '    '        'doAddForm(New idh.report.ContWhere(Me, 72))
        '    '        'doAddForm(New idh.report.VehProfit(Me, 73))
        '    '        'doAddForm(New idh.forms.SkipExpiryReport(Me, 80))
        '    '        ''doAddForm(New idh.forms.WasteCustomerEnquiry(Me))
        '    '        'doAddForm(New idh.forms.enquiry.Customer(Me, 81))
        '    '        ''doAddForm(New idh.forms.WasteSupplierEnquiry(Me))
        '    '        'doAddForm(New idh.forms.enquiry.Supplier(Me, 82))
        '    '        'doAddForm(New idh.forms.EAReport1(Me, 83))
        '    '        'doAddForm(New idh.forms.ConsigneeReturn(Me, 84))
        '    '        ''doAddForm(New idh.forms.OutstandingJobReport(Me))
        '    '        ''doAddForm(New idh.forms.Weighbridge(Me))
        '    '    End If

        '    '    '#Region "Training Example1"
        '    '    If Config.INSTANCE.getParameterAsBool("USETRAIN", False) Then
        '    '        ''				doAddForm(New idh.forms.Training01(Me, "IDHORD", 100))
        '    '        'com.idh.forms.oo.Form.doRegisterForm(GetType(WR1_FR2Forms.idh.forms.fr2.Training02))
        '    '        ''               com.idh.forms.oo.Form.doRegisterForm(GetType(idh.forms.fr2.Training03))
        '    '        com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.Training01), "IDHADM", 65)
        '    '        '#End Region
        '    '    End If

        '    '    doAddForm(New WR1_Admin.idh.forms.admin.FormSettings(Me, "IDHADM", 11))

        '    '    ''com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.DOTAXDETAIL), "IDH_DOTXDT", -1)

        '    '    'temporary commencted
        '    '    com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.DOTAXDETAIL))
        '    '    com.idh.forms.oo.Form.doRegisterForm(GetType(WR1_FR2Forms.idh.forms.fr2.BusinessPartnerAddressUpdater))
        '    '    com.idh.forms.oo.Form.doRegisterForm(GetType(com.uBC.forms.fr3.PostCode))

        '    If Config.ParameterWithDefault("ENQMOD", "FALSE") = "ENQMOD_TRUE" Then
        '        'NEWCORE-TEMP-BLOCK--> com.isb.forms.Enquiry.admin.MainForm.doAddForms(Me)
        '    End If

        '    '    'doSendAlert({"manager"}, "WR1 - Request for Document Approval", "Test004b", 122, "100004", True)
        '    '    'doSendApproval()

        '    '    'com.idh.win.forms.SBOForm.doRegisterForm(GetType(com.idh.form.SBO.Info), "IDHORD", "Win - Disposal Order", 60)
        '    'End If
        'End Sub

        ''** Recipient - UserCodes
        ''** Message Subject
        ''** Message Text
        ''** Link tyoe
        ''** LinkValue
        'Public Function doSendApproval() As Boolean
        '    'Dim approvalSrv As ApprovalRequestsService = goDICompany.GetCompanyService.GetBusinessService(ServiceTypes.ApprovalRequestsService)
        '    'Dim OpARAMS As ApprovalRequestParams = approvalSrv.GetDataInterface(ApprovalRequestsServiceDataInterfaces.arsApprovalRequestParams)
        '    'OpARAMS.Code = 1
        '    'Dim oData As ApprovalRequest = approvalSrv.GetApprovalRequest(OpARAMS)

        '    ''Add an approval decision 
        '    'oData.ApprovalRequestDecisions.Add(); 
        '    'oData.ApprovalRequestDecisions.Item(0).ApproverUserName = "manager"
        '    'oData.ApprovalRequestDecisions.Item(0).ApproverPassword = ""
        '    'oData.ApprovalRequestDecisions.Item(0).Status = BoApprovalRequestDecisionEnum.ardPending
        '    'oData.ApprovalRequestDecisions.Item(0).Remarks = "Waiting for a Approval"

        '    ''Update the approval request 
        '    'approvalSrv.UpdateRequest(oData)

        '    Dim bDoSend As Boolean = True
        '    Dim oCmpService As SAPbobsCOM.CompanyService
        '    Dim oMessageService As SAPbobsCOM.MessagesService
        '    Dim oMessage As SAPbobsCOM.Message
        '    Dim oLine As SAPbobsCOM.MessageDataLine
        '    Dim oLines As SAPbobsCOM.MessageDataLines

        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '    'Dim sMsgData As String
        '    'Dim sValue As String
        '    'Dim sKeyStr As String
        '    'Dim sDocType As String
        '    'Try
        '    '    Dim sQry As String = "SELECT MsgData, Subject From OALR WHERE Code = 17"
        '    'oRecordSet = DataHandler.INSTANCE.doSelectQuery("Main", sQry)
        '    '    If Not oRecordSet Is Nothing AndAlso oRecordSet.RecordCount > 0 Then
        '    '        sMsgData = oRecordSet.Fields.Item(0).Value
        '    '        sSubject = oRecordSet.Fields.Item(1).Value
        '    '    End If

        '    '    sQry = "SELECT Value, KeyStr, ObjType From ALR3 WHERE Code = 17"
        '    '    oRecordSet = DataHandler.INSTANCE.doSelectQuery("Main", sQry)
        '    '    If Not oRecordSet Is Nothing AndAlso oRecordSet.RecordCount > 0 Then
        '    '        sValue = oRecordSet.Fields.Item(0).Value
        '    '        sKeyStr = oRecordSet.Fields.Item(1).Value
        '    '        sDocType = oRecordSet.Fields.Item(2).Value
        '    '    End If
        '    'Catch ex As Exception
        '    '    com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing Query")
        '    'Finally
        '    '    IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
        '    'End Try

        '    Dim pMessageDataColumns As SAPbobsCOM.MessageDataColumns
        '    Dim pMessageDataColumn As SAPbobsCOM.MessageDataColumn
        '    Dim oRecipientCollection As SAPbobsCOM.RecipientCollection

        '    oCmpService = goDICompany.GetCompanyService
        '    oMessageService = oCmpService.GetBusinessService(SAPbobsCOM.ServiceTypes.MessagesService)

        '    Dim sUserCode As String = 1 '"manager"
        '    Dim sSubject As String = "Request for Document Approval"
        '    Dim sMessage As String = "Test004b"
        '    Dim oLinkType As SAPbouiCOM.BoLinkedObject = 122
        '    Try
        '        'doSaveAlert(oCompany, sUserCode, sSubject, sMessage, oLinkType, sLinkValue, bDoSend)
        '        If bDoSend Then
        '            oMessage = oMessageService.GetDataInterface(SAPbobsCOM.MessagesServiceDataInterfaces.msdiMessage)
        '            oMessage.Subject = sSubject
        '            oMessage.Text = sMessage

        '            'Add Recipient
        '            oRecipientCollection = oMessage.RecipientCollection

        '            oRecipientCollection.Add()
        '            oRecipientCollection.Item(0).SendInternal = SAPbobsCOM.BoYesNoEnum.tYES
        '            oRecipientCollection.Item(0).UserCode = "manager"

        '            'get columns data
        '            pMessageDataColumns = oMessage.MessageDataColumns
        '            pMessageDataColumn = pMessageDataColumns.Add()
        '            pMessageDataColumn.ColumnName = sSubject
        '            pMessageDataColumn.Link = SAPbobsCOM.BoYesNoEnum.tYES

        '            'get lines
        '            oLines = pMessageDataColumn.MessageDataLines()
        '            oLine = oLines.Add()
        '            oLine.Value = "Purchase Order based on draft no."
        '            oLine.Object = "122"
        '            oLine.ObjectKey = "11"

        '            oLine = oLines.Add()
        '            oLine.Value = "Draft Marketing Document"
        '            oLine.Object = "112"
        '            oLine.ObjectKey = "146"

        '            'oLine.Value = "Purchase Order based on draft no. 13215"
        '            'oLine.Object = SAPbouiCOM.BoLinkedObject.lf_Order
        '            'oLine.ObjectKey = "191"


        '            'send the message
        '            oMessageService.SendMessage(oMessage)
        '        End If
        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "")
        '    Finally
        '    End Try
        '    Return True
        'End Function

        'Public Sub doCloseFloatingForms()
        '    Dim iCount As Integer
        '    Dim oForms As SAPbouiCOM.Forms = goApplication.Forms
        '    Dim oForm As SAPbouiCOM.Form
        '    Dim iFormCount As Integer = oForms.Count

        '    iCount = 0
        '    While iCount < iFormCount
        '        Try
        '            oForm = oForms.Item(iCount)
        '            If oForm IsNot Nothing Then
        '                If oForm.UniqueID.StartsWith(goISVName) Then
        '                    oForm.Close()
        '                    iFormCount = iFormCount - 1
        '                Else
        '                    iCount = iCount + 1
        '                End If
        '            End If
        '        Catch ex As Exception
        '            iCount = iCount + 1
        '        End Try
        '    End While
        'End Sub

        'Private Sub doCreateDynamicReports(ByVal iMemPos As Integer)
        '    Dim sPath As String
        '    sPath = Config.INSTANCE.getParameter("REPDDI")
        '    If Not sPath Is Nothing AndAlso sPath.Trim().Length() > 0 Then
        '        If sPath.EndsWith("\") = False Then
        '            sPath = sPath & "\"
        '        End If

        '        If sPath.Length > 3 Then
        '            Dim wChar As Char = sPath.Chars(1)
        '            If Not sPath.Chars(1) = ":" AndAlso Not (sPath.Chars(0) = "\" AndAlso sPath.Chars(1) = "\") Then
        '                If Not sPath.Chars(0) = "\" Then
        '                    sPath = "\" & sPath
        '                End If
        '                sPath = IDHAddOns.idh.addon.Base.CURRENTWORKDIR & sPath
        '            End If
        '        End If
        '    Else
        '        'sPath = IDHAddOns.idh.addon.Base.CURRENTWORKDIR & "\reports\dynamic\"
        '        sPath = IDHAddOns.idh.addon.Base.REPORTPATH & "\dynamic\"
        '    End If

        '    Try
        '        'Get filelist
        '        DataHandler.INSTANCE.doInfo("Building Report Tree: " & sPath)
        '        Dim saFiles As String() = Directory.GetFiles(sPath)
        '        For iFile As Integer = 0 To saFiles.Length() - 1
        '            Dim sFileName As String = saFiles(iFile)

        '            Dim iFileNameStart As Integer = sFileName.LastIndexOf("\")
        '            If iFileNameStart < 0 Then
        '                iFileNameStart = 0
        '            End If

        '            Dim iFileNameEnd As Integer = sFileName.LastIndexOf(".")
        '            If iFileNameEnd < 0 OrElse iFileNameEnd < iFileNameStart Then
        '                iFileNameEnd = sFileName.Length - iFileNameStart
        '            End If

        '            Dim sTitle As String = sFileName.Substring(iFileNameStart + 1, iFileNameEnd - iFileNameStart - 1)
        '            If sTitle.Length > 30 Then
        '                sTitle = sTitle.Substring(0, 30) & ".."
        '            End If
        '            doAddForm(New IDHAddOns.idh.report.CrystalViewer(Me, "IRep_" & iFile, "IDHRE", iMemPos + iFile, sTitle, sFileName))
        '        Next
        '        DataHandler.INSTANCE.doInfo("Report Tree Done: " & sPath)

        '    Catch ex1 As Exception
        '    End Try
        'End Sub

        'Protected Overridable Sub doCreateExtraMenu()
        '    Dim oMenus As SAPbouiCOM.Menus
        '    Dim oMenuItem As SAPbouiCOM.MenuItem
        '    Dim oCreationPackage As SAPbouiCOM.MenuCreationParams

        '    'ADMIN
        '    Try
        '        oMenuItem = goApplication.Menus.Item("IDHADM")
        '    Catch ex1 As Exception
        '        oMenuItem = goApplication.Menus.Item(goMenuID)
        '        oMenus = oMenuItem.SubMenus

        '        oCreationPackage = goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        '        oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP
        '        oCreationPackage.UniqueID = "IDHADM"
        '        oCreationPackage.String = Translation.getTranslated(goMenuID, "IDHADM", Nothing, "Admin")
        '        oCreationPackage.Position = 1000
        '        Try
        '            oMenus.AddEx(oCreationPackage)
        '        Catch ex As Exception
        '        End Try
        '    End Try

        '    'MASTER DATA
        '    Try
        '        oMenuItem = goApplication.Menus.Item("IDHMAST")
        '    Catch ex1 As Exception
        '        oMenuItem = goApplication.Menus.Item(goMenuID)
        '        oMenus = oMenuItem.SubMenus

        '        oCreationPackage = goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        '        oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP
        '        oCreationPackage.UniqueID = "IDHMAST"
        '        oCreationPackage.String = Translation.getTranslated(goMenuID, "IDHMAST", Nothing, "Master Data")
        '        oCreationPackage.Position = 2000
        '        Try
        '            oMenus.AddEx(oCreationPackage)
        '        Catch ex As Exception
        '        End Try
        '    End Try

        '    'ORDERS
        '    Try
        '        oMenuItem = goApplication.Menus.Item("IDHORD")
        '    Catch ex1 As Exception
        '        oMenuItem = goApplication.Menus.Item(goMenuID)
        '        oMenus = oMenuItem.SubMenus

        '        oCreationPackage = goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        '        oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP
        '        oCreationPackage.UniqueID = "IDHORD"
        '        oCreationPackage.String = Translation.getTranslated(goMenuID, "IDHORD", Nothing, "Order")
        '        oCreationPackage.Position = 3000
        '        Try
        '            oMenus.AddEx(oCreationPackage)
        '        Catch ex As Exception
        '        End Try
        '    End Try

        '    'Planning and Scheduling
        '    Try
        '        oMenuItem = goApplication.Menus.Item("IDHPS")
        '    Catch ex1 As Exception
        '        oMenuItem = goApplication.Menus.Item(goMenuID)
        '        oMenus = oMenuItem.SubMenus

        '        oCreationPackage = goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        '        oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP
        '        oCreationPackage.UniqueID = "IDHPS"
        '        oCreationPackage.String = Translation.getTranslated(goMenuID, "IDHPS", Nothing, "Planning and Scheduling")
        '        oCreationPackage.Position = 5000
        '        Try
        '            oMenus.AddEx(oCreationPackage)
        '        Catch ex As Exception
        '        End Try
        '    End Try

        '    'Trans-frontier Shipments
        '    If Config.ParameterWithDefault("TFSMOD", "") = "TFSMOD_TRUE" Then
        '        Try
        '            oMenuItem = goApplication.Menus.Item("IDHTFS")
        '        Catch ex As Exception
        '            oMenuItem = goApplication.Menus.Item(goMenuID)
        '            oMenus = oMenuItem.SubMenus

        '            oCreationPackage = goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        '            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP
        '            oCreationPackage.UniqueID = "IDHTFS"
        '            oCreationPackage.String = Translation.getTranslatedWord("Transfrontier Shipments")
        '            oCreationPackage.Position = 6000
        '            Try
        '                oMenus.AddEx(oCreationPackage)
        '            Catch ex1 As Exception
        '            End Try
        '        End Try
        '    End If

        '    'Enquiry And Lab
        '    If Config.ParameterWithDefault("ENQMOD", "FALSE") = "ENQMOD_TRUE" Then
        '        Try
        '            oMenuItem = goApplication.Menus.Item("IDHENQ")
        '        Catch ex As Exception
        '            oMenuItem = goApplication.Menus.Item(goMenuID)
        '            oMenus = oMenuItem.SubMenus

        '            oCreationPackage = goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        '            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP
        '            oCreationPackage.UniqueID = "IDHENQ"
        '            oCreationPackage.String = Translation.getTranslatedWord("Enquiry Module")
        '            oCreationPackage.Position = 7000
        '            Try
        '                oMenus.AddEx(oCreationPackage)
        '            Catch ex1 As Exception
        '            End Try
        '        End Try

        '        Try
        '            oMenuItem = goApplication.Menus.Item("IDHENQADM")
        '        Catch ex As Exception
        '            oMenuItem = goApplication.Menus.Item("IDHENQ")
        '            oMenus = oMenuItem.SubMenus

        '            oCreationPackage = goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        '            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP
        '            oCreationPackage.UniqueID = "IDHENQADM"
        '            oCreationPackage.String = Translation.getTranslatedWord("Admin")
        '            oCreationPackage.Position = 1
        '            Try
        '                oMenus.AddEx(oCreationPackage)
        '            Catch ex1 As Exception
        '            End Try
        '        End Try

        '        Try
        '            oMenuItem = goApplication.Menus.Item("ENQAPR")
        '        Catch ex As Exception
        '            oMenuItem = goApplication.Menus.Item("IDHENQADM")
        '            oMenus = oMenuItem.SubMenus

        '            oCreationPackage = goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        '            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP
        '            oCreationPackage.UniqueID = "ENQAPR"
        '            oCreationPackage.String = Translation.getTranslatedWord("Approvals")
        '            oCreationPackage.Position = 2
        '            Try
        '                oMenus.AddEx(oCreationPackage)
        '            Catch ex1 As Exception
        '            End Try
        '        End Try
        '    End If

        '    'TRACKTRACE
        '    '            Try
        '    '                oMenuItem = goApplication.Menus.Item("IDHTTS")
        '    '            Catch ex1 As Exception
        '    '                oMenuItem = goApplication.Menus.Item(goMenuID)
        '    '                oMenus = oMenuItem.SubMenus
        '    '
        '    '                oCreationPackage = goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        '    '                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP
        '    '                oCreationPackage.UniqueID = "IDHTTS"
        '    '                oCreationPackage.String = Translation.getTranslated(goMenuID, "IDHTTS", Nothing, "Track and Trace")
        '    '                oCreationPackage.Position = 3000
        '    '                Try
        '    '                    oMenus.AddEx(oCreationPackage)
        '    '                Catch ex As Exception
        '    '                End Try
        '    '            End Try

        '    ''RECOVERY
        '    ''Try
        '    ''    oMenuItem = goApplication.Menus.Item("IDHRC")
        '    ''Catch ex1 As Exception
        '    ''    oMenuItem = goApplication.Menus.Item(goMenuID)
        '    ''    oMenus = oMenuItem.SubMenus

        '    ''    oCreationPackage = goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        '    ''    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP
        '    ''    oCreationPackage.UniqueID = "IDHRC"
        '    ''    oCreationPackage.String = Translation.getTranslated(goMenuID, "IDHRC", Nothing, "Recovery")
        '    ''    oCreationPackage.Position = 4100
        '    ''    Try
        '    ''        oMenus.AddEx(oCreationPackage)
        '    ''    Catch ex As Exception
        '    ''    End Try
        '    ''End Try

        '    ''PERIODIC PROCESSING
        '    ''Try
        '    ''    oMenuItem = goApplication.Menus.Item("IDHPP")
        '    ''Catch ex1 As Exception
        '    ''    oMenuItem = goApplication.Menus.Item(goMenuID)
        '    ''    oMenus = oMenuItem.SubMenus

        '    ''    oCreationPackage = goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        '    ''    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP
        '    ''    oCreationPackage.UniqueID = "IDHPP"
        '    ''    oCreationPackage.String = Translation.getTranslated(goMenuID, "IDHPP", Nothing, "Periodic Processing")
        '    ''    oCreationPackage.Position = 4000
        '    ''    Try
        '    ''        oMenus.AddEx(oCreationPackage)
        '    ''    Catch ex As Exception
        '    ''    End Try
        '    ''End Try

        '    ''Weighbridge
        '    ''Try
        '    ''    oMenuItem = goApplication.Menus.Item("IDHWE")
        '    ''Catch ex1 As Exception
        '    ''    oMenuItem = goApplication.Menus.Item(goMenuID)
        '    ''    oMenus = oMenuItem.SubMenus

        '    ''    oCreationPackage = goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        '    ''    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP
        '    ''    oCreationPackage.UniqueID = "IDHWE"
        '    ''    oCreationPackage.String = Translation.getTranslated(goMenuID, "IDHWE", Nothing, "Weighbridge")
        '    ''    oCreationPackage.Position = 6000
        '    ''    Try
        '    ''        oMenus.AddEx(oCreationPackage)
        '    ''    Catch ex As Exception
        '    ''    End Try
        '    ''End Try

        '    ''Asset Management
        '    ''Try
        '    ''    oMenuItem = goApplication.Menus.Item("IDHAM")
        '    ''Catch ex1 As Exception
        '    ''    oMenuItem = goApplication.Menus.Item(goMenuID)
        '    ''    oMenus = oMenuItem.SubMenus

        '    ''    oCreationPackage = goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        '    ''    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP
        '    ''    oCreationPackage.UniqueID = "IDHAM"
        '    ''    oCreationPackage.String = Translation.getTranslated(goMenuID, "IDHAM", Nothing, "Asset Management")
        '    ''    oCreationPackage.Position = 7000
        '    ''    Try
        '    ''        oMenus.AddEx(oCreationPackage)
        '    ''    Catch ex As Exception
        '    ''    End Try
        '    ''End Try

        '    ''Forms
        '    ''Try
        '    ''    oMenuItem = goApplication.Menus.Item("IDHFO")
        '    ''Catch ex1 As Exception
        '    ''    oMenuItem = goApplication.Menus.Item(goMenuID)
        '    ''    oMenus = oMenuItem.SubMenus

        '    ''    oCreationPackage = goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        '    ''    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP
        '    ''    oCreationPackage.UniqueID = "IDHFO"
        '    ''    oCreationPackage.String = Translation.getTranslated(goMenuID, "IDHFO", Nothing, "Forms")
        '    ''    oCreationPackage.Position = 8000
        '    ''    Try
        '    ''        oMenus.AddEx(oCreationPackage)
        '    ''    Catch ex As Exception
        '    ''    End Try
        '    ''End Try

        '    'Reports
        '    Try
        '        oMenuItem = goApplication.Menus.Item("IDHRE")
        '    Catch ex1 As Exception
        '        oMenuItem = goApplication.Menus.Item(goMenuID)
        '        oMenus = oMenuItem.SubMenus

        '        oCreationPackage = goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        '        oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP
        '        oCreationPackage.UniqueID = "IDHRE"
        '        oCreationPackage.String = Translation.getTranslated(goMenuID, "IDHRE", Nothing, "Reports")
        '        oCreationPackage.Position = 9000
        '        Try
        '            oMenus.AddEx(oCreationPackage)
        '        Catch ex As Exception
        '        End Try
        '    End Try
        'End Sub

        '*** Get the Company Object of the remote company
        Public Function getRemoteSettings(ByVal sCardCode As String, Optional ByVal bDoReRead As Boolean = False) As Hashtable
            Dim oResultSet As SAPbobsCOM.Recordset = Nothing

            Try
                Dim sServer As String = ""
                Dim sCompanyDB As String = ""
                Dim sDbUserName As String = ""
                Dim sDbPassword As String = ""
                Dim sUsername As String = ""
                Dim sPassword As String = ""
                Dim sShareCon As String = ""

                Dim hSettings As Hashtable = Nothing
                If mhRemoteSettings Is Nothing Then
                    mhRemoteSettings = New Hashtable
                End If

                If bDoReRead = False AndAlso mhRemoteSettings.Count > 0 AndAlso mhRemoteSettings.ContainsKey(sCardCode) Then
                    If hSettings.Item("FOUND") = False Then
                        Return Nothing
                    Else
                        hSettings = mhRemoteSettings.Item(sCardCode)
                    End If
                Else
                    doReleaseRemoteCompanies(sCardCode, True)

                    Dim sQry As String
                    sQry = "SELECT c1.Code, c1.U_Val, c1.U_Desc FROM [@" & goConfigTable & "] c1, [@" & goConfigTable & "] c2 " & _
                         "     WHERE c1.Code like 'RN%' + SUBSTRING(c2.Code,8,1) " & _
                         "     AND c2.Code like 'RNCRDCD%' " & _
                         "     AND c2.U_Val  = '" & sCardCode & "'"
                    oResultSet = goDB.doSelectQuery(sQry)

                    hSettings = New Hashtable
                    If Not oResultSet Is Nothing AndAlso oResultSet.RecordCount > 0 Then
                        hSettings.Add("FOUND", True)
                        For iR As Integer = 0 To oResultSet.RecordCount - 1
                            Dim sKey As String = oResultSet.Fields.Item(0).Value
                            Dim sVal As String = oResultSet.Fields.Item(1).Value
                            sKey = sKey.Substring(0, 7)

                            hSettings.Add(sKey, sVal)
                            oResultSet.MoveNext()
                        Next
                    Else
                        hSettings.Add("FOUND", False)
                    End If
                End If
                Return hSettings
            Catch ex As Exception
                mhRemoteSettings = Nothing
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error Retrieving the Remote connection settings")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRRCS", {Nothing})
                Return Nothing
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oResultSet)
            End Try
        End Function

        '*** Get the Company Object of the remote company
        Public Function doConnectRemoteCompany(ByRef hSettings As Hashtable) As SAPbobsCOM.Company
            Dim sCardCode As String
            If hSettings Is Nothing OrElse hSettings.Count = 0 Then
                Return Nothing
            Else
                If hSettings.Item("FOUND") = False Then
                    Return Nothing
                Else
                    sCardCode = hSettings.Item("RNCRDCD")
                End If
            End If

            Dim moComp As SAPbobsCOM.Company = Nothing
            'Dim oEnum As IEnumerator
            If mhRemoteCompanies Is Nothing Then
                mhRemoteCompanies = New Hashtable
            End If

            If mhRemoteCompanies.Count > 0 AndAlso mhRemoteCompanies.ContainsKey(sCardCode) = True Then
                moComp = mhRemoteCompanies.Item(sCardCode)
                If moComp.Connected Then
                    Return moComp
                Else
                    mhRemoteCompanies.Remove(sCardCode)
                End If
            End If

            Try
                Dim sServer As String = ""
                Dim sCompanyDB As String = ""
                Dim sDbUserName As String = ""
                Dim sDbPassword As String = ""
                Dim sUsername As String = ""
                Dim sPassword As String = ""
                Dim sShareCon As String = ""
                Dim sRecipients As String = ""

                If Not hSettings Is Nothing Then
                    sServer = hSettings.Item("RNSERVR")
                    sCompanyDB = hSettings.Item("RNCOMDB")
                    sDbUserName = hSettings.Item("RNDBUSR")
                    sDbPassword = hSettings.Item("RNDBPSS")
                    sUsername = hSettings.Item("RNCOUSR")
                    sPassword = hSettings.Item("RNCOPSS")
                    sShareCon = hSettings.Item("RNCONSH")
                    sRecipients = hSettings.Item("RNRECPT")

                    moComp = New SAPbobsCOM.Company

                    moComp.Server = sServer
                    moComp.CompanyDB = sCompanyDB
                    moComp.DbUserName = sDbUserName
                    moComp.DbPassword = sDbPassword
                    moComp.UserName = sUsername
                    moComp.Password = sPassword

                    Dim iResult As Integer
                    Dim sResult As String = Nothing
                    iResult = moComp.Connect()
                    If iResult <> 0 Then
                        moComp.GetLastError(iResult, sResult)
                        'com.idh.bridge.DataHandler.INSTANCE.doError("System: Error Connectiong to Remote Company - " & sResult, "Error connecting to the Company.")
                        DataHandler.INSTANCE.doResSystemError("Error Connectiong to Remote Company - " & sResult, "ERSYCRMC", {com.idh.bridge.Translation.getTranslatedWord(sResult)})
                        DataHandler.INSTANCE.doReleaseObject(CType(moComp, Object))
                    Else
                        mhRemoteCompanies.Add(sCardCode, moComp)
                        Return moComp
                    End If
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error connecting to the remote Company.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCRMC", {Nothing})
                DataHandler.INSTANCE.doReleaseObject(CType(moComp, Object))
            Finally
            End Try
            Return Nothing
        End Function

        '** Recipient - UserCode
        '** Mesaage Subject
        '** Message Text
        '** Link tyoe
        '** LinkValue
        Public Function doSendRemoteAlert(ByVal sCardCode As String, ByVal sUserCode As String, ByVal sSubject As String, ByVal sMessage As String, ByVal oLinkType As SAPbouiCOM.BoLinkedObject, ByVal sLinkValue As String, ByVal bDoSend As Boolean) As Boolean
            Dim hSettings As Hashtable = getRemoteSettings(sCardCode)
            If Not hSettings Is Nothing AndAlso hSettings.Count > 0 AndAlso hSettings.Item("FOUND") = True Then
                Dim oCompany As SAPbobsCOM.Company = doConnectRemoteCompany(hSettings)
                If oCompany Is Nothing Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Could not connected to the Remote Client.")
                    DataHandler.INSTANCE.doResSystemError("Could not connected to the Remote Client.", "ERSYRMTE", {Nothing})
                    Return False
                Else
                    Dim sRecpts As String = hSettings.Item("RNRECPT")
                    Return doSendTheRemoteAlert(oCompany, sCardCode, sRecpts, sSubject, sMessage, oLinkType, sLinkValue, bDoSend)
                End If
            Else
                Return True
            End If
        End Function

        '** Recipient - UserCode
        '** Mesaage Subject
        '** Message Text
        '** Link type
        '** LinkValue
        Public Function doSendTheRemoteAlert(ByRef oCompany As SAPbobsCOM.Company, ByVal sCardCode As String, ByVal sRecipients As String, ByVal sSubject As String, ByVal sMessage As String, ByVal oLinkType As SAPbouiCOM.BoLinkedObject, ByVal sLinkValue As String, ByVal bDoSend As Boolean) As Boolean
            Dim oCmpService As SAPbobsCOM.CompanyService
            Dim oMessageService As SAPbobsCOM.MessagesService
            Dim oMessage As SAPbobsCOM.Message
            Dim oLine As SAPbobsCOM.MessageDataLine
            Dim oLines As SAPbobsCOM.MessageDataLines
            Dim pMessageDataColumns As SAPbobsCOM.MessageDataColumns
            Dim pMessageDataColumn As SAPbobsCOM.MessageDataColumn
            Dim oRecipientCollection As SAPbobsCOM.RecipientCollection

            oCmpService = oCompany.GetCompanyService
            oMessageService = oCmpService.GetBusinessService(SAPbobsCOM.ServiceTypes.MessagesService)

            Try
                doSaveAlert(sCardCode, sRecipients, sSubject, sMessage, oLinkType, sLinkValue, bDoSend)
                If bDoSend Then
                    oMessage = oMessageService.GetDataInterface(SAPbobsCOM.MessagesServiceDataInterfaces.msdiMessage)
                    oMessage.Subject = sSubject
                    oMessage.Text = sMessage

                    'Add Recipient
                    oRecipientCollection = oMessage.RecipientCollection

                    Dim sList() As String = sRecipients.Split(",")
                    For iRecp As Integer = 0 To sList.Length - 1
                        oRecipientCollection.Add()
                        oRecipientCollection.Item(0).SendInternal = SAPbobsCOM.BoYesNoEnum.tYES
                        oRecipientCollection.Item(0).UserCode = sList(iRecp)
                    Next

                    'get columns data
                    pMessageDataColumns = oMessage.MessageDataColumns
                    pMessageDataColumn = pMessageDataColumns.Add()
                    pMessageDataColumn.ColumnName = "Linked Object"

                    'set link to a real object in the application
                    pMessageDataColumn.Link = SAPbobsCOM.BoYesNoEnum.tYES

                    'get lines
                    oLines = pMessageDataColumn.MessageDataLines()
                    oLine = oLines.Add()
                    oLine.Value = sLinkValue

                    'set the link to Items
                    If Not oLinkType = Nothing AndAlso Not sLinkValue Is Nothing Then
                        oLine.Object = oLinkType
                        oLine.ObjectKey = sLinkValue
                    End If

                    'send the message
                    oMessageService.SendMessage(oMessage)
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error sending the remote Alert - " & sLinkValue)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSRMI", {com.idh.bridge.Translation.getTranslatedWord("Alert"), sLinkValue})
            End Try
            Return True
        End Function

        '** Recipient - UserCode
        '** Mesaage Subject
        '** Message Text
        '** Link tyoe
        '** LinkValue
        'Public Function doSaveAlert(ByRef oCompany As SAPbobsCOM.Company, ByVal sCardCode As String, ByVal sUserCode As String, ByVal sSubject As String, ByVal sMessage As String, ByVal oLinkType As SAPbouiCOM.BoLinkedObject, ByVal sLinkValue As String, ByVal bDoSend As Boolean) As Boolean
        Public Function doSaveAlert(ByVal sCardCode As String, ByVal sUserCode As String, ByVal sSubject As String, ByVal sMessage As String, ByVal oLinkType As SAPbouiCOM.BoLinkedObject, ByVal sLinkValue As String, ByVal bDoSend As Boolean) As Boolean
            Dim oCompany As SAPbobsCOM.Company = goDICompany
            Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, "Alert")

            Dim oAlertTable As SAPbobsCOM.UserTable = Nothing
            Dim sTable As String = "IDH_WTALERT"
            Dim sVal As String
            Try
                oAlertTable = oCompany.UserTables.Item(sTable)

                oAlertTable.Code = oNumbers.CodeCode
                oAlertTable.Name = oNumbers.NameCode
                oAlertTable.UserFields.Fields.Item("U_CCODE").Value = sCardCode.Trim()
                oAlertTable.UserFields.Fields.Item("U_UCODE").Value = sUserCode.Trim()
                oAlertTable.UserFields.Fields.Item("U_SUBJ").Value = sSubject.Trim()

                sMessage = sMessage.Trim()
                If sMessage.Length > 500 Then
                    sMessage = sMessage.Substring(0, 500)
                End If
                oAlertTable.UserFields.Fields.Item("U_MESS").Value = sMessage

                oAlertTable.UserFields.Fields.Item("U_LNKTP").Value = oLinkType
                If sLinkValue Is Nothing Then
                    sLinkValue = ""
                End If
                oAlertTable.UserFields.Fields.Item("U_LNKVAL").Value = sLinkValue

                If bDoSend Then
                    sVal = "Y"
                Else
                    sVal = "N"
                End If
                oAlertTable.UserFields.Fields.Item("U_DOSEND").Value = sVal

                oAlertTable.UserFields.Fields.Item("U_STAT").Value = "S" 'Saved
                oAlertTable.UserFields.Fields.Item("U_SAVDAT").Value = Date.Now()

                Dim iResult As Integer
                Dim sResult As String = Nothing
                iResult = oAlertTable.Add()
                If iResult <> 0 Then
                    oCompany.GetLastError(iResult, sResult)
                    'com.idh.bridge.DataHandler.INSTANCE.doError("System: Alert Save Error - " + sResult, "Error saving the Alert.")
                    DataHandler.INSTANCE.doResSystemError("Alert Save Error - " + sResult, "ERSYASE", {com.idh.bridge.Translation.getTranslatedWord(sResult)})
                    Return False
                Else
                    Return True
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error saving the Alert.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXASE", {Nothing})
            Finally
                DataHandler.INSTANCE.doReleaseUserTable(oAlertTable)
            End Try
            Return False
        End Function

        Public Sub doReleaseRemoteCompanies(ByVal sCardCode As String, ByVal bForce As Boolean)
            If bForce = False AndAlso Not sCardCode Is Nothing Then
                If Not mhRemoteSettings Is Nothing AndAlso mhRemoteSettings.ContainsKey(sCardCode) Then
                    Dim hSettings As Hashtable = mhRemoteSettings.Item(sCardCode)
                    If hSettings.ContainsKey("RNCONSH") Then
                        Dim sShare As String = hSettings.Item("RNCONSH")
                        If sShare.ToUpper().Equals("TRUE") Then
                            Exit Sub
                        End If
                    End If
                End If
            End If

            If bForce Then
                If Not mhRemoteCompanies Is Nothing Then
                    Dim moComp As SAPbobsCOM.Company

                    If mhRemoteCompanies.Count > 0 Then
                        If sCardCode Is Nothing Then
                            Dim oEnum As IEnumerator
                            Dim sKey As String
                            oEnum = mhRemoteCompanies.Keys.GetEnumerator()
                            While oEnum.MoveNext
                                sKey = oEnum.Current()
                                moComp = mhRemoteCompanies.Item(sKey)
                                If moComp.Connected Then
                                    moComp.Disconnect()
                                    DataHandler.INSTANCE.doReleaseObject(CType(moComp, Object))
                                End If
                                mhRemoteCompanies.Remove(sKey)
                            End While
                        Else
                            moComp = mhRemoteCompanies.Item(sCardCode)
                            If moComp.Connected Then
                                moComp.Disconnect()
                                DataHandler.INSTANCE.doReleaseObject(CType(moComp, Object))
                            End If
                            mhRemoteCompanies.Remove(sCardCode)
                        End If
                    End If
                End If
            End If
        End Sub

        Protected Overrides Sub doClose()
            doCloseFloatingForms()
            doReleaseRemoteCompanies(Nothing, True)

            'OnTime Ticket 26267 - Dispose WR1 menu
            'Dispose/Remove WR1 menu structure from Business One when add-on is disconnected
            'Dim objMenu As SAPbouiCOM.Menus
            Try
                goApplication.Menus.RemoveEx("IDHISB")
            Catch ex As Exception
                ''
            End Try
        End Sub


        '** Handles the menu Event
        Protected Overrides Sub HandleMenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)

            'If pVal.BeforeAction AndAlso pVal.MenuUID = "275" AndAlso APPLICATION.Forms.ActiveForm IsNot Nothing _
            '    AndAlso APPLICATION.Forms.ActiveForm.TypeEx.ToLower.StartsWith("idh") Then
            '    ''call help file
            '    ''"hh.exe WR1_Help.chm::IDH_Topic50.htm#_Toc414457938"
            '    Dim sPath As String = GetAssemblyDirectory()
            '    sPath &= "WR1_Help.chm"
            '    Dim sHelpCall As String = """" & sPath & "::" & APPLICATION.Forms.ActiveForm.TypeEx & ".htm"""
            '    'If APPLICATION.Forms.ActiveForm.ActiveItem IsNot Nothing AndAlso APPLICATION.Forms.ActiveForm.ActiveItem <> "" Then
            '    '    sHelCall &= "#" & APPLICATION.Forms.ActiveForm.ActiveItem
            '    'End If
            '    BubbleEvent = False
            '    Dim psi As New ProcessStartInfo()
            '    psi.UseShellExecute = False
            '    psi.FileName = "hh.exe" ' sHelCall
            '    psi.Arguments = sHelpCall
            '    Process.Start(psi)
            '    Exit Sub
            'ElseIf pVal.BeforeAction AndAlso pVal.MenuUID = "281" AndAlso APPLICATION.Forms.ActiveForm IsNot Nothing _
            'AndAlso APPLICATION.Forms.ActiveForm.TypeEx.ToLower.StartsWith("idh") Then
            '    ''call help file
            '    ''"hh.exe WR1_Help.chm::IDH_Topic50.htm#_Toc414457938"
            '    Dim sPath As String = GetAssemblyDirectory()
            '    sPath &= "WR1_Help.chm"
            '    Dim sHelpCall As String = "" & sPath & "::" & APPLICATION.Forms.ActiveForm.TypeEx & ".htm"
            '    If APPLICATION.Forms.ActiveForm.ActiveItem IsNot Nothing AndAlso APPLICATION.Forms.ActiveForm.ActiveItem <> "" Then
            '        sHelpCall &= "#" & APPLICATION.Forms.ActiveForm.ActiveItem
            '    End If
            '    sHelpCall = """" & sHelpCall & """"
            '    BubbleEvent = False
            '    Dim psi As New ProcessStartInfo()
            '    psi.UseShellExecute = False
            '    psi.FileName = "hh.exe" ' sHelCall
            '    psi.Arguments = sHelpCall
            '    Process.Start(psi)
            '    Exit Sub
            'End If
            MyBase.HandleMenuEvent(pVal, BubbleEvent)

            If pVal.BeforeAction = False Then
                If pVal.MenuUID.Equals("1029") Then
                    Dim oForm As IDHAddOns.idh.forms.Base
                    Dim owForm As SAPbouiCOM.Form
                    Try
                        owForm = goApplication.Forms.ActiveForm()
                    Catch ex As Exception
                        Exit Sub
                    End Try

                    oForm = doGetForm("198")
                    If Not oForm Is Nothing Then
                        If oForm.LoadType = IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL Then
                            oForm.doLoadFormFromMemory(owForm, BubbleEvent)
                            oForm.doAddFormToList(owForm)
                        End If
                    End If
                End If
            End If
        End Sub

        Protected Overrides Sub HandleFormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
            If Config.ParameterAsBool("ENBADSYN", False) AndAlso BusinessObjectInfo.FormTypeEx = "134" AndAlso BusinessObjectInfo.ActionSuccess = True AndAlso BusinessObjectInfo.EventType = SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE Then
                Dim oform As SAPbouiCOM.Form = APPLICATION.Forms.Item(BusinessObjectInfo.FormUID)
                Dim oBP As WR1_SBOForms.idh.forms.SBO.BusinessPartner = doGetForm("134")
                oBP.HandleFormDataEvent(oform, BusinessObjectInfo, BubbleEvent)
                Return
           
                'ElseIf BusinessObjectInfo.FormTypeEx = "651" Then 'AndAlso BusinessObjectInfo.ActionSuccess = True AndAlso BusinessObjectInfo.EventType = SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE Then
                '    Dim oform As SAPbouiCOM.Form = APPLICATION.Forms.Item(BusinessObjectInfo.FormUID)
                '    Dim oActivity As WR1_SBOForms.idh.forms.SBO.Activity = doGetForm("651")
                '    ' Dim oform As SAPbouiCOM.Form = APPLICATION.Forms.Item(BusinessObjectInfo.FormUID)
                '    oActivity.HandleFormDataEvent(oform, BusinessObjectInfo, BubbleEvent)
            ElseIf BusinessObjectInfo.FormTypeEx = "IDH_ENQ" Then
                Dim oform As SAPbouiCOM.Form = APPLICATION.Forms.Item(BusinessObjectInfo.FormUID)
                'NEWCORE-TEMP-BLOCK--> Dim oEnquiry As com.isb.forms.Enquiry.Enquiry = com.idh.forms.oo.Form.doFindForm(oform)
                'NEWCORE-TEMP-BLOCK--> oEnquiry.HandleFormDataEvent(oform, BusinessObjectInfo, BubbleEvent)
            ElseIf BusinessObjectInfo.FormTypeEx = "IDH_WOQ" Then
                'Dim oform As SAPbouiCOM.Form = APPLICATION.Forms.Item(BusinessObjectInfo.FormUID)
                'Dim oWOQ As com.isb.forms.Enquiry.WOQuote = com.idh.forms.oo.Form.doFindForm(oform)
                'oWOQ.HandleFormDataEvent(oform, BusinessObjectInfo, BubbleEvent)
            End If
            MyBase.HandleFormDataEvent(BusinessObjectInfo, BubbleEvent)
        End Sub

        Protected Overrides Sub HandleItemEvent(FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            'If (pVal.FormTypeEx = "IDH_ENQ") Then
            '    goApplication.StatusBar.SetText("ENQ:" & pVal.EventType.ToString() & " BeforeAction:" & pVal.BeforeAction.ToString())
            'End If
            'If (pVal.FormTypeEx = "150") Then
            '    Console.WriteLine("HandleItemEvent--> " + pVal.EventType.ToString())
            'End If
            If pVal.BeforeAction AndAlso _
                (pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE OrElse (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN AndAlso pVal.CharPressed = "27") OrElse _
                 (pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED AndAlso pVal.ItemUID = "2")) _
                AndAlso _
                (pVal.FormTypeEx = "IDH_DRT" OrElse pVal.FormTypeEx = "IDHBPFRCT" OrElse pVal.FormTypeEx = "IDHJOBS" OrElse pVal.FormTypeEx = "IDHLABTODO") _
                AndAlso pVal.FormMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then ' 
                Dim oform As SAPbouiCOM.Form = APPLICATION.Forms.Item(FormUID)
                If pVal.FormTypeEx = "IDHLABTODO" AndAlso CType(oform.Items.Item("1").Specific, SAPbouiCOM.Button).Caption = Translation.getTranslatedWord("Find") Then
                    Return
                End If
                If (pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE AndAlso oform.Visible = False) Then
                    Return
                End If
                If com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYN("WRNUSAVE", Nothing) = 2 Then
                    'only worked for Esc Key with Update Mode; for Add mode in advance forms see next condition
                    BubbleEvent = False
                    Return
                End If
            ElseIf (pVal.FormTypeEx = "IDH_ENQ" OrElse pVal.FormTypeEx = "IDHWOQ") AndAlso (pVal.FormMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse pVal.FormMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE) AndAlso ((pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE) OrElse (pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED AndAlso pVal.ItemUID = "2") OrElse _
                      (pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN AndAlso pVal.CharPressed = "27")) Then
                Dim oform As SAPbouiCOM.Form = APPLICATION.Forms.Item(FormUID)
                If (pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE AndAlso oform.Visible = False) Then
                    Return
                End If
                doHandleFormCloserValidation(FormUID, pVal, BubbleEvent)
                If BubbleEvent = False Then
                    Return
                End If
            ElseIf (pVal.FormTypeEx = "IDHWOQ" OrElse pVal.FormTypeEx = "IDH_ENQ") AndAlso pVal.ItemUID = "LINESGRID" AndAlso pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED AndAlso pVal.Row < 0 Then
                ' goApplication.StatusBar.SetText("Main:" & pVal.EventType.ToString())
                'If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED AndAlso pVal.Row < 0 Then
                BubbleEvent = False
                Return
                'End If
            ElseIf pVal.BeforeAction = False AndAlso pVal.FormTypeEx = "1215000002" AndAlso pVal.EventType = SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK Then
                Dim oForm As IDHAddOns.idh.forms.Base
                Dim owForm As SAPbouiCOM.Form
                Try
                    owForm = goApplication.Forms.ActiveForm()
                Catch ex As Exception
                    Exit Sub
                End Try

                oForm = doGetForm("198")
                If Not oForm Is Nothing Then
                    If oForm.LoadType = IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL Then
                        oForm.doLoadFormFromMemory(owForm, BubbleEvent)
                        oForm.doAddFormToList(owForm)
                    End If
                End If
                End If
            MyBase.HandleItemEvent(FormUID, pVal, BubbleEvent)

        End Sub
#Region "Handle Form closer"
        Private Sub doHandleFormCloserValidation(FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Try
                Dim oform As SAPbouiCOM.Form = APPLICATION.Forms.Item(FormUID)
                Select Case pVal.FormTypeEx
                    Case "IDH_ENQ"
                        'NEWCORE-TEMP-BLOCK--> Dim oOOForm As com.isb.forms.Enquiry.Enquiry = com.isb.forms.Enquiry.Enquiry.doFindForm(oform)
                        'NEWCORE-TEMP-BLOCK--> If oOOForm IsNot Nothing AndAlso (oform.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse (oform.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE AndAlso oOOForm.DBEnquiry.doFormHaveSomeData())) Then
                        'NEWCORE-TEMP-BLOCK--> If (com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYN("WRNUSAVE", Nothing) = 2) Then
                        'NEWCORE-TEMP-BLOCK--> BubbleEvent = False
                        'NEWCORE-TEMP-BLOCK--> End If
                        'NEWCORE-TEMP-BLOCK--> End If

                        'Dim objform As Object
                        'For i As Int16 = 0 To Me.goForms.Count - 1
                        '    objform = Me.goForms(i)
                        '    Try
                        '        If objform.GetType().FullName = "com.idh.forms.oo.FormController" Then
                        '            Dim ofrm As com.idh.forms.oo.FormController = objform

                        '            Dim ofrm2 As IDHAddOns.idh.forms.Base = ofrm.getIDHForm(oform)
                        '            Dim oOOForm As com.isb.forms.Enquiry.Enquiry = com.isb.forms.Enquiry.Enquiry.doFindForm(oform) '(ofrm2, Nothing, oform)
                        '            If (oform.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse (oform.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE AndAlso oOOForm.DBEnquiry.doFormHaveSomeData()) _
                        '                AndAlso com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYN("WRNUSAVE", Nothing) = 2) Then
                        '                'only worked for cancel button
                        '                BubbleEvent = False
                        '            End If
                        '            Exit For
                        '        End If
                        '    Catch ex As Exception

                        '    End Try
                        '    '   If (Me.)objform
                        'Next
                        'Dim oOOForm As com.isb.forms.Enquiry.Enquiry = New com.isb.forms.Enquiry.Enquiry(Nothing, Nothing, oform)
                        ' oOOForm.doCancelButton(pVal, BubbleEvent)
                        'If (oform.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse (oform.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE AndAlso oOOForm.DBEnquiry.doFormHaveSomeData()) _
                        '    AndAlso com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYN("WRNUSAVE", Nothing) = 2) Then
                        '    'only worked for cancel button
                        '    BubbleEvent = False
                        'End If
                        Exit Select
                    Case "IDHWOQ"
                        'NEWCORE-TEMP-BLOCK--> Dim oOOForm As com.isb.forms.Enquiry.WOQuote = com.isb.forms.Enquiry.WOQuote.doFindForm(oform)
                        'NEWCORE-TEMP-BLOCK--> If oOOForm IsNot Nothing AndAlso (oform.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse (oform.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE AndAlso oOOForm.DBWOQ.doFormHaveSomeData()) _
                        'NEWCORE-TEMP-BLOCK-->     ) Then
                        'NEWCORE-TEMP-BLOCK--> If (com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYN("WRNUSAVE", Nothing) = 2) Then
                        'NEWCORE-TEMP-BLOCK--> BubbleEvent = False
                        'NEWCORE-TEMP-BLOCK--> End If
                        'NEWCORE-TEMP-BLOCK--> 'only worked for cancel button
                        'NEWCORE-TEMP-BLOCK--> End If
                        Exit Select
                End Select
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, Nothing, Nothing)
            End Try

        End Sub
#End Region
        Public Function GetAssemblyDirectory() As String
            Dim codeBase As String = Reflection.Assembly.GetExecutingAssembly().CodeBase
            Dim sbUri As New UriBuilder(codeBase)
            Dim spath As String = Uri.UnescapeDataString(sbUri.Path)

            spath = Path.GetDirectoryName(spath)
            If Not spath.EndsWith("\") Then
                spath &= "\"
            End If
            Return spath
        End Function

    End Class
End Namespace
