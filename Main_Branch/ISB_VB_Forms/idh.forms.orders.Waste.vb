

Imports System.IO
Imports System.Collections


Imports IDHAddOns.idh.controls
Imports System
Imports com.idh.utils
Imports com.idh.bridge.lookups
Imports WR1_Grids.idh.controls.grid
Imports com.idh.dbObjects

Imports com.idh.dbObjects.User
Imports System.Threading
Imports com.idh.bridge
Imports com.uBC.utils
Imports com.uBC.data
Imports com.idh.bridge.action
Imports com.idh.dbObjects.numbers
Imports com.idh.bridge.resources

Namespace idh.forms.orders
    Public Class Waste
        Inherits idh.forms.orders.Tmpl

        Shared msMainTable As String = "@IDH_JOBENTR"

        'Status Def
        Shared DRAFT As String = "6"

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            'OnTime [#Ico000????] USA
            'WR Config based WO Header form title
            'MyBase.New(oParent, "IDH_WASTORD", "Waste Order.srf", "WO", "@IDH_JOBENTR", "@IDH_JOBSHD", "Waste Order", iMenuPosition, "Waste Order")
            MyBase.New(oParent, "IDH_WASTORD", "Waste Order.srf", "WO", "@IDH_JOBENTR", "@IDH_JOBSHD", "Waste Order", iMenuPosition, Config.Parameter("WOHNAME"))
            msNextNum = "JOBENTR"
            'msNextNumPrefix = IDH_JOBENTR.AUTONUMPREFIX

            msNextRowNum = "JOBSCHE"
            'msNextRowNumPrefix = IDH_JOBSHD.AUTONUMPREFIX

            'doAddImagesToFix("IDH_FCL")
        End Sub

        Protected Overrides Function getNextNumPrefix() As String
            Return IDH_JOBENTR.AUTONUMPREFIX
        End Function

        Protected Overrides Function getNextRowNumPrefix() As String
            Return IDH_JOBSHD.AUTONUMPREFIX
        End Function

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", False) Then
                Dim sFormTypeId As String = oGridN.getSBOForm().TypeEx
                Dim oform As SAPbouiCOM.Form = oGridN.getSBOForm
                Dim oFormSettings As IDH_FORMSET = getWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId)
                If oFormSettings Is Nothing Then
                    oFormSettings = New IDH_FORMSET()
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, "")
                    setWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId, oFormSettings)
                    oFormSettings.doAddFieldToGrid(oGridN)
                    doAddListFields(oGridN)
                    'oGridN.doSetGridParameters("WO", sJobNr, sRowNr, True, dDocDate, True)

                    oFormSettings.doSyncDB(oGridN)

                Else
                    If oFormSettings.SkipFormSettings Then
                        doAddListFields(oGridN)
                        'oGridN.doSetGridParameters("WO", sJobNr, sRowNr, bCanEdit, dDocDate, True)
                    Else
                        'oGridN.doSetGridParameters("WO", sJobNr, sRowNr, bCanEdit, dDocDate, False)
                        oFormSettings.doAddFieldToGrid(oGridN)
                    End If
                End If
            Else
                doAddListFields(oGridN)
            End If
        End Sub
        Private Sub doAddListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            Dim sField As String
            sField = Config.INSTANCE.doGetProgressQueryStr("")
            oGridN.doAddListField(sField, "Progress", False, -1, "IGNORE", Nothing)
            oGridN.doAddListField("U_Status", "Status", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_PStat", "Purchase", False, -1, Nothing, Nothing)
            oGridN.doAddListField("Code", "LineID", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_JobNr", "Job Entry", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_JobTp", "Order Type", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_RDate", "Req. Start", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_RTime", "Req. Time From", False, 0, "TIME", Nothing)
            oGridN.doAddListField("U_BDate", "Row Booking Date", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_BTime", "Row Booking Time", False, 0, "TIME", Nothing)
            oGridN.doAddListField("U_RTimeT", "Req. Time To", False, 0, "TIME", Nothing)
            oGridN.doAddListField("U_ASDate", "Act. Start", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ASTime", "Act. S-Time", False, -1, "TIME", Nothing)
            oGridN.doAddListField("U_AEDate", "Act End", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_AETime", "Act E-Time", False, -1, "TIME", Nothing)
            oGridN.doAddListField("U_JbToBeDoneDt", "Job To Be Done Date", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_JbToBeDoneTm", "Job To Be Done Time", False, -1, "TIME", Nothing)

            oGridN.doAddListField(IDH_JOBSHD._WasCd, "Waste Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_WasDsc", "Waste Description", False, -1, Nothing, Nothing)

            oGridN.doAddListField("U_ItmGrp", "Item Grp", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ItemCd", "Item Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ItemDsc", "Item Desc.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Serial", "Item Serial No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_SLicNr", "Skip Lic. No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_SLicExp", "Skip Exp.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Lorry", "Vehicle", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Driver", "Driver", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_TRLReg", "2nd Tare Id", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TRLNM", "2nd Tare Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Tip", "Supplier", False, -1, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._SAddress, "Disp Address", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._SAddrsLN, "Disp Address LineNum", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TipWgt", "Sup. Wgt", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_CstWgt", "Cust. Wgt", False, -1, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._TRdWgt, "Sup. Read Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ExpLdWgt", "Expected Load Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_RdWgt", "Read Wgt", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_UOM", "Sales UOM", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_SAORD", "Sales Order", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_TIPPO", "Tipping PO", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Wei1", "Weighbridge Weigh 1", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Wei2", "Weighbridge Weigh 2", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TipCost", "TipCost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TipTot", "TipTot", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TCharge", "TCharge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TCTotal", "TCTotal", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CongCh", "CongCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_DocNum", "DocNum", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Price", "Price", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Discnt", "Discnt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Total", "Total", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_SLicCh", "SLicCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SLicSp", "SLicSp", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_VehTyp", "Vehicle Type", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_DisAmt", "Discount Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TaxAmt", "Tax Amount", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_SupRef", "Carrier Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ProRef", "Producer Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SiteRef", "SiteRef.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CustRef", "Customer Ref No.", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_Weight", "Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Quantity", "Order Quantity", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_JCost", "Order Cost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SAINV", "Sales Invoice", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TPCN", "Disposal Purchase Credit Note", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TCCN", "Customer Credit Note", False, 0, Nothing, Nothing)
            'oGridN.doAddListField("U_SAORD", "Sales Order", False, 0, Nothing, Nothing)
            'oGridN.doAddListField("U_TIPPO", "Tipping PO", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_JOBPO", "Job PO", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ProPO", "Producer PO", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_GRIn", "Goods Receipt In", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ProGRPO", "Producer GR PO", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SODlvNot", "SO Delivery Note", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CustCd", "Customer Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CarrCd", "CarrierCode", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._WHTRNF, "Stock Transfer number", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_ProCd", "ProducerCode", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_CongCd", "Congestion Supplier Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TipNm", "Dispoal Site Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CustNm", "Customer Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CarrNm", "Carrier Name", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_ProNm", "Producer Name", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_SLicNm", "Skip License Supplier Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SCngNm", "Congestion Supplier Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Dista", "Distance", False, 0, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Ser1", "Flash Serial 1", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Ser2", "Flash Serial 2", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_WDt1", "Weigh Date 1", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_WDt2", "Weigh Date 2", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_AddEx", "Additional Expences", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_OrdWgt", "Order Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._CarWgt, "Carrier Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_OrdCost", "Order Cost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_OrdTot", "Order Total", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CusQty", "Customer Qty", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_HlSQty", "Haulage Qty", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CusChr", "Customer Charge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_PUOM", "Purchase UOM", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SLicCQt", "Skip Licence Quantity", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SLicCst", "Skip Licence Cost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SLicCTo", "Skip Licence Cost Total", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SLicCTo", "Skip Licence Cost Total", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CntrNo", "Contract Number", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_WASLIC", "Waste Lic. Reg. No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_PayMeth", "Payment Method", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_PayStat", "Payment Status", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_CCNum", "Credit Card", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_CCType", "Credit Type", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CCStat", "Credit Card Status", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_CCExp", "CCard Expiry", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CCIs", "CCard Issue #", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CCSec", "CCard Security Number", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CCHNum", "CCard House Number", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CCPCd", "CCard PostCode", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_LorryCd", "Vehicle Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SLPO", "Purchase Order", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Covera", "Coverage String", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Comment", "Comments", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_IntComm", "Internal Comments", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_IssQty", "Issue Qty", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_AUOMQt", "Overriding Qty", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_AUOM", "Overriding UOM", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_UseWgt", "Weight to Use", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_WROrd", "WR Link Order", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_WRRow", "WR Link Order Row", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_JobRmD", "Reminder Date", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_JobRmT", "Reminder Time", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_RemNot", "Reminder Message", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_RemCnt", "Reminder Count", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CoverHst", "Coverage Hist", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_User", "User Name.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Branch", "Branch.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TAddCost", "Total Additional Cost.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TAddChrg", "Total Additional Charge.", False, 0, Nothing, Nothing)
            ''## MA Start 10-042014
            oGridN.doAddListField("U_TAddChVat", "Additional Charge VAT", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TAddCsVat", "Additional Cost VAT", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_AddCharge", "Additional Cost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_AddCost", "Additional Charge", False, 0, Nothing, Nothing)
            ''## MA End 10-042014
            oGridN.doAddListField(IDH_JOBSHD._WgtDed, "Weight Deduction", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._ValDed, "Value Deduction", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Obligated", "Obligated.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Origin", "Origin.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CustCs", "Compliancs Scheme", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ConNum", "Hazardous Waste Consignment Note Number", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_WastTNN", "WastTNN.", False, 0, Nothing, Nothing)
            'oGridN.doAddListField("U_HazWCNN", "HazWCNN.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ExtWeig", "ExtWeig.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TChrgVtRt", "Tipping Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_HChrgVtRt", "Haulage Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TCostVtRt", "Tipping Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_PCostVtRt", "Supplier Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_HCostVtRt", "Haulage Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TChrgVtGrp", "Tipping Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_HChrgVtGrp", "Haulage Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TCostVtGrp", "Tipping Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_PCostVtGrp", "Supplier Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_HCostVtGrp", "Haulage Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_VtCostAmt", "Purchase Vat Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CarrReb", "Carrier Rebate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CustReb", "Customer Rebate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_MANPRC", "Manual Prices Set", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_LnkPBI", "Pre Book Instruction", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_DPRRef", "Daily Profitability Report", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_MDChngd", "Changed Marketing Document", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Sched", "Scheduled Job", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_USched", "Un-Scheduled", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_RowSta", "Row Status", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_HaulAC", "Automatic Haulage Weight", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_TAUOMQt", "Disposal Cost Overriding Qty", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_PAUOMQt", "Purchase Overriding Qty", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_PRdWgt", "Producer Read Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ProWgt", "Producer Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ProUOM", "Producer Unit Of Measure", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_PCost", "Producer Cost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_PCTotal", "Producer Coharge Total", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_WgtDed", "Weight Deduction", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_AdjWgt", "Adjusted Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ValDed", "Value Deduction", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_BookIn", "Book into Stock", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_IsTrl", "Trail Load", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._LckPrc, "Price Lock", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._TrnCode, "Transaction Code", False, 0, Nothing, Nothing)

            'Add new fields in Grid
            oGridN.doAddListField("U_ExtComm1", "Ext. Comments1", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ExtComm2", "Ext. Comments2", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CBICont", "Comments", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CBIManQty", "Internal Comments", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CBIManUOM", "Comments", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_MaximoNum", "Maximo No.", False, 0, Nothing, Nothing)

            'Analysis Code - 20150408
            oGridN.doAddListField("U_Analys1", "Analysis Code 1", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Analys2", "Analysis Code 2", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Analys3", "Analysis Code 3", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Analys4", "Analysis Code 4", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_Analys5", "Analysis Code 5", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_TFSReq", "TFS Req", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TFSNum", "TFS Num", False, 0, Nothing, Nothing)

            'EVN Fields 
            oGridN.doAddListField("U_EVNReq", "EVN Req", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_EVNNum", "EVN Num", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_AltWasDsc", "Alternate Waste Description", False, -1, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._OTComments, "Order Type Comments", False, 0, Nothing, Nothing)

            'Multi Currency Pricing 
            oGridN.doAddListField(IDH_JOBSHD._DispCgCc, "Disp. Charge Currency", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._CarrCgCc, "Carr. Charge Currency", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._PurcCoCc, "Purc. Cost Currency", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._DispCoCc, "Disp. Cost Currency", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._CarrCoCc, "Carr. Cost Currency", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._LiscCoCc, "Lisc. Cost Currency", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._RouteCd, "Disposal Route Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._RtCdCode, "Disposal Route Internal Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._Sample, "Sample", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._SampleRef, "Sample Ref No", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_JOBSHD._SampStatus, "Sample Status", False, 0, Nothing, Nothing)

        End Sub
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
            'doAddFormDF(oForm, "IDH_USER", "U_User")

            'doAddFormDF(oForm, "IDH_CUSA2", "U_Address")
            'doAddFormDF(oForm, "IDH_PC2", "U_ZpCd")

            'doAddFormDF(oForm, "IDH_FORECS", "U_ForeCS")
            'doAddUF(oForm, "IDH_FCWC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20)
            'doAddUF(oForm, "IDH_FCREM", SAPbouiCOM.BoDataType.dt_QUANTITY, 5)
            'doAddUF(oForm, "IDH_FCUOM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 5)
            'doAddUF(oForm, "IDH_ONSTE", SAPbouiCOM.BoDataType.dt_QUANTITY, 10)

            'doAddUF(oForm, "IDH_DISPCH", SAPbouiCOM.BoDataType.dt_QUANTITY, 10)
            'doAddUF(oForm, "IDH_HAULCH", SAPbouiCOM.BoDataType.dt_QUANTITY, 10)
            'doAddUF(oForm, "IDH_TOTCH", SAPbouiCOM.BoDataType.dt_QUANTITY, 10)
            'doAddUF(oForm, "IDH_DISPCS", SAPbouiCOM.BoDataType.dt_QUANTITY, 10)
            'doAddUF(oForm, "IDH_HAULCS", SAPbouiCOM.BoDataType.dt_QUANTITY, 10)
            'doAddUF(oForm, "IDH_PROCS", SAPbouiCOM.BoDataType.dt_QUANTITY, 10)
            'doAddUF(oForm, "IDH_TOTCS", SAPbouiCOM.BoDataType.dt_QUANTITY, 10)
            'doAddUF(oForm, "IDH_PROFIT", SAPbouiCOM.BoDataType.dt_QUANTITY, 10)


            ''OnTime [#Ico000????] USA
            ''START
            ''UDF for Mailing Address Tab
            'doAddFormDF(oForm, "IDH_MLACON", "U_MContact")
            'doAddFormDF(oForm, "IDH_MLAADD", "U_MAddress")
            'doAddFormDF(oForm, "IDH_MLASTR", "U_MStreet")
            'doAddFormDF(oForm, "IDH_MLABLO", "U_MBlock")
            'doAddFormDF(oForm, "IDH_MLACIT", "U_MCity")
            'doAddFormDF(oForm, "IDH_MLAPOS", "U_MZpCd")
            'doAddFormDF(oForm, "IDH_MLAPHO", "U_MPhone1")

            'oForm.Items.Item("IDH_TABMLA").AffectsFormMode = False

            ''END
            ''OnTime [#Ico000????] USA

            'doAddFormDF(oForm, "IDH_MLASTA", "U_MState") 'Mailing Address Tab 
            ''##MA Start 12-09-2014 Issue#413
            'doAddFormDF(oForm, "IDH_MINJOB", "U_MinJob")
            ''##MA End 12-09-2014 Issue#413
            ''LINK BUTTONS
            'Dim oLink As SAPbouiCOM.LinkedButton
            'oLink = oForm.Items.Item("IDH_FCWCL").Specific
            'oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_Items

        End Sub

        Private Function thisWO(ByVal oForm As SAPbouiCOM.Form) As IDH_JOBENTR
            Dim oWO As IDH_JOBENTR = getWFValue(oForm, "WasteOrder")
            If oWO Is Nothing Then
                oWO = New IDH_JOBENTR(Nothing, oForm)
                oWO.MustLoadChildren = False
                doAddWF(oForm, "WasteOrder", oWO)
            End If
            Return oWO
        End Function

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            Dim oWO As IDH_JOBENTR = thisWO(oForm)
            doAddWF(oForm, "WasteOrder", oWO)


            doAddWF(oForm, "CALLEDFROMFC", False)
            doAddWF(oForm, "FCEXPWGT", 0)
            doAddWF(oForm, "TIPCHRG", 0)
            doAddWF(oForm, "TIPCST", 0)
            doAddWF(oForm, "HAULAGECHRG", 0)
            doAddWF(oForm, "HAULAGECST", 0)
            doAddWF(oForm, "PRODCST", 0)
            doAddWF(oForm, "CONTAINER", "")
            doAddWF(oForm, "FCBPCURR", "")

            doSwitchForecastItems(oForm)
            doSwitchContractItems(oForm)
            doFillUsers(oForm)

            FillCombos.TransactionCodeCombo(FillCombos.getCombo(oForm, "uBC_TRNCd"), Nothing, Nothing, "WO")

            If Config.Parameter("MDWFBP").ToUpper().Equals("TRUE") Then
                mbFolderFollowDB = True
            Else
                mbFolderFollowDB = False
            End If
            MyBase.doBeforeLoadData(oForm)
            doSetPreselectData(oForm)

            If ghOldDialogParams.Contains(oForm.UniqueID) Then
                Dim oWrkData As Object = ghOldDialogParams.Item(oForm.UniqueID)
                If oWrkData Is GetType(IDH_BPFORECST) Then
                    Dim oForcast As IDH_BPFORECST = oWrkData
                    setWFValue(oForm, "CALLEDFROMFC", True)
                    setUFValue(oForm, "IDH_FCWC", oForcast.U_ItemCd)
                    setUFValue(oForm, "IDH_FCREM", oForcast.U_RemQty)
                    setUFValue(oForm, "IDH_FCUOM", oForcast.U_UOM)
                    setWFValue(oForm, "FCCADTYPE", oForcast.U_CardType)
                    setWFValue(oForm, "FCEXPWGT", oForcast.U_ExpLdWgt)
                    setWFValue(oForm, "TIPCHRG", oForcast.U_SellingPrice)
                    setWFValue(oForm, "PRODCST", oForcast.U_BuyingPrice)
                    setWFValue(oForm, "FCBPCURR", oForcast.U_BPCurr)
                    '## MA Start 10-09-2015 Issue#909
                    setUFValue(oForm, "IDH_ALFCWC", oForcast.U_AltWDSCd)
                    '## MA End 10-09-2015 Issue#909

                    'setFormDFValue(oForm, IDH_JOBENTR._CustRef, oForcast.U_CustRef)

                    setWFValue(oForm, "FCDBObject", oForcast)
                Else
                    Dim oData As ArrayList = oWrkData
                    If Not (oData Is Nothing) AndAlso oData.Count > 1 Then
                        Dim sAction As String = oData.Item(0)
                        If sAction.Equals("FCDBOUpdate") Then
                            ' 0 - Action
                            ' 1 - WO Code
                            ' 2 - IDH_BPFORECST
                            Dim oForcast As IDH_BPFORECST = oData.Item(2)
                            setWFValue(oForm, "CALLEDFROMFC", True)
                            setUFValue(oForm, "IDH_FCWC", oForcast.U_ItemCd)
                            setUFValue(oForm, "IDH_FCREM", oForcast.U_RemQty)
                            setUFValue(oForm, "IDH_FCUOM", oForcast.U_UOM)
                            setWFValue(oForm, "FCCADTYPE", oForcast.U_CardType)
                            setWFValue(oForm, "FCEXPWGT", oForcast.U_ExpLdWgt)
                            setWFValue(oForm, "TIPCHRG", oForcast.U_SellingPrice)
                            setWFValue(oForm, "PRODCST", oForcast.U_BuyingPrice)
                            setWFValue(oForm, "FCBPCURR", oForcast.U_BPCurr)

                            setFormDFValue(oForm, IDH_JOBENTR._CustRef, oForcast.U_CustRef)
                            '## MA Start 10-09-2015 Issue#909
                            setUFValue(oForm, "IDH_ALFCWC", oForcast.U_AltWDSCd)
                            '## MA End 10-09-2015 Issue#909
                            '## Start 20-09-2013 When WOH open from Forecast then it shows Customer Ref in Carrier Ref, So to fix it write down following line
                            'It is bcz of line           getFormMainDataSource(oForm).Query(conds) in dobeforeLoad in Template class
                            setFormDFValue(oForm, IDH_JOBENTR._SupRef, "")
                            '## End
                            setWFValue(oForm, "FCDBObject", oForcast)
                        ElseIf sAction.Equals("FCUpdate") Then
                            ' 0 - Action
                            ' 1 - WO Code
                            ' 2 - Waste Code
                            ' 3 - Remainder
                            ' 4 - UOM
                            ' 5 - CardType
                            ' 6 - Expected Load Weight
                            ' 7 - Customer Reference Number

                            setWFValue(oForm, "CALLEDFROMFC", True)
                            setUFValue(oForm, "IDH_FCWC", oData.Item(2))
                            setUFValue(oForm, "IDH_FCREM", oData.Item(3))
                            setUFValue(oForm, "IDH_FCUOM", oData.Item(4))
                            setWFValue(oForm, "FCCADTYPE", oData.Item(5))
                            setWFValue(oForm, "FCEXPWGT", oData.Item(6))
                            setFormDFValue(oForm, IDH_JOBENTR._CustRef, oData.Item(7))
                        End If
                    End If
                End If
            End If

            Dim sCustomer As String = getFormDFValue(oForm, IDH_JOBENTR._CardCd)
            Dim sProducer As String = getFormDFValue(oForm, IDH_JOBENTR._PCardCd)
            FillCombos.TransactionCodeCombo(FillCombos.getCombo(oForm, "uBC_TRNCd"), sCustomer, sProducer, "WO")

        End Sub

        Private Sub doSwitchContractItems(ByVal oForm As SAPbouiCOM.Form)
            Dim sContract As String = getFormDFValue(oForm, IDH_JOBENTR._CntrNo)
            If sContract.Length > 0 Then
                setEnableItem(oForm, False, "IDH_CONTRN")
            Else
                setEnableItem(oForm, True, "IDH_CONTRN")
            End If
        End Sub

        Private Sub doSwitchForecastItems(ByVal oForm As SAPbouiCOM.Form)
            If Config.INSTANCE.doGetDoForecasting() Then
                oForm.Items.Item("IDH_FORECS").Visible = True
                oForm.Items.Item("181").Visible = True
                oForm.Items.Item("IDH_FCL").Visible = True

                oForm.Items.Item("183").Visible = True
                oForm.Items.Item("IDH_FCWC").Visible = True
                oForm.Items.Item("IDH_FCWCL").Visible = True
                oForm.Items.Item("185").Visible = True
                oForm.Items.Item("IDH_FCREM").Visible = True
                oForm.Items.Item("188").Visible = True
                oForm.Items.Item("IDH_FCUOM").Visible = True
            Else
                oForm.Items.Item("IDH_FORECS").Visible = False
                oForm.Items.Item("181").Visible = False
                oForm.Items.Item("IDH_FCL").Visible = False

                oForm.Items.Item("183").Visible = False
                oForm.Items.Item("IDH_FCWC").Visible = False
                oForm.Items.Item("IDH_FCWCL").Visible = False
                oForm.Items.Item("185").Visible = False
                oForm.Items.Item("IDH_FCREM").Visible = False
                oForm.Items.Item("188").Visible = False
                oForm.Items.Item("IDH_FCUOM").Visible = False

                'oForm.Items.Item("254").Visible = False
                'oForm.Items.Item("IDH_ALFCWC").Visible = False


            End If
        End Sub

        Private Sub doFillContracts(ByVal oForm As SAPbouiCOM.Form)
            If oForm.Items.Item("IDH_CONTRN").Enabled Then
                Dim sCustomer As String = getFormDFValue(oForm, "U_CardCd")
                Dim sContract As String = getFormDFValue(oForm, "U_CntrNo")
                Dim sWhere As String = "U_Cust = '" & sCustomer & "' AND ( Code = '" & sContract & "' Or Code Not In (Select U_CntrNo From [@IDH_JOBENTR] WHERE U_CntrNo Is Not NULL And U_CntrNo != ''))"

                doFillCombo(oForm, "IDH_CONTRN", "[@IDH_WASTCONT]", "Code", "Code", sWhere, "CAST(Code As Numeric)", "", "")
                'doFillCombo(oForm, "IDH_CONTRN", "[@IDH_WASTCONT]", "Code", "Code", sWhere, "Code", "", "")
            End If
        End Sub

        Private Sub doFillUsers(ByVal oForm As SAPbouiCOM.Form)
            doFillCombo(oForm, "IDH_USER", "OUSR", "USERID", "U_NAME")
        End Sub

        Protected Overrides Sub doClickFirstFocus(ByVal oForm As SAPbouiCOM.Form)
            Dim sFocusField As String = Config.INSTANCE.doGetFirstWOFocusField()
            doSetFocus(oForm, sFocusField)
        End Sub

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            Try
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    If Not oForm.PaneLevel = 3 Then
                        oForm.PaneLevel = 3
                        doSetFocus(oForm, "IDH_TABCUS")
                    End If
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    If Not oForm.PaneLevel = 1 Then
                        oForm.PaneLevel = 1
                        doSetFocus(oForm, "IDH_TABMAT")
                    End If
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    If Not oForm.PaneLevel = 1 Then
                        oForm.PaneLevel = 1
                        doSetFocus(oForm, "IDH_TABMAT")
                    End If
                Else
                    If Not oForm.PaneLevel = 1 Then
                        oForm.PaneLevel = 1
                        doSetFocus(oForm, "IDH_TABMAT")
                    End If
                End If

                MyBase.doLoadData(oForm)

                Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                Dim dQty As Double = oGridN.getOnSiteQty()
                setUFValue(oForm, "IDH_ONSTE", dQty)

                Dim sStatus As String = getFormDFValue(oForm, "U_Status")
                If dQty > 0 AndAlso sStatus = "9" Then
                    setFormDFValue(oForm, "U_Status", "1")
                End If

                Dim sCode As String = getFormDFValue(oForm, "Code")
                Dim oData As WR1_Data.idh.data.AdditionalExpenses = Nothing
                oData = getWFValue(oForm, "JobAdditionalHandler")
                If oData Is Nothing Then
                    oData = New WR1_Data.idh.data.AdditionalExpenses(goParent, "[@IDH_WOADDEXP]")
                    setWFValue(oForm, "JobAdditionalHandler", oData)
                End If
                oData.GetByJob(sCode)

                Dim sCustCodeHere As String = getParentSharedData(oForm, "IDH_CUST")
                If Not sCustCodeHere Is Nothing Then
                    If (sCustCodeHere.Length > 0) Then
                        setFormDFValue(oForm, "U_CardCd", sCustCodeHere)
                    End If
                End If

                Dim sCustomer As String = getFormDFValue(oForm, "U_CardCd")
                Dim sProducerCd As String = getFormDFValue(oForm, "U_PCardCd")
                If (Not sCustomer Is Nothing AndAlso sCustomer.Length > 0) OrElse (Not sProducerCd Is Nothing AndAlso sProducerCd.Length > 0) Then
                    oForm.Items.Item("IDH_LASTM").Visible = True
                    oForm.Items.Item("IDH_WABT").Visible = True
                    oForm.Items.Item("IDH_OSM").Visible = True
                    oForm.Items.Item("IDH_ONSITE").Visible = False

                    Dim sPhone As String = getFormDFValue(oForm, "U_SiteTl")
                    If sPhone.Length = 0 Then
                        sPhone = getFormDFValue(oForm, "U_Phone1")
                        setFormDFValue(oForm, "U_SiteTl", sPhone)
                    End If
                Else
                    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        oForm.Items.Item("IDH_LASTM").Visible = False
                        oForm.Items.Item("IDH_WABT").Visible = False
                        oForm.Items.Item("IDH_OSM").Visible = False
                        oForm.Items.Item("IDH_ONSITE").Visible = False
                    End If

                End If

                doSwitchSkip(oForm)
                oForm.DataSources.UserDataSources.Item("SKPCheck").ValueEx = "N"

                Dim bCalledFromFC As Boolean = getWFValue(oForm, "CALLEDFROMFC")
                Dim oForecast As User.IDH_BPFORECST = Nothing
                If bCalledFromFC = True Then
                    oForecast = getWFValue(oForm, "FCDBObject")

                    If oForecast Is Nothing Then
                        Dim sForecast As String = getFormDFValue(oForm, "U_ForeCS")
                        If Not sForecast Is Nothing AndAlso sForecast.Length > 0 Then
                            oForecast = New User.IDH_BPFORECST()
                            setWFValue(oForm, "FCDBObject", oForecast)

                            If oForecast.getByKey(sForecast) Then
                                setWFValue(oForm, "FCDBObject", oForecast)
                            Else
                                oForecast = Nothing
                            End If
                        End If
                    Else
                        'Start 
                        If Config.INSTANCE.doGetDoForecasting() Then
                            Dim sForecast As String = getFormDFValue(oForm, "U_ForeCS")
                            'If sForecast.Length > 0 Then
                            'End If
                            'oForecast = New User.IDH_BPFORECST()
                            'Dim oWO As String = getFormDFValue(oForm, "Code")
                            'If oForecast.getByKey(sForecast) Then
                            'End If
                            If sForecast.Length > 0 Then
                                setUFValue(oForm, "IDH_FCWC", oForecast.U_ItemCd)
                                setUFValue(oForm, "IDH_FCREM", oForecast.U_RemCQty)
                                setUFValue(oForm, "IDH_FCUOM", oForecast.U_UOM)

                                setWFValue(oForm, "FCCADTYPE", oForecast.U_CardType)
                                setWFValue(oForm, "FCEXPWGT", oForecast.U_ExpLdWgt)
                                setWFValue(oForm, "FCBPCURR", oForecast.U_BPCurr)
                                '## MA Start 10-09-2015 Issue#909
                                setUFValue(oForm, "IDH_ALFCWC", oForecast.U_AltWDSCd)
                                '## MA End 10-09-2015 Issue#909
                                '## MA Start 10-09-2015 Issue#928
                                setEnableItem(oForm, False, "uBC_TRNCd")
                                '## MA End 10-09-2015 Issue#928
                                Dim sCustRef As String = getFormDFValue(oForm, IDH_JOBENTR._CustRef)
                                If sCustRef Is Nothing OrElse sCustRef.Length = 0 Then
                                    setFormDFValue(oForm, IDH_JOBENTR._CustRef, oForecast.U_CustRef)
                                End If
                            End If
                        End If
                        'End 
                    End If
                    doSwitchToAdd(oForm)
                Else
                    setWFValue(oForm, "FCDBObject", Nothing)
                    setUFValue(oForm, "IDH_FCWC", "")
                    setUFValue(oForm, "IDH_ALFCWC", "")
                    setUFValue(oForm, "IDH_FCREM", 0)
                    setUFValue(oForm, "IDH_FCUOM", Config.INSTANCE.getDefaultUOM())
                    setWFValue(oForm, "FCCADTYPE", "C")
                    setWFValue(oForm, "FCEXPWGT", 0)
                    setWFValue(oForm, "FCBPCURR", "")
                    '## MA Start 10-09-2015 Issue#928
                    'setEnableItem("uBC_TRNCd", True)
                    '## MA End 10-09-2015 Issue#928
                    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        doSwitchToFind(oForm)
                    ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        doSwitchToAdd(oForm)
                    ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE OrElse
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then


                        If Config.INSTANCE.doGetDoForecasting() Then
                            Dim sForecast As String = getFormDFValue(oForm, "U_ForeCS")
                            If sForecast.Length > 0 Then
                                oForecast = New User.IDH_BPFORECST()
                                setWFValue(oForm, "FCDBObject", oForecast)

                                'Dim oWO As String = getFormDFValue(oForm, "Code")
                                If oForecast.getByKey(sForecast) Then
                                    setUFValue(oForm, "IDH_FCWC", oForecast.U_ItemCd)
                                    setUFValue(oForm, "IDH_FCREM", oForecast.U_RemCQty)
                                    setUFValue(oForm, "IDH_FCUOM", oForecast.U_UOM)

                                    setWFValue(oForm, "FCCADTYPE", oForecast.U_CardType)
                                    setWFValue(oForm, "FCEXPWGT", oForecast.U_ExpLdWgt)
                                    setWFValue(oForm, "TIPCHRG", oForecast.U_SellingPrice)
                                    setWFValue(oForm, "PRODCST", oForecast.U_BuyingPrice)
                                    setWFValue(oForm, "FCBPCURR", oForecast.U_BPCurr)
                                    '## MA Start 10-09-2015 Issue#909
                                    setUFValue(oForm, "IDH_ALFCWC", oForecast.U_AltWDSCd)
                                    '## MA End 10-09-2015 Issue#909
                                    '## MA Start 10-09-2015 Issue#928
                                    setEnableItem(oForm, False, "uBC_TRNCd")
                                    '## MA End 10-09-2015 Issue#928
                                    Dim sCustRef As String = getFormDFValue(oForm, IDH_JOBENTR._CustRef)
                                    If sCustRef Is Nothing OrElse sCustRef.Length = 0 Then
                                        setFormDFValue(oForm, IDH_JOBENTR._CustRef, oForecast.U_CustRef)
                                    End If
                                End If
                            End If
                        End If
                        doSwitchToAdd(oForm)
                    End If
                End If

                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    doSetAddDefaults(oForm)
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    doSetFindDefaults(oForm)
                End If

                setEnableItem(oForm, False, "IDH_PNA")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error loading the data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXLDBD", {Nothing})
            End Try
            oForm.Freeze(False)
        End Sub

        '** Load Defaults on new entry
        Public Overrides Sub doSetAddDefaults(ByVal oForm As SAPbouiCOM.Form)
            Dim sCarrCode As String
            Dim sSiteCode As String
            Dim sDefaultContainerGrp As String

            sCarrCode = Config.Parameter("MDDEWC")
            sSiteCode = Config.Parameter("MDDEDI")
            sDefaultContainerGrp = Config.Parameter("WODCOG")

            If Not sCarrCode Is Nothing AndAlso sCarrCode.Length > 0 Then
                If sCarrCode.Equals("DB") Then
                    setFormDFValue(oForm, "U_CCardCd", goParent.goDICompany.CompanyName)
                    setFormDFValue(oForm, "U_CCardNM", goParent.goDICompany.CompanyName)

                    Dim oData As ArrayList
                    oData = WR1_Search.idh.forms.search.AddressSearch.doGetCompanyAddress(goParent)
                    doSetCarrierAddress(oForm, oData)
                    setEnableItem(oForm, True, "IDH_ADDRES")
                ElseIf sCarrCode.Equals("LBPCUST") Then
                    Dim sCarrierCd As String = getFormDFValue(oForm, IDH_JOBENTR._CCardCd)
                    Dim sCustomerCd As String = getFormDFValue(oForm, IDH_JOBENTR._CardCd)
                    If Not sCustomerCd Is Nothing AndAlso sCustomerCd.Length <> 0 AndAlso (sCarrierCd Is Nothing OrElse sCarrierCd.Length = 0) Then
                        Dim oLinkBP As LinkedBP = New LinkedBP()
                        If oLinkBP.doGetLinkedBP(sCustomerCd) Then
                            setFormDFValue(oForm, "U_CCardCd", oLinkBP.LinkedBPCardCode)
                            setFormDFValue(oForm, "U_CCardNM", oLinkBP.LinkedBPName)
                            setEnableItem(oForm, True, "IDH_ADDRES")
                        End If
                    End If

                Else
                    If WR1_Search.idh.forms.search.BPSearch.doGetBPInfo(goParent, oForm, sCarrCode) Then
                        doSetChoosenCarrier(oForm)
                    End If
                End If
            End If

            If Not sSiteCode Is Nothing AndAlso sSiteCode.Length > 0 Then
                If sSiteCode.Equals("DB") Then
                    setFormDFValue(oForm, "U_SCardCd", goParent.goDICompany.CompanyName)
                    setFormDFValue(oForm, "U_SCardNM", goParent.goDICompany.CompanyName)

                    setEnableItem(oForm, True, "IDH_SITADD")
                    Dim oAddressData As ArrayList
                    oAddressData = WR1_Search.idh.forms.search.AddressSearch.doGetCompanyAddress(goParent)
                    doSetSiteAddress(oForm, oAddressData)
                ElseIf sSiteCode.Equals("USERWH") Then
                    Dim sDisposalSiteCode As String = Config.INSTANCE.doGetUserWarehouseDisposalBPCode()
                    Dim sDisposalSiteName As String
                    If sDisposalSiteCode IsNot Nothing AndAlso sDisposalSiteCode.Length > 0 Then
                        sDisposalSiteName = Config.INSTANCE.doGetBPName(sDisposalSiteCode)

                        setFormDFValue(oForm, "U_SCardCd", sDisposalSiteCode)
                        setFormDFValue(oForm, "U_SCardNM", sDisposalSiteName)

                        Dim oAddressData As ArrayList
                        Dim sAddress As String = Config.INSTANCE.doGetUserWarehouseAddress()
                        If sAddress IsNot Nothing AndAlso sAddress.Length > 0 Then
                            oAddressData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sDisposalSiteCode, "S", sAddress)
                        Else
                            oAddressData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sDisposalSiteCode, "S")
                        End If
                        setEnableItem(oForm, True, "IDH_SITADD")
                        doSetSiteAddress(oForm, oAddressData)
                    End If
                Else
                    If WR1_Search.idh.forms.search.BPSearch.doGetBPInfo(goParent, oForm, sSiteCode) Then
                        doSetChoosenSite(oForm)
                    End If
                End If
            End If

            If Not sDefaultContainerGrp Is Nothing AndAlso sDefaultContainerGrp.Length > 0 Then
                setFormDFValue(oForm, "U_ItemGrp", sDefaultContainerGrp)
            End If
        End Sub

        Protected Overrides Sub doSwitchToFind(ByVal oForm As SAPbouiCOM.Form)
            Dim oExclude() As String = {"IDH_CUST", "IDH_BOOREF"}

            '** Override for SBO focus / Disable issue
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDH_BOOREF")
            oItem.Enabled = True
            oItem.Click()
            '** End

            DisableAllEditItems(oForm, oExclude)
        End Sub

        Protected Overrides Sub doSwitchToAdd(ByVal oForm As SAPbouiCOM.Form)
            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE OrElse
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then

                '** Override for SBO focus / Disable issue
                Dim oItem As SAPbouiCOM.Item
                Dim oForecast As User.IDH_BPFORECST = getWFValue(oForm, "FCDBObject")
                If Not oForecast Is Nothing Then
                    Dim sForecastBPType As String = oForecast.U_CardType
                    If sForecastBPType = "C" Then
                        oItem = oForm.Items.Item("IDH_WPRODU")
                        If oItem.Enabled = True Then
                            oItem.Click()
                        End If
                    ElseIf sForecastBPType = "S" Then
                        oItem = oForm.Items.Item("IDH_CUST")
                        If oItem.Enabled = True Then
                            oItem.Click()
                        End If
                    End If
                Else
                    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        oItem = oForm.Items.Item("IDH_CUST")
                        oItem.Enabled = True
                        oItem.Click()
                    Else
                        oItem = oForm.Items.Item("IDH_CARRIE")
                        oItem.Enabled = True
                        oItem.Click()
                    End If
                End If

                '** End

                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                Dim iCount As Integer = oGridN.getRowCount
                If iCount > 1 Then
                    '                    Dim oExclude() As String = {"IDH_USER", "IDH_BOOREF", "IDH_STATUS", "IDH_CUST", "IDH_CUSTNM", "IDH_CUSTL", "IDH_CHKCA", "IDH_ORIGIN", _
                    '                    	"IDH_WPRODU", "IDH_WNAM", "IDH_CUSADD", "IDH_CUSPOS", "IDH_CUSA2", "IDH_CUSSTR", "IDH_CUSBLO", "IDH_CUSCIT", "IDH_COUNTY", _
                    '                    "IDH_PC2", "IDH_CUSPHO", "IDH_SITETL", "IDH_IGRP" }

                    'Dim oExclude() As String = {"IDH_USER", "IDH_BOOREF", "IDH_CUST", "IDH_CUSTNM", "IDH_CUSTL", "IDH_CHKCA", "IDH_ORIGIN", _
                    '    "IDH_WPRODU", "IDH_WNAM", "IDH_CUSADD", "IDH_CUSPOS", "IDH_CUSA2", "IDH_CUSSTR", "IDH_CUSBLO", "IDH_CUSCIT", "IDH_COUNTY", _
                    '"IDH_PC2", "IDH_CUSPHO", "IDH_SITETL", "IDH_IGRP"}

                    Dim oExclude() As String = {"IDH_USER", "IDH_MINJOB", "IDH_BOOREF", "IDH_CUST", "IDH_CUSTNM", "IDH_CUSTL", "IDH_CHKCA", "IDH_WPRODU", "IDH_WNAM", "IDH_IGRP", "IDH_CFNAME"}

                    Dim PBI_COde As String = getFormDFValue(oForm, "U_PBICODE")
                    If Config.ParamaterWithDefault("OWOCUADR", False) = False OrElse oForecast IsNot Nothing OrElse (PBI_COde IsNot Nothing AndAlso PBI_COde.Trim <> "") Then
                        oExclude = General.doCombineArrays(oExclude, New String() {"IDH_CUSADD", "IDH_CUSPOS", "IDH_CUSA2", "IDH_CUSSTR", "IDH_CUSBLO",
                                        "IDH_CUSCIT", "IDH_PC2", "IDH_COUNTY", "IDH_CUSPHO", "IDH_SITETL"})
                    End If
                    If Not oForecast Is Nothing Then
                        oExclude = General.doCombineArrays(oExclude, New String() {"uBC_TRNCd"})
                    End If
                    EnableAllEditItems(oForm, oExclude)

                    'The Lookup buttons
                    setEnableItem(oForm, False, "IDH_CUSL")
                    setEnableItem(oForm, False, "IDH_ORIGLU")
                    setEnableItem(oForm, False, "IDH_PRDL")
                    setEnableItem(oForm, False, "IDH_CUSAL")
                Else
                    '                    Dim oExclude() As String = {"IDH_USER", "IDH_BOOREF", "IDH_STATUS", "IDH_CHKCA"}
                    Dim oExclude() As String
                    If Not oForecast Is Nothing Then
                        Dim sForecastBPType As String = oForecast.U_CardType
                        If sForecastBPType = "C" Then
                            oExclude = New String() {"IDH_USER", "IDH_MINJOB", "IDH_BOOREF", "IDH_CHKCA", "IDH_CUST", "IDH_CUSTNM", "IDH_CFNAME", "IDH_CUSADD", "IDH_CUSPOS",
                                                     "IDH_CUSA2", "IDH_CUSSTR", "IDH_CUSBLO", "IDH_CUSCIT", "IDH_COUNTY",
                                                    "IDH_PC2", "uBC_TRNCd"}
                            setEnableItem(oForm, False, "IDH_CUSL")
                            setEnableItem(oForm, True, "IDH_PRDL")
                        ElseIf sForecastBPType = "S" Then
                            oExclude = New String() {"IDH_USER", "IDH_MINJOB", "IDH_BOOREF", "IDH_CHKCA", "IDH_WPRODU", "IDH_WNAM", "uBC_TRNCd"}
                            setEnableItem(oForm, True, "IDH_CUSL")
                            setEnableItem(oForm, False, "IDH_PRDL")
                        Else
                            oExclude = New String() {"IDH_USER", "IDH_MINJOB", "IDH_BOOREF", "IDH_CHKCA"}
                            setEnableItem(oForm, True, "IDH_CUSL")
                            setEnableItem(oForm, True, "IDH_PRDL")
                        End If

                        EnableAllEditItems(oForm, oExclude)
                    Else
                        oExclude = New String() {"IDH_USER", "IDH_MINJOB", "IDH_BOOREF", "IDH_CHKCA", "IDH_CFNAME", "IDH_CUSADD", "IDH_CUSA2", "IDH_ADDRES", "IDH_PRDADD", "IDH_SITADD"}
                        EnableAllEditItems(oForm, oExclude)
                        setEnableItem(oForm, True, "IDH_CUSL")
                        setEnableItem(oForm, True, "IDH_PRDL")
                    End If

                    setEnableItem(oForm, True, "IDH_ORIGLU")
                    setEnableItem(oForm, True, "IDH_CUSAL")
                End If
                'USA Requirement
                'Set Status dropdown enable based on User Profile
                'If Config.INSTANCE.getParameterAsBool("USAREL", False) Then
                MyBase.doSetUserAuthorizationForStatus(oForm)
                'End If

                'Set Forecast Fields always disabled 
                '"IDH_FORECS", "IDH_FCL", "IDH_FCWC", "IDH_FCREM", "IDH_FCUOM"
                oForm.Items.Item("IDH_FORECS").Enabled = False
                oForm.Items.Item("IDH_FCL").Enabled = False
                oForm.Items.Item("IDH_FCWC").Enabled = False
                oForm.Items.Item("IDH_ALFCWC").Enabled = False

                oForm.Items.Item("IDH_FCREM").Enabled = False
                oForm.Items.Item("IDH_FCUOM").Enabled = False

            End If
        End Sub

        Private Sub doSwitchSkip(ByVal oForm As SAPbouiCOM.Form)
            Dim sGrp As String
            Dim sCode As String = getFormDFValue(oForm, "Code")
            'Dim sOffR As String
            Dim bIsSkip As Boolean = False
            Dim bIsOffRoad As Boolean = False

            Dim oValidValue As SAPbouiCOM.ValidValue
            Dim oCombo As SAPbouiCOM.ComboBox

            oCombo = oForm.Items.Item("IDH_IGRP").Specific
            oValidValue = oCombo.Selected()

            If oValidValue Is Nothing Then
                With getFormMainDataSource(oForm)
                    sGrp = .GetValue("U_ItemGrp", .Offset).Trim()
                End With
            Else
                sGrp = oValidValue.Value().Trim()
            End If

            If Not sGrp Is Nothing And sGrp.Length > 0 Then
                Dim sSkipGrp As String = Config.Parameter("IGR-SKIP")

                If Not (sSkipGrp Is Nothing) AndAlso sSkipGrp = sGrp Then
                    bIsSkip = True
                    Dim sOffRoad As String = getFormDFValue(oForm, "U_ORoad")
                    If sOffRoad.Equals("Y") Then
                        bIsOffRoad = True
                    End If
                    '                    Dim oCheck As SAPbouiCOM.CheckBox
                    '                    oCheck = oForm.Items.Item("IDH_CHECK").Specific
                    '                    If oCheck.Checked Then
                    '                        bIsOffRoad = True
                    '                    End If
                Else
                    Dim sLicContainers As String = Config.Parameter("IGR-LCON")
                    If Not sLicContainers Is Nothing AndAlso sLicContainers.Length > 0 Then
                        Dim oList() As String = sLicContainers.Split(",")

                        Dim iCount As Integer
                        Dim sGroup As String
                        For iCount = 0 To oList.Length() - 1
                            sGroup = oList(iCount).Trim()
                            If sGroup.Length > 0 Then
                                If sGroup = sGrp Then
                                    bIsSkip = True
                                    Dim sOffRoad As String = getFormDFValue(oForm, "U_ORoad")
                                    If sOffRoad.Equals("Y") Then
                                        bIsOffRoad = True
                                    End If
                                    '                    				Dim oCheck As SAPbouiCOM.CheckBox
                                    '                    				oCheck = oForm.Items.Item("IDH_CHECK").Specific
                                    '                    				If oCheck.Checked Then
                                    '                        				bIsOffRoad = True
                                    '                    				End If
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                End If
            End If

            oForm.Items.Item("IDH_CHECK").Visible = bIsSkip

            If bIsSkip = True AndAlso bIsOffRoad = False Then
                bIsSkip = True

                Dim sSLicSupplier As String = getFormDFValue(oForm, "U_SLicSp")
                If sSLicSupplier.Length = 0 Then
                    sSLicSupplier = Config.Parameter("SPSLIC")
                    If sSLicSupplier.Length > 0 Then
                        setFormDFValue(oForm, "U_SLicSp", sSLicSupplier)
                    End If

                    Dim sSLicName As String = Config.INSTANCE.doGetBPName(sSLicSupplier)
                    setFormDFValue(oForm, "U_SLicNm", sSLicName)
                End If

                Dim dSkipChrg As Double = getFormDFValue(oForm, "U_SLicCh")
                If dSkipChrg = 0 Then
                    Dim sCustomer As String = getFormDFValue(oForm, "U_CardCd")
                    Dim sSkipCd As String = Config.INSTANCE.doGetSkipLicCode()

                    dSkipChrg = Config.INSTANCE.getSBOItemPrice(sSkipCd, sCustomer, Date.Now())
                    setFormDFValue(oForm, "U_SLicCh", dSkipChrg)
                End If
            Else
                setFormDFValue(oForm, "U_SLicSp", "")
                setFormDFValue(oForm, "U_SLicNm", "")
                setFormDFValue(oForm, "U_SLicCh", 0)

                bIsSkip = False
            End If
            oForm.Items.Item("IDH_LICSUP").Visible = bIsSkip
            oForm.Items.Item("IDH_SLICNM").Visible = bIsSkip
            oForm.Items.Item("IDH_SKPLIC").Visible = bIsSkip
            oForm.Items.Item("IDH_LICEXP").Visible = bIsSkip
            oForm.Items.Item("IDH_LICCHR").Visible = bIsSkip
            oForm.Items.Item("IDH_SLSL").Visible = bIsSkip
            oForm.Items.Item("IDH_LINKSL").Visible = bIsSkip
            oForm.Items.Item("60").Visible = bIsSkip
            oForm.Items.Item("143").Visible = bIsSkip
            oForm.Items.Item("61").Visible = bIsSkip
            oForm.Items.Item("67").Visible = bIsSkip
            oForm.Items.Item("70").Visible = bIsSkip
        End Sub

        Protected Overrides Sub doHandleModalCanceled(oParentForm As SAPbouiCOM.Form, sModalFormType As String)
            If sModalFormType.Equals("IDHCSRCH") OrElse sModalFormType.Equals("IDHNCSRCH") Then
                Dim sTrg As String = getSharedData(oParentForm, "TRG")
                If sTrg = "PR" Then
                    setFormDFValueFromUser(oParentForm, "U_PCardCd", "", False, True)
                    setFormDFValueFromUser(oParentForm, "U_PCardNM", "", False, True)
                ElseIf sTrg = "CU" Then
                    setFormDFValueFromUser(oParentForm, "U_CardCd", "", False, True)
                    setFormDFValueFromUser(oParentForm, "U_CardNM", "", False, True)
                ElseIf sTrg = "WC" Then
                    setFormDFValueFromUser(oParentForm, "U_CCardCd", "", False, True)
                    setFormDFValueFromUser(oParentForm, "U_CCardNM", "", False, True)
                ElseIf sTrg = "ST" Then
                    setFormDFValueFromUser(oParentForm, "U_SCardCd", "", False, True)
                    setFormDFValueFromUser(oParentForm, "U_SCardNM", "", False, True)
                End If
            End If
            MyBase.doHandleModalCanceled(oParentForm, sModalFormType)
        End Sub
        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            MyBase.doHandleModalResultShared(oForm, sModalFormType, sLastButton)
            Try

                If sModalFormType.Equals("IDH_FORECSR") Then
                    Dim sForeCast As String = getSharedData(oForm, "IDH_FORECST")
                    Dim sWastCd As String = getSharedData(oForm, "IDH_WASTCD")
                    Dim dEstimate As Double = getSharedData(oForm, "IDH_ESTQTY")
                    Dim dRemaining As Double = getSharedData(oForm, "IDH_REMCQTY")
                    Dim sUOM As String = getSharedData(oForm, "IDH_UOM")
                    Dim sType As String = getSharedData(oForm, "IDH_TYPE")
                    Dim dExpWgt As String = getSharedData(oForm, "IDH_EXPWGT")
                    Dim sAltWastCd As String = getSharedData(oForm, "IDH_ALFCWC")

                    setFormDFValue(oForm, "U_ForeCS", sForeCast)
                    setFormDFValue(oForm, "U_Status", sType)

                    setUFValue(oForm, "IDH_FCWC", sWastCd)
                    setUFValue(oForm, "IDH_ALFCWC", sAltWastCd)
                    setUFValue(oForm, "IDH_FCREM", dRemaining)
                    setUFValue(oForm, "IDH_FCUOM", sUOM)

                    setWFValue(oForm, "FCCADTYPE", sType)
                    setWFValue(oForm, "FCEXPWGT", dExpWgt)

                    doSetUpdate(oForm)
                ElseIf sModalFormType.Equals("IDHMASRCH") Then
                    Dim oArr As ArrayList = New ArrayList
                    oArr.Add(getSharedData(oForm, "ADDRESS"))
                    oArr.Add(getSharedData(oForm, "STREET"))
                    oArr.Add(getSharedData(oForm, "BLOCK"))
                    oArr.Add(getSharedData(oForm, "ZIPCODE"))
                    oArr.Add(getSharedData(oForm, "CITY"))
                    oArr.Add(getSharedData(oForm, "TEL1"))
                    oArr.Add(getSharedData(oForm, "STATENM"))

                    setFormDFValue(oForm, "U_MAddress", oArr.Item(0))
                    setFormDFValue(oForm, "U_MStreet", oArr.Item(1))
                    setFormDFValue(oForm, "U_MBlock", oArr.Item(2))
                    setFormDFValue(oForm, "U_MZpCd", oArr.Item(3))
                    setFormDFValue(oForm, "U_MCity", oArr.Item(4))
                    setFormDFValue(oForm, "U_MPhone1", oArr.Item(5))
                    setFormDFValue(oForm, "U_MState", oArr.Item(6))
                    doSetUpdate(oForm)

                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error handling the Modal results.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHMR", {Nothing})
            End Try
        End Sub

        Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            oForm.Freeze(True)
            Try
                If oData Is Nothing Then Exit Sub
                If sModalFormType = "IDHJOBS" Then
                    Dim oDataW As ArrayList = oData
                    Dim sLineID As String
                    Dim iRow As Integer = oDataW.Item(1)
                    Dim oGridN As OrderRowGrid = oDataW.Item(2)
                    Dim oExtra As Hashtable = oDataW.Item(3)
                    Dim oAction As String = oDataW.Item(4)
                    Dim sCardCode As String
                    Dim iAutoCollectDays As Integer = 0

                    If sLastButton = "1" Then
                        iAutoCollectDays = oExtra.Item("AUTOCOLLECT")

                        sLineID = oGridN.doGetFieldValue("Code").Trim()
                        oGridN.doSetFieldValue("Code", sLineID)
                        oGridN.doSetFieldValue("Name", sLineID)

                        Dim bIsNewRow As Boolean = oGridN.IsAddedRow()
                        If bIsNewRow OrElse
                            oGridN.hasFieldChanged("U_AEDate") OrElse
                            oGridN.hasFieldChanged("U_CusChr") Then
                            Dim sJobType As String = oGridN.doGetFieldValue("U_JobTp")
                            Dim sLicJob As String = Config.ParameterWithDefault("WOJBLRN", "")
                            Dim sWO As String = getFormDFValue(oForm, "Code")
                            If sJobType.Equals(sLicJob) Then
                                If bIsNewRow OrElse oGridN.hasFieldChanged("U_AEDate") Then
                                    Dim dExpDate As Date = oGridN.doGetFieldValue("U_AEDate")
                                    Dim sDate As String = com.idh.utils.Dates.doDateToSBODateStr(dExpDate)
                                    setFormDFValue(oForm, "U_SLicExp", sDate)
                                End If

                                If bIsNewRow OrElse oGridN.hasFieldChanged("U_CusChr") Then
                                    Dim dLicCost As Double = oGridN.doGetFieldValue("U_CusChr")
                                    setFormDFValue(oForm, "U_SLicCh", dLicCost)
                                End If
                            End If
                        End If
                        Dim sScheduleData As String = oGridN.doGetFieldValue("U_Covera")
                        If Not sScheduleData Is Nothing AndAlso
                               sScheduleData.Length > 0 AndAlso
                                Not sScheduleData.Chars(0) = "_" AndAlso
                                sScheduleData.StartsWith("Created From:") = False AndAlso
                                sScheduleData.StartsWith("PBI-") = False Then
                            'doDebug("Calculating the Dates", 2)
                            DataHandler.INSTANCE.DebugTick2 = "Calculating the Dates"

                            sCardCode = oGridN.doGetFieldValue("U_CustCd")
                            Dim oDates As com.idh.bridge.utils.Dates.ScheduleDates = com.idh.bridge.utils.Dates.doCreateScheduleDates(sCardCode, Nothing, Nothing, sScheduleData, DateTime.MinValue)

                            Dim sRemPT As String = oExtra.Item("REMPT")
                            Dim sRemPQ As String = oExtra.Item("REMPQ")
                            Dim iDateCount As Integer = oDates.Count

                            'doDebug("After Dates", 2)
                            DataHandler.INSTANCE.DebugTick2 = "After Dates"
                            If iDateCount > 0 Then
                                Dim iRepeat As Integer
                                'Dim iCode As Integer
                                Dim iLastRow As Integer

                                'doDebug("Duplicating the Row", 2)
                                DataHandler.INSTANCE.DebugTick2 = "Duplicating the Row"

                                iLastRow = oGridN.getLastRowIndex()
                                oGridN.doAddRows(iDateCount)

                                'doDebug("Start Row Copy", 2)
                                DataHandler.INSTANCE.DebugTick2 = "Start Row Copy"

                                'iCode = DataHandler.INSTANCE.getNextNumber(msNextRowNum, iDateCount)
                                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(msNextRowNum, iDateCount)
                                Dim oWrkDate As Date
                                For iRepeat = 0 To iDateCount - 1
                                    'doDuplicateRow(oGridN, iRow, iLastRow, iCode.ToString(), False, False, False, False, sScheduleData)
                                    doDuplicateRow__(oGridN, iRow, iLastRow, oNumbers.doPackCodeCode(iRepeat), oNumbers.doPackNameCode(iRepeat), False, False, False, False, False, False, sScheduleData)

                                    oWrkDate = oDates.Item(iRepeat)
                                    oGridN.getSBOGrid.DataTable.SetValue(oGridN.doIndexField("U_RDate"), iLastRow, oWrkDate)

                                    iLastRow = iLastRow + 1
                                    'iCode = iCode + 1
                                Next

                                If Not oWrkDate = Nothing AndAlso sRemPT.Length > 0 AndAlso sRemPQ.Length > 0 Then
                                    Dim oOrigDate As Date = oWrkDate
                                    Dim iQty As Integer = Conversions.ToInt(sRemPQ)

                                    If sRemPT.StartsWith("Hour") = True Then
                                        oWrkDate = oWrkDate.AddHours(-iQty)
                                    ElseIf sRemPT.StartsWith("Day") = True Then
                                        oWrkDate = oWrkDate.AddDays(-iQty)
                                    ElseIf sRemPT.StartsWith("Month") = True Then
                                        oWrkDate = oWrkDate.AddMonths(-iQty)
                                    ElseIf sRemPT.StartsWith("Year") = True Then
                                        oWrkDate = oWrkDate.AddYears(-iQty)
                                    End If

                                    Dim sTime As String = com.idh.utils.Dates.doTimeToNumStr(oWrkDate)
                                    Dim sOrigRow As String = oGridN.doGetFieldValue("Code", iRow)
                                    Dim sOrigOrder As String = oGridN.doGetFieldValue("U_JobNr", iRow)

                                    oGridN.doSetFieldValue("U_JobRmD", iRow, oWrkDate)
                                    oGridN.doSetFieldValue("U_JobRmT", iRow, sTime)
                                    oGridN.doSetFieldValue("U_RemNot", iRow, "Coverage Waste Order Series " & sOrigOrder & "." & sOrigRow & " will expire on " & goParent.doDateToSBODisplay(oOrigDate))
                                End If
                                oGridN.doSetFieldValue("U_Covera", iRow, "_" & sScheduleData)
                                'doDebug("Duplication Done", 2)
                                DataHandler.INSTANCE.DebugTick2 = "Duplication Done"

                            End If
                        End If

                        If iAutoCollectDays > 0 Then
                            'Dim iRepeat As Integer
                            'Dim sCode As String
                            Dim iLastRow As Integer

                            'doDebug("Duplicating the Row", 2)
                            DataHandler.INSTANCE.DebugTick2 = "Duplicating the Row"

                            iLastRow = oGridN.getLastRowIndex()
                            oGridN.doAddRows(1)

                            'sCode = DataHandler.doGenerateCode(msNextRowNumPrefix, msNextRowNum)
                            Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(getNextRowNumPrefix(), msNextRowNum)
                            doDuplicateRow__(oGridN, iRow, iLastRow, oNumbers.CodeCode, oNumbers.NameCode, False, False, False, False, False, False)

                            Dim oDate As Date = oGridN.doGetFieldValue("U_RDate")
                            oDate = oDate.AddDays(iAutoCollectDays)
                            oGridN.doSetFieldValue("U_RDate", iLastRow, oDate)

                            Dim sCollectionJob As String = Config.Parameter("WOACCJ")
                            oGridN.doSetFieldValue("U_JobTp", iLastRow, sCollectionJob)

                            oGridN.doSetFieldValue("U_Wei1", iLastRow, 0)
                            oGridN.doSetFieldValue("U_Wei2", iLastRow, 0)

                            oGridN.doSetFieldValue("U_Ser1", iLastRow, "")
                            oGridN.doSetFieldValue("U_Ser2", iLastRow, "")
                            oGridN.doSetFieldValue("U_WDt1", iLastRow, "")
                            oGridN.doSetFieldValue("U_WDt2", iLastRow, "")

                            oGridN.doSetFieldValue(IDH_JOBSHD._TRdWgt, iLastRow, 0)
                            oGridN.doSetFieldValue("U_TipWgt", iLastRow, 0)
                            oGridN.doSetFieldValue("U_TipCost", iLastRow, 0)
                            oGridN.doSetFieldValue("U_TipTot", iLastRow, 0)

                            oGridN.doSetFieldValue("U_SLicCTo", iLastRow, 0)
                            oGridN.doSetFieldValue("U_JCost", iLastRow, 0)

                            oGridN.doSetFieldValue("U_RdWgt", iLastRow, 0)
                            oGridN.doSetFieldValue("U_CstWgt", iLastRow, 0)
                            oGridN.doSetFieldValue("U_TCharge", iLastRow, 0)
                            oGridN.doSetFieldValue("U_TCTotal", iLastRow, 0)

                            oGridN.doSetFieldValue("U_Discnt", iLastRow, 0)

                            oGridN.doSetFieldValue("U_TAUOMQt", iLastRow, 0)
                            oGridN.doSetFieldValue("U_PAUOMQt", iLastRow, 0)
                            oGridN.doSetFieldValue("U_PRdWgt", iLastRow, 0)
                            oGridN.doSetFieldValue("U_ProWgt", iLastRow, 0)
                            oGridN.doSetFieldValue("U_PCost", iLastRow, 0)
                            oGridN.doSetFieldValue("U_PCTotal", iLastRow, 0)

                            oGridN.doSetFieldValue("U_DisAmt", iLastRow, 0)
                            oGridN.doSetFieldValue("U_TaxAmt", iLastRow, 0)
                            oGridN.doSetFieldValue("U_SLicCh", iLastRow, 0)
                            oGridN.doSetFieldValue("U_CongCh", iLastRow, 0)
                            oGridN.doSetFieldValue("U_Total", iLastRow, 0)

                            oGridN.doSetFieldValue("U_ItmTCost", iLastRow, 0)
                            oGridN.doSetFieldValue("U_ItmTChrg", iLastRow, 0)

                            oGridN.doSetFieldValue("U_WROrd", iLastRow, "")
                            oGridN.doSetFieldValue("U_WRRow", iLastRow, "")

                            ''**** NOW SET THE PRICES
                            Dim dWeightTotal As Double = 0
                            sCardCode = oGridN.doGetFieldValue("U_CustCd", iLastRow)
                            Dim sCarrCode As String = oGridN.doGetFieldValue(IDH_JOBSHD._CarrCd, iLastRow)
                            Dim sItemCode As String = oGridN.doGetFieldValue("U_ItemCd", iLastRow)
                            Dim sItemGrp As String = oGridN.doGetFieldValue("U_ItmGrp", iLastRow)
                            Dim sWastCd As String = oGridN.doGetFieldValue(IDH_JOBSHD._WasCd, iLastRow)
                            Dim sTipCode As String = oGridN.doGetFieldValue("U_Tip", iLastRow)
                            Dim sBranch As String = oGridN.doGetFieldValue("U_Branch", iLastRow)

                            Dim sSiteAddr As String = getFormDFValue(oForm, "U_Address")
                            Dim sZipCode As String = getFormDFValue(oForm, "U_ZpCd")

                            'Dim sBranch As String = getFormDFValue(oForm, "U_Branch")
                            'Dim sObligated As String = getFormDFValue(oForm, "U_Obligated")
                            Dim sUOM As String = Config.INSTANCE.getDefaultUOM()
                            'Dim sCustCs As String = getFormDFValue(oForm, "U_CustCs")

                            Dim oPrices As com.idh.dbObjects.User.Prices 'ArrayList
                            Dim sTipCostCalcType As String = com.idh.bridge.lookups.FixedValues.getWeightCalcVariable()
                            Dim sHaulageCostCalcType As String = com.idh.bridge.lookups.FixedValues.getWeightCalcVariable()

                            ''**********************
                            ''LOOKUP TIP PRICE COSTS
                            ''**********************
                            Dim dTipCost As Double = 0
                            Dim dOrdWeight As Double = 0
                            Dim dOrdCost As Double = 0
                            Dim dOrdTotal As Double = 0
                            Dim dHVatCostRate As Double = 0
                            Dim dHVatCost As Double = 0

                            If sCardCode.Length > 0 AndAlso sItemCode.Length > 0 Then
                                oPrices = Config.INSTANCE.doGetJobCostPrice("Waste Order", sCollectionJob, sItemGrp, sItemCode, sTipCode, dWeightTotal, sUOM, sWastCd, sCardCode, sSiteAddr, oDate, sBranch, sZipCode)
                                ' 0 - HAULAGE COST
                                ' 1 - TIP COST
                                ' 2 - VAT
                                ' 3 - CHARGE CALC TYPE
                                ' 4 - OFFSET WEIGHT
                                ' 5 - BAND
                                ' 6 - UOM
                                dOrdCost = oPrices.HaulPrice 'aPrices(0)
                                sHaulageCostCalcType = oPrices.HaulageChargeCalc 'aPrices(2)
                                dHVatCostRate = oPrices.HaulageVat

                                dOrdWeight = oGridN.doGetFieldValue("U_OrdWgt", iLastRow)
                                oGridN.doSetFieldValue("U_HCostVtRt", iLastRow, dHVatCostRate)
                                oGridN.doSetFieldValue("U_HCostVtGrp", iLastRow, oPrices.HaulageVatGroup)

                                If sHaulageCostCalcType.ToUpper().Equals(com.idh.bridge.lookups.FixedValues.getWeightCalcFixed()) Then
                                    If dOrdWeight = 0 Then
                                        dOrdTotal = 0
                                    Else
                                        dOrdTotal = dOrdCost
                                    End If
                                Else
                                    dOrdTotal = dOrdCost * dOrdWeight
                                End If
                            End If
                            oGridN.doSetFieldValue("U_OrdCost", iLastRow, dOrdCost)
                            oGridN.doSetFieldValue("U_OrdTot", iLastRow, dOrdTotal)

                            dHVatCost = dOrdTotal * (dHVatCostRate / 100)

                            oGridN.doSetFieldValue("U_JCost", iLastRow, dOrdTotal + dHVatCost)

                            Dim dHaulChrg As Double = 0
                            Dim dTVatPrc As Double = 0
                            Dim dHVatPrc As Double = 0
                            Dim dVatAmnt As Double = 0
                            Dim dTipChrg As Double = 0
                            Dim dPrice As Double = 0
                            Dim dHaulQty As Double = 0
                            Dim dTotal As Double = 0
                            Dim dCongCh As Double = 0
                            Dim dSkipLicCharge As Double = 0
                            If sCardCode.Length > 0 AndAlso sItemCode.Length > 0 Then
                                oPrices = Config.INSTANCE.doGetJobChargePrices("Waste Order", sCollectionJob, sItemGrp, sItemCode, sCardCode, 0, sUOM, sWastCd, sCarrCode, sSiteAddr, oDate, sBranch, sZipCode)
                                ' 0 - HAULAGE COST
                                ' 1 - TIP COST
                                ' 2 - VAT
                                ' 3 - CHARGE CALC TYPE
                                ' 4 - OFFSET WEIGHT
                                ' 5 - BAND
                                ' 6 - UOM
                                dHaulChrg = oPrices.HaulPrice 'aPrices(0)
                                If oPrices.Band = True Then
                                    Dim sZip As String = getFormDFValue(oForm, "U_ZpCd")
                                    dHaulChrg = dHaulChrg * Config.INSTANCE.doGetFactor(sZip, sCardCode, sItemCode, sItemGrp)
                                End If

                                oGridN.doSetFieldValue("U_CusChr", iLastRow, dPrice)

                                dTipChrg = oPrices.TipPrice 'aPrices(1)
                                dTVatPrc = oPrices.TipVat 'aPrices(2)
                                dHVatPrc = oPrices.HaulageVat

                                oGridN.doSetFieldValue("U_TChrgVtRt", iLastRow, dTVatPrc)
                                oGridN.doSetFieldValue("U_HChrgVtRt", iLastRow, dHVatPrc)

                                oGridN.doSetFieldValue("U_TChrgVtGrp", iLastRow, oPrices.TipVatGroup)
                                oGridN.doSetFieldValue("U_HChrgVtGrp", iLastRow, oPrices.HaulageVatGroup)

                                'dHaulQty = oGridN.doGetFieldValue("U_CusQty", iLastRow)
                                dHaulQty = oGridN.doGetFieldValue("U_HlSQty", iLastRow)
                                dPrice = dHaulQty * dHaulChrg
                            End If

                            oGridN.doSetFieldValue("U_CusChr", iLastRow, dHaulChrg)
                            oGridN.doSetFieldValue("U_Price", iLastRow, dPrice)

                            Dim dBefDiscTotal As Double = 0 + dPrice
                            'oAuditObj.setFieldValue("U_BefDis", dBefDiscTotal)

                            oGridN.doSetFieldValue("U_Discnt", iLastRow, 0)
                            oGridN.doSetFieldValue("U_DisAmt", iLastRow, 0)
                            Dim dDiscountAmt As Double = 0

                            dTotal = dBefDiscTotal - dDiscountAmt

                            If dHVatPrc > 0 Then
                                dVatAmnt = (dBefDiscTotal - dDiscountAmt) * dHVatPrc / 100
                            Else
                                dVatAmnt = 0
                            End If
                            oGridN.doSetFieldValue("U_TaxAmt", iLastRow, dVatAmnt)

                            dTotal = dTotal + dCongCh + dSkipLicCharge + dVatAmnt

                            oGridN.doSetFieldValue("U_Total", iLastRow, dTotal)
                            oGridN.doSetFieldValue("U_MANPRC", iLastRow, 0)
                        End If

                        If oGridN.hasChangedRows() OrElse oGridN.hasAddedRows() Then
                            If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                            End If
                        End If

                        Dim iCount As Integer = oGridN.getRowCount()
                        doSwitchToAdd(oForm)

                        Dim oRowData As WR1_Data.idh.data.AdditionalExpenses = Nothing
                        oRowData = getWFValue(oForm, "JobAdditionalHandler")
                        If Not oRowData Is Nothing Then
                            If oRowData.hasChanged() Then
                                doSetUpdate(oForm)
                            End If
                        End If

                        setUFValue(oForm, "IDH_ONSTE", oGridN.getOnSiteQty())

                        oGridN.doBuildJobEntries(msOrderType)

                        Dim dQty As Double = oGridN.getOnSiteQty()
                        setUFValue(oForm, "IDH_ONSTE", dQty)

                        Dim bWOClosed As Boolean = oGridN.doCheckCloseOrder()
                        If bWOClosed Then
                            setFormDFValue(oForm, "U_STATUS", "9")
                        End If

                        'set value to draft only if it is a USA release + bWOClosed is not true + sWOStatus has got some value
                        If Config.INSTANCE.getParameterAsBool("USAREL", False) = True AndAlso bWOClosed = False Then
                            Dim sWOStatus As String = getParentSharedData(oForm, "WOTODRAFT")
                            If sWOStatus Is Nothing OrElse sWOStatus.Length = 0 Then
                                sWOStatus = getSharedData(oForm, "WOTODRAFT")
                            End If
                            'If sWOStatus <> "-1" Then
                            If sWOStatus IsNot Nothing AndAlso sWOStatus.Length > 0 AndAlso sWOStatus <> "-1" Then
                                setFormDFValue(oForm, "U_STATUS", sWOStatus)
                            End If
                        End If

                    ElseIf sLastButton = "2" Then
                        oGridN.doRemoveRow(iRow, False)
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal results - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        '** Do validate before adding a row
        Public Function doValidateBeforeRow(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Return doValidateHeader(oForm) AndAlso doValidateWOHforNewRow(oForm)

        End Function
        Public Function doValidateWOHforNewRow(ByVal oform As SAPbouiCOM.Form) As Boolean
            ''MA Start 16-07-2015 Issue#863
            Dim sBPForecastCode As String = getFormDFValue(oform, IDH_JOBENTR._ForeCS)
            Dim sLockPrc As String = getFormDFValue(oform, IDH_JOBENTR._LckPrc)
            If sBPForecastCode IsNot Nothing AndAlso sLockPrc IsNot Nothing AndAlso sBPForecastCode.Trim <> "" AndAlso sLockPrc.Equals("Y") Then
                Dim oBPForecast As IDH_BPFORECST = New IDH_BPFORECST()
                If oBPForecast.getByKey(sBPForecastCode) Then
                    Dim sForecastStatus As String = oBPForecast.U_DocType
                    Dim sForecastActiveTo As Date = oBPForecast.U_ActiveTo
                    If sForecastStatus = "3" OrElse sForecastStatus = "4" OrElse sForecastStatus = "9" Then
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Forecast is closed. You cannot add more work order rows.", "ERFCSTCL", {Nothing})
                        Return False
                    ElseIf sForecastActiveTo < DateTime.Today Then
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Forecast is expired. You cannot add more work order rows.", "ERFCSTEX", {Nothing})
                        Return False
                    End If
                End If
            End If
            ''MA End 16-07-2015

            ''MA Start 22-07-2015 Issue#881
            Dim sStatus As String = getFormDFValue(oform, IDH_JOBENTR._Status)
            If sStatus IsNot Nothing AndAlso sStatus.Trim = "4" Then
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Work Order is cancelled. You cannot add more work order rows.", "ERWHSTCN", {Nothing})
                Return False
            End If
            ''MA End 22-07-2015
            Return True
        End Function
        '		Protected Overrides Sub doCheckCustomerAddress(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bFromSearch As Boolean = False)
        '			MyBase.doCheckCustomerAddress(oForm, bFromSearch)
        '			setEnableItem(oForm, isEnabled("IDH_CNA"), "IDH_PNA")
        '		End Sub

        '** Send By Custom Events
        'Return True done
        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If MyBase.doCustomItemEvent(oForm, pVal) = False Then
                Return False
            End If
            Dim sItemId As String = pVal.ItemUID
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_ADD Then
                If pVal.BeforeAction = True Then
                    If doValidateBeforeRow(oForm) = False Then
                        Return False
                    End If
                    Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                    Dim oData As New ArrayList
                    Dim oHead As Hashtable = New Hashtable
                    oGridN.setCurrentDataRowIndex(oGridN.getLastRowIndex)

                    oData.Add("IDHGRID")
                    oData.Add(oGridN.getLastRowIndex())
                    oData.Add(oGridN)
                    oData.Add(oHead)

                    'If oGridN.getOnSiteQty() = 0 AndAlso _
                    '	oGridN.getRowCount()
                    'End If

                    If doPrepareRowData(oForm, oData) = True Then
                        oData.Add("CANNOTDOCOVERAGE")
                        goParent.doOpenModalForm("IDHJOBS", oForm, oData)
                    Else
                        oGridN.doRemoveRow(oGridN.getCurrentDataRowIndex(), False, False)
                        'oGridN.doRemoveAddedRowFlag()
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DEL Then
                Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                Dim dQty As Double = oGridN.getOnSiteQty()
                'setUFValue( oForm, "IDH_ONSTE", dQty )

                Dim sStatus As String = getFormDFValue(oForm, "U_STATUS")
                If dQty > 0 AndAlso sStatus = "9" Then
                    setFormDFValue(oForm, "U_STATUS", "1")
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK Then 'GRID_ROW_VIEW_EDIT Then
                If sItemId = "LINESGRID" Then
                    If pVal.BeforeAction = False Then
                        Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                        Dim oData As New ArrayList
                        Dim oHead As Hashtable = New Hashtable

                        'Already set
                        'oGridN.setCurrentLine(pVal.Row)

                        oData.Add("IDHGRID")
                        oData.Add(pVal.Row)
                        oData.Add(oGridN)
                        oData.Add(oHead)

                        If doPrepareRowData(oForm, oData) = True Then
                            oData.Add("CANNOTDOCOVERAGE")
                            goParent.doOpenModalForm("IDHJOBS", oForm, oData)
                        End If
                    End If
                End If
                '            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DEL Then
                '                If sItemId = "LINESGRID" Then
                '                    If pVal.BeforeAction = False Then
                '                        doSwitchToAdd(oForm)
                '                    End If
                '                End If
                '            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DEL_MULTI Then
                '                If sItemId = "LINESGRID" Then
                '                    If pVal.BeforeAction = False Then
                '                        doSwitchToAdd(oForm)
                '                    End If
                '                End If
            End If
            Return True
        End Function

        '** The ItemSubEvent handler
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            Dim sItemId As String = pVal.ItemUID
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemChanged Then
                        If sItemId = "IDH_CUSA2" Then
                            oForm.Items.Item("IDH_CUSADD").Update()
                            doCheckCustomerAddress(oForm)
                        ElseIf sItemId = "IDH_PRDADD" Then
                            oForm.Items.Item("IDH_PRDADD").Update()
                            doCheckProducerAddress(oForm)
                        ElseIf sItemId = "IDH_CUSADD" Then
                            oForm.Items.Item("IDH_CUSA2").Update()
                        ElseIf sItemId = "IDH_CUSPOS" Then
                            oForm.Items.Item("IDH_PC2").Update()
                        ElseIf sItemId = "IDH_PC2" Then
                            oForm.Items.Item("IDH_CUSPOS").Update()
                        End If

                        If wasSetWithCode(oForm, "IDH_PRDADD") = True Then
                            Dim sCustCd As String = getFormDFValue(oForm, "U_CardCd")
                            Dim sProdCd As String = getFormDFValue(oForm, "U_PCardCd")

                            If sProdCd.Length = 0 OrElse sCustCd = sProdCd Then
                                If sItemId = "IDH_CUSCON" Then
                                    setFormDFValue(oForm, "U_PContact", getFormDFValue(oForm, "U_Contact"))
                                ElseIf sItemId = "IDH_CUSADD" OrElse sItemId = "IDH_CUSA2" Then
                                    setFormDFValue(oForm, "U_PAddress", getFormDFValue(oForm, "U_Address"))
                                ElseIf sItemId = "IDH_CUSSTR" Then
                                    setFormDFValue(oForm, "U_PStreet", getFormDFValue(oForm, "U_Street"))
                                ElseIf sItemId = "IDH_CUSBLO" Then
                                    setFormDFValue(oForm, "U_PBlock", getFormDFValue(oForm, "U_Block"))
                                ElseIf sItemId = "IDH_CUSCIT" Then
                                    setFormDFValue(oForm, "U_PCity", getFormDFValue(oForm, "U_City"))
                                ElseIf sItemId = "IDH_CUSPOS" OrElse sItemId = "IDH_PC2" Then
                                    setFormDFValue(oForm, "U_PZpCd", getFormDFValue(oForm, "U_ZpCd"))
                                ElseIf sItemId = "IDH_CUSPHO" Then
                                    setFormDFValue(oForm, "U_PPhone1", getFormDFValue(oForm, "U_Phone1"))
                                End If
                            End If
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                If pVal.BeforeAction = False Then
                    If sItemId = "IDH_IGRP" Then
                        doSwitchSkip(oForm)
                    ElseIf sItemId = "IDH_CONTRN" Then
                        Dim sContractNo As String = getFormDFValue(oForm, IDH_JOBENTR._CntrNo)
                        If sContractNo.Length > 0 Then
                            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                            Try
                                Dim sQry As String
                                Dim sVal As String
                                oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                sQry = "SELECT U_CusRef, U_Stat from [@IDH_WASTCONT] Where Code = " & sContractNo
                                oRecordSet.DoQuery(sQry)
                                If oRecordSet.RecordCount > 0 Then
                                    sVal = oRecordSet.Fields.Item("U_CusRef").Value.Trim()
                                    setFormDFValue(oForm, IDH_JOBENTR._CustRef, sVal)

                                    sVal = oRecordSet.Fields.Item("U_Stat").Value.Trim()
                                    If sVal.Equals(com.idh.bridge.lookups.FixedValues.getStatusApproved()) Then
                                        'CREATED Status = 1
                                        setFormDFValue(oForm, IDH_JOBENTR._Status, "1")
                                    Else
                                        setFormDFValue(oForm, IDH_JOBENTR._Status, sVal)
                                    End If
                                End If
                            Catch ex As Exception
                                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error finding the WO.")
                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXLUWO", {Nothing})
                            Finally
                                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
                            End Try
                        End If

                        '                    Dim lContract As Long = getWFValue(oForm, "WrkContractNumber")
                        '					Dim sCode As String
                        '					If lContract < 0 Then
                        '						sCode = lContract.ToString()
                        '					Else
                        '						sCode = getFormDFValue( oForm, "Code")
                        '					End If
                        '					setSharedData(oForm, "IDH_CONTRN", sCode)
                        '                    setSharedData(oForm, "WOCode", "")
                        '                    setSharedData(oForm, "IDH_CUST", getFormDFValue(oForm, "U_Cust"))
                        '                    setSharedData(oForm, "IDH_CUSTNM", getFormDFValue(oForm, "U_CustNm"))
                        '                    setSharedData(oForm, "IDH_CUSCRF", getFormDFValue(oForm, "U_CusRef"))
                        '                    setSharedData(oForm, "USESHARE", "" )
                        '                    Dim sStatus As String
                        '                    sStatus = getFormDFValue(oForm, "U_Stat")
                        '                    If sStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusApproved()) Then
                        '                        'CREATED Status = 1
                        '                        setSharedData(oForm, "IDH_STATUS", "1")
                        '                    Else
                        '                        setSharedData(oForm, "IDH_STATUS", sStatus)
                        '                    End If
                        '
                        '                    goParent.doOpenModalForm("IDH_WASTORD", oForm)
                        '                    Return False


                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If sItemId = "IDH_TABSUM" Then
                        ''MA Start 30-10-2014 Issue#417 Issue#418
                        'oForm.PaneLevel = 9
                        'Do the Totals
                        Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                        Dim oRowData As com.idh.bridge.data.RowGridTotals = oGridN.doTotals()
                        Dim dblProfit As Double = oRowData.Profit
                        If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) Then
                            oForm.PaneLevel = 9
                            'Do the Totals
                            'Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                            'Dim oRowData As com.idh.bridge.data.RowGridTotals = oGridN.doTotals()
                            'setUFValue(oForm, "IDH_DISPCH", oRowData.TCTotal)
                            'setUFValue(oForm, "IDH_HAULCH", oRowData.Price)
                            'setUFValue(oForm, "IDH_TOTCH", oRowData.ChargeSubAftDisc)

                            setUFValue(oForm, "IDH_DISPCS", oRowData.DisposalCostTotal)
                            setUFValue(oForm, "IDH_HAULCS", oRowData.HaulageCostTotal)
                            setUFValue(oForm, "IDH_PROCS", oRowData.ProducerCostTotal)
                            setUFValue(oForm, "IDH_TOTCS", oRowData.CostTotal)
                            'setUFValue(oForm, "IDH_PROFIT", oRowData.Profit)
                        Else
                            dblProfit = 0
                        End If
                        If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) AndAlso Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) Then
                            oForm.PaneLevel = 9
                            'Do the Totals
                            'Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                            'Dim oRowData As com.idh.bridge.data.RowGridTotals = oGridN.doTotals()
                            setUFValue(oForm, "IDH_DISPCH", oRowData.TCTotal)
                            setUFValue(oForm, "IDH_HAULCH", oRowData.Price)
                            setUFValue(oForm, "IDH_TOTCH", oRowData.ChargeSubAftDisc)

                            'setUFValue(oForm, "IDH_DISPCS", oRowData.DisposalCostTotal)
                            'setUFValue(oForm, "IDH_HAULCS", oRowData.HaulageCostTotal)
                            'setUFValue(oForm, "IDH_PROCS", oRowData.ProducerCostTotal)
                            'setUFValue(oForm, "IDH_TOTCS", oRowData.CostTotal)
                            'setUFValue(oForm, "IDH_PROFIT", oRowData.Profit)
                        Else
                            dblProfit = 0
                        End If
                        setUFValue(oForm, "IDH_PROFIT", dblProfit)
                        ''MA End 30-10-2014 Issue#417 Issue#418
                    ElseIf sItemId = "IDH_IMP" Then
                        doCallWinFormOpener(AddressOf doImportExcelDrumList, oForm)
                    ElseIf sItemId = "IDH_CHECK" Then
                        doSwitchSkip(oForm)
                    ElseIf sItemId = "IDH_ONSITE" Then
                        Dim sJobNr As String = getFormDFValue(oForm, "Code")
                        setSharedData(oForm, "IDH_WONR", sJobNr)
                        goParent.doOpenModalForm("IDHONST", oForm)
                    ElseIf sItemId = "IDH_FCL" Then
                        Dim sCustCd As String = ""
                        Dim sProdCd As String = ""
                        Dim sCardCd As String = ""

                        sCustCd = getFormDFValue(oForm, "U_CardCd")
                        sProdCd = getFormDFValue(oForm, "U_PCardCd")
                        If sCustCd.Length > 0 Then
                            sCardCd = sCustCd
                        End If
                        If sProdCd.Length > 0 Then
                            If sCardCd.Length > 0 Then
                                sCardCd = sCardCd & "," & sProdCd
                            Else
                                sCardCd = sProdCd
                            End If
                        End If

                        setSharedData(oForm, "IDH_CUSTCD", sCardCd)
                        goParent.doOpenModalForm("IDH_FORECSR", oForm)
                    ElseIf sItemId = "IDH_WABT" Then
                        Dim sCustomer As String = getFormDFValue(oForm, "U_CardCd")
                        Dim sSiteAddr As String = getFormDFValue(oForm, "U_Address")

                        If sCustomer.Trim().Length > 0 Then
                            Dim sReportFile As String
                            sReportFile = Config.Parameter("WOCWAB")

                            Dim oParams As New Hashtable
                            oParams.Add("Customer", sCustomer)
                            oParams.Add("SiteAddr", sSiteAddr)
                            If (Config.INSTANCE.useNewReportViewer()) Then
                                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                                IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
                            Else

                                IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
                            End If
                        End If
                    ElseIf sItemId = "IDH_LASTM" Then
                        Dim sCustomer As String = getFormDFValue(oForm, "U_CardCd")
                        Dim sSiteAddr As String = getFormDFValue(oForm, "U_Address")

                        If sCustomer.Trim().Length > 0 Then
                            Dim sReportFile As String
                            sReportFile = Config.Parameter("WOCLMO")

                            Dim oParams As New Hashtable
                            oParams.Add("Customer", sCustomer)
                            oParams.Add("SiteAddr", sSiteAddr)
                            If (Config.INSTANCE.useNewReportViewer()) Then
                                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                                IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
                            Else

                                IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
                            End If
                        End If
                        'ElseIf sItemId = "IDH_TFSREQ" Then
                        '    'if you have ticked TFS Required checkbox then
                        '    'display TFS Search modal form
                        '    Dim oChk As SAPbouiCOM.CheckBox = oForm.Items.Item("IDH_TFSREQ").Specific
                        '    If oChk.Checked Then
                        '        goParent.doOpenForm("IDHTFSNSR", oForm, True)
                        '    End If
                        'Post code Lookup
                    ElseIf pVal.ItemUID = "IDH_POSTCS" OrElse pVal.ItemUID = "IDH_PSTCS2" OrElse pVal.ItemUID = "IDH_PCDSPR" _
                        OrElse pVal.ItemUID = "IDH_PCDSSI" OrElse pVal.ItemUID = "IDH_PCDSCR" Then
                        doFindAddress(oForm, pVal.ItemUID)
                    End If
                End If
            End If
            Return False
        End Function

        '**
        ' Create a duplicate of the WO Row
        '**
        Protected Overrides Function doDuplicateWORow(ByVal oForm As SAPbouiCOM.Form,
                ByVal sSrcCode As String,
                ByVal sRowCode As String,
                ByVal sNewJobType As String) As Boolean

            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
            Dim oData As New ArrayList
            Dim oHead As Hashtable = New Hashtable
            oGridN.setCurrentDataRowIndex(oGridN.getLastRowIndex)

            oData.Add("IDHGRID")
            oData.Add(oGridN.getLastRowIndex())
            oData.Add(oGridN)
            oData.Add(oHead)

            If oGridN.getOnSiteQty() = 0 AndAlso
                oGridN.getRowCount() Then
            End If

            If doPrepareRowData(oForm, oData, sRowCode, sNewJobType) = True Then
                oData.Add("CANNOTDOCOVERAGE")
                goParent.doOpenModalForm("IDHJOBS", oForm, oData)
            End If
            Return True
        End Function

        Protected Sub doImportExcelDrumList(ByVal oForm As SAPbouiCOM.Form)
            Dim oWO As IDH_JOBENTR = thisWO(oForm)

            Dim sFileName As String
            Dim oOpenFile As System.Windows.Forms.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()

            Try
                Dim sExt As String = Config.INSTANCE.getImportExtension()
                If Not sExt Is Nothing AndAlso sExt.Length > 0 Then
                    If sExt.IndexOf("|") = -1 Then
                        sExt = "(" & sExt & ")|" & sExt
                    End If
                    oOpenFile.Filter = sExt
                End If
            Catch ex As Exception

            End Try
            oOpenFile.FileName = ""
            oOpenFile.InitialDirectory = Config.INSTANCE.getImportFolder()
            Dim oResult As System.Windows.Forms.DialogResult = com.idh.win.AppForm.OpenFileDialog(oOpenFile)
            If oResult = System.Windows.Forms.DialogResult.OK Then
                sFileName = oOpenFile.FileName

                Dim oParam As String() = {sFileName}
                Try
                    Dim sStatus As String = getFormDFValue(oForm, IDH_JOBENTR._Status)
                    If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE AndAlso sStatus.Equals(DRAFT) = False Then
                        DataHandler.INSTANCE.doInfo(Messages.getGMessage("INFIMEXN", oParam))

                        ''Duplicate the WO and Then Add the list
                        'Might get away by only changing the Code, clearing the WOR and setting the Form back into Add Mode.
                        'Dim sNewCode As String = DataHandler.doGenerateCode(msNextNumPrefix, msNextNum)
                        Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(getNextNumPrefix(), msNextNum)
                        setWFValue(oForm, "WOCreated", True)

                        Dim sTime As String = com.idh.utils.Dates.doTimeToNumStr()

                        setFormDFValue(oForm, IDH_JOBENTR._Code, oNumbers.CodeCode)
                        setFormDFValue(oForm, IDH_JOBENTR._Name, oNumbers.NameCode)
                        setFormDFValue(oForm, IDH_JOBENTR._User, goParent.goDICompany.UserSignature())
                        setFormDFValue(oForm, IDH_JOBENTR._BDate, goParent.doDateToStr())
                        setFormDFValue(oForm, IDH_JOBENTR._BTime, sTime)
                        setFormDFValue(oForm, IDH_JOBENTR._RSDate, goParent.doDateToStr())
                        setFormDFValue(oForm, IDH_JOBENTR._RSTime, sTime)
                        setFormDFValue(oForm, IDH_JOBENTR._Status, DRAFT)

                        setEnableItem(oForm, False, "IDH_CNA")
                        setEnableItem(oForm, False, "IDH_PNA")

                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE

                        doLoadData(oForm)

                        oForm.Freeze(False)
                        oForm.Update()
                        oForm.Freeze(True)
                    Else
                        DataHandler.INSTANCE.doInfo(Messages.getGMessage("INFIMPEX", oParam))
                    End If

                    Dim iProgress As Integer = 5

                    Dim oImporter As New com.idh.bridge.import.DrumList()
                    oImporter.doReadFile(sFileName, getFormDFValue(oForm, IDH_JOBENTR._ItemGrp))
                    goParent.doProgress("EXIMP", iProgress)

                    DataHandler.INSTANCE.doInfo(">>>>> IMPORT ROW COUNT: " & oImporter.ImportedRows.Count)

                    If oImporter.ImportedRows.Count > 0 Then

                        ''Check And Create the Items
                        oImporter.doCheckAndCreateItems()
                        iProgress = 10
                        goParent.doProgress("EXIMP", iProgress)

                        Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                        Dim sTimeF As String = getFormDFValue(oForm, "U_RTIMEF")
                        Dim sTimeT As String = getFormDFValue(oForm, "U_RTIMET")

                        'oGridN.doAddWORow(sCustomer, "kg", 0, Date.Now(), sTimeF, sTimeT, "WC001", "Waste Codw 001", "", "", "", "", "")

                        Dim dProgressIter As Double = 90 / oImporter.ImportedRows.Count

                        For Each oImportRow As com.idh.bridge.import.DrumList.ImportRow In oImporter.ImportedRows

                            DataHandler.INSTANCE.doInfo(">>>>> GOING INTO ADDIMPORTROW")

                            oGridN.doAddImportedWORow(oWO,
                                            getFormDFValue(oForm, IDH_JOBENTR._Code), getFormDFValue(oForm, IDH_JOBENTR._CardCd), getFormDFValue(oForm, IDH_JOBENTR._CardNM),
                                            getFormDFValue(oForm, IDH_JOBENTR._Address), getFormDFValue(oForm, IDH_JOBENTR._ZpCd),
                                            Date.Now(), sTimeF, sTimeT,
                                            getFormDFValue(oForm, IDH_JOBENTR._ItemGrp),
                                            "", "", "", "", "",
                                            getFormDFValue(oForm, IDH_JOBENTR._Origin),
                                            getFormDFValue(oForm, IDH_JOBENTR._CCardCd), getFormDFValue(oForm, IDH_JOBENTR._CCardNM),
                                            getFormDFValue(oForm, IDH_JOBENTR._PCardCd), getFormDFValue(oForm, IDH_JOBENTR._PCardNM),
                                            getFormDFValue(oForm, IDH_JOBENTR._SCardCd), getFormDFValue(oForm, IDH_JOBENTR._SCardNM),
                                            getFormDFValue(oForm, IDH_JOBENTR._SAddress),
                                            getFormDFValue(oForm, IDH_JOBENTR._CntrNo),
                                            oImportRow)

                            iProgress = iProgress + dProgressIter
                            goParent.doProgress("EXIMP", iProgress)
                        Next
                    End If
                Catch ex As Exception
                    If ex Is Nothing Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: BadBad Bad Exception", "Error importing the data.")
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", {Nothing})
                    Else
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error importing the data.")
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", {Nothing})
                    End If

                Finally
                    goParent.doProgress("EXIMP", 100)

                    Dim sCurrDirectory As String = Directory.GetCurrentDirectory()
                    Directory.SetCurrentDirectory(IDHAddOns.idh.addon.Base.CURRENTWORKDIR)
                End Try
            End If
        End Sub

        Protected Function doPrepareRowData(ByVal oForm As SAPbouiCOM.Form,
                ByRef oData As ArrayList) As Boolean
            Return doPrepareRowData(oForm, oData, Nothing, Nothing)
        End Function

        Protected Function doPrepareRowData(ByVal oForm As SAPbouiCOM.Form,
                ByRef oData As ArrayList,
                ByVal sRowCode As String,
                ByVal sJobType As String) As Boolean

            oForm.Freeze(True)
            Try
                Dim iRow As Integer = oData.Item(1)
                Dim oGridN As OrderRowGrid = oData.Item(2)
                Dim oHead As Hashtable = oData.Item(3)

                oGridN.setCurrentDataRowIndex(iRow)
                Dim sUOM As String = Nothing
                Dim sCode As String
                Dim sIGrp As String
                With getFormMainDataSource(oForm)
                    sCode = .GetValue("Code", .Offset).Trim()
                    sIGrp = .GetValue("U_ItemGrp", .Offset).Trim()
                End With

                If sCode.Length = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("A Job Entry must be selected.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("A Job Entry must be selected.", "ERUSJOBS", {Nothing})
                    Return False
                End If

                If sIGrp.Length = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("An Item Group must be selected.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("An Item Group must be selected.", "ERUSIGNS", {Nothing})
                    Return False
                End If

                Dim sCustomer As String
                Dim sTimeF As String
                Dim sTimeT As String
                Dim iTimeF As Integer
                Dim iTimeT As Integer
                Dim oWO As IDH_JOBENTR = getWFValue(oForm, "WasteOrder")

                sCustomer = oGridN.doGetFieldValue("U_CustCd").Trim()
                If sCustomer Is Nothing OrElse sCustomer.Length = 0 Then
                    sCustomer = getFormDFValue(oForm, "U_CardCd")
                    oGridN.doSetFieldValue("U_CustCd", sCustomer)
                End If

                oHead.Add("WasteOrder", oWO)
                oHead.Add("U_BDate", com.idh.utils.Dates.doStrToDate(getFormDFValue(oForm, "U_BDate")))
                oHead.Add("U_BTime", goParent.doTimeStrToStr(getFormDFValue(oForm, "U_BTime")))
                oHead.Add("U_ZpCd", getFormDFValue(oForm, "U_ZpCd"))
                oHead.Add("U_Address", getFormDFValue(oForm, "U_Address"))
                oHead.Add("U_SiteTl", getFormDFValue(oForm, "U_SiteTl"))
                oHead.Add("JobAdditionalHandler", getWFValue(oForm, "JobAdditionalHandler"))
                oHead.Add("U_SteId", getFormDFValue(oForm, "U_SteId"))

                'Added for the BP switching
                oHead.Add("U_PZpCd", getFormDFValue(oForm, "U_PZpCd"))
                oHead.Add("U_PAddress", getFormDFValue(oForm, "U_PAddress"))
                oHead.Add("U_PPhone1", getFormDFValue(oForm, "U_PPhone1"))
                oHead.Add("U_FirstBP", getFormDFValue(oForm, "U_FirstBP"))

                'Adding WO Header status for TFS
                oHead.Add("WOHSTATUS", getFormDFValue(oForm, "U_Status"))

                oHead.Add("LCKPRC", getFormDFValue(oForm, IDH_JOBENTR._LckPrc))
                oHead.Add("DOSIP", getWFValue(oForm, "DOSIP"))
                oHead.Add("CALLEDFROMFC", getWFValue(oForm, "CALLEDFROMFC"))
                oHead.Add("FORECS", getFormDFValue(oForm, IDH_JOBENTR._ForeCS))
                oHead.Add("FCEXPWGT", getWFValue(oForm, "FCEXPWGT"))
                oHead.Add("PRESELECT", getWFValue(oForm, "PRESELECT"))
                oHead.Add("FCBPCURR", getWFValue(oForm, "FCBPCURR"))

                'oHead.Add("TIPCHRG", getWFValue(oForm, "TIPCHRG"))
                'oHead.Add("TIPCST", getWFValue(oForm, "TIPCST"))
                'oHead.Add("HAULCHRG", getWFValue(oForm, "HAULCHRG"))
                'oHead.Add("HAULCST", getWFValue(oForm, "HAULCST"))
                'oHead.Add("PRODCST", getWFValue(oForm, "PRODCST"))

                Dim sVal As String
                Dim sLineID1 As String
                Dim sContainerCode As String = oGridN.JobEntries.LastContainerCode
                Dim sContainerName As String = oGridN.JobEntries.LastContainerName
                Dim oRDate As Date = com.idh.utils.Dates.doStrToDate(getFormDFValue(oForm, "U_RSDate"))
                Dim oBDate As Date = Date.Now()
                Dim sTransactionCode As String = getFormDFValue(oForm, IDH_JOBENTR._TRNCd)
                sLineID1 = oGridN.doGetFieldValue("Code")
                Dim sForecastCode As String = getFormDFValue(oForm, "U_ForeCS")
                Dim ForecastRemainingQty As Double = getUFValue(oForm, "IDH_FCREM")
                'New WO Row
                If sLineID1 Is Nothing OrElse sLineID1.Length = 0 Then
                    If sCustomer IsNot Nothing AndAlso sCustomer.Length > 0 Then
                        If Config.INSTANCE.doCheckBPActive(sCustomer, oBDate) = False Then
                            'com.idh.bridge.DataHandler.INSTANCE.doUserError("The Requested date is in an Inactive Date Range: " + oBDate)
                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("The Requested date is in an Inactive Date Range: " + oBDate, "ERUSIAD", {oBDate.ToString()})
                            Return False
                        End If
                    End If
                    ''##MA Start 07-09-2015 Issue#904
                    If Config.ParameterAsBool("VLDFCQTY", False) = True AndAlso sForecastCode IsNot Nothing AndAlso sForecastCode.Trim <> "" AndAlso ForecastRemainingQty <= 0 Then
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("BP Forecast is reached eastimated quantity. you cannot add more jobs to this forecast.", "ERBFCFUL", Nothing)
                        Return False
                    End If
                    ''##MA Start 07-09-2015 Issue#904
                    oData.Add("DOADD")

                    'sLineID = DataHandler.doGenerateCode(msNextRowNumPrefix, msNextRowNum)
                    Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(getNextRowNumPrefix(), msNextRowNum)
                    oGridN.doSetFieldValue("Code", oNumbers.CodeCode)
                    oGridN.doSetFieldValue("Name", oNumbers.NameCode)

                    oGridN.doSetFieldValue("U_BDate", oBDate)
                    oGridN.doSetFieldValue("U_BTime", goParent.doTimeStrToInt(Nothing))

                    ''## MA Start 10-09-2014 Issue#406

                    If Config.INSTANCE.getParameter("CTOFJBHR") <> "" AndAlso Config.INSTANCE.getParameter("CTOFJBHR") <> "0" _
                        AndAlso Config.ParamaterWithDefault("MAXJOBHR", "0") <> "0" Then
                        Dim cuttoffhrs As DateTime = Today.ToString("yyyy/MM/dd") & " " & Config.INSTANCE.getParameter("CTOFJBHR")
                        Dim BTime As DateTime = DateTime.Now 'goParent.doStrToTime(oGridN.doGetFieldValue("U_BTime"))
                        If BTime.TimeOfDay <= cuttoffhrs.TimeOfDay Then
                            'MAXJOBHR 
                            'Dim BDate As DateTime = Today 'goParent.doStrToDate(goParent.doDateToStr(oGridN.doGetFieldValue("U_BDate"), True))
                            'BTime = BDate.Add(BTime.TimeOfDay)

                            BTime = BTime.AddHours(CInt(Config.Parameter("MAXJOBHR")))
                            oGridN.doSetFieldValue("U_JbToBeDoneTm", com.idh.utils.Dates.doTimeToNumStr(BTime))
                            oGridN.doSetFieldValue("U_JbToBeDoneDt", goParent.doDateToStr(BTime))
                        Else
                            BTime = "08:00" 'if BDate is beyond the cuttof time then set to 8AM of next Day( Hayden to be confirm from Mike)
                            oGridN.doSetFieldValue("U_JbToBeDoneTm", goParent.doTimeStrToInt(BTime))
                            Dim BDate As DateTime = Today.AddDays(1) 'goParent.doStrToDate(goParent.doDateToStr(oGridN.doGetFieldValue("U_BDate"), True)).AddDays(1)
                            oGridN.doSetFieldValue("U_JbToBeDoneDt", goParent.doDateToStr(BDate))
                        End If
                        'Else
                    End If

                    ''## MA End 10-09-2014 Issue#406
                    oGridN.doSetFieldValue("U_RDate", oRDate)

                    sTimeF = getFormDFValue(oForm, "U_RTIMEF")
                    sTimeT = getFormDFValue(oForm, "U_RTIMET")

                    If sTimeF.Length < 4 OrElse sTimeF.Equals("00:00") Then
                        sTimeF = Config.ParameterWithDefault("WOTMFR", "8:00")
                    Else
                        sTimeF = goParent.doSBOStrToTimeStr(sTimeF)
                    End If

                    If sTimeT.Length < 4 OrElse sTimeT.Equals("00:00") Then
                        sTimeT = Config.ParameterWithDefault("WOTMTO", "17:00")
                    Else
                        sTimeT = goParent.doSBOStrToTimeStr(sTimeT)
                    End If

                    iTimeF = goParent.doTimeStrToInt(sTimeF)
                    iTimeT = goParent.doTimeStrToInt(sTimeT)

                    'sTimeF = Config.Parameter("WOTMFR", True)
                    'sTimeT = Config.Parameter("WOTMTO", True)

                    'If String.IsNullOrEmpty(sTimeF) = False Then
                    '    iTimeF = Dates.doTimeStrToIntStr(sTimeF)
                    'Else
                    'End If
                    'If String.IsNullOrEmpty(sTimeT) = False Then
                    '    iTimeT = Dates.doTimeStrToIntStr(sTimeT)
                    'Else
                    'End If

                    oGridN.doSetFieldValue("U_RTime", iTimeF)
                    oGridN.doSetFieldValue("U_RTimeT", iTimeT)

                    oGridN.doSetFieldValue(IDH_JOBSHD._TrnCode, sTransactionCode)

                    'If the SrcRow is set use this to create a new Row
                    If Not sRowCode Is Nothing AndAlso sRowCode.Length > 0 Then
                        'Get the Row values
                        Dim oTableSrc As SAPbobsCOM.UserTable = Nothing
                        oTableSrc = goParent.goDICompany.UserTables.Item(msRowTable.Substring(1))

                        If oTableSrc.GetByKey(sRowCode) Then
                            'Set the Grid values
                            Dim iFields As Integer = oTableSrc.UserFields.Fields.Count
                            Dim sFieldName As String
                            Dim oField As SAPbobsCOM.Field
                            'Dim iColIndex As Integer
                            For iCol As Integer = 0 To iFields - 1
                                oField = oTableSrc.UserFields.Fields.Item(iCol)
                                sFieldName = oField.Name
                                If sFieldName.Equals("Code") = False AndAlso _
                                    sFieldName.Equals("Name") = False AndAlso _
                                    sFieldName.Equals("U_RDate") = False AndAlso _
                                    sFieldName.Equals("U_RTime") = False AndAlso _
                                    sFieldName.Equals("U_RTimeT") = False AndAlso _
                                    sFieldName.Equals("U_ASDate") = False AndAlso _
                                    sFieldName.Equals("U_AEDate") = False AndAlso _
                                    sFieldName.Equals("U_JobNr") = False Then
                                    If sFieldName.Equals("U_JobTp") Then
                                        oGridN.doSetFieldValue(sFieldName, sJobType)
                                    Else
                                        oGridN.doSetFieldValue(sFieldName, oField.Value)
                                    End If
                                End If
                            Next
                            oGridN.doSetFieldValue("U_JobNr", sCode)

                            oGridN.doSetFieldValue("U_TipCost", 0)
                            oGridN.doSetFieldValue("U_PCost", 0)

                            oGridN.doSetFieldValue("U_UseWgt", "1")
                            oGridN.doSetFieldValue("U_Wei1", 0)
                            oGridN.doSetFieldValue("U_Wei2", 0)
                            oGridN.doSetFieldValue("U_Ser1", "")
                            oGridN.doSetFieldValue("U_Ser2", "")
                            oGridN.doSetFieldValue("U_WDt1", "")
                            oGridN.doSetFieldValue("U_WDt2", "")
                            oGridN.doSetFieldValue(IDH_JOBSHD._TRdWgt, 0)
                            oGridN.doSetFieldValue("U_TipWgt", 0)
                            oGridN.doSetFieldValue("U_TipCost", 0)
                            oGridN.doSetFieldValue("U_TipTot", 0)
                            oGridN.doSetFieldValue("U_SLicCTo", 0)
                            oGridN.doSetFieldValue("U_AUOMQt", 0)
                            oGridN.doSetFieldValue("U_RdWgt", 0)
                            oGridN.doSetFieldValue("U_CstWgt", 0)
                            oGridN.doSetFieldValue("U_TCharge", 0)
                            oGridN.doSetFieldValue("U_TCTotal", 0)

                            oGridN.doSetFieldValue("U_TAUOMQt", 0)
                            oGridN.doSetFieldValue("U_PAUOMQt", 0)
                            oGridN.doSetFieldValue("U_PRdWgt", 0)
                            oGridN.doSetFieldValue("U_ProWgt", 0)
                            'oGridN.doSetFieldValue("U_ProUOM", 0)
                            oGridN.doSetFieldValue("U_PCost", 0)
                            oGridN.doSetFieldValue("U_PCTotal", 0)

                            oGridN.doSetFieldValue("U_Discnt", 0)
                            oGridN.doSetFieldValue("U_DisAmt", 0)
                            oGridN.doSetFieldValue("U_SLicCh", 0)
                            oGridN.doSetFieldValue("U_CongCh", 0)
                            oGridN.doSetFieldValue("U_Total", 0)

                            oGridN.doSetFieldValue("U_JobRmD", "")
                            oGridN.doSetFieldValue("U_JobRmT", 0)
                            oGridN.doSetFieldValue("U_RemNot", "")
                            oGridN.doSetFieldValue("U_RemCnt", 0)

                            oGridN.doSetFieldValue("U_TaxAmt", 0)
                            oGridN.doSetFieldValue("U_VtCostAmt", 0)

                            oGridN.doSetFieldValue("U_TAddChrg", 0)
                            oGridN.doSetFieldValue("U_TAddCost", 0)

                            oGridN.doSetFieldValue("U_ConNum", "")
                            oGridN.doSetFieldValue("U_LoadSht", "")

                            oGridN.doSetFieldValue("U_Comment", "")
                            oGridN.doSetFieldValue("U_IntComm", "")
                            oGridN.doSetFieldValue("U_ExtComm1", "")
                            oGridN.doSetFieldValue("U_ExtComm2", "")

                            'Mitie
                            oGridN.doSetFieldValue("U_MaximoNum", "")

                            'Analysis Code - 20150408
                            oGridN.doSetFieldValue("U_Analys1", "")
                            oGridN.doSetFieldValue("U_Analys2", "")
                            oGridN.doSetFieldValue("U_Analys3", "")
                            oGridN.doSetFieldValue("U_Analys4", "")
                            oGridN.doSetFieldValue("U_Analys5", "")

                            'TFS
                            oGridN.doSetFieldValue("U_TFSReq", "")
                            oGridN.doSetFieldValue("U_TFSNum", "")

                            'EVN 
                            oGridN.doSetFieldValue("U_EVNReq", "")
                            oGridN.doSetFieldValue("U_EVNNum", "")

                            oGridN.doSetFieldValue("U_Covera", "")
                            oGridN.doSetFieldValue("U_CoverHst", "")

                            oGridN.doSetFieldValue("U_User", goParent.gsUserName)

                            Dim dWeightTotal As Double = 0

                            Dim sItemCode As String = oTableSrc.UserFields.Fields.Item("U_ItemCd").Value
                            oGridN.doSetFieldValue("U_TaxAmt", 0)

                            oGridN.doSetFieldValue("U_Status", com.idh.bridge.lookups.FixedValues.getStatusOpen())
                            oGridN.doSetFieldValue("U_PStat", com.idh.bridge.lookups.FixedValues.getStatusOpen())
                            oGridN.doSetFieldValue("U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusUnPaid())
                            oGridN.doSetFieldValue("U_PayMeth", "")
                            oGridN.doSetFieldValue("U_CCNum", "")
                            oGridN.doSetFieldValue("U_CCType", "0")
                            oGridN.doSetFieldValue("U_CCStat", "")

                            oGridN.doSetFieldValue("U_CCExp", "")
                            oGridN.doSetFieldValue("U_CCIs", "")
                            oGridN.doSetFieldValue("U_CCSec", "")
                            oGridN.doSetFieldValue("U_CCHNum", "")
                            oGridN.doSetFieldValue("U_CCPCd", "")

                            oGridN.doSetFieldValue("U_SAORD", "")
                            oGridN.doSetFieldValue("U_TIPPO", "")
                            oGridN.doSetFieldValue("U_JOBPO", "")
                            oGridN.doSetFieldValue("U_ProPO", "")
                            oGridN.doSetFieldValue("U_GRIn", "")
                            oGridN.doSetFieldValue("U_ProGRPO", "")
                            oGridN.doSetFieldValue("U_SODlvNot", "")
                            oGridN.doSetFieldValue("U_SAINV", "")
                            oGridN.doSetFieldValue("U_SLPO", "")
                            oGridN.doSetFieldValue("U_TPCN", "")
                            oGridN.doSetFieldValue("U_TCCN", "")
                            oGridN.doSetFieldValue("U_Covera", "")
                            oGridN.doSetFieldValue("U_CoverHst", "")
                            oGridN.doSetFieldValue("U_ASTime", "")
                            oGridN.doSetFieldValue("U_AETime", "")
                            oGridN.doSetFieldValue("U_WROrd", "")
                            oGridN.doSetFieldValue("U_WRRow", "")

                            oGridN.doSetFieldValue("U_LnkPBI", "")
                            oGridN.doSetFieldValue("U_Sched", "N")
                            oGridN.doSetFieldValue("U_USched", "Y")

                            oGridN.doSetFieldValue("U_DPRRef", "")

                            oGridN.doSetFieldValue("U_WastTNN", "")
                            oGridN.doSetFieldValue("U_HazWCNN", "")
                            oGridN.doSetFieldValue("U_ExtWeig", "")

                            oGridN.doSetFieldValue("U_SiteRef", "")
                            oGridN.doSetFieldValue("U_ProRef", "")
                            oGridN.doSetFieldValue("U_CustRef", "")
                            oGridN.doSetFieldValue("U_SupRef", "")

                            If Config.INSTANCE.getParameterAsBool("MDDPVE", True) Then
                                oGridN.doSetFieldValue("U_Lorry", "")
                                oGridN.doSetFieldValue("U_LorryCd", "")
                                oGridN.doSetFieldValue("U_Driver", "")
                                oGridN.doSetFieldValue("U_TRLReg", "")
                                oGridN.doSetFieldValue("U_TRLNM", "")
                                oGridN.doSetFieldValue("U_VehTyp", "")
                            End If

                            If Config.INSTANCE.getParameterAsBool("WORCLRCA", False) Then
                                oGridN.doSetFieldValue("U_CarrCd", "")
                                oGridN.doSetFieldValue("U_CarrNm", "")
                            End If

                            oGridN.doSetFieldValue("U_ProCd", "")
                            oGridN.doSetFieldValue("U_ProNm", "")

                            '	                		oGridN.doSetFieldValue("U_ProRef", "")
                            '	                		oGridN.doSetFieldValue("U_CarrRef", "")

                            Dim sUseTodaysDate As String = Config.Parameter("MDDPTD")
                            Dim oNowDate As DateTime = DateTime.Now
                            Dim sNowTime As String = com.idh.utils.Dates.doTimeToNumStr()

                            If Not sUseTodaysDate Is Nothing AndAlso sUseTodaysDate.ToUpper.Equals("TRUE") Then
                                oGridN.doSetFieldValue("U_RDate", oNowDate) 'oParent.doDateToStr())
                            End If
                            oGridN.doSetFieldValue("U_BDate", oNowDate)
                            oGridN.doSetFieldValue("U_BTime", sNowTime)
                            oGridN.doSetFieldValue(IDH_JOBSHD._RouteCd, "")
                            oGridN.doSetFieldValue(IDH_JOBSHD._RtCdCode, "")
                            oGridN.doSetFieldValue(IDH_JOBSHD._Sample, "")
                            oGridN.doSetFieldValue(IDH_JOBSHD._SampleRef, "")
                            oGridN.doSetFieldValue(IDH_JOBSHD._SampStatus, "")
                        End If
                    Else
                        Dim sDefaultContainer As String = ""

                        If oForm.Items.Item("SKPCheck").Specific.Value = "Y" Then
                            oGridN.doSetFieldValue("U_SLicSp", "")
                            oGridN.doSetFieldValue("U_SLicNr", "")
                            oGridN.doSetFieldValue("U_SLicExp", "")
                            oGridN.doSetFieldValue("U_SLicCh", 0)
                            oGridN.doSetFieldValue("U_SLicNm", "")
                            oGridN.doSetFieldValue("U_SLicCQt", 0)
                            oGridN.doSetFieldValue("U_SLicCst", 0)
                            oGridN.doSetFieldValue("U_SLicCTo", 0)
                        Else
                            oGridN.doSetFieldValue("U_SLicSp", getFormDFValue(oForm, "U_SLicSp"))
                            oGridN.doSetFieldValue("U_SLicNr", getFormDFValue(oForm, "U_SLicNr"))
                            oGridN.doSetFieldValue("U_SLicExp", com.idh.utils.Dates.doStrToDate(getFormDFValue(oForm, "U_SLicExp")))

                            ''MA Pt791 15-06-2015 Moved part out from Grid.rowcount>1's else part 
                            Dim sOffRoad As String = getFormDFValue(oForm, "U_ORoad")
                            If sOffRoad.Equals("Y") Then
                                oGridN.doSetFieldValue("U_SLicCh", 0)
                                oGridN.doSetFieldValue("U_SLicCst", 0)
                                oGridN.doSetFieldValue("U_SLicCTo", 0)
                            Else
                                oGridN.doSetFieldValue("U_SLicCh", getFormDFValue(oForm, "U_SLicCh"))
                                oGridN.doSetFieldValue("U_SLicCst", getFormDFValue(oForm, "U_SLicCh"))
                                oGridN.doSetFieldValue("U_SLicCTo", getFormDFValue(oForm, "U_SLicCh"))

                            End If

                            If oGridN.getRowCount > 1 Then
                                'oGridN.doSetFieldValue("U_SLicCh", "0") '.GetValue("U_SLicCh", .Offset).Trim())
                                'oGridN.doSetFieldValue("U_SLicCst", "0") '.GetValue("U_SLicCh", .Offset).Trim())
                                'oGridN.doSetFieldValue("U_SLicCTo", "0") '.GetValue("U_SLicCh", .Offset).Trim())
                            Else
                                If Config.INSTANCE.getParameterAsBool("WOUSFJ", True) Then
                                    Dim iJobIndex As Integer = User.IDH_JOBTYPE_SEQ.getFirstJob(Config.CAT_WO, sIGrp)
                                    If iJobIndex > -1 Then
                                        Dim oJobSEQ As User.IDH_JOBTYPE_SEQ = User.IDH_JOBTYPE_SEQ.getInstance()
                                        oJobSEQ.gotoRow(iJobIndex)
                                        oGridN.doSetFieldValue("U_JobTp", oJobSEQ.U_JobTp)
                                    End If
                                Else
                                    User.IDH_JOBTYPE_SEQ.getInstance()
                                    'oGridN.doSetFieldValue("U_JobTp", "")
                                End If
                            End If

                            oGridN.doSetFieldValue("U_SLicNm", getFormDFValue(oForm, "U_SLicNm"))
                            oGridN.doSetFieldValue("U_SLicCQt", 1)
                        End If

                        If getFormDFValue(oForm, "U_CopTRw") = "Y" Then
                            oGridN.doSetFieldValue("U_Comment", getFormDFValue(oForm, "U_SpInst"))
                        End If
                        oGridN.doSetFieldValue("U_IntComm", "")

                        oGridN.doSetFieldValue("U_ExtComm1", "")
                        oGridN.doSetFieldValue("U_ExtComm2", "")

                        'Mitie
                        oGridN.doSetFieldValue("U_MaximoNum", "")

                        'Analysis Code - 20150408
                        oGridN.doSetFieldValue("U_Analys1", "")
                        oGridN.doSetFieldValue("U_Analys2", "")
                        oGridN.doSetFieldValue("U_Analys3", "")
                        oGridN.doSetFieldValue("U_Analys4", "")
                        oGridN.doSetFieldValue("U_Analys5", "")

                        'TFS
                        oGridN.doSetFieldValue("U_TFSReq", "")
                        oGridN.doSetFieldValue("U_TFSNum", "")

                        'EVN 
                        oGridN.doSetFieldValue("U_EVNReq", "")
                        oGridN.doSetFieldValue("U_EVNNum", "")

                        oGridN.doSetFieldValue("U_UseWgt", "1")
                        oGridN.doSetFieldValue("U_OrdWgt", "1")
                        oGridN.doSetFieldValue(IDH_JOBSHD._CarWgt, "1")
                        oGridN.doSetFieldValue("U_CusQty", "1")
                        oGridN.doSetFieldValue("U_HlSQty", "1")

                        'Set the congestion charge
                        Dim dCongCharge As Double = Config.INSTANCE.doGetCongestion(sCustomer, getFormDFValue(oForm, "U_ZpCd"), getFormDFValue(oForm, "U_Address"))
                        If dCongCharge <> 0 Then
                            Dim sCongSupplier As String = Config.Parameter("SPCONG")
                            Dim sCongName As String = Config.INSTANCE.doGetBPName(sCongSupplier)

                            oGridN.doSetFieldValue("U_CongCd", sCongSupplier)
                            oGridN.doSetFieldValue("U_SCngNm", sCongName)
                        End If
                        oGridN.doSetFieldValue("U_CongCh", dCongCharge)

                        oGridN.doSetFieldValue("U_User", goParent.gsUserName)
                        oGridN.doSetFieldValue("U_Branch", Config.INSTANCE.doGetCurrentBranch())

                        Dim sFC As String = getFormDFValue(oForm, IDH_JOBENTR._ForeCS)
                        Dim dExpectedWeight As Double = 0
                        Dim sWasteCd As String = Nothing
                        Dim sAltWasteDescCode As String = ""
                        Dim oPreSelect As PreSelect = getWFValue(oForm, "PRESELECT")
                        If oPreSelect IsNot Nothing Then
                            sWasteCd = oPreSelect.WasteCode
                        Else
                            If Config.INSTANCE.doGetDoForecasting() AndAlso sFC.Length > 0 Then
                                sWasteCd = getUFValue(oForm, "IDH_FCWC")
                                sUOM = getUFValue(oForm, "IDH_FCUOM")
                                sAltWasteDescCode = getUFValue(oForm, "IDH_ALFCWC")

                                Dim dFCWeight As Double = getUFValue(oForm, "IDH_FCREM")
                                dExpectedWeight = getWFValue(oForm, "FCEXPWGT")
                                If dExpectedWeight > dFCWeight Then
                                    dExpectedWeight = dFCWeight
                                    setWFValue(oForm, "FCEXPWGT", dExpectedWeight)
                                End If
                                'oGridN.doSetFieldValue("U_ExpLdWgt", dExpectedWeight)
                                'If getWFValue(oForm, "FCCADTYPE").Equals("C") Then
                                '	oGridN.doSetFieldValue("U_RdWgt", dExpectedWeight)
                                '	oGridN.doSetFieldValue("U_CstWgt", dExpectedWeight)
                                'Else
                                '	oGridN.doSetFieldValue("U_PRdWgt", dExpectedWeight)
                                '	oGridN.doSetFieldValue("U_ProWgt", dExpectedWeight)
                                'End If

                                oGridN.doSetFieldValue("U_UOM", sUOM)
                            Else
                                sUOM = Config.INSTANCE.getBPUOM(sCustomer)
                                oGridN.doSetFieldValue("U_UOM", sUOM)
                                'oGridN.doSetFieldValue("U_ExpLdWgt", 0)
                            End If
                        End If

                        Dim sLckPrc As String = getFormDFValue(oForm, IDH_JOBENTR._LckPrc)
                        If Not sLckPrc Is Nothing AndAlso sLckPrc = "Y" Then
                            Dim sUUOM As String = getWFValue(oForm, "UOM")
                            Dim sPUOM As String = getWFValue(oForm, "PUOM")
                            Dim dTipCost As Double = getWFValue(oForm, "TIPCST")
                            Dim dProdCst As Double = getWFValue(oForm, "PRODCST")
                            'Dim dHaulageCst As Double = getWFValue(oForm, "HAULAGECST")
                            Dim dDistance As Double = getWFValue(oForm, "DISTANCE")
                            Dim dTipChrg As Double = getWFValue(oForm, "TIPCHRG")
                            'Dim dHaulageChrg As Double = getWFValue(oForm, "HAULAGECHRG")

                            oGridN.doSetFieldValue(IDH_JOBSHD._PUOM, sPUOM)
                            oGridN.doSetFieldValue(IDH_JOBSHD._PCost, dProdCst)
                            oGridN.doSetFieldValue(IDH_JOBSHD._ProUOM, sPUOM)

                            oGridN.doSetFieldValue(IDH_JOBSHD._TCharge, dTipChrg)
                            If sFC.Length = 0 Then 'OrElse sUOM Is Nothing Then
                                oGridN.doSetFieldValue(IDH_JOBSHD._UOM, sUUOM)
                            End If

                            oGridN.doSetFieldValue(IDH_JOBSHD._TipCost, dTipCost)

                            'Dim dDistance As Double = getSharedData(oForm, "DISTANCE")
                            Dim dHaulageCst As Double
                            Dim oHaulCst As IDH_HAULCST = New IDH_HAULCST()
                            dHaulageCst = oHaulCst.getChrgPriceForDistance(dDistance)
                            If dHaulageCst < 0 Then
                                dHaulageCst = getWFValue(oForm, "HAULAGECHRG")
                            End If
                            'oGridN.doSetFieldValue(IDH_JOBSHD._OrdCost, getWFValue(oForm, "HAULCST"))
                            oGridN.doSetFieldValue(IDH_JOBSHD._OrdCost, dHaulageCst)

                            Dim dHualChrg As Double = getWFValue(oForm, "HAULAGECST")
                            'oGridN.doSetFieldValue(IDH_JOBSHD._CusChr, getWFValue(oForm, "HAULCHRG"))
                            oGridN.doSetFieldValue(IDH_JOBSHD._CusChr, dHualChrg)

                            oGridN.doSetFieldValue(IDH_JOBSHD._LckPrc, "Y")

                            If sWasteCd Is Nothing Then
                                sWasteCd = getWFValue(oForm, "WASTCD")
                            End If

                            Dim sDefWCDesc As String = Config.INSTANCE.doGetItemDescription(sWasteCd)
                            oGridN.doSetFieldValue("U_WasCd", sWasteCd)
                            oGridN.doSetFieldValue("U_WasDsc", sDefWCDesc)

                            dExpectedWeight = getWFValue(oForm, "FCEXPWGT")
                            'oGridN.doSetFieldValue("U_ExpLdWgt", dExpectedWeight)

                            sDefaultContainer = getWFValue(oForm, "CONTAINER")
                        Else
                            oGridN.doSetFieldValue(IDH_JOBSHD._LckPrc, "N")

                            If sWasteCd Is Nothing OrElse sWasteCd.Length = 0 Then
                                sWasteCd = Config.Parameter("MDDEWA")
                            End If

                            Dim sDefWCDesc As String = Config.INSTANCE.doGetItemDescription(sWasteCd)
                            oGridN.doSetFieldValue("U_WasCd", sWasteCd)
                            oGridN.doSetFieldValue("U_WasDsc", sDefWCDesc)
                        End If
                        '## MA Start 08-09-2015 Issue#909
                        oGridN.doSetFieldValue("U_AltWasDsc", sAltWasteDescCode)
                        '## MA End 08-09-2015 Issue#909

                        'oGridN.doSetFieldValue("U_AltWasDsc", getFormDFValue(oForm, "U_AltWasDsc"))

                        oGridN.doSetFieldValue("U_CustRef", getFormDFValue(oForm, "U_CustRef"))
                        oGridN.doSetFieldValue("U_SupRef", getFormDFValue(oForm, "U_SupRef"))

                        oGridN.doSetFieldValue("U_ProRef", getFormDFValue(oForm, "U_ProRef"))
                        oGridN.doSetFieldValue("U_SiteRef", getFormDFValue(oForm, "U_SiteRef"))
                        oGridN.doSetFieldValue(IDH_JOBSHD._TrnCode, getFormDFValue(oForm, IDH_JOBENTR._TRNCd))
                        ''Set the Disposal Site Address for the first time
                        ''sVal = oGridN.doGetFieldValue(IDH_JOBSHD._SAddress).Trim()
                        ''If sVal Is Nothing OrElse sVal.Length = 0 Then
                        ''oGridN.doSetFieldValue(IDH_JOBSHD._SAddress, getFormDFValue(oForm, IDH_JOBENTR._SAddress))
                        ''End If

                        oGridN.doSetFieldValue("U_WRRow", "")
                        oGridN.doSetFieldValue("U_WROrd", "")

                        oGridN.doSetFieldValue("U_LnkPBI", "")
                        oGridN.doSetFieldValue("U_DPRRef", "")

                        oGridN.doSetFieldValue("U_Sched", "N")
                        oGridN.doSetFieldValue("U_USched", "Y")

                        oGridN.doSetFieldValue("U_PayMeth", "")
                        oGridN.doSetFieldValue("U_PayStat", com.idh.bridge.lookups.FixedValues.getStatusUnPaid())
                        oGridN.doSetFieldValue("U_CCNum", "")
                        oGridN.doSetFieldValue("U_CCType", "0")
                        oGridN.doSetFieldValue("U_CCStat", "")

                        'Get the BP Oligation
                        Dim sOlig As String = Config.INSTANCE.getBPOligated(sCustomer)
                        'Dim sOlig As String = .GetValue("U_Obligated", .Offset).Trim()
                        oGridN.doSetFieldValue("U_Obligated", sOlig)

                        oGridN.doSetFieldValue("U_WastTNN", "")
                        'oGridN.doSetFieldValue("U_HazWCNN", "")
                        oGridN.doSetFieldValue("U_ExtWeig", "")

                        oGridN.doSetFieldValue("U_ConNum", "")
                        oGridN.doSetFieldValue("U_CarrReb", "N")
                        oGridN.doSetFieldValue("U_CustReb", "N")

                        'Get the BP Oligation
                        Dim sCustCs As String = Config.INSTANCE.getBPCs(sCustomer)
                        oGridN.doSetFieldValue("U_CustCs", sCustCs)

                        If String.IsNullOrEmpty(sDefaultContainer) Then
                            sDefaultContainer = Config.INSTANCE.getDefaultWOContainer()
                        End If

                        If String.IsNullOrEmpty(sDefaultContainer) = False Then
                            Dim sContainerDescription As String = com.idh.bridge.lookups.Config.INSTANCE.doGetItemDescription(sDefaultContainer)
                            If sContainerDescription.Length > 0 Then
                                oGridN.doSetFieldValue("U_ItemCd", sDefaultContainer)
                                oGridN.doSetFieldValue("U_ItemDsc", sContainerDescription)
                            End If
                        End If

                        Dim sSwitchExpectedWeight As String = Config.Parameter("SWTCEXW")
                        If String.IsNullOrEmpty(sSwitchExpectedWeight) = False Then
                            If String.IsNullOrEmpty(sWasteCd) = False AndAlso sSwitchExpectedWeight = "DW" Then
                                dExpectedWeight = Config.INSTANCE.getItemDefaultWeight(sWasteCd)
                                'oGridN.doSetFieldValue("U_ExpLdWgt", dExpectedWeight)
                            ElseIf String.IsNullOrEmpty(sDefaultContainer) = False AndAlso sSwitchExpectedWeight = "DWC" Then
                                dExpectedWeight = Config.INSTANCE.getItemDefaultWeight(sDefaultContainer)
                                'oGridN.doSetFieldValue("U_ExpLdWgt", dExpectedWeight)
                            End If
                            'oGridN.doSetFieldValue("U_ExpLdWgt", dExpectedWeight)
                        End If
                        oGridN.doSetFieldValue("U_ExpLdWgt", dExpectedWeight)
                        oGridN.doSetFieldValue(IDH_JOBSHD._RouteCd, "")
                        oGridN.doSetFieldValue(IDH_JOBSHD._RtCdCode, "")
                        oGridN.doSetFieldValue(IDH_JOBSHD._Sample, "")
                        oGridN.doSetFieldValue(IDH_JOBSHD._SampleRef, "")
                        oGridN.doSetFieldValue(IDH_JOBSHD._SampStatus, "")
                    End If
                Else
                    oData.Add("DOCHANGE")

                    Dim dQty As Double
                    Dim dQty2 As Double


                    dQty = oGridN.doGetFieldValue(IDH_JOBSHD._CarWgt)
                    If dQty = 0 Then
                        oGridN.doSetFieldValue(IDH_JOBSHD._CarWgt, 1)
                    End If

                    dQty2 = oGridN.doGetFieldValue(IDH_JOBSHD._OrdWgt)
                    If dQty2 = 0 Then
                        If dQty <> 0 Then
                            oGridN.doSetFieldValue(IDH_JOBSHD._OrdWgt, dQty)
                        Else
                            oGridN.doSetFieldValue(IDH_JOBSHD._OrdWgt, 1)
                        End If
                    End If

                    dQty = oGridN.doGetFieldValue("U_CusQty")
                    If dQty = 0 Then
                        oGridN.doSetFieldValue("U_CusQty", 1)
                    End If

                    dQty2 = oGridN.doGetFieldValue("U_HlSQty")
                    If dQty2 = 0 Then
                        If dQty <> 0 Then
                            oGridN.doSetFieldValue("U_HlSQty", dQty)
                        Else
                            oGridN.doSetFieldValue("U_HlSQty", 1)
                        End If
                    End If
                End If
                oData.Add("WAIT")

                Dim sOrigin As String = getFormDFValue(oForm, "U_Origin")
                oGridN.doSetFieldValue("U_Origin", sOrigin)

                'Set the required times
                iTimeF = oGridN.doGetFieldValue("U_RTime")
                iTimeT = oGridN.doGetFieldValue("U_RTimeT")

                If iTimeF = 0 Then
                    iTimeF = goParent.doTimeStrToInt(Config.ParameterWithDefault("WOTMFR", "800")) 'getUFValue(oForm, "IDH_RTIMEF")
                    oGridN.doSetFieldValue("U_RTime", iTimeF)
                End If

                If iTimeT = 0 Then
                    iTimeT = goParent.doTimeStrToInt(Config.ParameterWithDefault("WOTMTO", "1700"))  'getUFValue(oForm, "IDH_RTIMET")
                    oGridN.doSetFieldValue("U_RTimeT", iTimeT)
                End If

                sVal = oGridN.doGetFieldValue(IDH_JOBSHD._LckPrc).Trim()
                If sVal Is Nothing OrElse sVal.Length = 0 Then
                    Dim sLckPrc As String = getFormDFValue(oForm, IDH_JOBENTR._LckPrc)
                    If Not sLckPrc Is Nothing AndAlso sLckPrc = "Y" Then
                        oGridN.doSetFieldValue(IDH_JOBSHD._LckPrc, "Y")
                    Else
                        oGridN.doSetFieldValue(IDH_JOBSHD._LckPrc, "N")
                    End If
                End If

                sVal = oGridN.doGetFieldValue(IDH_JOBSHD._CustNm).Trim()
                If sVal Is Nothing OrElse sVal.Length = 0 Then
                    oGridN.doSetFieldValue(IDH_JOBSHD._CustNm, getFormDFValue(oForm, "U_CardNM"))
                End If

                sVal = oGridN.doGetFieldValue("U_CarrCd").Trim()
                If sVal Is Nothing OrElse sVal.Length = 0 Then
                    oGridN.doSetFieldValue("U_CarrCd", getFormDFValue(oForm, "U_CCardCd"))
                End If

                sVal = oGridN.doGetFieldValue("U_CarrNm").Trim()
                If sVal Is Nothing OrElse sVal.Length = 0 Then
                    oGridN.doSetFieldValue("U_CarrNm", getFormDFValue(oForm, "U_CCardNM"))
                End If

                sVal = oGridN.doGetFieldValue("U_ProCd").Trim()
                If sVal Is Nothing OrElse sVal.Length = 0 Then
                    oGridN.doSetFieldValue("U_ProCd", getFormDFValue(oForm, "U_PCardCd"))
                End If

                sVal = oGridN.doGetFieldValue("U_ProNm").Trim()
                If sVal Is Nothing OrElse sVal.Length = 0 Then
                    oGridN.doSetFieldValue("U_ProNm", getFormDFValue(oForm, "U_PCardNM"))
                End If

                sVal = oGridN.doGetFieldValue("U_Tip").Trim()
                If sVal Is Nothing OrElse sVal.Length = 0 Then
                    oGridN.doSetFieldValue("U_Tip", getFormDFValue(oForm, "U_SCardCd"))
                End If

                If sLineID1 Is Nothing OrElse sLineID1.Length = 0 Then
                    sVal = oGridN.doGetFieldValue(IDH_JOBSHD._SAddress).Trim()
                    If sVal Is Nothing OrElse sVal.Length = 0 Then
                        oGridN.doSetFieldValue(IDH_JOBSHD._SAddress, getFormDFValue(oForm, IDH_JOBENTR._SAddress))
                    End If
                    sVal = oGridN.doGetFieldValue(IDH_JOBSHD._SAddrsLN).Trim()
                    If sVal Is Nothing OrElse sVal.Length = 0 Then
                        oGridN.doSetFieldValue(IDH_JOBSHD._SAddrsLN, getFormDFValue(oForm, IDH_JOBENTR._SAddrsLN))
                    End If
                End If

                sVal = oGridN.doGetFieldValue("U_TipNm").Trim()
                If sVal Is Nothing OrElse sVal.Length = 0 Then
                    oGridN.doSetFieldValue("U_TipNm", getFormDFValue(oForm, "U_SCardNM"))
                End If

                sVal = oGridN.doGetFieldValue("U_CntrNo").Trim()
                If sVal Is Nothing OrElse sVal.Length = 0 Then
                    sVal = getFormDFValue(oForm, "U_CntrNo")
                    If sVal.Length > 0 Then
                        oGridN.doSetFieldValue("U_CntrNo", sVal)
                    End If
                End If

                sVal = oGridN.doGetFieldValue("U_JobNr").Trim()
                If sVal Is Nothing OrElse sVal.Length = 0 Then
                    oGridN.doSetFieldValue("U_JobNr", getFormDFValue(oForm, "Code"))
                End If

                sVal = oGridN.doGetFieldValue("U_ItmGrp").Trim()
                If sVal Is Nothing OrElse sVal.Length = 0 Then
                    oGridN.doSetFieldValue("U_ItmGrp", sIGrp)
                End If

                sVal = oGridN.doGetFieldValue("U_ItemCd").Trim()
                If sVal Is Nothing OrElse sVal.Length = 0 Then
                    oGridN.doSetFieldValue("U_ItemCd", sContainerCode)
                End If

                sVal = oGridN.doGetFieldValue("U_ItemDsc").Trim()
                If sVal Is Nothing OrElse sVal.Length = 0 Then
                    oGridN.doSetFieldValue("U_ItemDsc", sContainerName)
                End If

                sVal = oGridN.doGetFieldValue("U_CustRef").Trim()
                If sVal Is Nothing OrElse sVal.Length = 0 Then
                    oGridN.doSetFieldValue("U_CustRef", getFormDFValue(oForm, "U_CustRef"))
                End If

                sUOM = oGridN.doGetFieldValue(IDH_JOBSHD._UOM).Trim()
                If sUOM Is Nothing OrElse sUOM.Length = 0 Then
                    sUOM = Config.INSTANCE.getDefaultUOM() ''"Kg"
                End If
                oGridN.doSetFieldValue(IDH_JOBSHD._UOM, sUOM)

                sVal = oGridN.doGetFieldValue(IDH_JOBSHD._PUOM).Trim()
                If sVal Is Nothing OrElse sVal.Length = 0 Then
                    'sVal = Config.INSTANCE.getDefaultUOM(goParent) ''"Kg"
                    sVal = sUOM
                End If
                oGridN.doSetFieldValue(IDH_JOBSHD._PUOM, sVal)

                sVal = oGridN.doGetFieldValue(IDH_JOBSHD._ProUOM).Trim()
                If sVal Is Nothing OrElse sVal.Length = 0 Then
                    'sVal = Config.INSTANCE.getDefaultUOM(goParent) ''"Kg"
                    sVal = sUOM
                End If
                oGridN.doSetFieldValue(IDH_JOBSHD._ProUOM, sVal)

                'Set BP Forecast Multi Currency 
                'Dim sForecastCode As String = getFormDFValue(oForm, "U_ForeCS")
                Dim sFCCurrency As String = getWFValue(oForm, "FCBPCURR")
                If sForecastCode IsNot Nothing AndAlso sForecastCode.Length > 0 Then
                    If sFCCurrency IsNot Nothing AndAlso sFCCurrency.Length > 0 Then
                        oGridN.doSetFieldValue(IDH_JOBSHD._DispCgCc, sFCCurrency)
                        oGridN.doSetFieldValue(IDH_JOBSHD._CarrCgCc, "")
                        oGridN.doSetFieldValue(IDH_JOBSHD._PurcCoCc, "")
                        oGridN.doSetFieldValue(IDH_JOBSHD._DispCoCc, sFCCurrency)
                        oGridN.doSetFieldValue(IDH_JOBSHD._CarrCoCc, "")
                        oGridN.doSetFieldValue(IDH_JOBSHD._LiscCoCc, "")
                    End If
                End If

                Return True
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error preparing the Row Data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBPR", {Nothing})
            Finally
                oForm.Freeze(False)
            End Try
            Return True
        End Function

        'Find the record containing the Customer and/or Order number
        Protected Overrides Function doFind(ByVal oForm As SAPbouiCOM.Form, ByRef sSwitch As String) As Boolean
            oForm.Freeze(True)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing

            '            Dim sMode As String
            Try
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    Dim sOrder As String = ""
                    Dim sCustomer As String = ""

                    sOrder = getFormDFValue(oForm, "Code", True).Trim()
                    sCustomer = getFormDFValue(oForm, "U_CardCd", True).Trim()

                    Dim sQry As String
                    oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    sQry = "SELECT Top 1 Code from [" & getFormMainTable() & "] Where Code = Code "
                    If sOrder.Length() > 0 Then
                        sQry = sQry & " AND Code = '" & sOrder & "'"
                    End If
                    If sCustomer.Length() > 0 Then
                        sCustomer = sCustomer.Replace("*", "%")
                        sQry = sQry & " AND U_CardCd LIKE '" & sCustomer & "%'"
                    End If

                    sQry = sQry & " Order by Code DESC" '" Order by CAST(Code As Numeric) DESC"
                    oRecordSet.DoQuery(sQry)
                    If oRecordSet.RecordCount > 0 Then
                        Dim db As SAPbouiCOM.DBDataSource
                        db = getFormMainDataSource(oForm)

                        sOrder = oRecordSet.Fields.Item("Code").Value.Trim()
                        db.Clear()
                        Dim oConditions As SAPbouiCOM.Conditions = New SAPbouiCOM.Conditions
                        Dim oCondition As SAPbouiCOM.Condition = oConditions.Add
                        oCondition.Alias = "Code"
                        oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                        oCondition.CondVal = sOrder
                        db.Query(oConditions)

                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        doLoadData(oForm)
                        Return True
                    Else
                        'com.idh.bridge.DataHandler.INSTANCE.doError("The Waste Order was not found: " & sOrder & "." & sCustomer)
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("The Waste Order was not found: " & sOrder & "." & sCustomer, "ERSYFWO", {sOrder, sCustomer})
                        Return False
                    End If
                End If
                Return False
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error finding the WO.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFWO", {Nothing})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
                oForm.Freeze(False)
            End Try
            Return False
        End Function

        Public Overrides Sub doSetFindDefaults(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        Protected Overrides Function doOtherMarketingProcesses(ByVal oForm As SAPbouiCOM.Form, ByVal oChangedAddedRows As ArrayList) As Boolean
            Dim oUpdateGrid As UpdateGrid
            oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")

            Dim oWO As IDH_JOBENTR = thisWO(oForm)
            Dim bResult As Boolean = True

            Dim sCode As String
            Dim oWrkWOR As IDH_JOBSHD

            If oChangedAddedRows IsNot Nothing Then
                oWrkWOR = New IDH_JOBSHD()

                For Each iCurDataRow As Integer In oChangedAddedRows
                    oUpdateGrid.setCurrentDataRowIndex(iCurDataRow)

                    sCode = oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Code)
                    If sCode.Trim().Length > 0 Then

                        oWrkWOR.SBOFormGUIOnly = oForm
                        If oWrkWOR.getByKey(sCode) Then
                            bResult = bResult And oWrkWOR.doMarketingDocuments()
                        End If
                    End If
                Next
            End If
            Return bResult
        End Function

        'Protected Overrides Function doOtherMarketingProcesses(ByVal oForm As SAPbouiCOM.Form, ByVal sHead As String, ByVal sRow As String) As Boolean
        '    Dim oWOR As IDH_JOBSHD = thisWOR(oForm)
        '    Dim bDoTheMarkDocuments As Boolean = Config.ParameterAsBool("MDAGEN", False)
        '    Dim bDoAPInvoice As Boolean = False
        '    Dim bResult As Boolean = True

        '    If oWOR.TRNCdDecodeRecord IsNot Nothing Then
        '        bDoTheMarkDocuments = True
        '        If oWOR.TRNCdDecodeRecord.Trigger = "3" AndAlso oWOR.U_BPWgt = 0 Then
        '            Return bResult
        '        End If

        '        bDoAPInvoice = oWOR.TRNCdDecodeRecord.APInvoice
        '    End If

        '    Dim sForecast As String = getFormDFValue(oForm, "U_ForeCS")
        '    Dim oForcast As IDH_BPFORECST = getWFValue(oForm, "FCDBObject")
        '    If oForcast Is Nothing Then
        '        com.idh.dbObjects.User.IDH_BPFORECST.doUpdateForecast(sForecast, sHead)
        '    Else
        '        oForcast.doUpdateRowWeights()
        '    End If

        '    MyBase.doOtherMarketingProcesses(oForm, sHead, sRow)

        '    'If bDoTheMarkDocuments Then
        '    '    doFoc(sHead, Nothing)
        '    '    doOrders(sHead, Nothing)
        '    '    doInvoicesAR(sHead, Nothing)
        '    '    doRebates(sHead, Nothing)
        '    'End If

        '    If bDoTheMarkDocuments Then
        '        Dim bAllowZerroValues As Boolean = Config.INSTANCE.getParameterAsBool("MDALLZE", False)
        '        Dim bAllowPOAtStart As Boolean = Config.INSTANCE.getParameterAsBool("MDPOBGI", False)

        '        If oWOR.U_RowSta <> FixedValues.getStatusDeleted() Then
        '            'If oWOR.U_Status = FixedValues.getDoFocStatus() AndAlso Dates.isValidDate(oWOR.U_AEDate) AndAlso oWOR.U_CstWgt <> 0 Then
        '            '    bResult &= MarketingDocs.doFoc(IDH_DISPORD.TableName, IDH_DISPROW.TableName, "DO", oWOR.U_JobNr, oWOR.Code)
        '            'End If

        '            Dim bDoSO As Boolean = False
        '            If bAllowZerroValues = False Then
        '                bDoSO = oWOR.U_Total <> 0 AndAlso oWOR.U_Status = FixedValues.getDoOrderStatus()
        '            Else
        '                bDoSO = oWOR.U_Status = FixedValues.getDoOrderStatus()
        '            End If

        '            Dim bDoPO As Boolean = False
        '            If (bAllowZerroValues = False) Then
        '                bDoPO = oWOR.U_JCost <> 0 AndAlso oWOR.U_PStat.StartsWith(FixedValues.getDoOrderStatus())
        '            Else
        '                bDoPO = oWOR.U_PStat.StartsWith(FixedValues.getDoOrderStatus())
        '            End If

        '            If ((bDoSO OrElse bDoPO) AndAlso oWOR.U_RowSta <> FixedValues.getStatusBillingQuarantine() AndAlso Dates.isValidDate(oWOR.U_AEDate)) Then
        '                bResult = bResult AndAlso MarketingDocs.doOrders(IDH_DISPORD.TableName, IDH_DISPROW.TableName, "DO", oWOR.U_JobNr, oWOR.Code, Nothing, -1, DateTime.MinValue, bDoSO, bDoPO)
        '            End If

        '            If (oWOR.U_Status = FixedValues.getDoInvoiceStatus() OrElse oWOR.U_Status = FixedValues.getDoPInvoiceStatus()) AndAlso
        '                Dates.isValidDate(oWOR.U_AEDate) AndAlso oWOR.U_Total <> 0 Then
        '                bResult = bResult AndAlso MarketingDocs.doInvoicesAR(IDH_DISPORD.TableName, IDH_DISPROW.TableName, "DO", oWOR.U_JobNr, oWOR.Code)
        '            End If

        '            If (oWOR.U_Status = FixedValues.getDoRebateStatus() OrElse oWOR.U_PStat.StartsWith(FixedValues.getDoRebateStatus())) AndAlso
        '                Dates.isValidDate(oWOR.U_AEDate) AndAlso (oWOR.U_Total <> 0 OrElse oWOR.U_JCost <> 0) Then
        '                bResult = bResult AndAlso MarketingDocs.doRebates(IDH_DISPORD.TableName, IDH_DISPROW.TableName, "DO", oWOR.U_JobNr, oWOR.Code)
        '            End If

        '            ''Reload for now until we generate it all from the Row
        '            'Dim oWrkWOR As IDH_JOBSHD = New IDH_JOBSHD()
        '            'If oWrkWOR.getByKey(oWOR.Code) Then
        '            '    If bDoAPInvoice Then
        '            '        bResult = bResult AndAlso oWrkWOR.doSupplierInvoiceAP(True)
        '            '    End If
        '            'End If
        '        End If
        '        Return bResult
        '    End If
        '    Return True
        'End Function
        'Protected Overrides Sub doUpdateFinalBatch(oForm As SAPbouiCOM.Form)
        '    IDH_PVHBTH.doUpdateContainerQtyOfWORs(getFormDFValue(oForm, "Code").ToString())
        'End Sub
        'Protected Overrides Sub doGetNCRnLabChangedRows(oForm As SAPbouiCOM.Form, ByRef oAddedRows As System.Collections.ArrayList, _
        '                                            ByRef oModfieldRows As System.Collections.ArrayList, ByRef oNCRRows_NCR As System.Collections.ArrayList, _
        '                                            ByRef oNCRRows_NCRCompliant As System.Collections.ArrayList, _
        '                                            ByRef oNCRRows_NCRResolved As System.Collections.ArrayList, _
        '                                            ByRef oLabTaskRows As System.Collections.ArrayList)
        '    Dim oUpdateGrid As UpdateGrid
        '    oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
        '    'Dim oNCRRows As New ArrayList
        '    'Dim sOrderNr As String = Nothing
        '    Dim bHasFinalBatch
        '    If Not oAddedRows Is Nothing AndAlso oAddedRows.Count > 0 Then
        '        'sOrderNr = getFormDFValue(oForm, "Code")
        '        Dim iRow As Integer
        '        'If Not oAddedRows Is Nothing Then
        '        For iIndex As Integer = 0 To oAddedRows.Count - 1
        '            iRow = oAddedRows.Item(iIndex)
        '            oUpdateGrid.setCurrentDataRowIndex(iRow)
        '            If oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys1) = "Yes" OrElse oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys1) = "Y" Then
        '                oNCRRows_NCR.Add(iRow)
        '            End If
        '            If oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys2) = "Yes" OrElse oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys2) = "Y" Then
        '                oNCRRows_NCRCompliant.Add(iRow)
        '            End If
        '            If oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys3) = "Yes" OrElse oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys3) = "Y" Then
        '                oNCRRows_NCRResolved.Add(iRow)
        '            End If
        '            If oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Sample) = "Y" Then
        '                oLabTaskRows.Add(iRow)
        '            End If
        '        Next
        '        'End If
        '    End If
        '    If Not oModfieldRows Is Nothing AndAlso oModfieldRows.Count > 0 Then
        '        'sOrderNr = getFormDFValue(oForm, "Code")
        '        Dim iRow As Integer
        '        'If Not oModfieldRows Is Nothing Then
        '        For iIndex As Integer = 0 To oModfieldRows.Count - 1
        '            iRow = oModfieldRows.Item(iIndex)
        '            oUpdateGrid.setCurrentDataRowIndex(iRow)
        '            If oUpdateGrid.hasFieldChanged(IDH_JOBSHD._Analys1) AndAlso (oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys1) = "Yes" OrElse oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys1) = "Y") Then
        '                oNCRRows_NCR.Add(iRow)
        '            End If
        '            If oUpdateGrid.hasFieldChanged(IDH_JOBSHD._Analys2) AndAlso (oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys2) = "Yes" OrElse oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys2) = "Y") Then
        '                oNCRRows_NCRCompliant.Add(iRow)
        '            End If
        '            If oUpdateGrid.hasFieldChanged(IDH_JOBSHD._Analys3) AndAlso (oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys3) = "Yes" OrElse oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys3) = "Y") Then
        '                oNCRRows_NCRResolved.Add(iRow)
        '            End If

        '            If (oUpdateGrid.hasFieldChanged(IDH_JOBSHD._Sample) OrElse oUpdateGrid.hasFieldChanged(IDH_JOBSHD._SampleRef)) AndAlso oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Sample) = "Y" Then
        '                oLabTaskRows.Add(iRow)
        '            End If

        '        Next
        '        'End If
        '    End If

        'End Sub
        'Protected Overrides Function doRemoteAlert(ByVal oForm As SAPbouiCOM.Form, ByRef oAddedRows As ArrayList) As Boolean
        '    Dim oUpdateGrid As UpdateGrid
        '    oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")

        '    Dim sOrderNr As String = Nothing
        '    If Not oAddedRows Is Nothing AndAlso oAddedRows.Count > 0 Then
        '        sOrderNr = getFormDFValue(oForm, "Code")
        '        Dim iRow As Integer
        '        If Not oAddedRows Is Nothing Then
        '            For iIndex As Integer = 0 To oAddedRows.Count - 1
        '                iRow = oAddedRows.Item(iIndex)
        '                oUpdateGrid.setCurrentDataRowIndex(iRow)
        '                Dim sCode As String = oUpdateGrid.doGetFieldValue("Code")

        '                Dim sCarrCd As String = oUpdateGrid.doGetFieldValue("U_CarrCd")
        '                Dim sSiteCd As String = oUpdateGrid.doGetFieldValue("U_Tip")

        '                Dim sSubject As String = "Remote Waste Order"
        '                Dim sMessage As String = "The Following Waste Order (" & sOrderNr & "." & sCode & ") was Raised on " & goParent.goDICompany.CompanyName & ":" & Conversions.ToChar(10) & _
        '                            "Customer: [" & oUpdateGrid.doGetFieldValue("U_CustCd") & "] " & oUpdateGrid.doGetFieldValue("U_CustNm") & Conversions.ToChar(10) & _
        '                            "Customer Site: [" & getFormDFValue(oForm, "U_Address") & "] " & getFormDFValue(oForm, "U_Street") & _
        '                                     ", " & getFormDFValue(oForm, "U_Block") & ", " & getFormDFValue(oForm, "U_City") & _
        '                                     ", " & getFormDFValue(oForm, "U_ZpCd") & Conversions.ToChar(10) & _
        '                            "Contact Number: " & getFormDFValue(oForm, "U_SiteTl") & Conversions.ToChar(10) & _
        '                            "Customer RefNr: " & oUpdateGrid.doGetFieldValue("U_CustRef") & Conversions.ToChar(10) & _
        '                            "Waste Carrier: [" & sCarrCd & "] " & oUpdateGrid.doGetFieldValue("U_CarrNm") & Conversions.ToChar(10) & _
        '                            "Disposal Site: [" & sSiteCd & "] " & oUpdateGrid.doGetFieldValue("U_TipNm") & Conversions.ToChar(10) & _
        '                            "Requested Date: " & goParent.doDateToSBODisplay(oUpdateGrid.doGetFieldValue("U_RDate")) & Conversions.ToChar(10) & _
        '                            "Special Instruction: " & getFormDFValue(oForm, "U_SpInst") & Conversions.ToChar(10) & _
        '                            "Order Type: " & oUpdateGrid.doGetFieldValue("U_JobTp") & Conversions.ToChar(10) & _
        '                            "Container: [" & oUpdateGrid.doGetFieldValue("U_ItemCd") & "] " & oUpdateGrid.doGetFieldValue("U_ItemDsc") & Conversions.ToChar(10) & _
        '                            "Waste Material: [" & oUpdateGrid.doGetFieldValue("U_WasCd") & "] " & oUpdateGrid.doGetFieldValue("U_WasDsc") & Conversions.ToChar(10) & _
        '                            "Comment: " & oUpdateGrid.doGetFieldValue("U_Comment") & Conversions.ToChar(10)

        '                Dim oLinkType As SAPbouiCOM.BoLinkedObject
        '                Dim sLinkValue As String = ""
        '                'Dim bDoSend As Boolean
        '                Dim sCompName As String = goParent.goDICompany.CompanyName

        '                If Not sCarrCd Is Nothing AndAlso _
        '                    Not sSiteCd Is Nothing AndAlso _
        '                    sCarrCd.Length > 0 AndAlso _
        '                    sCarrCd.Equals(sSiteCd) = True AndAlso _
        '                    sCarrCd.Equals(sCompName) = False Then
        '                    DirectCast(goParent, idh.main.MainForm).doSendRemoteAlert(sCarrCd, goParent.goDICompany.UserName, sSubject, sMessage, oLinkType, sLinkValue, True)
        '                Else
        '                    If Not sCarrCd Is Nothing And sCarrCd.Length > 0 AndAlso sCarrCd.Equals(sCompName) = False Then
        '                        DirectCast(goParent, idh.main.MainForm).doSendRemoteAlert(sCarrCd, goParent.goDICompany.UserName, sSubject, sMessage, oLinkType, sLinkValue, True)
        '                    End If
        '                    If Not sSiteCd Is Nothing And sSiteCd.Length > 0 AndAlso sSiteCd.Equals(sCompName) = False Then
        '                        DirectCast(goParent, idh.main.MainForm).doSendRemoteAlert(sSiteCd, goParent.goDICompany.UserName, sSubject, sMessage, oLinkType, sLinkValue, True)
        '                    End If
        '                End If
        '            Next
        '        End If
        '    End If
        '    Return True
        'End Function

        Public Overrides Function doBeforeBtn1(ByVal oForm As SAPbouiCOM.Form) As Boolean
            'Dim sCardCode As String = getFormDFValue(oForm, "U_CardCd", True)
            'Dim sCustAddress As String = getFormDFValue(oForm, "U_Address", True)
            'Dim sPCardCode As String = getFormDFValue(oForm, "U_PCardCd", True)
            'Dim sPAddress As String = getFormDFValue(oForm, "U_PAddress", True)
            'If sCardCode.Trim <> "" AndAlso sCustAddress.Trim <> "" Then
            '    If doValidateAddress(sCardCode, sCustAddress, oForm) = False Then
            '        PARENT.APPLICATION.StatusBar.SetText("Invalid customer address.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            '        Return False
            '    End If
            'ElseIf sPCardCode.Trim <> "" AndAlso sPAddress.Trim <> "" Then
            '    If doValidateAddress(sPCardCode, sPAddress, oForm) = False Then
            '        PARENT.APPLICATION.StatusBar.SetText("Invalid producer address.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            '        Return False
            '    End If
            'End If
            Return MyBase.doBeforeBtn1(oForm)

        End Function

        Public Overrides Function doAfterBtn1(ByVal oForm As SAPbouiCOM.Form, ByVal bDoNew As Boolean) As Boolean
            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then

                oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                oForm.Freeze(True)
                Try
                    'If wasSetByUser(oForm, "IDH_CUST") = True
                    '
                    'End If
                    setFormDFValue(oForm, "Code", "")
                    doLoadData(oForm)
                Catch ex As Exception
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing Button 1 process.")
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXBNT1", {Nothing})
                End Try
                oForm.Freeze(False)
            ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                setFormDFValue(oForm, "Code", "")
                doLoadData(oForm)
            End If
            Return True
        End Function

        Protected Overrides Sub doChooseProducer(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, sItemID As String)
            Dim sCardCode As String = getFormDFValue(oForm, "U_PCardCd", True)
            Dim sCardName As String = getFormDFValue(oForm, "U_PCardNM", True)
            If sCardCode.Length = 0 AndAlso sCardName.Length = 0 Then
                sCardCode = getFormDFValue(oForm, "U_CardCd", True)
            End If

            setSharedData(oForm, "TRG", "PR")

            If Config.INSTANCE.doGetDoBuyingAndTrade() Then
                setSharedData(oForm, "IDH_TYPE", "S")
                setSharedData(oForm, "IDH_GROUP", "")
            Else
                If sCardCode.Length > 0 OrElse _
                    sCardName.Length > 0 OrElse _
                    Config.INSTANCE.getParameterAsBool(msOrderType & "BPFLT", True) = False Then
                    setSharedData(oForm, "IDH_TYPE", "")
                    setSharedData(oForm, "IDH_GROUP", "")
                Else
                    setSharedData(oForm, "IDH_TYPE", "C")
                    setSharedData(oForm, "IDH_GROUP", Config.Parameter("BGCProdu"))
                End If
            End If

            setSharedData(oForm, "IDH_BPCOD", sCardCode)
            setSharedData(oForm, "IDH_NAME", sCardName)

            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            '## Start 18-06-2016
            'test.StartTime = Now
            '## Start 16-01-2014
            'goParent.doOpenModalForm("IDHCSRCH", oForm)
            setSharedData(oForm, "CALLEDITEM", sItemID)
            If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
                goParent.doOpenModalForm("IDHCSRCH", oForm) 'Actual BP Search Form 
            Else
                If (getSharedData(oForm, "IDH_BPSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_BPSRCHOPEND").ToString.Trim = "") Then
                    goParent.doOpenModalForm("IDHNCSRCH", oForm) 'NEW BP Search Form 
                End If
            End If
            '## End 16-01-2014
        End Sub

        Protected Overrides Sub doSetChoosenProducer(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoBPRules As Boolean = True, Optional ByVal bDoLinkCustomerAndProducer As Boolean = False)
            'Override bDoLinkCustomerAndProducer with the value from the Config file
            Dim sForecast As String = getFormDFValue(oForm, "U_ForeCS")
            Dim sCustomer As String = getFormDFValue(oForm, IDH_JOBENTR._CardCd)
            If Not sForecast Is Nothing AndAlso sForecast.Length > 0 AndAlso sCustomer.Length > 0 Then
                MyBase.doSetChoosenProducer(oForm, False, False)
            Else
                MyBase.doSetChoosenProducer(oForm, bDoBPRules, Config.INSTANCE.doGetDoBuyingAndTrade())
            End If
            oForm.Items.Item("IDH_LASTM").Visible = True
            oForm.Items.Item("IDH_WABT").Visible = True
            oForm.Items.Item("IDH_OSM").Visible = True
            'oForm.Items.Item("IDH_ONSITE").Visible = True
            oForm.Items.Item("IDH_ONSITE").Visible = False
            Dim sProducer As String = getFormDFValue(oForm, IDH_JOBENTR._PCardCd)
            FillCombos.TransactionCodeCombo(FillCombos.getCombo(oForm, "uBC_TRNCd"), sCustomer, sProducer, "WO")
        End Sub

        Protected Overrides Function doSetChoosenCustomerFromShared(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoBPRules As Boolean = True, Optional ByVal bDoLinkCustomerAndProducer As Boolean = False) As Boolean
            Dim sForecast As String = getFormDFValue(oForm, "U_ForeCS")
            Dim sProducer As String = getFormDFValue(oForm, IDH_JOBENTR._PCardCd)
            If Not sForecast Is Nothing AndAlso sForecast.Length > 0 AndAlso sProducer.Length > 0 Then
                MyBase.doSetChoosenCustomerFromShared(oForm, False, False)
                oForm.Items.Item("IDH_LASTM").Visible = True
                oForm.Items.Item("IDH_WABT").Visible = True
                oForm.Items.Item("IDH_OSM").Visible = True
                'oForm.Items.Item("IDH_ONSITE").Visible = True
                oForm.Items.Item("IDH_ONSITE").Visible = False

                Dim sCustomer As String = getFormDFValue(oForm, IDH_JOBENTR._CardCd)
                FillCombos.TransactionCodeCombo(FillCombos.getCombo(oForm, "uBC_TRNCd"), sCustomer, sProducer, "WO")
                Return True
            Else
                'Override bDoLinkCustomerAndProducer with the value from the Config file
                bDoLinkCustomerAndProducer = Config.INSTANCE.doGetDoBuyingAndTrade()
                If MyBase.doSetChoosenCustomerFromShared(oForm, bDoBPRules, bDoLinkCustomerAndProducer) = True Then
                    oForm.Items.Item("IDH_LASTM").Visible = True
                    oForm.Items.Item("IDH_WABT").Visible = True
                    oForm.Items.Item("IDH_OSM").Visible = True
                    'oForm.Items.Item("IDH_ONSITE").Visible = True
                    oForm.Items.Item("IDH_ONSITE").Visible = False

                    If bDoBPRules Then
                        Dim sWasteCarrierCode As String = getFormDFValue(oForm, IDH_JOBENTR._CCardCd)
                        If sWasteCarrierCode Is Nothing OrElse sWasteCarrierCode.Length = 0 Then
                            Dim sCustomerCode As String = getFormDFValue(oForm, "U_CardCd")
                            Dim sCarrierOption As String = Config.Parameter("MDDEWC") ' (BLANK, NONE, LBPCUST)
                            If sCarrierOption = "LBPCUST" Then
                                Dim oLnkBP As LinkedBP = New LinkedBP()
                                If oLnkBP.doGetLinkedBP(sCustomerCode) Then
                                    setFormDFValue(oForm, IDH_JOBENTR._CCardCd, oLnkBP.LinkedBPCardCode)
                                    setFormDFValue(oForm, IDH_JOBENTR._CCardNM, oLnkBP.LinkedBPName)
                                End If
                            End If
                        End If
                    End If
                    doFillContracts(oForm)

                    Dim sCustomer As String = getFormDFValue(oForm, IDH_JOBENTR._CardCd)
                    FillCombos.TransactionCodeCombo(FillCombos.getCombo(oForm, "uBC_TRNCd"), sCustomer, sProducer, "WO")

                    Return True
                Else
                    oForm.Items.Item("IDH_LASTM").Visible = False
                    oForm.Items.Item("IDH_WABT").Visible = False
                    oForm.Items.Item("IDH_OSM").Visible = False
                    oForm.Items.Item("IDH_ONSITE").Visible = False
                    doFillContracts(oForm)

                    Dim sCustomer As String = getFormDFValue(oForm, IDH_JOBENTR._CardCd)
                    FillCombos.TransactionCodeCombo(FillCombos.getCombo(oForm, "uBC_TRNCd"), sCustomer, sProducer, "WO")

                    Return False
                End If
            End If
        End Function

        Protected Overrides Sub doChooseSite(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, sItemID As String)
            Dim sCardCode As String = getFormDFValue(oForm, "U_SCardCd", True)
            Dim sCardName As String = getFormDFValue(oForm, "U_SCardNM", True)
            Dim sZipCode As String = getFormDFValue(oForm, IDH_JOBENTR._ZpCd, True)
            Dim sWasteCode As String = ""

            If sCardCode.Length > 0 OrElse _
                sCardName.Length > 0 OrElse _
                Config.INSTANCE.getParameterAsBool(msOrderType & "BPFLT", True) = False Then
                setSharedData(oForm, "IDH_TYPE", "")
                setSharedData(oForm, "IDH_GROUP", "")
            Else
                setSharedData(oForm, "IDH_TYPE", "S")
                setSharedData(oForm, "IDH_GROUP", Config.Parameter("BGSDispo"))
            End If

            setSharedData(oForm, "IDH_BPCOD", sCardCode)
            setSharedData(oForm, "IDH_NAME", sCardName)

            setSharedData(oForm, "TRG", "ST")
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If

            If Config.INSTANCE.getParameterAsBool("WODSPSP", False) Then
                'setSharedData(oForm, "IDH_ZIPCOD", sZipCode)
                setSharedData(oForm, "IDH_CUSTZP", sZipCode)
                setSharedData(oForm, "IDH_WSTCD", sWasteCode)
                setSharedData(oForm, "DOSIP", "TRUE")
                '    goParent.doOpenModalForm("IDHDSRCH", oForm)
            Else
                setSharedData(oForm, "DOSIP", "FALSE")
                '    goParent.doOpenModalForm("IDHCSRCH", oForm)
            End If
            setSharedData(oForm, "CALLEDITEM", sItemID)

            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub

        Protected Overrides Sub doSetChoosenSite(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoBPRules As Boolean = True)
            MyBase.doSetChoosenSite(oForm, bDoBPRules)

            If getSharedData(oForm, "DOSIP") = "TRUE" Then
                setFormDFValue(oForm, IDH_JOBENTR._LckPrc, "Y")
                setWFValue(oForm, "DOSIP", "Y")
                setWFValue(oForm, "WASTCD", getSharedData(oForm, "WASTCD"))
                setWFValue(oForm, "UOM", getSharedData(oForm, "UOM"))
                setWFValue(oForm, "PUOM", getSharedData(oForm, "PUOM"))
                setWFValue(oForm, "TIPCST", 0)
                setWFValue(oForm, "PRODCST", 0)
                setWFValue(oForm, "HAULAGECST", getSharedData(oForm, "HAULAGECST"))
                setWFValue(oForm, "DISTANCE", getSharedData(oForm, "DISTANCE"))
                setWFValue(oForm, "TIPCHRG", 0)
                setWFValue(oForm, "HAULAGECHRG", getSharedData(oForm, "HAULAGECHRG"))
                setWFValue(oForm, "PRCLINK", getSharedData(oForm, "LINK"))

                Dim sContainer As String = getSharedData(oForm, "CONTAINER")
                setWFValue(oForm, "CONTAINER", sContainer)

                'If sContainer IsNot Nothing AndAlso sContainer.Length > 0 Then
                '    Dim sSwitchExpectedWeight As String = Config.Parameter("SWTCEXW")
                '    If sSwitchExpectedWeight IsNot Nothing AndAlso sSwitchExpectedWeight = "DW" Then
                '        Dim dDefWeight As Double = Config.INSTANCE.getItemDefaultWeight(sContainer)
                '        setWFValue(oForm, "FCEXPWGT", dDefWeight)
                '    End If
                'End If

                Dim oWO As IDH_JOBENTR = thisWO(oForm)
                Dim oAddress As BPAddress = New BPAddress()
                Dim sAddress As String = getSharedData(oForm, "SUPADDR")
                If oAddress.doGetAddress(oWO.U_SCardCd, BPAddress.ADDRTYPE.SHIPTO, sAddress, True) > 0 Then
                    oWO.doSetDisposalSiteAddress(oAddress)
                End If
                setEnableItem(oForm, True, "IDH_SITADD")
                setEnableItem(oForm, False, "IDH_SITAL")
            Else
                setEnableItem(oForm, True, "IDH_SITAL")
            End If
        End Sub

        Protected Overrides Sub doCheckProducerAddress(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bFromSearch As Boolean = False)
            Dim sAddress As String
            Dim sCardCode As String

            sAddress = getFormDFValue(oForm, "U_PAddress")
            sCardCode = getFormDFValue(oForm, "U_PCardCd")

            If sAddress.Length() > 0 Then
                Dim oData1 As ArrayList
                oData1 = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sCardCode, "S", sAddress)
                If oData1 Is Nothing Then
                    If Config.INSTANCE.doGetDoBuyingAndTrade() Then
                        Dim sCustCd As String = getFormDFValue(oForm, "U_CardCd")
                        Dim sProdCd As String = getFormDFValue(oForm, "U_PCardCd")
                        If sProdCd.Length > 0 AndAlso sCustCd.Equals(sProdCd) = False Then
                            setEnableItem(oForm, True, "IDH_PNA")
                        End If

                        '                    setEnableItem(oForm, True, "IDH_CUSRF")
                    End If
                Else
                    setEnableItem(oForm, False, "IDH_PNA")
                    doSetProducerAddress(oForm, oData1, False)
                End If
            ElseIf bFromSearch = False Then
                setSharedData(oForm, "TRG", "IDH_PROAL")
                setSharedData(oForm, "IDH_CUSCOD", sCardCode)
                setSharedData(oForm, "IDH_ADRTYP", "S")
                '## Start 16-01-2014
                '   goParent.doOpenModalForm("IDHASRCH", oForm)
                If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
                    goParent.doOpenModalForm("IDHASRCH", oForm)
                Else
                    goParent.doOpenModalForm("IDHASRCH3", oForm)
                    'goParent.doOpenModalForm("IDHASRCH", oForm)
                End If
                '##End 16-01-2014
            Else
                setEnableItem(oForm, False, "IDH_PNA")
                '                setEnableItem(oForm, False, "IDH_CUSRF")
            End If
        End Sub

        Public Overrides Function doAddCustomerShipToAddress(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim oWO As IDH_JOBENTR = thisWO(oForm)
            If BPAddress.doAddCustomerShipToAddress(goParent.goDICompany, oWO) Then
                'If MyBase.doAddCustomerAddress(oForm) Then
                If Config.INSTANCE.doGetDoBuyingAndTrade() Then
                    Dim sCustCd As String = getFormDFValue(oForm, "U_CardCd")
                    Dim sProdCd As String = getFormDFValue(oForm, "U_PCardCd")

                    Dim oLinkedBP As LinkedBP = Config.INSTANCE.doGetLinkedBP(sCustCd)
                    'If Not oLinkedBP Is Nothing AndAlso sProdCd.Length > 0 AndAlso oLinkedBP.CardCode.Equals( sProdCd ) = True Then
                    If Not oLinkedBP Is Nothing AndAlso oLinkedBP.LinkedBPCardCode.Length > 0 Then
                        If doAddProducerShipToAddressFromCustomer(oForm, oLinkedBP.LinkedBPCardCode) Then
                            If sProdCd.Equals(oLinkedBP.LinkedBPCardCode) Then
                                doSetProducerAddressFromCustomer(oForm)
                            End If
                        End If
                    End If
                End If
                Return True
            Else
                Return False
            End If
        End Function

        Public Overrides Function doAddProducerShipToAddress(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim oWO As IDH_JOBENTR = thisWO(oForm)
            If BPAddress.doAddProducerShipToAddress(goParent.goDICompany, oWO) Then
                'If MyBase.doAddProducerAddress(oForm) Then
                If Config.INSTANCE.doGetDoBuyingAndTrade() Then
                    Dim sProdCd As String = getFormDFValue(oForm, "U_PCardCd")
                    Dim sCustCd As String = getFormDFValue(oForm, "U_CardCd")

                    '					If sCustCd.Length > 0 AndAlso sProdCd.Length > 0 AndAlso sCustCd.Equals( sProdCd ) = True Then
                    '						doAddCustomerAddressFromProducer( oForm )
                    '					End If
                    Dim oLinkedBP As LinkedBP = Config.INSTANCE.doGetLinkedBP(sProdCd)
                    If Not oLinkedBP Is Nothing AndAlso oLinkedBP.LinkedBPCardCode.Length > 0 Then
                        Dim oAddress As BPAddress = New BPAddress()
                        oAddress.doGetAddress(oLinkedBP.LinkedBPCardCode, "S", oWO.U_PAddress, True)
                        If oAddress.Address = "" Then
                            If doAddCustomerShipToAddressFromProducer(oForm, oLinkedBP.LinkedBPCardCode) Then
                                If sCustCd.Equals(oLinkedBP.LinkedBPCardCode) Then
                                    doSetCustomerAddressFromProducer(oForm)
                                End If
                            End If
                        End If

                    End If
                End If
                Return True
            Else
                Return False
            End If
        End Function

        Protected Overrides Function doSetCustomerAddress(ByVal oForm As SAPbouiCOM.Form, ByRef oData As System.Collections.ArrayList, ByVal sCardCode As String, _
                                                          ByVal sPhone1 As String, Optional ByVal bUpdateProdAddress As Boolean = True, Optional ByVal bDisableProducerNewAddressButton As Boolean = True) As Boolean
            Dim bResult As Boolean
            If Not (oData Is Nothing) AndAlso oData.Count > 0 Then
                Dim sAddress As String = oData.Item(0)
                Dim sCustRef As String = oData.Item(12)

                Dim dTotal As Double = getUnCommittedRowTotals(oForm)
                If Config.INSTANCE.doGetPOLimitsDOWO("WO", sCardCode, sAddress, sCustRef, dTotal) = False Then
                    Return False
                End If

                bResult = MyBase.doSetCustomerAddress(oForm, oData, sCardCode, sPhone1, bUpdateProdAddress)
                If bDisableProducerNewAddressButton Then setEnableItem(oForm, False, "IDH_PNA")

                'This will do the additional synch on the addresses
                If bUpdateProdAddress AndAlso Config.INSTANCE.doGetDoBuyingAndTrade() Then
                    Dim sProdCd As String = getFormDFValue(oForm, "U_PCardCd")
                    Dim oLinkedBP As LinkedBP = Config.INSTANCE.doGetLinkedBP(sCardCode)

                    If Not oLinkedBP Is Nothing AndAlso sProdCd.Length > 0 AndAlso oLinkedBP.LinkedBPCardCode.Equals(sProdCd) = True Then
                        'First try to find the Address on the Producer before creating a new one
                        Dim oData1 As ArrayList = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sProdCd, "S", sAddress)

                        If oData1 Is Nothing OrElse oData1.Count = 0 Then
                            'Add the Address if it was not found on the Producer

                            If doAddProducerShipToAddressFromCustomer(oForm, oLinkedBP.LinkedBPCardCode) Then
                                If wasSetWithCode(oForm, "IDH_PRDADD") Then
                                    doSetProducerAddressFromCustomer(oForm)
                                End If
                            End If
                        Else
                            If wasSetWithCode(oForm, "IDH_PRDADD") Then
                                doSetProducerAddressFromCustomer(oForm)
                            End If
                        End If
                    End If
                End If
            End If

            Return bResult
        End Function

        Protected Overrides Function doSetProducerAddress(ByVal oForm As SAPbouiCOM.Form, ByRef oData As ArrayList, ByVal bFromUser As Boolean, _
                                                          Optional ByVal bUpdateCustomer As Boolean = True, Optional ByVal bDisableCustomerNewAddressButton As Boolean = True) As Boolean
            Dim bResult As Boolean
            If Not (oData Is Nothing) AndAlso oData.Count > 0 Then
                Dim sAddress As String = oData.Item(0)

                bResult = MyBase.doSetProducerAddress(oForm, oData, bFromUser, bUpdateCustomer)
                If bDisableCustomerNewAddressButton Then setEnableItem(oForm, False, "IDH_CNA")
                '## Start 12-06-2013
                setFormDFValue(oForm, "U_PContact", oData(16))
                If oData IsNot Nothing AndAlso oData.Count >= 27 Then 'This will only execte if we have both General Phone and site phone of BP
                    Dim PhoneOption As String = Config.Parameter("WOPHSRC")
                    If PhoneOption.Length = 4 Then 'Invalid Value for switch
                        If PhoneOption.Substring(1, 1) = "1" Then 'Pick Phone for General;Producer
                            setFormDFValue(oForm, "U_PPhone1", oData(26))
                            setFormDFValue(oForm, "U_PContact", oData(27))
                        Else
                            setFormDFValue(oForm, "U_PPhone1", oData(13))
                            setFormDFValue(oForm, "U_PContact", oData(16))
                        End If
                    End If
                End If
                '## End
                'This will do the additional synch on the addresses
                If bUpdateCustomer AndAlso Config.INSTANCE.doGetDoBuyingAndTrade() Then
                    Dim sProdCd As String = getFormDFValue(oForm, "U_PCardCd")
                    Dim sCustomer As String = getFormDFValue(oForm, "U_CardCd")
                    If sCustomer.Length > 0 Then
                        Dim oLinkedBP As LinkedBP = Config.INSTANCE.doGetLinkedBP(sProdCd)

                        If Not oLinkedBP Is Nothing AndAlso sProdCd.Length > 0 AndAlso oLinkedBP.LinkedBPCardCode.Equals(sCustomer) = True Then
                            'First try to find the Address on the Customer before creating a new one
                            Dim oData1 As ArrayList = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sCustomer, "S", sAddress)

                            If oData1 Is Nothing OrElse oData1.Count = 0 Then
                                'Add the Address if it was not found on the Producer

                                If doAddCustomerShipToAddressFromProducer(oForm, oLinkedBP.LinkedBPCardCode) Then
                                    If wasSetWithCode(oForm, "IDH_CUSADD") Then
                                        doSetCustomerAddressFromProducer(oForm)
                                    End If
                                End If
                            Else
                                '##Start 05-09-2013
                                'It was placing producer address to customer(only on WOH form);and in result customer address in DB can be different.
                                'So commented it. Discussed with Haydan and KA 14:05 Hrs
                                If wasSetWithCode(oForm, "IDH_CUSADD") Then
                                    doSetCustomerAddressFromProducer(oForm)

                                End If
                                '## End
                            End If
                        End If
                    End If
                End If

            End If
            Return bResult
        End Function

        Public Shared Function doUpdateSkipLicExpiry(ByVal sCode As String, ByVal dExpiryDate As DateTime, ByVal dCost As Double) As Boolean
            Dim iwResult As Integer = 0
            Dim swResult As String = Nothing
            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(IDHAddOns.idh.addon.Base.PARENT, "@IDH_JOBENTR")
            Try
                If oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) = True Then
                    oAuditObj.setFieldValue("U_SLicExp", dExpiryDate)
                    oAuditObj.setFieldValue("U_SLicCh", dCost)
                End If
                iwResult = oAuditObj.Commit()

                If iwResult <> 0 Then
                    IDHAddOns.idh.addon.Base.PARENT.goDICompany.GetLastError(iwResult, swResult)
                    'com.idh.bridge.DataHandler.INSTANCE.doError("System: Error Updating the Skip Licence Expiry - " + swResult, "Error Updating the Skip Licence Expiry.")
                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Updating the Skip Licence Expiry - " + swResult, "ERSYSLEX", {swResult})
                    Return False
                End If
                Return True
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Updating the Skip Licence Expiry.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSLEX", {Nothing})
                Return False
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj)
            End Try
            Return False
        End Function

        '**
        ' This will be called from the UpdateHeader Function enabling the inherited classes to add or change values before it gets processed
        '**
        Protected Overrides Sub doUpdateHeaderMore(ByVal oForm As SAPbouiCOM.Form, ByVal oAuditObj As IDHAddOns.idh.data.AuditObject, ByVal bIsAdd As Boolean)
            oAuditObj.setFieldValue(IDH_JOBENTR._ForeCS, getFormDFValue(oForm, IDH_JOBENTR._ForeCS))

            'OnTime [#Ico000????] USA
            'START

            '** MAILING ADDRESS
            oAuditObj.setFieldValue(IDH_JOBENTR._MContact, getFormDFValue(oForm, IDH_JOBENTR._MContact))
            oAuditObj.setFieldValue(IDH_JOBENTR._MAddress, getFormDFValue(oForm, IDH_JOBENTR._MAddress))
            oAuditObj.setFieldValue(IDH_JOBENTR._MAddrsLN, getFormDFValue(oForm, IDH_JOBENTR._MAddrsLN))
            oAuditObj.setFieldValue(IDH_JOBENTR._MStreet, getFormDFValue(oForm, IDH_JOBENTR._MStreet))
            oAuditObj.setFieldValue(IDH_JOBENTR._MBlock, getFormDFValue(oForm, IDH_JOBENTR._MBlock))
            oAuditObj.setFieldValue(IDH_JOBENTR._MCity, getFormDFValue(oForm, IDH_JOBENTR._MCity))
            oAuditObj.setFieldValue(IDH_JOBENTR._MZpCd, getFormDFValue(oForm, IDH_JOBENTR._MZpCd))
            oAuditObj.setFieldValue(IDH_JOBENTR._MPhone1, getFormDFValue(oForm, IDH_JOBENTR._MPhone1))

            'END
            'OnTime [#Ico000????] USA
            'oAuditObj.setFieldValue(IDH_JOBENTR._TFSReq, getFormDFValue(oForm, IDH_JOBENTR._TFSReq))
            'oAuditObj.setFieldValue(IDH_JOBENTR._TFSNum, getFormDFValue(oForm, IDH_JOBENTR._TFSNum))
            ''##MA Start 12-09-2014 Issue#413
            oAuditObj.setFieldValue(IDH_JOBENTR._MinJob, getFormDFValue(oForm, IDH_JOBENTR._MinJob))
            ''##MA Start 12-09-2014 Issue#413
            If Not String.IsNullOrEmpty(getFormDFValue(oForm, IDH_JOBENTR._ForeCS)) Then
                Dim oForecast As User.IDH_BPFORECST = getWFValue(oForm, "FCDBObject")
                If oForecast IsNot Nothing AndAlso getFormDFValue(oForm, IDH_JOBENTR._Status).ToString.Trim <> oForecast.U_DocType Then
                    oForecast.U_DocType = getFormDFValue(oForm, IDH_JOBENTR._Status)
                    oForecast.doProcessData()
                    oForecast.doRefreshBPForeCast()
                    'User.IDH_BPFORECST.doUpdateForecastStatus(Code, U_Status)
                Else
                    User.IDH_BPFORECST.doUpdateForecastStatus(getFormDFValue(oForm, IDH_JOBENTR._Code), getFormDFValue(oForm, IDH_JOBENTR._Status))
                End If

            End If
        End Sub
        Protected Function doValidateContainer(sContainer As String) As Boolean
            If sContainer = "" Then
                Return False
            End If
            Dim sLinkedItemCode As String = Config.INSTANCE.getValueFromOITM(sContainer, OITM._WTITMLN).ToString()
            If (String.IsNullOrEmpty(sLinkedItemCode)) Then
                com.idh.bridge.DataHandler.INSTANCE.doError("Container does not have a linked stock item required for Transaction code Goods Receipt.")
                'return "Container does not have a linked stock item required for Transaction code Goods Receipt.";
                Return False
            End If
            If (Not Config.INSTANCE.isStockItem(sLinkedItemCode)) Then
                com.idh.bridge.DataHandler.INSTANCE.doError("Container does not have a linked stock item required for Transaction code Goods Receipt.")
                'return "Container does not have a linked stock item required for Transaction code Goods Receipt.";
                Return False
            End If
            Return True
        End Function
        Protected Function doValidateRows(ByVal oForm As SAPbouiCOM.Form) As Boolean

            Dim oUpdateGrid As UpdateGrid
            oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If (Config.INSTANCE.getParameterAsBool("VLGRTRCD", True) AndAlso getFormDFValue(oForm, IDH_JOBENTR._TRNCd) IsNot Nothing AndAlso getFormDFValue(oForm, IDH_JOBENTR._TRNCd) = "29") Then
                Dim oAddedRows As ArrayList
                If oUpdateGrid.hasAddedRows Then
                    oAddedRows = oUpdateGrid.doGetAddedRows().Clone()
                End If
                Dim oModifiedRows As ArrayList
                If oUpdateGrid.hasChangedRows Then
                    oModifiedRows = oUpdateGrid.doGetChangedRows().Clone()
                End If
                If Not oAddedRows Is Nothing AndAlso oAddedRows.Count > 0 Then
                    Dim iRow As Integer
                    For iIndex As Integer = 0 To oAddedRows.Count - 1
                        iRow = oAddedRows.Item(iIndex)
                        oUpdateGrid.setCurrentDataRowIndex(iRow)
                        Dim sContCd As String = oUpdateGrid.doGetFieldValue(IDH_JOBSHD._ItemCd).ToString()
                        If sContCd = String.Empty OrElse Me.doValidateContainer(sContCd) = False Then
                            com.idh.bridge.DataHandler.INSTANCE.doError("Container code is missing for Transaction code Goods Receipt.")
                            Return False
                        End If
                    Next
                End If
                If Not oModifiedRows Is Nothing AndAlso oModifiedRows.Count > 0 Then
                    Dim iRow As Integer
                    For iIndex As Integer = 0 To oModifiedRows.Count - 1
                        iRow = oModifiedRows.Item(iIndex)
                        oUpdateGrid.setCurrentDataRowIndex(iRow)
                        Dim sContCd As String = oUpdateGrid.doGetFieldValue(IDH_JOBSHD._ItemCd).ToString()
                        If sContCd = String.Empty OrElse Me.doValidateContainer(sContCd) = False Then
                            com.idh.bridge.DataHandler.INSTANCE.doError("Container code is missing for Transaction code Goods Receipt.")
                            Return False
                        End If
                    Next
                End If
            End If
            Return True
        End Function

        Protected Overrides Function doUpdateGridRows(ByVal oForm As SAPbouiCOM.Form) As Boolean
            If doValidateRows(oForm) AndAlso MyBase.doUpdateGridRows(oForm) Then
                Dim oData As WR1_Data.idh.data.AdditionalExpenses = Nothing
                oData = getWFValue(oForm, "JobAdditionalHandler")
                If Not oData Is Nothing Then
                    oData.doProcessData()
                End If
                Return True
            Else
                Return False
            End If
        End Function

        Public Overrides Sub doCreateFindEntry(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doCreateFindEntry(oForm)
            setEnableItem(oForm, False, "IDH_PNA")

            '** MAILING ADDRESS
            setFormDFValue(oForm, "U_MContact", "")
            setFormDFValue(oForm, "U_MAddress", "")
            setFormDFValue(oForm, "U_MAddrsLN", "")
            setFormDFValue(oForm, "U_MStreet", "")
            setFormDFValue(oForm, "U_MBlock", "")
            setFormDFValue(oForm, "U_MCity", "")
            setFormDFValue(oForm, "U_MZpCd", "")
            setFormDFValue(oForm, "U_MPhone1", "")

            'setFormDFValue(oForm, "U_TFSReq", "")
            'setFormDFValue(oForm, "U_TFSNum", "")

            'State Fields
            setFormDFValue(oForm, "U_MState", "")
            ''##MA Start 15-09-2014 Issue#413
            setFormDFValue(oForm, IDH_JOBENTR._MinJob, "")
            ''##MA End 15-09-2014 Issue#413
        End Sub

        '** Create a new Entry
        Public Overrides Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form) ', Optional ByVal bIsTemp As Boolean = False)
            MyBase.doCreateNewEntry(oForm)
            'MyBase.doCreateNewEntry(oForm, bIsTemp)
            setEnableItem(oForm, False, "IDH_PNA")

            Dim sDefaultStatus As String = Config.INSTANCE.getDefaultStatus()
            setFormDFValue(oForm, "U_Status", sDefaultStatus)

            setFormDFValue(oForm, "U_ForeCS", "")
            setUFValue(oForm, "U_FCWC", "")
            setUFValue(oForm, "U_FCREM", 0)

            Dim sContainerGroup As String = Config.INSTANCE.getParameterWithDefault("WODCOG", "")
            If sContainerGroup.Length > 0 Then
                setFormDFValue(oForm, "U_ItemGrp", sContainerGroup)
            Else
                setFormDFValue(oForm, "U_ItemGrp", "")
            End If

            '** MAILING ADDRESS
            setFormDFValue(oForm, "U_MContact", "")
            setFormDFValue(oForm, "U_MAddress", "")
            setFormDFValue(oForm, "U_MAddrsLN", "")
            setFormDFValue(oForm, "U_MStreet", "")
            setFormDFValue(oForm, "U_MBlock", "")
            setFormDFValue(oForm, "U_MCity", "")
            setFormDFValue(oForm, "U_MZpCd", "")
            setFormDFValue(oForm, "U_MPhone1", "")

            'setFormDFValue(oForm, "U_TFSReq", "")
            'setFormDFValue(oForm, "U_TFSNum", "")

            'State Fields
            setFormDFValue(oForm, "U_MState", "")
            ''##MA Start 15-09-2014 Issue#413
            setFormDFValue(oForm, "U_MinJob", "")
            ''##MA Start 15-09-2014 Issue#413
        End Sub

        Public Shared Function doDeleteOrder(ByVal sWOCode As String, ByVal sComment As String) As Boolean
            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(IDHAddOns.idh.addon.Base.PARENT, msMainTable)
            Dim iwResult As Integer
            Dim swResult As String = Nothing
            If oAuditObj.setKeyPair(sWOCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_REMOVE) Then
                oAuditObj.setComment(sComment)
                iwResult = oAuditObj.Commit()

                If iwResult <> 0 Then
                    IDHAddOns.idh.addon.Base.PARENT.goDICompany.GetLastError(iwResult, swResult)
                    'com.idh.bridge.DataHandler.INSTANCE.doError("System: Error Removing - " + swResult, "Error Removing: " + swResult)
                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Removing - " + swResult, "ERSYDBRR", {swResult, swResult})
                    Return False
                End If
            End If
            Return True
        End Function

        '** set the PreSelect Data if it was set
        Protected Sub doSetPreselectData(ByVal oForm As SAPbouiCOM.Form)
            Dim oPreData As PreSelect = getWFValue(oForm, "PRESELECT")
            If oPreData IsNot Nothing Then

                'setFormDFValue(oForm, IDH_JOBENTR._TRNCd, oPreData.TransactionCode)
                Dim oWO As IDH_JOBENTR = thisWO(oForm)
                oWO.U_TRNCd = oPreData.TransactionCode

                Dim sBPCode As String = oPreData.BPCode
                If sBPCode IsNot Nothing AndAlso sBPCode.Length > 0 Then
                    If Config.INSTANCE.doCheckIsVendor(sBPCode) Then
                        setFormDFValue(oForm, IDH_JOBENTR._PCardCd, oPreData.BPCode)
                        setFormDFValue(oForm, IDH_JOBENTR._PCardNM, oPreData.BPName)

                        doChooseProducer(oForm, True, "")
                    Else
                        setFormDFValue(oForm, IDH_JOBENTR._CardCd, oPreData.BPCode)
                        setFormDFValue(oForm, IDH_JOBENTR._CardNM, oPreData.BPName)

                        doChooseCustomer(oForm, True, "")
                    End If
                End If
            End If
        End Sub



    End Class
End Namespace
