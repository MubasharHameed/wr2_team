Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System
Imports com.idh.utils.Conversions
Imports WR1_Grids.idh.controls.grid
Imports com.idh.bridge.data
Imports com.idh.dbObjects.User
Imports com.idh.utils
Imports com.isb.bridge.lookups
Imports com.idh.bridge.action
Imports com.idh.bridge
Imports com.uBC.data
Imports com.idh.dbObjects.numbers
Imports com.idh.bridge.resources

Namespace idh.forms.orders
    Public MustInherit Class Tmpl
        Inherits IDHAddOns.idh.forms.Base

        Private ghLastRow As New Hashtable
        Private ghStatusChanged As New ArrayList

        Protected msRowTable As String = "@IDH_RR"
        Protected msOrdCat As String = com.idh.bridge.lookups.FixedValues.getStatusOrder()
        Protected msNextNum As String = "DN"
        'Protected msNextNumPrefix As String = Nothing

        Protected msNextRowNum As String = "DN"
        'Protected msNextRowNumPrefix As String = Nothing

        Protected msOrderType As String = "DN"
        Protected mbFolderFollowDB As Boolean = True
        ''Protected msWeightBridgeUOM As String = "kg"

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sType As String, ByVal sSRF As String, ByVal sOrderType As String, ByVal sHeadTable As String, ByVal sRowTable As String, ByVal sOCat As String, ByVal iMenuPosition As Integer, ByVal sTitle As String)
            MyBase.New(oParent, sType, "IDHORD", iMenuPosition, sSRF, True, True, False, sTitle, IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL)
            msOrderType = sOrderType
            msRowTable = sRowTable
            msOrdCat = sOCat

            Dim sSBOVersion As String = Conversions.ToString(oParent.goDICompany.Version)
            Dim dVersion As Long
            Try
                dVersion = Conversions.ToLong(sSBOVersion)
            Catch ex As Exception
                dVersion = 680124
            End Try

            If dVersion > 680124 Then
                MarketingDocs.gbUseUnitPrice = True
            End If

            doEnableHistory(sHeadTable)

            'doAddImagesToFix("IDH_SLSL")
            'doAddImagesToFix("IDH_CUSL")
            'doAddImagesToFix("IDH_PRDL")
            'doAddImagesToFix("IDH_SITL")
            'doAddImagesToFix("IDH_CARL")
            'doAddImagesToFix("IDH_CUSAL")
            'doAddImagesToFix("IDH_PRDAL")
            'doAddImagesToFix("IDH_SITAL")
            'doAddImagesToFix("IDH_CARAL")
            'doAddImagesToFix("IDH_WASAL")
            'doAddImagesToFix("IDH_PROAL")
            'doAddImagesToFix("IDH_ROUTL")
            'doAddImagesToFix("IDH_ORIGLU")
            'doAddImagesToFix("IDH_MLAAL")
        End Sub

        Protected Overridable Function getNextNumPrefix() As String
            Return Nothing
        End Function

        Protected Overridable Function getNextRowNumPrefix() As String
            Return Nothing
        End Function

        '        Protected MustOverride Function getTitle() As String
        '        Protected Overrides Sub doCreateSubMenu()
        '            doCreateFormMenu("IDHORD", getTitle())
        '        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_GOT_FOCUS)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_MENU_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        Protected Overridable Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            'Dim oItem As SAPbouiCOM.Item

            Try
                ''oForm.DataSources.DBDataSources.Add(msMDHead)
                'oItem = oForm.Items.Add("SKPCheck", SAPbouiCOM.BoFormItemTypes.it_EDIT)
                'oItem.Left = 0
                'oItem.Top = 0
                'oItem.Width = 0
                'oItem.Height = 0
                'oForm.DataSources.UserDataSources.Add("SKPCheck", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                'doSetEnabled(oItem, False)
                'oItem.Specific.DataBind.SetBound(True, "", "SKPCheck")
                'oForm.DataSources.UserDataSources.Item("SKPCheck").ValueEx = "N"

                'doAddUF(oForm, "IDH_LICREG", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30)

                'doAddFormDF(oForm, "IDH_BOOREF", "Code")
                'doAddFormDF(oForm, "IDH_STATUS", "U_Status")
                'doAddFormDF(oForm, "IDH_BOOKDT", "U_BDate")
                'doAddFormDF(oForm, "IDH_BOOKTM", "U_BTime")
                'doAddFormDF(oForm, "IDH_IGRP", "U_ItemGrp")
                'doAddFormDF(oForm, "IDH_CHECK", "U_ORoad")
                'doAddFormDF(oForm, "IDH_SKPLIC", "U_SLicNr")
                'doAddFormDF(oForm, "IDH_LICEXP", "U_SLicExp")
                'doAddFormDF(oForm, "IDH_LICCHR", "U_SLicCh")
                'doAddFormDF(oForm, "IDH_SPECIN", "U_SpInst")
                'doAddFormDF(oForm, "IDH_LICSUP", "U_SLicSp")
                'doAddFormDF(oForm, "IDH_SLICNM", "U_SLicNm")
                'doAddFormDF(oForm, "IDH_CONTRN", "U_CntrNo")

                ''doAddFormDF(oForm, "IDH_FIRSTBP", "U_FirstBP")

                ''**CUSTOMER
                'doAddFormDF(oForm, "IDH_CUST", "U_CardCd")
                'doAddFormDF(oForm, "IDH_CUSTNM", "U_CardNM")
                'doAddFormDF(oForm, "IDH_CUSCON", "U_Contact")
                'doAddFormDF(oForm, "IDH_CUSADD", "U_Address")
                'doAddFormDF(oForm, "IDH_CUSSTR", "U_Street")
                'doAddFormDF(oForm, "IDH_CUSBLO", "U_Block")
                'doAddFormDF(oForm, "IDH_CUSCIT", "U_City")
                'doAddFormDF(oForm, "IDH_CUSSTA", "U_State")
                'doAddFormDF(oForm, "IDH_COUNTY", "U_County")
                'doAddFormDF(oForm, "IDH_CUSPOS", "U_ZpCd")
                'doAddFormDF(oForm, "IDH_CUSPHO", "U_Phone1")
                'doAddFormDF(oForm, "IDH_SITETL", "U_SiteTl")
                'doAddFormDF(oForm, "IDH_SteId", "U_SteId")
                'doAddFormDF(oForm, "IDH_CUSCRF", "U_CustRef")
                'doAddFormDF(oForm, "IDH_ROUTE", "U_Route")
                'doAddFormDF(oForm, "IDH_SEQ", "U_Seq")

                'doAddFormDF(oForm, "IDH_PCD", "U_PremCd")
                'doAddFormDF(oForm, "IDH_SLCNO", "U_SiteLic")

                ''**PRODUCER
                'doAddFormDF(oForm, "IDH_WPRODU", "U_PCardCd")
                'doAddFormDF(oForm, "IDH_WNAM", "U_PCardNM")
                'doAddFormDF(oForm, "IDH_PRDCON", "U_PContact")
                'doAddFormDF(oForm, "IDH_PRDADD", "U_PAddress")
                'doAddFormDF(oForm, "IDH_PRDSTR", "U_PStreet")
                'doAddFormDF(oForm, "IDH_PRDBLO", "U_PBlock")
                'doAddFormDF(oForm, "IDH_PRDCIT", "U_PCity")
                'doAddFormDF(oForm, "IDH_PRDSTA", "U_PState")
                'doAddFormDF(oForm, "IDH_PRDPOS", "U_PZpCd")
                'doAddFormDF(oForm, "IDH_PRDPHO", "U_PPhone1")
                'doAddFormDF(oForm, "IDH_ORIGIN", "U_Origin")
                'doAddFormDF(oForm, "IDH_PRDCRF", "U_ProRef")

                ''**SITE
                'doAddFormDF(oForm, "IDH_DISSIT", "U_SCardCd")
                'doAddFormDF(oForm, "IDH_DISNAM", "U_SCardNM")
                'doAddFormDF(oForm, "IDH_SITCON", "U_SContact")
                'doAddFormDF(oForm, "IDH_SITADD", "U_SAddress")
                'doAddFormDF(oForm, "IDH_SITSTR", "U_SStreet")
                'doAddFormDF(oForm, "IDH_SITBLO", "U_SBlock")
                'doAddFormDF(oForm, "IDH_SITCIT", "U_SCity")
                'doAddFormDF(oForm, "IDH_SITSTA", "U_SState")
                'doAddFormDF(oForm, "IDH_SITPOS", "U_SZpCd")
                'doAddFormDF(oForm, "IDH_SITPHO", "U_SPhone1")
                'doAddFormDF(oForm, "IDH_SITCRF", "U_SiteRef")

                ''**CARRIER
                'doAddFormDF(oForm, "IDH_CARRIE", "U_CCardCd")
                'doAddFormDF(oForm, "IDH_CARNAM", "U_CCardNM")
                'doAddFormDF(oForm, "IDH_CONTNM", "U_CContact")
                'doAddFormDF(oForm, "IDH_ADDRES", "U_CAddress")
                'doAddFormDF(oForm, "IDH_STREET", "U_CStreet")
                'doAddFormDF(oForm, "IDH_BLOCK", "U_CBlock")
                'doAddFormDF(oForm, "IDH_CITY", "U_CCity")
                'doAddFormDF(oForm, "IDH_CARSTA", "U_CState")
                'doAddFormDF(oForm, "IDH_ZIP", "U_CZpCd")
                'doAddFormDF(oForm, "IDH_PHONE", "U_CPhone1")
                'doAddFormDF(oForm, "IDH_CARREF", "U_SupRef")
                ''## Start 26-09-2013;
                'doAddFormDF(oForm, "IDH_CUSLIC", "U_WasLic")
                'doAddFormDF(oForm, "IDH_LICREG", "U_CWasLic")
                ''## End


                'Dim oCheck As SAPbouiCOM.CheckBox

                ''                oForm.DataSources.UserDataSources.Add("U_CONSEL", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                ''                oCheck = oForm.Items.Item("IDH_CONSEL").Specific
                ''                oCheck.DataBind.SetBound(True, "", "U_CONSEL")
                ''                oCheck.ValOff = "N"
                ''                oCheck.ValOn = "Y"

                'doAddFormDF(oForm, "IDH_COPTRW", "U_CopTRw")
                'oCheck = oForm.Items.Item("IDH_COPTRW").Specific
                'oCheck.ValOff = "N"
                'oCheck.ValOn = "Y"

                'doAddFDBCheck(oForm, "IDH_PRCLCK", "U_LckPrc")
                ''oCheck = oForm.Items.Item("IDH_PRCLCK").Specific
                ''oCheck.ValOff = "N"
                ''oCheck.ValOn = "Y"

                'Dim oGridN As OrderRowGrid = New OrderRowGrid(Me, oForm, "LINESGRID", 7, 197, 785, 230, "")
                'oGridN.getSBOItem.FromPane = 1
                'oGridN.getSBOItem.ToPane = 1
                'oGridN.getSBOGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto
                'oGridN.getItem.AffectsFormMode = True

                ''LINK BUTTONS
                'Dim oLink As SAPbouiCOM.LinkedButton
                'oLink = oForm.Items.Item("IDH_LINKCU").Specific
                'oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

                'oLink = oForm.Items.Item("IDH_LINKPR").Specific
                'oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

                'oLink = oForm.Items.Item("IDH_LINKST").Specific
                'oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

                'oLink = oForm.Items.Item("IDH_LINKCA").Specific
                'oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

                'oLink = oForm.Items.Item("IDH_LINKSL").Specific
                'oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

                'Dim oOffRoad As SAPbouiCOM.CheckBox
                'oOffRoad = oForm.Items.Item("IDH_CHECK").Specific
                'oOffRoad.ValOff = "N"
                'oOffRoad.ValOn = "Y"

                'oForm.DataBrowser.BrowseBy = "IDH_BOOREF"

                'oForm.Items.Item("IDH_TABMAT").AffectsFormMode = False
                'oForm.Items.Item("IDH_TABCAR").AffectsFormMode = False
                'oForm.Items.Item("IDH_TABCUS").AffectsFormMode = False
                'oForm.Items.Item("IDH_TABPRO").AffectsFormMode = False
                'oForm.Items.Item("IDH_TABSIT").AffectsFormMode = False

                'Dim fSettings As SAPbouiCOM.FormSettings
                'fSettings = oForm.Settings
                'fSettings.Enabled = False
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
                BubbleEvent = False
            End Try
        End Sub

        Public Sub doGetStatuses(ByVal oForm As SAPbouiCOM.Form)
            com.uBC.utils.FillCombos.HeaderStatus(com.uBC.utils.FillCombos.getCombo(oForm, "IDH_STATUS"))
            'doFillCombo(oForm, "IDH_STATUS", "[@IDH_WRSTATUS]", "Code", "Name", Nothing, "CAST(Code As Numeric)")
        End Sub

        Public Sub doGetItemGroups(ByVal oForm As SAPbouiCOM.Form)
            '...            doFillCombo(oForm, "IDH_IGRP", "OITB g, [@IDH_JOBTYPE] jt", "ItmsGrpCod", "ItmsGrpNam", "g.ItmsGrpNam=jt.U_ItemGrp AND jt.U_OrdCat='" & msOrdCat & "'")
            doFillCombo(oForm, "IDH_IGRP", "OITB g, [@IDH_JOBTYPE] jt", "ItmsGrpCod", "ItmsGrpNam", "g.ItmsGrpCod=jt.U_ItemGrp AND jt.U_OrdCat='" & msOrdCat & "'")
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            doGetItemGroups(oForm)
            doGetStatuses(oForm)

            oForm.SupportedModes = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or SAPbouiCOM.BoFormMode.fm_EDIT_MODE Or SAPbouiCOM.BoFormMode.fm_FIND_MODE Or SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            oForm.AutoManaged = False

            doAddWF(oForm, "RulesApplied", False)
            doAddWF(oForm, "BussyWithCC", False)
            doAddWF(oForm, "CCROWS", False)
            doAddWF(oForm, "WOCreated", False)

            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New OrderRowGrid(Me, oForm, "LINESGRID", "")
            End If
            oGridN.getItem.AffectsFormMode = False
            'oGridN.setTableValue("[" & msRowTable & "]")
            'oGridN.setKeyField("Code")
            oGridN.doAddGridTable(New GridTable(msRowTable, Nothing, "Code", True, True), True)

            'oGridN.setOrderValue("Col1, U_RDate, U_RTime, CAST(Code As NUMERIC)")
            oGridN.setOrderValue("Col1, U_RDate, U_RTime, Code ") 'CAST(Code As NUMERIC)")
            oGridN.doSetDoCount(True)
            doSetListFields(oGridN)
            oGridN.doFinalize()

            ''MA Start 30-10-2014 Issue#417 Issue#418
            'Now Hide the fields as per authorization 
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES) = False OrElse
                Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) = False OrElse
                Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Charge_PRICES) = False OrElse
                Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Charge_PRICES) = False Then
                Dim oListField As com.idh.controls.strct.ListField
                For Each sField As String In IDH_JOBSHD.PriceFields_Charge
                    oListField = oGridN.getListfields().getListField(sField)
                    If Not oListField Is Nothing Then
                        oListField.miWidth = 0
                    End If
                Next
            End If
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_PRICES) = False OrElse
                Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_PRICES) = False OrElse
                Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_DO_WO_Cost_PRICES) = False OrElse
                Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_SHOW_DO_WO_Cost_PRICES) = False Then
                Dim oListField As com.idh.controls.strct.ListField
                For Each sField As String In IDH_JOBSHD.PriceFields_Cost
                    oListField = oGridN.getListfields().getListField(sField)
                    If Not oListField Is Nothing Then
                        oListField.miWidth = 0
                    End If
                Next
            End If
            ''MA End 30-10-2014 Issue#417 Issue#418

            oGridN.doSetHistory(msRowTable, "Code", False)

            Dim sCode As String = Nothing
            Dim oPreData As PreSelect = Nothing
            Dim bIsNew As Boolean = False
            Dim sExtra As String = Nothing
            Dim sAction As String = Nothing
            If getHasNewSharedData(oForm) AndAlso
                Not getParentSharedData(oForm, "USESHARE") Is Nothing Then
                sCode = getParentSharedData(oForm, "WOCode")
                sAction = getParentSharedData(oForm, "ACTION")
                oPreData = getParentSharedData(oForm, "PRESELECT")

                If oPreData IsNot Nothing Then
                    setWFValue(oForm, "PRESELECT", oPreData)
                End If

                If sCode Is Nothing OrElse sCode.Length = 0 Then
                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                    doCreateNewEntry(oForm)
                    sCode = getFormDFValue(oForm, IDH_JOBENTR._Code)
                    setWFValue(oForm, "WOCreated", True)
                    doReadInputParams(oForm)
                    setFormDFValue(oForm, IDH_JOBENTR._Code, sCode)
                ElseIf Not sAction Is Nothing AndAlso sAction.Equals("Duplicate") Then
                    Dim sNewWOCode As String = doDuplicateWO(oForm, sCode)

                    Dim sRowCode As String = Nothing
                    Dim sNewJobType As String = Nothing
                    sRowCode = getParentSharedData(oForm, "RowCode")
                    sNewJobType = getParentSharedData(oForm, "NewJobType")
                    If Not sNewWOCode Is Nothing Then
                        setWFValue(oForm, "DupRow", sRowCode)
                        setWFValue(oForm, "DupJob", sNewJobType)
                    End If
                Else
                    Dim cond As SAPbouiCOM.Condition
                    Dim conds As SAPbouiCOM.Conditions

                    conds = New SAPbouiCOM.Conditions
                    cond = conds.Add()
                    cond.Alias = "Code" 'FIELD NAME
                    cond.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                    cond.CondVal = sCode

                    getFormMainDataSource(oForm).Query(conds)

                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                    doSetEnabled(oForm.Items.Item("IDH_BOOREF"), False)
                End If
            ElseIf ghOldDialogParams.Contains(oForm.UniqueID) Then
                Dim oData As ArrayList = ghOldDialogParams.Item(oForm.UniqueID)
                If Not (oData Is Nothing) Then
                    If oData.Count > 1 Then
                        sExtra = oData.Item(0)
                        sCode = oData.Item(1)
                        If sExtra = "DoAdd" AndAlso oData.Count > 4 Then
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                            setFormDFValue(oForm, "Code", "")

                            doCreateNewEntry(oForm)
                            setWFValue(oForm, "WOCreated", True)

                            Dim sCardCode As String = oData.Item(3).Trim()
                            setFormDFValue(oForm, "U_CntrNo", oData.Item(2).Trim())
                            setFormDFValue(oForm, "U_CardCd", sCardCode)
                            setFormDFValue(oForm, "U_CardNM", oData.Item(4).Trim())
                            setFormDFValue(oForm, "U_RSDate", goParent.doDateToStr())

                            If oData.Count > 5 Then
                                setFormDFValue(oForm, "U_Status", oData.Item(5).Trim())
                            End If
                            doSetModalCustomer(oForm, sCardCode)
                        Else
                            If sCode.Length > 0 Then
                                Dim cond As SAPbouiCOM.Condition
                                Dim conds As SAPbouiCOM.Conditions

                                conds = New SAPbouiCOM.Conditions
                                cond = conds.Add()
                                cond.Alias = "Code" 'FIELD NAME
                                cond.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                                cond.CondVal = sCode

                                getFormMainDataSource(oForm).Query(conds)

                                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                                doSetEnabled(oForm.Items.Item("IDH_BOOREF"), False)
                            End If
                        End If
                    Else
                        sAction = getParentSharedData(oForm, "ACTION")
                        If Not sAction Is Nothing AndAlso sAction.Length Then
                            If sAction.ToUpper.Equals("DOADD") Then
                                setFormDFValue(oForm, "Code", "", True)
                                doSetEnabled(oForm.Items.Item("IDH_BOOREF"), True)
                                oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                            End If
                        End If
                    End If
                Else
                    doSetEnabled(oForm.Items.Item("IDH_BOOREF"), True)
                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                End If
            Else
                setFormDFValue(oForm, "Code", "", True)
                doSetEnabled(oForm.Items.Item("IDH_BOOREF"), True)
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            End If
            'oGridN.doFinalize()
        End Sub

        ''** set the PreSelect Data if it was set
        'Protected Overridable Sub doSetPreselectData(ByVal oForm As SAPbouiCOM.Form)
        '    Dim oPreData As PreSelect = getWFValue(oForm, "PRESELECT")
        '    If oPreData IsNot Nothing Then
        '        setFormDFValue(oForm, IDH_DISPORD._TRNCd, oPreData.TransactionCode)

        '        Dim sBPCode As String = oPreData.BPCode
        '        If sBPCode IsNot Nothing AndAlso sBPCode.Length > 0 Then
        '            If Config.INSTANCE.doCheckIsVendor(sBPCode) Then
        '                setFormDFValue(oForm, IDH_DISPORD._PCardCd, oPreData.BPCode)
        '                setFormDFValue(oForm, IDH_DISPORD._PCardNM, oPreData.BPName)

        '                doChooseProducer(oForm, True)
        '            Else
        '                setFormDFValue(oForm, IDH_DISPORD._CardCd, oPreData.BPCode)
        '                setFormDFValue(oForm, IDH_DISPORD._CardNM, oPreData.BPName)

        '                doChooseCustomer(oForm, True)
        '            End If
        '        End If
        '    End If
        'End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
            Dim sCode As String = getFormDFValue(oForm, "Code")
            If sCode.Length() = 0 Then
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    If getWFValue(oForm, "WOCreated") = False Then
                        doCreateNewEntry(oForm)
                    End If
                Else
                    doCreateFindEntry(oForm)
                End If
                sCode = "-999"
            Else
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    doCreateFindEntry(oForm)
                    sCode = "-999"
                End If
            End If

            oGridN.setRequiredFilter("U_JobNr = '" & sCode & "' And U_RowSta != '" & com.idh.bridge.lookups.FixedValues.getStatusDeleted() & "'")

            'To save time, do not format the grid if it is not visible
            'If oForm.PaneLevel = 1 Then
            oGridN.doReloadData()
            setWFValue(oForm, "RulesApplied", True)
            'Else
            '    oGridN.doReloadData(False)
            '    setWFValue(oForm, "RulesApplied", False)
            'End If

            oGridN.doBuildJobEntries(msOrderType)

            Dim sDuplicateRowCode As String = getWFValue(oForm, "DupRow")
            Dim sDuplicateJobType As String = getWFValue(oForm, "DupJob")
            If Not sDuplicateRowCode Is Nothing AndAlso sDuplicateRowCode.Length > 0 Then
                doDuplicateWORow(oForm, sCode, sDuplicateRowCode, sDuplicateJobType)
            End If

            '            Dim sCNo As String = getFormDFValue(oForm, "U_CntrNo")
            '            If sCNo.Length > 0 Then
            '                oForm.DataSources.UserDataSources.Item("U_CONSEL").ValueEx = "Y"
            '            Else
            '                oForm.DataSources.UserDataSources.Item("U_CONSEL").ValueEx = "N"
            '            End If

            setEnableItem(oForm, False, "IDH_CNA")
        End Sub

        Public Overridable Sub doSetAddDefaults(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        Public Overridable Sub doSetFindDefaults(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        '** Do the final form steps to show after loaddata
        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            oForm.AutoManaged = False
            oForm.Visible = True
            doClickFirstFocus(oForm)
        End Sub

        Protected Function doValidateAddress(ByVal sCardCode As String, ByVal sAddress As String, ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            Dim oAddresses As SAPbobsCOM.BPAddresses = Nothing
            Try
                oBP = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
                doValidateAddress = False
                If oBP.GetByKey(sCardCode) Then
                    oAddresses = oBP.Addresses
                    For i As Int32 = 0 To oAddresses.Count - 1
                        oAddresses.SetCurrentLine(i)
                        If oAddresses.AddressName = sAddress Then
                            doValidateAddress = True
                            Exit For
                        End If
                    Next
                Else
                    Return False
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error adding the Customers Address")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXACA", {Nothing})
                Return False
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oBP)
                IDHAddOns.idh.data.Base.doReleaseObject(oAddresses)
            End Try
        End Function
        '*** Validate the Header
        Protected Function doValidateHeader(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim sCardCode As String
            Dim sCardName As String
            Dim sBDate As String
            Dim sZpCd As String
            Dim sStreet As String
            Dim sItemGrp As String
            'Dim oPreData As PreSelect = getParentSharedData(oForm, "PRESELECT")
            Dim sTrNCode As String
            Dim sSuppCode As String
            Dim sSuppName As String

            sCardCode = getFormDFValue(oForm, "U_CardCd")
            sCardName = getFormDFValue(oForm, "U_CardNM")
            sBDate = getFormDFValue(oForm, "U_BDate")
            sZpCd = getFormDFValue(oForm, "U_ZpCd")
            sStreet = getFormDFValue(oForm, "U_STREET")
            sItemGrp = getFormDFValue(oForm, "U_ItemGrp")
            sTrNCode = getFormDFValue(oForm, IDH_JOBENTR._TRNCd)

            sSuppCode = getFormDFValue(oForm, "U_PCardCd")
            sSuppName = getFormDFValue(oForm, "U_PCardNM")

            Dim sError As String = ""
            If Config.ParameterAsBool("ORCUSTRQ", True) AndAlso sCardCode.Length = 0 Then
                sError = sError + "CustomerCode must be set, "
            Else
                If sCardName.Length > 0 Then
                    Dim sTestCustName As String = com.idh.bridge.lookups.Config.INSTANCE.doGetBPName(sCardCode)
                    If sTestCustName.Length = 0 Then
                        sError = sError & "CustomerCode is inValid,"
                    ElseIf sTestCustName.Equals(sCardName) = False Then
                        sError = sError & "CustomerName is inValid,"
                    End If
                ElseIf Config.ParameterAsBool("ORCUSTRQ", True) Then
                    sError = sError + "CustomerName must be set, "
                End If


                'If Config.ParameterAsBool("ORCUSTRQ", True) AndAlso sCardName.Length = 0 Then
                '    sError = sError + "CustomerName must be set, "
                'Else
                '    Dim sTestCustName As String = com.idh.bridge.lookups.Config.INSTANCE.doGetBPName(sCardCode)
                '    If sTestCustName.Length = 0 Then
                '        sError = sError & "CustomerCode is inValid,"
                '    ElseIf sTestCustName.Equals(sCardName) = False Then
                '        sError = sError & "CustomerName is inValid,"
                '    End If
                'End If
            End If
            If sError = "" AndAlso sCardCode.Trim() = String.Empty AndAlso sSuppCode.Trim() = String.Empty Then
                sError = sError + "You must provide Customer or Supplier, "
            End If
            If sBDate.Length = 0 Then sError = sError + "Booking Date must be set, "
            If sItemGrp.Length = 0 Then sError = sError + "Item Group must be set, "

            If sError.Length > 0 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("User: " & sError, sError)
                com.idh.bridge.DataHandler.INSTANCE.doResUserError(sError, "ERUSGEN", {com.idh.bridge.Translation.getTranslatedWord(sError)})
                Return False
            End If
            '## Start 29-05-2014
            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                'Dim sCustCardCode As String = getFormDFValue(oForm, "U_CardCd", True)
                If msOrderType.Equals("DO") = True Then
                Else
                    Dim sCustAddress As String = getFormDFValue(oForm, "U_Address", True)
                    If sCardCode.Trim.Length > 0 AndAlso sCustAddress.Trim = "" Then
                        doSetFocus(oForm, "IDH_CUSADD")
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Please select customer address.")
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Please select customer address.", "ERUSSCA", {Nothing})
                        'PARENT.APPLICATION.StatusBar.SetText("Please select customer address.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                        Return False
                    End If
                End If

                'Vaidate the address to ensure it is the correct address
                doValidateAndAddCustomerProducerAddress(oForm)

                If doValidateAllAddresses(oForm, False, False) = False Then
                    Return False
                End If
                ''Update Address Line Num
                doUpdateAddressIDs(oForm)
            End If
            '## End 29-05-2014
            Return True
        End Function

        Protected Overridable Function doFind(ByVal oForm As SAPbouiCOM.Form, ByRef sSwitch As String) As Boolean
            Return False
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            BubbleEvent = False
            doButtonID1Process(oForm)
        End Sub

        Public Function doButtonID1Process(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim bDoNew As Boolean = False

            Try
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse
                   oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then

                    Dim bDoContinue As Boolean = False
                    Dim sU_JOBNR As String

                    sU_JOBNR = getFormDFValue(oForm, "Code")

                    Try
                        If getWFValue(oForm, "BussyWithCC") = False Then
                            If doBeforeBtn1(oForm) = False Then
                                Return False
                            End If
                            If doValidateHeader(oForm) = False Then
                                Return False
                            End If
                            If Config.INSTANCE.getParameterAsBool("CCONROW", False) = False Then
                                If doCreditCardPayment(oForm) = False Then
                                    Return False
                                End If
                            End If
                        Else
                            setWFValue(oForm, "BussyWithCC", False)
                        End If

                        '20130515: Setting Driver Instructions to WOR level if 'Copy To Rows' ticked 
                        'START
                        Dim sCopyToRows As String = getFormDFValue(oForm, "U_CopTRw")
                        Dim sDriverInstructions As String = getFormDFValue(oForm, "U_SpInst")

                        If sCopyToRows IsNot Nothing _
                            AndAlso sCopyToRows.Length > 0 _
                            AndAlso sCopyToRows.Equals("Y") _
                            AndAlso sDriverInstructions IsNot Nothing _
                            AndAlso sDriverInstructions.Length > 0 Then
                            oForm.Freeze(True)
                            doAppendDriverInstructionsOnWOR(oForm, sDriverInstructions)
                            oForm.Freeze(False)
                        End If
                        'END 

                        goParent.goDICompany.StartTransaction()

                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            If doUpdateHeader(oForm, True) = True Then
                                bDoContinue = True
                                bDoNew = True
                            End If
                        Else
                            If doUpdateHeader(oForm, False) = True Then
                                bDoContinue = True
                            End If
                        End If

                        Dim oList As ArrayList = doGetModifiedRows(oForm)
                        If bDoContinue = True Then
                            bDoContinue = doUpdateGridRows(oForm)
                        End If
                        ''MA Start 23-07-2015 Issue#881
                        If bDoContinue = True AndAlso getFormDFValue(oForm, IDH_JOBENTR._Status) = "4" AndAlso getFormDFValue(oForm, IDH_JOBENTR._PBICODE).ToString.Trim <> "" Then
                            Dim oPBI As IDH_PBI = New IDH_PBI()
                            If (oPBI.getByKey(getFormDFValue(oForm, IDH_JOBENTR._PBICODE))) Then
                                oPBI.U_IDHRECEN = 2
                                oPBI.U_IDHRECED = DateTime.Today
                                oPBI.doUpdateDataRow(False, "WOH cancelled")
                            End If

                        End If
                        ''MA End 23-07-2015 Issue#881

                        If bDoContinue = True Then
                            If goParent.goDICompany.InTransaction Then
                                goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                                com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()

                                bDoContinue = doOtherMarketingProcesses(oForm, oList)
                            Else
                                'Something went wrong...
                                com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
                                oForm.Update()

                                Dim oUpdateGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                                oUpdateGrid.doLoadLastChangeIndicators()

                                Return False
                            End If
                        Else
                            If goParent.goDICompany.InTransaction Then
                                goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                            End If
                            com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
                            oForm.Update()

                            Dim oUpdateGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                            oUpdateGrid.doLoadLastChangeIndicators()

                            Return False
                        End If
                        doWarnInActiveBPnSites(oForm)
                        'If bDoContinue AndAlso getWFValue(oForm, "PRESELECT") IsNot Nothing Then
                        '    oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        'End If
                    Catch ex As Exception
                        If goParent.goDICompany.InTransaction Then
                            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                        End If
                        com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing Button 1.")
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXBNTO", {Nothing})
                        oForm.Update()

                        Dim oUpdateGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                        oUpdateGrid.doLoadLastChangeIndicators()

                        Return False
                    End Try
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    doFind(oForm, "BT1")
                    oForm.Update()
                    Return False
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    'Set the Modal data and close
                    If goParent.doCheckModal(oForm.UniqueID) = True Then
                        doSetModalData(oForm)
                    Else
                        doAfterBtn1(oForm, False)
                        oForm.Update()
                    End If
                    Return False
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing button 1.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXBNTO", {Nothing})
                Return False
            Finally
            End Try

            If getWFValue(oForm, "PRESELECT") Is Nothing Then
                If goParent.doCheckModal(oForm.UniqueID) = False Then
                    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                    End If
                End If

                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse
                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then

                    If goParent.doCheckModal(oForm.UniqueID) = True Then
                        doSetModalData(oForm)
                    Else
                        doAfterBtn1(oForm, False)
                        oForm.Update()
                    End If
                End If
            Else
                doReturnFromModalShared(oForm, False)
            End If
            Return True
        End Function
        Protected Overridable Sub doWarnInActiveBPnSites(ByVal oForm As SAPbouiCOM.Form)
            Try
                If Config.ParameterAsBool("SHWINACW", False) = False Then
                    Exit Sub
                End If
                Dim sValue As String = ""

                Dim sBPCodes As String() '= {""}
                Dim sAddresses As String() '= {""}

                sBPCodes = {"Customer", "IDH_CUST", "Producer/Supplier", "IDH_WPRODU", "Carrier/Haulier", "IDH_CARRIE",
                                              "Disposal Site", "IDH_DISSIT"}

                sAddresses = {"Customer Address", "IDH_CUST", "IDH_CUSADD", "Supplier/Producer Address", "IDH_WPRODU", "IDH_PRDADD",
                                                                                "Disposal Site Address", "IDH_DISSIT", "IDH_SITADD",
                                                                                "Haulier/Carrier Address", "IDH_CARRIE", "IDH_ADDRES"}
                If oForm.TypeEx.IndexOf("IDH_WASTORD") > -1 Then
                    'sBPCodes = com.idh.utils.General.doCombineArrays(sBPCodes, {"Customer", "IDH_CUST", "Producer", "IDH_WPRODU", "Carrier", "IDH_CARRIE", _
                    '                              "Disposal Site", "IDH_DISSIT"})

                    sAddresses = com.idh.utils.General.doCombineArrays(sAddresses, {"Mailing Address", "IDH_CUST", "IDH_MLAADD"})
                End If
                Dim sLabel, sBPField, sBPCode, sAddressField As String
                Dim oBPParam() As String = {"", ""}
                Dim oAddressParam() As String = {"", ""}
                'Dim oSelectedRows As SAPbouiCOM.SelectedRows = oGridN.getGrid.Rows.SelectedRows
                'Dim iSelected As Integer = oSelectedRows.Count

                'For iIndex As Integer = 0 To iSelected - 1
                For iitem As Int32 = 0 To sBPCodes.Count - 1 Step 2
                    sLabel = sBPCodes(iitem)
                    sBPField = sBPCodes(iitem + 1)
                    sValue = getItemValue(oForm, sBPField)
                    If sValue IsNot Nothing AndAlso sValue.Trim <> "" AndAlso Not Config.INSTANCE.doCheckBPActive(sValue, DateTime.Now) Then
                        oBPParam(0) = com.idh.bridge.Translation.getTranslatedWord(sLabel)
                        oBPParam(1) = sValue
                        doWarnMess(com.idh.bridge.res.Messages.getGMessage("WRINACBP", oBPParam), SAPbouiCOM.BoMessageTime.bmt_Short)
                    End If
                Next
                For iitem As Int32 = 0 To sAddresses.Count - 1 Step 3
                    sLabel = sAddresses(iitem)
                    sBPField = sAddresses(iitem + 1)
                    sAddressField = sAddresses(iitem + 2)

                    sValue = getItemValue(oForm, sAddressField)
                    sBPCode = getItemValue(oForm, sBPField)
                    If sValue IsNot Nothing AndAlso sValue.Trim <> "" AndAlso Not Config.INSTANCE.doCheckBPAddressActive(sBPCode, sValue) Then
                        'oBPParam(0) = com.idh.bridge.Translation.getTranslatedWord(sLabel)
                        'oBPParam(1) = sValue
                        oAddressParam(0) = Translation.getTranslatedWord(sLabel)
                        oAddressParam(1) = sValue
                        'oAddressParam(2) = sBPField
                        doWarnMess(com.idh.bridge.res.Messages.getGMessage("WRINACAD", oBPParam), SAPbouiCOM.BoMessageTime.bmt_Short)
                    End If
                Next
                'Next
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "doWarnInActiveBPnSites", {Nothing})
            End Try
        End Sub
        Public Overridable Function doAddCustomerShipToAddress(ByVal oForm As SAPbouiCOM.Form) As Boolean
            'Dim sCardCode As String = getFormDFValue(oForm, "U_CardCd")
            'Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            'Dim oAddress As SAPbobsCOM.BPAddresses = Nothing
            'If sCardCode.Length > 0 Then
            '    Try
            '        oBP = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
            '        If oBP.GetByKey(sCardCode) Then
            '            oAddress = oBP.Addresses
            '            'oAddress.Add()

            '            oAddress.AddressName = getFormDFValue(oForm, "U_Address")
            '            oAddress.Street = getFormDFValue(oForm, "U_Street")
            '            oAddress.Block = getFormDFValue(oForm, "U_Block")
            '            oAddress.ZipCode = getFormDFValue(oForm, "U_ZpCd")
            '            oAddress.City = getFormDFValue(oForm, "U_City")
            '            oAddress.County = getFormDFValue(oForm, "U_County")
            '            oAddress.State = getFormDFValue(oForm, "U_State")
            '            oAddress.UserFields.Fields.Item("U_ISBCUSTRN").Value = getFormDFValue(oForm, "U_CustRef")
            '            oAddress.UserFields.Fields.Item("U_TEL1").Value = getFormDFValue(oForm, "U_SiteTl")
            '            oAddress.UserFields.Fields.Item("U_IDHSteId").Value = getFormDFValue(oForm, "U_SteId")
            '            oAddress.UserFields.Fields.Item("U_ROUTE").Value = getFormDFValue(oForm, "U_Route")
            '            oAddress.UserFields.Fields.Item("U_IDHSEQ").Value = getFormDFValue(oForm, "U_Seq")

            '            oAddress.UserFields.Fields.Item("U_IDHPCD").Value = getFormDFValue(oForm, "U_PremCd")
            '            oAddress.UserFields.Fields.Item("U_IDHSLCNO").Value = getFormDFValue(oForm, "U_SiteLic")

            '            'site telephone - Allready saving back,
            '            oAddress.UserFields.Fields.Item("U_Origin").Value = getFormDFValue(oForm, "U_Origin") 'origin U_Origin,
            '            oAddress.UserFields.Fields.Item("U_IDHACN").Value = getFormDFValue(oForm, "U_Contact") 'contact person - U_IDHACN

            '            Dim iwResult As Integer
            '            Dim swResult As String = Nothing
            '            iwResult = oBP.Update()
            '            If iwResult <> 0 Then
            '                goParent.goDICompany.GetLastError(iwResult, swResult)
            '                com.idh.bridge.DataHandler.INSTANCE.doError("Error Adding Address: " + swResult)
            '                Return False
            '            End If
            '        End If
            '    Catch ex As Exception
            '        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error adding the Customers Address")
            '        Return False
            '    Finally
            '        IDHAddOns.idh.data.Base.doReleaseObject(oBP)
            '        IDHAddOns.idh.data.Base.doReleaseObject(oAddress)
            '    End Try
            'End If
            Return True
        End Function

        Public Overridable Function doAddProducerShipToAddress(ByVal oForm As SAPbouiCOM.Form) As Boolean
            '      Dim sCardCode As String = getFormDFValue(oForm, "U_PCardCd")
            '      Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            '      Dim oAddress As SAPbobsCOM.BPAddresses = Nothing
            '      If sCardCode.Length > 0 Then
            '          Try
            '              oBP = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
            '              If oBP.GetByKey(sCardCode) Then
            '                  oAddress = oBP.Addresses
            '                  'oAddress.Add()

            '                  oAddress.AddressName = getFormDFValue(oForm, "U_PAddress")
            '                  oAddress.Street = getFormDFValue(oForm, "U_PStreet")
            '                  oAddress.Block = getFormDFValue(oForm, "U_PBlock")
            '                  oAddress.ZipCode = getFormDFValue(oForm, "U_PZpCd")
            '                  oAddress.City = getFormDFValue(oForm, "U_PCity")
            '                  'oAddress.County = getFormDFValue(oForm, "U_County")
            '                  oAddress.State = getFormDFValue(oForm, "U_PState")

            '                  oAddress.UserFields.Fields.Item("U_ISBCUSTRN").Value = getFormDFValue(oForm, "U_ProRef")
            '                  oAddress.UserFields.Fields.Item("U_TEL1").Value = getFormDFValue(oForm, "U_PPhone1")
            '                  'oAddress.UserFields.Fields.Item("U_IDHSteId").Value = getFormDFValue(oForm, "U_SteId")
            '                  'oAddress.UserFields.Fields.Item("U_ROUTE").Value = getFormDFValue(oForm, "U_Route")
            '                  'oAddress.UserFields.Fields.Item("U_IDHSEQ").Value = getFormDFValue(oForm, "U_Seq")

            '                  'oAddress.UserFields.Fields.Item("U_IDHPCD").Value = getFormDFValue(oForm, "U_PremCd")
            '                  'oAddress.UserFields.Fields.Item("U_IDHSLCNO").Value = getFormDFValue(oForm, "U_SiteLic")

            ''site telephone - Allready saving back,
            'oAddress.UserFields.Fields.Item("U_Origin").Value = getFormDFValue(oForm, "U_Origin") 'origin U_Origin,
            ''oAddress.UserFields.Fields.Item("U_IDHACN").Value = getFormDFValue(oForm, "U_Contact") 'contact person - U_IDHACN

            '                  Dim iwResult As Integer
            '                  Dim swResult As String = Nothing
            '                  iwResult = oBP.Update()
            '                  If iwResult <> 0 Then
            '                      goParent.goDICompany.GetLastError(iwResult, swResult)
            '                      com.idh.bridge.DataHandler.INSTANCE.doError("Error Adding Address: " + swResult)
            '                      Return False
            '                  End If
            '              End If
            '          Catch ex As Exception
            '              com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error adding the Producer Address")
            '              Return False
            '          Finally
            '              IDHAddOns.idh.data.Base.doReleaseObject(oBP)
            '              IDHAddOns.idh.data.Base.doReleaseObject(oAddress)
            '          End Try
            '      End If
            Return True
        End Function

        Public Overridable Function doAfterBtn1(ByVal oForm As SAPbouiCOM.Form, ByVal bDoNew As Boolean) As Boolean
            Return True
        End Function

        Public Overridable Sub doValidateAndAddCustomerProducerAddress(ByVal oForm As SAPbouiCOM.Form)
            Dim bCustAddressValid As Boolean = True
            Dim bProdAddressValid As Boolean = True
            If doValidateCustomerAddress(oForm, False) = False Then
                bCustAddressValid = False
            End If
            If doValidateProducerAddress(oForm, False) = False Then
                bProdAddressValid = False
            End If
            If Not bCustAddressValid Then
                If doAddCustomerShipToAddress(oForm) = False Then
                End If
            ElseIf Not bProdAddressValid Then
                If doAddProducerShipToAddress(oForm) = False Then
                End If
            End If
        End Sub
        Public Overridable Sub doUpdateAddressIDs(ByVal oForm As SAPbouiCOM.Form)
            ''Customer
            Dim sCardCode As String = getFormDFValue(oForm, "U_CardCd", True)
            Dim sAddress As String = getFormDFValue(oForm, "U_Address", True)
            Dim sAddressLineNum As String = ""
            If sCardCode.Trim <> "" AndAlso sAddress.Trim <> "" Then
                sAddressLineNum = com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPShipToAddress(sCardCode, sAddress, "LineNum")
                setFormDFValue(oForm, "U_AddrssLN", sAddressLineNum)
            Else
                setFormDFValue(oForm, "U_AddrssLN", "")
            End If
            ''Supplier
            sCardCode = getFormDFValue(oForm, "U_PCardCd", True)
            sAddress = getFormDFValue(oForm, "U_PAddress", True)
            sAddressLineNum = ""
            If sCardCode.Trim <> "" AndAlso sAddress.Trim <> "" Then
                sAddressLineNum = com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPShipToAddress(sCardCode, sAddress, "LineNum")
                setFormDFValue(oForm, "U_PAddrsLN", sAddressLineNum)
            Else
                setFormDFValue(oForm, "U_PAddrsLN", sAddressLineNum)
            End If
            ''Carrier
            sCardCode = getFormDFValue(oForm, "U_CCardCd", True)
            sAddress = getFormDFValue(oForm, "U_CAddress", True)
            sAddressLineNum = ""
            If sCardCode.Trim <> "" AndAlso sAddress.Trim <> "" Then
                sAddressLineNum = com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPShipToAddress(sCardCode, sAddress, "LineNum")
                setFormDFValue(oForm, "U_CAddrsLN", sAddressLineNum)
            Else
                setFormDFValue(oForm, "U_CAddrsLN", sAddressLineNum)
            End If
            ''Site
            sCardCode = getFormDFValue(oForm, "U_SCardCd", True)
            sAddress = getFormDFValue(oForm, "U_SAddress", True)
            sAddressLineNum = ""
            If sCardCode.Trim <> "" AndAlso sAddress.Trim <> "" Then
                sAddressLineNum = com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPShipToAddress(sCardCode, sAddress, "LineNum")
                setFormDFValue(oForm, "U_SAddrsLN", sAddressLineNum)
            Else
                setFormDFValue(oForm, "U_SAddrsLN", sAddressLineNum)
            End If
        End Sub
        Public Overridable Function doValidateAllAddresses(ByVal oForm As SAPbouiCOM.Form,
             ByVal bDoCustomerAddress As Boolean, ByVal bDoProducerAddress As Boolean) As Boolean
            If bDoCustomerAddress AndAlso doValidateCustomerAddress(oForm) = False Then
                Return False
            End If

            If bDoProducerAddress AndAlso doValidateProducerAddress(oForm) = False Then
                Return False
            End If

            If doValidateCareerAddress(oForm) = False OrElse
                doValidateDisposalSiteAddress(oForm) = False Then
                Return False
            End If
            Return True
        End Function
        Public Overridable Function doValidateCareerAddress(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim sCarrCardCode As String = getFormDFValue(oForm, "U_CCardCd", True)
            Dim sCarrAddress As String = getFormDFValue(oForm, "U_CAddress", True)
            If sCarrCardCode.Trim <> "" AndAlso sCarrAddress.Trim <> "" Then
                If doValidateAddress(sCarrCardCode, sCarrAddress, oForm) = False Then
                    doSetFocus(oForm, "IDH_ADDRES")
                    IDHAddOns.idh.addon.Base.APPLICATION.StatusBar.SetText("Invalid carrier address.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    Return False
                End If
            End If
            Return True

        End Function

        Public Overridable Function doValidateCustomerAddress(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bShowErrMessage As Boolean = True) As Boolean
            Dim sCustCardCode As String = getFormDFValue(oForm, "U_CardCd", True)
            Dim sCustAddress As String = getFormDFValue(oForm, "U_Address", True)
            If sCustCardCode.Trim <> "" AndAlso sCustAddress.Trim <> "" Then
                If doValidateAddress(sCustCardCode, sCustAddress, oForm) = False Then
                    doSetFocus(oForm, "IDH_CUSADD")
                    If bShowErrMessage Then IDHAddOns.idh.addon.Base.APPLICATION.StatusBar.SetText("Invalid customer address.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    Return False
                End If
            End If
            Return True
        End Function

        Public Overridable Function doValidateProducerAddress(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bShowErrMessage As Boolean = True) As Boolean
            Dim sPCardCode As String = getFormDFValue(oForm, "U_PCardCd", True)
            Dim sPAddress As String = getFormDFValue(oForm, "U_PAddress", True)
            If sPCardCode.Trim <> "" AndAlso sPAddress.Trim <> "" Then
                If doValidateAddress(sPCardCode, sPAddress, oForm) = False Then
                    doSetFocus(oForm, "IDH_PRDADD")
                    If bShowErrMessage Then IDHAddOns.idh.addon.Base.APPLICATION.StatusBar.SetText("Invalid producer address.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    Return False
                End If
            End If
            Return True
        End Function

        Public Overridable Function doValidateDisposalSiteAddress(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bShowErrMessage As Boolean = True) As Boolean
            Dim sSCardCode As String = getFormDFValue(oForm, "U_SCardCd", True)
            Dim sSAddress As String = getFormDFValue(oForm, "U_SAddress", True)
            If sSCardCode.Trim <> "" AndAlso sSAddress.Trim <> "" Then
                If doValidateAddress(sSCardCode, sSAddress, oForm) = False Then
                    doSetFocus(oForm, "IDH_SITADD")
                    If bShowErrMessage Then IDHAddOns.idh.addon.Base.APPLICATION.StatusBar.SetText("Invalid Site address.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    Return False
                End If
            End If
            Return True
        End Function

        Public Overridable Function doBeforeBtn1(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Return doValidateCareerAddress(oForm)
        End Function

        Public Overrides Sub doButtonID2(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            doReturnFromCanceled(oForm, BubbleEvent)
        End Sub

        Protected Overridable Function doUpdateGridRows(ByVal oForm As SAPbouiCOM.Form) As Boolean
            '    Dim oUpdateGrid As UpdateGrid
            '    oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")

            '    Dim oAddedRows As ArrayList
            '    If oUpdateGrid.hasAddedRows Then
            '        oAddedRows = oUpdateGrid.doGetAddedRows().Clone()
            '    End If
            '    Dim oModifiedRows As ArrayList
            '    If oUpdateGrid.hasChangedRows Then
            '        oModifiedRows = oUpdateGrid.doGetChangedRows().Clone()
            '    End If
            '    Dim oNCRRows_NCR As New ArrayList
            '    Dim oNCRRows_NCRCompliant As New ArrayList
            '    Dim oNCRRows_NCRResolved As New ArrayList
            '    Dim oLabTaskRows As New ArrayList
            '    'If Config.INSTANCE.getParameterAsBool("NCRALRT", False) Then
            '    doGetNCRnLabChangedRows(oForm, oAddedRows, oModifiedRows, oNCRRows_NCR, oNCRRows_NCRCompliant, oNCRRows_NCRResolved, oLabTaskRows)
            '    'End If
            '    If oUpdateGrid.doProcessData() = True Then
            '        doRemoteAlert(oForm, oAddedRows)
            '        If oModifiedRows IsNot Nothing AndAlso oModifiedRows.Count > 0 Then
            '            doUpdateFinalBatch(oForm)
            '        End If
            '        ''call here the Update of Batch Qty
            '        If Config.INSTANCE.getParameterAsBool("NCRALRT", False) Then
            '            If oNCRRows_NCR IsNot Nothing AndAlso oNCRRows_NCR.Count > 0 Then
            '                For iIndex As Int16 = 0 To oNCRRows_NCR.Count - 1
            '                    Dim iRow As Int16 = oNCRRows_NCR.Item(iIndex)
            '                    oUpdateGrid.setCurrentDataRowIndex(iRow)
            '                    com.idh.bridge.utils.General.SendInternalAlert(Config.INSTANCE.getParameterWithDefault("NCRRECP1", ""), Config.INSTANCE.getParameterWithDefault("NCRMSG1", ""), Config.INSTANCE.getParameterWithDefault("NCRSUB1", ""), "NCR", oUpdateGrid.doGetFieldValue("Code"), "WOR")
            '                Next
            '            End If
            '            If oNCRRows_NCRCompliant IsNot Nothing AndAlso oNCRRows_NCRCompliant.Count > 0 Then
            '                For iIndex As Int16 = 0 To oNCRRows_NCRCompliant.Count - 1
            '                    Dim iRow As Int16 = oNCRRows_NCRCompliant.Item(iIndex)
            '                    oUpdateGrid.setCurrentDataRowIndex(iRow)
            '                    com.idh.bridge.utils.General.SendInternalAlert(Config.INSTANCE.getParameterWithDefault("NCRRECP2", ""), Config.INSTANCE.getParameterWithDefault("NCRMSG2", ""), Config.INSTANCE.getParameterWithDefault("NCRSUB2", ""), "NCR Compliant", oUpdateGrid.doGetFieldValue("Code"), "WOR")
            '                Next
            '            End If
            '            If oNCRRows_NCRResolved IsNot Nothing AndAlso oNCRRows_NCRResolved.Count > 0 Then
            '                For iIndex As Int16 = 0 To oNCRRows_NCRResolved.Count - 1
            '                    Dim iRow As Int16 = oNCRRows_NCRResolved.Item(iIndex)
            '                    oUpdateGrid.setCurrentDataRowIndex(iRow)
            '                    com.idh.bridge.utils.General.SendInternalAlert(Config.INSTANCE.getParameterWithDefault("NCRRECP3", ""), Config.INSTANCE.getParameterWithDefault("NCRMSG3", ""), Config.INSTANCE.getParameterWithDefault("NCRSUB3", ""), "NCR Resolved", oUpdateGrid.doGetFieldValue("Code"), "WOR")
            '                Next
            '            End If
            '        End If
            '        If oLabTaskRows IsNot Nothing AndAlso oLabTaskRows.Count > 0 Then
            '            For iIndex As Int16 = 0 To oLabTaskRows.Count - 1
            '                Dim iRow As Int16 = oLabTaskRows.Item(iIndex)
            '                oUpdateGrid.setCurrentDataRowIndex(iRow)
            '                If oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Sample) = "Y" AndAlso oUpdateGrid.doGetFieldValue(IDH_JOBSHD._SampleRef) <> "" Then
            '                    Dim oLabTask As New IDH_LABTASK()
            '                    oLabTask.UnLockTable = True
            '                    If (oLabTask.getByKey(oUpdateGrid.doGetFieldValue(IDH_JOBSHD._SampleRef)) AndAlso (oLabTask.U_Saved = 0)) Then
            '                        oLabTask.U_Saved = 1
            '                        Dim sUser As String = oLabTask.U_AssignedTo
            '                        oLabTask.doProcessData()

            '                        Dim oLabItem As New IDH_LABITM()
            '                        oLabItem.UnLockTable = True
            '                        oLabItem.getByKey(oLabTask.U_LabItemRCd)
            '                        oLabItem.U_Saved = 1
            '                        oLabItem.doProcessData()
            '                        If (Not String.IsNullOrEmpty(sUser)) Then
            '                            IDH_LABTASK.dosendAlerttoUser(sUser, com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTM1", Nothing), com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTS1", Nothing), False, oLabTask.Code)
            '                        End If
            '                    End If
            '                End If
            '                'com.idh.bridge.utils.General.SendInternalAlert(Config.INSTANCE.getParameterWithDefault("NCRRECP3", ""), Config.INSTANCE.getParameterWithDefault("NCRMSG3", ""), Config.INSTANCE.getParameterWithDefault("NCRSUB3", ""), "NCR Resolved", oUpdateGrid.doGetFieldValue("Code"), "WOR")
            '            Next
            '        End If
            Return True
            '    Else
            '        Return False
            '    End If
        End Function

        Protected Overridable Function doAppendDriverInstructionsOnWOR(ByVal oForm As SAPbouiCOM.Form, ByVal sDriverInstructions As String) As Boolean
            Dim oUpdateGrid As UpdateGrid
            oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")

            'Dim iRows As Integer = oUpdateGrid.doGetChangedRows()

            For iRows As Integer = 0 To oUpdateGrid.getRowCount - 1
                oUpdateGrid.setCurrentDataRowIndex(iRows)
                Dim sExistingComments As String = oUpdateGrid.doGetFieldValue("U_Comment")
                Dim sAddAutoStamp As String = If(Config.INSTANCE.getParameterAsBool("SPIASTMP", False), "WOH Drv. Inst. As On: " + DateAndTime.Now.ToString() + Chr(13), String.Empty)
                Dim sNewComments As String = sAddAutoStamp + sDriverInstructions + Chr(13) + sExistingComments
                oUpdateGrid.doSetFieldValue("U_Comment", sNewComments)
            Next
            Return True
        End Function

        Protected Overridable Function doCreditCardPayment(ByVal oForm As SAPbouiCOM.Form) As Boolean
            'Do the Credit Card processing here
            Dim oUpdateGrid As UpdateGrid
            oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")

            Dim dCCTotal As Double = 0
            Dim sStatus As String
            Dim sPayMethod As String
            Dim sPayStatus As String
            Dim dRowTotal As Double
            Dim saRows As New ArrayList
            Dim iIndex As Integer = 0
            Dim oList As ArrayList = oUpdateGrid.doGetChangedRows()

            Dim iRows As Integer = oUpdateGrid.getRowCount
            For iIndex = 0 To iRows - 1
                oUpdateGrid.setCurrentDataRowIndex(iIndex)
                sStatus = oUpdateGrid.doGetFieldValue("U_Status")
                sPayMethod = oUpdateGrid.doGetFieldValue("U_PayMeth")
                sPayStatus = oUpdateGrid.doGetFieldValue("U_PayStat")
                dRowTotal = oUpdateGrid.doGetFieldValue("U_Total")
                '                If ( sStatus.Equals("DoInvoice") OrElse sStatus.Equals("ReqInvoice")) AndAlso _
                '                    sPayMethod.Equals(Config.INSTANCE.getCreditCardName(goParent)) AndAlso _
                '                    sPayStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusUnPaid()) AndAlso _
                '                    dRowTotal > 0 Then
                '                    dCCTotal = dCCTotal + dRowTotal
                '                    saRows.Add(iIndex)
                '                End If
                If com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus) = True AndAlso
                    sPayMethod.Equals(Config.INSTANCE.getCreditCardName()) AndAlso
                    sPayStatus.Equals(com.idh.bridge.lookups.FixedValues.getStatusUnPaid()) AndAlso
                    dRowTotal <> 0 Then
                    dCCTotal = dCCTotal + dRowTotal
                    saRows.Add(iIndex)
                End If
            Next
            If dCCTotal <> 0 Then
                setWFValue(oForm, "CCROWS", saRows)
                Dim sCustCd As String = getFormDFValue(oForm, "U_CardCd")
                Dim sCustNm As String = getFormDFValue(oForm, "U_CardNM")

                setSharedData(oForm, "CUSTCD", sCustCd)
                setSharedData(oForm, "CUSTNM", sCustNm)
                setSharedData(oForm, "AMOUNT", dCCTotal)
                goParent.doOpenModalForm("IDHCCPAY", oForm)

                Return False
            End If
            Return True
        End Function

        Public Overridable Function getUnCommittedRowTotals(ByVal oForm As SAPbouiCOM.Form, Optional ByVal sRowCode As String = Nothing, Optional ByVal dRowTotal As Double = 0) As Double
            Dim oUpdateGrid As UpdateGrid
            oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")

            Dim dTotal As Double = 0
            Dim sStatus As String
            Dim sWRowCode As String
            Dim dRowsTotal As Double
            Dim iIndex As Integer = 0
            Dim iRow As Integer
            Dim oList As ArrayList = oUpdateGrid.doGetAddedRows()

            If Not oList Is Nothing Then
                While iIndex < oList.Count
                    iRow = oList.Item(iIndex)
                    oUpdateGrid.setCurrentDataRowIndex(iRow)

                    sWRowCode = oUpdateGrid.doGetFieldValue("Code")
                    sStatus = oUpdateGrid.doGetFieldValue("U_Status")
                    If Not sRowCode Is Nothing AndAlso sWRowCode.Equals(sRowCode) Then
                        dTotal = dTotal + dRowTotal
                        sRowCode = Nothing
                        dRowTotal = 0
                    Else
                        If sStatus.EndsWith(com.idh.bridge.lookups.FixedValues.getStatusFoc()) = False Then
                            dRowsTotal = dRowsTotal + oUpdateGrid.doGetFieldValue("U_Total")
                            dTotal = dTotal + dRowsTotal
                        End If
                    End If

                    iIndex = iIndex + 1
                End While
            End If

            Return dTotal + dRowTotal
        End Function

        Protected Function doGetModifiedRows(ByVal oForm As SAPbouiCOM.Form) As ArrayList
            Dim oUpdateGrid As UpdateGrid
            oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")

            Dim oChangedRows As ArrayList = oUpdateGrid.doGetChangedRows()
            Dim oAddedRows As ArrayList = oUpdateGrid.doGetAddedRows()
            Dim oChangedAddedRows As ArrayList = Nothing

            If oChangedRows IsNot Nothing OrElse oAddedRows IsNot Nothing Then
                oChangedAddedRows = New ArrayList()

                If oChangedRows IsNot Nothing Then
                    oChangedAddedRows.AddRange(oChangedRows)
                    If oAddedRows IsNot Nothing Then
                        oChangedAddedRows.AddRange(oAddedRows)
                    End If
                Else
                    oChangedAddedRows.AddRange(oAddedRows)
                End If
            End If

            Return oChangedAddedRows
        End Function

        Protected Overridable Function doOtherMarketingProcesses(ByVal oForm As SAPbouiCOM.Form, ByVal oChangedAddedRows As ArrayList) As Boolean
            Return True
        End Function

        Protected Function doFoc(ByVal sHead As String, ByVal sRow As String) As Boolean
            Return MarketingDocs.doFoc(getFormMainTable(), msRowTable, msOrderType, sHead, sRow)
        End Function

        Protected Function doOrders(ByVal sHead As String, ByVal sRow As String) As Boolean
            Return MarketingDocs.doOrders(getFormMainTable(), msRowTable, msOrderType, sHead, sRow)
        End Function

        Protected Function doRebates(ByVal sHead As String, ByVal sRow As String) As Boolean
            Return MarketingDocs.doRebates(getFormMainTable(), msRowTable, msOrderType, sHead, sRow)
        End Function

        Protected Function doInvoicesAR(ByVal sHead As String, ByVal sRow As String) As Boolean
            Return MarketingDocs.doInvoicesAR(getFormMainTable(), msRowTable, msOrderType, sHead, sRow)
        End Function

        'Protected Overridable Function doRemoteAlert(ByVal oForm As SAPbouiCOM.Form, ByRef oAddedRows As ArrayList) As Boolean
        '    Return True
        'End Function
        'Protected Overridable Sub doUpdateFinalBatch(ByVal oForm As SAPbouiCOM.Form)

        'End Sub
        'Protected Overridable Sub doGetNCRnLabChangedRows(ByVal oForm As SAPbouiCOM.Form, ByRef oAddedRows As ArrayList, ByRef oModfieldRows As ArrayList,
        '        ByRef oNCRRows_NCR As ArrayList, ByRef oNCRRows_NCRCompliant As ArrayList, ByRef oNCRRows_NCRResolved As ArrayList,
        '        ByRef oLabTaskRows As System.Collections.ArrayList)
        'End Sub

        Protected Function doUpdateHeader(ByVal oForm As SAPbouiCOM.Form, ByVal bIsAdd As Boolean) As Boolean
            Dim iwResult As Integer = 0
            Dim swResult As String = Nothing
            '            Dim sAction As Char

            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(goParent, getFormMainTable())
            Try
                With getFormMainDataSource(oForm)
                    Dim sCode As String = .GetValue("Code", .Offset).Trim()
                    If oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) = True Then
                        If bIsAdd Then
                            'com.idh.bridge.DataHandler.INSTANCE.doError("Duplicate entries are not allowed - " & sCode)
                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Duplicate entries are not allowed - " & sCode, "ERUSDBDU", {Nothing})
                            Return False
                        End If
                    Else
                        oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
                        oAuditObj.setName(.GetValue("Name", .Offset).Trim())
                    End If

                    oAuditObj.setFieldValue("U_User", .GetValue("U_User", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Status", .GetValue("U_Status", .Offset).Trim())
                    oAuditObj.setFieldValue("U_CntrNo", .GetValue("U_CntrNo", .Offset).Trim())

                    oAuditObj.setFieldValue("U_BDate", com.idh.utils.Dates.doStrToDate(.GetValue("U_BDate", .Offset).Trim()))
                    oAuditObj.setFieldValue("U_BTime", goParent.doSBOStrToTimeStr(.GetValue("U_BTime", .Offset).Trim()))

                    oAuditObj.setFieldValue("U_RSDate", com.idh.utils.Dates.doStrToDate(.GetValue("U_RSDate", .Offset).Trim()))
                    oAuditObj.setFieldValue("U_RSTime", goParent.doSBOStrToTimeStr(.GetValue("U_RSTime", .Offset).Trim()))

                    oAuditObj.setFieldValue("U_REDate", com.idh.utils.Dates.doStrToDate(.GetValue("U_REDate", .Offset).Trim()))
                    oAuditObj.setFieldValue("U_RETime", goParent.doSBOStrToTimeStr(.GetValue("U_RETime", .Offset).Trim()))

                    'oAuditObj.setFieldValue("U_ORDTP", goParent.doSBOStrToTimeStr(msOrderType))
                    oAuditObj.setFieldValue("U_ORDTP", msOrderType)

                    oAuditObj.setFieldValue("U_FirstBP", .GetValue("U_FirstBP", .Offset).Trim())

                    '**CUSTOMER
                    oAuditObj.setFieldValue("U_CardCd", .GetValue("U_CardCd", .Offset).Trim())
                    oAuditObj.setFieldValue("U_CardNM", .GetValue("U_CardNM", .Offset).Trim())
                    oAuditObj.setFieldValue("U_CusFrnNm", .GetValue("U_CusFrnNm", .Offset).Trim())

                    oAuditObj.setFieldValue("U_Contact", .GetValue("U_Contact", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Address", .GetValue("U_Address", .Offset).Trim())
                    oAuditObj.setFieldValue("U_AddrssLN", .GetValue("U_AddrssLN", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Street", .GetValue("U_Street", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Block", .GetValue("U_Block", .Offset).Trim())
                    oAuditObj.setFieldValue("U_City", .GetValue("U_City", .Offset).Trim())
                    oAuditObj.setFieldValue("U_County", .GetValue("U_County", .Offset).Trim())
                    oAuditObj.setFieldValue("U_State", .GetValue("U_State", .Offset).Trim())
                    oAuditObj.setFieldValue("U_ZpCd", .GetValue("U_ZpCd", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Phone1", .GetValue("U_Phone1", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SiteTl", .GetValue("U_SiteTl", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SteId", .GetValue("U_SteId", .Offset).Trim())
                    oAuditObj.setFieldValue("U_CustRef", .GetValue("U_CustRef", .Offset).Trim())

                    oAuditObj.setFieldValue("U_PremCd", .GetValue("U_PremCd", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SiteLic", .GetValue("U_SiteLic", .Offset).Trim())

                    oAuditObj.setFieldValue("U_Route", .GetValue("U_Route", .Offset).Trim())
                    Dim sSeq As String = .GetValue("U_Seq", .Offset).Trim()
                    Dim iSeq As Integer = 0
                    If sSeq.Length > 0 Then
                        Try
                            iSeq = ToInt(sSeq)
                        Catch ex As Exception
                        End Try
                    End If
                    oAuditObj.setFieldValue("U_Seq", iSeq)

                    '**PRODUCER
                    oAuditObj.setFieldValue("U_PCardCd", .GetValue("U_PCardCd", .Offset).Trim())
                    oAuditObj.setFieldValue("U_PCardNM", .GetValue("U_PCardNM", .Offset).Trim())
                    oAuditObj.setFieldValue("U_PContact", .GetValue("U_PContact", .Offset).Trim())
                    oAuditObj.setFieldValue("U_PAddress", .GetValue("U_PAddress", .Offset).Trim())
                    oAuditObj.setFieldValue("U_PAddrsLN", .GetValue("U_PAddrsLN", .Offset).Trim())
                    oAuditObj.setFieldValue("U_PStreet", .GetValue("U_PStreet", .Offset).Trim())
                    oAuditObj.setFieldValue("U_PBlock", .GetValue("U_PBlock", .Offset).Trim())
                    oAuditObj.setFieldValue("U_PCity", .GetValue("U_PCity", .Offset).Trim())
                    oAuditObj.setFieldValue("U_PState", .GetValue("U_PState", .Offset).Trim())
                    oAuditObj.setFieldValue("U_PZpCd", .GetValue("U_PZpCd", .Offset).Trim())
                    oAuditObj.setFieldValue("U_PPhone1", .GetValue("U_PPhone1", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Origin", .GetValue("U_Origin", .Offset).Trim())
                    oAuditObj.setFieldValue("U_ProRef", .GetValue("U_ProRef", .Offset).Trim())

                    '**SITE
                    oAuditObj.setFieldValue("U_SCardCd", .GetValue("U_SCardCd", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SCardNM", .GetValue("U_SCardNM", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SContact", .GetValue("U_SContact", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SAddress", .GetValue("U_SAddress", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SAddrsLN", .GetValue("U_SAddrsLN", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SStreet", .GetValue("U_SStreet", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SBlock", .GetValue("U_SBlock", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SCity", .GetValue("U_SCity", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SState", .GetValue("U_SState", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SZpCd", .GetValue("U_SZpCd", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SPhone1", .GetValue("U_SPhone1", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SiteRef", .GetValue("U_SiteRef", .Offset).Trim())

                    '**CARRIER
                    oAuditObj.setFieldValue("U_CCardCd", .GetValue("U_CCardCd", .Offset).Trim())
                    oAuditObj.setFieldValue("U_CCardNM", .GetValue("U_CCardNM", .Offset).Trim())
                    oAuditObj.setFieldValue("U_CContact", .GetValue("U_CContact", .Offset).Trim())
                    oAuditObj.setFieldValue("U_CAddress", .GetValue("U_CAddress", .Offset).Trim())
                    oAuditObj.setFieldValue("U_CAddrsLN", .GetValue("U_CAddrsLN", .Offset).Trim())
                    oAuditObj.setFieldValue("U_CStreet", .GetValue("U_CStreet", .Offset).Trim())
                    oAuditObj.setFieldValue("U_CBlock", .GetValue("U_CBlock", .Offset).Trim())
                    oAuditObj.setFieldValue("U_CCity", .GetValue("U_CCity", .Offset).Trim())
                    oAuditObj.setFieldValue("U_CState", .GetValue("U_CState", .Offset).Trim())
                    oAuditObj.setFieldValue("U_CZpCd", .GetValue("U_CZpCd", .Offset).Trim())
                    oAuditObj.setFieldValue("U_CPhone1", .GetValue("U_CPhone1", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SupRef", .GetValue("U_SupRef", .Offset).Trim())

                    'oAuditObj.setFieldValue("U_CustRef", .GetValue("U_CustRef", .Offset).Trim())
                    oAuditObj.setFieldValue("U_ItemGrp", .GetValue("U_ItemGrp", .Offset).Trim())
                    oAuditObj.setFieldValue("U_Qty", .GetValue("U_Qty", .Offset).Trim())

                    oAuditObj.setFieldValue("U_SLicSp", .GetValue("U_SLicSp", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SLicNm", .GetValue("U_SLicNm", .Offset).Trim())
                    oAuditObj.setFieldValue("U_ORoad", .GetValue("U_ORoad", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SLicNr", .GetValue("U_SLicNr", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SLicExp", com.idh.utils.Dates.doStrToDate(.GetValue("U_SLicExp", .Offset).Trim()))
                    oAuditObj.setFieldValue("U_SLicCh", .GetValue("U_SLicCh", .Offset).Trim())
                    oAuditObj.setFieldValue("U_SpInst", .GetValue("U_SpInst", .Offset).Trim())

                    oAuditObj.setFieldValue("U_RTIMEF", goParent.doSBOStrToTimeStr(.GetValue("U_RTIMEF", .Offset).Trim()))
                    oAuditObj.setFieldValue("U_RTIMET", goParent.doSBOStrToTimeStr(.GetValue("U_RTIMET", .Offset).Trim()))

                    oAuditObj.setFieldValue(IDH_JOBENTR._ForeCS, .GetValue(IDH_JOBENTR._ForeCS, .Offset).Trim())
                    oAuditObj.setFieldValue(IDH_JOBENTR._LckPrc, .GetValue(IDH_JOBENTR._LckPrc, .Offset).Trim())
                    ''##MA Start 16-10-2014
                    'oAuditObj.setFieldValue(IDH_JOBENTR._UseBPWgt, .GetValue(IDH_JOBENTR._UseBPWgt, .Offset).Trim())
                    ''##MA End 16-10-2014

                    oAuditObj.setFieldValue(IDH_JOBENTR._TRNCd, .GetValue(IDH_JOBENTR._TRNCd, .Offset).Trim())

                    doUpdateHeaderMore(oForm, oAuditObj, bIsAdd)

                    iwResult = oAuditObj.Commit()

                    If iwResult <> 0 Then
                        goParent.goDICompany.GetLastError(iwResult, swResult)
                        'com.idh.bridge.DataHandler.INSTANCE.doError("System: Error Updating the Header - " + swResult, "Error Updating the Header.")
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Updating the Header - " + swResult, "ERSYUHDR", {Nothing})
                        Return False
                    End If
                End With
                Return True
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Updating the Header.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXUHDR", {Nothing})
                Return False
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj)
            End Try
        End Function

        '**
        ' This will be called from the UpdateHeader Function enabling the inherited classes to add or change values before it gets processed
        '**
        Protected Overridable Sub doUpdateHeaderMore(ByVal oForm As SAPbouiCOM.Form, ByVal oAuditObj As IDHAddOns.idh.data.AuditObject, ByVal bIsAdd As Boolean)
        End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType.Equals("IDH_COMMFRM") Then
                    Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                    oGridN.doCheckAndDeleteSelection(getSharedData(oForm, "CMMNT"))
                ElseIf sModalFormType = "IDHEAORSR" Then
                    setOrigin(oForm, getSharedData(oForm, "IDH_ORCODE"))
                    doSetUpdate(oForm)
                ElseIf sModalFormType = "IDHASRCH" Or sModalFormType = "IDHASRCH3" Then
                    Dim oArr As ArrayList = New ArrayList
                    oArr.Add(getSharedData(oForm, "ADDRESS")) '0
                    oArr.Add(getSharedData(oForm, "STREET")) '1
                    oArr.Add(getSharedData(oForm, "BLOCK")) '2
                    oArr.Add(getSharedData(oForm, "ZIPCODE")) '3
                    oArr.Add(getSharedData(oForm, "CITY")) '4
                    oArr.Add(getSharedData(oForm, "COUNTRY")) '5
                    oArr.Add(getSharedData(oForm, "ROUTE")) '6
                    oArr.Add(getSharedData(oForm, "ISBACCDF")) '7
                    oArr.Add(getSharedData(oForm, "ISBACCTF")) '8
                    oArr.Add(getSharedData(oForm, "ISBACCTT")) '9
                    oArr.Add(getSharedData(oForm, "ISBACCRA")) '10
                    oArr.Add(getSharedData(oForm, "ISBACCRV")) '11
                    oArr.Add(getSharedData(oForm, "ISBCUSTRN")) '12
                    oArr.Add(getSharedData(oForm, "TEL1")) '13
                    oArr.Add(getSharedData(oForm, "ROUTE")) '14
                    oArr.Add(getSharedData(oForm, "SEQ")) '15
                    oArr.Add(getSharedData(oForm, "CONA")) '16 - Contact Person from Address
                    oArr.Add(getSharedData(oForm, "COUNTY")) '17
                    oArr.Add(getSharedData(oForm, "PRECD")) '18
                    oArr.Add(getSharedData(oForm, "ORIGIN")) '19
                    oArr.Add(getSharedData(oForm, "SITELIC")) '20
                    oArr.Add(getSharedData(oForm, "STEID")) '21

                    oArr.Add(getSharedData(oForm, "FAX")) '22
                    oArr.Add(getSharedData(oForm, "EMAIL")) '23
                    oArr.Add(getSharedData(oForm, "STRNO")) '24
                    oArr.Add(getSharedData(oForm, "")) '25 - No sales contact in Search Forms
                    oArr.Add(getSharedData(oForm, "PHONE1")) '26
                    oArr.Add(getSharedData(oForm, "MCONA")) '27 - Main Contact Person from BP
                    oArr.Add(getSharedData(oForm, "STATENM")) '28
                    ''##MA Start 12-09-2014 Issue#413
                    oArr.Add(getSharedData(oForm, "MINJOB")) '29
                    ''##MA End 12-09-2014 Issue#413
                    oArr.Add(getSharedData(oForm, "LINENUM")) '30
                    Dim sTrg As String = getSharedData(oForm, "TRG")
                    If sTrg = "IDH_CUSAL" Then
                        Dim sCustomer As String = getFormDFValue(oForm, "U_CardCd")
                        Dim sPh1 As String = getFormDFValue(oForm, "U_Phone1")
                        doSetCustomerAddress(oForm, oArr, sCustomer, sPh1)
                        '## Start 21-06-2013
                        'oArr dont have all the data as we get from Address(or silent Address) Search
                        If oForm.TypeEx = "IDH_WASTORD" Then 'For only Waste Order
                            Dim PhoneOption As String = Config.Parameter("WOPHSRC")
                            If PhoneOption.Length = 4 Then 'Invalid Value for switch
                                If PhoneOption.Substring(3, 1) = "1" Then 'Pick Contact Person for General
                                    setFormDFValue(oForm, "U_Contact", getSharedData(oForm, "MCONA"))
                                Else
                                    setFormDFValue(oForm, "U_Contact", getSharedData(oForm, "CONA"))
                                End If
                            Else
                                setFormDFValue(oForm, "U_Contact", getSharedData(oForm, "CONA"))
                            End If
                        Else
                            setFormDFValue(oForm, "U_Contact", getSharedData(oForm, "CONA"))
                        End If
                        '##End
                        '## Start 16-07-2013
                    ElseIf sTrg = "IDH_PRDADD" Then 'AndAlso oForm.TypeEx = "IDH_DISPORD" Then
                        doSetProducerAddress(oForm, oArr, True, False)
                    ElseIf (sTrg = "IDH_ADDRES" OrElse sTrg = "IDH_WASAL") Then 'AndAlso oForm.TypeEx = "IDH_DISPORD" Then
                        'Dim sCustomer As String = getFormDFValue(oForm, "U_CardCd")
                        ' Dim sPh1 As String = getFormDFValue(oForm, "U_Phone1")
                        doSetCarrierAddress(oForm, oArr)
                        '## Start 21-06-2013
                        'oArr dont have all the data as we get from Address(or silent Address) Search
                        'If oForm.TypeEx = "IDH_DISPORD" Then 'For only Waste Order
                        '    'Dim PhoneOption As String = Config.Parameter("WOPHSRC")
                        '    'If PhoneOption.Length = 4 Then 'Invalid Value for switch
                        '    'If PhoneOption.Substring(3, 1) = "1" Then 'Pick Contact Person for General
                        '    'setFormDFValue(oForm, "U_Contact", getSharedData(oForm, "MCONA"))
                        '    'Else
                        '    setFormDFValue(oForm, "U_Contact", getSharedData(oForm, "CONA"))
                        '    'End If
                        'Else
                        'setFormDFValue(oForm, "U_Contact", getSharedData(oForm, "CONA"))
                        'End If
                        'Else
                        'setFormDFValue(oForm, "U_Contact", getSharedData(oForm, "CONA"))
                        'End If
                    ElseIf (sTrg = "IDH_CUSADD" OrElse sTrg = "IDH_CUSA2" OrElse sTrg = "IDH_CUSAL") Then 'AndAlso oForm.TypeEx = "IDH_DISPORD" Then
                        Dim sCustomer As String = getFormDFValue(oForm, "U_CardCd")
                        Dim sPh1 As String = getFormDFValue(oForm, "U_Phone1")
                        doSetCustomerAddress(oForm, oArr, sCustomer, sPh1, False)
                        '## End
                    ElseIf sTrg = "IDH_WASAL" Then
                        '## Start12-06-2013; Add option to pick phone from general tab or from Address tab(site address)
                        doSetCarrierAddress(oForm, oArr)
                        Dim Phone1 As String = (getSharedData(oForm, "PHONE1"))
                        If oForm.TypeEx = "IDH_WASTORD" Then 'For only Waste Order
                            Dim PhoneOption As String = Config.Parameter("WOPHSRC")
                            If PhoneOption.Length = 4 Then 'Invalid Value for switch
                                If PhoneOption.Substring(0, 1) = "1" Then 'Pick Phone for General;Carrier
                                    setFormDFValue(oForm, "U_CPhone1", Phone1)
                                    setFormDFValue(oForm, "U_CContact", getSharedData(oForm, "MCONA"))
                                Else
                                    setFormDFValue(oForm, "U_CPhone1", oArr(13))
                                    setFormDFValue(oForm, "U_CContact", getSharedData(oForm, "CONA"))
                                End If
                            Else
                                setFormDFValue(oForm, "U_CPhone1", Phone1)
                                setFormDFValue(oForm, "U_CContact", getSharedData(oForm, "MCONA"))
                            End If
                        Else
                            setFormDFValue(oForm, "U_CContact", oArr.Item(16))
                            setFormDFValue(oForm, "U_CPhone1", Phone1)
                        End If
                        '## End
                    ElseIf sTrg = "IDH_PROAL" Then
                        doSetProducerAddress(oForm, oArr, True)
                        '## Start12-06-2013; Add option to pick phone from general tab or from Address tab(site address)
                        If oForm.TypeEx = "IDH_WASTORD" Then 'For only Waste Order
                            Dim PhoneOption As String = Config.Parameter("WOPHSRC")
                            If PhoneOption.Length = 4 Then 'Invalid Value for switch
                                If PhoneOption.Substring(1, 1) = "1" Then 'Pick Phone for General;Producer
                                    setFormDFValue(oForm, "U_PContact", getSharedData(oForm, "MCONA"))
                                    setFormDFValue(oForm, "U_PPhone1", getSharedData(oForm, "PHONE1"))
                                Else
                                    setFormDFValue(oForm, "U_PContact", getSharedData(oForm, "CONA"))
                                    setFormDFValue(oForm, "U_PPhone1", oArr(13))
                                End If
                            Else
                                setFormDFValue(oForm, "U_PContact", getSharedData(oForm, "MCONA"))
                                setFormDFValue(oForm, "U_PPhone1", getSharedData(oForm, "PHONE1"))
                            End If
                        End If
                        '## End
                    ElseIf sTrg = "IDH_SITAL" OrElse sTrg = "IDH_SITAL2" OrElse sTrg = "IDH_SITADD" OrElse sTrg = "IDH_DISPAD" Then
                        doSetSiteAddress(oForm, oArr)
                        '## Start 13-06-2013
                        If oForm.TypeEx = "IDH_WASTORD" Then 'For only Waste Order
                            Dim PhoneOption As String = Config.Parameter("WOPHSRC")
                            If PhoneOption.Length = 4 Then 'Invalid Value for switch
                                If PhoneOption.Substring(2, 1) = "1" Then 'Pick Phone for General;Site
                                    setFormDFValue(oForm, "U_SContact", getSharedData(oForm, "MCONA"))
                                    setFormDFValue(oForm, "U_SPhone1", getSharedData(oForm, "PHONE1"))
                                Else
                                    setFormDFValue(oForm, "U_SContact", getSharedData(oForm, "CONA"))
                                    setFormDFValue(oForm, "U_SPhone1", oArr(13))
                                End If
                            Else
                                setFormDFValue(oForm, "U_SContact", getSharedData(oForm, "MCONA"))
                                setFormDFValue(oForm, "U_SPhone1", getSharedData(oForm, "PHONE1"))
                            End If
                        End If
                        '## End
                    End If
                    doSetUpdate(oForm)
                ElseIf sModalFormType = "IDHCSRCH" Then
                    Dim sTarget As String = getSharedData(oForm, "TRG")
                    Dim val As String = getSharedData(oForm, "CARDCODE")
                    If Not (val Is Nothing) AndAlso val.Length > 0 Then
                        If sTarget = "WC" Then
                            doSetChoosenCarrier(oForm)
                            doSetUpdate(oForm)
                        ElseIf sTarget = "ST" Then
                            doSetChoosenSite(oForm)
                            doSetUpdate(oForm)
                        ElseIf sTarget = "PR" Then
                            doSetChoosenProducer(oForm)
                            doSetUpdate(oForm)
                        ElseIf sTarget = "LC" Then
                            doSetChoosenLic(oForm)
                            doSetUpdate(oForm)
                        ElseIf sTarget = "CU" Then
                            If doSetChoosenCustomerFromShared(oForm) = True Then
                                doSetUpdate(oForm)
                            Else
                                setFormDFValue(oForm, "U_CardCd", "", True)
                                setFormDFValue(oForm, "U_CardNM", "", True)
                                setFormDFValue(oForm, "U_CusFrnNm", "", True)
                            End If
                            If getParentSharedData(oForm, "CALLEDITEM") IsNot Nothing AndAlso getParentSharedData(oForm, "CALLEDITEM").Trim <> "" Then
                                doSetFocus(oForm, getParentSharedData(oForm, "CALLEDITEM"))
                            End If
                        End If
                        'doSetUpdate(oForm)
                    End If
                ElseIf sModalFormType = "IDHNCSRCH" Then 'New BP Search Form
                    '## Start 17-06-2013; New Search Form
                    Dim sTarget As String = getSharedData(oForm, "TRG")
                    Dim val As String = getSharedData(oForm, "CARDCODE")

                    If Not (val Is Nothing) AndAlso val.Length > 0 Then
                        If sTarget = "WC" Then
                            doSetChoosenCarrier(oForm)

                            doSetUpdate(oForm)
                        ElseIf sTarget = "ST" Then
                            doSetChoosenSite(oForm)
                            doSetUpdate(oForm)
                        ElseIf sTarget = "PR" Then
                            doSetChoosenProducer(oForm)
                            doSetUpdate(oForm)
                        ElseIf sTarget = "LC" Then
                            doSetChoosenLic(oForm)
                            doSetUpdate(oForm)
                        ElseIf sTarget = "CU" Then
                            If doSetChoosenCustomerFromShared(oForm) = True Then
                                doSetUpdate(oForm)
                            Else
                                setFormDFValue(oForm, "U_CardCd", "", True)
                                setFormDFValue(oForm, "U_CardNM", "", True)
                                setFormDFValue(oForm, "U_CusFrnNm", "", True)
                            End If
                        End If
                        'doSetUpdate(oForm)
                    End If
                    '## End
                ElseIf sModalFormType = "IDHROSRC" Then
                    Dim sRoute As String = getSharedData(oForm, "IDH_RTCODE")
                    Dim iSeq As String = getSharedData(oForm, "IDH_RTSEQ")

                    setFormDFValue(oForm, "U_Route", sRoute)
                    setFormDFValue(oForm, "U_Seq", iSeq)

                    doClearSharedData(oForm)
                ElseIf sModalFormType = "IDHCCPAY" Then
                    If Config.INSTANCE.getParameterAsBool("CCONROW", False) = False Then
                        Dim oRows As ArrayList = getWFValue(oForm, "CCROWS")

                        If Not oRows Is Nothing Then
                            Dim iRow As Integer
                            Dim iIndex As Integer
                            Dim oUpdateGrid As UpdateGrid
                            Dim sCCStatPre As String
                            Dim sPStat As String
                            Dim sCCNum As String
                            Dim sCCType As String
                            Dim sCCIs As String
                            Dim sCCSec As String
                            Dim sCCHNm As String
                            Dim sCCPCd As String
                            Dim sCCExp As String

                            oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")

                            sCCNum = getSharedData(oForm, "CCNUM")
                            sPStat = getSharedData(oForm, "PSTAT")
                            sCCType = getSharedData(oForm, "CCTYPE")
                            sCCIs = getSharedData(oForm, "CCIs")
                            sCCSec = getSharedData(oForm, "CCSec")
                            sCCHNm = getSharedData(oForm, "CCHNm")
                            sCCPCd = getSharedData(oForm, "CCPCd")
                            sCCExp = getSharedData(oForm, "CCEXP")

                            If sCCNum.Length = 0 Then
                                sCCStatPre = ""
                                sPStat = com.idh.bridge.lookups.FixedValues.getStatusUnPaid()
                            Else
                                If sPStat.Equals(com.idh.bridge.lookups.FixedValues.getStatusPaid()) Then
                                    sCCStatPre = "APPROVED: "
                                Else
                                    sCCStatPre = "DECLINED: "
                                End If
                            End If

                            For iIndex = 0 To oRows.Count - 1
                                iRow = oRows.Item(iIndex)
                                oUpdateGrid.setCurrentDataRowIndex(iRow)

                                oUpdateGrid.doSetFieldValue("U_CCNum", sCCNum)
                                oUpdateGrid.doSetFieldValue("U_CCType", sCCType)
                                oUpdateGrid.doSetFieldValue("U_CCStat", sCCStatPre & getSharedData(oForm, "CCSTAT"))
                                oUpdateGrid.doSetFieldValue("U_PayStat", sPStat)
                                oUpdateGrid.doSetFieldValue("U_CCExp", sCCExp)
                                oUpdateGrid.doSetFieldValue("U_CCIs", sCCIs)
                                oUpdateGrid.doSetFieldValue("U_CCSec", sCCSec)
                                oUpdateGrid.doSetFieldValue("U_CCHNum", sCCHNm)
                                oUpdateGrid.doSetFieldValue("U_CCPCd", sCCPCd)
                            Next

                            setWFValue(oForm, "BussyWithCC", True)
                            doButtonID1Process(oForm)
                        End If
                    End If
                ElseIf sModalFormType = "IDH_DUPLICATE" Then
                    Dim sSetDup As String = getSharedData(oForm, "SETDUP")
                    If sSetDup = "TRUE" Then
                        Dim iNoOfCopies As Integer = Convert.ToInt16(getSharedData(oForm, "IDHNOCOPY"))
                        Dim bCopyADDI As Boolean = Convert.ToBoolean(getSharedData(oForm, "IDHCHKADI"))
                        Dim bCopyComm As Boolean = Convert.ToBoolean(getSharedData(oForm, "IDHCHKCOM"))
                        Dim bCopyPrice As Boolean = Convert.ToBoolean(getSharedData(oForm, "IDHCOPYPR"))
                        Dim bCopyQty As Boolean = Convert.ToBoolean(getSharedData(oForm, "IDHCOPYQT"))

                        Dim bCopyVehReg As String = Convert.ToBoolean(getSharedData(oForm, "IDHVEHREG"))
                        Dim bCopyDriver As String = Convert.ToBoolean(getSharedData(oForm, "IDHDRIVER"))
                        If iNoOfCopies > 0 And iNoOfCopies < Convert.ToInt16(Config.Parameter("MXDUPCNT")) Then
                            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
                            For cnt As Integer = 1 To iNoOfCopies
                                doDuplicateRows(oGridN, bCopyComm, bCopyADDI, bCopyPrice, bCopyQty, bCopyVehReg, bCopyDriver)
                            Next
                        Else
                            doWarnMess("No. of copies exceeds 'MXDUPCNT' WR Config Key.", SAPbouiCOM.BoMessageTime.bmt_Short)
                        End If
                    Else
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error handling the Modal results.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHMR", {Nothing})
            End Try
        End Sub

        Protected Overridable Sub doSetModalCustomer(ByVal oForm As SAPbouiCOM.Form, ByVal sCardCode As String)
            Dim val As String = sCardCode
            Dim val2 As String
            Dim val3 As String
            Dim val4 As String
            Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            Try
                If Not (val Is Nothing) AndAlso val.Length > 0 Then
                    oBP = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
                    If oBP.GetByKey(sCardCode) Then
                        val = oBP.CardCode
                        val2 = oBP.Phone1
                        val3 = oBP.ContactPerson
                        val4 = oBP.CardName

                        Dim oData As ArrayList

                        setFormDFValue(oForm, "U_CardCd", val)
                        setFormDFValue(oForm, "U_Phone1", val2)
                        setFormDFValue(oForm, "U_Contact", val3)
                        setFormDFValue(oForm, "U_CardNM", val4)
                        setFormDFValue(oForm, "U_CusFrnNm", oBP.CardForeignName, True)
                        If wasSetWithCode(oForm, "IDH_WPRODU") OrElse
                   getFormDFValue(oForm, "U_PCardCd").Trim().Length = 0 Then

                            setFormDFValue(oForm, "U_PCardCd", val)
                            setFormDFValue(oForm, "U_PCardNM", val4)
                            setFormDFValue(oForm, "U_PPhone1", val2)
                            setFormDFValue(oForm, "U_PContact", val3)
                        End If

                        oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, val, "S")
                        If doSetCustomerAddress(oForm, oData, val, val2) = False Then
                            Exit Sub
                        End If

                        If wasSetWithCode(oForm, "IDH_WPRODU") OrElse
                   getFormDFValue(oForm, "U_PCardCd").Trim().Length = 0 Then

                            setFormDFValue(oForm, "U_PCardCd", val)
                            setFormDFValue(oForm, "U_PCardNM", val4)
                            setFormDFValue(oForm, "U_PPhone1", val2)
                            setFormDFValue(oForm, "U_PContact", val3)

                            oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, val, "S")
                            doSetProducerAddress(oForm, oData, False)
                        End If
                    End If
                    doSetUpdate(oForm)
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the Modal Customer.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSMC", {Nothing})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oBP)
            End Try

        End Sub

        Protected Overridable Function doSetChoosenCustomerFromShared(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoBPRules As Boolean = True, Optional ByVal bDoLinkCustomerAndProducer As Boolean = False) As Boolean
            Dim val As String = Nothing
            Dim val2 As String
            Dim val3 As String
            Dim val4 As String
            Dim dCreditLimit As Decimal = 0
            Dim dCreditRemain As Decimal = 0
            Dim dDeviationPrc As Decimal = 0
            Dim dTBalance As Decimal = 0
            Dim sFrozen As String = "N"
            Dim sFrozenComm As String = Nothing
            Dim sIgnoreCheck As String = "N"
            Dim sObligated As String
            Dim sCustomerAddress As String = Nothing
            Dim sPaymentTermCode As String
            Dim dBalance As Decimal = 0
            Dim dWROrders As Decimal = 0

            Dim sFrozenFrom As String
            Dim sFrozenTo As String
            Dim oFrozenFrom As Date
            Dim oFrozenTo As Date

            Dim WasteLicRegNo As String = ""
            Dim CusFName As String = ""
            Try
                val = getSharedData(oForm, "CARDCODE")
                val2 = getSharedData(oForm, "PHONE1")
                val3 = getSharedData(oForm, "CNTCTPRSN")
                val4 = getSharedData(oForm, "CARDNAME")
                dCreditLimit = getSharedData(oForm, "CREDITLINE")
                dCreditRemain = getSharedData(oForm, "CRREMAIN")
                dDeviationPrc = getSharedData(oForm, "DEVIATION")
                dTBalance = getSharedData(oForm, "TBALANCE")

                sFrozen = getSharedData(oForm, "FROZEN")
                sFrozenComm = getSharedData(oForm, "FROZENC")
                sFrozenFrom = getSharedData(oForm, "FROZENF")
                sFrozenTo = getSharedData(oForm, "FROZENT")

                sIgnoreCheck = getSharedData(oForm, "IDHICL")
                sObligated = getSharedData(oForm, "IDHOBLGT")
                sPaymentTermCode = getSharedData(oForm, "PAYTRM")
                dBalance = getSharedData(oForm, "BALANCE")
                dWROrders = getSharedData(oForm, "WRORDERS")
                WasteLicRegNo = getSharedData(oForm, "WASLIC")
                CusFName = getSharedData(oForm, "CARDFNAME")
                oFrozenFrom = com.idh.utils.Dates.doStrToDate(sFrozenFrom)
                oFrozenTo = com.idh.utils.Dates.doStrToDate(sFrozenTo)

                'oFrozenFrom = com.idh.utils.dates.doStrToDate(sFrozenFrom)
                'oFrozenTo = com.idh.utils.dates.doStrToDate(sFrozenTo)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the choosen Customer - " & val)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSCC", {val})
                Return False
            End Try

            If Not (val Is Nothing) AndAlso val.Length > 0 Then
                'CR 20150601: The Credit Check build for USA Clients will now be used for all under new WR Config Key "NEWCRCHK" 
                'If Config.INSTANCE.getParameterAsBool("USAREL", False) Then
                If Config.INSTANCE.getParameterAsBool("NEWCRCHK", False) Then
                    'OnTime [#Ico000????] USA 
                    'START
                    'USA Release 
                    'Company level Credit Check 

                    '*** NOTE: This Credit Check triggers when the Customer is selected 
                    Dim sHeaderMsg As String = String.Empty

                    'See if the WOH status is not 'Quote' 
                    'For a Quote WOH there is NO Credit Check 
                    Dim sStatus As String = getItemValue(oForm, "IDH_STATUS")

                    If Config.INSTANCE.getParameterAsBool("MDCRDO", False) AndAlso Not sStatus.Equals(Config.ParameterWithDefault("MDQUOTCD", "8")) Then
                        'BP exempt from credit check 
                        If sIgnoreCheck <> "Y" Then
                            'BP is Cash Customer or Other 
                            If Config.ParameterWithDefault("MDCPTC", "0") = sPaymentTermCode Then
                                goParent.doMessage("Customer is CIA")
                                'cash customer
                                If Config.INSTANCE.doCheckCreditCashCustomer(val, dTBalance, dWROrders) = False Then
                                    'Set Order to DRAFT
                                    setFormDFValueFromUser(oForm, "U_Status", Config.ParameterWithDefault("MDDRFTCD", "6"), True)
                                    sHeaderMsg = "Over Credit Limit"
                                    doSendCreditCheckAlertAndMessage(oForm, val, sHeaderMsg, "CCCASHAL", "CCCALERT")
                                End If
                            Else
                                'account customer 
                                Dim bPayTermLimitPass As Boolean = False
                                Dim bCreditCheckPass As Boolean = False
                                'Dim sMessage As String
                                Dim bDoAlert As Boolean = True
                                Dim bDoDraft As Boolean = False
                                Dim sMessageType As String = Nothing

                                'THIS IS NOT USED ANYMORE As THE SEARCH SHOULD FILTER INACTIVE BP's OUT - LEAVING IT IN JUST FOR NOW
                                'If com.idh.bridge.lookups.Config.INSTANCE.doCheckBPActive(Nothing, Nothing, Nothing, sFrozen, sFrozenComm, oFrozenFrom, oFrozenTo, DateTime.Now(), True) = True Then
                                If Config.INSTANCE.doCheckBPActive(val, DateTime.Now) Then 'com.idh.bridge.lookups.Config.INSTANCE.doCheckBPActive(Nothing, Nothing, Nothing, sFrozen, sFrozenComm, oFrozenFrom, oFrozenTo, DateTime.Now(), True) = True Then
                                    'if BP is not frozen 
                                    If Config.INSTANCE.getParameterAsBool("DPTLCHK", False) Then 'DoPaymentTermLimitCheck
                                        Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                                        Dim sQry As String = ""
                                        sQry = sQry & "SELECT DocDueDate + " + Config.ParameterWithDefault("PTGRCPRD", "0") + " AS BufferDocDueDate, "
                                        sQry = sQry & " DocEntry, "
                                        sQry = sQry & " CardCode "
                                        sQry = sQry & "FROM   OINV "
                                        sQry = sQry & "WHERE  CardCode = '" + val + "' "
                                        sQry = sQry & "       AND DocDueDate + " + Config.ParameterWithDefault("PTGRCPRD", "0") + " <= GetDate() "
                                        sQry = sQry & "       AND DocStatus = 'O' "

                                        oRecordSet = goParent.goDB.doSelectQuery(sQry)
                                        If oRecordSet.RecordCount > 0 Then
                                            bPayTermLimitPass = False
                                        Else
                                            bPayTermLimitPass = True
                                        End If
                                        IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
                                    Else
                                        bPayTermLimitPass = True
                                    End If

                                    If bPayTermLimitPass Then
                                        If Config.INSTANCE.getParameterAsBool("DCRLCHK", False) Then 'DoCreditLimitCheck
                                            Config.INSTANCE.doCheckCreditAdvance(
                                                val, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc,
                                                0, bDoAlert, False, Nothing, bDoDraft, sMessageType)

                                            If bDoDraft Then
                                                setFormDFValueFromUser(oForm, "U_Status", Config.ParameterWithDefault("MDDRFTCD", "6"), True)
                                            End If

                                            If sMessageType = "NEARING" Then
                                                sHeaderMsg = "Customer NEARING Credit Limit"
                                            ElseIf sMessageType = "OVER" Then
                                                sHeaderMsg = "Customer OVER Credit Limit"
                                            ElseIf sMessageType = "NONE" Then
                                                sHeaderMsg = String.Empty
                                            End If

                                            If sHeaderMsg.Length > 0 Then
                                                doSendCreditCheckAlertAndMessage(oForm, val, sHeaderMsg, "MDCDAL", "CCHKAMSG")
                                            End If
                                        End If

                                    Else
                                        setFormDFValueFromUser(oForm, "U_Status", Config.ParameterWithDefault("MDDRFTCD", "6"), True)
                                        sHeaderMsg = "Over Credit Terms"
                                        doSendCreditCheckAlertAndMessage(oForm, val, sHeaderMsg, "MDCDAL", "CCHKAMSG")
                                    End If
                                Else
                                    'As BP is frozen 
                                    Return False
                                End If
                            End If
                        End If
                    End If
                    'END
                    'OnTime [#Ico000????] USA 
                Else
                    'Non-USA Release 
                    If Not sIgnoreCheck = "Y" Then
                        Dim bBlockOnFail As Boolean = Config.INSTANCE.getParameterAsBool(msOrderType & "BCCL", True)
                        'If Config.INSTANCE.doCheckCredit2(goParent, val, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc, 0, sFrozen, sFrozenComm, False, bBlockOnFail, Nothing, oFrozenFrom, oFrozenTo) = False Then
                        '    Return False
                        'End If
                        If Config.INSTANCE.doCheckCredit2(val, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc, 0, False, bBlockOnFail, Nothing) = False Then
                            Return False
                        End If

                    End If
                End If

                Dim oData As ArrayList
                setFormDFValueFromUser(oForm, "U_CardCd", val, True)
                setFormDFValueFromUser(oForm, "U_CardNM", val4, True)

                setFormDFValue(oForm, "U_Phone1", val2)

                setFormDFValue(oForm, "U_Contact", val3)
                '## Start 24-09-2013
                setFormDFValue(oForm, "U_WasLic", WasteLicRegNo)
                '## End
                setFormDFValue(oForm, "U_CusFrnNm", CusFName)
                setEnableItem(oForm, True, "IDH_CUSADD")
                setEnableItem(oForm, True, "IDH_CUSA2")

                Dim bDoProducerAddress As Boolean = True

                oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, val, "S")
                '##MA Start 18-09-2014 Issue#403
                'If Not oData Is Nothing AndAlso oData.Count > 0 AndAlso Config.INSTANCE.getParameterAsBool("FLTRADDR", False) = True Then
                FilterRestrictedAddress(oData, val)
                'End If
                '##MA End 18-09-2014 Issue#403
                doSetCustomerAddress(oForm, oData, val, val2, bDoProducerAddress)
                If Not oData Is Nothing AndAlso oData.Count > 0 Then
                    sCustomerAddress = oData.Item(0)
                End If

                'OnTime [#Ico000????] USA 
                'START
                'Set Mailling Address 
                If val IsNot Nothing AndAlso val.Length > 0 Then
                    Dim oRs As SAPbobsCOM.Recordset
                    oRs = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    Dim sMAQry As String = "select U_IDHACN, Address, Street, Block, City, (SELECT NAME FROM OCST WHERE CODE = CRD1.STATE AND COUNTRY = CRD1.COUNTRY) As State, ZipCode, U_Tel1 From CRD1 where CardCode = '" & val & "' AND U_MailAdd = 'Y' "
                    oRs.DoQuery(sMAQry)
                    If oRs.RecordCount > 0 Then
                        'Set mail address fields 
                        setFormDFValue(oForm, "U_MContact", oRs.Fields.Item("U_IDHACN").Value.Trim())
                        setFormDFValue(oForm, "U_MAddress", oRs.Fields.Item("Address").Value.Trim())
                        setFormDFValue(oForm, "U_MStreet", oRs.Fields.Item("Street").Value.Trim())
                        setFormDFValue(oForm, "U_MBlock", oRs.Fields.Item("Block").Value.Trim())
                        setFormDFValue(oForm, "U_MCity", oRs.Fields.Item("City").Value.Trim())
                        setFormDFValue(oForm, "U_MState", oRs.Fields.Item("State").Value.Trim())
                        setFormDFValue(oForm, "U_MZpCd", oRs.Fields.Item("ZipCode").Value.Trim())
                        setFormDFValue(oForm, "U_MPhone1", oRs.Fields.Item("U_Tel1").Value.Trim())
                    Else
                        setFormDFValue(oForm, "U_MContact", "")
                        setFormDFValue(oForm, "U_MAddress", "")
                        setFormDFValue(oForm, "U_MStreet", "")
                        setFormDFValue(oForm, "U_MBlock", "")
                        setFormDFValue(oForm, "U_MCity", "")
                        setFormDFValue(oForm, "U_MState", "")
                        setFormDFValue(oForm, "U_MZpCd", "")
                        setFormDFValue(oForm, "U_MPhone1", "")
                    End If
                    oRs = Nothing
                End If
                'END
                'OnTime [#Ico000????] USA 

                'If the Producer was not set by the user change it relative to the customer
                Dim sProdCd As String = getFormDFValue(oForm, "U_PCardCd").Trim()
                If sProdCd.Length = 0 OrElse (bDoBPRules AndAlso wasSetWithCode(oForm, "IDH_WPRODU")) Then
                    Dim sCardCode As String

                    If bDoLinkCustomerAndProducer Then
                        Dim oLinkedBP As LinkedBP = Config.INSTANCE.doGetLinkedBP(val)

                        If Not oLinkedBP Is Nothing Then
                            setFormDFValue(oForm, "U_PCardCd", oLinkedBP.LinkedBPCardCode)
                            setFormDFValue(oForm, "U_PCardNM", oLinkedBP.LinkedBPName)
                            setFormDFValue(oForm, "U_PPhone1", oLinkedBP.Phone1)
                            setFormDFValue(oForm, "U_PContact", oLinkedBP.ContactPerson)

                            Dim oProdData As ArrayList = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, oLinkedBP.LinkedBPCardCode, "S", sCustomerAddress)
                            '##MA Start 18-09-2014 Issue#403
                            'If Not oProdData Is Nothing AndAlso oProdData.Count > 0 AndAlso Config.INSTANCE.getParameterAsBool("FLTRADDR", False) = True Then
                            FilterRestrictedAddress(oProdData, oLinkedBP.LinkedBPCardCode)
                            'End If
                            '##MA End 18-09-2014 Issue#403
                            If oProdData Is Nothing OrElse oProdData.Count = 0 Then
                                If Config.INSTANCE.doGetDoBuyingAndTrade() Then
                                    If doAddProducerShipToAddressFromCustomer(oForm, oLinkedBP.LinkedBPCardCode) Then
                                        If sProdCd.Equals(oLinkedBP.LinkedBPCardCode) Then
                                            doSetProducerAddressFromCustomer(oForm)
                                            '## Start 06-09-2013
                                        Else
                                            oProdData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, oLinkedBP.LinkedBPCardCode, "S", sCustomerAddress)
                                            doSetProducerAddress(oForm, oProdData, False)
                                        End If
                                        '## End
                                    End If
                                End If
                            Else
                                doSetProducerAddress(oForm, oProdData, False)
                                '## Start12-06-2013; Add option to pick phone from general tab or from Address tab(site address)
                                If oForm.TypeEx = "IDH_WASTORD" Then 'For only Waste Order
                                    Dim PhoneOption As String = Config.Parameter("WOPHSRC")
                                    If PhoneOption.Length = 4 Then 'Invalid Value for switch
                                        If PhoneOption.Substring(1, 1) = "1" Then 'Pick Phone for General;Producer
                                            setFormDFValue(oForm, "U_PPhone1", oProdData(26))
                                            setFormDFValue(oForm, "U_PContact", oProdData(27))
                                        Else
                                            setFormDFValue(oForm, "U_PPhone1", oProdData(13))
                                            setFormDFValue(oForm, "U_PContact", oProdData(16))
                                        End If
                                    Else
                                        setFormDFValue(oForm, "U_PContact", oProdData(27))
                                        setFormDFValue(oForm, "U_PPhone1", oProdData(26))
                                    End If
                                End If
                                '## End
                            End If
                            bDoProducerAddress = False
                        End If
                    Else
                        sCardCode = val

                        setFormDFValue(oForm, "U_PCardCd", val)
                        setFormDFValue(oForm, "U_PCardNM", val4)
                        setFormDFValue(oForm, "U_PPhone1", val2)
                        setFormDFValue(oForm, "U_PContact", val3)

                        Dim oProdData As ArrayList = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sCardCode, "S")
                        '##MA Start 18-09-2014 Issue#403
                        'If Not oProdData Is Nothing AndAlso oProdData.Count > 0 AndAlso Config.INSTANCE.getParameterAsBool("FLTRADDR", False) = True Then
                        FilterRestrictedAddress(oProdData, sCardCode)
                        'End If
                        '##MA End 18-09-2014 Issue#403
                        doSetProducerAddress(oForm, oProdData, False)
                    End If

                End If

                '                If doSetCustomerAddress(oForm, oData, val, val2, bDoProducerAddress) = False Then
                '                	doSetUpdate(oForm)
                '                    Return False
                '                End If
                If getFormDFValue(oForm, "U_PCardCd").Trim() <> "" Then
                    setEnableItem(oForm, True, "IDH_PRDADD")
                End If
                doSetUpdate(oForm)

                Dim sFirstBP As String = getFormDFValue(oForm, "U_FirstBP")
                If sFirstBP.Equals("None") Then
                    setFormDFValue(oForm, "U_FirstBP", "Customer")
                End If
                Return True
            End If
            Return False
        End Function

        '##MA Start 18-09-2014 Issue#403
        Protected Sub FilterRestrictedAddress(ByRef oData As ArrayList, ByVal sCardCode As String)
            If oData IsNot Nothing AndAlso oData.Count > 0 AndAlso Config.INSTANCE.getParameterAsBool("FLTRADDR", False) = True Then
                Dim oRecordSet As SAPbobsCOM.Recordset
                Dim sQry As String = "Select adrs.U_Address from [@IDH_ADDRST] adrs Where (adrs.U_CustCode='" & sCardCode & "' or adrs.U_CustCode in(Select OCRD.CardCode from OCRD WITH(NOLOCK) where OCRD.U_IDHLBPC='" & sCardCode & "') )" _
                                     & " And adrs.U_Address='" & oData.Item(0) & "' " _
                                     & " And adrs.U_AddType='S' And adrs.U_UserName='" & PARENT.gsUserName & "'"
                oRecordSet = goParent.goDB.doSelectQuery(sQry)
                If oRecordSet IsNot Nothing AndAlso oRecordSet.RecordCount > 0 Then
                    oData = Nothing ' means selected address of BP is default and restricted to user so don’t select any record and user will select alternate address
                End If
            End If
        End Sub
        '##MA End 18-09-2014 Issue#403
        Protected Overridable Sub doSetChoosenCarrier(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoBPRules As Boolean = True)
            Dim oData As ArrayList
            Dim Val As String = getSharedData(oForm, "CARDCODE")

            setFormDFValueFromUser(oForm, "U_CCardCd", Val, True)
            setFormDFValueFromUser(oForm, "U_CCardNM", getSharedData(oForm, "CARDNAME"), True)
            '## Start 26-09-2013
            setFormDFValue(oForm, "U_CWasLic", getSharedData(oForm, "WASLIC"), True)
            '## End
            setEnableItem(oForm, True, "IDH_ADDRES")
            'setFormDFValue(oForm, "U_CCardCd", Val)
            'setFormDFValue(oForm, "U_CCardNM", getSharedData(oForm, "CARDNAME"))
            'setFormDFValue(oForm, "U_CPhone1", getSharedData(oForm, "PHONE1"))
            setFormDFValue(oForm, "U_CContact", getSharedData(oForm, "CNTCTPRSN"))
            oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, Val, "S")
            doSetCarrierAddress(oForm, oData)
            If oData IsNot Nothing Then
                '## Start12-06-2013; Add option to pick phone from general tab or from Address tab(site address)
                If oForm.TypeEx = "IDH_WASTORD" Then 'For only Waste Order
                    Dim PhoneOption As String = Config.Parameter("WOPHSRC")
                    If PhoneOption.Length = 4 Then 'Invalid Value for switch
                        If PhoneOption.Substring(0, 1) = "1" Then 'Pick Phone for General;Carrier
                            setFormDFValue(oForm, "U_CContact", oData(27))
                            setFormDFValue(oForm, "U_CPhone1", oData(26))
                        Else
                            setFormDFValue(oForm, "U_CContact", oData(16))
                            setFormDFValue(oForm, "U_CPhone1", oData(13))
                        End If
                    Else
                        setFormDFValue(oForm, "U_CContact", oData(27))
                        setFormDFValue(oForm, "U_CPhone1", oData(26))
                    End If
                End If
            End If
            '## End
            setUFValue(oForm, "IDH_LICREG", getSharedData(oForm, "WASLIC"))
        End Sub

        Protected Overridable Sub doSetChoosenSite(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoBPRules As Boolean = True)
            Dim oData As ArrayList
            Dim Val As String = getSharedData(oForm, "CARDCODE")

            setFormDFValueFromUser(oForm, "U_SCardCd", Val)
            setFormDFValueFromUser(oForm, "U_SCardNM", getSharedData(oForm, "CARDNAME"))

            setFormDFValue(oForm, "U_SPhone1", getSharedData(oForm, "PHONE1"))
            setFormDFValue(oForm, "U_SContact", getSharedData(oForm, "CNTCTPRSN"))
            setEnableItem(oForm, True, "IDH_SITADD")

            oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, Val, "S")
            FilterRestrictedAddress(oData, Val)
            doSetSiteAddress(oForm, oData)
        End Sub

        Protected Overridable Sub doSetChoosenProducer(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoBPRules As Boolean = True, Optional ByVal bDoLinkCustomerAndProducer As Boolean = False)
            Dim oData As ArrayList
            Dim sProducerAddress As String = Nothing
            Dim sCardCode As String = getSharedData(oForm, "CARDCODE")

            setFormDFValueFromUser(oForm, "U_PCardCd", sCardCode)
            setFormDFValueFromUser(oForm, "U_PCardNM", getSharedData(oForm, "CARDNAME"))

            setFormDFValue(oForm, "U_PPhone1", getSharedData(oForm, "PHONE1"))
            setFormDFValue(oForm, "U_PContact", getSharedData(oForm, "CNTCTPESN"))

            oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sCardCode, "S")
            setEnableItem(oForm, True, "IDH_PRDADD")

            doSetProducerAddress(oForm, oData, False)
            If Not oData Is Nothing AndAlso oData.Count > 0 Then
                sProducerAddress = oData.Item(0)
            End If

            Dim sCustCd As String = getFormDFValue(oForm, "U_CardCd").Trim()
            If sCustCd.Length = 0 OrElse (bDoBPRules AndAlso wasSetWithCode(oForm, "IDH_CUST")) Then
                If bDoLinkCustomerAndProducer Then
                    Dim oLinkedBP As LinkedBP = Config.INSTANCE.doGetLinkedBP(sCardCode)

                    If Not oLinkedBP Is Nothing Then
                        setFormDFValue(oForm, "U_CardCd", oLinkedBP.LinkedBPCardCode)
                        setFormDFValue(oForm, "U_CardNM", oLinkedBP.LinkedBPName)
                        setFormDFValue(oForm, "U_CusFrnNm", oLinkedBP.CardForeignName)
                        setFormDFValue(oForm, "U_Phone1", oLinkedBP.Phone1)
                        setFormDFValue(oForm, "U_Contact", oLinkedBP.ContactPerson)

                        oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, oLinkedBP.LinkedBPCardCode, "S", sProducerAddress)

                        If oData Is Nothing OrElse oData.Count = 0 Then
                            If Config.INSTANCE.doGetDoBuyingAndTrade() Then
                                If doAddCustomerShipToAddressFromProducer(oForm, oLinkedBP.LinkedBPCardCode) Then
                                    'If sCustCd.Equals(oLinkedBP.CardCode) Then
                                    doSetCustomerAddressFromProducer(oForm)
                                    'End If
                                End If
                            End If
                        Else
                            doSetCustomerAddress(oForm, oData, sCardCode, oLinkedBP.Phone1, False)
                        End If
                    End If
                End If
            End If
            doSetUpdate(oForm)

            Dim sFirstBP As String = getFormDFValue(oForm, "U_FirstBP")
            If sFirstBP.Equals("None") Then
                setFormDFValue(oForm, "U_FirstBP", "Producer")
            End If
        End Sub

        Public Overridable Function doAddProducerShipToAddressFromCustomer(ByVal oForm As SAPbouiCOM.Form, ByVal sCardCode As String) As Boolean
            If getFormDFValue(oForm, "U_Address") Is Nothing OrElse getFormDFValue(oForm, "U_Address").ToString = "" Then
                Return False 'Customer has no address
            End If
            Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            Dim oAddress As SAPbobsCOM.BPAddresses = Nothing
            If sCardCode.Length > 0 Then
                Try
                    Dim bAddressFound As Boolean = False
                    oBP = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
                    If oBP.GetByKey(sCardCode) Then
                        oAddress = oBP.Addresses
                        ''## Start-10-06-2013
                        ''No need to add SubObject.Add (in this case oAddress.Add()) as by default 
                        ''One blank row is available; So commented it 
                        'oAddress.Add()
                        ''## End -10-06-2013
                        '##Start 09-09-2013
                        If oAddress.Count > 0 Then
                            For i As Int32 = 0 To oAddress.Count - 1
                                oAddress.SetCurrentLine(i)
                                Dim sCustomerAddressName As String = getFormDFValue(oForm, "U_Address")
                                Dim sProducerAddressName As String = oAddress.AddressName
                                If sProducerAddressName = sCustomerAddressName AndAlso oAddress.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo Then
                                    bAddressFound = True
                                    Exit For
                                End If
                            Next
                        End If
                        If Not bAddressFound Then
                            If oAddress.AddressName.Length > 0 Then
                                oAddress.Add()
                            End If
                            oAddress.SetCurrentLine(oAddress.Count - 1)
                        End If
                        '##
                        oAddress.AddressName = getFormDFValue(oForm, "U_Address")
                        oAddress.Street = getFormDFValue(oForm, "U_Street")
                        oAddress.Block = getFormDFValue(oForm, "U_Block")
                        oAddress.ZipCode = getFormDFValue(oForm, "U_ZpCd")
                        oAddress.City = getFormDFValue(oForm, "U_City")
                        oAddress.County = getFormDFValue(oForm, "U_County")
                        oAddress.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo
                        ''oAddress.TypeOfAddress = SAPbobsCOM.BoAddressType.bo_ShipTo
                        oAddress.State = Config.INSTANCE.doGetStateDetail("-1", getFormDFValue(oForm, "U_State"), "-1")
                        Dim sCountryCode As String
                        sCountryCode = Config.INSTANCE.doGetCountryCode(getFormDFValue(oForm, "U_CardCd"), oAddress.AddressName)
                        If sCountryCode.Length > 0 Then
                            oAddress.Country = sCountryCode
                        Else
                            oAddress.State = ""
                            oAddress.Country = ""
                        End If
                        '## Start 06-09-2013
                        If oAddress.UserFields.Fields.Item("U_ISBCUSTRN").Value.ToString.Trim.Length = 0 Then

                            oAddress.UserFields.Fields.Item("U_ISBCUSTRN").Value = getFormDFValue(oForm, "U_CustRef")
                        End If
                        '## End
                        oAddress.UserFields.Fields.Item("U_TEL1").Value = getFormDFValue(oForm, "U_SiteTl")
                        oAddress.UserFields.Fields.Item("U_IDHSteId").Value = getFormDFValue(oForm, "U_SteId")
                        oAddress.UserFields.Fields.Item("U_ROUTE").Value = getFormDFValue(oForm, "U_Route")
                        oAddress.UserFields.Fields.Item("U_IDHSEQ").Value = getFormDFValue(oForm, "U_Seq")

                        oAddress.UserFields.Fields.Item("U_IDHPCD").Value = getFormDFValue(oForm, "U_PremCd")
                        oAddress.UserFields.Fields.Item("U_IDHSLCNO").Value = getFormDFValue(oForm, "U_SiteLic")

                        'site telephone - Allready saving back,
                        oAddress.UserFields.Fields.Item("U_Origin").Value = getFormDFValue(oForm, "U_Origin") 'origin U_Origin,
                        oAddress.UserFields.Fields.Item("U_IDHACN").Value = getFormDFValue(oForm, "U_Contact") 'contact person - U_IDHACN

                        Dim iwResult As Integer
                        Dim swResult As String = Nothing
                        iwResult = oBP.Update()
                        If iwResult <> 0 Then
                            goParent.goDICompany.GetLastError(iwResult, swResult)
                            'com.idh.bridge.DataHandler.INSTANCE.doError("Error Adding Address: " + swResult)
                            com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Adding Address: " + swResult, "ERSYADDA", {Nothing})
                            Return False
                        End If
                        Return True
                    End If
                Catch ex As Exception
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error adding the Producer Address")
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXAPDA", {Nothing})
                    Return False
                Finally
                    IDHAddOns.idh.data.Base.doReleaseObject(oBP)
                    IDHAddOns.idh.data.Base.doReleaseObject(oAddress)
                End Try
            End If
            Return False
        End Function

        Public Overridable Function doAddCustomerShipToAddressFromProducer(ByVal oForm As SAPbouiCOM.Form, ByVal sCardCode As String) As Boolean
            Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            Dim oAddress As SAPbobsCOM.BPAddresses = Nothing
            If sCardCode.Length > 0 Then
                Try
                    oBP = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
                    If oBP.GetByKey(sCardCode) Then
                        oAddress = oBP.Addresses
                        oAddress.Add()
                        oAddress.SetCurrentLine(oAddress.Count - 1)
                        oAddress.AddressName = getFormDFValue(oForm, "U_PAddress")
                        oAddress.Street = getFormDFValue(oForm, "U_PStreet")
                        oAddress.Block = getFormDFValue(oForm, "U_PBlock")
                        oAddress.ZipCode = getFormDFValue(oForm, "U_PZpCd")
                        oAddress.City = getFormDFValue(oForm, "U_PCity")
                        oAddress.County = "" 'getFormDFValue(oForm, "U_County")
                        oAddress.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo

                        'oAddress.State = getFormDFValue(oForm, "U_PState")
                        'Dim oAdminInfo As SAPbobsCOM.AdminInfo = goParent.goDICompany.GetCompanyService.GetAdminInfo()
                        'oAddress.Country = oAdminInfo.Country

                        oAddress.State = Config.INSTANCE.doGetStateDetail("-1", getFormDFValue(oForm, "U_PState"), "-1")
                        Dim sCountryCode As String
                        sCountryCode = Config.INSTANCE.doGetCountryCode(getFormDFValue(oForm, "U_PCardCd"), oAddress.AddressName)
                        If sCountryCode.Length > 0 Then
                            oAddress.Country = sCountryCode
                        Else
                            oAddress.State = ""
                            oAddress.Country = ""
                        End If


                        oAddress.UserFields.Fields.Item("U_ISBCUSTRN").Value = getFormDFValue(oForm, "U_ProRef")
                        oAddress.UserFields.Fields.Item("U_TEL1").Value = getFormDFValue(oForm, "U_PPhone1")

                        oAddress.UserFields.Fields.Item("U_IDHSteId").Value = "" 'getFormDFValue(oForm, "U_SteId")
                        oAddress.UserFields.Fields.Item("U_ROUTE").Value = "" 'getFormDFValue(oForm, "U_Route")
                        oAddress.UserFields.Fields.Item("U_IDHSEQ").Value = "" 'getFormDFValue(oForm, "U_Seq")

                        oAddress.UserFields.Fields.Item("U_IDHPCD").Value = "" 'getFormDFValue(oForm, "U_PremCd")
                        oAddress.UserFields.Fields.Item("U_IDHSLCNO").Value = "" 'getFormDFValue(oForm, "U_SiteLic")

                        'site telephone - Allready saving back,
                        oAddress.UserFields.Fields.Item("U_Origin").Value = getFormDFValue(oForm, "U_Origin") 'origin U_Origin,
                        oAddress.UserFields.Fields.Item("U_IDHACN").Value = getFormDFValue(oForm, "U_PContact") 'contact person - U_IDHACN

                        Dim iwResult As Integer
                        Dim swResult As String = Nothing
                        iwResult = oBP.Update()
                        If iwResult <> 0 Then
                            goParent.goDICompany.GetLastError(iwResult, swResult)
                            'com.idh.bridge.DataHandler.INSTANCE.doError("Error Adding Address: " + swResult)
                            com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Adding Address: " + swResult, "ERSYADDA", {swResult})
                            Return False
                        End If
                        Return True
                    End If
                Catch ex As Exception
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error adding the Customer Address")
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXACA", {Nothing})
                    Return False
                Finally
                    IDHAddOns.idh.data.Base.doReleaseObject(oBP)
                    IDHAddOns.idh.data.Base.doReleaseObject(oAddress)
                End Try
            End If
            Return False
        End Function

        'Set the Producer Address From the Customer
        Protected Sub doSetProducerAddressFromCustomer(ByVal oForm As SAPbouiCOM.Form)
            setFormDFValue(oForm, "U_PAddress", getFormDFValue(oForm, "U_Address"))
            setFormDFValue(oForm, "U_PStreet", getFormDFValue(oForm, "U_Street"))
            setFormDFValue(oForm, "U_PBlock", getFormDFValue(oForm, "U_Block"))
            setFormDFValue(oForm, "U_PZpCd", getFormDFValue(oForm, "U_ZpCd"))
            setFormDFValue(oForm, "U_PCity", getFormDFValue(oForm, "U_City"))
            setFormDFValue(oForm, "U_PState", getFormDFValue(oForm, "U_State"))
            setFormDFValue(oForm, "U_ProRef", getFormDFValue(oForm, "U_CustRef"))
            '## Start12-06-2013; Add option to pick phone from general tab or from Address tab(site address)
            'setFormDFValue(oForm, "U_PPhone1", getFormDFValue(oForm, "U_SiteTl"))
            setFormDFValue(oForm, "U_PContact", getFormDFValue(oForm, "U_Contact"))
            If oForm.TypeEx = "IDH_WASTORD" Then 'For only Waste Order
                Dim PhoneOption As String = Config.Parameter("WOPHSRC")
                If PhoneOption.Length = 4 Then 'Invalid Value for switch
                    If PhoneOption.Substring(1, 1) = "1" Then 'Pick Phone for General;Producer
                        setFormDFValue(oForm, "U_PContact", getFormDFValue(oForm, "U_Contact"))
                        setFormDFValue(oForm, "U_PPhone1", getFormDFValue(oForm, "U_CPhone1"))
                    Else
                        setFormDFValue(oForm, "U_PPhone1", getFormDFValue(oForm, "U_SiteTl"))
                        setFormDFValue(oForm, "U_PContact", getFormDFValue(oForm, "U_Contact"))
                    End If
                Else
                    setFormDFValue(oForm, "U_PContact", getFormDFValue(oForm, "U_Contact"))
                    setFormDFValue(oForm, "U_PPhone1", getFormDFValue(oForm, "U_CPhone1"))
                End If
            End If

            'Was removed now back as per Hanna Doc
            setFormDFValue(oForm, "U_Origin", getFormDFValue(oForm, "U_Origin"))
            'shifted line of contact before If of FormType
            '## End
        End Sub

        Protected Sub doSetCustomerAddressFromProducer(ByVal oForm As SAPbouiCOM.Form)
            setFormDFValue(oForm, "U_Address", getFormDFValue(oForm, "U_PAddress"))
            setFormDFValue(oForm, "U_Street", getFormDFValue(oForm, "U_PStreet"))
            setFormDFValue(oForm, "U_Block", getFormDFValue(oForm, "U_PBlock"))
            setFormDFValue(oForm, "U_ZpCd", getFormDFValue(oForm, "U_PZpCd"))
            setFormDFValue(oForm, "U_City", getFormDFValue(oForm, "U_PCity"))
            setFormDFValue(oForm, "U_State", getFormDFValue(oForm, "U_PState"))
            setFormDFValue(oForm, "U_CustRef", getFormDFValue(oForm, "U_ProRef"))
            setFormDFValue(oForm, "U_SiteTl", getFormDFValue(oForm, "U_PPhone1"))
            'Was removed now back as per Hanna Doc
            setFormDFValue(oForm, "U_Origin", getFormDFValue(oForm, "U_Origin"))

            setFormDFValue(oForm, "U_Contact", getFormDFValue(oForm, "U_PContact"))

            '            setFormDFValue(oForm, "U_County", "")
            '            setFormDFValue(oForm, "U_SteId", "")
            '            setFormDFValue(oForm, "U_Route", "")
            '            setFormDFValue(oForm, "U_Seq", "")
            '
            '            setFormDFValue(oForm, "U_PremCd", "")
            '            setFormDFValue(oForm, "U_SiteLic", "")
        End Sub

        Protected Overridable Sub doSetChoosenLic(ByVal oForm As SAPbouiCOM.Form)
            setFormDFValue(oForm, "U_SLicSp", getSharedData(oForm, "CARDCODE"))
            setFormDFValue(oForm, "U_SLicNm", getSharedData(oForm, "CARDNAME"))
        End Sub

        Protected Function canSynchProToCustAddresse(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim sProAddr As String = getFormDFValue(oForm, "U_PAddress")

            If sProAddr.Length = 0 OrElse wasSetWithCode(oForm, "IDH_PRDADD") Then
                Dim sProducer As String = getFormDFValue(oForm, "U_PCardCd")
                Dim sCustomer As String = getFormDFValue(oForm, "U_CardCd")

                Dim oLinkedBP As LinkedBP = Config.INSTANCE.doGetLinkedBP(sCustomer)

                If (sProducer.Length = 0 OrElse oLinkedBP Is Nothing OrElse sProducer.Equals(oLinkedBP.LinkedBPCardCode)) Then
                    Return True
                End If
            End If
            Return False
        End Function

        Protected Function canSynchCustToProAddresse(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim sCustAddr As String = getFormDFValue(oForm, "U_Address")

            If sCustAddr.Length = 0 OrElse wasSetWithCode(oForm, "IDH_CUSADD") Then
                Dim sProducer As String = getFormDFValue(oForm, "U_PCardCd")
                Dim sCustomer As String = getFormDFValue(oForm, "U_CardCd")

                Dim oLinkedBP As LinkedBP = Config.INSTANCE.doGetLinkedBP(sProducer)

                If (sCustomer.Length = 0 OrElse oLinkedBP Is Nothing OrElse sCustomer.Equals(oLinkedBP.LinkedBPCardCode)) Then
                    Return True
                End If
            End If
            Return False
        End Function

        Protected Overridable Function doSetCustomerAddress(ByVal oForm As SAPbouiCOM.Form, ByRef oData As ArrayList, ByVal sCardCode As String, ByVal sPhone1 As String,
                                                            Optional ByVal bUpdateProdAddress As Boolean = True, Optional ByVal bDisableProducerNewAddressButton As Boolean = True) As Boolean
            If oData IsNot Nothing AndAlso oData.Count > 0 Then

                Dim sAddress As String = oData.Item(0)

                setFormDFValue(oForm, "U_Address", oData.Item(0))
                setFormDFValue(oForm, "U_Street", oData.Item(1))
                setFormDFValue(oForm, "U_Block", oData.Item(2))
                setFormDFValue(oForm, "U_ZpCd", oData.Item(3))
                setFormDFValue(oForm, "U_City", oData.Item(4))
                setFormDFValue(oForm, "U_State", oData.Item(28))
                setFormDFValue(oForm, "U_RTIMEF", oData.Item(8))
                setFormDFValue(oForm, "U_RTIMET", oData.Item(9))
                ''##MA Start 12-09-2014 Issue#413
                setFormDFValue(oForm, "U_MinJob", oData.Item(29))
                ''##MA Start 12-09-2014 Issue#413
                setFormDFValue(oForm, "U_AddrssLN", oData.Item(30))

                Dim sPhone As String = oData.Item(13)
                If sPhone Is Nothing OrElse sPhone.Trim().Length() = 0 Then
                    If Not sPhone1 Is Nothing Then
                        sPhone = sPhone1
                    End If
                End If
                setFormDFValue(oForm, "U_SiteTl", sPhone)

                setFormDFValue(oForm, "U_Route", oData.Item(14))
                setFormDFValue(oForm, "U_Seq", oData.Item(15))
                setFormDFValue(oForm, "U_PremCd", oData.Item(18))
                setFormDFValue(oForm, "U_SiteLic", oData.Item(20))
                setFormDFValue(oForm, "U_SteId", oData.Item(21))

                Dim sCustRef As String = oData.Item(12)
                If Not sCustRef Is Nothing AndAlso sCustRef.Length() > 0 AndAlso Config.Parameter("MDREAE").ToUpper().Equals("FALSE") Then
                    doSetEnabled(oForm.Items.Item("IDH_CUSCRF"), False)
                Else
                    doSetEnabled(oForm.Items.Item("IDH_CUSCRF"), True)
                End If
                setFormDFValue(oForm, "U_CustRef", sCustRef)

                'Dim sCounty As String = oData.Item(17)
                setFormDFValue(oForm, "U_County", oData.Item(17))

                setEnableItem(oForm, False, "IDH_CNA")
                '                setEnableItem(oForm, False, "IDH_CUSRF")
                '## Start 21-06-2013; Add option to pick phone/contact person from general tab or from Address tab(site address)
                If oForm.TypeEx = "IDH_WASTORD" AndAlso oData.Count > 27 Then 'For only Waste Order
                    Dim PhoneOption As String = Config.Parameter("WOPHSRC")
                    If PhoneOption.Length = 4 Then 'Invalid Value for switch
                        If PhoneOption.Substring(3, 1) = "1" Then 'Pick Contact Person for General
                            setFormDFValue(oForm, "U_Contact", oData(27))
                        Else
                            setFormDFValue(oForm, "U_Contact", oData(16))
                        End If
                    Else
                        setFormDFValue(oForm, "U_Contact", oData(27))
                    End If
                End If
                '## End
                If msOrderType.Equals("DO") = False Then
                    If bUpdateProdAddress Then
                        Dim sProAddr As String = getFormDFValue(oForm, "U_PAddress")
                        Dim sProducer As String = getFormDFValue(oForm, "U_PCardCd")

                        'This is the normal instance where the Customer and the Producer are the same BP
                        If (sProAddr.Length = 0 OrElse wasSetWithCode(oForm, "IDH_PRDADD")) _
                            AndAlso sCardCode.Equals(sProducer) Then
                            setFormDFValue(oForm, "U_PAddress", oData.Item(0))
                            setFormDFValue(oForm, "U_PStreet", oData.Item(1))
                            setFormDFValue(oForm, "U_PBlock", oData.Item(2))
                            setFormDFValue(oForm, "U_PZpCd", oData.Item(3))
                            setFormDFValue(oForm, "U_PCity", oData.Item(4))
                            setFormDFValue(oForm, "U_PState", oData.Item(28))
                            setFormDFValue(oForm, "U_PAddrsLN", oData.Item(30))

                            'Moved as per Hanna List
                            'setOrigin(oForm, oData.Item(19))
                        End If
                    End If
                    'Moved as per Hanna List
                    setOrigin(oForm, oData.Item(19))
                Else
                    setOrigin(oForm, oData.Item(19))
                End If
            Else
                setFormDFValue(oForm, "U_Address", "")
                setFormDFValue(oForm, "U_Street", "")
                setFormDFValue(oForm, "U_Block", "")
                setFormDFValue(oForm, "U_ZpCd", "")
                setFormDFValue(oForm, "U_City", "")
                setFormDFValue(oForm, "U_State", "")
                setFormDFValue(oForm, "U_County", "")
                setFormDFValue(oForm, "U_RTIMEF", "")
                setFormDFValue(oForm, "U_RTIMET", "")
                setFormDFValue(oForm, "U_CustRef", "")
                setFormDFValue(oForm, "U_SiteTl", "")
                setFormDFValue(oForm, "U_SteId", "")
                setFormDFValue(oForm, "U_Route", "")
                setFormDFValue(oForm, "U_Seq", 0)
                setFormDFValue(oForm, "U_PremCd", "")
                setFormDFValue(oForm, "U_SiteLic", "")
                setFormDFValue(oForm, "U_AddrssLN", "")
            End If
            Return True
        End Function


        Protected Overridable Sub doSetCarrierAddress(ByVal oForm As SAPbouiCOM.Form, ByRef oData As ArrayList)
            If Not (oData Is Nothing) AndAlso oData.Count > 0 Then
                setFormDFValue(oForm, "U_CAddress", oData.Item(0))
                setFormDFValue(oForm, "U_CStreet", oData.Item(1))
                setFormDFValue(oForm, "U_CBlock", oData.Item(2))
                setFormDFValue(oForm, "U_CZpCd", oData.Item(3))
                setFormDFValue(oForm, "U_CCity", oData.Item(4))
                setFormDFValue(oForm, "U_CState", oData.Item(28))
                '## Start11-06-2013
                'Need to check; is it working from all forms?
                'When we pick address fron CFL of address under Carrier Tab then it was not picking Contact Person; So placed the line to fill selected Contact person
                setFormDFValue(oForm, "U_CContact", oData.Item(16))
                '## End
                setFormDFValue(oForm, "U_CAddrsLN", oData.Item(30))

                '## Start 20-09-2013 ; Commented the following condition and paste the Carrier Ref unconditionaly

                'Dim sSupRef As String
                'sSupRef = getFormDFValue(oForm, "U_SupRef")
                'If sSupRef Is Nothing OrElse sSupRef.Length() = 0 Then
                setFormDFValue(oForm, "U_SupRef", oData.Item(12))
                'End If
                '## End
            Else
                setFormDFValue(oForm, "U_CAddress", "")
                setFormDFValue(oForm, "U_CAddrsLN", "")
                setFormDFValue(oForm, "U_CStreet", "")
                setFormDFValue(oForm, "U_CBlock", "")
                setFormDFValue(oForm, "U_CZpCd", "")
                setFormDFValue(oForm, "U_CCity", "")
                setFormDFValue(oForm, "U_CState", "")
                setFormDFValue(oForm, "U_SupRef", "")
                '## Start12-06-2013
                'Earlier Here line was setFormDFValue(oForm, "U_CContact", oData.Item(16))
                'And if oData is nothing then can cause exception
                setFormDFValue(oForm, "U_CContact", "")
                setFormDFValue(oForm, "U_CPhone1", "")
                '## End
            End If
        End Sub

        Protected Overridable Function doSetProducerAddress(ByVal oForm As SAPbouiCOM.Form, ByRef oData As ArrayList, ByVal bFromUser As Boolean,
                                                            Optional ByVal bUpdateCustomer As Boolean = True, Optional ByVal bDisableCustomerNewAddressButton As Boolean = True) As Boolean
            Dim sOrigin As String = ""
            If Not (oData Is Nothing) AndAlso oData.Count > 0 Then
                If bFromUser Then
                    setFormDFValueFromUser(oForm, "U_PAddress", oData.Item(0))
                Else
                    setFormDFValue(oForm, "U_PAddress", oData.Item(0))
                End If
                'setFormDFValue(oForm, "U_PAddress", oData.Item(0))
                setFormDFValue(oForm, "U_PStreet", oData.Item(1))
                setFormDFValue(oForm, "U_PBlock", oData.Item(2))
                setFormDFValue(oForm, "U_PZpCd", oData.Item(3))
                setFormDFValue(oForm, "U_PCity", oData.Item(4))
                setFormDFValue(oForm, "U_PState", oData.Item(28))
                setFormDFValue(oForm, "U_PAddrsLN", oData.Item(30)) 'Address Line num
                sOrigin = oData.Item(19)

                Dim sProRef As String
                sProRef = getFormDFValue(oForm, "U_ProRef")
                If sProRef Is Nothing OrElse sProRef.Length() = 0 Then
                    setFormDFValue(oForm, "U_ProRef", oData.Item(12))
                End If

                If msOrderType.Equals("DO") = False Then
                    If bUpdateCustomer Then
                        Dim sCustAddr As String = getFormDFValue(oForm, "U_Address")
                        Dim sCustomer As String = getFormDFValue(oForm, "U_CardCd")
                        Dim sProducer As String = getFormDFValue(oForm, "U_PCardCd")

                        'This is the normal instance where the Customer and the Producer are the same BP
                        If (sCustAddr.Length = 0 OrElse wasSetWithCode(oForm, "IDH_CUSADD")) _
                            AndAlso sCustomer.Equals(sProducer) Then
                            setFormDFValue(oForm, "U_Address", oData.Item(0))
                            setFormDFValue(oForm, "U_Street", oData.Item(1))
                            setFormDFValue(oForm, "U_Block", oData.Item(2))
                            setFormDFValue(oForm, "U_ZpCd", oData.Item(3))
                            setFormDFValue(oForm, "U_City", oData.Item(4))
                            setFormDFValue(oForm, "U_State", oData.Item(28))
                            setFormDFValue(oForm, "U_AddrssLN", oData.Item(30)) 'Address Line num

                        End If
                    End If
                End If
            Else
                setFormDFValue(oForm, "U_PAddress", "")
                setFormDFValue(oForm, "U_PAddrsLN", "")
                setFormDFValue(oForm, "U_PStreet", "")
                setFormDFValue(oForm, "U_PBlock", "")
                setFormDFValue(oForm, "U_PZpCd", "")
                setFormDFValue(oForm, "U_PCity", "")
                setFormDFValue(oForm, "U_PState", "")
                setFormDFValue(oForm, "U_ProRef", "")
            End If
            If Not sOrigin Is Nothing AndAlso sOrigin.Length > 0 Then
                setOrigin(oForm, sOrigin)
            End If
            Return True
        End Function

        Protected Overridable Sub doSetSiteAddress(ByVal oForm As SAPbouiCOM.Form, ByRef oData As ArrayList)
            If Not (oData Is Nothing) AndAlso oData.Count > 0 Then
                setFormDFValue(oForm, "U_SAddress", oData.Item(0))
                setFormDFValue(oForm, "U_SStreet", oData.Item(1))
                setFormDFValue(oForm, "U_SBlock", oData.Item(2))
                setFormDFValue(oForm, "U_SZpCd", oData.Item(3))
                setFormDFValue(oForm, "U_SCity", oData.Item(4))
                setFormDFValue(oForm, "U_SState", oData.Item(28))
                setFormDFValue(oForm, "U_SAddrsLN", oData.Item(30))
                If oData.Item(16).Length > 0 Then
                    setFormDFValue(oForm, "U_SContact", oData.Item(16))
                End If

                Dim sSiteRef As String
                sSiteRef = getFormDFValue(oForm, "U_SiteRef")
                If sSiteRef Is Nothing OrElse sSiteRef.Length() = 0 Then
                    setFormDFValue(oForm, "U_SiteRef", oData.Item(12))
                End If
                '## Start 13-06-2013
                If oForm.TypeEx = "IDH_WASTORD" AndAlso oData.Count > 27 Then 'For only Waste Order
                    Dim PhoneOption As String = Config.Parameter("WOPHSRC")
                    If PhoneOption.Length = 4 Then 'Invalid Value for switch
                        If PhoneOption.Substring(2, 1) = "1" Then 'Pick Phone for General;site
                            setFormDFValue(oForm, "U_SPhone1", oData(26))
                            setFormDFValue(oForm, "U_SContact", oData(27))
                        Else
                            setFormDFValue(oForm, "U_SPhone1", oData(13))
                            setFormDFValue(oForm, "U_SContact", oData(16))
                        End If
                    Else
                        setFormDFValue(oForm, "U_SPhone1", oData(26))
                        setFormDFValue(oForm, "U_SContact", oData(27))
                    End If
                    'Else
                    '   setFormDFValue(oForm, "U_CPhone1", Phone1)
                End If
                '## End
            Else
                setFormDFValue(oForm, "U_SAddress", "")
                setFormDFValue(oForm, "U_SAddrsLN", "")
                setFormDFValue(oForm, "U_SStreet", "")
                setFormDFValue(oForm, "U_SBlock", "")
                setFormDFValue(oForm, "U_SZpCd", "")
                setFormDFValue(oForm, "U_SCity", "")
                setFormDFValue(oForm, "U_SState", "")
                setFormDFValue(oForm, "U_SiteRef", "")
                setFormDFValue(oForm, "U_SContact", "")
                setFormDFValue(oForm, "U_SPhone1", "")
            End If
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim sItemID As String = pVal.ItemUID

            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_GOT_FOCUS Then
                If pVal.BeforeAction = False Then
                    If mbFolderFollowDB Then
                        If pVal.ItemUID.Equals("IDH_CUST") OrElse sItemID.Equals("IDH_CUSTNM") Then
                            doSetFocus(oForm, "IDH_TABCUS")
                        ElseIf sItemID.Equals("IDH_WPRODU") OrElse sItemID.Equals("IDH_WNAM") Then
                            doSetFocus(oForm, "IDH_TABPRO")
                        ElseIf sItemID.Equals("IDH_DISSIT") OrElse sItemID.Equals("IDH_DISNAM") Then
                            doSetFocus(oForm, "IDH_TABSIT")
                        ElseIf sItemID.Equals("IDH_CARRIE") OrElse sItemID.Equals("IDH_CARNAM") Then
                            doSetFocus(oForm, "IDH_TABCAR")
                        End If
                    End If
                    If sItemID.Equals("IDH_IGRP") Then
                        doSetFocus(oForm, "IDH_TABMAT")
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                Dim oData As New Hashtable
                If pVal.BeforeAction = False Then
                    If sItemID = "IDH_TABMAT" Then
                        oForm.PaneLevel = 1
                        '                        If getWFValue(oForm, "RulesApplied") = False Then
                        '                            'IDHGrid.getInstance(oForm, "LINESGRID").doApplyRules()
                        '                            setWFValue(oForm, "RulesApplied", True)
                        '                        End If
                    ElseIf sItemID = "IDH_TABCAR" Then
                        oForm.PaneLevel = 2
                    ElseIf sItemID = "IDH_TABCUS" Then
                        oForm.PaneLevel = 3
                    ElseIf sItemID = "IDH_TABPRO" Then
                        oForm.PaneLevel = 4
                    ElseIf sItemID = "IDH_TABSIT" Then
                        oForm.PaneLevel = 5
                    ElseIf sItemID = "IDH_TABMLA" Then
                        oForm.PaneLevel = 6
                    ElseIf sItemID = "IDH_CUSL" Then
                        If mbFolderFollowDB Then
                            doSetFocus(oForm, "IDH_TABCUS")
                        End If
                        doChooseCustomer(oForm, False, sItemID)
                    ElseIf sItemID = "IDH_SLSL" Then
                        doChooseLic(oForm, False, sItemID)
                    ElseIf sItemID = "IDH_CARL" Then
                        If mbFolderFollowDB Then
                            doSetFocus(oForm, "IDH_TABCAR")
                        End If
                        doChooseCarrier(oForm, False, sItemID)
                    ElseIf sItemID = "IDH_SITL" OrElse sItemID = "IDH_DISPCF" Then
                        If mbFolderFollowDB Then
                            doSetFocus(oForm, "IDH_TABSIT")
                        End If
                        doChooseSite(oForm, False, sItemID)
                    ElseIf sItemID = "IDH_PRDL" Then
                        If mbFolderFollowDB Then
                            doSetFocus(oForm, "IDH_TABPRO")
                        End If
                        doChooseProducer(oForm, False, sItemID)
                    ElseIf sItemID = "IDH_ROUTL" Then
                        doChooseRoute(oForm, False, "")
                    ElseIf sItemID = "IDH_ORIGLU" Then
                        Dim sOrigin As String = getFormDFValue(oForm, "U_Origin")
                        doChooseOrigin(oForm, False, sOrigin, "")
                    ElseIf sItemID = "IDH_CUSAL" OrElse
                           sItemID = "IDH_WASAL" OrElse
                           sItemID = "IDH_PROAL" OrElse
                           sItemID = "IDH_SITAL" OrElse
                           sItemID = "IDH_SITAL2" Then
                        doChooseAddress(oForm, sItemID)
                        '                 Dim sCardCode As String = Nothing

                        '                 If sItemID = "IDH_CUSAL" Then
                        '                     sCardCode = getFormDFValue(oForm, "U_CardCd")
                        ' ''##MA Start 18-09-2014 Issue#403
                        '                     setSharedData(oForm, "IDH_APPLYFLTR", Config.INSTANCE.getParameterAsBool("FLTRADDR", False).ToString)
                        '                     ''##MA End 18-09-2014 Issue#403
                        '                 ElseIf sItemID = "IDH_WASAL" Then
                        '                     sCardCode = getFormDFValue(oForm, "U_CCardCd")
                        '                 ElseIf sItemID = "IDH_PROAL" Then
                        '                     sCardCode = getFormDFValue(oForm, "U_PCardCd") ''.GetValue("U_PCardCd", .Offset).Trim()
                        ' ''##MA Start 18-09-2014 Issue#403
                        '                     setSharedData(oForm, "IDH_APPLYFLTR", Config.INSTANCE.getParameterAsBool("FLTRADDR", False).ToString)
                        '                     ''##MA End 18-09-2014 Issue#403
                        '                 ElseIf sItemID = "IDH_SITAL" Then
                        '                     sCardCode = getFormDFValue(oForm, "U_SCardCd")
                        ' ''##MA Start 18-09-2014 Issue#403
                        '                     setSharedData(oForm, "IDH_APPLYFLTR", Config.INSTANCE.getParameterAsBool("FLTRADDR", False).ToString)
                        '                     ''##MA End 18-09-2014 Issue#403
                        '                 End If

                        '                 setSharedData(oForm, "TRG", sItemID)
                        '                 setSharedData(oForm, "IDH_CUSCOD", sCardCode)
                        '                 setSharedData(oForm, "IDH_ADRTYP", "S")
                        '                 '## Start 20-06-2013; 16-07-2013
                        '                 If (sItemID = "IDH_SITAL" OrElse sItemID = "IDH_CUSAL" OrElse sItemID = "IDH_WASAL" OrElse sItemID = "IDH_PROAL") Then
                        '                     '(oForm.TypeEx = "IDH_WASTORD" OrElse oForm.TypeEx = "IDH_DISPORD") AndAlso 
                        '                     If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
                        '                         goParent.doOpenModalForm("IDHASRCH", oForm)
                        '                     Else
                        '                         goParent.doOpenModalForm("IDHASRCH3", oForm)
                        '                         'goParent.doOpenModalForm("IDHASRCH", oForm)
                        '                     End If
                        '                 Else
                        '                     goParent.doOpenModalForm("IDHASRCH", oForm)
                        '                 End If
                        '                 '## End
                    ElseIf sItemID = "IDH_MLAAL" Then 'For mailing Address 
                        'Dim sCardCode As String = Nothing
                        'sCardCode = getFormDFValue(oForm, "U_CardCd")
                        'setSharedData(oForm, "TRG", sItemID)
                        'setSharedData(oForm, "IDH_CUSCOD", sCardCode)
                        ''setSharedData(oForm, "IDH_ADRTYP", "S")
                        'setSharedData(oForm, "IDH_MADD", "Y")
                        'goParent.doOpenModalForm("IDHMASRCH", oForm)
                        doChooseMailingAddress(oForm, "IDH_MLAADD")
                    ElseIf sItemID = "IDH_OSM" Then
                        setSharedData(oForm, "IDH_CUST", getFormDFValue(oForm, "U_CardCd"))
                        setSharedData(oForm, "IDH_ADDR", getFormDFValue(oForm, "U_Address"))

                        Dim sOSM As String = Config.Parameter("WOOSM")
                        If sOSM IsNot Nothing AndAlso sOSM.Length > 0 Then
                            goParent.doOpenModalForm(sOSM, oForm)
                        Else
                            goParent.doOpenModalForm("IDHJOBR", oForm)
                        End If
                    ElseIf sItemID = "IDH_CNA" Then
                        '## Start 29-05-2014
                        'If doAddCustomerShipToAddress(oForm) = True Then
                        '    setEnableItem(oForm, False, "IDH_CNA")

                        'End If
                        doSetProducerAddressFromCustomer(oForm)
                        setEnableItem(oForm, False, "IDH_CNA")
                        '## End 29-05-2014
                    ElseIf sItemID = "IDH_ROWHTR" Then
                        'OnTime 26252: WR1_MDJ_DO_Audit Trail / History for Disposal Order Row
                        'Disposal Order ROW History
                        'Dim formDisposalRow As SAPbouiCOM.Form
                        'formDisposalRow = Me.PARENT.goApplication.Forms.ActiveForm
                        setSharedData(oForm, "TABLE", "@IDH_DISPROW")
                        setSharedData(oForm, "CODE", getItemValue(oForm, "IDH_ROW"))
                        goParent.doOpenModalForm("IDH_HIST", oForm)
                    ElseIf sItemID = "IDH_PNA" Then
                        '## Start 29-05-2014
                        'If doAddProducerShipToAddress(oForm) = True Then
                        '    setEnableItem(oForm, False, "IDH_PNA")

                        'End If
                        doSetCustomerAddressFromProducer(oForm)
                        setEnableItem(oForm, False, "IDH_PNA")
                        '## End 29-05-2014
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                Dim bIsInnerEvent As Boolean = pVal.InnerEvent
                If pVal.BeforeAction = False Then
                    If pVal.ItemChanged Then
                        If True OrElse wasSetWithCode(oForm, sItemID) = False Then
                            If bIsInnerEvent = False Then
                                If sItemID = "IDH_CARRIE" Then
                                    Dim sCardCode As String = getFormDFValue(oForm, "U_CCardCd", True)
                                    If sCardCode.Trim <> "" Then
                                        '## Start 12-07-2013
                                        'If sCardCode.StartsWith("*") OrElse sCardCode.EndsWith("*") Then
                                        '## end
                                        setItemValue(oForm, "IDH_CARNAM", "")
                                        doChooseCarrier(oForm, True, sItemID)
                                    ElseIf sCardCode.Length = 0 AndAlso Config.INSTANCE.getParameterAsBool("WOCARRMA") = True Then
                                        setItemValue(oForm, "IDH_CARNAM", "")
                                        doChooseCarrier(oForm, True, sItemID)
                                        ''##MA Start 19-02-2015 Issue#600
                                    ElseIf sCardCode.Length = 0 AndAlso Config.INSTANCE.getParameterAsBool("WOCARRMA") = False Then
                                        setItemValue(oForm, "IDH_CARNAM", "")
                                        ''##MA End 19-02-2015 Issue#600
                                    ElseIf sCardCode.Length > 0 Then
                                        setItemValue(oForm, "IDH_CARNAM", "")
                                        doChooseCarrier(oForm, True, sItemID)
                                    End If
                                ElseIf sItemID = "IDH_CARNAM" Then
                                    Dim sName As String = getItemValue(oForm, "IDH_CARNAM")
                                    'If sName.StartsWith("*") OrElse sName.EndsWith("*") Then
                                    setItemValue(oForm, "IDH_CARRIE", "")
                                    'End If
                                    doChooseCarrier(oForm, True, sItemID)
                                ElseIf sItemID = "IDH_DISSIT" Then 'OrElse sItemID = "IDH_DISPCD" Then
                                    setItemValue(oForm, "IDH_DISNAM", "")
                                    doChooseSite(oForm, True, sItemID)
                                ElseIf sItemID = "IDH_DISPCD" Then
                                    setItemValue(oForm, "IDH_DISPNM", "")
                                    doChooseSite(oForm, True, sItemID)
                                ElseIf sItemID = "IDH_DISNAM" Then 'OrElse sItemID = "IDH_DISPNM" Then
                                    Dim sName As String = getItemValue(oForm, "IDH_DISNAM")
                                    'If sName.IndexOf("*") > -1 Then
                                    setItemValue(oForm, "IDH_DISSIT", "")
                                    'End If
                                    doChooseSite(oForm, True, sItemID)
                                ElseIf sItemID = "IDH_DISPNM" Then 'OrElse sItemID = "IDH_DISPNM" Then
                                    Dim sName As String = getItemValue(oForm, "IDH_DISPNM")
                                    'If sName.IndexOf("*") > -1 Then
                                    setItemValue(oForm, "IDH_DISPCD", "")
                                    'End If
                                    doChooseSite(oForm, True, sItemID)
                                ElseIf sItemID = "IDH_CUST" Then
                                    '## Start 12-07-2013
                                    If getFormDFValue(oForm, "U_CardCd", True).ToString.Trim <> "" Then 'not (oForm.TypeEx = "IDH_DISPORD" AndAlso 
                                        setItemValue(oForm, "IDH_CUSTNM", "")
                                        doChooseCustomer(oForm, True, sItemID)
                                        '## Start 19-02-2015 Issue#600
                                    ElseIf getFormDFValue(oForm, "U_CardCd", True).ToString.Trim = "" Then
                                        setItemValue(oForm, "IDH_CUSTNM", "")
                                        '## End 19-02-2015 Issue#600
                                        setItemValue(oForm, "IDH_CUSCRF", "")
                                    End If
                                    '## End
                                ElseIf sItemID = "IDH_CUSTNM" Then
                                    Dim sName As String = getItemValue(oForm, "IDH_CUSTNM")
                                    If sName.Trim <> "" Then
                                        setItemValue(oForm, "IDH_CUST", "")
                                        doChooseCustomer(oForm, True, sItemID)
                                    ElseIf sName = "" Then
                                        setItemValue(oForm, "IDH_CUST", "")
                                        setItemValue(oForm, "IDH_CUSCRF", "")
                                    End If
                                ElseIf sItemID = "IDH_CUSADD" Then
                                    Dim sAddress As String = getItemValue(oForm, "IDH_CUSADD")
                                    If sAddress.Trim <> "" Then
                                        doChooseAddress(oForm, sItemID)
                                    End If
                                ElseIf sItemID = "IDH_CUSA2" Then
                                    Dim sAddress As String = getItemValue(oForm, "IDH_CUSA2")
                                    If sAddress.Trim.IndexOf("*") > -1 Then
                                        doChooseAddress(oForm, sItemID)
                                    End If
                                ElseIf sItemID = "IDH_ADDRES" Then
                                    Dim sAddress As String = getItemValue(oForm, "IDH_ADDRES")
                                    If sAddress.Trim <> "" Then
                                        doChooseAddress(oForm, sItemID)
                                    End If
                                ElseIf sItemID = "IDH_PRDADD" Then
                                    Dim sAddress As String = getItemValue(oForm, "IDH_PRDADD")
                                    If sAddress.Trim <> "" Then
                                        doChooseAddress(oForm, sItemID)
                                    End If
                                ElseIf sItemID = "IDH_SITADD" OrElse sItemID = "IDH_DISPAD" Then
                                    Dim sAddress As String = getItemValue(oForm, sItemID)
                                    If sAddress.Trim <> "" Then
                                        doChooseAddress(oForm, sItemID)
                                    End If
                                ElseIf sItemID = "IDH_MLAADD" Then
                                    Dim sMAddress As String = getItemValue(oForm, "IDH_MLAADD")
                                    If sMAddress.Trim <> "" Then
                                        doChooseMailingAddress(oForm, sItemID)
                                    End If
                                ElseIf sItemID = "IDH_ORIGIN" Then
                                    Dim sOrigin As String = getFormDFValue(oForm, "U_Origin")
                                    doChooseOrigin(oForm, True, sOrigin, sItemID)

                                ElseIf sItemID = "IDH_WPRODU" Then
                                    'setItemValue(oForm, "IDH_WNAM", "")
                                    'doChooseProducer(oForm, True)
                                    If getFormDFValue(oForm, "U_PCardCd", True).ToString.Trim <> "" Then 'not (oForm.TypeEx = "IDH_DISPORD" AndAlso 
                                        setItemValue(oForm, "IDH_WNAM", "")
                                        doChooseProducer(oForm, True, sItemID)
                                    ElseIf getFormDFValue(oForm, "U_PCardCd", True).ToString.Trim = "" Then
                                        setItemValue(oForm, "IDH_WNAM", "")
                                        '## End 19-02-2015 Issue#600
                                    End If
                                ElseIf sItemID = "IDH_WNAM" Then
                                    Dim sName As String = getItemValue(oForm, "IDH_WNAM")
                                    If sName <> "" Then
                                        setItemValue(oForm, "IDH_WPRODU", "")
                                        doChooseProducer(oForm, True, sItemID)
                                    Else
                                        setItemValue(oForm, "IDH_WPRODU", "")
                                    End If
                                ElseIf sItemID = "IDH_ROUTE" Then
                                    doChooseRoute(oForm, True, sItemID)
                                    ''MA Start Pt#791 15-06-2015
                                ElseIf sItemID = "IDH_LICSUP" Then
                                    Dim sLicSite As String = getItemValue(oForm, "IDH_LICSUP")
                                    If sLicSite <> "" Then
                                        setItemValue(oForm, "IDH_SLICNM", "")
                                        doChooseLic(oForm, True, sItemID)
                                    Else
                                        setItemValue(oForm, "IDH_SLICNM", "")
                                    End If
                                ElseIf sItemID = "IDH_SLICNM" Then
                                    Dim sLicSite As String = getItemValue(oForm, "IDH_SLICNM")
                                    If sLicSite <> "" Then
                                        setItemValue(oForm, "IDH_LICSUP", "")
                                        doChooseLic(oForm, True, sItemID)
                                    Else
                                        setItemValue(oForm, "IDH_LICSUP", "")
                                    End If
                                    ''MA Start Pt#791 15-06-2015
                                End If
                            End If

                            If sItemID = "IDH_CUSADD" Then
                                '## Start 16-07-2013
                                If oForm.TypeEx = "IDH_DISPORD" Then
                                    'Return False
                                End If
                                '## End
                                doCheckCustomerAddress(oForm)
                                '	                    	ElseIf sItemID = "IDH_CARRIE" OrElse sItemID = "IDH_CARNAM" Then
                                '	                            Dim sCode As String = getItemValue(oForm, "IDH_CARRIE")
                                '	                            Dim sName As String = getItemValue(oForm, "IDH_CARNAM")
                                '	                            If sCode.StartsWith("*") OrElse sCode.EndsWith("*") Then
                                '	                                setItemValue(oForm, "IDH_CARNAM", "")
                                '	                            ElseIf sName.StartsWith("*") OrElse sName.EndsWith("*") Then
                                '	                                setItemValue(oForm, "IDH_CARRIE", "")
                                '	                            End If
                                '	                            doChooseCarrier(oForm, True)
                                '	                        ElseIf sItemID = "IDH_DISSIT" OrElse sItemID = "IDH_DISNAM" Then
                                '	                            Dim sCode As String = getItemValue(oForm, "IDH_DISSIT")
                                '	                            Dim sName As String = getItemValue(oForm, "IDH_DISNAM")
                                '	                            If sCode.StartsWith("*") OrElse sCode.EndsWith("*") Then
                                '	                                setItemValue(oForm, "IDH_DISNAM", "")
                                '	                            ElseIf sName.StartsWith("*") OrElse sName.EndsWith("*") Then
                                '	                                setItemValue(oForm, "IDH_DISSIT", "")
                                '	                            End If
                                '	                            doChooseSite(oForm, True)
                                '	                        ElseIf sItemID = "IDH_CUST" OrElse sItemID = "IDH_CUSTNM" Then
                                '	                            Dim sCode As String = getItemValue(oForm, "IDH_CUST")
                                '	                            Dim sName As String = getItemValue(oForm, "IDH_CUSTNM")
                                '	                            If sCode.StartsWith("*") OrElse sCode.EndsWith("*") Then
                                '	                                setItemValue(oForm, "IDH_CUSTNM", "")
                                '	                            ElseIf sName.StartsWith("*") OrElse sName.EndsWith("*") Then
                                '	                                setItemValue(oForm, "IDH_CUST", "")
                                '	                            End If
                                '	                            doChooseCustomer(oForm, True)
                                '	                        ElseIf sItemID = "IDH_WPRODU" OrElse sItemID = "IDH_WNAM" Then
                                '	                            Dim sCode As String = getItemValue(oForm, "IDH_WPRODU")
                                '	                            Dim sName As String = getItemValue(oForm, "IDH_WNAM")
                                '	                            If sCode.StartsWith("*") OrElse sCode.EndsWith("*") Then
                                '	                                setItemValue(oForm, "IDH_WNAM", "")
                                '	                            ElseIf sName.StartsWith("*") OrElse sName.EndsWith("*") Then
                                '	                                setItemValue(oForm, "IDH_WPRODU", "")
                                '	                            End If
                                '	                            doChooseProducer(oForm, True)
                            End If
                        Else
                            ''Do THIS ALWAYS
                            '## Start 18-06-2013 Un-coment the folloing code to enable choose customer if CardCode is fully entered without CardCode*
                            'If sItemID = "IDH_CUST" Then
                            '    doChooseCustomer(oForm, True)
                            'End If
                            '##End
                        End If
                    Else
                        If bIsInnerEvent = False Then
                            If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE AndAlso
                             Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                If sItemID = "IDH_CUST" Then
                                    Dim sCardCd As String = getItemValue(oForm, "IDH_CUST")
                                    If sCardCd.IndexOf("*") > -1 Then '<> "" AndAlso wasSetWithCode(oForm, sItemID) = False Then
                                        doChooseCustomer(oForm, True, sItemID)
                                        BubbleEvent = False
                                    End If
                                ElseIf sItemID = "IDH_CUSTNM" Then
                                    Dim sCardNm As String = getItemValue(oForm, "IDH_CUSTNM")
                                    If sCardNm.IndexOf("*") > -1 Then ' <> "" AndAlso wasSetWithCode(oForm, sItemID) = False Then
                                        doChooseCustomer(oForm, True, sItemID)
                                        BubbleEvent = False
                                    End If
                                ElseIf sItemID = "IDH_CUSADD" Then
                                    Dim sAddress As String = getItemValue(oForm, "IDH_CUSADD")
                                    If sAddress.Trim.IndexOf("*") > -1 Then
                                        doChooseAddress(oForm, sItemID)
                                    End If
                                ElseIf sItemID = "IDH_CUSA2" Then
                                    Dim sAddress As String = getItemValue(oForm, "IDH_CUSA2")
                                    If sAddress.Trim.IndexOf("*") > -1 Then
                                        doChooseAddress(oForm, sItemID)
                                    End If
                                ElseIf sItemID = "IDH_ADDRES" Then
                                    Dim sAddress As String = getItemValue(oForm, "IDH_ADDRES")
                                    If sAddress.Trim.IndexOf("*") > -1 Then
                                        doChooseAddress(oForm, sItemID)
                                    End If
                                ElseIf sItemID = "IDH_PRDADD" Then
                                    Dim sAddress As String = getItemValue(oForm, "IDH_PRDADD")
                                    If sAddress.Trim.IndexOf("*") > -1 Then
                                        doChooseAddress(oForm, sItemID)
                                    End If
                                ElseIf sItemID = "IDH_SITADD" OrElse sItemID = "IDH_DISPAD" Then
                                    Dim sAddress As String = getItemValue(oForm, sItemID)
                                    If sAddress.Trim.IndexOf("*") > -1 Then
                                        doChooseAddress(oForm, sItemID)
                                    End If
                                    '
                                ElseIf sItemID = "IDH_CARRIE" Then 'OrElse sItemID = "IDH_CARNAM" Then
                                    Dim sCarriedCd As String = getItemValue(oForm, sItemID)
                                    If sCarriedCd.IndexOf("*") > -1 Then ' <> "" AndAlso wasSetWithCode(oForm, sItemID) = False Then
                                        setItemValue(oForm, "IDH_CARNAM", "")
                                        doChooseCarrier(oForm, True, sItemID)
                                    End If
                                ElseIf sItemID = "IDH_CARNAM" Then 'OrElse sItemID = "IDH_CARNAM" Then
                                    Dim sCarriedNm As String = getItemValue(oForm, sItemID)
                                    If sCarriedNm.IndexOf("*") > -1 Then ' <> "" AndAlso wasSetWithCode(oForm, sItemID) = False Then
                                        setItemValue(oForm, "IDH_CARRIE", "")
                                        doChooseCarrier(oForm, True, sItemID)
                                    End If
                                ElseIf sItemID = "IDH_MLAADD" Then
                                    Dim sMAddress As String = getItemValue(oForm, "IDH_MLAADD")
                                    If sMAddress.Trim.IndexOf("*") > -1 Then
                                        doChooseMailingAddress(oForm, sItemID)
                                    End If
                                ElseIf sItemID = "IDH_WPRODU" Then
                                    Dim sProdCd As String = getItemValue(oForm, "IDH_WPRODU")
                                    If sProdCd.IndexOf("*") > -1 Then ' <> "" AndAlso wasSetWithCode(oForm, sItemID) = False Then
                                        doChooseProducer(oForm, True, sItemID)
                                        BubbleEvent = False
                                    End If
                                ElseIf sItemID = "IDH_DISSIT" OrElse sItemID = "IDH_DISPCD" OrElse sItemID = "IDH_DISNAM" OrElse sItemID = "IDH_DISPNM" Then
                                    Dim sSiteCd As String = getItemValue(oForm, sItemID)
                                    If sSiteCd.IndexOf("*") > -1 Then ' <> "" AndAlso wasSetWithCode(oForm, sItemID) = False Then
                                        doChooseSite(oForm, True, sItemID)
                                        BubbleEvent = False
                                    End If
                                ElseIf sItemID = "IDH_WNAM" Then
                                    Dim sProdNm As String = getItemValue(oForm, "IDH_CUSTNM")
                                    If sProdNm.IndexOf("*") > -1 Then ' <> "" AndAlso wasSetWithCode(oForm, sItemID) = False Then
                                        doChooseProducer(oForm, True, sItemID)
                                        BubbleEvent = False
                                    End If
                                ElseIf sItemID = "IDH_ROUTE" Then
                                    Dim sRoute As String = getItemValue(oForm, "IDH_ROUTE")
                                    If sRoute.IndexOf("*") > -1 Then ' <> "" AndAlso wasSetWithCode(oForm, sItemID) = False Then
                                        doChooseRoute(oForm, True, sItemID)
                                        BubbleEvent = False
                                    End If
                                ElseIf sItemID = "IDH_ORIGIN" Then
                                    Dim sOrigin As String = getFormDFValue(oForm, "U_Origin")
                                    If sOrigin.IndexOf("*") > -1 Then ' <> "" AndAlso wasSetWithCode(oForm, sItemID) = False Then
                                        doChooseOrigin(oForm, True, sOrigin, sItemID)
                                    End If

                                    ''MA Start Pt#791 15-06-2015
                                ElseIf sItemID = "IDH_LICSUP" Then
                                    Dim sLicSite As String = getItemValue(oForm, "IDH_LICSUP")
                                    If sLicSite.IndexOf("*") > -1 Then
                                        setItemValue(oForm, "IDH_SLICNM", "")
                                        doChooseLic(oForm, True, sItemID)
                                    End If
                                ElseIf sItemID = "IDH_SLICNM" Then
                                    Dim sLicSite As String = getItemValue(oForm, "IDH_SLICNM")
                                    If sLicSite.IndexOf("*") > -1 Then
                                        setItemValue(oForm, "IDH_LICSUP", "")
                                        doChooseLic(oForm, True, sItemID)
                                    End If
                                    ''MA Start Pt#791 15-06-2015
                                    'ElseIf sItemID = "IDH_CARRIE" Then
                                    '    Dim sCardCd As String = getItemValue(oForm, "IDH_CARRIE")
                                    '    If Config.INSTANCE.getParameterAsBool("WOCARRMA") = False Then
                                    '        'If pVal.InnerEvent = True Then
                                    '        'Return True
                                    '        'End If
                                    '    Else
                                    '        If sCardCd.Length = 0 Then
                                    '            doChooseCarrier(oForm, True)
                                    '        End If
                                    '    End If
                                    'ElseIf sItemID = "IDH_DISSIT" Then
                                    '    Dim sCardCd As String = getItemValue(oForm, "IDH_DISSIT")
                                    '    If sCardCd.Length = 0 Then
                                    '        doChooseSite(oForm, True)
                                    '    End If
                                    'ElseIf sItemID = "IDH_WPRODU" Then
                                    '    Dim sCardCd As String = getItemValue(oForm, "IDH_WPRODU")
                                    '    If sCardCd.Length = 0 Then
                                    '        doChooseProducer(oForm, True)
                                    '    End If
                                End If
                            End If
                        End If
                    End If
                    BubbleEvent = False
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If doCheckSearchKey(pVal) = True Then
                    If pVal.BeforeAction = False Then
                        '                        If sItemID = "IDH_CARRIE" OrElse sItemID = "IDH_CARNAM" Then
                        '                            Dim sCode As String = getItemValue(oForm, "IDH_CARRIE")
                        '                            Dim sName As String = getItemValue(oForm, "IDH_CARNAM")
                        '                            If sCode.StartsWith("*") OrElse sCode.EndsWith("*") Then
                        '                                setItemValue(oForm, "IDH_CARNAM", "")
                        '                            ElseIf sName.StartsWith("*") OrElse sName.EndsWith("*") Then
                        '                                setItemValue(oForm, "IDH_CARRIE", "")
                        '                            End If
                        '                            doChooseCarrier(oForm, True)
                        '                        ElseIf sItemID = "IDH_DISSIT" OrElse sItemID = "IDH_DISNAM" Then
                        '                            Dim sCode As String = getItemValue(oForm, "IDH_DISSIT")
                        '                            Dim sName As String = getItemValue(oForm, "IDH_DISNAM")
                        '                            If sCode.StartsWith("*") OrElse sCode.EndsWith("*") Then
                        '                                setItemValue(oForm, "IDH_DISNAM", "")
                        '                            ElseIf sName.StartsWith("*") OrElse sName.EndsWith("*") Then
                        '                                setItemValue(oForm, "IDH_DISSIT", "")
                        '                            End If
                        '                            doChooseSite(oForm, True)
                        '                        ElseIf sItemID = "IDH_CUST" OrElse sItemID = "IDH_CUSTNM" Then
                        '                            Dim sCode As String = getItemValue(oForm, "IDH_CUST")
                        '                            Dim sName As String = getItemValue(oForm, "IDH_CUSTNM")
                        '                            If sCode.StartsWith("*") OrElse sCode.EndsWith("*") Then
                        '                                setItemValue(oForm, "IDH_CUSTNM", "")
                        '                            ElseIf sName.StartsWith("*") OrElse sName.EndsWith("*") Then
                        '                                setItemValue(oForm, "IDH_CUST", "")
                        '                            End If
                        '                            doChooseCustomer(oForm, True)
                        '                        ElseIf sItemID = "IDH_WPRODU" OrElse sItemID = "IDH_WNAM" Then
                        '                            Dim sCode As String = getItemValue(oForm, "IDH_WPRODU")
                        '                            Dim sName As String = getItemValue(oForm, "IDH_WNAM")
                        '                            If sCode.StartsWith("*") OrElse sCode.EndsWith("*") Then
                        '                                setItemValue(oForm, "IDH_WNAM", "")
                        '                            ElseIf sName.StartsWith("*") OrElse sName.EndsWith("*") Then
                        '                                setItemValue(oForm, "IDH_WPRODU", "")
                        '                            End If
                        '                            doChooseProducer(oForm, True)
                        '                        End If
                    ElseIf pVal.BeforeAction = True Then
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then

                'CR 20150601: The Credit Check build for USA Clients will now be used for all under new WR Config Key "NEWCRCHK" 
                ''For USA Release Only 
                ''If pVal.ItemUID = "IDH_STATUS" AndAlso Config.INSTANCE.getParameterAsBool("USAREL", False) Then

                If pVal.ItemUID = "IDH_STATUS" AndAlso Config.INSTANCE.getParameterAsBool("NEWCRCHK", False) Then
                    If pVal.BeforeAction = True Then
                        setSharedData(oForm, "OLDWOSTAT", getItemValue(oForm, "IDH_STATUS"))
                    Else
                        If getSharedData(oForm, "OLDWOSTAT") = 6 Then
                            If com.idh.bridge.res.Messages.INSTANCE.doResourceMessageYN("GENCHGS", Nothing, 1) = 1 Then
                                'start activity as modal form
                                setSharedData(oForm, "IDH_WOH", getItemValue(oForm, "IDH_BOOREF"))
                                setSharedData(oForm, "IDH_WOR", "")
                                setSharedData(oForm, "IDH_CUST", getItemValue(oForm, "IDH_CUST"))
                                setSharedData(oForm, "sFMode", SAPbouiCOM.BoFormMode.fm_ADD_MODE.ToString())
                                'set activity add mode 
                                setSharedData(oForm, "WODRFTACTY", "true")
                                goParent.doOpenModalForm("651", oForm)
                            Else
                                'reset status back to old 
                                setItemValue(oForm, "IDH_STATUS", getSharedData(oForm, "OLDWOSTAT"))
                            End If
                        End If
                    End If
                End If
            End If
            Return False
        End Function

        Protected Overridable Sub setOrigin(ByVal oForm As SAPbouiCOM.Form, ByVal sOriginCd As String)
            setFormDFValue(oForm, "U_Origin", sOriginCd)
        End Sub

        Protected Overridable Sub doChooseOrigin(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, ByVal sOriginCd As String, ByVal sItemID As String)
            '## Start 15-07-2013
            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                Return
            End If
            If getSharedData(oForm, "IDH_ORGSROPEND") Is Nothing OrElse getSharedData(oForm, "IDH_ORGSROPEND").ToString.Trim = "" Then
            Else
                Exit Sub
            End If
            '## End
            If sOriginCd = "*" Then
                sOriginCd = ""
            End If
            setSharedData(oForm, "IDH_ORCODE", sOriginCd)
            setSharedData(oForm, "CALLEDITEM", sItemID)
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            goParent.doOpenModalForm("IDHEAORSR", oForm)
        End Sub

        Protected Overridable Sub doChooseRoute(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, ByVal sItemID As String)
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            setSharedData(oForm, "CALLEDITEM", sItemID)
            setSharedData(oForm, "IDH_RTCODE", getItemValue(oForm, sItemID))
            goParent.doOpenModalForm("IDHROSRC", oForm)
        End Sub

        Protected Overridable Sub doChooseCustomer(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, ByVal sItemID As String)
            'Dim sCardCode As String =  getItemValue(oForm, "IDH_CUST"))
            'Dim sCardName As String =  getItemValue(oForm, "IDH_CUSTNM"))

            '##Start 12-07-2013
            'Not to open BP Search if form is in Find mode
            '20150628 - Start - Louis - why are you blocking this - this can cause incorrect customer codes for one
            'If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
            'Return
            'End If
            '20150628 - End - Louis
            '## End

            Dim sCardCode As String = getFormDFValue(oForm, "U_CardCd", True)
            Dim sCardName As String = getFormDFValue(oForm, "U_CardNM", True)

            '## MA Start 07-07-2014' remove carcode and cardname length condition, as when it send blank, BP search loaded all BPs, where as it need to open customer only here
            'sCardCode.Length > 0 OrElse sCardName.Length > 0 OrElse _
            '## MA End 07-07-2014
            If Config.INSTANCE.getParameterAsBool(msOrderType & "BPFLT", True) = False Then
                setSharedData(oForm, "IDH_TYPE", "")
                setSharedData(oForm, "IDH_GROUP", "")
            Else
                setSharedData(oForm, "IDH_TYPE", "F-C")
            End If

            setSharedData(oForm, "IDH_BPCOD", sCardCode)
            setSharedData(oForm, "IDH_NAME", sCardName)
            setSharedData(oForm, "TRG", "CU")
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            setSharedData(oForm, "CALLEDITEM", sItemID)
            '## Start 18-06-2013
            'If oForm.TypeEx = "IDH_DISPORD" OrElse oForm.TypeEx = "IDH_WASTORD" Then
            If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
                goParent.doOpenModalForm("IDHCSRCH", oForm) 'Actual BP Search Form 
            Else
                If (getSharedData(oForm, "IDH_BPSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_BPSRCHOPEND").ToString.Trim = "") Then
                    goParent.doOpenModalForm("IDHNCSRCH", oForm) 'NEW BP Search Form 
                End If
            End If
            'Else
            'goParent.doOpenModalForm("IDHCSRCH", oForm)
            'End If
            '## End
        End Sub

        Protected Overridable Sub doChooseSite(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, ByVal sItemID As String)
            'Dim sCardCode As String = getItemValue(oForm, "IDH_DISSIT")
            'Dim sCardName As String = getItemValue(oForm, "IDH_DISNAM")
            Dim sCardCode As String = getFormDFValue(oForm, "U_SCardCd", True)
            Dim sCardName As String = getFormDFValue(oForm, "U_SCardNM", True)

            If sCardCode.Length > 0 OrElse
                sCardName.Length > 0 OrElse
                Config.INSTANCE.getParameterAsBool(msOrderType & "BPFLT", True) = False Then
                setSharedData(oForm, "IDH_TYPE", "")
                setSharedData(oForm, "IDH_GROUP", "")
            Else
                setSharedData(oForm, "IDH_TYPE", "S")
                setSharedData(oForm, "IDH_GROUP", Config.Parameter("BGSDispo"))
            End If

            setSharedData(oForm, "IDH_BPCOD", sCardCode)
            setSharedData(oForm, "IDH_NAME", sCardName)

            setSharedData(oForm, "TRG", "ST")
            setSharedData(oForm, "CALLEDITEM", sItemID)
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub

        Protected Overridable Sub doChooseCarrier(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, ByVal sItemID As String)
            '##Start 12-07-2013
            'Not to open BP Search if form is in Find mode
            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                Return
            End If
            '## End
            setSharedData(oForm, "TRG", "WC")
            Dim sCardCode As String = getFormDFValue(oForm, "U_CCardCd", True)
            Dim sCardName As String = getFormDFValue(oForm, "U_CCardNM", True)

            If ((sCardCode.Length > 0 OrElse
            sCardName.Length > 0) AndAlso sCardCode.Trim <> "*") OrElse
            Config.INSTANCE.getParameterAsBool(msOrderType & "BPFLT", True) = False Then
                setSharedData(oForm, "IDH_TYPE", "")
                setSharedData(oForm, "IDH_GROUP", "")
            Else


                setSharedData(oForm, "IDH_TYPE", "S")
                setSharedData(oForm, "IDH_GROUP", Config.Parameter("BGSWCarr"))
            End If

            setSharedData(oForm, "IDH_BPCOD", sCardCode)
            setSharedData(oForm, "IDH_NAME", sCardName)

            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            'goParent.doOpenModalForm("IDHCSRCH", oForm)
            '## Start 18-06-2013
            'If oForm.TypeEx = "IDH_DISPORD" OrElse oForm.TypeEx = "IDH_WASTORD" Then
            setSharedData(oForm, "CALLEDITEM", sItemID)
            If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
                goParent.doOpenModalForm("IDHCSRCH", oForm) 'Actual BP Search Form 
            Else
                If (getSharedData(oForm, "IDH_BPSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_BPSRCHOPEND").ToString.Trim = "") Then
                    goParent.doOpenModalForm("IDHNCSRCH", oForm) 'NEW BP Search Form 
                End If
            End If
            'Else
            'goParent.doOpenModalForm("IDHCSRCH", oForm)
            'End If
            '## End
        End Sub

        Protected Overridable Sub doChooseLic(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, ByVal sItemID As String)
            setSharedData(oForm, "TRG", "LC")

            If Config.INSTANCE.getParameterAsBool(msOrderType & "BPFLT", True) = False Then
                setSharedData(oForm, "IDH_NAME", "")
                setSharedData(oForm, "IDH_TYPE", "")
                setSharedData(oForm, "IDH_GROUP", "")
            Else
                setSharedData(oForm, "IDH_TYPE", "F-S")
                setSharedData(oForm, "IDH_GROUP", Config.Parameter("BGSLicen"))
            End If
            setSharedData(oForm, "IDH_BPCOD", getItemValue(oForm, "IDH_LICSUP"))
            setSharedData(oForm, "CALLEDITEM", sItemID)
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub
        Protected Sub doChooseMailingAddress(ByVal oform As SAPbouiCOM.Form, ByVal sItemID As String)
            Dim sCardCode As String = Nothing
            sCardCode = getFormDFValue(oform, "U_CardCd")
            setSharedData(oform, "TRG", sItemID)
            setSharedData(oform, "IDH_CUSCOD", sCardCode)
            'setSharedData(oForm, "IDH_ADRTYP", "S")
            setSharedData(oform, "IDH_MADD", "Y")
            setSharedData(oform, "CALLEDITEM", sItemID)
            goParent.doOpenModalForm("IDHMASRCH", oform)

        End Sub

        Protected Overridable Sub doChooseProducer(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, ByVal sItemID As String)
            Dim sCardCode As String = getFormDFValue(oForm, "U_PCardCd", True)
            Dim sCardName As String = getFormDFValue(oForm, "U_PCardNM", True)
            If sCardCode.Length = 0 AndAlso sCardName.Length = 0 Then
                sCardCode = getFormDFValue(oForm, "U_CardCd", True)
            End If

            setSharedData(oForm, "TRG", "PR")

            If sCardCode.Length > 0 OrElse
             sCardName.Length > 0 OrElse
             Config.INSTANCE.getParameterAsBool(msOrderType & "BPFLT", True) = False Then
                setSharedData(oForm, "IDH_TYPE", "")
                setSharedData(oForm, "IDH_GROUP", "")
            Else
                setSharedData(oForm, "IDH_TYPE", "C")
                setSharedData(oForm, "IDH_GROUP", Config.Parameter("BGCProdu"))
            End If

            setSharedData(oForm, "IDH_BPCOD", sCardCode)
            setSharedData(oForm, "IDH_NAME", sCardName)
            setSharedData(oForm, "CALLEDITEM", sItemID)
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub

        Public Overridable Sub doCreateFindEntry(ByVal oForm As SAPbouiCOM.Form)
            setFormDFValue(oForm, "Code", "", True)
            setFormDFValue(oForm, "U_CardCd", "", True)
            setFormDFValue(oForm, "Name", "")

            setEnableItem(oForm, False, "IDH_CNA")
            '            setEnableItem(oForm, False, "IDH_CUSRF")

            ''setFormDFValue(oForm, "U_UseBPWgt", "")

            setFormDFValue(oForm, "U_User", "")
            setFormDFValue(oForm, "U_CntrNo", "")
            setFormDFValue(oForm, "U_Status", "")
            setFormDFValue(oForm, "U_BDate", "")
            setFormDFValue(oForm, "U_BTime", "")
            setFormDFValue(oForm, "U_RSDate", "")
            setFormDFValue(oForm, "U_RSTime", "")
            setFormDFValue(oForm, "U_REDate", "")
            setFormDFValue(oForm, "U_RETime", "")

            '** CUSTOMER
            setFormDFValue(oForm, "U_CardCd", "")
            setFormDFValue(oForm, "U_CardNM", "")
            setFormDFValue(oForm, "U_CusFrnNm", "")

            setFormDFValue(oForm, "U_Contact", "")
            setFormDFValue(oForm, "U_Address", "")
            setFormDFValue(oForm, "U_AddrssLN", "")
            setFormDFValue(oForm, "U_Street", "")
            setFormDFValue(oForm, "U_Block", "")
            setFormDFValue(oForm, "U_City", "")
            setFormDFValue(oForm, "U_State", "")
            setFormDFValue(oForm, "U_County", "")
            setFormDFValue(oForm, "U_ZpCd", "")
            setFormDFValue(oForm, "U_Phone1", "")
            setFormDFValue(oForm, "U_SiteTl", "")
            setFormDFValue(oForm, "U_SteId", "")
            setFormDFValue(oForm, "U_Route", "")
            setFormDFValue(oForm, "U_Seq", "")
            setFormDFValue(oForm, "U_PremCd", "")
            setFormDFValue(oForm, "U_SiteLic", "")
            setFormDFValue(oForm, "U_CustRef", "")

            '** PRODUCER
            setFormDFValue(oForm, "U_PCardCd", "")
            'setFormDFValue(oForm, "U_PCardCd", "")
            setFormDFValue(oForm, "U_PCardNM", "")
            setFormDFValue(oForm, "U_PContact", "")
            setFormDFValue(oForm, "U_PAddress", "")
            setFormDFValue(oForm, "U_PAddrsLN", "")
            setFormDFValue(oForm, "U_PStreet", "")
            setFormDFValue(oForm, "U_PBlock", "")
            setFormDFValue(oForm, "U_PCity", "")
            setFormDFValue(oForm, "U_PState", "")
            setFormDFValue(oForm, "U_PZpCd", "")
            setFormDFValue(oForm, "U_PPhone1", "")
            setFormDFValue(oForm, "U_ProRef", "")

            '** SITE
            setFormDFValue(oForm, "U_SCardCd", "")
            setFormDFValue(oForm, "U_SCardNM", "")
            setFormDFValue(oForm, "U_SContact", "")
            setFormDFValue(oForm, "U_SAddress", "")
            setFormDFValue(oForm, "U_SAddrsLN", "")
            setFormDFValue(oForm, "U_SStreet", "")
            setFormDFValue(oForm, "U_SBlock", "")
            setFormDFValue(oForm, "U_SCity", "")
            setFormDFValue(oForm, "U_SState", "")
            setFormDFValue(oForm, "U_SZpCd", "")
            setFormDFValue(oForm, "U_SPhone1", "")
            setFormDFValue(oForm, "U_SiteRef", "")

            '** CARRIER
            setFormDFValue(oForm, "U_CCardCd", "")
            setFormDFValue(oForm, "U_CCardNM", "")
            setFormDFValue(oForm, "U_CContact", "")
            setFormDFValue(oForm, "U_CAddress", "")
            setFormDFValue(oForm, "U_CAddrsLN", "")
            setFormDFValue(oForm, "U_CStreet", "")
            setFormDFValue(oForm, "U_CBlock", "")
            setFormDFValue(oForm, "U_CCity", "")
            setFormDFValue(oForm, "U_CState", "")
            setFormDFValue(oForm, "U_CZpCd", "")
            setFormDFValue(oForm, "U_CPhone1", "")

            setFormDFValue(oForm, "U_SupRef", "")
            setFormDFValue(oForm, "U_ItemGrp", "")
            setFormDFValue(oForm, "U_Qty", 1)
            setFormDFValue(oForm, "U_ORoad", "N")
            setFormDFValue(oForm, "U_SLicNr", "")
            setFormDFValue(oForm, "U_SLicExp", "")
            setFormDFValue(oForm, "U_SLicCh", 0)
            setFormDFValue(oForm, "U_SLicSp", "")
            setFormDFValue(oForm, "U_SlicNm", "")

            setFormDFValue(oForm, "U_SpInst", "")
            setFormDFValue(oForm, IDH_JOBENTR._LckPrc, "N")
            setOrigin(oForm, "")
        End Sub

        '** Create a new Entry
        'Public Overridable Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bIsTemp As Boolean = False)
        Public Overridable Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form)
            Dim sTime As String = com.idh.utils.Dates.doTimeToNumStr()
            'Dim iKey As Integer = goParent.goDB.doNextNumber(msNextNum)
            'If bIsTemp Then
            '    iKey = iKey * -1
            'End If
            'Dim sCode As String = com.idh.utils.Conversions.ToString(iKey)
            'Dim sCode As String = DataHandler.doGenerateCode(msNextNumPrefix, msNextNum)
            Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(getNextNumPrefix(), msNextNum)
            setEnableItem(oForm, False, "IDH_CNA")
            '            setEnableItem(oForm, False, "IDH_CUSRF")

            With getFormMainDataSource(oForm)
                .InsertRecord(.Offset)
            End With

            setFormDFValue(oForm, "Code", oNumbers.CodeCode)
            setFormDFValue(oForm, "Name", oNumbers.NameCode)
            setFormDFValue(oForm, "U_User", goParent.goDICompany.UserSignature())
            setFormDFValue(oForm, "U_CntrNo", "")
            setFormDFValue(oForm, "U_Status", "1")
            setFormDFValue(oForm, "U_BDate", goParent.doDateToStr())
            setFormDFValue(oForm, "U_BTime", sTime)
            setFormDFValue(oForm, "U_RSDate", goParent.doDateToStr())
            setFormDFValue(oForm, "U_RSTime", sTime)
            setFormDFValue(oForm, "U_REDate", "")
            setFormDFValue(oForm, "U_RETime", "")
            setFormDFValue(oForm, "U_SpInst", "")

            setFormDFValue(oForm, "U_FirstBP", "None")
            ''setFormDFValue(oForm, "U_UseBPWgt", "")

            '** CUSTOMER
            setFormDFValue(oForm, "U_CardCd", "")
            setFormDFValue(oForm, "U_CardNM", "")
            setFormDFValue(oForm, "U_CusFrnNm", "")
            setFormDFValue(oForm, "U_Contact", "")
            setFormDFValue(oForm, "U_Address", "")
            setFormDFValue(oForm, "U_AddrssLN", "")
            setFormDFValue(oForm, "U_Street", "")
            setFormDFValue(oForm, "U_Block", "")
            setFormDFValue(oForm, "U_City", "")
            setFormDFValue(oForm, "U_State", "")
            setFormDFValue(oForm, "U_County", "")
            setFormDFValue(oForm, "U_ZpCd", "")
            setFormDFValue(oForm, "U_Phone1", "")
            setFormDFValue(oForm, "U_SiteTl", "")
            setFormDFValue(oForm, "U_SteId", "")
            setFormDFValue(oForm, "U_Route", "")
            setFormDFValue(oForm, "U_Seq", "")
            setFormDFValue(oForm, "U_PremCd", "")
            setFormDFValue(oForm, "U_SiteLic", "")
            setFormDFValue(oForm, "U_CustRef", "")

            '** PRODUCER
            setFormDFValue(oForm, "U_PCardCd", "")
            setFormDFValue(oForm, "U_PCardNM", "")
            setFormDFValue(oForm, "U_PContact", "")
            setFormDFValue(oForm, "U_PAddress", "")
            setFormDFValue(oForm, "U_PAddrsLN", "")
            setFormDFValue(oForm, "U_PStreet", "")
            setFormDFValue(oForm, "U_PBlock", "")
            setFormDFValue(oForm, "U_PCity", "")
            setFormDFValue(oForm, "U_PState", "")
            setFormDFValue(oForm, "U_PZpCd", "")
            setFormDFValue(oForm, "U_PPhone1", "")
            setFormDFValue(oForm, "U_ProRef", "")

            '** SITE
            setFormDFValue(oForm, "U_SCardCd", "")
            setFormDFValue(oForm, "U_SCardNM", "")
            setFormDFValue(oForm, "U_SContact", "")
            setFormDFValue(oForm, "U_SAddress", "")
            setFormDFValue(oForm, "U_SAddrsLN", "")
            setFormDFValue(oForm, "U_SStreet", "")
            setFormDFValue(oForm, "U_SBlock", "")
            setFormDFValue(oForm, "U_SCity", "")
            setFormDFValue(oForm, "U_SState", "")
            setFormDFValue(oForm, "U_SZpCd", "")
            setFormDFValue(oForm, "U_SPhone1", "")
            setFormDFValue(oForm, "U_SiteRef", "")

            '** CARRIER
            setFormDFValue(oForm, "U_CCardCd", "")
            setFormDFValue(oForm, "U_CCardNM", "")
            setFormDFValue(oForm, "U_CContact", "")
            setFormDFValue(oForm, "U_CAddress", "")
            setFormDFValue(oForm, "U_CAddrsLN", "")
            setFormDFValue(oForm, "U_CStreet", "")
            setFormDFValue(oForm, "U_CBlock", "")
            setFormDFValue(oForm, "U_CCity", "")
            setFormDFValue(oForm, "U_CState", "")
            setFormDFValue(oForm, "U_CZpCd", "")
            setFormDFValue(oForm, "U_CPhone1", "")
            setFormDFValue(oForm, "U_SupRef", "")
            setFormDFValue(oForm, "U_ItemGrp", "")
            setFormDFValue(oForm, "U_Qty", 1)
            setFormDFValue(oForm, "U_ORoad", "N")
            setFormDFValue(oForm, "U_SLicNr", "")
            setFormDFValue(oForm, "U_SLicExp", "")
            setFormDFValue(oForm, "U_SLicCh", 0)
            setFormDFValue(oForm, "U_SlicSp", "")
            setFormDFValue(oForm, "U_SlicNm", "")

            setFormDFValue(oForm, "U_SpInst", "")
            setFormDFValue(oForm, IDH_JOBENTR._LckPrc, "N")
            setFormDFValue(oForm, IDH_JOBENTR._TRNCd, "")

            setOrigin(oForm, "")
        End Sub

        Protected Overridable Sub doClickFirstFocus(ByVal oForm As SAPbouiCOM.Form)
            doSetFocus(oForm, "IDH_CUST")
        End Sub

        '** The Menu Event handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean

            If pVal.MenuUID = Config.NAV_ADD Then
                If pVal.BeforeAction = False Then
                    oForm.Freeze(True)
                    Try
                        setFormDFValue(oForm, "Code", "")
                        doLoadData(oForm)
                        doSetFocusInAddMode(oForm)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing the menu event - " & oForm.UniqueID & "." & pVal.MenuUID)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try
                End If
            ElseIf pVal.MenuUID = Config.NAV_FIND OrElse
                   pVal.MenuUID = Config.NAV_FIRST OrElse
                   pVal.MenuUID = Config.NAV_LAST OrElse
                   pVal.MenuUID = Config.NAV_NEXT OrElse
                   pVal.MenuUID = Config.NAV_PREV Then
                If pVal.BeforeAction = True Then
                    oForm.Freeze(True)
                Else
                    Try
                        If pVal.MenuUID = Config.NAV_FIND Then
                            doLoadData(oForm)
                            doSetFocusInFindMode(oForm)
                        Else
                            doLoadData(oForm)
                        End If
                    Catch ex As Exception
                    End Try
                    oForm.Freeze(False)
                End If
            ElseIf pVal.MenuUID = Config.NAV_PRINT Then
                If pVal.BeforeAction = True Then
                    doStatusReport(oForm)
                    BubbleEvent = False
                    Return False
                End If
            End If
            Return True
        End Function

        'Open the Status Report
        Protected Overridable Sub doStatusReport(ByVal oForm As SAPbouiCOM.Form)
            Dim sReportFile As String
            sReportFile = Config.Parameter(msOrderType & "Status")

            Dim oParams As New Hashtable
            oParams.Add("DocNum", getFormDFValue(oForm, "Code"))
            oParams.Add("RowNum", "")
            oParams.Add("Status", getFormDFValue(oForm, "U_Status"))

            If (Config.INSTANCE.useNewReportViewer()) Then
                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
            Else
                IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
            End If
        End Sub

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCloseForm(oForm, BubbleEvent)
            ghLastRow.Remove(oForm.UniqueID)
            FilterGrid.doRemoveGrid(oForm, "LINESGRID")

            doClearParentSharedData(oForm)
        End Sub

        Public Overrides Sub doClose()
        End Sub

        Protected Overridable Sub doSwitchToFind(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        Protected Overridable Sub doSwitchToAdd(ByVal oForm As SAPbouiCOM.Form)
        End Sub
        Protected Overridable Sub doSetFocusInAddMode(ByVal oForm As SAPbouiCOM.Form)
        End Sub
        Protected Overridable Sub doSetFocusInFindMode(ByVal oForm As SAPbouiCOM.Form)
        End Sub
        '** Send By Custom Events
        'Return True If All is OK and others need to continue
        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
                If pVal.ItemUID = "LINESGRID" Then
                    If pVal.BeforeAction = True Then
                        Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)

                        Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows
                        Dim iSelectionCount As Integer = oSelected.Count

                        'If oGridN.getGrid().Rows.SelectedRows.Count > 0 Then
                        If iSelectionCount = 1 Then
                            'Hide Duplicate for CBI as they dont want to show the option there 
                            'Credit Check triggering issue. 
                            If Config.INSTANCE.getParameterAsBool("DSPDUPLI", True) Then
                                Dim iRow As Integer
                                iRow = oSelected.Item(0, SAPbouiCOM.BoOrderType.ot_RowOrder)


                                Dim sCurrentJobType As String
                                Dim sJobType As String
                                Dim sItemGrp As String
                                Dim dQty As Double

                                sCurrentJobType = oGridN.doGetFieldValue("U_JobTp", iRow)
                                sItemGrp = oGridN.doGetFieldValue("U_ItmGrp", iRow)
                                dQty = oGridN.getOnSiteQty()

                                Dim oJobSeq As IDH_JOBTYPE_SEQ = IDH_JOBTYPE_SEQ.getInstance()
                                Dim oJobTypes As ArrayList = IDH_JOBTYPE_SEQ.getNextAvailJobs(
                                  com.idh.bridge.lookups.Config.CAT_WO,
                                  sItemGrp,
                                  sCurrentJobType,
                                  dQty)

                                If oJobTypes.Count > 0 Then
                                    Dim iIndex As Integer
                                    For iIndex = 0 To oJobTypes.Count() - 1
                                        Try
                                            oJobSeq.gotoRow(oJobTypes.Item(iIndex))
                                            sJobType = oJobSeq.U_JobTp
                                            If sCurrentJobType.Equals(oJobSeq.U_JobTp) Then
                                                Dim oMenuItem As SAPbouiCOM.MenuItem
                                                oMenuItem = goParent.goApplication.Menus.Item(IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU)

                                                Dim oMenus As SAPbouiCOM.Menus
                                                Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
                                                oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)

                                                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING
                                                oCreationPackage.UniqueID = "DUPLICATE"
                                                'oCreationPackage.String = getTranslatedWord("Duplicate Selection")
                                                oCreationPackage.String = getTranslatedWord("Duplicate Row")
                                                oCreationPackage.Enabled = True
                                                oMenus = oMenuItem.SubMenus
                                                oMenus.AddEx(oCreationPackage)
                                                Exit For
                                            End If
                                        Catch
                                        End Try
                                    Next
                                End If
                            End If
                        End If
                        Return True
                    Else
                        If goParent.goApplication.Menus.Exists("DUPLICATE") Then
                            goParent.goApplication.Menus.RemoveEx("DUPLICATE")
                        End If
                        Return True
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DEL Then
                If pVal.ItemUID = "LINESGRID" Then
                    If pVal.BeforeAction = True Then
                        Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                        oGridN.doCommentDeletion()
                        Return False
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DEL_MULTI Then
                If pVal.ItemUID = "LINESGRID" Then
                    If pVal.BeforeAction = True Then
                        Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, pVal.ItemUID)
                        oGridN.doCommentDeletion()
                        Return False
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
                If pVal.ItemUID = "LINESGRID" Then
                    If pVal.BeforeAction = False Then
                        '            If pVal.oData.MenuUID.Equals("DUPLICATE") Then
                        '                Dim oGridN As UpdateGrid = pVal.oGrid 'UpdateGrid.getInstance(oForm, "LINESGRID")
                        '                Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows()
                        '                If Not oSelected Is Nothing AndAlso oSelected.Count > 0 Then
                        '                    If Config.INSTANCE.getParameterAsBool( "MDDUWA", False) = True Then
                        '                        Dim sMessageID As String
                        '                        If oSelected.Count > 1 Then
                        '                            sMessageID = "JBSDUPRS"
                        'Else
                        '                        	sMessageID = "JBSDUPR"
                        '                        End If
                        '                        If goParent.doResourceMessageYNAsk(sMessageID, Nothing, 1) = 2 Then
                        '                            Return True
                        '                        End If
                        '                    End If
                        '                    'doDuplicateRows(pVal.oEventOrigin)
                        '                    doDuplicateRows(pVal.oGrid)
                        '                End If
                        '            End If
                        If pVal.oData.MenuUID.Equals("DUPLICATE") Then
                            'Check if this is a valid request
                            Dim oGridN As UpdateGrid = pVal.oGrid
                            Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows()
                            If Not oSelected Is Nothing AndAlso oSelected.Count > 0 Then
                                If Config.INSTANCE.getParameterAsBool("MDDUWA", False) = True Then
                                    setSharedData(oForm, "SETDUP", "TRUE")
                                    goParent.doOpenModalForm("IDH_DUPLICATE", oForm)
                                Else
                                    Dim iNoOfCopies As Integer = Convert.ToInt16(Config.ParameterWithDefault("WODUPLNO", "1"))
                                    Dim bCopyADDI As String = Config.Parameter("WOADDITM", False)
                                    Dim bCopyComm As String = Config.Parameter("WOCOMENT", False)
                                    Dim bCopyPrice As String = Config.Parameter("WOCOPYPR", False)
                                    Dim bCopyQty As String = Config.Parameter("WOCOPYQT", False)
                                    Dim bCopyVehReg As String = Config.Parameter("WOCPYVHR", False)
                                    Dim bCopyDriver As String = Config.Parameter("WOCPYDRV", False)
                                    For cnt As Integer = 1 To iNoOfCopies
                                        doDuplicateRows(oGridN, bCopyComm, bCopyADDI, bCopyPrice, bCopyQty, bCopyVehReg, bCopyDriver)
                                    Next
                                End If
                            End If
                        End If
                    End If
                End If
            End If
            Return True
        End Function

        '        Protected Overridable Function doCheckCanDeleteRow(ByVal oGridN As IDHGrid) As Boolean
        '            Dim oSelectedRows As SAPbouiCOM.SelectedRows
        '            oSelectedRows = oGridN.getSBOGrid().Rows.SelectedRows()
        '
        '            Dim iIndex As Integer
        '            Dim iSRow As Integer
        '            Dim iRow As Integer
        '
        '            Dim iSize As Integer = oSelectedRows.Count
        '            If iSize = 0 Then Return True
        '
        '            Dim sList As String = ""
        '
        '            Dim oaRemove As New ArrayList
        '            Dim bDoContinue As Boolean = False
        '            For iIndex = 0 To iSize - 1
        '                iSRow = oSelectedRows.Item(iIndex, SAPbouiCOM.BoOrderType.ot_RowOrder)
        '                iRow = oGridN.getSBOGrid().GetDataTableRowIndex(iSRow)
        '                'iRow = oGridN.getDataTableRowIndex(oSelectedRows, iIndex)
        '
        '                Dim sSOStatus As String = oGridN.doGetFieldValue("U_Status", iRow)
        '                Dim sPOStatus As String = oGridN.doGetFieldValue("U_PStat", iRow)
        '				Dim sRowSta As String = oGridN.doGetFieldValue("U_RowSta", iRow)
        '
        '                If sRowSta.Equals(com.idh.bridge.lookups.FixedValues.getStatusDeleted()) OrElse _
        '                	sSOStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusOrdered()) OrElse _
        '                    sSOStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusInvoiced()) OrElse _
        '                    sPOStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusOrdered()) OrElse _
        '                    sPOStatus.StartsWith(com.idh.bridge.lookups.FixedValues.getStatusInvoiced()) Then
        '                    oaRemove.Add(iSRow)
        '                Else
        '                    bDoContinue = True
        '                End If
        '            Next
        '
        '            If oaRemove.Count > 0 Then
        '                For i As Integer = 0 To oaRemove.Count - 1
        '                    If i > 0 Then
        '                        sList = sList & ","
        '                    End If
        '                    sList = sList & (oaRemove.Item(i) + 1)
        '                    oSelectedRows.Remove(oaRemove.Item(i))
        '                Next
        '                Dim sRows As String = "row"
        '                If oaRemove.Count > 0 Then
        '                    sRows = sRows & "s"
        '                End If
        '                com.idh.bridge.DataHandler.INSTANCE.doError("The selected " & sRows & " has been processed and can not be deleted: " & sList)
        '            End If
        '            Return bDoContinue
        '        End Function

        '		Protected Overridable Function doMarkAsToDeleted(ByVal oGridN As IDHGrid) As Boolean
        '            oGridN.getSBOForm.Freeze(True)
        '            Try
        '                'Step through the selected rows and update
        '                Dim oSelected As SAPbouiCOM.SelectedRows
        '                oSelected = oGridN.getGrid().Rows.SelectedRows()
        '                Dim sDeleted As String = com.idh.bridge.lookups.FixedValues.getStatusDeleted()
        '                If Not oSelected Is Nothing Then
        '                    Dim iRow As Integer
        '                    Dim bFound As Boolean = False
        '                    For iIndex As Integer = 0 To oSelected.Count - 1
        '                        iRow = oGridN.getDataTableRowIndex(oSelected, iIndex)
        '
        '                        Dim sStatus As String
        '                        sStatus = oGridN.doGetFieldValue("r.U_RowSta", iRow)
        '                        If com.idh.bridge.lookups.FixedValues.isOpen(sStatus) OrElse _
        '                        		com.idh.bridge.lookups.FixedValues.isWaitingForOrder(sStatus) OrElse _
        '                            	com.idh.bridge.lookups.FixedValues.isWaitingForInvoice(sStatus) OrElse _
        '                            	com.idh.bridge.lookups.FixedValues.isWaitingForFoc(sStatus) Then
        '	                    	oGridN.doSetFieldValue("r.U_RowSta", iRow, sDeleted)
        '                        End If
        '                    Next
        '                End If
        '            Catch ex As Exception
        '                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error marking as Deleted.")
        '            Finally
        '                oGridN.getSBOForm.Freeze(False)
        '            End Try
        '		End Function

        '**
        ' Create a duplicate of the WO Row
        '**
        Protected Overridable Function doDuplicateWORow(ByVal oForm As SAPbouiCOM.Form,
          ByVal sSrcCode As String,
          ByVal sRowCode As String,
          ByVal sNewJobType As String) As Boolean
            Return True
        End Function

        '**
        ' Create a duplicate WO from the Source WO
        '*
        Protected Function doDuplicateWO(ByVal oForm As SAPbouiCOM.Form, ByVal sSrcCode As String) As String
            oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE

            Dim sNewCode As String = Nothing

            doCreateNewEntry(oForm)
            setWFValue(oForm, "WOCreated", True)

            Dim oTableSrc As SAPbobsCOM.UserTable = Nothing
            Dim sFieldName As String = "DN"
            Dim oValue As Object = "DN"
            Try
                'Step through the selected rows and update
                Dim oField As SAPbobsCOM.Field
                'Dim iwResult As Integer
                Dim swResult As String = Nothing
                oTableSrc = goParent.goDICompany.UserTables.Item(gsFormMainTable.Substring(1))

                If oTableSrc.GetByKey(sSrcCode) Then
                    Dim oExcludeList As ArrayList = New ArrayList()
                    oExcludeList.Add("Code")

                    Dim iFields As Integer = oTableSrc.UserFields.Fields.Count
                    For iCol As Integer = 0 To iFields - 1
                        oField = oTableSrc.UserFields.Fields.Item(iCol)
                        sFieldName = oField.Name
                        If sFieldName.Equals("Code") = False AndAlso
                         sFieldName.Equals("Name") = False AndAlso
                         oExcludeList.Contains(sFieldName) = False Then
                            If sFieldName.Equals("U_Status") Then
                                setFormDFValue(oForm, sFieldName, "1")
                            Else
                                oValue = oField.Value
                                setFormDFValue(oForm, sFieldName, oValue)
                            End If
                        End If
                    Next
                    sNewCode = getFormDFValue(oForm, "Code")
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error duplicating the Row. [" & sFieldName & ", " & oValue & "]")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBDS", {sFieldName, oValue.ToString()})
                sNewCode = Nothing
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oTableSrc)
            End Try
            Return sNewCode
        End Function

        '**
        ' Duplicate the selected rows
        '**
        Private Sub doDuplicateRows(ByVal oGridN As IDHGrid, ByVal bCopyComm As Boolean, ByVal bCopyADDI As Boolean, ByVal bCopyPrice As Boolean, ByVal bCopyQty As Boolean, ByVal bCopyVehReg As Boolean, ByVal bCopyDriver As Boolean)
            oGridN.getSBOForm.Freeze(True)
            Try
                Dim iLastRow As Integer = oGridN.getLastRowIndex()

                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows
                'Dim iCode As Integer

                oSelected = oGridN.getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    oGridN.doAddRows(oSelected.Count)
                    'iCode = com.idh.utils.Conversions.ToString(goParent.goDB.doNextNumber("JOBSCHE", oSelected.Count))
                    'iCode = DataHandler.INSTANCE.getNextNumber(msNextRowNum, oSelected.Count)
                    Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(getNextRowNumPrefix(), msNextRowNum, oSelected.Count)

                    'doDebug("Start Row Copy", 2)
                    DataHandler.INSTANCE.DebugTick2 = "Start Row Copy"
                    Dim iRow As Integer
                    For iIndex As Integer = 0 To oSelected.Count - 1
                        iRow = oGridN.getDataTableRowIndex(oSelected, iIndex)

                        'doDuplicateRow(oGridN, iRow, iLastRow, iCode.ToString(), bCopyComm, bCopyADDI, bCopyPrice, bCopyQty)
                        doDuplicateRow__(oGridN, iRow, iLastRow, oNumbers.doPackCodeCode(iIndex), oNumbers.doPackNameCode(iIndex), bCopyComm, bCopyADDI, bCopyPrice, bCopyQty, bCopyVehReg, bCopyDriver)
                        iLastRow = iLastRow + 1
                        'iCode = iCode + 1
                    Next
                    'doDebug("Duplication Done", 2)
                    DataHandler.INSTANCE.DebugTick2 = "Duplication Done"
                End If

                If Not oGridN.getSBOForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    oGridN.getSBOForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error duplicating the rows.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBD", {Nothing})
            Finally
                oGridN.getSBOForm.Freeze(False)
            End Try
        End Sub

        '		'**
        '		' Creates a duplicate row from the source index
        '		'**
        '        Protected Sub doDuplicateRow(ByVal oGridN As IDHGrid, ByVal iSrcRow As Integer, ByVal iDstRow As Integer, ByVal sNewCode As String, Optional ByVal sCoverageVal As String = Nothing)
        '            oGridN.doCopyRow(iSrcRow, iDstRow)
        '
        '			'Reset the values to their defaults
        '            Dim oDataTable As SAPbouiCOM.DataTable = oGridN.getSBOGrid.DataTable
        '            oDataTable.SetValue(oGridN.doIndexField("Code"), iDstRow, sNewCode)
        '            oDataTable.SetValue(oGridN.doIndexField("Name"), iDstRow, sNewCode)
        '            oDataTable.SetValue(oGridN.doIndexField("U_RowSta"), iDstRow, com.idh.bridge.lookups.FixedValues.getStatusOpen())
        '            oDataTable.SetValue(oGridN.doIndexField("U_Status"), iDstRow, com.idh.bridge.lookups.FixedValues.getStatusOpen())
        '            oDataTable.SetValue(oGridN.doIndexField("U_PayStat"), iDstRow, com.idh.bridge.lookups.FixedValues.getStatusUnPaid())
        '            oDataTable.SetValue(oGridN.doIndexField("U_CCNum"), iDstRow, "")
        '            oDataTable.SetValue(oGridN.doIndexField("U_CCType"), iDstRow, "0")
        '            oDataTable.SetValue(oGridN.doIndexField("U_CCStat"), iDstRow, "")
        '
        '            oDataTable.SetValue(oGridN.doIndexField("U_WROrd"), iDstRow, "")
        '            oDataTable.SetValue(oGridN.doIndexField("U_WRRow"), iDstRow, "")
        '            oDataTable.SetValue(oGridN.doIndexField("U_LnkPBI"), iDstRow, "")
        '            oDataTable.SetValue(oGridN.doIndexField("U_DPRRef"), iDstRow, "")
        '            
        '            oDataTable.SetValue(oGridN.doIndexField("U_Sched"), iDstRow, "N")
        '            oDataTable.SetValue(oGridN.doIndexField("U_USched"), iDstRow, "Y")
        '
        '            oDataTable.SetValue(oGridN.doIndexField("U_PStat"), iDstRow, com.idh.bridge.lookups.FixedValues.getStatusOpen())
        '            oDataTable.SetValue(oGridN.doIndexField("U_SAORD"), iDstRow, "")
        '            oDataTable.SetValue(oGridN.doIndexField("U_TIPPO"), iDstRow, "")
        '            oDataTable.SetValue(oGridN.doIndexField("U_JOBPO"), iDstRow, "")
        '            oDataTable.SetValue(oGridN.doIndexField("U_ProPO"), iDstRow, "")
        '            oDataTable.SetValue(oGridN.doIndexField("U_GRIn"), iDstRow, "")
        '            oDataTable.SetValue(oGridN.doIndexField("U_ProGRPO"), iDstRow, "")
        '            oDataTable.SetValue(oGridN.doIndexField("U_SODlvNot"), iDstRow, "")
        '            oDataTable.SetValue(oGridN.doIndexField("U_SAINV"), iDstRow, "")
        '            oDataTable.SetValue(oGridN.doIndexField("U_SLPO"), iDstRow, "")
        '            oDataTable.SetValue(oGridN.doIndexField("U_TPCN"), iDstRow, "")
        '            oDataTable.SetValue(oGridN.doIndexField("U_TCCN"), iDstRow, "")
        '			oDataTable.SetValue(oGridN.doIndexField("U_MDChngd"), iDstRow, "")
        '            
        '            oDataTable.SetValue(oGridN.doIndexField("U_ASDate"), iDstRow, "")
        '            oDataTable.SetValue(oGridN.doIndexField("U_AEDate"), iDstRow, "")
        '            oDataTable.SetValue(oGridN.doIndexField("U_ASTime"), iDstRow, 0)
        '            oDataTable.SetValue(oGridN.doIndexField("U_AETime"), iDstRow, 0)
        '
        '            oDataTable.SetValue(oGridN.doIndexField("U_JobRmD"), iDstRow, "")
        '            oDataTable.SetValue(oGridN.doIndexField("U_JobRmT"), iDstRow, "0")
        '            oDataTable.SetValue(oGridN.doIndexField("U_RemNot"), iDstRow, "")
        '            oDataTable.SetValue(oGridN.doIndexField("U_RemCnt"), iDstRow, "0")
        '
        '            oDataTable.SetValue(oGridN.doIndexField("U_SLicCh"), iDstRow, "0")
        '
        '            Dim sUseTodaysDate As String = Config.Parameter( "MDDPTD" )
        '           	Dim sNowDate As String = goParent.doDateToStr()
        '           	Dim sNowTime As String = com.idh.utils.dates.doTimeToNumStr()
        '            If Not sUseTodaysDate Is Nothing AndAlso sUseTodaysDate.ToUpper.Equals("TRUE") Then
        '                oDataTable.SetValue(oGridN.doIndexField("U_RDate"), iDstRow, sNowDate)
        '            End If
        '            oDataTable.SetValue(oGridN.doIndexField("U_BDate"), iDstRow, sNowDate)
        '            oDataTable.SetValue(oGridN.doIndexField("U_BTime"), iDstRow, sNowTime)
        '            
        '            '** Clear the Weights 
        '            
        '            '** Might have to recaculate the prices for the selected date
        '            
        '            
        '            If Not sCoverageVal Is Nothing Then
        '            	Dim sSource As String = oDataTable.GetValue(oGridN.doIndexField("U_JobNr"), iSrcRow) & "." & oDataTable.GetValue(oGridN.doIndexField("Code"), iSrcRow)
        '                oDataTable.SetValue(oGridN.doIndexField("U_Comment"), iDstRow, "")  ''Created From: " & sSource)
        '                oDataTable.SetValue(oGridN.doIndexField("U_IntComm"), iDstRow, "")
        '                oDataTable.SetValue(oGridN.doIndexField("U_Covera"), iDstRow, "_" & sCoverageVal & "-" & sSource)
        '                oDataTable.SetValue(oGridN.doIndexField("U_CoverHst"), iDstRow, "")
        '            Else
        '                oDataTable.SetValue(oGridN.doIndexField("U_Comment"), iDstRow, "")  ''Created From: " & sSource)
        '                oDataTable.SetValue(oGridN.doIndexField("U_IntComm"), iDstRow, "")
        '                oDataTable.SetValue(oGridN.doIndexField("U_Covera"), iDstRow, "")
        '                oDataTable.SetValue(oGridN.doIndexField("U_CoverHst"), iDstRow, "")
        '            End If
        '        End Sub

        '**
        ' Creates a duplicate row from the source index
        '**
        Protected Overridable Sub doDuplicateRow__(ByVal oGridN As IDHGrid, ByVal iSrcRow As Integer, ByVal iDstRow As Integer, ByVal sNewCode As String, ByVal sNewName As String,
                                       ByVal bCopyComm As Boolean, ByVal bCopyADDI As Boolean, ByVal bCopyPrice As Boolean, ByVal bCopyQty As Boolean, ByVal bCopyVehReg As Boolean,
                                       ByVal bCopyDriver As Boolean, Optional ByVal sCoverageVal As String = Nothing)
            Dim bUseTodaysDate As String = Config.INSTANCE.getParameterAsBool("MDDPTD", False)
            Dim bReCalculateCostVatTotal As Boolean = False
            Dim sCustCode As String = oGridN.doGetFieldValue(IDH_JOBSHD._CustCd, iSrcRow)
            Dim oCheckDate As DateTime
            If bUseTodaysDate Then
                oCheckDate = Date.Now
            Else
                oCheckDate = oGridN.doGetFieldValue(IDH_JOBSHD._RDate, iSrcRow)
            End If
            ''MA Start 24-02-2015 Issue#605; Validate All BP's on WOR for Active/Forzen status
            If sCustCode.Trim <> "" AndAlso Config.INSTANCE.doCheckBPActive(sCustCode, oCheckDate) = False Then
                'com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer is in-active:The Requested date is in an Inactive Date Range: " + oCheckDate)
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Customer is in-active:The Requested date is in an Inactive Date Range: " + oCheckDate, "ERUSIADG", {com.idh.bridge.Translation.getTranslatedWord("Customer"), oCheckDate.ToString()})
                Exit Sub
            End If
            If (Config.INSTANCE.getParameterAsBool("WOVLIDBP", False)) Then
                Dim sCarrierCode As String = oGridN.doGetFieldValue(IDH_JOBSHD._CarrCd, iSrcRow)
                Dim sProduceCode As String = oGridN.doGetFieldValue(IDH_JOBSHD._ProCd, iSrcRow)
                Dim sDisposalCode As String = oGridN.doGetFieldValue(IDH_JOBSHD._Tip, iSrcRow)
                Dim sSkipLicenseCode As String = oGridN.doGetFieldValue(IDH_JOBSHD._SLicSp, iSrcRow)
                If sCarrierCode.Trim <> "" AndAlso Config.INSTANCE.doCheckBPActive(sCarrierCode, oCheckDate) = False Then
                    'com.idh.bridge.DataHandler.INSTANCE.doUserError("Carrier " & sCarrierCode & " is in-active in the requested date range: " + oCheckDate)
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Carrier " & sCarrierCode & " is in-active in the requested date range: " + oCheckDate, "ERUSIADG",
                                                                       {"Carrier " & sCarrierCode, oCheckDate.ToString()})

                    Exit Sub
                ElseIf sProduceCode.Trim <> "" AndAlso Config.INSTANCE.doCheckBPActive(sProduceCode, oCheckDate) = False Then
                    'com.idh.bridge.DataHandler.INSTANCE.doUserError("Producer " & sProduceCode & " is in-active in the requested date range: " + oCheckDate)
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Producer " & sProduceCode & " is in-active in the requested date range: " + oCheckDate, "ERUSIADG",
                                                                       {"Producer " & sProduceCode, oCheckDate.ToString()})

                    Exit Sub
                ElseIf sDisposalCode.Trim <> "" AndAlso Config.INSTANCE.doCheckBPActive(sDisposalCode, oCheckDate) = False Then
                    'com.idh.bridge.DataHandler.INSTANCE.doUserError("Disposal site " & sDisposalCode & " is in-active in the requested date range: " + oCheckDate)
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Disposal site " & sDisposalCode & " is in-active in the requested date range: " + oCheckDate, "ERUSIADG",
                                                                       {"Disposal site " & sDisposalCode, oCheckDate.ToString()})

                    Exit Sub
                ElseIf sSkipLicenseCode <> "" AndAlso Config.INSTANCE.doCheckBPActive(sSkipLicenseCode, oCheckDate) = False Then
                    'com.idh.bridge.DataHandler.INSTANCE.doUserError("Skip License Supplier " & sSkipLicenseCode & " is in-active in the requested date range: " + oCheckDate)
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Skip License Supplier " & sSkipLicenseCode & " is in-active in the requested date range: " + oCheckDate, "ERUSIADG",
                                                                       {"Skip License Supplier " & sSkipLicenseCode, oCheckDate.ToString()})

                    Exit Sub
                End If
                ''MA End 24-02-2015 Issue#605; Validate All BP's on WOR for Active/Forzen status
            End If
            oGridN.doCopyRow(iSrcRow, iDstRow)

            Dim sSrcCode As String = oGridN.doGetFieldValue(IDH_JOBSHD._Code, iSrcRow)
            'Dim sJobNr As String = oGridN.doGetFieldValue(IDH_JOBSHD._JobNr, iSrcRow)

            'Reset the values to their defaults
            Dim oDataTable As SAPbouiCOM.DataTable = oGridN.getSBOGrid.DataTable
            oDataTable.SetValue(oGridN.doIndexField("Code"), iDstRow, sNewCode)
            oDataTable.SetValue(oGridN.doIndexField("Name"), iDstRow, sNewName)

            'Clear the Site PO on Duplication
            If Config.INSTANCE.getParameterAsBool("WOCLRSPO", False) Then
                Dim sCustRef As String = oGridN.doGetFieldValue("U_CustRef", iDstRow)
                If Not sCustRef Is Nothing AndAlso sCustRef.Length() > 0 Then
                    Dim sJobNumber As String = oGridN.doGetFieldValue("U_JobNr", iDstRow)
                    ''Check the site Customer Ref
                    Dim sCustDRef As String = Config.INSTANCE.doGetCustRef(sJobNumber)
                    If sCustDRef.Length = 0 OrElse sCustDRef.Equals(sCustRef) = False Then
                        oGridN.doSetFieldValue("U_CustRef", iDstRow, "")
                    End If
                End If
            End If

            'Clear All the Weights and Totals
            'BEGIN
            oGridN.doSetFieldValue("U_Ser1", iDstRow, "")
            oGridN.doSetFieldValue("U_Ser2", iDstRow, "")
            oGridN.doSetFieldValue("U_WDt1", iDstRow, "")
            oGridN.doSetFieldValue("U_WDt2", iDstRow, "")

            oGridN.doSetFieldValue("U_Jrnl", iDstRow, "")
            oGridN.doSetFieldValue("U_JStat", iDstRow, "")
            oGridN.doSetFieldValue("U_CBICont", iDstRow, "0")
            oGridN.doSetFieldValue("U_CBIManQty", iDstRow, "0")
            oGridN.doSetFieldValue("U_CBIManUOM", iDstRow, "")

            If bCopyQty = False Then
                'Shared
                oGridN.doSetFieldValue(IDH_JOBSHD._Wei1, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._Wei2, iDstRow, 0)

                'Cost
                oGridN.doSetFieldValue(IDH_JOBSHD._TAUOMQt, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._TipWgt, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._TRdWgt, iDstRow, 0)
                'If bClearHaulage = True Then
                oGridN.doSetFieldValue(IDH_JOBSHD._OrdWgt, iDstRow, 1)
                'End If
                oGridN.doSetFieldValue(IDH_JOBSHD._PAUOMQt, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._PRdWgt, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._ProWgt, iDstRow, 0)

                'Charge
                oGridN.doSetFieldValue(IDH_JOBSHD._AUOMQt, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._RdWgt, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._CstWgt, iDstRow, 0)
                'If bClearHaulage = True Then
                oGridN.doSetFieldValue(IDH_JOBSHD._CusQty, iDstRow, 1)
                oGridN.doSetFieldValue(IDH_JOBSHD._HlSQty, iDstRow, 1)
                'End If
                oGridN.doSetFieldValue(IDH_JOBSHD._CarWgt, iDstRow, 1)

                'Add items
                oGridN.doSetFieldValue(IDH_JOBSHD._TAddChrg, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._TAddChVat, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._AddCharge, iDstRow, 0)

                oGridN.doSetFieldValue(IDH_JOBSHD._AddCost, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._TAddCost, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._TAddCsVat, iDstRow, 0)
            End If
            'End If

            'Price Clear Start
            If bCopyPrice = False OrElse bCopyQty = False Then
                If bCopyPrice = False Then
                    'Cost
                    oGridN.doSetFieldValue(IDH_JOBSHD._TipCost, iDstRow, 0)
                    oGridN.doSetFieldValue(IDH_JOBSHD._OrdCost, iDstRow, 0)
                    oGridN.doSetFieldValue(IDH_JOBSHD._PCost, iDstRow, 0)

                    'Charge
                    oGridN.doSetFieldValue(IDH_JOBSHD._TCharge, iDstRow, 0)
                    oGridN.doSetFieldValue(IDH_JOBSHD._CusChr, iDstRow, 0)

                    'Add items
                    oGridN.doSetFieldValue(IDH_JOBSHD._TAddChrg, iDstRow, 0)
                    oGridN.doSetFieldValue(IDH_JOBSHD._TAddChVat, iDstRow, 0)
                    oGridN.doSetFieldValue(IDH_JOBSHD._AddCharge, iDstRow, 0)

                    oGridN.doSetFieldValue(IDH_JOBSHD._AddCost, iDstRow, 0)
                    oGridN.doSetFieldValue(IDH_JOBSHD._TAddCost, iDstRow, 0)
                    oGridN.doSetFieldValue(IDH_JOBSHD._TAddCsVat, iDstRow, 0)
                End If

                'Cost
                oGridN.doSetFieldValue(IDH_JOBSHD._TipTot, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._OrdTot, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._PCTotal, iDstRow, 0)

                oGridN.doSetFieldValue(IDH_JOBSHD._VtCostAmt, iDstRow, 0)
                'oGridN.doSetFieldValue(IDH_JOBSHD._TAddCost, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._JCost, iDstRow, 0)

                'Charge
                oGridN.doSetFieldValue(IDH_JOBSHD._SLicCTo, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._SLicCh, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._CongCh, iDstRow, 0)

                oGridN.doSetFieldValue(IDH_JOBSHD._TCTotal, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._Price, iDstRow, 0)

                oGridN.doSetFieldValue(IDH_JOBSHD._Discnt, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._DisAmt, iDstRow, 0)

                oGridN.doSetFieldValue(IDH_JOBSHD._TaxAmt, iDstRow, 0)
                'oGridN.doSetFieldValue(IDH_JOBSHD._TAddChrg, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._Total, iDstRow, 0)
            End If
            'Price Clear End

            oGridN.doSetFieldValue(IDH_JOBSHD._JobRmD, iDstRow, "")
            oGridN.doSetFieldValue(IDH_JOBSHD._JobRmT, iDstRow, 0)
            oGridN.doSetFieldValue(IDH_JOBSHD._RemNot, iDstRow, "")
            oGridN.doSetFieldValue(IDH_JOBSHD._RemCnt, iDstRow, 0)

            'New Duplicate comments logic as per Duplication dialogue 
            Dim sComment As String = oGridN.doGetFieldValue(IDH_JOBSHD._Comment, iDstRow)
            Dim sCommentInt As String = oGridN.doGetFieldValue(IDH_JOBSHD._IntComm, iDstRow)
            Dim sExtComm1 As String = oGridN.doGetFieldValue(IDH_JOBSHD._ExtComm1, iDstRow)
            Dim sExtComm2 As String = oGridN.doGetFieldValue(IDH_JOBSHD._ExtComm2, iDstRow)
            If bCopyComm Then
                oGridN.doSetFieldValue(IDH_JOBSHD._Comment, iDstRow, sComment)
                oGridN.doSetFieldValue(IDH_JOBSHD._IntComm, iDstRow, sCommentInt)
                oGridN.doSetFieldValue(IDH_JOBSHD._ExtComm1, iDstRow, sExtComm1)
                oGridN.doSetFieldValue(IDH_JOBSHD._ExtComm2, iDstRow, sExtComm2)
            Else
                oGridN.doSetFieldValue(IDH_JOBSHD._Comment, iDstRow, "")
                oGridN.doSetFieldValue(IDH_JOBSHD._IntComm, iDstRow, "")
                oGridN.doSetFieldValue(IDH_JOBSHD._ExtComm1, iDstRow, "")
                oGridN.doSetFieldValue(IDH_JOBSHD._ExtComm2, iDstRow, "")
            End If

            oGridN.doSetFieldValue(IDH_JOBSHD._User, iDstRow, goParent.gsUserName)
            oGridN.doSetFieldValue(IDH_JOBSHD._PayMeth, iDstRow, "")

            oGridN.doSetFieldValue(IDH_JOBSHD._CCExp, iDstRow, "")
            oGridN.doSetFieldValue(IDH_JOBSHD._CCIs, iDstRow, "")
            oGridN.doSetFieldValue(IDH_JOBSHD._CCSec, iDstRow, "")
            oGridN.doSetFieldValue(IDH_JOBSHD._CCHNum, iDstRow, "")
            oGridN.doSetFieldValue(IDH_JOBSHD._CCPCd, iDstRow, "")

            oGridN.doSetFieldValue(IDH_JOBSHD._WastTNN, iDstRow, "")
            oGridN.doSetFieldValue(IDH_JOBSHD._HazWCNN, iDstRow, "")
            oGridN.doSetFieldValue(IDH_JOBSHD._SiteRef, iDstRow, "") 'Not Sure why this gets cleared
            oGridN.doSetFieldValue(IDH_JOBSHD._ExtWeig, iDstRow, "")

            'MA Start 22-06-2016 Issue#1181; use user decision to leave or clear the fields
            'If Config.INSTANCE.getParameterAsBool("MDDPVE", True) Then
            '    oGridN.doSetFieldValue("U_Lorry", iDstRow, "")
            '    oGridN.doSetFieldValue("U_LorryCd", iDstRow, "")
            '    oGridN.doSetFieldValue("U_Driver", iDstRow, "")
            '    oGridN.doSetFieldValue("U_TRLReg", iDstRow, "")
            '    oGridN.doSetFieldValue("U_TRLNM", iDstRow, "")
            '    oGridN.doSetFieldValue("U_VehTyp", iDstRow, "")
            'End If

            'MA End 22-06-2016 Issue#1181
            If Config.INSTANCE.getParameterAsBool("WORCLRCA", False) Then
                oGridN.doSetFieldValue("U_CarrCd", iDstRow, "")
                oGridN.doSetFieldValue("U_CarrNm", iDstRow, "")
                oGridN.doSetFieldValue("U_SupRef", iDstRow, "")
                If CDbl(oGridN.doGetFieldValue(IDH_JOBSHD._OrdTot, iDstRow)) <> 0.0 Then
                    bReCalculateCostVatTotal = True
                End If
                oGridN.doSetFieldValue(IDH_JOBSHD._OrdTot, iDstRow, 0)
            End If
            If (bCopyVehReg = False) Then
                oGridN.doSetFieldValue("U_VehTyp", iDstRow, "")
                oGridN.doSetFieldValue("U_Lorry", iDstRow, "")
            End If
            If (bCopyDriver = False) Then
                oGridN.doSetFieldValue("U_Driver", iDstRow, "")
            End If

            Dim sUseTodaysDate As String = Config.Parameter("MDDPTD")
            Dim dNowDate As DateTime = DateTime.Now
            'If Not sUseTodaysDate Is Nothing AndAlso sUseTodaysDate.ToUpper.Equals("TRUE") Then
            If Config.ParameterAsBool("MDDPTD", False) = True Then
                oGridN.doSetFieldValue("U_RDate", iDstRow, dNowDate) 'oParent.doDateToStr())
            End If

            '?? Not the same as the DO 
            Dim sNowTime As String = com.idh.utils.Dates.doTimeToNumStr()
            oGridN.doSetFieldValue("U_BDate", iDstRow, dNowDate)
            oGridN.doSetFieldValue("U_BTime", iDstRow, sNowTime)

            '?? This was not aligned with the BO 
            If Config.ParameterAsBool("MDDPTT", False) = True Then
                oGridN.doSetFieldValue("U_RTime", iDstRow, sNowTime)
                oGridN.doSetFieldValue("U_RTimeT", iDstRow, sNowTime)
            End If

            '##MA Start 10-09-2014 Issue#406
            ''Update Job to be done as per new booking date
            If Config.INSTANCE.getParameter("CTOFJBHR") <> "" AndAlso Config.INSTANCE.getParameter("CTOFJBHR") <> "0" _
                        AndAlso Config.ParamaterWithDefault("MAXJOBHR", "0") <> "0" Then
                Dim cuttoffhrs As DateTime = Today.ToString("yyyy/MM/dd") & " " & Config.INSTANCE.getParameter("CTOFJBHR")
                Dim BTime As DateTime = DateTime.Now 'goParent.doStrToTime(oGridN.doGetFieldValue("U_BTime"))
                If BTime.TimeOfDay <= cuttoffhrs.TimeOfDay Then
                    'MAXJOBHR 
                    'Dim BDate As DateTime = Today 'goParent.doStrToDate(goParent.doDateToStr(oGridN.doGetFieldValue("U_BDate"), True))
                    'BTime = BDate.Add(BTime.TimeOfDay)

                    BTime = BTime.AddHours(CInt(Config.Parameter("MAXJOBHR")))
                    oGridN.doSetFieldValue("U_JbToBeDoneTm", com.idh.utils.Dates.doTimeToNumStr(BTime))
                    oGridN.doSetFieldValue("U_JbToBeDoneDt", (BTime)) 'goParent.doDateToStr
                Else
                    BTime = "08:00" 'if BDate is beyond the cuttof time then set to 8AM of next Day( Hayden to be confirm from Mike)
                    oGridN.doSetFieldValue("U_JbToBeDoneTm", goParent.doTimeStrToInt(BTime))
                    Dim BDate As DateTime = Today.AddDays(1) 'goParent.doStrToDate(goParent.doDateToStr(oGridN.doGetFieldValue("U_BDate"), True)).AddDays(1)
                    oGridN.doSetFieldValue("U_JbToBeDoneDt", (BDate)) 'goParent.doDateToStr(BDate))
                End If
                'Else
            End If
            '##MA End 10-09-2014 Issue#406

            oDataTable.SetValue(oGridN.doIndexField("U_RowSta"), iDstRow, com.idh.bridge.lookups.FixedValues.getStatusOpen())

            oDataTable.SetValue(oGridN.doIndexField("U_CCNum"), iDstRow, "")
            oDataTable.SetValue(oGridN.doIndexField("U_CCType"), iDstRow, "0")
            oDataTable.SetValue(oGridN.doIndexField("U_CCStat"), iDstRow, "")

            oDataTable.SetValue(oGridN.doIndexField("U_WROrd"), iDstRow, "")
            oDataTable.SetValue(oGridN.doIndexField("U_WRRow"), iDstRow, "")
            oDataTable.SetValue(oGridN.doIndexField("U_LnkPBI"), iDstRow, "")
            oDataTable.SetValue(oGridN.doIndexField("U_DPRRef"), iDstRow, "")

            oDataTable.SetValue(oGridN.doIndexField("U_Sched"), iDstRow, "N")
            oDataTable.SetValue(oGridN.doIndexField("U_USched"), iDstRow, "Y")

            Dim sTrnCode As String = oGridN.doGetFieldValue(IDH_JOBSHD._TrnCode, iSrcRow)
            If sTrnCode Is Nothing OrElse sTrnCode.Length = 0 Then
                oDataTable.SetValue(oGridN.doIndexField("U_Status"), iDstRow, com.idh.bridge.lookups.FixedValues.getStatusOpen())
                oDataTable.SetValue(oGridN.doIndexField("U_PayStat"), iDstRow, com.idh.bridge.lookups.FixedValues.getStatusUnPaid())
                oDataTable.SetValue(oGridN.doIndexField("U_PStat"), iDstRow, com.idh.bridge.lookups.FixedValues.getStatusOpen())
            Else
                Dim sStatus As String = oGridN.doGetFieldValue(IDH_JOBSHD._Status, iSrcRow)
                If com.idh.bridge.lookups.FixedValues.getStatusOrdered() = sStatus Then
                    sStatus = com.idh.bridge.lookups.FixedValues.getDoOrderStatus()
                ElseIf com.idh.bridge.lookups.FixedValues.getStatusInvoiced() = sStatus Then
                    sStatus = com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus()
                ElseIf com.idh.bridge.lookups.FixedValues.getStatusFoc() = sStatus Then
                    sStatus = com.idh.bridge.lookups.FixedValues.getDoFocStatus()
                End If

                Dim sPStatus As String = oGridN.doGetFieldValue(IDH_JOBSHD._PStat, iSrcRow)
                If com.idh.bridge.lookups.FixedValues.getStatusOrdered() = sPStatus Then
                    sPStatus = com.idh.bridge.lookups.FixedValues.getDoOrderStatus()
                ElseIf com.idh.bridge.lookups.FixedValues.getStatusInvoiced() = sPStatus Then
                    sPStatus = com.idh.bridge.lookups.FixedValues.getDoInvoiceStatus()
                ElseIf com.idh.bridge.lookups.FixedValues.getStatusFoc() = sPStatus Then
                    sPStatus = com.idh.bridge.lookups.FixedValues.getDoFocStatus()
                End If

                oDataTable.SetValue(oGridN.doIndexField("U_Status"), iDstRow, sStatus)
                'oDataTable.SetValue(oGridN.doIndexField("U_PayStat"), iDstRow, com.idh.bridge.lookups.FixedValues.getStatusUnPaid())
                oDataTable.SetValue(oGridN.doIndexField("U_PStat"), iDstRow, sPStatus)

            End If

            oDataTable.SetValue(oGridN.doIndexField("U_SAORD"), iDstRow, "")
            oDataTable.SetValue(oGridN.doIndexField("U_TIPPO"), iDstRow, "")
            oDataTable.SetValue(oGridN.doIndexField("U_JOBPO"), iDstRow, "")
            oDataTable.SetValue(oGridN.doIndexField("U_ProPO"), iDstRow, "")
            oDataTable.SetValue(oGridN.doIndexField("U_GRIn"), iDstRow, "")
            oDataTable.SetValue(oGridN.doIndexField("U_ProGRPO"), iDstRow, "")
            oDataTable.SetValue(oGridN.doIndexField("U_SODlvNot"), iDstRow, "")
            oDataTable.SetValue(oGridN.doIndexField("U_SAINV"), iDstRow, "")
            oDataTable.SetValue(oGridN.doIndexField("U_SLPO"), iDstRow, "")
            oDataTable.SetValue(oGridN.doIndexField("U_TPCN"), iDstRow, "")
            oDataTable.SetValue(oGridN.doIndexField("U_TCCN"), iDstRow, "")
            oDataTable.SetValue(oGridN.doIndexField("U_MDChngd"), iDstRow, "")
            oDataTable.SetValue(oGridN.doIndexField("U_WHTRNF"), iDstRow, "")

            oDataTable.SetValue(oGridN.doIndexField("U_ASTime"), iDstRow, 0)
            oDataTable.SetValue(oGridN.doIndexField("U_AETime"), iDstRow, 0)

            '            If bDoStart Then
            '            	oAuditObj.setFieldValue("U_ASDate", sNowDate)
            '            	oAuditObj.setFieldValue("U_ASTime", sNowTime)
            '            Else
            oDataTable.SetValue(oGridN.doIndexField("U_ASDate"), iDstRow, "")
            oDataTable.SetValue(oGridN.doIndexField("U_AEDate"), iDstRow, "")
            '            End If

            '            Dim sUseTodaysDate As String = Config.Parameter( "MDDPTD" )
            '           	Dim sNowDate As String = goParent.doDateToStr()
            '           	Dim sNowTime As String = com.idh.utils.dates.doTimeToNumStr()
            '            If Not sUseTodaysDate Is Nothing AndAlso sUseTodaysDate.ToUpper.Equals("TRUE") Then
            '                oDataTable.SetValue(oGridN.doIndexField("U_RDate"), iDstRow, sNowDate)
            '            End If
            '            oDataTable.SetValue(oGridN.doIndexField("U_BDate"), iDstRow, sNowDate)
            '            oDataTable.SetValue(oGridN.doIndexField("U_BTime"), iDstRow, sNowTime)

            '** Clear the Weights 

            '** Might have to recaculate the prices for the selected date

            If Not sCoverageVal Is Nothing Then
                Dim sSource As String = oDataTable.GetValue(oGridN.doIndexField("U_JobNr"), iSrcRow) & "." & oDataTable.GetValue(oGridN.doIndexField("Code"), iSrcRow)
                'oDataTable.SetValue(oGridN.doIndexField("U_Comment"), iDstRow, "")  ''Created From: " & sSource)
                'oDataTable.SetValue(oGridN.doIndexField("U_IntComm"), iDstRow, "")
                oDataTable.SetValue(oGridN.doIndexField("U_Covera"), iDstRow, "_" & sCoverageVal & "-" & sSource)
                oDataTable.SetValue(oGridN.doIndexField("U_CoverHst"), iDstRow, "")
            Else
                'oDataTable.SetValue(oGridN.doIndexField("U_Comment"), iDstRow, "")  ''Created From: " & sSource)
                'oDataTable.SetValue(oGridN.doIndexField("U_IntComm"), iDstRow, "")
                oDataTable.SetValue(oGridN.doIndexField("U_Covera"), iDstRow, "")
                oDataTable.SetValue(oGridN.doIndexField("U_CoverHst"), iDstRow, "")
            End If

            Dim U_VtCostAmt As Double = 0
            If bReCalculateCostVatTotal Then ' this trigger is we are going to clear Haulier and it has some price
                Dim HCostVtRt As Double = 0, TCostVtRt As Double = 0, PCostVtRt As Double = 0

                If Not String.IsNullOrEmpty(oGridN.doGetFieldValue(IDH_JOBSHD._CarrCd, iDstRow)) AndAlso Not String.IsNullOrEmpty(oGridN.doGetFieldValue(IDH_JOBSHD._ItemCd, iDstRow)) AndAlso oGridN.doGetFieldValue(IDH_JOBSHD._OrdTot, iDstRow) <> 0 Then
                    If String.IsNullOrEmpty(oGridN.doGetFieldValue(IDH_JOBSHD._HCostVtGrp, iDstRow)) Then
                        Dim HCostVtGrp As String = Config.INSTANCE.doGetPurchaceVatGroup(oGridN.doGetFieldValue(IDH_JOBSHD._CarrCd, iDstRow), oGridN.doGetFieldValue(IDH_JOBSHD._ItemCd, iDstRow))
                        HCostVtRt = Config.INSTANCE.doGetVatRate(HCostVtGrp, oGridN.doGetFieldValue(IDH_JOBSHD._RDate, iDstRow))
                    Else
                        HCostVtRt = oGridN.doGetFieldValue(IDH_JOBSHD._HCostVtRt, iDstRow)
                    End If
                End If

                If (Not String.IsNullOrEmpty(oGridN.doGetFieldValue(IDH_JOBSHD._Tip, iDstRow)) AndAlso Not String.IsNullOrEmpty(oGridN.doGetFieldValue(IDH_JOBSHD._WasCd, iDstRow)) AndAlso oGridN.doGetFieldValue(IDH_JOBSHD._TipTot, iDstRow) <> 0.0) Then
                    If (String.IsNullOrEmpty(oGridN.doGetFieldValue(IDH_JOBSHD._TCostVtGrp, iDstRow))) Then
                        Dim TCostVtGrp As String = Config.INSTANCE.doGetSalesVatGroup(oGridN.doGetFieldValue(IDH_JOBSHD._Tip, iDstRow), oGridN.doGetFieldValue(IDH_JOBSHD._WasCd, iDstRow))
                        TCostVtRt = Config.INSTANCE.doGetVatRate(TCostVtGrp, oGridN.doGetFieldValue(IDH_JOBSHD._RDate, iDstRow))
                    Else
                        TCostVtRt = oGridN.doGetFieldValue(IDH_JOBSHD._TCostVtRt, iDstRow)
                    End If
                End If
                If (Not String.IsNullOrEmpty(oGridN.doGetFieldValue(IDH_JOBSHD._ProCd, iDstRow)) AndAlso Not String.IsNullOrEmpty(oGridN.doGetFieldValue(IDH_JOBSHD._WasCd, iDstRow)) AndAlso oGridN.doGetFieldValue(IDH_JOBSHD._PCTotal, iDstRow) <> 0.0) Then
                    If (String.IsNullOrEmpty(oGridN.doGetFieldValue(IDH_JOBSHD._PCostVtGrp, iDstRow))) Then
                        Dim PCostVtGrp As String = Config.INSTANCE.doGetSalesVatGroup(oGridN.doGetFieldValue(IDH_JOBSHD._ProCd, iDstRow), oGridN.doGetFieldValue(IDH_JOBSHD._WasCd, iDstRow))
                        PCostVtRt = Config.INSTANCE.doGetVatRate(PCostVtGrp, oGridN.doGetFieldValue(IDH_JOBSHD._RDate, iDstRow))
                    Else
                        PCostVtRt = oGridN.doGetFieldValue(IDH_JOBSHD._PCostVtRt, iDstRow)
                    End If
                End If
                If (TCostVtRt <> 0.0) Then
                    U_VtCostAmt = (oGridN.doGetFieldValue(IDH_JOBSHD._TipTot, iDstRow) * (TCostVtRt / 100))
                End If
                If (PCostVtRt <> 0.0) Then
                    U_VtCostAmt += (oGridN.doGetFieldValue(IDH_JOBSHD._PCTotal, iDstRow) * (PCostVtRt / 100))
                End If
                If (HCostVtRt <> 0) Then
                    U_VtCostAmt = U_VtCostAmt + (oGridN.doGetFieldValue(IDH_JOBSHD._OrdTot, iDstRow) * (HCostVtRt / 100))
                End If
            End If
            If bCopyADDI Then
                Dim oData As WR1_Data.idh.data.AdditionalExpenses = Nothing
                oData = getWFValue(oGridN.getSBOForm(), "JobAdditionalHandler")
                If oData Is Nothing Then
                    'OrderRow.doDuplicateAdditionalItems(goParent, sSrcCode, sNewCode, bCopyPrice, bCopyQty)
                    IDH_WOADDEXP.doDuplicateAdditionalItems(sSrcCode, sNewCode, bCopyPrice, bCopyQty)
                Else
                    oData.doDuplicateRowEntries(sSrcCode, sNewCode, bCopyPrice, bCopyQty)
                End If
                If bReCalculateCostVatTotal Then 'if there is change in Vat then recalc Vat Total and total
                    Dim dblTot_Cost As Double = oGridN.doGetFieldValue(IDH_JOBSHD._JCost, iDstRow)
                    Dim dblTVAT_AddCost As Double = oGridN.doGetFieldValue(IDH_JOBSHD._TAddCsVat, iDstRow)
                    oGridN.doSetFieldValue(IDH_JOBSHD._VtCostAmt, iDstRow, U_VtCostAmt + dblTVAT_AddCost)
                    dblTot_Cost = oGridN.doGetFieldValue(IDH_JOBSHD._TAddCost, iDstRow) + oGridN.doGetFieldValue(IDH_JOBSHD._PCTotal, iDstRow) _
                        + oGridN.doGetFieldValue(IDH_JOBSHD._TipTot, iDstRow) + oGridN.doGetFieldValue(IDH_JOBSHD._OrdTot, iDstRow) _
                        + U_VtCostAmt
                    oGridN.doSetFieldValue(IDH_JOBSHD._JCost, iDstRow, dblTot_Cost)
                End If
            Else
                Dim dblTVAT_Charge As Double = oGridN.doGetFieldValue(IDH_JOBSHD._TaxAmt, iDstRow)
                Dim dblTVAT_Cost As Double = oGridN.doGetFieldValue(IDH_JOBSHD._VtCostAmt, iDstRow)
                Dim dblVAT_AddCharge As Double = oGridN.doGetFieldValue(IDH_JOBSHD._TAddChVat, iDstRow)
                Dim dblTVAT_AddCost As Double = oGridN.doGetFieldValue(IDH_JOBSHD._TAddCsVat, iDstRow)

                Dim dblTot_Cost As Double = oGridN.doGetFieldValue(IDH_JOBSHD._JCost, iDstRow)
                Dim dblTot_Charge As Double = oGridN.doGetFieldValue(IDH_JOBSHD._Total, iDstRow)

                Dim dblTot_AddCost As Double = oGridN.doGetFieldValue(IDH_JOBSHD._TAddCost, iDstRow)
                Dim dblTot_AddCharge As Double = oGridN.doGetFieldValue(IDH_JOBSHD._TAddChrg, iDstRow)

                'dblTot_Cost = dblTot_Cost - dblTot_AddCost
                dblTot_Charge = dblTot_Charge - dblTot_AddCharge

                dblTVAT_Charge = dblTVAT_Charge - dblVAT_AddCharge
                If bReCalculateCostVatTotal Then
                    dblTVAT_Cost = U_VtCostAmt 'as this U_VtCostAmt  Vat is without Addition Item'a Vat so use it staright
                Else
                    dblTVAT_Cost = dblTVAT_Cost - dblTVAT_AddCost
                End If
                'recalculate total without additinal item cost
                dblTot_Cost = oGridN.doGetFieldValue(IDH_JOBSHD._PCTotal, iDstRow) _
                            + oGridN.doGetFieldValue(IDH_JOBSHD._TipTot, iDstRow) + oGridN.doGetFieldValue(IDH_JOBSHD._OrdTot, iDstRow) _
                            + dblTVAT_Cost

                oGridN.doSetFieldValue(IDH_JOBSHD._TaxAmt, iDstRow, dblTVAT_Charge)
                oGridN.doSetFieldValue(IDH_JOBSHD._VtCostAmt, iDstRow, dblTVAT_Cost)

                oGridN.doSetFieldValue(IDH_JOBSHD._JCost, iDstRow, dblTot_Cost)
                oGridN.doSetFieldValue(IDH_JOBSHD._Total, iDstRow, dblTot_Charge)

                oGridN.doSetFieldValue(IDH_JOBSHD._TAddChrg, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._TAddChVat, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._AddCharge, iDstRow, 0)

                oGridN.doSetFieldValue(IDH_JOBSHD._AddCost, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._TAddCost, iDstRow, 0)
                oGridN.doSetFieldValue(IDH_JOBSHD._TAddCsVat, iDstRow, 0)

            End If
            oGridN.doSetFieldValue(IDH_JOBSHD._Sample, iDstRow, "")
            oGridN.doSetFieldValue(IDH_JOBSHD._SampleRef, iDstRow, "")
            oGridN.doSetFieldValue(IDH_JOBSHD._SampStatus, iDstRow, "")

            'If oGridN.doGetFieldValue(IDH_JOBSHD._WasCd, iDstRow) IsNot Nothing AndAlso oGridN.doGetFieldValue(IDH_JOBSHD._WasCd, iDstRow).ToString() <> String.Empty AndAlso
            '    Config.INSTANCE.getParameterAsBool("CPSMRWOR", True) Then
            '    Dim sSampleRef As String = "", sSampleStatus As String = ""
            '    If (com.isb.bridge.utils.General.doCheckIfLabTaskRequired(oGridN.doGetFieldValue(IDH_JOBSHD._WasCd, iDstRow).ToString())) Then
            '        com.isb.bridge.utils.General.doCreateLabItemNTask(oGridN.doGetFieldValue(IDH_JOBSHD._WasCd, iDstRow),
            '        oGridN.doGetFieldValue(IDH_JOBSHD._WasDsc, iDstRow),
            '        "",
            '        oGridN.doGetFieldValue(IDH_JOBSHD._UOM, iDstRow),
            '        oGridN.doGetFieldValue(IDH_JOBSHD._Quantity, iDstRow), oGridN.doGetFieldValue(IDH_JOBSHD._Comment, iDstRow),
            '        oGridN.doGetFieldValue(IDH_JOBSHD._Code, iDstRow), oGridN.doGetFieldValue(IDH_JOBSHD._JobNr, iDstRow), "WOR",
            '        oGridN.doGetFieldValue(IDH_JOBSHD._CustCd, iDstRow), oGridN.doGetFieldValue(IDH_JOBSHD._CustNm, iDstRow), "", False, 0, sSampleRef, sSampleStatus, "",
            '                                                              oGridN.doGetFieldValue(IDH_JOBSHD._RouteCd, iDstRow))
            '        oGridN.doSetFieldValue(IDH_JOBSHD._Sample, iDstRow, "Y")
            '        oGridN.doSetFieldValue(IDH_JOBSHD._SampleRef, iDstRow, sSampleRef)
            '        oGridN.doSetFieldValue(IDH_JOBSHD._SampStatus, iDstRow, sSampleStatus)
            '    End If
            'End If
        End Sub

        ''MODAL DIALOG
        ''*** Get the selected rows
        Protected Overridable Sub doSetModalData(ByVal oForm As SAPbouiCOM.Form)
            Dim sMode As String = ""
            If ghOldDialogParams.Contains(oForm.UniqueID) Then
                Dim oData As ArrayList = ghOldDialogParams.Item(oForm.UniqueID)
                If Not (oData Is Nothing) AndAlso oData.Count > 2 Then
                    If oData.Contains("LinkUpdated") Then
                        sMode = "LinkUpdated"
                    Else
                        sMode = oData.Item(1)
                    End If
                End If
            End If

            Dim aData As New ArrayList
            If sMode.Length = 0 Then
                sMode = "Add"
            ElseIf sMode.Equals("LinkUpdated") = False Then
                sMode = "Change"
            End If
            aData.Add(sMode) '0
            aData.Add(getFormDFValue(oForm, "Code")) '2
            doReturnFromModalBuffered(oForm, aData)
        End Sub

        '        ''** Delete the Order with the linked Order Rows
        '        Public Shared Function doDeleteOrder(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sMDHead As String, ByVal sMDRow As String, ByVal sOrderNum As String) As Boolean
        '            Dim sQry As String = "Delete from [" & sMDRow & "] where U_JobNr = '" & sOrderNum & "'"
        '            If oParent.goDB.doUpdateQuery(sQry) Then
        '                sQry = "Delete from [" & sMDHead & "] where Code = '" & sOrderNum & "'"
        '                Return oParent.goDB.doUpdateQuery(sQry)
        '            End If
        '            Return False
        '        End Function

        ''** Delete Orders linked to the Contract along with it's rows
        Public Shared Function doDeleteLinkedOrders(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sMDHead As String, ByVal sMDRow As String, ByVal sContract As String) As Boolean
            Dim sQry As String = "Delete from [" & sMDRow & "] where Code in (Select cl.Code from [" & sMDHead & "] c, [" & sMDRow & "] cl where c.U_CntrNo = '" & sContract & "' AND c.Code=cl.U_JobNr)"
            If oParent.goDB.doUpdateQuery(sQry) Then
                sQry = "Delete from [" & sMDHead & "] where U_CntrNo = '" & sContract & "'"
                Return oParent.goDB.doUpdateQuery(sQry)
            End If
            Return False
        End Function
        Protected Overridable Sub doCheckProducerAddress(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bFromSearch As Boolean = False)

        End Sub
        Protected Overridable Sub doCheckCustomerAddress(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bFromSearch As Boolean = False)
            Dim sAddress As String
            Dim sCardCode As String
            Dim sPh1 As String

            sAddress = getFormDFValue(oForm, "U_Address")
            sCardCode = getFormDFValue(oForm, "U_CardCd")
            sPh1 = getFormDFValue(oForm, "U_Phone1")

            If sAddress.Length() > 0 Then
                Dim oData1 As ArrayList
                oData1 = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sCardCode, "S", sAddress)
                If oData1 Is Nothing Then
                    setEnableItem(oForm, True, "IDH_CNA")
                    '                    setEnableItem(oForm, True, "IDH_CUSRF")
                Else
                    setEnableItem(oForm, False, "IDH_CNA")
                    doSetCustomerAddress(oForm, oData1, sCardCode, sPh1)
                End If
            ElseIf bFromSearch = False Then
                setSharedData(oForm, "TRG", "IDH_CUSAL")
                setSharedData(oForm, "IDH_CUSCOD", sCardCode)
                setSharedData(oForm, "IDH_ADRTYP", "S")
                ''##MA Start 18-09-2014 Issue#403
                setSharedData(oForm, "IDH_APPLYFLTR", Config.INSTANCE.getParameterAsBool("FLTRADDR", False).ToString)
                ''##MA End 18-09-2014 Issue#403

                '## Start 20-06-2013
                If oForm.TypeEx = "IDH_WASTORD" Then
                    'test.StartTime = Now
                    If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
                        goParent.doOpenModalForm("IDHASRCH", oForm)
                    Else
                        goParent.doOpenModalForm("IDHASRCH3", oForm)
                        'goParent.doOpenModalForm("IDHASRCH", oForm)
                    End If
                Else
                    goParent.doOpenModalForm("IDHASRCH", oForm)
                End If
                '## End
            Else
                setEnableItem(oForm, False, "IDH_CNA")
                '                setEnableItem(oForm, False, "IDH_CUSRF")
            End If
        End Sub

        Protected Overridable Sub doChooseAddress(ByVal oform As SAPbouiCOM.Form, sItemID As String)
            Dim sCardCode As String = Nothing
            Dim sAddress As String = ""
            setSharedData(oform, "SILENT", "SHOWMULTI")
            If sItemID = "IDH_CUSAL" OrElse sItemID = "IDH_CUSADD" OrElse sItemID = "IDH_CUSA2" Then
                If getFormDFValue(oform, "U_CardCd").ToString() = "" Then
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Please select customer code before address search", "WRINNOBP", New String() {"Customer Code"})
                    setFormDFValue(oform, "U_Address", "")
                    Return
                End If
                sCardCode = getFormDFValue(oform, "U_CardCd")
                sAddress = getFormDFValue(oform, "U_Address")
                ''##MA Start 18-09-2014 Issue#403
                setSharedData(oform, "IDH_APPLYFLTR", Config.INSTANCE.getParameterAsBool("FLTRADDR", False).ToString)
                ''##MA End 18-09-2014 Issue#403
                If sItemID = "IDH_CUSAL" Then setSharedData(oform, "SILENT", "")
            ElseIf sItemID = "IDH_WASAL" OrElse sItemID = "IDH_ADDRES" Then
                If getFormDFValue(oform, "U_CCardCd").ToString() = "" Then
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Please select Carrier Code before address search", "WRINNOBP", New String() {"Carrier Code"})
                    setFormDFValue(oform, "U_CAddress", "")
                    Return
                End If
                sCardCode = getFormDFValue(oform, "U_CCardCd")
                sAddress = getFormDFValue(oform, "U_CAddress")
                If sItemID = "IDH_WASAL" Then setSharedData(oform, "SILENT", "")
            ElseIf sItemID = "IDH_PROAL" OrElse sItemID = "IDH_PRDADD" Then
                If getFormDFValue(oform, "U_PCardCd").ToString() = "" Then
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Please select Producer Code before address search", "WRINNOBP", New String() {"Producer Code"})
                    setFormDFValue(oform, "U_PAddress", "")
                    Return
                End If
                sCardCode = getFormDFValue(oform, "U_PCardCd") ''.GetValue("U_PCardCd", .Offset).Trim()
                sAddress = getFormDFValue(oform, "U_PAddress")
                ''##MA Start 18-09-2014 Issue#403
                setSharedData(oform, "IDH_APPLYFLTR", Config.INSTANCE.getParameterAsBool("FLTRADDR", False).ToString)
                ''##MA End 18-09-2014 Issue#403
                If sItemID = "IDH_PROAL" Then setSharedData(oform, "SILENT", "")
            ElseIf sItemID = "IDH_SITAL" OrElse sItemID = "IDH_SITAL2" OrElse sItemID = "IDH_SITADD" OrElse sItemID = "IDH_DISPAD" Then
                If getFormDFValue(oform, "U_SCardCd").ToString() = "" Then
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Please select Disposal Site before address search", "WRINNOBP", New String() {"Disposal Site"})
                    setFormDFValue(oform, "U_SAddress", "")
                    Return
                End If
                sCardCode = getFormDFValue(oform, "U_SCardCd")
                sAddress = getFormDFValue(oform, "U_SAddress")
                ''##MA Start 18-09-2014 Issue#403
                setSharedData(oform, "IDH_APPLYFLTR", Config.INSTANCE.getParameterAsBool("FLTRADDR", False).ToString)
                ''##MA End 18-09-2014 Issue#403
                If sItemID = "IDH_SITAL" OrElse sItemID = "IDH_SITAL2" Then setSharedData(oform, "SILENT", "")
            End If

            setSharedData(oform, "TRG", sItemID)
            setSharedData(oform, "IDH_CUSCOD", sCardCode)
            setSharedData(oform, "IDH_ADRTYP", "S")
            setSharedData(oform, "CALLEDITEM", sItemID)

            setSharedData(oform, "IDH_ADDRES", sAddress)

            '## Start 20-06-2013; 16-07-2013
            If (sItemID = "IDH_SITAL" OrElse sItemID = "IDH_SITAL2" OrElse sItemID = "IDH_CUSAL" OrElse sItemID = "IDH_CUSADD" OrElse sItemID = "IDH_CUSA2" _
                OrElse sItemID = "IDH_WASAL" OrElse sItemID = "IDH_PROAL" OrElse sItemID = "IDH_ADDRES" OrElse sItemID = "IDH_PRDADD" OrElse sItemID = "IDH_SITADD" OrElse sItemID = "IDH_DISPAD") Then
                '(oForm.TypeEx = "IDH_WASTORD" OrElse oForm.TypeEx = "IDH_DISPORD") AndAlso 
                If Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
                    goParent.doOpenModalForm("IDHASRCH", oform)
                Else
                    goParent.doOpenModalForm("IDHASRCH3", oform)
                    'goParent.doOpenModalForm("IDHASRCH", oForm)
                End If
            Else
                goParent.doOpenModalForm("IDHASRCH", oform)
            End If
        End Sub


        Protected Overrides Sub doReturnCanceled(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String)
            'If sModalFormType = "IDHCSRCH" OrElse sModalFormType = "IDHNCSRCH" OrElse sModalFormType = "IDHASRCH" _
            '    OrElse sModalFormType = "IDHASRCH3" OrElse sModalFormType = "IDHMASRCH" Then
            If getParentSharedData(oForm, "CALLEDITEM") IsNot Nothing AndAlso getParentSharedData(oForm, "CALLEDITEM").Trim <> "" Then
                doSetFocus(oForm, getParentSharedData(oForm, "CALLEDITEM"))

            End If
            ' End If
        End Sub

        Protected Overridable Sub setAllRowsValues(ByVal oForm As SAPbouiCOM.Form, ByRef oParams As Hashtable)
            Dim oUpdateGrid As UpdateGrid
            oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")

            Dim iIndex As Integer = 0
            Dim iRows As Integer = oUpdateGrid.getRowCount
            Dim oItem As DictionaryEntry

            For iIndex = 0 To iRows - 1
                oUpdateGrid.setCurrentDataRowIndex(iIndex)
                For Each oItem In oParams
                    oUpdateGrid.doSetFieldValue(oItem.Key, oItem.Value)
                Next
            Next
        End Sub

        'OnTime 32355: WR1_NewDev_Buying&Trading_Add functionality to log Activity with a WOR

        'The sub below will just add Activity form on the Disposal Order Form
        Public Sub doADDActivity(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            Dim oGrid As SAPbouiCOM.Grid

            oItem = oForm.Items.Add("ACTGRID", SAPbouiCOM.BoFormItemTypes.it_GRID)

            With oItem
                .Left = 12
                .Top = 199
                .Width = 778
                .Height = 232
                .FromPane = 13
                .ToPane = 13
                oGrid = .Specific
            End With

            oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single

            oForm.DataSources.DataTables.Add("dtActivity")
            Dim strQry As String = "select ClgCode AS 'Activity Code', ReContact AS 'Date', BeginTime AS 'Time', Details AS 'Remarks', " _
                + " Notes AS 'Notes', U_IDHDO AS 'DO Header', U_IDHDOR AS 'DO Row' from OCLG where " _
                + " U_IDHDO = '-1' OR " _
                + " U_IDHDOR = '-1' "

            oForm.DataSources.DataTables.Item("dtActivity").ExecuteQuery(strQry)
            oGrid.DataTable = oForm.DataSources.DataTables.Item("dtActivity")

            'Setting Columns attributes
            oGrid.Columns.Item(0).Width = 70
            oGrid.Columns.Item(0).Editable = False
            'Add link button on Activity Code Column
            Dim oLinkCol As SAPbouiCOM.EditTextColumn
            oLinkCol = oGrid.Columns.Item("Activity Code")
            oLinkCol.LinkedObjectType = SAPbouiCOM.BoLinkedObject.lf_ContactWithCustAndVend

            oGrid.Columns.Item(1).Width = 60
            oGrid.Columns.Item(1).Editable = False

            oGrid.Columns.Item(2).Width = 60
            oGrid.Columns.Item(2).Editable = False

            oGrid.Columns.Item(3).Width = 130
            oGrid.Columns.Item(3).Editable = False

            oGrid.Columns.Item(4).Width = 230
            oGrid.Columns.Item(4).Editable = False

            oGrid.Columns.Item(5).Visible = False
            oGrid.Columns.Item(6).Visible = False
        End Sub

        Public Sub doLoadActivity(ByVal oForm As SAPbouiCOM.Form, ByRef sHdrCode As String, ByRef sRowCode As String)
            Try
                Dim oGrid As SAPbouiCOM.Grid
                oGrid = oForm.Items.Item("ACTGRID").Specific

                Dim strQry As String = "select ClgCode AS 'Activity Code', ReContact AS 'Date', BeginTime AS 'Time', Details AS 'Remarks', " _
                    + " Notes AS 'Notes', U_IDHDO AS 'DO Header', U_IDHDOR AS 'DO Row' from OCLG where " _
                    + " U_IDHDO = '" + sHdrCode + "' OR " _
                    + " U_IDHDOR = '" + sRowCode + "' "

                oForm.DataSources.DataTables.Item("dtActivity").ExecuteQuery(strQry)

                oGrid.DataTable = oForm.DataSources.DataTables.Item("dtActivity")

                'Setting Columns attributes
                oGrid.Columns.Item(0).Width = 70
                oGrid.Columns.Item(0).Editable = False
                'Add link button on Activity Code Column
                Dim oLinkCol As SAPbouiCOM.EditTextColumn
                oLinkCol = oGrid.Columns.Item("Activity Code")
                oLinkCol.LinkedObjectType = SAPbouiCOM.BoLinkedObject.lf_ContactWithCustAndVend

                oGrid.Columns.Item(1).Width = 60
                oGrid.Columns.Item(1).Editable = False

                oGrid.Columns.Item(2).Width = 60
                oGrid.Columns.Item(2).Editable = False

                oGrid.Columns.Item(3).Width = 130
                oGrid.Columns.Item(3).Editable = False

                oGrid.Columns.Item(4).Width = 510
                oGrid.Columns.Item(4).Editable = False

                oGrid.Columns.Item(5).Visible = False
                oGrid.Columns.Item(6).Visible = False
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "doLoadActivity()")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXACT", {Nothing})
            End Try
        End Sub

        Public Function doSetUserAuthorizationForStatus(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim bCanChangeStatus As Boolean = False
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_UPDATE_WO_STATUS) = False Then
                bCanChangeStatus = False
            Else
                'THIS IS NOT USED ANYMORE
                Dim sAuthorizedUser As String = Config.Parameter("DRCRUCD")
                If sAuthorizedUser.Length > 0 Then
                    Dim oUsers() As String = sAuthorizedUser.Split(",")

                    If System.Array.IndexOf(oUsers, goParent.gsUserName) > -1 Then
                        bCanChangeStatus = True
                    End If
                Else
                    bCanChangeStatus = True
                End If
            End If

            Dim oItm As SAPbouiCOM.Item
            oItm = oForm.Items.Item("IDH_STATUS")
            doSetEnabled(oItm, bCanChangeStatus)
            Return bCanChangeStatus
        End Function

        Public Sub doSendCreditCheckAlertAndMessage(ByVal oForm As SAPbouiCOM.Form, ByVal sCardCode As String, ByVal sHeaderMsg As String, ByVal sAlertUsersKey As String, ByVal sAlertMsgKey As String)
            Dim sAlertusers As String = Config.ParameterWithDefault(sAlertUsersKey, "")
            If sAlertusers IsNot Nothing AndAlso sAlertusers.Length > 0 Then
                Dim sAlertMsg As String = Config.ParameterWithDefault(sAlertMsgKey, "Credit check alert!")
                sAlertMsg = sAlertMsg + Chr(13)
                sAlertMsg = sAlertMsg + sHeaderMsg + Chr(13)
                sAlertMsg = sAlertMsg + "Work Order: " + getFormDFValue(oForm, "Code") + Chr(13)
                sAlertMsg = sAlertMsg + "From User: " + goParent.gsUserName + Chr(13)
                sAlertMsg = sAlertMsg + "Cc: " + sAlertusers
                If Not sAlertusers.Contains(goParent.gsUserName) Then
                    sAlertusers = sAlertusers + "," + goParent.gsUserName
                End If
                goParent.doMessage(sHeaderMsg)
                goParent.doSendAlert(sAlertusers.Split(","), sHeaderMsg & ": " & sCardCode, sAlertMsg, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner, sCardCode, True)
            End If
        End Sub

#Region "Post Code Lookup"
        Protected Overridable Sub doFindAddress(ByVal oForm As SAPbouiCOM.Form, sItemUID As String)
            If Config.ParamaterWithDefault("PCODEKEY", "").Trim.Equals(String.Empty) Then
                'doResUserError
                com.idh.bridge.DataHandler.INSTANCE.doResConfigError("Address search module is not active. Please contact support.", "ERPCDNAC", {""})
                Return
            End If
            If (sItemUID = "IDH_POSTCS" OrElse sItemUID = "IDH_PSTCS2") AndAlso getFormDFValue(oForm, "U_CardCd").ToString() = "" Then
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Please select customer code before address search", "WRINNOBP", New String() {"Customer Code"})
                setFormDFValue(oForm, "U_Address", "")
                Return
            ElseIf sItemUID = "IDH_PCDSPR" AndAlso getFormDFValue(oForm, "U_PCardCd").ToString() = "" Then
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Please select Producer Code before address search", "WRINNOBP", New String() {"Producer Code"})
                setFormDFValue(oForm, "U_PAddress", "")
                Return
            End If
            Dim oOOForm As com.uBC.forms.fr3.PostCode = New com.uBC.forms.fr3.PostCode(Nothing, oForm.UniqueID, Nothing)
            Dim sAddress As String = ""
            If sItemUID = "IDH_POSTCS" OrElse sItemUID = "IDH_PSTCS2" Then 'customer
                sAddress = (getFormDFValue(oForm, IDH_JOBENTR._Address).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._Street).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._Block).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._City).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._County).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._State).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._ZpCd).ToString()).Replace("  ", " ").Trim()
            ElseIf sItemUID = "IDH_PCDSPR" Then 'producer
                sAddress = (getFormDFValue(oForm, IDH_JOBENTR._PAddress).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._PStreet).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._PBlock).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._PCity).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._PState).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._PZpCd).ToString()).Replace("  ", " ").Trim()

            ElseIf sItemUID = "IDH_PCDSSI" Then 'site
                sAddress = (getFormDFValue(oForm, IDH_JOBENTR._SAddress).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._SStreet).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._SBlock).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._SCity).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._SState).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._SZpCd).ToString()).Replace("  ", " ").Trim()
            ElseIf sItemUID = "IDH_PCDSCR" Then 'CARRIER
                sAddress = (getFormDFValue(oForm, IDH_JOBENTR._CAddress).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._CStreet).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._CBlock).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._CCity).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._CState).ToString() + " " +
                     getFormDFValue(oForm, IDH_JOBENTR._CZpCd).ToString()).Replace("  ", " ").Trim()
            End If

            oOOForm.setUFValue("IDH_ADDRES", sAddress)
            oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doHandlePostCodeOKReturn)
            ''oOOForm.Handler_DialogButton1Return = New com.idh.forms.oo.Form.DialogReturn(AddressOf doHandlePostCodeOKReturn)
            '' oOOForm.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doHandlePostCodeCancelReturn)
            oOOForm.doShowModal(oForm)
            oOOForm.setSharedData("AddressOf", sItemUID)
            'setSharedData(oForm, "SETENQIDs", "")
            'setSharedData(oForm, "AddressDetail", Nothing)
        End Sub
        Protected Overridable Function doHandlePostCodeOKReturn(oDialogForm As com.idh.forms.oo.Form) As Boolean
            Try
                Dim oOOForm As com.uBC.forms.fr3.PostCode = CType(oDialogForm, com.uBC.forms.fr3.PostCode)
                Dim sItemUID As String = oOOForm.getSharedData("AddressOf")
                Dim Company As String = oOOForm.Company
                Dim SubBuilding As String = oOOForm.SubBuilding
                Dim BuildingNumber As String = oOOForm.BuildingNumber
                Dim BuildingName As String = oOOForm.BuildingName
                Dim SecondaryStreet As String = oOOForm.SecondaryStreet
                Dim Street As String = oOOForm.Street
                Dim Block As String = oOOForm.Block
                ''''Neighbourhood
                Dim District As String = oOOForm.District
                Dim City As String = oOOForm.City
                Dim Line1 As String = oOOForm.Line1
                Dim Line2 As String = oOOForm.Line2
                Dim Line3 As String = oOOForm.Line3
                Dim Line4 As String = oOOForm.Line4
                Dim Line5 As String = oOOForm.Line5
                Dim AdminAreaName As String = oOOForm.AdminAreaName

                ''Province
                Dim ProvinceName As String = oOOForm.ProvinceName
                Dim ProvinceCode As String = oOOForm.ProvinceCode
                Dim PostalCode As String = oOOForm.PostalCode
                Dim CountryName As String = oOOForm.CountryName
                Dim CountryIso2 As String = oOOForm.CountryIso2
                '////CountryIso3
                '////CountryIsoNumber
                '////SortingNumber1
                '////SortingNumber2
                '////Barcode
                Dim POBoxNumber As String = oOOForm.POBoxNumber

                If Street <> "" Then
                    'Street_POBox = Street
                ElseIf POBoxNumber <> "" Then
                    Street = POBoxNumber
                End If


                Dim oArr As ArrayList = New ArrayList
                oArr.Add((Company & " " & SubBuilding & " " & BuildingNumber & " " & BuildingName).Replace("  ", " ")) '0 getSharedData(oForm, "ADDRESS")
                oArr.Add(Street) '1 getSharedData(oForm, "STREET")
                oArr.Add(Block) '2 block
                oArr.Add(PostalCode) '3 ZipCode
                oArr.Add(City) '4 City
                oArr.Add(CountryName) '5 COUNTRY
                oArr.Add("") '6 ROUT
                oArr.Add("") '7 ISBACCDF
                oArr.Add("") '8 ISBACCTF
                oArr.Add("") '9 ISBACCTT
                oArr.Add("") '10 ISBACCRA
                oArr.Add("") '11 ISBACCRV
                oArr.Add("") '12 ISBCUSTRN
                oArr.Add("") '13 TEL1
                oArr.Add("") '14 ROUTE
                oArr.Add("") '15 SEQ
                oArr.Add("") '16 CONA - Contact Person from Address
                oArr.Add(AdminAreaName) '17 COUNTY
                oArr.Add("") '18 PRECD
                oArr.Add("") '19 ORIGIN
                oArr.Add("") '20 SITELIC
                oArr.Add("") '21 STEID

                oArr.Add("") '22 FAX
                oArr.Add("") '23 EMAIL
                oArr.Add("") '24 STRNO
                oArr.Add("") '25 - No sales contact in Search Forms
                oArr.Add("") '26 PHONE1
                oArr.Add("") '27 MCONA - Main Contact Person from BP
                oArr.Add("") '28 STATENM
                ''##MA Start 12-09-2014 Issue#413
                oArr.Add("") '29 MINJOB
                ''##MA End 12-09-2014 Issue#413
                oArr.Add("") '30 LINENUM

                If sItemUID = "IDH_POSTCS" OrElse sItemUID = "IDH_PSTCS2" Then 'customer
                    Dim sCustomer As String = getFormDFValue(oOOForm.SBOParentForm, "U_CardCd")
                    Dim sPh1 As String = getFormDFValue(oOOForm.SBOParentForm, "U_Phone1")
                    doSetCustomerAddress(oOOForm.SBOParentForm, oArr, sCustomer, sPh1, True, False)
                    doCheckCustomerAddress(oOOForm.SBOParentForm, False)
                ElseIf sItemUID = "IDH_PCDSPR" Then 'producer
                    doSetProducerAddress(oOOForm.SBOParentForm, oArr, True, False, False)
                    doCheckProducerAddress(oOOForm.SBOParentForm, False)
                ElseIf sItemUID = "IDH_PCDSSI" Then 'site
                    doSetSiteAddress(oOOForm.SBOParentForm, oArr)
                ElseIf sItemUID = "IDH_PCDSCR" Then 'CARRIER
                    doSetCarrierAddress(oOOForm.SBOParentForm, oArr)
                End If

            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", Nothing)
            End Try
            Return True
        End Function
#End Region
    End Class
End Namespace
