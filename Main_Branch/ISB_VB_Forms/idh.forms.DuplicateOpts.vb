﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge.lookups

Namespace idh.forms
    Public Class DuplicateOpts
        Inherits IDHAddOns.idh.forms.Base

        'Dim sHeadTable As String = "@ISB_SHIPNM" 'will be assigned a value in the constructor

#Region "Initialization"

        'Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParentMenu As String, ByVal iMenuPosition As Integer)
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            'MyBase.New(oParent, "IDH_DUPLICATE", sParentMenu, iMenuPosition, "Duplicate.srf", False, True, False, "Duplicate Rows", load_Types.idh_LOAD_NORMAL)
            MyBase.New(oParent, "IDH_DUPLICATE", Nothing, -1, "Duplicate.srf", False, True, False, "Duplicate Rows", load_Types.idh_LOAD_NORMAL)

            'sHeadTable = "@ISB_SHIPNM"

            'doEnableHistory(sHeadTable)

        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
        End Sub

        'Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
        '    Dim oItem As SAPbouiCOM.Item
        '    Dim oLbl As SAPbouiCOM.StaticText
        '    Dim oEdit As SAPbouiCOM.EditText
        '    Dim oChkAdi As SAPbouiCOM.CheckBox
        '    Dim oChkComm As SAPbouiCOM.CheckBox

        '    Dim oUDataSource As SAPbouiCOM.UserDataSource

        '    oForm.Freeze(True)

        '    Try
        '        oForm.Items.Item("IDH_MSG").AffectsFormMode = False
        '        oForm.Items.Item("IDH_NOCOPY").AffectsFormMode = False
        '        oForm.Items.Item("IDH_CHKADI").AffectsFormMode = False
        '        oForm.Items.Item("IDH_CHKCOM").AffectsFormMode = False

        '        oItem = oForm.Items.Item("IDH_MSG")
        '        oLbl = oItem.Specific
        '        oLbl.Caption = "The selected row(s) will be Duplicated. Do you want to continue as per options below?" ' - " + getSharedData(oForm, "SETDUP").ToString()

        '        oItem = oForm.Items.Item("IDH_NOCOPY")
        '        oEdit = oItem.Specific
        '        oEdit.Value = Config.Parameter("WODUPLNO").ToString()

        '        oUDataSource = oForm.DataSources.UserDataSources.Add("chkDS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)

        '        'oItem = oForm.Items.Item("IDH_CHKADI")
        '        'oChk = oItem.Specific
        '        oChkAdi = oForm.Items.Item("IDH_CHKADI").Specific
        '        oChkAdi.DataBind.SetBound(True, "", "chkDS")
        '        oChkAdi.Checked = Config.INSTANCE.getParameterAsBool("WOADDITM", False)

        '        'oItem = oForm.Items.Item("IDH_CHKCOM")
        '        'oChk = oItem.Specific
        '        oChkComm = oForm.Items.Item("IDH_CHKCOM").Specific
        '        oChkComm.DataBind.SetBound(True, "", "chkDS")
        '        oChkComm.Checked = Config.INSTANCE.getParameterAsBool("WOCOMENT", False)
        '    Catch ex As Exception
        '        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
        '        BubbleEvent = False
        '    End Try


        '    oForm.Freeze(False)

        '    'MyBase.doCompleteCreate(oForm, BubbleEvent)
        'End Sub
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                'doAddUF(oForm, "IDH_MSG", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDH_NOCOPY", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 2, False, False)
                doAddUF(oForm, "IDH_CHKADI", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDH_CHKCOM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDH_COPYPR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDH_COPYQT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)

                doAddUF(oForm, "IDH_CHKVEH", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDH_CHKDRV", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDH_REQDT", SAPbouiCOM.BoDataType.dt_DATE, 10, False, False)



                Dim sMsg As String = "The selected row(s) will be Duplicated. Do you want to continue as per options below?"
                Dim sCopies As String = Config.ParameterWithDefault("WODUPLNO", "1")
                Dim bCopyADDITMS As Boolean = Config.INSTANCE.getParameterAsBool("WOADDITM", False)
                Dim bCopyComments As Boolean = Config.INSTANCE.getParameterAsBool("WOCOMENT", False)
                Dim bCopyPrice As Boolean = Config.INSTANCE.getParameterAsBool("WOCOPYPR", False)
                Dim bCopyQt As Boolean = Config.INSTANCE.getParameterAsBool("WOCOPYQT", False)

                Dim bCopyVehReg As Boolean = Config.INSTANCE.getParameterAsBool("WOCPYVHR", False)
                Dim bCopyDriver As Boolean = Config.INSTANCE.getParameterAsBool("WOCPYDRV", False)
                Dim bCopyReqDt As Boolean = Config.INSTANCE.getParameterAsBool("WOSETRDT", False)

                'setUFValue(oForm, "IDH_MSG", sMsg)
                setUFIValue(oForm, "IDH_NOCOPY", sCopies)
                setUFIValue(oForm, "IDH_CHKADI", IIf(bCopyADDITMS, "Y", "N"))
                setUFIValue(oForm, "IDH_CHKCOM", IIf(bCopyComments, "Y", "N"))
                setUFIValue(oForm, "IDH_COPYPR", IIf(bCopyPrice, "Y", "N"))
                setUFIValue(oForm, "IDH_COPYQT", IIf(bCopyQt, "Y", "N"))

                setUFIValue(oForm, "IDH_CHKVEH", IIf(bCopyVehReg, "Y", "N"))
                setUFIValue(oForm, "IDH_CHKDRV", IIf(bCopyDriver, "Y", "N"))
                setVisible(oForm, "IDH_REQDT", bCopyReqDt)
                setVisible(oForm, "lblReqDate", bCopyReqDt)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub

#End Region

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
        End Sub

#Region "Event Handlers"

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Return True
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If goParent.doCheckModal(oForm.UniqueID) = True Then
                    If doPrepareModalData(oForm) = False Then
                        BubbleEvent = False
                    End If
                End If
            End If
        End Sub

        Protected Function doPrepareModalData(ByVal oForm As SAPbouiCOM.Form, Optional ByVal iSel As Integer = -1) As Boolean
            'Dim sMsg As String = getUFValue(oForm, "IDH_MSG")
            Dim sNoOfCopies As String = getUFValue(oForm, "IDH_NOCOPY")
            Dim sAddDetails As String = getUFValue(oForm, "IDH_CHKADI")
            Dim sComments As String = getUFValue(oForm, "IDH_CHKCOM")
            Dim sPrices As String = getUFValue(oForm, "IDH_COPYPR")
            Dim sQty As String = getUFValue(oForm, "IDH_COPYQT")

            Dim sVehReg As String = getUFValue(oForm, "IDH_CHKVEH")
            Dim sDriver As String = getUFValue(oForm, "IDH_CHKDRV")
            Dim sReqDate As String = getUFValue(oForm, "IDH_REQDT")


            'Dim oItem As SAPbouiCOM.Item
            'Dim oEdit As SAPbouiCOM.EditText
            'Dim oChkAdi As SAPbouiCOM.CheckBox
            'Dim oChkCom As SAPbouiCOM.CheckBox

            'Dim sNoOfCopies As String
            'Dim sAddDetails As String
            'Dim sComments As String

            'oItem = oForm.Items.Item("IDH_NOCOPY")
            'oEdit = oItem.Specific
            'sNoOfCopies = oEdit.Value

            ''oItem = oForm.Items.Item("IDH_CHKADI")
            ''oChkAdi = oItem.Specific
            'oChkAdi = oForm.Items.Item("IDH_CHKADI").Specific
            'sAddDetails = oChkAdi.Checked

            ''oItem = oForm.Items.Item("IDH_CHKCOM")
            ''oChkCom = oItem.Specific
            'oChkCom = oForm.Items.Item("IDH_CHKCOM").Specific
            'sComments = oChkCom.Checked

            setParentSharedData(oForm, "IDHNOCOPY", sNoOfCopies)
            If sAddDetails = "Y" Then
                setParentSharedData(oForm, "IDHCHKADI", "true")
            Else
                setParentSharedData(oForm, "IDHCHKADI", "false")
            End If
            If sComments = "Y" Then
                setParentSharedData(oForm, "IDHCHKCOM", "true")
            Else
                setParentSharedData(oForm, "IDHCHKCOM", "false")
            End If
            If sPrices = "Y" Then
                setParentSharedData(oForm, "IDHCOPYPR", "true")
            Else
                setParentSharedData(oForm, "IDHCOPYPR", "false")
            End If
            If sQty = "Y" Then
                setParentSharedData(oForm, "IDHCOPYQT", "true")
            Else
                setParentSharedData(oForm, "IDHCOPYQT", "false")
            End If

            If sVehReg = "Y" Then
                setParentSharedData(oForm, "IDHVEHREG", "true")
            Else
                setParentSharedData(oForm, "IDHVEHREG", "false")
            End If
            If sDriver = "Y" Then
                setParentSharedData(oForm, "IDHDRIVER", "true")
            Else
                setParentSharedData(oForm, "IDHDRIVER", "false")
            End If
            If sReqDate IsNot Nothing AndAlso sReqDate.Length > 0 Then
                setParentSharedData(oForm, "IDHREQDT", sReqDate)
            Else
                setParentSharedData(oForm, "IDHREQDT", "")
            End If
            doReturnFromModalShared(oForm, True)
            Return True
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        End Sub

        Protected Overrides Sub doHandleModalBufferedResult(ByVal oParentForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        End Sub

        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        Public Overrides Sub doClose()
        End Sub
#End Region

#Region "Custom Functions"
#End Region

    End Class
End Namespace