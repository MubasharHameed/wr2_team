Imports System.IO
Imports System.Collections
Imports System

'Needed for the Framework controls
Imports IDHAddOns.idh.controls
Imports com.idh.bridge.data
Imports com.idh.bridge
Imports com.idh.dbObjects.User
Imports com.idh.bridge.lookups
Imports com.idh.dbObjects.numbers

Namespace idh.forms
    Public Class ShipNameGen
        Inherits IDHAddOns.idh.forms.Base

        Dim sHeadTable As String = "@ISB_SHIPNM" 'will be assigned a value in the constructor

#Region "Initialization"

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParentMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "SHIPNMGEN", sParentMenu, iMenuPosition, "ShipName Generator.srf", False, True, False, "Shipping Name Generator", load_Types.idh_LOAD_NORMAL)

            sHeadTable = "@ISB_SHIPNM"

            doEnableHistory(sHeadTable)

        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)

        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            'Dim oItem As SAPbouiCOM.Item
            'Dim oButton As SAPbouiCOM.Button
            'Dim oFolder As SAPbouiCOM.Folder
            'Dim oOptionBtn As SAPbouiCOM.OptionBtn
            'Dim oCheckBox As SAPbouiCOM.CheckBox
            'Dim oStatic As SAPbouiCOM.StaticText

            'Dim i As Integer

            oForm.Freeze(True)

            Try
                'Adding controls to Shipping Generator Form 
                Dim iPane As Integer = 0
                Dim iLeft As Integer = 10
                Dim iTop As Integer = 5
                Dim iWidth As Integer = 100
                Dim iHeight As Integer = -1

                oForm.DataSources.DBDataSources.Add("@ISB_SHIPNM")

                doAddEdit(oForm, "IDH_SPNMCD", iPane, iLeft + iWidth + 350, iTop, 0, 60)

                doAddEdit(oForm, "IDH_SHPNM", iPane, iLeft, iTop + 20, iWidth + 340, 60)
                doAddStatic(oForm, "SHPNM1", iPane, iLeft, iTop, iWidth + 100, iHeight, "Sample Shipping Name", "IDH_SHPNM")

                iTop = 100
                doAddEdit(oForm, "IDH_PSHPNM", iPane, iLeft + 115, iTop, iWidth + 200, iHeight)
                doAddLookupButton(oForm, "PSNLKP", iPane, iLeft + 315 + iWidth, iTop, "IDH_PSHPNM")
                doAddStatic(oForm, "PSHPNM1", iPane, iLeft, iTop, iWidth, iHeight, "Proper Shipping Name", "IDH_PSHPNM")

                'doAddCombo(oForm, "IDH_PSHPNM", iPane, iLeft + 115, iTop, iWidth + 220, iHeight)
                'doAddStatic(oForm, "PSHPNM1", iPane, iLeft, iTop, iWidth, iHeight, "Proper Shipping Name", "IDH_PSHPNM")
                'doFillCombo(oForm, "IDH_PSHPNM", "[@IDH_PSHIPNM]", "Code", "U_PSName", Nothing, "U_PSName")


                doAddOption(oForm, "IDH_PKG1", iPane, iLeft + 115, iTop + 20, 60, iHeight, "PG N/A", "A", "PKGGRP1", "")
                doAddOption(oForm, "IDH_PKG2", iPane, iLeft + 195, iTop + 20, 60, iHeight, "PG I", "B", "", "IDH_PKG1")
                doAddOption(oForm, "IDH_PKG3", iPane, iLeft + 275, iTop + 20, 60, iHeight, "PG II", "C", "", "IDH_PKG1")
                doAddOption(oForm, "IDH_PKG4", iPane, iLeft + 355, iTop + 20, 60, iHeight, "PG III", "D", "", "IDH_PKG1")
                doAddStatic(oForm, "SPKG1", iPane, iLeft, iTop + 20, iWidth, iHeight, "Packaging Group", "")

                doAddCombo(oForm, "IDH_HAZCLS", iPane, iLeft + 115, iTop + 40, iWidth + 220, iHeight)
                doAddStatic(oForm, "HAZCLS1", iPane, iLeft, iTop + 40, iWidth, iHeight, "Hazardous Class", "IDH_HAZCLS")
                doFillCombo(oForm, "IDH_HAZCLS", "[@IDH_HAZCLASS]", "U_HazClassCd", "U_Desc", Nothing, "U_Desc")

                doAddEdit(oForm, "IDH_UNNANO", iPane, iLeft + 115, iTop + 60, iWidth, iHeight)
                doAddLookupButton(oForm, "UNNALKP", iPane, iLeft + 115 + iWidth, iTop + 60, "IDH_UNNANO")
                doAddCheck(oForm, "IDH_REPQTY", iPane, iLeft + 240, iTop + 60, iWidth, iHeight, "Reportable Qty")
                doAddCheck(oForm, "IDH_WASTE", iPane, iLeft + 340, iTop + 60, iWidth, iHeight, "Waste")
                doAddStatic(oForm, "UNNANO1", iPane, iLeft, iTop + 60, iWidth, iHeight, "UN/NA Number", "IDH_UNNANO")

                doAddEdit(oForm, "IDH_CONST", iPane, iLeft + 115, iTop + 80, iWidth + 200, iHeight)
                doAddLookupButton(oForm, "CONLKP", iPane, iLeft + 315 + iWidth, iTop + 80, "IDH_CONST")
                doAddStatic(oForm, "CONST1", iPane, iLeft, iTop + 80, iWidth, iHeight, "Constituent(s)", "IDH_CONST")

                'doAddButton(oForm, "1", iPane, iLeft, iTop, iWidth, iHeight, "Ok")
                'doAddButton(oForm, "2", iPane, iLeft, iTop, iWidth, iHeight, "Ok")


                'Mapp fields with data fields 
                doAddFormDF(oForm, "IDH_SPNMCD", "Code")
                doAddFormDF(oForm, "IDH_SHPNM", "U_ShipNm")
                doAddFormDF(oForm, "IDH_PSHPNM", "U_PropShpNm")
                doAddFormDF(oForm, "IDH_PKG1", "U_PackGrp")
                doAddFormDF(oForm, "IDH_PKG2", "U_PackGrp")
                doAddFormDF(oForm, "IDH_PKG3", "U_PackGrp")
                doAddFormDF(oForm, "IDH_PKG4", "U_PackGrp")
                doAddFormDF(oForm, "IDH_HAZCLS", "U_HazClass")
                doAddFormDF(oForm, "IDH_UNNANO", "U_UNNANo")
                doAddFormDF(oForm, "IDH_REPQTY", "U_IsRepQty")
                doAddFormDF(oForm, "IDH_WASTE", "U_IsWaste")
                doAddFormDF(oForm, "IDH_CONST", "U_Constit")

                'Form level settings 
                oForm.DataBrowser.BrowseBy = "IDH_SPNMCD"

                oForm.SupportedModes = -1
                oForm.AutoManaged = True

                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
                BubbleEvent = False
            End Try


            oForm.Freeze(False)

            'MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

#End Region

#Region "Loading Data"

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            'Using the Form's main table
            Dim sCode As String = getFormDFValue(oForm, "Code").ToString()

            If Not sCode Is Nothing AndAlso sCode.Length > 0 Then
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE

                Dim oCons As New SAPbouiCOM.Conditions
                Dim oCon As SAPbouiCOM.Condition

                oCon = oCons.Add
                oCon.Alias = "Code"
                oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                oCon.CondVal = sCode
                oForm.DataSources.DBDataSources.Item("@ISB_SHIPNM").Query(oCons)
            Else
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                doCreateNewEntry(oForm)
            End If

            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                doCreateNewEntry(oForm)
            ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                doCreateFindEntry(oForm)
            End If
            sCode = "-999"

            ''Try
            ''    Dim oGAttach As UpdateGrid = UpdateGrid.getInstance(oForm, "ATHGRID")
            ''    oGAttach.doReloadData()
            ''Catch ex As Exception
            ''    com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error reloading the Attachments Grid Data.")
            ''Finally
            ''End Try

        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            ''oForm.Freeze(True)

            MyBase.doBeforeLoadData(oForm)
            Dim sShipCode As String = getParentSharedData(oForm, "SHIPNMCD")
            setParentSharedData(oForm, "SHIPNMCD", "")
            If sShipCode IsNot Nothing AndAlso sShipCode.Length > 0 Then
                setFormDFValue(oForm, "Code", sShipCode)
            End If
            ''Dim owItem As SAPbouiCOM.Item
            ''Dim ow2Item As SAPbouiCOM.Folder

            ''owItem = oForm.Items.Item("TABDET") 'Console Tab
            ''ow2Item = owItem.Specific
            ''ow2Item.Select()
            ''oForm.PaneLevel = 101

            ''Setting Form Tabs not to affect form mode 
            ''oForm.Items.Item("TABDET").AffectsFormMode = False 'Detail 
            ''oForm.Items.Item("TABATH").AffectsFormMode = False 'Attachment

            ''Loading Data/Column structure for Grids 
            ''Attachment Grid 
            ''Dim oGAttach As UpdateGrid = UpdateGrid.getInstance(oForm, "ATHGRID")
            ''If oGAttach Is Nothing Then
            ''    oGAttach = New UpdateGrid(Me, oForm, "CONGRID")
            ''End If
            ''oGAttach.doSetSBOAutoReSize(False)
            ''oGAttach.doAddGridTable(New GridTable("ATC1", Nothing, "AbsEntry", True), True)
            ''oGAttach.setOrderValue("AbsEntry")
            ''oGAttach.setRequiredFilter(getListConsoleRequiredStr())
            ''oGAttach.doSetDoCount(False)
            ''oGAttach.doSetBlankLine(False)
            ''doSetListConsoleFields(oGAttach)

            ''Get shared values of form opened as Modal Form 
            ''If getHasSharedData(oForm) Then
            ''    'doCreateNewEntry(oForm)

            ''    'oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            ''    oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
            ''    Dim sAnexCd As String = getParentSharedData(oForm, "ANEXCD")
            ''    Dim sAnexNo As String = getParentSharedData(oForm, "ANEXNO")
            ''    Dim sTFSCd As String = getParentSharedData(oForm, "TFSCD")
            ''    Dim sTFSNo As String = getParentSharedData(oForm, "TFSNO")

            ''    setFormDFValue(oForm, "Code", sAnexCd, True)
            ''    setFormDFValue(oForm, "U_AnexNo", sAnexNo)
            ''    setFormDFValue(oForm, "U_TFSCd", sTFSCd)
            ''    setFormDFValue(oForm, "U_TFSNo", sTFSNo)
            ''End If

            ''oForm.Freeze(False)

        End Sub

#End Region

#Region "Unloading Data"

        Public Overrides Sub doClose()
        End Sub

#End Region

#Region "Event Handlers"

        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                If pVal.MenuUID = Config.NAV_ADD Then
                    oForm.Freeze(True)
                    Try
                        doLoadData(oForm)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Menu Event - " & oForm.UniqueID & "." & pVal.MenuUID)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try
                ElseIf pVal.MenuUID = Config.NAV_FIND OrElse
                       pVal.MenuUID = Config.NAV_FIRST OrElse
                       pVal.MenuUID = Config.NAV_LAST OrElse
                       pVal.MenuUID = Config.NAV_NEXT OrElse
                       pVal.MenuUID = Config.NAV_PREV Then
                    oForm.Freeze(True)
                    Try
                        doLoadData(oForm)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Menu Event - " & oForm.UniqueID & "." & pVal.MenuUID)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try
                End If
            End If
            Return True
        End Function

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim sItemID As String = pVal.ItemUID
            Dim owItem As SAPbouiCOM.Item
            Dim ow2Item As SAPbouiCOM.Folder

            
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If pVal.BeforeAction = True Then
                    If pVal.CharPressed = 13 Then
                        goParent.goApplication.SendKeys("{TAB}")
                        BubbleEvent = False
                    End If
                Else
                    If pVal.CharPressed = 9 Then
                        If sItemID = "IDH_UNNANO" AndAlso getFormDFValue(oForm, "IDH_UNNANO").ToString().Contains("*") Then
                            goParent.doOpenModalForm("IDH_WTUNSR", oForm)
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                If pVal.BeforeAction = False Then
                    If sItemID = "IDH_PSHPNM" Then
                        doSetShippingName(oForm)
                    ElseIf sItemID = "IDH_HAZCLS" Then
                        doSetShippingName(oForm)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.Before_Action = True Then
                    Select Case sItemID
                        Case "TABDET"
                            oForm.PaneLevel = 101
                            owItem = oForm.Items.Item("TABDET") 'General Tab
                            ow2Item = owItem.Specific
                            ow2Item.Select()
                        Case "TABATH"
                            oForm.PaneLevel = 102
                    End Select
                Else
                    If sItemID = "CONLKP" Then
                        'CONLKP
                        'Allow Max 6 of CAS Codes 
                        Dim sConstituents As String = getFormDFValue(oForm, "U_Constit")
                        If sConstituents.Split(" and ").Length >= 2 Then
                            doWarnMess("Maximum of 2 CAS Waste Codes allowed.", SAPbouiCOM.BoMessageTime.bmt_Short)
                        Else
                            setSharedData(oForm, "TRG", "IDH_CONST")
                            goParent.doOpenModalForm("CASTBLSCR", oForm)
                        End If
                    ElseIf sItemID = "UNNALKP" Then
                        'UN/NA Lookup 
                        setSharedData(oForm, "TRG", "IDH_UNNA")
                        goParent.doOpenModalForm("IDH_WTUNSR", oForm)
                    ElseIf sItemID = "PSNLKP" Then
                        goParent.doOpenModalForm("IDH_PSNMSR", oForm)
                    ElseIf sItemID = "IDH_BTNDIS" Then
                        ''
                    ElseIf sItemID = "IDH_REPQTY" OrElse sItemID = "IDH_WASTE" Then
                        doSetShippingName(oForm)
                    ElseIf sItemID = "IDH_PKG1" OrElse sItemID = "IDH_PKG2" OrElse sItemID = "IDH_PKG3" OrElse sItemID = "IDH_PKG4" Then
                        doSetShippingName(oForm)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_CLICK Then
                'If sItemID = "IDH_PKG1" OrElse sItemID = "IDH_PKG2" OrElse sItemID = "IDH_PKG3" OrElse sItemID = "IDH_PKG4" Then
                '    doSetShippingName(oForm)
                'End If
                'If sItemID = "IDH_REPQTY" OrElse sItemID = "IDH_WASTE" Then
                '    doSetShippingName(oForm)
                'End If
                If pVal.BeforeAction = True Then
                    'If sItemID = "IDH_REPQTY" OrElse sItemID = "IDH_WASTE" Then
                    '    doSetShippingName(oForm)
                    'End If
                End If
            End If
            Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Dim bIsTFSValid As Boolean = False

            If pVal.BeforeAction = True Then
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    Dim sCode As String = getFormDFValue(oForm, "Code").ToString()

                    If sCode Is Nothing OrElse sCode.Length = 0 Then
                        'sCode = DataHandler.doGenerateCode(ISB_SHIPNM.AUTONUMPREFIX, "GENSEQ")
                        Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(ISB_SHIPNM.AUTONUMPREFIX, "GENSEQ")
                        setFormDFValue(oForm, "Code", oNumbers.CodeCode)
                    End If

                    If doUpdateHeader(oForm, True) Then
                        oForm.Update()
                        'setSharedData(oForm, "", "")
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE

                        If goParent.doCheckModal(oForm.UniqueID) = True Then
                            If doPrepareModalData(oForm) = False Then
                                BubbleEvent = False
                            End If
                        End If

                        'BubbleEvent = False
                        'oForm.Close()
                    End If

                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    If doUpdateHeader(oForm, False) Then
                        oForm.Update()
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        BubbleEvent = False
                    End If
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    If goParent.doCheckModal(oForm.UniqueID) = True Then
                        If doPrepareModalData(oForm) = False Then
                            BubbleEvent = False
                        End If
                    End If
                End If
            Else
            End If

        End Sub

        Protected Function doPrepareModalData(ByVal oForm As SAPbouiCOM.Form, Optional ByVal iSel As Integer = -1) As Boolean
            'Dim sComment As String = getUFValue(oForm, "IDH_CMMNT")
            Dim sShpNm As String = getFormDFValue(oForm, "U_ShipNm")
            Dim sHazCls As String = getFormDFValue(oForm, "U_HazClass")
            Dim sUNNANo As String = getFormDFValue(oForm, "U_UNNANo")
            Dim sPkGp As String = getFormDFValue(oForm, "U_PackGrp")
            Dim sPkGpFinal As String = ""

            Select Case sPkGp
                Case "1"
                    sPkGpFinal = "PG N/A"
                Case "B"
                    sPkGpFinal = "PG I"
                Case "C"
                    sPkGpFinal = "PG II"
                Case "D"
                    sPkGpFinal = "PG III"
                Case Else
            End Select

            setParentSharedData(oForm, "SHIPNM", sShpNm)
            setParentSharedData(oForm, "HAZCLS", sHazCls)
            setParentSharedData(oForm, "UNNANO", sUNNANo)
            setParentSharedData(oForm, "PKGGRP", sPkGpFinal)

            doReturnFromModalShared(oForm, True)
            Return True
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            If sModalFormType.Equals("CASTBLSCR") Then
                Dim sTrg As String = getSharedData(oForm, "TRG").ToString()
                If sTrg = "IDH_CONST" Then
                    Dim sConstituents As String = getFormDFValue(oForm, "U_Constit")
                    If sConstituents IsNot Nothing AndAlso sConstituents.Length > 0 Then
                        sConstituents = sConstituents + " and " + getSharedData(oForm, "IDH_CHMNM")
                    Else
                        sConstituents = getSharedData(oForm, "IDH_CHMNM")
                    End If
                    setFormDFValue(oForm, "U_Constit", sConstituents)
                    doSetShippingName(oForm)
                End If
            ElseIf sModalFormType.Equals("IDH_WTUNSR") Then
                setFormDFValue(oForm, "U_UNNANo", getSharedData(oForm, "IDH_UNCODE"))
                doSetShippingName(oForm)
            ElseIf sModalFormType.Equals("IDH_PSNMSR") Then
                setFormDFValue(oForm, "U_PropShpNm", getSharedData(oForm, "IDH_PSHPNM"))
                doSetShippingName(oForm)
            End If

            'If sModalFormType.Equals("IDHCSRCH") Then
            '    Dim sTrg As String = getSharedData(oForm, "TRG")
            '    Dim saParam As String() = {sTrg}

            '    Dim sCustCd As String = getSharedData(oForm, "CARDCODE")
            '    Dim sCustName As String = getSharedData(oForm, "CARDNAME")

            '    If sTrg = "IDH_NOTL" Then
            '        If isValidBPTFSRegNo(oForm, sCustCd) Then
            '            setFormDFValue(oForm, "U_NOTCd", sCustCd)
            '            setFormDFValue(oForm, "U_NOTName", sCustName)
            '            doSetNotifierAddress(oForm)
            '        Else
            '            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            '        End If
            '    ElseIf sTrg = "IDH_CONL" Then
            '        If isValidBPTFSRegNo(oForm, sCustCd) Then
            '            setFormDFValue(oForm, "U_CONCd", sCustCd)
            '            setFormDFValue(oForm, "U_CONName", sCustName)
            '            doSetConsignorAddress(oForm)
            '        Else
            '            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            '        End If
            '    ElseIf sTrg = "IDH_COEL" Then
            '        If isValidBPTFSRegNo(oForm, sCustCd) Then
            '            setFormDFValue(oForm, "U_COECd", sCustCd)
            '            setFormDFValue(oForm, "U_COEName", sCustName)
            '            doSetConsigneeAddress(oForm)
            '        Else
            '            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            '        End If
            '    ElseIf sTrg = "IDH_CARL" Then
            '        If isValidBPTFSRegNo(oForm, sCustCd) Then
            '            setFormDFValue(oForm, "U_CARCd", sCustCd)
            '            setFormDFValue(oForm, "U_CARName", sCustName)
            '            doSetCarrierAddress(oForm)
            '        Else
            '            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            '        End If
            '    ElseIf sTrg = "IDH_GENL" Then
            '        If isValidBPTFSRegNo(oForm, sCustCd) Then
            '            setFormDFValue(oForm, "U_GENCd", sCustCd)
            '            setFormDFValue(oForm, "U_GENName", sCustName)
            '            doSetGeneratorAddress(oForm)
            '        Else
            '            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            '        End If
            '    ElseIf sTrg = "IDH_DSPL" Then
            '        If isValidBPTFSRegNo(oForm, sCustCd) Then
            '            setFormDFValue(oForm, "U_DSPCd", sCustCd)
            '            setFormDFValue(oForm, "U_DSPName", sCustName)
            '            doSetDisposalFacilityAddress(oForm)
            '        Else
            '            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            '        End If
            '    ElseIf sTrg = "IDH_FNDL" Then
            '        If isValidBPTFSRegNo(oForm, sCustCd) Then
            '            setFormDFValue(oForm, "U_FDSCd", sCustCd)
            '            setFormDFValue(oForm, "U_FDSName", sCustName)
            '            doSetFinalDestinationAddress(oForm)
            '        Else
            '            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            '        End If
            '    End If
            'ElseIf sModalFormType.Equals("IDHTFSNSR") Then
            '    Dim sTrg As String = getSharedData(oForm, "TRG")
            '    If sTrg = "IDH_TFSNL" Then
            '        If isValidTFSNumber(getSharedData(oForm, "IDH_TFSNUM")) Then
            '            setFormDFValue(oForm, "U_TFSNo", getSharedData(oForm, "IDH_TFSNUM"))
            '        Else
            '            Dim saParam As String() = {sTrg}
            '            doResError("Selected TFS Number not valid! Please choose an active TFS Number.", "INVLTFSN", saParam)
            '        End If
            '    End If
            'End If
            'MyBase.doHandleModalResultShared(oForm, sModalFormType, sLastButton)
            'Dim oParentForm As SAPbouiCOM.Form = goParent.doGetParentForm(oForm.UniqueID)
            'setSharedData(oParentForm, "ANXCD1", "123")
            'setSharedData(oParentForm, "ANXDESC1", "anx desc")
            'doReturnFromModalShared(oForm, True)
        End Sub

        Protected Overrides Sub doHandleModalBufferedResult(ByVal oParentForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            'If oData Is Nothing Then Exit Sub
            'Dim oArr As ArrayList = oData
            'If oArr.Count = 0 Then
            '    Return
            'End If
            'setFormDFValue(oParentForm, "U_BoxNo", oArr(0))
            'MyBase.doHandleModalBufferedResult(oParentForm, oData, sModalFormType, sLastButton)
        End Sub

        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            'MyBase.doReadInputParams(oForm)
            'Dim sVal As String = getParentSharedData(oForm, "Share_ACd")

            'setFormDFValue(oForm, "U_BoxNo", sVal)

        End Sub

#End Region

#Region "Custom Functions"

        Protected Function doUpdateHeader(ByVal oForm As SAPbouiCOM.Form, ByVal bIsAdd As Boolean) As Boolean
            'This will now Automatically update all the current DB fields and
            'generate and set the Code value if the code was not set
            Return doAutoSave(oForm, ISB_SHIPNM.AUTONUMPREFIX, "GENSEQ")
        End Function

        Public Overridable Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form)
            Dim sCode As String = ""

            setFormDFValue(oForm, "Code", sCode)
            setFormDFValue(oForm, "Name", sCode)
            setFormDFValue(oForm, "U_ShipNm", "")
            setFormDFValue(oForm, "U_PropShpNm", "")
            setFormDFValue(oForm, "U_PackGrp", "")
            setFormDFValue(oForm, "U_HazClass", "")
            setFormDFValue(oForm, "U_UNNANo", "")
            setFormDFValue(oForm, "U_IsRepQty", "")
            setFormDFValue(oForm, "U_IsWaste", "")
            setFormDFValue(oForm, "U_Constit", "")


        End Sub

        Public Overridable Sub doCreateFindEntry(ByVal oForm As SAPbouiCOM.Form)
            setFormDFValue(oForm, "Code", "", True)
            setFormDFValue(oForm, "Name", "")
            setFormDFValue(oForm, "U_ShipNm", "")
            setFormDFValue(oForm, "U_PropShpNm", "")
            setFormDFValue(oForm, "U_PackGrp", "")
            setFormDFValue(oForm, "U_HazClass", "")
            setFormDFValue(oForm, "U_UNNANo", "")
            setFormDFValue(oForm, "U_IsRepQty", "")
            setFormDFValue(oForm, "U_IsWaste", "")
            setFormDFValue(oForm, "U_Constit", "")

            Dim oExclude() As String = {"IDH_SPNMCD"}
            DisableAllEditItems(oForm, oExclude)

        End Sub

        Protected Overridable Sub doSetListConsoleFields(ByRef oGConsole As IDHAddOns.idh.controls.UpdateGrid)
            ''oGConsole.doAddListField("AbsEntry", "Code", False, 8, Nothing, Nothing)
            ''oGConsole.doAddListField("trgtPath", "Path", False, 8, Nothing, Nothing)
            ''oGConsole.doAddListField("FileName", "File Name", False, 20, Nothing, Nothing)
            ''oGConsole.doAddListField("Date", "Attachment Date", False, 20, Nothing, Nothing)
        End Sub

        Public Overridable Function getListConsoleRequiredStr() As String
            ''Dim oForm As SAPbouiCOM.Form
            ''Dim sField As String = "" '" [U_AnexNo] = '' "
            ''Return sField
            Return ""
        End Function

        Protected Sub doSetMode(ByVal oForm As SAPbouiCOM.Form, ByVal iMode As SAPbouiCOM.BoFormMode)
            If iMode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                oForm.Items.Item("1").Specific.Caption = getTranslatedWord("Find")
            ElseIf iMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                oForm.Items.Item("1").Specific.Caption = getTranslatedWord("Update")
                doSetUpdate(oForm)
            ElseIf iMode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                oForm.Items.Item("1").Specific.Caption = getTranslatedWord("Ok")
            End If
        End Sub

        Public Sub doAddOption(ByVal oForm As SAPbouiCOM.Form, ByVal sUID As String, ByVal iPaneLevel As Integer, _
                ByVal iLeft As Integer, ByVal iTop As Integer, ByVal iWidth As Integer, ByVal iHeight As Integer, _
                ByVal sCaption As String, ByVal sValOn As String, Optional ByVal sUDSource As String = "", Optional ByVal sGroupWith As String = "")

            Dim oOption As SAPbouiCOM.OptionBtn
            Dim oItem As SAPbouiCOM.Item
            Dim oUDSource As SAPbouiCOM.UserDataSource

            oItem = oForm.Items.Add(sUID, SAPbouiCOM.BoFormItemTypes.it_OPTION_BUTTON)
            oItem.Left = iLeft
            oItem.Top = iTop
            oItem.Width = iWidth
            If iHeight > 0 Then
                oItem.Height = iHeight
            End If
            oItem.FromPane = iPaneLevel
            oItem.ToPane = iPaneLevel
            oOption = oItem.Specific
            oOption.Caption = sCaption

            If sUDSource.Length > 0 Then
                oUDSource = oForm.DataSources.UserDataSources.Add(sUDSource, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                oOption.DataBind.SetBound(True, "", sUDSource)
            End If

            If sGroupWith.Length > 0 Then
                oOption.GroupWith(sGroupWith)
            End If
            oOption.ValOn = sValOn
            'oOption.ValOff = sValOff
        End Sub

        'Private Sub doFillProperShipName(ByVal oForm As SAPbouiCOM.Form)
        '    doFillCombo(oForm, "IDH_PSHPNM", "[@IDH_PSHIPNM]", "Code", "U_PSName", Nothing, "U_PSName")
        'End Sub

        'Private Sub doFillHazClass(ByVal oForm As SAPbouiCOM.Form)
        '    doFillCombo(oForm, "IDH_HAZCLS", "[@IDH_HAZCLASS]", "U_HazClassCd", "U_Desc", Nothing, "U_Desc")
        'End Sub

        Private Sub doSetShippingName(ByVal oForm As SAPbouiCOM.Form)
            Dim oCombo As SAPbouiCOM.ComboBox

            Dim sProperShipName As String = getFormDFValue(oForm, "U_PropShpNm")
            If getFormDFValue(oForm, "U_PropShpNm") IsNot Nothing AndAlso getFormDFValue(oForm, "U_PropShpNm").ToString().Length > 0 Then

                'oCombo = oForm.Items.Item("IDH_PSHPNM").Specific
                'sProperShipName = oCombo.Selected.Description + ", "
                sProperShipName = getFormDFValue(oForm, "U_PropShpNm") + ", "
            End If

            Dim sPackGroup As String = String.Empty
            If getFormDFValue(oForm, "U_PackGrp") IsNot Nothing AndAlso getFormDFValue(oForm, "U_PackGrp").ToString().Length > 0 Then
                Dim sDBPackGrp As String = getFormDFValue(oForm, "U_PackGrp")
                If sDBPackGrp = "1" Then
                    sPackGroup = "PG N/A"
                ElseIf sDBPackGrp = "B" Then
                    sPackGroup = "PG I"
                ElseIf sDBPackGrp = "C" Then
                    sPackGroup = "PG II"
                ElseIf sDBPackGrp = "D" Then
                    sPackGroup = "PG III"
                End If
                sPackGroup = sPackGroup + ","
            End If

            Dim sHazClass As String = String.Empty
            If getFormDFValue(oForm, "U_HazClass") IsNot Nothing AndAlso getFormDFValue(oForm, "U_HazClass").ToString().Length > 0 Then
                oCombo = oForm.Items.Item("IDH_HAZCLS").Specific
                'sHazClass = oCombo.Selected.Description + ", "
                sHazClass = getFormDFValue(oForm, "U_HazClass") + ", "
            End If

            Dim sUNNumber As String = getFormDFValue(oForm, "U_UNNANo")
            If getFormDFValue(oForm, "U_UNNANo") IsNot Nothing AndAlso getFormDFValue(oForm, "U_UNNANo").ToString().Length > 0 Then
                sUNNumber = getFormDFValue(oForm, "U_UNNANo") + ", "
            End If


            Dim sReportableQty As String
            If getFormDFValue(oForm, "U_IsRepQty").ToString() = "Y" Then
                sReportableQty = "RQ, "
            Else
                sReportableQty = ""
            End If

            Dim sWPTemplate As String
            If getFormDFValue(oForm, "U_IsWaste").ToString() = "Y" Then
                sWPTemplate = "Waste, "
            Else
                sWPTemplate = ""
            End If

            Dim sConstituents As String = getFormDFValue(oForm, "U_Constit")
            If getFormDFValue(oForm, "U_Constit") IsNot Nothing AndAlso getFormDFValue(oForm, "U_Constit").ToString().Length > 0 Then
                sConstituents = " (" + getFormDFValue(oForm, "U_Constit").ToString() + ")"
            End If

            Dim sFULLShipingname As String = String.Empty

            Dim sAddOrder As String = String.Empty
            sAddOrder = Config.INSTANCE.getParameterWithDefault("SHPNMSEQ", "sUNNumber, sWPTemplate, sReportableQty, sProperShipName, sHazClass, sPackGroup, sConstituents")
            'sAddOrder = Config.INSTANCE.getParameter("SHPNMSEQ")

            For Each sVar As String In sAddOrder.Split(",")
                sVar = sVar.Trim()
                sFULLShipingname = sFULLShipingname + If(sVar = "sUNNumber", sUNNumber, "")
                sFULLShipingname = sFULLShipingname + If(sVar = "sWPTemplate", sWPTemplate, "")
                sFULLShipingname = sFULLShipingname + If(sVar = "sReportableQty", sReportableQty, "")
                sFULLShipingname = sFULLShipingname + If(sVar = "sProperShipName", sProperShipName, "")
                sFULLShipingname = sFULLShipingname + If(sVar = "sHazClass", sHazClass, "")
                sFULLShipingname = sFULLShipingname + If(sVar = "sPackGroup", sPackGroup, "")
                sFULLShipingname = sFULLShipingname + If(sVar = "sConstituents", sConstituents, "")
            Next

            'sFULLShipingname = sReportableQty + sWPTemplate + sProperShipName + sHazClass + sUNNumber + sPackGroup
            'sFULLShipingname = sUNNumber + sWPTemplate + sReportableQty + sProperShipName + sHazClass + sPackGroup + sConstituents

            'Set trimed value to the field 
            setFormDFValue(oForm, "U_ShipNm", (sFULLShipingname.Trim()).Trim(","))

        End Sub

#End Region

    End Class
End Namespace
