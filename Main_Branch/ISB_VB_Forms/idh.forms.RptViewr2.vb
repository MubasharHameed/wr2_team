﻿Imports CrystalDecisions.CrystalReports.Engine
Imports System.Drawing.Printing
Imports com.idh.bridge

Imports com.idh.bridge.lookups

Namespace idh.forms
    Public Class RptViewr2
        Inherits IDHAddOns.idh.forms.Base
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            ' MyBase.New(oParent, "IDH_CUSRPT", "IDH_CUSRPT", iMenuPosition, "ReportForm.srf", False, True, False, "Report", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL)
            MyBase.New(oParent, "IDH_CUSRPT", iMenuPosition, "ReportForm.srf", False)
            'oParent.getWinHandle()
        End Sub
        Public Overrides Sub doClose()

        End Sub
        Public Overloads Sub doClose(ByVal oFOrm As SAPbouiCOM.Form)
            Dim bClosed As Boolean = False
            Dim item As SAPbouiCOM.Item
            Try
                item = oFOrm.Items.Item("oActiveX")
            Catch ex As Exception
                Exit Sub
            End Try
            Dim specific As SAPbouiCOM.ActiveX = DirectCast(item.Specific, SAPbouiCOM.ActiveX)
            myRptViewer = (specific.Object)

            If myRptViewer IsNot Nothing Then
                If myRptViewer.cryRpt IsNot Nothing Then
                    If (myRptViewer.cryRpt IsNot Nothing) Then
                        myRptViewer.cryRpt.Clone()
                        myRptViewer.cryRpt.Dispose()
                        GC.Collect()
                        bClosed = True
                    End If
                End If
                If myRptViewer IsNot Nothing Then
                    Try
                        myRptViewer.StartCloseReport()
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doCloseForm")
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXECTF", {Nothing})
                    End Try
                End If
                'System.Threading.Thread.Sleep(200)
                Me.setWFValue(oFOrm, "oActiveX", Nothing)
                'For i As Int32 = myRptViewer.Controls.Count - 1 To 0
                '    myRptViewer.Controls(i).Dispose()
                '    myRptViewer.Controls.RemoveAt(i)
                'Next
                If myRptViewer IsNot Nothing Then
                    Try
                        myRptViewer.Dispose()
                    Catch ex As Exception
                    End Try
                End If
                IDHAddOns.idh.data.Base.doReleaseObject(myRptViewer)
            End If
            'If bClosed Then
            '    PARENT.APPLICATION.StatusBar.SetText("Closed", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            'Else
            '    PARENT.APPLICATION.StatusBar.SetText("Not Closed", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            'End If
        End Sub
        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            'myRptViewer.StartCloseReport()
            doClose(oForm)
            MyBase.doCloseForm(oForm, BubbleEvent)
        End Sub
        Private Sub CloseForm(ByVal oForm As SAPbouiCOM.Form)
            Try
                If myRptViewer IsNot Nothing Then
                    myRptViewer.StartCloseReport()
                End If
                oForm.Close()
                'MyBase.doCloseForm(oForm, BubbleEvent)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doCloseForm")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXECTF", {Nothing})
            End Try
        End Sub
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False AndAlso pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_RESIZE Then
                'Dim oForm As SAPbouiCOM.Form = SBO_Application.Forms.ActiveForm
                'myRptViewer.SuspendLayout()
                oForm.Freeze(True)
                Dim oItem As SAPbouiCOM.Item = oForm.Items.Item("oActiveX")
                oItem.Width = oForm.Width - 20
                oItem.Height = oForm.Height - 70
                oForm.Freeze(False)
            End If
            Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
        End Function

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)
        End Sub
        Private Sub doAddReportViewer(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Add("oActiveX", SAPbouiCOM.BoFormItemTypes.it_ACTIVE_X)
            'oItem = oForm.Items.Item("oActiveX")
            oItem.Left = 5
            oItem.Top = 5
            oItem.Width = oForm.Width - 20 '1282
            oItem.Height = oForm.Height - 70 '562
            oItem.AffectsFormMode = True
        End Sub
        Private _myRptViewer As ReportViewer2.ReportViewer2
        Private Property myRptViewer() As ReportViewer2.ReportViewer2
            Get
                Return Me._myRptViewer
            End Get
            Set(ByVal value As ReportViewer2.ReportViewer2)
                If (Me._myRptViewer IsNot Nothing) Then
                End If
                Me._myRptViewer = value
            End Set
        End Property
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)

            MyBase.doLoadData(oForm)
        End Sub

        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            'MyBase.doFinalizeShow(oForm)
            If getParentSharedData(oForm, "IDH_RPTSHOW") = "FALSE" OrElse Config.Parameter("MDSWDR") = "FALSE" Then
                oForm.Visible = False
                'If getParentSharedData(oForm, "IDH_RPTPRINT") = "TRUE" Then
                doPrintReport(oForm)
                'doClose()
                doCloseForm(oForm, True)
                CloseForm(oForm)

                'oForm.Close()
                'End If
            Else
                Dim strPath As String = Config.Parameter("REPDIR") & "\" 'System.AppDomain.CurrentDomain.BaseDirectory()
                strPath = (strPath & getParentSharedData(oForm, "IDH_RPTNAME"))
                If Not System.IO.File.Exists(strPath) Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Error: ", "Report file not found")
                    com.idh.bridge.DataHandler.INSTANCE.doResConfigError("Report file not found", "ERCONREP", {String.Empty})
                    'doClose()
                    doCloseForm(oForm, False)
                    CloseForm(oForm)
                    Return
                End If
                doAddReportViewer(oForm)
                MyBase.doFinalizeShow(oForm)
                oForm.Freeze(False)
                doLoadnShowReport(oForm)
                'LoadReportNew(oForm)
            End If
            'oForm.Freeze(False)
            'myRptViewer.RptViewrBase.Height = 300

        End Sub
        Protected Overrides Sub doSetEventFilters()
            'Me.doAddEvent(1)
            Me.doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_RESIZE)
            Me.doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
        End Sub

        Private Sub doPrintReport(ByVal oForm As SAPbouiCOM.Form)
            Dim cryRpt As CrystalDecisions.CrystalReports.Engine.ReportDocument = Nothing
            Try
                Dim sRptPath As String = Config.Parameter("REPDIR") & "\" 'System.AppDomain.CurrentDomain.BaseDirectory()
                sRptPath = (sRptPath & getParentSharedData(oForm, "IDH_RPTNAME"))
                If Not System.IO.File.Exists(sRptPath) Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Error: ", "Report file not found")
                    com.idh.bridge.DataHandler.INSTANCE.doResConfigError("Report file not found", "ERCONREP", {String.Empty})
                    Return
                End If

                cryRpt = New CrystalDecisions.CrystalReports.Engine.ReportDocument()
                cryRpt.Load(sRptPath)

                Dim tableLogonInfo As New CrystalDecisions.Shared.TableLogOnInfo
                Dim connectionInfo As New CrystalDecisions.Shared.ConnectionInfo
                With connectionInfo
                    '.Type = CrystalDecisions.Shared.ConnectionInfoType.SQL
                    .ServerName = Config.Parameter("REPDSN")
                    .DatabaseName = IDHAddOns.idh.addon.Base.APPLICATION.Company.DatabaseName
                    .UserID = Config.Parameter("REPUSR")
                    .Password = Config.Parameter("REPPAS")
                End With
                tableLogonInfo.ConnectionInfo = connectionInfo

                Dim table As CrystalDecisions.CrystalReports.Engine.Table
                For Each table In cryRpt.Database.Tables
                    cryRpt.Database.Tables(table.Name).ApplyLogOnInfo(tableLogonInfo)
                Next

                Dim oParameters As Collections.Hashtable = getParentSharedData(oForm, "IDH_RPTPARMS")
                Dim parameterFields As CrystalDecisions.Shared.ParameterFields = cryRpt.ParameterFields '.Item(0).

                Try
                    For i As Int32 = 0 To parameterFields.Count - 1
                        Dim parameterField As CrystalDecisions.Shared.ParameterField = parameterFields(i)
                        Dim parametervalues As New CrystalDecisions.Shared.ParameterValues()
                        Dim parameterFieldName As String = parameterField.Name
                        parameterField.CurrentValues.Clear()
                        If (oParameters.ContainsKey(parameterFieldName)) Then
                            Dim objectValue As Object = Runtime.CompilerServices.RuntimeHelpers.GetObjectValue(oParameters(parameterFieldName))
                            If (objectValue.[GetType]() Is GetType(String)) Then
                                parametervalues.AddValue((objectValue.ToString))
                                parameterField.CurrentValues.Add(parametervalues(0))
                            ElseIf (objectValue.[GetType]() Is GetType(Collections.ArrayList)) Then
                                Dim arrayLists As Collections.ArrayList = DirectCast(objectValue, Collections.ArrayList)
                                parametervalues.AddValue((arrayLists(0).ToString))
                                parameterField.CurrentValues.Add(parametervalues(0))
                                parametervalues.Clear()
                                If (parameterField.EnableAllowMultipleValue) Then
                                    Dim count As Integer = arrayLists.Count - 1
                                    For iVal As Integer = 1 To count
                                        parametervalues.AddValue((arrayLists(iVal).ToString))
                                        parameterField.CurrentValues.Add(parametervalues(0))
                                        parametervalues.Clear()
                                    Next
                                End If
                            End If
                        End If
                    Next
                Catch ex As Exception
                    Throw ex
                    Exit Sub
                Finally

                End Try
                'Dim s As String = ""
                'For i As Int32 = 0 To parameterFields.Count - 1
                '    Dim pField As CrystalDecisions.Shared.ParameterField = parameterFields(0)
                '    If pField.CurrentValues.Count > 0 Then
                '        Dim disVal As New CrystalDecisions.Shared.ParameterDiscreteValue
                '        'DirectCast(DirectCast(DirectCast(parameterFields.Item(0), CrystalDecisions.Shared.ParameterField).CurrentValues, System.Collections.ArrayList).Items(0), CrystalDecisions.Shared.ParameterDiscreteValue).Value()
                '        disVal = pField.CurrentValues(0)
                '        s &= parameterFields(i).Name & " " & disVal.Value
                '    End If
                'Next
                'If s <> "" Then
                '    'MessageBox.Show(s)
                'End If
                Dim iCopies As Int32 = 1
                Dim sPrinterName As String = Nothing
                Dim collate As Boolean = False
                Dim printerSetting As com.idh.bridge.PrintSettings = Nothing
                Dim iPageF As Integer = 0
                Dim iPageT As Integer = 0
                Dim bLandScape As Boolean = False
                Dim bGetPrinterSettings As Boolean = True
                'cryRpt.ParameterFields.Clear()
                'cryRpt.ParameterFields.Add(parameterFields)

                If getParentSharedData(oForm, "COPIES") IsNot Nothing Then
                    iCopies = getParentSharedData(oForm, "COPIES")
                End If
                printerSetting = com.idh.bridge.PrintSettings.getPrinterSetting(sRptPath, doGetParentID(oForm.UniqueID), idh.const.Lookup.INSTANCE.doGetBranch(goParent.gsUserName))

                If printerSetting IsNot Nothing Then
                    sPrinterName = printerSetting.msPrinterName
                End If
                If False Then 'Config.Parameter("MDSWPD") = "TRUE" Then 'ShowDialog 
                    Dim printDialog As System.Windows.Forms.PrintDialog = New System.Windows.Forms.PrintDialog()
                    With printDialog
                        .AllowCurrentPage = False
                        .AllowPrintToFile = False
                        .AllowSelection = False
                        .AllowSomePages = True
                    End With
                    Dim printerSettings As System.Drawing.Printing.PrinterSettings = printDialog.PrinterSettings
                    printerSettings.Copies = CShort(iCopies)
                    printerSettings.PrinterName = sPrinterName
                    If (printDialog.ShowDialog(com.idh.controls.SBOWindow.getInstance()) <> Windows.Forms.DialogResult.OK) Then

                    Else
                        bGetPrinterSettings = False
                        sPrinterName = printerSettings.PrinterName
                        iCopies = printDialog.PrinterSettings.Copies
                        collate = printDialog.PrinterSettings.Collate
                        iPageF = printDialog.PrinterSettings.FromPage
                        iPageT = printDialog.PrinterSettings.ToPage
                        bLandScape = printDialog.PrinterSettings.DefaultPageSettings.Landscape
                    End If
                End If
                Dim defaultPrinter As com.idh.bridge.sysdata.Printer
                If bGetPrinterSettings Then
                    If printerSetting IsNot Nothing Then
                        iCopies = printerSetting.miCopies
                        sPrinterName = printerSetting.msPrinterName
                        defaultPrinter = com.idh.bridge.sysdata.Printer.doFind(sPrinterName)
                        If (defaultPrinter Is Nothing) Then
                            defaultPrinter = com.idh.bridge.sysdata.Printer.getDefaultPrinter()
                        End If
                    Else
                        defaultPrinter = com.idh.bridge.sysdata.Printer.getDefaultPrinter()
                        iCopies = 1
                        sPrinterName = defaultPrinter.PrinterName
                    End If

                End If
                If sPrinterName IsNot Nothing AndAlso sPrinterName <> "" Then
                    cryRpt.PrintOptions.PrinterName = sPrinterName
                End If
                cryRpt.PrintToPrinter(iCopies, False, iPageF, iPageT)
                System.Threading.Thread.Sleep(200)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doPrintReport")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPRNT", {Nothing})
            Finally
                If (cryRpt IsNot Nothing) Then
                    cryRpt.Clone()
                    cryRpt.Dispose()
                    GC.Collect()
                End If
            End Try
        End Sub
        Private Sub doLoadnShowReport(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim strPath As String = Config.Parameter("REPDIR") & "\" 'System.AppDomain.CurrentDomain.BaseDirectory()
                strPath = (strPath & getParentSharedData(oForm, "IDH_RPTNAME"))
                If Not System.IO.File.Exists(strPath) Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Error: ", "Report file not found")
                    com.idh.bridge.DataHandler.INSTANCE.doResConfigError("Report file not found", "ERCONREP", {String.Empty})
                    Return
                End If


                Dim paramater As String = "ISBGLOBAL.ReportViewer2.RptViewr2ActiveX"
                Dim item As SAPbouiCOM.Item = oForm.Items.Item("oActiveX")
                Dim specific As SAPbouiCOM.ActiveX = DirectCast(item.Specific, SAPbouiCOM.ActiveX)
                specific.ClassID = (paramater)
                myRptViewer = (specific.Object)
                'Dim RptVwr As CrystalDecisions.Windows.Forms.CrystalReportViewer = myRptViewer.RptViewr


                myRptViewer.ReportPath = strPath
                ' myRptViewer.doLoadReport()
                myRptViewer.SQLUserName = Config.Parameter("REPUSR") '"sa"
                myRptViewer.SQLServerName = Config.Parameter("REPDSN") '"WR1ODBC"
                myRptViewer.DatabaseName = IDHAddOns.idh.addon.Base.APPLICATION.Company.DatabaseName '"WR1"

                myRptViewer.SQLPassword = Config.Parameter("REPPAS") '""

                myRptViewer.ClearParameter()
                Dim oParm As Collections.Hashtable = getParentSharedData(oForm, "IDH_RPTPARMS")
                If oParm IsNot Nothing Then
                    For Each entry As Collections.DictionaryEntry In oParm
                        myRptViewer.AddParameter(entry.Key.ToString, entry.Value)
                        'PARENT.APPLICATION.StatusBar.SetText("Pname:" & entry.Key.ToString & ":Val:" & entry.Value.ToString, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Next
                Else
                    'PARENT.APPLICATION.StatusBar.SetText("No Parameter passed", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                End If
                ' Dim t As String = getSharedData(oForm, "IDH_RPTNAME")
                myRptViewer.AutoSize = False
                'AUTOPRNT check this Flag for auto print
                If getParentSharedData(oForm, "IDH_RPTPRINT") = "TRUE" OrElse Config.Parameter("AUTOPRNT") = "TRUE" Then
                    Dim iCopies As Int32 = 1
                    Dim sPrinterName As String = Nothing
                    Dim collate As Boolean = False
                    Dim printerSetting As com.idh.bridge.PrintSettings = Nothing
                    Dim iPageF As Integer = 0
                    Dim iPageT As Integer = 0
                    Dim bLandScape As Boolean = False


                    If getParentSharedData(oForm, "COPIES") IsNot Nothing Then
                        iCopies = getParentSharedData(oForm, "COPIES")
                    End If
                    myRptViewer.PrintReport = True
                    printerSetting = com.idh.bridge.PrintSettings.getPrinterSetting(strPath & getParentSharedData(oForm, "IDH_RPTNAME"), doGetParentID(oForm.UniqueID), idh.const.Lookup.INSTANCE.doGetBranch(goParent.gsUserName))

                    If printerSetting IsNot Nothing Then
                        sPrinterName = printerSetting.msPrinterName
                    End If
                    Dim bGetPrinterSettings As Boolean = True
                    If False Then 'Config.Parameter("MDSWPD") = "TRUE" Then 'ShowDialog 
                        Dim printDialog As System.Windows.Forms.PrintDialog = New System.Windows.Forms.PrintDialog()
                        With printDialog
                            .AllowCurrentPage = False
                            .AllowPrintToFile = False
                            .AllowSelection = False
                            .AllowSomePages = True
                        End With
                        Dim printerSettings As System.Drawing.Printing.PrinterSettings = printDialog.PrinterSettings
                        printerSettings.Copies = CShort(iCopies)
                        printerSettings.PrinterName = sPrinterName
                        If (printDialog.ShowDialog(com.idh.controls.SBOWindow.getInstance()) <> Windows.Forms.DialogResult.OK) Then
                            Me.doDBDebug("Crystal Viewer", "Print job cancled.", 5)
                        Else
                            bGetPrinterSettings = False
                            sPrinterName = printerSettings.PrinterName
                            iCopies = printDialog.PrinterSettings.Copies
                            collate = printDialog.PrinterSettings.Collate
                            iPageF = printDialog.PrinterSettings.FromPage
                            iPageT = printDialog.PrinterSettings.ToPage
                            bLandScape = printDialog.PrinterSettings.DefaultPageSettings.Landscape
                            Me.doDBDebug("Crystal Viewer", String.Concat("Printer Selected [", sPrinterName, "]."), 5)
                        End If

                    End If
                    Dim defaultPrinter As com.idh.bridge.sysdata.Printer '= com.idh.bridge.sysdata.Printer.getDefaultPrinter()
                    If bGetPrinterSettings Then
                        If printerSetting IsNot Nothing Then
                            iCopies = printerSetting.miCopies
                            sPrinterName = printerSetting.msPrinterName
                            defaultPrinter = com.idh.bridge.sysdata.Printer.doFind(sPrinterName)
                            If (defaultPrinter Is Nothing) Then
                                defaultPrinter = com.idh.bridge.sysdata.Printer.getDefaultPrinter()
                            End If
                        Else
                            defaultPrinter = com.idh.bridge.sysdata.Printer.getDefaultPrinter()
                            iCopies = 1
                            sPrinterName = defaultPrinter.PrinterName
                        End If
                    End If
                    myRptViewer.PagesFrom = iPageF
                    myRptViewer.PagesTo = iPageT
                    myRptViewer.PrinterName = sPrinterName
                    myRptViewer.Copies = iCopies
                End If
                'myRptViewer.Zoom = 70
                myRptViewer.Zoom = 66

                myRptViewer.StartReport()
                If myRptViewer.oException IsNot Nothing Then
                    Dim ex As Exception = myRptViewer.oException
                End If
                'System.Threading.Thread.Sleep(500)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Loading Report")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXELRT", {Nothing})
            End Try
        End Sub

        Private Sub LoadReportNew(ByVal oForm As SAPbouiCOM.Form) 'As Object
            Dim cryRpt2 As CrystalDecisions.CrystalReports.Engine.ReportDocument

            Try
                Dim strPath As String = Config.Parameter("REPDIR") & "\" 'System.AppDomain.CurrentDomain.BaseDirectory()
                strPath = (strPath & getParentSharedData(oForm, "IDH_RPTNAME"))
                If Not System.IO.File.Exists(strPath) Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Error: ", "Report file not found")
                    com.idh.bridge.DataHandler.INSTANCE.doResConfigError("Report file not found", "ERCONREP", {String.Empty})
                    Return
                End If


                Dim paramater As String = "ISBGLOBAL.ReportViewer2.RptViewr2ActiveX"
                Dim item As SAPbouiCOM.Item = oForm.Items.Item("oActiveX")
                Dim specific As SAPbouiCOM.ActiveX = DirectCast(item.Specific, SAPbouiCOM.ActiveX)
                specific.ClassID = (paramater)
                myRptViewer = (specific.Object)


                cryRpt2 = New CrystalDecisions.CrystalReports.Engine.ReportDocument()
                cryRpt2.Load(strPath)

                myRptViewer.SQLUserName = Config.Parameter("REPUSR") '"sa"
                myRptViewer.SQLServerName = Config.Parameter("REPDSN") '"WR1ODBC"
                myRptViewer.DatabaseName = IDHAddOns.idh.addon.Base.APPLICATION.Company.DatabaseName '"WR1"

                myRptViewer.SQLPassword = Config.Parameter("REPPAS") '""

                myRptViewer.ClearParameter()
                Dim oParm As Collections.Hashtable = getParentSharedData(oForm, "IDH_RPTPARMS")
                If oParm IsNot Nothing Then
                    For Each entry As Collections.DictionaryEntry In oParm
                        myRptViewer.AddParameter(entry.Key.ToString, entry.Value)
                        'PARENT.APPLICATION.StatusBar.SetText("Pname:" & entry.Key.ToString & ":Val:" & entry.Value.ToString, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Next
                Else
                    'PARENT.APPLICATION.StatusBar.SetText("No Parameter passed", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                End If
                ' Dim t As String = getSharedData(oForm, "IDH_RPTNAME")
                myRptViewer.AutoSize = False


                If getParentSharedData(oForm, "IDH_RPTPRINT") = "TRUE" OrElse Config.Parameter("AUTOPRNT") = "TRUE" Then
                    Dim iCopies As Int32 = 1
                    Dim sPrinterName As String = Nothing
                    Dim collate As Boolean = False
                    Dim printerSetting As com.idh.bridge.PrintSettings = Nothing
                    Dim iPageF As Integer = 0
                    Dim iPageT As Integer = 0
                    Dim bLandScape As Boolean = False

                    If getParentSharedData(oForm, "COPIES") IsNot Nothing Then
                        iCopies = getParentSharedData(oForm, "COPIES")
                    End If
                    myRptViewer.PrintReport = True
                    printerSetting = com.idh.bridge.PrintSettings.getPrinterSetting(strPath & getParentSharedData(oForm, "IDH_RPTNAME"), doGetParentID(oForm.UniqueID), idh.const.Lookup.INSTANCE.doGetBranch(goParent.gsUserName))

                    If printerSetting IsNot Nothing Then
                        sPrinterName = printerSetting.msPrinterName
                    End If
                    Dim bGetPrinterSettings As Boolean = True
                    If False Then 'Config.Parameter("MDSWPD") = "TRUE" Then 'ShowDialog 
                        Dim printDialog As System.Windows.Forms.PrintDialog = New System.Windows.Forms.PrintDialog()
                        With printDialog
                            .AllowCurrentPage = False
                            .AllowPrintToFile = False
                            .AllowSelection = False
                            .AllowSomePages = True
                        End With
                        Dim printerSettings As System.Drawing.Printing.PrinterSettings = printDialog.PrinterSettings
                        printerSettings.Copies = CShort(iCopies)
                        printerSettings.PrinterName = sPrinterName
                        If (printDialog.ShowDialog(com.idh.controls.SBOWindow.getInstance()) <> Windows.Forms.DialogResult.OK) Then
                            Me.doDBDebug("Crystal Viewer", "Print job cancled.", 5)
                        Else
                            bGetPrinterSettings = False
                            sPrinterName = printerSettings.PrinterName
                            iCopies = printDialog.PrinterSettings.Copies
                            collate = printDialog.PrinterSettings.Collate
                            iPageF = printDialog.PrinterSettings.FromPage
                            iPageT = printDialog.PrinterSettings.ToPage
                            bLandScape = printDialog.PrinterSettings.DefaultPageSettings.Landscape
                            Me.doDBDebug("Crystal Viewer", String.Concat("Printer Selected [", sPrinterName, "]."), 5)
                        End If

                    End If
                    Dim defaultPrinter As com.idh.bridge.sysdata.Printer '= com.idh.bridge.sysdata.Printer.getDefaultPrinter()
                    If bGetPrinterSettings Then
                        If printerSetting IsNot Nothing Then
                            iCopies = printerSetting.miCopies
                            sPrinterName = printerSetting.msPrinterName
                            defaultPrinter = com.idh.bridge.sysdata.Printer.doFind(sPrinterName)
                            If (defaultPrinter Is Nothing) Then
                                defaultPrinter = com.idh.bridge.sysdata.Printer.getDefaultPrinter()
                            End If
                        Else
                            defaultPrinter = com.idh.bridge.sysdata.Printer.getDefaultPrinter()
                            iCopies = 1
                            sPrinterName = defaultPrinter.PrinterName
                        End If
                    End If
                    myRptViewer.PagesFrom = iPageF
                    myRptViewer.PagesTo = iPageT
                    myRptViewer.PrinterName = sPrinterName
                    myRptViewer.Copies = iCopies
                End If
                myRptViewer.Zoom = 66
                myRptViewer.cryRpt = cryRpt2
                myRptViewer.StartReportNew()

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doPrintReport")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPRNT", {Nothing})
            Finally
            End Try

        End Sub
 
    End Class

End Namespace
