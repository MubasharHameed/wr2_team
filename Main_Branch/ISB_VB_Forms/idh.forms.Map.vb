Imports System.IO
Imports System.Collections
Imports System
Imports com.idh.bridge
Imports com.idh.bridge.lookups


Namespace idh.forms
    Public Class Map
        Inherits IDHAddOns.idh.forms.Base
        Dim AxBrowser As SHDocVw.InternetExplorer
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHMAP", Nothing, 0, "Map.srf", False, True, False, "Map", load_Types.idh_LOAD_NORMAL)
        End Sub

'        '*** Create Sub-Menu
'        Protected Overrides Sub doCreateSubMenu()
'        	doCreateFormMenu(goParent.goMenuID, "Map Search")
''            Dim oMenus As SAPbouiCOM.Menus
''            Dim oMenuItem As SAPbouiCOM.MenuItem
''            Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
''
''            oMenuItem = goParent.goApplication.Menus.Item(goParent.goMenuID)
''            oMenus = oMenuItem.SubMenus
''
''            oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
''            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING
''            oCreationPackage.UniqueID = gsType
''            oCreationPackage.String = "Map Search"
''            oCreationPackage.Position = 10
''
''            Try
''                oMenus.AddEx(oCreationPackage)
''            Catch ex As Exception
''                gcom.idh.bridge.DataHandler.INSTANCE.doError("idh.forms.ProductSearch", "doCreateSubMenu", "Exception: " & ex.ToString, "")
''            End Try
'        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_RESIZE)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            doAddWF(oForm, "PCodes")
            Dim oItem As SAPbouiCOM.Item

            oForm.Freeze(True)
            Try
'                Dim l As Integer
'                Dim t As Integer
'                Dim w As Integer
'                Dim h As Integer

                oForm.Height = goParent.goApplication.Desktop.Height()
                oForm.Top = 100
                'oForm.Left = goParent.goApplication.Desktop.Width() - oForm.Width - 10
                oForm.Left = 200
                oForm.Items.Item("1").Top = oForm.Height - 58

                oItem = oForm.Items.Item("IDH_MapRec")
                oItem.Left = 5
                oItem.Top = 5
                oItem.Width = oForm.Width - 17
                oItem.Height = oForm.Height - 70

                oItem = oForm.Items.Add("IDHBRW", SAPbouiCOM.BoFormItemTypes.it_ACTIVE_X)
                oItem.Top = 15
                oItem.Left = 15
                oItem.Width = oForm.Width - 37
                oItem.Height = oForm.Height - 88
                oItem.AffectsFormMode = False

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            If ghOldDialogParams.Contains(oForm.UniqueID) Then
                Dim oData As ArrayList = ghOldDialogParams.Item(oForm.UniqueID)
                If Not (oData Is Nothing) Then
                    If oData.Count > 1 AndAlso Not (oData.Item(1) Is Nothing) Then
                        setWFValue(oForm, "PCodes", oData.Item(1))
                    End If
                End If
            End If
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim sPCode As String
                sPCode = getWFValue(oForm, "PCodes")

                Dim owItem As SAPbouiCOM.Item
                owItem = oForm.Items.Item("IDHBRW")
                Dim oBrowser As SAPbouiCOM.ActiveX
                oBrowser = owItem.Specific
                oBrowser.ClassID = Config.INSTANCE.getParameter("AXBRW") 'Config.Parameter("AXBRW") '"Shell.Explorer.2"
                AxBrowser = oBrowser.Object

                sPCode = sPCode.Replace(" ", "%20")

                Dim sSize As String = "Width=" & (owItem.Width - 20) & "&Height=" & (owItem.Height - 40)
                'AxBrowser.Navigate2("http://www.google.com/maps?q=" & sPCode)
                'Dim sURL As String = "http://maps.idhweb.com/maps/isb.jsp?" & sSize & "&" & sPCode
                Dim sURL As String = Config.Parameter("MAPURL") & "?" & sSize & "&" & sPCode
                AxBrowser.Navigate2(sURL)

                '//http://www.streetmap.co.uk/streetmap.dll?MfcISAPICommand=GridConvert&type=PostCode&name=TW8%208LB
            Catch ex As Exception

            End Try
        End Sub

        '*** Get the selected rows
        Private Sub doPrepareModalData(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        End Sub

        Public Overrides Sub doButtonID2(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_RESIZE Then
                If pVal.BeforeAction = False Then
                    Dim oItem As SAPbouiCOM.Item
                    Dim oItemRec As SAPbouiCOM.Item
                    oItem = oForm.Items.Item("IDHBRW")

                    oItemRec = oForm.Items.Item("IDH_MapRec")
                    If oForm.Width > 100 Then
                        oItemRec.Width = oForm.Width - 20
                    End If

                    If oForm.Height > 50 Then
                        oItemRec.Height = oForm.Height - 70
                    End If
                    oItem.Width = oItemRec.Width - 10
                    oItem.Height = oItemRec.Height - 10
                    doLoadData(oForm)
                End If
            End If
            Return True
        End Function

        Public Overrides Sub doClose()
        End Sub

    End Class
End Namespace
