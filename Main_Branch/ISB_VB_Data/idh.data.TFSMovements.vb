Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge
Imports com.idh.dbObjects.User
Imports com.idh.utils
Imports com.idh.dbObjects.numbers
'
' Created by SharpDevelop.
' User: Louis Viljoen
' Date: 02/06/2008
' Time: 14:47
' 
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Namespace idh.data
    Public Class TFSMovements

        Private Sub doAddFields()
            doAddField("Code", SAPbouiCOM.BoFieldsType.ft_Text, 8)
            doAddField("Name", SAPbouiCOM.BoFieldsType.ft_Text, 20)
            'doAddField("ArchiveDate", SAPbouiCOM.BoFieldsType.ft_Date, 10)
            doAddField("U_ShpNo", SAPbouiCOM.BoFieldsType.ft_Text, 50)
            doAddField("U_PreNotDt", SAPbouiCOM.BoFieldsType.ft_Date, 10)
            doAddField("U_ShpDt", SAPbouiCOM.BoFieldsType.ft_Date, 10)
            'doAddField("U_ShpDt", SAPbouiCOM.BoFieldsType.ft_Text, 10)
            doAddField("U_CarCd", SAPbouiCOM.BoFieldsType.ft_Text, 20)
            doAddField("U_CarNm", SAPbouiCOM.BoFieldsType.ft_Text, 50)
            doAddField("U_Shipd", SAPbouiCOM.BoFieldsType.ft_Text, 50)
            doAddField("U_UOM", SAPbouiCOM.BoFieldsType.ft_Text, 20)
            doAddField("U_Bal", SAPbouiCOM.BoFieldsType.ft_Text, 20)
            doAddField("U_Status", SAPbouiCOM.BoFieldsType.ft_Text, 20)
            doAddField("U_TFSCd", SAPbouiCOM.BoFieldsType.ft_Text, 20)
            doAddField("U_TFSNo", SAPbouiCOM.BoFieldsType.ft_Text, 20)

            doAddField("U_WRHdr", SAPbouiCOM.BoFieldsType.ft_Text, 8)
            doAddField("U_WRRow", SAPbouiCOM.BoFieldsType.ft_Text, 8)
            doAddField("U_ReceiptDt", SAPbouiCOM.BoFieldsType.ft_Date, 10)
            doAddField("U_Tonnage", SAPbouiCOM.BoFieldsType.ft_Text, 8)
            doAddField("U_RecoveryDt", SAPbouiCOM.BoFieldsType.ft_Date, 10)
        End Sub

        ''MORE DATA HANDLING
        'U_JobNr,"U_RowNr","U_ItemCd","U_ItemDsc","U_WgtDed","U_ValDed"
        Public Sub doAddTFSMovement( _
          ByVal sTFSCd As String, _
          ByVal sTFSNo As String, _
          ByVal sShpNo As String, _
          ByVal dPreNotDt As Date, _
          ByVal sShpDt As String, _
          ByVal sCarCd As String, _
          ByVal sCarNm As String, _
          ByVal sShipd As String, _
          ByVal sUOM As String, _
          ByVal sBal As String, _
          ByVal sStatus As String, _
          ByVal sWRHdr As String, _
          ByVal sWRRow As String, _
          ByVal dReceiptDt As Date, _
          ByVal sTonnage As String,
          ByVal dRecoveryDt As Date)

            miCurrentRow = doAddRow()

            Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(IDH_TFSMAST.AUTONUMPREFIX, "TFSMOV")
            setFieldValue("Code", oNumbers.CodeCode)
            setFieldValue("Name", oNumbers.NameCode)
            'setFieldValue("ArchiveDate", sKey)
            setFieldValue("U_TFSCd", sTFSCd)
            setFieldValue("U_TFSNo", sTFSNo)
            setFieldValue("U_ShpNo", sShpNo)
            setFieldValue("U_PreNotDt", dPreNotDt)
            setFieldValue("U_ShpDt", sShpDt)
            setFieldValue("U_CarCd", sCarCd)
            setFieldValue("U_CarNm", sCarNm)
            'setFieldValue("U_Shipd", sShipd)
            setFieldValue("U_Shipd", com.idh.utils.Dates.doStrToDate(sShipd))
            setFieldValue("U_UOM", sUOM)
            setFieldValue("U_Bal", sBal)
            setFieldValue("U_Status", sStatus)
            setFieldValue("U_WRHdr", sWRHdr)
            setFieldValue("U_WRRow", sWRRow)
            setFieldValue("U_ReceiptDt", dReceiptDt)
            setFieldValue("U_Tonnage", sTonnage)
            setFieldValue("U_RecoveryDt", dRecoveryDt)
        End Sub

#Region "GridLikeControl"
        Protected moRows As New ArrayList()
        Protected moHead As New ArrayList()
        Protected moType As New ArrayList()
        Protected moSize As New ArrayList()

        Protected miRows As Integer = 0
        Protected miCols As Integer = 0

        Protected moParent As IDHAddOns.idh.addon.Base
        Protected msTable As String
        Protected miCurrentRow As Integer = 0

        Protected mhChangedRowNr As Hashtable = New Hashtable 'The rows that was changed
        Protected maAddedRows As ArrayList = New ArrayList 'The list of rows added
        Protected maDeletedRows As ArrayList = New ArrayList 'Contains the list of deleted rows

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sTable As String)
            moParent = oParent
            msTable = sTable

            doAddFields()
        End Sub

        Public Function GetByJob(ByVal sTFSCd As String) As Integer
            Return doQuery(" U_TFSCd = '" + sTFSCd + "'")
        End Function

        'Public Function GetByJobRow(ByVal sRow As String) As Integer
        '    Return doQuery(" U_RowNr = '" + sRow + "'")
        'End Function

        Public Function GetByCode(ByVal sCode As String) As Integer
            Return doQuery(" Code = '" + sCode + "'")
        End Function

        Public Sub doAddField(ByVal sFieldName As String, ByVal oType As SAPbouiCOM.BoFieldsType, ByVal iWidth As Integer)
            moHead.Add(sFieldName)
            moType.Add(oType)
            moSize.Add(iWidth)
        End Sub

        Private Function doCreateSelectList() As String
            Dim sList As String = ""
            For i As Integer = 0 To moHead.Count - 1
                If i > 0 Then
                    sList = sList & ","
                End If
                sList = sList & CType(moHead.Item(i), String)
            Next

            Return sList
        End Function

        Private Function doQuery(ByVal sWhere As String) As Integer
            moRows.Clear()
            maAddedRows.Clear()
            maDeletedRows.Clear()
            mhChangedRowNr.Clear()

            miCurrentRow = 0

            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing

            Dim sSelectList As String = doCreateSelectList()
            Dim sQry As String = "SELECT " & sSelectList & " FROM " & msTable & " WHERE " & sWhere
            Try
                oRecordSet = moParent.goDB.doSelectQuery(sQry)
                If oRecordSet Is Nothing Then
                    Return 0
                End If
                miCols = oRecordSet.Fields.Count()
                miRows = oRecordSet.RecordCount()

                If miRows > 0 Then
                    While Not oRecordSet.EoF
                        Dim oRowData(moHead.Count) As Object
                        For i As Integer = 0 To moHead.Count() - 1
                            oRowData(i) = oRecordSet.Fields.Item(i).Value()
                        Next

                        moRows.Add(oRowData)
                        oRecordSet.MoveNext()

                    End While
                End If
                Return miRows
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, sQry)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", {sQry})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
            Return 0
        End Function

        Public Function getRowCount() As Integer
            If moRows Is Nothing Then
                Return 0
            Else
                Return moRows.Count()
            End If
        End Function

        Public Function getColCount() As Integer
            If moHead Is Nothing Then
                Return 0
            Else
                Return moHead.Count()
            End If
        End Function

        Public Function getColName(ByVal iIndex As Integer) As String
            If moHead Is Nothing Then
                Return ""
            Else
                Return CType(moHead.Item(iIndex), String)
            End If
        End Function

        Public Function getColIndex(ByVal sFieldName As String) As Integer
            If moHead Is Nothing Then
                Return -1
            Else
                Return moHead.IndexOf(sFieldName)
            End If
        End Function

        Public Function getColType(ByVal iIndex As Integer) As SAPbouiCOM.BoFieldsType
            If moType Is Nothing Then
                Return SAPbouiCOM.BoFieldsType.ft_AlphaNumeric
            Else
                Return CType(moType.Item(iIndex), SAPbouiCOM.BoFieldsType)
            End If
        End Function

        Public Function getColSize(ByVal iIndex As Integer) As Integer
            If moSize Is Nothing Then
                Return 0
            Else
                Return CType(moSize.Item(iIndex), Integer)
            End If
        End Function

        Private Sub setFieldValue(ByVal sFieldName As String, ByVal oValue As Object)
            Dim iFieldIndex As Integer = moHead.IndexOf(sFieldName)
            setFieldValue(iFieldIndex, miCurrentRow, oValue)
        End Sub

        Public Sub setFieldValue(ByVal iFieldIndex As Integer, ByVal oValue As Object)
            setFieldValue(iFieldIndex, miCurrentRow, oValue)
        End Sub

        Public Sub setFieldValue(ByVal sFieldName As String, ByVal iRow As Integer, ByVal oValue As Object)
            Dim iFieldIndex As Integer = moHead.IndexOf(sFieldName)
            setFieldValue(iFieldIndex, iRow, oValue)
        End Sub

        Public Sub setFieldValue(ByVal iFieldIndex As Integer, ByVal iRow As Integer, ByVal oValue As Object)
            If iFieldIndex >= 0 Then
                Dim aRowData() As Object = CType(moRows.Item(iRow), Object())
                If iFieldIndex < moHead.Count Then
                    aRowData(iFieldIndex) = oValue
                End If

                If maAddedRows.Contains(iRow) = False AndAlso _
                    maDeletedRows.Contains(iRow) = False Then
                    Dim oRowData As ArrayList
                    If mhChangedRowNr.ContainsKey(iRow) Then
                        oRowData = CType(mhChangedRowNr.Item(iRow), ArrayList)
                        If oRowData.Contains(iFieldIndex) = False Then
                            oRowData.Add(iFieldIndex)
                        End If
                    Else
                        oRowData = New ArrayList
                        oRowData.Add(iFieldIndex)
                        mhChangedRowNr.Add(iRow, oRowData)
                    End If
                End If
            End If
        End Sub

        Public Function getFieldValue(ByVal sFieldName As String) As Object
            Dim iFieldIndex As Integer = moHead.IndexOf(sFieldName)
            Return getFieldValue(iFieldIndex, miCurrentRow)
        End Function

        Public Function getFieldValue(ByVal iFieldIndex As Integer) As Object
            Return getFieldValue(iFieldIndex, miCurrentRow)
        End Function

        Public Function getFieldValue(ByVal sFieldName As String, ByVal iRow As Integer) As Object
            Dim iFieldIndex As Integer = moHead.IndexOf(sFieldName)
            Return getFieldValue(iFieldIndex, iRow)
        End Function

        Public Function getFieldValue(ByVal iFieldIndex As Integer, ByVal iRow As Integer) As Object
            If iFieldIndex < 0 Then
                Return Nothing
            End If
            Dim aRowData() As Object = CType(moRows.Item(iRow), Object())
            If iFieldIndex < aRowData.Length Then
                Return aRowData(iFieldIndex)
            Else
                Return Nothing
            End If
        End Function

        Public Function doFindEntry(ByVal sCode As String) As Integer
            Dim iFieldIndex As Integer = moHead.IndexOf("Code")
            Dim iRow As Integer
            Dim sValue As String
            For iRow = 0 To moRows.Count - 1
                sValue = Conversions.ToString(CType(moRows.Item(iRow), Object())(iFieldIndex))
                If sValue.Equals(sCode) Then
                    Return iRow
                End If
            Next
            Return -1
        End Function

        ' Set the Job Row Code for all rows with a Row Code of -1
        Public Sub doSetTFSNumber(ByVal sTFSNumber As String)
            Dim iFieldIndex As Integer = moHead.IndexOf("U_TFSNo")
            Dim iRow As Integer
            Dim sValue As String
            For iRow = 0 To moRows.Count - 1
                sValue = Conversions.ToString(CType(moRows.Item(iRow), Object())(iFieldIndex))
                If sValue.Equals("-1") Then
                    setFieldValue(iFieldIndex, iRow, sTFSNumber)
                End If
            Next
        End Sub

        Public Function doAddRow() As Integer
            Dim oRowData(moHead.Count) As Object
            moRows.Add(oRowData)
            maAddedRows.Add(moRows.Count - 1)
            Return moRows.Count - 1
        End Function

        '' Will try to find the row if not found add it
        Public Function doSetRow(ByVal sCode As String) As Integer
            Dim iIndex As Integer
            iIndex = doFindEntry(sCode)
            If iIndex = -1 Then
                Return doAddRow()
            Else
                Return iIndex
            End If
        End Function

        '' Remove the Row - Only mark to be deleted
        Public Sub doRemoveRow(ByVal sCode As String)
            doRemoveRow(doFindEntry(sCode))
        End Sub

        '' Remove the Row - Only mark to be deleted
        Public Sub doRemoveRow(ByVal iRow As Integer)
            If maDeletedRows.Contains(iRow) = False Then
                maDeletedRows.Add(iRow)
            End If
        End Sub

        Public Function hasChanged() As Boolean
            If maAddedRows.Count > 0 OrElse mhChangedRowNr.Count > 0 OrElse maDeletedRows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Function wasAdded(ByVal iRow As Integer) As Boolean
            Return maAddedRows.Contains(iRow)
        End Function

        Public Function wasDeleted(ByVal iRow As Integer) As Boolean
            Return maDeletedRows.Contains(iRow)
        End Function

        Public Function doGetChangedRows() As ArrayList
            If mhChangedRowNr Is Nothing OrElse mhChangedRowNr.Count = 0 Then
                Return Nothing
            End If

            Dim oArray As ArrayList
            oArray = New ArrayList(mhChangedRowNr.Count)

            Dim en As IEnumerator = mhChangedRowNr.Keys.GetEnumerator
            While en.MoveNext
                oArray.Add(en.Current)
            End While
            Return oArray
        End Function

        'Public Function doRemoveRowEntries(ByVal sRow As String) As Boolean
        '    If getRowCount() > 0 Then
        '        For iRow As Integer = 0 To getRowCount() - 1
        '            Dim sRowCodeW As String = getFieldValue("U_RowNr", iRow)
        '            If Not sRowCodeW Is Nothing AndAlso sRowCodeW.Equals(sRow) Then
        '                doRemoveRow(iRow)
        '            End If
        '        Next
        '    End If
        '    Return True
        'End Function

        Private Function doProcessChangedRows() As Boolean
            If mhChangedRowNr Is Nothing OrElse mhChangedRowNr.Count = 0 Then
                Return True
            End If

            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(moParent, msTable)
            Try
                Dim oFields As ArrayList
                Dim iRow As Integer
                Dim sKey As String
                Dim iwResult As Integer
                Dim swResult As String = Nothing
                Dim oValue As Object
                Dim iFieldIndex As Integer
                Dim sField As String = ""

                Dim oList As ArrayList = doGetChangedRows()
                Dim iIndex As Integer = 0
                Dim iCount As Integer = oList.Count
                While iIndex < iCount
                    iRow = CType(oList.Item(iIndex), Integer)
                    If wasDeleted(iRow) Then
                        mhChangedRowNr.Remove(iRow)
                    Else
                        sKey = CType(getFieldValue("Code", iRow), String)
                        If sKey.Trim().Length > 0 Then

                            If oAuditObj.setKeyPair(sKey, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) Then
                                oFields = CType(mhChangedRowNr.Item(iRow), ArrayList)
                                If Not (oFields Is Nothing) AndAlso oFields.Count > 0 Then
                                    For ii As Integer = 0 To oFields.Count - 1
                                        iFieldIndex = CType(oFields.Item(ii), Integer)
                                        Try
                                            sField = CType(moHead(iFieldIndex), String)
                                            oValue = getFieldValue(iFieldIndex, iRow)
                                            If sField.Equals("Name") = True Then
                                                oAuditObj.setName(CType(oValue, String))
                                            ElseIf sField.Equals("Code") = False Then
                                                oAuditObj.setFieldValue(sField, oValue)
                                            End If
                                        Catch ex As Exception
                                            'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing changed rows - " & msTable & "." & sField)
                                            DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPARS", {msTable, sField})
                                        End Try
                                    Next

                                    iwResult = oAuditObj.Commit()

                                    If iwResult <> 0 Then
                                        moParent.goDICompany.GetLastError(iwResult, swResult)
                                        'com.idh.bridge.DataHandler.INSTANCE.doError("Error Updating: " + swResult)
                                        DataHandler.INSTANCE.doResSystemError("Error Updating: " + swResult, "ERSYUGEN", {swResult})
                                        Return False
                                    Else
                                        mhChangedRowNr.Remove(iRow)
                                    End If
                                End If
                            End If
                        End If
                    End If
                    iIndex = iIndex + 1
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the changed rows.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBCR", {Nothing})
                Return False
            Finally
                oAuditObj.Close()
                DataHandler.INSTANCE.doReleaseObject(CType(oAuditObj, Object))
            End Try
            Return True
        End Function

        Private Function doProcessAddedRows() As Boolean
            If maAddedRows Is Nothing OrElse maAddedRows.Count = 0 Then
                Return True
            End If

            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(moParent, msTable)
            Try
                Dim iRow As Integer
                Dim sField As String
                Dim oValue As Object
                Dim sKey As String
                Dim sName As String
                Dim iwResult As Integer
                Dim swResult As String = Nothing

                Dim iFieldsCount As Integer = oAuditObj.getFieldsCount()

                Dim iIndex As Integer = 0
                Dim iCount As Integer = maAddedRows.Count
                While iIndex < iCount
                    iRow = CType(maAddedRows.Item(iIndex), Integer)
                    If wasDeleted(iRow) Then
                        maAddedRows.Remove(iRow)
                    Else
                        sKey = CType(getFieldValue("Code", iRow), String)
                        If sKey.Trim().Length > 0 Then

                            oAuditObj.setKeyPair(sKey, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
                            sName = CType(getFieldValue("Name", iRow), String)
                            oAuditObj.setName(sName)

                            For ii As Integer = 0 To iFieldsCount - 1
                                sField = oAuditObj.getFieldName(ii)
                                Try
                                    oValue = getFieldValue(sField, iRow)
                                    If Not oValue Is Nothing Then
                                        oAuditObj.setFieldValue(ii, oValue)
                                    End If
                                Catch ex As Exception
                                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing added the rows - " & msTable & "." & sField)
                                    DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPARS", {msTable, sField})
                                End Try
                            Next

                            iwResult = oAuditObj.Commit()

                            If iwResult <> 0 Then
                                moParent.goDICompany.GetLastError(iwResult, swResult)
                                'com.idh.bridge.DataHandler.INSTANCE.doError("Error Adding the rows: " + swResult)
                                DataHandler.INSTANCE.doResSystemError("Error Adding the rows: " + swResult, "ERSYADDR", {swResult})
                                Return False
                            Else
                                maAddedRows.Remove(iRow)
                                iCount = iCount - 1
                                iIndex = iIndex - 1
                            End If
                        End If
                    End If
                    iIndex = iIndex + 1
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error processing the added rows.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPAR", {Nothing})
                Return False
            Finally
                oAuditObj.Close()
                DataHandler.INSTANCE.doReleaseObject(CType(oAuditObj, Object))
            End Try
            Return True
        End Function

        Private Function doProcessRemovedRows() As Boolean
            If maDeletedRows Is Nothing OrElse maDeletedRows.Count = 0 Then
                Return True
            End If

            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(moParent, msTable)
            Try
                Dim iwResult As Integer
                Dim swResult As String = Nothing
                Dim sKey As String

                Dim iRow As Integer = 0
                Dim iIndex As Integer
                Dim iCount As Integer = maDeletedRows.Count
                While iIndex < iCount
                    iRow = CType(maDeletedRows.Item(iIndex), Integer)

                    If maAddedRows.Contains(iRow) Then
                        doRemoveRowFromBuffer(iRow)
                        iIndex = iIndex - 1
                        iCount = iCount - 1
                    Else
                        sKey = CType(getFieldValue("Code", iRow), String)

                        If sKey.Trim().Length > 0 Then
                            oAuditObj.setKeyPair(sKey, IDHAddOns.idh.data.AuditObject.ac_Types.idh_REMOVE)
                            iwResult = oAuditObj.Commit()

                            If iwResult <> 0 Then
                                moParent.goDICompany.GetLastError(iwResult, swResult)
                                'com.idh.bridge.DataHandler.INSTANCE.doError("Error Removing the rows: " + swResult, "Error Removing the rows.")
                                DataHandler.INSTANCE.doResSystemError("Error Removing the rows: " + swResult, "ERSYRGEN", {swResult})
                                Return False
                            Else
                                doRemoveRowFromBuffer(iRow)
                                iIndex = iIndex - 1
                                iCount = iCount - 1
                            End If
                        End If
                    End If
                    iIndex = iIndex + 1
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error Removing the rows.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRGEN", {Nothing})
                Return False
            Finally
                oAuditObj.Close()
                DataHandler.INSTANCE.doReleaseObject(CType(oAuditObj, Object))
            End Try
            Return True
        End Function

        Private Sub doRemoveRowFromBuffer(ByVal iRow As Integer)
            If maAddedRows.Contains(iRow) Then
                maAddedRows.Remove(iRow)
            End If

            If mhChangedRowNr.ContainsKey(iRow) Then
                mhChangedRowNr.Remove(iRow)
            End If

            If maDeletedRows.Contains(iRow) Then
                maDeletedRows.Remove(iRow)
            End If

            Dim iIndex As Integer
            For iIndex = 0 To maAddedRows.Count - 1
                If CType(maAddedRows.Item(iIndex), Integer) > iRow Then
                    maAddedRows.Item(iIndex) = CType(maAddedRows.Item(iIndex), Integer) - 1
                End If
            Next

            Dim en As IEnumerator = mhChangedRowNr.Keys.GetEnumerator
            While en.MoveNext
                iIndex = CType(en.Current, Integer)
                If iIndex > iRow Then
                    mhChangedRowNr.Add(iIndex - 1, mhChangedRowNr.Item(iIndex))
                    mhChangedRowNr.Remove(iIndex)
                End If
            End While

            For iIndex = 0 To maDeletedRows.Count - 1
                If CType(maAddedRows.Item(iIndex), Integer) > iRow Then
                    maDeletedRows.Item(iIndex) = CType(maAddedRows.Item(iIndex), Integer) - 1
                End If
            Next

            moRows.Remove(iRow)
        End Sub

        Public Function doProcessData() As Boolean
            Dim bResult As Boolean = True
            bResult = bResult And doProcessRemovedRows()
            bResult = bResult And doProcessAddedRows()
            bResult = bResult And doProcessChangedRows()
            Return bResult
        End Function
#End Region

    End Class
End Namespace

