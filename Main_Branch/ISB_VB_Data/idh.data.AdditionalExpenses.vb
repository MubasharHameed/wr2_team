Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.dbObjects.User
Imports com.idh.bridge
Imports com.idh.utils
Imports com.idh.dbObjects.numbers
'
' Created by SharpDevelop.
' User: Louis Viljoen
' Date: 02/06/2008
' Time: 14:47
' 
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Namespace idh.data
	Public Class AdditionalExpenses
		
		'Protected moList As New ArrayList()
		Protected moRows As New ArrayList()
		Protected moHead As New ArrayList()
		Protected moType As New ArrayList()
		Protected moSize As New ArrayList()

		Protected miRows As Integer = 0
		Protected miCols As Integer = 0

		Protected moParent As IDHAddOns.idh.addon.Base
        Private msTable As String
		Protected miCurrentRow As Integer = 0

        Protected mhChangedRowNr As Hashtable = New Hashtable 'The rows that was changed
        Protected maAddedRows As ArrayList = New ArrayList 'The list of rows added
        Protected maDeletedRows As ArrayList = New ArrayList 'Contains the list of deleted rows
        Protected msAutoNumberPrefix As String = Nothing

		Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sTable As String)
			moParent = oParent
			msTable = sTable

            If sTable.Contains("WO") Then
                msAutoNumberPrefix = IDH_WOADDEXP.AUTONUMPREFIX
            Else
                msAutoNumberPrefix = IDH_DOADDEXP.AUTONUMPREFIX
            End If

            doAddFields()
        End Sub
		
		Private Sub doAddFields()
			doAddField("Code",SAPbouiCOM.BoFieldsType.ft_Text,8)
			doAddField("Name",SAPbouiCOM.BoFieldsType.ft_Text,20)
			doAddField("U_JobNr",SAPbouiCOM.BoFieldsType.ft_Text,20)
			doAddField("U_RowNr",SAPbouiCOM.BoFieldsType.ft_Text,20)
			doAddField("U_ItemCd",SAPbouiCOM.BoFieldsType.ft_Text,20)
			doAddField("U_ItemDsc",SAPbouiCOM.BoFieldsType.ft_Text,50)
			doAddField("U_CustCd",SAPbouiCOM.BoFieldsType.ft_Text,20)
			doAddField("U_CustNm",SAPbouiCOM.BoFieldsType.ft_Text,50)
			doAddField("U_SuppCd",SAPbouiCOM.BoFieldsType.ft_Text,20)
			doAddField("U_SuppNm",SAPbouiCOM.BoFieldsType.ft_Text,50)
			doAddField("U_UOM",SAPbouiCOM.BoFieldsType.ft_Text,8)
			doAddField("U_Quantity",SAPbouiCOM.BoFieldsType.ft_Float,8)
			doAddField("U_ItmCost",SAPbouiCOM.BoFieldsType.ft_Float,8)
			doAddField("U_ItmChrg",SAPbouiCOM.BoFieldsType.ft_Float,8)
			doAddField("U_ItmCostVat",SAPbouiCOM.BoFieldsType.ft_Float,8)
			doAddField("U_ItmChrgVat",SAPbouiCOM.BoFieldsType.ft_Float,8)
			doAddField("U_ItmTCost",SAPbouiCOM.BoFieldsType.ft_Float,8)
			doAddField("U_ItmTChrg",SAPbouiCOM.BoFieldsType.ft_Float,8)
			doAddField("U_CostVatGrp",SAPbouiCOM.BoFieldsType.ft_Text,20)
			doAddField("U_ChrgVatGrp",SAPbouiCOM.BoFieldsType.ft_Text,20)
			doAddField("U_SONr",SAPbouiCOM.BoFieldsType.ft_Text,20)
			doAddField("U_PONr",SAPbouiCOM.BoFieldsType.ft_Text,20)
			doAddField("U_SINVNr",SAPbouiCOM.BoFieldsType.ft_Text,20)
            doAddField("U_PINVNr", SAPbouiCOM.BoFieldsType.ft_Text, 20)

            doAddField("U_EmpId", SAPbouiCOM.BoFieldsType.ft_Text, 20)
            doAddField("U_EmpNm", SAPbouiCOM.BoFieldsType.ft_Text, 50)
            doAddField("U_SuppCurr", SAPbouiCOM.BoFieldsType.ft_Text, 20)
            doAddField("U_AutoAdd", SAPbouiCOM.BoFieldsType.ft_Text, 1)
            doAddField("U_BPAutoAdd", SAPbouiCOM.BoFieldsType.ft_Text, 1)
            doAddField("U_CustCurr", SAPbouiCOM.BoFieldsType.ft_Text, 3)
        End Sub
		
		Public Function GetByJob(ByVal sJobNr As String) As Integer
			Return doQuery(" U_JobNr = '" + sJobNr + "'")
		End Function

		Public Function GetByJobRow(ByVal sRow As String) As Integer
			Return doQuery(" U_RowNr = '" + sRow + "'")
		End Function

		Public Function GetByCode(ByVal sCode As String) As Integer
			Return doQuery(" Code = '" + sCode + "'")
		End Function

		Public Sub doAddField(ByVal sFieldName As String, ByVal oType As SAPbouiCOM.BoFieldsType, ByVal iWidth As Integer)
			moHead.Add(sFieldName)
			moType.Add(oType)
			moSize.Add(iWidth)
		End Sub

		Private Function doCreateSelectList() As String
			Dim sList As String = ""
'			For i As Integer = 0 To moList.Count - 1
'				If i > 0 Then
'					sList = sList & ","
'				End If
'				sList = sList & moList.Item(i)
'			Next

			For i As Integer = 0 To moHead.Count - 1
				If i > 0 Then
					sList = sList & ","
				End If
                sList = sList & CType(moHead.Item(i), String)
			Next
	
			Return sList
		End Function

		Private Function doQuery(ByVal sWhere As String) As Integer
			moRows.Clear()
			maAddedRows.Clear()
			maDeletedRows.Clear()
			mhChangedRowNr.Clear()

			miCurrentRow = 0

			Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
			
			Dim sSelectList As String = doCreateSelectList()
			Dim sQry As String = "SELECT " & sSelectList & " FROM " & msTable & " WHERE " & sWhere
			Try
				oRecordSet = moParent.goDB.doSelectQuery(sQry)
				If oRecordSet Is Nothing Then
					Return 0
				End If
			    miCols = oRecordSet.Fields.Count()
			    
'			    Dim oType As SAPbouiCOM.BoFieldsType 
'				For i As Integer = 0 To miCols - 1
'					oType = oRecordSet.Fields.Item(i).Type()
'					
'					moHead.Add(oRecordSet.Fields.Item(i).Name())
'					moType.Add(oType)
'					moSize.Add(oRecordSet.Fields.Item(i).Size())
'				Next

'				Dim iRowCount As Integer
				
				miRows = oRecordSet.RecordCount()
				
				If miRows > 0 Then
'				    miCols = oRecordSet.Field.Count()
'					For i As Integer = 0 To miCols - 1
'						moHead.Add(oRecordSet.Fields.Item(i).Name())
'						moType.Add(oRecordSet.Fields.Item(i).Type())
'						moSize.Add(oRecordSet.Fields.Item(i).Size())
'					Next
	
					While Not oRecordSet.EoF
                        'Dim oRowData As New ArrayList()
                        'For i As Integer = 0 To moHead.Count() - 1
                        '	oRowData.Add(oRecordSet.Fields.Item(i).Value())
                        'Next

                        Dim oRowData(moHead.Count) As Object
                        For i As Integer = 0 To moHead.Count() - 1
                            oRowData(i) = oRecordSet.Fields.Item(i).Value()
                        Next

                        moRows.Add(oRowData)
                        oRecordSet.MoveNext()

                    End While
                End If
                Return miRows
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, sQry)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", {sQry})
			Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
			End Try
			Return 0
		End Function

		Public Function getRowCount() As Integer
			If moRows Is Nothing Then
				Return 0
			Else
				Return moRows.Count()
			End If
		End Function
		
		Public Function getColCount() As Integer
			If moHead Is Nothing Then
				Return 0
			Else
				Return moHead.Count()
			End If
		End Function
		
		Public Function getColName(ByVal iIndex As Integer) As String 
			If moHead Is Nothing Then
				Return ""
			Else
                Return CType(moHead.Item(iIndex), String)
			End If
		End Function
		
		Public Function getColIndex(ByVal sFieldName As String) As Integer
			If moHead Is Nothing Then
				Return -1
			Else
				Return moHead.IndexOf(sFieldName)
			End If
		End Function

		Public Function getColType(ByVal iIndex As Integer) As SAPbouiCOM.BoFieldsType
			If moType Is Nothing Then
                Return SAPbouiCOM.BoFieldsType.ft_AlphaNumeric
			Else
                Return CType(moType.Item(iIndex), SAPbouiCOM.BoFieldsType)
			End If
		End Function

		Public Function getColSize(ByVal iIndex As Integer) As Integer
			If moSize Is Nothing Then
				Return 0
			Else
                Return Conversions.ToInt(moSize.Item(iIndex))
			End If
		End Function

		Private Sub setFieldValue(ByVal sFieldName As String, ByVal oValue As Object)
			Dim iFieldIndex As Integer = moHead.IndexOf(sFieldName)
			setFieldValue(iFieldIndex, miCurrentRow, oValue)
		End Sub

		Public Sub setFieldValue(ByVal iFieldIndex As Integer, ByVal oValue As Object)
			setFieldValue(iFieldIndex, miCurrentRow, oValue)
		End Sub

		Public Sub setFieldValue(ByVal sFieldName As String, ByVal iRow As Integer, ByVal oValue As Object)
			Dim iFieldIndex As Integer = moHead.IndexOf(sFieldName)
            setFieldValue(iFieldIndex, iRow, oValue)
        End Sub

        Public Sub setFieldValue(ByVal iFieldIndex As Integer, ByVal iRow As Integer, ByVal oValue As Object)
            If iFieldIndex >= 0 Then
                'Dim aRowData As ArrayList = moRows.Item(iRow)
                '            If iFieldIndex < moHead.Count Then
                '                aRowData.Item(iFieldIndex) = oValue
                Dim aRowData() As Object = CType(moRows.Item(iRow), Object())
                If iFieldIndex < moHead.Count Then
                    aRowData(iFieldIndex) = oValue
                End If
                ''DirectCast(moRows.Item(iRow), ArrayList).Item(iFieldIndex) = oValue

                If maAddedRows.Contains(iRow) = False AndAlso _
                    maDeletedRows.Contains(iRow) = False Then
                    Dim oRowData As ArrayList
                    If mhChangedRowNr.ContainsKey(iRow) Then
                        oRowData = CType(mhChangedRowNr.Item(iRow), ArrayList)
                        If oRowData.Contains(iFieldIndex) = False Then
                            oRowData.Add(iFieldIndex)
                        End If
                    Else
                        oRowData = New ArrayList
                        oRowData.Add(iFieldIndex)
                        mhChangedRowNr.Add(iRow, oRowData)
                    End If
                End If
            End If
        End Sub

        Public Function getFieldValue(ByVal sFieldName As String) As Object
            Dim iFieldIndex As Integer = moHead.IndexOf(sFieldName)
            Return getFieldValue(iFieldIndex, miCurrentRow)
        End Function

        Public Function getFieldValue(ByVal iFieldIndex As Integer) As Object
            Return getFieldValue(iFieldIndex, miCurrentRow)
        End Function

        Public Function getFieldValue(ByVal sFieldName As String, ByVal iRow As Integer) As Object
            Dim iFieldIndex As Integer = moHead.IndexOf(sFieldName)
            Return getFieldValue(iFieldIndex, iRow)
        End Function

        Public Function getFieldValue(ByVal iFieldIndex As Integer, ByVal iRow As Integer) As Object
            If iFieldIndex < 0 Then
                Return Nothing
            End If
            'Dim aRowData As ArrayList = moRows.Item(iRow)
            'If iFieldIndex < aRowData.Count Then
            '   Return aRowData.Item(iFieldIndex)
            Dim aRowData() As Object = CType(moRows.Item(iRow), Object())
            If iFieldIndex < aRowData.Length Then
                Return aRowData(iFieldIndex)
            Else
                Return Nothing
            End If
            ''Return DirectCast(moRows.Item(iRow), ArrayList).Item(iFieldIndex)
        End Function

        '		Public Sub setCode(ByVal sCode As String)
        '			setFieldValue("Code", sCode)
        '		End Sub
        '
        '		Public Function getCode() As String
        '			Return DirectCast(getFieldValue("Code"), String)
        '		End Function
        '
        '		Public Sub setName(ByVal sName As String)
        '			setFieldValue("Name", sName)
        '		End Sub
        '
        '		Public Function getName() As String
        '			Return getFieldValue("Name")
        '		End Function
        '
        '		Public Sub setJobNumber(ByVal sJobNr As String)
        '			setFieldValue("U_JobNr", sJobNr)
        '		End Sub
        '
        '		Public Function getJobNumber() As String
        '			Return getFieldValue("U_JobNr")
        '		End Function

        '	[U_RowNr] [nvarchar](max) COLLATE SQL_Latin1_General_CP850_CI_AS NULL,
        '	[U_ItemCd] [nvarchar](max) COLLATE SQL_Latin1_General_CP850_CI_AS NULL,
        '	[U_ItemDsc] [nvarchar](max) COLLATE SQL_Latin1_General_CP850_CI_AS NULL,
        '	[U_CustCd] [nvarchar](max) COLLATE SQL_Latin1_General_CP850_CI_AS NULL,
        '	[U_CustNm] [nvarchar](max) COLLATE SQL_Latin1_General_CP850_CI_AS NULL,
        '	[U_SuppCd] [nvarchar](max) COLLATE SQL_Latin1_General_CP850_CI_AS NULL,
        '	[U_SuppNm] [nvarchar](max) COLLATE SQL_Latin1_General_CP850_CI_AS NULL,
        '	[U_UOM] [nvarchar](max) COLLATE SQL_Latin1_General_CP850_CI_AS NULL,
        '	[U_Quantity] [int] NULL,
        '	[U_ItmCost] [numeric](19, 6) NULL,
        '	[U_ItmChrg] [numeric](19, 6) NULL,
        '	[U_ItmCostVat] [numeric](19, 6) NULL,
        '	[U_ItmChrgVat] [numeric](19, 6) NULL,
        '	[U_ItmTCost] [numeric](19, 6) NULL,
        '	[U_ItmTChrg] [numeric](19, 6) NULL,

        'Returns only the row numbers

        Public Function doFindEntry(ByVal sCode As String) As Integer
            Dim iFieldIndex As Integer = moHead.IndexOf("Code")
            Dim iRow As Integer
            Dim sValue As String
            For iRow = 0 To moRows.Count - 1
                'sValue = DirectCast(moRows.Item(iRow), ArrayList).Item(iFieldIndex)

                Dim aRowData() As Object = CType(moRows.Item(iRow), Object())

                sValue = Conversions.ToString(aRowData(iFieldIndex))
                If sValue.Equals(sCode) Then
                    Return iRow
                End If
            Next
            Return -1
        End Function

		' Set the Job Row Code for all rows with a Row Code of -1
        Public Sub doSetRowCode(ByVal sRowCode As String)
            Dim iFieldIndex As Integer = moHead.IndexOf("U_RowNr")
            Dim iRow As Integer
            Dim sValue As String
            For iRow = 0 To moRows.Count - 1
                Dim aRowData() As Object = CType(moRows.Item(iRow), Object())
                sValue = Conversions.ToString(aRowData(iFieldIndex))
                If sValue.Equals("-1") Then
                    setFieldValue(iFieldIndex, iRow, sRowCode)
                End If
            Next
        End Sub

        Public Function doAddRow() As Integer
            Dim oRowData(moHead.Count) As Object
            'ReDim Preserve oRowData(moHead.Count)
            'Dim oRowData As ArrayList = New ArrayList(moHead.Count)
            moRows.Add(oRowData)
            maAddedRows.Add(moRows.Count - 1)
            Return moRows.Count - 1
        End Function

		'' Will try to find the row if not found add it
        Public Function doSetRow(ByVal sCode As String) As Integer
            Dim iIndex As Integer
            iIndex = doFindEntry(sCode)
            If iIndex = -1 Then
                Return doAddRow()
            Else
                Return iIndex
            End If
        End Function
        
        '' Remove the Row - Only mark to be deleted
        Public Sub doRemoveRow(ByVal sCode As String)
            doRemoveRow(doFindEntry(sCode))
        End Sub

        '' Remove the Row - Only mark to be deleted
        Public Sub doRemoveRow(ByVal iRow As Integer)
		    If maDeletedRows.Contains(iRow) = False Then
				maDeletedRows.Add(iRow)
			End If
        End Sub

        Public Function hasChanged() As Boolean
            If maAddedRows.Count > 0 OrElse mhChangedRowNr.Count > 0 OrElse maDeletedRows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Function
        
        Public Function wasAdded(ByVal iRow As Integer) As Boolean
        	Return maAddedRows.Contains(iRow)
        End Function

		Public Function wasDeleted(ByVal iRow As Integer) As Boolean
        	Return maDeletedRows.Contains(iRow)
        End Function

        Public Function doGetChangedRows() As ArrayList
            If mhChangedRowNr Is Nothing OrElse mhChangedRowNr.Count = 0 Then
                Return Nothing
            End If

            Dim oArray As ArrayList
            oArray = New ArrayList(mhChangedRowNr.Count)

            Dim en As IEnumerator = mhChangedRowNr.Keys.GetEnumerator
            While en.MoveNext
                oArray.Add(en.Current)
            End While
            Return oArray
        End Function

        Public Function doRemoveRowEntries(ByVal sRow As String ) As Boolean
        	If getRowCount() > 0 Then				
				For iRow As Integer = 0 To getRowCount() - 1
                    Dim sRowCodeW As String = Conversions.ToString(getFieldValue("U_RowNr", iRow))
					If Not sRowCodeW Is Nothing AndAlso sRowCodeW.Equals(sRow) Then
'						If maAddedRows.Contains(iRow) Then
'        					maAddedRows.Remove(iRow)
'        				Else
'        				    If maDeletedRows.Contains(iRow) = False Then
'        						maDeletedRows.Add(iRow)
'        					End If
'	       				End If
'        				If mhChangedRowNr.ContainsKey(iRow) Then
'        					mhChangedRowNr.Remove(iRow)
'        				End If
						doRemoveRow(iRow)
'    				    If maDeletedRows.Contains(iRow) = False Then
'    						maDeletedRows.Add(iRow)
'    					End If
					End If
           		Next
			End If
        	Return True
        End Function

        Private Function doProcessChangedRows() As Boolean
            If mhChangedRowNr Is Nothing OrElse mhChangedRowNr.Count = 0 Then
                Return True
            End If

            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(moParent, msTable)
            Try
                Dim oFields As ArrayList
                Dim iRow As Integer
                Dim sKey As String
'                Dim sName As String
                Dim iwResult As Integer
                Dim swResult As String = Nothing
                Dim oValue As Object
                Dim iFieldIndex As Integer
                Dim sField As String = ""

                Dim oList As ArrayList = doGetChangedRows()
                Dim iIndex As Integer = 0
                Dim iCount As Integer =  oList.Count
                While iIndex < iCount
                    iRow = CType(oList.Item(iIndex), Integer)
                    If wasDeleted(iRow) Then
                    	mhChangedRowNr.Remove(iRow)
                    Else
                        sKey = Conversions.ToString(getFieldValue("Code", iRow))
	                    If sKey.Trim().Length > 0 Then
	                        'sName = doGetFieldValue("Name")
	'                        If sKey.Chars(0) = "-" Then
	'                    		sKey = sKey.Substring(1)
	'                    	End If
	                    	
	                        If oAuditObj.setKeyPair(sKey, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) Then
                                oFields = CType(mhChangedRowNr.Item(iRow), ArrayList)
	                            If Not (oFields Is Nothing) AndAlso oFields.Count > 0 Then
	                                For ii As Integer = 0 To oFields.Count - 1
                                        iFieldIndex = CType(oFields.Item(ii), Integer)
	                                    Try
                                            sField = CType(moHead(iFieldIndex), String)
	                                        oValue = getFieldValue(iFieldIndex, iRow)
	                                        If sField.Equals("Name") = True Then
	'                                        	If oValue.Length > 1 AndAlso oValue.Chars(0) = "-" Then
	'                    							oValue = oValue.Substring(1)
	'                    						End If
                                                oAuditObj.setName(Conversions.ToString(oValue))
	                                        ElseIf sField.Equals("Code") = False Then
	                                            oAuditObj.setFieldValue(sField, oValue)
	                                        End If
	                                    Catch ex As Exception
                                            'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing changed rows - " & msTable & "." & sField)
                                            DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGBCR", {msTable, sField})
	                                    End Try
	                                Next
	
	                                iwResult = oAuditObj.Commit()
	
	                                If iwResult <> 0 Then
	                                    moParent.goDICompany.GetLastError(iwResult, swResult)
                                        'com.idh.bridge.DataHandler.INSTANCE.doError("Error Updating: " + swResult)
                                        DataHandler.INSTANCE.doResSystemError("Error Updating: " + swResult, "ERSYUGEN", {com.idh.bridge.Translation.getTranslatedWord(swResult)})
	                                    Return False
	                                Else
	                                    mhChangedRowNr.Remove(iRow)
	                                End If
	                            End If
	                        End If
	                    End If
					End If                    
                    iIndex = iIndex + 1
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the changed rows.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGBCR", {String.Empty, String.Empty})
                Return False
            Finally
                oAuditObj.Close()
                DataHandler.INSTANCE.doReleaseObject(CType(oAuditObj, Object))
            End Try
            Return True
        End Function

        Private Function doProcessAddedRows() As Boolean
            If maAddedRows Is Nothing OrElse maAddedRows.Count = 0 Then
                Return True
            End If

            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(moParent, msTable)
            Try
                Dim iRow As Integer
'                Dim oDataColumn As SAPbouiCOM.DataColumn
                Dim sField As String
                Dim oValue As Object
                Dim sKey As String
                Dim sName As String
'                Dim iFieldIndex As Integer
                Dim iwResult As Integer
                Dim swResult As String = Nothing

                Dim iFieldsCount As Integer = oAuditObj.getFieldsCount()

                Dim iIndex As Integer = 0
                Dim iCount As Integer =  maAddedRows.Count
                While iIndex < iCount
                    iRow = CType(maAddedRows.Item(iIndex), Integer)
                    If wasDeleted(iRow) Then
                    	maAddedRows.Remove(iRow)
                    Else
                        sKey = CType(getFieldValue("Code", iRow), String)
	                    If sKey.Trim().Length > 0 Then
	'                    	If sKey.Chars(0) = "-" Then
	'                    		sKey = sKey.Substring(1)
	'                    	End If
	                    	
	                        oAuditObj.setKeyPair(sKey, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
                            sName = CType(getFieldValue("Name", iRow), String)
	'                        If sName.Length > 1 AndAlso sName.Chars(0) = "-" Then
	'                    		sName = sName.Substring(1)
	'                    	End If
	                        oAuditObj.setName(sName)
	
	                        For ii As Integer = 0 To iFieldsCount - 1
	                            sField = oAuditObj.getFieldName(ii)
	                            Try
	                                oValue = getFieldValue(sField, iRow)
	                                If Not oValue Is Nothing Then
	                                    oAuditObj.setFieldValue(ii, oValue)
	                                End If
	                            Catch ex As Exception
                                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing added the rows - " & msTable & "." & sField)
                                    DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGBCR", {msTable, sField})
	                            End Try
	                        Next
	
	                        iwResult = oAuditObj.Commit()
	
	                        If iwResult <> 0 Then
	                            moParent.goDICompany.GetLastError(iwResult, swResult)
                                'com.idh.bridge.DataHandler.INSTANCE.doError("Error Adding the rows: " + swResult)
                                DataHandler.INSTANCE.doResSystemError("Error Adding the rows: " + swResult, "ERSYADD", {com.idh.bridge.Translation.getTranslatedWord("the rows: ") + swResult})
	                            Return False
	                        Else
	                            maAddedRows.Remove(iRow)
	                            iCount = iCount - 1
	                            iIndex = iIndex - 1
	                        End If
	                    End If
					End If
                    iIndex = iIndex + 1
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error processing the added rows.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGBCR", {String.Empty, String.Empty})
                Return False
            Finally
                oAuditObj.Close()
                DataHandler.INSTANCE.doReleaseObject(CType(oAuditObj, Object))
            End Try
            Return True
        End Function

        Private Function doProcessRemovedRows() As Boolean
            If maDeletedRows Is Nothing OrElse maDeletedRows.Count = 0 Then
                Return True
            End If
            
            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(moParent, msTable)
            Try
                Dim iwResult As Integer
                Dim swResult As String = Nothing
                Dim sKey As String

                Dim iRow As Integer = 0
                Dim iIndex As Integer
                Dim iCount As Integer = maDeletedRows.Count
                While iIndex < iCount
                    iRow = CType(maDeletedRows.Item(iIndex), Integer)

					If maAddedRows.Contains(iRow) Then
						doRemoveRowFromBuffer(iRow)
						iIndex = iIndex - 1
	                    iCount = iCount - 1
	                Else
                        sKey = CType(getFieldValue("Code", iRow), String)
						
	                    If sKey.Trim().Length > 0 Then
	                        oAuditObj.setKeyPair(sKey, IDHAddOns.idh.data.AuditObject.ac_Types.idh_REMOVE)
	                        iwResult = oAuditObj.Commit()
	
	                        If iwResult <> 0 Then
	                            moParent.goDICompany.GetLastError(iwResult, swResult)
                                'com.idh.bridge.DataHandler.INSTANCE.doError("System: Error Removing the rows: " + swResult, "Error Removing the rows.")
                                DataHandler.INSTANCE.doResSystemError("Error Removing the rows: " + swResult, "ERSYRGEN", {swResult})
	                            Return False
	                        Else
	                        	doRemoveRowFromBuffer(iRow)
	                            iIndex = iIndex - 1
	                            iCount = iCount - 1
	                        End If
						End If
                    End If
                    iIndex = iIndex + 1
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error Removing the rows.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRGEN", {Nothing})
                Return False
            Finally
                oAuditObj.Close()
                DataHandler.INSTANCE.doReleaseObject(CType(oAuditObj, Object))
            End Try
            Return True
        End Function
        
        Private Sub doRemoveRowFromBuffer(ByVal iRow As Integer)
        	If maAddedRows.Contains(iRow) Then
        		maAddedRows.Remove(iRow)
        	End If
        	
			If mhChangedRowNr.ContainsKey(iRow) Then
				mhChangedRowNr.Remove(iRow)
			End If			
			
			If maDeletedRows.Contains(iRow) Then
				maDeletedRows.Remove(iRow)
			End If

			Dim iIndex As Integer
			For iIndex = 0 To maAddedRows.Count - 1
                If CType(maAddedRows.Item(iIndex), Integer) > iRow Then
                    maAddedRows.Item(iIndex) = CType(maAddedRows.Item(iIndex), Integer) - 1
                End If
			Next
			
			Dim en As IEnumerator = mhChangedRowNr.Keys.GetEnumerator
            While en.MoveNext
                iIndex = CType(en.Current, Integer)
                If iIndex > iRow Then
					mhChangedRowNr.Add(iIndex-1, mhChangedRowNr.Item(iIndex))
					mhChangedRowNr.Remove(iIndex)
				End If
            End While
			
			For iIndex = 0 To maDeletedRows.Count - 1
                If CType(maDeletedRows.Item(iIndex), Integer) > iRow Then
                    maDeletedRows.Item(iIndex) = CType(maAddedRows.Item(iIndex), Integer) - 1
                End If
			Next
			
			moRows.Remove(iRow)
        End Sub

        Public Function doProcessData() As Boolean
            Dim bResult As Boolean = True
            bResult = bResult And doProcessRemovedRows()
            bResult = bResult And doProcessAddedRows()
            bResult = bResult And doProcessChangedRows()
            Return bResult
        End Function

        Public Sub doDuplicateRowEntries(ByVal sSrcRowCode As String, ByVal sNewRowCode As String, ByVal bCopyPrice As Boolean, ByVal bCopyQty As Boolean)
            Dim iFieldIndex As Integer = moHead.IndexOf("U_RowNr")
            Dim iRow As Integer
            Dim sValue As String
            For iRow = 0 To moRows.Count - 1
                sValue = Conversions.ToString(CType(moRows.Item(iRow), Object())(iFieldIndex))
                If sValue.Equals(sSrcRowCode) Then

                    Dim dQty As Double = 0
                    If bCopyQty Then
                        dQty = Conversions.ToDouble(getFieldValue(IDH_WOADDEXP._Quantity, iRow))
                    End If

                    Dim dChrgPrice As Double = 0
                    Dim dCstPrice As Double = 0
                    If bCopyPrice Then
                        dChrgPrice = Conversions.ToDouble(getFieldValue(IDH_WOADDEXP._ItmChrg, iRow))
                        dCstPrice = Conversions.ToDouble(getFieldValue(IDH_WOADDEXP._ItmCost, iRow))
                    End If

                    Dim dChrgVat As Double = 0
                    Dim dCstVat As Double = 0
                    Dim dChrgTotal As Double = 0
                    Dim dCstTotal As Double = 0
                    If bCopyPrice AndAlso bCopyQty Then
                        dChrgVat = Conversions.ToDouble(getFieldValue(IDH_WOADDEXP._ItmChrgVat, iRow))
                        dCstVat = Conversions.ToDouble(getFieldValue(IDH_WOADDEXP._ItmCostVat, iRow))
                        dChrgTotal = Conversions.ToDouble(getFieldValue(IDH_WOADDEXP._ItmTChrg, iRow))
                        dCstTotal = Conversions.ToDouble(getFieldValue(IDH_WOADDEXP._ItmTCost, iRow))
                    End If

                    miCurrentRow = doAddRow()

                    'Dim sKey As String = DataHandler.doGenerateCode(msAutoNumberPrefix, "ADDEXPKEY")
                    Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(IDH_WOADDEXP.AUTONUMPREFIX, "ADDEXPKEY")
                    setFieldValue(IDH_WOADDEXP._Code, oNumbers.CodeCode)
                    setFieldValue(IDH_WOADDEXP._Name, oNumbers.NameCode)
                    setFieldValue(IDH_WOADDEXP._JobNr, getFieldValue(IDH_WOADDEXP._JobNr, iRow))
                    setFieldValue(IDH_WOADDEXP._RowNr, sNewRowCode)
                    setFieldValue(IDH_WOADDEXP._CustCd, getFieldValue(IDH_WOADDEXP._CustCd, iRow))
                    setFieldValue(IDH_WOADDEXP._CustNm, getFieldValue(IDH_WOADDEXP._CustNm, iRow))
                    setFieldValue(IDH_WOADDEXP._SuppCd, getFieldValue(IDH_WOADDEXP._SuppCd, iRow))
                    setFieldValue(IDH_WOADDEXP._SuppNm, getFieldValue(IDH_WOADDEXP._SuppNm, iRow))
                    setFieldValue(IDH_WOADDEXP._ItemCd, getFieldValue(IDH_WOADDEXP._ItemCd, iRow))
                    setFieldValue(IDH_WOADDEXP._ItemDsc, getFieldValue(IDH_WOADDEXP._ItemDsc, iRow))
                    setFieldValue(IDH_WOADDEXP._UOM, getFieldValue(IDH_WOADDEXP._UOM, iRow))
                    setFieldValue(IDH_WOADDEXP._Quantity, dQty)
                    setFieldValue(IDH_WOADDEXP._ItmCost, dCstPrice)
                    setFieldValue(IDH_WOADDEXP._ItmChrg, dChrgPrice)
                    setFieldValue(IDH_WOADDEXP._CostVatGrp, getFieldValue(IDH_WOADDEXP._CostVatGrp, iRow))
                    setFieldValue(IDH_WOADDEXP._ItmCostVat, dCstVat)
                    setFieldValue(IDH_WOADDEXP._ChrgVatGrp, getFieldValue(IDH_WOADDEXP._ChrgVatGrp, iRow))
                    setFieldValue(IDH_WOADDEXP._ItmChrgVat, dChrgVat)
                    setFieldValue(IDH_WOADDEXP._ItmTChrg, dChrgTotal)
                    setFieldValue(IDH_WOADDEXP._ItmTCost, dCstTotal)
                    setFieldValue(IDH_WOADDEXP._FPCost, getFieldValue(IDH_WOADDEXP._FPCost, iRow))
                    setFieldValue(IDH_WOADDEXP._FPChrg, getFieldValue(IDH_WOADDEXP._FPChrg, iRow))
                    setFieldValue("U_EmpId", getFieldValue("U_EmpId", iRow))
                    setFieldValue("U_EmpNm", getFieldValue("U_EmpNm", iRow))
                    setFieldValue(IDH_WOADDEXP._SuppCurr, getFieldValue(IDH_WOADDEXP._SuppCurr, iRow))
                    setFieldValue(IDH_WOADDEXP._AutoAdd, getFieldValue(IDH_WOADDEXP._AutoAdd, iRow))
                    setFieldValue(IDH_WOADDEXP._BPAutoAdd, getFieldValue(IDH_WOADDEXP._BPAutoAdd, iRow))
                    setFieldValue(IDH_WOADDEXP._CustCurr, getFieldValue(IDH_WOADDEXP._CustCurr, iRow))
                End If
            Next
        End Sub

		''MORE DATA HANDLING
        Public Sub doAddExpense( _
            ByVal sJobNr As String, _
            ByVal sRowNr As String, _
            ByVal sCustCd As String, _
            ByVal sCustNm As String, _
            ByVal sCustCurr As String, _
            ByVal sSuppCd As String, _
            ByVal sSuppNm As String, _
            ByVal sSuppCurr As String, _
            ByVal sEmpId As String, _
            ByVal sEmpNm As String, _
            ByVal sItemCd As String, _
            ByVal sItemNm As String, _
            ByVal sUOM As String, _
            ByVal nQuantity As Double, _
            ByVal nItmCost As Double, _
            ByVal nItmChrg As Double, _
            ByVal sCostVatGrp As String, _
            ByVal sChrgVatGrp As String, _
            ByVal bCostFroozen As Boolean, _
            ByVal bChrgFroozen As Boolean, _
            ByVal bAutoAdd As Boolean, _
            ByVal bBPAutoAdd As Boolean)

            Dim sCostFroozen As String
            If bCostFroozen Then
                sCostFroozen = "Y"
            Else
                sCostFroozen = "N"
            End If

            Dim sChrgFroozen As String
            If bChrgFroozen Then
                sChrgFroozen = "Y"
            Else
                sChrgFroozen = "N"
            End If

            Dim sAutoAdd As String
            If bAutoAdd Then
                sAutoAdd = "Y"
            Else
                sAutoAdd = "N"
            End If

            Dim sBPAutoAdd As String
            If bBPAutoAdd Then
                sBPAutoAdd = "Y"
            Else
                sBPAutoAdd = "N"
            End If

            doAddExpense(sJobNr, sRowNr, sCustCd, sCustNm, sCustCurr, sSuppCd, sSuppNm, sSuppCurr, sEmpId, sEmpNm, sItemCd, sItemNm, sUOM, nQuantity, nItmCost, nItmChrg, sCostVatGrp, sChrgVatGrp, sCostFroozen, sChrgFroozen, sAutoAdd, sBPAutoAdd)
        End Sub

        Public Sub doAddExpense( _
            ByVal sJobNr As String, _
            ByVal sRowNr As String, _
            ByVal sCustCd As String, _
            ByVal sCustNm As String, _
            ByVal sCustCurr As String, _
            ByVal sSuppCd As String, _
            ByVal sSuppNm As String, _
            ByVal sSuppCurr As String, _
            ByVal sEmpId As String, _
            ByVal sEmpNm As String, _
            ByVal sItemCd As String, _
            ByVal sItemNm As String, _
            ByVal sUOM As String, _
            ByVal nQuantity As Double, _
            ByVal nItmCost As Double, _
            ByVal nItmChrg As Double, _
            ByVal sCostVatGrp As String, _
            ByVal sChrgVatGrp As String, _
            ByVal sCostFroozen As String, _
            ByVal sChrgFroozen As String, _
            ByVal sAutoAdd As String, _
            ByVal sBPAutoAdd As String)
            miCurrentRow = doAddRow()

            Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(msAutoNumberPrefix, "ADDEXPKEY")
            'Dim sKey As String = DataHandler.doGenerateCode(msAutoNumberPrefix, "ADDEXPKEY")
            setFieldValue("Code", oNumbers.CodeCode)
            setFieldValue("Name", oNumbers.NameCode)
            setFieldValue("U_JobNr", sJobNr)
            setFieldValue("U_RowNr", sRowNr)
            setFieldValue("U_CustCd", sCustCd)
            setFieldValue("U_CustCurr", sCustCurr)
            setFieldValue("U_CustNm", sCustNm)
            setFieldValue("U_SuppCd", sSuppCd)
            setFieldValue("U_SuppNm", sSuppNm)
            setFieldValue("U_SuppCurr", sSuppCurr)
            setFieldValue("U_ItemCd", sItemCd)
            setFieldValue("U_ItemDsc", sItemNm)
            setFieldValue("U_UOM", sUOM)
            setFieldValue("U_Quantity", nQuantity)
            setFieldValue("U_ItmCost", nItmCost)
            setFieldValue("U_ItmChrg", nItmChrg)
            setFieldValue("U_CostVatGrp", sCostVatGrp)
            'setFieldValue("U_ItmCostVat", nItmCostVat)
            setFieldValue("U_ChrgVatGrp", sChrgVatGrp)
            'setFieldValue("U_ItmChrgVat", nItmChrgVat)
            setFieldValue("U_FPCost", sCostFroozen)
            setFieldValue("U_FPChrg", sChrgFroozen)
            setFieldValue("U_EmpId", sEmpId)
            setFieldValue("U_EmpNm", sEmpNm)
            setFieldValue("U_AutoAdd", sAutoAdd)
            setFieldValue("U_BPAutoAdd", sBPAutoAdd)
        End Sub

    End Class
End Namespace
