
' Created by SharpDevelop.
' User: Louis Viljoen
' Date: 2008/11/07
' Time: 01:28 AM
'
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Imports System

Imports com.idh.bridge.lookups
Imports System.Collections
Imports com.idh.dbObjects.User
Imports com.idh.utils
Imports com.idh.bridge

Namespace idh.data
	Public Class RowData
		Private Shared msDefaultAUOM As String = Config.INSTANCE.getDefaultAUOM()
		
		Private mdWrkWeight As Double = 0
		Private mdTCostWrkWeight As Double = 0
		Private mdPCostWrkWeight As Double = 0
		
		Private msBPUOM As String
		Private msDefUOM As String
		Private msFormMode As String
		
		Private mbIsWBWeight As Boolean
        Private mbUseCIPUom As Boolean = False
        Private mbUseSIPPUOM As Boolean = False
        Private mbUseSIPProUOM As Boolean = False
        Private msCCHChr As String = "STANDARD"
        Private msCCTChr As String = "VARIABLE"
        Private mdTOffset As Double = -99999
        Private mdHOffset As Double = -99999
        Private mdTCostOffset As Double = -99999
        Private mdPCostOffset As Double = -99999
        Private mdHCostOffset As Double = -99999
        Private mdDefWei As Double = -99999
        
		Private msCCTCos As String = "VARIABLE"
		Private msCCPCos As String = "VARIABLE"
		Private msCCHCos As String = "STANDARD"

        Private msUseWgt As String = "1"

        Private msCardCode As String
        Private msSTADDR As String
        Private msSupplierAddress As String
		Private msZIPCODE As String
		Private msCarrCode As String
		Private msTipCode As String
		Private msProCode As String
		
        Private msWastCd As String
        Private msItemCode As String
        Private msItemGrp As String
        Private msBranch As String
        Private msJobType As String
        Private mdDocDate As Date
        Private msStatus As String
        
        Private mdAUOMQty As Double
		Private mdRdWgt As Double
		Private mdTipWgt As Double
		Private mdTRdWgt As Double
		Private mdCstWgt As Double
		
		Private msUOM As String
		Public ReadOnly Property UOM() As String
            Get
                Return msUOM
            End Get
        End Property
		
		Private msAUOM As String
		Private mdTCharge As Double
		Private mdTChrgVtRt As Double
		Private mdCusChr As Double
		Private mdHChrgVtRt As Double
		Private mdTipCost As Double
		
		Private msPUOM As String
		Public ReadOnly Property PUOM() As String
            Get
                Return msPUOM
            End Get
        End Property
        
		Private msTChrgVtGrp As String
		Private msHChrgVtGrp As String
		Private mdTCostVtRt As Double
		Private msTCostVtGrp As String

        Private mdPCostVtRt As Double
        Private msPCostVtGrp As String


		Private mdAdjWgt As Double
		Private mdWgtDed As Double
		
		Private msProUOM As String
		Public ReadOnly Property ProUOM() As String
            Get
                Return msProUOM
            End Get
        End Property
        
		Private mdProWgt As Double
		Private mdPCost As Double
		Private mdPCtotal As Double
		
		Private mdTCTotal As Double
        Private mdAddEx As Double
        Private mdDisAmt As Double
        Private mdTaxAmt As Double
        Private mdDiscnt As Double
        Private mdSubTotal As Double
        Private mdTotal As Double
        Private mdTipTot As Double
		
		Private mdOrdCost As Double
        Private mdHCostVtRt As Double
        Private msHCostVtGrp As String
        Private mdOrdWgt As Double
                    
		Private mIDHForm As IDHAddOns.idh.forms.Base
		Private moForm As SAPbouiCOM.Form
		Private moParent As IDHAddOns.idh.addon.Base
        Private msTable As String
        Private msHeadTable As String
        Private msWROrdType As String
        Private msCIPWROrdType As String
		
		Private mbWasRead As Boolean = False
		Private mbSetFromHeader As Boolean = False
		
		Private msLoadSht As String = ""


        Private moCIP As IDH_CSITPR = New IDH_CSITPR()
        Public ReadOnly Property CIP() As IDH_CSITPR
            Get
                Return moCIP
            End Get
        End Property

        Private moSIPT As IDH_SUITPR = New IDH_SUITPR()
        Public ReadOnly Property SIPT() As IDH_SUITPR
            Get
                Return moSIPT
            End Get
        End Property

        Private moSIPH As IDH_SUITPR = New IDH_SUITPR()
        Public ReadOnly Property SIPH() As IDH_SUITPR
            Get
                Return moSIPH
            End Get
        End Property

        Private moSIPP As IDH_SUITPR = New IDH_SUITPR()
        Public ReadOnly Property SIPP() As IDH_SUITPR
            Get
                Return moSIPP
            End Get
        End Property

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByRef IDHForm As IDHAddOns.idh.forms.Base, ByVal oForm As SAPbouiCOM.Form, ByVal sWR1OrdType As String)
            moParent = oParent
            mIDHForm = IDHForm
            moForm = oForm
            msWROrdType = sWR1OrdType

            If msWROrdType = "DO" Then
                msTable = "@IDH_DISPROW"
                msHeadTable = "@IDH_DISPORD"
            Else
                msTable = "@IDH_JOBSHD"
                msHeadTable = "@IDH_JOBENTR"
            End If
            msCIPWROrdType = msWROrdType

            msDefUOM = Config.INSTANCE.getDefaultUOM()
            'msDefaultAUOM = Config.INSTANCE.getDefaultUOM(moParent)
        End Sub
		 
		 'This will set the Row Header information else it will be retrieved from the Form if it is set.
        Public Sub setHeader(ByRef sCustomer As String, ByRef sCarrier As String, ByRef sTipCode As String, ByRef sProCode As String, ByRef sZipCode As String, ByRef sAddress As String, ByRef sSupplierAddress As String, ByRef dDocDate As Date, ByVal sCIPWR1ORDType As String)
            If sCIPWR1ORDType Is Nothing OrElse sCIPWR1ORDType.Length = 0 Then
                msCIPWROrdType = msWROrdType
            Else
                msCIPWROrdType = sCIPWR1ORDType
            End If

            msCardCode = sCustomer
            msCarrCode = sCarrier
            msTipCode = sTipCode
            msProCode = sProCode
            msZIPCODE = sZipCode
            msSTADDR = sAddress
            msSupplierAddress = sSupplierAddress
            mbSetFromHeader = True

            doReadData()
            mdDocDate = dDocDate
        End Sub
		 
        ' Read the Row data from the Form
        Private Sub doReadData()
            If Not mIDHForm Is Nothing Then
                doReadData(Not mbSetFromHeader)
            End If
        End Sub
        Public Sub doReadData(ByVal bGetFromForm As Boolean)
            If Not mIDHForm Is Nothing Then
                If bGetFromForm = True Then
                    msCardCode = CType(mIDHForm.getDFValue(moForm, msHeadTable, IDH_DISPORD._CardCd), String)
                    msCarrCode = CType(mIDHForm.getDFValue(moForm, msHeadTable, IDH_DISPORD._CCardCd), String)
                    msTipCode = CType(mIDHForm.getDFValue(moForm, msHeadTable, IDH_DISPORD._SCardCd), String)
                    msProCode = CType(mIDHForm.getDFValue(moForm, msHeadTable, IDH_DISPORD._PCardCd), String)

                    msZIPCODE = CType(mIDHForm.getDFValue(moForm, msHeadTable, IDH_DISPORD._ZpCd), String)
                    msSTADDR = CType(mIDHForm.getDFValue(moForm, msHeadTable, IDH_DISPORD._Address), String)
                    msSupplierAddress = CType(mIDHForm.getDFValue(moForm, msHeadTable, IDH_DISPORD._SAddress), String)
                    'msCardCode = mIDHForm.getDFValue(moForm, msTable, "U_CustCd")
                    'msCarrCode = mIDHForm.getDFValue(moForm, msTable, "U_CarrCd")
                    'msTipCode = mIDHForm.getDFValue(moForm, msTable, "U_Tip")
                    'msProCode = mIDHForm.getDFValue(moForm, msTable, "U_ProCd")

                    'msZIPCODE = mIDHForm.getWFValue(moForm, "ZIPCODE")
                    'msSTADDR = mIDHForm.getWFValue(moForm, "STADDR")
                End If

                msWastCd = CType(mIDHForm.getDFValue(moForm, msTable, "U_WasCd"), String)
                msBPUOM = Config.INSTANCE.getBPUOM(msCardCode)
                msItemCode = CType(mIDHForm.getDFValue(moForm, msTable, "U_ItemCd"), String)
                msItemGrp = CType(mIDHForm.getDFValue(moForm, msTable, "U_ItmGrp"), String)
                msBranch = CType(mIDHForm.getDFValue(moForm, msTable, "U_Branch"), String)
                msJobType = CType(mIDHForm.getDFValue(moForm, msTable, "U_JobTp"), String)
                msStatus = CType(mIDHForm.getDFValue(moForm, msTable, "U_Status"), String)
                msUseWgt = CType(mIDHForm.getDFValue(moForm, msTable, "U_UseWgt"), String)

                Dim sFormMode As String = CType(mIDHForm.getWFValue(moForm, "FormMode"), String)

                If msWROrdType = "DO" Then
                    If Config.INSTANCE.doCheckCanPurchaseWB(msWastCd) AndAlso
                                       Config.INSTANCE.isIncomingJob(Config.INSTANCE.doGetDOContainerGroup(), msJobType) Then
                        msFormMode = "PO"
                    Else
                        msFormMode = "SO"
                    End If
                Else
                    msFormMode = "SO"
                End If

                mdDefWei = CType(mIDHForm.getWFValue(moForm, "DEFWEI"), Double)

                Dim sDocDate As String
                If msWROrdType = "DO" Then
                    'sDocDate = mIDHForm.getDFValue(moForm, msTable, "U_RDate")
                    sDocDate = Conversions.ToString(mIDHForm.getDFValue(moForm, msHeadTable, "U_BDate"))
                Else
                    sDocDate = Conversions.ToString(mIDHForm.getDFValue(moForm, msTable, "U_AEDate"))
                End If
                mdDocDate = com.idh.utils.Dates.doStrToDate(sDocDate)

                msLoadSht = CType(mIDHForm.getDFValue(moForm, msTable, "U_LoadSht"), String)
                mdRdWgt = CType(mIDHForm.getDFValue(moForm, msTable, "U_RdWgt"), Double)
                mdCstWgt = CType(mIDHForm.getDFValue(moForm, msTable, "U_CstWgt"), Double)
                mdAUOMQty = CType(mIDHForm.getDFValue(moForm, msTable, "U_AUOMQt"), Double)
                msAUOM = CType(mIDHForm.getDFValue(moForm, msTable, "U_AUOM"), String)
                mdTCharge = CType(mIDHForm.getDFValue(moForm, msTable, "U_TCharge"), Double)
                mdTChrgVtRt = CType(mIDHForm.getDFValue(moForm, msTable, "U_TChrgVtRt"), Double)
                mdHChrgVtRt = CType(mIDHForm.getDFValue(moForm, msTable, "U_HChrgVtRt"), Double)
                msTChrgVtGrp = CType(mIDHForm.getDFValue(moForm, msTable, "U_TChrgVtGrp"), String)
                msHChrgVtGrp = CType(mIDHForm.getDFValue(moForm, msTable, "U_HChrgVtGrp"), String)

                mdWgtDed = CType(mIDHForm.getDFValue(moForm, msTable, "U_WgtDed"), Double)
                mdAdjWgt = CType(mIDHForm.getDFValue(moForm, msTable, "U_AdjWgt"), Double)

                msPUOM = CType(mIDHForm.getDFValue(moForm, msTable, "U_PUOM"), String)

                mdTRdWgt = CType(mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._TRdWgt), Double)
                mdTipWgt = CType(mIDHForm.getDFValue(moForm, msTable, "U_TipWgt"), Double)
                mdTipCost = CType(mIDHForm.getDFValue(moForm, msTable, "U_TipCost"), Double)
                mdTCostVtRt = CType(mIDHForm.getDFValue(moForm, msTable, "U_TCostVtRt"), Double)
                msTCostVtGrp = CType(mIDHForm.getDFValue(moForm, msTable, "U_TCostVtGrp"), String)

                mdPCostVtRt = CType(mIDHForm.getDFValue(moForm, msTable, "U_PCostVtRt"), Double)
                msPCostVtGrp = CType(mIDHForm.getDFValue(moForm, msTable, "U_PCostVtGrp"), String)

                mdProWgt = CType(mIDHForm.getDFValue(moForm, msTable, "U_ProWgt"), Double)
                msProUOM = CType(mIDHForm.getDFValue(moForm, msTable, "U_ProUOM"), String)
                mdPCost = CType(mIDHForm.getDFValue(moForm, msTable, "U_PCost"), Double)
                mdPCtotal = CType(mIDHForm.getDFValue(moForm, msTable, "U_PCTotal"), Double)

                mdOrdWgt = CType(mIDHForm.getDFValue(moForm, msTable, "U_OrdWgt"), Double)
                mdOrdCost = CType(mIDHForm.getDFValue(moForm, msTable, "U_OrdCost"), Double)
                mdHCostVtRt = CType(mIDHForm.getDFValue(moForm, msTable, "U_HCostVtRt"), Double)
                msHCostVtGrp = CType(mIDHForm.getDFValue(moForm, msTable, "U_HCostVtGrp"), String)
                'mdVtCostAmt

                mdCusChr = CType(mIDHForm.getDFValue(moForm, msTable, "U_CusChr"), Double)
                mdTCTotal = CType(mIDHForm.getDFValue(moForm, msTable, "U_TCTotal"), Double)
                mdTCTotal = CType(mIDHForm.getDFValue(moForm, msTable, "U_TCTotal"), Double)
                mdAddEx = CType(mIDHForm.getDFValue(moForm, msTable, "U_AddEx"), Double)
                mdDisAmt = CType(mIDHForm.getDFValue(moForm, msTable, "U_DisAmt"), Double)
                mdTaxAmt = CType(mIDHForm.getDFValue(moForm, msTable, "U_TaxAmt"), Double)
                mdDiscnt = CType(mIDHForm.getDFValue(moForm, msTable, "U_Discnt"), Double)
                msUOM = CType(mIDHForm.getDFValue(moForm, msTable, "U_UOM"), String)

                msCCHCos = CType(mIDHForm.getWFValue(moForm, "IDH_CCHCos"), String)
                msCCHChr = CType(mIDHForm.getWFValue(moForm, "IDH_CCHChr"), String)
                msCCTChr = CType(mIDHForm.getWFValue(moForm, "IDH_CCTChr"), String)
                mdTOffset = CType(mIDHForm.getWFValue(moForm, "IDH_TOffset"), Double)
                mdHOffset = CType(mIDHForm.getWFValue(moForm, "IDH_HOffset"), Double)
                mdTCostOffset = CType(mIDHForm.getWFValue(moForm, "IDH_TCostOffset"), Double)
                mdPCostOffset = CType(mIDHForm.getWFValue(moForm, "IDH_PCostOffset"), Double)
                mdHCostOffset = CType(mIDHForm.getWFValue(moForm, "IDH_HCostOffset"), Double)

                msCCTCos = CType(mIDHForm.getWFValue(moForm, "IDH_CCTCos"), String)

            End If
            mbWasRead = True
        End Sub
		 
		Private Function getFormMode() As String
			If Not mIDHForm Is Nothing Then
                Return CType(mIDHForm.getWFValue(moForm, "FormMode"), String)
			Else
				Return ""
			End If
		End Function

		Public Function getWrkWeight() As Double
			Return mdWrkWeight
		End Function

		Private Sub setData()
			If Not mIDHForm Is Nothing Then
				'mIDHForm.setDFValue(moForm, msTable, "U_Tip", msTipCode)
				
				mIDHForm.setDFValue(moForm, msTable, "U_TipWgt", mdTipWgt)
				mIDHForm.setDFValue(moForm, msTable, "U_CstWgt", mdCstWgt)
				mIDHForm.setDFValue(moForm, msTable, "U_ProWgt", mdProWgt)
				
				mIDHForm.setDFValue(moForm, msTable, "U_ItemCd", msItemCode)
			End If
		End Sub
		
		Private Sub setWeightData()
			If Not mIDHForm Is Nothing Then
				mIDHForm.setDFValue(moForm, msTable, "U_RdWgt", mdRdWgt)
				
				'Keep ReadWeights in synch
				If msFormMode = "PO" Then
					mIDHForm.setDFValue(moForm, msTable, IDH_JOBSHD._PRdWgt, mdRdWgt)
				End If
				mIDHForm.setDFValue(moForm, msTable, IDH_JOBSHD._TRdWgt, mdRdWgt)
			End If
		End Sub
		
		Private Sub setUOMData()
			'UOM Start
			If Not mIDHForm Is Nothing Then
				If msPUOM Is Nothing OrElse msPUOM.Length = 0 Then
					msPUOM = msUOM
				End If
				
				mIDHForm.setDFValue(moForm, msTable, "U_PUOM", msPUOM)
				mIDHForm.setDFValue(moForm, msTable, "U_ProUOM", msProUOM)
				mIDHForm.setDFValue(moForm, msTable, "U_AUOM", msAUOM)
				If mIDHForm.wasSetWithCode(moForm, "IDH_UOM") = True AndAlso msLoadSht.Length = 0 Then
					mIDHForm.setDFValue(moForm, msTable, "U_UOM", msUOM)
				End If
			End If
			'UOM End
		End Sub
		
        Private Sub setChargeData(ByVal bDoTPrice As Boolean, ByVal bDoHPrice As Boolean)
            'Charge Pricing Start
            If Not mIDHForm Is Nothing Then
                mIDHForm.setDFValue(moForm, msTable, "U_UseWgt", msUseWgt)

                If bDoTPrice = True Then
                    mIDHForm.setDFValue(moForm, msTable, "U_TCharge", mdTCharge)

                    Dim iPrcChanged As Integer = CType(mIDHForm.getDFValue(moForm, msTable, "U_MANPRC"), Integer)
                    iPrcChanged = iPrcChanged And (Not Config.MASK_TIPCHRG)
                    mIDHForm.setDFValue(moForm, msTable, "U_MANPRC", iPrcChanged)
                End If
                If bDoHPrice = True Then
                    mIDHForm.setDFValue(moForm, msTable, "U_CusChr", mdCusChr)

                    Dim iPrcChanged As Integer = CType(mIDHForm.getDFValue(moForm, msTable, "U_MANPRC"), Integer)
                    iPrcChanged = iPrcChanged And (Not Config.MASK_HAULCHRG)
                    mIDHForm.setDFValue(moForm, msTable, "U_MANPRC", iPrcChanged)
                End If

                mIDHForm.setDFValue(moForm, msTable, "U_TChrgVtRt", mdTChrgVtRt)
                mIDHForm.setDFValue(moForm, msTable, "U_HChrgVtRt", mdHChrgVtRt)
                mIDHForm.setDFValue(moForm, msTable, "U_TChrgVtGrp", msTChrgVtGrp)
                mIDHForm.setDFValue(moForm, msTable, "U_HChrgVtGrp", msHChrgVtGrp)

                mIDHForm.setWFValue(moForm, "IDH_TOffset", mdTOffset)
                mIDHForm.setWFValue(moForm, "IDH_HOffset", mdHOffset)
                mIDHForm.setWFValue(moForm, "IDH_CCTChr", msCCTChr)
                mIDHForm.setWFValue(moForm, "IDH_CCHChr", msCCHChr)
            End If
            'Charge Pricing End
        End Sub
		
		Private Sub setTipCostData(ByVal bDoTPrice As Boolean )
			'Charge Pricing Start
			If Not mIDHForm Is Nothing Then
				If bDoTPrice = True Then
					mIDHForm.setDFValue(moForm, msTable, "U_TipCost", mdTipCost)
					
                    Dim iPrcChanged As Integer = CType(mIDHForm.getDFValue(moForm, msTable, "U_MANPRC"), Integer)
                    iPrcChanged = iPrcChanged And (Not Config.MASK_TIPCST)
					mIDHForm.setDFValue(moForm, msTable, "U_MANPRC", iPrcChanged)
				End If
				
				mIDHForm.setDFValue(moForm, msTable, "U_TCostVtRt", mdTCostVtRt)
				mIDHForm.setDFValue(moForm, msTable, "U_TCostVtGrp", msTCostVtGrp)
				mIDHForm.setWFValue(moForm, "IDH_TCostOffset", mdTCostOffset)
				mIDHForm.setWFValue(moForm, "IDH_CCTCos", msCCTCos)
            End If
		End Sub

		Private Sub setProducerCostData(ByVal bDoPPrice As Boolean )
			'Charge Pricing Start
			If Not mIDHForm Is Nothing Then
				If bDoPPrice = True Then
					mIDHForm.setDFValue(moForm, msTable, "U_PCost", mdPCost)
					
                    Dim iPrcChanged As Integer = CType(mIDHForm.getDFValue(moForm, msTable, "U_MANPRC"), Integer)
                    iPrcChanged = iPrcChanged And (Not Config.MASK_PRODUCERCST)
					mIDHForm.setDFValue(moForm, msTable, "U_MANPRC", iPrcChanged)
				End If

                mIDHForm.setDFValue(moForm, msTable, "U_PCostVtRt", mdPCostVtRt)
                mIDHForm.setDFValue(moForm, msTable, "U_PCostVtGrp", msPCostVtGrp)
				mIDHForm.setWFValue(moForm, "IDH_PCostOffset", mdPCostOffset)
				mIDHForm.setWFValue(moForm, "IDH_CCPCos", msCCPCos)
            End If
		End Sub
		
		Private Sub setHaulageCostData(ByVal bDoHPrice As Boolean )
			'Charge Pricing Start
			If Not mIDHForm Is Nothing Then
				mIDHForm.setDFValue(moForm, msTable, "U_OrdWgt", mdOrdWgt)
				
				If bDoHPrice = True Then
					mIDHForm.setDFValue(moForm, msTable, "U_OrdCost", mdOrdCost)

                    Dim iPrcChanged As Integer = CType(mIDHForm.getDFValue(moForm, msTable, "U_MANPRC"), Integer)
                    iPrcChanged = iPrcChanged And (Not Config.MASK_HAULCST)
					mIDHForm.setDFValue(moForm, msTable, "U_MANPRC", iPrcChanged)
				End If
				
				mIDHForm.setDFValue(moForm, msTable, "U_HCostVtRt", mdHCostVtRt)
				mIDHForm.setDFValue(moForm, msTable, "U_HCostVtGrp", msHCostVtGrp)
				mIDHForm.setWFValue(moForm, "IDH_HCostOffset", mdHCostOffset)
				mIDHForm.setWFValue(moForm, "IDH_CCHCos", msCCHCos)
            End If
		End Sub

'		Private Sub setChargeCalcData()
'			If Not mIDHForm Is Nothing Then
'                mIDHForm.setDFValue(moForm, msTable, "U_TChrgVtRt", mdTChrgVtRt)
'                mIDHForm.setDFValue(moForm, msTable, "U_HChrgVtRt", mdHChrgVtRt)
'                mIDHForm.setDFValue(moForm, msTable, "U_Discnt", mdDiscnt)
'                mIDHForm.setDFValue(moForm, msTable, "U_DisAmt", mdDisAmt)
'                mIDHForm.setDFValue(moForm, msTable, "U_TaxAmt", mdTaxAmt)
'                mIDHForm.setDFValue(moForm, msTable, "U_TCTotal", mdTCTotal)
'                mIDHForm.setDFValue(moForm, msTable, "U_Total", mdTotal)
'
'                mIDHForm.setUFValue(moForm, "IDH_SUBTOT", mdSubTotal)
'			End If
'		End Sub
'
'		Private Sub setCostCalcData()
'			If Not mIDHForm Is Nothing Then
'                mIDHForm.setDFValue(moForm, msTable, "U_TipTot", mdTipTot)
'                mIDHForm.setDFValue(moForm, msTable, "U_TipTot", mdVTipTot) 'mdVtCostAmt
''                mIDHForm.setDFValue(moForm, msTable, "U_HChrgVtRt", mdHChrgVtRt)
''                mIDHForm.setDFValue(moForm, msTable, "U_Discnt", mdDiscnt)
''                mIDHForm.setDFValue(moForm, msTable, "U_DisAmt", mdDisAmt)
''                mIDHForm.setDFValue(moForm, msTable, "U_TaxAmt", mdTaxAmt)
''                mIDHForm.setDFValue(moForm, msTable, "U_TCTotal", mdTCTotal)
''                mIDHForm.setDFValue(moForm, msTable, "U_Total", mdTotal)
''
''                mIDHForm.setUFValue(moForm, "IDH_SUBTOT", mdSubTotal)
'			End If
'		End Sub
		
		Private Sub setCostCalcData()
			If Not mIDHForm Is Nothing Then
				
			End If
		End Sub
		
		Private Sub setDOItem()
			If Not mIDHForm Is Nothing Then
				mIDHForm.setDFValue(moForm, msTable, "U_ItemCd", msItemCode)
			End If
		End Sub
		
		Private Sub setTipBuyWeightData()
			If Not mIDHForm Is Nothing Then
				mIDHForm.setDFValue(moForm, msTable, "U_TipWgt", mdTipWgt)
			End If
		End Sub
		
		Private Sub setHaulageBuyWeightData()
			If Not mIDHForm Is Nothing Then
				mIDHForm.setDFValue(moForm, msTable, "U_OrdWgt", mdOrdWgt)
			End If
		End Sub
		
		Private Sub setProducerBuyWeightData()
			If Not mIDHForm Is Nothing Then
				mIDHForm.setDFValue(moForm, msTable, "U_ProWgt", mdProWgt)
			End If
		End Sub
		
		Private Sub setTipSellWeightData()
			If Not mIDHForm Is Nothing Then
				mIDHForm.setDFValue(moForm, msTable, "U_CstWgt", mdCstWgt)
			End If
		End Sub
		
'		Public Sub setDocDate(ByRef dDocDate As Date)
'			mdDocDate = dDocDate
'		End Sub
		 
		Public Function doCalcTipCostWgtCalc() As Boolean
            msCCTCos = CType(Config.INSTANCE.getSIPCIPValue("U_ChrgCal", False, 1, msCIPWROrdType, msJobType, msItemGrp, msItemCode, msCardCode, msTipCode, msWastCd, msSTADDR, mdDocDate, msBranch, msZIPCODE), String)
            Return True
		End Function
		
		Public Function doCalcHaulCostWgtCalc() As Boolean
            msCCHCos = CType(Config.INSTANCE.getSIPCIPValue("U_HaulCal", False, 1, msCIPWROrdType, msJobType, msItemGrp, msItemCode, msCardCode, msTipCode, msWastCd, msSTADDR, mdDocDate, msBranch, msZIPCODE), String)
            Return True
        End Function
		
	    'Return True If UOM Set
        'Else False
        Public Function doCalcUOM() As Boolean
        	'Check to see if this is a CIP line where the UOM must be pushed from the CIP
            Dim sWrkItemCode As String = msItemCode

            If msCIPWROrdType = "DO" Then
                If Config.Parameter("MDDODI").ToUpper().Equals("TRUE") Then
                    sWrkItemCode = Config.Parameter("MDDODE")
                Else
                    If String.IsNullOrWhiteSpace(sWrkItemCode) Then
                        sWrkItemCode = Config.Parameter("MDDODE")
                        msItemCode = sWrkItemCode
                    End If
                End If
            End If

            Dim sCIPUOM As String = Nothing
            Dim sPUOM As String = Nothing
            Dim sProUOM As String = Nothing
            If msLoadSht.Length = 0 Then
                sPUOM = Config.INSTANCE.getSIPAnyUOM(msCIPWROrdType, msJobType, msItemGrp, sWrkItemCode, msCardCode, msTipCode, msWastCd, msSTADDR, mdDocDate, msBranch, msZIPCODE)
                If Not msFormMode = "PO" Then
                    sCIPUOM = Config.INSTANCE.getCIPAnyUOM(msCIPWROrdType, msJobType, msItemGrp, sWrkItemCode, msCardCode, msWastCd, msSTADDR, mdDocDate, msBranch, msZIPCODE)
                Else
                    sProUOM = Config.INSTANCE.getSIPAnyUOM(msCIPWROrdType, msJobType, msItemGrp, sWrkItemCode, msCardCode, msProCode, msWastCd, msSTADDR, mdDocDate, msBranch, msZIPCODE)
                End If
            End If

            If Not sCIPUOM Is Nothing Then
                mbUseCIPUom = True
                If sProUOM Is Nothing AndAlso msFormMode = "PO" Then
                    msProUOM = sCIPUOM
                End If

                If sPUOM Is Nothing Then
                    msPUOM = sCIPUOM
                End If
            End If

            If Not sPUOM Is Nothing Then
                msPUOM = sPUOM
                mbUseSIPPUOM = True
            End If

            If Not sProUOM Is Nothing Then
                msProUOM = sProUOM
                mbUseSIPProUOM = True
            End If

            If Not msFormMode = "PO" Then
                If mbUseCIPUom AndAlso mbUseSIPPUOM Then
                    setUOMData()
                    Return True
                End If
            Else
                If mbUseSIPProUOM AndAlso mbUseSIPPUOM Then
                    setUOMData()
                    Return True
                End If
            End If

            If msLoadSht.Length = 0 Then
                If msBPUOM.Equals(msDefUOM) Then
                    'If msFormMode = "PO" Then
                    '    msProUOM = msDefUOM
                    'Else
                    '    msUOM = msDefUOM
                    'End If
                Else
                    If mdAUOMQty <> 0 Then
                        Dim sRealUOM As String
                        If msFormMode = "PO" AndAlso Not msProUOM Is Nothing AndAlso msProUOM.Length > 0 Then
                            sRealUOM = msProUOM
                        Else
                            sRealUOM = msUOM
                        End If

                        If sRealUOM.Equals(msDefUOM) = True OrElse sRealUOM.Equals(msDefaultAUOM) = True Then
                            If Not msAUOM Is Nothing AndAlso msAUOM.Length() > 0 AndAlso msAUOM.Equals(msDefaultAUOM) = False Then
                                sRealUOM = msAUOM
                            Else
                                If msWastCd.Length() > 0 Then
                                    sRealUOM = Config.INSTANCE.doGetItemSalesUOM(msCardCode, msWastCd)
                                    If sRealUOM.Length = 0 Then
                                        sRealUOM = msDefaultAUOM
                                    End If
                                    msAUOM = sRealUOM
                                End If
                            End If

                            If msFormMode = "PO" Then
                                If mbUseSIPPUOM = False Then
                                    msProUOM = sRealUOM
                                End If
                            Else
                                If mbUseCIPUom = False Then
                                    msUOM = sRealUOM
                                End If
                            End If
                        End If
                    Else
                        If msFormMode = "PO" Then
                            If mbUseSIPPUOM = False Then
                                msProUOM = msDefUOM
                            End If
                        Else
                            If mbUseCIPUom = False Then
                                msUOM = msDefUOM
                            End If
                        End If
                    End If

                    setUOMData()

                    Return True
                End If
            Else
                Return True
            End If

            Return False
        End Function

        '
        ' This will use the ReadWeights to create the correct work weights
        '
        Public Sub doGetWrkWeight()
            If mbWasRead = False Then
                Exit Sub
            End If

            Dim sUOM As String

            If msFormMode = "PO" Then
                If msProUOM Is Nothing Then
                    msProUOM = msUOM
                End If

                sUOM = msProUOM
            Else
                sUOM = msUOM
            End If

            If msWROrdType.Equals("DO") = False Then
                mbIsWBWeight = False
            End If

            If msLoadSht.Length > 0 Then
                mdWrkWeight = mdCstWgt
                mbIsWBWeight = True
            Else
                'If (msBPUOM.Length > 0 AndAlso msBPUOM.Equals(msDefUOM) = False) OrElse mbUseCIPUom OrElse sUOM.Equals(msDefaultAUOM) Then
                If Config.INSTANCE.doGetBaseWeightFactors(sUOM) < 0 Then
                    msUseWgt = "2"
                End If

                If msUseWgt = "2" Then
                    mdWrkWeight = mdAUOMQty

                    mbIsWBWeight = False
                    Dim dDefWeight As Double = 0
                    dDefWeight = mdDefWei
                    If dDefWeight <> 0 AndAlso mdRdWgt = 0 Then
                        'Dim dWei As Double
                        mdRdWgt = dDefWeight * mdWrkWeight
                        setWeightData()
                    End If
                Else
                    If mdWgtDed <> 0 Then
                        mdWrkWeight = mdAdjWgt
                    Else
                        mdWrkWeight = mdRdWgt
                    End If
                    mbIsWBWeight = True
                    'setWeightData()
                End If

                If Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), msJobType) Then
                    mdTCostWrkWeight = mdWrkWeight
                Else
                    mdTCostWrkWeight = 0
                End If

                If msFormMode = "PO" Then
                    mdPCostWrkWeight = mdWrkWeight
                Else
                    mdPCostWrkWeight = 0
                End If

                If mbIsWBWeight = True AndAlso msLoadSht.Length = 0 Then
                    mdWrkWeight = Config.INSTANCE.doConvertWeights(Config.INSTANCE.getWeighBridgeUOM(), msUOM, mdWrkWeight)
                    mdTCostWrkWeight = Config.INSTANCE.doConvertWeights(Config.INSTANCE.getWeighBridgeUOM(), msPUOM, mdTCostWrkWeight)

                    If msFormMode = "PO" Then
                        mdPCostWrkWeight = Config.INSTANCE.doConvertWeights(Config.INSTANCE.getWeighBridgeUOM(), msProUOM, mdPCostWrkWeight)
                    End If
                End If
            End If
        End Sub

        Public Function doApplyWeightFormulas() As Boolean
            If mbWasRead = False Then
                Return True
            End If

            'The Charge Tipping Weight
            If msLoadSht.Length = 0 AndAlso
             mbIsWBWeight = True AndAlso
             mdWrkWeight <> 0 Then
                If msCCTChr.ToUpper().Equals("OFFSET") Then
                    If mdTOffset < mdWrkWeight Then
                        mdCstWgt = mdWrkWeight - mdTOffset
                    Else
                        mdCstWgt = 0
                    End If
                ElseIf msCCTChr.ToUpper().Equals("QTY1") Then
                    mdCstWgt = 1
                Else
                    mdCstWgt = mdWrkWeight
                End If
            Else
                mdCstWgt = mdWrkWeight
            End If
            setTipSellWeightData()

            'Producer Weight
            'If msFormMode = "PO" Then
            If msLoadSht.Length = 0 AndAlso
                      mbIsWBWeight = True AndAlso
                      mdPCostWrkWeight <> 0 Then
                If msCCPCos.ToUpper().Equals("OFFSET") Then
                    If mdPCostOffset < mdPCostWrkWeight Then
                        mdProWgt = mdPCostWrkWeight - mdPCostOffset
                    Else
                        mdProWgt = 0
                    End If
                ElseIf msCCPCos.ToUpper().Equals("QTY1") Then
                    mdProWgt = 1
                Else
                    mdProWgt = mdPCostWrkWeight
                End If
            Else
                mdProWgt = mdPCostWrkWeight
            End If
            setProducerBuyWeightData()
            'End If

            'Tipping Cost Weight
            'If Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), msJobType) Then
            If msLoadSht.Length = 0 AndAlso
          mbIsWBWeight = True AndAlso
          mdTCostWrkWeight <> 0 Then
                If msCCTCos.ToUpper().Equals("OFFSET") Then
                    If mdTCostOffset < mdTCostWrkWeight Then
                        mdTipWgt = mdTCostWrkWeight - mdTCostOffset
                    Else
                        mdTipWgt = 0
                    End If
                ElseIf msCCTCos.ToUpper().Equals("QTY1") Then
                    mdTipWgt = 1
                ElseIf msCCTCos.ToUpper().Equals("WEIGHT") Then
                    mdTipWgt = mdTCostWrkWeight

                    'If Not msUOM = msPUOM Then
                    '    mdTipWgt = Config.INSTANCE.doConvertWeights(moParent, msUOM, msPUOM, mdTipWgt)
                    'End If
                ElseIf msCCTCos.ToUpper().Equals("VARIABLE") Then
                    mdTipWgt = mdTCostWrkWeight

                    'If Not msUOM = msPUOM Then
                    '    mdTipWgt = Config.INSTANCE.doConvertWeights(moParent, msUOM, msPUOM, dBuyQty)
                    'End If
                End If
            Else
                mdTipWgt = mdTCostWrkWeight
            End If
            setTipBuyWeightData()
            'End If

            'Haulage Cost
            If msCCHCos.ToUpper().Equals("OFFSET") Then
                If mdHCostOffset < mdWrkWeight Then
                    mdOrdWgt = mdWrkWeight - mdHCostOffset
                Else
                    mdOrdWgt = 0
                End If
            ElseIf msCCHCos.ToUpper().Equals("QTY1") Then
                mdOrdWgt = 1
            ElseIf msCCHCos.ToUpper().Equals("WEIGHT") Then
                mdOrdWgt = mdWrkWeight
            Else
                'mdOrdWgt = 0
            End If
            setHaulageBuyWeightData()

            '            If msWROrdType = "DO" Then
            '				If msFormMode = "PO" Then
            '            		mdProWgt = dCSWeight
            '            		setProducerBuyWeightData()
            '				Else
            '                    If Config.INSTANCE.isOutgoingJob(Config.INSTANCE.doGetDOContainerGroup(), msJobType) Then
            '                        Dim dBuyQty As Double = -99999
            '
            '                        If msLoadSht.Length > 0 Then
            '                            dBuyQty = mdWrkWeight
            '                        Else
            '                            If msCCTCos.ToUpper().Equals("OFFSET") Then
            '                                If mdTCostOffset < mdTCostWrkWeight Then
            '                                    dBuyQty = mdTCostWrkWeight - mdTCostOffset
            '                                Else
            '                                    dBuyQty = 0
            '                                End If
            '                            ElseIf msCCTCos.ToUpper().Equals("QTY1") Then
            '                                dBuyQty = 1
            '                            ElseIf msCCTCos.ToUpper().Equals("WEIGHT") Then
            '                                dBuyQty = mdTCostWrkWeight
            '
            '                                If Not msUOM = msPUOM Then
            '                                    dBuyQty = Config.INSTANCE.doConvertWeights(moParent, msUOM, msPUOM, dBuyQty)
            '                                End If
            '                            ElseIf msCCTCos.ToUpper().Equals("VARIABLE") Then
            '                                dBuyQty = mdTCostWrkWeight
            '
            '                                If Not msUOM = msPUOM Then
            '                                    dBuyQty = Config.INSTANCE.doConvertWeights(moParent, msUOM, msPUOM, dBuyQty)
            '                                End If
            '                            End If
            '
            '                            If dBuyQty > -99999 Then
            '                                mdTipWgt = dBuyQty
            '                                setTipBuyWeightData()
            '                            End If
            '                        End If
            '
            '                        dBuyQty = -99999
            '                        If msCCHCos.ToUpper().Equals("OFFSET") Then
            '                            If mdHCostOffset < mdWrkWeight Then
            '                                dBuyQty = mdWrkWeight - mdHCostOffset
            '                            Else
            '                                dBuyQty = 0
            '                            End If
            '                        ElseIf msCCHCos.ToUpper().Equals("QTY1") Then
            '                            dBuyQty = 1
            '                        ElseIf msCCHCos.ToUpper().Equals("WEIGHT") Then
            '                            dBuyQty = mdWrkWeight
            '                        End If
            '                        If dBuyQty > -99999 Then
            '                            mdOrdWgt = dBuyQty
            '                            setHaulageBuyWeightData()
            '                        End If
            '                    End If
            '
            '            		mdCstWgt = dCSWeight
            '            		setTipSellWeightData()
            '            	End If
            '            Else
            '            	mdCstWgt = dCSWeight
            '            	setTipSellWeightData()
            '            End If
            Return True
        End Function

        'Get the Auto Additional Expenses
        Public Function getAutoAdditionalCodes() As ArrayList
            Dim oList As ArrayList = Nothing

            Try
                If Not msCardCode Is Nothing AndAlso msCardCode.Length > 0 AndAlso Not msWastCd Is Nothing AndAlso msWastCd.Length > 0 Then
                    If msCIPWROrdType = "DO" Then
                        If Config.Parameter("MDDODI").ToUpper().Equals("TRUE") Then
                            msItemCode = Config.Parameter("MDDODE")
                        Else
                            If String.IsNullOrWhiteSpace(msItemCode) Then
                                msItemCode = Config.Parameter("MDDODE")
                            End If
                        End If
                    End If

                    oList = Config.INSTANCE.getAutoCIPAdditionalCodes(msCIPWROrdType, msJobType, msItemGrp, msItemCode, msCardCode, msWastCd, msSTADDR, mdDocDate, msBranch, msZIPCODE)

                    'if (isMarketingModeSOAswell() && U_CustCd != null && U_CustCd.Length > 0)
                    '    oList = com.idh.bridge.lookups.Config.INSTANCE.getAutoCIPAdditionalCodes("WO", U_JobTp, F_ContainerGroup, F_ContainerCode, U_CustCd, U_WasCd, F_CustomerAddress, U_RDate, U_Branch, F_CustomerZipCode);
                    'else if (isMarketingModePOAswell() && U_ProCd != null && U_ProCd.Length > 0)
                    '    oList = com.idh.bridge.lookups.Config.INSTANCE.getAutoSIPAdditionalCodes("WO", U_JobTp, F_ContainerGroup, F_ContainerCode, U_ProCd, U_CustCd, U_WasCd, F_CustomerAddress, U_RDate, U_Branch, F_CustomerZipCode);

                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error getting the auto additional Expenses - " & moForm.UniqueID)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGAAE", {moForm.UniqueID})
            End Try
            Return oList
        End Function

        Public Function doLookupChargePrices(ByVal oForm As SAPbouiCOM.Form) As com.idh.dbObjects.User.Prices
            'Dim sCardCode As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._CustCd)
            'Dim sItemCode As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._ItemCd)
            'Dim sItemGrp As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._ItmGrp)
            'Dim sWastCd As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._WasCd)

            'Dim sAddress As String = mIDHForm.getWFValue(oForm, "STADDR")
            'Dim sZip As String = mIDHForm.getWFValue(oForm, "ZIPCODE")

            'Dim sBranch As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._Branch)
            'Dim sJobType As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._JobTp)

            'Dim sDocDate As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._RDate)
            'Dim dDocDate As Date = com.idh.utils.dates.doStrToDate(sDocDate)

            'Dim sUOM As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._UOM)
            Dim dWeight As Double = CType(mIDHForm.getDFValue(oForm, msTable, IDH_JOBSHD._CstWgt), Double)

            Dim sWrkItemCode As String = msItemCode

            If msCIPWROrdType = "DO" Then
                If Config.Parameter("MDDODI").ToUpper().Equals("TRUE") Then
                    sWrkItemCode = Config.Parameter("MDDODE")
                Else
                    If String.IsNullOrWhiteSpace(sWrkItemCode) Then
                        sWrkItemCode = Config.Parameter("MDDODE")
                        msItemCode = sWrkItemCode
                    End If
                End If
            End If

            Dim oPrices As com.idh.dbObjects.User.Prices

            'oPrices = moCIP.doGetJobChargePrices(msWROrdType, sJobType, sItemGrp, sItemCode, sCardCode, dWeight, sUOM, sWastCd, sAddress, dDocDate, sBranch, sZip)
            oPrices = moCIP.doGetJobChargePrices(msCIPWROrdType, msJobType, msItemGrp, sWrkItemCode, msCardCode, dWeight, msUOM, msWastCd, msCarrCode, msSTADDR, mdDocDate, msBranch, msZIPCODE)
            Return oPrices
        End Function

        'Public Sub doGetChargePrices(Optional ByVal bReplaceHaulage As Boolean = False)
        Public Sub doGetChargePrices(ByVal bDoTPrice As Boolean, ByVal bDoHPrice As Boolean)

            If mbWasRead = False Then
                Exit Sub
            End If

            If mdWrkWeight = -999999 Then
                doGetWrkWeight()
            End If

            If mdWrkWeight = 0 Then
                If bDoHPrice = True Then
                    mdCusChr = 0
                End If
                If bDoTPrice = True Then
                    mdTCharge = 0
                End If

                mdTChrgVtRt = 0
                mdHChrgVtRt = 0
                msTChrgVtGrp = ""
                msHChrgVtGrp = ""

                msCCHChr = "STANDARD"
                msCCTChr = "VARIABLE"
                mdTOffset = 0
                mdHOffset = 0

                setChargeData(bDoTPrice, bDoHPrice)
                Exit Sub
            End If

            If Config.INSTANCE.doCheckCanChange(msStatus) = False Then
                Exit Sub
            End If
            Try
                If msCardCode.Length > 0 AndAlso msItemCode.Length > 0 Then
                    Dim oPrices As com.idh.dbObjects.User.Prices

                    Dim sWrkItemCode As String = msItemCode

                    If msCIPWROrdType = "DO" Then
                        If Config.Parameter("MDDODI").ToUpper().Equals("TRUE") Then
                            sWrkItemCode = Config.Parameter("MDDODE")
                        Else
                            If String.IsNullOrWhiteSpace(sWrkItemCode) Then
                                sWrkItemCode = Config.Parameter("MDDODE")
                                msItemCode = sWrkItemCode
                            End If
                        End If
                    End If

                    'oPrices = Config.INSTANCE.doGetJobChargePrices(msWROrdType, msJobType, msItemGrp, sWrkItemCode, msCardCode, mdWrkWeight, msUOM, msWastCd, msSTADDR, mdDocDate, msBranch, msZIPCODE)
                    oPrices = moCIP.doGetJobChargePrices(msCIPWROrdType, msJobType, msItemGrp, sWrkItemCode, msCardCode, mdWrkWeight, msUOM, msWastCd, msCarrCode, msSTADDR, mdDocDate, msBranch, msZIPCODE)

                    'PRICES
                    ' 0 - HAULAGE PRICE
                    ' 1 - TIPP CHARGE
                    ' 2 - VAT
                    ' 3 - CHARGE TYPE
                    ' 4 - Offset
                    ' 5 - Do Band
                    ' 6 - Configured UOM

                    Dim dPrice As Double
                    If msCIPWROrdType = "WO" Then
                        If bDoHPrice = True Then
                            dPrice = oPrices.HaulPrice 'aPrices(0)
                            If oPrices.Band = True Then 'aPrices(5) = True Then
                                dPrice = dPrice * doGetBandFactor()
                            End If
                            mdCusChr = dPrice
                        End If

                        If bDoTPrice = True Then
                            dPrice = oPrices.TipPrice
                            If Not msWastCd Is Nothing AndAlso msWastCd.Length > 0 Then
                                If Config.INSTANCE.doCheckIsRebate(msWastCd) Then
                                    '			                   		dPrice = dPrice * -1
                                End If
                            End If
                            mdTCharge = dPrice 'oPrices.mdTipPrice ) 'aPrices(1))
                        End If
                    Else
                        If bDoTPrice = True Then
                            dPrice = oPrices.TipPrice
                            mdTCharge = dPrice 'oPrices.mdTipPrice ) 'aPrices(1))
                        End If
                    End If

                    mdTChrgVtRt = oPrices.TipVat
                    mdHChrgVtRt = oPrices.HaulageVat
                    msTChrgVtGrp = oPrices.TipVatGroup
                    msHChrgVtGrp = oPrices.HaulageVatGroup

                    msCCHChr = oPrices.HaulageChargeCalc
                    msCCTChr = oPrices.TipChargeCalc

                    'Offset is based on the readweight UOM
                    'mdTOffset = Config.INSTANCE.doConvertWeights(oPrices.msUOM, msUOM, oPrices.mdTipOffsetWeight)
                    mdTOffset = oPrices.TipOffsetWeight

                    mdHOffset = oPrices.HaulageOffsetWeight
                Else
                    If bDoHPrice = True Then
                        mdCusChr = 0
                    End If

                    If bDoTPrice = True Then
                        mdTCharge = 0
                    End If
                    mdTChrgVtRt = 0
                    mdHChrgVtRt = 0
                    msTChrgVtGrp = ""
                    msHChrgVtGrp = ""

                    msCCHChr = "STANDARD"
                    msCCTChr = "VARIABLE"
                    mdTOffset = 0
                    mdHOffset = 0
                End If

                '                Dim oGrid As idh.controls.grid.AdditionalExpenses = idh.controls.grid.AdditionalExpenses.getInstance(oForm, "ADDGRID")
                '	           	If Not oGrid Is Nothing Then
                '	           		oGrid.doCalculateLinesPrices(sCardCode,sItemCode,sItemGrp,sWastCd,sAddress,sBranch,sJobType,dDocDate,dReadWeight,sUOM)
                '	           	End If
                setChargeData(bDoTPrice, bDoHPrice)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error getting the prices to charge - " & moForm.UniqueID)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRPTC", {moForm.UniqueID})
            End Try
        End Sub
        
'        'P - percentage, A - Amount, S - AutoSelect
'        Public Sub doCalcMarketingTotals(ByVal cT As Char)
'        	If msWROrdType = "DO" AndAlso msFormMode = "PO" Then
'                If msCCTChr.ToUpper().Equals("FIXED") Then
'                    If mdTipCost = 0 Then
'                        mdTipTot = 0
'                    Else
'                        mdTipTot = mdTipCost
'                    End If
'                Else
'                    mdTipTot = mdTipCost * mdTipWgt
'	            End If
'
'	            mdVtCostAmt = mdTipTot * (mdTCostVtRt / 100)
'	            mdJCost     = mdTipTot + mdVtCostAmt
'
''	            mdVtCostAmt = dPOTotal * (dVatPerc / 100)
''	            dPOTotal = dPOTotal + dVatAmnt
''
''	            setDFValue(oForm, "@IDH_DISPROW", "U_VtCostAmt", dVatAmnt)
''        		setDFValue(oForm, "@IDH_DISPROW", "U_JCost", CStr(dPOTotal))
'        	Else
'	            Dim dTotal As Double = 0
'	            Dim dCustWeight As Double
'	            Dim dCustCharge As Double
'	            Dim dCustTotal As Double
'	            Dim dAddExpenses As Double
'	            Dim dBefDiscTotal As Double
'	            Dim dDiscountAmt As Double
'	            Dim dDiscnt As Double
'	            Dim dVatAmnt As Double
'	            Dim dVatPerc As Double = 0
'	            Dim sCalcType As String = msCCTChr
'
'	            Try
'	                If cT = "R" Then
'	                    dCustTotal = mdTCTotal
'	                    dAddExpenses = mdAddEx
'	                    dBefDiscTotal = dCustTotal + dAddExpenses
'	                    dDiscountAmt = mdDisAmt
'
'	                    mdSubTotal = dBefDiscTotal - dDiscountAmt
'
'	                    dVatAmnt = mdTaxAmt
'	                    If dVatPerc = 0 AndAlso dVatAmnt > 0 Then
'	                        dTotal = dBefDiscTotal - dDiscountAmt
'	                        dVatPerc = (dVatAmnt / dTotal * 100)
'	                        mdTChrgVtRt = dVatPerc
'	                        mdHChrgVtRt = dVatPerc
'	                    End If
'	                Else
'	                    dDiscnt = mdDiscnt
'	                    dDiscountAmt = mdDisAmt
'	                    dCustCharge = mdTCharge
'	                    dCustWeight = mdCstWgt
'	                    dVatPerc = mdTChrgVtRt
'
'	                    If sCalcType.ToUpper().Equals("FIXED") Then
'	                        If dCustWeight = 0 Then
'	                            dCustTotal = 0
'	                        Else
'	                            dCustTotal = dCustCharge
'	                        End If
'	                    Else
'	                        dCustTotal = dCustCharge * dCustWeight
'	                    End If
'
'	                    dAddExpenses = mdAddEx
'	                    dBefDiscTotal = dCustTotal + dAddExpenses ' + dPrice
'
'	                    If cT = "S" Then
'	                        If dDiscnt = 0 AndAlso dDiscountAmt <> 0 Then
'	                            dDiscnt = dDiscountAmt / dBefDiscTotal * 100
'	                            mdDiscnt = CStr(dDiscnt)
'	                        ElseIf dDiscnt <> 0 Then
'	                            dDiscountAmt = (dDiscnt / 100) * dBefDiscTotal
'	                            mdDisAmt = CStr(dDiscountAmt)
'	                        End If
'	                    ElseIf cT = "P" Then
'	                        dDiscountAmt = (dDiscnt / 100) * dBefDiscTotal
'	                        mdDisAmt = CStr(dDiscountAmt)
'	                    ElseIf cT = "A" Then
'	                        dDiscnt = dDiscountAmt / dBefDiscTotal * 100
'	                        mdDiscnt = CStr(dDiscnt)
'	                    End If
'
'	                    dTotal = dBefDiscTotal - dDiscountAmt
'	                    mdSubTotal = CStr(dTotal)
'
'	                    dVatAmnt = dTotal * (dVatPerc / 100)
'	                    dTotal = dTotal + dVatAmnt
'
'	                    mdTaxAmt = dVatAmnt
'	                    mdTCTotal = dCustTotal)
'	                    mdTotal = CStr(dTotal)
'	                End If
'	            Catch ex As Exception
'	               com.idh.bridge.DataHandler.INSTANCE.doError("doCalcCustTotals", ex.ToString, "")
'	            End Try
'
'        	End If
'        End Sub

        Public Function doLookupTipCostPrices(ByVal oForm As SAPbouiCOM.Form) As com.idh.dbObjects.User.Prices
            'Dim sTipCode As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._Tip)
            'Dim sItemCode As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._ItemCd)
            'Dim sItemGrp As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._ItmGrp)
            'Dim sJobType As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._JobTp)
            Dim dWeight As Double = CType(mIDHForm.getDFValue(oForm, msTable, IDH_JOBSHD._TipWgt), Double)
            'Dim sWastCd As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._WasCd)
            'Dim sCustCd As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._CustCd)
            'Dim sAddress As String = mIDHForm.getWFValue(oForm, "STADDR")
            'Dim sZip As String = mIDHForm.getWFValue(oForm, "ZIPCODE")
            'Dim sPUOM As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._PUOM)
            'Dim sBranch As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._Branch)

            'Dim sDocDate As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._RDate)
            'Dim dDocDate As Date = com.idh.utils.dates.doStrToDate(sDocDate)

            Dim oPrices As com.idh.dbObjects.User.Prices
            'oPrices = moSIPT.doGetJobCostPrice("Waste Order", sJobType, sItemGrp, sItemCode, sTipCode, dWeight, sPUOM, sWastCd, sCustCd, sAddress, dDocDate, sBranch, sZip)
            oPrices = moSIPT.doGetJobCostPrice(msCIPWROrdType, msJobType, msItemGrp, msItemCode, msTipCode, dWeight, msPUOM, msWastCd, msCardCode, msSTADDR, msSupplierAddress, mdDocDate, msBranch, msZIPCODE)
            Return oPrices
        End Function

        Public Sub doGetTipCostPrices(ByVal bDoTPrice As Boolean)
        	If mbWasRead = False Then
        		Exit Sub
        	End If
        	
			If mdWrkWeight = 0 Then
				If bDoTPrice = True Then
					mdTipCost = 0
				End If
				
				mdTCostVtRt = 0
				msTCostVtGrp = ""
				msCCTCos = "VARIABLE"
				mdTCostOffset = 0
				
				setTipCostData(bDoTPrice)
                Exit Sub
			End If
			
            Try
'                Dim sItemCode As String = getItemCode()
'                Dim sItemGrp As String = getItemGrp()
'                Dim sJobType As String = getJobTp()
'                Dim sWastCd As String = getWasteCode()
'                Dim sCustCd As String = getCardCode()
'                Dim sAddress As String = getSTADDR()
'                Dim sPUOM As String = getPUOM()
'                Dim sBranch As String = getBranch()
'                Dim dDocDate As Date = getDocDate()
'                Dim sTipCode As String = getTipCode()

                If msTipCode.Length > 0 AndAlso msItemCode.Length > 0 Then
                    Dim oPrices As com.idh.dbObjects.User.Prices 'ArrayList
                    'oPrices = Config.INSTANCE.doGetJobCostPrice(msWROrdType, msJobType, msItemGrp, msItemCode, msTipCode, mdWrkWeight, msPUOM, msWastCd, msCardCode, msSTADDR, mdDocDate, msBranch, msZIPCODE)
                    oPrices = moSIPT.doGetJobCostPrice(msCIPWROrdType, msJobType, msItemGrp, msItemCode, msTipCode, mdWrkWeight, msPUOM, msWastCd, msCardCode, msSTADDR, msSupplierAddress, mdDocDate, msBranch, msZIPCODE)
			        If bDoTPrice = True Then
                        mdTipCost = oPrices.TipPrice 'aPrices(1))
			        End If
                    
                    mdTCostVtRt = oPrices.TipVat
                    msTCostVtGrp = oPrices.TipVatGroup
                    
                    msCCTCos = oPrices.TipChargeCalc
                    
                    'Offset Weight based on the ReadWeights UOM
                    'mdTCostOffset = Config.INSTANCE.doConvertWeights(oPrices.msUOM, msPUOM, oPrices.mdTipOffsetWeight)
                    mdTCostOffset = oPrices.TipOffsetWeight
				Else
                	If bDoTPrice = True Then
						mdTipCost = 0
					End If
					
					mdTCostVtRt = 0
					msTCostVtGrp = ""
					msCCTCos = "VARIABLE"
					mdTCostOffset = 0
                End If
                
                setTipCostData(bDoTPrice)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error retrieving the tipping cost prices - " & moForm.UniqueID)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRTCP", {moForm.UniqueID})
            End Try
        End Sub

        Public Function doLookupProducerCostPrices(ByVal oForm As SAPbouiCOM.Form) As com.idh.dbObjects.User.Prices
            'Dim sProdCode As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._ProCd)
            'Dim sItemCode As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._ItemCd)
            'Dim sItemGrp As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._ItmGrp)
            'Dim sJobType As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._JobTp)
            Dim dWeight As Double = CType(mIDHForm.getDFValue(oForm, msTable, IDH_JOBSHD._ProWgt), Double)
            'Dim sWastCd As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._WasCd)
            'Dim sCustCd As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._CustCd)
            'Dim sAddress As String = mIDHForm.getWFValue(oForm, "PSTADDR")
            'Dim sZip As String = mIDHForm.getWFValue(oForm, "PZIPCODE")
            'Dim sPUOM As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._ProUOM)
            'Dim sBranch As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._Branch)

            'Dim sDocDate As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._RDate)
            'Dim dDocDate As Date = com.idh.utils.dates.doStrToDate(sDocDate)

            Dim oPrices As com.idh.dbObjects.User.Prices
            'oPrices = moSIPP.doGetJobCostPrice("Waste Order", sJobType, sItemGrp, sItemCode, sProdCode, dWeight, sPUOM, sWastCd, sCustCd, sAddress, dDocDate, sBranch, sZip)
            oPrices = moSIPP.doGetJobCostPrice(msCIPWROrdType, msJobType, msItemGrp, msItemCode, msProCode, dWeight, msProUOM, msWastCd, msCardCode, msSTADDR, mdDocDate, msBranch, msZIPCODE)
            Return oPrices
        End Function

        Public Sub doGetProducerCostPrices(ByVal bDoPPrice As Boolean)
        	If mbWasRead = False Then
        		Exit Sub
        	End If
        	
			If mdWrkWeight = 0 Then
				If bDoPPrice = True Then
					mdPCost = 0
				End If
				
                mdPCostVtRt = 0
                msPCostVtGrp = ""
				msCCPCos = "VARIABLE"
				mdPCostOffset = 0
				
				setProducerCostData(bDoPPrice)
                Exit Sub
			End If
			
            Try
                If msProCode.Length > 0 AndAlso msItemCode.Length > 0 Then
                    Dim oPrices As com.idh.dbObjects.User.Prices 'ArrayList
                    'oPrices = Config.INSTANCE.doGetJobCostPrice(msWROrdType, msJobType, msItemGrp, msItemCode, msProCode, mdWrkWeight, msProUOM, msWastCd, msCardCode, msSTADDR, mdDocDate, msBranch, msZIPCODE)
                    oPrices = moSIPP.doGetJobCostPrice(msCIPWROrdType, msJobType, msItemGrp, msItemCode, msProCode, mdWrkWeight, msProUOM, msWastCd, msCardCode, msSTADDR, mdDocDate, msBranch, msZIPCODE)
			        If bDoPPrice = True Then
                        mdPCost = oPrices.TipPrice
			        End If
                    
                    mdPCostVtRt = oPrices.TipVat
                    msPCostVtGrp = oPrices.TipVatGroup
                    
                    msCCPCos = oPrices.TipChargeCalc
                    
                    'Offset Weight based on the ReadWeights UOM
                    'mdPCostOffset = Config.INSTANCE.doConvertWeights(oPrices.msUOM, msProUOM, oPrices.mdTipOffsetWeight)
                    mdPCostOffset = oPrices.TipOffsetWeight
				Else
                	If bDoPPrice = True Then
						mdPCost = 0
					End If
					
                    mdPCostVtRt = 0
                    msPCostVtGrp = ""
					msCCPCos = "VARIABLE"
					mdPCostOffset = 0
                End If
                
                setProducerCostData(bDoPPrice)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error retrieving the producer cost prices - " & moForm.UniqueID)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRPCP", {moForm.UniqueID})
            End Try
        End Sub

        Public Function doLookupHaulageCostPrices(ByVal oForm As SAPbouiCOM.Form) As com.idh.dbObjects.User.Prices
            'Dim sCarrierCode As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._CarrCd)
            'Dim sItemCode As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._ItemCd)
            'Dim sItemGrp As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._ItmGrp)
            'Dim sJobType As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._JobTp)
            Dim dWeight As Double = CType(mIDHForm.getDFValue(oForm, msTable, IDH_JOBSHD._OrdWgt), Double)
            'Dim sWastCd As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._WasCd)
            'Dim sCustCd As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._CustCd)
            'Dim sAddress As String = mIDHForm.getWFValue(oForm, "STADDR")
            'Dim sZip As String = mIDHForm.getWFValue(oForm, "ZIPCODE")
            'Dim sBranch As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._Branch)
            'Dim sPUOM As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._PUOM)

            'Dim sDocDate As String = mIDHForm.getFormDFValue(oForm, IDH_JOBSHD._RDate)
            'Dim dDocDate As Date = com.idh.utils.dates.doStrToDate(sDocDate)

            Dim oPrices As com.idh.dbObjects.User.Prices 'ArrayList
            'oPrices = moSIPH.doGetJobCostPrice("Waste Order", sJobType, sItemGrp, sItemCode, sCarrierCode, dWeight, sPUOM, sWastCd, sCustCd, sAddress, dDocDate, sBranch, sZip)
            oPrices = moSIPH.doGetJobCostPrice(msCIPWROrdType, msJobType, msItemGrp, msItemCode, msCarrCode, dWeight, msPUOM, msWastCd, msCardCode, msSTADDR, mdDocDate, msBranch, msZIPCODE)
            Return oPrices
        End Function

        Public Sub doGetHaulageCostPrices(ByVal bDoHPrice As Boolean)
        	If mbWasRead = False Then
        		Exit Sub
        	End If
        	
'			If mdOrdWgt = 0 Then
'					If bDoHPrice = True Then
'                    	mdOrdCost = 0
'                    End If
'
'                    mdHCostVtRt = 0
'                    msHCostVtGrp = ""
'
'                    msCCHCos = "STANDARD"
'                    setHaulageCostData(bDoHPrice)
'                Exit Sub
'			End If
			
            Try
                If mdOrdWgt = 0 Then
                    mdOrdWgt = 1
                End If

                If msCarrCode.Length > 0 AndAlso msItemCode.Length > 0 Then
                    Dim oPrices As com.idh.dbObjects.User.Prices 'ArrayList
                    'oPrices = Config.INSTANCE.doGetJobCostPrice(msWROrdType, msJobType, msItemGrp, msItemCode, msCarrCode, mdOrdWgt, msPUOM, msWastCd, msCardCode, msSTADDR, mdDocDate, msBranch, msZIPCODE)
                    oPrices = moSIPH.doGetJobCostPrice(msCIPWROrdType, msJobType, msItemGrp, msItemCode, msCarrCode, mdOrdWgt, msPUOM, msWastCd, msCardCode, msSTADDR, mdDocDate, msBranch, msZIPCODE)
				    ' 0 - HAULAGE COST
				    ' 1 - TIP COST
				    ' 2 - VAT
				    ' 3 - CHARGE CALC TYPE
				    ' 4 - OFFSET WEIGHT
				    ' 5 - BAND
				    ' 6 - UOM
				    If bDoHPrice = True Then
                        mdOrdCost = oPrices.HaulPrice 'aPrices(0))
                    End If
                    
                    mdHCostVtRt = oPrices.HaulageVat
                    msHCostVtGrp = oPrices.HaulageVatGroup
                    
                    msCCHCos = oPrices.HaulageChargeCalc 'aPrices(3))
				Else
                	If bDoHPrice = True Then
                    	mdOrdCost = 0
                    End If
                    
                    mdHCostVtRt = 0
                    msHCostVtGrp = ""

                    msCCHCos = "STANDARD"
                End If
                setHaulageCostData(bDoHPrice)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error retrieving the Haulage Cost price - " & moForm.UniqueID)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRHCP", {moForm.UniqueID})
            End Try
        End Sub
        
        Private Function doGetBandFactor() As Double
            Return Config.INSTANCE.doGetFactor(msZIPCODE, msCardCode, msItemCode, msItemGrp)
        End Function

        Public Function doCancelAddUpdatePrices(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Return True
        End Function

        'Public Function doAddUpdatePrices(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
        '    Dim oPForm As idh.forms.fr2.PriceUpdateOption = oOForm
        '    Dim sFromDate As String = oPForm.FromDate
        '    Dim sToDate As String = oPForm.ToDate
        '    Dim sAllSites As String = oPForm.AllSites
        '    Dim oForm As SAPbouiCOM.Form = oOForm.SBOParentForm

        '    If oPForm.PriceType = idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TChrg OrElse _
        '        oPForm.PriceType = idh.forms.fr2.PriceUpdateOption.en_PriceTypes.HChrg OrElse _
        '        oPForm.PriceType = idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TUOM Then

        '        Dim dTPrice As Double = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._TCharge)
        '        Dim dHPrice As Double = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._CusChr)
        '        Dim sUOM As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._UOM)
        '        If moCIP.Count > 0 AndAlso Not moCIP.Price Is Nothing AndAlso moCIP.Price.mbFound Then
        '            If sAllSites = "Y" Then
        '                moCIP.U_StAddr = ""
        '            End If

        '            If sFromDate.Length > 0 Then
        '                moCIP.U_StDate = com.idh.utils.dates.doStrToDate(sFromDate)
        '            End If

        '            If sToDate.Length > 0 Then
        '                moCIP.U_EnDate = com.idh.utils.dates.doStrToDate(sToDate)
        '            End If

        '            If oPForm.PriceType = idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TChrg Then
        '                moCIP.U_TipTon = dTPrice
        '            ElseIf oPForm.PriceType = idh.forms.fr2.PriceUpdateOption.en_PriceTypes.HChrg Then
        '                moCIP.U_Haulge = dHPrice
        '            ElseIf oPForm.PriceType = idh.forms.fr2.PriceUpdateOption.en_PriceTypes.TUOM Then
        '                moCIP.U_UOM = sUOM
        '            End If
        '            moCIP.doUpdateData()

        '            moCIP.doRerunLastPriceLookup()
        '        Else
        '            moCIP.doAddEmptyRow(True, True, False)

        '            Dim sCardCode As String = mIDHForm.getDFValue(moForm, msHeadTable, IDH_DISPORD._CardCd)
        '            Dim sCardName As String = mIDHForm.getDFValue(moForm, msHeadTable, IDH_DISPORD._CardNM)
        '            Dim sItemCode As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._ItemCd)
        '            Dim sItemName As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._ItemDsc)
        '            Dim sItemGrp As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._ItmGrp)
        '            Dim sWastCd As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._WasCd)
        '            Dim sWastName As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._WasDsc)
        '            Dim sBranch As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._Branch)
        '            Dim sJobType As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._JobTp)
        '            Dim sDocDate As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._RDate)

        '            Dim dDocDate As Date = com.idh.utils.dates.doStrToDate(sDocDate)
        '            Dim dFromDate As Date = com.idh.utils.dates.doStrToDate(sFromDate)
        '            Dim dToDate As Date = com.idh.utils.dates.doStrToDate(sToDate)

        '            Dim sAddress As String = mIDHForm.getDFValue(moForm, msHeadTable, IDH_DISPORD._Address)
        '            Dim sZip As String = mIDHForm.getDFValue(moForm, msHeadTable, IDH_DISPORD._ZpCd)

        '            moCIP.doApplyDefaults()

        '            moCIP.Code = ""
        '            moCIP.Name = ""

        '            moCIP.U_CustCd = sCardCode
        '            moCIP.U_ItemCd = sItemCode
        '            moCIP.U_ItemDs = sItemName

        '            moCIP.U_StDate = dFromDate
        '            moCIP.U_EnDate = dToDate

        '            'moCIP.U_ItmPLs = ""
        '            'moCIP.U_ItemPr = ""
        '            'moCIP.U_ItemTPr = ""

        '            'USE DEFAULTS
        '            'moCIP.U_FTon = 0
        '            'moCIP.U_TTon = 500
        '            'moCIP.U_ChrgCal =
        '            'moCIP.U_AItmPr =
        '            'moCIP.U_AItmCd =
        '            'moCIP.U_AItmDs =
        '            'moCIP.U_FrmBsWe =
        '            'moCIP.U_ToBsWe =
        '            'moCIP.U_BP2CD =
        '            'moCIP.U_HaulCal =
        '            'moCIP.U_AAddExp =

        '            moCIP.U_TipTon = dTPrice
        '            moCIP.U_Haulge = dHPrice

        '            moCIP.U_WasteCd = sWastCd
        '            moCIP.U_WasteDs = sWastName

        '            If sAllSites = "Y" Then
        '                moCIP.U_StAddr = ""
        '            Else
        '                moCIP.U_StAddr = sAddress
        '            End If

        '            moCIP.U_JobTp = sJobType
        '            moCIP.U_WR1ORD = "DO"

        '            moCIP.U_UOM = sUOM
        '            moCIP.U_Branch = sBranch
        '            moCIP.U_ZpCd = sZip

        '            moCIP.doAddData()

        '            doGetChargePrices(False, False)
        '        End If

        '    ElseIf oPForm.PriceType = idh.forms.fr2.PriceUpdateOption.en_PriceTypes.DispCst OrElse _
        '        oPForm.PriceType = idh.forms.fr2.PriceUpdateOption.en_PriceTypes.PUOM Then
        '        Dim dTPrice As Double = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._TipCost)
        '        Dim sUOM As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._PUOM)

        '        If moSIPT.Count > 0 AndAlso Not moSIPT.Price Is Nothing AndAlso moSIPT.Price.mbFound Then
        '            If sAllSites = "Y" Then
        '                moSIPT.U_StAddr = ""
        '            End If

        '            If sFromDate.Length > 0 Then
        '                moSIPT.U_StDate = com.idh.utils.dates.doStrToDate(sFromDate)
        '            End If

        '            If sToDate.Length > 0 Then
        '                moSIPT.U_EnDate = com.idh.utils.dates.doStrToDate(sToDate)
        '            End If

        '            If oPForm.PriceType = idh.forms.fr2.PriceUpdateOption.en_PriceTypes.DispCst Then
        '                moSIPT.U_TipTon = dTPrice
        '            Else
        '                moSIPT.U_UOM = sUOM
        '            End If
        '            moSIPT.doUpdateData()
        '        Else
        '            Dim sSuppCd As String = mIDHForm.getDFValue(moForm, msHeadTable, IDH_DISPORD._SCardCd)
        '            doUpdateSIPDataBuffer(oOForm.SBOParentForm, sSuppCd, _
        '                                 sFromDate, sToDate, sAllSites, _
        '                                 moSIPT, dTPrice, 0, sUOM)
        '        End If
        '    ElseIf oPForm.PriceType = idh.forms.fr2.PriceUpdateOption.en_PriceTypes.HaulCst Then
        '        Dim dHPrice As Double = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._OrdCost)
        '        Dim sUOM As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._PUOM)

        '        If moSIPH.Count > 0 AndAlso Not moSIPH.Price Is Nothing AndAlso moSIPH.Price.mbFound Then
        '            If sAllSites = "Y" Then
        '                moSIPH.U_StAddr = ""
        '            End If

        '            If sFromDate.Length > 0 Then
        '                moSIPH.U_StDate = com.idh.utils.dates.doStrToDate(sFromDate)
        '            End If

        '            If sToDate.Length > 0 Then
        '                moSIPH.U_EnDate = com.idh.utils.dates.doStrToDate(sToDate)
        '            End If

        '            moSIPH.U_TipTon = dHPrice
        '            moSIPH.doUpdateData()
        '        Else
        '            Dim sSuppCd As String = mIDHForm.getDFValue(moForm, msHeadTable, IDH_DISPORD._CCardCd)
        '            doUpdateSIPDataBuffer(oOForm.SBOParentForm, sSuppCd, _
        '                                 sFromDate, sToDate, sAllSites, _
        '                                 moSIPH, 0, dHPrice, sUOM)
        '        End If
        '    ElseIf oPForm.PriceType = idh.forms.fr2.PriceUpdateOption.en_PriceTypes.ProCst OrElse _
        '        oPForm.PriceType = idh.forms.fr2.PriceUpdateOption.en_PriceTypes.PRUOM Then
        '        Dim dPPrice As Double = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._PCost)
        '        Dim sUOM As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._ProUOM)

        '        If moSIPP.Count > 0 AndAlso Not moSIPP.Price Is Nothing AndAlso moSIPP.Price.mbFound Then
        '            If sAllSites = "Y" Then
        '                moSIPP.U_StAddr = ""
        '            End If

        '            If sFromDate.Length > 0 Then
        '                moSIPP.U_StDate = com.idh.utils.dates.doStrToDate(sFromDate)
        '            End If

        '            If sToDate.Length > 0 Then
        '                moSIPP.U_EnDate = com.idh.utils.dates.doStrToDate(sToDate)
        '            End If

        '            If oPForm.PriceType = idh.forms.fr2.PriceUpdateOption.en_PriceTypes.ProCst Then
        '                moSIPP.U_TipTon = dPPrice
        '            ElseIf oPForm.PriceType = idh.forms.fr2.PriceUpdateOption.en_PriceTypes.PRUOM Then
        '                moSIPP.U_UOM = sUOM
        '            End If

        '            moSIPP.doUpdateData()
        '        Else
        '            Dim sSuppCd As String = mIDHForm.getDFValue(moForm, msHeadTable, IDH_DISPORD._PCardCd)
        '            doUpdateSIPDataBuffer(oOForm.SBOParentForm, sSuppCd, _
        '                                 sFromDate, sToDate, sAllSites, _
        '                                 moSIPP, dPPrice, 0, sUOM)
        '        End If
        '    End If
        '    Return True
        'End Function

        'Private Sub doUpdateSIPDataBuffer(ByVal oForm As SAPbouiCOM.Form, _
        '                                 ByVal sSuppCd As String, _
        '                                 ByVal sFromDate As String, ByVal sToDate As String, ByVal sAllSites As String, _
        '                                 ByVal oSIP As IDH_SUITPR, ByVal dTPrice As Double, ByVal dHPrice As Double, _
        '                                 ByVal sUOM As String)

        '    oSIP.doAddEmptyRow(True, True, False)

        '    Dim sCustCd As String = mIDHForm.getDFValue(moForm, msHeadTable, IDH_DISPORD._CardCd)
        '    Dim sCardName As String = mIDHForm.getDFValue(moForm, msTable, IDH_DISPORD._CardNM)
        '    Dim sItemCode As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._ItemCd)
        '    Dim sItemName As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._ItemDsc)
        '    Dim sItemGrp As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._ItmGrp)
        '    Dim sWastCd As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._WasCd)
        '    Dim sWastName As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._WasDsc)
        '    Dim sBranch As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._Branch)
        '    Dim sJobType As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._JobTp)
        '    Dim sDocDate As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._RDate)
        '    'Dim sUOM As String = mIDHForm.getDFValue(moForm, msTable, IDH_JOBSHD._PUOM)

        '    Dim dDocDate As Date = com.idh.utils.dates.doStrToDate(sDocDate)
        '    Dim dFromDate As Date = com.idh.utils.dates.doStrToDate(sFromDate)
        '    Dim dToDate As Date = com.idh.utils.dates.doStrToDate(sToDate)

        '    Dim sAddress As String = mIDHForm.getDFValue(moForm, msHeadTable, IDH_DISPORD._Address)
        '    Dim sZip As String = mIDHForm.getDFValue(moForm, msHeadTable, IDH_DISPORD._ZpCd)

        '    oSIP.doApplyDefaults()

        '    oSIP.Code = ""
        '    oSIP.Name = ""

        '    oSIP.U_CardCd = sSuppCd
        '    oSIP.U_BP2CD = sCustCd
        '    oSIP.U_ItemCd = sItemCode
        '    oSIP.U_ItemDs = sItemName

        '    oSIP.U_StDate = dFromDate
        '    oSIP.U_EnDate = dToDate

        '    'oSIP.U_ItmPLs = ""
        '    'oSIP.U_ItemPr = ""
        '    'oSIP.U_ItemTPr = ""

        '    'USE DEFAULTS
        '    'oSIP.U_FTon = 0
        '    'oSIP.U_TTon = 500
        '    'oSIP.U_ChrgCal =
        '    'oSIP.U_AItmPr =
        '    'oSIP.U_AItmCd =
        '    'oSIP.U_AItmDs =
        '    'oSIP.U_FrmBsWe =
        '    'oSIP.U_ToBsWe =
        '    'oSIP.U_BP2CD =
        '    'oSIP.U_HaulCal =
        '    'oSIP.U_AAddExp =

        '    oSIP.U_TipTon = dTPrice
        '    oSIP.U_Haulge = dHPrice

        '    oSIP.U_WasteCd = sWastCd
        '    oSIP.U_WasteDs = sWastName

        '    If sAllSites = "Y" Then
        '        oSIP.U_StAddr = ""
        '    Else
        '        oSIP.U_StAddr = sAddress
        '    End If

        '    oSIP.U_JobTp = sJobType
        '    oSIP.U_WR1ORD = "DO"

        '    oSIP.U_UOM = sUOM
        '    oSIP.U_Branch = sBranch
        '    oSIP.U_ZpCd = sZip

        '    oSIP.doAddData()
        'End Sub

	End Class
End Namespace
