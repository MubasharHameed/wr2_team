﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Namespace idh.forms.TFS
    Public Class PackagingType
        Inherits IDHAddOns.idh.forms.Base
        'OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
        'Joachim Alleritz
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHPKGTYP", Nothing, -1, "PackagingType.srf", False, True, False, "Packaging Type", load_Types.idh_LOAD_NORMAL)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                doAddUF(oForm, "IDHPKG1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHPKG2", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHPKG3", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHPKG4", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHPKG5", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHPKG6", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHPKG7", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHPKG8", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHOTH", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHOTHTXT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 250, False, False)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            MyBase.doBeforeLoadData(oForm)
            'Get shared values of form opened as Modal Form 
            If getHasSharedData(oForm) Then
                Dim sCurrentValue As String = getParentSharedData(oForm, "CURVALUE")
                If sCurrentValue IsNot Nothing AndAlso sCurrentValue.Length > 0 Then
                    Dim aChkValue() As String = sCurrentValue.Split(",")
                    For Each sVal As String In aChkValue
                        If sVal.Trim() = "1 - Drum" Then
                            setUFValue(oForm, "IDHPKG1", "Y")
                        ElseIf sVal.Trim() = "2 - Wooden barrel" Then
                            setUFValue(oForm, "IDHPKG2", "Y")
                        ElseIf sVal.Trim() = "3 - Jerrican" Then
                            setUFValue(oForm, "IDHPKG3", "Y")
                        ElseIf sVal.Trim() = "4 - Box" Then
                            setUFValue(oForm, "IDHPKG4", "Y")
                        ElseIf sVal.Trim() = "5 - Bag" Then
                            setUFValue(oForm, "IDHPKG5", "Y")
                        ElseIf sVal.Trim() = "6 - Composite packaging" Then
                            setUFValue(oForm, "IDHPKG6", "Y")
                        ElseIf sVal.Trim() = "7 - Pressure receptacle" Then
                            setUFValue(oForm, "IDHPKG7", "Y")
                        ElseIf sVal.Trim() = "8 - Bulk" Then
                            setUFValue(oForm, "IDHPKG8", "Y")
                        Else
                            setUFValue(oForm, "IDHOTH", "Y")
                            'setEnableItem(oForm, True, "IDHOTHTXT")
                            'setUFValue(oForm, "IDHOTHTXT", sVal.Trim())
                            Dim sWithoutOther As String = sVal.Substring(sVal.IndexOf("(") + 1, sVal.IndexOf(")") - sVal.IndexOf("(") - 1)
                            setUFValue(oForm, "IDHOTHTXT", sWithoutOther.Trim())

                        End If
                    Next
                End If
            End If

            oForm.Freeze(False)
        End Sub

        '*** Get the selected fields
        Protected Function doPrepareModalData(ByVal oForm As SAPbouiCOM.Form, Optional ByVal iSel As Integer = -1) As Boolean
            Dim sPkg1 As String = getUFValue(oForm, "IDHPKG1")
            Dim sPkg2 As String = getUFValue(oForm, "IDHPKG2")
            Dim sPkg3 As String = getUFValue(oForm, "IDHPKG3")
            Dim sPkg4 As String = getUFValue(oForm, "IDHPKG4")
            Dim sPkg5 As String = getUFValue(oForm, "IDHPKG5")
            Dim sPkg6 As String = getUFValue(oForm, "IDHPKG6")
            Dim sPkg7 As String = getUFValue(oForm, "IDHPKG7")
            Dim sPkg8 As String = getUFValue(oForm, "IDHPKG8")
            Dim sOTH As String = getUFValue(oForm, "IDHOTH")
            Dim sOTHTXT As String = getUFValue(oForm, "IDHOTHTXT")

            Dim sField As String = ""

            If sPkg1.Length <> 0 Then
                If sPkg1 = "Y" Then
                    sField = "1 - Drum"
                End If
            End If

            If sPkg2.Length <> 0 Then
                If sPkg2 = "Y" Then
                    If sField.Length <> 0 Then
                        sField = sField + ", "
                    End If
                    sField = sField + "2 - Wooden barrel"
                End If
            End If

            If sPkg3.Length <> 0 Then
                If sPkg3 = "Y" Then
                    If sField.Length <> 0 Then
                        sField = sField + ", "
                    End If
                    sField = sField + "3 - Jerrican"
                End If
            End If

            If sPkg4.Length <> 0 Then
                If sPkg4 = "Y" Then
                    If sField.Length <> 0 Then
                        sField = sField + ", "
                    End If
                    sField = sField + "4 - Box"
                End If
            End If

            If sPkg5.Length <> 0 Then
                If sPkg5 = "Y" Then
                    If sField.Length <> 0 Then
                        sField = sField + ", "
                    End If
                    sField = sField + "5 - Bag"
                End If
            End If

            If sPkg6.Length <> 0 Then
                If sPkg6 = "Y" Then
                    If sField.Length <> 0 Then
                        sField = sField + ", "
                    End If
                    sField = sField + "6 - Composite packaging"
                End If
            End If

            If sPkg7.Length <> 0 Then
                If sPkg7 = "Y" Then
                    If sField.Length <> 0 Then
                        sField = sField + ", "
                    End If
                    sField = sField + "7 - Pressure receptacle"
                End If
            End If

            If sPkg8.Length <> 0 Then
                If sPkg8 = "Y" Then
                    If sField.Length <> 0 Then
                        sField = sField + ", "
                    End If
                    sField = sField + "8 - Bulk"
                End If
            End If

            If sOTH.Length <> 0 Then
                If sOTH = "Y" AndAlso sOTHTXT.Length > 0 Then
                    If sField.Length <> 0 Then
                        sField = sField + ", "
                    End If
                    sField = sField + "9 - Other (" + sOTHTXT + ")"
                End If
            End If

            If sField.Length = 0 Then
                setParentSharedData(oForm, "IDHPKGTYP", "")
                Return False
            Else
                setParentSharedData(oForm, "IDHPKGTYP", sField)
                doReturnFromModalShared(oForm, True)
                Return True
            End If
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If goParent.doCheckModal(oForm.UniqueID) = True Then
                    If doPrepareModalData(oForm) = False Then
                        BubbleEvent = False
                    End If
                End If
            End If
        End Sub

        '** The ItemEvent handler
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            'If pVal.EventType = SAPbouiCOM.BoEventTypes.et_CLICK Then
            '    If pVal.BeforeAction = False Then
            '        If pVal.ItemUID = "IDHOTH" Then
            '            Dim sOTH As String = getUFValue(oForm, "IDHOTH")
            '            Dim sOTHTXT As String = getUFValue(oForm, "IDHOTHTXT")
            '            If sOTH.Length <> 0 Then
            '                If sOTH = "Y" Then
            '                    doSetEnabled(oForm.Items.Item("IDHOTHTXT"), False)
            '                Else
            '                    doSetEnabled(oForm.Items.Item("IDHOTHTXT"), True)
            '                End If
            '            Else
            '                doSetEnabled(oForm.Items.Item("IDHOTHTXT"), True)
            '            End If

            '        End If
            '    End If
            'End If
            Return True
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        End Sub

        Protected Overrides Sub doReturnCanceled(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String)
        End Sub

        Public Overrides Sub doClose()
        End Sub
    End Class
End Namespace
