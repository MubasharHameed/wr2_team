﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Namespace idh.forms.TFS
    Public Class TransportationMode
        Inherits IDHAddOns.idh.forms.Base
        'OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
        'Joachim Alleritz
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHTRNMOD", Nothing, -1, "TransportationMode.srf", False, True, False, "Transportation Mode", load_Types.idh_LOAD_NORMAL)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                'doAddUF(oForm, "IDHTM1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                'doAddUF(oForm, "IDHTM2", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                'doAddUF(oForm, "IDHTM3", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                'doAddUF(oForm, "IDHTM4", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                'doAddUF(oForm, "IDHTM5", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                'doAddUF(oForm, "IDHOTH", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHOTHTXT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 250, False, False)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            MyBase.doBeforeLoadData(oForm)
            'Get shared values of form opened as Modal Form 
            If getHasSharedData(oForm) Then
                Dim sCurrentValue As String = getParentSharedData(oForm, "CURVALUE")
                If sCurrentValue IsNot Nothing AndAlso sCurrentValue.Length > 0 Then
                    setUFValue(oForm, "IDHOTHTXT", sCurrentValue)
                    'Dim aChkValue() As String = sCurrentValue.Split(",")
                    'For Each sVal As String In aChkValue
                    '    If sVal = "Road (R)" Then
                    '        setUFValue(oForm, "IDHTM1", "Y")
                    '    ElseIf sVal = "Train/Rail (T)" Then
                    '        setUFValue(oForm, "IDHTM2", "Y")
                    '    ElseIf sVal = "Sea (S)" Then
                    '        setUFValue(oForm, "IDHTM3", "Y")
                    '    ElseIf sVal = "Air (A)" Then
                    '        setUFValue(oForm, "IDHTM4", "Y")
                    '    ElseIf sVal = "Inland waterways (W)" Then
                    '        setUFValue(oForm, "IDHTM5", "Y")
                    '    Else
                    '        setUFValue(oForm, "IDHOTH", "Y")
                    '        setEnableItem(oForm, True, "IDHOTHTXT")
                    '        setUFValue(oForm, "IDHOTHTXT", sVal)
                    '    End If
                    'Next
                End If
            End If

            oForm.Freeze(False)
        End Sub

        '*** Get the selected fields
        Protected Function doPrepareModalData(ByVal oForm As SAPbouiCOM.Form, Optional ByVal iSel As Integer = -1) As Boolean
            'Dim sTrnMd1 As String = getUFValue(oForm, "IDHTM1")
            'Dim sTrnMd2 As String = getUFValue(oForm, "IDHTM2")
            'Dim sTrnMd3 As String = getUFValue(oForm, "IDHTM3")
            'Dim sTrnMd4 As String = getUFValue(oForm, "IDHTM4")
            'Dim sTrnMd5 As String = getUFValue(oForm, "IDHTM5")
            'Dim sOTH As String = getUFValue(oForm, "IDHOTH")
            Dim sOTHTXT As String = getUFValue(oForm, "IDHOTHTXT")

            'Dim sField As String = ""

            'If sTrnMd1.Length <> 0 Then
            '    If sTrnMd1 = "Y" Then
            '        sField = "Road (R)"
            '    End If
            'End If

            'If sTrnMd2.Length <> 0 Then
            '    If sTrnMd2 = "Y" Then
            '        If sField.Length <> 0 Then
            '            sField = sField + ","
            '        End If
            '        sField = sField + "Train/Rail (T)"
            '    End If
            'End If

            'If sTrnMd3.Length <> 0 Then
            '    If sTrnMd3 = "Y" Then
            '        If sField.Length <> 0 Then
            '            sField = sField + ","
            '        End If
            '        sField = sField + "Sea (S)"
            '    End If
            'End If

            'If sTrnMd4.Length <> 0 Then
            '    If sTrnMd4 = "Y" Then
            '        If sField.Length <> 0 Then
            '            sField = sField + ","
            '        End If
            '        sField = sField + "Air (A)"
            '    End If
            'End If

            'If sTrnMd5.Length <> 0 Then
            '    If sTrnMd5 = "Y" Then
            '        If sField.Length <> 0 Then
            '            sField = sField + ","
            '        End If
            '        sField = sField + "Inland waterways (W)"
            '    End If
            'End If

            'If sOTH.Length <> 0 Then
            '    If sOTH = "Y" AndAlso sOTHTXT.Length > 0 Then
            '        If sField.Length <> 0 Then
            '            sField = sField + ","
            '        End If
            '        sField = sField + sOTHTXT
            '    End If
            'End If

            If sOTHTXT.Length = 0 Then
                setParentSharedData(oForm, "IDHTRNMD", "")
            Else
                setParentSharedData(oForm, "IDHTRNMD", sOTHTXT)
                doReturnFromModalShared(oForm, True)
            End If
            Return True
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If goParent.doCheckModal(oForm.UniqueID) = True Then
                    If doPrepareModalData(oForm) = False Then
                        BubbleEvent = False
                    End If
                End If
            End If
        End Sub

        '** The ItemEvent handler
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_CLICK Then
                If pVal.BeforeAction = False Then
                    'If pVal.ItemUID = "IDHOTH" Then
                    '    Dim sOTH As String = getUFValue(oForm, "IDHOTH")
                    '    Dim sOTHTXT As String = getUFValue(oForm, "IDHOTHTXT")
                    '    If sOTH.Length <> 0 Then
                    '        If sOTH = "Y" Then
                    '            doSetEnabled(oForm.Items.Item("IDHOTHTXT"), False)
                    '        Else
                    '            doSetEnabled(oForm.Items.Item("IDHOTHTXT"), True)
                    '        End If
                    '    Else
                    '        doSetEnabled(oForm.Items.Item("IDHOTHTXT"), True)
                    '    End If

                    'End If
                    Dim sOTHTXT As String = getUFValue(oForm, "IDHOTHTXT")
                    If sOTHTXT.Length > 0 AndAlso
                        (pVal.ItemUID = "btnRoad" OrElse pVal.ItemUID = "btnTrain" OrElse pVal.ItemUID = "btnSea" OrElse pVal.ItemUID = "btnAir" OrElse pVal.ItemUID = "btnInland") Then
                        sOTHTXT = sOTHTXT + " - "
                    End If

                    If pVal.ItemUID = "btnRoad" Then
                        sOTHTXT = sOTHTXT + "Road (R)"
                        setUFValue(oForm, "IDHOTHTXT", sOTHTXT)
                    ElseIf pVal.ItemUID = "btnTrain" Then
                        sOTHTXT = sOTHTXT + "Train/Rail (T)"
                        setUFValue(oForm, "IDHOTHTXT", sOTHTXT)
                    ElseIf pVal.ItemUID = "btnSea" Then
                        sOTHTXT = sOTHTXT + "Sea (S)"
                        setUFValue(oForm, "IDHOTHTXT", sOTHTXT)
                    ElseIf pVal.ItemUID = "btnAir" Then
                        sOTHTXT = sOTHTXT + "Air (A)"
                        setUFValue(oForm, "IDHOTHTXT", sOTHTXT)
                    ElseIf pVal.ItemUID = "btnInland" Then
                        sOTHTXT = sOTHTXT + "Inland waterways (W)"
                        setUFValue(oForm, "IDHOTHTXT", sOTHTXT)
                    ElseIf pVal.ItemUID = "btnClear" Then
                        sOTHTXT = String.Empty
                        setUFValue(oForm, "IDHOTHTXT", sOTHTXT)
                    End If

                End If
            End If
            Return True
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        End Sub

        Protected Overrides Sub doReturnCanceled(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String)
        End Sub

        Public Overrides Sub doClose()
        End Sub
    End Class
End Namespace
