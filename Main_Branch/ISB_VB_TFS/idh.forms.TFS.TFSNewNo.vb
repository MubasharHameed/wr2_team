Imports System.IO
Imports System.Collections
Imports System

'Needed for the Framework controls
Imports IDHAddOns.idh.controls
Imports com.idh.bridge.data
Imports com.idh.bridge
Imports com.idh.dbObjects.User
Imports com.idh.dbObjects.numbers

Namespace idh.forms.TFS
    Public Class TFSNewNo
        Inherits IDHAddOns.idh.forms.Base

        Dim sHeadTable As String = "@IDH_Master" 'will be assigned a value in the constructor

#Region "Initialization"

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParentMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_TFSNEWNo", sParentMenu, iMenuPosition, "TFS NewNumber.srf", False, True, False, "New TFS Number", load_Types.idh_LOAD_NORMAL)

            sHeadTable = "@IDH_TFSNUM"

            doEnableHistory(sHeadTable)

        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            oForm.Freeze(True)
            Try
                doAddFormDF(oForm, "IDH_TFSNo", "U_TFSNum")
                doAddFormDF(oForm, "IDH_TFSDes", "U_Desc")
                doAddFormDF(oForm, "IDH_TFSVFR", "U_ValidFrm")
                doAddFormDF(oForm, "IDH_TFSVTO", "U_ValidTo")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
                BubbleEvent = False
            End Try

            oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            oForm.Freeze(False)
        End Sub

#End Region

#Region "Loading Data"

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            'Using the Form's main table
            Dim sCode As String = getFormDFValue(oForm, "Code").ToString()

            If Not sCode Is Nothing AndAlso sCode.Length > 0 Then
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE

                Dim oCons As New SAPbouiCOM.Conditions
                Dim oCon As SAPbouiCOM.Condition

                oCon = oCons.Add
                oCon.Alias = "Code"
                oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                oCon.CondVal = sCode
                oForm.DataSources.DBDataSources.Item("@IDH_TFSNUM").Query(oCons)
            Else
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                doCreateNewEntry(oForm)
            End If

            ''If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
            ''    doCreateNewEntry(oForm)
            ''ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
            ''    doCreateFindEntry(oForm)
            ''End If
            ''sCode = "-999"

            'Try
            '    Dim oGAttach As UpdateGrid = UpdateGrid.getInstance(oForm, "ATHGRID")
            '    oGAttach.doReloadData()
            'Catch ex As Exception
            '    com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error reloading the Attachments Grid Data.")
            'Finally
            'End Try

        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)

            MyBase.doBeforeLoadData(oForm)
            'Dim owItem As SAPbouiCOM.Item
            'Dim ow2Item As SAPbouiCOM.Folder

            'owItem = oForm.Items.Item("TABDET") 'Console Tab
            'ow2Item = owItem.Specific
            'ow2Item.Select()
            'oForm.PaneLevel = 101

            ''Setting Form Tabs not to affect form mode 
            'oForm.Items.Item("TABDET").AffectsFormMode = False 'Detail 
            'oForm.Items.Item("TABATH").AffectsFormMode = False 'Attachment

            ''Loading Data/Column structure for Grids 
            ''Attachment Grid 
            'Dim oGAttach As UpdateGrid = UpdateGrid.getInstance(oForm, "ATHGRID")
            'If oGAttach Is Nothing Then
            '    oGAttach = New UpdateGrid(Me, oForm, "CONGRID")
            'End If
            'oGAttach.doSetSBOAutoReSize(False)
            'oGAttach.doAddGridTable(New GridTable("ATC1", Nothing, "AbsEntry", True), True)
            'oGAttach.setOrderValue("AbsEntry")
            'oGAttach.setRequiredFilter(getListConsoleRequiredStr())
            'oGAttach.doSetDoCount(False)
            'oGAttach.doSetBlankLine(False)
            'doSetListConsoleFields(oGAttach)

            ''Get shared values of form opened as Modal Form 
            'If getHasSharedData(oForm) Then
            '    'doCreateNewEntry(oForm)

            '    'oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            '    oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
            '    Dim sAnexCd As String = getParentSharedData(oForm, "ANEXCD")
            '    Dim sAnexNo As String = getParentSharedData(oForm, "ANEXNO")
            '    Dim sTFSCd As String = getParentSharedData(oForm, "TFSCD")
            '    Dim sTFSNo As String = getParentSharedData(oForm, "TFSNO")

            '    setFormDFValue(oForm, "Code", sAnexCd, True)
            '    setFormDFValue(oForm, "U_AnexNo", sAnexNo)
            '    setFormDFValue(oForm, "U_TFSCd", sTFSCd)
            '    setFormDFValue(oForm, "U_TFSNo", sTFSNo)
            'End If

            'oForm.Freeze(False)

        End Sub

#End Region

#Region "Unloading Data"

        Public Overrides Sub doClose()
        End Sub

#End Region

#Region "Event Handlers"

        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                If pVal.MenuUID = lookups.Base.NAV_ADD Then
                    oForm.Freeze(True)
                    Try
                        doLoadData(oForm)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Menu Event - " & oForm.UniqueID & "." & pVal.MenuUID)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try
                ElseIf pVal.MenuUID = lookups.Base.NAV_FIND OrElse _
                       pVal.MenuUID = lookups.Base.NAV_FIRST OrElse _
                       pVal.MenuUID = lookups.Base.NAV_LAST OrElse _
                       pVal.MenuUID = lookups.Base.NAV_NEXT OrElse _
                       pVal.MenuUID = lookups.Base.NAV_PREV Then
                    oForm.Freeze(True)
                    Try
                        doLoadData(oForm)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Menu Event - " & oForm.UniqueID & "." & pVal.MenuUID)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try
                End If
            End If
            Return True
        End Function

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim sItemID As String = pVal.ItemUID
            'Dim owItem As SAPbouiCOM.Item
            'Dim ow2Item As SAPbouiCOM.Folder

            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED And pVal.Before_Action = True Then
                'Select Case sItemID
                '    Case "TABDET"
                '        oForm.PaneLevel = 101
                '        owItem = oForm.Items.Item("TABDET") 'General Tab
                '        ow2Item = owItem.Specific
                '        ow2Item.Select()
                '    Case "TABATH"
                '        oForm.PaneLevel = 102
                'End Select
            End If

            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    'If sItemID = "IDH_BTNPRT" Then
                    '    ''
                    'ElseIf sItemID = "IDH_BTNBRW" Then
                    '    ''
                    'ElseIf sItemID = "IDH_BTNDIS" Then
                    '    ''
                    'End If
                End If
            End If
            Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then

                    'Validate TFS Number 
                    Dim sTFSNum As String = getFormDFValue(oForm, "U_TFSNum")

                    If IsValidTFSNumber(sTFSNum) Then
                        Dim sCode As String = getFormDFValue(oForm, "Code").ToString()
                        If sCode Is Nothing OrElse sCode.Length = 0 Then
                            Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, "TFSNUM")
                            sCode = oNumbers.CodeCode
                            setFormDFValue(oForm, "Code", sCode)
                        End If

                        If doUpdateHeader(oForm, True) Then
                            oForm.Update()

                            'Insert a blank TFSConsole 
                            doAddTSFConsole(sCode, getFormDFValue(oForm, "U_TFSNum"))

                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                            BubbleEvent = False
                        End If
                    Else
                        doWarnMess("TFS Number already exist!", SAPbouiCOM.BoMessageTime.bmt_Medium)
                    End If
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    Dim sTFSNum As String = getFormDFValue(oForm, "U_TFSNum")
                    Dim sCode As String = getFormDFValue(oForm, "Code").ToString()

                    If IsValidTFSNumber(sTFSNum, sCode) Then
                        If doUpdateHeader(oForm, False) Then
                            oForm.Update()
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                            BubbleEvent = False
                        End If
                    Else
                        doWarnMess("TFS Number already exist!", SAPbouiCOM.BoMessageTime.bmt_Medium)
                    End If
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    setSharedData(oForm, "NEWANX", "NEWANX")
                    'setSharedData(oParentForm, "ANXDESC1", "anx desc")
                    doReturnFromModalShared(oForm, True)

                End If
            Else
            End If

        End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            ''If sModalFormType.Equals("IDHCSRCH") Then
            ''    Dim sTrg As String = getSharedData(oForm, "TRG")
            ''    Dim saParam As String() = {sTrg}

            ''    Dim sCustCd As String = getSharedData(oForm, "CARDCODE")
            ''    Dim sCustName As String = getSharedData(oForm, "CARDNAME")

            ''    If sTrg = "IDH_NOTL" Then
            ''        If isValidBPTFSRegNo(oForm, sCustCd) Then
            ''            setFormDFValue(oForm, "U_NOTCd", sCustCd)
            ''            setFormDFValue(oForm, "U_NOTName", sCustName)
            ''            doSetNotifierAddress(oForm)
            ''        Else
            ''            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            ''        End If
            ''    ElseIf sTrg = "IDH_CONL" Then
            ''        If isValidBPTFSRegNo(oForm, sCustCd) Then
            ''            setFormDFValue(oForm, "U_CONCd", sCustCd)
            ''            setFormDFValue(oForm, "U_CONName", sCustName)
            ''            doSetConsignorAddress(oForm)
            ''        Else
            ''            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            ''        End If
            ''    ElseIf sTrg = "IDH_COEL" Then
            ''        If isValidBPTFSRegNo(oForm, sCustCd) Then
            ''            setFormDFValue(oForm, "U_COECd", sCustCd)
            ''            setFormDFValue(oForm, "U_COEName", sCustName)
            ''            doSetConsigneeAddress(oForm)
            ''        Else
            ''            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            ''        End If
            ''    ElseIf sTrg = "IDH_CARL" Then
            ''        If isValidBPTFSRegNo(oForm, sCustCd) Then
            ''            setFormDFValue(oForm, "U_CARCd", sCustCd)
            ''            setFormDFValue(oForm, "U_CARName", sCustName)
            ''            doSetCarrierAddress(oForm)
            ''        Else
            ''            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            ''        End If
            ''    ElseIf sTrg = "IDH_GENL" Then
            ''        If isValidBPTFSRegNo(oForm, sCustCd) Then
            ''            setFormDFValue(oForm, "U_GENCd", sCustCd)
            ''            setFormDFValue(oForm, "U_GENName", sCustName)
            ''            doSetGeneratorAddress(oForm)
            ''        Else
            ''            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            ''        End If
            ''    ElseIf sTrg = "IDH_DSPL" Then
            ''        If isValidBPTFSRegNo(oForm, sCustCd) Then
            ''            setFormDFValue(oForm, "U_DSPCd", sCustCd)
            ''            setFormDFValue(oForm, "U_DSPName", sCustName)
            ''            doSetDisposalFacilityAddress(oForm)
            ''        Else
            ''            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            ''        End If
            ''    ElseIf sTrg = "IDH_FNDL" Then
            ''        If isValidBPTFSRegNo(oForm, sCustCd) Then
            ''            setFormDFValue(oForm, "U_FDSCd", sCustCd)
            ''            setFormDFValue(oForm, "U_FDSName", sCustName)
            ''            doSetFinalDestinationAddress(oForm)
            ''        Else
            ''            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            ''        End If
            ''    End If
            ''ElseIf sModalFormType.Equals("IDHTFSNSR") Then
            ''    Dim sTrg As String = getSharedData(oForm, "TRG")
            ''    If sTrg = "IDH_TFSNL" Then
            ''        If isValidTFSNumber(getSharedData(oForm, "IDH_TFSNUM")) Then
            ''            setFormDFValue(oForm, "U_TFSNo", getSharedData(oForm, "IDH_TFSNUM"))
            ''        Else
            ''            Dim saParam As String() = {sTrg}
            ''            doResError("Selected TFS Number not valid! Please choose an active TFS Number.", "INVLTFSN", saParam)
            ''        End If
            ''    End If
            ''End If
            ''MyBase.doHandleModalResultShared(oForm, sModalFormType, sLastButton)
            'Dim oParentForm As SAPbouiCOM.Form = goParent.doGetParentForm(oForm.UniqueID)
            'setSharedData(oParentForm, "ANXCD1", "123")
            'setSharedData(oParentForm, "ANXDESC1", "anx desc")
            'doReturnFromModalShared(oForm, True)
            Dim oParentForm As SAPbouiCOM.Form = goParent.doGetParentForm(oForm.UniqueID)
            setSharedData(oParentForm, "NEWANX", getFormDFValue(oForm, "IDH_TFSNo"))
            'setSharedData(oParentForm, "ANXDESC1", "anx desc")
            doReturnFromModalShared(oForm, True)

        End Sub

        Protected Overrides Sub doHandleModalBufferedResult(ByVal oParentForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            'If oData Is Nothing Then Exit Sub
            'Dim oArr As ArrayList = oData
            'If oArr.Count = 0 Then
            '    Return
            'End If
            'setFormDFValue(oParentForm, "U_BoxNo", oArr(0))
            'MyBase.doHandleModalBufferedResult(oParentForm, oData, sModalFormType, sLastButton)
        End Sub

        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            'MyBase.doReadInputParams(oForm)
            'Dim sVal As String = getParentSharedData(oForm, "Share_ACd")

            'setFormDFValue(oForm, "U_BoxNo", sVal)

        End Sub

#End Region

#Region "Custom Functions"

        Protected Function doUpdateHeader(ByVal oForm As SAPbouiCOM.Form, ByVal bIsAdd As Boolean) As Boolean
            'This will now Automatically update all the current DB fields and
            'generate and set the Code value if the code was not set
            Return doAutoSave(oForm, Nothing, "TFSNUM")
        End Function

        Public Overridable Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form)
            Dim sCode As String = ""

            setFormDFValue(oForm, "Code", sCode)
            setFormDFValue(oForm, "Name", sCode)
            setFormDFValue(oForm, "U_TFSNUM", "")

        End Sub

        Public Overridable Sub doCreateFindEntry(ByVal oForm As SAPbouiCOM.Form)
            setFormDFValue(oForm, "Code", "", True)
            setFormDFValue(oForm, "Name", "")
            setFormDFValue(oForm, "U_TFSNUM", "")

        End Sub

        'Protected Overridable Sub doSetListConsoleFields(ByRef oGConsole As IDHAddOns.idh.controls.UpdateGrid)
        '    oGConsole.doAddListField("AbsEntry", "Code", False, 8, Nothing, Nothing)
        '    oGConsole.doAddListField("trgtPath", "Path", False, 8, Nothing, Nothing)
        '    oGConsole.doAddListField("FileName", "File Name", False, 20, Nothing, Nothing)
        '    oGConsole.doAddListField("Date", "Attachment Date", False, 20, Nothing, Nothing)
        'End Sub

        'Public Overridable Function getListConsoleRequiredStr() As String
        '    Dim oForm As SAPbouiCOM.Form
        '    Dim sField As String = "" '" [U_AnexNo] = '' "
        '    Return sField
        'End Function

        Protected Sub doSetMode(ByVal oForm As SAPbouiCOM.Form, ByVal iMode As SAPbouiCOM.BoFormMode)
            If iMode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                oForm.Items.Item("1").Specific.Caption = getTranslatedWord("Find")
            ElseIf iMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                oForm.Items.Item("1").Specific.Caption = getTranslatedWord("Update")
                doSetUpdate(oForm)
            ElseIf iMode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                oForm.Items.Item("1").Specific.Caption = getTranslatedWord("Ok")
            End If
        End Sub


        Public Function doAddTSFConsole(ByVal sCode As String, ByVal sTFSNumber As String) As Boolean
            'Dim sQry As String
            Dim oRecordset As SAPbobsCOM.Recordset
            oRecordset = goParent.goDB.doSelectQuery("insert into [@IDH_TFSMAST](Code, Name, U_TFSNum, U_Status) " + _
                                                     " values('" + sCode + "', '" + sCode + "', '" + sTFSNumber + "', '01')" _
                                                    )
            If oRecordset.RecordCount > 0 Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Function IsValidTFSNumber(ByVal sTFSNum As String) As Boolean
            Dim oRS As SAPbobsCOM.Recordset = goParent.goDB.doSelectQuery("select U_TFSNUM from [@IDH_TFSNUM] where U_TFSNum = '" + sTFSNum + "'")

            If oRS.RecordCount > 0 Then
                Return False
            Else
                Return True
            End If
        End Function

        Public Function IsValidTFSNumber(ByVal sTFSNum As String, ByVal sTFSCode As String) As Boolean
            Dim oRS As SAPbobsCOM.Recordset = goParent.goDB.doSelectQuery("select U_TFSNUM from [@IDH_TFSNUM] where U_TFSNum = '" + sTFSNum + "' AND Code = '" + sTFSCode + "'")

            If oRS.RecordCount > 0 Then
                Return False
            Else
                Return True
            End If
        End Function

#End Region

    End Class
End Namespace
