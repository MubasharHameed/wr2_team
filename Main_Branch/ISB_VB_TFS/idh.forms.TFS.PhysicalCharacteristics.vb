﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Namespace idh.forms.TFS
    Public Class PhysicalCharacteristics
        Inherits IDHAddOns.idh.forms.Base
        'OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
        'Joachim Alleritz
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDHPHYCHR", Nothing, -1, "PhysicalCharacteristics.srf", False, True, False, "Choose Physical Characteristics", load_Types.idh_LOAD_NORMAL)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                doAddUF(oForm, "IDHPOW", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHSOL", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHVIS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHSLU", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHLIQ", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHGAS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHOTH", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False)
                doAddUF(oForm, "IDHOTHTXT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 250, False, False)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            MyBase.doBeforeLoadData(oForm)
            'Get shared values of form opened as Modal Form 
            If getHasSharedData(oForm) Then
                Dim sCurrentValue As String = CType(getParentSharedData(oForm, "CURVALUE"), String)
                If sCurrentValue IsNot Nothing AndAlso sCurrentValue.Length > 0 Then
                    Dim aChkValue() As String = sCurrentValue.Split(","c)
                    For Each sVal As String In aChkValue
                        If sVal.Trim() = "1 - Powdery/powder" Then
                            setUFValue(oForm, "IDHPOW", "Y")
                        ElseIf sVal.Trim() = "2 - Solid" Then
                            setUFValue(oForm, "IDHSOL", "Y")
                        ElseIf sVal.Trim() = "3 - Viscous/paste" Then
                            setUFValue(oForm, "IDHVIS", "Y")
                        ElseIf sVal.Trim() = "4 - Sludgy" Then
                            setUFValue(oForm, "IDHSLU", "Y")
                        ElseIf sVal.Trim() = "5 - Liquid" Then
                            setUFValue(oForm, "IDHLIQ", "Y")
                        ElseIf sVal.Trim() = "6 - Gaseous" Then
                            setUFValue(oForm, "IDHGAS", "Y")
                        Else
                            setUFValue(oForm, "IDHOTH", "Y")
                            'setEnableItem(oForm, True, "IDHOTHTXT")
                            'setUFValue(oForm, "IDHOTHTXT", sVal.Trim())
                            Dim sWithoutOther As String = sVal.Substring(sVal.IndexOf("(") + 1, sVal.IndexOf(")") - sVal.IndexOf("(") - 1)
                            setUFValue(oForm, "IDHOTHTXT", sWithoutOther.Trim())
                        End If
                    Next
                End If
            End If

            oForm.Freeze(False)
        End Sub

        '*** Get the selected fields
        Protected Function doPrepareModalData(ByVal oForm As SAPbouiCOM.Form, Optional ByVal iSel As Integer = -1) As Boolean
            Dim sPOW As String = getUFValue(oForm, "IDHPOW")
            Dim sSOL As String = getUFValue(oForm, "IDHSOL")
            Dim sVIS As String = getUFValue(oForm, "IDHVIS")
            Dim sSLU As String = getUFValue(oForm, "IDHSLU")
            Dim sLIQ As String = getUFValue(oForm, "IDHLIQ")
            Dim sGAS As String = getUFValue(oForm, "IDHGAS")
            Dim sOTH As String = getUFValue(oForm, "IDHOTH")
            Dim sOTHTXT As String = getUFValue(oForm, "IDHOTHTXT")

            Dim sField As String = ""

            If sPOW.Length <> 0 Then
                If sPOW = "Y" Then
                    sField = "1 - Powdery/powder"
                End If
            End If

            If sSOL.Length <> 0 Then
                If sSOL = "Y" Then
                    If sField.Length <> 0 Then
                        sField = sField + ", "
                    End If
                    sField = sField + "2 - Solid"
                End If
            End If

            If sVIS.Length <> 0 Then
                If sVIS = "Y" Then
                    If sField.Length <> 0 Then
                        sField = sField + ", "
                    End If
                    sField = sField + "3 - Viscous/paste"
                End If
            End If

            If sSLU.Length <> 0 Then
                If sSLU = "Y" Then
                    If sField.Length <> 0 Then
                        sField = sField + ", "
                    End If
                    sField = sField + "4 - Sludgy"
                End If
            End If

            If sLIQ.Length <> 0 Then
                If sLIQ = "Y" Then
                    If sField.Length <> 0 Then
                        sField = sField + ", "
                    End If
                    sField = sField + "5 - Liquid"
                End If
            End If

            If sGAS.Length <> 0 Then
                If sGAS = "Y" Then
                    If sField.Length <> 0 Then
                        sField = sField + ", "
                    End If
                    sField = sField + "6 - Gaseous"
                End If
            End If

            If sOTH.Length <> 0 Then
                If sOTH = "Y" AndAlso sOTHTXT.Length > 0 Then
                    If sField.Length <> 0 Then
                        sField = sField + ", "
                    End If
                    sField = sField + "7 - Other (" + sOTHTXT + ")"
                End If
            End If

            If sField.Length = 0 Then
                setParentSharedData(oForm, "IDHPHYCHR", "")
                Return False
            Else
                setParentSharedData(oForm, "IDHPHYCHR", sField)
                doReturnFromModalShared(oForm, True)
                Return True
            End If
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If goParent.doCheckModal(oForm.UniqueID) = True Then
                    If doPrepareModalData(oForm) = False Then
                        BubbleEvent = False
                    End If
                End If
            End If
        End Sub

        '** The ItemEvent handler
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            'If pVal.EventType = SAPbouiCOM.BoEventTypes.et_CLICK Then
            '    If pVal.BeforeAction = False Then
            '        If pVal.ItemUID = "IDHOTH" Then
            '            Dim sOTH As String = getUFValue(oForm, "IDHOTH")
            '            Dim sOTHTXT As String = getUFValue(oForm, "IDHOTHTXT")
            '            If sOTH.Length <> 0 Then
            '                If sOTH = "Y" Then
            '                    doSetEnabled(oForm.Items.Item("IDHOTHTXT"), False)
            '                Else
            '                    doSetEnabled(oForm.Items.Item("IDHOTHTXT"), True)
            '                End If
            '            Else
            '                doSetEnabled(oForm.Items.Item("IDHOTHTXT"), True)
            '            End If

            '        End If
            '    End If
            'End If
            Return True
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        End Sub

        Protected Overrides Sub doReturnCanceled(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String)
        End Sub

        Public Overrides Sub doClose()
        End Sub
    End Class
End Namespace
