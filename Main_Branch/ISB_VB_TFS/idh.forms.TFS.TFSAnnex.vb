Imports System.IO
Imports System.Collections
Imports System

'Needed for the Framework controls
Imports IDHAddOns.idh.controls
Imports com.idh.bridge.data
Imports com.idh.bridge
Imports com.idh.dbObjects.User
Imports IDHAddOns.idh.lookups.Base
Imports com.idh.dbObjects.numbers

Namespace idh.forms.TFS
    Public Class TFSAnnex
        Inherits IDHAddOns.idh.forms.Base

        Dim sHeadTable As String = "@IDH_Master" 'will be assigned a value in the constructor

#Region "Initialization"

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParentMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_TFSANX", sParentMenu, iMenuPosition, "TFS Annex.srf", False, True, False, "TFS Annex Builder", load_Types.idh_LOAD_NORMAL)

            sHeadTable = "@IDH_TFSANX"

            doEnableHistory(sHeadTable)

        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE)
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Dim oItem As SAPbouiCOM.Item
            'Dim oButton As SAPbouiCOM.Button
            Dim oFolder As SAPbouiCOM.Folder
            Dim oOptionBtn As SAPbouiCOM.OptionBtn
            'Dim oCheckBox As SAPbouiCOM.CheckBox
            'Dim oStatic As SAPbouiCOM.StaticText

            'Dim i As Integer

            oForm.Freeze(True)

            Try
                oForm.DataSources.UserDataSources.Add("FldrDSKA", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                oForm.DataSources.UserDataSources.Add("Fldr6DSKA", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)

                'Adding individual tabs instead of loop 
                'Details Tab
                oItem = oForm.Items.Add("TABDET", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
                oItem.Left = 5
                oItem.Width = 100
                oItem.Top = 65
                oItem.Height = 19
                oFolder = oItem.Specific
                oFolder.Caption = getTranslatedWord("Details")
                oFolder.DataBind.SetBound(True, "", "FldrDSKA")
                oFolder.Select()

                'Attachments Tab
                oItem = oForm.Items.Add("TABATH", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
                oItem.Left = 5 + 100
                oItem.Width = 100
                oItem.Top = 65
                oItem.Height = 19
                oFolder = oItem.Specific
                oFolder.Caption = getTranslatedWord("Attachments")
                oFolder.DataBind.SetBound(True, "", "FldrDSKA")
                oFolder.GroupWith("TABDET")

                'Adding outer rectangle 
                oItem = oForm.Items.Add("Rec1", SAPbouiCOM.BoFormItemTypes.it_RECTANGLE)
                oItem.Top = 85
                oItem.Left = 5
                oItem.Height = 265
                oItem.Width = 520

                'Adding Editable Option Buttons 
                Dim oUserdatasource As SAPbouiCOM.UserDataSource

                'Editable YES
                oItem = oForm.Items.Add("IDH_OPEDY", SAPbouiCOM.BoFormItemTypes.it_OPTION_BUTTON)
                oItem.Top = 35
                oItem.Left = 90
                oItem.Height = 14
                oItem.Width = 40
                oItem.FromPane = 0
                oItem.ToPane = 0
                oOptionBtn = oItem.Specific
                oOptionBtn.Caption = getTranslatedWord("Yes")

                oUserdatasource = oForm.DataSources.UserDataSources.Add("ds_MI", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                oOptionBtn.DataBind.SetBound(True, , "ds_MI")

                'Multiple Shipment
                oItem = oForm.Items.Add("IDH_OPEDN", SAPbouiCOM.BoFormItemTypes.it_OPTION_BUTTON)
                oItem.Top = 35
                oItem.Left = 135
                oItem.Height = 14
                oItem.Width = 40
                oItem.FromPane = 0
                oItem.ToPane = 0
                oOptionBtn = oItem.Specific
                oOptionBtn.Caption = getTranslatedWord("No")
                oOptionBtn.GroupWith("IDH_OPEDY")

                'Mapp fields with data fields 
                doAddFormDF(oForm, "IDH_ANEXCD", "Code")
                doAddFormDF(oForm, "IDH_ANEXNO", "U_AnexNo")
                doAddFormDF(oForm, "IDH_TFSCD", "U_TFSCd")
                doAddFormDF(oForm, "IDH_TFSNO", "U_TFSNo")
                doAddFormDF(oForm, "IDH_OPEDY", "U_Edtable")
                doAddFormDF(oForm, "IDH_OPEDN", "U_Edtable")
                doAddFormDF(oForm, "IDH_DETAIL", "U_Detail")
                doAddFormDF(oForm, "IDH_ATCENT", "U_AtcEntry")

                'Setting Form Tabs not to affect form mode 
                oForm.Items.Item("TABDET").AffectsFormMode = False 'Details 
                oForm.Items.Item("TABATH").AffectsFormMode = False 'Attachments

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
                BubbleEvent = False
            End Try

            setEnableItem(oForm, False, "IDH_ANEXCD")

            'Grid Section 

            'Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            'If oGridN Is Nothing Then
            '    oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
            'End If
            'oGridN.getSBOItem().AffectsFormMode = True

            Dim oGAttach As UpdateGrid = New UpdateGrid(Me, oForm, "ATHGRID", 15, 90, 500, 250)

            oGAttach.getSBOItem().AffectsFormMode = False
            oGAttach.getSBOItem().FromPane = 102
            oGAttach.getSBOItem().ToPane = 102

            'Form level settings 
            oForm.DataBrowser.BrowseBy = "IDH_ANEXCD"
            oForm.SupportedModes = -1
            oForm.AutoManaged = True

            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE

            oForm.Freeze(False)

            'MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

#End Region

#Region "Loading Data"

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            'Using the Form's main table
            Dim sCode As String = getFormDFValue(oForm, "Code").ToString()

            Dim sTFSCd As String = getFormDFValue(oForm, "U_TFSCd").ToString()
            Dim sTFSNm As String = getFormDFValue(oForm, "U_TFSNo").ToString()

            If Not sCode Is Nothing AndAlso sCode.Length > 0 Then
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE

                Dim oCons As New SAPbouiCOM.Conditions
                Dim oCon As SAPbouiCOM.Condition

                oCon = oCons.Add
                oCon.Alias = "Code"
                oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                oCon.CondVal = sCode
                oForm.DataSources.DBDataSources.Item("@IDH_TFSANX").Query(oCons)
            ElseIf sTFSCd IsNot Nothing AndAlso sTFSCd.Length > 0 Then
                'oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                'Try
                '    Dim oCons As New SAPbouiCOM.Conditions
                '    Dim oCon As SAPbouiCOM.Condition

                '    oCon = oCons.Add
                '    oCon.BracketOpenNum = 2
                '    oCon.Alias = "U_TFSCd"
                '    oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                '    oCon.CondVal = sTFSCd
                '    oCon.BracketCloseNum = 1
                '    oCon.Relationship = SAPbouiCOM.BoConditionRelationship.cr_AND

                '    oCon = oCons.Add
                '    oCon.BracketOpenNum = 1
                '    oCon.Alias = "U_TFSNo"
                '    oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                '    oCon.CondVal = sTFSNm
                '    oCon.BracketCloseNum = 2

                '    oForm.DataSources.DBDataSources.Item("@IDH_TFSANX").Query(oCons)
                'Catch ex As Exception
                '    com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error loading data.")
                'End Try
            Else
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                doCreateNewEntry(oForm)
            End If

            'If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
            '    doCreateNewEntry(oForm)
            'ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
            '    doCreateFindEntry(oForm)
            'End If
            'sCode = "-999"

            Try
                Dim sAttachmentCode As String = getFormDFValue(oForm, "U_AtcEntry").ToString()

                Dim oGAttach As UpdateGrid = UpdateGrid.getInstance(oForm, "ATHGRID")
                If sAttachmentCode.Length > 0 Then
                    oGAttach.setRequiredFilter(" AbsEntry = " + sAttachmentCode + " ")
                Else
                    oGAttach.setRequiredFilter(" AbsEntry = -1")
                End If
                oGAttach.doReloadData()
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error reloading the Attachments Grid Data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "ERECAGD", {com.idh.bridge.Translation.getTranslatedWord("Attachments")})
            Finally
            End Try

        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)

            MyBase.doBeforeLoadData(oForm)
            Dim owItem As SAPbouiCOM.Item
            Dim ow2Item As SAPbouiCOM.Folder

            owItem = oForm.Items.Item("TABDET") 'Console Tab
            ow2Item = owItem.Specific
            ow2Item.Select()
            oForm.PaneLevel = 101

            'Setting Form Tabs not to affect form mode 
            oForm.Items.Item("TABDET").AffectsFormMode = False 'Detail 
            oForm.Items.Item("TABATH").AffectsFormMode = False 'Attachment

            'Loading Data/Column structure for Grids 
            'Attachment Grid 
            Dim oGAttach As UpdateGrid = UpdateGrid.getInstance(oForm, "ATHGRID")
            If oGAttach Is Nothing Then
                oGAttach = New UpdateGrid(Me, oForm, "CONGRID")
            End If

            oGAttach.doSetSBOAutoReSize(False)
            oGAttach.doAddGridTable(New GridTable("ATC1", Nothing, "AbsEntry", True, True), True)
            oGAttach.setOrderValue("AbsEntry")
            oGAttach.setRequiredFilter(getListConsoleRequiredStr())
            oGAttach.doSetDoCount(False)
            oGAttach.doSetBlankLine(False)
            doSetListConsoleFields(oGAttach)

            'Get shared values of form opened as Modal Form 
            If getHasSharedData(oForm) Then
                'doCreateNewEntry(oForm)

                'oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                Dim sAnexCd As String = getParentSharedData(oForm, "ANEXCD")
                Dim sAnexNo As String = getParentSharedData(oForm, "ANEXNO")

                Dim sTFSCd As String = getParentSharedData(oForm, "TFSCODE")
                Dim sTFSNo As String = getParentSharedData(oForm, "TFSNUM")

                setFormDFValue(oForm, "Code", sAnexCd, True)
                setFormDFValue(oForm, "U_AnexNo", sAnexNo)
                setFormDFValue(oForm, "U_TFSCd", sTFSCd)
                setFormDFValue(oForm, "U_TFSNo", sTFSNo)
            End If

            oForm.Freeze(False)

        End Sub

#End Region

#Region "Unloading Data"

        Public Overrides Sub doClose()
        End Sub

#End Region

#Region "Event Handlers"

        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                If pVal.MenuUID = lookups.Base.NAV_ADD Then
                    oForm.Freeze(True)
                    Try
                        doLoadData(oForm)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Menu Event - " & oForm.UniqueID & "." & pVal.MenuUID)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try
                ElseIf pVal.MenuUID = lookups.Base.NAV_FIND OrElse _
                       pVal.MenuUID = lookups.Base.NAV_FIRST OrElse _
                       pVal.MenuUID = lookups.Base.NAV_LAST OrElse _
                       pVal.MenuUID = lookups.Base.NAV_NEXT OrElse _
                       pVal.MenuUID = lookups.Base.NAV_PREV Then
                    oForm.Freeze(True)
                    Try
                        doLoadData(oForm)
                    Catch ex As Exception
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try
                End If
            End If
            Return True
        End Function

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim sItemID As String = pVal.ItemUID
            Dim owItem As SAPbouiCOM.Item
            Dim ow2Item As SAPbouiCOM.Folder

            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED And pVal.Before_Action = True Then
                Select Case sItemID
                    Case "TABDET"
                        oForm.PaneLevel = 101
                        owItem = oForm.Items.Item("TABDET") 'General Tab
                        ow2Item = owItem.Specific
                        ow2Item.Select()
                    Case "TABATH"
                        oForm.PaneLevel = 102
                End Select
            End If

            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If sItemID = "IDH_BTNDEL" Then
                        Try
                            'Open file 
                            Dim oGAttach As UpdateGrid = UpdateGrid.getInstance(oForm, "ATHGRID")
                            Dim oSelected As SAPbouiCOM.SelectedRows = oGAttach.getGrid().Rows.SelectedRows
                            Dim iSelectionCount As Integer = oSelected.Count
                            If iSelectionCount = 1 Then
                                doDeleteAttachFile(oForm, oGAttach)
                                oGAttach.doReloadData()
                            Else
                                doWarnMess("Select 1 row to delete the file.")
                            End If
                        Catch ex As Exception
                            com.idh.bridge.DataHandler.INSTANCE.doUserError("Error occured while system trying to delete selected file.")
                        End Try
                    ElseIf sItemID = "IDH_BTNBRW" Then
                        Try
                            doCallWinFormOpener(AddressOf doAttachFile, oForm)
                        Catch ex As Exception
                            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
                        End Try
                    ElseIf sItemID = "IDH_BTNDIS" Then
                        Try
                            'Open file 
                            Dim oGAttach As UpdateGrid = UpdateGrid.getInstance(oForm, "ATHGRID")
                            Dim oSelected As SAPbouiCOM.SelectedRows = oGAttach.getGrid().Rows.SelectedRows
                            Dim iSelectionCount As Integer = oSelected.Count
                            If iSelectionCount = 1 Then
                                Dim sFileInfo As String = oGAttach.doGetFieldValue("trgtPath") + "\" + oGAttach.doGetFieldValue("FileName") + "." + oGAttach.doGetFieldValue("FileExt")
                                doDisplayFile(sFileInfo)
                            Else
                                doWarnMess("Select 1 row to display the file.")
                            End If
                        Catch ex As Exception
                            com.idh.bridge.DataHandler.INSTANCE.doUserError("Error occured while system trying to load selected file.")
                        End Try
                    End If
                End If
            End If
            Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Dim bIsTFSValid As Boolean = False

            If pVal.BeforeAction = True Then
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    Dim sCode As String = getFormDFValue(oForm, "Code").ToString()
                    If sCode Is Nothing OrElse sCode.Length = 0 Then
                        'sCode = DataHandler.doGenerateCode(Nothing, "TFSANX")
                        Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, "TFSANX")
                        setFormDFValue(oForm, "Code", oNumbers.CodeCode)
                    End If

                    If doUpdateHeader(oForm, True) Then
                        oForm.Update()
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        BubbleEvent = False
                    End If

                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    If doUpdateHeader(oForm, False) Then
                        oForm.Update()
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        BubbleEvent = False
                    End If
                End If
            Else
            End If

        End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            'If sModalFormType.Equals("IDHCSRCH") Then
            '    Dim sTrg As String = getSharedData(oForm, "TRG")
            '    Dim saParam As String() = {sTrg}

            '    Dim sCustCd As String = getSharedData(oForm, "CARDCODE")
            '    Dim sCustName As String = getSharedData(oForm, "CARDNAME")

            '    If sTrg = "IDH_NOTL" Then
            '        If isValidBPTFSRegNo(oForm, sCustCd) Then
            '            setFormDFValue(oForm, "U_NOTCd", sCustCd)
            '            setFormDFValue(oForm, "U_NOTName", sCustName)
            '            doSetNotifierAddress(oForm)
            '        Else
            '            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            '        End If
            '    ElseIf sTrg = "IDH_CONL" Then
            '        If isValidBPTFSRegNo(oForm, sCustCd) Then
            '            setFormDFValue(oForm, "U_CONCd", sCustCd)
            '            setFormDFValue(oForm, "U_CONName", sCustName)
            '            doSetConsignorAddress(oForm)
            '        Else
            '            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            '        End If
            '    ElseIf sTrg = "IDH_COEL" Then
            '        If isValidBPTFSRegNo(oForm, sCustCd) Then
            '            setFormDFValue(oForm, "U_COECd", sCustCd)
            '            setFormDFValue(oForm, "U_COEName", sCustName)
            '            doSetConsigneeAddress(oForm)
            '        Else
            '            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            '        End If
            '    ElseIf sTrg = "IDH_CARL" Then
            '        If isValidBPTFSRegNo(oForm, sCustCd) Then
            '            setFormDFValue(oForm, "U_CARCd", sCustCd)
            '            setFormDFValue(oForm, "U_CARName", sCustName)
            '            doSetCarrierAddress(oForm)
            '        Else
            '            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            '        End If
            '    ElseIf sTrg = "IDH_GENL" Then
            '        If isValidBPTFSRegNo(oForm, sCustCd) Then
            '            setFormDFValue(oForm, "U_GENCd", sCustCd)
            '            setFormDFValue(oForm, "U_GENName", sCustName)
            '            doSetGeneratorAddress(oForm)
            '        Else
            '            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            '        End If
            '    ElseIf sTrg = "IDH_DSPL" Then
            '        If isValidBPTFSRegNo(oForm, sCustCd) Then
            '            setFormDFValue(oForm, "U_DSPCd", sCustCd)
            '            setFormDFValue(oForm, "U_DSPName", sCustName)
            '            doSetDisposalFacilityAddress(oForm)
            '        Else
            '            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            '        End If
            '    ElseIf sTrg = "IDH_FNDL" Then
            '        If isValidBPTFSRegNo(oForm, sCustCd) Then
            '            setFormDFValue(oForm, "U_FDSCd", sCustCd)
            '            setFormDFValue(oForm, "U_FDSName", sCustName)
            '            doSetFinalDestinationAddress(oForm)
            '        Else
            '            doResError("Selected BP TFS Registration is expired! Select a BP with valid registration no.", "IVBPRN", saParam)
            '        End If
            '    End If
            'ElseIf sModalFormType.Equals("IDHTFSNSR") Then
            '    Dim sTrg As String = getSharedData(oForm, "TRG")
            '    If sTrg = "IDH_TFSNL" Then
            '        If isValidTFSNumber(getSharedData(oForm, "IDH_TFSNUM")) Then
            '            setFormDFValue(oForm, "U_TFSNo", getSharedData(oForm, "IDH_TFSNUM"))
            '        Else
            '            Dim saParam As String() = {sTrg}
            '            doResError("Selected TFS Number not valid! Please choose an active TFS Number.", "INVLTFSN", saParam)
            '        End If
            '    End If
            'End If
            'MyBase.doHandleModalResultShared(oForm, sModalFormType, sLastButton)
            Dim oParentForm As SAPbouiCOM.Form = goParent.doGetParentForm(oForm.UniqueID)
            setSharedData(oParentForm, "ANXCD1", "123")
            setSharedData(oParentForm, "ANXDESC1", "anx desc")
            doReturnFromModalShared(oForm, True)
        End Sub

        Protected Overrides Sub doHandleModalBufferedResult(ByVal oParentForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            'If oData Is Nothing Then Exit Sub
            'Dim oArr As ArrayList = oData
            'If oArr.Count = 0 Then
            '    Return
            'End If
            'setFormDFValue(oParentForm, "U_BoxNo", oArr(0))
            'MyBase.doHandleModalBufferedResult(oParentForm, oData, sModalFormType, sLastButton)
        End Sub

        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            'MyBase.doReadInputParams(oForm)
            'Dim sVal As String = getParentSharedData(oForm, "Share_ACd")

            'setFormDFValue(oForm, "U_BoxNo", sVal)

        End Sub

#End Region

#Region "Custom Functions"

        Protected Function doUpdateHeader(ByVal oForm As SAPbouiCOM.Form, ByVal bIsAdd As Boolean) As Boolean
            'This will now Automatically update all the current DB fields and
            'generate and set the Code value if the code was not set
            Return doAutoSave(oForm, Nothing, "TFSANX")
        End Function

        Public Overridable Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form)
            Dim sCode As String = ""

            setFormDFValue(oForm, "Code", sCode)
            setFormDFValue(oForm, "Name", sCode)
            setFormDFValue(oForm, "U_AnexNo", "")
            setFormDFValue(oForm, "U_TFSCd", "")
            setFormDFValue(oForm, "U_TFSNo", "")
            setFormDFValue(oForm, "U_Edtable", "")
            setFormDFValue(oForm, "U_Detail", "")
            setFormDFValue(oForm, "U_AtcEntry", "")

        End Sub

        Public Overridable Sub doCreateFindEntry(ByVal oForm As SAPbouiCOM.Form)
            setFormDFValue(oForm, "Code", "", True)
            setFormDFValue(oForm, "Name", "")
            setFormDFValue(oForm, "U_AnexNo", "")
            setFormDFValue(oForm, "U_TFSCd", "")
            setFormDFValue(oForm, "U_TFSNo", "")
            setFormDFValue(oForm, "U_Edtable", "")
            setFormDFValue(oForm, "U_Detail", "")
            setFormDFValue(oForm, "U_AtcEntry", "")

            Dim oExclude() As String = {"IDH_ANEXCD"}
            DisableAllEditItems(oForm, oExclude)

        End Sub

        Protected Overridable Sub doSetListConsoleFields(ByRef oGConsole As IDHAddOns.idh.controls.UpdateGrid)
            oGConsole.doAddListField("AbsEntry", "Abs Entry", False, 8, Nothing, Nothing)
            oGConsole.doAddListField("Line", "Line", False, 8, Nothing, Nothing)
            oGConsole.doAddListField("trgtPath", "Path", False, 8, Nothing, Nothing)
            oGConsole.doAddListField("FileName", "File Name", False, 20, Nothing, Nothing)
            oGConsole.doAddListField("FileExt", "File Extension", False, 20, Nothing, Nothing)
            oGConsole.doAddListField("Date", "Attachment Date", False, 20, Nothing, Nothing)
        End Sub

        Public Overridable Function getListConsoleRequiredStr() As String
            'Dim oForm As SAPbouiCOM.Form
            Dim sField As String = "" '" [U_AnexNo] = '' "
            Return sField
        End Function

        Protected Sub doSetMode(ByVal oForm As SAPbouiCOM.Form, ByVal iMode As SAPbouiCOM.BoFormMode)
            If iMode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                oForm.Items.Item("1").Specific.Caption = getTranslatedWord("Find")
            ElseIf iMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                oForm.Items.Item("1").Specific.Caption = getTranslatedWord("Update")
                doSetUpdate(oForm)
            ElseIf iMode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                oForm.Items.Item("1").Specific.Caption = getTranslatedWord("Ok")
            End If
        End Sub

        Protected Sub doAttachFile(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim sFileName As String
                Dim oOpenFile As System.Windows.Forms.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()
                oOpenFile.InitialDirectory = "C:\"
                oOpenFile.Filter = "All files (*.*)|*.*"
                oOpenFile.FileName = ""

                Dim oResult As System.Windows.Forms.DialogResult = com.idh.win.AppForm.OpenFileDialog(oOpenFile)
                If oResult = System.Windows.Forms.DialogResult.OK Then
                    sFileName = oOpenFile.FileName

                    'Check SBO Attachment Path is set 
                    'Sample 
                    'SAPbobsCOM.CompanyService ComService = SBO_Company.GetCompanyService();
                    'SAPbobsCOM.PathAdmin oPathAdmin = ComService.GetPathAdmin();
                    'string path = oPathAdmin.AttachmentsFolderPath;

                    Dim oCmpSrv As SAPbobsCOM.CompanyService = goParent.goDICompany.GetCompanyService()
                    Dim sDefaultAttachmentPath As String = oCmpSrv.GetPathAdmin().AttachmentsFolderPath

                    If sDefaultAttachmentPath.Length > 0 Then
                        'Do attachment on a static file 
                        Dim oAtt As SAPbobsCOM.Attachments2 = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oAttachments2)
                        Dim sAttatchmentEntry As String = getFormDFValue(oForm, "U_AtcEntry")
                        If sAttatchmentEntry.Length > 0 Then
                            If (oAtt.GetByKey(Convert.ToInt16(sAttatchmentEntry))) Then
                                oAtt.Lines.Add()
                                oAtt.Lines.FileName = System.IO.Path.GetFileNameWithoutExtension(sFileName)
                                oAtt.Lines.FileExtension = System.IO.Path.GetExtension(sFileName).Substring(1)
                                oAtt.Lines.SourcePath = System.IO.Path.GetDirectoryName(sFileName)
                                oAtt.Lines.Override = SAPbobsCOM.BoYesNoEnum.tYES
                                If oAtt.Update() = 0 Then
                                    'oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                    'load grid again to refresh results 
                                    Dim oGAttach As UpdateGrid = UpdateGrid.getInstance(oForm, "ATHGRID")
                                    oGAttach.setRequiredFilter(" AbsEntry = " + sAttatchmentEntry + " ")
                                    oGAttach.doReloadData()
                                Else
                                    'catch error details 
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription())
                                End If
                            Else
                                'Attachment record not found error 
                            End If
                        Else
                            'Add new 
                            oAtt.Lines.Add()
                            oAtt.Lines.FileName = System.IO.Path.GetFileNameWithoutExtension(sFileName)
                            oAtt.Lines.FileExtension = System.IO.Path.GetExtension(sFileName).Substring(1)
                            oAtt.Lines.SourcePath = System.IO.Path.GetDirectoryName(sFileName)
                            oAtt.Lines.Override = SAPbobsCOM.BoYesNoEnum.tYES

                            Dim iAttEntry As Int16 = -1
                            If (oAtt.Add() = 0) Then
                                iAttEntry = DataHandler.INSTANCE.SBOCompany.GetNewObjectKey()
                                'setUFValue(oForm, "IDH_ATCENT", iAttEntry)
                                setFormDFValue(oForm, "U_AtcEntry", iAttEntry)
                                'load grid again to refresh results 
                                Dim oGAttach As UpdateGrid = UpdateGrid.getInstance(oForm, "ATHGRID")
                                oGAttach.setRequiredFilter(" AbsEntry = " + iAttEntry.ToString() + " ")
                                oGAttach.doReloadData()
                                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                            End If
                        End If
                    Else
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("doAttachFile(): SBO Company attachment path is not set. Please set under 'General Settings -->>' Path tab.")
                    End If

                End If
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doUserError("doAttachFile(): Error occured  while attaching the file.")
            End Try

        End Sub

        Protected Sub doDeleteAttachFile(ByVal oForm As SAPbouiCOM.Form, ByVal oGAttach As UpdateGrid)
            Try
                Dim oAtt As SAPbobsCOM.Attachments2 = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oAttachments2)
                Dim sAttatchmentEntry As String = getFormDFValue(oForm, "U_AtcEntry")
                If sAttatchmentEntry.Length > 0 Then
                    If (oAtt.GetByKey(Convert.ToInt16(sAttatchmentEntry))) Then
                        Dim sLineNo As String = oGAttach.doGetFieldValue("Line")
                        Dim sQry As String = "delete from ATC1 where AbsEntry = " + sAttatchmentEntry + " and Line = " + sLineNo + "; "
                        Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                        oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        oRecordSet.DoQuery(sQry)
                    Else
                        doWarnMess("System cannot find attachment record.")
                    End If
                End If
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doUserError("doAttachFile(): Error occured  while attaching the file.")
            End Try

        End Sub

        Public Sub doDisplayFile(ByVal sFileInfo As String)
            Try
                Dim oProcess As New System.Diagnostics.Process
                Dim oInfo As New System.Diagnostics.ProcessStartInfo(sFileInfo) '("C:\ISBGlobal\0 - Attachment_upload\SampleUpload1.txt")
                oInfo.UseShellExecute = True
                oInfo.WindowStyle = ProcessWindowStyle.Normal
                oProcess.StartInfo = oInfo
                oProcess.Start()
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doUserError("doDisplayFile(): Error occured while opening the file.")
            End Try
        End Sub

#End Region

    End Class
End Namespace
