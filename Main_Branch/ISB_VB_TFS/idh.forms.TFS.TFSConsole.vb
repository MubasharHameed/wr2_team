Imports System.IO
Imports System.Collections
Imports System

'Needed for the Framework controls
Imports IDHAddOns.idh.controls
Imports com.idh.bridge.data
Imports com.idh.dbObjects.User
Imports com.idh.bridge.lookups
Imports com.idh.bridge
Imports com.idh.controls
Imports com.idh.dbObjects.numbers

'Imports WR1_Forms.idh.const

Namespace idh.forms.TFS
    Public Class TFSConsole
        Inherits IDHAddOns.idh.forms.Base

        Dim sHeadTable As String = "@IDH_Master" 'will be assigned a value in the constructor
        Dim sRowTable As String = "IDH_Detail"

#Region "Initialization"

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParentMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_TFSMAST", sParentMenu, iMenuPosition, "TFS ManagementConsole.srf", False, True, False, "Transfrontier Shipment", load_Types.idh_LOAD_NORMAL)

            sHeadTable = "@IDH_TFSMAST"

            doEnableHistory(sHeadTable)

        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Dim oItem As SAPbouiCOM.Item
            'Dim oButton As SAPbouiCOM.Button
            Dim oFolder As SAPbouiCOM.Folder
            Dim oOptionBtn As SAPbouiCOM.OptionBtn
            'Dim oCheckBox As SAPbouiCOM.CheckBox
            'Dim oStatic As SAPbouiCOM.StaticText

            'Dim i As Integer

            oForm.Freeze(True)

            Try
                oForm.DataSources.UserDataSources.Add("FldrDSKA", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                oForm.DataSources.UserDataSources.Add("Fldr6DSKA", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)

                'Adding individual tabs instead of loop 
                'Console Tab
                oItem = oForm.Items.Add("TABCON", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
                oItem.Left = 5
                oItem.Width = 100
                oItem.Top = 125
                oItem.Height = 19
                oFolder = oItem.Specific
                oFolder.Caption = getTranslatedWord("Console")
                oFolder.DataBind.SetBound(True, "", "FldrDSKA")
                oFolder.Select()

                'Notification Tab
                oItem = oForm.Items.Add("TABNOT", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
                oItem.Left = 5 + 100
                oItem.Width = 100
                oItem.Top = 125
                oItem.Height = 19
                oFolder = oItem.Specific
                oFolder.Caption = getTranslatedWord("Notifications")
                oFolder.DataBind.SetBound(True, "", "FldrDSKA")
                oFolder.GroupWith("TABCON")

                'Movements Tab
                oItem = oForm.Items.Add("TABMOV", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
                oItem.Left = 5 + 200
                oItem.Width = 100
                oItem.Top = 125
                oItem.Height = 19
                oFolder = oItem.Specific
                oFolder.Caption = getTranslatedWord("Movements")
                oFolder.DataBind.SetBound(True, "", "FldrDSKA")
                oFolder.GroupWith("TABNOT")

                'Index Tab
                oItem = oForm.Items.Add("TABIND", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
                oItem.Left = 5 + 300
                oItem.Width = 100
                oItem.Top = 125
                oItem.Height = 19
                oFolder = oItem.Specific
                oFolder.Caption = getTranslatedWord("Index")
                oFolder.DataBind.SetBound(True, "", "FldrDSKA")
                oFolder.GroupWith("TABMOV")

                'Comments Tab
                oItem = oForm.Items.Add("TABCOM", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
                oItem.Left = 5 + 400
                oItem.Width = 100
                oItem.Top = 125
                oItem.Height = 19
                oFolder = oItem.Specific
                oFolder.Caption = getTranslatedWord("Comments")
                oFolder.DataBind.SetBound(True, "", "FldrDSKA")
                oFolder.GroupWith("TABIND")

                'Adding outer rectangle 
                oItem = oForm.Items.Add("Rec1", SAPbouiCOM.BoFormItemTypes.it_RECTANGLE)
                oItem.Top = 145
                oItem.Left = 5
                oItem.Height = 400
                oItem.Width = 1000

                'Adding Inner Tabs 
                'General Tab
                oItem = oForm.Items.Add("TABGEN", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
                oItem.Left = 20
                oItem.Width = 100
                oItem.Top = 150
                oItem.Height = 19
                oItem.FromPane = 200
                oItem.ToPane = 299
                oFolder = oItem.Specific
                oFolder.Caption = getTranslatedWord("General")
                oFolder.DataBind.SetBound(True, "", "Fldr6DSKA")
                oFolder.Select()

                'Notifier Tab
                oItem = oForm.Items.Add("TABNOR", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
                oItem.Left = 20 + 100
                oItem.Width = 100
                oItem.Top = 150
                oItem.Height = 19
                oItem.FromPane = 200
                oItem.ToPane = 299
                oFolder = oItem.Specific
                oFolder.Caption = getTranslatedWord("Notifier")
                oFolder.DataBind.SetBound(True, "", "Fldr6DSKA")
                oFolder.GroupWith("TABGEN")

                'Consignor Tab
                oItem = oForm.Items.Add("TABCOR", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
                oItem.Left = 20 + 200
                oItem.Width = 100
                oItem.Top = 150
                oItem.Height = 19
                oItem.FromPane = 200
                oItem.ToPane = 299
                oFolder = oItem.Specific
                oFolder.Caption = getTranslatedWord("Consignor")
                oFolder.DataBind.SetBound(True, "", "Fldr6DSKA")
                oFolder.GroupWith("TABNOR")

                'Consignee Tab
                oItem = oForm.Items.Add("TABCOE", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
                oItem.Left = 20 + 300
                oItem.Width = 100
                oItem.Top = 150
                oItem.Height = 19
                oItem.FromPane = 200
                oItem.ToPane = 299
                oFolder = oItem.Specific
                oFolder.Caption = getTranslatedWord("Consignee")
                oFolder.DataBind.SetBound(True, "", "Fldr6DSKA")
                oFolder.GroupWith("TABCOR")

                'Country Tab
                oItem = oForm.Items.Add("TABCTY", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
                oItem.Left = 20 + 400
                oItem.Width = 100
                oItem.Top = 150
                oItem.Height = 19
                oItem.FromPane = 200
                oItem.ToPane = 299
                oFolder = oItem.Specific
                oFolder.Caption = getTranslatedWord("Countries")
                oFolder.DataBind.SetBound(True, "", "Fldr6DSKA")
                oFolder.GroupWith("TABCOE")

                'Generator Tab
                oItem = oForm.Items.Add("TABGER", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
                oItem.Left = 20 + 500
                oItem.Width = 100
                oItem.Top = 150
                oItem.Height = 19
                oItem.FromPane = 200
                oItem.ToPane = 299
                oFolder = oItem.Specific
                oFolder.Caption = getTranslatedWord("Generator")
                oFolder.DataBind.SetBound(True, "", "Fldr6DSKA")
                oFolder.GroupWith("TABCTY")

                'Disposal Facility Tab
                oItem = oForm.Items.Add("TABDIF", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
                oItem.Left = 20 + 600
                oItem.Width = 100
                oItem.Top = 150
                oItem.Height = 19
                oItem.FromPane = 200
                oItem.ToPane = 299
                oFolder = oItem.Specific
                oFolder.Caption = getTranslatedWord("Disposal Facility")
                oFolder.DataBind.SetBound(True, "", "Fldr6DSKA")
                oFolder.GroupWith("TABGER")

                'Final Destination Tab
                oItem = oForm.Items.Add("TABFID", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
                oItem.Left = 20 + 700
                oItem.Width = 100
                oItem.Top = 150
                oItem.Height = 19
                oItem.FromPane = 200
                oItem.ToPane = 299
                oFolder = oItem.Specific
                oFolder.Caption = getTranslatedWord("Final Destination")
                oFolder.DataBind.SetBound(True, "", "Fldr6DSKA")
                oFolder.GroupWith("TABDIF")

                'Carrier Tab 
                oItem = oForm.Items.Add("TABCAR", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
                oItem.Left = 20 + 800
                oItem.Width = 100
                oItem.Top = 150
                oItem.Height = 19
                oItem.FromPane = 200
                oItem.ToPane = 299
                oFolder = oItem.Specific
                oFolder.Caption = getTranslatedWord("Carrier")
                oFolder.DataBind.SetBound(True, "", "Fldr6DSKA")
                oFolder.GroupWith("TABFID")

                'Adding inner rectangle 
                oItem = oForm.Items.Add("Rec2", SAPbouiCOM.BoFormItemTypes.it_RECTANGLE)
                oItem.Top = 170
                oItem.Left = 20
                oItem.Height = 370
                oItem.Width = 970
                oItem.FromPane = 200
                oItem.ToPane = 299

                'Adding Option Buttons 
                Dim oUserdatasource As SAPbouiCOM.UserDataSource

                'Indiviual Shipment
                oItem = oForm.Items.Add("IDH_OPIND", SAPbouiCOM.BoFormItemTypes.it_OPTION_BUTTON)
                oItem.Top = 186
                oItem.Left = 25
                oItem.Height = 14
                oItem.Width = 140
                oItem.FromPane = 201
                oItem.ToPane = 201
                oOptionBtn = oItem.Specific
                oOptionBtn.Caption = getTranslatedWord("A (i) Individual shipment:")

                oUserdatasource = oForm.DataSources.UserDataSources.Add("ds_MI", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                oOptionBtn.DataBind.SetBound(True, , "ds_MI")

                'Multiple Shipment
                oItem = oForm.Items.Add("IDH_OPMUL", SAPbouiCOM.BoFormItemTypes.it_OPTION_BUTTON)
                oItem.Top = 186
                oItem.Left = 220
                oItem.Height = 14
                oItem.Width = 120
                oItem.FromPane = 201
                oItem.ToPane = 201
                oOptionBtn = oItem.Specific
                oOptionBtn.Caption = getTranslatedWord("(ii) Multiple shipments:")
                oOptionBtn.GroupWith("IDH_OPIND")

                'Disposal Option 
                oItem = oForm.Items.Add("IDH_OPDIS", SAPbouiCOM.BoFormItemTypes.it_OPTION_BUTTON)
                oItem.Top = 202
                oItem.Left = 25
                oItem.Height = 14
                oItem.Width = 120
                oItem.FromPane = 201
                oItem.ToPane = 201
                oOptionBtn = oItem.Specific
                oOptionBtn.Caption = getTranslatedWord("B (i) Disposal (1):")

                oUserdatasource = oForm.DataSources.UserDataSources.Add("ds_DR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                oOptionBtn.DataBind.SetBound(True, , "ds_DR")

                'Recovery Option 
                oItem = oForm.Items.Add("IDH_OPREC", SAPbouiCOM.BoFormItemTypes.it_OPTION_BUTTON)
                oItem.Top = 202
                oItem.Left = 220
                oItem.Height = 14
                oItem.Width = 120
                oItem.FromPane = 201
                oItem.ToPane = 201
                oOptionBtn = oItem.Specific
                oOptionBtn.Caption = getTranslatedWord("(ii) Recovery:")
                oOptionBtn.GroupWith("IDH_OPDIS")

                'Pre-connected YES Option 
                oItem = oForm.Items.Add("IDH_OPCOY", SAPbouiCOM.BoFormItemTypes.it_OPTION_BUTTON)
                oItem.Top = 219
                oItem.Left = 220
                oItem.Height = 14
                oItem.Width = 40
                oItem.FromPane = 201
                oItem.ToPane = 201
                oOptionBtn = oItem.Specific
                oOptionBtn.Caption = getTranslatedWord("Yes")

                oUserdatasource = oForm.DataSources.UserDataSources.Add("ds_PC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                oOptionBtn.DataBind.SetBound(True, , "ds_PC")

                'Pre-connected NO Option 
                oItem = oForm.Items.Add("IDH_OPCON", SAPbouiCOM.BoFormItemTypes.it_OPTION_BUTTON)
                oItem.Top = 219
                oItem.Left = 275
                oItem.Height = 14
                oItem.Width = 40
                oItem.FromPane = 201
                oItem.ToPane = 201
                oOptionBtn = oItem.Specific
                oOptionBtn.Caption = getTranslatedWord("No")
                oOptionBtn.GroupWith("IDH_OPCOY")

                'Handling Requested YES Option 
                oItem = oForm.Items.Add("IDH_OPHRY", SAPbouiCOM.BoFormItemTypes.it_OPTION_BUTTON)
                oItem.Top = 398
                oItem.Left = 186 '195
                oItem.Height = 14
                oItem.Width = 40
                oItem.FromPane = 201
                oItem.ToPane = 201
                oOptionBtn = oItem.Specific
                oOptionBtn.Caption = getTranslatedWord("Yes")

                oUserdatasource = oForm.DataSources.UserDataSources.Add("ds_HR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                oOptionBtn.DataBind.SetBound(True, , "ds_HR")

                'Handling Requested NO Option 
                oItem = oForm.Items.Add("IDH_OPHRN", SAPbouiCOM.BoFormItemTypes.it_OPTION_BUTTON)
                oItem.Top = 398
                oItem.Left = 228 '253
                oItem.Height = 14
                oItem.Width = 40
                oItem.FromPane = 201
                oItem.ToPane = 201
                oOptionBtn = oItem.Specific
                oOptionBtn.Caption = getTranslatedWord("No")
                oOptionBtn.GroupWith("IDH_OPHRY")

                'Tonnes and Cubes YES Option 
                oItem = oForm.Items.Add("IDH_OPTON", SAPbouiCOM.BoFormItemTypes.it_OPTION_BUTTON)
                oItem.Top = 302 '308
                oItem.Left = 25
                oItem.Height = 14
                oItem.Width = 85
                oItem.FromPane = 201
                oItem.ToPane = 201
                oOptionBtn = oItem.Specific
                oOptionBtn.Caption = getTranslatedWord("Tonnes (Mg)")

                oUserdatasource = oForm.DataSources.UserDataSources.Add("ds_TC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                oOptionBtn.DataBind.SetBound(True, , "ds_TC")

                'Cubic Meter Yes caption 
                oItem = oForm.Items.Add("IDH_OPCMT", SAPbouiCOM.BoFormItemTypes.it_OPTION_BUTTON)
                oItem.Top = 302 '308
                oItem.Left = 112
                oItem.Height = 14
                oItem.Width = 107
                oItem.FromPane = 201
                oItem.ToPane = 201
                oOptionBtn = oItem.Specific
                oOptionBtn.Caption = getTranslatedWord("Cubic Meters (m3)")
                oOptionBtn.GroupWith("IDH_OPTON")

                ''D or R codes 
                ''D-Codes
                'oItem = oForm.Items.Add("IDH_OPDCD", SAPbouiCOM.BoFormItemTypes.it_OPTION_BUTTON)
                'oItem.Top = 439
                'oItem.Left = 25
                'oItem.Height = 14
                'oItem.Width = 85
                'oItem.FromPane = 201
                'oItem.ToPane = 201
                'oOptionBtn = oItem.Specific
                'oOptionBtn.Caption = getTranslatedWord("D-Codes")

                'oUserdatasource = oForm.DataSources.UserDataSources.Add("ds_DRCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
                'oOptionBtn.DataBind.SetBound(True, , "ds_DRCd")

                ''R-Codes 
                'oItem = oForm.Items.Add("IDH_OPRCD", SAPbouiCOM.BoFormItemTypes.it_OPTION_BUTTON)
                'oItem.Top = 439
                'oItem.Left = 112
                'oItem.Height = 14
                'oItem.Width = 85
                'oItem.FromPane = 201
                'oItem.ToPane = 201
                'oOptionBtn = oItem.Specific
                'oOptionBtn.Caption = getTranslatedWord("R-Codes")
                'oOptionBtn.GroupWith("IDH_OPDCD")


                'Mapp fields with data fields 
                doAddFormDF(oForm, "IDH_CODE", "Code")
                doAddFormDF(oForm, "IDH_TFSNO1", "U_TFSNum")
                doAddFormDF(oForm, "IDH_TFSTYP", "U_Type")
                doAddFormDF(oForm, "IDH_STAT", "U_Status")

                '******************************************
                'START HERE MAPP FIELDS FROM RIGHT TO LEFT 
                '******************************************


                'form tfs details 
                doAddFormDF(oForm, "IDH_OPIND", "U_ShpType") 'Shipment Type - Yes 
                doAddFormDF(oForm, "IDH_OPMUL", "U_ShpType") 'Shipment Type - No
                doAddFormDF(oForm, "IDH_OPDIS", "U_OpType") 'Operation Type - Disposal 
                doAddFormDF(oForm, "IDH_OPREC", "U_OpType") 'Operation Type - Recovery  
                doAddFormDF(oForm, "IDH_OPCOY", "U_SPreAp") 'Site pre-approved - Yes 
                doAddFormDF(oForm, "IDH_OPCON", "U_SPreAp") 'Site pre-approved - No 
                doAddFormDF(oForm, "IDH_SHPINT", "U_IntShip")
                doAddFormDF(oForm, "IDH_SHPACT", "U_ActShip")
                doAddFormDF(oForm, "IDH_QTYINT", "U_IntQty")
                doAddFormDF(oForm, "IDH_QTYTON", "U_QtyT")
                doAddFormDF(oForm, "IDH_QTYCM", "U_QtyCMtr")
                doAddFormDF(oForm, "IDH_ISHDTF", "U_SDtFrm")
                doAddFormDF(oForm, "IDH_ISHDTT", "U_SDtTo")
                'doAddFormDF(oForm, "IDH_PKGLKP", "U_PackTyp")
                doAddFormDF(oForm, "IDH_PKGTYP", "U_PackTyp")
                doAddFormDF(oForm, "IDH_OPHRY", "U_SpHandle") 'Special Handling - Yes 
                doAddFormDF(oForm, "IDH_OPHRN", "U_SpHandle") 'Special Handling - No 
                doAddFormDF(oForm, "IDH_SPHDET", "U_SpHDetails") 'Special Handling Details

                doAddFormDF(oForm, "IDH_DRCODE", "U_DCode") 'Lookup field 
                doAddFormDF(oForm, "IDH_TECHEM", "U_TechEmp")
                'doAddFormDF(oForm, "IDH_", "U_ExpRes")
                'doAddFormDF(oForm, "IDH_", "U_WasteCmp")
                'doAddFormDF(oForm, "IDH_PHYLKP", "U_PhyChtr")
                doAddFormDF(oForm, "IDH_PHYCHR", "U_PhyChtr")
                doAddFormDF(oForm, "IDH_BASEL", "U_BaselCd")
                doAddFormDF(oForm, "IDH_OECDCD", "U_OECDCd")
                doAddFormDF(oForm, "IDH_EWCCD", "U_EWCCd")
                doAddFormDF(oForm, "IDH_EXPCCD", "U_ExpNatCd")
                doAddFormDF(oForm, "IDH_IMPCCD", "U_ImpNatCd")
                doAddFormDF(oForm, "IDH_OTHCCD", "U_Other")
                doAddFormDF(oForm, "IDH_YCODE", "U_YCode")
                doAddFormDF(oForm, "IDH_HCODE", "U_HCode")
                doAddFormDF(oForm, "IDH_UNNUM", "U_UNNumber")
                doAddFormDF(oForm, "IDH_UNCLAS", "U_UNClass")
                doAddFormDF(oForm, "IDH_SHIPNM", "U_UNShipNm")
                doAddFormDF(oForm, "IDH_CUSTCD", "U_CustCd")

                'Notifier
                doAddFormDF(oForm, "IDH_NOTCD", "U_NOTCd")
                doAddFormDF(oForm, "IDH_NOTNM", "U_NOTName")
                doAddFormDF(oForm, "IDH_NOTRGN", "U_NotRegNo")
                doAddFormDF(oForm, "IDH_NOTADD", "U_NotAdres")
                doAddFormDF(oForm, "IDH_NOTCPR", "U_NotCntPr")
                doAddFormDF(oForm, "IDH_NOTPH", "U_NotPh")
                doAddFormDF(oForm, "IDH_NOTFAX", "U_NotFax")
                doAddFormDF(oForm, "IDH_NOTEML", "U_NotEmail")
                doAddFormDF(oForm, "IDH_NOTSIG", "U_NotSign")

                'Consignor
                doAddFormDF(oForm, "IDH_CONCD", "U_CONCd")
                doAddFormDF(oForm, "IDH_CONNM", "U_CONName")
                doAddFormDF(oForm, "IDH_CONRGN", "U_ConRegNo")
                doAddFormDF(oForm, "IDH_CONADD", "U_ConAdres")
                doAddFormDF(oForm, "IDH_CONCPR", "U_ConCntPr")
                doAddFormDF(oForm, "IDH_CONPH", "U_ConPh")
                doAddFormDF(oForm, "IDH_CONFAX", "U_ConFax")
                doAddFormDF(oForm, "IDH_CONEML", "U_ConEmail")

                'Consignee
                doAddFormDF(oForm, "IDH_COECD", "U_COECd")
                doAddFormDF(oForm, "IDH_COENM", "U_COEName")
                doAddFormDF(oForm, "IDH_COERGN", "U_CoeRegNo")
                doAddFormDF(oForm, "IDH_COEADD", "U_CoeAdres")
                doAddFormDF(oForm, "IDH_COECPR", "U_CoeCntPr")
                doAddFormDF(oForm, "IDH_COEPH", "U_CoePh")
                doAddFormDF(oForm, "IDH_COEFAX", "U_CoeFax")
                doAddFormDF(oForm, "IDH_COEEML", "U_CoeEmail")

                'Carrier
                doAddFormDF(oForm, "IDH_CARCD", "U_CARCd")
                doAddFormDF(oForm, "IDH_CARNM", "U_CARName")
                '20151007: New fields for Carrier details 
                doAddFormDF(oForm, "IDH_CARRGN", "U_CarRegNo")
                doAddFormDF(oForm, "IDH_CARADD", "U_CarAdres")
                doAddFormDF(oForm, "IDH_CARCPR", "U_CarCntPr")
                doAddFormDF(oForm, "IDH_CARPH", "U_CarPh")
                doAddFormDF(oForm, "IDH_CARFAX", "U_CarFax")
                doAddFormDF(oForm, "IDH_CAREML", "U_CarEmail")
                doAddFormDF(oForm, "IDH_CARCRG", "U_CarCmpReg")

                doAddFormDF(oForm, "IDH_TRNMOD", "U_TranMode")

                'Generator
                doAddFormDF(oForm, "IDH_GENCD", "U_GENCd")
                doAddFormDF(oForm, "IDH_GENNM", "U_GENName")
                doAddFormDF(oForm, "IDH_GENRGN", "U_GenRegNo")
                doAddFormDF(oForm, "IDH_GENADD", "U_GenAdres")
                doAddFormDF(oForm, "IDH_GENCPR", "U_GenCntPr")
                doAddFormDF(oForm, "IDH_GENPH", "U_GenPh")
                doAddFormDF(oForm, "IDH_GENFAX", "U_GenFax")
                doAddFormDF(oForm, "IDH_GENEML", "U_GenEmail")

                'Disp. Facility
                doAddFormDF(oForm, "IDH_DSPCD", "U_DSPCd")
                doAddFormDF(oForm, "IDH_DSPNM", "U_DSPName")
                doAddFormDF(oForm, "IDH_DSPRGN", "U_DOFRegNo")
                doAddFormDF(oForm, "IDH_DSPADD", "U_DOFAdres")
                doAddFormDF(oForm, "IDH_DSPCPR", "U_DOFCntPr")
                doAddFormDF(oForm, "IDH_DSPPH", "U_DOFPh")
                doAddFormDF(oForm, "IDH_DSPFAX", "U_DOFFax")
                doAddFormDF(oForm, "IDH_DSPEML", "U_DOFEmail")
                doAddFormDF(oForm, "IDH_DSPSIZ", "U_DOPreAp")

                'Final Dest.
                doAddFormDF(oForm, "IDH_FNDCD", "U_FDSCd")
                doAddFormDF(oForm, "IDH_FNDNM", "U_FDSName")
                doAddFormDF(oForm, "IDH_FDTRGN", "U_FDTRegNo")
                doAddFormDF(oForm, "IDH_FDTADD", "U_FDTAdres")
                doAddFormDF(oForm, "IDH_FDTCPR", "U_FDTCntPr")
                doAddFormDF(oForm, "IDH_FDTPH", "U_FDTPh")
                doAddFormDF(oForm, "IDH_FDTFAX", "U_FDTFax")
                doAddFormDF(oForm, "IDH_FDTEML", "U_FDTEmail")

                doAddFormDF(oForm, "IDH_MULTIP", "U_Multiple")

                '20151001: New fields on Countries tab 
                doAddFormDF(oForm, "IDH_CENTRY", "U_CustEntry")
                doAddFormDF(oForm, "IDH_CEXIT", "U_CustExit")
                doAddFormDF(oForm, "IDH_CEXPRT", "U_CustExport")

                'Annex felds 
                doAddFormDF(oForm, "IDH_AXCD1", "U_AnxCd1")
                doAddFormDF(oForm, "IDH_AXNO1", "U_AnxDesc1")
                doAddFormDF(oForm, "IDH_AXCD2", "U_AnxCd2")
                doAddFormDF(oForm, "IDH_AXNO2", "U_AnxDesc2")
                doAddFormDF(oForm, "IDH_AXCD3", "U_AnxCd3")
                doAddFormDF(oForm, "IDH_AXNO3", "U_AnxDesc3")
                doAddFormDF(oForm, "IDH_AXCD4", "U_AnxCd4")
                doAddFormDF(oForm, "IDH_AXNO4", "U_AnxDesc4")
                doAddFormDF(oForm, "IDH_AXCD5", "U_AnxCd5")
                doAddFormDF(oForm, "IDH_AXNO5", "U_AnxDesc5")
                doAddFormDF(oForm, "IDH_AXCD6", "U_AnxCd6")
                doAddFormDF(oForm, "IDH_AXNO6", "U_AnxDesc6")
                doAddFormDF(oForm, "IDH_AXCD7", "U_AnxCd7")
                doAddFormDF(oForm, "IDH_AXNO7", "U_AnxDesc7")
                doAddFormDF(oForm, "IDH_AXCD8", "U_AnxCd8")
                doAddFormDF(oForm, "IDH_AXNO8", "U_AnxDesc8")
                doAddFormDF(oForm, "IDH_AXCD9", "U_AnxCd9")
                doAddFormDF(oForm, "IDH_AXNO9", "U_AnxDesc9")
                doAddFormDF(oForm, "IDH_AXCD10", "U_AnxCd10")
                doAddFormDF(oForm, "IDH_AXNO10", "U_AnxDesc10")


                'Setting Form Tabs not to affect form mode 
                oForm.Items.Item("TABCON").AffectsFormMode = False 'Console 
                oForm.Items.Item("TABNOT").AffectsFormMode = False 'Notification 
                oForm.Items.Item("TABMOV").AffectsFormMode = False 'Movements 
                oForm.Items.Item("TABIND").AffectsFormMode = False 'Indexing 
                oForm.Items.Item("TABCOM").AffectsFormMode = False 'Comments 
                oForm.Items.Item("TABGEN").AffectsFormMode = False 'General 
                oForm.Items.Item("TABNOR").AffectsFormMode = False 'Notifier
                oForm.Items.Item("TABCOR").AffectsFormMode = False 'Consignor 
                oForm.Items.Item("TABCOE").AffectsFormMode = False 'Consignee 
                oForm.Items.Item("TABCTY").AffectsFormMode = False 'Country 
                oForm.Items.Item("TABGER").AffectsFormMode = False 'General 
                oForm.Items.Item("TABDIF").AffectsFormMode = False 'Disposal Facility 
                oForm.Items.Item("TABFID").AffectsFormMode = False 'Final Destination 
                oForm.Items.Item("TABCAR").AffectsFormMode = False 'Carrier 


                'Company Registration No from BP 
                doAddFormDF(oForm, "IDH_NOTCRG", "U_NotCmpReg")
                doAddFormDF(oForm, "IDH_CONCRG", "U_ConCmpReg")
                doAddFormDF(oForm, "IDH_COECRG", "U_CoeCmpReg")
                doAddFormDF(oForm, "IDH_GENCRG", "U_GenCmpReg")
                doAddFormDF(oForm, "IDH_DSPCRG", "U_DOFCmpReg")
                doAddFormDF(oForm, "IDH_FDTCRG", "U_FDTCmpReg")

                doAddFormDF(oForm, "IDH_FFADD", "U_FFAdres")
                doAddFormDF(oForm, "IDH_FFCPR", "U_FFCntPr")
                doAddFormDF(oForm, "IDH_FFPH", "U_FFPh")
                doAddFormDF(oForm, "IDH_FFFAX", "U_FFFax")
                doAddFormDF(oForm, "IDH_FFEML", "U_FFEmail")

                doAddFormDF(oForm, "IDH_FFCD", "U_FFCd")
                doAddFormDF(oForm, "IDH_FFNM", "U_FFName")
                doAddFormDF(oForm, "IDH_FFCRG", "U_FFCmpReg")
                doAddFormDF(oForm, "IDH_FFRGN", "U_FFRegNo")
                doAddFormDF(oForm, "IDH_GENSIG", "U_FFSCnt")
                doAddFormDF(oForm, "IDH_GENSPG", "U_GenSPG")
                doAddFormDF(oForm, "IDH_REXPO", "U_RExpo")
                doAddFormDF(oForm, "IDH_WSTCOM", "U_WasteComp")

                '20151101
                doAddFormDF(oForm, "IDH_GENSIT", "U_GenSite")

                'NEW Annex felds 
                doAddFormDF(oForm, "IDH_AXCD11", "U_AnxCd11")
                doAddFormDF(oForm, "IDH_AXNO11", "U_AnxDesc11")
                doAddFormDF(oForm, "IDH_AXCD12", "U_AnxCd12")
                doAddFormDF(oForm, "IDH_AXNO12", "U_AnxDesc12")
                doAddFormDF(oForm, "IDH_AXCD13", "U_AnxCd13")
                doAddFormDF(oForm, "IDH_AXNO13", "U_AnxDesc13")
                doAddFormDF(oForm, "IDH_AXCD14", "U_AnxCd14")
                doAddFormDF(oForm, "IDH_AXNO14", "U_AnxDesc14")
                doAddFormDF(oForm, "IDH_AXCD15", "U_AnxCd15")
                doAddFormDF(oForm, "IDH_AXNO15", "U_AnxDesc15")
                doAddFormDF(oForm, "IDH_AXCD16", "U_AnxCd16")
                doAddFormDF(oForm, "IDH_AXNO16", "U_AnxDesc16")
                doAddFormDF(oForm, "IDH_AXCD17", "U_AnxCd17")
                doAddFormDF(oForm, "IDH_AXNO17", "U_AnxDesc17")
                doAddFormDF(oForm, "IDH_AXCD18", "U_AnxCd18")
                doAddFormDF(oForm, "IDH_AXNO18", "U_AnxDesc18")
                doAddFormDF(oForm, "IDH_AXCD19", "U_AnxCd19")
                doAddFormDF(oForm, "IDH_AXNO19", "U_AnxDesc19")
                doAddFormDF(oForm, "IDH_AXCD20", "U_AnxCd20")
                doAddFormDF(oForm, "IDH_AXNO20", "U_AnxDesc20")

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
                BubbleEvent = False
            End Try

            setEnableItem(oForm, False, "IDH_CODE")
            setEnableItem(oForm, False, "IDH_TFSNO1")

            'Form level settings 
            oForm.DataBrowser.BrowseBy = "IDH_CODE"
            oForm.SupportedModes = -1
            oForm.AutoManaged = True

            'oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE

            oForm.Freeze(False)

            'MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

#End Region

#Region "Loading Data"

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            'Using the Form's main table
            Dim sCode As String = getFormDFValue(oForm, "Code").ToString()
            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                doCreateNewEntry(oForm)
            ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                doCreateFindEntry(oForm)
                sCode = getParentSharedData(oForm, "TFSNUM")
                If sCode Is Nothing OrElse sCode.Length <= 0 Then
                    sCode = "-999"
                Else
                    'setFormDFValue(oForm, "U_TFSNum", sCode)
                    'doSetUpdate(oForm)
                End If
            End If
            'sCode = "-999"
            'Using a table other than the Form's main table        	
            'Dim sCode As String = getDFValue(oForm, "@IDH_TRAIN01", "Code")

            ''Dim sTFSTypeID As String = getItemValue(oForm, "IDH_TFSTYP")
            ''Dim oItm As SAPbouiCOM.ComboBox = oForm.Items.Item("IDH_TFSTYP").Specific
            ''If oItm.Selected.Description.ToLower().StartsWith("interim") Then
            ''End If
            'doSetInterimFacilityVisability(oForm)

            'Fill Grids 
            doFillGrids(oForm, sCode)

        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            MyBase.doBeforeLoadData(oForm)
            setEnableItem(oForm, False, "IDH_TFSNO1")
            'Loading data if opened as modal form from the TFS Summary 
            If ghOldDialogParams.Contains(oForm.UniqueID) OrElse getHasSharedData(oForm) = True Then
                Dim oData As ArrayList = ghOldDialogParams.Item(oForm.UniqueID)
                Dim sTFSCode As String = getParentSharedData(oForm, "TFSCODE")
                Dim sTFSNum As String = getParentSharedData(oForm, "TFSNUM")

                Dim sWOHNo As String = getParentSharedData(oForm, "WOHNO")
                Dim sWORNo As String = getParentSharedData(oForm, "WORNO")

                If Not (oData Is Nothing) Then
                    With getFormMainDataSource(oForm)

                        doCreateFindEntry(oForm)

                        Dim oCons As New SAPbouiCOM.Conditions
                        Dim oCon As SAPbouiCOM.Condition
                        oCon = oCons.Add
                        oCon.Alias = "Code"
                        oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                        oCon.CondVal = sTFSCode
                        setFormDFValue(oForm, "Code", sTFSCode)
                        getFormMainDataSource(oForm).Query(oCons)
                        'remark this line: doSetUpdate(oForm)
                    End With
                ElseIf sWOHNo IsNot Nothing AndAlso sWORNo IsNot Nothing Then
                    setFormDFValue(oForm, "U_TFSNum", sTFSNum)
                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                    doFind(oForm)
                ElseIf sTFSCode IsNot Nothing Then
                    'With getFormMainDataSource(oForm)

                    '    doCreateFindEntry(oForm)

                    '    Dim oCons As New SAPbouiCOM.Conditions
                    '    Dim oCon As SAPbouiCOM.Condition
                    '    oCon = oCons.Add
                    '    oCon.Alias = "U_TFSNum"
                    '    oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                    '    oCon.CondVal = sTFSCode
                    setFormDFValue(oForm, "U_TFSNum", sTFSCode)
                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                    '    getFormMainDataSource(oForm).Query(oCons)
                    '    doSetUpdate(oForm)
                    'End With
                    doFind(oForm)
                End If
            Else
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            End If

            Try

                Dim owItem As SAPbouiCOM.Item
                Dim ow2Item As SAPbouiCOM.Folder

                owItem = oForm.Items.Item("TABNOT") 'Console Tab
                ow2Item = owItem.Specific
                ow2Item.Select()
                oForm.PaneLevel = 201

                'Setting Form Tabs not to affect form mode 
                oForm.Items.Item("TABCON").AffectsFormMode = False 'Console 
                oForm.Items.Item("TABNOT").AffectsFormMode = False 'Notification 
                oForm.Items.Item("TABMOV").AffectsFormMode = False 'Movements 
                oForm.Items.Item("TABIND").AffectsFormMode = False 'Indexing 
                oForm.Items.Item("TABCOM").AffectsFormMode = False 'Comments 
                oForm.Items.Item("TABGEN").AffectsFormMode = False 'General 
                oForm.Items.Item("TABNOR").AffectsFormMode = False 'Notifier
                oForm.Items.Item("TABCOR").AffectsFormMode = False 'Consignor 
                oForm.Items.Item("TABCOE").AffectsFormMode = False 'Consignee 
                oForm.Items.Item("TABCTY").AffectsFormMode = False 'Country 
                oForm.Items.Item("TABGER").AffectsFormMode = False 'General 
                oForm.Items.Item("TABDIF").AffectsFormMode = False 'Disposal Facility 
                oForm.Items.Item("TABFID").AffectsFormMode = False 'Final Destination |
                oForm.Items.Item("TABCAR").AffectsFormMode = False 'Carrier 

                'Fill Combo boxes 
                'doFillTFSTypes(oForm)
                'doFillTFSStatus(oForm)

                doFillCombo(oForm, "IDH_TFSTYP", "[@IDH_TFSType]", "U_TCode", "U_TName", Nothing, "U_TCode")
                doFillCombo(oForm, "IDH_STAT", "[@IDH_TFSStatus]", "U_SCode", "U_SName")

                'Filling detail section combos 
                'doFillPhyCharacteristicsCombo(oForm)
                'Dim oComboBox As SAPbouiCOM.ComboBox

                'oComboBox = oForm.Items.Item("IDH_BASEL").Specific
                'doFillCombo(oForm, oComboBox, "[@IDH_BASEL]", "U_BASELCd", "U_Desc", Nothing, Nothing, "N/A", "")

                'oComboBox = oForm.Items.Item("IDH_OECDCD").Specific
                'doFillCombo(oForm, oComboBox, "[@IDH_OECD]", "U_OECDCd", "U_Desc", Nothing, Nothing, "N/A", "")

                'doFillCombo(oForm, "IDH_EWCCD", "[@IDH_WTEWC]", "U_EWCCode", "U_EWCName")
                'doFillCombo(oForm, "IDH_EXPCCD", "[@IDH_WTEWC]", "U_EWCCode", "U_EWCName")
                'doFillCombo(oForm, "IDH_IMPCCD", "[@IDH_WTEWC]", "U_EWCCode", "U_EWCName")
                'doFillCombo(oForm, "IDH_YCODE", "[@IDH_YCODE]", "U_YCode", "U_YDesc")
                'doFillCombo(oForm, "IDH_HCODE", "[@IDH_HCODE]", "U_HCode", "U_HDesc")

                'doFillCombo(oForm, "IDH_UNNUM", "[@IDH_HCODE]", "Code", "U_HCode")

                'oComboBox = oForm.Items.Item("IDH_UNCLAS").Specific
                'doFillCombo(oForm, oComboBox, "[@IDH_HCODE]", "Code", "U_HCode", Nothing, Nothing, "N/A", "")

                'oComboBox = oForm.Items.Item("IDH_SHIPNM").Specific
                'doFillCombo(oForm, oComboBox, "[@IDH_HCODE]", "Code", "U_HCode", Nothing, Nothing, "N/A", "")

                'Fill Combos 
                'doFillPackaging(oForm)
                'doFillPhysicalChars(oForm)

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the doBeforeLoad()")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBPL", {Nothing})
            End Try

            'Manage Grids 
            doManageMovementsGrid(oForm)
            doManageCountryGrid(oForm)

            'doCreateFindEntry(oForm)
            doFillGrids(oForm, "-1")


            oForm.Freeze(False)
        End Sub

#End Region

#Region "Unloading Data"

        Public Overrides Sub doClose()
        End Sub

#End Region

#Region "Event Handlers"

        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
                If pVal.MenuUID = lookups.Base.NAV_ADD Then
                    oForm.Freeze(True)
                    Try
                        doLoadData(oForm)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Menu Event - " & oForm.UniqueID & "." & pVal.MenuUID)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try
                ElseIf pVal.MenuUID = lookups.Base.NAV_FIND OrElse _
                       pVal.MenuUID = lookups.Base.NAV_FIRST OrElse _
                       pVal.MenuUID = lookups.Base.NAV_LAST OrElse _
                       pVal.MenuUID = lookups.Base.NAV_NEXT OrElse _
                       pVal.MenuUID = lookups.Base.NAV_PREV Then
                    oForm.Freeze(True)
                    Try
                        doLoadData(oForm)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Menu Event - " & oForm.UniqueID & "." & pVal.MenuUID)
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try
                End If
            End If

            'If pVal.MenuUID = idh.const.Lookup.NAV_ADD Then
            '    If pVal.BeforeAction = False Then
            '        'oForm.Freeze(True)
            '        Try
            '            Dim sTFSCode = goParent.goDB.doNextNumber("TFSMST").ToString()

            '            setFormDFValue(oForm, "Code", sTFSCode)
            '            'setDFValue(oForm, sRowTable, "Code", "-1")

            '            'doSetEnabled(oForm.Items.Item("IDH_TFSMAST"), False)
            '            'doSetEnabled(oForm.Items.Item("IDH_TFSDET"), False)

            '        Catch ex As Exception
            '            com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing the menu event - " & oForm.UniqueID & "." & pVal.MenuUID)
            '        Finally
            '            'oForm.Freeze(False)
            '        End Try
            '    End If
            'ElseIf pVal.MenuUID = idh.const.Lookup.NAV_FIND OrElse _
            '        pVal.MenuUID = idh.const.Lookup.NAV_FIRST OrElse _
            '        pVal.MenuUID = idh.const.Lookup.NAV_LAST OrElse _
            '        pVal.MenuUID = idh.const.Lookup.NAV_NEXT OrElse _
            '        pVal.MenuUID = idh.const.Lookup.NAV_PREV Then
            '    'do what ever you like to do for menu events above 
            'End If
            'Return MyBase.doMenuEvent(oForm, pVal, BubbleEvent)
            Return True
        End Function

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim sItemID As String = pVal.ItemUID
            Dim owItem As SAPbouiCOM.Item
            Dim ow2Item As SAPbouiCOM.Folder

            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED And pVal.Before_Action = True Then
                Select Case sItemID
                    Case "TABCON"
                        oForm.PaneLevel = 100
                    Case "TABNOT"
                        oForm.PaneLevel = 201
                        owItem = oForm.Items.Item("TABGEN") 'General Tab
                        ow2Item = owItem.Specific
                        ow2Item.Select()
                    Case "TABMOV"
                        oForm.PaneLevel = 300
                    Case "TABIND"
                        oForm.PaneLevel = 400
                    Case "TABCOM"
                        oForm.PaneLevel = 500
                    Case "TABGEN"
                        oForm.PaneLevel = 201
                    Case "TABNOR"
                        oForm.PaneLevel = 202
                    Case "TABCOR"
                        oForm.PaneLevel = 203
                    Case "TABCOE"
                        oForm.PaneLevel = 204
                    Case "TABCTY"
                        oForm.PaneLevel = 205
                    Case "TABGER"
                        oForm.PaneLevel = 206
                    Case "TABDIF"
                        oForm.PaneLevel = 207
                    Case "TABFID"
                        oForm.PaneLevel = 208
                        doSetInterimFacilityVisability(oForm)
                    Case "TABCAR"
                        oForm.PaneLevel = 209
                End Select
            End If
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If sItemID = "IDH_TFSNL" Then
                        doChooseTFSNumber(oForm, False, "IDH_TFSNL")
                    End If
                    If sItemID = "IDH_NOTL" Then
                        oForm.PaneLevel = 202
                        owItem = oForm.Items.Item("TABNOR")
                        ow2Item = owItem.Specific
                        ow2Item.Select()
                        doChooseCustomer(oForm, False, "IDH_NOTL")
                    ElseIf sItemID = "IDH_CONL" Then
                        oForm.PaneLevel = 203
                        owItem = oForm.Items.Item("TABCOR")
                        ow2Item = owItem.Specific
                        ow2Item.Select()
                        doChooseCustomer(oForm, False, "IDH_CONL")
                    ElseIf sItemID = "IDH_COEL" Then
                        oForm.PaneLevel = 204
                        owItem = oForm.Items.Item("TABCOE")
                        ow2Item = owItem.Specific
                        ow2Item.Select()
                        doChooseCustomer(oForm, False, "IDH_COEL")
                    ElseIf sItemID = "IDH_CARL" Then
                        'oForm.PaneLevel = 205
                        'owItem = oForm.Items.Item("TABCTY")
                        oForm.PaneLevel = 209
                        owItem = oForm.Items.Item("TABCAR")
                        ow2Item = owItem.Specific
                        ow2Item.Select()
                        doChooseCustomer(oForm, False, "IDH_CARL")
                    ElseIf sItemID = "IDH_GENL" Then
                        oForm.PaneLevel = 206
                        owItem = oForm.Items.Item("TABGER")
                        ow2Item = owItem.Specific
                        ow2Item.Select()
                        doChooseCustomer(oForm, False, "IDH_GENL")
                    ElseIf sItemID = "IDH_DSPL" Then
                        oForm.PaneLevel = 207
                        owItem = oForm.Items.Item("TABDIF")
                        ow2Item = owItem.Specific
                        ow2Item.Select()
                        doChooseCustomer(oForm, False, "IDH_DSPL")
                    ElseIf sItemID = "IDH_FNDL" Then
                        oForm.PaneLevel = 208
                        owItem = oForm.Items.Item("TABFID")
                        ow2Item = owItem.Specific
                        ow2Item.Select()
                        doChooseCustomer(oForm, False, "IDH_FNDL")
                    ElseIf sItemID = "IDH_FFL" Then
                        oForm.PaneLevel = 208
                        owItem = oForm.Items.Item("TABFID")
                        ow2Item = owItem.Specific
                        ow2Item.Select()
                        doChooseCustomer(oForm, False, "IDH_FFL")
                    ElseIf sItemID = "IDH_NOTLAD" OrElse _
                            sItemID = "IDH_CONLAD" OrElse _
                            sItemID = "IDH_COELAD" OrElse _
                            sItemID = "IDH_GENLAD" OrElse _
                            sItemID = "IDH_DSPLAD" OrElse _
                            sItemID = "IDH_FDTLAD" OrElse _
                            sItemID = "IDH_FFLAD" OrElse _
                            sItemID = "IDH_CARLAD" Then
                        doChooseAddress(oForm, False, sItemID, sItemID)
                    ElseIf sItemID = "IDH_NOTLCP" OrElse _
                            sItemID = "IDH_CONLCP" OrElse _
                            sItemID = "IDH_COELCP" OrElse _
                            sItemID = "IDH_GENLCP" OrElse _
                            sItemID = "IDH_GENLSG" OrElse _
                            sItemID = "IDH_NOTLSG" OrElse _
                            sItemID = "IDH_DSPLCP" OrElse _
                            sItemID = "IDH_FDTLCP" OrElse _
                            sItemID = "IDH_FFLCP" OrElse _
                            sItemID = "IDH_CARLCP" Then
                        doChooseContactPerson(oForm, False, sItemID, sItemID)

                        'analysing Annex Link & Search buttons
                    ElseIf sItemID = "IDH_ANXL01" Then
                        doChooseAnnex(oForm, False, "IDH_ANX01", "1")
                    ElseIf sItemID = "IDH_ANXL02" Then
                        doChooseAnnex(oForm, False, "IDH_ANX01", "2")
                    ElseIf sItemID = "IDH_ANXL03" Then
                        doChooseAnnex(oForm, False, "IDH_ANX01", "3")
                    ElseIf sItemID = "IDH_ANXL04" Then
                        doChooseAnnex(oForm, False, "IDH_ANX01", "4")
                    ElseIf sItemID = "IDH_ANXL05" Then
                        doChooseAnnex(oForm, False, "IDH_ANX01", "5")
                    ElseIf sItemID = "IDH_ANXL06" Then
                        doChooseAnnex(oForm, False, "IDH_ANX01", "6")
                    ElseIf sItemID = "IDH_ANXL07" Then
                        doChooseAnnex(oForm, False, "IDH_ANX07", "7")
                    ElseIf sItemID = "IDH_ANXL08" Then
                        doChooseAnnex(oForm, False, "IDH_ANX08", "8")
                    ElseIf sItemID = "IDH_ANXL09" Then
                        doChooseAnnex(oForm, False, "IDH_ANX09", "9")
                    ElseIf sItemID = "IDH_ANXL10" Then
                        doChooseAnnex(oForm, False, "IDH_ANX10", "10")
                    ElseIf sItemID = "IDH_ANXL11" Then
                        doChooseAnnex(oForm, False, "IDH_ANX11", "11")
                    ElseIf sItemID = "IDH_ANXL12" Then
                        doChooseAnnex(oForm, False, "IDH_ANX12", "12")
                    ElseIf sItemID = "IDH_ANXL13" Then
                        doChooseAnnex(oForm, False, "IDH_ANX13", "13")
                    ElseIf sItemID = "IDH_ANXL14" Then
                        doChooseAnnex(oForm, False, "IDH_ANX14", "14")
                    ElseIf sItemID = "IDH_ANXL15" Then
                        doChooseAnnex(oForm, False, "IDH_ANX15", "15")
                    ElseIf sItemID = "IDH_ANXL16" Then
                        doChooseAnnex(oForm, False, "IDH_ANX16", "16")
                    ElseIf sItemID = "IDH_ANXL17" Then
                        doChooseAnnex(oForm, False, "IDH_ANX17", "17")
                    ElseIf sItemID = "IDH_ANXL18" Then
                        doChooseAnnex(oForm, False, "IDH_ANX18", "18")
                    ElseIf sItemID = "IDH_ANXL19" Then
                        doChooseAnnex(oForm, False, "IDH_ANX19", "19")
                    ElseIf sItemID = "IDH_ANXL20" Then
                        doChooseAnnex(oForm, False, "IDH_ANX20", "20")
                    ElseIf sItemID = "IDH_LKBAX1" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd As String = getFormDFValue(oForm, "U_AnxCd1")
                                setSharedData(oForm, "TRG", "IDH_LKBAX1")
                                setSharedData(oForm, "ANEXCD", sAnxCd)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDH_LKBAX2" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd2 As String = getFormDFValue(oForm, "U_AnxCd2")
                                setSharedData(oForm, "TRG", "IDH_LKBAX2")
                                setSharedData(oForm, "ANEXCD", sAnxCd2)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDH_LKBAX3" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd3 As String = getFormDFValue(oForm, "U_AnxCd3")
                                setSharedData(oForm, "TRG", "IDH_LKBAX3")
                                setSharedData(oForm, "ANEXCD", sAnxCd3)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDH_LKBAX4" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd4 As String = getFormDFValue(oForm, "U_AnxCd4")
                                setSharedData(oForm, "TRG", "IDH_LKBAX4")
                                setSharedData(oForm, "ANEXCD", sAnxCd4)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDH_LKBAX5" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd5 As String = getFormDFValue(oForm, "U_AnxCd5")
                                setSharedData(oForm, "TRG", "IDH_LKBAX5")
                                setSharedData(oForm, "ANEXCD", sAnxCd5)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDH_LKBAX6" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd6 As String = getFormDFValue(oForm, "U_AnxCd6")
                                setSharedData(oForm, "TRG", "IDH_LKBAX6")
                                setSharedData(oForm, "ANEXCD", sAnxCd6)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDH_LKBAX7" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd7 As String = getFormDFValue(oForm, "U_AnxCd7")
                                setSharedData(oForm, "TRG", "IDH_LKBAX7")
                                setSharedData(oForm, "ANEXCD", sAnxCd7)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDH_LKBAX8" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd8 As String = getFormDFValue(oForm, "U_AnxCd8")
                                setSharedData(oForm, "TRG", "IDH_LKBAX8")
                                setSharedData(oForm, "ANEXCD", sAnxCd8)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDH_LKBAX9" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd9 As String = getFormDFValue(oForm, "U_AnxCd9")
                                setSharedData(oForm, "TRG", "IDH_LKBAX9")
                                setSharedData(oForm, "ANEXCD", sAnxCd9)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDHLKBAX10" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd10 As String = getFormDFValue(oForm, "U_AnxCd10")
                                setSharedData(oForm, "TRG", "IDHLKBAX10")
                                setSharedData(oForm, "ANEXCD", sAnxCd10)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDHLKBAX11" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd11 As String = getFormDFValue(oForm, "U_AnxCd11")
                                setSharedData(oForm, "TRG", "IDHLKBAX11")
                                setSharedData(oForm, "ANEXCD", sAnxCd11)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDHLKBAX12" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd12 As String = getFormDFValue(oForm, "U_AnxCd12")
                                setSharedData(oForm, "TRG", "IDHLKBAX12")
                                setSharedData(oForm, "ANEXCD", sAnxCd12)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDHLKBAX13" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd13 As String = getFormDFValue(oForm, "U_AnxCd13")
                                setSharedData(oForm, "TRG", "IDHLKBAX13")
                                setSharedData(oForm, "ANEXCD", sAnxCd13)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDHLKBAX14" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd14 As String = getFormDFValue(oForm, "U_AnxCd14")
                                setSharedData(oForm, "TRG", "IDHLKBAX14")
                                setSharedData(oForm, "ANEXCD", sAnxCd14)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDHLKBAX15" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd15 As String = getFormDFValue(oForm, "U_AnxCd15")
                                setSharedData(oForm, "TRG", "IDHLKBAX15")
                                setSharedData(oForm, "ANEXCD", sAnxCd15)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDHLKBAX16" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd16 As String = getFormDFValue(oForm, "U_AnxCd16")
                                setSharedData(oForm, "TRG", "IDHLKBAX16")
                                setSharedData(oForm, "ANEXCD", sAnxCd16)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDHLKBAX17" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd17 As String = getFormDFValue(oForm, "U_AnxCd17")
                                setSharedData(oForm, "TRG", "IDHLKBAX17")
                                setSharedData(oForm, "ANEXCD", sAnxCd17)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDHLKBAX18" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd18 As String = getFormDFValue(oForm, "U_AnxCd18")
                                setSharedData(oForm, "TRG", "IDHLKBAX18")
                                setSharedData(oForm, "ANEXCD", sAnxCd18)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDHLKBAX19" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd19 As String = getFormDFValue(oForm, "U_AnxCd19")
                                setSharedData(oForm, "TRG", "IDHLKBAX19")
                                setSharedData(oForm, "ANEXCD", sAnxCd19)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDHLKBAX20" Then
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Else
                            If getFormDFValue(oForm, "Code").ToString().Length > 0 Then
                                Dim sAnxCd20 As String = getFormDFValue(oForm, "U_AnxCd20")
                                setSharedData(oForm, "TRG", "IDHLKBAX20")
                                setSharedData(oForm, "ANEXCD", sAnxCd20)
                                goParent.doOpenModalForm("IDH_TFSANX", oForm)
                            End If
                        End If
                    ElseIf sItemID = "IDH_DRCDL" Then 'D and R Code Lookup 
                        'Dim oRdoDCode As SAPbouiCOM.OptionBtn = oForm.Items.Item("IDH_OPDCD").Specific
                        'Dim oRdoRCode As SAPbouiCOM.OptionBtn = oForm.Items.Item("IDH_OPRCD").Specific
                        'If oRdoDCode.Selected Then
                        '    'goParent.doOpenModalForm("IDH_WTHZSR", oForm) 
                        '    goParent.doOpenModalForm("IDH_DRCODS", oForm)
                        'ElseIf oRdoRCode.Selected Then
                        '    'goParent.doOpenModalForm("IDH_WTHZSR", oForm)
                        '    goParent.doOpenModalForm("IDH_DRCODS", oForm)
                        'End If
                        goParent.doOpenModalForm("IDH_DRCODS", oForm)
                    ElseIf sItemID = "IDH_EWCL" Then 'EWC Code search 
                        'setSharedData(oForm, "IDH_WTWC", getFormDFValue(oForm, "U_EWCCd"))
                        setSharedData(oForm, "TRG", "IDH_EWCL")
                        goParent.doOpenModalForm("IDH_WTECSR", oForm)
                    ElseIf sItemID = "IDH_EXPEL" Then 'Export EWC Code search 
                        'setSharedData(oForm, "IDH_WTWC", getFormDFValue(oForm, "U_ExpNatCd"))
                        setSharedData(oForm, "TRG", "IDH_EXPEL")
                        goParent.doOpenModalForm("IDH_WTECSR", oForm)
                    ElseIf sItemID = "IDH_IMPEL" Then 'Import EWC Code search 
                        'setSharedData(oForm, "IDH_WTWC", getFormDFValue(oForm, "U_ImpNatCd"))
                        setSharedData(oForm, "TRG", "IDH_IMPEL")
                        goParent.doOpenModalForm("IDH_WTECSR", oForm)
                    ElseIf sItemID = "IDH_YCDLKP" Then
                        'setSharedData(oForm, "IDH_YCOD", getFormDFValue(oForm, "U_YCode"))
                        goParent.doOpenModalForm("IDH_YCDSR", oForm)
                    ElseIf sItemID = "IDH_HCDLKP" Then
                        'setSharedData(oForm, "IDH_HCOD", getFormDFValue(oForm, "U_HCode"))
                        setSharedData(oForm, "TRG", "IDH_HCDLKP")
                        goParent.doOpenModalForm("IDH_HCDSR", oForm)
                        'ElseIf sItemID = "IDH_UNLKP" Then
                        '    'setSharedData(oForm, "IDH_UNNUM", getFormDFValue(oForm, "U_UNNumber"))
                        '    setSharedData(oForm, "TRG", "IDH_UNLKP")
                        '    goParent.doOpenModalForm("IDH_HCDSR", oForm)
                    ElseIf pVal.ItemUID = "IDH_BTNPNT" Then
                        doDocReport(oForm, "IDH_BTNPNT", Config.Parameter("TFSNOTRP"))
                    ElseIf pVal.ItemUID = "IDH_BTNPIN" Then
                        doDocReport(oForm, "IDH_BTNPIN", Config.Parameter("TFSINTRP"))
                    ElseIf pVal.ItemUID = "IDH_BTNPMO" Then
                        doDocReport(oForm, "IDH_BTNPMO", Config.Parameter("TFSMOVRP"))
                    ElseIf pVal.ItemUID = "IDH_PHYL" Then
                        setSharedData(oForm, "CURVALUE", getFormDFValue(oForm, "U_PhyChtr"))
                        goParent.doOpenModalForm("IDHPHYCHR", oForm)
                    ElseIf pVal.ItemUID = "IDH_PKGL" Then
                        setSharedData(oForm, "CURVALUE", getFormDFValue(oForm, "U_PackTyp"))
                        goParent.doOpenModalForm("IDHPKGTYP", oForm)
                    ElseIf pVal.ItemUID = "IDH_TRMDLK" Then
                        setSharedData(oForm, "CURVALUE", getFormDFValue(oForm, "U_TranMode"))
                        goParent.doOpenModalForm("IDHTRNMOD", oForm)
                    ElseIf pVal.ItemUID = "IDH_BSLKP" Then
                        goParent.doOpenModalForm("IDHBASELSC", oForm)
                    ElseIf pVal.ItemUID = "IDH_OELKP" Then
                        goParent.doOpenModalForm("IDHOECDSC", oForm)
                    ElseIf pVal.ItemUID = "IDH_UNCLKP" Then
                        goParent.doOpenModalForm("IDH_WTUNSR", oForm)
                    ElseIf pVal.ItemUID = "IDH_UNLKP" Then
                        goParent.doOpenModalForm("IDH_PSNMSR", oForm)
                    End If
                End If
            End If
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_CLICK Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_OPCOY" Or pVal.ItemUID = "IDH_OPCON" Then
                        Dim sSDtFrom As String = getFormDFValue(oForm, "U_SDtFrm")
                        Dim dSDtFrom As Date = com.idh.utils.Dates.doStrToDate(sSDtFrom)
                        Dim sSDtTo As String = getFormDFValue(oForm, "U_SDtTo")
                        Dim dSDtTo As Date = com.idh.utils.Dates.doStrToDate(sSDtTo)
                        Dim sMsg As String = String.Empty

                        If sSDtFrom IsNot Nothing AndAlso (Not sSDtFrom.Length > 0) Then
                            dSDtFrom = Date.Today
                        End If

                        If pVal.ItemUID = "IDH_OPCOY" Then
                            dSDtTo = dSDtFrom.AddDays(1094)
                            sMsg = "Last Departure Date set from first departure date to 3yrs."
                        ElseIf pVal.ItemUID = "IDH_OPCON" Then
                            dSDtTo = dSDtFrom.AddDays(364)
                            setFormDFValue(oForm, "U_SDtTo", dSDtTo)
                            sMsg = "Last Departure Date set from first departure date to 1yr."
                        End If

                        setFormDFValue(oForm, "U_SDtFrm", dSDtFrom)
                        setFormDFValue(oForm, "U_SDtTo", dSDtTo)
                        doWarnMess(sMsg)
                    ElseIf pVal.ItemUID = "IDH_OPTON" Or pVal.ItemUID = "IDH_OPCMT" Then
                        Dim oQtyTon As SAPbouiCOM.Item = oForm.Items.Item("IDH_QTYTON")
                        Dim oQtyCM As SAPbouiCOM.Item = oForm.Items.Item("IDH_QTYCM")
                        'Dim oFocusIntQty As SAPbouiCOM.Item = oForm.Items.Item("IDH_QTYINT")

                        'Dim oEdit As SAPbouiCOM.EditText

                        If pVal.ItemUID = "IDH_OPTON" Then
                            doSetFocus(oForm, "IDH_QTYINT")
                            oQtyTon.Visible = True
                            'oQtyCM.Enabled = False
                            oQtyCM.Visible = False
                        Else
                            doSetFocus(oForm, "IDH_QTYINT")
                            'oQtyTon.Enabled = False
                            oQtyTon.Visible = False
                            oQtyCM.Visible = True
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                If pVal.BeforeAction = False AndAlso pVal.ItemUID = "IDH_TFSTYP" AndAlso pVal.ItemChanged Then
                    doSetInterimFacilityVisability(oForm)
                End If

            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE Then
                If pVal.BeforeAction Then

                Else
                    Dim sTFSNUM As String = getParentSharedData(oForm, "TFSNUM")
                    If sTFSNUM IsNot Nothing AndAlso sTFSNUM.Length > 0 AndAlso oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        goParent.goApplication.ActivateMenuItem("1281")
                    End If
                End If
            End If
            'Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            Return True
        End Function

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Dim bIsTFSValid As Boolean = False
            Dim saParam As String() = {""}
            If pVal.BeforeAction = True Then
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then

                    If isValidTFSNumber(getFormDFValue(oForm, "U_TFSNum")) Then
                        bIsTFSValid = True
                    Else
                        'Dim saParam As String() = {""}
                        doResError("Selected TFS Number not valid! Please choose an active TFS Number.", "INVLTFSN", saParam)
                        Exit Sub
                    End If

                    Dim sSpHandleFlag As String = getFormDFValue(oForm, "U_SpHandle")
                    Dim sSpHDetails As String = getFormDFValue(oForm, "U_SpHDetails")
                    If sSpHandleFlag.Equals("1") AndAlso sSpHDetails.Length < 1 Then
                        doResError("Please provide Special Handling details for the point: 7.", "TFSMISIN", saParam)
                        'doWarnMess("Please provide Special Handling details for the point: 7!", SAPbouiCOM.BoMessageTime.bmt_Medium)
                        Exit Sub
                    Else
                        bIsTFSValid = True
                    End If

                    'another validation 
                    If bIsTFSValid Then

                    End If

                    If bIsTFSValid Then
                        Dim sCode As String = getFormDFValue(oForm, "Code").ToString()
                        If sCode Is Nothing OrElse sCode.Length = 0 Then
                            'sCode = DataHandler.doGenerateCode(Nothing, "TFSMST")
                            Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, "TFSMST")
                            setFormDFValue(oForm, "Code", oNumbers.CodeCode)
                        End If

                        'Setting Annex Codes if Annex Names are null 
                        For i As Integer = 1 To 20
                            If (getFormDFValue(oForm, "U_AnxDesc" + i.ToString()).ToString().Length = 0) Then
                                setFormDFValue(oForm, "U_AnxCd" + i.ToString(), "")
                            End If
                        Next
                        'If (getFormDFValue(oForm, "U_AnxDesc1").ToString().Length = 0) Then
                        '    setFormDFValue(oForm, "U_AnxCd1", "")
                        'End If
                        'If (getFormDFValue(oForm, "U_AnxDesc2").ToString().Length = 0) Then
                        '    setFormDFValue(oForm, "U_AnxCd2", "")
                        'End If
                        'If (getFormDFValue(oForm, "U_AnxDesc3").ToString().Length = 0) Then
                        '    setFormDFValue(oForm, "U_AnxCd3", "")
                        'End If
                        'If (getFormDFValue(oForm, "U_AnxDesc4").ToString().Length = 0) Then
                        '    setFormDFValue(oForm, "U_AnxCd4", "")
                        'End If
                        'If (getFormDFValue(oForm, "U_AnxDesc5").ToString().Length = 0) Then
                        '    setFormDFValue(oForm, "U_AnxCd5", "")
                        'End If
                        'If (getFormDFValue(oForm, "U_AnxDesc6").ToString().Length = 0) Then
                        '    setFormDFValue(oForm, "U_AnxCd6", "")
                        'End If

                        If doUpdateHeader(oForm, True) Then
                            doUpdateGridRows(oForm)
                            oForm.Update()
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                            BubbleEvent = False
                        End If
                        oForm.Refresh()
                    End If

                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    Dim sCode As String = "-1"

                    If isValidTFSNumber(getFormDFValue(oForm, "U_TFSNum")) Then
                        sCode = getFormDFValue(oForm, "Code").ToString()
                        bIsTFSValid = True
                    Else
                        'doResError("Selected TFS Number not valid! Please choose an active TFS Number.", "INVLTFSN", saParam)
                        doWarnMess(com.idh.bridge.Translation.getTranslatedWord("Selected TFS Number not valid! Please choose an active TFS Number!"))
                        Exit Sub
                    End If

                    Dim sSpHandleFlag As String = getFormDFValue(oForm, "U_SpHandle")
                    Dim sSpHDetails As String = getFormDFValue(oForm, "U_SpHDetails")
                    If sSpHandleFlag.Equals("1") AndAlso sSpHDetails.Length < 1 Then
                        'doResError("Please provide Special Handling details for the point: 7.", "TFSMISIN", saParam)
                        doWarnMess(com.idh.bridge.Translation.getTranslatedWord("Please provide Special Handling details for the point: 7!"), SAPbouiCOM.BoMessageTime.bmt_Medium)
                        Exit Sub
                    Else
                        bIsTFSValid = True
                    End If

                    If bIsTFSValid Then
                        'Setting Annex Codes if Annex Names are null 
                        For i As Integer = 1 To 20
                            If (getFormDFValue(oForm, "U_AnxDesc" + i.ToString()).ToString().Length = 0) Then
                                setFormDFValue(oForm, "U_AnxCd" + i.ToString(), "")
                            End If
                        Next
                        'If (getFormDFValue(oForm, "U_AnxDesc1").ToString().Length = 0) Then
                        '    setFormDFValue(oForm, "U_AnxCd1", "")
                        'End If
                        'If (getFormDFValue(oForm, "U_AnxDesc2").ToString().Length = 0) Then
                        '    setFormDFValue(oForm, "U_AnxCd2", "")
                        'End If
                        'If (getFormDFValue(oForm, "U_AnxDesc3").ToString().Length = 0) Then
                        '    setFormDFValue(oForm, "U_AnxCd3", "")
                        'End If
                        'If (getFormDFValue(oForm, "U_AnxDesc4").ToString().Length = 0) Then
                        '    setFormDFValue(oForm, "U_AnxCd4", "")
                        'End If
                        'If (getFormDFValue(oForm, "U_AnxDesc5").ToString().Length = 0) Then
                        '    setFormDFValue(oForm, "U_AnxCd5", "")
                        'End If
                        'If (getFormDFValue(oForm, "U_AnxDesc6").ToString().Length = 0) Then
                        '    setFormDFValue(oForm, "U_AnxCd6", "")
                        'End If

                        If doUpdateHeader(oForm, False) Then
                            doUpdateGridRows(oForm)
                            oForm.Update()
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                            BubbleEvent = False
                        End If
                    End If
                ElseIf pVal.FormMode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    BubbleEvent = False
                    doFind(oForm)
                ElseIf pVal.FormMode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    doReturnFromModalShared(oForm, True)
                End If
            Else
            End If

        End Sub

        Protected Function doFind(ByVal oForm As SAPbouiCOM.Form) As Boolean
            oForm.Freeze(True)

            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    Dim sTFSCode As String = getFormDFValue(oForm, "Code", True)
                    Dim sTFSNum As String = getFormDFValue(oForm, "U_TFSNum", True)

                    Dim sWhereClause As String = Nothing

                    If sTFSCode IsNot Nothing AndAlso sTFSCode.Length > 0 Then
                        If sWhereClause IsNot Nothing AndAlso sWhereClause.Length > 0 Then
                            sWhereClause = sWhereClause + " AND Code = " + sTFSCode
                        Else
                            sWhereClause = " Code = " + sTFSCode
                        End If
                    End If

                    If sTFSNum IsNot Nothing AndAlso sTFSNum.Length > 0 Then
                        If sWhereClause IsNot Nothing AndAlso sWhereClause.Length > 0 Then
                            sWhereClause = sWhereClause + " AND U_TFSNum = '" + sTFSNum + "'"
                        Else
                            sWhereClause = " U_TFSNum = '" + sTFSNum + "'"
                        End If
                    End If

                    Dim sQry As String = "select * from [@IDH_TFSMAST] where " + sWhereClause
                    oRecordSet = goParent.goDB.doSelectQuery(sQry)
                    If oRecordSet IsNot Nothing AndAlso oRecordSet.RecordCount > 0 Then
                        Dim db As SAPbouiCOM.DBDataSource
                        db = oForm.DataSources.DBDataSources.Item("@IDH_TFSMAST")

                        sTFSCode = oRecordSet.Fields.Item("Code").Value.Trim()
                        db.Clear()

                        Dim oConditions As SAPbouiCOM.Conditions = New SAPbouiCOM.Conditions
                        Dim oCondition As SAPbouiCOM.Condition = oConditions.Add
                        oCondition.Alias = "Code"
                        oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                        oCondition.CondVal = sTFSCode

                        db.Query(oConditions)

                        oForm.Update()

                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        doLoadData(oForm)
                        Return True
                    Else
                        doWarnMess("TFS Code not found.", SAPbouiCOM.BoMessageTime.bmt_Medium)
                        Return False
                    End If
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error finding the Waste Order.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFWO", {Nothing})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
                oForm.Freeze(False)
            End Try

            Return False
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                Dim sError As String = String.Empty

                If sModalFormType.Equals("IDHCSRCH") Then
                    Dim sTrg As String = getSharedData(oForm, "TRG")
                    Dim saParam As String() = {sTrg}
                    Dim sCustCd As String = getSharedData(oForm, "CARDCODE")
                    Dim sCustName As String = getSharedData(oForm, "CARDNAME")

                    If sTrg = "IDH_NOTL" Then
                        If isValidBPTFSRegNo(oForm, sCustCd) Then
                            setFormDFValue(oForm, "U_NOTCd", sCustCd, False, True)
                            setFormDFValue(oForm, "U_NOTName", sCustName)
                            doSetNotifierAddress(oForm, sCustCd, sCustName)
                        Else
                            sError = "Selected BP TFS Registration is expired! Select a BP with valid registration no."
                        End If
                    ElseIf sTrg = "IDH_CONL" Then
                        If isValidBPTFSRegNo(oForm, sCustCd) Then
                            setFormDFValue(oForm, "U_CONCd", sCustCd, False, True)
                            setFormDFValue(oForm, "U_CONName", sCustName)
                            doSetConsignorAddress(oForm)
                        Else
                            sError = "Selected BP TFS Registration is expired! Select a BP with valid registration no."
                        End If
                    ElseIf sTrg = "IDH_COEL" Then
                        If isValidBPTFSRegNo(oForm, sCustCd) Then
                            setFormDFValue(oForm, "U_COECd", sCustCd, False, True)
                            setFormDFValue(oForm, "U_COEName", sCustName)
                            doSetConsigneeAddress(oForm)
                        Else
                            sError = "Selected BP TFS Registration is expired! Select a BP with valid registration no."
                        End If
                    ElseIf sTrg = "IDH_CARL" Then
                        If isValidBPTFSRegNo(oForm, sCustCd) Then
                            setFormDFValue(oForm, "U_CARCd", sCustCd, False, True)
                            setFormDFValue(oForm, "U_CARName", sCustName)
                            doSetCarrierAddress(oForm)
                        Else
                            sError = "Selected BP TFS Registration is expired! Select a BP with valid registration no."
                        End If
                    ElseIf sTrg = "IDH_GENL" Then
                        If isValidBPTFSRegNo(oForm, sCustCd) Then
                            setFormDFValue(oForm, "U_GENCd", sCustCd, False, True)
                            setFormDFValue(oForm, "U_GENName", sCustName)
                            doSetGeneratorAddress(oForm)
                        Else
                            sError = "Selected BP TFS Registration is expired! Select a BP with valid registration no."
                        End If
                    ElseIf sTrg = "IDH_DSPL" Then
                        If isValidBPTFSRegNo(oForm, sCustCd) Then
                            setFormDFValue(oForm, "U_DSPCd", sCustCd, False, True)
                            setFormDFValue(oForm, "U_DSPName", sCustName)
                            doSetDisposalFacilityAddress(oForm)
                        Else
                            sError = "Selected BP TFS Registration is expired! Select a BP with valid registration no."
                        End If
                    ElseIf sTrg = "IDH_FNDL" Then
                        If isValidBPTFSRegNo(oForm, sCustCd) Then
                            setFormDFValue(oForm, "U_FDSCd", sCustCd, False, True)
                            setFormDFValue(oForm, "U_FDSName", sCustName)
                            doSetFinalDestinationAddress(oForm)
                        Else
                            sError = "Selected BP TFS Registration is expired! Select a BP with valid registration no."
                        End If
                    ElseIf sTrg = "IDH_FFL" Then
                        If isValidBPTFSRegNo(oForm, sCustCd) Then
                            setFormDFValue(oForm, "U_FFCd", sCustCd, False, True)
                            setFormDFValue(oForm, "U_FFName", sCustName)
                            doSetFirstFacilityAddress(oForm)
                        Else
                            sError = "Selected BP TFS Registration is expired! Select a BP with valid registration no."
                        End If
                    End If
                    If sError.Length > 0 Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError("INVALID BP: " & sError)
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("INVALID BP: " & sError, "ERUSIBPS", {com.idh.bridge.Translation.getTranslatedWord(sError)})
                    End If

                ElseIf sModalFormType.Equals("IDHTFSNSR") Then
                    Dim sTrg As String = getSharedData(oForm, "TRG")
                    If sTrg = "IDH_TFSNL" Then
                        If isValidTFSNumber(getSharedData(oForm, "IDH_TFSNUM")) Then
                            setFormDFValue(oForm, "U_TFSNum", getSharedData(oForm, "IDH_TFSNUM"), False, True)
                        Else
                            'com.idh.bridge.DataHandler.INSTANCE.doError("Selected TFS Number not valid! Please choose an active TFS Number.")
                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Selected TFS Number not valid! Please choose an active TFS Number.", "ERUSSTFS", {Nothing})
                        End If
                    End If
                ElseIf sModalFormType.Equals("IDHASRCH") Then
                    Dim sTrg As String = getSharedData(oForm, "TRG")
                    Dim sCustCd As String = getSharedData(oForm, "CARDCODE")
                    Dim sCustName As String = getSharedData(oForm, "CARDNAME")

                    'Get address details from Address Search Modal Form 
                    Dim oArr As ArrayList = New ArrayList
                    oArr.Add(getSharedData(oForm, "ADDRESS"))
                    oArr.Add(getSharedData(oForm, "TEL1"))
                    oArr.Add(getSharedData(oForm, "FAX"))
                    oArr.Add(getSharedData(oForm, "EMAIL"))

                    'If sTrg = "IDH_NOTL" Then
                    '    doResetNotifierAddress(oForm, oArr)
                    'ElseIf sTrg = "IDH_CONL" Then
                    'End If
                    Select Case sTrg
                        Case "IDH_NOTADD"
                            doResetBPAddress(oForm, oArr, "NOT")
                        Case "IDH_CONADD"
                            doResetBPAddress(oForm, oArr, "CON")
                        Case "IDH_COEADD"
                            doResetBPAddress(oForm, oArr, "COE")
                        Case "IDH_GENADD"
                            doResetBPAddress(oForm, oArr, "GEN")
                        Case "IDH_DSPADD"
                            doResetBPAddress(oForm, oArr, "DOF")
                        Case "IDH_FDTADD"
                            doResetBPAddress(oForm, oArr, "FDT")
                        Case "IDH_FFADD"
                            doResetBPAddress(oForm, oArr, "FF")
                        Case "IDH_CARADD"
                            doResetBPAddress(oForm, oArr, "CAR")
                    End Select
                ElseIf sModalFormType.Equals("IDHBPCSRC") Then
                    Dim sTrg As String = getSharedData(oForm, "TRG")
                    Dim sCustCd As String = getSharedData(oForm, "BPCODE")
                    Dim sBPContactName As String = getSharedData(oForm, "FNAME") + " " + getSharedData(oForm, "LNAME")
                    Dim sTel1 As String = getSharedData(oForm, "CTEL1")
                    Dim sFax As String = getSharedData(oForm, "CFAX")
                    Dim sEmail As String = getSharedData(oForm, "CEMAIL")


                    Select Case sTrg
                        Case "IDH_NOTCPR"
                            doResetBPContact(oForm, "NOT", sBPContactName, sTel1, sFax, sEmail)
                        Case "IDH_CONCPR"
                            doResetBPContact(oForm, "CON", sBPContactName, sTel1, sFax, sEmail)
                        Case "IDH_COECPR"
                            doResetBPContact(oForm, "COE", sBPContactName, sTel1, sFax, sEmail)
                        Case "IDH_GENCPR"
                            doResetBPContact(oForm, "GEN", sBPContactName, sTel1, sFax, sEmail)
                        Case "IDH_GENSIG"
                            'doResetBPContact(oForm, sBPContactName, "FF")
                            setFormDFValue(oForm, "U_FFSCnt", sBPContactName)
                            'setFormDFValue(oForm, "U_FFSCnt", sBPContactName)
                            'setFormDFValue(oForm, "U_FFSCnt", sBPContactName)
                            'setFormDFValue(oForm, "U_FFSCnt", sBPContactName)
                            ', sBPContactName, sTel1, sFax, sEmail)

                            doSetUpdate(oForm)
                        Case "IDH_NOTSIG"
                            setFormDFValue(oForm, "U_NotSign", sBPContactName)
                        Case "IDH_DSPCPR"
                            doResetBPContact(oForm, "DOF", sBPContactName, sTel1, sFax, sEmail)
                        Case "IDH_FDTCPR"
                            doResetBPContact(oForm, "FDT", sBPContactName, sTel1, sFax, sEmail)
                        Case "IDH_FFCPR"
                            doResetBPContact(oForm, "FF", sBPContactName, sTel1, sFax, sEmail)
                        Case "IDH_CARCPR"
                            doResetBPContact(oForm, "CAR", sBPContactName, sTel1, sFax, sEmail)
                    End Select
                ElseIf sModalFormType.Equals("IDHANXS") Then
                    Dim sTrg As String = getSharedData(oForm, "TRG")
                    Dim sAnxFieldID As String = getSharedData(oForm, "ANXFIELDID")
                    Dim sAnxCd As String = getSharedData(oForm, "ANXCD")
                    Dim sAnxNo As String = getSharedData(oForm, "ANXNO")

                    If sAnxCd.Length > 0 And sAnxNo.Length > 0 Then
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                        setFormDFValue(oForm, "U_AnxCd" + sAnxFieldID, getSharedData(oForm, "ANXCD"))
                        setFormDFValue(oForm, "U_AnxDesc" + sAnxFieldID, getSharedData(oForm, "ANXNO"))
                    End If
                ElseIf sModalFormType = "IDH_DRCODS" Then
                    'setItemValue(oForm, "IDH_DRCODE", getSharedData(oForm, "IDH_WTWCO"), False)
                    Dim sDRCd As String = getFormDFValue(oForm, "U_DCode")
                    If sDRCd IsNot Nothing AndAlso sDRCd.Length > 0 Then
                        sDRCd = sDRCd + "," + getSharedData(oForm, "IDH_WTWCO")
                    Else
                        sDRCd = getSharedData(oForm, "IDH_WTWCO")
                    End If
                    setFormDFValue(oForm, "U_DCode", sDRCd)
                ElseIf sModalFormType = "IDH_WTECSR" Then
                    Dim sEWCCd As String '= getFormDFValue(oForm, "U_EWCCd")
                    Dim sWTWCO As String = getSharedData(oForm, "IDH_WTWCO")
                    Dim sTrg As String = getSharedData(oForm, "TRG")
                    If sTrg = "IDH_EWCL" Then
                        sEWCCd = getFormDFValue(oForm, "U_EWCCd")
                        If sEWCCd IsNot Nothing AndAlso sEWCCd.Length > 0 Then
                            sEWCCd = sEWCCd + "," + sWTWCO
                        Else
                            sEWCCd = sWTWCO
                        End If
                        setFormDFValue(oForm, "U_EWCCd", sEWCCd)
                    ElseIf sTrg = "IDH_EXPEL" Then
                        sEWCCd = getFormDFValue(oForm, "U_ExpNatCd")
                        If sEWCCd IsNot Nothing AndAlso sEWCCd.Length > 0 Then
                            sEWCCd = sEWCCd + "," + sWTWCO
                        Else
                            sEWCCd = sWTWCO
                        End If
                        setFormDFValue(oForm, "U_ExpNatCd", sEWCCd)
                    ElseIf sTrg = "IDH_IMPEL" Then
                        sEWCCd = getFormDFValue(oForm, "U_ImpNatCd")
                        If sEWCCd IsNot Nothing AndAlso sEWCCd.Length > 0 Then
                            sEWCCd = sEWCCd + "," + sWTWCO
                        Else
                            sEWCCd = sWTWCO
                        End If
                        setFormDFValue(oForm, "U_ImpNatCd", sEWCCd)
                    End If
                ElseIf sModalFormType = "IDH_HCDSR" Then
                    Dim sHCd As String = getSharedData(oForm, "IDH_HCODE")
                    Dim sTrg As String = getSharedData(oForm, "TRG")
                    If sTrg = "IDH_HCDLKP" Then
                        Dim sExistingHCode As String = getFormDFValue(oForm, "U_HCode")
                        If sExistingHCode IsNot Nothing AndAlso sExistingHCode.Length > 0 Then
                            sExistingHCode = sExistingHCode + "," + sHCd
                        Else
                            sExistingHCode = sHCd
                        End If
                        setFormDFValue(oForm, "U_HCode", sExistingHCode)
                        'ElseIf sTrg = "IDH_UNLKP" Then
                        '    Dim sExistingUNN As String = getFormDFValue(oForm, "U_UNNumber")
                        '    If sExistingUNN IsNot Nothing AndAlso sExistingUNN.Length > 0 Then
                        '        sExistingUNN = sExistingUNN + "," + sHCd
                        '    Else
                        '        sExistingUNN = sHCd
                        '    End If
                        '    setFormDFValue(oForm, "U_UNNumber", sExistingUNN)
                    End If
                ElseIf sModalFormType = "IDH_YCDSR" Then
                    Dim sYCd As String = getSharedData(oForm, "IDH_YCODE")
                    Dim sExistingYCode As String = getFormDFValue(oForm, "U_YCode")
                    If sExistingYCode IsNot Nothing AndAlso sExistingYCode.Length > 0 Then
                        sExistingYCode = sExistingYCode + "," + sYCd
                    Else
                        sExistingYCode = sYCd
                    End If
                    setFormDFValue(oForm, "U_YCode", sExistingYCode)
                ElseIf sModalFormType = "IDHPHYCHR" Then
                    Dim sPhyChr As String = getSharedData(oForm, "IDHPHYCHR")
                    setFormDFValue(oForm, "U_PhyChtr", sPhyChr, False, True)
                ElseIf sModalFormType = "IDHPKGTYP" Then
                    Dim sPkgTyp As String = getSharedData(oForm, "IDHPKGTYP")
                    setFormDFValue(oForm, IDH_TFSMAST._PackTyp, sPkgTyp, False, True)
                ElseIf sModalFormType = "IDHTRNMOD" Then
                    Dim sTrnMd As String = getSharedData(oForm, "IDHTRNMD")
                    setFormDFValue(oForm, "U_TranMode", sTrnMd, False, True)
                ElseIf sModalFormType = "IDHBASELSC" Then
                    setFormDFValue(oForm, "U_BaselCd", getSharedData(oForm, "IDHBSCD"), False, True)
                ElseIf sModalFormType = "IDHOECDSC" Then
                    setFormDFValue(oForm, "U_OECDCd", getSharedData(oForm, "IDHOECD"), False, True)
                ElseIf sModalFormType = "IDH_WTUNSR" Then
                    setFormDFValue(oForm, "U_UNClass", getSharedData(oForm, "IDH_UNCODE"), False, True)
                ElseIf sModalFormType = "IDH_PSNMSR" Then
                    setFormDFValue(oForm, "U_UNNumber", getSharedData(oForm, "IDH_UNNUM"), False, True)
                    setFormDFValue(oForm, "U_UNShipNm", getSharedData(oForm, "IDH_PSHPNM"), False, True)
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error handeling Modal results - " & sModalFormType)
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
            'MyBase.doHandleModalResultShared(oForm, sModalFormType, sLastButton)

        End Sub

        Public Overrides Function doCustomItemEvent(oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            'If MyBase.doCustomItemEvent(oForm, pVal) = False Then
            '    Return False
            'End If
            If pVal.ItemUID = "MOVEGR" OrElse pVal.ItemUID = "CONTGR" OrElse pVal.ItemUID = "DOWNTGR" Then
                If pVal.BeforeAction = False Then
                    oForm.Freeze(True)
                    Dim oGrid As DBOGrid = getWFValue(oForm, pVal.ItemUID)
                    If Not oGrid Is Nothing Then
                        If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_ADD_EMPTY Then
                            ''
                        ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED Then
                            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE _
                                    OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                doSetUpdate(oForm)
                            End If

                            ''Calculate the Clock Hours column in Employee grid 
                            'If pVal.ItemUID = "MOVEGR" Then
                            '    If pVal.ColUID = IDH_RCEMPLOYEE._RCCLOCKIN OrElse pVal.ColUID = IDH_RCEMPLOYEE._RCCLOCKOUT Then
                            '        Dim iClockIN As Int16 = pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_RCEMPLOYEE._RCCLOCKIN), pVal.Row)
                            '        Dim iClockOUT As Int16 = pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_RCEMPLOYEE._RCCLOCKOUT), pVal.Row)
                            '        If iClockOUT >= iClockIN Then
                            '            Dim iClkHours As Int16 = iClockOUT - iClockIN
                            '            pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_RCEMPLOYEE._RCTHOUR1), pVal.Row, iClkHours)
                            '        End If
                            '    End If
                            'End If
                        ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SEARCH Then
                        ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
                        ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY Then
                            Dim sTFSCode As String = getItemValue(oForm, "IDH_CODE").ToString.Trim
                            If sTFSCode = "" AndAlso oForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                Return False
                            End If
                            doSetLastLine(oForm, pVal.Row, pVal.ItemUID)
                        End If
                    End If
                    oForm.Freeze(False)
                End If
            End If
            Return MyBase.doCustomItemEvent(oForm, pVal)
        End Function

        'Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean

        '    If pVal.ItemUID = "MOVEGR" Then
        '        Dim oGrid As WR1_Grids.idh.controls.grid.TFSMovements = WR1_Grids.idh.controls.grid.TFSMovements.getInstance(oForm, "MOVEGR")
        '        If Not oGrid Is Nothing Then
        '            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
        '                If pVal.BeforeAction = True Then
        '                    Dim oGridN As UpdateGrid = pVal.oGrid
        '                    Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows
        '                    Dim iSelectionCount As Integer = oSelected.Count

        '                    If iSelectionCount = 1 Then
        '                        Dim oMenuItem As SAPbouiCOM.MenuItem
        '                        Dim oMenus As SAPbouiCOM.Menus
        '                        Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
        '                        Dim sSelectedForcast As String
        '                        Dim iRow As Integer
        '                        Dim sTFSNr As String
        '                        Dim sTFSMess As String
        '                        Dim sWORowNr As String

        '                        'iRow = oSelected.Item(0, SAPbouiCOM.BoOrderType.ot_RowOrder)
        '                        iRow = oGridN.GetDataTableRowIndex(oSelected, 0)
        '                        sTFSNr = getFormDFValue(oForm, "Code")
        '                        sTFSMess = oGridN.doGetFieldValue("Code", iRow)
        '                        sWORowNr = oGridN.doGetFieldValue("U_WRRow", iRow)

        '                        If sWORowNr.Length = 0 Then
        '                            oMenuItem = goParent.goApplication.Menus.Item(IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU)
        '                            oMenus = oMenuItem.SubMenus
        '                            oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        '                            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING

        '                            oCreationPackage.UniqueID = "NEWWOR"
        '                            oCreationPackage.String = getTranslatedWord("Create Waste Order Row from the Message") & " (" & (iRow + 1) & ")[" & sTFSMess & "]"
        '                            oCreationPackage.Enabled = True
        '                            oMenus.AddEx(oCreationPackage)

        '                        End If
        '                    End If
        '                    Return True
        '                Else
        '                    If goParent.goApplication.Menus.Exists("NEWWOR") Then
        '                        goParent.goApplication.Menus.RemoveEx("NEWWOR")
        '                    End If
        '                    Return True
        '                End If
        '            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
        '                If pVal.BeforeAction = False Then
        '                    If pVal.oData.MenuUID.Equals("NEWWOR") Then
        '                        Dim sJobType As String = Config.Parameter("TFSDJTP") '"Delivery"
        '                        Dim sContainerCd As String = Config.Parameter("TFSDCCD") '"Artic"
        '                        Dim sContainerDsc As String = Config.Parameter("TFSDCDS") '"Artic Lorry"
        '                        Dim sItemGroup As String = Config.Parameter("TFSDIGR") '"118"
        '                        Dim sItemGroupDsc As String = Config.Parameter("TFSDIGD") '"Other"
        '                        Dim sWasteCode As String = Config.Parameter("TFSDWCD") '"GEN001"
        '                        Dim sWasteDsc As String = Config.Parameter("TFSDWDS") '"General Waste"

        '                        'If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
        '                        'oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse _
        '                        'oForm.Mode = SAPbouiCOM.BoFormMode.fm_EDIT_MODE Then
        '                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
        '                            com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage("UNSAVED")
        '                        Else
        '                            Dim oSelected As SAPbouiCOM.SelectedRows = pVal.oGrid.getGrid().Rows.SelectedRows
        '                            Dim iRow As Integer = pVal.oGrid.GetDataTableRowIndex(oSelected, 0)
        '                            Dim sTFSNr As String
        '                            Dim sTFSMove As String
        '                            Dim sWONr As String
        '                            Dim sWORowNr As String

        '                            sTFSNr = getFormDFValue(oForm, "Code")
        '                            sTFSMove = oGrid.doGetFieldValue("Code", iRow)
        '                            sWONr = getFormDFValue(oForm, "U_WROrd")
        '                            sWORowNr = oGrid.doGetFieldValue("U_WRRow", iRow)

        '                            'Check And Create the Linked WO
        '                            Dim oWO As com.idh.dbObjects.User.IDH_JOBENTR = New com.idh.dbObjects.User.IDH_JOBENTR()
        '                            If sWONr.Length = 0 Then
        '                                oWO.doApplyDefaults()
        '                                'oWO.doSetCustomer(sCustomer, False)
        '                                'oWO.doSetDisposalSite(sDisposalSite, False)
        '                                'oWO.doSetProducer(sProducer, False)
        '                                'oWO.doSetCarrier(sCarrier, False)

        '                                oWO.doSetCustomer(getFormDFValue(oForm, IDH_TFSMAST._NOTCd))

        '                                oWO.U_Address = getFormDFValue(oForm, IDH_TFSMAST._ConAdres)
        '                                oWO.U_Contact = getFormDFValue(oForm, IDH_TFSMAST._ConCntPr)
        '                                oWO.U_Phone1 = getFormDFValue(oForm, IDH_TFSMAST._ConPh)

        '                                oWO.doSetDisposalSite(getFormDFValue(oForm, IDH_TFSMAST._DSPCd))
        '                                oWO.U_SAddress = getFormDFValue(oForm, IDH_TFSMAST._DOFAdres)
        '                                oWO.U_SContact = getFormDFValue(oForm, IDH_TFSMAST._DOFCntPr)
        '                                oWO.U_SPhone1 = getFormDFValue(oForm, IDH_TFSMAST._DOFPh)

        '                                oWO.doSetProducer(getFormDFValue(oForm, IDH_TFSMAST._GENCd))
        '                                oWO.U_PAddress = getFormDFValue(oForm, IDH_TFSMAST._GenAdres)
        '                                oWO.U_PContact = getFormDFValue(oForm, IDH_TFSMAST._GenCntPr)
        '                                oWO.U_PPhone1 = getFormDFValue(oForm, IDH_TFSMAST._GenPh)

        '                                oWO.doSetCarrier(getFormDFValue(oForm, IDH_TFSMAST._CARCd))
        '                                'oWO.U_SAddress = getFormDFValue(oForm, IDH_TFSMAST._DOFAdres)
        '                                'oWO.U_SContact = getFormDFValue(oForm, IDH_TFSMAST._DOFCntPr)
        '                                'oWO.U_SPhone1 = getFormDFValue(oForm, IDH_TFSMAST._DOFPh)

        '                                oWO.setValue("U_TFSNotif", sTFSNr)
        '                                oWO.U_ItemGrp = sItemGroup

        '                                oWO.U_Status = 1
        '                                oWO.U_BDate = DateTime.Now()
        '                                oWO.U_BTime = 800
        '                                oWO.U_User = goParent.goDICompany.UserSignature()

        '                                If oWO.doAddDataRow() Then

        '                                    sWONr = oWO.Code
        '                                    pVal.oGrid.doSetFieldValue("U_WROrd", iRow, oWO.Code)

        '                                    'Dim oData As New ArrayList
        '                                    'oData.Add("doUpdate")
        '                                    'oData.Add(oWO.Code)
        '                                    'goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)
        '                                End If
        '                            Else
        '                                oWO.getByKey(sWONr)
        '                            End If

        '                            'Check And Create the Linked WOR
        '                            If (Not oWO Is Nothing AndAlso oWO.Code.Length > 0) AndAlso sWORowNr.Length = 0 Then
        '                                Dim oWOR As com.idh.dbObjects.User.IDH_JOBSHD = New com.idh.dbObjects.User.IDH_JOBSHD(oWO)
        '                                Dim sCarrierCd As String = oGrid.doGetFieldValue("U_CarCd", iRow)
        '                                Dim sCarrierNm As String = oGrid.doGetFieldValue("U_CarNm", iRow)
        '                                Dim sUOM As String = oGrid.doGetFieldValue("U_UOM", iRow)

        '                                oWOR.doApplyDefaults()

        '                                oWOR.U_JobNr = oWO.Code
        '                                oWOR.setValue("U_TFSMov", sTFSMove)

        '                                oWOR.U_CustCd = oWO.U_CardCd
        '                                oWOR.U_CustNm = oWO.U_CardNM

        '                                oWOR.U_CarrCd = sCarrierCd
        '                                oWOR.U_CarrNm = sCarrierNm

        '                                oWOR.U_Tip = oWO.U_SCardCd
        '                                oWOR.U_TipNm = oWO.U_SCardNM

        '                                oWOR.U_ProCd = oWO.U_PCardCd
        '                                oWOR.U_ProNm = oWO.U_PCardNM

        '                                oWOR.U_JobTp = sJobType
        '                                oWOR.U_ItmGrp = sItemGroup
        '                                oWOR.U_ItemCd = sContainerCd
        '                                oWOR.U_ItemDsc = sContainerDsc
        '                                oWOR.U_WasCd = sWasteCode
        '                                oWOR.U_WasDsc = sWasteDsc
        '                                oWOR.U_UOM = sUOM
        '                                oWOR.U_PUOM = sUOM

        '                                oWOR.U_BDate = oWO.U_BDate
        '                                oWOR.U_BTime = oWO.U_BTime
        '                                oWOR.U_RDate = oWO.U_BDate
        '                                oWOR.U_RTime = oWO.U_BTime

        '                                oWOR.U_User = goParent.goDICompany.UserSignature()
        '                                oWOR.U_Branch = idh.const.Lookup.INSTANCE.doGetCurrentBranch()

        '                                If oWOR.doAddDataRow() Then
        '                                    sWORowNr = oWOR.Code
        '                                    pVal.oGrid.doSetFieldValue("U_WRRow", iRow, oWOR.Code)

        '                                    Dim oData As New ArrayList
        '                                    oData.Add("DoUpdate")
        '                                    oData.Add(sWORowNr)
        '                                    oData.Add(DateTime.Now())
        '                                    oData.Add("")
        '                                    oData.Add(oWO.U_ZpCd)
        '                                    oData.Add(oWO.U_Address)
        '                                    oData.Add("CANNOTDOCOVERAGE")
        '                                    oData.Add(oWO.U_SteId)

        '                                    'Added to handle the BP Switching
        '                                    oData.Add(Nothing)
        '                                    oData.Add(oWO.U_PAddress)
        '                                    oData.Add(oWO.U_PZpCd)
        '                                    oData.Add(oWO.U_PPhone1)
        '                                    oData.Add(oWO.U_FirstBP)

        '                                    goParent.doOpenModalForm("IDHJOBS", oForm, oData)
        '                                End If
        '                            End If
        '                        End If
        '                    End If
        '                End If
        '            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK Then
        '                ''Louis Code
        '                If pVal.BeforeAction = False Then
        '                    If pVal.ColUID.Equals("U_WROrd") Then
        '                        Dim sWROrd As String = oGrid.doGetFieldValue("U_WROrd")
        '                        If sWROrd.Length > 0 Then
        '                            Dim oWData As New ArrayList
        '                            oWData.Add("DoUpdate")
        '                            oWData.Add(sWROrd)
        '                            goParent.doOpenModalForm("IDH_WASTORD", oForm, oWData)
        '                        End If
        '                    ElseIf pVal.ColUID.Equals("U_WRRow") Then
        '                        Dim sWRRow As String = oGrid.doGetFieldValue("U_WRRow")
        '                        If sWRRow.Length > 0 Then
        '                            Dim oWData As New ArrayList
        '                            oWData.Add("DoUpdate")
        '                            If sWRRow.Length > 0 Then
        '                                oWData.Add(sWRRow)
        '                                oWData.Add(com.idh.utils.Dates.doStrToDate(Nothing)) 'getFormDFValue(oForm, "U_BDate")))
        '                                oWData.Add("00:00") 'goParent.doTimeStrToStr(getFormDFValue(oForm, "U_BTime")))
        '                                oWData.Add("") 'getFormDFValue(oForm, "U_ZpCd"))
        '                                oWData.Add("") 'getFormDFValue(oForm, "U_Address"))
        '                                oWData.Add("CANNOTDOCOVERAGE")
        '                                oWData.Add("") 'getFormDFValue(oForm, "U_SteId"))

        '                                'Added to handle the BP Switching
        '                                oWData.Add(Nothing)
        '                                oWData.Add("") 'getFormDFValue(oForm, "U_PAddress"))
        '                                oWData.Add("") 'getFormDFValue(oForm, "U_PZpCd"))
        '                                oWData.Add("") 'getFormDFValue(oForm, "U_PPhone1"))
        '                                oWData.Add("") ' getFormDFValue(oForm, "U_FirstBP"))

        '                                goParent.doOpenModalForm("IDHJOBS", oForm, oWData)
        '                            End If
        '                        End If
        '                    End If
        '                End If
        '            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY Then
        '                Dim iRow As Integer = pVal.Row
        '                Dim sTFSCode As String = getFormDFValue(oForm, "Code")
        '                Dim sTFSNumber As String = getFormDFValue(oForm, "U_TFSNum")
        '                'Dim sCustCd As String = getFormDFValue(oForm, "U_CardCd")
        '                'Dim sCustNm As String = getFormDFValue(oForm, "U_CardNm")

        '                oGrid.doSetFieldValue("U_TFSCd", iRow, sTFSCode)
        '                oGrid.doSetFieldValue("U_TFSNo", iRow, sTFSNumber)
        '                'oGrid.doSetFieldValue("U_Qty", iRow, 1)
        '                'oGrid.doSetFieldValue("U_WgtDed", iRow, 0)
        '                'oGrid.doSetFieldValue("U_ValDed", iRow, 0)
        '            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED Then
        '                If pVal.BeforeAction = False Then
        '                    'If pVal.ColUID = "U_Qty" Then
        '                    '    oGrid.doSetQty()
        '                    '    Dim dTotal As Double = oGrid.doCalculateWeightTotals()

        '                    '    'setUFValue(oForm, "IDH_TWGTDE", dTotal)

        '                    '    'Update the Deducted Weight
        '                    '    setDFValue(oForm, msRowTable, "U_WgtDed", dTotal)

        '                    '    'Update the Adjusted Weight
        '                    '    Dim dRdWgt As Double = getDFValue(oForm, msRowTable, "U_RdWgt")
        '                    '    dTotal = dRdWgt - dTotal
        '                    '    setDFValue(oForm, msRowTable, "U_AdjWgt", dTotal)

        '                    '    'Update the Customer Weight
        '                    '    doItAll(oForm)
        '                    'ElseIf pVal.ColUID = "U_WgtDed" Then
        '                    '    Dim dTotal As Double = oGrid.doCalculateWeightTotals()

        '                    '    'setUFValue(oForm, "IDH_TWGTDE", dTotal)

        '                    '    'Update the Deducted Weight
        '                    '    setDFValue(oForm, msRowTable, "U_WgtDed", dTotal)

        '                    '    'Update the Adjusted Weight
        '                    '    Dim dRdWgt As Double = getDFValue(oForm, msRowTable, "U_RdWgt")
        '                    '    dTotal = dRdWgt - dTotal
        '                    '    setDFValue(oForm, msRowTable, "U_AdjWgt", dTotal)

        '                    '    'Update the Customer Weight
        '                    '    doItAll(oForm)
        '                    'ElseIf pVal.ColUID = "U_ValDed" Then
        '                    '    Dim dTotal As Double = oGrid.doCalculateValueTotals()

        '                    '    'Update the Value total
        '                    '    'setUFValue(oForm, "IDH_TVALDE", dTotal)

        '                    '    'Update the Deducted value
        '                    '    setDFValue(oForm, msRowTable, "U_ValDed", dTotal)
        '                    '    doItAll(oForm)
        '                    'End If
        '                End If
        '            End If
        '            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE _
        '             OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
        '                doSetUpdate(oForm)
        '            End If
        '        End If
        '    End If
        '    Return True
        'End Function

#End Region

#Region "Custom Functions"

        Protected Function isValidTFSNumber(ByVal sTFSNo As String) As Boolean
            Dim sQry As String
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                sQry = "select U_TFSNUM from [@IDH_TFSNUM] where U_ValidTo >= GETDATE() AND U_TFSNUM = '" + sTFSNo + "'"
                oRecordSet = goParent.goDB.doSelectQuery(sQry)
                If Not oRecordSet Is Nothing Then
                    If oRecordSet.RecordCount > 0 Then
                        Return True
                    Else
                        'Return False
                        'need fixing after demo 
                        'TO DO 
                        Return True
                    End If
                End If
            Catch ex As Exception
                Throw ex
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
            End Try
            Return True

        End Function

        Protected Function isValidBPTFSRegNo(ByVal oForm As SAPbouiCOM.Form, ByVal sCardCode As String) As Boolean
            Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            oBP = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
            If oBP.GetByKey(sCardCode) Then
                If oBP.UserFields.Fields.Item("U_RGVLDTO").Value >= Date.Now.Date Then
                    Return True
                Else
                    Return False
                End If
            End If
            Return False
        End Function

        Protected Function doUpdateHeader(ByVal oForm As SAPbouiCOM.Form, ByVal bIsAdd As Boolean) As Boolean
            'This will now Automatically update all the current DB fields and
            'generate and set the Code value if the code was not set
            Return doAutoSave(oForm, IDH_TFSMAST.AUTONUMPREFIX, "TFSMST")
        End Function

        Public Overridable Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form)
            Dim sCode = DataHandler.INSTANCE.doGenerateCode(IDH_TFSMAST.AUTONUMPREFIX, "TFSMST")

            setFormDFValue(oForm, "Code", sCode)
            setFormDFValue(oForm, "Name", sCode)
            setFormDFValue(oForm, "U_TFSNum", "")
            'setFormDFValue(oForm, "U_Type", "")
            'setFormDFValue(oForm, "U_Status", "")

            'form tfs details 
            setFormDFValue(oForm, "U_ShpType", "")
            setFormDFValue(oForm, "U_OpType", "")
            setFormDFValue(oForm, "U_SPreAp", "")
            setFormDFValue(oForm, "U_IntShip", "")
            setFormDFValue(oForm, "U_ActShip", "")
            setFormDFValue(oForm, "U_IntQty", "")
            setFormDFValue(oForm, "U_QtyT", "")
            setFormDFValue(oForm, "U_QtyCMtr", "")
            setFormDFValue(oForm, "U_SDtFrm", "")
            setFormDFValue(oForm, "U_SDtTo", "")
            setFormDFValue(oForm, "U_PackTyp", "")
            setFormDFValue(oForm, "U_SpHandle", "")
            setFormDFValue(oForm, "U_SpHDetails", "")
            setFormDFValue(oForm, "U_DCode", "")
            setFormDFValue(oForm, "U_TechEmp", "")
            setFormDFValue(oForm, "U_ExpRes", "")
            setFormDFValue(oForm, "U_WasteCmp", "")
            setFormDFValue(oForm, "U_PhyChtr", "")
            setFormDFValue(oForm, "U_BaselCd", "")
            setFormDFValue(oForm, "U_OECDCd", "")
            setFormDFValue(oForm, "U_EWCCd", "")
            setFormDFValue(oForm, "U_ExpNatCd", "")
            setFormDFValue(oForm, "U_ImpNatCd", "")
            setFormDFValue(oForm, "U_Other", "")
            setFormDFValue(oForm, "U_YCode", "")
            setFormDFValue(oForm, "U_HCode", "")
            setFormDFValue(oForm, "U_UNNumber", "")
            setFormDFValue(oForm, "U_UNClass", "")
            setFormDFValue(oForm, "U_UNShipNm", "")
            setFormDFValue(oForm, "U_CustCd", "")

            'Notifier
            setFormDFValue(oForm, "U_NOTCd", "")
            setFormDFValue(oForm, "U_NOTName", "")
            setFormDFValue(oForm, "U_NotRegNo", "")
            setFormDFValue(oForm, "U_NotAdres", "")
            setFormDFValue(oForm, "U_NotCntPr", "")
            setFormDFValue(oForm, "U_NotPh", "")
            setFormDFValue(oForm, "U_NotFax", "")
            setFormDFValue(oForm, "U_NotEmail", "")
            setFormDFValue(oForm, "U_NotSign", "")

            'Consignor
            setFormDFValue(oForm, "U_CONCd", "")
            setFormDFValue(oForm, "U_CONName", "")
            setFormDFValue(oForm, "U_ConRegNo", "")
            setFormDFValue(oForm, "U_ConAdres", "")
            setFormDFValue(oForm, "U_ConCntPr", "")
            setFormDFValue(oForm, "U_ConPh", "")
            setFormDFValue(oForm, "U_ConFax", "")
            setFormDFValue(oForm, "U_ConEmail", "")

            'Consignee
            setFormDFValue(oForm, "U_COECd", "")
            setFormDFValue(oForm, "U_COEName", "")
            setFormDFValue(oForm, "U_CoeRegNo", "")
            setFormDFValue(oForm, "U_CoeAdres", "")
            setFormDFValue(oForm, "U_CoeCntPr", "")
            setFormDFValue(oForm, "U_CoePh", "")
            setFormDFValue(oForm, "U_CoeFax", "")
            setFormDFValue(oForm, "U_CoeEmail", "")

            'Carrier
            setFormDFValue(oForm, "U_CARCd", "")
            setFormDFValue(oForm, "U_CARName", "")
            '20151007 New fields for Carrier 
            setFormDFValue(oForm, "U_CarRegNo", "")
            setFormDFValue(oForm, "U_CarAdres", "")
            setFormDFValue(oForm, "U_CarCntPr", "")
            setFormDFValue(oForm, "U_CarPh", "")
            setFormDFValue(oForm, "U_CarFax", "")
            setFormDFValue(oForm, "U_CarEmail", "")
            setFormDFValue(oForm, "U_CarCmpReg", "")
            setFormDFValue(oForm, "U_TranMode", "")

            'Generator
            setFormDFValue(oForm, "U_GENCd", "")
            setFormDFValue(oForm, "U_GENName", "")
            setFormDFValue(oForm, "U_GenRegNo", "")
            setFormDFValue(oForm, "U_GenAdres", "")
            setFormDFValue(oForm, "U_GenCntPr", "")
            setFormDFValue(oForm, "U_GenPh", "")
            setFormDFValue(oForm, "U_GenFax", "")
            setFormDFValue(oForm, "U_GenEmail", "")

            'Disp. Facility
            setFormDFValue(oForm, "U_DSPCd", "")
            setFormDFValue(oForm, "U_DSPName", "")
            setFormDFValue(oForm, "U_DOFRegNo", "")
            setFormDFValue(oForm, "U_DOFAdres", "")
            setFormDFValue(oForm, "U_DOFCntPr", "")
            setFormDFValue(oForm, "U_DOFPh", "")
            setFormDFValue(oForm, "U_DOFFax", "")
            setFormDFValue(oForm, "U_DOFEmail", "")
            setFormDFValue(oForm, "U_DOPreAp", "")

            'Final Dest.
            setFormDFValue(oForm, "U_FDSCd", "")
            setFormDFValue(oForm, "U_FDSName", "")
            setFormDFValue(oForm, "U_FDTRegNo", "")
            setFormDFValue(oForm, "U_FDTAdres", "")
            setFormDFValue(oForm, "U_FDTCntPr", "")
            setFormDFValue(oForm, "U_FDTPh", "")
            setFormDFValue(oForm, "U_FDTFax", "")
            setFormDFValue(oForm, "U_FDTEmail", "")

            setFormDFValue(oForm, "U_Multiple", "")

            '20151001: New Customs fields 
            setFormDFValue(oForm, "U_CustEntry", "")
            setFormDFValue(oForm, "U_CustExit", "")
            setFormDFValue(oForm, "U_CustExport", "")

            'Annex felds 
            setFormDFValue(oForm, "U_AnxCd1", "")
            setFormDFValue(oForm, "U_AnxDesc1", "")
            setFormDFValue(oForm, "U_AnxCd2", "")
            setFormDFValue(oForm, "U_AnxDesc2", "")
            setFormDFValue(oForm, "U_AnxCd3", "")
            setFormDFValue(oForm, "U_AnxDesc3", "")
            setFormDFValue(oForm, "U_AnxCd4", "")
            setFormDFValue(oForm, "U_AnxDesc4", "")
            setFormDFValue(oForm, "U_AnxCd5", "")
            setFormDFValue(oForm, "U_AnxDesc5", "")
            setFormDFValue(oForm, "U_AnxCd6", "")
            setFormDFValue(oForm, "U_AnxDesc6", "")
            setFormDFValue(oForm, "U_AnxCd7", "")
            setFormDFValue(oForm, "U_AnxDesc7", "")
            setFormDFValue(oForm, "U_AnxCd8", "")
            setFormDFValue(oForm, "U_AnxDesc8", "")
            setFormDFValue(oForm, "U_AnxCd9", "")
            setFormDFValue(oForm, "U_AnxDesc9", "")
            setFormDFValue(oForm, "U_AnxCd10", "")
            setFormDFValue(oForm, "U_AnxDesc10", "")

            'Company Registration No from BP 
            setFormDFValue(oForm, "U_NotCmpReg", "")
            setFormDFValue(oForm, "U_ConCmpReg", "")
            setFormDFValue(oForm, "U_CoeCmpReg", "")
            setFormDFValue(oForm, "U_GenCmpReg", "")
            setFormDFValue(oForm, "U_DOFCmpReg", "")
            setFormDFValue(oForm, "U_FDTCmpReg", "")


            'Dim oExclude() As String = {"IDH_TFSTYP", "IDH_STAT"}
            'EnableAllEdittems(oForm, oExclude)

            'IDH_TFSTYP", "[@IDH_TFSType]", "Code", "Name", Nothing, "Cast(Code as Numeric)")
            'doFillCombo(oForm, "IDH_STAT

            doSetGridsEnable(oForm, True)
            doFillGrids(oForm, "-1")

        End Sub

        Public Overridable Sub doCreateFindEntry(ByVal oForm As SAPbouiCOM.Form)
            setFormDFValue(oForm, "Code", "", True)
            setFormDFValue(oForm, "Name", "")
            setFormDFValue(oForm, "U_TFSNum", "")
            'setFormDFValue(oForm, "U_Type", "")
            'setFormDFValue(oForm, "U_Status", "")

            'form tfs details 
            setFormDFValue(oForm, "U_ShpType", "")
            setFormDFValue(oForm, "U_OpType", "")
            setFormDFValue(oForm, "U_SPreAp", "")
            setFormDFValue(oForm, "U_IntShip", "")
            setFormDFValue(oForm, "U_ActShip", "")
            setFormDFValue(oForm, "U_IntQty", "")
            setFormDFValue(oForm, "U_QtyT", "")
            setFormDFValue(oForm, "U_QtyCMtr", "")
            setFormDFValue(oForm, "U_SDtFrm", "")
            setFormDFValue(oForm, "U_SDtTo", "")
            setFormDFValue(oForm, "U_PackTyp", "")
            setFormDFValue(oForm, "U_SpHandle", "")
            setFormDFValue(oForm, "U_SpHDetails", "")
            setFormDFValue(oForm, "U_DCode", "")
            setFormDFValue(oForm, "U_TechEmp", "")
            setFormDFValue(oForm, "U_ExpRes", "")
            setFormDFValue(oForm, "U_WasteCmp", "")
            setFormDFValue(oForm, "U_PhyChtr", "")
            setFormDFValue(oForm, "U_BaselCd", "")
            setFormDFValue(oForm, "U_OECDCd", "")
            setFormDFValue(oForm, "U_EWCCd", "")
            setFormDFValue(oForm, "U_ExpNatCd", "")
            setFormDFValue(oForm, "U_ImpNatCd", "")
            setFormDFValue(oForm, "U_Other", "")
            setFormDFValue(oForm, "U_YCode", "")
            setFormDFValue(oForm, "U_HCode", "")
            setFormDFValue(oForm, "U_UNNumber", "")
            setFormDFValue(oForm, "U_UNClass", "")
            setFormDFValue(oForm, "U_UNShipNm", "")
            setFormDFValue(oForm, "U_CustCd", "")

            'Notifier
            setFormDFValue(oForm, "U_NOTCd", "")
            setFormDFValue(oForm, "U_NOTName", "")
            setFormDFValue(oForm, "U_NotRegNo", "")
            setFormDFValue(oForm, "U_NotAdres", "")
            setFormDFValue(oForm, "U_NotCntPr", "")
            setFormDFValue(oForm, "U_NotPh", "")
            setFormDFValue(oForm, "U_NotFax", "")
            setFormDFValue(oForm, "U_NotEmail", "")
            setFormDFValue(oForm, "U_NotSign", "")

            'Consignor
            setFormDFValue(oForm, "U_CONCd", "")
            setFormDFValue(oForm, "U_CONName", "")
            setFormDFValue(oForm, "U_ConRegNo", "")
            setFormDFValue(oForm, "U_ConAdres", "")
            setFormDFValue(oForm, "U_ConCntPr", "")
            setFormDFValue(oForm, "U_ConPh", "")
            setFormDFValue(oForm, "U_ConFax", "")
            setFormDFValue(oForm, "U_ConEmail", "")

            'Consignee
            setFormDFValue(oForm, "U_COECd", "")
            setFormDFValue(oForm, "U_COEName", "")
            setFormDFValue(oForm, "U_CoeRegNo", "")
            setFormDFValue(oForm, "U_CoeAdres", "")
            setFormDFValue(oForm, "U_CoeCntPr", "")
            setFormDFValue(oForm, "U_CoePh", "")
            setFormDFValue(oForm, "U_CoeFax", "")
            setFormDFValue(oForm, "U_CoeEmail", "")

            'Carrier
            setFormDFValue(oForm, "U_CARCd", "")
            setFormDFValue(oForm, "U_CARName", "")
            '20151007 New fields for Carrier 
            setFormDFValue(oForm, "U_CarRegNo", "")
            setFormDFValue(oForm, "U_CarAdres", "")
            setFormDFValue(oForm, "U_CarCntPr", "")
            setFormDFValue(oForm, "U_CarPh", "")
            setFormDFValue(oForm, "U_CarFax", "")
            setFormDFValue(oForm, "U_CarEmail", "")
            setFormDFValue(oForm, "U_CarCmpReg", "")
            setFormDFValue(oForm, "U_TranMode", "")

            'Generator
            setFormDFValue(oForm, "U_GENCd", "")
            setFormDFValue(oForm, "U_GENName", "")
            setFormDFValue(oForm, "U_GenRegNo", "")
            setFormDFValue(oForm, "U_GenAdres", "")
            setFormDFValue(oForm, "U_GenCntPr", "")
            setFormDFValue(oForm, "U_GenPh", "")
            setFormDFValue(oForm, "U_GenFax", "")
            setFormDFValue(oForm, "U_GenEmail", "")

            'Disp. Facility
            setFormDFValue(oForm, "U_DSPCd", "")
            setFormDFValue(oForm, "U_DSPName", "")
            setFormDFValue(oForm, "U_DOFRegNo", "")
            setFormDFValue(oForm, "U_DOFAdres", "")
            setFormDFValue(oForm, "U_DOFCntPr", "")
            setFormDFValue(oForm, "U_DOFPh", "")
            setFormDFValue(oForm, "U_DOFFax", "")
            setFormDFValue(oForm, "U_DOFEmail", "")
            setFormDFValue(oForm, "U_DOPreAp", "")

            'Final Dest.
            setFormDFValue(oForm, "U_FDSCd", "")
            setFormDFValue(oForm, "U_FDSName", "")
            setFormDFValue(oForm, "U_FDTRegNo", "")
            setFormDFValue(oForm, "U_FDTAdres", "")
            setFormDFValue(oForm, "U_FDTCntPr", "")
            setFormDFValue(oForm, "U_FDTPh", "")
            setFormDFValue(oForm, "U_FDTFax", "")
            setFormDFValue(oForm, "U_FDTEmail", "")

            setFormDFValue(oForm, "U_Multiple", "")

            '20151001: New Customs fields 
            setFormDFValue(oForm, "U_CustEntry", "")
            setFormDFValue(oForm, "U_CustExit", "")
            setFormDFValue(oForm, "U_CustExport", "")

            setFormDFValue(oForm, "U_GenSite", "")

            'Annex felds 
            setFormDFValue(oForm, "U_AnxCd1", "")
            setFormDFValue(oForm, "U_AnxDesc1", "")
            setFormDFValue(oForm, "U_AnxCd2", "")
            setFormDFValue(oForm, "U_AnxDesc2", "")
            setFormDFValue(oForm, "U_AnxCd3", "")
            setFormDFValue(oForm, "U_AnxDesc3", "")
            setFormDFValue(oForm, "U_AnxCd4", "")
            setFormDFValue(oForm, "U_AnxDesc4", "")
            setFormDFValue(oForm, "U_AnxCd5", "")
            setFormDFValue(oForm, "U_AnxDesc5", "")
            setFormDFValue(oForm, "U_AnxCd6", "")
            setFormDFValue(oForm, "U_AnxDesc6", "")
            setFormDFValue(oForm, "U_AnxCd7", "")
            setFormDFValue(oForm, "U_AnxDesc7", "")
            setFormDFValue(oForm, "U_AnxCd8", "")
            setFormDFValue(oForm, "U_AnxDesc8", "")
            setFormDFValue(oForm, "U_AnxCd9", "")
            setFormDFValue(oForm, "U_AnxDesc9", "")
            setFormDFValue(oForm, "U_AnxCd10", "")
            setFormDFValue(oForm, "U_AnxDesc10", "")

            'Company Registration No from BP 
            setFormDFValue(oForm, "U_NotCmpReg", "")
            setFormDFValue(oForm, "U_ConCmpReg", "")
            setFormDFValue(oForm, "U_CoeCmpReg", "")
            setFormDFValue(oForm, "U_GenCmpReg", "")
            setFormDFValue(oForm, "U_DOFCmpReg", "")
            setFormDFValue(oForm, "U_FDTCmpReg", "")


            Dim oExclude() As String = {"IDH_CODE"}
            DisableAllEditItems(oForm, oExclude)
            setEnableItem(oForm, True, "IDH_CODE")
            setEnableItem(oForm, True, "IDH_TFSNO1")

            doSetGridsEnable(oForm, False)

        End Sub

        Protected Sub doChooseTFSNumber(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, ByVal sTRG As String)
            Dim sTFSNum As String
            sTFSNum = "" 'getDFValue(oForm, "@IDH_TFSNUM", "U_TFSNUM")

            setSharedData(oForm, "IDH_TFSNUM", sTFSNum)
            setSharedData(oForm, "TRG", sTRG)
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            goParent.doOpenModalForm("IDHTFSNSR", oForm)

        End Sub

        Protected Overridable Sub doChooseCustomer(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, ByVal sTRG As String)
            'Dim sCardCode As String =  getItemValue(oForm, "IDH_CUST"))
            'Dim sCardName As String =  getItemValue(oForm, "IDH_CUSTNM"))

            Dim sCardCode As String = String.Empty 'getFormDFValue(oForm, "U_CardCd", True)
            Dim sCardName As String = String.Empty 'getFormDFValue(oForm, "U_CardNM", True)

            If sCardCode.Length > 0 OrElse _
                sCardName.Length > 0 Then
                'idh.const.Lookup.INSTANCE.getParameterAsBool(msOrderType & "BPFLT", True) = False 
                setSharedData(oForm, "IDH_TYPE", "")
                setSharedData(oForm, "IDH_GROUP", "")
            Else
                setSharedData(oForm, "IDH_TYPE", "")
            End If

            setSharedData(oForm, "IDH_BPCOD", sCardCode)
            setSharedData(oForm, "IDH_NAME", sCardName)
            setSharedData(oForm, "TRG", sTRG)
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If

            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub

        Protected Overridable Sub doChooseAddress(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, ByVal sTRG As String, ByVal sSource As String)
            Dim sCustCd As String = String.Empty

            Select Case sSource
                Case "IDH_NOTLAD"
                    sCustCd = getFormDFValue(oForm, "U_NOTCd")
                    sTRG = "IDH_NOTADD"
                Case "IDH_CONLAD"
                    sCustCd = getFormDFValue(oForm, "U_CONCd")
                    sTRG = "IDH_CONADD"
                Case "IDH_COELAD"
                    sCustCd = getFormDFValue(oForm, "U_COECd")
                    sTRG = "IDH_COEADD"
                Case "IDH_GENLAD"
                    sCustCd = getFormDFValue(oForm, "U_GENCd")
                    sTRG = "IDH_GENADD"
                Case "IDH_DSPLAD"
                    sCustCd = getFormDFValue(oForm, "U_DSPCd")
                    sTRG = "IDH_DSPADD"
                Case "IDH_FDTLAD"
                    sCustCd = getFormDFValue(oForm, "U_FDSCd")
                    sTRG = "IDH_FDTADD"
                Case "IDH_FFLAD"
                    sCustCd = getFormDFValue(oForm, "U_FDSCd")
                    sTRG = "IDH_FFADD"
                Case "IDH_CARLAD"
                    sCustCd = getFormDFValue(oForm, "U_CARCd")
                    sTRG = "IDH_CARADD"
            End Select
            setSharedData(oForm, "IDH_CUSCOD", sCustCd)
            'setSharedData(oForm, "IDH_ADRTYP", "S")
            setSharedData(oForm, "TRG", sTRG)

            goParent.doOpenModalForm("IDHASRCH", oForm)

        End Sub

        Protected Overridable Sub doChooseContactPerson(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, ByVal sTRG As String, ByVal sSource As String)
            Dim sCustCd As String = String.Empty

            Select Case sSource
                Case "IDH_NOTLCP"
                    sCustCd = getFormDFValue(oForm, "U_NOTCd")
                    sTRG = "IDH_NOTCPR"
                Case "IDH_CONLCP"
                    sCustCd = getFormDFValue(oForm, "U_CONCd")
                    sTRG = "IDH_CONCPR"
                Case "IDH_COELCP"
                    sCustCd = getFormDFValue(oForm, "U_COECd")
                    sTRG = "IDH_COECPR"
                Case "IDH_GENLCP"
                    sCustCd = getFormDFValue(oForm, "U_GENCd")
                    sTRG = "IDH_GENCPR"
                Case "IDH_GENLSG"
                    sCustCd = getFormDFValue(oForm, "U_GENCd")
                    sTRG = "IDH_GENSIG"
                Case "IDH_DSPLCP"
                    sCustCd = getFormDFValue(oForm, "U_DSPCd")
                    sTRG = "IDH_DSPCPR"
                Case "IDH_FDTLCP"
                    sCustCd = getFormDFValue(oForm, "U_FDSCd")
                    sTRG = "IDH_FDTCPR"
                Case "IDH_FFLCP"
                    sCustCd = getFormDFValue(oForm, "U_FFCd")
                    sTRG = "IDH_FFCPR"
                Case "IDH_CARLCP"
                    sCustCd = getFormDFValue(oForm, "U_CARCd")
                    sTRG = "IDH_CARCPR"
                Case "IDH_NOTLSG"
                    sCustCd = getFormDFValue(oForm, "U_NOTCd")
                    sTRG = "IDH_NOTSIG"
            End Select

            setSharedData(oForm, "IDH_BPCODE", sCustCd)
            'setSharedData(oForm, "IDH_ADRTYP", "S")
            setSharedData(oForm, "TRG", sTRG)
            goParent.doOpenModalForm("IDHBPCSRC", oForm)

        End Sub

        Protected Overridable Sub doChooseAnnex(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, ByVal sTRG As String, ByVal sID As String)
            Dim sAnexCd As String = getFormDFValue(oForm, "U_AnxCd" + sID)
            Dim sAnexNo As String = getFormDFValue(oForm, "U_AnxDesc" + sID)

            Dim sTFSCode As String = getFormDFValue(oForm, "Code")
            Dim sTFSNo As String = getFormDFValue(oForm, "U_TFSNum")

            setSharedData(oForm, "TRG", sTRG)
            setSharedData(oForm, "ANXFIELDID", sID)
            setSharedData(oForm, "IDH_ANXCD", sAnexCd)
            setSharedData(oForm, "IDH_ANXNO", sAnexNo)

            setSharedData(oForm, "IDH_TFSCD", sTFSCode)
            setSharedData(oForm, "IDH_TFSNO", sTFSNo)

            goParent.doOpenModalForm("IDHANXS", oForm)

        End Sub

        Protected Sub doSetNotifierAddress(ByVal oForm As SAPbouiCOM.Form, ByVal sCardCd As String, ByVal sCardNm As String)
            Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            Dim oData As ArrayList = New ArrayList
            oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sCardCd, "S")

            Try
                If Not (oData Is Nothing) AndAlso oData.Count > 0 Then
                    setFormDFValue(oForm, "U_NotName", sCardNm)
                    setFormDFValue(oForm, "U_NotAdres", oData.Item(0))
                    setFormDFValue(oForm, "U_NotPh", oData.Item(13))
                    setFormDFValue(oForm, "U_NotFax", oData.Item(22))
                    setFormDFValue(oForm, "U_NotEmail", oData.Item(23))

                    oBP = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
                    If oBP.GetByKey(sCardCd) Then
                        setFormDFValue(oForm, "U_NotRegNo", oBP.UserFields.Fields.Item("U_REGNO").Value)
                        setFormDFValue(oForm, "U_NotCntPr", oBP.ContactPerson)
                        setFormDFValue(oForm, "U_NotCmpReg", oBP.CompanyRegistrationNumber)
                    Else
                        setFormDFValue(oForm, "U_NotRegNo", "")
                        setFormDFValue(oForm, "U_NotCntPr", "")
                        setFormDFValue(oForm, "U_NotCmpReg", "")
                    End If
                Else
                    setFormDFValue(oForm, "U_NotRegNo", "")
                    setFormDFValue(oForm, "U_NotName", "")
                    setFormDFValue(oForm, "U_NotAdres", "")
                    setFormDFValue(oForm, "U_NotCntPr", "")
                    setFormDFValue(oForm, "U_NotPh", "")
                    setFormDFValue(oForm, "U_NotFax", "")
                    setFormDFValue(oForm, "U_NotEmail", "")
                    setFormDFValue(oForm, "U_NotCmpReg", "")
                End If
                doSetUpdate(oForm)

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the Notifier Address")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXAGEN", {com.idh.bridge.Translation.getTranslatedWord("Notifier")})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oBP)
            End Try
        End Sub

        Protected Sub doResetBPAddress(ByVal oForm As SAPbouiCOM.Form, ByRef oData As ArrayList, ByVal sCtrlPrefix As String)
            setFormDFValue(oForm, "U_" + sCtrlPrefix + "Adres", oData.Item(0))
            'setFormDFValue(oForm, "U_" + sCtrlPrefix + "Ph", oData.Item(1))
            'setFormDFValue(oForm, "U_" + sCtrlPrefix + "Fax", oData.Item(2))
            'setFormDFValue(oForm, "U_" + sCtrlPrefix + "Email", oData.Item(3))
            doSetUpdate(oForm)

        End Sub

        Protected Sub doResetBPContact(ByVal oForm As SAPbouiCOM.Form, ByVal sCtrlPrefix As String, ByVal sContactPerson As String, ByVal sTel1 As String, ByVal sFax As String, ByVal sEmail As String)
            setFormDFValue(oForm, "U_" + sCtrlPrefix + "CntPr", sContactPerson)
            setFormDFValue(oForm, "U_" + sCtrlPrefix + "Ph", sTel1)
            setFormDFValue(oForm, "U_" + sCtrlPrefix + "Fax", sFax)
            setFormDFValue(oForm, "U_" + sCtrlPrefix + "Email", sEmail)
            doSetUpdate(oForm)
        End Sub

        Protected Sub doSetConsignorAddress(ByVal oForm As SAPbouiCOM.Form)
            Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            Dim oData As ArrayList = New ArrayList
            oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, getSharedData(oForm, "CARDCODE"), "S")

            Try
                If Not (oData Is Nothing) AndAlso oData.Count > 0 Then
                    setFormDFValue(oForm, "U_ConName", getSharedData(oForm, "CARDNAME"))
                    setFormDFValue(oForm, "U_ConAdres", oData.Item(0))
                    'setFormDFValue(oForm, "U_ConPh", oData.Item(13))
                    'setFormDFValue(oForm, "U_ConFax", oData.Item(22))
                    'setFormDFValue(oForm, "U_ConEmail", oData.Item(23))

                    oBP = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
                    If oBP.GetByKey(getSharedData(oForm, "CARDCODE")) Then
                        setFormDFValue(oForm, "U_ConRegNo", oBP.UserFields.Fields.Item("U_REGNO").Value)
                        'setFormDFValue(oForm, "U_ConCntPr", oBP.ContactPerson)
                        setFormDFValue(oForm, "U_ConCmpReg", oBP.CompanyRegistrationNumber)
                    Else
                        setFormDFValue(oForm, "U_ConRegNo", "")
                        'setFormDFValue(oForm, "U_ConCntPr", "")
                        setFormDFValue(oForm, "U_ConCmpReg", "")
                    End If
                Else
                    setFormDFValue(oForm, "U_ConRegNo", "")
                    setFormDFValue(oForm, "U_ConName", "")
                    setFormDFValue(oForm, "U_ConAdres", "")
                    'setFormDFValue(oForm, "U_ConCntPr", "")
                    'setFormDFValue(oForm, "U_ConPh", "")
                    'setFormDFValue(oForm, "U_ConFax", "")
                    'setFormDFValue(oForm, "U_ConEmail", "")
                    setFormDFValue(oForm, "U_ConCmpReg", "")
                End If
                doSetUpdate(oForm)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the Consignor Address")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXAGEN", {com.idh.bridge.Translation.getTranslatedWord("Consignor")})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oBP)
            End Try
        End Sub

        Protected Sub doSetConsigneeAddress(ByVal oForm As SAPbouiCOM.Form)
            Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            Dim oData As ArrayList = New ArrayList
            oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, getSharedData(oForm, "CARDCODE"), "S")

            Try
                If Not (oData Is Nothing) AndAlso oData.Count > 0 Then
                    setFormDFValue(oForm, "U_CoeName", getSharedData(oForm, "CARDNAME"))
                    setFormDFValue(oForm, "U_CoeAdres", oData.Item(0))
                    'setFormDFValue(oForm, "U_CoePh", oData.Item(13))
                    'setFormDFValue(oForm, "U_CoeFax", oData.Item(22))
                    'setFormDFValue(oForm, "U_CoeEmail", oData.Item(23))

                    oBP = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)

                    If oBP.GetByKey(getSharedData(oForm, "CARDCODE")) Then
                        setFormDFValue(oForm, "U_CoeRegNo", oBP.UserFields.Fields.Item("U_REGNO").Value)
                        'setFormDFValue(oForm, "U_CoeCntPr", oBP.ContactPerson)
                        setFormDFValue(oForm, "U_CoeCmpReg", oBP.CompanyRegistrationNumber)
                    Else
                        setFormDFValue(oForm, "U_CoeRegNo", "")
                        'setFormDFValue(oForm, "U_CoeCntPr", "")
                        setFormDFValue(oForm, "U_CoeCmpReg", "")
                    End If
                Else
                    setFormDFValue(oForm, "U_CoeRegNo", "")
                    setFormDFValue(oForm, "U_CoeName", "")
                    setFormDFValue(oForm, "U_CoeAdres", "")
                    'setFormDFValue(oForm, "U_CoeCntPr", "")
                    'setFormDFValue(oForm, "U_CoePh", "")
                    'setFormDFValue(oForm, "U_CoeFax", "")
                    'setFormDFValue(oForm, "U_CoeEmail", "")
                    setFormDFValue(oForm, "U_CoeCmpReg", "")
                End If
                doSetUpdate(oForm)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the Consignee Address")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXAGEN", {com.idh.bridge.Translation.getTranslatedWord("Consignee")})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oBP)
            End Try
        End Sub

        Protected Sub doSetCarrierAddress(ByVal oForm As SAPbouiCOM.Form)
            Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            Dim oData As ArrayList = New ArrayList
            oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, getSharedData(oForm, "CARDCODE"), "S")

            Try
                If Not (oData Is Nothing) AndAlso oData.Count > 0 Then
                    setFormDFValue(oForm, "U_CarName", getSharedData(oForm, "CARDNAME"))
                    setFormDFValue(oForm, "U_CarAdres", oData.Item(0))
                    'setFormDFValue(oForm, "U_CarPh", oData.Item(13))
                    'setFormDFValue(oForm, "U_CarFax", oData.Item(22))
                    'setFormDFValue(oForm, "U_CarEmail", oData.Item(23))

                    oBP = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
                    If oBP.GetByKey(getSharedData(oForm, "CARDCODE")) Then
                        setFormDFValue(oForm, "U_CarRegNo", oBP.UserFields.Fields.Item("U_REGNO").Value)
                        'setFormDFValue(oForm, "U_CarCntPr", oBP.ContactPerson)
                        setFormDFValue(oForm, "U_CarCmpReg", oBP.CompanyRegistrationNumber)
                    Else
                        setFormDFValue(oForm, "U_CarRegNo", "")
                        'setFormDFValue(oForm, "U_CarCntPr", "")
                        setFormDFValue(oForm, "U_CarCmpReg", "")
                    End If
                Else
                    setFormDFValue(oForm, "U_CarRegNo", "")
                    setFormDFValue(oForm, "U_CarName", "")
                    setFormDFValue(oForm, "U_CarAdres", "")
                    'setFormDFValue(oForm, "U_CarCntPr", "")
                    'setFormDFValue(oForm, "U_CarPh", "")
                    'setFormDFValue(oForm, "U_CarFax", "")
                    'setFormDFValue(oForm, "U_CarEmail", "")
                    setFormDFValue(oForm, "U_CarCmpReg", "")
                End If
                doSetUpdate(oForm)
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the Carrier Address")
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oBP)
            End Try
        End Sub

        Protected Sub doSetGeneratorAddress(ByVal oForm As SAPbouiCOM.Form)
            Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            Dim oData As ArrayList = New ArrayList
            oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, getSharedData(oForm, "CARDCODE"), "S")

            Try
                If Not (oData Is Nothing) AndAlso oData.Count > 0 Then
                    setFormDFValue(oForm, "U_GenName", getSharedData(oForm, "CARDNAME"))
                    setFormDFValue(oForm, "U_GenAdres", oData.Item(0))
                    'setFormDFValue(oForm, "U_GenPh", oData.Item(13))
                    'setFormDFValue(oForm, "U_GenFax", oData.Item(22))
                    'setFormDFValue(oForm, "U_GenEmail", oData.Item(23))

                    oBP = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
                    If oBP.GetByKey(getSharedData(oForm, "CARDCODE")) Then
                        setFormDFValue(oForm, "U_GenRegNo", oBP.UserFields.Fields.Item("U_REGNO").Value)
                        'setFormDFValue(oForm, "U_GenCntPr", oBP.ContactPerson)
                        setFormDFValue(oForm, "U_GenCmpReg", oBP.CompanyRegistrationNumber)
                    Else
                        setFormDFValue(oForm, "U_GenRegNo", "")
                        'setFormDFValue(oForm, "U_GenCntPr", "")
                        setFormDFValue(oForm, "U_GenCmpReg", "")
                    End If
                Else
                    setFormDFValue(oForm, "U_GenRegNo", "")
                    setFormDFValue(oForm, "U_GenName", "")
                    setFormDFValue(oForm, "U_GenAdres", "")
                    'setFormDFValue(oForm, "U_GenCntPr", "")
                    'setFormDFValue(oForm, "U_GenPh", "")
                    'setFormDFValue(oForm, "U_GenFax", "")
                    'setFormDFValue(oForm, "U_GenEmail", "")
                    setFormDFValue(oForm, "U_GenCmpReg", "")
                End If
                doSetUpdate(oForm)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the Generator Address")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXAGEN", {com.idh.bridge.Translation.getTranslatedWord("Generator")})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oBP)
            End Try
        End Sub

        Protected Sub doSetDisposalFacilityAddress(ByVal oForm As SAPbouiCOM.Form)
            Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            Dim oData As ArrayList = New ArrayList
            oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, getSharedData(oForm, "CARDCODE"), "S")

            Try
                If Not (oData Is Nothing) AndAlso oData.Count > 0 Then
                    setFormDFValue(oForm, "U_DSPName", getSharedData(oForm, "CARDNAME"))
                    setFormDFValue(oForm, "U_DOFAdres", oData.Item(0))
                    'setFormDFValue(oForm, "U_DOFPh", oData.Item(13))
                    'setFormDFValue(oForm, "U_DOFFax", oData.Item(22))
                    'setFormDFValue(oForm, "U_DOFEmail", oData.Item(23))

                    oBP = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
                    If oBP.GetByKey(getSharedData(oForm, "CARDCODE")) Then
                        setFormDFValue(oForm, "U_DOFRegNo", oBP.UserFields.Fields.Item("U_REGNO").Value)
                        'setFormDFValue(oForm, "U_DOFCntPr", oBP.ContactPerson)
                        setFormDFValue(oForm, "U_DOFCmpReg", oBP.CompanyRegistrationNumber)
                    Else
                        setFormDFValue(oForm, "U_DOFRegNo", "")
                        'setFormDFValue(oForm, "U_DOFCntPr", "")
                        setFormDFValue(oForm, "U_DOFCmpReg", "")
                    End If
                Else
                    setFormDFValue(oForm, "U_DOFRegNo", "")
                    setFormDFValue(oForm, "U_DSPName", "")
                    setFormDFValue(oForm, "U_DOFAdres", "")
                    'setFormDFValue(oForm, "U_DOFCntPr", "")
                    'setFormDFValue(oForm, "U_DOFPh", "")
                    'setFormDFValue(oForm, "U_DOFFax", "")
                    'setFormDFValue(oForm, "U_DOFEmail", "")
                    setFormDFValue(oForm, "U_DOFCmpReg", "")
                End If
                doSetUpdate(oForm)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the Disposal Facility Address")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXAGEN", {com.idh.bridge.Translation.getTranslatedWord("Disposal Facility")})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oBP)
            End Try
        End Sub

        Protected Sub doSetFinalDestinationAddress(ByVal oForm As SAPbouiCOM.Form)
            Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            Dim oData As ArrayList = New ArrayList
            oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, getSharedData(oForm, "CARDCODE"), "S")

            Try
                If Not (oData Is Nothing) AndAlso oData.Count > 0 Then
                    setFormDFValue(oForm, "U_FDSName", getSharedData(oForm, "CARDNAME"))
                    setFormDFValue(oForm, "U_FDTAdres", oData.Item(0))
                    'setFormDFValue(oForm, "U_FDTPh", oData.Item(13))
                    'setFormDFValue(oForm, "U_FDTFax", oData.Item(22))
                    'setFormDFValue(oForm, "U_FDTEmail", oData.Item(23))

                    oBP = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
                    If oBP.GetByKey(getSharedData(oForm, "CARDCODE")) Then
                        setFormDFValue(oForm, "U_FDTRegNo", oBP.UserFields.Fields.Item("U_REGNO").Value)
                        'setFormDFValue(oForm, "U_FDTCntPr", oBP.ContactPerson)
                        setFormDFValue(oForm, "U_FDTCmpReg", oBP.CompanyRegistrationNumber)
                    Else
                        setFormDFValue(oForm, "U_FDTRegNo", "")
                        'setFormDFValue(oForm, "U_FDTCntPr", "")
                        setFormDFValue(oForm, "U_FDTCmpReg", "")
                    End If
                Else
                    setFormDFValue(oForm, "U_FDTRegNo", "")
                    setFormDFValue(oForm, "U_FDSName", "")
                    setFormDFValue(oForm, "U_FDTAdres", "")
                    'setFormDFValue(oForm, "U_FDTCntPr", "")
                    'setFormDFValue(oForm, "U_FDTPh", "")
                    'setFormDFValue(oForm, "U_FDTFax", "")
                    'setFormDFValue(oForm, "U_FDTEmail", "")
                    setFormDFValue(oForm, "U_FDTCmpReg", "")
                End If
                doSetUpdate(oForm)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the Final Destination Address")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXAGEN", {com.idh.bridge.Translation.getTranslatedWord("Final Destination")})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oBP)
            End Try
        End Sub

        Protected Sub doSetFirstFacilityAddress(ByVal oForm As SAPbouiCOM.Form)
            Dim oBP As SAPbobsCOM.BusinessPartners = Nothing
            Dim oData As ArrayList = New ArrayList
            oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, getSharedData(oForm, "CARDCODE"), "S")

            Try
                If Not (oData Is Nothing) AndAlso oData.Count > 0 Then
                    setFormDFValue(oForm, "U_FFName", getSharedData(oForm, "CARDNAME"))
                    setFormDFValue(oForm, "U_FFAdres", oData.Item(0))
                    'setFormDFValue(oForm, "U_FFPh", oData.Item(13))
                    'setFormDFValue(oForm, "U_FFFax", oData.Item(22))
                    'setFormDFValue(oForm, "U_FFEmail", oData.Item(23))

                    oBP = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners)
                    If oBP.GetByKey(getSharedData(oForm, "CARDCODE")) Then
                        setFormDFValue(oForm, "U_FFRegNo", oBP.UserFields.Fields.Item("U_REGNO").Value)
                        'setFormDFValue(oForm, "U_FFCntPr", oBP.ContactPerson)
                        setFormDFValue(oForm, "U_FFCmpReg", oBP.CompanyRegistrationNumber)
                    Else
                        setFormDFValue(oForm, "U_FFRegNo", "")
                        'setFormDFValue(oForm, "U_FFCntPr", "")
                        setFormDFValue(oForm, "U_FFCmpReg", "")
                    End If
                Else
                    setFormDFValue(oForm, "U_FFRegNo", "")
                    setFormDFValue(oForm, "U_FFName", "")
                    setFormDFValue(oForm, "U_FFAdres", "")
                    'setFormDFValue(oForm, "U_FFCntPr", "")
                    'setFormDFValue(oForm, "U_FFPh", "")
                    'setFormDFValue(oForm, "U_FFFax", "")
                    'setFormDFValue(oForm, "U_FFEmail", "")
                    setFormDFValue(oForm, "U_FFCmpReg", "")
                End If
                doSetUpdate(oForm)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the Final Destination Address")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXAGEN", {com.idh.bridge.Translation.getTranslatedWord("First Facility Destination")})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oBP)
            End Try
        End Sub

        Protected Sub doFillTFSTypes(ByVal oForm As SAPbouiCOM.Form)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Dim sQry As String
            Dim oItem As SAPbouiCOM.Item
            Dim oTFSType As SAPbouiCOM.ComboBox

            Try
                sQry = "select U_TCode, U_TName from [@IDH_TFSType] "
                oRecordSet = goParent.goDB.doSelectQuery(sQry)
                If Not oRecordSet Is Nothing Then
                    If oRecordSet.RecordCount > 0 Then
                        oItem = oForm.Items.Item("IDH_TFSTYP")
                        oItem.DisplayDesc = True
                        oTFSType = oItem.Specific

                        doClearValidValues(oTFSType.ValidValues)

                        Dim iCount As Integer
                        Dim oCheck As ArrayList = New ArrayList
                        Dim sKey As String
                        Dim sValue As String
                        For iCount = 0 To oRecordSet.RecordCount - 1
                            sKey = oRecordSet.Fields.Item(0).Value
                            sValue = oRecordSet.Fields.Item(1).Value
                            Try
                                oTFSType.ValidValues.Add(sKey, sValue)
                            Catch ex As Exception
                            End Try
                            oRecordSet.MoveNext()
                        Next
                        'oTFSType.Select("01")
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the tfs types combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("tfs type")})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
            End Try

        End Sub

        Protected Sub doFillTFSStatus(ByVal oForm As SAPbouiCOM.Form)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Dim sQry As String
            Dim oItem As SAPbouiCOM.Item
            Dim oTFSStauts As SAPbouiCOM.ComboBox

            Try
                sQry = "select U_SCode, U_SName from [@IDH_TFSStatus] "
                oRecordSet = goParent.goDB.doSelectQuery(sQry)
                If Not oRecordSet Is Nothing Then
                    If oRecordSet.RecordCount > 0 Then
                        oItem = oForm.Items.Item("IDH_STAT")
                        oItem.DisplayDesc = True
                        oTFSStauts = oItem.Specific

                        Dim iCount As Integer
                        Dim oCheck As ArrayList = New ArrayList
                        Dim sKey As String
                        Dim sValue As String
                        For iCount = 0 To oRecordSet.RecordCount - 1
                            sKey = oRecordSet.Fields.Item(0).Value
                            sValue = oRecordSet.Fields.Item(1).Value
                            Try
                                oTFSStauts.ValidValues.Add(sKey, sValue)
                            Catch ex As Exception
                            End Try
                            oRecordSet.MoveNext()
                        Next
                        oTFSStauts.Select("01")
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the tfs types combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("tfs types")})
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
            End Try

        End Sub

        'Private Sub doFillPhyCharacteristicsCombo(ByVal oForm As SAPbouiCOM.Form)
        '    Dim oItem As SAPbouiCOM.Item
        '    oItem = oForm.Items.Item("IDH_PHYCHR")
        '    Dim oCombo As SAPbouiCOM.ComboBox
        '    oCombo = oItem.Specific
        '    doClearValidValues(oCombo.ValidValues)

        '    oCombo.ValidValues.Add("1", "Powdery/powder")
        '    oCombo.ValidValues.Add("2", "Solid")
        '    oCombo.ValidValues.Add("3", "Viscous/paste")
        '    oCombo.ValidValues.Add("4", "Sludgy")
        '    oCombo.ValidValues.Add("5", "Liquid")
        '    oCombo.ValidValues.Add("6", "Gaseous")
        '    oCombo.ValidValues.Add("7", "Other (specify)")
        'End Sub

        'Private Sub doFillMovStatus(ByVal oForm As SAPbouiCOM.Form, ByVal oComboCol As SAPbouiCOM.ComboBoxColumn)
        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '    Try
        '        oRecordSet = goParent.goDB.doSelectQuery("SELECT U_MCode, U_MName FROM [@IDH_MovStatus]")
        '        While Not oRecordSet.EoF
        '            oComboCol.ValidValues.Add(oRecordSet.Fields.Item(0).Value, oRecordSet.Fields.Item(1).Value)
        '            oRecordSet.MoveNext()
        '        End While
        '    Catch ex As Exception
        '        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Country Combo.")
        '        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Country")})
        '    Finally
        '        IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
        '    End Try
        'End Sub

        'Private Sub doFillUOM(ByVal oForm As SAPbouiCOM.Form, ByVal oComboCol As SAPbouiCOM.ComboBoxColumn)
        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '    Try
        '        oRecordSet = goParent.goDB.doSelectQuery("SELECT UnitDisply, UnitName FROM OWGT")
        '        While Not oRecordSet.EoF
        '            oComboCol.ValidValues.Add(oRecordSet.Fields.Item(0).Value, oRecordSet.Fields.Item(1).Value)
        '            oRecordSet.MoveNext()
        '        End While
        '    Catch ex As Exception
        '        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the UOM Combo.")
        '        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("UOM")})
        '    Finally
        '        IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
        '    End Try
        'End Sub

        'Private Sub doFillShipped(ByVal oForm As SAPbouiCOM.Form, ByVal oComboCol As SAPbouiCOM.ComboBoxColumn)
        '    Try
        '        oComboCol.ValidValues.Add("Y", "Yes")
        '        oComboCol.ValidValues.Add("N", "No")
        '    Catch ex As Exception
        '        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Status Combo.")
        '        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Status")})
        '    Finally
        '    End Try
        'End Sub

        'Private Sub doCountryCombo(ByVal oForm As SAPbouiCOM.Form)
        '    Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
        '    Dim oCombo As SAPbouiCOM.ComboBoxColumn
        '    oCombo = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_CNTCd"))
        '    oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

        '    doClearValidValues(oCombo.ValidValues)

        '    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '    Try
        '        oRecordSet = goParent.goDB.doSelectQuery("SELECT Code, Name FROM OCRY")
        '        While Not oRecordSet.EoF
        '            oCombo.ValidValues.Add(oRecordSet.Fields.Item(0).Value, oRecordSet.Fields.Item(1).Value)
        '            oRecordSet.MoveNext()
        '        End While
        '    Catch ex As Exception
        '        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Country Combo.")
        '        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Country")})
        '    Finally
        '        IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
        '    End Try
        'End Sub

        'Private Sub doFillPackaging(ByVal oForm As SAPbouiCOM.Form)
        '    Dim oItem As SAPbouiCOM.Item
        '    oItem = oForm.Items.Item("IDH_PKGLKP")
        '    Dim oCombo As SAPbouiCOM.ComboBox
        '    oCombo = oItem.Specific
        '    doClearValidValues(oCombo.ValidValues)
        '    oCombo.ValidValues.Add("1", "Drum")
        '    oCombo.ValidValues.Add("2", "Wooden barrel")
        '    oCombo.ValidValues.Add("3", "Jerrican")
        '    oCombo.ValidValues.Add("4", "Box")
        '    oCombo.ValidValues.Add("5", "Bag")
        '    oCombo.ValidValues.Add("6", "Composite packaging")
        '    oCombo.ValidValues.Add("7", "Pressure receptacle")
        '    oCombo.ValidValues.Add("8", "Bulk")
        '    oCombo.ValidValues.Add("9", "Other")
        'End Sub

        'Private Sub doFillPhysicalChars(ByVal oForm As SAPbouiCOM.Form)
        '    Dim oItem As SAPbouiCOM.Item
        '    oItem = oForm.Items.Item("IDH_PHYLKP")
        '    Dim oCombo As SAPbouiCOM.ComboBox
        '    oCombo = oItem.Specific
        '    doClearValidValues(oCombo.ValidValues)
        '    oCombo.ValidValues.Add("1", "Powdery/Powder")
        '    oCombo.ValidValues.Add("2", "Soild")
        '    oCombo.ValidValues.Add("3", "Viscous/Paste")
        '    oCombo.ValidValues.Add("4", "Sludgy")
        '    oCombo.ValidValues.Add("5", "Liquid")
        '    oCombo.ValidValues.Add("6", "Gaseous")
        '    oCombo.ValidValues.Add("9", "Other")
        'End Sub

        Protected Overridable Sub doDocReport(ByVal oForm As SAPbouiCOM.Form, ByVal sBtnName As String, ByVal sReportFile As String)
            Dim sTFSCode As String = getFormDFValue(oForm, "Code")

            If sReportFile.StartsWith("http://") Then
                ''Start ISB Code
                'Dim oParams As New Hashtable
                'oParams.Add("RowNum", sTFSCode)

                ''implementation of doCallReportDefaults
                'doCallHTTPRpts(goParent, oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType, idh.const.Lookup.INSTANCE.doGetBranch(goParent.gsUserName))
                ''End ISB Code
            Else
                Dim oParams As New Hashtable
                oParams.Add("TFSCode", sTFSCode)
                If sBtnName = "IDH_BTNPMO" Then
                    Dim oMovGrid As DBOGrid = getWFValue(oForm, "MOVEGR")
                    Dim oSelectedRows As SAPbouiCOM.SelectedRows
                    oSelectedRows = oMovGrid.getSBOGrid.Rows.SelectedRows

                    If oSelectedRows.Count > 0 Then
                        Dim oRows As ArrayList = oMovGrid.doPrepareListFromSelection("Code")
                        oParams.Add("MOVCode", oRows)
                    Else
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Select a TFS Movement row.", "ERTFSROW", {Nothing})
                        Exit Sub
                    End If
                End If
                If (Config.INSTANCE.useNewReportViewer()) Then
                    IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                    IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                    IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
                Else
                    IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
                End If
            End If
        End Sub

        Private Sub doSetInterimFacilityVisability(ByVal oForm As SAPbouiCOM.Form)
            Dim oItm As SAPbouiCOM.ComboBox = oForm.Items.Item("IDH_TFSTYP").Specific
            If oItm.Value.Length > 0 Then
                If oItm.Selected.Description.ToLower().StartsWith("interim") Then
                    oForm.Items.Item("IDH_BTNPIN").Visible = True
                    oForm.Items.Item("304").Visible = True
                    oForm.Items.Item("290").Visible = True
                    oForm.Items.Item("241").Visible = True
                    oForm.Items.Item("243").Visible = True
                    oForm.Items.Item("245").Visible = True
                    oForm.Items.Item("247").Visible = True
                    oForm.Items.Item("249").Visible = True
                    oForm.Items.Item("IDH_FFADD").Visible = True
                    oForm.Items.Item("IDH_FFCPR").Visible = True
                    oForm.Items.Item("IDH_FFPH").Visible = True
                    oForm.Items.Item("IDH_FFFAX").Visible = True
                    oForm.Items.Item("IDH_FFEML").Visible = True
                    oForm.Items.Item("IDH_FFLAD").Visible = True
                    oForm.Items.Item("IDH_FFLCP").Visible = True

                    oForm.Items.Item("309").Visible = True
                    oForm.Items.Item("316").Visible = True
                    oForm.Items.Item("311").Visible = True
                    oForm.Items.Item("317").Visible = True
                    oForm.Items.Item("314").Visible = True
                    oForm.Items.Item("IDH_FFCD").Visible = True
                    oForm.Items.Item("IDH_FFNM").Visible = True
                    oForm.Items.Item("IDH_FFCRG").Visible = True
                    oForm.Items.Item("IDH_FFRGN").Visible = True
                    oForm.Items.Item("IDH_FFL").Visible = True
                Else
                    oForm.Items.Item("IDH_BTNPIN").Visible = True
                    oForm.Items.Item("304").Visible = False
                    oForm.Items.Item("290").Visible = False
                    oForm.Items.Item("241").Visible = False
                    oForm.Items.Item("243").Visible = False
                    oForm.Items.Item("245").Visible = False
                    oForm.Items.Item("247").Visible = False
                    oForm.Items.Item("249").Visible = False
                    oForm.Items.Item("IDH_FFADD").Visible = False
                    oForm.Items.Item("IDH_FFCPR").Visible = False
                    oForm.Items.Item("IDH_FFPH").Visible = False
                    oForm.Items.Item("IDH_FFFAX").Visible = False
                    oForm.Items.Item("IDH_FFEML").Visible = False
                    oForm.Items.Item("IDH_FFLAD").Visible = False
                    oForm.Items.Item("IDH_FFLCP").Visible = False

                    oForm.Items.Item("309").Visible = False
                    oForm.Items.Item("316").Visible = False
                    oForm.Items.Item("311").Visible = False
                    oForm.Items.Item("317").Visible = False
                    oForm.Items.Item("314").Visible = False
                    oForm.Items.Item("IDH_FFCD").Visible = False
                    oForm.Items.Item("IDH_FFNM").Visible = False
                    oForm.Items.Item("IDH_FFCRG").Visible = False
                    oForm.Items.Item("IDH_FFRGN").Visible = False
                    oForm.Items.Item("IDH_FFL").Visible = False
                End If
            End If
        End Sub

#End Region

#Region "Grid Manipulation"
        Private Sub doFillGrids(ByVal oForm As SAPbouiCOM.Form, ByVal sTFSCd As String)
            Dim oMovGrid As DBOGrid = DBOGrid.getInstance(oForm, "MOVEGR") 'getWFValue(oForm, "MOVEGR")
            If oMovGrid IsNot Nothing Then
                Dim oMData As IDH_TFSMOVE = oMovGrid.DBObject
                oMData.getByTFSCode(sTFSCd)
                oMovGrid.doPostReloadData(True)
                'doEmployeeCombo(oMovGrid)
                doFillShipped(oMovGrid)
                doFillUOM(oMovGrid)
                doFillMoveStatus(oMovGrid)
            End If

            Dim oCountryGrid As DBOGrid = DBOGrid.getInstance(oForm, "CONTGR") 'getWFValue(oForm, "CONTGR")
            If oCountryGrid IsNot Nothing Then
                Dim oCData As IDH_TFSCARR = oCountryGrid.DBObject
                oCData.getByTFSCode(sTFSCd)
                oCountryGrid.doPostReloadData(True)
                'doFillStateType(oCountryGrid)
                doFillCountries(oCountryGrid)
            End If

            'Dim oDownGrid As DBOGrid = getWFValue(oForm, "DOWNTGR")
            'Dim oDTData As IDH_RCDOWNT = oDownGrid.DBObject
            'oDTData.getByRCRow(sRCCode)
            'oDownGrid.doPostReloadData(True)

        End Sub

        Private Sub doManageMovementsGrid(ByVal oForm As SAPbouiCOM.Form)
            'Employee Grid 
            Dim oMovGrid As DBOGrid = getWFValue(oForm, "MOVEGR")
            Dim oTFSMove As IDH_TFSMOVE = Nothing
            If oMovGrid Is Nothing Then
                oMovGrid = DBOGrid.getInstance(oForm, "MOVEGR")
                If oMovGrid Is Nothing Then
                    If oTFSMove Is Nothing Then
                        oTFSMove = New IDH_TFSMOVE(Me, oForm)
                        'oRCEmployee.U_ROUTECL = getFormDFValue(oForm, "U_ROUTECL")
                    End If

                    oMovGrid = New DBOGrid(Me, oForm, "MOVEGR", "MOVEGR", oTFSMove)
                    oMovGrid.getSBOItem().AffectsFormMode = True
                    oMovGrid.doSetDeleteActive(True)
                Else
                    oTFSMove = oMovGrid.DBObject()
                End If
                setWFValue(oForm, "MOVEGR", oMovGrid)
            Else
                oTFSMove = oMovGrid.DBObject()
            End If
            oTFSMove.getData()

            ''Set the grid list fields
            oMovGrid.doAddListField("Code", "Code", False, 5, Nothing, Nothing)
            oMovGrid.doAddListField("Name", "Name", False, 0, Nothing, Nothing)
            oMovGrid.doAddListField("U_TFSCd", "TFS Code", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_UserDefinedObject)
            oMovGrid.doAddListField("U_TFSNo", "TFS Number", False, 0, Nothing, Nothing)
            oMovGrid.doAddListField("U_ShpNo", "Shipment No", True, 10, Nothing, Nothing)
            oMovGrid.doAddListField("U_PreNotDt", "Pre-Notf. Dt", True, 10, Nothing, Nothing)
            oMovGrid.doAddListField("U_ShpDt", "Shipment Date", True, 10, Nothing, Nothing)
            oMovGrid.doAddListField("U_CarCd", "Carrier Code", True, -1, "SRC:IDHCSRCH(CUS)[IDH_BPCOD;IDH_TYPE=%F-S][CARDCODE;U_CarNm=CARDNAME]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oMovGrid.doAddListField("U_CarNm", "Carrier Name", False, 10, Nothing, Nothing)
            oMovGrid.doAddListField("U_Shipd", "Shipped", True, 10, "COMBOBOX", Nothing)
            oMovGrid.doAddListField("U_UOM", "UOM", True, 20, "COMBOBOX", Nothing)
            oMovGrid.doAddListField("U_Bal", "Balance", True, 10, Nothing, Nothing)
            oMovGrid.doAddListField("U_Status", "Status", True, 30, "COMBOBOX", Nothing)
            oMovGrid.doAddListField("U_WRHdr", "Waste Order Header", False, 10, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_UserDefinedObject)
            oMovGrid.doAddListField("U_WRRow", "Waste Order Row", False, 10, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_UserDefinedObject)
            oMovGrid.doAddListField("U_ReceiptDt", "Receipt Date", True, 10, Nothing, Nothing)
            oMovGrid.doAddListField("U_Tonnage", "Tonnage", True, 10, Nothing, Nothing)
            oMovGrid.doAddListField("U_RecoveryDt", "Recovery Date", True, 10, Nothing, Nothing)

            oMovGrid.doSynchFields()

        End Sub

        Private Sub doManageCountryGrid(ByVal oForm As SAPbouiCOM.Form)
            'Employee Grid 
            Dim oCountryGrid As DBOGrid = getWFValue(oForm, "CONTGR")
            Dim oTFSCont As IDH_TFSCARR = Nothing
            If oCountryGrid Is Nothing Then
                oCountryGrid = DBOGrid.getInstance(oForm, "CONTGR")
                If oCountryGrid Is Nothing Then
                    If oTFSCont Is Nothing Then
                        oTFSCont = New IDH_TFSCARR(Me, oForm)
                        'oRCEmployee.U_ROUTECL = getFormDFValue(oForm, "U_ROUTECL")
                    End If

                    oCountryGrid = New DBOGrid(Me, oForm, "CONTGR", "CONTGR", oTFSCont)
                    oCountryGrid.getSBOItem().AffectsFormMode = True
                    oCountryGrid.doSetDeleteActive(True)
                Else
                    oTFSCont = oCountryGrid.DBObject()
                End If
                setWFValue(oForm, "CONTGR", oCountryGrid)
            Else
                oTFSCont = oCountryGrid.DBObject()
            End If
            oTFSCont.getData()

            ''Set the grid list fields
            oCountryGrid.doAddListField("Code", "Code", False, 5, Nothing, Nothing)
            oCountryGrid.doAddListField("Name", "Name", False, 0, Nothing, Nothing)
            'oCountryGrid.doAddListField("U_StateTyp", "State Type", True, -1, "COMBOBOX", Nothing)
            oCountryGrid.doAddListField("U_Country", "Country", True, -1, "COMBOBOX", Nothing)
            oCountryGrid.doAddListField("U_CACode", "CompAuth Code", True, -1, "SRC:IDHTFSCA(TCA)[IDH_CACD][U_CACode=CACODE;U_CAName=CANAME]", Nothing)
            oCountryGrid.doAddListField("U_CAName", "CompAuth Name", False, -1, Nothing, Nothing)
            oCountryGrid.doAddListField("U_ENTCity", "Entry City", True, -1, Nothing, Nothing)
            oCountryGrid.doAddListField("U_ENTCntCd", "Entry Country", True, -1, "COMBOBOX", Nothing)
            oCountryGrid.doAddListField("U_EXTCity", "Exit City", True, -1, Nothing, Nothing)
            oCountryGrid.doAddListField("U_EXTCntCd", "Exit Country", True, -1, "COMBOBOX", Nothing)
            'oCountryGrid.doAddListField("U_CarrCd", "Carrier Code", True, -1, Nothing, Nothing)

            oCountryGrid.doAddListField("U_TFSCd", "TFS Code", False, 0, Nothing, Nothing)
            oCountryGrid.doAddListField("U_TFSNo", "TFS No", False, 0, Nothing, Nothing)

            oCountryGrid.doSynchFields()

        End Sub

        Private Sub doSetGridsEnable(ByVal oForm As SAPbouiCOM.Form, ByVal bEnable As Boolean)
            Dim oMovGrid As DBOGrid = getWFValue(oForm, "MOVEGR")
            If Not oMovGrid Is Nothing Then
                oMovGrid = DBOGrid.getInstance(oForm, "MOVEGR")
                oMovGrid.doEnabled(bEnable)
            End If

            Dim oCountryGrid As DBOGrid = getWFValue(oForm, "CONTGR")
            If Not oCountryGrid Is Nothing Then
                oCountryGrid = DBOGrid.getInstance(oForm, "CONTGR")
                oCountryGrid.doEnabled(bEnable)
            End If

            'Dim oDownGrid As DBOGrid = getWFValue(oForm, "DOWNTGR")
            'If Not oDownGrid Is Nothing Then
            '    oDownGrid = DBOGrid.getInstance(oForm, "DOWNTGR")
            '    oDownGrid.doEnabled(bEnable)
            'End If
        End Sub

        Private Function doUpdateGridRows(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim sTFSCd As String = getFormDFValue(oForm, "Code")

            Dim oMovGrid As DBOGrid = getWFValue(oForm, "MOVEGR")
            If oMovGrid.dataHasChanged Then
                For ctr As Integer = 0 To oMovGrid.getRowCount() - 1
                    oMovGrid.doSetFieldValue("U_TFSCd", ctr, sTFSCd, True)
                Next
                oMovGrid.doProcessData()
            End If

            Dim oCountryGrid As DBOGrid = getWFValue(oForm, "CONTGR")
            If oCountryGrid.dataHasChanged Then
                For ctr As Integer = 0 To oCountryGrid.getRowCount() - 1
                    oCountryGrid.doSetFieldValue("U_TFSCd", ctr, sTFSCd, True)
                Next
                oCountryGrid.doProcessData()
            End If

            'Dim oDownGrid As DBOGrid = getWFValue(oForm, "DOWNTGR")
            'If oDownGrid.dataHasChanged Then
            '    For ctr As Integer = 0 To oDownGrid.getRowCount() - 1
            '        oDownGrid.doSetFieldValue("U_ROUTECL", ctr, sRCCode, True)
            '    Next
            '    oDownGrid.doProcessData()
            'End If

            Return True
        End Function

        Public Sub doSetLastLine(ByVal oForm As SAPbouiCOM.Form, iRow As Int32, sGridID As String)
            Dim oGridN As DBOGrid = DBOGrid.getInstance(oForm, sGridID)
            If iRow = -1 Then
                iRow = oGridN.getCurrentDataRowIndex()
            End If
            Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode("GENSEQ")
            oGridN.doSetFieldValue("Code", iRow, oNumbers.CodeCode, True)
            oGridN.doSetFieldValue("Name", iRow, oNumbers.NameCode, True)
            Dim sTFSCode As String = getItemValue(oForm, "IDH_CODE").ToString.Trim
            Dim sTFSNo As String = getItemValue(oForm, "IDH_TFSNO1").ToString.Trim

            oGridN.doSetFieldValue("U_TFSCd", iRow, sTFSCode, True)
            oGridN.doSetFieldValue("U_TFSNo", iRow, sTFSNo, True)

            'If sGridID = "MOVEGR" Then
            '    'oGridN.doSetFieldValue("U_RCEMP", iRow, "", True)
            '    'oGridN.doSetFieldValue("U_RCPRIM", iRow, "", True)
            '    'oGridN.doSetFieldValue("U_RCRTASST", iRow, "", True)
            '    oGridN.doSetFieldValue("U_RCCLOCKIN", iRow, "00:00", True)
            '    oGridN.doSetFieldValue("U_RCCLOCKOUT", iRow, "00:00", True)
            '    oGridN.doSetFieldValue("U_RCHOUR1", iRow, "00:00", True)
            'ElseIf sGridID = "LANDFGR" Then
            '    oGridN.doSetFieldValue("U_RCONROUTE", iRow, "00:00", True)
            '    oGridN.doSetFieldValue("U_RCOFFROUTE", iRow, "00:00", True)
            '    'oGridN.doSetFieldValue("U_RCLOADTP", iRow, "", True)
            '    'oGridN.doSetFieldValue("U_RCSITE", iRow, "", True)
            '    oGridN.doSetFieldValue("U_RCTIMEIN", iRow, "00:00", True)
            '    oGridN.doSetFieldValue("U_RCTIMEOUT", iRow, "00:00", True)
            '    'oGridN.doSetFieldValue("U_RCTKTNO", iRow, "", True)
            '    oGridN.doSetFieldValue("U_RCTONS", iRow, 0, True)
            '    oGridN.doSetFieldValue("U_RCCOST", iRow, 0, True)
            'ElseIf sGridID = "DOWNTGR" Then
            '    oGridN.doSetFieldValue("U_RCTIMED", iRow, "00:00", True)
            '    oGridN.doSetFieldValue("U_RCTIMEU", iRow, "00:00", True)
            '    'oGridN.doSetFieldValue("U_RCSPILL", iRow, "", True)
            '    'oGridN.doSetFieldValue("U_RCREASON", iRow, "", True)
            'End If
        End Sub

        Private Sub doEmployeeCombo(ByVal oGridN As DBOGrid)
            'Combobox2.doFillCombo(oCombo, "OHEM", "empID", "(CAST(empID as nvarchar) + ' - ' + lastName + ' ' + firstName) As Detail", null, "lastName", sBlankDescription);
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC(IDH_RCEMPLOYEE._RCEMP))
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

                doFillCombo(oGridN.getSBOForm(), oCombo, "OHEM", "empID", "(CAST(empID as nvarchar) + ' - ' + lastName + ' ' + firstName) As Detail", Nothing, Nothing, Nothing, Nothing)
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("UOM")})
            End Try
        End Sub

        Private Sub doFillShipped(ByVal oGridN As DBOGrid)
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC(IDH_TFSMOVE._Shipd))
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

                oCombo.ValidValues.Add("Y", "Yes")
                oCombo.ValidValues.Add("N", "No")
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Shipped")})
            Finally
            End Try
        End Sub

        Private Sub doFillUOM(ByVal oGridN As DBOGrid)
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC(IDH_TFSMOVE._UOM))
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

                doFillCombo(oGridN.getSBOForm(), oCombo, "OWGT", "UnitDisply", "UnitName", Nothing, Nothing, Nothing, Nothing)
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Shipped")})
            Finally
            End Try
        End Sub

        Private Sub doFillMoveStatus(ByVal oGridN As DBOGrid)
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC(IDH_TFSMOVE._Status))
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

                doFillCombo(oGridN.getSBOForm(), oCombo, "[@IDH_MovStatus]", "U_MCode", "U_MName", Nothing, Nothing, Nothing, Nothing)
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Shipped")})
            Finally
            End Try
        End Sub

        Private Sub doFillStateType(ByVal oGridN As DBOGrid)
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC(IDH_TFSCARR._StateTyp))
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

                doFillCombo(oGridN.getSBOForm(), oCombo, "[@IDH_TFSTYPE]", "U_TCode", "U_TName", Nothing, Nothing, Nothing, Nothing)
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("StateType")})
            Finally
            End Try
        End Sub

        Private Sub doFillCountries(ByVal oGridN As DBOGrid)
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC(IDH_TFSCARR._Country))
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
                doFillCombo(oGridN.getSBOForm(), oCombo, "OCRY", "Code", "Name", Nothing, Nothing, "N/A", "")

                oCombo = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC(IDH_TFSCARR._ENTCntCd))
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
                doFillCombo(oGridN.getSBOForm(), oCombo, "OCRY", "Code", "Name", Nothing, Nothing, "N/A", "")

                oCombo = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC(IDH_TFSCARR._EXTCntCd))
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
                doFillCombo(oGridN.getSBOForm(), oCombo, "OCRY", "Code", "Name", Nothing, Nothing, "N/A", "")
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Countries")})
            Finally
            End Try
        End Sub

#End Region

    End Class
End Namespace
