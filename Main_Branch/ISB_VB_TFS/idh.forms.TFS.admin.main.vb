﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Namespace idh.forms.TFS.admin
    Public Class TFSMainForm

#Region "Forms Registration"
        Public Sub doAddForms()

        End Sub

#End Region
    End Class

    Public Class TFSDatastructures

#Region "Configuration"
        Public Sub doConfigTableData()

        End Sub

#End Region

#Region "Messages"
        Public Sub doAddMessages()

        End Sub

#End Region

#Region "User Tables"
        Public Sub doSetupUserDefined(ByRef CurrentReleaseCount As Double, ByRef oBase As IDHAddOns.idh.data.Base)

        End Sub

        Public Sub doAddViews()

        End Sub

#End Region

    End Class
End Namespace

