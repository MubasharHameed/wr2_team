Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class WPosCodes
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_WTPOCO", sParMenu, iMenuPosition, Nothing, "Waste Position Codes" )
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Waste Position Codes"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_WTPOCO"
        End Function

    End Class
End Namespace
