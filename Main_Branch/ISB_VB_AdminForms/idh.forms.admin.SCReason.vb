Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class SCReason
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHSCREAS", sParMenu, iMenuPosition, Nothing, "Service Call Reasons" )
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_SCREAS"
        End Function

    End Class
End Namespace
