Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge

Namespace idh.forms.admin
    Public Class TipZones
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHTZONE", sParMenu, iMenuPosition, Nothing, "Tip Zones Setup" )
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Tip Zones Setup"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_TPZONES"
        End Function

		Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
			doBranchCombo(oForm)
		End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Desc", "Description", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Branch", "Branch", True, -1, "COMBOBOX", Nothing, -1)
        End Sub
        
        Private Sub doBranchCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_Branch")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            'oCombo.ValidValues.Add("", getTranslatedWord("Any"))
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDB.doSelectQuery("select Code, Remarks from OUBR")
                While Not oRecordSet.EoF
                	Try
                        oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(1).Value, String))
                    Catch ex As Exception
                    End Try
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Branch combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Branch")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
       End Sub

    End Class
End Namespace
