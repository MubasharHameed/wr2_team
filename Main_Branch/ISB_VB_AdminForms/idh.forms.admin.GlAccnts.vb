Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System
Imports com.idh.bridge.resources

Imports com.idh.bridge
Imports com.idh.dbObjects.User
Imports com.uBC.utils
Imports com.idh.bridge.lookups

Namespace idh.forms.admin
    Public Class GlAccnts
        Inherits idh.forms.admin.Tmpl
        'Private iFilterBackColor As Integer = &HFFCCCC
        Private iFilterBackColor As Integer = -1

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHGlAccnt", sParMenu, iMenuPosition, "GLMasterAdmin.srf", "GL Account Setup")
        End Sub

        '        Protected Overrides Function getTitle() As String
        '            Return "GL Account Setup"
        '        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_GLMAS"
        End Function

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            'MyBase.doBeforeLoadData(oForm)
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
            End If
            doSetFilterFields(oGridN)
            'doSetListFields(oGridN)
            oGridN.doAddGridTable(New com.idh.bridge.data.GridTable(getTable(), Nothing, "Code", True, True), True)
            oGridN.doSetDoCount(True)
            oGridN.doSetHistory(getTable(), "Code")

            If getHasSharedData(oForm) Then
                doReadInputParams(oForm)
            End If
            'Set the required List Fields
            'Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            doTheGridLayout(oGridN)

            'Fill filters combos
            Dim oCombo As SAPbouiCOM.ComboBox

            'Order Type 
            oCombo = CType(oForm.Items.Item("IDH_ORDTYP").Specific, SAPbouiCOM.ComboBox)
            doClearValidValues(oCombo.ValidValues)
            oCombo.ValidValues.Add("", "Any")
            oCombo.ValidValues.Add("DO", "Disposal Order")
            oCombo.ValidValues.Add("WO", "Waste Order")

            'Job Type 
            doFilterJobTypeCombo(oForm)
            'oCombo = oForm.Items.Item("IDH_JOBTYP").Specific
            'doFillCombo(oForm, oCombo, "@IDH_JOBTYPE", "U_JobTp", "U_JobTp", Nothing, Nothing, "Any", "")

            'Branch
            oCombo = CType(oForm.Items.Item("IDH_BRANCH").Specific, SAPbouiCOM.ComboBox)
            doFillCombo(oForm, oCombo, "OUBR", "Code", "Remarks", Nothing, Nothing, "Any", "")

            'Postal Value Src
            oCombo = CType(oForm.Items.Item("IDH_PSTVSR").Specific, SAPbouiCOM.ComboBox)
            doClearValidValues(oCombo.ValidValues)
            oCombo.ValidValues.Add("", "Any")
            oCombo.ValidValues.Add("TipTot", "Disposal Cost")
            oCombo.ValidValues.Add("TAddCost", "Additional Cost")
            oCombo.ValidValues.Add("OrdTot", "Haulage Cost")
            oCombo.ValidValues.Add("PCTotal", "Purchase Cost")

            ''20151013: KA/HG Discuss not to have Grand Total columns as 
            ''there wont be a chance to use them in a Journal Transaction 

            '20151202: KA/HG have revert back to what it was before. 
            'Bring those options back 
            oCombo.ValidValues.Add("JCost", "Total Order Cost")
            oCombo.ValidValues.Add("TCTotal", "Disposal Charge")
            oCombo.ValidValues.Add("TAddChrg", "Additional Charges")
            oCombo.ValidValues.Add("Price", "Haulage Charge")
            oCombo.ValidValues.Add("Total", "Total Order Charge")

            'Intra - NonIntra 
            oCombo = CType(oForm.Items.Item("IDH_ICNIC").Specific, SAPbouiCOM.ComboBox)
            doClearValidValues(oCombo.ValidValues)
            oCombo.ValidValues.Add("", "I/C & Non I/C")
            oCombo.ValidValues.Add("I/C", "I/C")
            oCombo.ValidValues.Add("Non I/C", "Non I/C")

            'Transaction Type 
            oCombo = CType(oForm.Items.Item("IDH_TRNTYP").Specific, SAPbouiCOM.ComboBox)
            doClearValidValues(oCombo.ValidValues)
            oCombo.ValidValues.Add("", "Any")
            oCombo.ValidValues.Add("PO", "Purchace Order")
            oCombo.ValidValues.Add("PJ", "PO - Journal Entry")
            oCombo.ValidValues.Add("PG", "PO - GRNI")
            oCombo.ValidValues.Add("PA", "PO - Accruals")
            oCombo.ValidValues.Add("SO", "Sales Order")
            oCombo.ValidValues.Add("SJ", "SO - Journal Entry")
            oCombo.ValidValues.Add("SA", "SO - Accruals")
            oCombo.ValidValues.Add("JR", "Journal Entry")
            oCombo.ValidValues.Add("TR", "StockTransfer")
        End Sub

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
            doGetItemGroups(oForm)
            doFillWasteItemGroups(oForm)
            doOrderCombo(oForm)
            doFillWR1OrderTypes(oForm)
            doFillObligated(oForm)
            doBranchCombo(oForm)
            doFillTransactionTypes(oForm)
            doFillTipSites(oForm)
            doFillPostingValueSourceTypes(oForm)
            doFillWR1TransactionCode(oForm)
            doFillDimensions(oForm)

            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            oGridN.doSetDeleteActive(True)

        End Sub


        Protected Sub doTheGridLayout(ByVal oGridN As UpdateGrid)
            If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", False) Then
                Dim sFormTypeId As String = oGridN.getSBOForm().TypeEx
                Dim oFormSettings As IDH_FORMSET = getWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId)
                If oFormSettings Is Nothing Then
                    oFormSettings = New IDH_FORMSET()
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, "")
                    setWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId, oFormSettings)

                    oFormSettings.doAddFieldToGrid(oGridN)

                    doSetListFields(oGridN)
                    ' doAddAdditionalRequiredListFields(oGridN)

                    oFormSettings.doSyncDB(oGridN)
                Else
                    If oFormSettings.SkipFormSettings Then
                        doSetListFields(oGridN)
                        '    doAddAdditionalRequiredListFields(oGridN)
                    Else
                        oFormSettings.doAddFieldToGrid(oGridN)
                    End If
                End If
            Else
                doSetListFields(oGridN)
                'doAddAdditionalRequiredListFields(oGridN)
            End If
        End Sub


        '****Start from here... form giving erros 
        Protected Overrides Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddFilterField("IDH_ORDTYP", "U_WR1ORD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30)
            oGridN.doAddFilterField("IDH_JOBTYP", "U_JobTp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30)
            oGridN.doAddFilterField("IDH_BRANCH", "U_Branch", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30)
            oGridN.doAddFilterField("IDH_PSTVSR", "U_PVSrc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30)
            oGridN.doAddFilterField("IDH_ICNIC", "U_TipSite", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30)
            oGridN.doAddFilterField("IDH_TRNTYP", "U_TRNType", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30)

            'oGridN.doAddFilterField("IDH_ONTIP", "U_DoTip", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            'oGridN.doAddFilterField("IDH_ONHAUL", "U_DoHaul", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            'oGridN.doAddFilterField("IDH_ADDITM", "U_AddCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            'oGridN.doAddFilterField("IDH_TIPSIT", "U_TipSite", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            oGridN.doAddFilterField("IDH_CARRCD", "U_CarrCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_PRODCD", "U_ProdCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_TPSTCD", "U_SCardCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

            oGridN.doAddFilterField("IDH_ADIVCD", "U_ADIVendCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_DBFACC", "U_DebFAcc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_CRFACC", "U_CreFAcc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
        End Sub


        '** DELETE FROM [@IDH_FORMSET] WHERE U_FormID = 'IDHGlAccnt'
        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            Dim sGrp As String = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup()
            Dim sSiteGrp As String = com.idh.bridge.lookups.Config.Parameter("BGSDispo")

            oGridN.doAddListField("Code", "Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_WR1ORD", "WR1 OrderType", True, -1, "COMBOBOX", Nothing, iFilterBackColor)
            oGridN.doAddListField("U_JobTp", "Order Type", True, -1, "COMBOBOX", Nothing, iFilterBackColor)
            oGridN.doAddListField("U_ItmGrp", "Container Group", True, -1, "COMBOBOX", Nothing, iFilterBackColor) ', SAPbouiCOM.BoLinkedObject.lf_ItemGroups)
            oGridN.doAddListField("U_ItemCd", "Container Code", True, -1, "SRC*IDHISRC(PROD)[IDH_ITMCOD=#U_ItemCd;IDH_GRPCOD=#U_ItmGrp][U_ItemCd=ITEMCODE;U_ItemDs=ITEMNAME]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("U_ItemDs", "Container Description", True, -1, Nothing, Nothing, iFilterBackColor)
            oGridN.doAddListField("U_WItmGrp", "Waste Item Group", True, -1, "COMBOBOX", Nothing, iFilterBackColor) ', SAPbouiCOM.BoLinkedObject.lf_ItemGroups)
            oGridN.doAddListField("U_WasteCd", "Waste Code", True, -1, "SRC*IDHWISRC(WAST)[IDH_ITMCOD=#U_WasteCd;IDH_GRPCOD=%" & sGrp & ";IDH_INVENT=N][U_WasteCd=ITEMCODE;U_WasteDs=ITEMNAME]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("U_WasteDs", "Waste Description", True, -1, Nothing, Nothing, iFilterBackColor)
            oGridN.doAddListField("U_AddCd", "Additional Code", True, -1, "SRC*IDHISRC(ADD)[IDH_ITMCOD;IDH_GRPCOD=%][ITEMCODE;U_AddDs=ITEMNAME]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("U_AddDs", "Additional Services Description", True, -1, Nothing, Nothing, iFilterBackColor)
            oGridN.doAddListField("U_Branch", "Branch", True, -1, "COMBOBOX", Nothing, iFilterBackColor)
            oGridN.doAddListField("U_VehType", "Vehicle Type", True, -1, Nothing, Nothing, iFilterBackColor)
            oGridN.doAddListField("U_DoTip", "On Tipping", True, -1, "CHECKBOX", Nothing, iFilterBackColor)
            oGridN.doAddListField("U_DoHaul", "On Haulage", True, -1, "CHECKBOX", Nothing, iFilterBackColor)
            'OnTime [#Ico000????] USA 
            'START
            oGridN.doAddListField("U_DoAdItem", "On Additional Item", True, -1, "CHECKBOX", Nothing, iFilterBackColor)

            'END
            'OnTime [#Ico000????] USA 

            oGridN.doAddListField("U_CustCd", "Customer Code", True, -1, "SRC*IDHCSRCH(CUST)[IDH_BPCOD;IDH_TYPE=%F-C][CARDCODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("U_PostVal", "Posting Value", True, -1, Nothing, Nothing, iFilterBackColor)

            'OnTime [#Ico000????] USA 
            'START
            oGridN.doAddListField("U_PVSrc", "Posting Value Source", True, -1, "COMBOBOX", Nothing, iFilterBackColor)

            'END
            'OnTime [#Ico000????] USA 

            oGridN.doAddListField("U_TipSite", "Tip Site", True, -1, "COMBOBOX", Nothing, iFilterBackColor)
            oGridN.doAddListField("U_SCardCd", "Tip Site Code", True, -1, "SRC*IDHCSRCH(TIP)[IDH_BPCOD;IDH_TYPE=%S;IDH_GROUP=" & sSiteGrp & "][CARDCODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)

            oGridN.doAddListField("U_ADIVendCd", "Add. Item Vendor Code", True, -1, "SRC*IDHCSRCH(AIVC)[IDH_BPCOD;IDH_TYPE=%][CARDCODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("U_CarrCd", "Carrier Code", True, -1, "SRC*IDHCSRCH(CARR)[IDH_BPCOD;IDH_TYPE=%][CARDCODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("U_ProdCd", "Producer Code", True, -1, "SRC*IDHCSRCH(PROD)[IDH_BPCOD;IDH_TYPE=%][CARDCODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)

            oGridN.doAddListField("U_TRNType", "Transaction Type", True, -1, "COMBOBOX", Nothing, iFilterBackColor, Nothing)

            'oGridN.doAddListField("U_DebFAcc", "Debit FAccount", True, -1, "SRC*IDHGLSRC(DEBT)[IDH_FCODE][IDHFCODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_GLAccounts)

            'Movinng P&L Debit/Credit Accounts and Balance Sheet Debit/Credit Account 
            'Also the Dimension columns 
            ''oGridN.doAddListField("U_DebAcc", "DebitAccount", False, -1, Nothing, Nothing, iFilterBackColor) 'This column is used in Marketing Docs
            ''oGridN.doAddListField("U_DebFAcc", "Debit FormatCd", True, -1, Nothing, Nothing, iFilterBackColor)

            ''oGridN.doAddListField("U_CreAcc", "CreditAccount", False, -1, Nothing, Nothing, iFilterBackColor) 'This column is used in Marketing Docs
            ''oGridN.doAddListField("U_CreFAcc", "Credit FormatCd", True, -1, Nothing, Nothing, iFilterBackColor)

            '20160407: Addition of Balance Sheet Accounts 
            'The above change was done for ECWaste. When they will 'doJournals' on billing manager 
            'System is going to include two more lines in Journal entry with Balance Sheet Accounts 
            ''oGridN.doAddListField("U_BSDebAcc", "Balance Sheet Db Acc", False, -1, Nothing, Nothing, iFilterBackColor)
            ''oGridN.doAddListField("U_BSDebFAcc", "BS Deb Format Cd", True, -1, Nothing, Nothing, iFilterBackColor)

            ''oGridN.doAddListField("U_BSCreAcc", "Balance Sheet Cr Acc", False, -1, Nothing, Nothing, iFilterBackColor)
            ''oGridN.doAddListField("U_BSCreFAcc", "BS Cre FormatCd", True, -1, Nothing, Nothing, iFilterBackColor)

            doSetDimensionsListFields(oGridN)

            'Warehouse
            '"SRC*IDHCSRCH(AIVC)[IDH_BPCOD;IDH_TYPE=%S][CARDCODE]"
            oGridN.doAddListField("U_WhseCd", "Warehouse Code", True, -1, Nothing, Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_Warehouses)
            oGridN.doAddListField("U_WhseNm", "Warehouse Name", True, -1, Nothing, Nothing, iFilterBackColor)

            oGridN.doAddListField("U_ProjCd", "Project Code", True, -1, "SRC*IDHPRJSRC(PRJ)[IDH_CODE][IDH_CODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_ProjectCodes)

            ''START 
            ''201509: Dimensions - 4 new fields 
            'Dim sUseMltDims As String = CType(com.idh.bridge.lookups.Config.INSTANCE.doLookupTableField("OADM", "UseMltDims", Nothing), String)
            'If sUseMltDims.Equals("Y") Then
            '    Dim oCompServices As SAPbobsCOM.CompanyService = goParent.goDICompany.GetCompanyService()
            '    Dim oDimService As SAPbobsCOM.DimensionsService = CType(oCompServices.GetBusinessService(SAPbobsCOM.ServiceTypes.DimensionsService), SAPbobsCOM.DimensionsService)
            '    Dim oDimParams As SAPbobsCOM.DimensionsParams = oDimService.GetDimensionList()
            '    Dim oDim As SAPbobsCOM.Dimension

            '    'For i As Integer = 0 To oDimParams.Count - 1
            '    'Hardcoding the dimension count for future enhancement 
            '    If oDimParams.Count > 0 Then
            '        'For i As Integer = 0 To 4
            '        '    oDim = oDimService.GetDimension(oDimParams.Item(i))
            '        '    Dim bDimChk As Boolean = oDim.IsActive
            '        'Next

            '        'Old columns logic 
            '        'oGridN.doAddListField("U_ProfCn", "Profit Centre", True, -1, "SRC*IDHPRCSRC(PRC)[IDH_CODE][IDH_CODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_ProfitCenter)
            '        'oGridN.doAddListField("U_Dimension2", "Dimension 2", True, -1, "SRC*IDHPRCSRC(DIM2)[IDH_CODE][IDH_CODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_ProfitCenter)
            '        'oGridN.doAddListField("U_Dimension3", "Dimension 3", True, -1, "SRC*IDHPRCSRC(DIM3)[IDH_CODE][IDH_CODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_ProfitCenter)
            '        'oGridN.doAddListField("U_Dimension4", "Dimension 4", True, -1, "SRC*IDHPRCSRC(DIM4)[IDH_CODE][IDH_CODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_ProfitCenter)
            '        'oGridN.doAddListField("U_Dimension5", "Dimension 5", True, -1, "SRC*IDHPRCSRC(DIM5)[IDH_CODE][IDH_CODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_ProfitCenter)

            '        'Based on each Dimension attribute 
            '        'START FROM HERE: ADD FILTER TO THE SEARCH BOX FOR DIMENSIONS
            '        oDim = oDimService.GetDimension(oDimParams.Item(0))
            '        'oGridN.doAddListField("U_ProfCn", oDim.DimensionDescription, True, If(oDim.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "SRC*IDHPRCSRC(PRC)[IDH_CODE][IDH_CODE;DimCode=1]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_ProfitCenter)
            '        oGridN.doAddListField("U_ProfCn", oDim.DimensionDescription, True, If(oDim.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "SRC*IDHPRCSRC(PRC)[IDH_DIMC=1][IDHCODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_ProfitCenter)

            '        oDim = oDimService.GetDimension(oDimParams.Item(1))
            '        oGridN.doAddListField("U_Dimension2", oDim.DimensionDescription, True, If(oDim.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "SRC*IDHPRCSRC(DIM2)[IDH_DIMC=2][IDHCODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_ProfitCenter)

            '        oDim = oDimService.GetDimension(oDimParams.Item(2))
            '        oGridN.doAddListField("U_Dimension3", oDim.DimensionDescription, True, If(oDim.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "SRC*IDHPRCSRC(DIM3)[IDH_DIMC=3][IDHCODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_ProfitCenter)

            '        oDim = oDimService.GetDimension(oDimParams.Item(3))
            '        oGridN.doAddListField("U_Dimension4", oDim.DimensionDescription, True, If(oDim.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "SRC*IDHPRCSRC(DIM4)[IDH_DIMC=4][IDHCODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_ProfitCenter)

            '        oDim = oDimService.GetDimension(oDimParams.Item(4))
            '        oGridN.doAddListField("U_Dimension5", oDim.DimensionDescription, True, If(oDim.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "SRC*IDHPRCSRC(DIM5)[IDH_DIMC=5][IDHCODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_ProfitCenter)
            '        'oDim.DimensionDescription
            '    End If
            'Else
            '    oGridN.doAddListField("U_ProfCn", "Profit Centre", True, -1, "SRC*IDHPRCSRC(PRC)[IDH_CODE][IDHCODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_ProfitCenter)
            'End If
            ''END 

            '201509: WR1 Transaction Codes 
            oGridN.doAddListField("U_WRTranCd", "WR1 Transaction Code", True, -1, "COMBOBOX", Nothing, iFilterBackColor, Nothing)

            oGridN.doAddListField("U_IDHOBLGT", "Obligated", True, -1, "COMBOBOX", Nothing, iFilterBackColor, Nothing)

        End Sub

        Public Sub doGetItemGroups(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_ItmGrp")), SAPbouiCOM.ComboBoxColumn)
            Catch ex As Exception
                Exit Sub
            End Try
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            oCombo.ValidValues.Add("", getTranslatedWord("Any"))
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                '...                oRecordSet = goParent.goDB.doSelectQuery("select DISTINCT ItmsGrpCod, ItmsGrpNam from OITB g, [@IDH_JOBTYPE] jt WHERE g.ItmsGrpNam=jt.U_ItemGrp")
                oRecordSet = goParent.goDB.doSelectQuery("select DISTINCT ItmsGrpCod, ItmsGrpNam from OITB g, [@IDH_JOBTYPE] jt WHERE g.ItmsGrpCod=jt.U_ItemGrp")
                While Not oRecordSet.EoF
                    Try
                        oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(1).Value, String))
                    Catch ex As Exception
                    End Try
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling Item Groups Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Item Groups")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub
        Public Sub doFillWasteItemGroups(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try

                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_WItmGrp")), SAPbouiCOM.ComboBoxColumn)
            Catch ex As Exception
                Exit Sub
            End Try
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_ValueDescription
            com.idh.bridge.utils.Combobox2.doClearCombo(oCombo)
            doFillCombo(oForm, oCombo, "[OITB]", "ItmsGrpCod", "ItmsGrpNam", "", "ItmsGrpNam", "Any", "")

            'doClearValidValues(oCombo.ValidValues)
            'oCombo.ValidValues.Add("", getTranslatedWord("Any"))
            'Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            'Try
            '    '...                oRecordSet = goParent.goDB.doSelectQuery("select DISTINCT ItmsGrpCod, ItmsGrpNam from OITB g, [@IDH_JOBTYPE] jt WHERE g.ItmsGrpNam=jt.U_ItemGrp")
            '    oRecordSet = goParent.goDB.doSelectQuery("select DISTINCT ItmsGrpCod, ItmsGrpNam from OITB g, [@IDH_JOBTYPE] jt WHERE g.ItmsGrpCod=jt.U_ItemGrp")
            '    While Not oRecordSet.EoF
            '        Try
            '            oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(1).Value, String))
            '        Catch ex As Exception
            '        End Try
            '        oRecordSet.MoveNext()
            '    End While
            'Catch ex As Exception
            '    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling Item Groups Combo.")
            '    DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Item Groups")})
            'Finally
            '    DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            'End Try
        End Sub
        Private Sub doOrderCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_JobTp")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            oCombo.ValidValues.Add("", getTranslatedWord("Any"))
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDB.doSelectQuery("select DISTINCT U_JobTp from [@IDH_JOBTYPE]")
                While Not oRecordSet.EoF
                    Try
                        oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(0).Value, String))
                    Catch ex As Exception
                    End Try
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling OrderType Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Order Type")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

        Private Sub doBranchCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_Branch")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            oCombo.ValidValues.Add("", getTranslatedWord("Any"))
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDB.doSelectQuery("select Code, Remarks from OUBR")
                While Not oRecordSet.EoF
                    Try
                        oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(1).Value, String))
                    Catch ex As Exception
                    End Try
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling Branch Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Branch")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub
        Private Sub doFilterJobTypeCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oCmb As SAPbouiCOM.ComboBox
            oCmb = CType(oForm.Items.Item("IDH_JOBTYP").Specific, SAPbouiCOM.ComboBox)
            doClearValidValues(oCmb.ValidValues)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDB.doSelectQuery("select DISTINCT U_JobTp from [@IDH_JOBTYPE]")
                oCmb.ValidValues.Add("", "Any")
                While Not oRecordSet.EoF
                    Try
                        oCmb.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(0).Value, String))
                    Catch ex As Exception
                    End Try
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling OrderType Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Order Type")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try

        End Sub

        Public Sub doFillTransactionTypes(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_TRNType")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            Try
                oCombo.ValidValues.Add("PO", "Purchace Order")
                oCombo.ValidValues.Add("PJ", "PO - Journal Entry")
                oCombo.ValidValues.Add("PG", "PO - GRNI")
                oCombo.ValidValues.Add("PA", "PO - Accruals")

                oCombo.ValidValues.Add("SO", "Sales Order")
                oCombo.ValidValues.Add("SJ", "SO - Journal Entry")
                'oCombo.ValidValues.Add("SG", "SO - GRNI")
                oCombo.ValidValues.Add("SA", "SO - Accruals")

                'USA
                oCombo.ValidValues.Add("JR", "Journal Entry")

                oCombo.ValidValues.Add("TR", "StockTransfer")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling Transaction Type Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Transaction Type")})
            End Try
        End Sub

        Public Sub doFillTipSites(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_TipSite")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            Try
                oCombo.ValidValues.Add("", "I/C & Non I/C")
                oCombo.ValidValues.Add("I/C", "I/C")
                oCombo.ValidValues.Add("Non I/C", "Non I/C")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Tipping Site Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Tipping Site")})
            End Try
        End Sub

        Public Sub doFillPostingValueSourceTypes(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_PVSrc")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            Try
                oCombo.ValidValues.Add("TipTot", "Disposal Cost")
                oCombo.ValidValues.Add("TAddCost", "Additional Cost")
                oCombo.ValidValues.Add("OrdTot", "Haulage Cost")
                oCombo.ValidValues.Add("PCTotal", "Purchase Cost")
                oCombo.ValidValues.Add("JCost", "Total Order Cost")

                oCombo.ValidValues.Add("TCTotal", "Disposal Charge")
                oCombo.ValidValues.Add("TAddChrg", "Additional Charges")
                oCombo.ValidValues.Add("Price", "Haulage Charge")
                oCombo.ValidValues.Add("Total", "Total Order Charge")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling Transaction Type Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Transaction Type")})
            End Try
        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            'Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemChanged AndAlso pVal.ItemUID <> "LINESGRID" Then
                        doCheckForFilter(oForm)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_ORDTYP" OrElse
                    pVal.ItemUID = "IDH_JOBTYP" OrElse
                    pVal.ItemUID = "IDH_BRANCH" OrElse
                    pVal.ItemUID = "IDH_PSTVSR" OrElse
                    pVal.ItemUID = "IDH_ICNIC" OrElse
                    pVal.ItemUID = "IDH_TRNTYP" Then
                        doCheckForFilter(oForm)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_CLICK Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_ONTIP" OrElse pVal.ItemUID = "IDH_ONHAUL" OrElse pVal.ItemUID = "IDH_ADDITM" Then
                        doCheckForFilter(oForm)
                        'ElseIf pVal.ItemUID = "1" Then
                        '    Dim isValid As Boolean = True
                        '    Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                        '    Dim iIndex As Integer = 0
                        '    Dim iRow As Integer

                        '    Dim sOnTip As String
                        '    Dim sOnHaulage As String
                        '    Dim sOnAddItem As String
                        '    Dim sPostingValSrc As String
                        '    Dim sTransType As String

                        '    'Validate updated rows 
                        '    If isValid Then
                        '        Dim aUpdatedRows As ArrayList = oGridN.doGetChangedRows()
                        '        If Not aUpdatedRows Is Nothing Then
                        '            While iIndex < aUpdatedRows.Count
                        '                iRow = aUpdatedRows.Item(iIndex)
                        '                oGridN.setCurrentDataRowIndex(iRow)

                        '                sOnTip = oGridN.doGetFieldValue("U_DoTip")
                        '                sOnHaulage = oGridN.doGetFieldValue("U_DoHaul")
                        '                sOnAddItem = oGridN.doGetFieldValue("U_DoAdItem")
                        '                sPostingValSrc = oGridN.doGetFieldValue("U_PVSrc")
                        '                sTransType = oGridN.doGetFieldValue("U_TRNType")

                        '                If sOnTip = String.Empty OrElse sOnHaulage = String.Empty OrElse sOnAddItem = String.Empty OrElse sPostingValSrc = String.Empty OrElse sTransType = String.Empty Then
                        '                    'doWarnMess("Validation failed at Row# " + iRow.ToString() + ". Make sure the following mandatory fields has got value: OnTipping, OnHaulage, OnAdditionalItems, Posting Value Source and Transaction Type.", SAPbouiCOM.BoMessageTime.bmt_Long)
                        '                    com.idh.bridge.DataHandler.INSTANCE.doError("Validation failed at Row# " + iRow.ToString() + ".", "Make sure the following mandatory fields has got value: OnTipping, OnHaulage, OnAdditionalItems, Posting Value Source and Transaction Type.")
                        '                    isValid = False
                        '                    Exit While
                        '                End If
                        '                iIndex = iIndex + 1
                        '            End While
                        '        End If
                        '    End If

                        '    'Reset iIndex  
                        '    iIndex = 0

                        '    'Validate new rows added 
                        '    If isValid Then
                        '        Dim aNewRows As ArrayList = oGridN.doGetAddedRows()
                        '        If Not aNewRows Is Nothing Then
                        '            While iIndex < aNewRows.Count
                        '                iRow = aNewRows.Item(iIndex)
                        '                oGridN.setCurrentDataRowIndex(iRow)

                        '                sOnTip = oGridN.doGetFieldValue("U_DoTip")
                        '                sOnHaulage = oGridN.doGetFieldValue("U_DoHaul")
                        '                sOnAddItem = oGridN.doGetFieldValue("U_DoAdItem")
                        '                sPostingValSrc = oGridN.doGetFieldValue("U_PVSrc")
                        '                sTransType = oGridN.doGetFieldValue("U_TRNType")

                        '                If sOnTip = String.Empty OrElse sOnHaulage = String.Empty OrElse sOnAddItem = String.Empty OrElse sPostingValSrc = String.Empty OrElse sTransType = String.Empty Then
                        '                    'doWarnMess("Validation failed at Row# " + iRow.ToString() + ". Make sure the following mandatory fields has got value: OnTipping, OnHaulage, OnAdditionalItems, Posting Value Source and Transaction Type.", SAPbouiCOM.BoMessageTime.bmt_Long)
                        '                    com.idh.bridge.DataHandler.INSTANCE.doError("Validation failed at Row# " + iRow.ToString() + ".", "Make sure the following mandatory fields has got value: OnTipping, OnHaulage, OnAdditionalItems, Posting Value Source and Transaction Type.")
                        '                    isValid = False
                        '                    Exit While
                        '                End If
                        '                iIndex = iIndex + 1
                        '            End While
                        '        End If
                        '    End If

                        '    If Not isValid Then
                        '        BubbleEvent = False
                        '    End If

                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If pVal.BeforeAction = False Then
                    If pVal.CharPressed = 9 Then
                        If pVal.ItemUID = "LINESGRID" Then
                            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                            Dim sDebitAcctCd As String = CType(oGridN.doGetFieldValue("U_DebFAcc"), String)
                            Dim sCreditAcctCd As String = CType(oGridN.doGetFieldValue("U_CreFAcc"), String)

                            Dim sBSDebitAcctCd As String = CType(oGridN.doGetFieldValue("U_BSDebFAcc"), String)
                            Dim sBSCreditAcctCd As String = CType(oGridN.doGetFieldValue("U_BSCreFAcc"), String)

                            Dim sWhseCd As String = CType(oGridN.doGetFieldValue("U_WhseCd"), String)

                            If sDebitAcctCd = "*" Then
                                setSharedData(oForm, "TRG", "DEBIT")
                                setSharedData(oForm, "SILENT", "SHOWMULTI")
                                goParent.doOpenModalForm("IDHGLSRC", oForm)
                            ElseIf sCreditAcctCd = "*" Then
                                setSharedData(oForm, "TRG", "CREDIT")
                                setSharedData(oForm, "SILENT", "SHOWMULTI")
                                goParent.doOpenModalForm("IDHGLSRC", oForm)
                            ElseIf sBSDebitAcctCd = "*" Then
                                setSharedData(oForm, "TRG", "BSDEBIT")
                                setSharedData(oForm, "SILENT", "SHOWMULTI")
                                goParent.doOpenModalForm("IDHGLSRC", oForm)
                            ElseIf sBSCreditAcctCd = "*" Then
                                setSharedData(oForm, "TRG", "BSCREDIT")
                                setSharedData(oForm, "SILENT", "SHOWMULTI")
                                goParent.doOpenModalForm("IDHGLSRC", oForm)
                            ElseIf sWhseCd = "*" Then
                                setSharedData(oForm, "SILENT", "SHOWMULTI")
                                goParent.doOpenModalForm("IDHWHSSRC", oForm)
                            End If

                        End If
                    End If
                End If
            End If
            Return True
        End Function

        Public Sub doFillObligated(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oObjColumn As Object = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_IDHOBLGT"))

            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oObjColumn, SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            Try
                Dim sWord As String
                sWord = getTranslatedWord("None")
                oCombo.ValidValues.Add("", sWord)

                sWord = Config.Parameter("OBTRUE")
                If sWord Is Nothing OrElse sWord.Length = 0 Then
                    sWord = getTranslatedWord("Obligated")
                End If
                oCombo.ValidValues.Add(sWord, sWord)

                sWord = Config.Parameter("OBFALSE")
                If sWord Is Nothing OrElse sWord.Length = 0 Then
                    sWord = getTranslatedWord("Non-Obligated")
                End If
                oCombo.ValidValues.Add(sWord, sWord)
            Catch ex As Exception
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Obligated")})
            End Try
        End Sub

        Public Sub doFillWR1OrderTypes(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oObjColumn As Object = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_WR1ORD"))

            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oObjColumn, SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            Try
                oCombo.ValidValues.Add("", getTranslatedWord("Any"))
                oCombo.ValidValues.Add("DO", getTranslatedWord("Disposal Order"))
                oCombo.ValidValues.Add("WO", getTranslatedWord("Waste Order"))
            Catch ex As Exception
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Order Types")})
            End Try
        End Sub

        Private Sub doFillWR1TransactionCode(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oObjColumn As Object = oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_WRTranCd"))

            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oObjColumn, SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            oCombo.ValidValues.Add("", getTranslatedWord("Any"))
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDB.doSelectQuery("select DISTINCT Code, U_Process from [@IDH_TRANCD]")
                While Not oRecordSet.EoF
                    Try
                        oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item("Code").Value, String), CType(oRecordSet.Fields.Item("U_Process").Value, String))
                    Catch ex As Exception
                    End Try
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling OrderType Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Order Type")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

        Public Overrides Sub doButtonID1(oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True AndAlso _
                (oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE) Then

                Dim isValid As Boolean = True
                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                Dim iIndex As Integer = 0
                Dim iRow As Integer

                Dim sOnTip As String
                Dim sOnHaulage As String
                Dim sOnAddItem As String
                Dim sPostingValSrc As String
                Dim sTransType As String

                'Validate updated rows 
                If isValid Then
                    Dim aUpdatedRows As ArrayList = oGridN.doGetChangedRows()
                    If Not aUpdatedRows Is Nothing Then
                        While iIndex < aUpdatedRows.Count
                            iRow = CType(aUpdatedRows.Item(iIndex), Integer)
                            oGridN.setCurrentDataRowIndex(iRow)

                            sOnTip = CType(oGridN.doGetFieldValue("U_DoTip"), String)
                            sOnHaulage = CType(oGridN.doGetFieldValue("U_DoHaul"), String)
                            sOnAddItem = CType(oGridN.doGetFieldValue("U_DoAdItem"), String)
                            sPostingValSrc = CType(oGridN.doGetFieldValue("U_PVSrc"), String)
                            sTransType = CType(oGridN.doGetFieldValue("U_TRNType"), String)

                            If sOnTip = String.Empty OrElse sOnHaulage = String.Empty OrElse sOnAddItem = String.Empty OrElse sPostingValSrc = String.Empty OrElse sTransType = String.Empty Then
                                DataHandler.INSTANCE.doResExceptionError("Validation failed at Row# " + iRow.ToString() + ".", "ERGLVLD", {iRow.ToString()})
                                isValid = False
                                Exit While
                            End If
                            iIndex = iIndex + 1
                        End While
                    End If
                End If

                'Reset iIndex  
                iIndex = 0

                'Validate new rows added 
                If isValid Then
                    Dim aNewRows As ArrayList = oGridN.doGetAddedRows()
                    If Not aNewRows Is Nothing Then
                        While iIndex < aNewRows.Count
                            iRow = CType(aNewRows.Item(iIndex), Integer)
                            oGridN.setCurrentDataRowIndex(iRow)

                            sOnTip = CType(oGridN.doGetFieldValue("U_DoTip"), String)
                            sOnHaulage = CType(oGridN.doGetFieldValue("U_DoHaul"), String)
                            sOnAddItem = CType(oGridN.doGetFieldValue("U_DoAdItem"), String)
                            sPostingValSrc = CType(oGridN.doGetFieldValue("U_PVSrc"), String)
                            sTransType = CType(oGridN.doGetFieldValue("U_TRNType"), String)

                            If sOnTip = String.Empty OrElse sOnHaulage = String.Empty OrElse sOnAddItem = String.Empty OrElse sPostingValSrc = String.Empty OrElse sTransType = String.Empty Then
                                DataHandler.INSTANCE.doResExceptionError("Validation failed at Row# " + iRow.ToString() + ".", "ERGLVLD", {iRow.ToString()})
                                isValid = False
                                Exit While
                            End If
                            iIndex = iIndex + 1
                        End While
                    End If
                End If

                If isValid Then
                    If oGridN.doProcessData() = False Then
                        BubbleEvent = False
                    End If
                Else
                    BubbleEvent = False
                End If
            End If

        End Sub

        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.ItemUID = "LINESGRID" Then
                If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED Then
                    If pVal.BeforeAction = False Then
                        If pVal.ColUID = "U_DebFAcc" OrElse pVal.ColUID = "U_CreFAcc" Then
                            Dim oGrid As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                            Dim sAcCdVal As String = ""
                            Dim sVal As String = CType(oGrid.doGetFieldValue(pVal.ColUID), String)
                            If sVal.Length = 0 Then
                                If pVal.ColUID = "U_DebFAcc" Then
                                    oGrid.doSetFieldValue("U_DebAcc", "")
                                ElseIf pVal.ColUID = "U_CreFAcc" Then
                                    oGrid.doSetFieldValue("U_CreAcc", "")
                                End If
                            ElseIf sVal.Length > 0 AndAlso sVal <> "*" Then
                                If pVal.ColUID = "U_DebFAcc" Then
                                    If doGetAccountCode("U_DebAcc", sAcCdVal, "U_DebFAcc", oGrid.doGetFieldValue("U_DebFAcc").ToString()) Then
                                        oGrid.doSetFieldValue("U_DebAcc", sAcCdVal)
                                    Else
                                        doWarnMess("Invalid Debit Account Code!", SAPbouiCOM.BoMessageTime.bmt_Short)
                                        Return False
                                    End If
                                ElseIf pVal.ColUID = "U_CreFAcc" Then
                                    If doGetAccountCode("U_CreAcc", sAcCdVal, "U_CreFAcc", CType(oGrid.doGetFieldValue("U_CreFAcc"), String)) Then
                                        oGrid.doSetFieldValue("U_CreAcc", sAcCdVal)
                                    Else
                                        doWarnMess("Invalid Credit Account Code!", SAPbouiCOM.BoMessageTime.bmt_Short)
                                        Return False
                                    End If
                                End If
                            End If
                            oGrid = Nothing
                        ElseIf pVal.ColUID = "U_BSDebFAcc" OrElse pVal.ColUID = "U_BSCreFAcc" Then
                            Dim oGrid As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                            Dim sAcCdVal As String = ""
                            Dim sVal As String = CType(oGrid.doGetFieldValue(pVal.ColUID), String)
                            If sVal.Length = 0 Then
                                If pVal.ColUID = "U_BSDebFAcc" Then
                                    oGrid.doSetFieldValue("U_BSDebAcc", "")
                                ElseIf pVal.ColUID = "U_BSCreFAcc" Then
                                    oGrid.doSetFieldValue("U_BSCreAcc", "")
                                End If
                            ElseIf sVal.Length > 0 AndAlso sVal <> "*" Then
                                If pVal.ColUID = "U_BSDebFAcc" Then
                                    If doGetAccountCode("U_BSDebAcc", sAcCdVal, "U_BSDebFAcc", oGrid.doGetFieldValue("U_BSDebFAcc").ToString()) Then
                                        oGrid.doSetFieldValue("U_BSDebAcc", sAcCdVal)
                                    Else
                                        doWarnMess("Invalid Balance Sheet Debit Account Code!", SAPbouiCOM.BoMessageTime.bmt_Short)
                                        Return False
                                    End If
                                ElseIf pVal.ColUID = "U_BSCreFAcc" Then
                                    If doGetAccountCode("U_BSCreAcc", sAcCdVal, "U_BSCreFAcc", CType(oGrid.doGetFieldValue("U_BSCreFAcc"), String)) Then
                                        oGrid.doSetFieldValue("U_BSCreAcc", sAcCdVal)
                                    Else
                                        doWarnMess("Invalid Balance Sheet Credit Account Code!", SAPbouiCOM.BoMessageTime.bmt_Short)
                                        Return False
                                    End If
                                End If
                            End If
                            oGrid = Nothing
                        End If

                    End If
                ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SEARCH AndAlso pVal.oData.GetType.Name.ToString = "en_DialogResults" Then
                    If (DirectCast(pVal.oData, com.idh.bridge.lookups.Base.en_DialogResults) = com.idh.bridge.lookups.Base.en_DialogResults.CANCELED) Then
                        pVal.oGrid.doSetFieldValue(pVal.ColUID, pVal.Row, "")
                        Return False
                    End If
                End If
            End If
            Return MyBase.doCustomItemEvent(oForm, pVal)
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            'MyBase.doHandleModalResultShared(oParentForm, sModalFormType, sLastButton)
            If sModalFormType = "IDHGLSRC" Then
                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                Dim sTarget As String = getSharedDataAsString(oForm, "TRG")
                Dim sAcctCode As String = getSharedDataAsString(oForm, "IDH_CODE")
                Dim sFormatCode As String = getSharedDataAsString(oForm, "IDHFCODE")

                If sTarget = "DEBIT" Then
                    oGridN.doSetFieldValue("U_DebAcc", sAcctCode)
                    oGridN.doSetFieldValue("U_DebFAcc", sFormatCode)
                ElseIf sTarget = "CREDIT" Then
                    oGridN.doSetFieldValue("U_CreAcc", sAcctCode)
                    oGridN.doSetFieldValue("U_CreFAcc", sFormatCode)
                ElseIf sTarget = "BSDEBIT" Then
                    oGridN.doSetFieldValue("U_BSDebAcc", sAcctCode)
                    oGridN.doSetFieldValue("U_BSDebFAcc", sFormatCode)
                ElseIf sTarget = "BSCREDIT" Then
                    oGridN.doSetFieldValue("U_BSCreAcc", sAcctCode)
                    oGridN.doSetFieldValue("U_BSCreFAcc", sFormatCode)
                End If
            ElseIf sModalFormType = "IDHWHSSRC" Then
                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                Dim sWhsCd As String = getSharedDataAsString(oForm, "WHSCODE")
                Dim sWhsNm As String = getSharedDataAsString(oForm, "WHSNAME")

                oGridN.doSetFieldValue("U_WhseCd", sWhsCd)
                oGridN.doSetFieldValue("U_WhseNm", sWhsNm)
            End If
        End Sub

        Protected Function doGetAccountCode(ByRef sAcCd As String, ByRef sAcCdVal As String, ByRef sFAcCd As String, ByRef sFAcCdVal As String) As Boolean
            Dim sQry As String
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                sQry = "select AcctCode, FormatCode From OACT Where FormatCode = '" & sFAcCdVal & "'"
                oRecordSet = goParent.goDB.doSelectQuery(sQry)
                If Not oRecordSet Is Nothing Then
                    If oRecordSet.RecordCount > 0 Then
                        sAcCdVal = CType(oRecordSet.Fields.Item("AcctCode").Value, String)
                        Return True
                    Else
                        Return False
                    End If
                End If
            Catch ex As Exception
                Throw ex
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
            Return False
        End Function

        Private Sub doCheckForFilter(ByVal oForm As SAPbouiCOM.Form)
            If (oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE) Then
                If Messages.INSTANCE.doResourceMessageYN("GEDCOMT", Nothing, 1) = 1 Then
                    Dim oUpdateGrid As UpdateGrid
                    oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    If oUpdateGrid.doProcessData() = True Then
                        doLoadData(oForm)
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                    End If
                End If
            Else
                doLoadData(oForm)
            End If
        End Sub

        Private Sub doSetDimensionsListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            Dim sUseMltDims As String = CType(com.idh.bridge.lookups.Config.INSTANCE.doLookupTableField("OADM", "UseMltDims", Nothing), String)
            If sUseMltDims.Equals("Y") Then
                Dim oCompServices As SAPbobsCOM.CompanyService = goParent.goDICompany.GetCompanyService()
                Dim oDimService As SAPbobsCOM.DimensionsService = CType(oCompServices.GetBusinessService(SAPbobsCOM.ServiceTypes.DimensionsService), SAPbobsCOM.DimensionsService)
                Dim oDimParams As SAPbobsCOM.DimensionsParams = oDimService.GetDimensionList()
                If oDimParams.Count > 0 Then
                    Dim oDim1 As SAPbobsCOM.Dimension = oDimService.GetDimension(oDimParams.Item(0))
                    Dim oDim2 As SAPbobsCOM.Dimension = oDimService.GetDimension(oDimParams.Item(1))
                    Dim oDim3 As SAPbobsCOM.Dimension = oDimService.GetDimension(oDimParams.Item(2))
                    Dim oDim4 As SAPbobsCOM.Dimension = oDimService.GetDimension(oDimParams.Item(3))
                    Dim oDim5 As SAPbobsCOM.Dimension = oDimService.GetDimension(oDimParams.Item(4))

                    'PL DEBIT ACC 
                    oGridN.doAddListField("U_DebAcc", "DebitAccount", False, -1, Nothing, Nothing, iFilterBackColor) 'This column is used in Marketing Docs
                    oGridN.doAddListField("U_DebFAcc", "Debit FormatCd", True, -1, Nothing, Nothing, iFilterBackColor)
                    oGridN.doAddListField("U_ProfCn", "PL Db - " + oDim1.DimensionDescription, True, If(oDim1.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)
                    oGridN.doAddListField("U_Dimension2", "PL Db - " + oDim2.DimensionDescription, True, If(oDim2.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)
                    oGridN.doAddListField("U_Dimension3", "PL Db - " + oDim3.DimensionDescription, True, If(oDim3.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)
                    oGridN.doAddListField("U_Dimension4", "PL Db - " + oDim4.DimensionDescription, True, If(oDim4.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)
                    oGridN.doAddListField("U_Dimension5", "PL Db - " + oDim5.DimensionDescription, True, If(oDim5.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)

                    'PL CREDIT ACC 
                    oGridN.doAddListField("U_CreAcc", "CreditAccount", False, -1, Nothing, Nothing, iFilterBackColor) 'This column is used in Marketing Docs
                    oGridN.doAddListField("U_CreFAcc", "Credit FormatCd", True, -1, Nothing, Nothing, iFilterBackColor)
                    oGridN.doAddListField("U_PLCRDim1", "PL Cr - " + oDim1.DimensionDescription, True, If(oDim1.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)
                    oGridN.doAddListField("U_PLCRDim2", "PL Cr - " + oDim2.DimensionDescription, True, If(oDim2.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)
                    oGridN.doAddListField("U_PLCRDim3", "PL Cr - " + oDim3.DimensionDescription, True, If(oDim3.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)
                    oGridN.doAddListField("U_PLCRDim4", "PL Cr - " + oDim4.DimensionDescription, True, If(oDim4.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)
                    oGridN.doAddListField("U_PLCRDim5", "PL Cr - " + oDim5.DimensionDescription, True, If(oDim5.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)

                    'BS DEBIT ACC 
                    oGridN.doAddListField("U_BSDebAcc", "Balance Sheet Db Acc", False, -1, Nothing, Nothing, iFilterBackColor)
                    oGridN.doAddListField("U_BSDebFAcc", "BS Deb Format Cd", True, -1, Nothing, Nothing, iFilterBackColor)
                    oGridN.doAddListField("U_BSDBDim1", "BS Db - " + oDim1.DimensionDescription, True, If(oDim1.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)
                    oGridN.doAddListField("U_BSDBDim2", "BS Db - " + oDim2.DimensionDescription, True, If(oDim2.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)
                    oGridN.doAddListField("U_BSDBDim3", "BS Db - " + oDim3.DimensionDescription, True, If(oDim3.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)
                    oGridN.doAddListField("U_BSDBDim4", "BS Db - " + oDim4.DimensionDescription, True, If(oDim4.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)
                    oGridN.doAddListField("U_BSDBDim5", "BS Db - " + oDim5.DimensionDescription, True, If(oDim5.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)

                    'BS DEBIT ACC 
                    oGridN.doAddListField("U_BSCreAcc", "Balance Sheet Cr Acc", False, -1, Nothing, Nothing, iFilterBackColor)
                    oGridN.doAddListField("U_BSCreFAcc", "BS Cre FormatCd", True, -1, Nothing, Nothing, iFilterBackColor)
                    oGridN.doAddListField("U_BSCRDim1", "BS Cr - " + oDim1.DimensionDescription, True, If(oDim1.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)
                    oGridN.doAddListField("U_BSCRDim2", "BS Cr - " + oDim2.DimensionDescription, True, If(oDim2.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)
                    oGridN.doAddListField("U_BSCRDim3", "BS Cr - " + oDim3.DimensionDescription, True, If(oDim3.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)
                    oGridN.doAddListField("U_BSCRDim4", "BS Cr - " + oDim4.DimensionDescription, True, If(oDim4.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)
                    oGridN.doAddListField("U_BSCRDim5", "BS Cr - " + oDim5.DimensionDescription, True, If(oDim5.IsActive.Equals(SAPbobsCOM.BoYesNoEnum.tYES), -1, 0), "COMBOBOX", Nothing, iFilterBackColor, Nothing)

                End If
            Else
                'Add Profit Centre 
                oGridN.doAddListField("U_ProfCn", "Profit Centre", True, -1, "SRC*IDHPRCSRC(PRC)[IDH_CODE][IDHCODE]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_ProfitCenter)
            End If

        End Sub

        Private Sub doFillDimensions(ByVal oForm As SAPbouiCOM.Form)
            Dim sColName As String = "U_ProfCn"
            Dim iDimCode As Integer = 1

            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")

            'PL DEBIT ACC
            Dim oCmbPLDbD1 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_ProfCn"))).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox Then
            oCmbPLDbD1 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_ProfCn")), SAPbouiCOM.ComboBoxColumn)
            oCmbPLDbD1.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbPLDbD1.ValidValues)
            oCmbPLDbD1.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbPLDbD2 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_Dimension2")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            oCmbPLDbD2 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_Dimension2")), SAPbouiCOM.ComboBoxColumn)
            oCmbPLDbD2.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbPLDbD2.ValidValues)
            oCmbPLDbD2.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbPLDbD3 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_Dimension3")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            oCmbPLDbD3 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_Dimension3")), SAPbouiCOM.ComboBoxColumn)
            oCmbPLDbD3.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbPLDbD3.ValidValues)
            oCmbPLDbD3.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbPLDbD4 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_Dimension4")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            oCmbPLDbD4 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_Dimension4")), SAPbouiCOM.ComboBoxColumn)
            oCmbPLDbD4.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbPLDbD4.ValidValues)
            oCmbPLDbD4.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbPLDbD5 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_Dimension5")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            oCmbPLDbD5 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_Dimension5")), SAPbouiCOM.ComboBoxColumn)
            oCmbPLDbD5.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbPLDbD5.ValidValues)
            oCmbPLDbD5.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbPLCrD1 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_PLCRDim1")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            'PL CREDIT ACC 
            oCmbPLCrD1 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_PLCRDim1")), SAPbouiCOM.ComboBoxColumn)
            oCmbPLCrD1.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbPLCrD1.ValidValues)
            oCmbPLCrD1.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbPLCrD2 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_PLCRDim2")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            oCmbPLCrD2 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_PLCRDim2")), SAPbouiCOM.ComboBoxColumn)
            oCmbPLCrD2.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbPLCrD2.ValidValues)
            oCmbPLCrD2.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbPLCrD3 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_PLCRDim3")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            oCmbPLCrD3 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_PLCRDim3")), SAPbouiCOM.ComboBoxColumn)
            oCmbPLCrD3.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbPLCrD3.ValidValues)
            oCmbPLCrD3.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbPLCrD4 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_PLCRDim4")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            oCmbPLCrD4 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_PLCRDim4")), SAPbouiCOM.ComboBoxColumn)
            oCmbPLCrD4.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbPLCrD4.ValidValues)
            oCmbPLCrD4.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbPLCrD5 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_PLCRDim5")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            oCmbPLCrD5 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_PLCRDim5")), SAPbouiCOM.ComboBoxColumn)
            oCmbPLCrD5.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbPLCrD5.ValidValues)
            oCmbPLCrD5.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbBSDbD1 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSDBDim1")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            'BS DEBIT ACC 
            oCmbBSDbD1 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSDBDim1")), SAPbouiCOM.ComboBoxColumn)
            oCmbBSDbD1.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbBSDbD1.ValidValues)
            oCmbBSDbD1.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbBSDbD2 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSDBDim2")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            oCmbBSDbD2 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSDBDim2")), SAPbouiCOM.ComboBoxColumn)
            oCmbBSDbD2.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbBSDbD2.ValidValues)
            oCmbBSDbD2.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbBSDbD3 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSDBDim3")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            oCmbBSDbD3 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSDBDim3")), SAPbouiCOM.ComboBoxColumn)
            oCmbBSDbD3.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbBSDbD3.ValidValues)
            oCmbBSDbD3.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbBSDbD4 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSDBDim4")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            oCmbBSDbD4 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSDBDim4")), SAPbouiCOM.ComboBoxColumn)
            oCmbBSDbD4.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbBSDbD4.ValidValues)
            oCmbBSDbD4.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbBSDbD5 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSDBDim5")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            oCmbBSDbD5 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSDBDim5")), SAPbouiCOM.ComboBoxColumn)
            oCmbBSDbD5.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbBSDbD5.ValidValues)
            oCmbBSDbD5.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbBSCrD1 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSDBDim5")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            'BS CREDIT ACC 
            oCmbBSCrD1 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSCRDim1")), SAPbouiCOM.ComboBoxColumn)
            oCmbBSCrD1.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbBSCrD1.ValidValues)
            oCmbBSCrD1.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbBSCrD2 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSCRDim2")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            oCmbBSCrD2 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSCRDim2")), SAPbouiCOM.ComboBoxColumn)
            oCmbBSCrD2.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbBSCrD2.ValidValues)
            oCmbBSCrD2.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbBSCrD3 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSCRDim3")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            oCmbBSCrD3 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSCRDim3")), SAPbouiCOM.ComboBoxColumn)
            oCmbBSCrD3.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbBSCrD3.ValidValues)
            oCmbBSCrD3.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbBSCrD4 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSCRDim4")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            oCmbBSCrD4 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSCRDim4")), SAPbouiCOM.ComboBoxColumn)
            oCmbBSCrD4.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbBSCrD4.ValidValues)
            oCmbBSCrD4.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oCmbBSCrD5 As SAPbouiCOM.ComboBoxColumn
            If (oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSCRDim5")).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox) Then
            oCmbBSCrD5 = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BSCRDim5")), SAPbouiCOM.ComboBoxColumn)
            oCmbBSCrD5.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCmbBSCrD5.ValidValues)
            oCmbBSCrD5.ValidValues.Add("", getTranslatedWord("Any"))
            End If

            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDB.doSelectQuery("select OcrCode, OcrName, DimCode from OOCR ") 'where DimCode = " + iDimCode.ToString() + "")
                While Not oRecordSet.EoF
                    Try


                        Select Case oRecordSet.Fields.Item("DimCode").Value
                            Case 1
                                If oCmbPLDbD1 IsNot Nothing Then
                                oCmbPLDbD1.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                oCmbPLCrD1.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                oCmbBSDbD1.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                oCmbBSCrD1.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                End If

                            Case 2
                                If oCmbPLDbD2 IsNot Nothing Then
                                oCmbPLDbD2.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                oCmbPLCrD2.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                oCmbBSDbD2.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                oCmbBSCrD2.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                End If
                            Case 3
                                If oCmbPLDbD3 IsNot Nothing Then
                                oCmbPLDbD3.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                oCmbPLCrD3.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                oCmbBSDbD3.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                oCmbBSCrD3.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                End If
                            Case 4
                                If oCmbPLDbD4 IsNot Nothing Then
                                oCmbPLDbD4.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                oCmbPLCrD4.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                oCmbBSDbD4.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                oCmbBSCrD4.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                End If
                            Case 5
                                If oCmbPLDbD5 IsNot Nothing Then
                                oCmbPLDbD5.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                oCmbPLCrD5.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                oCmbBSDbD5.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                oCmbBSCrD5.ValidValues.Add(CType(oRecordSet.Fields.Item("OcrCode").Value, String), CType(oRecordSet.Fields.Item("OcrName").Value, String))
                                End If
                            Case Else
                                Exit Select
                        End Select
                    Catch ex As Exception
                    End Try
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling OrderType Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("doFillDimensions")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

    End Class
End Namespace
