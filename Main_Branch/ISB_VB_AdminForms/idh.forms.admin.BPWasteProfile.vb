Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.bridge

Imports com.idh.dbObjects.User

Namespace idh.forms.admin
    Public Class BPWasteProfile
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "ISBBPWPRO", sParMenu, iMenuPosition, Nothing, "BP Waste Profiles")
            '**********************************************************
            '******OnTime [#Ico000????] USA  **************************
            '**********************************************************
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "ISB_WSTPROFILE"
        End Function

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", True, 0, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", True, 0, Nothing, Nothing)

            oGridN.doAddListField("U_ProfileNo", "Profile No.", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Descp", "Description", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ValidFm", "Valid From", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ValidTo", "Valid To", True, -1, Nothing, Nothing)

            oGridN.doAddListField("U_CardCd", "BP Code", True, -1, "COMBOBOX", Nothing)
            'oGridN.doAddListField("U_CardNm", "BP Name", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_AddCd", "Address", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_ItemCd", "Item Code", True, -1, "COMBOBOX", Nothing)
            'oGridN.doAddListField("U_ItemNm", "Item Name", True, -1, Nothing, Nothing)

            'oGridN.doAddListField("U_ItemCd", "Container Code", True, -1, "SRC*IDHISRC(PROD)[IDH_ITMCOD=#U_ItemCd;IDH_GRPCOD=#U_ItmGrp][U_ItemCd=ITEMCODE;U_ItemDs=ITEMNAME]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_Items)
            'oGridN.doAddListField("U_ItemDs", "Container Description", True, -1, Nothing, Nothing, iFilterBackColor)

        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)

            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
            End If

            oGridN.setOrderValue("Code")
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            MyBase.doButtonID1(oForm, pVal, BubbleEvent)

            If pVal.BeforeAction = False Then
                'IDH_JOBTYPE_SEQ.getInstance(true)
            End If
        End Sub


        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
            doCountyCombo(oForm)
            doWasteGroupCombo(oForm)
        End Sub

        Private Sub doCountyCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_CntyCd")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            'oCombo.ValidValues.Add("", getTranslatedWord("Any"))
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDB.doSelectQuery("select U_CountyCd, U_CountyNm from [@ISB_COUNTY]")
                'oRecordSet = goParent.goDB.doSelectQuery( _
                '                "select FldValue, Descr from UFD1 where TableID = 'CRD1' " + _
                '                " AND FieldID = (select FieldID from CUFD where tableID = 'CRD1' AND AliasID = 'USACOUNTY')")
                While Not oRecordSet.EoF
                    Try
                        oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(1).Value, String))
                    Catch ex As Exception
                    End Try
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the County combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("County")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

        Private Sub doWasteGroupCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_WstGpCd")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            'oCombo.ValidValues.Add("", getTranslatedWord("Any"))
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDB.doSelectQuery("select U_WstGpCd, U_WstGpNm from [@ISB_WASTEGRP]")
                While Not oRecordSet.EoF
                    Try
                        oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(1).Value, String))
                    Catch ex As Exception
                    End Try
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the UOM combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("UOM")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub
    End Class
End Namespace
