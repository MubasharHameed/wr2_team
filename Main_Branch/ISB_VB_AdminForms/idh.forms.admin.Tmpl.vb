Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System
Imports com.idh.bridge.data
Imports com.idh.bridge

Imports com.idh.dbObjects.numbers

Namespace idh.forms.admin
    Public MustInherit Class Tmpl
        Inherits IDHAddOns.idh.forms.Base
        Protected msKeyGen As String = "GENSEQ"

        Private ghStatusChanged As New ArrayList

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sID As String, ByVal sParMenu As String, ByVal iMenuPosition As Integer, ByVal sSRF As String, ByVal sTitle As String)
            MyBase.New(oParent, sID, sParMenu, iMenuPosition, (If(sSRF Is Nothing, "Admin.srf", sSRF)), True, True, False, sTitle, load_Types.idh_LOAD_NORMAL)
            doEnableHistory(getUserTable())
        End Sub
        
        Protected MustOverride Function getUserTable() As String

        Protected Overridable Function getTable() As String
            Return "@" & getUserTable()
        End Function

        ''** The History Menu Event handler
        'Public Overrides Sub doHistoryEvent(ByVal oForm As SAPbouiCOM.Form)
        '    setSharedData(oForm, "TABLE", getUserTable())
        '    setSharedData(oForm, "CODE", "")
        '    goParent.doOpenModalForm("IDH_HIST", oForm)
        'End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                oForm.Title = gsTitle
                
                Dim oItem As SAPbouiCOM.Item
                oItem = oForm.Items.Item("IDH_CONTA")
                Dim oGridN As UpdateGrid = New UpdateGrid(Me, oForm, "LINESGRID", _
                        oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane)

                'doSetFilterLayout(oForm)

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
                BubbleEvent = False
            End Try
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
            End If
            doSetFilterFields(oGridN)
            doSetListFields(oGridN)
            oGridN.doAddGridTable(New GridTable(getTable(), Nothing, "Code", True, True), True)
            oGridN.doSetDoCount(True)
            'oGridN.doSetHistory(getUserTable(), "Code")
            oGridN.doSetHistory(getTable(), "Code")

            If getHasSharedData(oForm) Then
                doReadInputParams(oForm)
            End If
        End Sub

        '*** Read input params
        Protected Overrides Sub doReadInputParams(ByVal oForm As SAPbouiCOM.Form)
            If getHasSharedData(oForm) Then
                Dim oGridN As FilterGrid
                oGridN = FilterGrid.getInstance(oForm, "LINESGRID")

                Dim iIndex As Integer
                For iIndex = 0 To oGridN.getGridControl().getFilterFields().Count - 1
                    Dim oField As com.idh.controls.strct.FilterField = CType(oGridN.getGridControl().getFilterFields().Item(iIndex), com.idh.controls.strct.FilterField)
                    Dim sFieldName As String = oField.msFieldName
                    Dim sFieldValue As String = getParentSharedDataAsString(oForm, sFieldName)
                    Try
                        setUFValue(oForm, sFieldName, sFieldValue)
                    Catch ex As Exception
                    End Try
                Next
            End If
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            oGridN.doReloadData()
        End Sub

        Protected Overridable Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
        End Sub

        Protected Overridable Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            Dim utTable As SAPbobsCOM.UserTable = Nothing
            Try
                utTable = goParent.goDICompany.UserTables.Item(getUserTable())
                Dim iSize As Integer = utTable.UserFields.Fields.Count
                Dim iIndex As Integer
                Dim sFieldName As String
                Dim sFieldDesc As String
                oGridN.doAddListField("Code", "Code", True, -1, Nothing, Nothing)
                oGridN.doAddListField("Name", "Name", True, -1, Nothing, Nothing)
                For iIndex = 0 To iSize - 1
                    sFieldName = utTable.UserFields.Fields.Item(iIndex).Name
                    sFieldDesc = utTable.UserFields.Fields.Item(iIndex).Description
                    oGridN.doAddListField(sFieldName, sFieldDesc, True, -1, Nothing, Nothing)
                Next
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the list fields.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSLF", {Nothing})
            Finally
                DataHandler.INSTANCE.doReleaseObject(CType(utTable, Object))
            End Try
        End Sub

        ''*** This is where the search fields are set
        'Protected Overridable Sub doSetFilterLayout(ByVal oForm As SAPbouiCOM.Form)
        'End Sub

        Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True AndAlso _
                (oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE) Then
                Dim oUpdateGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                If oUpdateGrid IsNot Nothing Then
                If oUpdateGrid.doProcessData() = False Then
                    BubbleEvent = False
                    End If
                End If
            End If
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Return True
        End Function

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCloseForm(oForm, BubbleEvent)
            FilterGrid.doRemoveGrid(oForm, "LINESGRID")
        End Sub

        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY Then
                Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                Dim oNumbers As NumbersPair = doSetKeyValue(oForm)
                oGridN.doSetFieldValue("Code", pVal.Row, oNumbers.CodeCode, True)
                    oGridN.doSetFieldValue("Name", pVal.Row, oNumbers.NameCode, True)
                   
                    'Set default values of checkboxes to 'N' to avoid empty.string 
                    oGridN.doSetFieldValue("U_DoTip", pVal.Row, "N", True)
                    oGridN.doSetFieldValue("U_DoHaul", pVal.Row, "N", True)
                    oGridN.doSetFieldValue("U_DoAdItem", pVal.Row, "N", True)

                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SORT Then
                If pVal.BeforeAction = False Then
                    oForm.Freeze(True)
                    doLoadData(oForm)
                    oForm.Resize(oForm.Width, oForm.Height)
                    oForm.Freeze(False)
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
                If pVal.BeforeAction Then
                    'show popup menu 
                    Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                    If oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) Then
                        Dim oMenuItem As SAPbouiCOM.MenuItem
                        Dim sRMenuId As String = IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU
                        oMenuItem = goParent.goApplication.Menus.Item(sRMenuId)

                        Dim oMenus As SAPbouiCOM.Menus
                        Dim iMenuPos As Integer = 1
                        Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
                        oCreationPackage = CType(goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams), SAPbouiCOM.MenuCreationParams)
                        oCreationPackage.Enabled = True
                        oMenus = oMenuItem.SubMenus
                        oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING

                        Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows
                        Dim iSelectionCount As Integer = oSelected.Count

                        'Display Delete option for 1-row selection only 
                        If iSelectionCount = 1 Then
                            Dim iRow As Integer

                            'Fix for Right Click Menu when Grid is in Group Mode 
                            If oGridN.getSBOGrid().CollapseLevel > 0 Then
                                iRow = oGridN.GetDataTableRowIndex(pVal.Row)
                            Else
                                iRow = oSelected.Item(0, SAPbouiCOM.BoOrderType.ot_RowOrder)
                            End If

                            If oGridN.doGetDeleteActive() Then
                                If Not (pVal.Row = iRow) Then
                                    oCreationPackage.UniqueID = IDHAddOns.idh.controls.IDHGrid.GRIDMENUREMM
                                    oCreationPackage.String = getTranslatedWord("Delete &Selection")
                                    oCreationPackage.Enabled = True
                                    oMenus.AddEx(oCreationPackage)
                                End If
                            End If
                        End If
                    End If
                Else
                    'hide menu 
                    If goParent.goApplication.Menus.Exists(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREMM) Then
                        goParent.goApplication.Menus.RemoveEx(IDHAddOns.idh.controls.IDHGrid.GRIDMENUREMM)
                    End If
                End If
            End If
            Return MyBase.doCustomItemEvent(oForm, pVal)
        End Function

        Public Overridable Function doSetKeyValue(ByVal oForm As SAPbouiCOM.Form) As NumbersPair
            If msKeyGen Is Nothing Then
                Return DataHandler.INSTANCE.doGenerateCode("")
            Else
                Return DataHandler.INSTANCE.doGenerateCode(msKeyGen)
            End If
        End Function

        Public Overrides Sub doClose()
        End Sub

    End Class
End Namespace
