Imports System.IO
Imports System.Collections
Imports System
Imports IDHAddOns.idh.controls

Imports com.idh.bridge

Namespace idh.forms.admin
    Public Class Printers
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_PRNTRS", sParMenu, iMenuPosition, Nothing, "Report Printers" )
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Report Printers"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_PRINTERS"
        End Function


        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            MyBase.doButtonID1(oForm, pVal, BubbleEvent)
            If pVal.BeforeAction = True Then
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    'com.idh.bridge.lookups.Config.INSTANCE.doResetPrinters(goParent)

                    com.idh.bridge.PrintSettings.doResetPrinters()
                End If
            End If
        End Sub
        
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
			doBranchCombo(oForm)
			doPrinterCombo(oForm)
		End Sub
        
        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_PrntName", "Printer Name", True, -1, "COMBOBOX", Nothing, -1)
            
            oGridN.doAddListField("U_Report", "Report", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_FromFrm", "Calling Form", True, -1, Nothing, Nothing)
            
            oGridN.doAddListField("U_Branch", "Branch", True, -1, "COMBOBOX", Nothing, -1)
            oGridN.doAddListField("U_User", "User", True, -1, Nothing, Nothing, -1)
            oGridN.doAddListField("U_Desc", "Description", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Width", "Width", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Height", "Height", True, -1, Nothing, Nothing)
            
            oGridN.doAddListField("U_Copies", "Copies", True, -1, Nothing, Nothing)
        End Sub
        
        Private Sub doPrinterCombo(ByVal oForm As SAPbouiCOM.Form)
        	Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_PrntName")), SAPbouiCOM.ComboBoxColumn)
            'oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
		    	Dim prt() As com.idh.bridge.sysdata.Printer 
		       	prt = com.idh.bridge.sysdata.Printer.List()
		       	If Not prt Is Nothing Then
		       		Dim sKey As String
		       		Dim sVal As String
		       		Dim iIndex As Integer
		       		For iIndex = 0 To prt.Length - 1 
		       			sKey = prt(iIndex).PrinterName
		       			sVal = prt(iIndex).DriverName ' & " - " & prt(iIndex).PortName
		       			
		       			oCombo.ValidValues.Add( sKey, sVal )
		       		Next
		       	End If            	
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Printer Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Printer")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

        Private Sub doBranchCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_Branch")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            oCombo.ValidValues.Add("", getTranslatedWord("Any"))
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDB.doSelectQuery("select Code, Remarks from OUBR")
                While Not oRecordSet.EoF
                	Try
                        oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(1).Value, String))
                    Catch ex As Exception
                    End Try
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Branch Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Branch")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
       End Sub

    End Class
End Namespace
