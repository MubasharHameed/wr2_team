Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class TFS
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_WTTFS", sParMenu, iMenuPosition, Nothing, "TFS Data" )
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "TFS Data"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_WTTFS"
        End Function

    End Class
End Namespace
