Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class UNData
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_WTUN", sParMenu, iMenuPosition, Nothing, "UN Data" )
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "UN Data"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_WTUN"
        End Function

    End Class
End Namespace
