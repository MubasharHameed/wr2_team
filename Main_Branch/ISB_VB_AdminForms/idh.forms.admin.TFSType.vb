Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Imports com.idh.dbObjects.User

Namespace idh.forms.admin
    Public Class TFSTyp
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHTFSTYP", sParMenu, iMenuPosition, Nothing, "TFS Type")
            '************************************************************************************
            '******OnTime [#Ico00036727] Transfrontier Shipment Module **************************
            '************************************************************************************
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_TFSType"
        End Function

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", True, 0, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_TCode", "Type Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_TName", "Type Name", True, -1, Nothing, Nothing)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)

            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
            End If

            oGridN.setOrderValue("Code")
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            MyBase.doButtonID1(oForm, pVal, BubbleEvent)

            If pVal.BeforeAction = False Then
                'IDH_JOBTYPE_SEQ.getInstance(true)
            End If
        End Sub


        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
        End Sub
    End Class
End Namespace
