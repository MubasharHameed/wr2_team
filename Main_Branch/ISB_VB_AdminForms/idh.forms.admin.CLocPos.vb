Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class CLocPos
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_WTLOPO", sParMenu, iMenuPosition, Nothing, "Container Location Positions" )
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Container Location Positions"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_WTLOPO"
        End Function

    End Class
End Namespace
