Imports System.IO
Imports System.Collections
Imports System
Imports com.idh.utils.Conversions
Imports IDHAddOns.idh.controls
Imports com.idh.bridge

Imports com.idh.bridge.lookups
Imports com.idh.utils

Namespace idh.forms.price
    Public MustInherit Class Templ
		Inherits idh.forms.admin.Tmpl
        
        Protected msType As String = "DN"

        Protected Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sID As String, ByVal sType As String, ByVal iMenuPosition As Integer, ByVal sTitle As String )
            MyBase.New(oParent, sID, "IDHMAST", iMenuPosition, "Item Pricing.srf", sTitle)
            msType = sType
            doEnableHistory(getTable())

            'doAddImagesToFix("IDH_CUSL")
            'doAddImagesToFix("IDH_WASTSE")
        End Sub

        Protected MustOverride Function getBPType() As String

        Protected MustOverride Function getBPCodeField() As String
        
        Protected MustOverride Function getBPUOMField() As String

		Protected MustOverride Function getBPInFeild() As String

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_CSITPR"
        End Function

        Protected Overrides Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
        	oGridN.doAddFilterField("IDH_CUST", getBPCodeField(), SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "EQUEM", 30)
        	oGridN.doAddFilterField("IDH_WASTCD", "U_WasteCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "Like", 30)
        	oGridN.doAddFilterField("IDH_WASTNM", "U_WasteDs", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "Like", 100)
        	oGridN.doAddFilterField("IDH_COVER", "(U_StDate < '[@]' And U_EnDate > '[@]')", SAPbouiCOM.BoDataType.dt_DATE, "REPLACE", 255)
        End Sub

		'		SRC:IDHISRC(PROD)[ITEMCODE=U_ItemCd;IDH_GRPCOD=#Test][U_ItemCd=ITEMCODE;U_ItemDs=ITEMNAME]
        '		   :FORMTPE(TARGET)[INPUT][OUTPUT]
        '					 INPUT - %USE VALUE AS IS
        '							 #USE GRID VALUE
        '							 >USE ITEM VALUE
		'					 OUTPUT - SRCFIELDVAL - use this value on as the new value of the current field
    	'							  COLID=SRCFIELDVAL - Use the result of the search field val as the new value for the
    	'												- Noted Col
    	'							  (EQVAL?NEWVAL:VAL) - If the Value = to EQVAL Then Use NEWVAL else use Val
        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
           	Dim sAddGrp As String  = com.idh.bridge.lookups.Config.INSTANCE.doGetAdditionalExpGroup()
            Dim sWastGrp As String = Config.INSTANCE.doWasteMaterialGroup()
            Dim sDefaultUOM As String = Config.INSTANCE.getDefaultUOM()
        	        	
            oGridN.doAddListField("Code", "Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", False,0, Nothing, Nothing)
            
            oGridN.doAddListField("U_AItmPr","Additional Price", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_AAddExp","Auto Add", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_Haulge","Haul Price", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_HaulCal","Haul. Charge Calc", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_TipTon","Tip Price", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ChrgCal","Charge Calc", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_FTon","From (Unit)", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_TTon","To (Unit)", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_UOM","UOM", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_StDate","Start", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_EnDate","Finish", True, -1, Nothing, Nothing)
            
            oGridN.doAddListField("U_WasteCd","Waste Code", True, -1, "SRC*IDHISRC(WST)[IDH_ITMCOD;IDH_GRPCOD=" & sWastGrp & "][ITEMCODE;U_WasteDs=ITEMNAME;U_UOM=" & getBPUOMField() & "(?" & sDefaultUOM & ")]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("U_WasteDs","Waste Desc.", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_AItmCd","Additional Code", True, -1, "SRC*IDHISRC(ADDX)[IDH_ITMCOD;IDH_GRPCOD=" & sAddGrp & "][ITEMCODE;U_AItmDs=ITEMNAME]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("U_AItmDs","Additional Desc", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ItemCd","Container Code", True, -1, "SRC*IDHISRC(ITM)[IDH_ITMCOD][ITEMCODE;U_ItemDs=ITEMNAME]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("U_ItemDs","Container Desc.", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_StAddr", "Site Addr.", True, -1, "SRC*IDHASRCH(CADDR)[IDH_CUSCOD=#" + getBPCodeField() + ";IDH_ADRTYP=%S][ADDRESS;U_ZpCd=ZIPCODE;U_StAdLN=ADDRSCD]", Nothing)
            oGridN.doAddListField("U_ZpCd","Postal Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_WR1ORD","WR1 Order", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_JobTp","Order Type", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_Branch","Branch", True, -1, "COMBOBOX", Nothing)
            
            oGridN.doAddListField(getBPCodeField(),"Business Partner", False, -1, "SRC*IDHCSRCH(CUS)[IDH_BPCOD;IDH_TYPE=F-C][CARDCODE]" , Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("U_ItmPLs","Item Price List", False, 0, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_ItemPr","Item Price", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ItemTPr","Item Tip. Price", False, 0, Nothing, Nothing)
            
            oGridN.doAddListField("U_FrmBsWe","Weight Base From", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ToBsWe","Weight Base To", False, -1, Nothing, Nothing)
		End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
        	MyBase.doCompleteCreate(oForm, BubbleEvent)

            Dim bCanChange As Boolean = True
            Try
                'Dim oItem As SAPbouiCOM.Item
                doAddUF( oForm, "IDH_CUSNAM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50, False, False )
				doAddUF( oForm, "IDH_CONNAM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 200, False, False )
                doAddUF( oForm, "IDH_PHONE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False )
                doAddUF( oForm, "U_Plist", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, True, False )

                doAddUF(oForm, "IDH_ADDR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 200, False, False)
                doAddUF(oForm, "IDH_ADLN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 10, False, False)
                doAddUF( oForm, "IDH_STREET", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100, False, False )
                doAddUF( oForm, "IDH_BLOCK", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100, False, False )
                doAddUF( oForm, "IDH_CITY", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100, False, False )
                doAddUF( oForm, "IDH_POSCOD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False )

'                'LINK BUTTONS
                Dim oLink As SAPbouiCOM.LinkedButton
                oLink = CType(oForm.Items.Item("IDH_LINKCU").Specific, SAPbouiCOM.LinkedButton)
                oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner

                oLink = CType(oForm.Items.Item("IDH_WASTLK").Specific, SAPbouiCOM.LinkedButton)
                oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_Items

                oForm.SupportedModes = SAPbouiCOM.BoFormMode.fm_OK_MODE Or SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                oForm.AutoManaged = False
                
                Dim fSettings As SAPbouiCOM.FormSettings
                fSettings = oForm.Settings
                fSettings.Enabled = False
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub

        '** Do the final form steps to show before loaddata
        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
        	MyBase.doBeforeLoadData( oForm )
        	
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
            End If
            
            If getHasSharedData(oForm) Then
                Dim sCardCode As String = getParentSharedDataAsString(oForm, getBPInFeild())
                Dim sItemCode As String = getParentSharedDataAsString(oForm, "ITEMCD")
                Dim sItemGrp As String = getParentSharedDataAsString(oForm, "ITMGRP")
                Dim sWastCd As String = getParentSharedDataAsString(oForm, "WASCD")
                Dim sAddress As String = getParentSharedDataAsString(oForm, "STADDR")
                Dim sJobType As String = getParentSharedDataAsString(oForm, "JOBTP")

                setUFValue(oForm, "IDH_CUST", sCardCode)
                setUFValue(oForm, "IDH_CUSNAM", "")
                setUFValue(oForm, "IDH_PHONE", "")
                setUFValue(oForm, "IDH_CONNAM", "")
                setUFValue(oForm, "U_Plist", "")
                
                setUFValue(oForm, "U_WasteCd", sWastCd)
            Else
                doSetFallback(oForm)
            End If            
            'setUFValue(oForm, "IDH_CUST", "")
            'setUFValue(oForm, "IDH_CUSNAM", "Fallback");
            'oGridN.setInitialFilterValue(getBPCodeField() & " = ''")
            
            doSetMode( oForm, SAPbouiCOM.BoFormMode.fm_OK_MODE )
        End Sub

        Public Sub doSetFallback(ByVal oForm As SAPbouiCOM.Form)
            setUFValue(oForm, "IDH_CUST", "")
            setUFValue(oForm, "IDH_CUSNAM", "Fallback")
            setUFValue(oForm, "IDH_PHONE", "")
            setUFValue(oForm, "IDH_CONNAM", "")
            setUFValue(oForm, "U_Plist", "")
            setUFValue(oForm, "U_WasteCd", "")
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
			Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            doLoadCombos(oForm)
        End Sub
        
#Region "FormModeHandler"
        '** Have to use this because of the fact that no Browse By is being used
        Protected Function doGetMode(ByVal oForm As SAPbouiCOM.Form) As SAPbouiCOM.BoFormMode
            Dim sCaption As String = CType(oForm.Items.Item("1").Specific, SAPbouiCOM.Button).Caption
            If sCaption.Equals( Translation.getTranslatedWord("Update") ) Then
            	Return SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            ElseIf sCaption.Equals( Translation.getTranslatedWord("Find") ) Then
                Return SAPbouiCOM.BoFormMode.fm_FIND_MODE
            ElseIf sCaption.Equals( Translation.getTranslatedWord("Add") ) Then
                Return SAPbouiCOM.BoFormMode.fm_ADD_MODE
            Else
            	Return SAPbouiCOM.BoFormMode.fm_OK_MODE
            End If
        End Function
        
        '** Have to use this because of the fact that no Browse By is being used
        Protected Sub doSetMode(ByVal oForm As SAPbouiCOM.Form, ByVal iMode As SAPbouiCOM.BoFormMode)
            If iMode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                CType(oForm.Items.Item("1").Specific, SAPbouiCOM.Button).Caption = Translation.getTranslatedWord("Find")
            ElseIf iMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                CType(oForm.Items.Item("1").Specific, SAPbouiCOM.Button).Caption = Translation.getTranslatedWord("Update")
                doSetUpdate(oForm)
            ElseIf iMode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                CType(oForm.Items.Item("1").Specific, SAPbouiCOM.Button).Caption = Translation.getTranslatedWord("Ok")
            End If
        End Sub
#End Region

#Region "CFLookups"
'        '** Item List
'        Private Sub AddChooseItemList(ByVal oForm As SAPbouiCOM.Form, ByVal sID As String, ByVal sGrp As String)
'            Try
'                Dim oCFLs As SAPbouiCOM.ChooseFromListCollection
'                Dim oCons As SAPbouiCOM.Conditions
'                Dim oCon As SAPbouiCOM.Condition
'
'                oCFLs = oForm.ChooseFromLists
'
'                Dim oCFL As SAPbouiCOM.ChooseFromList
'                Dim oCFLCreationParams As SAPbouiCOM.ChooseFromListCreationParams
'                oCFLCreationParams = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)
'
'                ' Adding 2 CFL, one for the button and one for the edit text.
'                oCFLCreationParams.MultiSelection = False
'                oCFLCreationParams.ObjectType = SAPbouiCOM.BoLinkedObject.lf_Items
'                oCFLCreationParams.UniqueID = sID
'                oCFL = oCFLs.Add(oCFLCreationParams)
'
'                If sGrp.Length > 0 Then
'                    ' Adding Conditions to CFL1
'                    oCons = oCFL.GetConditions()
'                    oCon = oCons.Add()
'                    oCon.Alias = "ItmsGrpCod"
'                    oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
'                    oCon.CondVal = sGrp
'
'                    oCFL.SetConditions(oCons)
'                End If
'            Catch ex As Exception
'                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error Creating ChooseList.")
'            End Try
'        End Sub

        '** Customer selection List
'        Private Sub AddChooseCustomerList(ByVal oForm As SAPbouiCOM.Form)
'            Try
'                Dim oCFLs As SAPbouiCOM.ChooseFromListCollection
'                Dim oCons As SAPbouiCOM.Conditions
'                Dim oCon As SAPbouiCOM.Condition
'
'                oCFLs = oForm.ChooseFromLists
'
'                Dim oCFL As SAPbouiCOM.ChooseFromList
'                Dim oCFLCreationParams As SAPbouiCOM.ChooseFromListCreationParams
'                oCFLCreationParams = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)
'
'                ' Adding 2 CFL, one for the button and one for the edit text.
'                oCFLCreationParams.MultiSelection = False
'                oCFLCreationParams.ObjectType = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner
'                oCFLCreationParams.UniqueID = "CFL1"
'                oCFL = oCFLs.Add(oCFLCreationParams)
'
'                ' Adding Conditions to CFL1
'                oCons = oCFL.GetConditions()
'                oCon = oCons.Add()
'                oCon.Alias = "CardType"
'                oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
'                oCon.CondVal = getBPType()
'                oCFL.SetConditions(oCons)
'
'                oCFLCreationParams.UniqueID = "CFL22"
'                oCFL = oCFLs.Add(oCFLCreationParams)
'
'                ' Adding Conditions to CFL2
'                oCFL.SetConditions(oCons)
'            Catch ex As Exception
'                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error Creating ChooseList.")
'            End Try
'        End Sub

        '** Customer selection List
'        Private Sub AddChooseCustomerListN(ByVal oForm As SAPbouiCOM.Form)
'            Try
'                Dim oCFLs As SAPbouiCOM.ChooseFromListCollection
'                Dim oCons As SAPbouiCOM.Conditions
'                Dim oCon As SAPbouiCOM.Condition
'
'                oCFLs = oForm.ChooseFromLists
'
'                Dim oCFL As SAPbouiCOM.ChooseFromList
'                Dim oCFLCreationParams As SAPbouiCOM.ChooseFromListCreationParams
'                oCFLCreationParams = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)
'
'                ' Adding 2 CFL, one for the button and one for the edit text.
'                oCFLCreationParams.MultiSelection = False
'                oCFLCreationParams.ObjectType = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner
'                oCFLCreationParams.UniqueID = "CFL11"
'                oCFL = oCFLs.Add(oCFLCreationParams)
'
'                ' Adding Conditions to CFL1
'                oCons = oCFL.GetConditions()
'                oCon = oCons.Add()
'                oCon.Alias = "CardType"
'                oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
'                oCon.CondVal = getBPType()
'                oCFL.SetConditions(oCons)
'
'                oCFLCreationParams.UniqueID = "CFL2"
'                oCFL = oCFLs.Add(oCFLCreationParams)
'
'                ' Adding Conditions to CFL2
'                oCFL.SetConditions(oCons)
'            Catch ex As Exception
'                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error Creating ChooseList.")
'            End Try
'        End Sub
#End Region

        Protected Sub doLoadCombos(ByVal oForm As SAPbouiCOM.Form)
        	doCalcTypeCombo(oForm)
        	doCalcTypeHaulCombo(oForm)
            doJobTypeCombo(oForm)
            doWR1OrdTypeCombo(oForm)
            doUOMCombo(oForm)
            doBranchCombo(oForm)
            doPriceListCombo(oForm)
            doAddAddCombo(oForm)
        End Sub

#Region "PopulateGridCombos"
        Private Sub doCalcTypeCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Dim oValidValues As SAPbouiCOM.ValidValues
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_ChrgCal")), SAPbouiCOM.ComboBoxColumn)
            	oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
				oValidValues = oCombo.ValidValues
			
            	doClearValidValues(oValidValues)
                oValidValues.Add("VARIABLE", "VARIABLE")
                oValidValues.Add("FIXED", "FIXED")
                oValidValues.Add("OFFSET", "OFFSET")
                oValidValues.Add("WEIGHT", "Use Weight")
                oValidValues.Add("QTY1", "Use Quantity 1")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Calculation type Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Calculation Type")})
            End Try
        End Sub
        
        Private Sub doCalcTypeHaulCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Dim oValidValues As SAPbouiCOM.ValidValues
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_HaulCal")), SAPbouiCOM.ComboBoxColumn)
            	oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
				oValidValues = oCombo.ValidValues
			
            	doClearValidValues(oValidValues)
                oValidValues.Add("STANDARD", "STANDARD")
                oValidValues.Add("WEIGHT", "Use Weight")
                oValidValues.Add("QTY1", "Use Quantity 1")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Haulage Calculation type Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Haulage Calculation Type")})
            End Try
        End Sub
        
        Private Sub doWR1OrdTypeCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Dim oValidValues As SAPbouiCOM.ValidValues
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_WR1ORD")), SAPbouiCOM.ComboBoxColumn)
            	oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
				oValidValues = oCombo.ValidValues
			
            	doClearValidValues(oValidValues)
            	oValidValues.Add("", getTranslatedWord("Any"))
                oValidValues.Add("DO", getTranslatedWord("Disposal Order"))
                oValidValues.Add("WO", getTranslatedWord("Waste Order"))
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Order Type Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Order Type")})
            End Try
        End Sub
        
        Private Sub doUOMCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_UOM")), SAPbouiCOM.ComboBoxColumn)
            	oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
        		
        		doFillCombo(oForm, oCombo, "OWGT", "UnitDisply", "UnitName", Nothing, Nothing, Nothing, Nothing)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the UOM Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("UOM")})
            End Try
        End Sub
        
        Private Sub doJobTypeCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_JobTp")), SAPbouiCOM.ComboBoxColumn)
            	oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
        		
        		doFillCombo(oForm, oCombo, "[@IDH_JOBTYPE]", "U_JobTp", Nothing, Nothing, Nothing, "Any", "")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Job Type Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Job Type")})
            End Try
        End Sub

        Public Sub doBranchCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_Branch")), SAPbouiCOM.ComboBoxColumn)
            	oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
        		
        		doFillCombo(oForm, oCombo, "OUBR", "Code", "Remarks", Nothing, Nothing, "Any", "")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Branch Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Branch")})
            End Try
        End Sub
        
        Public Sub doAddAddCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Dim oValidValues As SAPbouiCOM.ValidValues
            Try
            	Dim iIndex As Integer = oGridN.doIndexFieldWC("U_AAddExp")
            	If iIndex > 0 Then
                    oCombo = CType(oGridN.getSBOGrid.Columns.Item(iIndex), SAPbouiCOM.ComboBoxColumn)
	            	oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
					oValidValues = oCombo.ValidValues
				
	            	doClearValidValues(oValidValues)
	                oValidValues.Add(" ", "IGNORE")
	                oValidValues.Add("A", "Add")
	                oValidValues.Add("N", "Do Not Add")
            	End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Auto Add Additional Item.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Auto Add Additional Item")})
            End Try            
        End Sub 
        
        Public Sub doPriceListCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_ItmPLs")), SAPbouiCOM.ComboBoxColumn)
            	oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
        		
        		doFillCombo(oForm, oCombo, "OPLN", "ListNum", "ListName", Nothing, Nothing, Nothing, Nothing)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Branch Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Branch")})
            End Try
        End Sub
#End Region

#Region "DialogLookups"
        Protected Overridable Sub doChooseFilterBP(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean)
        	Dim sCardCode As String = getItemValue(oForm, "IDH_CUST" )
        	
   	        setSharedData(oForm, "IDH_TYPE", "F-" & getBPType())

            setSharedData(oForm, "IDH_BPCOD", sCardCode)
    	    setSharedData(oForm, "IDH_NAME", "")
        	setSharedData(oForm, "TRG", "BPF")
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
           
            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub
        
        Protected Overridable Sub doChooseFilterWaste(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean)
        	Dim sWasteCode As String = getItemValue(oForm, "IDH_WASTCD")
        	Dim sWasteDesc As String = getItemValue(oForm, "IDH_WASTNM")
        	
            Dim sWastGrp As String = Config.INSTANCE.doWasteMaterialGroup()
   	        setSharedData(oForm, "ITMSGRPCOD", sWastGrp)

            setSharedData(oForm, "ITEMCODE", sWasteCode)
    	    setSharedData(oForm, "ITEMNAME", sWasteDesc)
        	setSharedData(oForm, "TRG", "WST")
            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If

            If String.IsNullOrEmpty(getSharedDataAsString(oForm, "IDH_ITMSRCHOPEND")) Then
                goParent.doOpenModalForm("IDHWISRC", oForm)
            End If
        End Sub
#End Region

        Private Sub doSetLastLine(ByVal oForm As SAPbouiCOM.Form, Optional ByVal iCRow As Integer = -1)
            Dim sList As String = CType(getUFValue(oForm, "U_Plist", True), String)
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")

			Dim sCardCode As String = getItemValue(oForm, "IDH_CUST" )
			oGridN.doSetFieldValue( getBPCodeField(), iCRow, sCardCode )
            oGridN.doSetFieldValue("U_BP2CD", iCRow, "")
            oGridN.doSetFieldValue("U_StAddr", iCRow, "")
            oGridN.doSetFieldValue("U_JobTp", iCRow, "")
            oGridN.doSetFieldValue("U_ItemCd", iCRow, "")
            oGridN.doSetFieldValue("U_ItemDs", iCRow, "")
            oGridN.doSetFieldValue("U_WasteCd", iCRow, "")
            oGridN.doSetFieldValue("U_WasteDs", iCRow, "")
            oGridN.doSetFieldValue("U_ItmPLs", iCRow, sList)
            oGridN.doSetFieldValue("U_ItemPr", iCRow, 0)
            oGridN.doSetFieldValue("U_ItemTPr", iCRow, 0)

            oGridN.doSetFieldValue("U_AItmPr", iCRow, 0)
            oGridN.doSetFieldValue("U_AItmCd", iCRow, "")
            oGridN.doSetFieldValue("U_AItmDs", iCRow, "")
            oGridN.doSetFieldValue("U_Branch", iCRow, "")

            Dim sDefaultUOM As String = Config.INSTANCE.getDefaultUOM()
            oGridN.doSetFieldValue("U_UOM", iCRow, sDefaultUOM) ''"Kg")

            oGridN.doSetFieldValue("U_WR1ORD", iCRow, "")
            oGridN.doSetFieldValue("U_ChrgCal", iCRow, "VARIABLE")
			oGridN.doSetFieldValue("U_HaulCal", iCRow, "STANDARD")

            oGridN.doSetFieldValue("U_Haulge", iCRow, 0)
            oGridN.doSetFieldValue("U_TipTon", iCRow, 0)

			oGridN.doSetFieldValue("U_FTon", iCRow, 0)
			oGridN.doSetFieldValue("U_FrmBsWe", iCRow, 0)

            Dim sDefaultToWeight As String = Config.Parameter("IPDETW")
            If sDefaultToWeight Is Nothing Then
                sDefaultToWeight = "0"
                oGridN.doSetFieldValue("U_ToBsWe", iCRow, 0)
            Else
                Dim dDefault As Double = ToDouble(sDefaultToWeight)
                dDefault = Config.INSTANCE.doGetBaseWeight(sDefaultUOM, dDefault)
                oGridN.doSetFieldValue("U_ToBsWe", iCRow, dDefault)
            End If
            oGridN.doSetFieldValue("U_TTon", iCRow, sDefaultToWeight)

            Dim oStartDate As Date = Date.Now()
            Dim oEndDate As Date = oStartDate
            oGridN.doSetFieldValue("U_StDate", iCRow, oStartDate)
            
            Dim sEndDate As String = Config.Parameter("IPDFED")
            If Not sEndDate Is Nothing AndAlso sEndDate.Length > 0 Then
                oEndDate = com.idh.utils.dates.doStrToDate(sEndDate)
                If Date.Compare(oEndDate, oStartDate) < 0 Then
                    oEndDate = oStartDate
                End If
            Else
            	oEndDate = oEndDate.AddYears(1)
            End If
            oGridN.doSetFieldValue("U_EnDate", iCRow, oEndDate)
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        	Dim oMode As SAPbouiCOM.BoFormMode = doGetMode( oForm )
            If pVal.BeforeAction = True Then
                If oMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
					oMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                	Dim oUpdateGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                	If oUpdateGrid.doProcessData() = False Then
                    	BubbleEvent = False
                    Else
                        doSetMode( oForm, SAPbouiCOM.BoFormMode.fm_OK_MODE )
						doLoadData( oForm )
						BubbleEvent = False
                	End If
                ElseIf oMode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    If goParent.doCheckModal(oForm.UniqueID) = True Then
                        doReturnFromModalShared(oForm, True)
                    Else
                    	oForm.Close()
                    End If
                End If
            Else
                If oMode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
					doLoadData( oForm )
					BubbleEvent = False
                End If
            End If
        End Sub

        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED Then
                If pVal.BeforeAction = True Then
                    Dim sField As String
                    sField = CType(pVal.oData, String)

                    Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                    If oGridN.doCheckIsSameCol(sField, "U_FTon") OrElse _
                    	oGridN.doCheckIsSameCol(sField, "U_TTon") OrElse _
                    	oGridN.doCheckIsSameCol(sField, "U_UOM") Then
						doCalculateBaseWeights(oForm)
                    End If
                    doSetMode( oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE )
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_ADD_EMPTY Then
                If pVal.BeforeAction = False Then
                	doSetLastLine( oForm, pVal.Row )
                End If
            End If
            Return MyBase.doCustomItemEvent(oForm, pVal)
        End Function

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_CUSL" Then
						doChooseFilterBP(oForm, False)
					ElseIf pVal.ItemUID = "IDH_WASTSE" Then
						doChooseFilterWaste(oForm, True)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
            	If pVal.BeforeAction = False Then
            		If pVal.ItemChanged AndAlso pVal.InnerEvent = False Then
            			Dim sItem As String = pVal.ItemUID
            			If sItem = "IDH_COVER" Then
            				doLoadData( oForm )
            			ElseIf sItem = "IDH_CUST" Then
							Dim sCardCode As String = getItemValue(oForm, "IDH_CUST")
            				Dim sCardName As String = getItemValue(oForm, "IDH_CUSNAM")
            				If sCardCode.Length > 0 AndAlso sCardName.Length > 0 Then
								setItemValue( oForm, "IDH_CUSNAM", "" )
            				End If
            				doLoadData( oForm )
            			ElseIf sItem = "IDH_CUSNAM" Then
							Dim sCardCode As String = getItemValue(oForm, "IDH_CUST")
            				Dim sCardName As String = getItemValue(oForm, "IDH_CUSNAM")
            				If sCardCode.Length > 0 AndAlso sCardName.Length > 0 Then
								setItemValue( oForm, "IDH_CUST", "" )
            				End If
            				doLoadData( oForm )
            			ElseIf sItem = "IDH_WASTCD" Then
            				Dim sItemCode As String = getItemValue(oForm, "IDH_WASTCD")
            				Dim sItemDesc As String = getItemValue(oForm, "IDH_WASTNM")
            				If sItemCode.Length > 0 AndAlso sItemDesc.Length > 0 Then
								setItemValue( oForm, "IDH_WASTNM", "" )
            				End If
            				doLoadData( oForm )
            			ElseIf sItem = "IDH_WASTNM" Then
            				Dim sItemCode As String = getItemValue(oForm, "IDH_WASTCD")
            				Dim sItemDesc As String = getItemValue(oForm, "IDH_WASTNM")
            				If sItemCode.Length > 0 AndAlso sItemDesc.Length > 0 Then
		                		setItemValue( oForm, "IDH_WASTCD", "" )
							End If
	                		doLoadData( oForm )
            			End If
            		End If
            	End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If doCheckSearchKey(pVal) = True Then
                    If CType(getWFValue(oForm, "FromSearch"), Boolean) = True Then
                        setWFValue(oForm, "FromSearch", False)
                    Else
                    End If
                End If
                'OnTime Ticket 23218 - Form Mode turns to 'Update' for the first change only
                'for ever second and onwards change the button remains 'Find'
                'Note: this change will be effective for both
                'idh.forms.price.Supplier and idh.forms.price.Customer classes
                If pVal.FormMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE And _
                    (CType(oForm.Items.Item("1").Specific, SAPbouiCOM.Button).Caption = Translation.getTranslatedWord("Find") Or _
                     CType(oForm.Items.Item("1").Specific, SAPbouiCOM.Button).Caption = Translation.getTranslatedWord("Ok")) And _
                     pVal.ItemUID = "LINESGRID" Then
                    CType(oForm.Items.Item("1").Specific, SAPbouiCOM.Button).Caption = Translation.getTranslatedWord("Update")
                End If
            End If
            Return False
        End Function

        '** Calculate the base From and To weights
        Private Function doCalculateBaseWeights(ByVal oForm As SAPbouiCOM.Form) As Boolean
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            
            Dim sUOM As String
            Dim dFWeight As Double = Conversions.ToDouble(oGridN.doGetFieldValue("U_FTon"))
            Dim dTWeight As Double = Conversions.ToDouble(oGridN.doGetFieldValue("U_TTon"))

            sUOM = CType(oGridN.doGetFieldValue("U_UOM"), String)
            Dim dFactor As Double = Config.INSTANCE.doGetBaseWeightFactors(sUOM)
            If dFactor <= 0 Then
            	dFactor = 1
            End If
            
			dFWeight = dFWeight * dFactor
			dTWeight = dTWeight * dFactor
			
			Dim bFailed As Boolean = False
			'9200000000000
			If dFWeight > 9200000000000 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("The 'From' base Weight is to big. Calculated - " & dFWeight)
                DataHandler.INSTANCE.doResSystemError("The 'From' base Weight is to big. Calculated - " & dFWeight, "ERSYWHTO", {com.idh.bridge.Translation.getTranslatedWord("From"), dFWeight.ToString()})
				
				dFWeight = 9200000000000 / dFactor
				oGridN.doSetFieldValue("U_FTon", dFWeight)
				oGridN.doSetFieldValue("U_FrmBsWe", 9200000000000)
				Return False
			Else
				oGridN.doSetFieldValue("U_FrmBsWe", dFWeight)
			End If
			
			If dTWeight > 9200000000000 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("The 'To' base Weight is to big. Calculated - " & dTWeight)
                DataHandler.INSTANCE.doResSystemError("The 'To' base Weight is to big. Calculated - " & dFWeight, "ERSYWHTO", {com.idh.bridge.Translation.getTranslatedWord("To"), dFWeight.ToString()})
				
            	dTWeight = 9200000000000 / dFactor
            	oGridN.doSetFieldValue("U_TTon", dTWeight)
            	oGridN.doSetFieldValue("U_ToBsWe", 9200000000000)
				Return False
			Else
				oGridN.doSetFieldValue("U_ToBsWe", dTWeight)
			End If
            Return True
        End Function

        '** Create a new Entry if the Add option is selected
        Private Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        '** Clears all the fields when going into the find mode.
        Private Sub doClearAll(ByVal oForm As SAPbouiCOM.Form)
        End Sub

        '** The Menu Event handler
        '** Return True if the Event must be handled by the other Objects
        Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.BeforeAction = False Then
            End If
            Return True
        End Function

        Public Overrides Sub doCloseForm(ByVal oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCloseForm(oForm, BubbleEvent)
        End Sub

        Public Overrides Sub doClose()
        End Sub

        ''MODAL DIALOG
        ''*** Set the return data when called as a modal dialog
        Private Sub doSetModalData(ByVal oForm As SAPbouiCOM.Form)
            Dim sMode As String = ""
            If ghOldDialogParams.Contains(oForm.UniqueID) Then
                Dim oData As ArrayList = CType(ghOldDialogParams.Item(oForm.UniqueID), ArrayList)
                If Not (oData Is Nothing) AndAlso oData.Count > 2 Then
                    sMode = CType(oData.Item(1), String)
                End If
            End If

            Dim aData As New ArrayList
            If sMode.Length = 0 Then
                sMode = "Add"
            Else
                sMode = "Change"
            End If

            With oForm.DataSources.DBDataSources.Item("@IDH_CUSTITPR")
                aData.Add(sMode)
                aData.Add("")
            End With

            doReturnFromModalBuffered(oForm, CType(aData, Object))
        End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType = "IDHCSRCH" Then
                    Dim sTarget As String = getSharedDataAsString(oForm, "TRG")
					
					If sTarget.Equals("BPF") Then
                        Dim sCardCode As String = getSharedDataAsString(oForm, "CARDCODE")
                        Dim sCardName As String = getSharedDataAsString(oForm, "CARDNAME")
                        Dim sPhone1 As String = getSharedDataAsString(oForm, "PHONE1")
                        Dim sCntctPrsn As String = getSharedDataAsString(oForm, "CNTCTPRSN")
						
                        Dim sListNum As String = getSharedDataAsString(oForm, "PLIST")
						
						setItemValue( oForm, "IDH_CUST", sCardCode )
						setItemValue( oForm, "IDH_CUSNAM", sCardName )
						setItemValue( oForm, "IDH_PHONE", sPhone1 )
						setItemValue( oForm, "IDH_CONNAM", sCntctPrsn )
						setUFValue( oForm, "U_Plist", sListNum )
						setDefaultAddress( oForm )
						
						doLoadData( oForm )
					End If
                ElseIf sModalFormType = "IDHWISRC" Then
                    Dim sTarget As String = getSharedDataAsString(oForm, "TRG")
                	
                	If sTarget.Equals("WST") Then
                        Dim sItemCode As String = getSharedDataAsString(oForm, "ITEMCODE")
                        Dim sItemDesc As String = getSharedDataAsString(oForm, "ITEMNAME")
                		
                		setItemValue( oForm, "IDH_WASTCD", sItemCode )
						setItemValue( oForm, "IDH_WASTNM", sItemDesc )
						
						doLoadData( oForm )
                	End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing Modal Result - " & sModalFormType)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub
        
        Private Sub setDefaultAddress( ByVal oForm As SAPbouiCOM.Form )
            Dim sCardCode As String = getItemValue(oForm, "IDH_CUST")
            Dim bAddrSet As Boolean = False
            Try
                If sCardCode.Length > 0 Then
                    Dim oData As ArrayList = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(goParent, sCardCode, "S")
                	If Not oData Is Nothing Then
	                	setUFValue(oForm, "IDH_ADDR", oData.Item(0))
	                	setUFValue(oForm, "IDH_STREET", oData.Item(1))
	                	setUFValue(oForm, "IDH_BLOCK", oData.Item(2))
	                	setUFValue(oForm, "IDH_POSCOD", oData.Item(3))
                        setUFValue(oForm, "IDH_CITY", oData.Item(4))
                        setUFValue(oForm, "IDH_ADLN", oData.Item(30))
                		bAddrSet = True
                	End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error Loading Data.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXLDBD", {Nothing})
            End Try

			If bAddrSet = False Then
            	setUFValue(oForm, "IDH_ADDR", "")
            	setUFValue(oForm, "IDH_STREET", "")
            	setUFValue(oForm, "IDH_BLOCK", "")
            	setUFValue(oForm, "IDH_POSCOD", "")
                setUFValue(oForm, "IDH_CITY", "")
                setUFValue(oForm, "IDH_ADLN", "")
			End If
        End Sub
        
    End Class
End Namespace
