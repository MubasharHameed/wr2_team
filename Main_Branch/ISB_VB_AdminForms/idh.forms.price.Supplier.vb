Imports System.IO
Imports System.Collections
Imports System
Imports IDHAddOns.idh.controls

Namespace idh.forms.price
    Public Class Supplier
        Inherits idh.forms.price.Templ

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_SUITPR", "SUP", iMenuPosition, "Supplier Item Price")
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_SUITPR"
        End Function

        Protected Overrides Function getBPType() As String
            Return "S"
        End Function

        Protected Overrides Function getBPCodeField() As String
            Return "U_CardCd"
        End Function
        
        Protected Overrides Function getBPInFeild() As String
        	Return "SUPCD"
        End Function
        
        Protected Overrides Function getBPUOMField() As String
        	Return "PUOM"
        End Function 
        
'		'		SRC:IDHISRC(PROD)[ITEMCODE=U_ItemCd;IDH_GRPCOD=#Test][U_ItemCd=ITEMCODE;U_ItemDs=ITEMNAME]
'        '		   :FORMTPE(TARGET)[INPUT][OUTPUT]
'        '					 INPUT - %USE VALUE AS IS
'        '							 #USE GRID VALUE
'        '							 >USE ITEM VALUE
'		'					 OUTPUT - SRCFIELDVAL - use this value on as the new value of the current field
'    	'							  COLID=SRCFIELDVAL - Use the result of the search field val as the new value for the
'    	'												- Noted Col
'    	'							  (EQVAL?NEWVAL:VAL) - If the Value = to EQVAL Then Use NEWVAL else use Val
'        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
'           	Dim sAddGrp As String  = com.idh.bridge.lookups.Config.INSTANCE.doGetAdditionalExpGroup()
'           	Dim sWastGrp As String = Config.INSTANCE.doWasteMaterialGroup(goParent)
'           	Dim sDefaultUOM As String = Config.INSTANCE.getDefaultUOM(goParent)
'        	        	
'            oGridN.doAddListField("Code", "Code", False, -1, Nothing, Nothing)
'            oGridN.doAddListField("Name", "Name", False,0, Nothing, Nothing)
'            oGridN.doAddListField("U_JobTp","Order Type", True, -1, "COMBOBOX", Nothing)
'            oGridN.doAddListField("U_ItemCd","Container Code", True, -1, "SRC*IDHISRC(ITM)[IDH_ITMCOD][ITEMCODE;U_ItemDs=ITEMNAME]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
'            oGridN.doAddListField("U_ItemDs","Container Desc.", True, -1, Nothing, Nothing)
'            oGridN.doAddListField(getBPCodeField(),"Business Partner", False, -1, "SRC*IDHCSRCH(SUP)[IDH_BPCOD;IDH_TYPE=F-S][CARDCODE]" , Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
'            oGridN.doAddListField("U_BP2CD","Customer", True, -1, "SRC*IDHCSRCH(CUS)[IDH_BPCOD;IDH_TYPE=F-C][CARDCODE]" , Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
'            'oGridN.doAddListField("U_StAddr","Site Addr.", True, -1, "SRC*IDHASRCH(CADDR)[IDH_CUSCOD=#" + getBPCodeField() + ";IDH_ADRTYP=%S][ADDRESS;U_ZpCd=ZIPCODE]", Nothing)
'            oGridN.doAddListField("U_StAddr","Site Addr.", True, -1, "SRC*IDHASRCH(CADDR)[IDH_CUSCOD=#U_BP2CD;IDH_ADRTYP=%S][ADDRESS;U_ZpCd=ZIPCODE]", Nothing)
'            oGridN.doAddListField("U_ZpCd","Postal Code", True, -1, Nothing, Nothing)
'            oGridN.doAddListField("U_StDate","Start", True, -1, Nothing, Nothing)
'            oGridN.doAddListField("U_EnDate","Finish", True, -1, Nothing, Nothing)
'            oGridN.doAddListField("U_Branch","Branch", True, -1, "COMBOBOX", Nothing)
'            oGridN.doAddListField("U_WasteCd","Waste Code", True, -1, "SRC*IDHISRC(WST)[IDH_ITMCOD;IDH_GRPCOD=" & sWastGrp & "][ITEMCODE;U_WasteDs=ITEMNAME;U_UOM=" & getBPUOMField() & "(?" & sDefaultUOM & ")]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
'            oGridN.doAddListField("U_WasteDs","Waste Desc.", True, -1, Nothing, Nothing)
'            oGridN.doAddListField("U_WR1ORD","WR1 Order", True, -1, "COMBOBOX", Nothing)
'            oGridN.doAddListField("U_ItmPLs","Item Price List", False, -1, "COMBOBOX", Nothing)
'            oGridN.doAddListField("U_ItemPr","Item Price", False, -1, Nothing, Nothing)
'            oGridN.doAddListField("U_ItemTPr","Item Tip. Price", False, -1, Nothing, Nothing)
'            oGridN.doAddListField("U_AItmCd","Additional Code", True, -1, "SRC*IDHISRC(ADDX)[IDH_ITMCOD;IDH_GRPCOD=" & sAddGrp & "][ITEMCODE;U_AItmDs=ITEMNAME]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
'            oGridN.doAddListField("U_AItmDs","Additional Desc", True, -1, Nothing, Nothing)
'            oGridN.doAddListField("U_UOM","UOM", True, -1, "COMBOBOX", Nothing)
'            oGridN.doAddListField("U_FTon","From (Unit)", True, -1, Nothing, Nothing)
'            oGridN.doAddListField("U_TTon","To (Unit)", True, -1, Nothing, Nothing)
'            oGridN.doAddListField("U_ChrgCal","Charge Calc", True, -1, "COMBOBOX", Nothing)
'            oGridN.doAddListField("U_HaulCal","Haul. Charge Calc", True, -1, "COMBOBOX", Nothing)
'            oGridN.doAddListField("U_TipTon","Tip Price", True, -1, Nothing, Nothing)
'            oGridN.doAddListField("U_Haulge","Haul Price", True, -1, Nothing, Nothing)
'            oGridN.doAddListField("U_AItmPr","Additional Price", True, -1, Nothing, Nothing)
'            oGridN.doAddListField("U_FrmBsWe","Weight Base From", False, -1, Nothing, Nothing)
'            oGridN.doAddListField("U_ToBsWe","Weight Base To", False, -1, Nothing, Nothing)
'		End Sub
        
		'		SRC:IDHISRC(PROD)[ITEMCODE=U_ItemCd;IDH_GRPCOD=#Test][U_ItemCd=ITEMCODE;U_ItemDs=ITEMNAME]
        '		   :FORMTPE(TARGET)[INPUT][OUTPUT]
        '					 INPUT - %USE VALUE AS IS
        '							 #USE GRID VALUE
        '							 >USE ITEM VALUE
		'					 OUTPUT - SRCFIELDVAL - use this value on as the new value of the current field
    	'							  COLID=SRCFIELDVAL - Use the result of the search field val as the new value for the
    	'												- Noted Col
    	'							  (EQVAL?NEWVAL:VAL) - If the Value = to EQVAL Then Use NEWVAL else use Val
        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
           	Dim sAddGrp As String  = com.idh.bridge.lookups.Config.INSTANCE.doGetAdditionalExpGroup()
            Dim sWastGrp As String = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup()
            Dim sDefaultUOM As String = com.idh.bridge.lookups.Config.INSTANCE.getDefaultUOM()
        	        	
            oGridN.doAddListField("Code", "Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", False,0, Nothing, Nothing)
            
            oGridN.doAddListField("U_AItmPr","Additional Price", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Haulge","Haul Price", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_HaulCal","Haul. Charge Calc", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_TipTon","Tip Price", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ChrgCal","Charge Calc", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_FTon","From (Unit)", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_TTon","To (Unit)", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_UOM","UOM", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_StDate","Start", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_EnDate","Finish", True, -1, Nothing, Nothing)
            
            oGridN.doAddListField("U_WasteCd","Waste Code", True, -1, "SRC*IDHISRC(WST)[IDH_ITMCOD;IDH_GRPCOD=" & sWastGrp & "][ITEMCODE;U_WasteDs=ITEMNAME;U_UOM=" & getBPUOMField() & "(?" & sDefaultUOM & ")]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("U_WasteDs","Waste Desc.", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_AItmCd","Additional Code", True, -1, "SRC*IDHISRC(ADDX)[IDH_ITMCOD;IDH_GRPCOD=" & sAddGrp & "][ITEMCODE;U_AItmDs=ITEMNAME]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("U_AItmDs","Additional Desc", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ItemCd","Container Code", True, -1, "SRC*IDHISRC(ITM)[IDH_ITMCOD][ITEMCODE;U_ItemDs=ITEMNAME]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("U_ItemDs","Container Desc.", True, -1, Nothing, Nothing)
            'oGridN.doAddListField("U_StAddr","Site Addr.", True, -1, "SRC*IDHASRCH(CADDR)[IDH_CUSCOD=#" + getBPCodeField() + ";IDH_ADRTYP=%S][ADDRESS;U_ZpCd=ZIPCODE]", Nothing)
            oGridN.doAddListField("U_StAddr", "Site Addr.", True, -1, "SRC*IDHASRCH(CADDR)[IDH_CUSCOD=#U_BP2CD;IDH_ADRTYP=%S][ADDRESS;U_ZpCd=ZIPCODE;U_StAdLN=ADDRSCD]", Nothing)
            oGridN.doAddListField("U_ZpCd","Postal Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_WR1ORD","WR1 Order", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_JobTp","Order Type", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_Branch","Branch", True, -1, "COMBOBOX", Nothing)
            
            oGridN.doAddListField(getBPCodeField(),"Business Partner", False, -1, "SRC*IDHCSRCH(SUP)[IDH_BPCOD;IDH_TYPE=F-S][CARDCODE]" , Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("U_BP2CD","Customer", True, -1, "SRC*IDHCSRCH(CUS)[IDH_BPCOD;IDH_TYPE=F-C][CARDCODE]" , Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            
            oGridN.doAddListField("U_ItmPLs","Item Price List", False, 0, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_ItemPr","Item Price", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ItemTPr","Item Tip. Price", False, 0, Nothing, Nothing)
            
            oGridN.doAddListField("U_FrmBsWe","Weight Base From", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ToBsWe","Weight Base To", False, -1, Nothing, Nothing)
		End Sub        
        
    End Class
End Namespace
