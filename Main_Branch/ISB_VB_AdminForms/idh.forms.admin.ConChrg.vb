Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class ConChrg
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_WTCOCH", sParMenu, iMenuPosition, Nothing, "Consignment Charge" )
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Consignment Charge"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_WTCOCH"
        End Function

    End Class
End Namespace
