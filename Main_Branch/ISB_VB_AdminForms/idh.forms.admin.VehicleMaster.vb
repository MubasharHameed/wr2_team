Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System
Imports com.idh.bridge.resources
Imports com.idh.bridge
Imports com.idh.utils

Namespace idh.forms.admin
    Public Class VehicleMaster
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHVEAD", sParMenu, iMenuPosition, "VehicleAdmin.srf", "Vehicle Master")
            msKeyGen = "VEHMAST"
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Vehicle Master"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_VEHMAS"
        End Function

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
        End Sub

        Private Sub doVehTypeCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_VehType")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)

            oCombo.ValidValues.Add("A", getTranslatedWord("Asset-OnRoad"))
            oCombo.ValidValues.Add("O", getTranslatedWord("Asset-OffRoad"))
            oCombo.ValidValues.Add("S", getTranslatedWord("Sub Contract"))
            oCombo.ValidValues.Add("H", getTranslatedWord("Owner Driver"))
            '##MA Start 06-08-2014; Add new Vehicle type: Requested by Scott(Ovendon) via HH
            oCombo.ValidValues.Add("C", getTranslatedWord("Customer"))
            '##MA End 06-08-2014
        End Sub

        Private Sub doAccFlagsCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_MarkAc")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)

            oCombo.ValidValues.Add("", "Other")
            oCombo.ValidValues.Add("DOFOC", getTranslatedWord("Free Of Charge"))
            oCombo.ValidValues.Add("DOORD", getTranslatedWord("Account - Order"))
            oCombo.ValidValues.Add("DOARI", getTranslatedWord("Invoice"))
            oCombo.ValidValues.Add("DOAINV", getTranslatedWord("Invoice PrePaid"))
            oCombo.ValidValues.Add("DOARIP", getTranslatedWord("Invoice + Payment"))
        End Sub

        Private Sub doWR1OrderTypeCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_WR1ORD")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)

            oCombo.ValidValues.Add("", getTranslatedWord("All"))
            oCombo.ValidValues.Add("WO", getTranslatedWord("Waste Order"))
            oCombo.ValidValues.Add("DO", getTranslatedWord("Disposal  Order"))
        End Sub

        Private Sub doWFFilterCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            Dim oCombo As SAPbouiCOM.ComboBox

            oItem = oForm.Items.Item("IDH_WF")
            oItem.DisplayDesc = True
            oCombo = CType(oItem.Specific, SAPbouiCOM.ComboBox)

            doClearValidValues(oCombo.ValidValues)

            oCombo.ValidValues.Add("", getTranslatedWord("Any"))
            oCombo.ValidValues.Add("ReqAdd", getTranslatedWord("Request to Add"))
            oCombo.ValidValues.Add("ReqUpdate", getTranslatedWord("Request to Update"))
            oCombo.ValidValues.Add("Approved", getTranslatedWord("Request Approved"))
            oCombo.ValidValues.Add("Denied", getTranslatedWord("Request Denied"))
        End Sub

        Private Sub doWFStatusCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_WFStat")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)

            oCombo.ValidValues.Add("", getTranslatedWord("DK"))
            oCombo.ValidValues.Add("ReqAdd", getTranslatedWord("Request to Add"))
            oCombo.ValidValues.Add("ReqUpdate", getTranslatedWord("Request to Update"))
            oCombo.ValidValues.Add("Approved", getTranslatedWord("Request Approved"))
            oCombo.ValidValues.Add("Denied", getTranslatedWord("Request Denied"))
        End Sub

        'Private Sub doBranchCombo(ByVal oForm As SAPbouiCOM.Form)
        '    doFillCombo(oForm, "IDH_BRANCH", "OUBR", "Code", "Remarks", Nothing, Nothing, True)
        'End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
            doVehTypeCombo(oForm)
            doAccFlagsCombo(oForm)
            doWFStatusCombo(oForm)
            doWR1OrderTypeCombo(oForm)
            'doBranchCombo(oForm)
			
			'## Start 03-10-2013
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            oGridN.doSetDeleteActive(True)
            '## End
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
            doWFFilterCombo(oForm)
        End Sub

        Protected Overrides Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddFilterField("IDH_CUST", "U_CCrdCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            ''MA Issue#1142 18-07-2016 removed branch filter
            ''oGridN.doAddFilterField("IDH_BRANCH", "U_Branch", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 15)
            ''MA Issue#1142 18-07-2016
            oGridN.doAddFilterField("IDH_REG", "U_VehReg", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_CODE", "Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_WF", "U_WFStat", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
        End Sub


        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            'Protected Overridable Sub doTheGridLayout(ByVal oGridN As UpdateGrid)
            If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", False) Then
                Dim sFormTypeId As String = oGridN.getSBOForm().TypeEx
                Dim oFormSettings As com.idh.dbObjects.User.IDH_FORMSET = getWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId)
                If oFormSettings Is Nothing Then
                    oFormSettings = New com.idh.dbObjects.User.IDH_FORMSET()
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, "")
                    setWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId, oFormSettings)

                    oFormSettings.doAddFieldToGrid(oGridN)

                    doAddListFields(oGridN)

                    'If oFormSettings.SkipFormSettings = False Then
                    '    oFormSettings.doSetGrid(oGridN)
                    'End If
                    oFormSettings.doSyncDB(oGridN)
                Else
                    If oFormSettings.SkipFormSettings Then
                        doAddListFields(oGridN)
                    Else
                        oFormSettings.doAddFieldToGrid(oGridN)
                        'oFormSettings.doSetGrid(oGridN)
                    End If
                End If
            Else
                doAddListFields(oGridN)
            End If
        End Sub
        Protected Sub doAddListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", False, -1, Nothing, Nothing)

            oGridN.doAddListField("U_VehType", "Vehicle Type (A-Asset, S-Supplier)", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_VehReg", "Vehicle Registration", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_VehDesc", "Vehicle Description", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Tarre", "Tare", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_TRWTE1", "Tare Weight Date", True, -1, "DATE", Nothing)

            oGridN.doAddListField("U_DrivrNr", "Driver Number", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Driver", "Driver", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ItmGrp", "Item Group Code", True, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_ItemGroups)
            If com.idh.bridge.lookups.Config.ParameterAsBool("HIDITGPD") Then
                oGridN.doAddListField("U_IGrpDesc", "Item Group Description", True, 0, Nothing, Nothing)
            Else
                oGridN.doAddListField("U_IGrpDesc", "Item Group Description", True, -1, Nothing, Nothing)
            End If

            oGridN.doAddListField("U_TRLReg", "2nd Tare Id", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_TRLNM", "2nd Tare Name", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_TRLTar", "2nd Tare Weight (kg)", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_TRWTE2", "2nd Tare Weight Date", True, -1, "DATE", Nothing)

            oGridN.doAddListField("U_CCCrdCd", "Waste Carrier Code (Cust)", True, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("U_CCName", "Waste Carrier Name (Cust)", True, -1, Nothing, Nothing)

            oGridN.doAddListField("U_CSCrdCd", "Waste Carrier Code (Supp)", True, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("U_CSName", "Waste Carrier Name (Supp)", True, -1, Nothing, Nothing)

            oGridN.doAddListField("U_CCrdCd", "Customer Code", True, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("U_CName", "Customer Name", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_CAddress", "Customer Address", True, -1, "SRC*IDHASRCH(CADDR)[IDH_CUSCOD=#U_CCrdCd;IDH_ADRTYP=%S][ADDRESS;U_CAddrLN=ADDRSCD]", Nothing)
            oGridN.doAddListField("U_CAddrLN", "Customer Address LineNum", True, 0, Nothing, Nothing)

            oGridN.doAddListField("U_SCrdCd", "Disposal Site", True, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("U_SName", "Disposal Name", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_SAddress", "Disposal Address", True, -1, "SRC*IDHASRCH(SADDR)[IDH_CUSCOD=#U_SCrdCd;IDH_ADRTYP=%S]][ADDRESS;U_SAddrLN=ADDRSCD]", Nothing)
            oGridN.doAddListField("U_SAddrLN", "Disposal Site Address LineCd", True, 0, Nothing, Nothing)

            oGridN.doAddListField("U_Fuel", "Fuel Usage", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_RCost", "Running Cost Per Mile", True, 0, Nothing, Nothing)
            'oGridN.doAddListField("U_Tarre", "Tare", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_RCost", "Running Cost Per Mile", True, 0, Nothing, Nothing)

            oGridN.doAddListField("U_ServInt", "Service Interval (per mile)", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_LServDt", "Last Service Date", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ATrvDt", "Actual Mileage Date", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_LServMl", "Last Service Mileage", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_EAnnMl", "Estimated Annual Mileage", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ATrv", "Actual Mileage", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_FCWages", "FC: Wages (pa)", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_FCDepr", "FC: Deprecation (pa)", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_FCVLic", "FC: Vehicle Licence (pa)", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_FCVIns", "FC: Vehicle Insurance (pa)", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_FCVInt", "FC: Interest on Capital (pa)", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_VCTyre", "VC: Tyres Cost (per mile)", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_VCMain", "VC: Repairs/Maint. (per mile)", True, 0, Nothing, Nothing)

            oGridN.doAddListField("U_ItemCd", "Container Code", True, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("U_ItemDsc", "Container Desc.", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_WasCd", "Waste Code", True, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("U_WasDsc", "Waste Description", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_MarkAc", "Marketing Doc Action", True, -1, "COMBOBOX", Nothing)

            oGridN.doAddListField("U_WFStat", "WorkFlow Status", True, -1, "COMBOBOX", Nothing)

            oGridN.doAddListField("U_WR1ORD", "WR1 Order Type", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_IDHVMAN1", "Analysis 1", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHVMAN2", "Analysis 2", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHVMAN3", "Analysis 3", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHVMAN4", "Analysis 4", True, -1, Nothing, Nothing)

            oGridN.doAddListField("U_DayCap", "Daily Capacity", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ROUTE", "Default Route Code", True, -1, Nothing, Nothing)

            oGridN.doAddListField("U_Origin", "Origin", True, -1, "SRC*IDHEAORSR(ORI)[IDH_ORCODE=#U_Origin][IDH_ORCODE]", Nothing)

        End Sub

		'## Start 03-10-2013
        Private Function doCheckRecordExists(ByVal oForm As SAPbouiCOM.Form, ByVal sLorryCode As String) As Boolean
            Dim oRecSet As SAPbobsCOM.Recordset
            oRecSet = CType(goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
            Dim sSQL As String = "Select (Select Count(1) as C from [@IDH_JOBSHD] Where U_LorryCd='" & sLorryCode.Trim & "')" _
                                    & " +(Select  Count(1) as C  from [@IDH_HAULCST] Where U_Lorry='" & sLorryCode.Trim & "') " _
                                    & " + (Select Count(1) as C from [@IDH_DISPROW] Where U_LorryCd='" & sLorryCode.Trim & "') As C"
            oRecSet.DoQuery(sSQL)
            If oRecSet.RecordCount = 0 OrElse CType(oRecSet.Fields.Item("C").Value, Integer) = 0 Then
                DataHandler.INSTANCE.doReleaseRecordset(oRecSet)
                Return True
            Else
                DataHandler.INSTANCE.doReleaseRecordset(oRecSet)
                Return False
            End If
        End Function
        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
                Dim oMenuEvt As SAPbouiCOM.MenuEvent = CType(pVal.oData, SAPbouiCOM.MenuEvent)
                If pVal.BeforeAction = False AndAlso oMenuEvt.MenuUID.Equals("IDHGMREML") OrElse oMenuEvt.MenuUID.Equals("IDHGMREMM") Then
                    Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                    Dim oRows As ArrayList = oGridN.doPrepareListFromSelection("Code")
                    If oRows IsNot Nothing Then
                        For i As Int32 = 0 To oRows.Count - 1
                            If doCheckRecordExists(oForm, Conversions.ToString(oRows(i))) = False Then
                                'PARENT.APPLICATION.StatusBar.SetText("Selected vehicle(s) cannot be deleted, it is in use.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                DataHandler.INSTANCE.doResUserError("Selected vehicle(s) cannot be deleted, it is in use.", "ERUVMVDL", Nothing)
                                Return False
                            End If
                        Next
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED And pVal.BeforeAction = False Then
                If pVal.ColUID = "U_SAddress" Then
                    Dim Address As String = CType(pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC("U_SAddress"), pVal.Row), String)
                    Dim sDispSite As String = CType(pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC("U_SCrdCd"), pVal.Row), String)
                    If sDispSite.Trim <> "" AndAlso Address IsNot Nothing AndAlso Address.Trim <> "" Then
                        Dim sAddressADDRSCD As String = Conversions.ToString(com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPShipToAddress(sDispSite, Address, "U_AddrsCd"))
                        pVal.oGrid.doSetFieldValue("U_SAddrLN", sAddressADDRSCD)
                    Else
                        pVal.oGrid.doSetFieldValue("U_SAddrLN", "")
                    End If
                ElseIf pVal.ColUID = "U_CAddress" Then
                    Dim Address As String = CType(pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC("U_CAddress"), pVal.Row), String)
                    Dim sCarrCD As String = CType(pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC("U_CCrdCd"), pVal.Row), String)
                    If sCarrCD.Trim <> "" AndAlso Address IsNot Nothing AndAlso Address.Trim <> "" Then
                        Dim sAddressAddrsCD As String = Conversions.ToString(com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPShipToAddress(sCarrCD, Address, "U_AddrsCd"))
                        pVal.oGrid.doSetFieldValue("U_CAddrLN", sAddressAddrsCD)
                    Else
                        pVal.oGrid.doSetFieldValue("U_CAddrLN", "")
                    End If
                End If
            End If
            Return MyBase.doCustomItemEvent(oForm, pVal)
        End Function
        '## End
        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        'Public Overrides Function doRightClickEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean) As Boolean

        '    'If pVal.BeforeAction = False AndAlso pVal.EventType = SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK Then
        '    '    PARENT.APPLICATION.StatusBar.SetText("Deltee")
        '    'End If
        '    ' Return MyBase.doRightClickEvent(oForm, pVal, BubbleEvent)

        'End Function
        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            ''MA Start 06-02-2015'' Applied validation in DB Object also, but this will highlight the cell and from DB Object I think it will not possible
            If com.idh.bridge.lookups.Config.ParameterAsBool("VALTRWGT") AndAlso pVal.BeforeAction = True AndAlso _
                (oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE) Then
                Dim oUpdateGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                Dim aRows As ArrayList = oUpdateGrid.doGetChangedRows()
                If Not aRows Is Nothing Then
                    'Dim aChangedFields As ArrayList
                    Dim sVal As String = ""
                    Dim currentexpirydate As Date
                    Dim iValidityMonths As Int32 = com.idh.bridge.lookups.Config.ParameterAsInt("TARWTEXP")
                    For iIndex As Integer = 0 To aRows.Count - 1
                        ' oUpdateGrid.setCurrentDataRowIndex(()
                        Dim iRow As Integer = CType(aRows(iIndex), Integer)
                        If oUpdateGrid.doGetFieldValue("U_VehReg", iRow).ToString.Trim <> "" Then
                            If Conversions.ToDouble(oUpdateGrid.doGetFieldValue("U_Tarre", iRow)) <> 0 Then
                                sVal = Conversions.ToString(oUpdateGrid.doGetFieldValue("U_TRWTE1", iRow))
                                If String.IsNullOrEmpty(sVal) Then
                                    oUpdateGrid.doSetCellColor(iRow + 1, oUpdateGrid.doFieldIndex("U_TRWTE1") + 1, 254)
                                    BubbleEvent = False
                                Else
                                    currentexpirydate = com.idh.utils.Dates.doStrToDate(sVal)
                                    If currentexpirydate.AddMonths(iValidityMonths) < Today Then
                                        oUpdateGrid.doSetCellColor(iRow + 1, oUpdateGrid.doFieldIndex("U_TRWTE1") + 1, 254)
                                        BubbleEvent = False
                                    End If
                                End If
                            End If
                            If Conversions.ToDouble(oUpdateGrid.doGetFieldValue("U_TRLTar", iRow)) <> 0 Then
                                sVal = Conversions.ToString(oUpdateGrid.doGetFieldValue("U_TRWTE2", iRow))
                                If String.IsNullOrEmpty(sVal) Then
                                    oUpdateGrid.doSetCellColor(iRow + 1, oUpdateGrid.doFieldIndex("U_TRWTE2") + 1, 254)
                                    BubbleEvent = False
                                Else
                                    currentexpirydate = com.idh.utils.Dates.doStrToDate(sVal)
                                    If currentexpirydate.AddMonths(iValidityMonths) < Today Then
                                        oUpdateGrid.doSetCellColor(iRow + 1, oUpdateGrid.doFieldIndex("U_TRWTE2") + 1, 254)
                                        BubbleEvent = False
                                    End If
                                End If
                            End If
                        End If
                    Next
                    If BubbleEvent = False Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Tare weight is expired, Please update the tare weight of vehicle.")
                        DataHandler.INSTANCE.doResSystemError("Tare weight is expired, Please update the tare weight of vehicle.", "ERSYTWEU", Nothing)
                        Exit Sub
                    End If
                End If
            End If
            ''MA End 06-02-2015
            MyBase.doButtonID1(oForm, pVal, BubbleEvent)
        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "LINESGRID" Then
                        Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                        Dim iCurRow As Integer = oGridN.getGrid().GetDataTableRowIndex(pVal.Row)
                        oGridN.setCurrentDataRowIndex(iCurRow)
                        If oGridN.doCheckIsSameCol(pVal.ColUID, "U_VehReg") Then
                            If pVal.ItemChanged Then
                                Dim sValue As String
                                sValue = CType(oGridN.doGetFieldValue("U_VehReg"), String)
                                sValue = com.idh.bridge.utils.General.doFixReg(sValue)
                                oGridN.doSetFieldValue("U_VehReg", sValue)
                            End If
                        ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "U_Driver") Then 'OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
                            Dim sValue As String = CType(oGridN.doGetFieldValue("U_Driver"), String)
                            If sValue.Length() = 0 OrElse sValue.IndexOf("*") > -1 OrElse pVal.ItemChanged Then
                                doChooseDriver(oForm, oGridN)
                            End If
                        ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "U_CCCrdCd") Then
                            Dim sValue As String = CType(oGridN.doGetFieldValue("U_CCCrdCd"), String)
                            If sValue.Length() = 0 OrElse sValue.IndexOf("*") > -1 OrElse pVal.ItemChanged Then
                                doChooseCarrierCustomer(oForm, oGridN)
                            End If
                        ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "U_CSCrdCd") Then
                            Dim sValue As String = CType(oGridN.doGetFieldValue("U_CSCrdCd"), String) 'Waste Carrier Code (Supplier)
                            If sValue.Length() = 0 OrElse sValue.IndexOf("*") > -1 OrElse pVal.ItemChanged Then
                                'doChooseCarrierSupplier(oForm, oGridN)
                                doChooseCarrierSupplier(oForm, True, CType(oGridN.doGetFieldValue("U_CSCrdCd"), String), CType(oGridN.doGetFieldValue("U_CSName"), String))
                            End If
                        ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "U_CCrdCd") Then
                            Dim sValue As String = CType(oGridN.doGetFieldValue("U_CCrdCd"), String)
                            If sValue.Length() = 0 OrElse sValue.IndexOf("*") > -1 OrElse pVal.ItemChanged Then
                                doChooseCustomer(oForm, oGridN)
                            End If
                        ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "U_SCrdCd") Then
                            Dim sValue As String = CType(oGridN.doGetFieldValue("U_SCrdCd"), String)
                            If sValue.Length() = 0 OrElse sValue.IndexOf("*") > -1 OrElse pVal.ItemChanged Then
                                doChooseCarrierSupplier(oForm, oGridN)
                            End If
                            '## Start
                        ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "U_WasCd") Then
                            'ElseIf pVal.ItemUID = "IDH_WASTCD" Then
                            Dim sWasteCode As String = CType(oGridN.doGetFieldValue("U_WasCd"), String) 'getUFValue("IDH_WASTCD")
                            If sWasteCode.Length = 0 OrElse sWasteCode.IndexOf("*") > -1 OrElse pVal.ItemChanged Then
                                doWasteCodeLookup(oForm, oGridN) 'pVal, BubbleEvent)
                            End If
                            Return False
                        ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "U_ItemCd") Then 'Container Code Look up
                            Dim sContainerCode As String = CType(oGridN.doGetFieldValue("U_ItemCd"), String)
                            If sContainerCode.Length = 0 OrElse sContainerCode.IndexOf("*") > -1 OrElse pVal.ItemChanged Then
                                doContainerCodeLookup(oForm, oGridN)
                            End If
                            Return False
                            '## End
                        End If
                    ElseIf pVal.ItemUID = "IDH_CUST" OrElse pVal.ItemUID = "IDH_REG" OrElse pVal.ItemUID = "IDH_WF" Then
                        If pVal.ItemChanged Then
                            doCheckForFilter(oForm)
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_WF" Then
                        doCheckForFilter(oForm)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_CUST" OrElse pVal.ItemUID = "IDH_REG" OrElse pVal.ItemUID = "IDH_WF" Then
                        If pVal.CharPressed = 13 Then
                            goParent.goApplication.SendKeys("{TAB}")
                        End If
                    End If
                End If
            End If
            Return True
        End Function
        Protected Overridable Sub doChooseDriver(ByRef oForm As SAPbouiCOM.Form, ByRef oGridN As UpdateGrid)
            'OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
            setSharedData(oForm, "IDH_DRIVNM", "")
            setSharedData(oForm, "IDH_DRIVSN", "")
            setSharedData(oForm, "IDH_VEHREG", "")
            setSharedData(oForm, "SHOWCREATE", "TRUE")
            setSharedData(oForm, "SILENT", "SHOWMULTI")
            goParent.doOpenModalForm("IDHDRVSRC", oForm)
        End Sub
        Protected Overridable Sub doChooseCarrierSupplier(ByVal oForm As SAPbouiCOM.Form, ByVal oGridN As UpdateGrid)
            setSharedData(oForm, "TRG", "WCD")

            setSharedData(oForm, "IDH_BPCOD", oGridN.doGetFieldValue("U_SCrdCd"))
            setSharedData(oForm, "IDH_TYPE", "S") '## Start 18-09-2013 it was "F-S" 
            setSharedData(oForm, "IDH_GROUP", com.idh.bridge.lookups.Config.Parameter("BGSWCarr"))
            setSharedData(oForm, "IDH_NAME", oGridN.doGetFieldValue("U_SName"))
            setSharedData(oForm, "SILENT", "SHOWMULTI")
            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub
        '## Start 18-09-2013
        Protected Overridable Sub doChooseCarrierSupplier(ByVal oForm As SAPbouiCOM.Form, ByVal bCanDoSilent As Boolean, ByVal sCardCode As String, ByVal sCardName As String)
            'Not to open BP Search if form is in Find mode
            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                Return
            End If
            '## End
            setSharedData(oForm, "TRG", "WCS")
            'Dim sCardCode As String = getFormDFValue(oForm, "U_CCardCd", True)
            'Dim sCardName As String = getFormDFValue(oForm, "U_CCardNM", True)
            setSharedData(oForm, "IDH_TYPE", "S")
            setSharedData(oForm, "IDH_GROUP", com.idh.bridge.lookups.Config.Parameter("BGSWCarr"))
            setSharedData(oForm, "SILENT", "SHOWMULTI")
            'If sCardCode.Length > 0 OrElse _
            ' sCardName.Length > 0 OrElse _
            ' com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool(msOrderType & "BPFLT", True) = False Then
            '    setSharedData(oForm, "IDH_TYPE", "")
            '    setSharedData(oForm, "IDH_GROUP", "")
            'Else
            '    setSharedData(oForm, "IDH_TYPE", "S")
            '    setSharedData(oForm, "IDH_GROUP", com.idh.bridge.lookups.Config.Parameter("BGSWCarr"))
            'End If

            setSharedData(oForm, "IDH_BPCOD", sCardCode)
            setSharedData(oForm, "IDH_NAME", sCardName)

            If bCanDoSilent = True Then
                setSharedData(oForm, "SILENT", "SHOWMULTI")
            End If
            If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
                goParent.doOpenModalForm("IDHCSRCH", oForm) 'Actual BP Search Form 
            Else
                If (getSharedData(oForm, "IDH_BPSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_BPSRCHOPEND").ToString.Trim = "") Then
                    goParent.doOpenModalForm("IDHNCSRCH", oForm) 'NEW BP Search Form 
                End If
            End If
        End Sub
        '## End
        Protected Overridable Sub doChooseCarrierCustomer(ByVal oForm As SAPbouiCOM.Form, ByVal oGridN As UpdateGrid)
            setSharedData(oForm, "TRG", "WCC")
            setSharedData(oForm, "IDH_BPCOD", oGridN.doGetFieldValue("U_CCCrdCd"))
            setSharedData(oForm, "IDH_NAME", oGridN.doGetFieldValue("U_CCName"))
            setSharedData(oForm, "IDH_TYPE", "F-C")
            setSharedData(oForm, "SILENT", "SHOWMULTI")
            '## Start 30-09-2013
            'goParent.doOpenModalForm("IDHCSRCH", oForm)
            If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("UNEWSRCH", False) = False Then
                goParent.doOpenModalForm("IDHCSRCH", oForm) 'Actual BP Search Form 
            Else
                If (getSharedData(oForm, "IDH_BPSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_BPSRCHOPEND").ToString.Trim = "") Then
                    goParent.doOpenModalForm("IDHNCSRCH", oForm) 'NEW BP Search Form 
                End If
            End If
            '## End

        End Sub

        Protected Overridable Sub doChooseCustomer(ByVal oForm As SAPbouiCOM.Form, ByVal oGridN As UpdateGrid)
            setSharedData(oForm, "TRG", "CUS")
            setSharedData(oForm, "IDH_BPCOD", oGridN.doGetFieldValue("U_CCrdCd"))
            setSharedData(oForm, "IDH_TYPE", "F-C")
            setSharedData(oForm, "IDH_NAME", oGridN.doGetFieldValue("U_CName"))
            setSharedData(oForm, "SILENT", "SHOWMULTI")
            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub
        '## Start 18-09-2013
        Public Sub doContainerCodeLookup(ByVal oForm As SAPbouiCOM.Form, ByVal oGridN As UpdateGrid)
            setSharedData(oForm, "TRG", "CCD")
            setSharedData(oForm, "IDH_ITMCOD", oGridN.doGetFieldValue("U_ItemCd"))
            setSharedData(oForm, "IDH_ITMNAM", oGridN.doGetFieldValue("U_ItemDsc"))
            setSharedData(oForm, "IDH_GRPCOD", com.idh.bridge.lookups.Config.INSTANCE.getParameterWithDefault("WODCOG", ""))
            setSharedData(oForm, "SILENT", "SHOWMULTI")
            goParent.doOpenModalForm("IDHISRC", oForm)
        End Sub
        '## End
        Public Sub doWasteCodeLookup(ByVal oForm As SAPbouiCOM.Form, ByVal oGridN As UpdateGrid)
            'If pVal.Before_Action = False Then
            Dim sWasteCode As String = CType(oGridN.doGetFieldValue("U_WasCd"), String)
            setUFValue(oForm, "IDH_WASTNM", "")

            setSharedData(oForm, "TRG", "WC")
            setSharedData(oForm, "IDH_GRPCOD", com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup())
            setSharedData(oForm, "IDH_ITMCOD", sWasteCode)
            setSharedData(oForm, "IDH_ITMNAM", "")
            setSharedData(oForm, "IDH_INVENT", "N")
            setSharedData(oForm, "SILENT", "SHOWMULTI")
            'If getSharedData(oForm, "IDH_ITMSRCHOPEND") Is Nothing OrElse getSharedData(oForm, "IDH_ITMSRCHOPEND").ToString.Trim = "" Then
            goParent.doOpenModalForm("IDHWISRC", oForm)
            'End If

            'setSharedData(oForm, "TRG", "CUS")
            'setSharedData(oForm, "IDH_BPCOD", oGridN.doGetFieldValue("U_CCrdCd"))
            'setSharedData(oForm, "IDH_TYPE", "F-C")
            'setSharedData(oForm, "IDH_NAME", oGridN.doGetFieldValue("U_CName"))
            'goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType = "IDHCSRCH" Or sModalFormType = "IDHNCSRCH" Then
                    Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    Dim sTarget As String = getSharedDataAsString(oForm, "TRG")
                    Dim valCd As String = getSharedDataAsString(oForm, "CARDCODE")
                    Dim valNm As String = getSharedDataAsString(oForm, "CARDNAME")

                    If Not (valCd Is Nothing) AndAlso valCd.Length > 0 Then
                        '## Start 18-09-2013
                        If sTarget = "WCD" Then
                            oGridN.doSetFieldValue("U_SCrdCd", valCd)
                            oGridN.doSetFieldValue("U_SName", valNm)
                            oGridN.doSetFieldValue("U_SAddress", getSharedData(oForm, "ADDRESS"))
                            Dim Address As String = getSharedDataAsString(oForm, "ADDRESS")
                            If valCd.Trim <> "" AndAlso Address IsNot Nothing AndAlso Address.Trim <> "" Then
                                Dim sAddressAddrsCd As String = Conversions.ToString(com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPBIllToAddress(valCd, Address, "U_AddrsCd"))
                                oGridN.doSetFieldValue("U_SAddrLN", sAddressAddrsCd)
                            Else
                                oGridN.doSetFieldValue("U_SAddrLN", "")
                            End If
                        ElseIf sTarget = "WCS" Then
                            oGridN.doSetFieldValue("U_CSCrdCd", valCd)
                            oGridN.doSetFieldValue("U_CSName", valNm)
                            '## End
                        ElseIf sTarget = "WCC" Then
                            oGridN.doSetFieldValue("U_CCCrdCd", valCd)
                            oGridN.doSetFieldValue("U_CCName", valNm)
                        ElseIf sTarget = "CUS" Then
                            oGridN.doSetFieldValue("U_CCrdCd", valCd)
                            oGridN.doSetFieldValue("U_CName", valNm)
                            oGridN.doSetFieldValue("U_CAddress", getSharedData(oForm, "ADDRESS"))
                            Dim Address As String = getSharedDataAsString(oForm, "ADDRESS")
                            If valCd.Trim <> "" AndAlso Address IsNot Nothing AndAlso Address.Trim <> "" Then
                                Dim sAddressAddrsCd As String = Conversions.ToString(com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPBIllToAddress(valCd, Address, "U_AddrsCd"))
                                oGridN.doSetFieldValue("U_CAddrLN", sAddressAddrsCd)
                            Else
                                oGridN.doSetFieldValue("U_CAddrLN", "")
                            End If
                        End If
                        doSetUpdate(oForm)
                    End If
                End If
                '##Start 05-09-2013
                If sModalFormType = "IDHWISRC" Then
                    Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    Dim sTarget As String = getSharedDataAsString(oForm, "TRG")
                    Dim valItemCode As String = getSharedDataAsString(oForm, "ITEMCODE")
                    Dim valItemName As String = getSharedDataAsString(oForm, "ITEMNAME")
                    If (valItemCode IsNot Nothing) AndAlso valItemName.Length > 0 Then
                        If sTarget = "WC" Then
                            oGridN.doSetFieldValue("U_WasCd", valItemCode)
                            oGridN.doSetFieldValue("U_WasDsc", valItemName)
                        End If
                        doSetUpdate(oForm)
                    End If
                ElseIf sModalFormType = "IDHISRC" Then
                    Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    Dim sTarget As String = getSharedDataAsString(oForm, "TRG")
                    Dim valItemCode As String = getSharedDataAsString(oForm, "ITEMCODE")
                    Dim valItemName As String = getSharedDataAsString(oForm, "ITEMNAME")
                    If sTarget = "CCD" Then 'Container Code
                        oGridN.doSetFieldValue("U_ItemCd", valItemCode)
                        oGridN.doSetFieldValue("U_ItemDsc", valItemName)
                    End If
                End If
                '## End

                'OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
                'Joachim Alleritz
                If sModalFormType = "IDHDRVSRC" Then
                    Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    Dim sDriver As String = getSharedDataAsString(oForm, "IDH_DRIVNM") & " " & getSharedDataAsString(oForm, "IDH_DRIVSN")
                    If sDriver.Length > 0 Then
                        oGridN.doSetFieldValue("U_Driver", sDriver)
                        doSetUpdate(oForm)
                    End If
                End If
                'End OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal Result - " & sModalFormType)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub

        Private Sub doCheckForFilter(ByVal oForm As SAPbouiCOM.Form)
            If (oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE) Then
                'If goParent.goApplication.M_essageBox("Do you want to commit the changes.", 1, "Yes", "No") = 1 Then
                If Messages.INSTANCE.doResourceMessageYN("GEDCOMT", Nothing, 1) = 1 Then
                    Dim oUpdateGrid As UpdateGrid
                    oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    If oUpdateGrid.doProcessData() = True Then
                        doLoadData(oForm)
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                    End If
                End If
            Else
                doLoadData(oForm)
            End If
        End Sub

        'Shared Function doFixReg(ByVal sReg As String) As String
        '    If sReg Is Nothing OrElse sReg.Length = 0 Then
        '        Return ""
        '    End If

        '    Dim sWrkVal As String = ""
        '    For iIndex As Integer = 0 To sReg.Length - 1
        '        If Not sReg.Chars(iIndex) = " " Then
        '            sWrkVal = sWrkVal & sReg.Chars(iIndex)
        '        End If
        '    Next
        '    sWrkVal = sWrkVal.ToUpper()
        '    Return sWrkVal
        'End Function

    End Class
End Namespace
