Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge

Namespace idh.forms.admin
    Public Class CCDetails
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHCCDT", sParMenu, iMenuPosition, "CCAdmin.srf", "CreditCard Details")
            msKeyGen = "CCSEQ"
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "CreditCard Details"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_CCDTL"
        End Function

        Protected Overrides Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            Dim sCardCode As String = getParentSharedDataAsString(oGridN.getSBOForm, "IDH_CUSTCD")
            oGridN.doAddFilterField("IDH_CUSTCD", "U_CardCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, sCardCode)
        End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_CardCd", "Customer", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_CCType", "Card Type", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_CCNum", "Card number", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_CCExp", "Card Expiry", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_CCAvDe", "Card Delay", True, -1, Nothing, Nothing)
            
            oGridN.doAddListField("U_IDHCCIs", "CCard Issue #", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHCCSec", "CCard Security Number", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHCCHNm", "CCard House Number", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHCCPCd", "CCard PostCode", True, -1, Nothing, Nothing)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
            doFillTypeCombo(oForm)
        End Sub

        Public Sub doFillTypeCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_CCType")), SAPbouiCOM.ComboBoxColumn)
            Catch ex As Exception
                Exit Sub
            End Try
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)

            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDB.doSelectQuery("SELECT CreditCard, CardName FROM OCRC")
                While Not oRecordSet.EoF
                    oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(1).Value, String))
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the CC Type combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("CC Type")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_ADD_EMPTY Then
                If pVal.BeforeAction = False Then
                    Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                    Dim sCustomer As String
                    sCustomer = CType(getUFValue(oForm, "IDH_CUSTCD"), String)
                    oGridN.doSetFieldValue("U_CardCd", pVal.Row, sCustomer)
                    oGridN.doSetFieldValue("U_CCType", pVal.Row, "1")
                End If
            End If
            Return MyBase.doCustomItemEvent(oForm, pVal)
        End Function
    End Class
End Namespace
