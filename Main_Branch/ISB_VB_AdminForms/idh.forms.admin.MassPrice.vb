Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.utils.Conversions
Imports com.idh.bridge
Imports com.idh.dbObjects.User
Imports com.idh.dbObjects.numbers

Imports com.idh.bridge.resources
Imports SAPbouiCOM

Namespace idh.forms.admin
    Public Class MassPrice
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHMPrice", sParMenu, iMenuPosition, "MassPrcUp.srf", "Mass Price Update (CIP)")
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Mass Price Update (CIP)"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_CSITPR"
        End Function

        Protected Overrides Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddFilterField("IDH_CUST", "U_CustCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_JOBTP", "U_JobTp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_CONT", "U_ItemCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_STADDR", "U_StAddr", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            '##MA Start 12-04-2017 issue# 378 (Change both fields Length Limit 255 into 10 limit)
            oGridN.doAddFilterField("IDH_START", "U_StDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_FINISH", "U_EnDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            '##MA End 12-04-2017
            oGridN.doAddFilterField("IDH_WSCD", "U_WasteCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WR1O", "U_WR1ORD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
        	oGridN.doAddFilterField("IDH_COVER", "(U_StDate < '[@]' And U_EnDate > '[@]')", SAPbouiCOM.BoDataType.dt_DATE, "REPLACE", 255)
            '#MM 1-16-2017 start
            oGridN.doAddFilterField("IDH_SUPLR", "U_BP2CD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            '#MM 1-16-2017
        End Sub

        Protected Sub doAddListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CustCd", "Customer Code", True, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("U_StAddr", "Site Address", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ZpCd", "Postal Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_JobTp", "Order Type", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_ItemCd", "Container Code", True, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("U_ItemDs", "Container Desc.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_WasteCd", "Waste Code", True, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("U_WasteDs", "Waste Desc.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_WR1ORD", "WR1 OType", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_StDate", "From Date", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_EnDate", "To Date", True, -1, Nothing, Nothing)
            'oGridN.doAddListField("U_ItmPLs","", True, -1, Nothing, Nothing)
            'oGridN.doAddListField("U_ItemPr","", True, -1, Nothing, Nothing)
            'oGridN.doAddListField("U_ItemTPr","", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_UOM", "UOM", False, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_FTon", "From Weight", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_TTon", "To Weight", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ChrgCal", "Tip Calc Type", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField(IDH_CSITPR._HaulCal, "Haul Calc Type", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_TipTon", "Tip Price", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Haulge", "Haul Price", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_FrmBsWe", "Weight Base From", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ToBsWe", "Weight Base To", False, -1, Nothing, Nothing)

            '#MM 1-16-2017 start
            oGridN.doAddListField("U_BP2CD", "Supplier", False, -1, Nothing, Nothing)
            '#MM 1-16-2017
        End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            'Protected Overridable Sub doTheGridLayout(ByVal oGridN As UpdateGrid)
            If com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", False) Then
                Dim sFormTypeId As String = oGridN.getSBOForm().TypeEx
                Dim oFormSettings As IDH_FORMSET = getWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId)
                If oFormSettings Is Nothing Then
                    oFormSettings = New IDH_FORMSET()
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, "")
                    setWFValue("FRMSET", sFormTypeId & "." & oGridN.GridId, oFormSettings)

                    oFormSettings.doAddFieldToGrid(oGridN)

                    doAddListFields(oGridN)

                    'If oFormSettings.SkipFormSettings = False Then
                    '    oFormSettings.doSetGrid(oGridN)
                    'End If
                    oFormSettings.doSyncDB(oGridN)
                Else
                    If oFormSettings.SkipFormSettings Then
                        doAddListFields(oGridN)
                    Else
                        oFormSettings.doAddFieldToGrid(oGridN)
                        'oFormSettings.doSetGrid(oGridN)
                    End If
                End If
            Else
                doAddListFields(oGridN)
            End If
        End Sub
        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
            Try
                doAddUF(oForm, "IDH_NTIP", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
                doAddUF(oForm, "IDH_NHAUL", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
                doAddUF(oForm, "IDH_NWC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
                doAddUF(oForm, "IDH_NWCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50, False, False)
                doAddUF(oForm, "IDH_NCONT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, False, False)
                doAddUF(oForm, "IDH_NCONTD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50, False, False)

                doAddUF(oForm, "IDH_NFDATE", SAPbouiCOM.BoDataType.dt_DATE, 30, False, False)
                doAddUF(oForm, "IDH_NTDATE", SAPbouiCOM.BoDataType.dt_DATE, 30, False, False)

                doAddUFCheck(oForm, "IDH_ANYEAR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
                '#MM 1-16-2017 start
                doAddUF(oForm, "IDH_SUPLR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50, False, False)
                '#MM 1-16-2017 END
                '##MA START 20-04-2017 issue#378
                doAddUFCheck(oForm, "IDH_EDCHCK", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
                doAddUFCheck(oForm, "IDH_SDCHCK", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
                '##MA END 20-04-2017

                oForm.SupportedModes = SAPbouiCOM.BoFormMode.fm_FIND_MODE Or SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
            End Try
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
            '#MA start 21-04-2017 issue#378
            oForm.Freeze(True)      'MA  SET FORM FREEZE DUE TO RELOAD GRID 
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            Dim sFilter As String = oGridN.getRequiredFilter()
            sFilter = If(getUFValue(oForm, "IDH_EDCHCK").ToString.Length > 0 AndAlso getUFValue(oForm, "IDH_EDCHCK").Equals("Y"), sFilter + " U_EnDate IN('" + com.idh.utils.Dates.doSBODateToSQLDate(getUFValue(oForm, "IDH_FINISH").ToString) + "')", sFilter + "")
            sFilter = If(getUFValue(oForm, "IDH_SDCHCK").ToString.Length > 0 AndAlso getUFValue(oForm, "IDH_SDCHCK").Equals("Y") AndAlso getUFValue(oForm, "IDH_EDCHCK").ToString.Length > 0 AndAlso getUFValue(oForm, "IDH_EDCHCK").Equals("Y"), sFilter + "AND U_StDate IN('" + com.idh.utils.Dates.doSBODateToSQLDate(getUFValue(oForm, "IDH_START").ToString) + "')", If(getUFValue(oForm, "IDH_SDCHCK").ToString.Length > 0 AndAlso getUFValue(oForm, "IDH_SDCHCK").Equals("Y"), sFilter + "U_StDate IN('" + com.idh.utils.Dates.doSBODateToSQLDate(getUFValue(oForm, "IDH_START").ToString) + "')", sFilter + ""))
            oGridN.setRequiredFilter(sFilter)
            oGridN.doReloadData()
            oGridN.setRequiredFilter(String.Empty)
            oForm.Freeze(False)
            '#MA END 21-04-2017
            doLoadCombos(oForm)
            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)
        End Sub

        '** Have to use this because of the fact that no Browse By is being used
        Protected Sub doSetMode(ByVal oForm As SAPbouiCOM.Form, ByVal iMode As SAPbouiCOM.BoFormMode)
            If iMode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                CType(oForm.Items.Item("1").Specific, SAPbouiCOM.Button).Caption = Translation.getTranslatedWord("Find")
                'oForm.Items.Item("2").Visible = False
            ElseIf iMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                CType(oForm.Items.Item("1").Specific, SAPbouiCOM.Button).Caption = Translation.getTranslatedWord("Update")
                'oForm.Items.Item("2").Visible = True
                doSetUpdate(oForm)
            End If
        End Sub

        '** Have to use this because of the fact that no Browse By is being used
        Protected Function doGetMode(ByVal oForm As SAPbouiCOM.Form) As SAPbouiCOM.BoFormMode
            Dim sCaption As String = CType(oForm.Items.Item("1").Specific, SAPbouiCOM.Button).Caption
            If sCaption.Equals(Translation.getTranslatedWord("Update")) Then
                Return SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            ElseIf sCaption.Equals(Translation.getTranslatedWord("Find")) Then
                Return SAPbouiCOM.BoFormMode.fm_FIND_MODE
            Else
                Return SAPbouiCOM.BoFormMode.fm_OK_MODE
            End If
        End Function

        Protected Sub doLoadCombos(ByVal oForm As SAPbouiCOM.Form)
            doJobTypeCombo(oForm)
            doWR1OrdTypeCombo(oForm)
            doUOMCombo(oForm)
            doCalcTypeCombo(oForm)
            doHaulCalcTypeCombo(oForm)
        End Sub

        Private Sub doCalcTypeCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Dim oValidValues As SAPbouiCOM.ValidValues
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_ChrgCal")), SAPbouiCOM.ComboBoxColumn)
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
                oValidValues = oCombo.ValidValues

                doClearValidValues(oValidValues)
                oValidValues.Add("VARIABLE", "VARIABLE")
                oValidValues.Add("FIXED", "FIXED")
                oValidValues.Add("OFFSET", "OFFSET")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Calculation type Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Calculation Type")})
            End Try
        End Sub

        Private Sub doHaulCalcTypeCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Dim oValidValues As SAPbouiCOM.ValidValues
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC(IDH_CSITPR._HaulCal)), SAPbouiCOM.ComboBoxColumn)
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
                oValidValues = oCombo.ValidValues

                doClearValidValues(oValidValues)
                oValidValues.Add("STANDARD", "STANDARD")
                oValidValues.Add("WEIGHT", "Use Weight")
                oValidValues.Add("QTY1", "Use Quantity 1")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Haulage Calculation type Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Calculation Type")})
            End Try
        End Sub

        Private Sub doWR1OrdTypeCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Dim oValidValues As SAPbouiCOM.ValidValues
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_WR1ORD")), SAPbouiCOM.ComboBoxColumn)
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
                oValidValues = oCombo.ValidValues

                doClearValidValues(oValidValues)
                oValidValues.Add("", getTranslatedWord("Any"))
                oValidValues.Add("DO", getTranslatedWord("Disposal Order"))
                oValidValues.Add("WO", getTranslatedWord("Waste Order"))
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Order Type Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Order Type")})
            End Try
        End Sub

        Private Sub doUOMCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Dim oValidValues As SAPbouiCOM.ValidValues
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_UOM")), SAPbouiCOM.ComboBoxColumn)
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
                oValidValues = oCombo.ValidValues

                doClearValidValues(oValidValues)

                Dim sQry As String
                sQry = "select UnitDisply, UnitName from OWGT"

                oRecordSet = goParent.goDB.doSelectQuery(sQry)
                If oRecordSet.RecordCount > 0 Then
                    Dim iCount As Integer
                    Dim sKey As String
                    Dim sValue As String
                    For iCount = 0 To oRecordSet.RecordCount - 1
                        sKey = CType(oRecordSet.Fields.Item(0).Value, String)
                        sValue = CType(oRecordSet.Fields.Item(1).Value, String)
                        Try
                            oValidValues.Add(sKey, sValue)
                        Catch ex As Exception
                        End Try
                        oRecordSet.MoveNext()
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the UOM Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("UOM")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

        Private Sub doJobTypeCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Dim oValidValues As SAPbouiCOM.ValidValues
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_JobTp")), SAPbouiCOM.ComboBoxColumn)
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
                oValidValues = oCombo.ValidValues

                doClearValidValues(oValidValues)
                oValidValues.Add("", getTranslatedWord("Any"))

                Dim sQry As String
                sQry = "select DISTINCT U_JobTp from [@IDH_JOBTYPE]"

                oRecordSet = goParent.goDB.doSelectQuery(sQry)
                If oRecordSet.RecordCount > 0 Then
                    Dim iCount As Integer
                    Dim sKey As String
                    Dim sValue As String
                    For iCount = 0 To oRecordSet.RecordCount - 1
                        sKey = CType(oRecordSet.Fields.Item(0).Value, String)
                        sValue = CType(oRecordSet.Fields.Item(0).Value, String)
                        Try
                            oValidValues.Add(sKey, sValue)
                        Catch ex As Exception
                        End Try
                        oRecordSet.MoveNext()
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Job Type Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Job Type")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                If doGetMode(oForm) = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    MyBase.doButtonID1(oForm, pVal, BubbleEvent)
                    doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)
                ElseIf doGetMode(oForm) = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    doLoadData(oForm)
                    BubbleEvent = False
                End If
            End If
        End Sub

        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SORT Then
                If pVal.BeforeAction = False Then
                    doLoadCombos(oForm)
                End If
            End If
            Return True
        End Function

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim bReturn As Boolean = MyBase.doItemEvent(oForm, pVal, BubbleEvent)

            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_APPLY" Then
                        Dim sDupVal As String = CType(getUFValue(oForm, "IDH_ANYEAR"), String)
                        If sDupVal = "Y" Then
                            doDuplicateRows(oForm)
                        Else
                            doSetValues(oForm)
                        End If
                        '#MA Start 09-05-2017 Issue#378
                    ElseIf pVal.ItemUID = "IDH_EDCHCK" Or pVal.ItemUID = "IDH_SDCHCK"
                        doLoadData(oForm)
                        'End 09-05-2017
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If pVal.BeforeAction = False Then
                    If pVal.CharPressed = 9 Then
                        If pVal.ItemUID = "IDH_NWC" Then
                            'Dim sDesc As String = getItemValue(oForm, "IDH_WASMAT")
                            Dim sGrp As String = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup()
                            Dim sItem As String = CType(getUFValue(oForm, "IDH_NWC"), String)

                            'If doNeedSearch(sDesc) Then
                            setSharedData(oForm, "TP", "WAST")
                            setSharedData(oForm, "IDH_ITMCOD", sItem)
                            setSharedData(oForm, "IDH_GRPCOD", sGrp)
                            'setSharedData(oForm, "IDH_CRDCD", getDFValue(oForm, "@IDH_JOBSHD", "U_CustCd"))
                            'setSharedData(oForm, "IDH_ITMNAM", sDesc)
                            setSharedData(oForm, "IDH_INVENT", "N")
                            setSharedData(oForm, "SILENT", "SHOWMULTI")
                            goParent.doOpenModalForm("IDHWISRC", oForm)
                            'End If
                        ElseIf pVal.ItemUID = "IDH_NCONT" Then
                            Dim sItem As String = CType(getUFValue(oForm, "IDH_NCONT"), String)
                            'Dim sGrp As String
                            'With oForm.DataSources.DBDataSources.Item("@IDH_JOBSHD")
                            '    sItem = .GetValue("U_ItemCd", .Offset).Trim()
                            '    sGrp = .GetValue("U_ItmGrp", .Offset).Trim()
                            'End With

                            setSharedData(oForm, "TRG", "PROD")
                            setSharedData(oForm, "IDH_ITMCOD", sItem)
                            'setSharedData(oForm, "IDH_GRPCOD", sGrp)
                            setSharedData(oForm, "SILENT", "SHOWMULTI")
                            goParent.doOpenModalForm("IDHISRC", oForm)
                        End If
                    End If
                    'OnTime Ticket 23218 - Form Mode turns to 'Update' for the first change only
                    'for ever second and onwards change the button remains 'Find'
                    If pVal.FormMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE And
                        CType(oForm.Items.Item("1").Specific, SAPbouiCOM.Button).Caption = Translation.getTranslatedWord("Find") And
                        pVal.ItemUID = "LINESGRID" Then
                        CType(oForm.Items.Item("1").Specific, SAPbouiCOM.Button).Caption = Translation.getTranslatedWord("Update")
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE '#MA 2017228 start
                If pVal.BeforeAction = False Then
                    'IDH_CUST,IDH_JOBTP,IDH_CONT,IDH_STADDR,IDH_START,IDH_FINISH,IDH_WSCD,IDH_WR1O,IDH_COVER,IDH_SUPLR
                    If pVal.ItemUID = "IDH_CUST" OrElse pVal.ItemUID = "IDH_JOBTP" OrElse pVal.ItemUID = "IDH_CONT" OrElse pVal.ItemUID = "IDH_STADDR" OrElse pVal.ItemUID = "IDH_START" OrElse pVal.ItemUID = "IDH_FINISH" OrElse pVal.ItemUID = "IDH_WSCD" OrElse pVal.ItemUID = "IDH_WR1O" OrElse pVal.ItemUID = "IDH_COVER" OrElse pVal.ItemUID = "IDH_SUPLR" Then
                        If pVal.ItemChanged Then
                            doCheckForFilter(oForm)
                        End If
                    End If

                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_CUST" OrElse pVal.ItemUID = "IDH_JOBTP" OrElse pVal.ItemUID = "IDH_CONT" OrElse pVal.ItemUID = "IDH_STADDR" OrElse pVal.ItemUID = "IDH_START" OrElse pVal.ItemUID = "IDH_FINISH" OrElse pVal.ItemUID = "IDH_WSCD" OrElse pVal.ItemUID = "IDH_WR1O" OrElse pVal.ItemUID = "IDH_COVER" OrElse pVal.ItemUID = "IDH_SUPLR" Then
                        If pVal.CharPressed = 13 Then
                            goParent.goApplication.SendKeys("{TAB}")
                        ElseIf pVal.CharPressed = 9 Then
                            doLoadData(oForm)
                        End If
                    End If
                End If

                '#MA 2017228 End

            End If
            Return bReturn
        End Function

        Public Function doCalcValue(ByVal sValIn As String, ByVal dCurValIn As Double) As Double
            Dim cWrkChar As Char = sValIn.Chars(0)
            Dim dValue As Double
            If cWrkChar < "0" OrElse cWrkChar > "9" Then
                sValIn = sValIn.Substring(1)

                dValue = ToDouble(sValIn)

                If cWrkChar = "+" Then
                    dValue = dCurValIn + dValue
                ElseIf cWrkChar = "-" Then
                    dValue = dCurValIn - dValue
                ElseIf cWrkChar = "*" Then
                    dValue = dCurValIn * dValue
                ElseIf cWrkChar = "/" Then
                    dValue = dCurValIn / dValue
                ElseIf cWrkChar = "%" Then
                    dValue = dCurValIn + (dCurValIn * dValue / 100)
                End If
            Else
                dValue = ToDouble(sValIn)
            End If
            Return dValue
        End Function

        Public Sub doSetValues(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            Try
                Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                Dim sVal As String
                Dim oSelected As SAPbouiCOM.SelectedRows
                Dim bValSet As Boolean = False
                oSelected = oGridN.getGrid().Rows.SelectedRows()

                If Not oSelected Is Nothing Then
                    Dim iRow As Integer
                    Dim bFound As Boolean = False
                    For iIndex As Integer = 0 To oSelected.Count - 1
                        'iRow = oSelected.Item(iIndex, SAPbouiCOM.BoOrderType.ot_RowOrder)
                        'iRow = oGridN.getGrid().GetDataTableRowIndex(iRow)
                        iRow = oGridN.GetDataTableRowIndex(oSelected, iIndex)

                        sVal = CType(getUFValue(oForm, "IDH_NTIP"), String)
                        If sVal.Length > 0 Then
                            Dim dVal As Double = doCalcValue(sVal, CType(oGridN.doGetFieldValue("U_TipTon", iRow), Double))
                            oGridN.doSetFieldValue("U_TipTon", iRow, dVal)
                            bValSet = True
                        End If

                        sVal = CType(getUFValue(oForm, "IDH_NHAUL"), String)
                        If sVal.Length > 0 Then
                            Dim dVal As Double = doCalcValue(sVal, CType(oGridN.doGetFieldValue("U_Haulge", iRow), Double))
                            oGridN.doSetFieldValue("U_Haulge", iRow, dVal)
                            bValSet = True
                        End If

                        sVal = CType(getUFValue(oForm, "IDH_NWC"), String)
                        If sVal.Length > 0 Then
                            oGridN.doSetFieldValue("U_WasteCd", iRow, sVal)
                            bValSet = True
                        End If

                        sVal = CType(getUFValue(oForm, "IDH_NWCD"), String)
                        If sVal.Length > 0 Then
                            oGridN.doSetFieldValue("U_WasteDs", iRow, sVal)
                            bValSet = True
                        End If

                        sVal = CType(getUFValue(oForm, "IDH_NCONT"), String)
                        If sVal.Length > 0 Then
                            oGridN.doSetFieldValue("U_ItemCd", iRow, sVal)
                            bValSet = True
                        End If

                        sVal = CType(getUFValue(oForm, "IDH_NCONTD"), String)
                        If sVal.Length > 0 Then
                            oGridN.doSetFieldValue("U_ItemDs", iRow, sVal)
                            bValSet = True
                        End If


                        sVal = CType(getUFValue(oForm, "IDH_NFDATE"), String)
                        If sVal.Length > 0 Then
                            oGridN.doSetFieldValue("U_StDate", iRow, com.idh.utils.Dates.doStrToDate(sVal))
                            bValSet = True
                        End If

                        sVal = CType(getUFValue(oForm, "IDH_NTDATE"), String)
                        If sVal.Length > 0 Then
                            oGridN.doSetFieldValue("U_EnDate", iRow, com.idh.utils.Dates.doStrToDate(sVal))
                            bValSet = True
                        End If
                    Next

                    If bValSet = True Then
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the values.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSV", {Nothing})
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        Private Sub doDuplicateRows(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            oForm.Freeze(True)
            Try
                Dim iLastRow As Integer = oGridN.getLastRowIndex()
                'Step through the selected rows and update
                Dim oSelected As SAPbouiCOM.SelectedRows
                'Dim iCode As Integer

                'Dim sCode As String
                Dim sVal As String

                oSelected = oGridN.getGrid().Rows.SelectedRows()
                If Not oSelected Is Nothing Then
                    oGridN.doAddRows(oSelected.Count)
                    Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode("CUSLP", oSelected.Count)
                    'iCode = 100000 + goParent.goDB.doNextNumber("CUSLP", oSelected.Count)
                    'iCode = DataHandler.INSTANCE.getNextNumber("CUSLP", oSelected.Count)
                    ''iCode = com.idh.utils.Conversions.ToString(goParent.goDB.doNextNumber("JOBSCHE", oSelected.Count))

                    'doDebug("Start Row Copy", 2)
                    DataHandler.INSTANCE.DebugTick2 = "Row Copy"

                    Dim iRow As Integer
                    Dim oDataTable As SAPbouiCOM.DataTable = oGridN.getSBOGrid.DataTable
                    Dim sLCode As String
                    Dim sLName As String
                    For iIndex As Integer = 0 To oSelected.Count - 1
                        Try
                            sLCode = oNumbers.doPackCodeCode(iIndex)
                            sLName = sLCode 'oNumbers.NameCode(iIndex)

                            iRow = oGridN.GetDataTableRowIndex(oSelected, iIndex)

                            oGridN.doCopyRow(iRow, iLastRow)
                            'sCode = DataHandler.doPackCode(Nothing, iCode)
                            oDataTable.SetValue(oGridN.doIndexField("Code"), iLastRow, sLCode)
                            oDataTable.SetValue(oGridN.doIndexField("Name"), iLastRow, sLName)

                            sVal = CType(getUFValue(oForm, "IDH_NTIP"), String)
                            If sVal.Length > 0 Then
                                Dim dVal As Double = doCalcValue(sVal, CType(oGridN.doGetFieldValue("U_TipTon", iRow), Double))
                                oDataTable.SetValue(oGridN.doIndexField("U_TipTon"), iLastRow, dVal)
                            End If

                            sVal = CType(getUFValue(oForm, "IDH_NHAUL"), String)
                            If sVal.Length > 0 Then
                                Dim dVal As Double = doCalcValue(sVal, CType(oGridN.doGetFieldValue("U_Haulge", iRow), Double))
                                oDataTable.SetValue(oGridN.doIndexField("U_Haulge"), iLastRow, dVal)
                            End If

                            sVal = CType(getUFValue(oForm, "IDH_NWC"), String)
                            If sVal.Length > 0 Then
                                oDataTable.SetValue(oGridN.doIndexField("U_WasteCd"), iLastRow, sVal)
                            End If

                            sVal = CType(getUFValue(oForm, "IDH_NWCD"), String)
                            If sVal.Length > 0 Then
                                oDataTable.SetValue(oGridN.doIndexField("U_WasteDs"), iLastRow, sVal)
                            End If

                            sVal = CType(getUFValue(oForm, "IDH_NCONT"), String)
                            If sVal.Length > 0 Then
                                oDataTable.SetValue(oGridN.doIndexField("U_ItemCd"), iLastRow, sVal)
                            End If

                            sVal = CType(getUFValue(oForm, "IDH_NCONTD"), String)
                            If sVal.Length > 0 Then
                                oDataTable.SetValue(oGridN.doIndexField("U_ItemDs"), iLastRow, sVal)
                            End If

                            sVal = CType(getUFValue(oForm, "IDH_NFDATE"), String)
                            If sVal.Length > 0 Then
                                oDataTable.SetValue(oGridN.doIndexField("U_StDate"), iLastRow, com.idh.utils.Dates.doStrToDate(sVal))
                            End If

                            sVal = CType(getUFValue(oForm, "IDH_NTDATE"), String)
                            If sVal.Length > 0 Then
                                oDataTable.SetValue(oGridN.doIndexField("U_EnDate"), iLastRow, com.idh.utils.Dates.doStrToDate(sVal))
                            End If

                            iLastRow = iLastRow + 1
                        Catch ex As Exception
                            'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error duplicating the rows.")
                            DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBD", {Nothing})
                        End Try
                        'iCode = iCode + 1
                    Next
                    'doDebug("Duplication Done", 2)
                    DataHandler.INSTANCE.DebugTick2 = "Duplication Done"
                End If

                If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error duplicating the rows.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBD", {Nothing})
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType = "IDHWISRC" Then
                    setUFValue(oForm, "IDH_NWC", getSharedData(oForm, "ITEMCODE"))
                    setUFValue(oForm, "IDH_NWCD", getSharedData(oForm, "ITEMNAME"))
                ElseIf sModalFormType = "IDHISRC" Then
                    setUFValue(oForm, "IDH_NCONT", getSharedData(oForm, "ITEMCODE"))
                    setUFValue(oForm, "IDH_NCONTD", getSharedData(oForm, "ITEMNAME"))
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal Results - " & sModalFormType)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub
        Private Sub doCheckForFilter(ByVal oForm As SAPbouiCOM.Form) 'Mubashar
            If (oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE) Then
                'If goParent.goApplication.M_essageBox("Do you want to commit the changes.", 1, "Yes", "No") = 1 Then
                If Messages.INSTANCE.doResourceMessageYN("GEDCOMT", Nothing, 1) = 1 Then
                    Dim oUpdateGrid As UpdateGrid
                    oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    If oUpdateGrid.doProcessData() = True Then
                        doLoadData(oForm)
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                    End If
                Else
                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                End If
            Else
                doLoadData(oForm)
            End If
        End Sub
    End Class
End Namespace
