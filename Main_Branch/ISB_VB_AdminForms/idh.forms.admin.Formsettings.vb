﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System
Imports com.idh.bridge.data

Imports com.idh.bridge
Imports com.idh.utils
Imports IDHAddOns.idh.events
Imports SAPbouiCOM

Namespace idh.forms.admin
    Public Class FormSettings
        Inherits idh.forms.admin.Tmpl

        Private msUserDept As String = "0"
        Private bIsSuperUser As Boolean = False
        Dim oIndexChangeList As New Dictionary(Of String, String) '#MA START 29-05-2017



        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHFRMSET", sParMenu, iMenuPosition, "Form Settings.srf", "Form Settings")

        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_FORMSET"
        End Function

        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            'goParent.goDB.doSelectQuery("If (Select Count(1) from [@IDH_FORMSET])=1211 Begin  Exec IDH_CopyDeptForAllUsers; Exec IDH_UpdateFormSettings; end ")
            'doe("Select 1; Exec IDH_CopyDeptForAllUsers; ")
            'doUpdateQuery("Select 1; Exec IDH_UpdateFormSettings")
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
            End If
            oGridN.AddEditLine = False

            doSetFilterFields(oGridN)
            doSetListFields(oGridN)

            oGridN.doAddGridTable(New GridTable(getTable(), Nothing, "Code", True), True)

            oGridN.doSetDoCount(True)
            oGridN.doSetHistory(getUserTable(), "Code")
            oGridN.setOrderValue("U_GridName ASC, U_Index Asc,U_Name Asc,Cast(Code as Int) ASC ")

            doFillCombos(oForm)
            '#MA Start 12-06-2017 Point#453 copy index from one user to another user
            oForm.Items.Item("IDH_CUSER").AffectsFormMode = False
            oForm.Mode = BoFormMode.fm_OK_MODE
            '#MA 12-06-2017 End 
        End Sub

        Private Sub doFillCombos(ByVal oForm As SAPbouiCOM.Form)
            bIsSuperUser = True
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            'oRecordSet = CType(goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)
            oRecordSet = goParent.goDB.doSelectQuery("Select * from OUSR Where USER_CODE='" & goParent.goDICompany.UserName & "'")
            If oRecordSet.RecordCount > 0 Then
                bIsSuperUser = CType(IIf(CType(oRecordSet.Fields.Item("SUPERUSER").Value, String) = "Y", True, False), Boolean)
                msUserDept = Conversions.ToString(oRecordSet.Fields.Item("Department").Value)
            End If
            If oRecordSet IsNot Nothing Then DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)

            If bIsSuperUser Then
                doFillCombo(oForm, "IDH_DEPT", "OUDP", "Code", "Name", Nothing, "NAME", "Select", "0")
            Else
                doFillCombo(oForm, "IDH_DEPT", "OUDP", "Code", "Name", "Code=" & msUserDept, "NAME", "Select", "0")
            End If
           
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = CType(oForm.Items.Item("IDH_DEPT").Specific, SAPbouiCOM.ComboBox)
            oCombo.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index)
            doFormsCombo(oForm)
            doLoadUserCombo(oForm)
            '#MA Start 12-06-2017 point#453 copy index from one user to another user saved
            doLoadCopyUserCombo(oForm)
            '#MA End 12-06-2017
        End Sub

        Public Sub doFormsCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                Dim oItem As SAPbouiCOM.Item
                oItem = oForm.Items.Item("IDH_FORMS")
                Dim oCombo As SAPbouiCOM.ComboBox
                oCombo = CType(oItem.Specific, SAPbouiCOM.ComboBox)
                Dim sQuery As String = "SELECT Distinct U_FormID,U_FormName From [@IDH_FORMSET] Order by U_FormID "
                oRecordSet = goParent.goDB.doSelectQuery("doFormsCombo", sQuery)
                If oRecordSet.RecordCount > 0 Then
                    Dim oValidValues As SAPbouiCOM.ValidValues
                    oValidValues = oCombo.ValidValues
                    doClearValidValues(oValidValues)
                    oValidValues.Add(getTranslatedWord("Select"), "Select")
                    Dim iCount As Integer
                    Dim sVal As String
                    Dim sDesc As String
                    For iCount = 0 To oRecordSet.RecordCount - 1
                        sVal = Conversions.ToString(oRecordSet.Fields.Item("U_FormID").Value)
                        sDesc = Conversions.ToString(oRecordSet.Fields.Item("U_FormName").Value)
                        Try
                            oValidValues.Add(sVal, sDesc)

                        Catch ex As Exception
                        End Try
                        oRecordSet.MoveNext()
                    Next
                    oCombo.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index)
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling forms Combo: " & oForm.TypeEx & ".IDH_FORMS")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFFC", {oForm.TypeEx, "IDH_FORMS"})
            End Try
            If oRecordSet IsNot Nothing Then DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
                Try
                    Dim oComboUser, oComboForm, oComboDept As SAPbouiCOM.ComboBox
                '#MA Start POINT#452 25-05-2017
                Dim oRecordSet As DataRecords
                Dim oStrList() As Object
                Dim oTempvalue As String = String.Empty
                '#MA  GRID CHANGED ROW INDEX AND CODE ADD IN DICITIONARY OBJECT
                Dim oUpdateGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                Dim aRows As ArrayList = oUpdateGrid.doGetChangedRows()
                If Not aRows Is Nothing Then
                    For iIndex As Integer = 0 To aRows.Count - 1
                        Dim iRow As Integer = CType(aRows(iIndex), Integer)
                        oIndexChangeList.Add(Conversions.ToString(oUpdateGrid.doGetFieldValue("U_Index", iRow)), Conversions.ToString(oUpdateGrid.doGetFieldValue("Code", iRow)))
                    Next
                End If
                '#MA End 25-05-2017
                    oComboUser = CType(oForm.Items.Item("IDH_USERS").Specific, SAPbouiCOM.ComboBox)
                    oComboForm = CType(oForm.Items.Item("IDH_FORMS").Specific, SAPbouiCOM.ComboBox)
                    oComboDept = CType(oForm.Items.Item("IDH_DEPT").Specific, SAPbouiCOM.ComboBox)
                    If (oComboForm.Selected.Value <> "Select" AndAlso oComboUser.Selected.Value <> "Select" AndAlso oComboDept.Selected.Value <> "Select") Then
                        MyBase.doButtonID1(oForm, pVal, BubbleEvent)
                        If oComboUser.Selected.Value = "All" And pVal.BeforeAction = False Then
                            IDHAddOns.idh.addon.Base.STATUSBAR.SetText("Please wait while system is updating data...", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            ''Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                            '' oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            'goParent.goDB.doSelectQuery("sGetUpdateQueryForUsers", sGetUpdateQueryForUsers(oComboDept.Selected.Value, oComboForm.Selected.Value))

                            If DataHandler.INSTANCE.doUpdateQuery("sGetUpdateQueryForUsers", sGetUpdateQueryForUsers(Conversions.ToInt(oComboDept.Selected.Value), oComboForm.Selected.Value)) Then


                                ''If oRecordSet IsNot Nothing Then IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
                                IDHAddOns.idh.addon.Base.STATUSBAR.SetText("Operation completed successfully.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                            End If
                        End If
                    End If
                '#MA POINT#452 30-05-2017
                If Not oIndexChangeList.Count = 0 Then
                    oForm.Freeze(True)
                    Dim SQuery As String = "SELECT Code,U_Index from [@IDH_FORMSET] WITH (NOLOCK) Where U_FormID='" & oComboForm.Selected.Value & "' And U_UserName='" & oComboUser.Selected.Value & "' And U_DeptCode='" & oComboDept.Selected.Value & "' ORDER BY U_Index ASC"
                    oRecordSet = DataHandler.INSTANCE.doBufferedSelectQuery("doGetIndexes", SQuery)
                    If oRecordSet IsNot Nothing And oRecordSet.RecordCount > 0 Then
                        oStrList = New Object(oRecordSet.RecordCount - 1) {}
                        For iCount = 0 To oRecordSet.RecordCount - 1
                            oRecordSet.gotoRow(iCount)
                            oStrList(iCount) = oRecordSet.getValueAsString(0) + "," + oRecordSet.getValueAsString(1)
                        Next
                    End If
                    For index = 0 To UBound(oStrList)
                        Dim oStrData = oStrList(index)
                        Dim oStrValue = oStrData.Split(","c)
                        If oIndexChangeList.ContainsKey(oStrValue(1)) Then
                            If Not oIndexChangeList.Item(oStrValue(1)) = oStrValue(0) AndAlso index = oStrValue(1) Then
                                oTempvalue = oStrValue(0) + "," + oStrValue(1)
                                oStrList(index) = oIndexChangeList.Item(oStrValue(1)) + "," + oStrValue(1)
                                oStrList(index + 1) = oTempvalue
                            End If
                        End If
                        If oIndexChangeList.ContainsKey(oStrValue(1)) Then
                            If oIndexChangeList.Item(oStrValue(1)) = oStrValue(0) AndAlso index < oStrValue(1) Then
                                oTempvalue = oStrList(index + 1)
                                oStrList(index + 1) = oIndexChangeList.Item(oStrValue(1)) + "," + oStrValue(1)
                                oStrList(index) = oTempvalue
                            End If
                        End If
                    Next index
                    For index = 0 To UBound(oStrList)
                        Dim oStrData = oStrList(index)
                        Dim oStrValue = oStrData.Split(","c)
                        Dim UpdateQuery = "UPDATE [@IDH_FORMSET] SET U_Index='" + index.ToString + "' WHERE U_UserName='" + oComboUser.Selected.Value + "' AND U_FormID='" + oComboForm.Selected.Value + "' AND U_DeptCode='" + oComboDept.Selected.Value + "' AND Code='" + oStrValue(0) + "'"
                        DataHandler.INSTANCE.doUpdateQuery(UpdateQuery)
                    Next index
                    doLoadData(oForm)
                    oIndexChangeList.Clear()
                    oForm.Freeze(False)
                End If
                '#MA End 30-05-2017
                Catch ex As Exception
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling forms Combo: " & oForm.TypeEx & ".IDH_FORMS")
                    DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFFC", {oForm.TypeEx, "IDH_FORMS"})
                oIndexChangeList.Clear()
                oForm.Freeze(False)
                End Try

        End Sub
        Private Function sGetUpdateQueryForUsers(ByVal iDeptCode As Int32, ByVal sFormID As String) As String
            Dim var1 As New System.Text.StringBuilder
            var1.Append("BEGIN " & vbCrLf)
            var1.Append(" Update [@IDH_FORMSET]  Set U_deptCode=(Select Department from OUSR Where OUSR.USer_Code=[@IDH_FORMSET].U_UserName) Where [@IDH_FORMSET].U_UserName<> 'All' And [@IDH_FORMSET].U_UserName<> 'template' " & vbCrLf)
            var1.Append("    DELETE FROM [@idh_formset] " & vbCrLf)
            var1.Append("    WHERE  u_deptcode = " & iDeptCode & " " & vbCrLf)
            var1.Append("           AND u_username <> 'All' " & vbCrLf)
            var1.Append("           AND u_formid = '" & sFormID & "' " & vbCrLf)

            
            var1.Append(" " & vbCrLf)
            var1.Append("    DECLARE @CurrentCodeID INT " & vbCrLf)
            var1.Append(" " & vbCrLf)
            var1.Append("    SELECT @CurrentCodeID = Max(Cast(code AS INT)) + 1 " & vbCrLf)
            var1.Append("    FROM   [@idh_formset] " & vbCrLf)
            var1.Append(" " & vbCrLf)
            var1.Append("    INSERT INTO [@idh_formset] " & vbCrLf)
            var1.Append("                (code, " & vbCrLf)
            var1.Append("                 name, " & vbCrLf)
            var1.Append("                 " & vbCrLf)
            var1.Append("                 [u_formid], " & vbCrLf)
            var1.Append("                 [u_username], " & vbCrLf)
            var1.Append("                 [u_name], " & vbCrLf)
            var1.Append("                 [u_fieldid], " & vbCrLf)
            var1.Append("                 [u_title], " & vbCrLf)
            var1.Append("                 [u_width], " & vbCrLf)
            var1.Append("                 [u_editable], " & vbCrLf)
            var1.Append("                 [u_stype], " & vbCrLf)
            var1.Append("                 [u_ibackcolor], " & vbCrLf)
            var1.Append("                 [u_olinkobject], " & vbCrLf)
            var1.Append("                 [u_bwidthismax], " & vbCrLf)
            var1.Append("                 [u_bpinned], " & vbCrLf)
            var1.Append("                 [u_osumtype], " & vbCrLf)
            var1.Append("                 [u_odisplaytype], " & vbCrLf)
            var1.Append("                 [u_index], " & vbCrLf)
            var1.Append("                 [u_suid], " & vbCrLf)
            var1.Append("                 [u_deptcode], " & vbCrLf)
            var1.Append("                 [u_formname], " & vbCrLf)
            var1.Append("                 [u_GridName], " & vbCrLf)
            var1.Append("                 [U_OrderBy]) " & vbCrLf)
            var1.Append("    SELECT ( Row_number() " & vbCrLf)
            var1.Append("               OVER( " & vbCrLf)
            var1.Append("                 ORDER BY Cast(a.code AS INT) DESC) ) + @CurrentCodeID AS [Code] " & vbCrLf)
            var1.Append("           , " & vbCrLf)
            var1.Append("           ( Row_number() " & vbCrLf)
            var1.Append("               OVER( " & vbCrLf)
            var1.Append("                 ORDER BY Cast(a.code AS INT) DESC) ) + @CurrentCodeID AS [Name] " & vbCrLf)
            var1.Append("           , " & vbCrLf)
            var1.Append("           " & vbCrLf)
            var1.Append("           a.[u_formid], " & vbCrLf)
            var1.Append("           b.user_code AS " & vbCrLf)
            var1.Append("           [U_UserName], " & vbCrLf)
            var1.Append("           a.[u_name], " & vbCrLf)
            var1.Append("           a.[u_fieldid], " & vbCrLf)
            var1.Append("           a.[u_title], " & vbCrLf)
            var1.Append("           a.[u_width], " & vbCrLf)
            var1.Append("           a.[u_editable], " & vbCrLf)
            var1.Append("           a.[u_stype], " & vbCrLf)
            var1.Append("           a.[u_ibackcolor], " & vbCrLf)
            var1.Append("           a.[u_olinkobject], " & vbCrLf)
            var1.Append("           a.[u_bwidthismax], " & vbCrLf)
            var1.Append("           a.[u_bpinned], " & vbCrLf)
            var1.Append("           a.[u_osumtype], " & vbCrLf)
            var1.Append("           a.[u_odisplaytype], " & vbCrLf)
            var1.Append("           a.[u_index], " & vbCrLf)
            var1.Append("           a.[u_suid], " & vbCrLf)
            var1.Append("           " & iDeptCode & " AS " & vbCrLf)
            var1.Append("           U_DeptCode " & vbCrLf)
            var1.Append("           , " & vbCrLf)
            var1.Append("           a.[u_formname] " & vbCrLf)
            var1.Append("           ,a.[u_GridName] " & vbCrLf)
            var1.Append("           ,a.[U_OrderBy] " & vbCrLf)
            var1.Append("    FROM   [@idh_formset] a, " & vbCrLf)
            var1.Append("           ousr b " & vbCrLf)
            var1.Append("    WHERE  a.u_deptcode = " & iDeptCode & " " & vbCrLf)
            var1.Append("           AND a.[u_username] = 'All' " & vbCrLf)
            var1.Append("           AND a.u_formid = '" & sFormID & "' " & vbCrLf)
            var1.Append("           AND a.u_deptcode = b.department " & vbCrLf)
            var1.Append("    ORDER  BY b.user_code " & vbCrLf)
            var1.Append("END ")
            var1.Append(" UPDATE [@IDH_NNUMBERS] set U_SEQ = ( SELECT MAX(CAST( Code As Numeric)) + 1 FROM [@IDH_FORMSET] )  WHERE Code = 'FRMSET' ")
            Return var1.ToString
        End Function
        Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doFinalizeShow(oForm)
        End Sub

        Protected Overrides Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddFilterField("IDH_DEPT", "U_DeptCode", SAPbouiCOM.BoDataType.dt_SHORT_NUMBER, "=", 10, Nothing)
            oGridN.doAddFilterField("IDH_USERS", "U_UserName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "")
            oGridN.doAddFilterField("IDH_FORMS", "U_FormID", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "")
        End Sub

        Private Sub doSetOrderByCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_OrderBy")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            oCombo.ValidValues.Add("", getTranslatedWord("Select"))
            oCombo.ValidValues.Add("ASC", getTranslatedWord("Ascendeing"))
            oCombo.ValidValues.Add("DESC", getTranslatedWord("Descending"))
        End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("U_FormID", "FormID", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_GridName", "Grid Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Name", "Column Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Title", "Caption", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Index", "Position", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Width", "Width", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_FieldID", "Field Name", False, 30, Nothing, Nothing)
            oGridN.doAddListField("u_editable", "Editable(Y/N)", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_OrderBy", "Order By", True, -1, "COMBOBOX", Nothing)
            
            oGridN.doAddListField("Code", "Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", False, 0, Nothing, Nothing)
            'oGridN.setOrderValue("U_GridName ASC, U_Index ASC")
        End Sub

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            Dim oComboUser, oComboDept, oComboForm As SAPbouiCOM.ComboBox
            oComboUser = CType(oForm.Items.Item("IDH_USERS").Specific, SAPbouiCOM.ComboBox)
            oComboForm = CType(oForm.Items.Item("IDH_FORMS").Specific, SAPbouiCOM.ComboBox)
            oComboDept = CType(oForm.Items.Item("IDH_DEPT").Specific, SAPbouiCOM.ComboBox)
            If (oComboDept.Selected IsNot Nothing AndAlso oComboForm.Selected IsNot Nothing And oComboUser.Selected IsNot Nothing) _
                AndAlso (oComboDept.Selected.Value <> "Select" AndAlso oComboForm.Selected.Value <> "Select" And oComboUser.Selected.Value <> "Select") Then

                'If oComboUser.Selected IsNot Nothing Then
                Dim sSql As String = "Select * from [@IDH_FORMSET] Where U_FormID='" & oComboForm.Selected.Value & "' And U_UserName='" & oComboUser.Selected.Value & "' And U_DeptCode='" & oComboDept.Selected.Value & "' Order by IsNull(U_GridName,'') Desc , U_Index Asc, Cast(code as Int)"
                If oComboUser.Selected.Value.ToLower.Trim <> "all" Then
                    sSql = " Update [@IDH_FORMSET]  Set U_deptCode=(Select Department from OUSR Where OUSR.USer_Code=[@IDH_FORMSET].U_UserName) Where [@IDH_FORMSET].U_UserName= '" & oComboUser.Selected.Value & "'"
                    'PARENT.goDB.doSelectQuery(sSql)
                    DataHandler.INSTANCE.doUpdateQuery("doLoadData", sSql)
                    sSql = "Select * from [@IDH_FORMSET] Where U_FormID='" & oComboForm.Selected.Value & "' And U_UserName='" & oComboUser.Selected.Value & "' Order by IsNull(U_GridName,'') Desc ,U_Index Asc, Cast(code as Int)"
                Else
                    Dim var1 As New System.Text.StringBuilder
                    var1.Append("    IF (SELECT Count(1) " & vbCrLf)
                    var1.Append("                  FROM   [@idh_formset] " & vbCrLf)
                    var1.Append("                  WHERE  [u_username] = 'manager' " & vbCrLf)
                    'var1.Append("                         AND u_deptcode = '" & oComboDept.Selected.Value & "' " & vbCrLf)
                    var1.Append("                         AND u_formid = '" & oComboForm.Selected.Value & "') " & vbCrLf)
                    var1.Append("    <> (SELECT Count(1) " & vbCrLf)
                    var1.Append("                  FROM   [@idh_formset] " & vbCrLf)
                    var1.Append("                  WHERE  [u_username] = 'All' " & vbCrLf)
                    var1.Append("                         AND u_deptcode = '" & oComboDept.Selected.Value & "' " & vbCrLf)
                    var1.Append("                         AND u_formid = '" & oComboForm.Selected.Value & "') " & vbCrLf)
                    'var1.Append("      BEGIN " & vbCrLf)
                    var1.Append("       Delete " & vbCrLf)
                    var1.Append("                  FROM   [@idh_formset] " & vbCrLf)
                    var1.Append("                  WHERE  [u_username] = 'All' " & vbCrLf)
                    var1.Append("                         AND u_deptcode = '" & oComboDept.Selected.Value & "' " & vbCrLf)
                    var1.Append("                         AND u_formid = '" & oComboForm.Selected.Value & "' " & vbCrLf)
                    'PARENT.goDB.doSelectQuery(var1.ToString)
                    DataHandler.INSTANCE.doUpdateQuery("doLoadData", var1.ToString)
                End If
                Dim oRecSet As SAPbobsCOM.Recordset = DataHandler.INSTANCE.doSBOSelectQuery("doLoadData", sSql)
                If oRecSet Is Nothing OrElse oRecSet.RecordCount = 0 Then
                    IDHAddOns.idh.addon.Base.STATUSBAR.SetText("Please wait while system is updating data...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

                    sSql = "Select * from [@IDH_FORMSET] Where U_FormID='" & oComboForm.Selected.Value & "' And U_UserName='All' And U_DeptCode=" & oComboDept.Selected.Value & " Order by IsNull(U_GridName,'') Desc ,U_Index Asc, Cast(code as Int) Asc"

                    DataHandler.INSTANCE.doReleaseRecordset(oRecSet)

                    'oRecSet = PARENT.goDB.doSelectQuery(sSql)
                    oRecSet = DataHandler.INSTANCE.doSBOSelectQuery("doLoadData", sSql)
                    If oRecSet Is Nothing OrElse oRecSet.RecordCount = 0 Then 'No Data for Dept and user 'All' in dept.; So Add from manager to all user under selected dept.

                        sSql = sGetSQLForAllUser(oComboDept.Selected.Value, oComboForm.Selected.Value)
                        'PARENT.goDB.doSelectQuery(sSql)
                        DataHandler.INSTANCE.doUpdateQuery("doLoadData", sSql)

                        'Also insert for users under the dept.
                        sSql = sGetSQLForUsersInDepartment(oComboDept.Selected.Value, oComboForm.Selected.Value, oComboUser.Selected.Value)
                        'PARENT.goDB.doSelectQuery(sSql)
                        DataHandler.INSTANCE.doUpdateQuery("doLoadData", sSql)

                        IDHAddOns.idh.addon.Base.STATUSBAR.SetText("Database updated successfully for Form Settings.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

                        'sSql = "Select * from [@IDH_FORMSET] Where U_FormID='" & oComboForm.Selected.Value & "' And U_UserName='template' Order by U_Index Asc, Cast(code as Int) Asc"
                        'oRecSet = PARENT.goDB.doSelectQuery(sSql)
                    Else 'We found Dept Data so Insert data from Dept to User
                        IDHAddOns.idh.addon.Base.STATUSBAR.SetText("Please wait while system is updating data...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

                        sSql = sGetSQLForUsersInDepartment(oComboDept.Selected.Value, oComboForm.Selected.Value, oComboUser.Selected.Value)
                        'PARENT.goDB.doSelectQuery(sSql)
                        DataHandler.INSTANCE.doUpdateQuery("doLoadData", sSql)

                        IDHAddOns.idh.addon.Base.STATUSBAR.SetText("Database updated successfully for Form Settings.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

                    End If
                End If
                'End If
                If oRecSet IsNot Nothing Then DataHandler.INSTANCE.doReleaseRecordset(oRecSet)
            End If
            'Dim oGridN As IDHAddOns.idh.controls.FilterGrid = oForm.Items.Item("")
            'oGridN.setOrderValue("U_GridName ASC, U_Index ASC")
            MyBase.doLoadData(oForm)

            doSetOrderByCombo(oForm)
        End Sub
        Public Function sGetSQLForUsersInDepartment(ByVal iDeptCode As String, ByVal sFormID As String, ByVal sUserCode As String) As String
            Dim var11 As New System.Text.StringBuilder

            var11.Append("BEGIN " & vbCrLf)
            var11.Append("DECLARE @CurrentCodeID INT")
            var11.Append(" " & vbCrLf)
            var11.Append("IF NOT EXISTS(SELECT code " & vbCrLf)
            var11.Append("              FROM   [@idh_formset] " & vbCrLf)
            'var11.Append("              WHERE  [u_deptcode] = " & iDeptCode & " " & vbCrLf)
            If sUserCode.Trim.ToLower = "all" Then
                var11.Append("              WHERE  [u_deptcode] = " & iDeptCode & " AND  " & vbCrLf)
            Else
                var11.Append("              WHERE  " & vbCrLf)
            End If
            var11.Append("                     [u_username] = '" & sUserCode & "' " & vbCrLf)
            var11.Append("                     AND u_formid = '" & sFormID & "') " & vbCrLf)
            var11.Append("  BEGIN " & vbCrLf)
            var11.Append("      IF EXISTS(SELECT code " & vbCrLf)
            var11.Append("                FROM   [@idh_formset] " & vbCrLf)
            var11.Append("                WHERE  [u_deptcode] = " & iDeptCode & " " & vbCrLf)
            var11.Append("                       AND [u_username] = 'All' " & vbCrLf)
            var11.Append("                       AND u_formid = '" & sFormID & "') " & vbCrLf)
            var11.Append("        BEGIN " & vbCrLf)
            var11.Append("            SELECT @CurrentCodeID = Max(Cast(code AS INT)) + 1 " & vbCrLf)
            var11.Append("            FROM   [@idh_formset] " & vbCrLf)
            var11.Append(" " & vbCrLf)
            var11.Append("            INSERT INTO [@idh_formset] " & vbCrLf)
            var11.Append("                        (code, " & vbCrLf)
            var11.Append("                         name, " & vbCrLf)
            var11.Append("                         " & vbCrLf)
            var11.Append("                         [u_formid], " & vbCrLf)
            var11.Append("                         [u_username], " & vbCrLf)
            var11.Append("                         [u_name], " & vbCrLf)
            var11.Append("                         [u_fieldid], " & vbCrLf)
            var11.Append("                         [u_title], " & vbCrLf)
            var11.Append("                         [u_width], " & vbCrLf)
            var11.Append("                         [u_editable], " & vbCrLf)
            var11.Append("                         [u_stype], " & vbCrLf)
            var11.Append("                         [u_ibackcolor], " & vbCrLf)
            var11.Append("                         [u_olinkobject], " & vbCrLf)
            var11.Append("                         [u_bwidthismax], " & vbCrLf)
            var11.Append("                         [u_bpinned], " & vbCrLf)
            var11.Append("                         [u_osumtype], " & vbCrLf)
            var11.Append("                         [u_odisplaytype], " & vbCrLf)
            var11.Append("                         [u_index], " & vbCrLf)
            var11.Append("                         [u_suid], " & vbCrLf)
            var11.Append("                         [u_deptcode], " & vbCrLf)
            var11.Append("                         [u_formname], " & vbCrLf)
            var11.Append("                         [U_GridName], " & vbCrLf)
            var11.Append("                         [U_OrderBy]) " & vbCrLf)
            var11.Append("            SELECT ( Row_number() " & vbCrLf)
            var11.Append("                       OVER( " & vbCrLf)
            var11.Append("                         ORDER BY Cast(code AS INT) DESC) ) + @CurrentCodeID AS " & vbCrLf)
            var11.Append("                   [Code] " & vbCrLf)
            var11.Append("                   , " & vbCrLf)
            var11.Append("                   ( Row_number() " & vbCrLf)
            var11.Append("                       OVER( " & vbCrLf)
            var11.Append("                         ORDER BY Cast(code AS INT) DESC) ) + @CurrentCodeID AS " & vbCrLf)
            var11.Append("                   [Name] " & vbCrLf)
            var11.Append("                   , " & vbCrLf)
            var11.Append("                   " & vbCrLf)
            var11.Append("                   [u_formid], " & vbCrLf)
            var11.Append("                   '" & sUserCode & "' AS " & vbCrLf)
            var11.Append("                   [U_UserName] " & vbCrLf)
            var11.Append("                   , " & vbCrLf)
            var11.Append("                   [u_name], " & vbCrLf)
            var11.Append("                   [u_fieldid], " & vbCrLf)
            var11.Append("                   [u_title], " & vbCrLf)
            var11.Append("                   [u_width], " & vbCrLf)
            var11.Append("                   [u_editable], " & vbCrLf)
            var11.Append("                   [u_stype], " & vbCrLf)
            var11.Append("                   [u_ibackcolor], " & vbCrLf)
            var11.Append("                   [u_olinkobject], " & vbCrLf)
            var11.Append("                   [u_bwidthismax], " & vbCrLf)
            var11.Append("                   [u_bpinned], " & vbCrLf)
            var11.Append("                   [u_osumtype], " & vbCrLf)
            var11.Append("                   [u_odisplaytype], " & vbCrLf)
            var11.Append("                   [u_index], " & vbCrLf)
            var11.Append("                   [u_suid], " & vbCrLf)
            var11.Append("                   " & iDeptCode & ", " & vbCrLf)
            var11.Append("                   [u_formname], " & vbCrLf)
            var11.Append("                   [u_GridName], " & vbCrLf)
            var11.Append("                   [U_OrderBy] " & vbCrLf)
            var11.Append("            FROM   [@idh_formset] " & vbCrLf)
            var11.Append("            WHERE  u_username = 'All' " & vbCrLf)
            var11.Append("                   AND [u_deptcode] = " & iDeptCode & " " & vbCrLf)
            var11.Append("                   AND u_formid = '" & sFormID & "' " & vbCrLf)
            var11.Append("            ORDER  BY Cast(code AS INT) " & vbCrLf)
            var11.Append("        END " & vbCrLf)
            var11.Append("      ELSE--Insert from Manager's Record-- As not found from User's Dept List " & vbCrLf)
            var11.Append("        BEGIN " & vbCrLf)
            var11.Append("            SELECT @CurrentCodeID = Max(Cast(code AS INT)) + 1 " & vbCrLf)
            var11.Append("            FROM   [@idh_formset] " & vbCrLf)
            var11.Append(" " & vbCrLf)
            var11.Append("            INSERT INTO [@idh_formset] " & vbCrLf)
            var11.Append("                        (code, " & vbCrLf)
            var11.Append("                         name, " & vbCrLf)
            var11.Append("                         " & vbCrLf)
            var11.Append("                         [u_formid], " & vbCrLf)
            var11.Append("                         [u_username], " & vbCrLf)
            var11.Append("                         [u_name], " & vbCrLf)
            var11.Append("                         [u_fieldid], " & vbCrLf)
            var11.Append("                         [u_title], " & vbCrLf)
            var11.Append("                         [u_width], " & vbCrLf)
            var11.Append("                         [u_editable], " & vbCrLf)
            var11.Append("                         [u_stype], " & vbCrLf)
            var11.Append("                         [u_ibackcolor], " & vbCrLf)
            var11.Append("                         [u_olinkobject], " & vbCrLf)
            var11.Append("                         [u_bwidthismax], " & vbCrLf)
            var11.Append("                         [u_bpinned], " & vbCrLf)
            var11.Append("                         [u_osumtype], " & vbCrLf)
            var11.Append("                         [u_odisplaytype], " & vbCrLf)
            var11.Append("                         [u_index], " & vbCrLf)
            var11.Append("                         [u_suid], " & vbCrLf)
            var11.Append("                         [u_deptcode], " & vbCrLf)
            var11.Append("                         [u_formname], " & vbCrLf)
            var11.Append("                         [u_GridName], " & vbCrLf)
            var11.Append("                         [U_OrderBy]) " & vbCrLf)
            var11.Append("            SELECT ( Row_number() " & vbCrLf)
            var11.Append("                       OVER( " & vbCrLf)
            var11.Append("                         ORDER BY Cast(code AS INT) DESC) ) + @CurrentCodeID AS " & vbCrLf)
            var11.Append("                   [Code] " & vbCrLf)
            var11.Append("                   , " & vbCrLf)
            var11.Append("                   ( Row_number() " & vbCrLf)
            var11.Append("                       OVER( " & vbCrLf)
            var11.Append("                         ORDER BY Cast(code AS INT) DESC) ) + @CurrentCodeID AS " & vbCrLf)
            var11.Append("                   [Name] " & vbCrLf)
            var11.Append("                   , " & vbCrLf)
            var11.Append("                   " & vbCrLf)
            var11.Append("                   [u_formid], " & vbCrLf)
            var11.Append("                   '" & sUserCode & "' AS " & vbCrLf)
            var11.Append("                   [U_UserName] " & vbCrLf)
            var11.Append("                   , " & vbCrLf)
            var11.Append("                   [u_name], " & vbCrLf)
            var11.Append("                   [u_fieldid], " & vbCrLf)
            var11.Append("                   [u_title], " & vbCrLf)
            var11.Append("                   [u_width], " & vbCrLf)
            var11.Append("                   [u_editable], " & vbCrLf)
            var11.Append("                   [u_stype], " & vbCrLf)
            var11.Append("                   [u_ibackcolor], " & vbCrLf)
            var11.Append("                   [u_olinkobject], " & vbCrLf)
            var11.Append("                   [u_bwidthismax], " & vbCrLf)
            var11.Append("                   [u_bpinned], " & vbCrLf)
            var11.Append("                   [u_osumtype], " & vbCrLf)
            var11.Append("                   [u_odisplaytype], " & vbCrLf)
            var11.Append("                   [u_index], " & vbCrLf)
            var11.Append("                   [u_suid], " & vbCrLf)
            var11.Append("                   " & iDeptCode & ", " & vbCrLf)
            var11.Append("                   [u_formname], " & vbCrLf)
            var11.Append("                   [u_GridName], " & vbCrLf)
            var11.Append("                   [U_OrderBy] " & vbCrLf)
            var11.Append("            FROM   [@idh_formset] " & vbCrLf)
            var11.Append("            WHERE  u_username = 'manager' " & vbCrLf) ''template' changed user to manager
            'var11.Append("                   AND [u_deptcode] = " & iDeptCode & " " & vbCrLf)
            var11.Append("                   AND u_formid = '" & sFormID & "' " & vbCrLf)
            var11.Append("            ORDER  BY Cast(code AS INT) " & vbCrLf)
            var11.Append("        END " & vbCrLf)
            var11.Append("  END " & vbCrLf)
            var11.Append(" UPDATE [@IDH_NNUMBERS] set U_SEQ = ( SELECT MAX(CAST( Code As Numeric)) + 1 FROM [@IDH_FORMSET] )  WHERE Code = 'FRMSET' ")
            var11.Append("END")
            Return var11.ToString
        End Function
        Public Function sGetSQLForAllUser(ByVal iDeptCode As String, ByVal sFormID As String) As String
            Dim var1 As New System.Text.StringBuilder
            var1.Append("BEGIN " & vbCrLf)
            var1.Append("    DECLARE @CurrentCodeID INT " & vbCrLf)
            var1.Append(" " & vbCrLf)
            var1.Append("    IF (SELECT Count(1) " & vbCrLf)
            var1.Append("                  FROM   [@idh_formset] " & vbCrLf)
            var1.Append("                  WHERE  [u_username] = 'All' " & vbCrLf)
            var1.Append("                         AND u_deptcode = '" & iDeptCode & "' " & vbCrLf)
            var1.Append("                         AND u_formid = '" & sFormID & "') " & vbCrLf)
            var1.Append("    <> (SELECT Count(1) " & vbCrLf)
            var1.Append("                  FROM   [@idh_formset] " & vbCrLf)
            var1.Append("                  WHERE  [u_username] = 'manager' " & vbCrLf)
            'var1.Append("                         AND u_deptcode = '" & iDeptCode & "' " & vbCrLf)
            var1.Append("                         AND u_formid = '" & sFormID & "') " & vbCrLf)
            var1.Append("      BEGIN " & vbCrLf)
            var1.Append("       Delete " & vbCrLf)
            var1.Append("                  FROM   [@idh_formset] " & vbCrLf)
            var1.Append("                  WHERE  [u_username] = 'All' " & vbCrLf)
            var1.Append("                         AND u_deptcode = '" & iDeptCode & "' " & vbCrLf)
            var1.Append("                         AND u_formid = '" & sFormID & "' " & vbCrLf)
            var1.Append("          SELECT @CurrentCodeID = Max(Cast(code AS INT)) + 1 " & vbCrLf)
            var1.Append("          FROM   [@idh_formset] " & vbCrLf)
            var1.Append(" " & vbCrLf)
            var1.Append("          INSERT INTO [@idh_formset] " & vbCrLf)
            var1.Append("                      (code, " & vbCrLf)
            var1.Append("                       name, " & vbCrLf)
            var1.Append("                       " & vbCrLf)
            var1.Append("                       [u_formid], " & vbCrLf)
            var1.Append("                       [u_username], " & vbCrLf)
            var1.Append("                       [u_name], " & vbCrLf)
            var1.Append("                       [u_fieldid], " & vbCrLf)
            var1.Append("                       [u_title], " & vbCrLf)
            var1.Append("                       [u_width], " & vbCrLf)
            var1.Append("                       [u_editable], " & vbCrLf)
            var1.Append("                       [u_stype], " & vbCrLf)
            var1.Append("                       [u_ibackcolor], " & vbCrLf)
            var1.Append("                       [u_olinkobject], " & vbCrLf)
            var1.Append("                       [u_bwidthismax], " & vbCrLf)
            var1.Append("                       [u_bpinned], " & vbCrLf)
            var1.Append("                       [u_osumtype], " & vbCrLf)
            var1.Append("                       [u_odisplaytype], " & vbCrLf)
            var1.Append("                       [u_index], " & vbCrLf)
            var1.Append("                       [u_suid], " & vbCrLf)
            var1.Append("                       [u_deptcode], " & vbCrLf)
            var1.Append("                       [u_formname], " & vbCrLf)
            var1.Append("                       [u_GridName], " & vbCrLf)
            var1.Append("                       [U_OrderBy]) " & vbCrLf)
            var1.Append("          SELECT ( Row_number() " & vbCrLf)
            var1.Append("                     OVER( " & vbCrLf)
            var1.Append("                       ORDER BY Cast(code AS INT) DESC) ) + @CurrentCodeID AS " & vbCrLf)
            var1.Append("                 [Code] " & vbCrLf)
            var1.Append("                 , " & vbCrLf)
            var1.Append("                 ( Row_number() " & vbCrLf)
            var1.Append("                     OVER( " & vbCrLf)
            var1.Append("                       ORDER BY Cast(code AS INT) DESC) ) + @CurrentCodeID AS " & vbCrLf)
            var1.Append("                 [Name] " & vbCrLf)
            var1.Append("                 , " & vbCrLf)
            var1.Append("                 " & vbCrLf)
            var1.Append("                 [u_formid], " & vbCrLf)
            var1.Append("                 'All' AS " & vbCrLf)
            var1.Append("                 [U_UserName] " & vbCrLf)
            var1.Append("                 , " & vbCrLf)
            var1.Append("                 [u_name], " & vbCrLf)
            var1.Append("                 [u_fieldid], " & vbCrLf)
            var1.Append("                 [u_title], " & vbCrLf)
            var1.Append("                 [u_width], " & vbCrLf)
            var1.Append("                 [u_editable], " & vbCrLf)
            var1.Append("                 [u_stype], " & vbCrLf)
            var1.Append("                 [u_ibackcolor], " & vbCrLf)
            var1.Append("                 [u_olinkobject], " & vbCrLf)
            var1.Append("                 [u_bwidthismax], " & vbCrLf)
            var1.Append("                 [u_bpinned], " & vbCrLf)
            var1.Append("                 [u_osumtype], " & vbCrLf)
            var1.Append("                 [u_odisplaytype], " & vbCrLf)
            var1.Append("                 [u_index], " & vbCrLf)
            var1.Append("                 [u_suid], " & vbCrLf)
            var1.Append("                 '" & iDeptCode & "'  AS " & vbCrLf)
            var1.Append("                 U_DeptCode, " & vbCrLf)
            var1.Append("                 [u_formname], " & vbCrLf)
            var1.Append("                 [u_GridName], " & vbCrLf)
            var1.Append("                 [U_OrderBy] " & vbCrLf)
            var1.Append("          FROM   [@idh_formset] " & vbCrLf)
            var1.Append("          WHERE  u_username = 'manager' " & vbCrLf) ''template' changed user to manager
            var1.Append("                 AND u_formid = '" & sFormID & "' " & vbCrLf)
            var1.Append("          ORDER  BY Cast(code AS INT) " & vbCrLf)
            var1.Append("      END " & vbCrLf)
            var1.Append(" UPDATE [@IDH_NNUMBERS] set U_SEQ = ( SELECT MAX(CAST( Code As Numeric)) + 1 FROM [@IDH_FORMSET] )  WHERE Code = 'FRMSET' ")
            var1.Append("END ")

            Return var1.ToString
        End Function

        Private Sub doLoadUserCombo(ByVal oForm As SAPbouiCOM.Form) 'ByVal sDeptName As String)
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = CType(oForm.Items.Item("IDH_DEPT").Specific, SAPbouiCOM.ComboBox)
            If oCombo.Selected.Value = "0" Then
                Dim oValidValues As SAPbouiCOM.ValidValues
                oCombo = CType(oForm.Items.Item("IDH_USERS").Specific, SAPbouiCOM.ComboBox)
                oValidValues = oCombo.ValidValues
                doClearValidValues(oValidValues)
                oValidValues.Add(getTranslatedWord("Select"), "Select")
                oCombo.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index)
            Else
                If bIsSuperUser Then
                    doFillCombo(oForm, "IDH_USERS", "OUSR", "USER_CODE", "U_NAME", "Department='" & oCombo.Selected.Value & "' ", "U_NAME", "All", "All")
                Else
                    doFillCombo(oForm, "IDH_USERS", "OUSR", "USER_CODE", "U_NAME", "USER_CODE='" & goParent.goDICompany.UserName & "'", "U_NAME", "Select", "Select")
                End If
                oCombo = CType(oForm.Items.Item("IDH_USERS").Specific, SAPbouiCOM.ComboBox)
                oCombo.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index)
            End If
        End Sub

        Private Sub doLoadCopyUserCombo(ByVal oForm As SAPbouiCOM.Form) 'ByVal sDeptName As String)
            Dim oCombo As SAPbouiCOM.ComboBox
            oCombo = CType(oForm.Items.Item("IDH_DEPT").Specific, SAPbouiCOM.ComboBox)
            If oCombo.Selected.Value = "0" Then
                Dim oValidValues As SAPbouiCOM.ValidValues
                oCombo = CType(oForm.Items.Item("IDH_CUSER").Specific, SAPbouiCOM.ComboBox)
                oValidValues = oCombo.ValidValues
                doClearValidValues(oValidValues)
                oValidValues.Add(getTranslatedWord("Select"), "Select")
                oCombo.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index)
            Else
                If bIsSuperUser Then
                    doFillCombo(oForm, "IDH_CUSER", "OUSR", "USER_CODE", "U_NAME", "Department='" & oCombo.Selected.Value & "' ", "U_NAME", "Select", "Select")
                Else
                    doFillCombo(oForm, "IDH_CUSER", "OUSR", "USER_CODE", "U_NAME", "USER_CODE='" & goParent.goDICompany.UserName & "'", "U_NAME", "Select", "Select")
                End If
                oCombo = CType(oForm.Items.Item("IDH_CUSER").Specific, SAPbouiCOM.ComboBox)
                oCombo.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index)
            End If
        End Sub


        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            If pVal.ItemUID = "IDH_DEPT" AndAlso pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT AndAlso pVal.BeforeAction = False Then
                doLoadUserCombo(oForm)
                doLoadCopyUserCombo(oForm)
            ElseIf (pVal.ItemUID = "IDH_USERS" OrElse pVal.ItemUID = "IDH_FORMS") AndAlso pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT AndAlso pVal.BeforeAction = False Then
                doLoadData(oForm)
            ElseIf (pVal.ItemUID = "IDH_DEL") AndAlso pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED AndAlso (getUFValue(oForm, "IDH_FORMS").ToString() <> "Select") AndAlso pVal.BeforeAction = False Then
                If com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYNAsk("DELREC") = 1 Then
                    'Get the Deletion comment
                    Dim sDept As String = getUFValue(oForm, "IDH_DEPT").ToString()
                    Dim sFormName As String = getUFValue(oForm, "IDH_FORMS").ToString()
                    Dim sUserName As String = getUFValue(oForm, "IDH_USERS").ToString()
                    Dim ssql As String = "Delete From [@IDH_FORMSET] where U_FormID Like '" & sFormName & "'" ' and U_DeptCode Like '" & sDept & "'"
                    'If sUserName <> "All" Then
                    '    ssql &= "and U_UserName Like '" & sUserName & "'"
                    'End If
                    DataHandler.INSTANCE.doUpdateQuery("doDeleteFormSetting", ssql)
                    doLoadData(oForm)
                    sFormName = getItemValue(oForm, "IDH_FORMS")
                    goParent.doMessage("You need to rebuild database, please use manager login and restart WR1 and open " & sFormName)
                    ' DataHandler.INSTANCE.doErrorToStatusbar("deleted")
                    'goParent.doOpenModalForm("IDH_COMMFRM", oGridN.getSBOForm())
                Else
                    'Return False
                End If
                '#MA Start 12-06-2017 Point#453 Copy Index from one user to another user saved
            ElseIf (pVal.ItemUID = "IDH_UCOPY") AndAlso pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED AndAlso pVal.BeforeAction = False Then
                If (getUFValue(oForm, "IDH_FORMS").ToString() <> "Select") AndAlso (getUFValue(oForm, "IDH_DEPT").ToString() <> "Select") AndAlso (getUFValue(oForm, "IDH_USERS").ToString() <> "Select") AndAlso (getItemValue(oForm, "IDH_CUSER").ToString() <> "Select") Then
                    doCopyUserData(oForm)
                End If
                '#MA End 12-06-2017
            End If
            Return True
        End Function
        Private Sub doCopyUserData(ByVal oForm As SAPbouiCOM.Form)
            Try
                IDHAddOns.idh.addon.Base.STATUSBAR.SetText("Please Wait While System is Copying data form one user to another user...", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                Dim oRecordSet As DataRecords
                Dim SQuery As String = "SELECT U_FieldId,U_Index,U_Title,U_Width,U_Editable,U_OrderBy FROM [@IDH_FORMSET] WITH (NOLOCK) Where U_FormID='" & getUFValue(oForm, "IDH_FORMS").ToString() & "' And U_UserName='" & getUFValue(oForm, "IDH_USERS").ToString() & "' And U_DeptCode='" & getUFValue(oForm, "IDH_DEPT").ToString() & "' ORDER BY U_Index ASC"
                oRecordSet = DataHandler.INSTANCE.doBufferedSelectQuery("doGetCopyIndexes", SQuery)
                If oRecordSet IsNot Nothing And oRecordSet.RecordCount > 0 Then
                    For iCount = 0 To oRecordSet.RecordCount - 1
                        oRecordSet.gotoRow(iCount)
                        Dim UpdateQuery = "UPDATE [@IDH_FORMSET] SET U_Index='" + oRecordSet.getValueAsString("U_Index") + "',U_Title='" + oRecordSet.getValueAsString("U_Title") + "',U_Width='" + oRecordSet.getValueAsString("U_Width") + "',U_Editable='" + oRecordSet.getValueAsString("U_Editable") + "',U_OrderBy='" + oRecordSet.getValueAsString("U_OrderBy") + "' WHERE U_UserName='" + getItemValue(oForm, "IDH_CUSER").ToString + "' AND U_FormID='" + getUFValue(oForm, "IDH_FORMS").ToString() + "' AND U_DeptCode='" + getUFValue(oForm, "IDH_DEPT").ToString() + "' AND U_FieldId LIKE'" + oRecordSet.getValue("U_FieldId") + "'"
                        DataHandler.INSTANCE.doUpdateQuery(UpdateQuery)
                    Next
                End If
                IDHAddOns.idh.addon.Base.STATUSBAR.SetText("Operation Completed Successfully.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
            Catch ex As Exception
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFFC", {oForm.TypeEx, "IDH_FORMS"})
            End Try
        End Sub
    End Class
End Namespace
