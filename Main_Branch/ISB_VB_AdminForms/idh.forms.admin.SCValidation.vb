Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class SCValidation
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHSCVALID", sParMenu, iMenuPosition, Nothing, "Service Call Validations" )
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_SCVALID"
        End Function

    End Class
End Namespace