Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge.lookups
Imports com.idh.dbObjects.User
Imports com.idh.bridge.resources

Namespace idh.forms.admin
    Public Class Routes2
        Inherits idh.forms.admin.Tmpl
        'OnTime [#Ico00033263] WR1_NewDev_Routing and Scheduling Module
        'Joachim Alleritz
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)

            MyBase.New(oParent, "IDHRTADM2", sParMenu, iMenuPosition, "Route Admin.srf", "Routes Setup")

        End Sub

        '        Protected Overrides Function getTitle() As String
        '            Return "Routes Setup"
        '        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_ROUTES"
        End Function
        
        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
        End Sub
        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
            doAMPMCombo(oForm)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
            doAMFilterCombo(oForm)
        End Sub
        Private Sub doAMPMCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_IDHAMPM")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)

            oCombo.ValidValues.Add("", getTranslatedWord("Any"))
            oCombo.ValidValues.Add("AM", getTranslatedWord("AM"))
            oCombo.ValidValues.Add("PM", getTranslatedWord("PM"))

        End Sub
        Protected Overrides Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddFilterField("IDH_FROUTE", "Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_FWDAY", "U_IDHWDAY", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_FAMPM", "U_IDHAMPM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_FCONT", "U_IDHCCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_FWASTE", "U_IDHWCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_FVEH", "U_IDHVHRG", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
        End Sub
        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Route", True, -1, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_DESCR", "Route Description", True, 0, Nothing, Nothing) '#MA issue:411 20170602
            oGridN.doAddListField("U_IDHAREA", "Area/Zone", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHZPCD", "Post Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHWDAY", "Week Day", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHAMPM", "AM/PM", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_IDHCCD", "Container Type", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHWCD", "Waste Type", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHVHRG", "Default Vehicle", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHDRVR", "Default Driver", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHVFDT", "Valid From", True, -1, "DATE", Nothing)
            oGridN.doAddListField("U_IDHSTATUS", "Canceled", True, -1, "CHECKBOX", Nothing)
            oGridN.doAddListField("U_COST", "Route Cost", True, -1, Nothing, Nothing)
        End Sub
        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "LINESGRID" Then
                        Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                        Dim iCurrentDataRow As Integer = oGridN.getGrid().GetDataTableRowIndex(pVal.Row)
                        oGridN.setCurrentDataRowIndex(iCurrentDataRow)
                    ElseIf pVal.ItemUID = "IDH_FROUTE" OrElse pVal.ItemUID = "IDH_FWDAY" OrElse pVal.ItemUID = "IDH_FAMPM" _
                    OrElse pVal.ItemUID = "IDH_FCONT" OrElse pVal.ItemUID = "IDH_FWASTE" OrElse pVal.ItemUID = "IDH_FVEH" Then
                        If pVal.ItemChanged Then
                            doCheckForFilter(oForm)
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_FAMPM" Then
                        doCheckForFilter(oForm)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If pVal.BeforeAction = False Then
                    If pVal.CharPressed = 9 Then
                        If pVal.ItemUID = "LINESGRID" Then
                            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                            Dim sField3 As String = CType(oGridN.doGetFieldValue("U_IDHZPCD"), String) 'U_IDHZPCD Postal Codes
                            Dim sField4 As String = CType(oGridN.doGetFieldValue("U_IDHWDAY"), String) 'U_IDHWDAY Week Days
                            Dim sField6 As String = CType(oGridN.doGetFieldValue("U_IDHCCD"), String) 'U_IDHCCD Container
                            Dim sField7 As String = CType(oGridN.doGetFieldValue("U_IDHWCD"), String) 'U_IDHWCD Waste
                            Dim sField8 As String = CType(oGridN.doGetFieldValue("U_IDHVHRG"), String) 'U_IDHVHRG Vehicle
                            Dim sField9 As String = CType(oGridN.doGetFieldValue("U_IDHDRVR"), String) 'U_IDHDRVR Driver
                            Dim SearchForThis As String = "*" 'this is the character i am looking for
                            Dim FirstCharacter As Integer = sField3.IndexOf(SearchForThis) 'looking for a * anywhere in the field
                            If FirstCharacter <> -1 Then '-1 equals not found, anything other is the position of the character *
                                'If sField3 = "*" Then 'Postal Code
                                setSharedData(oForm, "SILENT", "SHOWMULTI")
                                setSharedData(oForm, "IDH_CODE", "")
                                goParent.doOpenModalForm("IDHZONSRC", oForm)
                            ElseIf sField4 = "*" Then 'Week Days
                                setSharedData(oForm, "SILENT", "SHOWMULTI")
                                setSharedData(oForm, "IDHWDAY", "")
                                goParent.doOpenModalForm("IDHCHD", oForm)
                            ElseIf sField6 = "*" Then 'Container
                                Dim sGrp As String = Config.Parameter("IGR-LCON")
                                Dim sItem As String = "" 'getUFValue(oForm, "U_IDHCCD")
                                setSharedData(oForm, "TRG", "PROD")
                                setSharedData(oForm, "IDH_ITMCOD", sItem)
                                setSharedData(oForm, "IDH_GRPCOD", sGrp)
                                setSharedData(oForm, "SILENT", "SHOWMULTI")
                                goParent.doOpenModalForm("IDHISRC", oForm)
                            ElseIf sField7 = "*" Then 'Waste
                                Dim sGrp As String = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup()
                                Dim sItem As String = "" 'getUFValue(oForm, "U_IDHWCD")
                                setSharedData(oForm, "TRG", "WAST")
                                setSharedData(oForm, "TP", "WAST")
                                setSharedData(oForm, "IDH_ITMCOD", sItem)
                                setSharedData(oForm, "IDH_GRPCOD", sGrp)
                                setSharedData(oForm, "IDH_INVENT", "N")
                                setSharedData(oForm, "SILENT", "SHOWMULTI")
                                goParent.doOpenModalForm("IDHWISRC", oForm)
                            ElseIf sField8 = "*" Then 'Vehicle
                                doLorrySearch(oForm)

                                'setSharedData(oForm, "IDH_VEHREG", "")
                                'setSharedData(oForm, "IDH_DRVR", "")
                                'setSharedData(oForm, "SHOWCREATE", "TRUE")
                                'setSharedData(oForm, "SILENT", "SHOWMULTI")
                                'setSharedData(oForm, "SHOWACTIVITY", "FALSE")
                                'goParent.doOpenModalForm("IDHLOSCH", oForm)
                            ElseIf sField9 = "*" Then 'Driver
                                setSharedData(oForm, "IDH_DRIVNM", "")
                                setSharedData(oForm, "IDH_DRIVSN", "")
                                setSharedData(oForm, "IDH_VEHREG", "")
                                setSharedData(oForm, "SHOWCREATE", "TRUE")
                                goParent.doOpenModalForm("IDHDRVSRC", oForm)
                            End If
                        End If
                    End If
                End If
            End If
            Return True
        End Function
        Private Sub doCheckForFilter(ByVal oForm As SAPbouiCOM.Form)
            If (oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE) Then
                If Messages.INSTANCE.doResourceMessageYN("GEDCOMT", Nothing, 1) = 1 Then
                    Dim oUpdateGrid As UpdateGrid
                    oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    If oUpdateGrid.doProcessData() = True Then
                        doLoadData(oForm)
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                    End If
                End If
            Else
                doLoadData(oForm)
            End If
        End Sub
        Private Sub doAMFilterCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            Dim oCombo As SAPbouiCOM.ComboBox

            oItem = oForm.Items.Item("IDH_FAMPM")
            oItem.DisplayDesc = True
            oCombo = CType(oItem.Specific, SAPbouiCOM.ComboBox)

            doClearValidValues(oCombo.ValidValues)

            oCombo.ValidValues.Add("", getTranslatedWord("Any"))
            oCombo.ValidValues.Add("AM", getTranslatedWord("AM"))
            oCombo.ValidValues.Add("PM", getTranslatedWord("PM"))

        End Sub
        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            If sModalFormType = "IDHZONSRC" Then
                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                Dim sValueNew As String = getSharedDataAsString(oForm, "IDH_CODE")
                Dim sValueOld As String = CType(oGridN.doGetFieldValue("U_IDHZPCD"), String)
                Dim sValueOut As String
                If sValueNew.Length <> 0 Then
                    sValueOld = Replace(sValueOld, "*", "")
                    If sValueOld.Length <> 0 Then
                        sValueOut = sValueOld + "," + sValueNew
                    Else
                        sValueOut = sValueNew
                    End If
                Else
                    sValueOut = Replace(sValueOld, "*", "")
                End If
                oGridN.doSetFieldValue("U_IDHZPCD", sValueOut)
            End If

            If sModalFormType = "IDHISRC" Then
                Dim sTarget As String = getSharedDataAsString(oForm, "TRG")
                If sTarget = "PROD" Then
                    Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    Dim sValue As String = getSharedDataAsString(oForm, "ITEMCODE")
                    oGridN.doSetFieldValue("U_IDHCCD", sValue)
                End If
            End If

            If sModalFormType = "IDHWISRC" Then
                Dim sTarget As String = getSharedDataAsString(oForm, "TP")
                If sTarget = "WAST" Then
                    Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    Dim sValue As String = getSharedDataAsString(oForm, "ITEMCODE")
                    oGridN.doSetFieldValue("U_IDHWCD", sValue)
                End If
            End If

            If sModalFormType = "IDHCHD" Then
                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                Dim sValue As String = getSharedDataAsString(oForm, "IDHWDAY")
                oGridN.doSetFieldValue("U_IDHWDAY", sValue)
            End If

            If sModalFormType = "IDHLOSCH" Then
                doHandleVehicleSearchResult(oForm, sLastButton)

                'Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                'Dim sValue As String = getSharedData(oForm, "REG")
                'oGridN.doSetFieldValue("U_IDHVHRG", sValue)
                'Dim sValue2 As String = getSharedData(oForm, "DRIVER")
                'oGridN.doSetFieldValue("U_IDHDRVR", sValue2)
            End If

            If sModalFormType = "IDHDRVSRC" Then
                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                Dim sDriver As String = getSharedDataAsString(oForm, "IDH_DRIVNM") & " " & getSharedDataAsString(oForm, "IDH_DRIVSN")
                oGridN.doSetFieldValue("U_IDHDRVR", sDriver)
            End If

            doClearSharedData(oForm)
        End Sub

#Region "Buffer Zone"
        Private Sub doLorrySearch(ByVal oForm As SAPbouiCOM.Form)
            If Config.ParameterAsBool("VMUSENEW", False) = False Then
                setSharedData(oForm, "IDH_VEHREG", "")
                setSharedData(oForm, "IDH_DRVR", "")
                setSharedData(oForm, "SHOWCREATE", "TRUE")
                setSharedData(oForm, "SILENT", "SHOWMULTI")
                setSharedData(oForm, "SHOWACTIVITY", "FALSE")
                goParent.doOpenModalForm("IDHLOSCH", oForm)
            Else
                doCreateLorrySearch(oForm)
            End If
        End Sub

        Private Sub doHandleVehicleSearchResult(ByVal oForm As SAPbouiCOM.Form, ByVal sLastButton As String)
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            Dim sValue As String = getSharedDataAsString(oForm, "REG")
            oGridN.doSetFieldValue("U_IDHVHRG", sValue)
            Dim sValue2 As String = getSharedDataAsString(oForm, "DRIVER")
            oGridN.doSetFieldValue("U_IDHDRVR", sValue2)
        End Sub

#End Region

#Region "New Lorry Search"
        Private Sub doCreateLorrySearch(ByVal oForm As SAPbouiCOM.Form)
            Dim oOOForm As com.uBC.forms.fr3.VehicleM = New com.uBC.forms.fr3.VehicleM(Nothing, oForm.UniqueID, Nothing)
            oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doLorrySelection)
            oOOForm.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doLorryCancel)

            oOOForm.doShowModal(oForm)
        End Sub

        Public Function doLorrySelection(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Dim oOOForm As com.uBC.forms.fr3.VehicleM = CType(oOForm, com.uBC.forms.fr3.VehicleM)
            Dim oVehMast As IDH_VEHMAS2 = oOOForm.VehicleMaster

            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oOForm.SBOParentForm, "LINESGRID")
            oGridN.doSetFieldValue("U_IDHVHRG", oVehMast.U_VehReg)
            oGridN.doSetFieldValue("U_IDHDRVR", oVehMast.U_Driver)
            Return True
        End Function

        Public Function doLorryCancel(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Return True
        End Function
#End Region

    End Class
End Namespace
