Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class CLocTypes
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_WTLOTP", sParMenu, iMenuPosition, Nothing, "Container Location Types" )
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Container Location Types"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_WTLOTP"
        End Function

    End Class
End Namespace
