Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class Zones
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHZONE", sParMenu, iMenuPosition, Nothing, "Zones Setup" )
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Zones Setup"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_ZONES"
        End Function

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ZpCd", "ZipCode", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Desc", "Description", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Band", "Band", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ConChrg", "Congestion Apply", True, -1, "CHECKBOX", Nothing)
        End Sub

    End Class
End Namespace
