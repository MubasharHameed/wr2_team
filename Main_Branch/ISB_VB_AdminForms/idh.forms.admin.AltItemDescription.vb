﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Namespace idh.forms.admin
    Public Class AltItemDescription
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_ALTITM", sParMenu, iMenuPosition, Nothing, "Alternative Item Description")
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_ITMDESC"
        End Function

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ItemCode", "Item Code", True, -1, "SRC*IDHISRC(PROD)[IDH_ITMCOD=#U_ItemCode;][U_ItemCode=ITEMCODE;U_ItemName=ITEMNAME]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("U_ItemName", "Item Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ItemAltName", "Alternative Description", True, -1, Nothing, Nothing)
        End Sub

        Public Overrides Function doCustomItemEvent(oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED AndAlso pVal.BeforeAction = False Then
                If (pVal.ColUID = "U_ItemCode") Then
                    Dim sICode As String = pVal.oGrid.doGetFieldValue("U_ItemCode", pVal.Row).ToString()
                    Dim sIName As String = pVal.oGrid.doGetFieldValue("U_ItemName", pVal.Row).ToString()
                    If sICode.Trim = "" AndAlso sIName.Trim <> "" Then
                        pVal.oGrid.doSetFieldValue("U_ItemName", pVal.Row, "")
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SEARCH AndAlso pVal.oData.GetType.Name.ToString = "en_DialogResults" Then
                If (pVal.ColUID = "U_ItemCode" AndAlso DirectCast(pVal.oData, com.idh.bridge.lookups.Base.en_DialogResults) = com.idh.bridge.lookups.Base.en_DialogResults.CANCELED) Then
                    pVal.oGrid.doSetFieldValue("U_ItemCode", pVal.Row, "")
                    pVal.oGrid.doSetFieldValue("U_ItemName", pVal.Row, "")
                    Return False
                End If
            End If

            Return MyBase.doCustomItemEvent(oForm, pVal)
        End Function

    End Class
End Namespace
