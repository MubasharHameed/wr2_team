﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.dbObjects.User
Imports com.idh.bridge

Namespace idh.forms.admin
    Public Class WasteGrpJobLink
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_WGRPJLK", sParMenu, iMenuPosition, "WasteGrpJobLink.srf", "Link Waste Group and Job Type")
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_WGRPJLK"
        End Function
        
        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)
            
            doWasteGroupCombo(oForm)
            doJobTypeCombo(oForm)
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
        End Sub
        
        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
            doGridWasteGroupCombo(oForm)
            doGridJobTypeCombo(oForm)
        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

        Protected Overrides Sub doSetFilterFields( ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddFilterField("IDH_WSTGRP", "U_WstGpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30)
            oGridN.doAddFilterField("IDH_JOBTP", "U_JobTp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30)
        End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "	Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("Name", "	Name", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_WstGpCd", "Waste Group", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_JobTp", "Job Type", True, -1, "COMBOBOX", Nothing)
        End Sub
        
        Private Sub doGridWasteGroupCombo(ByVal oForm As SAPbouiCOM.Form)
        	Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_WstGpCd")), SAPbouiCOM.ComboBoxColumn)
            	oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
        		
        		doFillCombo(oForm, oCombo, "[@ISB_WASTEGRP]", "U_WstGpCd", "U_WstGpNm", Nothing, "U_WstGpCd", "Any", "")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Waste Groups.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Waste Groups")})
            End Try
        End Sub

        Private Sub doGridJobTypeCombo(ByVal oForm As SAPbouiCOM.Form)
			Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_JobTp")), SAPbouiCOM.ComboBoxColumn)
            	oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
        		
        		doFillCombo(oForm, oCombo, "[@IDH_JOBTYPE]", "U_JobTp", Nothing, Nothing, Nothing, "Any", "")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Job Type Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Job Type")})
            End Try
        End Sub

        Private Sub doWasteGroupCombo(ByVal oForm As SAPbouiCOM.Form)
        	doFillCombo(oForm, "IDH_WSTGRP","[@ISB_WASTEGRP]", "U_WstGpCd", "U_WstGpNm", Nothing, "U_WstGpCd", True)
        End Sub

        Private Sub doJobTypeCombo(ByVal oForm As SAPbouiCOM.Form)
        	doFillCombo(oForm, "IDH_JOBTP","[@IDH_JOBTYPE]", IDH_JOBTYPE._JobTp, Nothing, Nothing, Nothing, True)
        End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
        End Sub

		'** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                If pVal.BeforeAction = False Then
                	Dim sItem As String = pVal.ItemUID
                    If sItem = "IDH_WSTGRP" OrElse sItem = "IDH_JOBTP" Then
                        doLoadData(oForm)
                    End If
                End If
            End If
            Return True
        End Function

    End Class
End Namespace