﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge

Namespace idh.forms.admin
    Public Class ContainerDetails
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_CONTDET", sParMenu, iMenuPosition, Nothing, "Container Details")
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_CONTDET"
        End Function

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_ItemGrp", "Item Group", True, -1, "COMBOBOX", Nothing)

            oGridN.doAddListField("U_ItemCode", "Item Code", True, -1, "SRC*IDHISRC(PROD)[IDH_ITMCOD=#U_ItemCode;IDH_GRPCOD=#U_ItemGrp][U_ItemCode=ITEMCODE;U_ItemName=ITEMNAME]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("U_ItemName", "Item Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ContDet", "Container Details", True, -1, Nothing, Nothing)
        End Sub

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
            doGetItemGroups(oForm)
        End Sub

        Public Overrides Function doCustomItemEvent(oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED AndAlso pVal.BeforeAction = False Then
                If (pVal.ColUID = "U_ItemCode") Then
                    Dim sICode As String = pVal.oGrid.doGetFieldValue("U_ItemCode", pVal.Row).ToString()
                    Dim sIName As String = pVal.oGrid.doGetFieldValue("U_ItemName", pVal.Row).ToString()
                    If sICode.Trim = "" AndAlso sIName.Trim <> "" Then
                        pVal.oGrid.doSetFieldValue("U_ItemName", pVal.Row, "")
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SEARCH AndAlso pVal.oData.GetType.Name.ToString = "en_DialogResults" Then
                If (pVal.ColUID = "U_ItemCode" AndAlso DirectCast(pVal.oData, com.idh.bridge.lookups.Base.en_DialogResults) = com.idh.bridge.lookups.Base.en_DialogResults.CANCELED) Then
                    pVal.oGrid.doSetFieldValue("U_ItemCode", pVal.Row, "")
                    pVal.oGrid.doSetFieldValue("U_ItemName", pVal.Row, "")
                    Return False
                End If
            End If

            Return MyBase.doCustomItemEvent(oForm, pVal)
        End Function

        Public Sub doGetItemGroups(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_ItemGrp")), SAPbouiCOM.ComboBoxColumn)
            Catch ex As Exception
                Exit Sub
            End Try
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            oCombo.ValidValues.Add("", getTranslatedWord("Any"))
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                '...                oRecordSet = goParent.goDB.doSelectQuery("select DISTINCT ItmsGrpCod, ItmsGrpNam from OITB g, [@IDH_JOBTYPE] jt WHERE g.ItmsGrpNam=jt.U_ItemGrp")
                oRecordSet = goParent.goDB.doSelectQuery("select DISTINCT ItmsGrpCod, ItmsGrpNam from OITB g, [@IDH_JOBTYPE] jt WHERE g.ItmsGrpCod=jt.U_ItemGrp")
                While Not oRecordSet.EoF
                    Try
                        oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(1).Value, String))
                    Catch ex As Exception
                    End Try
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling Item Groups Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Groups")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

    End Class
End Namespace
