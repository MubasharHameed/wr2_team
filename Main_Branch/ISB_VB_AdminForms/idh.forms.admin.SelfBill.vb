﻿Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class SelfBill
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_SELFBL", sParMenu, iMenuPosition, Nothing, "Self Billing" )
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_SELFBILL"
        End Function
        
        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Self Bill Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("Name", "Self Bill Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_CustCd", "Customer Code", True, -1, "SRC*IDHCSRCH(CUS01)[IDH_BPCOD;IDH_TYPE=F-C][CARDCODE]", Nothing)
            oGridN.doAddListField("U_SuppCd", "Supplier Code", True, -1, "SRC*IDHCSRCH(SUP01)[IDH_BPCOD;IDH_TYPE=F-S][CARDCODE]", Nothing)
            oGridN.doAddListField("U_Comment", "Comment", True, -1, Nothing, Nothing)
        End Sub        
        
    End Class
End Namespace
