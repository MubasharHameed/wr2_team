﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Imports com.idh.dbObjects.User

Namespace idh.forms.admin
    Public Class Distance
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHMILEA", sParMenu, iMenuPosition, Nothing, "Distance")
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_MILEAGES"
        End Function

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", True, 0, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_SupZip", "Supplier Zip Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_CustZip", "Customer Zip Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Distance", "Distance", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Duration", "Duration", True, -1, Nothing, Nothing)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)

            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
            End If

            oGridN.setOrderValue("Code")
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            MyBase.doButtonID1(oForm, pVal, BubbleEvent)

            If pVal.BeforeAction = False Then
            End If
        End Sub


        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
        End Sub
    End Class
End Namespace

