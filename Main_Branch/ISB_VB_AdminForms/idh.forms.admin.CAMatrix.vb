Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System
Imports com.idh.bridge.resources

Imports com.idh.bridge

Namespace idh.forms.admin
    Public Class CAMatrix
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHCAMAT", sParMenu, iMenuPosition, "CAMatrix.srf", "Competent Authority Matrix")
            msKeyGen = "CAMSEQ"
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_CAMATRIX"
        End Function

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
            doDocumentCombo(oForm)
        End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ExpCACd", "Export Authority", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ImpCACd", "Import Authority", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_DocCd", "Document Code", True, -1, "COMBOBOX", Nothing)

        End Sub

        Protected Overrides Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddFilterField("IDH_EXCACD", "U_ExpCACd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_IMCACD", "U_ImpCACd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_DOCNUM", "U_DocCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)

        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_CACode" Then
                        If pVal.ItemChanged Then
                            doCheckForFilter(oForm)
                        End If
                    End If
                End If
            End If
            Return True
        End Function

        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            'If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED Then
            '    If pVal.BeforeAction = True Then
            '        Dim sField As String
            '        sField = pVal.oData

            '        Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            '        Dim sValue As String
            '        'If oGridN.doCheckIsSameCol(sField, "U_ItemCd") Then
            '        '    Dim sGrp As String
            '        '    sValue = oGridN.doGetFieldValue(sField)
            '        '    sGrp = oGridN.doGetFieldValue("U_ItmGrp")

            '        '    setSharedData(oForm, "TRG", "PROD")
            '        '    setSharedData(oForm, "IDH_ITMCOD", sValue)
            '        '    setSharedData(oForm, "IDH_GRPCOD", sGrp)
            '        '    goParent.doOpenModalForm("IDHISRC", oForm)

            '        '    'Dim oData As New ArrayList
            '        '    'oData.Add("PROD")
            '        '    'oData.Add(sValue)
            '        '    'oData.Add(sGrp)
            '        '    'oData.Add("")
            '        '    'goParent.doOpenModalForm("IDHISRCH", oForm, oData)
            '        'ElseIf oGridN.doCheckIsSameCol(sField, "U_CNTCd") Then
            '        '    sValue = oGridN.doGetFieldValue("U_CNTCd")
            '        '    setSharedData(oForm, "TRG", "CUST")
            '        '    setSharedData(oForm, "IDH_BPCOD", sValue)
            '        '    setSharedData(oForm, "IDH_TYPE", "F-C")
            '        '    goParent.doOpenModalForm("IDHCSRCH", oForm)
            '        'End If
            '    End If
            'ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_ADD_EMPTY Then
            '    If pVal.BeforeAction = False Then
            '        'Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            '        'oGridN.doSetFieldValue("U_Factor", "1")
            '    End If
            'End If
            Return MyBase.doCustomItemEvent(oForm, pVal)
            'Return True
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            If sModalFormType = "IDHCSRCH" Then
                Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                Dim sValue As String = getSharedData(oForm, "CARDCODE").ToString()
                oGridN.doSetFieldValue("U_CustCd", sValue)
            ElseIf sModalFormType = "IDHISRC" Then
                Dim sTarget As String = getSharedData(oForm, "TRG").ToString()
                If sTarget = "PROD" Then
                    Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                    Dim sValue As String = getSharedData(oForm, "ITEMCODE").ToString()
                    oGridN.doSetFieldValue("U_ItemCd", sValue)
                End If
            End If
            doClearSharedData(oForm)
        End Sub

        Protected Overrides Sub doReturnCanceled(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String)
            Try
                Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                If sModalFormType = "IDHISRC" Then
                    oGridN.doSetFieldValue("U_ItemCd", oGridN.moLastFieldValue)
                ElseIf sModalFormType = "IDHCSRCH" Then
                    oGridN.doSetFieldValue("U_CustCd", oGridN.moLastFieldValue)
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Cancled Modal Form - " & sModalFormType)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCMR", {sModalFormType})
            End Try
        End Sub

        Private Sub doCheckForFilter(ByVal oForm As SAPbouiCOM.Form)
            If (oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE) Then
                If Messages.INSTANCE.doResourceMessageYN("GEDCOMT", Nothing, 1) = 1 Then
                    Dim oUpdateGrid As UpdateGrid
                    oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    If oUpdateGrid.doProcessData() = True Then
                        doLoadData(oForm)
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                    End If
                End If
            Else
                doLoadData(oForm)
            End If
        End Sub

        Private Sub doDocumentCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_DocCd")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)

            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDB.doSelectQuery("SELECT Code, Name FROM [@IDH_CADOCS]")
                While Not oRecordSet.EoF
                    oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(1).Value, String))
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the CA Documents Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("CA Documents")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

    End Class
End Namespace
