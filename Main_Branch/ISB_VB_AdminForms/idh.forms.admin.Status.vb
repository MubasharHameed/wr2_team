Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class Status
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_WDSTA", sParMenu, iMenuPosition, Nothing, "WO/DO Statuses" )
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_WRSTATUS"
        End Function

    End Class
End Namespace