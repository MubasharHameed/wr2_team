Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class EWC
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_WTEWC", sParMenu, iMenuPosition, Nothing, "EWC Codes" )
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_WTEWC"
        End Function

    End Class
End Namespace