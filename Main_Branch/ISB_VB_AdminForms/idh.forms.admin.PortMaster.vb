Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Imports com.idh.dbObjects.User
Imports com.idh.bridge

Namespace idh.forms.admin
    Public Class PortMaster
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "ISBPORTMST", sParMenu, iMenuPosition, Nothing, "Port Master")
            '**********************************************************
            '******OnTime [#Ico000????] USA  **************************
            '**********************************************************
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "ISB_PORTMASTR"
        End Function

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", True, 0, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_PortCd", "Port Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_PortNm", "Port Name", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_SiteAdd", "Site Address", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_WstGpCd", "Waste Group", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_VendorCd", "Vendor Code", True, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("U_VendorNm", "Vendor Name", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_UOM", "UOM", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_CostTrf", "Cost Tariff", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ChargeTrf", "Charge Tariff", True, -1, Nothing, Nothing)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)

            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
            End If

            oGridN.setOrderValue("Code")
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            MyBase.doButtonID1(oForm, pVal, BubbleEvent)

            If pVal.BeforeAction = False Then
                'IDH_JOBTYPE_SEQ.getInstance(true)
            End If
        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            'Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If pVal.BeforeAction = False Then
                    If pVal.CharPressed = 9 Then
                        If pVal.ItemUID = "LINESGRID" Then
                            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                            Dim sSiteAdd As String = CType(oGridN.doGetFieldValue("U_SiteAdd"), String)
                            Dim sVendorCd As String = CType(oGridN.doGetFieldValue("U_VendorCd"), String)
                            If sVendorCd = "*" Then
                                setSharedData(oForm, "SILENT", "SHOWMULTI")
                                setSharedData(oForm, "IDH_TYPE", "S")
                                goParent.doOpenModalForm("IDHCSRCH", oForm)
                            ElseIf sSiteAdd = "*" Then
                                setSharedData(oForm, "IDH_ADRTYP", "S")
                                goParent.doOpenModalForm("IDHASRCH", oForm)
                            End If
                        End If
                    End If
                End If
            End If
            Return True
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            If sModalFormType = "IDHCSRCH" Then
                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                Dim sCardCode As String = getSharedDataAsString(oForm, "CARDCODE")
                Dim sCardName As String = getSharedDataAsString(oForm, "CARDNAME")

                'Dim sValueNew As String = getSharedData(oForm, "IDH_CODE")
                'Dim sValueOld As String = oGridN.doGetFieldValue("U_IDHZPCD")
                'Dim sValueOut As String
                'If sValueNew.Length <> 0 Then
                '    sValueOld = Replace(sValueOld, "*", "")
                '    If sValueOld.Length <> 0 Then
                '        sValueOut = sValueOld + "," + sValueNew
                '    Else
                '        sValueOut = sValueNew
                '    End If
                'Else
                '    sValueOut = Replace(sValueOld, "*", "")
                'End If
                oGridN.doSetFieldValue("U_VendorCd", sCardCode)
                oGridN.doSetFieldValue("U_VendorNm", sCardName)
            ElseIf sModalFormType = "IDHASRCH" Then
                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                Dim sCardCode As String = getSharedDataAsString(oForm, "CARDCODE")
                Dim sAddress As String = getSharedDataAsString(oForm, "ADDRESS")
                oGridN.doSetFieldValue("U_SiteAdd", sAddress)
            End If
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
            doUOMCombo(oForm)
            doWasteGroupCombo(oForm)
        End Sub

        Private Sub doUOMCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_UOM")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            'oCombo.ValidValues.Add("", getTranslatedWord("Any"))
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDB.doSelectQuery("select UnitDisply, UnitName from OWGT")
                While Not oRecordSet.EoF
                    Try
                        oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(1).Value, String))
                    Catch ex As Exception
                    End Try
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the UOM combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("UOM")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

        Private Sub doWasteGroupCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_WstGpCd")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            'oCombo.ValidValues.Add("", getTranslatedWord("Any"))
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDB.doSelectQuery("select U_WstGpCd, U_WstGpNm from [@ISB_WASTEGRP]")
                While Not oRecordSet.EoF
                    Try
                        oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(1).Value, String))
                    Catch ex As Exception
                    End Try
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the UOM combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("UOM")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

    End Class
End Namespace
