﻿Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports com.idh.bridge
Imports com.idh.dbObjects.numbers

Namespace idh.forms.admin
    Public Class Container
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base)
            MyBase.New(oParent, "IDH_WOCNTLST", Nothing, -1, Nothing, "Container List")
            msKeyGen = "WOCNTLST"
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_WOCNTLST"
        End Function

        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY Then
                Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                Dim oNumbers As NumbersPair = doSetKeyValue(oForm)
                oGridN.doSetFieldValue("Code", pVal.Row, oNumbers.CodeCode, True)
                oGridN.doSetFieldValue("Name", pVal.Row, oNumbers.NameCode, True)
                oGridN.doSetFieldValue("U_RowNr", getParentSharedData(oForm, "IDH_WOR"))
                oGridN.doSetFieldValue("U_Action", getParentSharedData(oForm, "IDH_ACT"))
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                Return True
            End If
            Return MyBase.doCustomItemEvent(oForm, pVal)
        End Function

        Protected Overrides Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddWFFilterField("IDH_WOR", "U_RowNr", "=", 30, "")
            oGridN.doAddWFFilterField("IDH_ACT", "U_Action", "=", 30, "")
        End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", False, 30, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("U_RowNr", "WO Row", False, 30, Nothing, Nothing)
            oGridN.doAddListField("U_ContNr", "Container", True, 380, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_Action", "Action", False, 10, Nothing, Nothing)
        End Sub

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
            doGetContainerDetails(oForm)
        End Sub

        Public Sub doGetContainerDetails(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")

            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_ContNr")), SAPbouiCOM.ComboBoxColumn)
            Catch ex As Exception
                Exit Sub
            End Try
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            oCombo.ValidValues.Add("", getTranslatedWord("Any"))
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                Dim sContCd As String = getParentSharedDataAsString(oForm, "IDH_CNT")
                Dim sQry As String = String.Empty
                If sContCd IsNot Nothing AndAlso sContCd.Length > 0 Then
                    sQry = "select Code, U_ContDet FROM [@IDH_CONTDET] where U_ItemCode = '" + sContCd + "'"
                Else
                    sQry = "select Code, U_ContDet FROM [@IDH_CONTDET]"
                End If

                oRecordSet = goParent.goDB.doSelectQuery(sQry)
                While Not oRecordSet.EoF
                    Try
                        oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(1).Value, String))
                    Catch ex As Exception
                    End Try
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling Container Details Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Container Details")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

    End Class
End Namespace
