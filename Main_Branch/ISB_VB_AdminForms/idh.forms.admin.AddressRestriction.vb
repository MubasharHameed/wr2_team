Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge.resources
Imports com.idh.bridge
Imports com.idh.utils

Namespace idh.forms.admin
    Public Class AddressRestriction
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHADDR", sParMenu, iMenuPosition, "LockedSitesAdmin.srf", "Address Restriction")
            ' msKeyGen = "ADDRST"
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_ADDRST"
        End Function

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
        End Sub


        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)
            doUserComboFilter(oForm)
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
            doUserCombo(oForm)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            oGridN.doSetDeleteActive(True)
        End Sub
        Private Sub doUserCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_UserName")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)

            Dim oResult As com.idh.bridge.DataRecords
            Dim sQry As String = "Select USER_CODE,U_NAME from OUSR"
            oResult = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("doGetSBOUsers", sQry)
            If oResult.RecordCount > 0 Then
                For i As Int32 = 0 To oResult.RecordCount - 1
                    oResult.gotoRow(i)
                    oCombo.ValidValues.Add(oResult.getValueAsString("USER_CODE"), oResult.getValueAsString("U_NAME"))
                Next
            End If
        End Sub
        Private Sub doUserComboFilter(ByVal oForm As SAPbouiCOM.Form)
            Dim oCombo_Filter As SAPbouiCOM.ComboBox
            oCombo_Filter = CType(oForm.Items.Item("IDH_USER").Specific, SAPbouiCOM.ComboBox)

            doClearValidValues(oCombo_Filter.ValidValues)
            oCombo_Filter.ValidValues.Add("%", "All")

            Dim oResult As com.idh.bridge.DataRecords
            Dim sQry As String = "Select USER_CODE,U_NAME from OUSR"
            oResult = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("doGetSBOUsers", sQry)
            If oResult.RecordCount > 0 Then
                For i As Int32 = 0 To oResult.RecordCount - 1
                    oResult.gotoRow(i)
                    oCombo_Filter.ValidValues.Add(oResult.getValueAsString("USER_CODE"), oResult.getValueAsString("U_NAME"))
                Next
            End If
        End Sub
        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)

            'doWFFilterCombo(oForm)
        End Sub

        Protected Overrides Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddFilterField("IDH_CUST", "U_CustCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30)
            oGridN.doAddFilterField("IDH_USER", "U_UserName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 50)

        End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", True, -1, Nothing, Nothing)

            oGridN.doAddListField("U_UserCode", "User", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_User)
            oGridN.doAddListField("U_UserName", "User", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_CustCode", "Business Partner Code", True, -1, "SRC*IDHCSRCH(BP)[IDH_BPCOD;IDH_TYPE=%][CARDCODE;U_CustName=CARDNAME]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("U_CustName", "Business Partner Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Address", "Address", True, -1, "SRC*IDHASRCH(CADDR)[IDH_CUSCOD=#U_CustCode;IDH_ADRTYP=%S][ADDRESS;U_AddType=ADRESTYPE;U_AddrLN=ADDRSCD]", Nothing)
            oGridN.doAddListField("U_AddType", "Address Type", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_AddrLN", "Address Line Num", True, 0, Nothing, Nothing)

            '"SRC*IDHASRCH(ADDR)[IDH_CUSCOD=#U_CustCode][ADDRESS]", Nothing)
            
        End Sub

        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            'Clear the description field if code is empty
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED AndAlso pVal.BeforeAction = False Then
                If (pVal.ColUID = "U_CustCode") Then
                    Dim sCustCode As String = CType(pVal.oGrid.doGetFieldValue("U_CustCode", pVal.Row), String)
                    Dim sCustName As String = CType(pVal.oGrid.doGetFieldValue("U_CustName", pVal.Row), String)
                    If sCustCode.Trim = "" AndAlso sCustName.Trim <> "" Then
                        pVal.oGrid.doSetFieldValue("U_CustName", pVal.Row, "")

                        pVal.oGrid.doSetFieldValue("U_Address", pVal.Row, "")
                        pVal.oGrid.doSetFieldValue("U_AddType", pVal.Row, "")
                        pVal.oGrid.doSetFieldValue("U_AddrLN", pVal.Row, "")

                    End If
                ElseIf (pVal.ColUID = "U_Address") Then
                    Dim sCustCode As String = CType(pVal.oGrid.doGetFieldValue("U_CustCode", pVal.Row), String)
                    Dim aAddress As String = CType(pVal.oGrid.doGetFieldValue("U_Address", pVal.Row), String)
                    Dim sAddressType As String = CType(pVal.oGrid.doGetFieldValue("U_AddType", pVal.Row), String)
                    If sCustCode.Trim <> "" AndAlso aAddress IsNot Nothing AndAlso sAddressType IsNot Nothing AndAlso aAddress.Trim <> "" AndAlso sAddressType <> "" Then
                        Dim sAddressAddrsCd As String
                        If sAddressType = "S" Then
                            sAddressAddrsCd = Conversions.ToString(com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPShipToAddress(sCustCode, aAddress, "U_AddrsCd"))
                        Else
                            sAddressAddrsCd = Conversions.ToString(com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPBIllToAddress(sCustCode, aAddress, "U_AddrsCd"))
                        End If
                        'Dim sAddressLineNum As String = com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPShipToAddress(sCustCode, aAddress, "LineNum")
                        pVal.oGrid.doSetFieldValue("U_AddrLN", sAddressAddrsCd)
                    Else
                        pVal.oGrid.doSetFieldValue("U_AddrLN", "")
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SEARCH AndAlso pVal.oData.GetType.Name.ToString = "en_DialogResults" Then
                If (pVal.ColUID = "U_CustCode" AndAlso DirectCast(pVal.oData, com.idh.bridge.lookups.Base.en_DialogResults) = com.idh.bridge.lookups.Base.en_DialogResults.CANCELED) Then
                    pVal.oGrid.doSetFieldValue("U_CustCode", pVal.Row, "")
                    pVal.oGrid.doSetFieldValue("U_CustName", pVal.Row, "")
                    '  Dim sCustCode As String = pVal.oGrid.doGetFieldValue("U_CustCode", pVal.Row)
                    '  Dim sCustName As String = pVal.oGrid.doGetFieldValue("U_CustName", pVal.Row)
                    pVal.oGrid.doSetFieldValue("U_Address", pVal.Row, "")
                    pVal.oGrid.doSetFieldValue("U_AddType", pVal.Row, "")
                    pVal.oGrid.doSetFieldValue("U_AddrLN", pVal.Row, "")
                    Return False
                End If

            End If

            Return MyBase.doCustomItemEvent(oForm, pVal)
        End Function
        '## End
        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Sub doButtonID1(oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction Then
                'Validate form
                Dim oUpdateGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                Dim aRows As ArrayList = oUpdateGrid.doGetChangedRows
                If arows IsNot Nothing Then
                    Dim sUser, sAddress, sCustCode As String
                    For iIndex As Integer = 0 To aRows.Count - 1
                        oUpdateGrid.setCurrentDataRowIndex(CType(aRows(iIndex), Integer))
                        sUser = CType(oUpdateGrid.doGetFieldValue("U_UserName"), String)
                        sCustCode = CType(oUpdateGrid.doGetFieldValue("U_CustCode"), String)
                        sAddress = CType(oUpdateGrid.doGetFieldValue("U_Address"), String)
                        If sCustCode.Trim <> "" Then
                            If sAddress.Trim = "" Then
                                'com.idh.bridge.DataHandler.INSTANCE.doError("Customer address required.")
                                DataHandler.INSTANCE.doResUserError("Customer address required.", "ERUSSCA", {Nothing})
                                BubbleEvent = False
                                Exit Sub
                            ElseIf sUser.Trim = "" Then
                                'com.idh.bridge.DataHandler.INSTANCE.doError("Please select user.")
                                DataHandler.INSTANCE.doResUserError("Please select user.", "EREXUSER", {Nothing})
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If
                    Next
                End If
            End If
            MyBase.doButtonID1(oForm, pVal, BubbleEvent)
        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_CUST" OrElse pVal.ItemUID = "IDH_USER" Then
                        If pVal.ItemChanged Then
                            doCheckForFilter(oForm)
                        End If
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_USER" Then
                        doCheckForFilter(oForm)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_CUST" OrElse pVal.ItemUID = "IDH_USER" Then
                        If pVal.CharPressed = 13 Then
                            goParent.goApplication.SendKeys("{TAB}")
                        End If
                    End If
                End If
            End If
            Return True
        End Function
        'Protected Overrides Sub doReturnCanceled(oParentForm As SAPbouiCOM.Form, sModalFormType As String)
        '    If sModalFormType = "IDHCSRCH" Then
        '        Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oParentForm, "LINESGRID")
        '        Return
        '    End If
        '    MyBase.doReturnCanceled(oParentForm, sModalFormType)
        'End Sub
        

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType = "IDHCSRCH" Or sModalFormType = "IDHNCSRCH" Then
                    Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    Dim sTarget As String = getSharedDataAsString(oForm, "TRG")
                    Dim valCd As String = getSharedDataAsString(oForm, "CARDCODE")
                    Dim valNm As String = getSharedDataAsString(oForm, "CARDNAME")
                    If Not (valCd Is Nothing) AndAlso valCd.Length > 0 Then
                        '## Start 18-09-2013
                        If sTarget = "BP" Then
                            oGridN.doSetFieldValue("U_CustCode", valCd)
                            oGridN.doSetFieldValue("U_CustName", valNm)
                        End If
                        doSetUpdate(oForm)
                    End If
                ElseIf sModalFormType = "IDHASRCH" OrElse sModalFormType = "IDHASRCH3" Then
                    Dim sTarget As String
                    sTarget = getSharedDataAsString(oForm, "TRG")
                    If sTarget.Equals("ADDR") Then
                        Dim AddrType As String = getSharedDataAsString(oForm, "ADRESTYPE")

                        'Dim valNm As String = getSharedData(oForm, "CARDNAME")
                        Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                        oGridN.doSetFieldValue("U_AddType", AddrType)
                        doSetUpdate(oForm)
                        'doSetFocus("IDH_ADDR")
                        '    setUFValue("IDH_ADDR", "")
                        '    setUFValue("IDH_POSCOD", "")
                        '    doLoadData()
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal Result - " & sModalFormType)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub

        Private Sub doCheckForFilter(ByVal oForm As SAPbouiCOM.Form)
            If (oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE) Then
                If Messages.INSTANCE.doResourceMessageYN("GEDCOMT", Nothing, 1) = 1 Then
                    Dim oUpdateGrid As UpdateGrid
                    oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    If oUpdateGrid.doProcessData() = True Then
                        doLoadData(oForm)
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                    End If
                End If
            Else
                doLoadData(oForm)
            End If
        End Sub
    End Class
End Namespace
