Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge

Namespace idh.forms.admin
    Public Class JobDefaults
        Inherits idh.forms.admin.Tmpl
        Private iFilterBackColor As Integer = &HFFCCCC

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHJTDE", sParMenu, iMenuPosition, Nothing, "Job Defaults" )
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Job Defaults"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_JOBDEF"
        End Function

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", True, 0, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", True, 0, Nothing, Nothing)

            oGridN.doAddListField("U_WR1Ord", "WR1 Order Type", True, -1, "COMBOBOX", Nothing, iFilterBackColor)
            oGridN.doAddListField("U_CustCd", "Customer", True, -1, Nothing, Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("U_ItmGrp", "Container Group", True, -1, "COMBOBOX", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_ItemGroups)
            oGridN.doAddListField("U_ItemCd", "Container", True, -1, Nothing, Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("U_JobTp", "Order Type", True, -1, "COMBOBOX", Nothing, iFilterBackColor)
            oGridN.doAddListField("U_CarrCd", "Carrier", True, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("U_Tip", "Disposal Site", True, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
        End Sub

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
            doFillWR1TypeCombo(oForm)
            doFillJobTypeCombo(oForm)
            doGetItemGroups(oForm)
        End Sub

        Public Sub doGetItemGroups(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_ItmGrp")), SAPbouiCOM.ComboBoxColumn)
            Catch ex As Exception
                Exit Sub
            End Try
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            oCombo.ValidValues.Add("", getTranslatedWord("Any"))
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
'...                oRecordSet = goParent.goDB.doSelectQuery("select DISTINCT ItmsGrpCod, ItmsGrpNam from OITB g, [@IDH_JOBTYPE] jt WHERE g.ItmsGrpNam=jt.U_ItemGrp")
				oRecordSet = goParent.goDB.doSelectQuery("select DISTINCT ItmsGrpCod, ItmsGrpNam from OITB g, [@IDH_JOBTYPE] jt WHERE g.ItmsGrpCod=jt.U_ItemGrp")
                While Not oRecordSet.EoF
                    Try
                        oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(1).Value, String))
                    Catch ex As Exception
                    End Try
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Item Group Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Item Group")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

        Public Sub doFillWR1TypeCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_WR1Ord")), SAPbouiCOM.ComboBoxColumn)
            Catch ex As Exception
                Exit Sub
            End Try
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            oCombo.ValidValues.Add("", getTranslatedWord("ALL"))
            oCombo.ValidValues.Add("WO", getTranslatedWord("Waste Order"))
            oCombo.ValidValues.Add("DO", getTranslatedWord("Disposal Order"))
        End Sub

        Public Sub doFillJobTypeCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_JobTp")), SAPbouiCOM.ComboBoxColumn)
            Catch ex As Exception
                Exit Sub
            End Try
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            oCombo.ValidValues.Add("", getTranslatedWord("Any"))
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDB.doSelectQuery("select DISTINCT U_JobTp from [@IDH_JOBTYPE]")
                While Not oRecordSet.EoF
                    Try
                        oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(0).Value, String))
                    Catch ex As Exception
                    End Try
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Job Type Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Job Type")})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

        Protected Overridable Sub doChooseCustomer(ByVal oForm As SAPbouiCOM.Form, ByVal oGridN As UpdateGrid)
            setSharedData(oForm, "TRG", "CUS")
            setSharedData(oForm, "IDH_BPCOD", oGridN.doGetFieldValue("U_CustCd"))
            setSharedData(oForm, "IDH_TYPE", "F-C")
            setSharedData(oForm, "IDH_NAME", "")
            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub

        Protected Overridable Sub doChooseCarrier(ByVal oForm As SAPbouiCOM.Form, ByVal oGridN As UpdateGrid)
            setSharedData(oForm, "TRG", "WCC")
            setSharedData(oForm, "IDH_BPCOD", oGridN.doGetFieldValue("U_CarrCd"))
            setSharedData(oForm, "IDH_TYPE", "S")
            setSharedData(oForm, "IDH_NAME", "")
            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub

        Protected Overridable Sub doChooseSite(ByVal oForm As SAPbouiCOM.Form, ByVal oGridN As UpdateGrid)
            setSharedData(oForm, "TRG", "TIP")
            setSharedData(oForm, "IDH_BPCOD", oGridN.doGetFieldValue("U_Tip"))
            setSharedData(oForm, "IDH_TYPE", "S")
            setSharedData(oForm, "IDH_NAME", "")
            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub

        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "LINESGRID" Then
                        Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                        Dim iCurRow As Integer = oGridN.getGrid().GetDataTableRowIndex(pVal.Row)
                        oGridN.setCurrentDataRowIndex(iCurRow)
                        If oGridN.doCheckIsSameCol(pVal.ColUID, "U_CarrCd") Then
                            Dim sValue As String = CType(oGridN.doGetFieldValue("U_CarrCd"), String)
                            If sValue.Length() > 0 AndAlso sValue.Chars(0) = "*" Then
                                doChooseCarrier(oForm, oGridN)
                            End If
                        ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "U_CustCd") Then
                            Dim sValue As String = CType(oGridN.doGetFieldValue("U_CustCd"), String)
                            If sValue.Length() > 0 AndAlso sValue.Chars(0) = "*" Then
                                doChooseCustomer(oForm, oGridN)
                            End If
                        ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "U_Tip") Then
                            Dim sValue As String = CType(oGridN.doGetFieldValue("U_Tip"), String)
                            If sValue.Length() > 0 AndAlso sValue.Chars(0) = "*" Then
                                doChooseSite(oForm, oGridN)
                            End If
                        ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "U_ItemCd") Then
                            Dim sItem As String = CType(oGridN.doGetFieldValue("U_ItemCd"), String)
                            If sItem.Length() > 0 AndAlso sItem.Chars(0) = "*" Then
                                Dim sGrp As String = CType(oGridN.doGetFieldValue("U_ItmGrp"), String)
                                setSharedData(oForm, "TRG", "PROD")
                                setSharedData(oForm, "IDH_ITMCOD", sItem)
                                setSharedData(oForm, "IDH_GRPCOD", sGrp)
                                setSharedData(oForm, "SILENT", "SHOWMULTI")
                                goParent.doOpenModalForm("IDHISRC", oForm)
                            End If
                        End If
                        Return False
                    End If
                End If
            End If
            Return True
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType = "IDHISRC" Then
                    Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    Dim sTarget As String = getSharedDataAsString(oForm, "TRG")
                    Dim sItemCode As String = getSharedDataAsString(oForm, "ITEMCODE")
                    Dim sItemGrp As String = getSharedDataAsString(oForm, "ITMSGRPCOD")
                    Dim sGrp As String = CType(oGridN.doGetFieldValue("U_ItmGrp"), String)
                    If sGrp.Length() = 0 Then
                        oGridN.doSetFieldValue("U_ItmGrp", sItemGrp)
                    End If
                    oGridN.doSetFieldValue("U_ItemCd", sItemCode)

                ElseIf sModalFormType = "IDHCSRCH" Then
                    Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    Dim sTarget As String = getSharedDataAsString(oForm, "TRG")
                    Dim valCd As String = getSharedDataAsString(oForm, "CARDCODE")
                    Dim valNm As String = getSharedDataAsString(oForm, "CARDNAME")

                    If Not (valCd Is Nothing) AndAlso valCd.Length > 0 Then
                        If sTarget = "TIP" Then
                            oGridN.doSetFieldValue("U_Tip", valCd)
                        ElseIf sTarget = "WCC" Then
                            oGridN.doSetFieldValue("U_CarrCd", valCd)
                        ElseIf sTarget = "CUS" Then
                            oGridN.doSetFieldValue("U_CustCd", valCd)
                        End If
                        doSetUpdate(oForm)
                    End If
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal results.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {String.Empty})
            End Try
        End Sub
    End Class
End Namespace
