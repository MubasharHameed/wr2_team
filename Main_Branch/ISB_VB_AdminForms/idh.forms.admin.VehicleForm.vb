Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge
Imports com.idh.dbObjects.numbers

Namespace idh.forms
	Public Class VehicleForm
		Inherits IDHAddOns.idh.forms.Base

		Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
			'MyBase.New(oParent, "IDHVMA", iMenuPosition, "Vehicle Form.srf", False, True, False)
			MyBase.New(oParent, "IDHVMA", "IDHMAST", iMenuPosition, "Vehicle Form.srf", False, True, False, "Vehicle Form", load_Types.idh_LOAD_NORMAL)
			
            'doAddImagesToFix("IDH_BDSITE")
            'doAddImagesToFix("IDH_REGL")
            'doAddImagesToFix("IDH_CUSL")
            'doAddImagesToFix("IDH_WASCF")
            'doAddImagesToFix("IDH_ITCF")
            'doAddImagesToFix("IDH_CARL")
            '   doAddImagesToFix("IDH_DLF")
		    
			doEnableHistory("IDH_VEHMAS")
		End Sub
		
		Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
			oForm.Visible = True
		End Sub
		
'		'*** Create Sub-Menu
'		Protected Overrides Sub doCreateSubMenu()
'			Dim oMenus As SAPbouiCOM.Menus
'			Dim oMenuItem As SAPbouiCOM.MenuItem
'			Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
'			
'			oMenuItem = goParent.goApplication.Menus.Item("IDHMAST")
'			
'			oMenus = oMenuItem.SubMenus
'			oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
'			oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING
'			oCreationPackage.UniqueID = gsType
'			oCreationPackage.String = "Vehicle Form"
'			oCreationPackage.Position = giMenuPosition
'			
'			Try
'				oMenus.AddEx(oCreationPackage)
'			Catch ex As Exception
'			End Try
'		End Sub
		
		'*** Add event filters to avoid receiving all events from SBO
		Protected Overrides Sub doSetEventFilters()
			doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
			doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
			doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
			doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
		End Sub
		
		'** Create the form
		Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
			MyBase.doCompleteCreate(oForm, BubbleEvent)
			
			Try
				Dim oItem As SAPbouiCOM.Item
				
				oForm.DataSources.DBDataSources.Add("@IDH_VEHMAS")
				
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_REG", "U_VehReg")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_VT", "U_VehType")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_VDESC",  "U_VehDesc")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_DN",  "U_DrivrNr")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_DRIVER",  "U_DRIVER")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_TARE",  "U_Tarre")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_2TID",   "U_TRLReg")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_2TNM",  " U_TRLNM")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_2TW",   "U_TRLTar")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_CUST",  "U_CCrdCd")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_CUSTNM",  "U_CName")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_DSITE", "U_SCrdCd")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_SUPNM", "U_SName")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_WCC", "U_CCCrdCd")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_WCNM", "U_CCName")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_WCODE", "U_WasCd")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_WDEC", "U_WasDsc")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_CODE", "Code")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_NM", "Name")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_CCODE", "U_ItemCd")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_CONDEC", "U_ItemDsc")
				doAddDF(oForm, "@IDH_VEHMAS", "IDH_IGC",  "U_ItmGrp")
				doAddDF(oForm, "@IDH_VEHMAS","IDH_IGD",  "U_IGrpDesc")
				doAddDF(oForm, "@IDH_VEHMAS","IDH_WF",  "U_WFStat")
				doAddDF(oForm, "@IDH_VEHMAS","IDH_MDA",  "U_MarkAc")

                ''MA Start 04-02-2015
                'doAddDF(oForm, "@IDH_VEHMAS", "IDH_TRWTE1", "U_TRWTE1")
                'doAddDF(oForm, "@IDH_VEHMAS", "IDH_TRWTE2", "U_TRWTE2")
                ''MA End 04-02-2015

				oForm.DataBrowser.BrowseBy = "IDH_CODE"
				
'				Dim oEdit As SAPbouiCOM.EditText
'				Dim oButton As SAPbouiCOM.Button
				
				Dim oCombo As SAPbouiCOM.ComboBox
				oItem = oForm.Items.Item("IDH_VT")
				oItem.DisplayDesc = True
                oCombo = CType(oItem.Specific, SAPbouiCOM.ComboBox)
				oCombo.ValidValues.Add("A", "Asset")
				oCombo.ValidValues.Add("S", "Sub Contract")
				oCombo.ValidValues.Add("H", "Owner Driver")
				
				oItem = oForm.Items.Item("IDH_WF")
				oItem.DisplayDesc = True
                oCombo = CType(oItem.Specific, SAPbouiCOM.ComboBox)
				oCombo.ValidValues.Add("", getTranslatedWord("Any"))
				oCombo.ValidValues.Add("ReqAdd", getTranslatedWord("Request to Add"))
				oCombo.ValidValues.Add("ReqUpdate", getTranslatedWord("Request to Update"))
				oCombo.ValidValues.Add("Approved", getTranslatedWord("Request Approved"))
				oCombo.ValidValues.Add("Denied", getTranslatedWord("Request Denied"))
				
				oItem = oForm.Items.Item("IDH_MDA")
				oItem.DisplayDesc = True
                oCombo = CType(oItem.Specific, SAPbouiCOM.ComboBox)
				oCombo.ValidValues.Add("", getTranslatedWord("DK"))
				oCombo.ValidValues.Add("ReqAdd", getTranslatedWord("Request to Add"))
				oCombo.ValidValues.Add("ReqUpdate", getTranslatedWord("Request to Update"))
				oCombo.ValidValues.Add("Approved", getTranslatedWord("Request Approved"))
				oCombo.ValidValues.Add("Denied", getTranslatedWord("Request Denied"))
				
				oItem = oForm.Items.Item("IDH_IGC")
				oItem.DisplayDesc = True
                oCombo = CType(oItem.Specific, SAPbouiCOM.ComboBox)
				oCombo.ValidValues.Add("104", getTranslatedWord("Rolonof"))
				oCombo.ValidValues.Add("105", getTranslatedWord("Skip"))
				oCombo.ValidValues.Add("106", getTranslatedWord("Compactor"))
				oCombo.ValidValues.Add("107", getTranslatedWord("Wheelies"))
				oCombo.ValidValues.Add("111", getTranslatedWord("Cage"))
				oCombo.ValidValues.Add("112", getTranslatedWord("Dustcart"))
				
				oForm.SupportedModes = -1
				oForm.AutoManaged = False
				
				oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
				
			Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
			End Try
		End Sub
		
		Private Function doValidateHeader(ByVal oForm As SAPbouiCOM.Form) As Boolean
			Dim sCust As String
			
            sCust = CType(getDFValue(oForm, "@IDH_VEHMAS", "U_CCrdCd"), String)
			
			Dim sError As String = ""
			If sCust.Length = 0 Then sError = sError + "Customer, "
			
			If sError.Length > 0 Then
                'com.idh.bridge.DataHandler.INSTANCE.doError("The following fields must be set: " & sError)
                DataHandler.INSTANCE.doResUserError("The following fields must be set: " & sError, "ERUSGENS", {com.idh.bridge.Translation.getTranslatedWord(sError)})
				Return False
			End If
			Return True
		End Function
	  
	  Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
	  	If pVal.BeforeAction = False Then
                If pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_ADD Then
                    oForm.Freeze(True)
                    Try
                        'doRemoveLinkedDocs(oForm)
                        doCreateNewEntry(oForm)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the menu event - " & oForm.UniqueID & "." & pVal.MenuUID)
                        DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try

                ElseIf pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_FIND OrElse _
                  pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_FIRST OrElse _
                  pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_LAST OrElse _
                  pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_NEXT OrElse _
                  pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_PREV Then
                    oForm.Freeze(True)
                    Try
                        If pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_FIND Then
                            doClearAll(oForm)
                        Else
                            doLoadData(oForm)
                        End If
                    Catch ex As Exception
                    Finally
                        oForm.Freeze(False)
                    End Try
                End If
	  	Else
                If pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_ADD OrElse _
                   pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_FIND OrElse _
                   pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_FIRST OrElse _
                   pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_LAST OrElse _
                   pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_NEXT OrElse _
                   pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_PREV Then

                    Return True
                End If
            End If
            Return True
	  End Function
 
		  '** The Initializer
		  Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            Dim sCODE As String = CType(getDFValue(oForm, "@IDH_VEHMAS", "Code"), String)
		  	If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
		  		doSwitchToFind(oForm)
		  	ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
		  		doSwitchToAdd(oForm)
		  	ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
		  		doSwitchToAdd(oForm)
		  	End If
		  End Sub
			  
			   
			   Protected Sub doSwitchToFind(ByVal oForm As SAPbouiCOM.Form)
			   	Dim oExclude() As String = {"IDH_CUST","IDH_REG","IDH_CODE"}
			   	doSetFocus(oForm, "IDH_CUST")
			   	DisableAllEditItems(oForm, oExclude)
			   End Sub
			   
			   Protected Sub doSwitchToAdd(ByVal oForm As SAPbouiCOM.Form)
			   	
			   	Dim oExclude() As String = {"IDH_CODE","IDH_NM"}
			   	doSetFocus(oForm, "IDH_CUST")
			   	EnableAllEditItems(oForm, oExclude)
			   End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            Dim bDoNew As Boolean = False
            Dim sU_CODE As String
            If pVal.BeforeAction = True Then
                sU_CODE = CType(getDFValue(oForm, "@IDH_VEHMAS", "Code"), String)

                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                  oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    oForm.Freeze(True)
                    Try
                        If doValidateHeader(oForm) = False Then
                            BubbleEvent = False
                            Exit Sub
                        End If

                        Dim bDoContinue As Boolean = False
                        goParent.goDICompany.StartTransaction()

                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            If doUpdateHeader(oForm, True) = True Then
                                bDoContinue = True
                                bDoNew = True
                            End If
                        Else
                            If doUpdateHeader(oForm, False) = True Then
                                bDoContinue = True
                            End If
                        End If

                        If bDoContinue Then
                            If goParent.goDICompany.InTransaction Then
                                goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                            End If
                            com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
                            doLoadData(oForm)

                            'doRemoveFormRows(oForm.UniqueID)
                            If bDoNew Then
                                setWFValue(oForm, "LastContract", -1)
                                If goParent.doCheckModal(oForm.UniqueID) = False Then
                                    doCreateNewEntry(oForm)
                                Else
                                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                                    BubbleEvent = False
                                End If
                            End If
                        Else
                            If goParent.goDICompany.InTransaction Then
                                goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                            End If
                            com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
                            BubbleEvent = False
                            Exit Sub
                        End If
                        oForm.Update()
                    Catch ex As Exception
                        If goParent.goDICompany.InTransaction Then
                            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                        End If
                        DataHandler.INSTANCE.doBufferedErrors()
                    Finally
                        oForm.Freeze(False)
                    End Try
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    'Set the Modal data and close
                    If goParent.doCheckModal(oForm.UniqueID) = True Then
                        doSetModalData(oForm)
                        BubbleEvent = False
                    End If
                ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    doFind(oForm)
                    BubbleEvent = False
                End If
            Else
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    oForm.Items.Item("IDH_CODE").Enabled = False
                End If
            End If
        End Sub
	
        Protected Function doFind(ByVal oForm As SAPbouiCOM.Form) As Boolean
            oForm.Freeze(True)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing

            '		Dim sMode As String
            Try
                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    Dim sCode As String = ""
                    Dim sCustomerCd As String = ""
                    Dim sCustomerNm As String = ""
                    Dim sREG As String = ""

                    sREG = getItemValue(oForm, "IDH_REG")
                    sCode = getItemValue(oForm, "IDH_CODE")
                    sCustomerCd = getItemValue(oForm, "IDH_CUST")
                    sCustomerNm = getItemValue(oForm, "IDH_CUSTNM")

                    Dim db As SAPbouiCOM.DBDataSource
                    db = oForm.DataSources.DBDataSources.Item("@IDH_VEHMAS")

                    'sCode = oRecordSet.Fields.Item("Code").Value.Trim()
                    db.Clear()

                    Dim oConditions As SAPbouiCOM.Conditions = New SAPbouiCOM.Conditions
                    Dim oCondition As SAPbouiCOM.Condition
                    If Not sCode Is Nothing AndAlso sCode.Length > 0 Then
                        oCondition = oConditions.Add
                        oCondition.Alias = "Code"
                        oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                        oCondition.CondVal = sCode
                    End If
                    'db.Query(oConditions)
                    If Not sREG Is Nothing AndAlso sREG.Length > 0 Then
                        oCondition = oConditions.Add()
                        oCondition.Alias = "U_VehReg"
                        oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                        oCondition.CondVal = sREG
                    End If

                    db.Query(oConditions)

                    oForm.Update()

                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                    doLoadData(oForm)
                    Return True
                    'Else
                    'doError("doFind", "The Code was not found", "Code not found: " & sCode & "." & sCustomerCd & "." & sCustomerNm)
                End If
                'End If
                'Return False
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error finding the Vehicle.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFVEH", {Nothing})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
                oForm.Freeze(False)
            End Try
            Return False
        End Function
	
        Private Function doUpdateHeader(ByVal oForm As SAPbouiCOM.Form, ByVal bIsAdd As Boolean) As Boolean
            Dim iwResult As Integer = 0
            Dim swResult As String = Nothing

            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_VEHMAS")
            Try

                Dim sCode As String = CType(getDFValue(oForm, "@IDH_VEHMAS", "Code"), String)
                If oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) Then
                    If bIsAdd Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError("A Duplicate entries are not allowed.")
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Duplicate entries are not allowed.", "ERUSDBDU", {Nothing})
                        Return False

                    End If
                Else
                    oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
                    oAuditObj.setName(CType(getDFValue(oForm, "@IDH_VEHMAS", "Name"), String))
                End If

                oAuditObj.setFieldValue("U_VehReg", getDFValue(oForm, "@IDH_VEHMAS", "U_VehReg"))
                oAuditObj.setFieldValue("U_CName", getDFValue(oForm, "@IDH_VEHMAS", "U_CName"))
                oAuditObj.setFieldValue("U_VehType", getDFValue(oForm, "@IDH_VEHMAS", "U_VehType"))
                oAuditObj.setFieldValue("U_VehDesc", getDFValue(oForm, "@IDH_VEHMAS", "U_VehDesc"))
                oAuditObj.setFieldValue("U_DrivrNr", getDFValue(oForm, "@IDH_VEHMAS", "U_DrivrNr"))
                oAuditObj.setFieldValue("U_DRIVER", getDFValue(oForm, "@IDH_VEHMAS", "U_DRIVER"))
                oAuditObj.setFieldValue("U_Tarre", getDFValue(oForm, "@IDH_VEHMAS", "U_Tarre"))
                oAuditObj.setFieldValue("U_TRLReg", getDFValue(oForm, "@IDH_VEHMAS", "U_TRLReg"))
                oAuditObj.setFieldValue("U_TRLNM", getDFValue(oForm, "@IDH_VEHMAS", "U_TRLNM"))
                oAuditObj.setFieldValue("U_TRLTar", getDFValue(oForm, "@IDH_VEHMAS", "U_TRLTar"))
                oAuditObj.setFieldValue("U_CCrdCd", getDFValue(oForm, "@IDH_VEHMAS", "U_CCrdCd"))
                oAuditObj.setFieldValue("U_SCrdCd", getDFValue(oForm, "@IDH_VEHMAS", "U_SCrdCd"))
                oAuditObj.setFieldValue("U_SName", getDFValue(oForm, "@IDH_VEHMAS", "U_VehReg"))
                oAuditObj.setFieldValue("U_CCCrdCd", getDFValue(oForm, "@IDH_VEHMAS", "U_CCCrdCd"))
                oAuditObj.setFieldValue("U_CCName", getDFValue(oForm, "@IDH_VEHMAS", "U_CCName"))
                oAuditObj.setFieldValue("U_WasCd", getDFValue(oForm, "@IDH_VEHMAS", "U_WasCd"))
                oAuditObj.setFieldValue("U_WasDsc", getDFValue(oForm, "@IDH_VEHMAS", "U_WasDsc"))
                oAuditObj.setFieldValue("U_ItemCd", getDFValue(oForm, "@IDH_VEHMAS", "U_ItemCd"))
                oAuditObj.setFieldValue("U_ItemDsc", getDFValue(oForm, "@IDH_VEHMAS", "U_ItemDsc"))
                oAuditObj.setFieldValue("U_ItmGrp", getDFValue(oForm, "@IDH_VEHMAS", "U_ItmGrp"))
                oAuditObj.setFieldValue("U_IGrpDesc", getDFValue(oForm, "@IDH_VEHMAS", "U_IGrpDesc"))
                oAuditObj.setFieldValue("U_WFStat", getDFValue(oForm, "@IDH_VEHMAS", "U_WFStat"))
                oAuditObj.setFieldValue("U_MarkAc", getDFValue(oForm, "@IDH_VEHMAS", "U_MarkAc"))


                iwResult = oAuditObj.Commit()
                If iwResult <> 0 Then
                    goParent.goDICompany.GetLastError(iwResult, swResult)
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Error Adding: " + swResult)
                    DataHandler.INSTANCE.doResSystemError("Error Adding: " + swResult, "ERSYADD", {com.idh.bridge.Translation.getTranslatedWord(swResult)})
                    Return False
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error updating the Header.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXUHDR", {Nothing})
                Return False
            Finally
                DataHandler.INSTANCE.doReleaseObject(CType(oAuditObj, Object))
            End Try
            Return True
        End Function
		
		
		Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
			MyBase.doItemEvent(oForm, pVal, BubbleEvent)
			
			If pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
				If pVal.BeforeAction = False Then
					If doCheckSearchKey(pVal) Then
						If pVal.ItemUID = "IDH_REG" Then
                            Dim sAct As String = CType(getWFValue(oForm, "LastVehAct"), String)
							If sAct = "DOSEARCH" Then
                                Dim sReg As String = CType(getDFValue(oForm, "@IDH_VEHMAS", "U_VehReg", True), String)
								
								setSharedData(oForm, "IDH_REG", sReg)
								setSharedData(oForm, "SHOWCREATE", "TRUE")
								setSharedData(oForm, "SILENT", "SHOWMULTI")
								setSharedData(oForm, "SHOWACTIVITY", "FALSE")
								goParent.doOpenModalForm("IDHLOSCH", oForm)
							End If
							
						End If
					Else
						If pVal.ItemUID = "IDH_REG" Then
							setWFValue(oForm, "LastVehAct", "DOSEARCH")
						End If
					End If
					
				End If
			ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
				
				If pVal.ItemUID = "IDH_BDSITE" Then
					setSharedData(oForm, "TRG", "ST")
					setSharedData(oForm, "IDH_BPCOD", getItemValue(oForm, "IDH_DSITE"))
					setSharedData(oForm, "IDH_TYPE", "F-S")
                    'setSharedData(oForm, "IDH_GROUP", com.idh.bridge.lookups.Config.Parameter( "BGSDispo"))
					setSharedData(oForm, "IDH_NAME", getItemValue(oForm, "IDH_SUPNM"))
					setSharedData(oForm, "SILENT", "SHOWMULTI")
					goParent.doOpenModalForm("IDHCSRCH", oForm)
					
				ElseIf pVal.ItemUID = "IDH_CARL" Then
					setSharedData(oForm, "TRG", "WC")
					setSharedData(oForm, "IDH_BPCOD", getItemValue(oForm, "IDH_WCC"))
					setSharedData(oForm, "IDH_TYPE", "C")
                    'setSharedData(oForm, "IDH_GROUP", com.idh.bridge.lookups.Config.Parameter( "BGCWCarr"))
					setSharedData(oForm, "IDH_NAME", getItemValue(oForm, "IDH_WCNM"))
					setSharedData(oForm, "SILENT", "SHOWMULTI")
					goParent.doOpenModalForm("IDHCSRCH", oForm)
					
				ElseIf pVal.ItemUID = "IDH_CUSL" Then
					setSharedData(oForm, "TRG", "CU")
					setSharedData(oForm, "IDH_BPCOD", getItemValue(oForm, "IDH_CUST"))
					setSharedData(oForm, "IDH_TYPE", "F-C")
					setSharedData(oForm, "IDH_NAME", getItemValue(oForm, "IDH_CUSTNM"))
					setSharedData(oForm, "SILENT", "SHOWMULTI")
					goParent.doOpenModalForm("IDHCSRCH", oForm)
					
				ElseIf pVal.ItemUID = "IDH_WASCF" Then
					setSharedData(oForm, "TP", "WAST")
                    'setSharedData(oForm, "IDH_GRPCOD", com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup(goParent))
					'setSharedData(oForm, "IDH_CRDCD", getDFValue(oForm, "@IDH_VEHMAS", "U_CardCd"))
					'setSharedData(oForm, "IDH_INVENT", "N")
					goParent.doOpenModalForm("IDHWISRC", oForm)
					
				ElseIf pVal.ItemUID = "IDH_WCODE" Then
					setSharedData(oForm, "TP", "WAST")
                    setSharedData(oForm, "IDH_GRPCOD", com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup())
					setSharedData(oForm, "IDH_CRDCD", getDFValue(oForm, "@IDH_VEHMAS", "U_CardCd"))
					setSharedData(oForm, "IDH_ITMCOD", getDFValue(oForm, "@IDH_VEHMAS", "U_WasCd"))
					setSharedData(oForm, "IDH_INVENT", "N")
					setSharedData(oForm, "SILENT", "SHOWMULTI")
					goParent.doOpenModalForm("IDHWISRC", oForm)
					
				ElseIf pVal.ItemUID = "IDH_WDEC" Then
					Dim sDesc As String = getItemValue(oForm, "IDH_WDEC")
					If sDesc.Length = 0 OrElse sDesc.StartsWith("*") OrElse sDesc.EndsWith("*") Then
						setSharedData(oForm, "TP", "WAST")
                        setSharedData(oForm, "IDH_GRPCOD", com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup())
						setSharedData(oForm, "IDH_CRDCD", getDFValue(oForm, "@IDH_VEHMAS", "U_CardCd"))
						
						setSharedData(oForm, "IDH_ITMNAM", sDesc)
						setSharedData(oForm, "IDH_INVENT", "N")
						goParent.doOpenModalForm("IDHWISRC", oForm)
					End If
					
				ElseIf pVal.ItemUID = "IDH_ITCF" Then
					setSharedData(oForm, "TRG", "CD")
					setSharedData(oForm, "IDH_ITMCOD", "")
                    setSharedData(oForm, "IDH_GRPCOD", com.idh.bridge.lookups.Config.Parameter("MDDDIG"))
					goParent.doOpenModalForm("IDHISRC", oForm)
					
				ElseIf pVal.ItemUID = "IDH_CCODE" Then
                    Dim sItem As String = CType(getDFValue(oForm, "@IDH_VEHMAS", "U_ItemCd"), String)
					setSharedData(oForm, "TRG", "CD")
					setSharedData(oForm, "IDH_ITMCOD", sItem)
					setSharedData(oForm, "IDH_GRPCOD", "")
					setSharedData(oForm, "SILENT", "SHOWMULTI")
					goParent.doOpenModalForm("IDHISRC", oForm)
					
				ElseIf pVal.ItemUID = "IDH_CONDEC" Then
                    Dim sDesc1 As String = CType(getDFValue(oForm, "@IDH_VEHMAS", "U_ItemDsc"), String)
					If sDesc1.Length = 0 OrElse sDesc1.StartsWith("*") OrElse sDesc1.EndsWith("*") Then
						setSharedData(oForm, "TRG", "CD")
						setSharedData(oForm, "IDH_ITMCOD", "")
                        setSharedData(oForm, "IDH_GRPCOD", com.idh.bridge.lookups.Config.Parameter("MDDDIG"))
						setSharedData(oForm, "IDH_ITMNAM", sDesc1)
						goParent.doOpenModalForm("IDHISRC", oForm)
					End If
					
				ElseIf pVal.ItemUID = "IDH_REGL" Then
					Dim sReg As String
                    sReg = CType(getDFValue(oForm, "@IDH_VEHMAS", "U_VehReg", True), String)
					setSharedData(oForm, "IDH_REG", sReg)
					setSharedData(oForm, "SHOWCREATE", "TRUE")
					setSharedData(oForm, "SHOWACTIVITY", "FALSE")
					goParent.doOpenModalForm("IDHLOSCH", oForm)
					
				ElseIf pVal.ItemUID = "IDH_DLF" Then
					setSharedData(oForm, "IDH_DRIVNM", getItemValue(oForm, "IDH_DRIVER"))
					setSharedData(oForm, "IDH_DRIVSN", getItemValue(oForm, "IDH_DN"))
					setSharedData(oForm, "IDH_VEHREG", getItemValue(oForm, "IDH_REG"))
					setSharedData(oForm, "SHOWCREATE", "TRUE")
					goParent.doOpenModalForm("IDHDRVSRC", oForm)
					
					Else If pVal.ItemUID = "IDH_DEL" Then
					doRemove(oForm)
				End If
				
			End If
			Return False
		End Function
		
		Private Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form)
			
			With oForm.DataSources.DBDataSources.Item("@IDH_VEHMAS")
				.InsertRecord(.Offset)
				
                'Dim iCode As Integer = goParent.goDB.doNextNumber("CONNO")
				setWFValue(oForm, "LastContract", -2)
                'Dim sCode As String = DataHandler.doGenerateCode(Nothing, "VEHMAST")
				
                Dim sDefaultStatus As String = com.idh.bridge.lookups.Config.Parameter("WCDEFST")
                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode("VEHMAST")
                setDFValue(oForm, "@IDH_VEHMAS", "Code", oNumbers.CodeCode)
                setDFValue(oForm, "@IDH_VEHMAS", "Name", oNumbers.NameCode)
				setDFValue(oForm, "@IDH_VEHMAS", "U_CCrdCd", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_VehReg", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_VehType", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_VehDesc", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_DrivrNr", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_DRIVER", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_Tarre", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_TRLReg", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_TRLNM", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_TRLTar", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_CCrdCd", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_CName", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_SCrdCd", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_SName", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_CCCrdCd", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_CCName", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_WasCd", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_WasDsc", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_ItemCd", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_ItmGrp", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_IGrpDesc", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_WFStat", "")
				setDFValue(oForm, "@IDH_VEHMAS", "U_MarkAc", "")
				
			End With
			
			doRefreshForm(oForm)
			doLoadData(oForm)
			
		End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            MyBase.doHandleModalResultShared(oForm, sModalFormType, sLastButton)

            Try
                If sModalFormType = "IDHISRC" Then
                    'If sTarget = "cd" Then 'cd
                    setDFValue(oForm, "@IDH_VEHMAS", "U_ItemCd", getSharedData(oForm, "ITEMCODE"), False, True)
                    setDFValue(oForm, "@IDH_VEHMAS", "U_ItemDsc", getSharedData(oForm, "ITEMNAME"))
                ElseIf sModalFormType = "IDHDRVSRC" Then
                    Dim sDriver As String = getSharedDataAsString(oForm, "IDH_DRIVNM") & " " & getSharedDataAsString(oForm, "IDH_DRIVSN")
                    setDFValue(oForm, "@IDH_VEHMAS", "U_DrivrNr", getSharedData(oForm, "IDH_CONTN"), False, True)
                    setDFValue(oForm, "@IDH_VEHMAS", "U_Driver", sDriver)
                ElseIf sModalFormType = "IDHLOSCH" Then
                    setDFValue(oForm, "@IDH_VEHMAS", "U_VehReg", getSharedData(oForm, "REG"), False, True)
                    setDFValue(oForm, "@IDH_VEHMAS", "U_VehDesc", getSharedData(oForm, "VDESC"))
                ElseIf sModalFormType = "IDHWISRC" Then
                    setDFValue(oForm, "@IDH_VEHMAS", "U_WasCd", getSharedData(oForm, "ITEMCODE"))
                    setDFValue(oForm, "@IDH_VEHMAS", "U_WasDsc", getSharedData(oForm, "ITEMNAME"), False, True)
                ElseIf sModalFormType = "IDHCSRCH" Then
                    Dim sTarget As String = getSharedDataAsString(oForm, "TRG")
                    If sTarget = "ST" Then 'Disposal Site
                        setDFValue(oForm, "@IDH_VEHMAS", "U_SCrdCd", getSharedData(oForm, "CARDCODE"), False, True)
                        setDFValue(oForm, "@IDH_VEHMAS", "U_SName", getSharedData(oForm, "CARDNAME"), False, True)
                    ElseIf sTarget = "CU" Then 'Customer
                        setDFValue(oForm, "@IDH_VEHMAS", "U_CCrdCd", getSharedData(oForm, "CARDCODE"), False, True)
                        setDFValue(oForm, "@IDH_VEHMAS", "U_CName", getSharedData(oForm, "CARDNAME"), False, True)
                    ElseIf sTarget = "WC" Then 'Waste Carrier
                        setDFValue(oForm, "@IDH_VEHMAS", "U_CCCrdCd", getSharedData(oForm, "CARDCODE"), False, True)
                        setDFValue(oForm, "@IDH_VEHMAS", "U_CCName", getSharedData(oForm, "CARDNAME"), False, True)

                    End If
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal result - " & sModalFormType)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try

        End Sub
      
      Private Sub doRemove(ByVal oForm As SAPbouiCOM.Form)
      	Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_VEHMAS")
      	Try
                Dim sCODE As String = CType(getDFValue(oForm, "@IDH_VEHMAS", "Code"), String)
      		Dim iwResult As Integer
      		Dim swResult As String = Nothing
      		
      		oAuditObj.setKeyPair(sCODE, IDHAddOns.idh.data.AuditObject.ac_Types.idh_REMOVE)
      		iwResult = oAuditObj.Remove()
      		If iwResult <> 0 Then
      			goParent.goDICompany.GetLastError(iwResult, swResult)
                    'com.idh.bridge.DataHandler.INSTANCE.doError("System: Error Remove - " + swResult, "Error removing the row - " & sCODE)
                    DataHandler.INSTANCE.doResSystemError("Error Remove - " + swResult + ", Error removing the row - " & sCODE, "ERSYDBRR", {com.idh.bridge.Translation.getTranslatedWord(swResult), sCODE})
      		Else
      			oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
      			doRefreshForm(oForm)
      		End If
      	Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error removing the row.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRGEN", {Nothing})
      	End Try
      End Sub
		
		Private Sub doClearAll(ByVal oForm As SAPbouiCOM.Form)
			Dim sCode As String = Nothing
			Dim bIsFind As Boolean = False
			If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
				bIsFind = True
			End If
			
			setDFValue(oForm, "@IDH_VEHMAS", "Code", sCode)
			setDFValue(oForm, "@IDH_VEHMAS", "Name", sCode)
			setDFValue(oForm, "@IDH_VEHMAS", "U_CCrdCd", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_VehReg", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_VehType", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_VehDesc", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_DrivrNr", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_DRIVER", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_Tarre", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_TRLReg", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_TRLNM", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_TRLTar", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_CCrdCd", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_CName", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_SCrdCd", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_SName", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_CCCrdCd", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_CCName", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_WasCd", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_WasDsc", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_ItemCd", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_ItmGrp", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_IGrpDesc", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_WFStat", "")
			setDFValue(oForm, "@IDH_VEHMAS", "U_MarkAc", "")
			
			doRefreshForm(oForm)
			doLoadData(oForm)
			
		End Sub
		
		Public Overrides Sub doClose()
		End Sub
	
		'MODAL DIALOG
		'*** Set the return data when called as a modal dialog
		Private Sub doSetModalData(ByVal oForm As SAPbouiCOM.Form)
			doReturnFromModalShared(oForm, True)
		End Sub
		
	End Class
End Namespace


