Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class WorkFlow
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_WTWF", sParMenu, iMenuPosition, Nothing, "Workflow (Action)" )
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Workflow (Action)"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_WTWF"
        End Function

    End Class
End Namespace
