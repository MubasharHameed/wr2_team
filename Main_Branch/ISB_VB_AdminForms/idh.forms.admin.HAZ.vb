Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class HAZ
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_WTHAZ", sParMenu, iMenuPosition, Nothing, "HAZ Codes" )
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "HAZ Codes"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_WTHAZ"
        End Function

    End Class
End Namespace
