Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System
Imports com.idh.utils.Conversions
Imports com.idh.utils

Namespace idh.forms.admin
    Public Class PBIDetails
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHPBID", "IDHPS", iMenuPosition, "PBIDetail.srf", "Scheduled Breakdown Per Day")
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_PBIDTL"
        End Function

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)

            doAddUF(oForm, "IDH_PFROM", SAPbouiCOM.BoDataType.dt_DATE)
            doAddUF(oForm, "IDH_PTO", SAPbouiCOM.BoDataType.dt_DATE)
            doAddUF(oForm, "IDH_FREQ", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 200)
            doAddUF(oForm, "IDH_BINQTY", SAPbouiCOM.BoDataType.dt_QUANTITY, 10)
        End Sub

        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            oGridN.doSetDeleteActive(False)
            oGridN.BlockPopupMenu = True

            Dim dBinTotal As Double = 0
            Dim iCount As Integer = oGridN.getRowCount()
            Dim oData As SAPbouiCOM.DataTable = oGridN.getDataTable()

            Dim bSameWOR As Boolean = True
            Dim sWOR As String = ""
            Dim sTWOR As String = ""
            If iCount > 0 Then
                Dim iQtyIndex As Integer = oGridN.doIndexField("U_IDHCOIQT")
                Dim iRowIndex As Integer = oGridN.doIndexField("U_IDHWOR")

                If iCount = 0 Then
                    bSameWOR = True
                End If

                For iRow As Integer = 0 To iCount - 1
                    dBinTotal = dBinTotal + ToDouble(oData.GetValue(iQtyIndex, iRow))
                    sWOR = Conversions.ToString(oData.GetValue(iRowIndex, iRow))
                    If sWOR.Length > 0 AndAlso iRow > 0 AndAlso sWOR.Equals(sTWOR) = False Then
                        bSameWOR = False
                    End If
                    sTWOR = sWOR
                Next

                Dim dPeriodStart As Date = Conversions.ToDateTime(oData.GetValue(oGridN.doIndexField("U_IDHPRDSD"), 0))
                Dim dPeriodEnd As Date = Conversions.ToDateTime(oData.GetValue(oGridN.doIndexField("U_IDHPRDED"), 0))
                sWOR = Conversions.ToString(oData.GetValue(oGridN.doIndexField("U_IDHWOR"), 0))
                Dim sPBI As String = Conversions.ToString(oData.GetValue(oGridN.doIndexField("U_IDHPBI"), 0))

                setUFValue(oForm, "IDH_PFROM", Dates.doDateToSBODateStr(dPeriodStart))
                setUFValue(oForm, "IDH_PTO", Dates.doDateToSBODateStr(dPeriodEnd))
                If bSameWOR Then
                    setUFValue(oForm, "IDH_WOR", sWOR)
                Else
                    setUFValue(oForm, "IDH_WOR", "")
                End If
                setUFValue(oForm, "IDH_PBI", sPBI)
                setUFValue(oForm, "IDH_BINQTY", dBinTotal)
                setUFValue(oForm, "IDH_FREQ", doGetFreq(oForm, sWOR))

            End If
        End Sub

        Protected Function doGetFreq(ByVal oForm As SAPbouiCOM.Form, ByVal sRowCode As String) As String
            Dim sSql As String = "SELECT U_IntComm FROM [@IDH_JOBSHD] WHERE Code = '" & sRowCode & "'"
            Dim sFreq As String = ""

            Dim oRecord As com.idh.bridge.DataRecords = Nothing
            Try
                oRecord = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("doOrders", sSql)
                If Not oRecord Is Nothing AndAlso oRecord.RecordCount > 0 Then
                    sFreq = oRecord.getValueAsString(0)
                End If
            Catch e As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & e.ToString, "Error getting the frequency.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(e.Message, "EREXFREQ", {Nothing})
            Finally
                com.idh.bridge.DataHandler.INSTANCE.doReleaseDataRecords(oRecord)
            End Try
            Return sFreq
        End Function

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", False, 0, Nothing, Nothing)

            oGridN.doAddListField("U_IDHPRDSD", "Period Start Date", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHPRDED", "Period End Date", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHPBI", "PBI Code", False, -1, Nothing, Nothing)

            oGridN.doAddListField("U_IDHWOR", "WOR Ref", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHJDAY", "Day", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHJDATE", "Date", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHJTYPE", "Job Type", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHCOICD", "Container ItemCode", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHWCICD", "Waste Code ItemCode", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHCOIQT", "No of Bins", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHDONE", "Job Done", True, -1, "CHECKBOX", Nothing)

            oGridN.doAddListField("U_IDHRECCT", "Run Count", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IDHDTYP", "Detail Type", False, -1, Nothing, Nothing)

            oGridN.setOrderValue("U_IDHPRDSD, U_IDHPRDED, U_IDHJDATE")
        End Sub

        Protected Overrides Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddFilterField("IDH_WOR", "U_IDHWOR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 20)
            oGridN.doAddFilterField("IDH_PBI", "U_IDHPBI", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 20)
            oGridN.doAddFilterField("IDH_RUNCNT", "U_IDHRECCT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 20)
        End Sub

        '        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
        '            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
        '            	If pVal.BeforeAction = True Then
        '            		Return False
        '            	End If
        '            Else
        '            	Return MyBase.doCustomItemEvent(oForm, pVal)
        '            End If
        '            Return True
        '        End Function

    End Class
End Namespace
