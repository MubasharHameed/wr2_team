Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Namespace idh.forms.price
    Public Class Customer
        Inherits idh.forms.price.Templ

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_CSITPR", "CUS", iMenuPosition, "Customer Item Price")
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_CSITPR"
        End Function
'
        Protected Overrides Function getBPType() As String
            Return "C"
        End Function

        Protected Overrides Function getBPCodeField() As String
            Return "U_CustCd"
        End Function
        
        Protected Overrides Function getBPInFeild() As String
        	Return "CUSCD"
        End Function        
        
        Protected Overrides Function getBPUOMField() As String
        	Return "SUOM"
        End Function
    End Class
End Namespace
