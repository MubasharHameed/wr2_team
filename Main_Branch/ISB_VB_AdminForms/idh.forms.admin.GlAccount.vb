Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System
Imports com.idh.dbObjects.numbers

Imports com.idh.bridge

Namespace idh.forms
	Public Class GlAccountForm
		Inherits IDHAddOns.idh.forms.Base

		Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
			'MyBase.New(oParent, "IDHGLAC",iMenuPosition, "GL ACCNT.srf", False, True, False)
			MyBase.New(oParent, "IDHGLAC", "IDHMAST", iMenuPosition, "GL ACCNT.srf", False, True, False, "GL Form", load_Types.idh_LOAD_NORMAL)
			
            'doAddImagesToFix("IDH_BCGC")
            'doAddImagesToFix("IDH_WASCF")
            'doAddImagesToFix("IDH_CUSL")
            'doAddImagesToFix("IDH_BADDC")
            'doAddImagesToFix("IDH_BEXP")
            'doAddImagesToFix("IDH_BSA")
		    
		End Sub
		
	Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
			oForm.Visible = True
		End Sub
		
'		'*** Create Sub-Menu
'		Protected Overrides Sub doCreateSubMenu()
'			Dim oMenus As SAPbouiCOM.Menus
'			Dim oMenuItem As SAPbouiCOM.MenuItem
'			Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
'			
'			oMenuItem = goParent.goApplication.Menus.Item("IDHMAST")
'			
'			oMenus = oMenuItem.SubMenus
'			oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
'			oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING
'			oCreationPackage.UniqueID = gsType
'			oCreationPackage.String = "GL Form"
'			oCreationPackage.Position = giMenuPosition
'			
'			Try
'				oMenus.AddEx(oCreationPackage)
'			Catch ex As Exception
'			End Try
'		End Sub	
		
		'*** Add event filters to avoid receiving all events from SBO
		Protected Overrides Sub doSetEventFilters()
			doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
			doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
			doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
			doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
		End Sub
		
		
	'** Create the form
		Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
			MyBase.doCompleteCreate(oForm, BubbleEvent)
			
			Try
				Dim oItem As SAPbouiCOM.Item
				
				oForm.DataSources.DBDataSources.Add("@IDH_GLMAS")
				
				doAddDF(oForm, "@IDH_GLMAS", "IDH_CGC", "U_ItemCd")
				doAddDF(oForm, "@IDH_GLMAS", "IDH_CDES", "U_ItemDs")
				doAddDF(oForm, "@IDH_GLMAS", "IDH_WCODE",  "U_WasteCd")
				doAddDF(oForm, "@IDH_GLMAS", "IDH_WDEC",  "U_WasteDs")
				doAddDF(oForm, "@IDH_GLMAS", "IDH_CODE",  "CODE")
				doAddDF(oForm, "@IDH_GLMAS", "IDH_ADDC",  "U_AddCd")
				doAddDF(oForm, "@IDH_GLMAS", "IDH_ADDSD",   "U_AddDs")
				doAddDF(oForm, "@IDH_GLMAS", "IDH_RVACC",  " U_DebAcc")
				doAddDF(oForm, "@IDH_GLMAS", "IDH_EXPACC",   "U_CreAcc")
				doAddDF(oForm, "@IDH_GLMAS", "IDH_SACC",  "U_ProfCn")
				doAddDF(oForm, "@IDH_GLMAS", "IDH_STACC",  "U_ProjCd")
				
			    oForm.DataBrowser.BrowseBy = "IDH_CODE"
				
'				Dim oEdit As SAPbouiCOM.EditText
'				Dim oButton As SAPbouiCOM.Button		
				
				Dim oCombo As SAPbouiCOM.ComboBox
				oItem = oForm.Items.Item("IDH_CGRP")
				oItem.DisplayDesc = True
                oCombo = CType(oItem.Specific, SAPbouiCOM.ComboBox)
				oCombo.ValidValues.Add("", getTranslatedWord("Any"))
				oCombo.ValidValues.Add("106", "Compactor")
				oCombo.ValidValues.Add("108", "Bags")
			    oCombo.ValidValues.Add("111", "Cage")
				oCombo.ValidValues.Add("115", "Disposal")
			    oCombo.ValidValues.Add("112", "Dustcart")
				oCombo.ValidValues.Add("100", "Item")
			    oCombo.ValidValues.Add("109", "Licence")
				oCombo.ValidValues.Add("110", "Other Charges")
				oCombo.ValidValues.Add("113", "Vehicle")
				oCombo.ValidValues.Add("105", "Skip")
			    oCombo.ValidValues.Add("102", "services")
			    
			    oItem = oForm.Items.Item("IDH_ORDTYP")
				oItem.DisplayDesc = True
                oCombo = CType(oItem.Specific, SAPbouiCOM.ComboBox)
				oCombo.ValidValues.Add("", getTranslatedWord("Any"))
				oCombo.ValidValues.Add("106", "Compactor")
				oCombo.ValidValues.Add("108", "Bags")
			    oCombo.ValidValues.Add("111", "Cage")
				oCombo.ValidValues.Add("115", "Disposal")
			    oCombo.ValidValues.Add("112", "Dustcart")
				oCombo.ValidValues.Add("100", "Item")
			    oCombo.ValidValues.Add("109", "Licence")
				oCombo.ValidValues.Add("110", "Other Charges")
				oCombo.ValidValues.Add("113", "Vehicle")
				oCombo.ValidValues.Add("105", "Skip")
			    oCombo.ValidValues.Add("102", "services")
			 						    
			    oForm.SupportedModes = -1
				oForm.AutoManaged = False
				
				oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
				
			Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
			End Try
		End Sub  
		
		Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
	  	If pVal.BeforeAction = False Then
                If pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_ADD Then
                    oForm.Freeze(True)
                    Try
                        'doRemoveLinkedDocs(oForm)
                        doCreateNewEntry(oForm)
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Menu Event - " & oForm.UniqueID & "." & pVal.MenuUID)
                        DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMEN", {oForm.UniqueID, pVal.MenuUID})
                    Finally
                        oForm.Freeze(False)
                    End Try

                ElseIf pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_FIND OrElse _
                  pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_FIRST OrElse _
                  pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_LAST OrElse _
                  pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_NEXT OrElse _
                  pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_PREV Then
                    oForm.Freeze(True)
                    Try
                        If pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_FIND Then
                            doClearAll(oForm)
                        Else
                            doLoadData(oForm)
                        End If
                    Catch ex As Exception
                    Finally
                        oForm.Freeze(False)
                    End Try
                End If
	  	Else
                If pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_ADD OrElse _
                   pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_FIND OrElse _
                   pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_FIRST OrElse _
                   pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_LAST OrElse _
                   pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_NEXT OrElse _
                   pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_PREV Then

                    Return True
                End If
            End If
            Return True
	  End Function
	  
	   Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            Dim sCODE As String = CType(getDFValue(oForm, "@IDH_GLMAS", "Code"), String)
		  	If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
		  		doSwitchToFind(oForm)
		  	ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
		  		doSwitchToAdd(oForm)
		  	ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
		  		doSwitchToAdd(oForm)
		  	End If
		  End Sub
		  
	   Protected Sub doSwitchToFind(ByVal oForm As SAPbouiCOM.Form)
			  Dim oExclude() As String = {"IDH_ORDTYP","IDH_CGRP","IDH_CODE","IDH_WDEC"}
			  doSetFocus(oForm, "IDH_ORDTYP")
			  DisableAllEditItems(oForm, oExclude)
			  End Sub
			   
       Protected Sub doSwitchToAdd(ByVal oForm As SAPbouiCOM.Form)
			   	
			  Dim oExclude() As String = {"IDH_CODE","IDH_CGRP","IDH_WDEC"}
			  doSetFocus(oForm, "IDH_ORDTYP")
			  EnableAllEditItems(oForm, oExclude)
			  End Sub
			  
	Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
	Dim bDoNew As Boolean = False
	Dim sU_CODE As String
	If pVal.BeforeAction = True Then
                sU_CODE = CType(getDFValue(oForm, "@IDH_GLMAS", "Code"), String)
		
		If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
				oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
			oForm.Freeze(True)
			Try
'				If doValidateHeader(oForm) = False Then
'					BubbleEvent = False
'					Exit Sub
'				End If
'				
				Dim bDoContinue As Boolean = False
				goParent.goDICompany.StartTransaction()
				
				If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
					If doUpdateHeader(oForm, True) = True Then
						bDoContinue = True
						bDoNew = True
					End If
				Else
					If doUpdateHeader(oForm, False) = True Then
						bDoContinue = True
					End If
				End If
				
				If bDoContinue Then
					If goParent.goDICompany.InTransaction Then
						goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
					End If
					com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
					doLoadData(oForm)
					
					'doRemoveFormRows(oForm.UniqueID)
					If bDoNew Then
						setWFValue(oForm, "LastContract", -1)
						If goParent.doCheckModal(oForm.UniqueID) = False Then
							doCreateNewEntry(oForm)
						Else
							oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
							BubbleEvent = False
						End If
					End If
				Else
					If goParent.goDICompany.InTransaction Then
						goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
					End If
					com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
					BubbleEvent = False
					Exit Sub
				End If
				oForm.Update()
			Catch ex As Exception
				If goParent.goDICompany.InTransaction Then
					goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
				End If
				com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
			Finally
				oForm.Freeze(False)
			End Try
		ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
			'Set the Modal data and close
			If goParent.doCheckModal(oForm.UniqueID) = True Then
				doSetModalData(oForm)
				BubbleEvent = False
			End If
		ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
			doFind(oForm)
			BubbleEvent = False
		End If
	Else
		If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
			oForm.Items.Item("IDH_CODE").Enabled = False
		End If
	End If
End Sub		  


Protected Function doFind(ByVal oForm As SAPbouiCOM.Form) As Boolean
		oForm.Freeze(True)
		Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
		
'		Dim sMode As String
		Try
			If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
				Dim sCode As String = ""
				Dim sCONT As String = ""
				Dim sADDC As String = ""
				Dim sADDSD As String = ""
				
				sCONT = getItemValue(oForm, "IDH_CGC")
				sCode = getItemValue(oForm, "IDH_CODE")
				sADDC = getItemValue(oForm, "IDH_ADDC")
				sADDSD = getItemValue(oForm, "IDH_ADDSD")
				
				Dim db As SAPbouiCOM.DBDataSource
				db = oForm.DataSources.DBDataSources.Item("@IDH_GLHMAS")
				
				'sCode = oRecordSet.Fields.Item("Code").Value.Trim()
				db.Clear()
				
				Dim oConditions As SAPbouiCOM.Conditions = New SAPbouiCOM.Conditions
				Dim oCondition As SAPbouiCOM.Condition
				If Not sCode Is Nothing AndAlso sCode.Length > 0 Then
					oCondition = oConditions.Add
					oCondition.Alias = "Code"
					oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
					oCondition.CondVal = sCode
				End If
				'db.Query(oConditions)
				If Not sCONT Is Nothing AndAlso sCONT.Length > 0 Then
					oCondition = oConditions.Add()
					oCondition.Alias = "U_ContCg"
					oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
					oCondition.CondVal = sCONT
				End If
				
				db.Query(oConditions)
				
				oForm.Update()
				
				oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                    doLoadData(oForm)
                    Return True
				 'Else
				'doError("doFind", "The Code was not found", "Code not found: " & sCode & "." & sCustomerCd & "." & sCustomerNm)
			End If
			'End If
			'Return False
		Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error finding the GLAccount item.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFGLI", {Nothing})
		Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
			oForm.Freeze(False)
		End Try
            Return False
	 End Function
	 
	Private Function doUpdateHeader(ByVal oForm As SAPbouiCOM.Form, ByVal bIsAdd As Boolean) As Boolean
		Dim iwResult As Integer = 0
		Dim swResult As String = Nothing
		
            Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_GLHMAS")
		Try
			
                Dim sCode As String = CType(getDFValue(oForm, "@IDH_GLHMAS", "Code"), String)
			If oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) Then
				If bIsAdd Then
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Duplicate entries are not allowed")
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Duplicate entries are not allowed", "ERUSDBDU", {Nothing})
                        Return False
					
				End If
			Else
				oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
                    oAuditObj.setName(CType(getDFValue(oForm, "@IDH_GLHMAS", "Name"), String))
			End If
			
			oAuditObj.setFieldValue("U_ItemCd", getDFValue(oForm, "@IDH_GLHMAS","U_ItemCd"))
			oAuditObj.setFieldValue("IDH_CDES", getDFValue(oForm, "@IDH_GLHMAS","IDH_CDES"))
			oAuditObj.setFieldValue("U_WasteCd", getDFValue(oForm, "@IDH_GLHMAS","U_WasteCd"))
			oAuditObj.setFieldValue("U_WasteDs", getDFValue(oForm, "@IDH_GLHMAS","U_WasteDs"))
			oAuditObj.setFieldValue("CODE", getDFValue(oForm, "@IDH_GLHMAS","CODE"))
			oAuditObj.setFieldValue("U_AddCd", getDFValue(oForm, "@IDH_GLHMAS","U_AddCd"))
			oAuditObj.setFieldValue("U_AddDes", getDFValue(oForm, "@IDH_GLHMAS","U_AddDes"))
			oAuditObj.setFieldValue("U_RevAcc", getDFValue(oForm, "@IDH_GLHMAS","U_RevAcc"))
			oAuditObj.setFieldValue("U_ExpAcc", getDFValue(oForm, "@IDH_GLHMAS","U_ExpAcc"))
			oAuditObj.setFieldValue("U_SrnAcc", getDFValue(oForm, "@IDH_GLHMAS","U_SrnAcc"))
			oAuditObj.setFieldValue("U_StAcc", getDFValue(oForm, "@IDH_GLHMAS","U_StAcc"))
			oAuditObj.setFieldValue("U_GrAcc", getDFValue(oForm, "@IDH_GLHMAS","U_GrAcc")) 
			oAuditObj.setFieldValue("U_ContCg", getDFValue(oForm, "@IDH_GLHMAS","U_ContCg"))
			oAuditObj.setFieldValue("U_JobTp", getDFValue(oForm, "@IDH_GLHMAS","U_JobTp")) 
			
			iwResult = oAuditObj.Commit()
			If iwResult <> 0 Then
				goParent.goDICompany.GetLastError(iwResult, swResult)
                    'com.idh.bridge.DataHandler.INSTANCE.doError("System: Error Adding- " + swResult, "Error updating the Header.")
                    DataHandler.INSTANCE.doResSystemError("Error Adding- " + swResult + ", Error updating the Header.", "ERSYADD", {swResult})
				Return False
			End If
			
		Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error updating the Header.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXUHDR", {Nothing})
			Return False
		Finally
                DataHandler.INSTANCE.doReleaseObject(CType(oAuditObj, Object))
		End Try
		Return True
	End Function
	
	Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
			MyBase.doItemEvent(oForm, pVal, BubbleEvent)
			
		If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
				If pVal.BeforeAction = False Then
				If doCheckSearchKey(pVal) Then
					ElseIf pVal.ItemUID = "IDH_WASCF" Then
					setSharedData(oForm, "TP", "WAST")
                        'setSharedData(oForm, "IDH_GRPCOD", com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup(goParent))
					'setSharedData(oForm, "IDH_CRDCD", getDFValue(oForm, "@IDH_VEHMAS", "U_CardCd"))
					'setSharedData(oForm, "IDH_INVENT", "N")
                        If getSharedData(oForm, "IDH_ITMSRCHOPEND") Is Nothing OrElse getSharedDataAsString(oForm, "IDH_ITMSRCHOPEND") = "" Then
                            goParent.doOpenModalForm("IDHWISRC", oForm)
                        End If
                    End If
                End If
					
				ElseIf pVal.ItemUID = "IDH_WCODE" Then
					setSharedData(oForm, "TP", "WAST")
                setSharedData(oForm, "IDH_GRPCOD", com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup())
					setSharedData(oForm, "IDH_CRDCD", getDFValue(oForm, "@IDH_GLHMAS", "U_CardCd"))
					setSharedData(oForm, "IDH_ITMCOD", getDFValue(oForm, "@IDH_GLHMAS", "U_WasteCd"))
					setSharedData(oForm, "IDH_INVENT", "N")
					setSharedData(oForm, "SILENT", "SHOWMULTI")
                If getSharedData(oForm, "IDH_ITMSRCHOPEND") Is Nothing OrElse getSharedDataAsString(oForm, "IDH_ITMSRCHOPEND") = "" Then
                    goParent.doOpenModalForm("IDHWISRC", oForm)
                End If
					

            ElseIf pVal.ItemUID = "IDH_WDEC" Then
                Dim sDesc As String = getItemValue(oForm, "IDH_WDEC")
                If sDesc.Length = 0 OrElse sDesc.StartsWith("*") OrElse sDesc.EndsWith("*") Then
                    setSharedData(oForm, "TP", "WAST")
                    setSharedData(oForm, "IDH_GRPCOD", com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup())
                    setSharedData(oForm, "IDH_CRDCD", getDFValue(oForm, "@IDH_GLHMAS", "U_CardCd"))

                    setSharedData(oForm, "IDH_ITMNAM", sDesc)
                    setSharedData(oForm, "IDH_INVENT", "N")
                    If getSharedData(oForm, "IDH_ITMSRCHOPEND") Is Nothing OrElse getSharedDataAsString(oForm, "IDH_ITMSRCHOPEND") = "" Then
                        goParent.doOpenModalForm("IDHWISRC", oForm)
                    End If
                End If
            ElseIf pVal.ItemUID = "IDH_BCGC" Then
                setSharedData(oForm, "TRG", "CD")
                setSharedData(oForm, "IDH_ITMCOD", "")
                setSharedData(oForm, "IDH_GRPCOD", com.idh.bridge.lookups.Config.Parameter("MDDDIG"))
                goParent.doOpenModalForm("IDHISRC", oForm)
					
				ElseIf pVal.ItemUID = "IDH_CGC" Then
                Dim sItem As String = CType(getDFValue(oForm, "@IDH_GLHMAS", "U_ItemCd"), String)
					setSharedData(oForm, "TRG", "CD")
					setSharedData(oForm, "IDH_ITMCOD", sItem)
					setSharedData(oForm, "IDH_GRPCOD", "")
					setSharedData(oForm, "SILENT", "SHOWMULTI")
					goParent.doOpenModalForm("IDHISRC", oForm)
					
				ElseIf pVal.ItemUID = "IDH_CDES " Then
                Dim sDesc1 As String = CType(getDFValue(oForm, "@IDH_GLHMAS", "U_ItemDsc"), String)
					If sDesc1.Length = 0 OrElse sDesc1.StartsWith("*") OrElse sDesc1.EndsWith("*") Then
						setSharedData(oForm, "TRG", "CD")
						setSharedData(oForm, "IDH_ITMCOD", "")
                    setSharedData(oForm, "IDH_GRPCOD", com.idh.bridge.lookups.Config.Parameter("MDDDIG"))
						setSharedData(oForm, "IDH_ITMNAM", sDesc1)
						goParent.doOpenModalForm("IDHISRC", oForm)
					End If
				
				ElseIf pVal.ItemUID = "IDH_BADDC" Then
					setSharedData(oForm, "TRG", "AC")
					setSharedData(oForm, "IDH_EXPACC", getItemValue(oForm, "IDH_GRACC"))
					setSharedData(oForm, "IDH_SRNACC", getItemValue(oForm, "IDH_EXPACC"))
					setSharedData(oForm, "IDH_STACC", getItemValue(oForm, "IDH_SRNACC"))
					setSharedData(oForm, "SHOWCREATE", "TRUE")
					goParent.doOpenModalForm("IDHGLSRC", oForm)
				
				ElseIf pVal.ItemUID = "IDH_BEXP" Then
					setSharedData(oForm, "TRG", "EX")
					setSharedData(oForm, "IDH_EXPACC", getItemValue(oForm, "IDH_GRACC"))
					setSharedData(oForm, "IDH_SRNACC", getItemValue(oForm, "IDH_EXPACC"))
					setSharedData(oForm, "IDH_STACC", getItemValue(oForm, "IDH_SRNACC"))
					setSharedData(oForm, "SHOWCREATE", "TRUE")
					goParent.doOpenModalForm("IDHGLSRC", oForm)	
					
				ElseIf pVal.ItemUID = "IDH_BSA" Then
					setSharedData(oForm, "TRG", "SA")
					setSharedData(oForm, "IDH_EXPACC", getItemValue(oForm, "IDH_GRACC"))
					setSharedData(oForm, "IDH_SRNACC", getItemValue(oForm, "IDH_EXPACC"))
					setSharedData(oForm, "IDH_STACC", getItemValue(oForm, "IDH_SRNACC"))
					setSharedData(oForm, "SHOWCREATE", "TRUE")
					goParent.doOpenModalForm("IDHGLSRC", oForm)
					
				End If
			Return False
		End Function
		
		Private Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form)
			
			With oForm.DataSources.DBDataSources.Item("@IDH_GLHMAS")
				.InsertRecord(.Offset)

                'Dim sCode As String = DataHandler.doGenerateCode(Nothing, "CONNO")
                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode("CONNO")
                setWFValue(oForm, "LastContract", -2)
                'Dim sCode As String = com.idh.utils.Conversions.ToString(iCode)
				
                Dim sDefaultStatus As String = com.idh.bridge.lookups.Config.Parameter("WCDEFST")

                setDFValue(oForm, "@IDH_GLHMAS", "Code", oNumbers.CodeCode)
                setDFValue(oForm, "@IDH_GLHMAS", "U_ItemCd", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_ItemDs", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_WasteCd", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_WasteDs", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_AddCd", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_AddDes", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_RevAcc", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_ExpAcc", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_SrnAcc", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_StAcc", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_GrAcc", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_CONTCg", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_JobTp", "")
				
		End With
			
			doRefreshForm(oForm)
			doLoadData(oForm)
			
		End Sub
		
Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
	MyBase.doHandleModalResultShared(oForm, sModalFormType, sLastButton)
	
	Try
		If sModalFormType = "IDHISRC" Then
			setDFValue(oForm, "@IDH_GLHMAS", "U_ItemCd", getSharedData(oForm, "ITEMCODE"), False, True)
			setDFValue(oForm, "@IDH_GLHMAS", "U_ItemDs", getSharedData(oForm, "ITEMNAME"))
		
		Else If sModalFormType = "IDHGLSRC" Then
	        setDFValue(oForm, "@IDH_GLHMAS", "U_ItemCd", getSharedData(oForm, "ITEMCODE"), False, True)
			setDFValue(oForm, "@IDH_GLHMAS", "U_ItemDs", getSharedData(oForm, "ITEMNAME"))
		
		
		
		Else If sModalFormType = "IDHWISRC" Then
	        setDFValue(oForm, "@IDH_GLHMAS", "U_WasteCd", getSharedData(oForm, "ITEMCODE"))
			setDFValue(oForm, "@IDH_GLHMAS", "U_WasteDs", getSharedData(oForm, "ITEMNAME"), False, True)
		End If
		
		Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal Results - " & sModalFormType)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
	End Try
	
End Sub

Private Sub doClearAll(ByVal oForm As SAPbouiCOM.Form)
			Dim sCode As String = Nothing
			Dim bIsFind As Boolean = False
			If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
				bIsFind = True
			End If
			
				setDFValue(oForm, "@IDH_GLHMAS", "Code", sCode)
				setDFValue(oForm, "@IDH_GLHMAS", "U_ItemCd", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_ItemDs", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_WasteCd", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_WasteDs", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_AddCd", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_AddDes", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_RevAcc", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_ExpAcc", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_SrnAcc", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_StAcc", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_GrAcc", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_CONTCg", "")
				setDFValue(oForm, "@IDH_GLHMAS", "U_JobTp", "")
				
			
			doRefreshForm(oForm)
			doLoadData(oForm)
			
		End Sub
		
		Public Overrides Sub doClose()
		End Sub
		
		'MODAL DIALOG
		'*** Set the return data when called as a modal dialog
		Private Sub doSetModalData(ByVal oForm As SAPbouiCOM.Form)
			doReturnFromModalShared(oForm, True)
		End Sub
		
	End Class
End Namespace

