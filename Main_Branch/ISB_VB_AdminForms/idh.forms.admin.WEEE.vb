Imports System.IO
Imports System.Collections
Imports com.idh.bridge

Namespace idh.forms.admin
    Public Class WEEE
        Inherits idh.forms.admin.Tmpl
        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHWEEE", sParMenu, iMenuPosition, Nothing, "WEEE Codes")

        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_WEEE"
        End Function
    End Class
End Namespace
