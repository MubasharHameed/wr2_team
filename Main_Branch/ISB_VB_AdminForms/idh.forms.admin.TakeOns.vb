 Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class TakeOns
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHTAKN", sParMenu, iMenuPosition, Nothing, "Container Location TakeOns")
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Container Location TakeOns"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_TAKEON"
        End Function

    End Class
End Namespace
