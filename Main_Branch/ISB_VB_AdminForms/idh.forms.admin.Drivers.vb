Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class Drivers
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHDriver", sParMenu, iMenuPosition, Nothing, "Driver Setup")
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Driver Setup"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_DRVMAS"
        End Function

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_DRIVNM", "Driver Name", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_DRIVSN", "Driver Surname", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_VehReg", "Vehicle Reg", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_CONTN", "Contact Number", True, -1, Nothing, Nothing)
        End Sub

    End Class
End Namespace
