Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class Routes
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHRTAD", sParMenu, iMenuPosition, Nothing, "Routes Master")
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Routes Setup"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_ROUTES"
        End Function

    End Class
End Namespace
