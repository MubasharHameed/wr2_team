Imports System.Collections
Imports com.idh.bridge
Imports com.idh.bridge.data
Imports com.idh.dbObjects.User
Imports com.idh.controls
Imports IDHAddOns.idh.controls
Imports com.idh.bridge.lookups
Imports com.uBC.utils
Imports com.idh.bridge.resources
Imports com.idh.utils
Imports com.idh.dbObjects.numbers

Namespace idh.forms.admin
    Public Class BPForecasting
        Inherits IDHAddOns.idh.forms.Base

        Dim miDefaultMonthPeriod As Integer = 1

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            ''MyBase.New(oParent, "IDH_CIP", sParMenu, iMenuPosition, "BPForecasting.srf", "BP Forecasting")

            'IDHBPFRCT   IDH_CIP
            MyBase.New(oParent, "IDHBPFRCT", sParMenu, iMenuPosition, "BPForecasting.srf", True, True, False, "BP Forecasting", load_Types.idh_LOAD_NORMAL)

        End Sub
        Public Overrides Sub doClose()

        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As DBOGrid = CType(getWFValue(oForm, "LINESGRID"), DBOGrid)
            If oGridN Is Nothing Then
                oGridN = CType(DBOGrid.getInstance(oForm, "LINESGRID"), DBOGrid)
                If oGridN Is Nothing Then
                    Dim oForecast As IDH_BPFORECST = New IDH_BPFORECST(Me, oForm)
                    Dim oItem As SAPbouiCOM.Item
                    oItem = oForm.Items.Item("IDH_CONTA")
                    oGridN = New DBOGrid(Me, oForm, "LINESGRID", _
                        oItem.Left, oItem.Top, oItem.Width, oItem.Height, _
                        oForecast)

                    oGridN.getSBOItem.FromPane = oItem.FromPane
                    oGridN.getSBOItem.ToPane = oItem.ToPane
                End If
                setWFValue(oForm, "LINESGRID", oGridN)
            End If

            oGridN.doSetDoCount(True)
            doSetFilterFields(oGridN)
            doSetListFields(oGridN)

            If getHasSharedData(oForm) Then
                doReadInputParams(oForm)
            End If
            oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
        End Sub


        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            If lookups.Config.INSTANCE.doGetDoForecasting() Then

                Dim oGridN As DBOGrid = CType(getWFValue(oForm, "LINESGRID"), DBOGrid) 'DBOGrid.getInstance(oForm, "LINESGRID")
                Dim sCardCode As String = getItemValue(oForm, "IDH_CUST").ToString.Trim
                Dim sCardName As String = getItemValue(oForm, "IDH_NAME").ToString.Trim
                Dim sBPCurrency As String = getItemValue(oForm, "IDH_BPCURR").ToString.Trim

                If sCardCode.Length = 0 OrElse sCardCode.StartsWith("*") OrElse sCardCode.EndsWith("*") Then
                    sCardCode = "1QAZ2WSX"
                    oGridN.doEnabled(False)
                Else
                    oGridN.doEnabled(True)
                End If
                Dim oForecast As IDH_BPFORECST = CType(oGridN.DBObject, IDH_BPFORECST)
                oForm.Freeze(True)

                If getItemValue(oForm, "IDH_SHWACT") = "Y" Then
                    oForecast.getByBPOnlyActive(sCardCode)
                Else
                oForecast.getByBP(sCardCode)
                End If

                Dim db As SAPbouiCOM.DBDataSource
                Dim oConditions As SAPbouiCOM.Conditions = New SAPbouiCOM.Conditions
                Dim oCondition As SAPbouiCOM.Condition = oConditions.Add

                db = oForm.DataSources.DBDataSources.Item("OCRD")
                If db.GetValue("CardCode", db.Offset) IsNot Nothing AndAlso db.GetValue("CardCode", db.Offset).ToString().ToLower() <> sCardCode.ToLower() Then
                    db.Clear()
                    oCondition.Alias = "CardCode"
                    oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                    oCondition.CondVal = sCardCode
                    db.Query(oConditions)

                End If
                If sCardCode <> "1QAZ2WSX" Then
                    setUFValue(oForm, "IDH_CUST", sCardCode)
                    setUFValue(oForm, "IDH_NAME", sCardName)
                    setItemValue(oForm, "IDH_CUST", sCardCode)
                    setItemValue(oForm, "IDH_NAME", sCardName)

                    setDFValue(oForm, "OCRD", "CardCode", sCardCode)
                    setDFValue(oForm, "OCRD", "CardName", sCardCode)
                    setItemValue(oForm, "IDH_CODE", sCardCode)
                    setItemValue(oForm, "IDH_CDNM", sCardName)
                End If
                oForm.Freeze(False)
                oGridN.doPostReloadData(True)
                doUOMCombo(oGridN)
                doDocTypeCombo(oGridN)
                doCCICCombo(oGridN)
                doTransactionCode(oGridN)
                doCurrencyCombo(oGridN)

                If Not sBPCurrency.Equals("##") Then
                    oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BPCurr")).Editable = False
                Else
                    oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BPCurr")).Editable = True
                End If

            End If
        End Sub
        'Private Function thisBPForecast(ByVal oForm As SAPbouiCOM.Form) As IDH_BPFORECST
        '    Dim oBPFC As IDH_BPFORECST = getWFValue(oForm, "BPForeCast")
        '    If oBPFC Is Nothing Then
        '        oBPFC = New IDH_BPFORECST(Nothing, oForm)
        '        oBPFC.Handler_ForecastRefresh = New IDH_BPFORECST.et_ExternalTrigger(AddressOf doHandleBPForecastUpdated)
        '        doAddWF(oForm, "BPForeCast", oBPFC)
        '    End If
        '    Return oBPFC
        'End Function
#Region "Handler"
        Public Sub doHandleBPForecastUpdated(ByVal oCaller As IDH_BPFORECST)
            Dim sDoc As String = oCaller.U_DocType
            Dim oForm As SAPbouiCOM.Form = oCaller.SBOForm
            Dim oGridN As DBOGrid = CType(getWFValue(oForm, "LINESGRID"), DBOGrid) 'DBOGrid.getInstance(oForm, "LINESGRID")
            oForm.Freeze(True)
            oGridN.doPostReloadData(True)
            doUOMCombo(oGridN)
            doDocTypeCombo(oGridN)
            doCCICCombo(oGridN)
            doTransactionCode(oGridN)
            doCurrencyCombo(oGridN)

            'If Not sBPCurrency.Equals("##") Then
            '    oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BPCurr")).Editable = False
            'Else
            '    oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BPCurr")).Editable = True
            'End If

            oForm.Freeze(False)
            'doLoadData(oForm)
            'Dim sParentID As String = doGetParentID(oForm.UniqueID)
            'If Not String.IsNullOrEmpty(sParentID) Then
            '    'Dim oParentForm As SAPbouiCOM.Form = doGetForm(sParentID)
            '    'Dim oForecast As User.IDH_BPFORECST = getWFValue(oForm, "FCDBObject")

            '    'If oForecast IsNot Nothing Then
            '    'here want to call data reload
            '    '         oForecast.
            '    'End If
            'End If
        End Sub
#End Region
        Public Overrides Sub doFinalizeShow(oForm As SAPbouiCOM.Form)
            MyBase.doFinalizeShow(oForm)
            ' Dim oBPFC As IDH_BPFORECST = thisBPForecast(oForm)

        End Sub
        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                oForm.AutoManaged = False
                oForm.EnableMenu("1282", False)
                oForm.EnableMenu("1281", True)
                oForm.DataBrowser.BrowseBy = "IDH_CODE"
                oForm.Items.Item("IDH_CODE").AffectsFormMode = False
                oForm.Title = gsTitle
                oForm.DefButton = ""
                doAddUFCheck(oForm, "IDH_SHWACT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, False, "Y", "N")
                doAddUF(oForm, "IDH_CUST", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50, False, True)
                doAddUF(oForm, "IDH_NAME", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 200, False, True)
                'setUFValue(oForm, "IDH_NAME", sCardName)
                'setItemValue(oForm, "IDH_CUST", sCardCode)
                'setItemValue(oForm, "IDH_NAME", sCardName)
                'setItemValue(oForm, "IDH_CODE", sCardCode)
                'setItemValue(oForm, "IDH_CDNM", sCardName)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
                BubbleEvent = False
            End Try

        End Sub

        Protected Sub doSetFilterFields(ByVal oGridN As DBOGrid) 'IDHAddOns.idh.controls.FilterGrid)

            'oGridN.doAddFilterField("IDH_CUST", IDH_BPFORECST._CardCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30)
            'oGridN.doAddFilterField("IDH_NAME", IDH_BPFORECST._CardName, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 100)

        End Sub

        Protected Sub doSetListFields(ByVal oGridN As DBOGrid) 'IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField(IDH_BPFORECST._Code, "Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._Name, "Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._MatStatus, "Status", True, 0, "CHECKBOX", Nothing)
            oGridN.doAddListField(IDH_BPFORECST._DocType, "Doc Type", True, 20, "COMBOBOX", Nothing)
            ''oGridN.doAddListField("U_ItemCd", "Waste Code", True, 20, "SRC:IDHWISRC(WST)[IDH_ITMCOD][ITEMCODE;U_ItemDsc=ITEMNAME;U_UOM=UOM]", Nothing, iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField(IDH_BPFORECST._ItemCd, "Waste Code", True, 20, "SRC:IDHWISRC(WST)[IDH_ITMCOD;IDH_INVENT=][ITEMCODE;U_ItemDsc=ITEMNAME]", Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField(IDH_BPFORECST._ItemDsc, "Waste Description", True, -1, "SRC:IDHWISRC(WST)[IDH_ITMNAM;IDH_INVENT=%N][U_ItemCd=ITEMCODE;ITEMNAME]", Nothing)

            If Config.ParameterAsBool("FLALTITM", False) = True Then
                '#MA issue:336 20170329
                'oGridN.doAddListField(IDH_BPFORECST._AltWasDsc, "Alternative Waste Description", True, 20, "SRC:IDHALTWISRC(AWST)[IDH_ALITM=" + IDH_BPFORECST._AltWasDsc + ";IDH_ITMCOD=#" + IDH_BPFORECST._ItemCd + ";IDH_ITMNAM=#" + IDH_BPFORECST._ItemDsc + "][ItemAltName;" + IDH_BPFORECST._AltWDSCd + "=ItemAltCode]", Nothing)
                oGridN.doAddListField(IDH_BPFORECST._AltWasDsc, "Alternative Waste Description", True, 20, "SRC*IDHALTWISRC(AWST)[IDH_ALITM=" + IDH_BPFORECST._AltWasDsc + ";IDH_ITMCOD=#" + IDH_BPFORECST._ItemCd + ";IDH_ITMNAM=#" + IDH_BPFORECST._ItemDsc + "][ItemAltName;" + IDH_BPFORECST._AltWDSCd + "=ItemAltCode]", Nothing)
                '#MA end 20170329
                oGridN.doAddListField(IDH_BPFORECST._AltWDSCd, "Alternative Waste Desc Code", False, 0, Nothing, Nothing)
            End If

            oGridN.doAddListField(IDH_BPFORECST._EstQty, "Estimated Qty", True, 15, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._UOM, "UOM", True, 15, "COMBOBOX", Nothing)
            oGridN.doAddListField(IDH_BPFORECST._ActiveFrom, "Active From", True, 20, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._ActiveTo, "Active To", True, 20, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._ExpLdWgt, "Expected Load Weight", True, 15, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._ComQty, "Committed Qty", True, 15, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._RemCQty, "Remaining Commited Qty", True, 15, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._DoneQty, "Done Qty", True, 15, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._RemQty, "Remaining Qty", True, 15, Nothing, Nothing)
            ''##MA Start 23-10-2014 Issue#419
            If Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_BPFORECAST_PRICERS) = False Then '
                oGridN.doAddListField(IDH_BPFORECST._BuyingPrice, "Buying Price", True, 0, Nothing, Nothing)
                oGridN.doAddListField(IDH_BPFORECST._SellingPrice, "Selling Price", True, 0, Nothing, Nothing)
            Else
                oGridN.doAddListField(IDH_BPFORECST._BuyingPrice, "Buying Price", True, 20, Nothing, Nothing)
                oGridN.doAddListField(IDH_BPFORECST._SellingPrice, "Selling Price", True, 20, Nothing, Nothing)
            End If
            ''##MA Start 23-10-2014 Issue#419
            oGridN.doAddListField(IDH_BPFORECST._CardCd, "Customer", False, 0, Nothing, Nothing) ', iFilterBackColor, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField(IDH_BPFORECST._CardName, "Customer Name", False, 0, Nothing, Nothing)
            'oGridN.doAddListField(IDH_BPFORECST._BPCurr, "Customer Currency", False, 20, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._BPCurr, "Customer Currency", True, 20, "COMBOBOX", Nothing)
            'oGridN.doAddListField(IDH_BPFORECST._CardType, "Customer Type", False, 20, Nothing, Nothing)

            oGridN.doAddListField(IDH_BPFORECST._Address, "Address", True, -1, "SRC*IDHASRCH(ADDR)[IDH_CUSCOD=>IDH_CUST;IDH_ADRTYP=%S;IDH_APPLYFLTR=" & Config.INSTANCE.getParameterAsBool("FLTRADDR", False).ToString & "][ADDRESS;U_AddrssLN=ADDRSCD]", Nothing)
            oGridN.doAddListField(IDH_BPFORECST._AddrssLN, "Address Line Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField(IDH_BPFORECST._CCIC, "CCIC", True, 20, "COMBOBOX", Nothing)
            oGridN.doAddListField(IDH_BPFORECST._CustRef, "Customer PO", True, -1, Nothing, Nothing)
            '## MA Start 15-10-2014
            oGridN.doAddListField(IDH_BPFORECST._TRNCd, "Transaction Code", True, 30, "COMBOBOX", Nothing)
            '## MA End 15-10-2014
            oGridN.doAddListField(IDH_BPFORECST._LnkWOH, "Linked WOH", False, -1, Nothing, Nothing)

            oGridN.doSynchFields()
        End Sub

        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            'Clear the description field if code is empty
            'If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SEARCH AndAlso pVal.BeforeAction AndAlso pVal.ColUID = IDH_BPFORECST._AltWasDsc Then
            '    'goParent.STATUSBAR.SetText(pVal.EventType.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            '    setParentSharedData(oForm, "IDH_ITMCOD", pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._ItemCd), pVal.Row))
            '    setParentSharedData(oForm, "IDH_ITMNAM", pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._ItemDsc), pVal.Row))
            ''Using below login in DB Object
            'ElseIf False AndAlso pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED AndAlso pVal.BeforeAction = False Then
            '    If pVal.ColUID = IDH_BPFORECST._Address Then
            '        Dim sAddress As String = pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._Address), pVal.Row)
            '        If sAddress.Trim = "" Then
            '            pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._AddrssLN), pVal.Row, "")
            '        Else

            '            Dim sCardCode As String = IDH_BPFORECST._CardCd
            '            If sCardCode.Trim <> "" AndAlso sAddress.Trim <> "" Then
            '                Dim sAddressLineNum As String = com.idh.bridge.lookups.Config.INSTANCE.getValueFromBPShipToAddress(sCardCode, sAddress, "LineNum")
            '                pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._AddrssLN), pVal.Row, sAddressLineNum)
            '            Else

            '                pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._AddrssLN), pVal.Row, "")
            '            End If
            '        End If
            '    End If



            '    'If pVal.ColUID = IDH_BPFORECST._ItemCd Then '(pVal.ColUID = "U_CustCode") Then
            '    '    Dim sItmCode As String = pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._ItemCd), pVal.Row)
            '    '    Dim sItmName As String = pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._ItemDsc), pVal.Row)
            '    '    If sItmCode.Trim = "" AndAlso sItmName.Trim <> "" Then
            '    '        pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._ItemDsc), pVal.Row, "")
            '    '    End If

            '    'ElseIf pVal.ColUID = IDH_BPFORECST._ItemDsc Then '(pVal.ColUID = "U_CustCode") Then
            '    '    Dim sItmCode As String = pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._ItemCd), pVal.Row)
            '    '    Dim sItmName As String = pVal.oGrid.doGetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._ItemDsc), pVal.Row)
            '    '    If sItmCode.Trim <> "" AndAlso sItmName.Trim = "" Then
            '    '        pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._ItemDsc), pVal.Row, "")
            '    '    End If
            '    'End If


            '    'ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SEARCH AndAlso pVal.oData.GetType.Name.ToString = "en_DialogResults" Then
            '    'If ((pVal.ColUID = IDH_BPFORECST._ItemCd OrElse pVal.ColUID = IDH_BPFORECST._ItemDsc) AndAlso DirectCast(pVal.oData, com.idh.bridge.lookups.Base.en_DialogResults) = com.idh.bridge.lookups.Base.en_DialogResults.CANCELED) Then
            '    '    pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._ItemCd), pVal.Row, "")
            '    '    pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._ItemCd), pVal.Row, "")
            '    '    Return False
            '    'ElseIf ((pVal.ColUID = IDH_BPFORECST._Address) AndAlso DirectCast(pVal.oData, com.idh.bridge.lookups.Base.en_DialogResults) = com.idh.bridge.lookups.Base.en_DialogResults.CANCELED) Then
            '    '    pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._ItemCd), pVal.Row, "")
            '    '    pVal.oGrid.doSetFieldValue(pVal.oGrid.doIndexFieldWC(IDH_BPFORECST._ItemCd), pVal.Row, "")
            '    '    Return False
            '    'End If

            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
                If pVal.BeforeAction = True Then
                    If lookups.Config.INSTANCE.doGetDoForecasting() Then
                        Dim oGridN As DBOGrid = CType(DBOGrid.getInstance(oForm, "LINESGRID"), DBOGrid) 'getWFValue(oForm, "LINESGRID")
                        Dim oSelected As SAPbouiCOM.SelectedRows = oGridN.getGrid().Rows.SelectedRows
                        Dim iSelectionCount As Integer = oSelected.Count

                        If iSelectionCount = 1 Then
                            Dim oMenuItem As SAPbouiCOM.MenuItem
                            Dim oMenus As SAPbouiCOM.Menus
                            Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
                            'Dim sSelectedForcast As String
                            Dim iRow As Integer
                            Dim sForecastNr As String
                            Dim sWONr As String

                            iRow = oGridN.GetDataTableRowIndex(oSelected, 0)
                            sForecastNr = CType(oGridN.doGetFieldValue("Code", iRow), String)
                            sWONr = CType(oGridN.doGetFieldValue("U_LnkWOH", iRow), String)

                            oMenuItem = goParent.goApplication.Menus.Item(IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU)
                            oMenus = oMenuItem.SubMenus
                            oCreationPackage = CType(goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams), SAPbouiCOM.MenuCreationParams)
                            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING

                            oCreationPackage.UniqueID = "NEXTPER"
                            oCreationPackage.String = getTranslatedWord("Create Forecast for Next Period") & " (" & (iRow + 1) & ")[" & sForecastNr & "]"
                            oCreationPackage.Enabled = True
                            oMenus.AddEx(oCreationPackage)
                            '## MA Start 28-08-2014 Issue#409; Stop Create WOR if not active of below Activeto date
                            Dim sActiveTo As String = CType(oGridN.doGetFieldValue("U_ActiveTo", iRow), String)
                            Dim bActive As Boolean = True
                            If sActiveTo IsNot Nothing AndAlso sActiveTo <> "" AndAlso CDate(sActiveTo) < Today Then
                                bActive = False
                            End If
                            'Dim sStart As String = oGridN.doGetFieldValue("U_MatStatus", iRow)
                            If sWONr.Length = 0 AndAlso bActive Then
                                oCreationPackage.UniqueID = "NEWWO"
                                oCreationPackage.String = getTranslatedWord("Create Waste Order from Forecast") & " (" & (iRow + 1) & ")[" & sForecastNr & "]"
                                oCreationPackage.Enabled = True
                                oMenus.AddEx(oCreationPackage)
                            End If
                            '## MA End 28-08-2014 Issue#409; Stop Create WOR if not active of below Activeto date
                        End If
                        Return True
                    End If
                Else
                    If goParent.goApplication.Menus.Exists("NEXTPER") Then
                        goParent.goApplication.Menus.RemoveEx("NEXTPER")
                    End If
                    If goParent.goApplication.Menus.Exists("NEWWO") Then
                        goParent.goApplication.Menus.RemoveEx("NEWWO")
                    End If
                    Return True
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK Then
                Dim sField As String = pVal.ColUID
                If sField.Equals("U_LnkWOH") Then
                    If pVal.BeforeAction = False Then
                        Dim oGridN As DBOGrid = CType(DBOGrid.getInstance(oForm, "LINESGRID"), DBOGrid) 'getWFValue(oForm, "LINESGRID")
                        Dim oForecast As IDH_BPFORECST = CType(oGridN.DBObject, IDH_BPFORECST)
                        oForecast.Handler_ForecastRefresh = New IDH_BPFORECST.et_ExternalTrigger(AddressOf doHandleBPForecastUpdated)
                        'Dim oData As New ArrayList

                        '' 0 - Action
                        '' 1 - WO Code
                        '' 2 - IDH_BPFORECST
                        'oData.Add("FCDBOUpdate")
                        'oData.Add(oForecast.U_LnkWOH)
                        'oData.Add(oForecast)
                        'goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)

                        Try
                            Dim oOrder As com.isb.core.forms.WasteOrder = New com.isb.core.forms.WasteOrder(oForm.UniqueID, oForecast.U_LnkWOH, oForecast)
                            'oOrder.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WOROKReturn)
                            'oOrder.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WORCancelReturn)

                            oOrder.doShowModal()
                        Catch Ex As Exception
                            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD001", Nothing)
                        End Try

                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT Then
                If pVal.BeforeAction = False Then
                    Dim oMenuEvt As SAPbouiCOM.MenuEvent = CType(pVal.oData, SAPbouiCOM.MenuEvent)
                    If oMenuEvt.MenuUID.Equals("NEXTPER") Then
                        Dim oGridN As DBOGrid = CType(DBOGrid.getInstance(oForm, "LINESGRID"), DBOGrid) 'getWFValue(oForm, "LINESGRID")
                        doCreateNextPeriod(oGridN)
                    ElseIf oMenuEvt.MenuUID.Equals("NEWWO") Then

                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_EDIT_MODE Then
                            com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage("UNSAVED")
                        Else
                            Dim oWO As com.idh.dbObjects.User.IDH_JOBENTR = New com.idh.dbObjects.User.IDH_JOBENTR()
                            Dim sBP1 As String = getItemValue(oForm, "IDH_CUST")
                            Dim sBR2 As String = getItemValue(oForm, "IDH_LBPC") 'getDFValue(oForm, "OCRD", "U_IDHLBPC")
                            Dim sBPType As String = getItemValue(oForm, "IDH_BPTYP") ' getDFValue(oForm, "OCRD", "CardType")

                            Dim oSelected As SAPbouiCOM.SelectedRows = pVal.oGrid.getGrid().Rows.SelectedRows
                            Dim iSrcRow As Integer = pVal.oGrid.GetDataTableRowIndex(oSelected, 0)

                            If lookups.Config.INSTANCE.doGetDoForecasting() Then
                                Dim oGridN As DBOGrid = CType(DBOGrid.getInstance(oForm, "LINESGRID"), DBOGrid) 'getWFValue(oForm, "LINESGRID")
                                Dim oForecast As IDH_BPFORECST = CType(oGridN.DBObject, IDH_BPFORECST)
                                oForecast.Handler_ForecastRefresh = New IDH_BPFORECST.et_ExternalTrigger(AddressOf doHandleBPForecastUpdated)

                                oForecast.gotoRow(iSrcRow)
                                Dim sAddress As String = oForecast.U_Address
                                Dim sAddressLN As String = oForecast.U_AddrssLN
                                oWO.doSetDefaultBPs()
                                ''## MA Start 03-07-2017
                                If sBPType = "C" Then
                                    'oWO.doSetCustomer(sBP1, sAddress)
                                    'oWO.doSetProducer(sBR2)
                                    oWO.doSetCustomer(sBP1, sAddress)
                                    oWO.doSetProducer(sBR2, sAddress)
                                Else
                                    'oWO.doSetCustomer(sBR2)
                                    'oWO.doSetProducer(sBP1, sAddress)
                                    oWO.doSetProducer(sBP1, sAddress)
                                    oWO.doSetCustomer(sBR2, sAddress)
                                End If
                                ''## MA End 03-07-2017
                                oWO.U_ForeCS = oForecast.Code
                                oWO.U_Status = oForecast.U_DocType

                                oWO.MustUpdateForecast = False
                                oWO.U_User = CType(goParent.goDICompany.UserSignature(), Short)
                                oWO.U_LckPrc = "Y"
                                ''## MA Start 15-10-2017
                                oWO.U_TRNCd = oForecast.U_TRNCd
                                ''## MA End 15-10-2017

                                If Not oForecast.U_CustRef Is Nothing AndAlso oForecast.U_CustRef.Length > 0 Then
                                    oWO.U_CustRef = oForecast.U_CustRef
                                End If

                                Try
                                    oWO.MustLoadChildren = True
                                    Dim oOrder As com.isb.core.forms.WasteOrder = New com.isb.core.forms.WasteOrder(oForm.UniqueID, oWO, oForecast)
                                    oOrder.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WOROKReturn)
                                    'oOrder.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WORCancelReturn)

                                    oOrder.doShowModal()
                                Catch Ex As Exception
                                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD001", Nothing)
                                End Try

                                'If oWO.doAddDataRow() Then
                                '    oForecast.U_LnkWOH = oWO.Code
                                '    If Not oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                '        oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                '    End If
                                '    'Dim oData As New ArrayList
                                '    '' 0 - Action
                                '    '' 1 - WO Code
                                '    '' 2 - IDH_BPFORECST
                                '    'oData.Add("FCDBOUpdate")
                                '    'oData.Add(oWO.Code)
                                '    'oData.Add(oForecast)
                                '    'goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)

                                '    Try
                                '        oWO.MustLoadChildren = True
                                '        Dim oOrder As com.isb.core.forms.WasteOrder = New com.isb.core.forms.WasteOrder(oForm.UniqueID, oWO.Code, oForecast)
                                '        'oOrder.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WOROKReturn)
                                '        'oOrder.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf Handler_WORCancelReturn)

                                '        oOrder.doShowModal()
                                '    Catch Ex As Exception
                                '        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD001", Nothing)
                                '    End Try

                                'End If
                                oWO.MustUpdateForecast = True
                            End If
                        End If
                    End If
                End If
            ElseIf pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY Then
                Dim sCardCode As String = getItemValue(oForm, "IDH_CUST").ToString.Trim
                If sCardCode = "" Then
                    Return False
                End If
                If pVal.BeforeAction = False Then
                    doSetLastLine(oForm, pVal.Row)
                End If
            End If

            Return MyBase.doCustomItemEvent(oForm, pVal)
        End Function

        Public Function Handler_WOROKReturn(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Dim oOrder As com.isb.core.forms.WasteOrder = oOForm
            oOrder.Forecast.U_LnkWOH = oOrder.SourceWO.Code
            If Not oOForm.SBOParentForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                oOForm.SBOParentForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            End If
            'oWO.MustUpdateForecast = True
            Return True
        End Function

        '## End
        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doMenuEvent(oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.MenuUID = Config.NAV_FIND OrElse _
                   pVal.MenuUID = Config.NAV_FIRST OrElse _
                   pVal.MenuUID = Config.NAV_LAST OrElse _
                   pVal.MenuUID = Config.NAV_NEXT OrElse _
                   pVal.MenuUID = Config.NAV_PREV Then
                If pVal.BeforeAction = True Then
                    oForm.Freeze(True)
                Else
                    Try
                        If pVal.MenuUID = Config.NAV_FIND Then
                            'Dim sCardCode As String = getItemValue(oForm, "IDH_CODE")
                            setItemValue(oForm, "IDH_CUST", "")
                            setItemValue(oForm, "IDH_NAME", "")
                            setItemValue(oForm, "IDH_CODE", "")
                            setItemValue(oForm, "IDH_CDNM", "")
                            setItemValue(oForm, "IDH_BPCURR", "")
                            doLoadData(oForm)
                        Else
                            Dim sCardCode As String = getItemValue(oForm, "IDH_CODE")
                            setItemValue(oForm, "IDH_CUST", sCardCode)
                            setItemValue(oForm, "IDH_NAME", getItemValue(oForm, "IDH_CDNM"))
                            'setItemValue(oForm, "IDH_BPCURR", getItemValue(oForm, "IDH_BPCURR"))
                            doLoadData(oForm)
                            'oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        End If
                    Catch ex As Exception
                    End Try
                    oForm.Freeze(False)
                End If
            End If
            Return MyBase.doMenuEvent(oForm, pVal, BubbleEvent)
        End Function
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = True Then
                    If (pVal.ItemUID = "IDH_CUST") AndAlso pVal.ItemChanged AndAlso wasSetByUser(oForm, pVal.ItemUID) Then
                        setItemValue(oForm, "IDH_NAME", "")
                        doCheckForFilter(oForm)
                        'BubbleEvent = False
                        'Return False
                    ElseIf (pVal.ItemUID = "IDH_NAME") AndAlso pVal.ItemChanged AndAlso wasSetByUser(oForm, pVal.ItemUID) Then
                        setItemValue(oForm, "IDH_CUST", "")
                        doCheckForFilter(oForm)
                        'BubbleEvent = False
                        'Return False
                    End If
                    End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ALL_EVENTS.et_ITEM_PRESSED Then
                If pVal.ItemUID = "IDH_SHWACT" AndAlso pVal.BeforeAction = False Then
                    doLoadData(oForm)
                    ' doFilterRecord(getItemValue(oForm, "IDH_SHWACT"))
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_CLICK Then
                'IDH_CFLBP
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_CFLBP" Then
                        doChooseBP(oForm)
                    End If
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_CUST" OrElse pVal.ItemUID = "IDH_NAME" Then
                        If pVal.CharPressed = 13 Then
                            goParent.goApplication.SendKeys("{TAB}")
                        End If
                    End If
                End If
            ElseIf pVal.ItemUID.Equals("LINESGRID") Then
                If pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
                    If pVal.BeforeAction = True AndAlso _
                     pVal.ItemChanged Then
                        Dim oGridN As DBOGrid = CType(DBOGrid.getInstance(oForm, "LINESGRID"), DBOGrid) 'getWFValue(oForm, "FORECGRID")
                        Dim oForecast As IDH_BPFORECST = CType(oGridN.DBObject, IDH_BPFORECST)
                        Dim sFieldItem As String = oGridN.doFindDBField(pVal.ColUID, True).Trim()
                        Dim iRow As Integer = oGridN.GetDataTableRowIndex(pVal.Row)
                        If sFieldItem.Equals(IDH_BPFORECST._UOM) Then
                            'If oForecast.U_ComQty <> 0 OrElse oForecast.U_DoneQty <> 0 Then
                            oForecast.OldUOM = CType(oGridN.doGetFieldValue(IDH_BPFORECST._UOM, iRow), String)
                            'Else
                            '   oForecast.OldUOM = "NS"
                            'End If
                        End If
                    End If
                End If

            End If
            Return True
        End Function

        Public Overrides Sub doButtonID1(oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                If Not pVal.BeforeAction Then
                    doFind(oForm)
                    BubbleEvent = False
                End If
                Exit Sub
            ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE AndAlso pVal.BeforeAction Then
                If lookups.Config.INSTANCE.doGetDoForecasting() Then
                    Dim oUpdateGrid As DBOGrid = CType(getWFValue(oForm, "LINESGRID"), DBOGrid)
                    'Validate Grid for Multi Ccy BP's 
                    If getItemValue(oForm, "IDH_BPCURR").ToString.Trim = "##" Then
                        Dim oDBGrid As DBOGrid = CType(DBOGrid.getInstance(oForm, "LINESGRID"), DBOGrid)
                        Dim bValid As Boolean = True
                        Dim iRowCount As Integer = If(oDBGrid.getRowCount > 1, oDBGrid.getRowCount - 2, oDBGrid.getRowCount - 1)
                        Dim iLastIndex As Integer = 0
                        For iIndex As Integer = 0 To iRowCount
                            iLastIndex = iIndex
                            Dim sBPCurr As String = CType(oDBGrid.doGetFieldValue(IDH_BPFORECST._BPCurr, iIndex), String)
                            If sBPCurr = "" Then
                                bValid = False
                                Exit For
                            End If
                        Next
                        If bValid Then
                            If oUpdateGrid.DBObject.doProcessData() = False Then
                                BubbleEvent = False
                            End If
                        Else
                            doResError("Invalid Currency, please select a valid currency for the forecast row(" + iLastIndex.ToString + ").", "ERIVCBPF", Nothing)
                            BubbleEvent = False
                            Exit Sub
                        End If
                    Else
                        If oUpdateGrid.DBObject.doProcessData() = False Then
                            BubbleEvent = False
                        End If
                    End If
                End If
                If goParent.doCheckModal(oForm.UniqueID) = True Then
                    doReturnFromModalShared(oForm, True)
                Else
                    'If getWFValue(oForm, "DOWR1") = True Then
                    '    doShowWR1(oForm, True)
                    '    oForm.Items.Item("IDH_BACOPY").Visible = False
                    'End If
                End If
            Else
                'setWFValue(oForm, "WR1REFRESH", True)
            End If


            'MyBase.doButtonID1(oForm, pVal, BubbleEvent)

        End Sub
        Private Function doFind(oForm As SAPbouiCOM.Form) As Boolean
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try

                Dim sCardCode As String = getItemValue(oForm, "IDH_CUST")
                Dim sCardName As String = getItemValue(oForm, "IDH_NAME")

                If sCardCode.Trim = "" AndAlso sCardName.Trim = "" Then
                    Return False
                End If
                Dim sSQL As String = "Select CardCode,CardName from OCRD "
                Dim sRequired As String = ""
                If sCardCode.Trim <> "" Then
                    sRequired = " CardCode='" & sCardCode.Trim.Replace("*", "%") & "' "
                End If
                If sCardName.Trim <> "" Then
                    If sRequired = "" Then
                        sRequired = " CardName LIKE '" & sCardName.Trim.Replace("*", "%") & "' "
                    Else
                        sRequired = " And CardName LIKE '" & sCardName.Trim.Replace("*", "%") & "' "
                    End If
                End If
                sSQL &= " WHERE " & sRequired & " Order By CardCode"
                oRecordSet = goParent.goDB.doSelectQuery(sSQL)
                If oRecordSet.RecordCount = 0 Then
                    Return False
                End If
                sCardCode = CType(oRecordSet.Fields.Item("CardCode").Value, String).Trim()
                sCardName = CType(oRecordSet.Fields.Item("CardName").Value, String).Trim()

                doLoadForm(oForm, sCardCode, sCardName)
                Return True
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString(), "Error finding BP Forecasting.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFBPF", {Nothing})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
                oForm.Freeze(False)
            End Try
            Return False
        End Function

        Private Overloads Sub doLoadForm(ByVal OForm As SAPbouiCOM.Form, ByVal sCardCode As String, ByVal sCardName As String)
            setUFValue(OForm, "IDH_CUST", sCardCode)
            setUFValue(OForm, "IDH_NAME", sCardName)
            setItemValue(OForm, "IDH_CUST", sCardCode)
            setItemValue(OForm, "IDH_NAME", sCardName)
            setItemValue(OForm, "IDH_CODE", sCardCode)
            setItemValue(OForm, "IDH_CDNM", sCardName)
            Dim db As SAPbouiCOM.DBDataSource
            Dim oConditions As SAPbouiCOM.Conditions = New SAPbouiCOM.Conditions
            Dim oCondition As SAPbouiCOM.Condition = oConditions.Add

            db = OForm.DataSources.DBDataSources.Item("OCRD")
            db.Clear()
            oCondition.Alias = "CardCode"
            oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
            oCondition.CondVal = sCardCode
            db.Query(oConditions)

            doLoadData(OForm)
            setItemValue(OForm, "IDH_CUST", sCardCode)
            setItemValue(OForm, "IDH_NAME", sCardName)
            OForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE

        End Sub

        Protected Overrides Sub doHandleModalCanceled(ByVal oParentForm As SAPbouiCOM.Form, ByVal sModalFormType As String)
            Try
                If sModalFormType = "IDHCSRCH" Then
                    Dim sTarget As String = getSharedDataAsString(oParentForm, "TRG")
                    If sTarget = "BPSRC" Then
                        setItemValue(oParentForm, "IDH_CUST", "")
                        setItemValue(oParentForm, "IDH_NAME", "")
                        doLoadData(oParentForm)
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Model Canceling - " & sModalFormType)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCMR", {sModalFormType})
            End Try
        End Sub
        Protected Overrides Sub doHandleModalBufferedResult(ByVal oForm As SAPbouiCOM.Form, ByRef oData As Object, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            MyBase.doHandleModalBufferedResult(oForm, oData, sModalFormType, sLastButton)
        End Sub
        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            Try
                If sModalFormType = "IDHCSRCH" Then
                    Dim sTarget As String = getSharedDataAsString(oForm, "TRG")
                    ' Dim val As String = getSharedData(oParentForm, "CARDCODE")
                    If sTarget = "BPSRC" Then
                        Dim sCardCode As String = getSharedDataAsString(oForm, "CARDCODE")
                        Dim sCardName As String = getSharedDataAsString(oForm, "CARDNAME")
                        Dim sBPCurrency As String = getSharedDataAsString(oForm, "CURRENCY")
                        'If sBPCurrency.Equals("##") Then
                        '    sBPCurrency = String.Empty
                        '    setSharedData(oForm, "CURRENCY", sBPCurrency)
                        '    setItemValue(oForm, "IDH_BPCURR", sBPCurrency)
                        'Else
                        '    setItemValue(oForm, "IDH_BPCURR", sBPCurrency)
                        'End If
                        setItemValue(oForm, "IDH_BPCURR", sBPCurrency)
                        doLoadForm(oForm, sCardCode, sCardName)
                        '#MA point:386 20170609
                        If doSetChoosenCustomerFromShared(oForm) = False Then
                            Dim oGridN As DBOGrid = CType(DBOGrid.getInstance(oForm, "LINESGRID"), DBOGrid)
                            oGridN.doEnabled(False)
                        End If
                        '#MA point:386 end 20170609
                        'doSetLastLine(oForm, -1)
                    End If
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal Result - " & sModalFormType)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", {sModalFormType})
            End Try
        End Sub

        Private Sub doChooseBP(ByRef oForm As SAPbouiCOM.Form)

            Dim sCardCode As String = getItemValue(oForm, "IDH_CUST").ToString.Trim
            Dim sCardName As String = getItemValue(oForm, "IDH_NAME").ToString.Trim


            setSharedData(oForm, "TRG", "BPSRC")
            setSharedData(oForm, "IDH_BPCOD", sCardCode)
            setSharedData(oForm, "IDH_NAME", sCardName)
            'setSharedData(oForm, "IDH_TYPE", "F-S")
            setSharedData(oForm, "IDH_TYPE", "")
            setSharedData(oForm, "IDH_GROUP", "")
            setSharedData(oForm, "SILENT", "SHOWMULTI")
            goParent.doOpenModalForm("IDHCSRCH", oForm)
        End Sub
        Public Sub doSetLastLine(ByVal oForm As SAPbouiCOM.Form, ByVal iRow As Int32)
            If lookups.Config.INSTANCE.doGetDoForecasting() Then
                Dim oGridN As DBOGrid = CType(DBOGrid.getInstance(oForm, "LINESGRID"), DBOGrid)
                If iRow = -1 Then
                    iRow = oGridN.getCurrentDataRowIndex()
                End If
                'Dim sKey As String = DataHandler.doGenerateCode("BPFORECST")
                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode("BPFORECST")
                oGridN.doSetFieldValue("Code", iRow, oNumbers.CodeCode, True)
                oGridN.doSetFieldValue("Name", iRow, oNumbers.NameCode, True)

                Dim sCardCode As String = getItemValue(oForm, "IDH_CUST").ToString.Trim
                oGridN.doSetFieldValue("U_CardCd", iRow, sCardCode, True)

                Dim sCardName As String = getItemValue(oForm, "IDH_NAME").ToString.Trim
                oGridN.doSetFieldValue("U_CardName", iRow, sCardName, True)

                Dim sBPUOM As String = getItemValue(oForm, "IDH_BPUOM").ToString.Trim
                oGridN.doSetFieldValue("U_UOM", iRow, sBPUOM, True)

                Dim sCardType As String = getItemValue(oForm, "IDH_BPTYP").ToString.Trim
                oGridN.doSetFieldValue("U_CardType", iRow, sCardType, True)

                'Dim sBPCURR As String = getItemValue(oForm, "IDH_BPCURR").ToString.Trim
                oGridN.doSetFieldValue("U_BPCurr", iRow, "", True)

                oGridN.doSetFieldValue("U_EstQty", iRow, 0, True)
                oGridN.doSetFieldValue("U_ComQty", iRow, 0, True)
                oGridN.doSetFieldValue("U_DoneQty", iRow, 0, True)
                oGridN.doSetFieldValue("U_RemQty", iRow, 0, True)
                oGridN.doSetFieldValue("U_RemCQty", iRow, 0, True)
            End If
        End Sub

        Private Sub doDocTypeCombo(ByVal oGridN As DBOGrid)
            ' Dim oGridN As DBOGrid = DBOGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_DocType")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            Try
                doFillCombo(oGridN.getSBOForm(), oCombo, "[@IDH_WRSTATUS]", "Code", "Name", Nothing, Nothing, Nothing, Nothing)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Type Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Type")})
            End Try
        End Sub

        Private Sub doUOMCombo(ByVal oGridN As DBOGrid)
            ' Dim oGridN As DBOGrid = DBOGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_UOM")), SAPbouiCOM.ComboBoxColumn)
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

                doFillCombo(oGridN.getSBOForm(), oCombo, "OWGT", "UnitDisply", "UnitName", Nothing, Nothing, Nothing, Nothing)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the UOM Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("UOM")})
            End Try
        End Sub
        Private Sub doCCICCombo(ByVal oGridN As DBOGrid)
            ' Dim oGridN As DBOGrid = DBOGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_CCIC")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCombo.ValidValues)

            Try
                Dim sCCICValue As String = Config.INSTANCE.getParameterWithDefault("CCICSTR", "")
                Dim oList() As String = sCCICValue.Split(","c)
                If oList.Length > 0 Then
                    oCombo.ValidValues.Add("", "")
                    Dim iCount As Integer
                    Dim sCode As String
                    For iCount = 0 To oList.Length() - 1
                        sCode = oList(iCount).Trim()
                        oCombo.ValidValues.Add(sCode, sCode)
                    Next
                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Type Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Type")})
            End Try
        End Sub

        Private Sub doTransactionCode(ByVal oGridN As DBOGrid)
            ' Dim oGridN As DBOGrid = DBOGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_TRNCd")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            doClearValidValues(oCombo.ValidValues)

            Try
                oCombo = CType(oGridN.getSBOGrid().Columns.Item(oGridN.doIndexFieldWC("U_TRNCd")), SAPbouiCOM.ComboBoxColumn)
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
                FillCombos.TransactionCodeCombo(oCombo, getItemValue(oGridN.getSBOForm, "IDH_CUST").ToString.Trim, Nothing, "WO")
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Transaction Code Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("Transaction Code")})
            End Try
        End Sub

        Private Sub doCurrencyCombo(ByVal oGridN As DBOGrid)
            ' Dim oGridN As DBOGrid = DBOGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Try
                oCombo = CType(oGridN.getSBOGrid.Columns.Item(oGridN.doIndexFieldWC("U_BPCurr")), SAPbouiCOM.ComboBoxColumn)
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
                doClearValidValues(oCombo.ValidValues)
                FillCombos.CurrencyCombo(oCombo)
                'doFillCombo(oGridN.getSBOForm(), oCombo, "OWGT", "UnitDisply", "UnitName", Nothing, Nothing, Nothing, Nothing)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the UOM Combo.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {com.idh.bridge.Translation.getTranslatedWord("UOM")})
            End Try
        End Sub

        Public Sub doCreateNextPeriod(ByVal oGridN As DBOGrid)
            oGridN.getSBOForm().Freeze(True)
            Try
                Dim oForecast As IDH_BPFORECST = CType(oGridN.DBObject, IDH_BPFORECST)

                'Dim sForecastNr As String
                Dim iSrcRow As Integer
                Dim iDstRow As Integer = oGridN.getLastRowIndex()
                Dim oSelected As SAPbouiCOM.SelectedRows

                oSelected = oGridN.getGrid().Rows.SelectedRows()
                oGridN.doAddRow(False, True)

                'sForecastNr = DataHandler.doGenerateCode("BPFORECST")
                iSrcRow = oGridN.GetDataTableRowIndex(oSelected, 0)
                oGridN.doCopyRow(iSrcRow, iDstRow)

                Dim oToDate As DateTime = Conversions.ToDateTime(oGridN.doGetFieldValue("U_ActiveTo", iSrcRow))
                If com.idh.utils.Dates.isValidDate(oToDate) Then
                    Dim oFromDate As DateTime
                    Dim iMonth As Integer = oToDate.Month
                    Dim iYear As Integer = oToDate.Year

                    iMonth = iMonth + 1
                    If iMonth > 12 Then
                        iMonth = iMonth - 12
                        iYear = iYear + 1
                    End If
                    oFromDate = New DateTime(iYear, iMonth, 1)
                    oToDate = oForecast.doCalcToDate(oFromDate, miDefaultMonthPeriod)

                    oGridN.doSetFieldValue("U_ActiveFrom", iDstRow, oFromDate, True)
                    oGridN.doSetFieldValue("U_ActiveTo", iDstRow, oToDate, True)
                End If
                Dim oEstQty As Double = Conversions.ToDouble(oGridN.doGetFieldValue("U_EstQty", iSrcRow))

                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode("BPFORECST")

                oGridN.doActivateRow(iDstRow)
                oGridN.doSetFieldValue("Code", iDstRow, oNumbers.CodeCode, True)
                oGridN.doSetFieldValue("Name", iDstRow, oNumbers.NameCode, True)

                'oGridN.doSetFieldValue("U_EstQty", iDstRow, 0, True)
                oGridN.doSetFieldValue("U_ComQty", iDstRow, 0, True)
                oGridN.doSetFieldValue("U_DoneQty", iDstRow, 0, True)
                oGridN.doSetFieldValue("U_RemQty", iDstRow, oEstQty, True)
                oGridN.doSetFieldValue("U_RemCQty", iDstRow, oEstQty, True)
                oGridN.doSetFieldValue("U_LnkWOH", iDstRow, "", True)

                If Not oGridN.getSBOForm().Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    oGridN.getSBOForm().Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                End If
            Finally
                oGridN.getSBOForm().Freeze(False)
            End Try
        End Sub

        Private Sub doCheckForFilter(ByVal oForm As SAPbouiCOM.Form)

            If (oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                Dim oUpdateGrid As DBOGrid = CType(getWFValue(oForm, "LINESGRID"), DBOGrid)
                If oUpdateGrid.getRowCount > 0 Then
                    Dim sCardCode As String = CType(oUpdateGrid.doGetFieldValue("U_CardCd", oUpdateGrid.getLastRowIndex), String)
                    If sCardCode.Trim <> "" Then
                        If Messages.INSTANCE.doResourceMessageYN("GEDCOMT", Nothing, 1) = 1 Then
                            If oUpdateGrid.DBObject.doProcessData() = False Then
                                'doLoadData(oForm)
                                'oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                                Exit Sub
                            End If
                        End If
                        'Else
                        '    doLoadData(oForm)
                        '    oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                    End If
                    'doLoadData(oForm)
                    'oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                    'Else
                    '    doLoadData(oForm)
                    '    oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                End If
                doChooseBP(oForm)
                'doLoadData(oForm)
                'oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
            ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Or oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                doChooseBP(oForm)
                'doLoadData(oForm)
                'oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
            End If
        End Sub
        Protected Function doSetChoosenCustomerFromShared(ByVal oForm As SAPbouiCOM.Form, Optional ByVal bDoBPRules As Boolean = True, Optional ByVal bDoBuyAndTrade As Boolean = False) As Boolean
            Dim val As String = Nothing
            Dim val2 As String
            Dim val3 As String
            Dim val4 As String
            Dim dCreditLimit As Decimal = 0
            Dim dCreditRemain As Decimal = 0
            Dim dDeviationPrc As Decimal = 0
            Dim dTBalance As Decimal = 0
            Dim sFrozen As String = "N"
            Dim sFrozenComm As String = Nothing
            Dim sIgnoreCheck As String = "N"
            Dim sObligated As String
            Dim sCustomerAddress As String = Nothing
            Dim sPaymentTermCode As String
            Dim dBalance As Decimal = 0
            Dim dWROrders As Decimal = 0

            Dim sFrozenFrom As String
            Dim sFrozenTo As String
            Dim oFrozenFrom As Date
            Dim oFrozenTo As Date

            Dim WasteLicRegNo As String = ""
            Dim CusFName As String = ""
            Try
                val = getSharedData(oForm, "CARDCODE")
                val2 = getSharedData(oForm, "PHONE1")
                val3 = getSharedData(oForm, "CNTCTPRSN")
                val4 = getSharedData(oForm, "CARDNAME")
                dCreditLimit = getSharedData(oForm, "CREDITLINE")
                dCreditRemain = getSharedData(oForm, "CRREMAIN")
                dDeviationPrc = getSharedData(oForm, "DEVIATION")
                dTBalance = getSharedData(oForm, "TBALANCE")

                sFrozen = getSharedData(oForm, "FROZEN")
                sFrozenComm = getSharedData(oForm, "FROZENC")
                sFrozenFrom = getSharedData(oForm, "FROZENF")
                sFrozenTo = getSharedData(oForm, "FROZENT")

                sIgnoreCheck = getSharedData(oForm, "IDHICL")
                sObligated = getSharedData(oForm, "IDHOBLGT")
                sPaymentTermCode = getSharedData(oForm, "PAYTRM")
                dBalance = getSharedData(oForm, "BALANCE")
                dWROrders = getSharedData(oForm, "WRORDERS")
                WasteLicRegNo = getSharedData(oForm, "WASLIC")
                CusFName = getSharedData(oForm, "CARDFNAME")
                oFrozenFrom = com.idh.utils.Dates.doStrToDate(sFrozenFrom)
                oFrozenTo = com.idh.utils.Dates.doStrToDate(sFrozenTo)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the chosen Customer.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSVS", {com.idh.bridge.Translation.getTranslatedWord("Chosen Customer"), val.ToString()})
                Return False
            End Try

            If Not sIgnoreCheck = "Y" Then
                Dim bBlockOnFail As Boolean = Config.INSTANCE.getParameterAsBool("DOBCCL", True)
                'If idh.const.Lookup.INSTANCE.doCheckCredit2(goParent, val, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc, 0, sFrozen, sFrozenComm, False, bBlockOnFail, Nothing, oFrozenFrom, oFrozenTo) = False Then
                '    Return False
                'End If
                If Config.INSTANCE.doCheckCredit2(val, dCreditLimit, dTBalance, dCreditRemain, dDeviationPrc, 0, False, bBlockOnFail, Nothing) = False Then
                    Return False
                End If

            End If
            Return True
        End Function


    End Class
End Namespace
