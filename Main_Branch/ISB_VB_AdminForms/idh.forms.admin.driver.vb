Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports com.idh.bridge
Imports com.uBC.forms
Imports com.idh.bridge.lookups
Imports com.idh.dbObjects.User

Namespace idh.forms
	Public Class DriverForm
		Inherits IDHAddOns.idh.forms.Base

		Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
			'MyBase.New(oParent, "IDHDDF","IDHMAST", iMenuPosition, "Driver Form.srf", False, True, False )
			MyBase.New(oParent, "IDHDDF","IDHMAST", iMenuPosition, "Driver Form.srf", False, True, False, "Driver Form", load_Types.idh_LOAD_NORMAL)
            'doAddImagesToFix("IDH_REGL")
		   
		   	doEnableHistory("IDH_DRVMAS")
		End Sub
		
		Public Overrides Sub doFinalizeShow(ByVal oForm As SAPbouiCOM.Form)
			oForm.Visible = True
		End Sub
		
'	   '*** Create Sub-Menu
'		Protected Overrides Sub doCreateSubMenu()
'			Dim oMenus As SAPbouiCOM.Menus
'			Dim oMenuItem As SAPbouiCOM.MenuItem
'			Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
'			
'			oMenuItem = goParent.goApplication.Menus.Item("IDHMAST")
'			
'			oMenus = oMenuItem.SubMenus
'			oCreationPackage = goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
'			oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING
'			oCreationPackage.UniqueID = gsType
'			oCreationPackage.String = "Driver Form"
'			oCreationPackage.Position = giMenuPosition
'			
'			Try
'				oMenus.AddEx(oCreationPackage)
'			Catch ex As Exception
'			End Try
'		End Sub
		
		Protected Overrides Sub doSetEventFilters()
			doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
		   	doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
		End Sub
		
	Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
			MyBase.doCompleteCreate(oForm, BubbleEvent)
			
			Try
'				Dim oItem As SAPbouiCOM.Item
				
				oForm.DataSources.DBDataSources.Add("@IDH_DRVMAS")
				
				doAddDF(oForm, "@IDH_DRVMAS", "IDH_VEHREG", "U_VehReg")
				doAddDF(oForm, "@IDH_DRVMAS", "IDH_DRIVNM",  "U_DrivNM")
				doAddDF(oForm, "@IDH_DRVMAS", "IDH_DRIVSN",  "U_DrivSN")
			    doAddDF(oForm, "@IDH_DRVMAS", "IDH_CODE",  "CODE")
			    doAddDF(oForm, "@IDH_DRVMAS", "IDH_CONTN",  "U_ContN")
			  
			    oForm.DataBrowser.BrowseBy = "IDH_CODE"
			 
	            oForm.SupportedModes = -1
                oForm.AutoManaged = False

                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE

			Catch ex As Exception
				com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
			End Try
		End Sub		 
	
	Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
		MyBase.doItemEvent(oForm, pVal, BubbleEvent)
		If pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
			If pVal.BeforeAction = False Then
				If doCheckSearchKey(pVal) Then
					If pVal.ItemUID = "IDH_VEGREG" Then
						Dim sAct As String = getWFValue(oForm, "LastVehAct")
                            If sAct = "DOSEARCH" Then
                                doLorrySearch(oForm)
                                'Dim sReg As String = getDFValue(oForm, "@IDH_DRVMAS", "U_VehReg", True)

                                'setSharedData(oForm, "IDH_VEHREG", sReg)
                                'setSharedData(oForm, "SHOWCREATE", "TRUE")
                                'setSharedData(oForm, "SILENT", "SHOWMULTI")

                                'setSharedData(oForm, "SHOWACTIVITY", "FALSE")
                                'goParent.doOpenModalForm("IDHLOSCH", oForm)
                            End If
                        End If
				Else
					If pVal.ItemUID = "IDH_REGL" Then
						setWFValue(oForm, "LastVehAct", "DOSEARCH")
					End If
				End If
			End If
		ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
			If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_REGL" Then
                        doLorrySearch(oForm)
                        'Dim sReg As String
                        'sReg = getDFValue(oForm, "@IDH_DRVMAS", "U_VehReg", True)
                        'setSharedData(oForm, "IDH_VEHREG", sReg)
                        'setSharedData(oForm, "SHOWCREATE", "TRUE")
                        'setSharedData(oForm, "SHOWACTIVITY", "FALSE")
                        'goParent.doOpenModalForm("IDHLOSCH", oForm)
                    ElseIf pVal.ItemUID = "IDH_DEL" Then
                        doRemove(oForm)
                    End If
			End If
			
		End If
		Return False
	End Function
	
	Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            MyBase.doHandleModalResultShared(oForm, sModalFormType, sLastButton)
            doHandleVehicleSearchResult(oForm, sLastButton)
            'Try
            '    If sModalFormType = "IDHLOSCH" Then
            '        setDFValue(oForm, "@IDH_DRVMAS", "U_VehReg", getSharedData(oForm, "REG"), False, True)
            '        setDFValue(oForm, "@IDH_DRVMAS", "U_DRIVNM", getSharedData(oForm, "DRVERNM"))
            '    End If
            'Catch ex As Exception
            'End Try
	End Sub

	Private Sub doRemove(ByVal oForm As SAPbouiCOM.Form)
		Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_DRVMAS")
		Try
			Dim sCODE As String = getDFValue(oForm, "@IDH_DRVMAS", "Code")
			Dim iwResult As Integer
			Dim swResult As String = Nothing
			
			oAuditObj.setKeyPair(sCODE, IDHAddOns.idh.data.AuditObject.ac_Types.idh_REMOVE)
			iwResult = oAuditObj.Remove()
			If iwResult <> 0 Then
				goParent.goDICompany.GetLastError(iwResult, swResult)
				com.idh.bridge.DataHandler.INSTANCE.doError("Error Remove: " + swResult, "Error removing the driver - " & sCODE)
			Else
				oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
				doRefreshForm(oForm)
			End If
		Catch ex As Exception
			com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error removing the driver.")
		End Try
	End Sub
  	
  	Private Function doValidateHeader(ByVal oForm As SAPbouiCOM.Form) As Boolean 
  		Dim sDRIVNM As String
  		
  		sDRIVNM = getDFValue(oForm, "@IDH_DRVMAS", "U_DRIVNM")
  		
  		Dim sError As String = ""
  		If sDRIVNM.Length = 0 Then sError = sError + "DRIVNM, "
  		
  		If sError.Length > 0 Then
  			com.idh.bridge.DataHandler.INSTANCE.doError("The following fields must be set: " & sError)
  			Return False
  		End If
  		Return True
  	End Function
  	
  	
  	Public Overrides Function doMenuEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) As Boolean
  		If pVal.BeforeAction = False Then
                If pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_ADD Then
                    oForm.Freeze(True)
                    Try
                        'doRemoveLinkedDocs(oForm)
                        doCreateNewEntry(oForm)
                    Catch ex As Exception
                        com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the menu event - " & oForm.UniqueID & "." & pVal.MenuUID)
                    Finally
                        oForm.Freeze(False)
                    End Try

                ElseIf pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_FIND OrElse _
                  pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_FIRST OrElse _
                  pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_LAST OrElse _
                  pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_NEXT OrElse _
                  pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_PREV Then
                    oForm.Freeze(True)
                    Try
                        If pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_FIND Then
                            'doRemoveLinkedDocs(oForm)
                            doClearAll(oForm)
                        Else
                            'doRemoveLinkedDocs(oForm)
                            doLoadData(oForm)
                        End If
                    Catch ex As Exception
                    Finally
                        oForm.Freeze(False)
                    End Try
                End If
  		Else
                If pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_ADD OrElse _
                    pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_FIND OrElse _
                    pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_FIRST OrElse _
                    pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_LAST OrElse _
                    pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_NEXT OrElse _
                    pVal.MenuUID = com.idh.bridge.lookups.Config.NAV_PREV Then

                    Return True
                End If
  		End If
        End Function	
	 
	 Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
	 	Dim sCODE As String = getDFValue(oForm, "@IDH_DRVMAS", "Code")
	 	If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
	 		doSwitchToFind(oForm)
	 	ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
	 		doSwitchToAdd(oForm)
	 	ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
	 		doSwitchToAdd(oForm)
	 	End If
	 End Sub
	 
	 Protected Sub doSwitchToFind(ByVal oForm As SAPbouiCOM.Form)
	 	Dim oExclude() As String = {"IDH_DRIVNM","IDH_CODE"}
	 	doSetFocus(oForm, "IDH_DRIVNM")
	 	DisableAllEditItems(oForm, oExclude)
	 End Sub
	 
	 Protected Sub doSwitchToAdd(ByVal oForm As SAPbouiCOM.Form)
	 	Dim oExclude() As String = {"IDH_CODE"}
	 	doSetFocus(oForm, "IDH_DRIVNM")
	 	EnableAllEditItems(oForm, oExclude)
	 End Sub
	 
	 Protected Function doFind(ByVal oForm As SAPbouiCOM.Form) As Boolean
	 	oForm.Freeze(True)
	 	Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
	 	
'	 	Dim sMode As String
	 	Try
	 		If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
	 			Dim sCode As String = ""
	 			Dim sCustomerCd As String = ""
	 			Dim sCustomerNm As String = ""
	 			
	 			sCode = getItemValue(oForm, "IDH_CODE")
	 			sCustomerCd = getItemValue(oForm, "IDH_DRIVNM")
	 			sCustomerNm = getItemValue(oForm, "IDH_DRIVSN")
	 			
	 			Dim sQry As String
	 			
	 			'oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
	 			sQry = "SELECT Top 1 Code from [@IDH_DRVMAS] Where Code = Code "
	 			If sCode.Length() > 0 Then
	 				sQry = sQry & " AND Code = '" & sCODE & "'"
	 			End If
	 			
	 			If sCustomerCd.Length() > 0 Then
	 				sCustomerCd = sCustomerCd.Replace("*", "%")
	 				sQry = sQry & " AND U_DRIVNM LIKE '" & sCustomerCd & "%'"
	 			End If
	 			
	 			If sCustomerNm.Length() > 0 Then
	 				sCustomerNm = sCustomerNm.Replace("*", "%")
	 				sQry = sQry & " AND U_DRIVSN LIKE '" & IDHAddOns.idh.data.Base.doFixForSQL(sCustomerNm) & "%'"
	 			End If
	 			
                    'sQry = sQry & " Order by CAST(Code As Numeric) DESC"
                    sQry = sQry & " Order by Code DESC"
	 			'oRecordSet.DoQuery(sQry)
	 			oRecordSet = goParent.goDB.doSelectQuery(sQry)
	 			If oRecordSet.RecordCount > 0 Then
	 				Dim db As SAPbouiCOM.DBDataSource
	 				db = oForm.DataSources.DBDataSources.Item("@IDH_DRVMAS")
	 				
	 				sCode = oRecordSet.Fields.Item("Code").Value.Trim()
	 				db.Clear()
	 				
	 				Dim oConditions As SAPbouiCOM.Conditions = New SAPbouiCOM.Conditions
	 				Dim oCondition As SAPbouiCOM.Condition = oConditions.Add
	 				oCondition.Alias = "Code"
	 				oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
	 				oCondition.CondVal = sCode
	 				db.Query(oConditions)
	 				
	 				oForm.Update()
	 				
	 				oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
	 				doLoadData(oForm)
	 			Else
	 				com.idh.bridge.DataHandler.INSTANCE.doError("Code not found: " & sCode & "." & sCustomerCd & "." & sCustomerNm)
	 			End If
	 		End If
	 		Return False
	 	Catch ex As Exception
	 		com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error finding the driver.")
	 	Finally
	 		IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
	 		oForm.Freeze(False)
	 	End Try
	 End Function
	
'	Protected Function doFind(ByVal oForm As SAPbouiCOM.Form) As Boolean
'		oForm.Freeze(True)
'		Dim oRecordSet As SAPbobsCOM.Recordset
'		
'		Dim sMode As String
'		Try
'			If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
'				Dim sCode As String = ""
'				'Dim sDrivNM As String = ""
'				'Dim sDrivSN As String = ""
'				Dim sREG As String = ""
'				
'				sREG = getItemValue(oForm, "IDH_VEHREG")
'				sCode = getItemValue(oForm, "IDH_CODE")
'				'sDrivnm = getItemValue(oForm, "IDH_DrivNM")
'				'sDrivsn = getItemValue(oForm, "IDH_DrivSN")
'				
'				Dim db As SAPbouiCOM.DBDataSource
'				db = oForm.DataSources.DBDataSources.Item("@IDH_DRVMAS")
'				
'				
'				'sCode = oRecordSet.Fields.Item("Code").Value.Trim()
'				db.Clear()
'				
'				Dim oConditions = New SAPbouiCOM.Conditions
'				Dim oCondition As SAPbouiCOM.Condition
'				If Not sCode Is Nothing AndAlso sCode.Length > 0 Then
'					oCondition = oConditions.Add
'					oCondition.Alias = "Code"
'					oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
'					oCondition.CondVal = sCode
'				End If
'				'db.Query(oConditions)
'				If Not sREG Is Nothing AndAlso sREG.Length > 0 Then
'					oCondition = oConditions.Add()
'					oCondition.Alias = "U_VehReg"
'					oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
'					oCondition.CondVal = sREG
'				End If
'				
'				db.Query(oConditions)
'				
'				oForm.Update()
'				
'				oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
'				doLoadData(oForm)
'				' Else
'				'doError("doFind", "The Code was not found", "Code not found: " & sCode & "." & sCustomerCd & "." & sCustomerNm)
'			End If
'			'End If
'			'Return False
'		Catch ex As Exception
'			com.idh.bridge.DataHandler.INSTANCE.doError("doFind", ex.ToString(), "")
'		Finally
'			IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
'			oForm.Freeze(False)
'		End Try
'	 
'	 End Function
	
	Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
		Dim bDoNew As Boolean = False
		Dim sU_CODE As String
		If pVal.BeforeAction = True Then
			sU_CODE = getDFValue(oForm, "@IDH_DRVMAS", "Code")
			If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
					oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
				oForm.Freeze(True)
				Try
					If doValidateHeader(oForm) = False Then
						BubbleEvent = False
						Exit Sub
					End If
					
					Dim bDoContinue As Boolean = False
					goParent.goDICompany.StartTransaction()
					
					If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
						If doUpdateHeader(oForm, True) = True Then
							bDoContinue = True
							bDoNew = True
						End If
					Else
						If doUpdateHeader(oForm, False) = True Then
							bDoContinue = True
						End If
					End If
					
					If bDoContinue Then
						If goParent.goDICompany.InTransaction Then
							goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
						End If
						com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
						doLoadData(oForm)
						
						'doRemoveFormRows(oForm.UniqueID)
						If bDoNew Then
							setWFValue(oForm, "LastContract", -1)
							If goParent.doCheckModal(oForm.UniqueID) = False Then
								doCreateNewEntry(oForm)
							Else
								oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
								BubbleEvent = False
							End If
						End If
					Else
						If goParent.goDICompany.InTransaction Then
							goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
						End If
						com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
						BubbleEvent = False
						Exit Sub
					End If
					oForm.Update()
				Catch ex As Exception
					If goParent.goDICompany.InTransaction Then
						goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
					End If
					com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors()
				Finally
					oForm.Freeze(False)
				End Try
			ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
				'Set the Modal data and close
				If goParent.doCheckModal(oForm.UniqueID) = True Then
					doSetModalData(oForm)
					BubbleEvent = False
				End If
			ElseIf oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
				doFind(oForm)
				BubbleEvent = False
			End If
		Else
			If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
				oForm.Items.Item("IDH_CODE").Enabled = False
			End If
		End If
	End Sub
	
	Private Function doUpdateHeader(ByVal oForm As SAPbouiCOM.Form, ByVal bIsAdd As Boolean) As Boolean
		Dim iwResult As Integer = 0
		Dim swResult As String = Nothing
		
		Dim oAuditObj As New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_DRVMAS")
		Try
			
			Dim sCode As String = getDFValue(oForm, "@IDH_DRVMAS", "Code")
			If oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) Then
				If bIsAdd Then
					com.idh.bridge.DataHandler.INSTANCE.doError("Duplicate entries are not allowed")
					Return False
					
				End If
			Else
			oAuditObj.setKeyPair(sCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
			oAuditObj.setName(getDFValue(oForm, "@IDH_DRVMAS","Name"))
			End If
			oAuditObj.setFieldValue("U_VehReg", getDFValue(oForm, "@IDH_DRVMAS","U_VehReg"))
			oAuditObj.setFieldValue("U_CONTN", getDFValue(oForm, "@IDH_DRVMAS","U_CONTN"))
			oAuditObj.setFieldValue("U_DRIVNM", getDFValue(oForm, "@IDH_DRVMAS","U_DRIVNM"))
			oAuditObj.setFieldValue("U_DRIVSN", getDFValue(oForm, "@IDH_DRVMAS","U_DRIVSN"))
			
			iwResult = oAuditObj.Commit()
			If iwResult <> 0 Then
				goParent.goDICompany.GetLastError(iwResult, swResult)
				com.idh.bridge.DataHandler.INSTANCE.doError("System: Error Adding - " + swResult, "Error updating the header.")
				Return False
			End If
			
		Catch ex As Exception
			com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error updating the header.")
			Return False
		Finally
			IDHAddOns.idh.data.Base.doReleaseObject(oAuditObj)
		End Try
		Return True
	End Function
	
	Private Sub doCreateNewEntry(ByVal oForm As SAPbouiCOM.Form)
		
		With oForm.DataSources.DBDataSources.Item("@IDH_DRVMAS")
			.InsertRecord(.Offset)
                'Dim iCode As Integer = goParent.goDB.doNextNumber("DRVMAST")
                Dim sCode As String = DataHandler.doGenerateCode(Nothing, "DRVMAST")
			
			setDFValue(oForm, "@IDH_DRVMAS", "Code", sCode)
			setDFValue(oForm, "@IDH_DRVMAS", "Name", sCode)
			setDFValue(oForm, "@IDH_DRVMAS", "U_DRIVNM", "")
			setDFValue(oForm, "@IDH_DRVMAS", "U_CONTN", "")
			setDFValue(oForm, "@IDH_DRVMAS", "U_VehReg", "")
			setDFValue(oForm, "@IDH_DRVMAS", "U_DRIVSN", "")
	
	End With
		
		doRefreshForm(oForm)
		doLoadData(oForm)
		
	End Sub

	Private Sub doClearAll(ByVal oForm As SAPbouiCOM.Form)
		Dim sCode As String = Nothing
		Dim bIsFind As Boolean = False
		If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
			bIsFind = True
		End If
		
		setDFValue(oForm, "@IDH_DRVMAS", "Code", "sCode")
		setDFValue(oForm, "@IDH_DRVMAS", "Name", sCode)
		setDFValue(oForm, "@IDH_DRVMAS", "U_DRIVNM", "")
		setDFValue(oForm, "@IDH_DRVMAS", "U_CONTN", "")
		setDFValue(oForm, "@IDH_DRVMAS", "U_VehReg", "")
		setDFValue(oForm, "@IDH_DRVMAS", "U_DRIVSN", "")
		
		doRefreshForm(oForm)
		doLoadData(oForm)
		
	End Sub
	
	Public Overrides Sub doClose()
	End Sub
	
	'MODAL DIALOG
	'*** Set the return data when called as a modal dialog
	Private Sub doSetModalData(ByVal oForm As SAPbouiCOM.Form)
		doReturnFromModalShared(oForm, True)
        End Sub

#Region "Buffer Zone"
        Private Sub doLorrySearch(ByVal oForm As SAPbouiCOM.Form)
            If Config.ParameterAsBool("VMUSENEW", False) = False Then
                Dim sReg As String = getDFValue(oForm, "@IDH_DRVMAS", "U_VehReg", True)

                setSharedData(oForm, "IDH_VEHREG", sReg)
                setSharedData(oForm, "SHOWCREATE", "TRUE")
                setSharedData(oForm, "SILENT", "SHOWMULTI")

                setSharedData(oForm, "SHOWACTIVITY", "FALSE")
                goParent.doOpenModalForm("IDHLOSCH", oForm)
            Else
                doCreateLorrySearch(oForm)
            End If
        End Sub

        Private Sub doHandleVehicleSearchResult(ByVal oForm As SAPbouiCOM.Form, ByVal sLastButton As String)
            Try
                setDFValue(oForm, "@IDH_DRVMAS", "U_VehReg", getSharedData(oForm, "REG"), False, True)
                setDFValue(oForm, "@IDH_DRVMAS", "U_DRIVNM", getSharedData(oForm, "DRVERNM"))
            Catch ex As Exception
            End Try
        End Sub

#End Region

#Region "New Lorry Search"
        Private Sub doCreateLorrySearch(ByVal oForm As SAPbouiCOM.Form)
            Dim oOOForm As com.uBC.forms.fr3.VehicleM = New com.uBC.forms.fr3.VehicleM(Nothing, oForm.UniqueID, Nothing)
            oOOForm.Handler_DialogOkReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doLorrySelection)
            oOOForm.Handler_DialogCancelReturn = New com.idh.forms.oo.Form.DialogReturn(AddressOf doLorryCancel)

            oOOForm.doShowModal(oForm)
        End Sub

        Public Function doLorrySelection(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Dim oOOForm As com.uBC.forms.fr3.VehicleM = oOForm
            Dim oVehMast As IDH_VEHMAS2 = oOOForm.DBObject
            setFormDFValueFromUser(oOForm.SBOParentForm, "U_VehReg", oVehMast.U_VehReg)
            setFormDFValueFromUser(oOForm.SBOParentForm, "U_DRIVNM", oVehMast.U_Driver)
            Return True
        End Function

        Public Function doLorryCancel(ByVal oOForm As com.idh.forms.oo.Form) As Boolean
            Return True
        End Function
#End Region
End Class
End Namespace
