Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Imports com.idh.dbObjects.User

Namespace idh.forms.admin
    Public Class FuelSurcharge
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "ISBFULSCH", sParMenu, iMenuPosition, Nothing, "Fuel Surcharge")
            '**********************************************************
            '******OnTime [#Ico000????] USA  **************************
            '**********************************************************
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "ISB_FUELSCHG"
        End Function

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", True, 0, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_VendorCd", "Vendor Code", True, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("U_VendorNm", "Vendor Name", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_StartDt", "Start Date", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_EndDt", "End Date", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Rate", "Rate %", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_CostTrf", "Cost Tariff", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_ChargeTrf", "Charge Tariff", True, -1, Nothing, Nothing)
        End Sub

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)

            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
            End If

            oGridN.setOrderValue("Code")
        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            MyBase.doButtonID1(oForm, pVal, BubbleEvent)

            If pVal.BeforeAction = False Then
                'IDH_JOBTYPE_SEQ.getInstance(true)
            End If
        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            'Return MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
                If pVal.BeforeAction = False Then
                    If pVal.CharPressed = 9 Then
                        If pVal.ItemUID = "LINESGRID" Then
                            Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
                            Dim sVendorCd As String = CType(oGridN.doGetFieldValue("U_VendorCd"), String)
                            If sVendorCd = "*" Then
                                setSharedData(oForm, "SILENT", "SHOWMULTI")
                                setSharedData(oForm, "IDH_TYPE", "S")
                                goParent.doOpenModalForm("IDHCSRCH", oForm)
                            End If
                            oGridN = Nothing
                        End If
                    End If
                End If
            End If
            Return True
        End Function

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            If sModalFormType = "IDHCSRCH" Then
                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                Dim sCardCode As String = getSharedDataAsString(oForm, "CARDCODE")
                Dim sCardName As String = getSharedDataAsString(oForm, "CARDNAME")

                'Dim sValueNew As String = getSharedData(oForm, "IDH_CODE")
                'Dim sValueOld As String = oGridN.doGetFieldValue("U_IDHZPCD")
                'Dim sValueOut As String
                'If sValueNew.Length <> 0 Then
                '    sValueOld = Replace(sValueOld, "*", "")
                '    If sValueOld.Length <> 0 Then
                '        sValueOut = sValueOld + "," + sValueNew
                '    Else
                '        sValueOut = sValueNew
                '    End If
                'Else
                '    sValueOut = Replace(sValueOld, "*", "")
                'End If
                oGridN.doSetFieldValue("U_VendorCd", sCardCode)
                oGridN.doSetFieldValue("U_VendorNm", sCardName)
            End If
        End Sub

        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
        End Sub

    End Class
End Namespace
