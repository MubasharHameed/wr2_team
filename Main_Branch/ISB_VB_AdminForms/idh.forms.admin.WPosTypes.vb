Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class WPosTypes
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_WTPOTP", sParMenu, iMenuPosition, Nothing, "Waste Position Types" )
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Waste Position Types"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_WTPOTP"
        End Function

    End Class
End Namespace
