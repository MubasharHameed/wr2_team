Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class DIRECodes
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_WTDIRE", sParMenu, iMenuPosition, Nothing, "Disposal and Recovery Codes")
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Disposal and Recovery Codes"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_WTDIRE"
        End Function

    End Class
End Namespace
