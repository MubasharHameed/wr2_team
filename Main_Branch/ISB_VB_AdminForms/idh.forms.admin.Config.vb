Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System

Imports System.Threading
Imports com.idh.dbObjects.User

Namespace idh.forms.admin
    Public Class Config
        Inherits idh.forms.admin.Tmpl

        Private msPass As String = Nothing
        Private mbAuthPassed As Boolean = False

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHCONF", sParMenu, iMenuPosition, "WRConfig.srf", "WR Config")
        End Sub

        '        Protected Overrides Function getTitle() As String
        '            Return "WR Config"
        '        End Function

        Protected Overrides Function getUserTable() As String
            Return goParent.goConfigTable '"IDH_WRCONFIG"
        End Function

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)

            'Use This if you only wants to read the value when the AddOn starts
            'If msPass Is Nothing Then
            msPass = com.idh.bridge.lookups.Config.INSTANCE.doGetWRConfigPassword()
            'End If

            If mbAuthPassed OrElse msPass Is Nothing OrElse msPass.Length = 0 Then
                oForm.PaneLevel = 2
                setVisible(oForm, "IDH_SWPN", True)
            Else
                oForm.PaneLevel = 1
            End If

            Try
                Dim oCons As New SAPbouiCOM.Conditions
                Dim oCon As SAPbouiCOM.Condition

                oCon = oCons.Add
                oCon.Alias = "Name"
                oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                oCon.CondVal = "WR"
                oForm.DataSources.DBDataSources.Item("@IDH_AOVER").Query(oCons)

                If com.idh.bridge.lookups.Config.USERCACHEPREFIX IsNot Nothing AndAlso com.idh.bridge.lookups.Config.USERCACHEPREFIX.Length > 0 Then
                    setUFValue(oForm, "IDH_UC", com.idh.bridge.lookups.Config.USERCACHEPREFIX)
                Else
                    setUFValue(oForm, "IDH_UC", "Not Set")
                End If

                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                oGridN.doSetSBOAutoReSize(False)
                oGridN.doApplyExpand(IDHGrid.EXPANDED)
            Catch ex As Exception
                com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error preLoading the data.")
            End Try
        End Sub


        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True Then
                Dim oChangedRows As ArrayList = Nothing
                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                If oGridN IsNot Nothing Then
                    oChangedRows = oGridN.doGetChangedRows()

                    If oChangedRows IsNot Nothing AndAlso oChangedRows.Count > 0 Then
                        For Each iRow As Integer In oChangedRows
                            If oGridN.doGetFieldValue("Code", iRow) = "WOPRGC" Then
                                DirectCast(goParent.goDB, idh.data.DataStructures).doUpdateProgressView(-1)
                            End If
                        Next
                    End If
                End If
            End If

            MyBase.doButtonID1(oForm, pVal, BubbleEvent)

            If pVal.BeforeAction = False Then
                'If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE OrElse _
                '   oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                com.idh.bridge.lookups.Config.INSTANCE.doResetAll()
                com.idh.bridge.lookups.FixedValues.doReadValues()
                'End If
            End If

        End Sub

        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
            doAddUF(oForm, "IDH_PASS", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 20, False, False)
            doAddUF(oForm, "IDH_UC", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 254, False, False)
        End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("U_Cat", "Category", True, -1, Nothing, Nothing)
            oGridN.doAddListField("Code", "Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Val", "Value", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Desc", "Description", True, -1, Nothing, Nothing)
        End Sub

        '** The ItemEvent handler
        '** Return True if the Event must be handled by the other Objects 
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim bReturn As Boolean = MyBase.doItemEvent(oForm, pVal, BubbleEvent)

            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_CLRFRM" Then
                        doCloseMemoryForms()
                    ElseIf pVal.ItemUID = "IDH_SWPN" Then
                        If oForm.PaneLevel = 1 Then
                            oForm.PaneLevel = 2
                        Else
                            oForm.PaneLevel = 1
                        End If
                    ElseIf pVal.ItemUID = "IDH_DOLOGI" Then
                        Dim sEPass As String = getUFValue(oForm, "IDH_PASS", True)
                        If sEPass = msPass Then
                            oForm.PaneLevel = 2
                            mbAuthPassed = True
                            setVisible(oForm, "IDH_SWPN", True)
                        Else
                            Dim sUser As String = com.idh.bridge.DataHandler.INSTANCE.SBOCompany.UserName()
                            com.idh.bridge.DataHandler.INSTANCE.doError("AUTH: Incorrect Password - " & sUser & " - " & sEPass, "Incorrect Password please retry.")
                            'setUFValue(oForm, "IDH_MESS", "Incorrect Password please try again.")
                        End If
                    End If
                End If
            End If
            Return bReturn
        End Function

    End Class
End Namespace
