Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class CLocCodes
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_WTLOCA", sParMenu, iMenuPosition, Nothing, "Container Location Codes")
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "Container Location Codes"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_WTLOCA"
        End Function

    End Class
End Namespace
