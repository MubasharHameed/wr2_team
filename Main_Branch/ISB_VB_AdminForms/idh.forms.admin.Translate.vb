Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System
Imports com.idh.bridge.data

Imports com.idh.bridge.lookups
Imports com.idh.bridge
Imports com.idh.utils

Namespace idh.forms.admin
    Public Class Translate
        Inherits WR1_Admin.idh.forms.admin.Tmpl
        Private mbSimple As Boolean = True

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHTrans", sParMenu, iMenuPosition, "Translation.srf", "Translation")
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_TRANSL"
        End Function
        
        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
        End Sub
        
        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
            End If
            oGridN.AddEditLine = False
            
            doSetFilterFields(oGridN)
            doSetListFields(oGridN)
            'oGridN.setTableValue(getTable())
            'oGridN.setKeyField("Code")
            oGridN.doAddGridTable(New GridTable(getTable(), Nothing, "Code", True, True), True)
            
            oGridN.doSetDoCount(True)
            oGridN.doSetHistory(getUserTable(), "Code")
            If mbSimple Then
                oGridN.setOrderValue("U_Lang, U_OrigCap")
            Else
                oGridN.setOrderValue("U_Lang, U_FormId, U_ItemId, U_ColId, U_OrigCap")
            End If
            
            doLanguageCombo(oForm)
        End Sub

        Protected Overrides Sub doSetFilterFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            If mbSimple Then
                oGridN.doAddFilterField("IDH_LANG", "U_Lang", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, "")
                setEnableItem(oGridN.getSBOForm(), False, "IDH_LOC")
                setEnableItem(oGridN.getSBOForm(), False, "IDH_ID")
                setEnableItem(oGridN.getSBOForm(), False, "IDH_COL")
            Else
                oGridN.doAddFilterField("IDH_LANG", "U_Lang", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, "")
                oGridN.doAddFilterField("IDH_LOC", "U_FormId", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, "")
                oGridN.doAddFilterField("IDH_ID", "U_ItemId", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, "")
                oGridN.doAddFilterField("IDH_COL", "U_ColId", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, "")
            End If
        End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("U_FormId", "Location", False, 15, Nothing, Nothing)
            oGridN.doAddListField("U_Lang", "Language", True, 20, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_ItemId", "Identifier", True, CType(IIf(mbSimple, 0, -1), Integer), Nothing, Nothing)
            oGridN.doAddListField("U_ColId", "Column Id", True, CType(IIf(mbSimple, 0, -1), Integer), Nothing, Nothing)
            oGridN.doAddListField("U_OrigCap", "Original Word", False, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Caption", "Translation", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Descrpt", "Description", True, CType(IIf(mbSimple, 0, -1), Integer), Nothing, Nothing)

            oGridN.doAddListField("Code", "Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", False, 0, Nothing, Nothing)
        End Sub
        
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
            doGridLanguageCombo(UpdateGrid.getInstance(oForm, "LINESGRID"))
		End Sub

	   Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim bReturn As Boolean = MyBase.doItemEvent(oForm, pVal, BubbleEvent)
	        If pVal.EventType = SAPbouiCOM.BoEventTypes.et_COMBO_SELECT Then
	            If pVal.BeforeAction = False Then
	                If pVal.ItemUID = "IDH_LANG" Then
	                    doLocationCombo(oForm)
	                    doLoadData(oForm)
                    ElseIf pVal.ItemUID = "IDH_LOC" Then
                        If mbSimple = False Then
                            doIdentifiersCombo(oForm)
                            doLoadData(oForm)
                        End If
                    ElseIf pVal.ItemUID = "IDH_ID" Then
                        doColCombo(oForm)
                        doLoadData(oForm)
                    ElseIf pVal.ItemUID = "IDH_COL" Then
                        doLoadData(oForm)
                        End If
                    End If
                End If
	        Return bReturn
        End Function
                
        Private Sub doLanguageCombo(ByVal oForm As SAPbouiCOM.Form)
        	doFillCombo(oForm, "IDH_LANG", "OLNG", "Code", "Name", Nothing, "CAST(Code As Numeric)", True)
        	doLocationCombo(oForm)
        End Sub

        Private Sub doGridLanguageCombo(ByVal oGridN As UpdateGrid)
            ''GRID COMBO
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Dim oValidValues As SAPbouiCOM.ValidValues

            oCombo = CType(oGridN.Columns.Item(oGridN.doIndexFieldWC("U_Lang")), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
            oValidValues = oCombo.ValidValues

            doFillValidVaues(oGridN.getSBOForm(), oValidValues, "OLNG", "Code", "Name", Nothing, "CAST(Code As Numeric)", Nothing, Nothing)
        End Sub
        
        Public Sub doLocationCombo(ByVal oForm As SAPbouiCOM.Form)
            If mbSimple Then
                'setUFValue(oForm, "U_FormId", "WORDS")
                Dim oItem As SAPbouiCOM.Item
                oItem = oForm.Items.Item("IDH_LOC")
                oItem.DisplayDesc = True
                Dim oCombo As SAPbouiCOM.ComboBox
                oCombo = CType(oItem.Specific(), SAPbouiCOM.ComboBox)

                Dim oValidValues As SAPbouiCOM.ValidValues
                oValidValues = oCombo.ValidValues
                doClearValidValues(oValidValues)

                oValidValues.Add("WORDS", getTranslatedWord("Words"))
                oCombo.Select("WORDS")
            Else
                Dim sLang As String = Conversions.ToString(getUFValue(oForm, "IDH_LANG"))
                If sLang.Length = 0 Then
                    doClearCombo(oForm, "IDH_LOC", True)
                Else
                    Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                    Try
                        Dim oItem As SAPbouiCOM.Item
                        oItem = oForm.Items.Item("IDH_LOC")
                        oItem.DisplayDesc = True
                        Dim oCombo As SAPbouiCOM.ComboBox
                        oCombo = CType(oItem.Specific, SAPbouiCOM.ComboBox)

                        oRecordSet = CType(goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset), SAPbobsCOM.Recordset)

                        Dim sForms As String = " FROM [@IDH_TRANSL] WHERE U_Lang LIKE '" & sLang & "%' AND U_ItemId = '#TITLE'"
                        Dim sQuery As String = "SELECT U_FormId, U_Caption " & sForms & _
                         " UNION " & _
                         " SELECT U_FormId, 'No Caption' FROM [@IDH_TRANSL] WHERE U_Lang LIKE '" & sLang & "%' " & _
                           " AND U_FormId Not In (SELECT U_FormId " & sForms & ")"

                        oRecordSet.DoQuery(sQuery)
                        If oRecordSet.RecordCount > 0 Then
                            Dim oValidValues As SAPbouiCOM.ValidValues
                            oValidValues = oCombo.ValidValues
                            doClearValidValues(oValidValues)

                            oValidValues.Add("", getTranslatedWord("Any"))

                            Dim iCount As Integer
                            Dim sVal As String
                            Dim sDesc As String

                            For iCount = 0 To oRecordSet.RecordCount - 1
                                sVal = Conversions.ToString(oRecordSet.Fields.Item(0).Value)
                                sDesc = Conversions.ToString(oRecordSet.Fields.Item(1).Value)
                                Try
                                    oValidValues.Add(sVal, sDesc)
                                Catch ex As Exception
                                End Try
                                oRecordSet.MoveNext()
                            Next
                        End If
                    Catch ex As Exception
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling location Combo: " & oForm.TypeEx & ".IDH_LOC")
                        DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFFC", {oForm.TypeEx, "IDH_LOC"})
                    End Try
                    DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
                End If
                doIdentifiersCombo(oForm)
            End If
        End Sub

       	Public Sub doIdentifiersCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim sLang As String = Conversions.ToString(getUFValue(oForm, "IDH_LANG"))
            Dim sLoc As String = Conversions.ToString(getUFValue(oForm, "IDH_LOC"))
       		If sLoc.Length = 0 Then
       			doClearCombo(oForm, "IDH_ID", True)
			Else
       			doFillCombo(oForm, "IDH_ID", "[@IDH_TRANSL]", "U_ItemId", "U_OrigCap", " U_Lang LIKE '" & sLang & "%' AND U_FormId LIKE '" & sLoc & "%'", Nothing, True)
       		End If
       		doColCombo(oForm)
       	End Sub

       	Public Sub doColCombo(ByVal oForm As SAPbouiCOM.Form)
            Dim sLang As String = Conversions.ToString(getUFValue(oForm, "IDH_LANG"))
            Dim sLoc As String = Conversions.ToString(getUFValue(oForm, "IDH_LOC"))
            Dim sId As String = Conversions.ToString(getUFValue(oForm, "IDH_ID"))
       		If sId.Length = 0 Then
       			doClearCombo(oForm, "IDH_COL", True)
			Else
       			doFillCombo(oForm, "IDH_COL", "[@IDH_TRANSL]", "U_ColId", "U_OrigCap", " U_Lang LIKE '" & sLang & "%' AND U_FormId LIKE '" & sLoc & "%' AND U_ItemId LIKE '" & sId & "%'", Nothing, True)
       		End If
       		
       	End Sub
       	
       	Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            If pVal.BeforeAction = True AndAlso _
                (oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE OrElse oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE) Then
                Dim oUpdateGrid As UpdateGrid
                oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                If oUpdateGrid.doProcessData() = False Then
                    BubbleEvent = False
				Else
                    DirectCast(goParent.goDB, WR1_Data.idh.data.DataStructures).doCreateViews(True)
                    Config.INSTANCE.doResetProgressQueryStr()
                End If
            End If
        End Sub

    End Class
End Namespace

