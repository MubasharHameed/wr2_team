Imports System.IO
Imports System.Collections

Namespace idh.forms.admin
    Public Class Origin
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDH_ORIGIN", sParMenu, iMenuPosition, Nothing, "EA Origins")
        End Sub

'        Protected Overrides Function getTitle() As String
'            Return "EA Origins"
'        End Function

        Protected Overrides Function getUserTable() As String
            Return "IDH_ORIGIN"
        End Function

    End Class
End Namespace

