Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls

Imports com.idh.dbObjects.User
Imports com.idh.bridge

Namespace idh.forms.admin
    Public Class JobTypes
        Inherits idh.forms.admin.Tmpl

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal sParMenu As String, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHJTAD", sParMenu, iMenuPosition, Nothing, "Item Group / Order type" )
        End Sub

        Protected Overrides Function getUserTable() As String
            Return "IDH_JOBTYPE"
        End Function

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.FilterGrid)
            oGridN.doAddListField("Code", "Code", True, 0, Nothing, Nothing)
            oGridN.doAddListField("Name", "Name", True, 0, Nothing, Nothing)
            oGridN.doAddListField("U_ItemGrp", "Item Group", True, -1, "COMBOBOX", Nothing)
            oGridN.doAddListField("U_JobTp", "Order Type", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Seq", "Sequence", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Direct", "Direction", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IsFirst", "First Job", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Billa", "Tip Billable", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_HBilla", "Haulage Billable", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_IsJob", "Is a Job", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_RemarksReq", "Remarks Required", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_SellF", "OITM Sel Field", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_CostF", "OITM Cost Field", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_OrdCat", "Order Category", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_WastCa", "Waste Carrier (Doc)", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Custom", "Customer (Doc)", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Produc", "Producer(Doc)", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Site", "Disposal Site(Doc)", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_SuppLc", "Skip licence Supplier(Doc)", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_SuppCo", "Congestion supplier(Doc)", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_CarrCd", "Carrier Code(Doc)", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_SuppCo", "Congestion supplier(Doc)", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_CarrCd", "Carrier Code", True, -1, Nothing, Nothing)
            oGridN.doAddListField("U_Tip", "Disposal Site Code", True, -1, Nothing, Nothing)
        End Sub
        
        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
        	MyBase.doBeforeLoadData(oForm)
        	
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If oGridN Is Nothing Then
                oGridN = New UpdateGrid(Me, oForm, "LINESGRID")
            End If
            
            oGridN.setOrderValue("U_ItemGrp, U_Seq, U_JobTp")
        End Sub
        
        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
            MyBase.doButtonID1(oForm,pVal,BubbleEvent)

            If pVal.BeforeAction = False Then
            	IDH_JOBTYPE_SEQ.getInstance(true)
            End If
        End Sub
        
        
        '** The Initializer
        Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doLoadData(oForm)
            doItemGroupCombo(oForm)
        End Sub        

        Private Sub doItemGroupCombo(ByVal oForm As SAPbouiCOM.Form)
        	Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
            Dim oCombo As SAPbouiCOM.ComboBoxColumn
            Dim iColIndex As Integer = oGridN.doIndexFieldWC("U_ItemGrp")
            oCombo = CType(oGridN.getSBOGrid.Columns.Item(iColIndex), SAPbouiCOM.ComboBoxColumn)
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

            doClearValidValues(oCombo.ValidValues)
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                oRecordSet = goParent.goDB.doSelectQuery("select ItmsGrpCod, ItmsGrpNam from OITB")
                While Not oRecordSet.EoF
                	Try
                        oCombo.ValidValues.Add(CType(oRecordSet.Fields.Item(0).Value, String), CType(oRecordSet.Fields.Item(1).Value, String))
                    Catch ex As Exception
                    End Try
                    oRecordSet.MoveNext()
                End While
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the Item Group.")
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", {"Item Group"})
            Finally
                DataHandler.INSTANCE.doReleaseRecordset(oRecordSet)
            End Try
        End Sub

    End Class
End Namespace
