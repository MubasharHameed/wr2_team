<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" Inherits="_707_P1.Orders" Codebehind="Orders.aspx.cs" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
    <asp:Content ID="order" ContentPlaceHolderID="MainContent" runat="server">
        <asp:Panel ID="BPPanel" runat="server" Height="120px" Width="305px" CssClass="box">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <table style="height: 100px;" border="0" cellpadding="0" cellspacing="0" valign="center">
                    <tr><td class="entry">Customer</td><td><asp:TextBox ID="IDH_CUST" runat="server" Enabled="False" CssClass="short text"></asp:TextBox></td></tr>
                    <tr><td class="entry">Name</td><td><asp:TextBox ID="IDH_NAME" runat="server" Enabled="False" CssClass="short text"></asp:TextBox></td></tr>
                    <tr><td class="entry">Obligated</td><td><asp:DropDownList ID="IDH_OBLED" runat="server" CssClass="drop short">
                        </asp:DropDownList></td></tr>
                    <tr><td class="entry">Contact Person</td><td><asp:TextBox ID="IDH_CUSCON" runat="server" CssClass="short text"></asp:TextBox></td></tr>
                    <tr><td class="entry">Po Number</td><td><asp:TextBox ID="IDH_CUSREF" runat="server" CssClass="short text"></asp:TextBox></td></tr>
                    <tr><td class="entry">Waste Lic. Rec. No.</td><td><asp:TextBox ID="IDH_CUSLIC" runat="server" CssClass="short text"></asp:TextBox></td></tr>
                </table>
            </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        
        <asp:Panel ID="SitePanel" runat="server" Width="305px" CssClass="box">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table style="height: 100px;" border="0" cellpadding="0" cellspacing="0" >
                    <tr><td class="entry">Address</td><td>
                            <asp:DropDownList ID="IDH_CUSADD" runat="server" OnSelectedIndexChanged="IDH_CUSADD_SelectedIndexChanged" AutoPostBack="True" CssClass="drop short">
                            </asp:DropDownList>
                        </td></tr>
                    <tr><td class="entry">Street</td><td><asp:TextBox ID="IDH_CUSSTR" runat="server" CssClass="short text"></asp:TextBox></td></tr>
                    <tr><td class="entry">Block</td><td><asp:TextBox ID="IDH_CUSBLO" runat="server" CssClass="short text"></asp:TextBox></td></tr>
                    <tr><td class="entry">Town/City</td><td><asp:TextBox ID="IDH_CUSCIT" runat="server" CssClass="short text"></asp:TextBox></td></tr>
                    <tr><td class="entry">County</td><td><asp:TextBox ID="IDH_COUNTY" runat="server" CssClass="short text"></asp:TextBox></td></tr>
                    <tr><td class="entry">Postal Code</td><td><asp:TextBox ID="IDH_CUSPOS" runat="server" CssClass="short text"></asp:TextBox></td></tr>
                    <tr><td class="entry">Phone No.</td><td><asp:TextBox ID="IDH_CUSPHO" runat="server" CssClass="short text"></asp:TextBox></td></tr>
                    <tr><td class="entry">&nbsp;</td><td></td></tr>
                    <tr><td class="entry">Site Id</td><td><asp:TextBox ID="IDH_SteId" runat="server" CssClass="short text"></asp:TextBox></td></tr>
                    <tr><td class="entry">Site Phone No.</td><td><asp:TextBox ID="IDH_SITETL" runat="server" CssClass="short text"></asp:TextBox></td></tr>
                    <tr><td class="entry">Site Licence</td><td><asp:TextBox ID="IDH_SLCNO" runat="server" CssClass="short text"></asp:TextBox></td></tr>
                    <tr><td class="entry">Premesis Code</td><td><asp:TextBox ID="IDH_PCD" runat="server" CssClass="short text"></asp:TextBox></td></tr>
                </table>
            </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>        

        <asp:Panel ID="OrderPanel" runat="server" Width="305px" CssClass="box">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <table style="height: 100px;" border="0" cellpadding="0" cellspacing="0">
                    <tr><td class="entry">Order No</td><td><asp:TextBox ID="IDH_JOBENT" runat="server" Enabled="False" CssClass="short text"></asp:TextBox></td></tr>
                    <tr><td class="entry">Order Row No</td><td><asp:TextBox ID="IDH_JOBSCH" runat="server" Enabled="False" CssClass="short text"></asp:TextBox></td></tr>
                    <tr><td class="entry">Type</td><td><asp:DropDownList ID="IDH_JOBTYP" runat="server" CssClass="drop short">
                        </asp:DropDownList></td></tr>
                    <tr><td class="entry">&nbsp;</td><td>&nbsp;</td></tr>
                    <tr><td class="entry">Booking Date</td><td><asp:TextBox ID="IDH_BOOKDT" runat="server" Enabled="False" CssClass="short text"></asp:TextBox></td></tr>
                    <tr><td class="entry">Requested Date</td><td><asp:TextBox ID="IDH_REQDAT" runat="server" CssClass="short text"></asp:TextBox>
                        <cc1:CalendarExtender ID="IDH_REQDAT_CalendarExtender" runat="server" 
                            TargetControlID="IDH_REQDAT" Format="yyyy/MM/dd">
                        </cc1:CalendarExtender></td></tr>
                    <tr><td class="entry">Actual StartDate</td><td><asp:TextBox ID="IDH_STARTDT" runat="server" Enabled="False" CssClass="short text"></asp:TextBox>
                        <cc1:CalendarExtender ID="IDH_STARTDT_CalendarExtender" runat="server" 
                            TargetControlID="IDH_STARTDT" Format="yyyy/MM/dd">
                        </cc1:CalendarExtender></td></tr>
                    <tr><td class="entry">Actual End Date</td><td><asp:TextBox ID="IDH_ENDDT" runat="server" Enabled="False" CssClass="short text"></asp:TextBox>
                        <cc1:CalendarExtender ID="IDH_ENDDT_CalendarExtender" runat="server" 
                            TargetControlID="IDH_ENDDT" Format="yyyy/MM/dd">
                        </cc1:CalendarExtender></td></tr>
                    <tr><td class="entry">&nbsp;</td><td>&nbsp;</td></tr>

                    <tr><td class="entry" >Container Code</td>
                        <td rowspan="2">
                            <asp:Panel ID="Panel1a" runat="server">
                            <asp:UpdatePanel ID="UpdatePanel4a" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="IDH_ITMCOD" runat="server" CssClass="drop short" 
                                    AutoPostBack="True" onselectedindexchanged="IDH_ITMCOD_SelectedIndexChanged">
                                </asp:DropDownList><br />
                                <asp:TextBox ID="IDH_DESC" runat="server" CssClass="short text"></asp:TextBox>
                             </ContentTemplate>
                             </asp:UpdatePanel>
                             </asp:Panel>
                        </td>
                    </tr>
                    <tr><td class="entry">Container Description</td>
                    </tr>
                    <tr><td class="entry">&nbsp;</td><td>&nbsp;</td></tr>

                    <tr><td class="entry">Waste Code</td>
                        <td rowspan="2">
                            <asp:Panel ID="Panel1b" runat="server">
                            <asp:UpdatePanel ID="UpdatePanel4b" runat="server">
                            <ContentTemplate>                        
                                <asp:DropDownList ID="IDH_WASCL1" runat="server" CssClass="drop short" 
                                    AutoPostBack="True" onselectedindexchanged="IDH_WASCL1_SelectedIndexChanged">
                                </asp:DropDownList><br />
                                <asp:TextBox ID="IDH_WASMAT" runat="server" CssClass="short text"></asp:TextBox>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr><td class="entry">Waste Material</td></tr>


                    <tr><td class="entry">&nbsp;</td><td>&nbsp;</td></tr>
                    <tr><td class="entry">Quantity</td><td><asp:TextBox ID="IDH_CUSQTY" runat="server" Enabled="False" CssClass="short text"></asp:TextBox></td></tr>
                </table>
            </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel> 

        <asp:Panel ID="ActionPanel" runat="server" Height="30px" Width="305px" CssClass="box">
                <asp:Button ID="Button1" runat="server" Text="Add" OnClick="Button1_Click" />
        </asp:Panel>
</asp:Content>