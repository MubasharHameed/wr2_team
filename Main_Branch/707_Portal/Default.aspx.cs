﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.IO;
using com.idh.dbObjects.User;
using com.idh.bridge;

namespace _707_P1
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Login_Click(object sender, EventArgs e)
        {
            string sUsername = Username.Text;
            string sPassword = Password.Text;

            string sPath2 = Request.PhysicalApplicationPath + "\\App_Data\\Config.xml";

            BP oBP = new BP();
            //if (sPassword.Equals("isb") && oBP.doWebLogIn(sPath2, sUsername, sPassword))
            if (oBP.doWebLogIn(sPath2, sUsername, sPassword))
            {
                Session["BP"] = oBP;
                Response.Output.Write("Welcome" + oBP.CardName);
                Response.Redirect("Welcome.aspx?Cardcode=" + sUsername);
            }
            else
            {
                Session.Clear();
                oResponse.Text = "<strong>ERROR:</strong> Please enter the correct username/password.<br><br>";
                //Response.Output.Write("Incorrect loggin details, please try again or contact customer care.");
            }
        }
    }
}