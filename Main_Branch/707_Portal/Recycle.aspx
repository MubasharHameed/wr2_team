<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Recycle.aspx.cs" Inherits="_707_P1.Recycle" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content100" ContentPlaceHolderID="MainContent" runat="server">
        <asp:Panel ID="Panel100" runat="server" CssClass="box">
            <h3>707 Waste Analysis Report</h3>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="Filter" runat="server" CssClass="contentpanel" Width="100%" style="padding-top: 3px; padding-bottom: 3px;">
                        <asp:Label ID="Label1" runat="server" Text="From Date:" CssClass="entry"></asp:Label>
                        <asp:TextBox ID="oFromDate" runat="server" CssClass="short text"></asp:TextBox>
                        <cc1:CalendarExtender ID="oFromDate_CalendarExtender" runat="server" 
                            TargetControlID="oFromDate">
                        </cc1:CalendarExtender>
                        &nbsp;
                        <asp:Label ID="Label2" runat="server" Text="  To Date:  " CssClass="entry"></asp:Label>&nbsp;
                        <asp:TextBox ID="oToDate" runat="server" CssClass="short text"></asp:TextBox>
                        <cc1:CalendarExtender ID="oToDate_CalendarExtender" runat="server" 
                            TargetControlID="oToDate">
                        </cc1:CalendarExtender>
                        &nbsp;
                        <asp:Button ID="oSearch" runat="server" Text="Search" onclick="oSearch_Click" />
                    </asp:Panel>
                <asp:Table ID="oDataTable" runat="server" Width="100%" CellSpacing="0" BorderWidth="0" CellPadding="0">
                </asp:Table>
                <script type="text/javascript" language="javascript"">
                    function doClick(Supplier, Site, Container, WasteCode, FromDate, ToDate) {
                        location = 'Recycle.aspx?CardCode=<%=Page.Request.QueryString.Get("CardCode")%>&Supplier=' + Supplier + '&Site=' + Site + '&Container=' + Container + '&WasteCode=' + WasteCode + '&oFromDate=' + FromDate + '&oToDate=' + ToDate;
                    }
                </script> 
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
</asp:Content>
