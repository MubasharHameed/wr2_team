using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using com.idh.dbObjects.User;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.idh.utils;

namespace _707_P1
{
    public partial class Balance : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack)
            {
            }
            else
            {
                string sCardCode = Request.QueryString.Get("CardCode");
                if (sCardCode == null)
                    sCardCode = "";

                BP oBP = new BP();

                oBP.doGetBPInfo(sCardCode);
                IDH_Balance.Text = Conversions.doDoubleToMoney(oBP.CurrentAccountBalance);
                IDH_DNotesBal.Text = Conversions.doDoubleToMoney(oBP.DNotesBal);
                IDH_OrdersBal.Text = Conversions.doDoubleToMoney(oBP.OrdersBal);
                IDH_WRBalance.Text = Conversions.doDoubleToMoney(oBP.WRBalance);
                IDH_TBalance.Text = Conversions.doDoubleToMoney(oBP.TBalance);

                IDH_CreditLine.Text = Conversions.doDoubleToMoney(oBP.CreditLine);
                IDH_RemainBal.Text = Conversions.doDoubleToMoney(oBP.RemainBal);
            }
        }
    }
}