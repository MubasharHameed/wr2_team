using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using com.idh.dbObjects.User;
using com.idh.bridge;
using com.idh.bridge.lookups;

namespace _707_P1
{
    public partial class Orders : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack)
            {
            }
            else
            {
                string sCardCode = Request.QueryString.Get("CardCode");
                if (sCardCode == null)
                    sCardCode = "";

                BPAddress oAddress = new BPAddress();

                if (oAddress.doGetAddress(sCardCode, "S", false) > 0)
                {
                    while (oAddress.next())
                        IDH_CUSADD.Items.Add(oAddress.Address);
                }

                BP oBP = new BP();

                oBP.doGetBPInfo(sCardCode);
                IDH_CUST.Text = oBP.CardCode;
                IDH_NAME.Text = oBP.CardName;
                IDH_CUSCON.Text = oBP.Phone1;
                IDH_CUSLIC.Text = oBP.U_WASLIC;

                IDH_BOOKDT.Text = DateTime.Now.ToString("yyyy/MM/dd");

                string sDefaultShipToAddress = Config.INSTANCE.getBPShipToDefault(sCardCode);
                if (sDefaultShipToAddress != null && sDefaultShipToAddress.Length > 0)
                {
                    oAddress.first();
                    while (oAddress.next())
                    {
                        if (oAddress.Address.Equals(sDefaultShipToAddress))
                        {
                            IDH_CUSADD.Text = oAddress.Address;
                            IDH_CUSSTR.Text = oAddress.Street;
                            IDH_CUSBLO.Text = oAddress.Block;
                            IDH_CUSSTR.Text = oAddress.Street;
                            IDH_CUSCIT.Text = oAddress.City;
                            IDH_COUNTY.Text = oAddress.County;
                            IDH_CUSPOS.Text = oAddress.ZipCode;

                            IDH_SteId.Text = oAddress.U_IDHSteId;
                            IDH_SLCNO.Text = oAddress.U_IDHSLCNO;
                            IDH_SITETL.Text = oAddress.U_TEL1;
                            break;
                        }
                    }
                }
                else
                {
                    IDH_CUSADD.Text = Config.EMPTYSTR;
                    IDH_CUSSTR.Text = Config.EMPTYSTR;
                    IDH_CUSBLO.Text = Config.EMPTYSTR;
                    IDH_CUSSTR.Text = Config.EMPTYSTR;
                    IDH_CUSCIT.Text = Config.EMPTYSTR;
                    IDH_COUNTY.Text = Config.EMPTYSTR;
                    IDH_CUSPOS.Text = Config.EMPTYSTR;

                    IDH_SteId.Text = Config.EMPTYSTR;
                    IDH_SLCNO.Text = Config.EMPTYSTR;
                    IDH_SITETL.Text = Config.EMPTYSTR;
                }

                //Data.INSTANCE.doReleaseObject( ref oBP );
                //Data.INSTANCE.doReleaseObject( ref oAddress );
            }

            DataRecords oResult = null;
            string sQuery;
            string sValue;
            //Fill the Waste code dropdown
            //SELECT ItemCode, ItemName FROM OITM WHERE ItmsGrpCod = 104
            try
            {
                sQuery = "SELECT ItemCode, ItemName FROM OITM WHERE ItmsGrpCod = 104";
                oResult = Data.INSTANCE.doBufferedSelectQuery("Orders-WasteCodes", sQuery);
                if (oResult != null && oResult.Count > 0)
                {
                    for (int i = 0; i < oResult.Count; i++)
                    {
                        oResult.gotoRow(i);
                        sValue = oResult.getValueAsString(0);
                        IDH_WASCL1.Items.Add(sValue);
                    }
                }

            //Fill the Containers
            //SELECT ItemCode, ItemName FROM OITM WHERE ItmsGrpCod > = 106
                sQuery = "SELECT ItemCode, ItemName FROM OITM WHERE ItmsGrpCod > = 106";
                oResult = Data.INSTANCE.doBufferedSelectQuery("Orders-Containers", sQuery);
                if (oResult != null && oResult.Count > 0)
                {
                    for (int i = 0; i < oResult.Count; i++)
                    {
                        oResult.gotoRow(i);
                        sValue = oResult.getValueAsString(0);
                        IDH_ITMCOD.Items.Add(sValue);
                    }
                }

            //Fill the Job types
            //SELECT jt.U_JobTp, jt.U_ItemGrp FROM [@IDH_JOBTYPE] jt, OITB ig WHERE jt.U_ItemGrp = ig.ItmsGrpNam AND U_OrdCat = 'Waste Order'
                sQuery = "SELECT jt.U_JobTp, jt.U_ItemGrp FROM [@IDH_JOBTYPE] jt, OITB ig WHERE jt.U_ItemGrp = ig.ItmsGrpNam AND U_OrdCat = 'Waste Order'";
                oResult = Data.INSTANCE.doBufferedSelectQuery("Orders-JobTypes", sQuery);
                if (oResult != null && oResult.Count > 0)
                {
                    for (int i = 0; i < oResult.Count; i++)
                    {
                        oResult.gotoRow(i);
                        sValue = oResult.getValueAsString(0);
                        IDH_JOBTYP.Items.Add(sValue);
                    }
                }

            }
            catch (Exception ex) { }
            finally
            {
                Data.INSTANCE.doReleaseDataRecords(ref oResult);
            }

            IDH_OBLED.Items.Add("");
            IDH_OBLED.Items.Add("Obligated");
            IDH_OBLED.Items.Add("Non-Obligated");

        }
        protected void Button1_Click(object sender, EventArgs e)
        {

        }
        protected void IDH_CUSADD_SelectedIndexChanged(object sender, EventArgs e)
        {
            BPAddress oAddress = new BPAddress();
            string sCardCode = IDH_CUST.Text;
            string sAddress = IDH_CUSADD.Text;

            if (oAddress.doGetAddress(sCardCode, "S", sAddress, false) > 0)
            {
                IDH_CUSADD.Text = oAddress.Address;
                IDH_CUSSTR.Text = oAddress.Street;
                IDH_CUSBLO.Text = oAddress.Block;
                IDH_CUSSTR.Text = oAddress.Street;
                IDH_CUSCIT.Text = oAddress.City;
                IDH_COUNTY.Text = oAddress.County;
                IDH_CUSPOS.Text = oAddress.ZipCode;

                IDH_SteId.Text = oAddress.U_IDHSteId;
            }
            else
            {
                //IDH_CUSADD.Text = Config.EMPTYSTR;
                IDH_CUSSTR.Text = Config.EMPTYSTR;
                IDH_CUSBLO.Text = Config.EMPTYSTR;
                IDH_CUSSTR.Text = Config.EMPTYSTR;
                IDH_CUSCIT.Text = Config.EMPTYSTR;
                IDH_COUNTY.Text = Config.EMPTYSTR;
                IDH_CUSPOS.Text = Config.EMPTYSTR;

                IDH_SteId.Text = Config.EMPTYSTR;
            }
        }

        protected void IDH_ITMCOD_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRecords oResult = null;
            string sQuery;
            try
            {
                sQuery = "SELECT ItemCode, ItemName FROM OITM WHERE ItemCode = '" + IDH_ITMCOD.Text + "'";
                oResult = Data.INSTANCE.doBufferedSelectQuery("Orders-Containers", sQuery);
                if (oResult != null && oResult.Count > 0)
                {
                    IDH_DESC.Text = oResult.getValueAsString(1);
                }
            }
            catch (Exception) { }
            finally
            {
                Data.INSTANCE.doReleaseDataRecords(ref oResult);
            }
        }

        protected void IDH_WASCL1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRecords oResult = null;
            string sQuery;
            try
            {
                sQuery = "SELECT ItemCode, ItemName FROM OITM WHERE ItemCode = '" + IDH_WASCL1.Text + "'";
                oResult = Data.INSTANCE.doBufferedSelectQuery("Orders-Containers", sQuery);
                if (oResult != null && oResult.Count > 0)
                {
                    IDH_WASMAT.Text = oResult.getValueAsString(1);
                }
            }
            catch (Exception) { }
            finally
            {
                Data.INSTANCE.doReleaseDataRecords(ref oResult);
            }
        }
    }
}