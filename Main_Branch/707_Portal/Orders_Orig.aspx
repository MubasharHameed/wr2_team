<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" Inherits="_707_P1.Orders" Codebehind="Orders_Orig.aspx.cs" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
    <asp:Content ID="order" ContentPlaceHolderID="MainContent" runat="server">
        <cc1:DragPanelExtender ID="DragPanelExtender3" runat="server" TargetControlID="BPPanel">
        </cc1:DragPanelExtender>
        <asp:Panel ID="BPPanel" runat="server" Height="120px" Width="305px" CssClass="contentpanel" 
            style="position: absolute; left: 285px; top: 100px;"
            HorizontalAlign="Center">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <table style="height: 100px;" border="0" cellpadding="0" cellspacing="0" valign="center">
                    <tr><td class="LabelCell">Customer</td><td><asp:TextBox ID="IDH_CUST" runat="server" Width="140px" Enabled="False"></asp:TextBox></td></tr>
                    <tr><td class="LabelCell">Name</td><td><asp:TextBox ID="IDH_NAME" runat="server" Width="140px" Enabled="False"></asp:TextBox></td></tr>
                    <tr><td class="LabelCell">Obligated</td><td><asp:DropDownList ID="IDH_OBLED" runat="server" Width="143px">
                        </asp:DropDownList></td></tr>
                    <tr><td class="LabelCell">Contact Person</td><td><asp:TextBox ID="IDH_CUSCON" runat="server" Width="140px"></asp:TextBox></td></tr>
                    <tr><td class="LabelCell">Po Number</td><td><asp:TextBox ID="IDH_CUSREF" runat="server" Width="140px"></asp:TextBox></td></tr>
                    <tr><td class="LabelCell">Waste Lic. Rec. No.</td><td><asp:TextBox ID="IDH_CUSLIC" runat="server" Width="140px"></asp:TextBox></td></tr>
                </table>
            </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        
        <cc1:DragPanelExtender ID="DragPanelExtender1" runat="server" TargetControlID="SitePanel">
        </cc1:DragPanelExtender>
        <asp:Panel ID="SitePanel" runat="server" Width="305px" CssClass="contentpanel" 
            style="position: absolute; left: 285px; top: 315px; height: 231px;"
            HorizontalAlign="Center">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table style="height: 100px;" border="0" cellpadding="0" cellspacing="0" >
                    <tr><td class="LabelCell">Address</td><td>
                            <asp:DropDownList ID="IDH_CUSADD" runat="server" Width="143px" OnSelectedIndexChanged="IDH_CUSADD_SelectedIndexChanged" AutoPostBack="True">
                            </asp:DropDownList>
                        </td></tr>
                    <tr><td class="LabelCell">Street</td><td><asp:TextBox ID="IDH_CUSSTR" runat="server" Width="140px"></asp:TextBox></td></tr>
                    <tr><td class="LabelCell">Block</td><td><asp:TextBox ID="IDH_CUSBLO" runat="server" Width="140px"></asp:TextBox></td></tr>
                    <tr><td class="LabelCell">Town/City</td><td><asp:TextBox ID="IDH_CUSCIT" runat="server" Width="140px"></asp:TextBox></td></tr>
                    <tr><td class="LabelCell">County</td><td><asp:TextBox ID="IDH_COUNTY" runat="server" Width="140px"></asp:TextBox></td></tr>
                    <tr><td class="LabelCell">Postal Code</td><td><asp:TextBox ID="IDH_CUSPOS" runat="server" Width="140px"></asp:TextBox></td></tr>
                    <tr><td class="LabelCell">Phone No.</td><td><asp:TextBox ID="IDH_CUSPHO" runat="server" Width="140px"></asp:TextBox></td></tr>
                    <tr><td class="LabelCell">&nbsp;</td><td></td></tr>
                    <tr><td class="LabelCell">Site Id</td><td><asp:TextBox ID="IDH_SteId" runat="server" Width="140px"></asp:TextBox></td></tr>
                    <tr><td class="LabelCell">Site Phone No.</td><td><asp:TextBox ID="IDH_SITETL" runat="server" Width="140px"></asp:TextBox></td></tr>
                    <tr><td class="LabelCell">Site Licence</td><td><asp:TextBox ID="IDH_SLCNO" runat="server" Width="140px"></asp:TextBox></td></tr>
                    <tr><td class="LabelCell">Premesis Code</td><td><asp:TextBox ID="IDH_PCD" runat="server" Width="140px"></asp:TextBox></td></tr>
                </table>
            </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>        

        <cc1:DragPanelExtender ID="DragPanelExtender2" runat="server" TargetControlID="OrderPanel">
        </cc1:DragPanelExtender>
        <asp:Panel ID="OrderPanel" runat="server" Width="305px" CssClass="contentpanel" 
            style="position: absolute; left: 605px; top: 100px; height: 404px;"
            HorizontalAlign="Center">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <table style="height: 100px;" border="0" cellpadding="0" cellspacing="0">
                    <tr><td class="LabelCell">Order No</td><td><asp:TextBox ID="IDH_JOBENT" runat="server" Width="140px" Enabled="False"></asp:TextBox></td></tr>
                    <tr><td class="LabelCell">Order Row No</td><td><asp:TextBox ID="IDH_JOBSCH" runat="server" Width="140px" Enabled="False"></asp:TextBox></td></tr>
                    <tr><td class="LabelCell">Type</td><td><asp:DropDownList ID="IDH_JOBTYP" runat="server" Width="143px">
                        </asp:DropDownList></td></tr>
                    <tr><td class="LabelCell">&nbsp;</td><td>&nbsp;</td></tr>
                    <tr><td class="LabelCell">Booking Date</td><td><asp:TextBox ID="IDH_BOOKDT" runat="server" Width="140px" Enabled="False"></asp:TextBox></td></tr>
                    <tr><td class="LabelCell">Requested Date</td><td><asp:TextBox ID="IDH_REQDAT" runat="server" Width="140px"></asp:TextBox>
                        <cc1:CalendarExtender ID="IDH_REQDAT_CalendarExtender" runat="server" 
                            TargetControlID="IDH_REQDAT" Format="yyyy/MM/dd">
                        </cc1:CalendarExtender></td></tr>
                    <tr><td class="LabelCell">Actual StartDate</td><td><asp:TextBox ID="IDH_STARTDT" runat="server" Width="140px" Enabled="False"></asp:TextBox>
                        <cc1:CalendarExtender ID="IDH_STARTDT_CalendarExtender" runat="server" 
                            TargetControlID="IDH_STARTDT" Format="yyyy/MM/dd">
                        </cc1:CalendarExtender></td></tr>
                    <tr><td class="LabelCell">Actual End Date</td><td><asp:TextBox ID="IDH_ENDDT" runat="server" Width="140px" Enabled="False"></asp:TextBox>
                        <cc1:CalendarExtender ID="IDH_ENDDT_CalendarExtender" runat="server" 
                            TargetControlID="IDH_ENDDT" Format="yyyy/MM/dd">
                        </cc1:CalendarExtender></td></tr>
                    <tr><td class="LabelCell">&nbsp;</td><td>&nbsp;</td></tr>

                    <tr><td class="LabelCell" >Container Code</td>
                        <td rowspan="2">
                            <asp:Panel ID="Panel1a" runat="server">
                            <asp:UpdatePanel ID="UpdatePanel4a" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="IDH_ITMCOD" runat="server" Width="143px" 
                                    AutoPostBack="True" onselectedindexchanged="IDH_ITMCOD_SelectedIndexChanged">
                                </asp:DropDownList><br />
                                <asp:TextBox ID="IDH_DESC" runat="server" Width="140px"></asp:TextBox>
                             </ContentTemplate>
                             </asp:UpdatePanel>
                             </asp:Panel>
                        </td>
                    </tr>
                    <tr><td class="LabelCell">Container Description</td>
                    </tr>
                    <tr><td class="LabelCell">&nbsp;</td><td>&nbsp;</td></tr>

                    <tr><td class="LabelCell">Waste Code</td>
                        <td rowspan="2">
                            <asp:Panel ID="Panel1b" runat="server">
                            <asp:UpdatePanel ID="UpdatePanel4b" runat="server">
                            <ContentTemplate>                        
                                <asp:DropDownList ID="IDH_WASCL1" runat="server" Width="143px" 
                                    AutoPostBack="True" onselectedindexchanged="IDH_WASCL1_SelectedIndexChanged">
                                </asp:DropDownList><br />
                                <asp:TextBox ID="IDH_WASMAT" runat="server" Width="140px"></asp:TextBox>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr><td class="LabelCell">Waste Material</td></tr>


                    <tr><td class="LabelCell">&nbsp;</td><td>&nbsp;</td></tr>
                    <tr><td class="LabelCell">Quantity</td><td><asp:TextBox ID="IDH_CUSQTY" runat="server" Enabled="False" Width="140px"></asp:TextBox></td></tr>
                </table>
            </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel> 

        <cc1:DragPanelExtender ID="DragPanelExtender4" runat="server" TargetControlID="ActionPanel">
        </cc1:DragPanelExtender>
        <asp:Panel ID="ActionPanel" runat="server" Height="30px" Width="305px" CssClass="contentpanel" 
            style="position: absolute; left: 285px; top: 558px;"
            HorizontalAlign="Center">
                <asp:Button ID="Button1" runat="server" Text="Add" OnClick="Button1_Click" />
        </asp:Panel>
</asp:Content>