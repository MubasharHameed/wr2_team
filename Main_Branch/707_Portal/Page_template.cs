﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using com.idh.dbObjects.User;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.idh.utils;

namespace _707_P1
{
    public class Page_template : System.Web.UI.Page
    {
        protected TableRow moRow;

        protected TableCell doAddHeadCell(object oValue)
        {
            return doAddHeadCell(oValue, 1, "border-b");
        }
        protected TableCell doAddHeadCell(object oValue, int span)
        {
            return doAddHeadCell(oValue, span, "border-b");
        }
        protected TableCell doAddHeadCell(object oValue, int span, string cssclass)
        {
            if (moRow != null)
            {
                TableHeaderCell oCell = new TableHeaderCell();
                oCell.Text = Conversions.ToString(oValue);
                oCell.CssClass = cssclass;
                if ( span > 1 )
                    oCell.ColumnSpan = span;
                moRow.Cells.Add(oCell);
                return oCell;
            }
            return null;
        }

        protected TableCell doAddSpanLine(object oValue, int span, string cssclass)
        {
            if (moRow != null)
            {
                TableCell oCell = new TableCell();
                oCell.Text = Conversions.ToString(oValue);
                oCell.CssClass = cssclass;
                if (span > 1)
                    oCell.ColumnSpan = span;
                moRow.Cells.Add(oCell);
                return oCell;
            }
            return null;
        }

        protected TableCell doAddSpanLineCreateRow(Table oTable, object oValue, int span, string cssclass)
        {
            moRow = new TableRow();
            TableCell oCell = new TableCell();
            oCell.Text = Conversions.ToString(oValue);
            oCell.CssClass = cssclass;
            if (span > 1)
                oCell.ColumnSpan = span;
            moRow.Cells.Add(oCell);
            oTable.Rows.Add(moRow);
            return oCell;
        }

        protected TableCell doAddTextCell(object oValue)
        {
            return doAddTextCell(oValue, 1, null );
        }
        protected TableCell doAddTextCell(object oValue, int span)
        {
            return doAddTextCell(oValue, span, null);
        }
        protected TableCell doAddTextCell(object oValue, int span, string cssclass)
        {
            if (moRow != null)
            {
                TableCell oCell = new TableCell();
                oCell.Text = Conversions.ToString(oValue);
                if ( cssclass != null && cssclass.Length > 0 )
                    oCell.CssClass = cssclass;
                if (span > 1)
                    oCell.ColumnSpan = span;
                moRow.Cells.Add(oCell);
                return oCell;
            }
            return null;
        }

        protected TableCell doAddMoneyCell(object oValue, string cssExtra)
        {
            if (moRow != null)
            {
                TableCell oCell = new TableCell();
                oCell.Text = Conversions.doDoubleToMoney(Conversions.ToDouble(oValue));
                if ( cssExtra==null || cssExtra.Trim().Length == 0 )
                    oCell.CssClass = "money";
                else
                    oCell.CssClass = "money " + cssExtra;
                moRow.Cells.Add(oCell);
                return oCell;
            }
            return null;
        }

        protected Table doCreateGraphTable(int iTableWidth, int iValeColWidth, string sCaption)
        {
            Table oTable = new Table();
            oTable.Attributes["id"] = "barchart";
            oTable.Height = 40;
            oTable.Width = iTableWidth;
            oTable.Caption = sCaption;
            return oTable;
        }

        protected void doAddGraphBlank(Table oTable, string sFirstOrLast)
        {
            TableRow oGRow = new TableRow();
            TableCell oGCell = new TableCell();
            oGCell.CssClass = sFirstOrLast.Equals("first") ? "" : "last";
            oGCell.Text = "";
            oGCell.Height = 2;
            oGRow.Cells.Add(oGCell);

            oGCell = new TableCell();
            oGCell.CssClass = "value " + sFirstOrLast;
            oGCell.Text = "";
            oGCell.Height = 2;
            oGRow.Cells.Add(oGCell);
            oTable.Rows.Add(oGRow);
        }

        protected void doAddGraphBar(Table oTable, int iValeColWidth, string sLabel, double dMaxValue, double dValue)
        {
            TableRow oGRow = new TableRow();
            TableCell oGCell = new TableCell();
            oGCell.Text = sLabel;
            oGRow.Cells.Add(oGCell);

            oGCell = new TableCell();
            oGCell.CssClass = "value";
            oGCell.Width = iValeColWidth;
            oGCell.Height = 16;
            int iImageWidth = (int)(dValue / dMaxValue * iValeColWidth);
            oGCell.Text = "<img src='assets/images/bar.png' alt='' width='" + iImageWidth + "' height='16' />" + dValue.ToString("#,##0.00");
            oGRow.Cells.Add(oGCell);

            oTable.Rows.Add(oGRow);
        }

        protected TableCell doAddSpaceLine(Table oTable, int span)
        {
            return doAddSpaceLine(oTable, span, "space");
        }
        protected TableCell doAddSpaceLine(Table oTable, int span, string sClass)
        {
            TableCell oCell;
            moRow = new TableRow();
            oCell = doAddTextCell("", span, sClass);
            oTable.Rows.Add(moRow);

            return oCell;
        }
    }
}
