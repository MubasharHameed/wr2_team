﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" Inherits="_707_P1.Scheduled" Codebehind="Scheduled.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Scheduled" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="BPPanel" runat="server" Width="100%" CssClass="box">
        <h3>Scheduled Service Report</h3>
        <asp:Table ID="oDataTable" runat="server" CssClass="rtable" Height="31px" Width="100%">
        </asp:Table>
    </asp:Panel>
</asp:Content>

