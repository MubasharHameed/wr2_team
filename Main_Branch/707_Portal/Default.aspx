﻿<%@ Page Language="C#" AutoEventWireup="true" Codebehind="Default.aspx.cs" Inherits="_707_P1.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>707 Resource Management Client Portal | Login</title>
    <link rel="icon" type="image/ico" href="/favicon.ico"/>
    <link rel="stylesheet" href="assets/css/login.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="assets/css/form.css" type="text/css" media="screen" />
</head>
<body>
    <div id="box">
        <h2>707 Resource Management Client Portal</h2>
        <form id="mainform" runat="server">
	        <asp:Label ID="oResponse" runat="server" CssClass="error"></asp:Label>
	        <ul>
		        <li>
			        <label>Username</label>
			        <div>
                        <asp:TextBox ID="Username" runat="server" CssClass="max text"></asp:TextBox>
			        </div>
		        </li>
		        <li>
			        <label>Password</label>
			        <div>
			            <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="max text"></asp:TextBox>
			        </div>
		        </li>
		        <!--
		        <li>
			        <input id="remember" class="checkbox" name="remember" type="checkbox" value="1" /><label class="choice">Remember me on this computer</label>
		        </li>
		        -->
		        <li>
			        <asp:Button ID="Login" runat="server" OnClick="Login_Click" Text="Login" CssClass="button"/>
		        </li>
		        <!--
		        <li>
			        <a href="password.html">Forgot password?</a>
		        </li>
		        -->
	        </ul>
        </form>
    </div>
</body>
</html>