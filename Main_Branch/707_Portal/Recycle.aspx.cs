﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

using com.idh.dbObjects.User;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.idh.utils;

namespace _707_P1
{
    public partial class Recycle : Page_template
    {
        vRR3x moRep;
        string msSupplier = "";
        string msSite = "";
        string msContainer = "";
        string msMainCode = "";

        double mdSubWeight = 0;
        double mdSubRecycled = 0;
        double mdSubLandfill = 0;

        double mdTotalRecycled = 0;
        double mdTotalLandfill = 0;
        double mdTotalWeight = 0;

        string msFromDate;
        string msToDate;

        double mdChldRecycle = 0;
        double mdChldLandfill = 0;
        double mdChldWeight = 0;

        string msLastChildItem = "";

        TableCell moLastChildWeightCell = null;

        ArrayList mdSubDetails;

        struct SubDetails
        {
            public string ParentItemCode;
            public string ItemCode;
            public double Weight;
            public double Recycled;
            public double LandFill;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack)
            {
            }
            else
            {
                msFromDate = Request.QueryString.Get("oFromDate");
                if (msFromDate != null)
                    oFromDate.Text = msFromDate;

                msToDate = Request.QueryString.Get("oToDate");
                if (msToDate != null)
                    oToDate.Text = msToDate;

                doLoadData();
            }
        }

        private void doLoadData()
        {
            string sDateFilter = "";
            DateTime oWrkDate;
            //From Date
            if (msFromDate != null && msFromDate.Length > 0)
            {
                oWrkDate = Conversions.ToDateTime(msFromDate);
                string sFromDate = dates.doSimpleSQLDate(oWrkDate);
                sDateFilter += " AND " + vRR3x._JobDate + " >= '" + sFromDate + "'";
            }
            else
                msFromDate = "";

            //To Date
            if (msToDate != null && msToDate.Length > 0)
            {
                oWrkDate = Conversions.ToDateTime(msToDate);
                string sToDate = dates.doSimpleSQLDate(oWrkDate);
                sDateFilter += " AND " + vRR3x._JobDate + " <= '" + msToDate + "'";
            }
            else
                msToDate = "";

            string sCardCode = Request.QueryString.Get("CardCode");
            if (sCardCode == null)
                sCardCode = "";

            //Supplier=' + Supplier + '
            //Site=' + Site + '
            //Container=' + Container + '
            //WasteCode=' + WasteCode + '
            //oFromDate=' + FromDate + '
            //oToDate=' + ToDate;
            msSupplier = Request.QueryString.Get("Supplier");
            if (msSupplier == null)
                msSupplier = "";

            msSite = Request.QueryString.Get("Site");
            if (msSite == null)
                msSite = "";

            msContainer = Request.QueryString.Get("Container");
            if (msContainer == null)
                msContainer = "";

            msMainCode = Request.QueryString.Get("WasteCode");
            if (msMainCode == null)
                msMainCode = "";

            moRep = new vRR3x();

            string sWhere = vRR3x._CHDICODE + " Is Not Null " +
                            " AND " + vRR3x._CardCode + " = '" + sCardCode + '\'' +
                            " AND " + vRR3x._BinWgt + " > 0 " +
                            " AND " + vRR3x._CHDPERCENT + " > 0 " +
                            sDateFilter;
            string sOrderBy = vRR3x._SupplierCode + ',' +
                              vRR3x._SiteCode + ',' +
                              vRR3x._ParentItemCode + ',' +
                              vRR3x._ChildItemCode + ',' +
                              vRR3x._CHDICODE;

            bool bIsFirstTime = true;

            string sSupplier = "";
            string sSite = "";
            string sContainer = "";
            string sMainCode = "";
            string sChldCode = "";

            TableCell oLastMainWeightCell = null;
            
            if (moRep.getData(sWhere, sOrderBy) > 0)
            {
                while (moRep.next())
                {
                    mdTotalRecycled += moRep.Recycled;
                    mdTotalLandfill += moRep.LandFilled;
                    mdTotalWeight += moRep.FinalWgt;
                    
                    if (!sSupplier.Equals(moRep.SupplierCode))
                    {
                        moRow = new TableHeaderRow();
                        doAddTextCell("Supplier:", 2, "tlabel");
                        doAddTextCell(moRep.SupplierCode, 2);
                        doAddTextCell("", 4);
                        oDataTable.Rows.Add(moRow);

                        sSite = "";
                        sSupplier = moRep.SupplierCode;
                    }

                    if (!sSite.Equals(moRep.SiteCode))
                    {
                        moRow = new TableHeaderRow();
                        doAddTextCell("Site Code:", 2, "tlabel");
                        doAddTextCell(moRep.SiteCode, 2);
                        doAddTextCell("", 4);
                        oDataTable.Rows.Add(moRow);

                        sContainer = "";
                        sSite = moRep.SiteCode;
                    }

                    if (!sContainer.Equals(moRep.ParentItemCode))
                    {
                        moRow = new TableHeaderRow();
                        doAddTextCell("Container:", 2, "tlabel");
                        doAddTextCell(moRep.ParentItemCode, 2);
                        doAddTextCell("", 4);
                        oDataTable.Rows.Add(moRow);

                        sMainCode = "";
                        sContainer = moRep.ParentItemCode;
                    }

                    if (!sMainCode.Equals(moRep.ChildItemCode))
                    {
                        doFinalizeChildData(true);

                        if ( !bIsFirstTime )
                        {
                            doAddSubTotalsLines();
                        }

                        moRow = new TableHeaderRow();
                        doAddTextCell("", 1, "pointer").Width = 20;
                        doAddTextCell(moRep.ChildItemCode, 2, "pointer");
                        doAddTextCell(moRep.ChildItemName, 2, "pointer");
                        doAddTextCell("", 2, "pointer");
                        oLastMainWeightCell = doAddTextCell(0, 1, "pointer");

                        //if (oLastMainWeightCell != null)
                        //    oLastMainWeightCell.BackColor = System.Drawing.Color.Aqua;

                        oDataTable.Rows.Add(moRow);

                        mdSubWeight = 0;
                        mdSubRecycled = 0;
                        mdSubLandfill = 0;

                        sChldCode = "";
                        sMainCode = moRep.ChildItemCode;

                        //Supplier, Site, Container, WasteCode, FromDate, ToDate
                        string seSuplier = Server.UrlEncode(sSupplier);
                        string seSite = Server.UrlEncode(sSite);
                        string seContainer = Server.UrlEncode(sContainer);
                        string seWasteCode = Server.UrlEncode(sMainCode);
                        moRow.Attributes.Add("onClick", "doClick('" + seSuplier + "','" + seSite + "','" + seContainer + "','" + seWasteCode + "','" + msFromDate + "','" + msToDate + "')");
                    }

                    if (!sChldCode.Equals(moRep.U_CHDICODE))
                    {
                        doFinalizeChildData(false);

                        if (msSupplier.Equals(sSupplier) &&
                             msSite.Equals(sSite) &&
                             msContainer.Equals(sContainer) &&
                             msMainCode.Equals(sMainCode))
                        {

                            moRow = new TableHeaderRow();
                            doAddTextCell("", 3).Width = 20;
                            doAddTextCell(moRep.U_CHDICODE, 1);
                            doAddTextCell(moRep.U_CHDINAME, 1);
                            doAddTextCell("", 1);
                            moLastChildWeightCell = doAddTextCell(moRep.Recycled, 1);
                            msLastChildItem = moRep.U_CHDICODE;

                            //if (moLastChildWeightCell != null)
                            //    moLastChildWeightCell.BackColor = System.Drawing.Color.Yellow;

                            doAddTextCell("", 1);
                            oDataTable.Rows.Add(moRow);

                            if (mdSubDetails == null)
                                mdSubDetails = new ArrayList();
                        }

                        sChldCode = moRep.U_CHDICODE;

                        mdChldRecycle = moRep.Recycled;
                        mdChldLandfill = moRep.LandFilled;
                        mdChldWeight = moRep.FinalWgt;
                    }
                    else
                    {
                        mdChldRecycle += moRep.Recycled;
                        mdChldLandfill += moRep.LandFilled;
                        mdChldWeight += moRep.FinalWgt;
                    }
                    
                    mdSubWeight += moRep.FinalWgt;
                    mdSubRecycled += moRep.Recycled;
                    mdSubLandfill += moRep.LandFilled;

                    bIsFirstTime = false;
                }

                if (oLastMainWeightCell != null)
                    oLastMainWeightCell.Text = mdSubWeight.ToString("#,##0.00");

                doFinalizeChildData(true);

                doAddSubTotalsLines();
            }

            doTotalsLine();

            doGraphs();
        }

        private void doFinalizeChildData(bool bCloseGraph)
        {
            if (moLastChildWeightCell != null)
            {
                moLastChildWeightCell.Text = mdChldWeight.ToString("#,##0.00");
                moLastChildWeightCell = null;

                if (mdSubDetails != null)
                {
                    SubDetails oSub = new SubDetails();
                    oSub.ParentItemCode = msMainCode;
                    oSub.ItemCode = msLastChildItem;
                    oSub.Weight = mdChldWeight;
                    oSub.LandFill = mdChldLandfill;
                    oSub.Recycled = mdChldRecycle;
                    mdSubDetails.Add(oSub);
                }
            }
        }

        private void doAddSubTotalsLines()
        {
            moRow = new TableHeaderRow();
            doAddTextCell("", 5);
            doAddTextCell("Recovered", 1, "tlabel");
            doAddTextCell(mdSubRecycled.ToString("#,##0.00"), 1, "border-t");
            doAddTextCell("", 1);
            oDataTable.Rows.Add(moRow);

            moRow = new TableHeaderRow();
            doAddTextCell("", 5);
            doAddTextCell("Landfill", 1, "tlabel").Font.Bold = true;
            doAddTextCell(mdSubLandfill.ToString("#,##0.00"), 1, "border-b");
            doAddTextCell("", 1);
            oDataTable.Rows.Add(moRow);

            doAddSpaceLine(oDataTable, 8).Width = 695;
        }

        private void doTotalsLine()
        {
            doAddSpaceLine(oDataTable, 8).Width = 695;
            doAddSpaceLine(oDataTable, 8, "space-border-bottom").Width = 695;

            moRow = new TableHeaderRow();
            doAddTextCell("", 1);
            doAddTextCell("Total Weight", 1, "tlabelr").Font.Bold = true;
            doAddTextCell(mdTotalWeight.ToString("#,##0.00"), 1);
            doAddTextCell("Total Recovered", 1, "tlabelr");
            doAddTextCell(mdTotalRecycled.ToString("#,##0.00"), 1);
            doAddTextCell("Total Landfill", 1, "tlabelr").Font.Bold = true;
            doAddTextCell(mdTotalLandfill.ToString("#,##0.00"), 1);
            doAddTextCell("", 1);
            oDataTable.Rows.Add(moRow);

            doAddSpaceLine(oDataTable, 8, "space-border-top").Width = 695;
        }

        /*
         * Dot the total Recyle graph
         */
        private void doGraphs()
        {
            bool oDoSubWeight = (mdSubDetails != null && mdSubDetails.Count > 0);

            int iValueColWidth = 200;
            Table oTable = doCreateGraphTable(300, iValueColWidth, "Recycling vs. Landfill");
            doAddGraphBlank(oTable, "first");
            doAddGraphBar(oTable, iValueColWidth, "Recycled", mdTotalWeight, mdTotalRecycled);
            doAddGraphBar(oTable, iValueColWidth, "LandFill", mdTotalWeight, mdTotalLandfill);
            doAddGraphBlank(oTable, "last");

            //Now add the graph to the Report
            moRow = new TableHeaderRow();
            TableCell oCell = doAddTextCell("", oDoSubWeight?4:8 );
            oCell.Controls.Add(oTable);
            moRow.Cells.Add(oCell);
            oDataTable.Rows.Add(moRow);

            if (oDoSubWeight)
            {
                Table oSubRecycleGraphTable = doCreateGraphTable(300, iValueColWidth, "[" + msMainCode + "] - Item Breakdown");
                doAddGraphBlank(oSubRecycleGraphTable, "first");
                foreach (SubDetails oSub in mdSubDetails)
                {
                    doAddGraphBar(oSubRecycleGraphTable, iValueColWidth, oSub.ItemCode, mdTotalWeight, oSub.Weight);
                }
                doAddGraphBlank(oSubRecycleGraphTable, "last");

                //Now add the graph to the Report
                //moRow = new TableHeaderRow();
                oCell = doAddTextCell("", 4);
                oCell.Controls.Add(oSubRecycleGraphTable);
                moRow.Cells.Add(oCell);
                oDataTable.Rows.Add(moRow);
            }

        }

        protected void oSearch_Click(object sender, EventArgs e)
        {
            msFromDate = oFromDate.Text;
            msToDate = oToDate.Text;

            doLoadData();
        }
    }
}
