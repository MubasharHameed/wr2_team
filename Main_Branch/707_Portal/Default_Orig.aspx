﻿<%@ Page Language="C#" AutoEventWireup="true" Codebehind="Default_Orig.aspx.cs" Inherits="_707_P1.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>707 Portal</title>
    <link rel="icon" type="image/ico" href="/favicon.ico"/>
    <link href="./assets/css/screen.css" rel="stylesheet" type="text/css"/>
    <link href="./assets/css/nav.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="./assets/css/isb.css"/>
</head>
<body>
    <form id="mainform" runat="server">
    <div>
        <h1 id="logo"></h1>
        <div>
            <table style="width: 300px">
                <tr>
                    <td colspan="3" style="height: 70px">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 100px">&nbsp;</td>
                    <td style="height: 21px">
                    </td>
                    <td style="width: 5px; height: 21px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">&nbsp;</td>
                    <td>
                        Username</td>
                    <td style="width: 5px">
                        <asp:TextBox ID="Username" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 100px">&nbsp;</td>
                    <td>
                        Password</td>
                    <td style="width: 5px">
                        <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 100px">&nbsp;</td>
                    <td>
                    </td>
                    <td style="width: 5px">
                        <asp:Button ID="Login" runat="server" OnClick="Login_Click" Text="Login" Width="88px" /></td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>