<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" Inherits="_707_P1.Balance" Codebehind="Balance.aspx.cs" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Balance" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="BPPanel" runat="server" Width="305px" CssClass="box">
        <h3>Account Balance</h3>
        <table style="width: 100%;">
            <tr>
                <td class="tlabel" style="width: 173px">CurrentAccountBalance</td>
                <td style="text-align: right;">
                    <asp:Label ID="IDH_Balance" runat="server" Text="00.00"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tlabel" style="width: 173px">Open Orders Balance</td>
                <td style="text-align: right;">
                    <asp:Label ID="IDH_OrdersBal" runat="server" Text="00.00"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tlabel" style="width: 173px">Open Delivery Note Balance</td>
                <td style="text-align: right;">
                    <asp:Label ID="IDH_DNotesBal" runat="server" Text="00.00"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tlabel" style="width: 173px">Open WR1 Order Balance</td>
                <td style="text-align: right;">
                    <asp:Label ID="IDH_WRBalance" runat="server" Text="00.00"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tlabel" style="width: 173px">Total Commited Balance</td>
                <td style="text-align: right;">
                    <asp:Label ID="IDH_TBalance" runat="server" Text="00.00"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="border-b" style="width: 173px">&nbsp;</td>
                <td class="border-b">&nbsp;</td>
            </tr>
            <tr>
                <td class="tlabel" style="width: 173px">CreditLimit</td>
                <td style="text-align: right;">
                    <asp:Label ID="IDH_CreditLine" runat="server" Text="00.00"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tlabel" style="width: 173px">Remaining Credit</td>
                <td style="text-align: right;">
                    <asp:Label ID="IDH_RemainBal" runat="server" Text="00.00"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

