﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

using com.idh.dbObjects.User;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.idh.utils;

namespace _707_P1
{
    public partial class WebForm2 : Page_template
    {
        vRpt707_ServiceCall_2 moRep;
        string msCallType = "";
        string msSite = "";
        string msIssue = "";

        string msFromDate;
        string msToDate;

        int miCallTypeCount;
        int miTotalCalls;

        ArrayList moCallTypes;
        struct CallTypes
        {
            public TableCell Cell;
            public string CallType;
            public int NumberOfCalls;
        }
        //CallTypes moLastCallType;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack)
            {
            }
            else
            {
                msFromDate = Request.QueryString.Get("oFromDate");
                if (msFromDate != null)
                    oFromDate.Text = msFromDate;

                msToDate = Request.QueryString.Get("oToDate");
                if (msToDate != null)
                    oToDate.Text = msToDate;

                doLoadData();
            }
        }

        private void doLoadData()
        {
            string sDateFilter = "";
            DateTime oWrkDate;
            if (msFromDate != null && msFromDate.Length > 0)
            {
                oWrkDate = Conversions.ToDateTime(msFromDate);
                string sFromDate = dates.doSimpleSQLDate(oWrkDate);
                sDateFilter += " AND " + vRpt707_ServiceCall_2._CreatedOn + " >= '" + sFromDate + "'";
            }
            else
                msFromDate = "";

            if (msToDate != null && msToDate.Length > 0)
            {
                oWrkDate = Conversions.ToDateTime(msToDate);
                string sToDate = dates.doSimpleSQLDate(oWrkDate);
                sDateFilter += " AND " + vRpt707_ServiceCall_2._CreatedOn + " <= '" + msToDate + "'";
            }
            else
                msToDate = "";

            string sCardCode = Request.QueryString.Get("CardCode");
            if (sCardCode == null)
                sCardCode = "";

            msCallType = Request.QueryString.Get("CallType");
            if (msCallType == null)
                msCallType = "";

            msSite = Request.QueryString.Get("Site");
            if (msSite == null)
                msSite = "";

            msIssue = Request.QueryString.Get("Issue");
            if (msIssue == null)
                msIssue = "";

            moRep = new vRpt707_ServiceCall_2();

            string sWhere = vRpt707_ServiceCall_2._customer + " = '" + sCardCode + '\'' +
                            sDateFilter;
            string sOrderBy = vRpt707_ServiceCall_2._callTypeID + ',' +
                              vRpt707_ServiceCall_2._Site + ',' +
                              vRpt707_ServiceCall_2._subject;

            if (moRep.getData(sWhere, sOrderBy) > 0)
            {
                moRow = new TableHeaderRow();
                doAddHeadCell("#", 1, "border-b").Width = 30;
                doAddHeadCell("Call Type", 3, "border-b");
                doAddHeadCell("Numbers", 1, "border-b").Width = 30;
                doAddHeadCell("%", 1, "border-b").Width = 30;
                doAddHeadCell("", 1);
                oDataTable.Rows.Add(moRow);

                int iRowEntry = oDataTable.Rows.Count;
                miCallTypeCount = 1;
                miTotalCalls = 0;

                moRow.Attributes.Add("onClick", "doClick('','','','" + msFromDate + "','" + msToDate + "')");

                int iInsertRow = oDataTable.Rows.Count;

                moRow = new TableRow();
                doAddTextCell("", 1, "space").Width = 30;
                doAddTextCell("", 1, "space").Width = 50;
                doAddTextCell("", 1, "space").Width = 120;
                doAddTextCell("", 1, "space").Width = 100;
                doAddTextCell("", 1, "space").Width = 50;
                doAddTextCell("", 1, "space").Width = 30;
                doAddTextCell("", 1, "space").Width = 150;
                oDataTable.Rows.Add(moRow);

                moCallTypes = new ArrayList();
                if (moRep.first())
                {
                    while (doCallType()) ;
                }

                moRow = new TableRow();
                doAddTextCell("", 1).Width = 30;
                doAddTextCell("Total Call Types", 2, "border-b border-t");
                doAddTextCell("", 1).Width = 40;
                doAddTextCell(miCallTypeCount - 1, 1, "border-b border-t");
                doAddTextCell("", 1);
                doAddTextCell("", 1);
                oDataTable.Rows.Add(moRow);

                doGraphs();
            }
        }

        private void doGraphs()
        {
            if (moCallTypes != null && moCallTypes.Count > 0)
            {
                doAddSpanLineCreateRow(oDataTable, "&nbsp;",7,"");
                doAddSpaceLine(oDataTable, 7, "space-border-bottom");

                int iValueColWidth = 200;
                Table oTable = doCreateGraphTable(300, iValueColWidth, "CallType Count");
                doAddGraphBlank(oTable, "first");
                foreach (CallTypes oCall in moCallTypes)
                {
                    doAddGraphBar(oTable, iValueColWidth, oCall.CallType, miTotalCalls, oCall.NumberOfCalls);
                }
                doAddGraphBlank(oTable, "last");

                //Now add the graph to the Report
                moRow = new TableHeaderRow();
                TableCell oCell = doAddTextCell("", 8);
                oCell.Controls.Add(oTable);
                moRow.Cells.Add(oCell);
                oDataTable.Rows.Add(moRow);
            }
        }

        private bool doCallType()
        {
            miCallTypeCount++;

            int iSiteCount = 1;
            int iCurrentRow;
            string sCallType  = moRep.CallType;

            moRow = new TableRow();
            TableRow oLastCallTypeRow = moRow;
            doAddTextCell(miCallTypeCount, 1, "pointer");
            doAddTextCell(sCallType, 3, "pointer");
            doAddTextCell("", 1, "pointer");
            doAddTextCell("0%", 1, "pointer");
            doAddTextCell("", 1, "pointer");
            oDataTable.Rows.Add(moRow);

            iCurrentRow = oDataTable.Rows.Count - 1;

            string seCallType = Server.UrlEncode(sCallType);
            moRow.Attributes.Add("onClick", "doClick('" + seCallType + "','ALL','','" + msFromDate + "','" + msToDate + "')");
            
            bool bResult = true;
            bool bDoAddRows = (msCallType.Length > 0 && msCallType.Equals(sCallType));
            //bool bDoAddRows = false;
            
            while (sCallType.Equals(moRep.CallType))
            {
                bResult = doSite(bDoAddRows, iSiteCount++);

                if (!bResult)
                    break;
            }

            CallTypes oCall = new CallTypes();
            //moLastCallType = oCall;
            

            oCall.CallType = sCallType;
                        
            //if ((iSiteCount-1) > 0)
            if (bDoAddRows)
            {
                moRow = new TableRow();
                doAddTextCell("", 4);
                doAddTextCell("Total Sites", 1, "nowrap");
                doAddTextCell((iSiteCount - 1), 1);
                doAddTextCell("", 1);
                oDataTable.Rows.Add(moRow);
            }
            else
            {
                oLastCallTypeRow.Cells[2].Text = Conversions.ToString((iSiteCount - 1));
            }

            oCall.NumberOfCalls = iSiteCount - 1;
            miTotalCalls += iSiteCount;
            moCallTypes.Add(oCall);

            return bResult;
        }

        /*
         * Do the Site record as long as the CallType and the Site stay the same.
         * return false if the end of data has been reached.
         */
        private bool doSite(bool bDoAddRows, int iSiteCount)
        {
            string sSite = moRep.Site;
            string sCallType = moRep.CallType;

            int iIssueCount = 1;
            int iCurrentRow;

            if (bDoAddRows)
            {
                moRow = new TableRow();
                doAddTextCell("&nbsp;", 1, "border-b border-t border-l pointer");
                doAddTextCell(iSiteCount + " - Site: " + sSite, 4, "border-b border-t pointer").Width = 200;
                doAddTextCell("0%", 1, "border-b border-t border-r pointer ");
                doAddTextCell("", 1); ;

                oDataTable.Rows.Add(moRow);

                string seCallType = Server.UrlEncode(sCallType);
                string seSite = Server.UrlEncode(sSite);
                moRow.Attributes.Add("onClick", "doClick('" + seCallType + "','" + seSite + "','ALL','" + msFromDate + "','" + msToDate + "')");

                doAddSpaceLine(7);

                iCurrentRow = oDataTable.Rows.Count - 1;
            }

            if ( !bDoAddRows || (msSite.Length > 0 && (msSite.Equals("ALL") || msSite.Equals(sSite))) )
            {
                while (sSite.Equals(moRep.Site))
                {
                    if (!doIssue(bDoAddRows, iIssueCount++))
                        return false;
                }
            }
            else
            {
                if (!moRep.next())
                    return false;
            }
            return true;
        }

        private bool doIssue(bool bDoAddRows, int iIssueCount)
        {
            int iActivityCount = 1;

            string sSite = moRep.Site;
            string sCallType = moRep.CallType;
            string sIssue = moRep.subject;

            if ( !bDoAddRows || (msIssue.Length > 0 && (msIssue.Equals("ALL") || msIssue.Equals(sIssue))))
            {
                do
                {
                    if (bDoAddRows)
                    {
                        moRow = new TableRow();
                        doAddTextCell("&nbsp;", 1, "pointer");
                        doAddTextCell("&nbsp;", 1, "pointer").Width = 30;
                        doAddTextCell("<span class=\"tlabel\">Issue</span><br> " + moRep.subject, 3, "border-b border-t border-l pointer").Width = 200;
                        doAddTextCell("<span class=\"tlabel\">Resolution</span><br> " + moRep.resolution, 2, "border-b border-t border-r pointer").Width = 200;
                        oDataTable.Rows.Add(moRow);

                        string seCallType = Server.UrlEncode(sCallType);
                        string seSite = Server.UrlEncode(sSite);
                        string seIssue = Server.UrlEncode(sIssue);
                        moRow.Attributes.Add("onClick", "doClick('" + seCallType + "','" + seSite + "','" + seIssue + "','" + msFromDate + "','" + msToDate + "')");

                        doAddSpaceLine(7);
                        doActivity(iActivityCount);
                    }

                    if (!moRep.next())
                        return false;

                } while (sCallType.Equals(moRep.CallType) && sSite.Equals(moRep.Site) && sIssue.Equals(moRep.subject));
            }
            else
            {
                if (!moRep.next())
                    return false;
            }
            return true;
        }

        private bool doActivity(int iActivityCount)
        {
            moRow = new TableRow();
            doAddTextCell("&nbsp;", 1);
            doAddTextCell("&nbsp;", 1);
            doAddTextCell("&nbsp;", 1).Width = 30;
            doAddTextCell("<span class=\"tlabel\">Activity ID</span><br> " + moRep.ClgCode, 1, "border-b border-t border-l");
            doAddTextCell("<span class=\"tlabel\">Description</span><br> " + moRep.Descriptio, 3, "border-b border-t border-r");

            oDataTable.Rows.Add(moRow);

            doAddSpaceLine(7);

            return true;
        }

        private void doAddSpaceLine(int span)
        {
            moRow = new TableRow();
            doAddTextCell("", span, "space");
            oDataTable.Rows.Add(moRow);
        }

        protected void oSearch_Click(object sender, EventArgs e)
        {
            msFromDate = oFromDate.Text;
            msToDate = oToDate.Text;

            doLoadData();
        }
    }
}
