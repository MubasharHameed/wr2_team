﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using com.idh.dbObjects.User;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.idh.utils;

namespace _707_P1
{
    public partial class Scheduled : Page_template
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack)
            {
            }
            else
            {
                string sCardCode = Request.QueryString.Get("CardCode");
                if (sCardCode == null)
                    sCardCode = "";

                String sJobTrigger = "'PBI','COV'";
                //oTitle.Text = "Scheduled Service Report";

                REP_AllServicesReport3 oRep = new REP_AllServicesReport3();
                string sWhere = REP_AllServicesReport3._Customer_Code + " = '" + sCardCode + '\'' +
                                " AND " + REP_AllServicesReport3._JobTrigger + " IN (" + sJobTrigger + ')';
                string sOrderBy = REP_AllServicesReport3._Customer_Code + ',' +
                                  REP_AllServicesReport3._Site_Address1 + ',' +
                                  "CAST(" + REP_AllServicesReport3._WORow + " As Int)," +
                                  REP_AllServicesReport3._Container_Code + ',' +
                                  REP_AllServicesReport3._Waste_Code;

                if (oRep.getData(sWhere, sOrderBy) > 0)
                {
                    int iCols = 16;
                    //iCols = moRow.Cells.Count;

                    string sSite = null;
                    bool bIsOdd = false;
                    while (oRep.next() )
                    {
                        if (sSite == null || !sSite.Equals(oRep.Site_Address1))
                        {
                            if (sSite != null)
                            {
                                moRow = new TableRow();
                                doAddSpanLine("&nbsp;", iCols, "");
                                oDataTable.Rows.Add(moRow);
                            }

                            moRow = new TableRow();
                            doAddSpanLine(oRep.Site_Id + " - " + oRep.Site_Address1 + " - " + oRep.Site_Address2, iCols, "odd-cap");
                            oDataTable.Rows.Add(moRow);
                            doAddColHeaders();
                        }
                        

                        sSite = oRep.Site_Address1;

                        moRow = new TableRow();
                        if (bIsOdd)
                            moRow.CssClass = "odd";
                        bIsOdd = !bIsOdd;

                        doAddTextCell(oRep.WOHeader + '.' + oRep.WORow, 1, "border-b border-r border-l");
                        doAddTextCell(oRep.Post_Code,1,"border-b border-r");
                        doAddTextCell(oRep.Order_Type, 1, "border-b border-r");
                        doAddTextCell(oRep.Container_Desc, 1, "border-b border-r");

                        doAddTextCell(oRep.Waste_Code, 1, "border-b border-r");
                        doAddTextCell(oRep.Waste_Desc, 1, "border-b border-r");

                        doAddTextCell(oRep.Qty, 1, "border-b border-r");
                        doAddTextCell(oRep.Txt, 1, "border-b border-r");
                        doAddTextCell(oRep.Lifts_Per_Week, 1, "border-b border-r");

                        doAddTextCell(oRep.MON, 1, "border-b border-r");
                        doAddTextCell(oRep.TUE, 1, "border-b border-r");
                        doAddTextCell(oRep.WED, 1, "border-b border-r");
                        doAddTextCell(oRep.THU, 1, "border-b border-r");
                        doAddTextCell(oRep.FRI, 1, "border-b border-r");
                        doAddTextCell(oRep.SAT, 1, "border-b border-r");
                        doAddTextCell(oRep.SUN, 1, "border-b border-r");

                        //doAddTextCell(oRep.Lift_Type);
                        
                        //doAddTextCell(oRep.Cust_Charge_Weight);
                        //doAddTextCell(oRep.Cust_Disposal_UOM);
                        //doAddMoneyCell(oRep.Cust_Disposal_Rate);
                        //doAddMoneyCell(oRep.Cust_Disposal_Total);

                        //doAddTextCell(oRep.Cust_Haulage_Qty);
                        //doAddMoneyCell(oRep.Cust_Haulage_Rate);
                        //doAddMoneyCell(oRep.Cust_Haulage_Total);

                        //doAddMoneyCell(oRep.Cust_VAT_Amount);
                        //doAddMoneyCell(oRep.Cust_Discount_Amt);
                        //doAddMoneyCell(oRep.Cust_Order_Total);

                        oDataTable.Rows.Add(moRow);
                    }
                }
            }
        }

        private void doAddColHeaders()
        {
            moRow = new TableHeaderRow();
            doAddHeadCell("Service Id");
            doAddHeadCell("Postcode");
            doAddHeadCell("Order Type");
            doAddHeadCell("Container Desc");
            doAddHeadCell("Waste Code");
            doAddHeadCell("Waste Desc");

            doAddHeadCell("Qty");
            doAddHeadCell("Frequency");
            doAddHeadCell("Lifts Per Week");

            doAddHeadCell("Mon");
            doAddHeadCell("Tue");
            doAddHeadCell("Wed");
            doAddHeadCell("Thu");
            doAddHeadCell("Fri");
            doAddHeadCell("Sat");
            doAddHeadCell("Sun");

            //doAddHeadCell("Lift Type");

            //doAddHeadCell("Charge Weight");
            //doAddHeadCell("UOM");
            //doAddHeadCell("Disposal Rate");
            //doAddHeadCell("Disposal Total");

            //doAddHeadCell("Haulage Qty");
            //doAddHeadCell("Haulage Rate");
            //doAddHeadCell("Haulage Total");

            //doAddHeadCell("Vat Amount");
            //doAddHeadCell("Discount");
            //doAddHeadCell("Order Total");

            oDataTable.Rows.Add(moRow);
        }
    }
}
