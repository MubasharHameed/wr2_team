﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using com.idh.dbObjects.User;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.idh.utils;

namespace _707_P1
{
    public partial class UnScheduled : Page_template
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.IsPostBack)
            {
            }
            else
            {
                string sFromDate = Request.QueryString.Get("oFromDate");
                string sToDate = Request.QueryString.Get("oToDate");

                if ( sFromDate == null || sFromDate.Length == 0 ) {
                    DateTime oDate = DateTime.Now;
                    sFromDate = dates.doSimpleSQLDate(oDate);
                    oFromDate.Text = oDate.ToString("dd/MM/yyyy");
                }

                if ( sToDate == null || sToDate.Length == 0 ) {
                    DateTime oDate = DateTime.Now;
                    sToDate = dates.doSimpleSQLDate(oDate);
                    oToDate.Text = oDate.ToString("dd/MM/yyyy");
                }

                doLoadData(sFromDate, sToDate);
            }
        }

        private void doLoadData( string sFromDate, string sToDate ) {
            string sCardCode = Request.QueryString.Get("CardCode");
            if (sCardCode == null)
                sCardCode = "";

            string sJobTrigger = "'UNS'";

            string sDateFilter = "";
            DateTime oWrkDate;
            if (sFromDate != null && sFromDate.Length > 0)
            {
                oWrkDate = DateTime.ParseExact(sFromDate, "dd/MM/yyyy", null);
                sFromDate = dates.doSimpleSQLDate(oWrkDate);
                //sDateFilter += " AND CAST(CONVERT(VARCHAR, " + REP_AllServicesReport3._DoneDate + ",101) AS DATETIME) >= CAST(CONVERT(VARCHAR, '" + sFromDate + "',101)";
                sDateFilter += " AND " + REP_AllServicesReport3._DoneDate + " >= '" + sFromDate + "'";
            }
            
            if (sToDate != null && sToDate.Length > 0)
            {
                oWrkDate = Conversions.ToDateTime(sToDate,true);
                sToDate = dates.doSimpleSQLDate(oWrkDate);
                //sDateFilter += " AND CAST(CONVERT(VARCHAR, " + REP_AllServicesReport3._DoneDate + ",101) AS DATETIME) <= CAST(CONVERT(VARCHAR, '" + sToDate + "',101)";
                sDateFilter += " AND " + REP_AllServicesReport3._DoneDate + " <= '" + sToDate + "'";
            }

            REP_AllServicesReport3 oRep = new REP_AllServicesReport3();
            string sWhere = REP_AllServicesReport3._Customer_Code + " = '" + sCardCode + '\'' +
                            " AND " + REP_AllServicesReport3._JobTrigger + " IN (" + sJobTrigger + ')' +
                            sDateFilter;
            string sOrderBy = REP_AllServicesReport3._Customer_Code + ',' +
                              REP_AllServicesReport3._Site_Address1 + ',' +
                              REP_AllServicesReport3._Container_Code + ',' +
                              REP_AllServicesReport3._Waste_Code;

            if (oRep.getData(sWhere, sOrderBy) > 0)
            {
                int iCols = 11;
                //iCols = moRow.Cells.Count;

                string sSite = null;

                bool bIsOdd = false;
                while (oRep.next())
                {
                    if (sSite == null || !sSite.Equals(oRep.Site_Address1))
                    {
                        if (sSite != null)
                        {
                            moRow = new TableRow();
                            doAddSpanLine("&nbsp;", iCols, "");
                            oDataTable.Rows.Add(moRow);
                        }

                        moRow = new TableRow();
                        doAddSpanLine(oRep.Site_Id + " - " + oRep.Site_Address1 + " - " + oRep.Site_Address2, iCols, "odd-cap");
                        oDataTable.Rows.Add(moRow);
                        doAddColHeaders();
                        bIsOdd = false;
                    }


                    sSite = oRep.Site_Address1;

                    moRow = new TableRow();
                    if ( bIsOdd )
                        moRow.CssClass = "odd";
                    bIsOdd = !bIsOdd;

                    doAddTextCell(oRep.WOHeader + '.' + oRep.WORow, 1, "border-b border-r border-l");
                    doAddTextCell(oRep.Post_Code, 1, "border-b border-r");
                    doAddTextCell(oRep.Order_Type, 1, "border-b border-r");
                    doAddTextCell(oRep.Container_Desc, 1, "border-b border-r");
                    doAddTextCell(oRep.Waste_Code, 1, "border-b border-r");
                    doAddTextCell(oRep.Waste_Desc, 1, "border-b border-r");

                    doAddTextCell(oRep.Qty, 1, "border-b border-r");
                    doAddTextCell(oRep.DoneDate, 1, "border-b border-r");
                    //doAddTextCell(oRep.RemovalDate, 1, "border-b border-r");
                    doAddTextCell(oRep.Customer_Reference, 1, "border-b border-r");

                    //doAddTextCell(oRep.Lift_Type, 1, "border-b border-r");
                    //doAddTextCell(oRep.Txt, 1, "border-b border-r");

                    //doAddTextCell(oRep.Cust_Charge_Weight, 1, "border-b border-r");
                    //doAddMoneyCell(oRep.Cust_Disposal_Rate, 1, "border-b border-r");
                    doAddMoneyCell(oRep.Cust_Disposal_Total, "border-b border-r");
                    doAddTextCell(oRep.Cust_Disposal_UOM, 1, "border-b border-r");

                    //doAddTextCell(oRep.Cust_Haulage_Qty, 1, "border-b border-r");
                    //doAddMoneyCell(oRep.Cust_Haulage_Rate, 1, "border-b border-r");
                    //doAddMoneyCell(oRep.Cust_Haulage_Total, 1, "border-b border-r");

                    //doAddMoneyCell(oRep.Cust_VAT_Amount, 1, "border-b border-r");
                    //doAddMoneyCell(oRep.Cust_Discount_Amt, 1, "border-b border-r");
                    //doAddMoneyCell(oRep.Cust_Order_Total, 1, "border-b border-r");

                    oDataTable.Rows.Add(moRow);
                }
            }
        }

        private void doAddColHeaders()
        {
            moRow = new TableHeaderRow();
            doAddHeadCell("Service Id");

            doAddHeadCell("Postcode");
            doAddHeadCell("Order Type");
            doAddHeadCell("Container Desc");
            doAddHeadCell("Waste Code");
            doAddHeadCell("Waste Desc");

            doAddHeadCell("Qty");
            doAddHeadCell("Service Date");
            //doAddHeadCell("Removal Date");
            doAddHeadCell("Customer Reference");

            //doAddHeadCell("Lift Type");
            //doAddHeadCell("Lift Per Week");

            //doAddHeadCell("Charge Weight");
            //doAddHeadCell("Disposal Rate");
            doAddHeadCell("Disposal Total");
            doAddHeadCell("UOM");

            //doAddHeadCell("Haulage Qty");
            //doAddHeadCell("Haulage Rate");
            //doAddHeadCell("Haulage Total");

            //doAddHeadCell("Vat Amount");
            //doAddHeadCell("Discount");
            //doAddHeadCell("Order Total");

            oDataTable.Rows.Add(moRow);
        }

        protected void oSearch_Click(object sender, EventArgs e)
        {
            string sFromDate = oFromDate.Text;
            string sToDate = oToDate.Text;

            doLoadData(sFromDate,sToDate);
        }
    }
}
