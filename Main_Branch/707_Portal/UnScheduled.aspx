﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" Inherits="_707_P1.UnScheduled" Codebehind="UnScheduled.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="UnScheduled" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="BPPanel" runat="server" Width="100%" CssClass="box" >
        <h3>Un-Scheduled Service Report</h3>
        <asp:Panel ID="Filter" runat="server" CssClass="contentpanel" Width="100%" style="padding-top: 3px; padding-bottom: 3px;">
            <asp:Label ID="Label1" runat="server" Text="From Date:" CssClass="entry"></asp:Label>
            <asp:TextBox ID="oFromDate" runat="server" CssClass="medium text"></asp:TextBox>
            <cc1:CalendarExtender ID="oFromDate_CalendarExtender" runat="server" 
                TargetControlID="oFromDate" CssClass="table_cal" Format="dd/MM/yyyy">
            </cc1:CalendarExtender>
            &nbsp;
            <asp:Label ID="Label2" runat="server" Text="  To Date:  " CssClass="entry"></asp:Label>&nbsp;
            <asp:TextBox ID="oToDate" runat="server" CssClass="short text"></asp:TextBox>
            <cc1:CalendarExtender ID="oToDate_CalendarExtender" runat="server" 
                TargetControlID="oToDate" Format="dd/MM/yyyy">
            </cc1:CalendarExtender>
            &nbsp;
            <asp:Button ID="oSearch" runat="server" Text="Search" onclick="oSearch_Click" />
        </asp:Panel>
        <asp:Table ID="oDataTable" runat="server" Height="31px" Width="100%">
        </asp:Table>
    </asp:Panel>
</asp:Content>

