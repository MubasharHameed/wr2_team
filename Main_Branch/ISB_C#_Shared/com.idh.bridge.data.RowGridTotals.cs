﻿/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2010/05/28
 * Time: 10:02 AM
 */
using System;

namespace com.idh.bridge.data {
	/// <summary>
	/// Object holding the Row Grid totals.
	/// </summary>
    public class RowGridTotals {
    	private double mdCstWgt = 0;
        private double mdAftDisc = 0;
        private double mdTotal = 0;
        private double mdDisAmt = 0;
        private double mdAddEx = 0;
        private double mdTxAmt = 0;
        private double mdRdWgt = 0;

        private double mdChargeDed = 0;


		private double mdTCTotal = 0;
		private double mdPrice = 0;
        private double mdChargeSubAftDisc = 0;
        private double mdCostTotal = 0;

        private double mdDisposalCostTotal = 0;
        private double mdHaulageCostTotal = 0;
        private double mdProducerCostTotal = 0;
        
        private double mdAdditionalCostTotal = 0;
        private double mdAdditionalChargeTotal = 0;
        private double mdTxAmtCostTotal = 0;
        private double mdTotalJCost = 0;

		public double CstWgt {
			get { return mdCstWgt; }
			set { mdCstWgt = value; }
		}

        public double RdWgt {
            get { return mdRdWgt; }
            set { mdRdWgt = value;  }
        }
		
		public double AftDisc {
			get { return mdAftDisc; }
			set { mdAftDisc = value; }
		}
		
		public double Total {
			get { return mdTotal; }
			set { mdTotal = value; }
		}
		
		public double AddEx {
			get { return mdAddEx; }
			set { mdAddEx = value; }
		}
		
		public double TxAmt {
			get { return mdTxAmt; }
			set { mdTxAmt = value; }
		}
        public double ChargeDed {
            get { return mdChargeDed; }
            set { mdChargeDed = value; }
		}
        

		public double DisAmt {
			get { return mdDisAmt; }
			set { 
				mdDisAmt = value; 
				doCalcSubAfterDiscount();
			}
		}
        
		public double TCTotal {
			get { return mdTCTotal; }
			set { 
				mdTCTotal = value;
				doCalcSubAfterDiscount();
			}
		}
		
        public double Price {
			get { return mdPrice; }
			set { 
				mdPrice = value;
				doCalcSubAfterDiscount();
			}
		}

        public double DisposalCostTotal {
            get { return mdDisposalCostTotal; }
        }

        public double HaulageCostTotal {
            get { return mdHaulageCostTotal; }
        }

        public double ProducerCostTotal {
            get { return mdProducerCostTotal; }
        }
        
        public double ChargeSubAftDisc {
        	get { return mdChargeSubAftDisc; }
        }
        
        public RowGridTotals(){}
		
        private void doCalcSubAfterDiscount() {
			mdChargeSubAftDisc = mdTCTotal + mdPrice - mdDisAmt;
		}

        private void doCalcCostTotal() {
            mdCostTotal = mdDisposalCostTotal + mdHaulageCostTotal + mdProducerCostTotal;
        }


        public double CostTotal {
            get { return mdCostTotal; }
        }

        public double Profit {
            get { return mdChargeSubAftDisc - mdCostTotal; }
        }

        
        public double AdditionalCostTotal {
            get { return mdAdditionalCostTotal; }
        }

        public double AdditionalChargeTotal {
            get { return mdAdditionalChargeTotal; }
        }

        public double TxAmtCostTotal {
            get { return mdTxAmtCostTotal; }
        }
        public double JCostTotal {
            get { return mdTotalJCost; }
        }


        public void CstWgtAdd( double dValue ) {
			mdCstWgt += dValue;
		}

        public void RdWgtAdd(double dValue) {
            mdRdWgt += dValue;
        }

        public void ChargeDedAdd(double dValue) {
            mdChargeDed+= dValue;
        }


        public void AftDiscAdd( double dValue ) {
			mdAftDisc += dValue;
		}
		
    	public void TotalAdd( double dValue ) {
			mdTotal += dValue;
		}
		
    	public void AddExAdd( double dValue ) {
			mdAddEx += dValue;
		}
		
    	public void TxAmtAdd( double dValue ) {
			mdTxAmt += dValue;
		}
		
    	public void DisAmtAdd( double dValue )  {
			mdDisAmt += dValue;
			doCalcSubAfterDiscount();
		}        
		
		public void TCTotalAdd( double dValue ) {
			mdTCTotal += dValue;
			doCalcSubAfterDiscount();
		}
		
		public void PriceAdd( double dValue ) {
			mdPrice += dValue;
			doCalcSubAfterDiscount();
		}

        public void DisposalCostAdd(double dValue) {
            mdDisposalCostTotal += dValue;
            doCalcCostTotal();
        }

        public void HaulageCostAdd(double dValue) {
            mdHaulageCostTotal += dValue;
            doCalcCostTotal();
        }

        public void ProducerCostAdd(double dValue) {
            mdProducerCostTotal += dValue;
            doCalcCostTotal();
        }


        public void AdditionalCostAdd(double dValue) {
            mdAdditionalCostTotal += dValue;
        }
        public void AdditionalChargeAdd(double dValue) {
            mdAdditionalChargeTotal += dValue;
        }
        public void TxAmtCostAdd(double dValue) {
            mdTxAmtCostTotal += dValue;
        }

        public void TotalJCostAdd(double dValue) {
            mdTotalJCost += dValue;
        }

	}
}
