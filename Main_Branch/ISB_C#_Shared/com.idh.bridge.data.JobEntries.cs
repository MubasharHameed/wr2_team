﻿/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2010/06/01
 * Time: 02:17 PM
 */
using System;
using System.Collections;
using com.idh.bridge;
using com.idh.dbObjects.User;
using com.idh.utils;
	
namespace com.idh.bridge.data {
	/// <summary>
	/// The Collection of job entries
	/// </summary>
	    
    public class JobEntries {
		string msLastJobType = "";
		string msLastContainerCode = "";
		string msLastContainerName = "";		
		string msLastContainerGrp = "";
		string msLastJobNumber = "";
        double currentQty = 0;
		DateTime moLastAEDate;
		
		public string LastJobType {
			get { return msLastJobType; }
			set { msLastJobType = value; }
		}
		
		public string LastContainerCode {
			get { return msLastContainerCode; }
			set { msLastContainerCode = value; }
		}
		
		public string LastContainerName {
			get { return msLastContainerName; }
			set { msLastContainerName = value; }
		}
		
		public string LastContainerGrp {
			get { return msLastContainerGrp; }
			set { msLastContainerGrp = value; }
		}
		
		public string MsLastJobNumber {
			get { return msLastJobNumber; }
			set { msLastJobNumber = value; }
		}
			
		private Hashtable moJobs = new Hashtable();

		public class JobsPerOrder {
			public double dQty;
			public ArrayList oList;
		}
		
		/**
		 * Find the 
		 */
		public JobsPerOrder findTheJobQty( string sJobNumber ) {
			JobsPerOrder oJobsPerOrder = null;
			if ( moJobs.ContainsKey( sJobNumber ) ) {
				oJobsPerOrder = (JobsPerOrder)moJobs[ sJobNumber ];
			}
			return oJobsPerOrder;
		}
		
		/**
		 * Get the AEDate - Actual End Date of the row
		 */
		public DateTime findTheJobAEDate( string sJobNumber, string sRowNumber ) {
			JobEntry oJob = findTheRow( sJobNumber, sRowNumber );
			if ( oJob != null )
				return oJob.AEDate;
			else
				return DateTime.MinValue;
		}
		
		/**
		 * Find the JobEntry
		 */
		public JobEntry findTheRow( string sJobNumber, string sRowNumber ) {
			ArrayList oRows = null;
			if ( moJobs.ContainsKey( sJobNumber ) ) {
				oRows = ((JobsPerOrder)moJobs[ sJobNumber ]).oList;
				foreach ( JobEntry oJob in oRows ) {
					if ( sRowNumber.Equals( oJob.RowNumber ) )
					    return oJob;
				}
			}			
			return null;
		}
		
		/**
		 * Find the Last JobEntry
		 */
		public JobEntry findTheLastRow( string sJobNumber ) {
			JobEntry oJob = null;
			ArrayList oRows = null;
			if ( moJobs.ContainsKey( sJobNumber ) ) {
				oRows = ((JobsPerOrder)moJobs[ sJobNumber ]).oList;
				oJob = (JobEntry)oRows[oRows.Count-1];
			}			
			return oJob;
		}		
		
		/**
		 * Do check if WO should be closed - using the last JobNumber
		 * This works on a list that was ordered at population time to find the first and last job
		 */
		public bool checkIfClosed(){
			return checkIfClosed( msLastJobNumber );
		}
		public bool checkIfClosed( string sJobNumber ){
			if ( getOnSiteQty(sJobNumber ) <= 0 ) {
				ArrayList oRows = null;
				if ( moJobs.ContainsKey( sJobNumber ) ) {
					oRows = ((JobsPerOrder)moJobs[ sJobNumber ]).oList;
					if ( oRows.Count > 1 ) {
						JobEntry oJob = (JobEntry)oRows[oRows.Count-1];
						if ( oJob.Direction ==  lookups.Base.DIRECT_IN ) {
							DateTime oAEDate = oJob.AEDate;
							if ( Dates.isValidDate(oAEDate) )
								return true;
						}
					}
				}
			}
			return false;
		}
		
		/**
		 * Add a new Job entry to the list
		 */
		public void doAddEntry( string sJobNumber, string sRowNumber, DateTime oReqDate, DateTime oAEDate, string sOrderType, 
		                       string sItemGroup, string sJobType, string sContainerCode, 
		                       string sContainerName, double dQty ){
			
			int iIndex = IDH_JOBTYPE_SEQ.getIndex( sOrderType, sItemGroup, sJobType );
			int iSeq;
	        char cDirect;
	        string sDirect;
			JobEntry oJob;
	        if ( iIndex >= 0 ) {
        		iSeq = IDH_JOBTYPE_SEQ.getInstance().U_Seq;
        		
        		sDirect = IDH_JOBTYPE_SEQ.getInstance().U_Direct;
        		if ( sDirect.Length > 0 ) 
        			cDirect = sDirect[0];
        		else
        			cDirect =  lookups.Base.DIRECT_NONE;
        		
            	oJob = new JobEntry( sRowNumber, oReqDate, oAEDate, cDirect, dQty, iSeq );

				ArrayList oList;
				JobsPerOrder oJobsPerOrder;
				if ( moJobs.ContainsKey( sJobNumber ) ) {
					oJobsPerOrder = (JobsPerOrder)moJobs[ sJobNumber ];
                    oList = oJobsPerOrder.oList;

					
                    if (oAEDate.Date.Year.Equals(1)) {
                        currentQty = currentQty + 0;
                    } else {
                        currentQty = currentQty + doActionQty(oJob);
                    }
                    oJobsPerOrder.dQty = currentQty;
					
					JobEntry oJobEntry;
					bool bInserted = false;
					for ( int iIdx = 0; iIdx < oList.Count; iIdx++ ){
						oJobEntry = (JobEntry)oList[iIdx];
						if ( oJob.Requested.CompareTo( oJobEntry.Requested ) < 0 ) {
							oList.Insert(iIdx, oJob );
							bInserted = true;
							break;
                        } else if (oJob.Requested.CompareTo(oJobEntry.Requested) == 0) {
							long iCurrRow = com.idh.utils.Conversions.ToLong( oJob.RowNumber );
							long iPrevRow = com.idh.utils.Conversions.ToLong( oJobEntry.RowNumber );
							if ( iPrevRow > iCurrRow ) {
								oList.Insert(iIdx, oJob );
								bInserted = true;
								break;
							}
						}
					}
					if ( !bInserted ) {
						msLastJobType = sJobType;
						msLastContainerCode = sContainerCode;
						msLastContainerName = sContainerName;		
						msLastContainerGrp = sItemGroup;
						msLastJobNumber = sJobNumber;
						moLastAEDate = oAEDate;

						oList.Add(oJob);
					}
                } else {
					oList = new ArrayList();
					oList.Add( oJob );
					
					oJobsPerOrder = new JobsPerOrder();
					oJobsPerOrder.oList = oList;
                    //oAEDate.Date.Year
                    if (oAEDate.Date.Year.Equals(1)) {
                        currentQty = currentQty + 0;
                    } else {
                        currentQty = currentQty + doActionQty(oJob);
                    }
                    oJobsPerOrder.dQty = currentQty;

                    //oJobsPerOrder.dQty = doActionQty( oJob );
	
					moJobs.Add( sJobNumber, oJobsPerOrder );
					
					msLastJobType = sJobType;
					msLastContainerCode = sContainerCode;
					msLastContainerName = sContainerName;		
					msLastContainerGrp = sItemGroup;
					msLastJobNumber = sJobNumber;
					moLastAEDate = oAEDate;
				}
			}
		}
		
		/**
		 * Do Calculate the Action Quantity
		 */
		private double doActionQty( JobEntry oJob ) {
			if ( oJob.Direction == lookups.Base.DIRECT_IN )
				return oJob.Qty * -1;
			else if ( oJob.Direction == lookups.Base.DIRECT_OUT )
				return oJob.Qty;
			else
				return 0;
		}
		
		/**
		 * Returns the sum of the onsite Qty
		 */
		public double getOnSiteQty(){
			return getOnSiteQty( msLastJobNumber );
		}
		public double getOnSiteQty( string sJobNumber ){
			JobsPerOrder oJobQty;
			double dQty = 0;
			if ( moJobs.ContainsKey( sJobNumber ) ) {			
				oJobQty = (JobsPerOrder)moJobs[ sJobNumber ];
				dQty  	= oJobQty.dQty;			
			}
			return dQty;
		}
	}
}