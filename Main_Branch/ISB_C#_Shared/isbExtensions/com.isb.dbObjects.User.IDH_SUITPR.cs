﻿
using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

using com.idh.bridge.lookups;
using com.idh.bridge;
using com.idh.utils;
using com.idh.bridge.resources;
using com.idh.dbObjects.strct;
using com.idh.dbObjects.numbers;
using com.idh.dbObjects.User;

namespace com.isb.dbObjects.User {

    public class IDH_SUITPR : com.idh.dbObjects.User.IDH_SUITPR {
        public IDH_SUITPR()
            : base() {
        }

        public IDH_SUITPR(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oIDHForm, oForm) {
        }

        public static bool doCalculateNUpdateSIPPrices(string sSupplierCd, string sSupplierNm, string sSupplierAddress, string sSupplierPostcode,
            string sCardCd, string sCardNm, string sCardAddress, string sCardPostcode, string s_SMnSVst, string s_SMnChg, string s_SVarRFix,
            string s_JbTypeDs,
            double d_SMinChg, double d_SMCgVol, double d_SRminVol, double d_SHaulage, DateTime dt_EffDate,
            string s_WasCd, string s_WasNm, string s_ItemCd, string s_ItemNm, string s_UOM, string sItemGrpCode, string sJobSpecficPrice, string s_Rebate, bool doOnlyForRebate) {
            try {
                if (string.IsNullOrWhiteSpace(s_SMnSVst) && string.IsNullOrWhiteSpace(s_SMnChg) && string.IsNullOrWhiteSpace(s_SVarRFix)) {
                    return true;
                } else
               if (d_SMinChg == 0 && d_SMCgVol == 0 && d_SRminVol == 0 && d_SHaulage == 0)
                    return true;
                if (string.IsNullOrWhiteSpace(s_SMnSVst))
                    s_SMnSVst = "";
                if (string.IsNullOrWhiteSpace(s_SMnChg))
                    s_SMnChg = "";
                if (string.IsNullOrWhiteSpace(s_SVarRFix))
                    s_SVarRFix = "";
                double SIPU_FTon, SIPU_TTon, SIPU_Haulage, SIPU_TipTon, SIPBaseWeightFrom, SIPBaseWeightTo;
                string sChrgCal = "";
                string sSupAddressCd = "";
                object oAddressLine = Config.INSTANCE.doLookupTableField("CRD1", "U_AddrsCd", "CardCode='" + sSupplierCd.Replace("'", "''") + "' And Address='" + sSupplierAddress.Replace("'", "''") + "' And AdresType='S'", "", true);
                if (oAddressLine != null && oAddressLine.ToString() != string.Empty) {
                    sSupAddressCd = oAddressLine.ToString();
                }

                string sCustAddressCd = "";
                oAddressLine = Config.INSTANCE.doLookupTableField("CRD1", "U_AddrsCd", "CardCode='" + sCardCd.Replace("'", "''") + "' And Address='" + sCustAddressCd.Replace("'", "''") + "' And AdresType='S'", "", true);
                if (oAddressLine != null && oAddressLine.ToString() != string.Empty) {
                    sCustAddressCd = oAddressLine.ToString();
                }

                //string sItemGrpCode = Config.INSTANCE.getValueFromOITM(s_ItemCd, "ItmsGrpCod").ToString();
                //string sJobSpecficPrice = Config.INSTANCE.getValueFromTablebyKey("[@IDH_JOBTYPE]", "U_SuppLc", "U_JobTp", s_JbTypeDs, " And U_ItemGrp=\'" + sItemGrpCode + "\' And (isnull(U_OrdCat,'')='Waste Order' or isnull(U_OrdCat,'')='') ", "").ToString();

                //string sCustomerLinkedBPCode = "", sCustomerLinkedBPName = "";
                //LinkedBP oLinkedBP= Config.INSTANCE.doGetLinkedBP(U_CustCd);
                //sCustomerLinkedBPCode = oLinkedBP.LinkedBPCardCode;
                //sCustomerLinkedBPName= oLinkedBP.LinkedBPName;

                Prices oPrices;

                string m_sSupCd = sSupplierCd;
                string m_sSupNm = sSupplierNm;
                string m_sSupAdr = sSupplierAddress;
                string m_sSupZipCd = sSupplierPostcode;

                string m_sCustCd = sCardCd;
                string m_sCustNm = sCardNm;
                string m_sCusstAdr = sCardAddress;
                string m_sCusstZipCd = sCardPostcode;

                string m_sUOM = s_UOM;
                string m_sItemCd = s_ItemCd;
                string m_sItemNm = s_ItemNm;
                string m_sWasteCd = s_WasCd;
                string m_sWasteNM = s_WasNm;
                string m_sSupAddressCd = sSupAddressCd;
                string m_sCustAddressCd = sCustAddressCd;
                string m_JbTypeDs = s_JbTypeDs;

                SIPU_FTon = 0;
                //SIP Pricing Min Site Visit
                if (s_SMnSVst.Trim().ToUpper() == "YES" || s_SMnSVst.Trim().ToUpper() == "Y") {//Minimum Site visit charge
                    SIPU_FTon = 0;
                    SIPU_TTon = 0;
                    SIPU_Haulage = 0;
                    SIPU_TipTon = d_SMinChg;
                    SIPBaseWeightFrom = 0;
                    SIPBaseWeightTo = 0;
                    bool bdoAddNewPrice = true;
                    sChrgCal = "QTY1";
                    IDH_SUITPR oSIP = null;// new IDH_SUITPR();
                    oPrices = doValidateSIPPriceExists(ref oSIP, SIPU_FTon, SIPU_TTon, SIPU_Haulage, SIPU_TipTon, sChrgCal, sSupplierCd, sSupplierNm,
                        sSupplierAddress, sSupplierPostcode, sCardCd, sCardAddress, sCardPostcode, s_JbTypeDs, dt_EffDate, s_WasCd, s_ItemCd, s_UOM,
                        sItemGrpCode, sJobSpecficPrice);
                    if (oPrices != null && oPrices.Found && oPrices.PriceType != Price.PRICETYPE_NONE) {

                        if (sJobSpecficPrice != "Y" || (sJobSpecficPrice == "Y" && oSIP.U_JobTp == m_JbTypeDs)) {//Non Job Specific
                            if (Convert.ToDecimal(SIPU_FTon) != Convert.ToDecimal(oSIP.U_FTon) || Convert.ToDecimal(SIPU_TTon) != Convert.ToDecimal(oSIP.U_TTon)
                            || Convert.ToDecimal(SIPU_Haulage) != Convert.ToDecimal(oPrices.HaulPrice)
                            || Convert.ToDecimal(SIPU_TipTon) != Convert.ToDecimal(oPrices.TipPrice)) {//As Price is different so de-activate current and add new
                                //m_sSupCd= oSIP.U_CardCd;
                                m_JbTypeDs = s_JbTypeDs;
                                //m_sAddressCd = "";
                                if (oSIP.U_StAddr == "") {//found price is national so add site specific price
                                    bdoAddNewPrice = true;
                                    if (oSIP.U_BP2CD == "") {
                                        m_sCusstAdr = "";
                                        m_sCusstZipCd = "";
                                        m_sCustAddressCd = "";
                                    }
                                    //if(oCIP.U_ItemCd!=m_sItemCd || oCIP.U_WasteCd != m_sWasteCd || oCIP.U_JobTp!=m_JbTypeDs) {
                                    //    m_sCusstAdr = "";
                                    //    m_sCusstZipCd = "";
                                    //    m_sAddressCd = "";
                                    //}
                                } else if (oSIP.U_StAddr != "") {//found price is site specific so add site specific price
                                    oSIP.doBookmark();
                                    bool bHaveNationalPrice = false;
                                    while (oSIP.next()) {
                                        if (oSIP.U_StAddr == "") {
                                            bHaveNationalPrice = true;
                                            break;
                                        }
                                    }
                                    oSIP.doRecallBookmark();
                                    if (bHaveNationalPrice) {
                                        oSIP.U_EnDate = dt_EffDate.AddDays(-1);
                                        bdoAddNewPrice = oSIP.doUpdateDataRow();
                                    } else {
                                        if (oSIP.U_StAddr == m_sCusstAdr) {
                                            oSIP.U_EnDate = dt_EffDate.AddDays(-1);
                                            bdoAddNewPrice = oSIP.doUpdateDataRow();
                                        }
                                        m_sCusstAdr = "";
                                        m_sCusstZipCd = "";
                                        m_sCustAddressCd = "";
                                        // oCIP.U_EnDate = dt_EffDate.AddDays(-1);
                                        //  bdoAddNewPrice = oCIP.doUpdateDataRow();
                                    }
                                }

                                //m_sSupplierCd = oCIP.U_BP2CD;
                                //m_sUOM = oCIP.U_UOM;
                                //m_sItemCd = oCIP.U_ItemCd;
                                //m_sItemNm = oCIP.U_ItemDs;
                                //m_sWasteCd = oCIP.U_WasteCd;
                                //m_sWasteNM = oCIP.U_WasteDs;
                                if (sJobSpecficPrice != "Y")
                                    m_JbTypeDs = "";
                                //Add price

                            } else
                                bdoAddNewPrice = false;
                        } else {//job specific and job not matched                            
                                //As job type is different so add new 
                                // m_sSupCd = oSIP.U_CardCd;
                            bdoAddNewPrice = true;
                            //m_sAddressCd = "";
                            //if (oCIP.U_StAddr == "") {//found price is national so add site specific price
                            //     bdoAddNewPrice = true;
                            //} else 

                            //if (oCIP.U_StAddr != "") {//found price is site specific so add site specific price
                            //    oCIP.doBookmark();
                            //    bool bHaveNationalPrice = false;
                            //    while (oCIP.next()) {
                            //        if (oCIP.U_StAddr == "") {
                            //            bHaveNationalPrice = true;
                            //            break;
                            //        }
                            //    }
                            //    oCIP.doRecallBookmark();
                            //    if (bHaveNationalPrice) {
                            //        //m_sCusstAdr = oCIP.U_StAddr;
                            //        //m_sCusstZipCd = oCIP.U_ZpCd;
                            //        //oCIP.U_EnDate = dt_EffDate.AddDays(-1);
                            //        bdoAddNewPrice = true;// oCIP.doUpdateDataRow();
                            //    } else {
                            m_sCusstAdr = "";
                            m_sCusstZipCd = "";
                            m_sCustAddressCd = "";
                            //        //oCIP.U_EnDate = dt_EffDate.AddDays(-1);
                            //        //bdoAddNewPrice = oCIP.doUpdateDataRow();
                            //    }
                            //}

                            //m_sSupplierCd = oCIP.U_BP2CD;
                            //m_sUOM = oCIP.U_UOM;
                            //m_sItemCd = oCIP.U_ItemCd;
                            //m_sItemNm = oCIP.U_ItemDs;
                            //m_sWasteCd = oCIP.U_WasteCd;
                            //m_sWasteNM = oCIP.U_WasteDs;
                            m_JbTypeDs = s_JbTypeDs;
                            //Add price
                        }
                    } else {
                        m_sCusstAdr = "";
                        m_sCusstZipCd = "";
                        m_sCustAddressCd = "";
                        if (sJobSpecficPrice != "Y")
                            m_JbTypeDs = "";
                    }
                    if (bdoAddNewPrice) {//do Add New Price
                        if (doAddPriceinSIP(SIPU_FTon, SIPU_TTon, SIPU_Haulage, SIPU_TipTon, SIPBaseWeightFrom, SIPBaseWeightTo, sChrgCal,
                            m_sSupCd, m_sSupNm, m_sSupAdr, m_sSupZipCd, m_sSupAddressCd
                            , m_sCustCd, m_sCusstAdr, m_sCusstZipCd, m_sCusstZipCd, s_UOM, dt_EffDate, s_WasCd, s_WasNm, s_ItemCd, s_ItemNm, m_JbTypeDs) == false) {
                            return false;
                        }
                    }
                    SIPU_FTon = SIPU_TTon + 0.01;
                }

                m_sSupCd = sSupplierCd;
                m_sSupNm = sSupplierNm;
                m_sSupAdr = sSupplierAddress;
                m_sSupZipCd = sSupplierPostcode;

                m_sCustCd = sCardCd;
                m_sCustNm = sCardNm;
                m_sCusstAdr = sCardAddress;
                m_sCusstZipCd = sCardPostcode;

                m_sUOM = s_UOM;
                m_sItemCd = s_ItemCd;
                m_sItemNm = s_ItemNm;
                m_sWasteCd = s_WasCd;
                m_sWasteNM = s_WasNm;
                m_sSupAddressCd = sSupAddressCd;
                m_sCustAddressCd = sCustAddressCd;
                m_JbTypeDs = s_JbTypeDs;

                //SIPU_FTon = 0;
                SIPU_TTon = 0;
                SIPU_Haulage = 0;
                SIPU_TipTon = 0;
                SIPBaseWeightFrom = 0;
                SIPBaseWeightTo = 0;
                //SIP Pricing Min Charge Price Row 2
                if (s_SMnChg.Trim().ToUpper() == "YES" || s_SMnChg.Trim().ToUpper() == "Y") {//Minimum charge
                    bool bdoAddNewPrice = true;
                    sChrgCal = "Fixed";
                    //SIPU_FTon = 0;
                    SIPU_TTon = d_SMCgVol;
                    SIPU_Haulage = d_SHaulage;
                    SIPU_TipTon = d_SMinChg;

                    //sActiveDateRange = IDH_SUITPR._StDate + " \'" + U_EffDate.ToString("yyyy/MM/dd") + "\' ";
                    IDH_SUITPR oSIP = null;// new IDH_SUITPR();
                    oPrices = doValidateSIPPriceExists(ref oSIP, SIPU_FTon, SIPU_TTon, SIPU_Haulage, SIPU_TipTon, sChrgCal, sSupplierCd, sSupplierNm,
                        sSupplierAddress, sSupplierPostcode, sCardCd, sCardAddress, sCardPostcode, s_JbTypeDs, dt_EffDate, s_WasCd, s_ItemCd, s_UOM, sItemGrpCode, sJobSpecficPrice);
                    if (oPrices != null && oPrices.Found && oPrices.PriceType != Price.PRICETYPE_NONE) {

                        if (sJobSpecficPrice != "Y" || (sJobSpecficPrice == "Y" && oSIP.U_JobTp == m_JbTypeDs)) {//Non Job Specific
                            if (Convert.ToDecimal(SIPU_FTon) != Convert.ToDecimal(oSIP.U_FTon) || Convert.ToDecimal(SIPU_TTon) != Convert.ToDecimal(oSIP.U_TTon)
                            || Convert.ToDecimal(SIPU_Haulage) != Convert.ToDecimal(oPrices.HaulPrice)
                            || Convert.ToDecimal(SIPU_TipTon) != Convert.ToDecimal(oPrices.TipPrice)) {//As Price is different so de-activate current and add new
                                //m_sSupCd= oSIP.U_CardCd;
                                m_JbTypeDs = s_JbTypeDs;
                                //m_sAddressCd = "";
                                if (oSIP.U_StAddr == "") {//found price is national so add site specific price
                                    bdoAddNewPrice = true;
                                    if (oSIP.U_BP2CD == "") {
                                        m_sCusstAdr = "";
                                        m_sCusstZipCd = "";
                                        m_sCustAddressCd = "";
                                    }
                                    //if(oCIP.U_ItemCd!=m_sItemCd || oCIP.U_WasteCd != m_sWasteCd || oCIP.U_JobTp!=m_JbTypeDs) {
                                    //    m_sCusstAdr = "";
                                    //    m_sCusstZipCd = "";
                                    //    m_sAddressCd = "";
                                    //}
                                } else if (oSIP.U_StAddr != "") {//found price is site specific so add site specific price
                                    oSIP.doBookmark();
                                    bool bHaveNationalPrice = false;
                                    while (oSIP.next()) {
                                        if (oSIP.U_StAddr == "") {
                                            bHaveNationalPrice = true;
                                            break;
                                        }
                                    }
                                    oSIP.doRecallBookmark();
                                    if (bHaveNationalPrice) {
                                        oSIP.U_EnDate = dt_EffDate.AddDays(-1);
                                        bdoAddNewPrice = oSIP.doUpdateDataRow();
                                    } else {
                                        if (oSIP.U_StAddr == m_sCusstAdr) {
                                            oSIP.U_EnDate = dt_EffDate.AddDays(-1);
                                            bdoAddNewPrice = oSIP.doUpdateDataRow();
                                        }
                                        m_sCusstAdr = "";
                                        m_sCusstZipCd = "";
                                        m_sCustAddressCd = "";
                                        // oCIP.U_EnDate = dt_EffDate.AddDays(-1);
                                        //  bdoAddNewPrice = oCIP.doUpdateDataRow();
                                    }
                                }

                                //m_sSupplierCd = oCIP.U_BP2CD;
                                //m_sUOM = oCIP.U_UOM;
                                //m_sItemCd = oCIP.U_ItemCd;
                                //m_sItemNm = oCIP.U_ItemDs;
                                //m_sWasteCd = oCIP.U_WasteCd;
                                //m_sWasteNM = oCIP.U_WasteDs;
                                if (sJobSpecficPrice != "Y")
                                    m_JbTypeDs = "";
                                //Add price

                            } else {
                                bdoAddNewPrice = false;
                                SIPU_TTon = oSIP.U_TTon;
                            }
                        } else {//job specific and job not matched                            
                                //As job type is different so add new 
                                // m_sSupCd = oSIP.U_CardCd;
                            bdoAddNewPrice = true;
                            //m_sAddressCd = "";
                            //if (oCIP.U_StAddr == "") {//found price is national so add site specific price
                            //     bdoAddNewPrice = true;
                            //} else 

                            //if (oCIP.U_StAddr != "") {//found price is site specific so add site specific price
                            //    oCIP.doBookmark();
                            //    bool bHaveNationalPrice = false;
                            //    while (oCIP.next()) {
                            //        if (oCIP.U_StAddr == "") {
                            //            bHaveNationalPrice = true;
                            //            break;
                            //        }
                            //    }
                            //    oCIP.doRecallBookmark();
                            //    if (bHaveNationalPrice) {
                            //        //m_sCusstAdr = oCIP.U_StAddr;
                            //        //m_sCusstZipCd = oCIP.U_ZpCd;
                            //        //oCIP.U_EnDate = dt_EffDate.AddDays(-1);
                            //        bdoAddNewPrice = true;// oCIP.doUpdateDataRow();
                            //    } else {
                            m_sCusstAdr = "";
                            m_sCusstZipCd = "";
                            m_sCustAddressCd = "";
                            //        //oCIP.U_EnDate = dt_EffDate.AddDays(-1);
                            //        //bdoAddNewPrice = oCIP.doUpdateDataRow();
                            //    }
                            //}

                            //m_sSupplierCd = oCIP.U_BP2CD;
                            //m_sUOM = oCIP.U_UOM;
                            //m_sItemCd = oCIP.U_ItemCd;
                            //m_sItemNm = oCIP.U_ItemDs;
                            //m_sWasteCd = oCIP.U_WasteCd;
                            //m_sWasteNM = oCIP.U_WasteDs;
                            m_JbTypeDs = s_JbTypeDs;
                            //Add price
                        }
                    } else {
                        m_sCusstAdr = "";
                        m_sCusstZipCd = "";
                        m_sCustAddressCd = "";
                        if (sJobSpecficPrice != "Y")
                            m_JbTypeDs = "";
                    }
                    if (bdoAddNewPrice) {//do Add New Price
                        if (doAddPriceinSIP(SIPU_FTon, SIPU_TTon, SIPU_Haulage, SIPU_TipTon, SIPBaseWeightFrom, SIPBaseWeightTo, sChrgCal,
                            m_sSupCd, m_sSupNm, m_sSupAdr, m_sSupZipCd, m_sSupAddressCd
                            , m_sCustCd, m_sCusstAdr, m_sCustAddressCd, m_sCusstZipCd, s_UOM, dt_EffDate, s_WasCd, s_WasNm, s_ItemCd, s_ItemNm, m_JbTypeDs) == false) {
                            return false;
                        } //else
                          // SIPU_TTon = oSIP.U_TTon;
                    }
                }
                //here i did not reset prices varabiles as its been used in next pricing row
                bool doVariablePrice = false;
                m_sSupCd = sSupplierCd;
                m_sSupNm = sSupplierNm;
                m_sSupAdr = sSupplierAddress;
                m_sSupZipCd = sSupplierPostcode;

                m_sCustCd = sCardCd;
                m_sCustNm = sCardNm;
                m_sCusstAdr = sCardAddress;
                m_sCusstZipCd = sCardPostcode;

                m_sUOM = s_UOM;
                m_sItemCd = s_ItemCd;
                m_sItemNm = s_ItemNm;
                m_sWasteCd = s_WasCd;
                m_sWasteNM = s_WasNm;
                m_sSupAddressCd = sSupAddressCd;
                m_sCustAddressCd = sCustAddressCd;
                m_JbTypeDs = s_JbTypeDs;

                if (s_SVarRFix.Trim().ToUpper() == "FIXED") {//Variable Or Fixed QTY
                    sChrgCal = "Variable";
                    if (SIPU_TTon > 0.0)//here used this to get weight for next stage of price; max weight from +.01 
                        SIPU_FTon = SIPU_TTon + 0.01;
                    else
                        SIPU_FTon = 0.0;

                    SIPU_TTon = 999.0;

                    SIPU_Haulage = 0;
                    if (s_SMnChg.Trim().ToUpper() == "YES" || s_SMnChg.Trim().ToUpper() == "Y") {
                        if (d_SRminVol == 0)
                            SIPU_Haulage = 0;
                        else {
                            if (SIPU_TipTon / d_SMCgVol == d_SRminVol)
                                SIPU_Haulage = d_SHaulage;
                            else {
                                SIPU_Haulage = SIPU_TipTon - (d_SMCgVol * d_SRminVol) + d_SHaulage;
                            }
                        }
                    } else {
                        SIPU_Haulage = d_SRminVol;
                    }

                    if (s_SMnChg.Trim().ToUpper() == "YES" || s_SMnChg.Trim().ToUpper() == "Y")
                        SIPU_TipTon = d_SRminVol;
                    else
                        SIPU_TipTon = 0;


                    doVariablePrice = true;
                } else if (s_SVarRFix.Trim().ToUpper() == "VARIABLE") {
                    //Variable is default: Haze email: 10-02-2017
                    //User must select one of the value Hazel/HN skype call 14-02-2017
                    sChrgCal = "Variable";
                    if (SIPU_TTon > 0.0)//here used this to get weight for next stage of price; max weight from +.01 
                        SIPU_FTon = SIPU_TTon + 0.01;
                    else
                        SIPU_FTon = 0.0;

                    SIPU_TTon = 999.0;

                    SIPU_Haulage = 0;
                    if (s_SMnChg.Trim().ToUpper() == "YES" || s_SMnChg.Trim().ToUpper() == "Y") {
                        if (d_SRminVol == 0)
                            SIPU_Haulage = 0;
                        else {
                            if (SIPU_TipTon / d_SMCgVol == d_SRminVol)
                                SIPU_Haulage = d_SHaulage;
                            else {
                                SIPU_Haulage = SIPU_TipTon - (d_SMCgVol * d_SRminVol) + d_SHaulage;
                            }
                        }
                    } else {
                        SIPU_Haulage = d_SHaulage;
                    }
                    SIPU_TipTon = d_SRminVol;

                    doVariablePrice = true;
                }
                if (s_Rebate == "Y" && doOnlyForRebate == false)
                    SIPU_TipTon = 0;
                else if (s_Rebate == "Y" && doOnlyForRebate == true)
                    SIPU_Haulage = 0;
                if (doVariablePrice) {
                    bool bdoAddNewPrice = true;
                    IDH_SUITPR oSIP = null;//= new IDH_SUITPR();

                    oPrices = doValidateSIPPriceExists(ref oSIP, SIPU_FTon, SIPU_TTon, SIPU_Haulage, SIPU_TipTon, sChrgCal, sSupplierCd, sSupplierNm,
                        sSupplierAddress, sSupplierPostcode, sCardCd, sCardAddress, sCardPostcode, s_JbTypeDs, dt_EffDate, s_WasCd, s_ItemCd, s_UOM,
                        sItemGrpCode, sJobSpecficPrice);
                    //bool bRet = true;

                    if (oPrices != null && oPrices.Found && oPrices.PriceType != Price.PRICETYPE_NONE) {

                        if (sJobSpecficPrice != "Y" || (sJobSpecficPrice == "Y" && oSIP.U_JobTp == m_JbTypeDs)) {//Non Job Specific
                            if (Convert.ToDecimal(SIPU_FTon) != Convert.ToDecimal(oSIP.U_FTon) || Convert.ToDecimal(SIPU_TTon) != Convert.ToDecimal(oSIP.U_TTon)
                            || Convert.ToDecimal(SIPU_Haulage) != Convert.ToDecimal(oPrices.HaulPrice)
                            || Convert.ToDecimal(SIPU_TipTon) != Convert.ToDecimal(oPrices.TipPrice)) {//As Price is different so de-activate current and add new
                                //m_sSupCd= oSIP.U_CardCd;
                                m_JbTypeDs = s_JbTypeDs;
                                //m_sAddressCd = "";
                                if (oSIP.U_StAddr == "") {//found price is national so add site specific price
                                    bdoAddNewPrice = true;
                                    if (oSIP.U_BP2CD == "") {
                                        m_sCusstAdr = "";
                                        m_sCusstZipCd = "";
                                        m_sCustAddressCd = "";
                                    }
                                    //if(oCIP.U_ItemCd!=m_sItemCd || oCIP.U_WasteCd != m_sWasteCd || oCIP.U_JobTp!=m_JbTypeDs) {
                                    //    m_sCusstAdr = "";
                                    //    m_sCusstZipCd = "";
                                    //    m_sAddressCd = "";
                                    //}
                                } else if (oSIP.U_StAddr != "") {//found price is site specific so add site specific price
                                    oSIP.doBookmark();
                                    bool bHaveNationalPrice = false;
                                    while (oSIP.next()) {
                                        if (oSIP.U_StAddr == "") {
                                            bHaveNationalPrice = true;
                                            break;
                                        }
                                    }
                                    oSIP.doRecallBookmark();
                                    if (bHaveNationalPrice) {
                                        oSIP.U_EnDate = dt_EffDate.AddDays(-1);
                                        bdoAddNewPrice = oSIP.doUpdateDataRow();
                                    } else {
                                        if (oSIP.U_StAddr == m_sCusstAdr) {
                                            oSIP.U_EnDate = dt_EffDate.AddDays(-1);
                                            bdoAddNewPrice = oSIP.doUpdateDataRow();
                                        }
                                        m_sCusstAdr = "";
                                        m_sCusstZipCd = "";
                                        m_sCustAddressCd = "";
                                        // oCIP.U_EnDate = dt_EffDate.AddDays(-1);
                                        //  bdoAddNewPrice = oCIP.doUpdateDataRow();
                                    }
                                }

                                //m_sSupplierCd = oCIP.U_BP2CD;
                                //m_sUOM = oCIP.U_UOM;
                                //m_sItemCd = oCIP.U_ItemCd;
                                //m_sItemNm = oCIP.U_ItemDs;
                                //m_sWasteCd = oCIP.U_WasteCd;
                                //m_sWasteNM = oCIP.U_WasteDs;
                                if (sJobSpecficPrice != "Y")
                                    m_JbTypeDs = "";
                                //Add price

                            } else
                                bdoAddNewPrice = false;
                        } else {//job specific and job not matched                            
                                //As job type is different so add new 
                                // m_sSupCd = oSIP.U_CardCd;
                            bdoAddNewPrice = true;
                            //m_sAddressCd = "";
                            //if (oCIP.U_StAddr == "") {//found price is national so add site specific price
                            //     bdoAddNewPrice = true;
                            //} else 

                            //if (oCIP.U_StAddr != "") {//found price is site specific so add site specific price
                            //    oCIP.doBookmark();
                            //    bool bHaveNationalPrice = false;
                            //    while (oCIP.next()) {
                            //        if (oCIP.U_StAddr == "") {
                            //            bHaveNationalPrice = true;
                            //            break;
                            //        }
                            //    }
                            //    oCIP.doRecallBookmark();
                            //    if (bHaveNationalPrice) {
                            //        //m_sCusstAdr = oCIP.U_StAddr;
                            //        //m_sCusstZipCd = oCIP.U_ZpCd;
                            //        //oCIP.U_EnDate = dt_EffDate.AddDays(-1);
                            //        bdoAddNewPrice = true;// oCIP.doUpdateDataRow();
                            //    } else {
                            m_sCusstAdr = "";
                            m_sCusstZipCd = "";
                            m_sCustAddressCd = "";
                            //        //oCIP.U_EnDate = dt_EffDate.AddDays(-1);
                            //        //bdoAddNewPrice = oCIP.doUpdateDataRow();
                            //    }
                            //}

                            //m_sSupplierCd = oCIP.U_BP2CD;
                            //m_sUOM = oCIP.U_UOM;
                            //m_sItemCd = oCIP.U_ItemCd;
                            //m_sItemNm = oCIP.U_ItemDs;
                            //m_sWasteCd = oCIP.U_WasteCd;
                            //m_sWasteNM = oCIP.U_WasteDs;
                            m_JbTypeDs = s_JbTypeDs;
                            //Add price
                        }
                    } else {
                        m_sCusstAdr = "";
                        m_sCusstZipCd = "";
                        m_sCustAddressCd = "";
                        if (sJobSpecficPrice != "Y")
                            m_JbTypeDs = "";
                    }
                    if (bdoAddNewPrice) {//do Add New Price
                        if (doAddPriceinSIP(SIPU_FTon, SIPU_TTon, SIPU_Haulage, SIPU_TipTon, SIPBaseWeightFrom, SIPBaseWeightTo, sChrgCal,
                            m_sSupCd, m_sSupNm, m_sSupAdr, m_sSupZipCd, m_sSupAddressCd
                            , m_sCustCd, m_sCusstAdr, m_sCustAddressCd, m_sCusstZipCd, s_UOM, dt_EffDate, s_WasCd, s_WasNm, s_ItemCd, s_ItemNm, m_JbTypeDs) == false) {
                            return false;
                        }
                    }
                }

                return true;

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResUserError(ex.Message, "EREXSGEN",
                      new string[] { "IDH_LVSBTH.doCalculateNUpdateCIPPrices" });
                return false;
            }
        }


        private static Prices doValidateSIPPriceExists(ref IDH_SUITPR oSIP, double SIPU_FTon, double SIPU_TTon, double SIPU_Haulage, double SIPU_TipTon, string sChrgCal,
            string sSupCd, string sSupNm, string sSupAdr, string sSupPcode,
            string sCardCd, string sCardAddress, string sCardPostcode, string sJobType, DateTime dt_EffDate, string s_WastCd,
            string s_ItemCd, string s_UOM, string sItemGrpCode, string sJobSpecficPrice) {
            oSIP = new IDH_SUITPR();
            oSIP.UnLockTable = true;
            Prices oPrices = oSIP.doGetJobCostPrice("WO", sJobType, sItemGrpCode, s_ItemCd, sSupCd, SIPU_TTon, s_UOM, s_WastCd, sCardCd, sCardAddress, sSupAdr, dt_EffDate, "", sCardPostcode, sChrgCal, sJobSpecficPrice);
            if (oPrices.Count > 0 && oPrices.PriceType != Price.PRICETYPE_NONE && oPrices.TipChargeCalc.ToUpper() != sChrgCal.ToUpper())
                oPrices.Clear();
            return oPrices;
            //string sActiveDateRange = " (DATEDIFF(DD, U_StDate, CAST((CONVERT(NVARCHAR, \'" + dt_EffDate.ToString("yyyy/MM/dd") + "\'  , 12) ) AS DATETIME)) >= 0 And " +
            //                "DATEDIFF(DD, U_EnDate, CAST((CONVERT(NVARCHAR, \'" + dt_EffDate.ToString("yyyy/MM/dd") + "\', 12) ) AS DATETIME)) <= 0) ";
            ////sActiveDateRange = IDH_SUITPR._StDate + " \'" + U_EffDate.ToString("yyyy/MM/dd") + "\' ";
            //oSIP.getData(IDH_SUITPR._CardCd + "=\'" + sSupCd + "\' And "
            //   + IDH_SUITPR._JobTp + "=\'" + sJobType + "\' And "
            //   + IDH_SUITPR._WasteCd + "=\'" + s_WastCd + "\' And "
            //   + IDH_SUITPR._ItemCd + "=\'" + s_ItemCd + "\' And "
            //   + IDH_SUITPR._SupAddr + "=\'" + sSupAdr + "\' And "
            //   + IDH_SUITPR._SupZipCd + "=\'" + sSupPcode + "\' And "
            //   + IDH_SUITPR._ChrgCal + "=\'" + sChrgCal + "\' And "
            //  + IDH_SUITPR._BP2CD + "=\'" + sCardCd + "\' And "
            //  + IDH_SUITPR._StAddr + "=\'" + sCardAddress + "\' And "
            //  + IDH_SUITPR._ZpCd + "=\'" + sCardPostcode + "\' And "
            //  + IDH_SUITPR._UOM + "=\'" + s_UOM + "\' And "
            //  + sActiveDateRange, "");
            //if (oSIP.Count > 0)
            //    return oSIP.Count;
            //oSIP.getData(IDH_SUITPR._CardCd + "=\'" + sSupCd + "\' And "
            //  + IDH_SUITPR._JobTp + "=\'" + sJobType + "\' And "
            //    + IDH_SUITPR._WasteCd + "=\'" + s_WastCd + "\' And "
            //    + IDH_SUITPR._ItemCd + "=\'" + s_ItemCd + "\' And "
            //   + " IsNull(" + IDH_SUITPR._SupAddr + ",\'\')=\'" + "" + "\' And "
            //   + " IsNull(" + IDH_SUITPR._SupZipCd + ",\'\')=\'" + "" + "\' And "
            //    + IDH_SUITPR._ChrgCal + "=\'" + sChrgCal + "\' And "
            //   + IDH_SUITPR._BP2CD + "=\'" + sCardCd + "\' And "
            //   + " IsNull(" + IDH_SUITPR._StAddr + ",\'\')=\'" + "" + "\' And "
            //   + " IsNull(" + IDH_SUITPR._ZpCd + ",\'\')=\'" + "" + "\' And "
            //   + IDH_SUITPR._UOM + "=\'" + s_UOM + "\' And "
            //  + sActiveDateRange, "");
            //if (oSIP.Count > 0)
            //    return oSIP.Count;
            //oSIP.getData(IDH_SUITPR._CardCd + "=\'" + sSupCd + "\' And "
            //    + " IsNull(" + IDH_SUITPR._JobTp + ",\'\')=\'" + "" + "\' And "
            //    + IDH_SUITPR._WasteCd + "=\'" + s_WastCd + "\' And "
            //    + IDH_SUITPR._ItemCd + "=\'" + s_ItemCd + "\' And "
            //    + IDH_SUITPR._SupAddr + "=\'" + sSupAdr + "\' And "
            //    + IDH_SUITPR._SupZipCd + "=\'" + sSupPcode + "\' And "
            //    + IDH_SUITPR._ChrgCal + "=\'" + sChrgCal + "\' And "
            //   + IDH_SUITPR._BP2CD + "=\'" + sCardCd + "\' And "
            //   + IDH_SUITPR._StAddr + "=\'" + sCardAddress + "\' And "
            //   + IDH_SUITPR._ZpCd + "=\'" + sCardPostcode + "\' And "
            //   + IDH_SUITPR._UOM + "=\'" + s_UOM + "\' And "
            //   + sActiveDateRange, "");
            //if (oSIP.Count > 0)
            //    return oSIP.Count;
            //return oSIP.getData(IDH_SUITPR._CardCd + "=\'" + sSupCd + "\' And "
            //     + " IsNull(" + IDH_SUITPR._JobTp + ",\'\')=\'" + "" + "\' And "
            //     + IDH_SUITPR._WasteCd + "=\'" + s_WastCd + "\' And "
            //     + IDH_SUITPR._ItemCd + "=\'" + s_ItemCd + "\' And "
            //     + " IsNull(" + IDH_SUITPR._SupAddr + ",\'\')=\'" + "" + "\' And "
            //     + " IsNull(" + IDH_SUITPR._SupZipCd + ",\'\')=\'" + "" + "\' And "
            //      + IDH_SUITPR._ChrgCal + "=\'" + sChrgCal + "\' And "
            //     + IDH_SUITPR._BP2CD + "=\'" + sCardCd + "\' And "
            //     + " IsNull(" + IDH_SUITPR._StAddr + ",\'\')=\'" + "" + "\' And "
            //     + " IsNull(" + IDH_SUITPR._ZpCd + ",\'\')=\'" + "" + "\' And "
            //     + IDH_SUITPR._UOM + "=\'" + s_UOM + "\' And "
            //    + sActiveDateRange, "");


        }
        //Will move it in SIP db object after testing the basic working 
        private static bool doAddPriceinSIP(double SIPU_FTon, double SIPU_TTon, double SIPU_Haulage, double SIPU_TipTop, double SIPBaseWeightFrom,
            double SIPBaseWeightTo, string sChrgCal, string sSupCode, string sSupName, string sSupAdr, string sSupPCode, string sSupAddressCd,
            string sCardCd, string sCardAddress, string sCustAddressCd, string sCardPostcode, string s_UOM, DateTime dt_EffDate, string s_WastCd, string s_WasteNm,
            string s_ItemCd, string s_ItemNm, string s_JbTypeDs) {
            IDH_SUITPR oSIP = new IDH_SUITPR();
            oSIP.doAddEmptyRow(true);
            if (SIPU_Haulage < 0)
                SIPU_Haulage = 0;
            oSIP.U_Haulge = Convert.ToDouble(Convert.ToDecimal(SIPU_Haulage));
            oSIP.U_HaulCal = "STANDARD";
            oSIP.U_TipTon = Convert.ToDouble(Convert.ToDecimal(SIPU_TipTop));
            oSIP.U_ChrgCal = sChrgCal;
            oSIP.U_FTon = Convert.ToDouble(Convert.ToDecimal(SIPU_FTon));
            oSIP.U_TTon = Convert.ToDouble(Convert.ToDecimal(SIPU_TTon));
            oSIP.U_UOM = s_UOM;
            oSIP.U_StDate = dt_EffDate;

            oSIP.U_WasteCd = s_WastCd;
            oSIP.U_WasteDs = s_WasteNm;

            oSIP.U_ItemCd = s_ItemCd;
            oSIP.U_ItemDs = s_ItemNm;


            //oSIP.U_FrmBsWe = SIPBaseWeightFrom;
            //oSIP.U_ToBsWe = SIPBaseWeightTo;



            oSIP.U_CardCd = sSupCode;
            //oSIP.U_car = U_SupAddr;

            oSIP.U_SupAddr = sSupAdr;
            oSIP.U_SupAdLN = sSupAddressCd;
            oSIP.U_SupZipCd = sSupPCode;

            oSIP.U_BP2CD = sCardCd;
            oSIP.U_StAddr = sCardAddress;
            oSIP.U_ZpCd = sCardPostcode;
            oSIP.U_StAdLN = sCustAddressCd;


            oSIP.U_WR1ORD = "WO";
            oSIP.U_JobTp = s_JbTypeDs;
            com.idh.dbObjects.numbers.NumbersPair oNumbers = oSIP.getNewKey();
            oSIP.Code = oNumbers.CodeCode;
            oSIP.Name = oNumbers.NameCode;
            return oSIP.doProcessData();
        }


    }
}
