﻿
using System;
using com.idh.dbObjects.User;
using com.idh.bridge;
using com.idh.bridge.lookups;

namespace com.isb.dbObjects.User {

    public class IDH_JOBSHD : com.idh.dbObjects.User.IDH_JOBSHD {
        public IDH_JOBSHD()
            : base() {
        }

        public IDH_JOBSHD(IDH_JOBENTR oParent) : base(oParent) {
        }

        public IDH_JOBSHD(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm, IDH_JOBENTR oParent)
            : base(oIDHForm, oForm, oParent) { }

        public IDH_JOBSHD(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm, IDH_JOBENTR oParent, bool bASSBODataTable)
            : base(oIDHForm, oForm, oParent, bASSBODataTable) {
        }

        public IDH_JOBSHD(System.Windows.Forms.Control oParentControl, IDH_JOBENTR oParent)
            : base(oParentControl, oParent) {
        }

        //public override string doDuplicateRow(int iSrcRow, bool bCopyComments, bool bCopyPrice, bool bCopyQty, string sJob, bool bClearHaulage, bool bSetStartDate, bool bCopyAdditems, bool bCopyVehReg, bool bCopyDriver, string sReqDate, bool bFromOptions) {
        //    string sNewCode = base.doDuplicateRow(iSrcRow, bCopyComments, bCopyPrice, bCopyQty, sJob, bClearHaulage, bSetStartDate, bCopyAdditems, bCopyVehReg, bCopyDriver, sReqDate, bFromOptions);

        //    if (sNewCode != null) { 
        //        BlockChangeNotif = true;
        //        try {

        //            if (U_WasCd != string.Empty && Config.INSTANCE.getParameterAsBool("CPSMRWOR", true))
        //                AddLabTaskFlagIfRequired(U_WasCd);

        //        } catch (Exception ex) {
        //            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDDDR", null);
        //            sNewCode = null;
        //        } finally {

        //            BlockChangeNotif = false;
        //        }
        //    }


        //    return sNewCode;
        //}


        //public override bool doPostAddUpdateProcess() {
        //    bool bResult = false;
        //    try {

        //        if (!DataHandler.INSTANCE.IsInTransaction() && U_Status != FixedValues.getStatusCanceled() && !mbFromDelink)
        //            bResult = doMarketingDocuments();
        //        doSaveSampleTask();
        //        doSentNCRAlert();
        //        doUpdateFinalBatchQty();
        //    } catch (Exception ex) {
        //        DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBUP", null);
        //        bResult = false;
        //    } finally {
        //    }


        //    return bResult;
        //}


        //public override void doSetFieldHasChanged(int iRow, string sFieldName, object oOldValue, object oNewValue) {
        //    base.doSetFieldHasChanged(iRow, sFieldName, oOldValue, oNewValue);

        //    if (BlockChangeNotif)
        //        return;


        //    BlockChangeNotif = true;
        //    try {

        //        if (sFieldName.Equals(_WasCd)) { //Waste Code
        //            if (!string.IsNullOrEmpty(U_WasCd)) {
        //                string sVal = oNewValue.ToString();
        //                if (!string.IsNullOrWhiteSpace(sVal) && sVal != "*" && U_Sample != "Y")
        //                    AddLabTaskFlagIfRequired(oNewValue.ToString());

        //            }

        //        } else if (sFieldName.Equals(_Sample) && (oOldValue != oNewValue)) {
        //            if ((oOldValue == null || oOldValue.ToString() == "" || oOldValue.ToString() == "N") && oNewValue.ToString() == "Y" && U_SampleRef == string.Empty) {
        //                //create sample ref
        //                doCreateSampleTask(0);
        //            } else if (((oOldValue == null || oOldValue.ToString() == "Y") && oNewValue.ToString() == "N" && U_SampleRef != string.Empty)) {
        //                BlockChangeNotif = true;
        //                IDH_LABTASK oLabTask = new IDH_LABTASK();
        //                if (oLabTask.getByKey(U_SampleRef.Trim()) && (oLabTask.U_Saved == 1)) {
        //                    if (Handler_ExternalTriggerWithData_ShowMessage != null)
        //                        Handler_ExternalTriggerWithData_ShowMessage(this, "LAB00001");
        //                    U_Sample = "Y";

        //                    //send alert to make it cancel if they want from lab module
        //                }

        //                BlockChangeNotif = false;

        //            }
        //        }


        //    } catch (Exception e) {
        //        throw new Exception("Field: [" + sFieldName + "] OValue: [" + oOldValue + "] NValue: [" + oNewValue + "] " + e.Message);
        //    } finally {
        //        BlockChangeNotif = false;
        //    }
        //}



        //public bool AddLabTaskFlagIfRequired(string sWasCode) {
        //    if (com.isb.bridge.utils.General.doCheckIfLabTaskRequired(sWasCode)) {
        //        BlockChangeNotif = true;
        //        try {

        //            U_Sample = "Y";
        //            doCreateSampleTask(0);
        //        } finally {

        //            BlockChangeNotif = false;
        //        }

        //        return true;
        //    } else

        //        return false;
        //}

        //public static bool doCreateSampleTask(int iSave, string WasCd, string WasDsc, string WasFDsc, string UOM, int Quantity, string Comment,
        //    string sLineNum, string sDocNum, string sCardCode, string CardName, ref string sSampleRef, ref string sSampleStatus, string sDispRouteCode) {
        //    bool bResult = false;
        //    //string sSampleRef = "", sSampleStatus = "";
        //    bResult = com.isb.bridge.utils.General.doCreateLabItemNTask(WasCd,
        //        WasDsc,
        //        WasFDsc,
        //        UOM,



        //        Quantity.ToString(), Comment,
        //        sLineNum, sDocNum, "WOR",
        //        sCardCode, CardName, "", false, iSave, ref sSampleRef, ref sSampleStatus, "", sDispRouteCode);
        //    return bResult;
        //}

        //public bool doCreateSampleTask(int iSave) {
        //    try {

        //        BlockChangeNotif = true;
        //        string sSampleRef = "", sSampleStatus = "";
        //        if (doCreateSampleTask(iSave, U_WasCd, U_WasDsc, "", U_UOM, U_Quantity, U_Comment, Code, U_JobNr, U_CustCd,
        //            U_CustNm, ref sSampleRef, ref sSampleStatus, U_RouteCd)) {
        //            U_SampleRef = sSampleRef;
        //            U_SampStatus = sSampleStatus;
        //            return true;
        //        }


        //        U_Sample = "N";
        //        return false;
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", null);
        //        return false;
        //    } finally {
        //        BlockChangeNotif = false;
        //    }
        //}


        //public void doSaveSampleTask() {
        //    if (U_Sample == "Y") {
        //        if (string.IsNullOrEmpty(U_SampleRef)) {
        //            doCreateSampleTask(1);
        //        } else {

        //            IDH_LABTASK oLabTask = new IDH_LABTASK();
        //            oLabTask.UnLockTable = true;
        //            if (oLabTask.getByKey(U_SampleRef.Trim()) && (oLabTask.U_Saved == 0)) {
        //                oLabTask.U_Saved = 1;
        //                string sUser = oLabTask.U_AssignedTo;
        //                oLabTask.doProcessData();

        //                IDH_LABITM oLabItem = new IDH_LABITM();
        //                oLabItem.UnLockTable = true;
        //                oLabItem.getByKey(oLabTask.U_LabItemRCd);
        //                oLabItem.U_Saved = 1;
        //                oLabItem.doProcessData();
        //                if (!string.IsNullOrEmpty(sUser))
        //                    IDH_LABTASK.dosendAlerttoUser(sUser, com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTM1", null), com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTS1", null), false, U_SampleRef);
        //            }
        //        }
        //    }
        //}




        //public void doUpdateFinalBatchQty() {
        //    try {

        //        if (U_LABFBTCD != null && U_LABFBTCD != string.Empty) {
        //            IDH_PVHBTH.doUpdateWOR_ContainerQty(U_LABFBTCD);
        //        }

        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", null);
        //    } finally {
        //    }
        //}
    }
}
