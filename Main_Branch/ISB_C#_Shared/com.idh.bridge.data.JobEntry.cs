﻿/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2010/05/31
 * Time: 08:16 AM.
 */
using System;

namespace com.idh.bridge.data {
	/// <summary>
	/// Job entry used to calculate the sequence
	/// </summary>
      
    public class JobEntry {
		private string msRowNumber;
		private DateTime moRequested;
		private DateTime moAEDate;
		private char mcDirection;
		private double mdQty;
		private int miSequence;

		public string RowNumber {
			get { return msRowNumber; }
			set { msRowNumber = value; }
		}
		
		public DateTime Requested {
			get { return moRequested; }
			set { moRequested = value; }
		}
		
		public DateTime AEDate {
			get { return moAEDate; }
			set { moAEDate = value; }
		}		
		
		public char Direction {
			get { return mcDirection; }
			set { mcDirection = value; }
		}
		
		public double Qty {
			get { return mdQty; }
			set { mdQty = value; }
		}
		
		public int Sequence {
			get { return miSequence; }
			set { miSequence = value; }
		}	
		
		public JobEntry(string sRowNumber, DateTime oRequested, DateTime oAEDate, char cDirection, double dQty, int iSequence ){
			msRowNumber = sRowNumber;
			moRequested = oRequested;
			moAEDate = oAEDate;
			mcDirection = cDirection;
			mdQty = dQty;
			miSequence = iSequence;
		}
	}
}