/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 28/04/2016 12:56:02
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
   [Serializable]
	public class IDH_EVNMASTER: com.idh.dbObjects.DBBase { 

       private IDHAddOns.idh.forms.Base moIDHForm;
       public IDHAddOns.idh.forms.Base IDHForm {
           get { return moIDHForm; }
           set { moIDHForm = value; }
       }

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_EVNMASTER() : base("@IDH_EVNMASTER"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_EVNMASTER( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_EVNMASTER"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_EVNMASTER";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Activity
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Activity
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Activity = "U_Activity";
		public string U_Activity { 
			get {
 				return getValueAsString(_Activity); 
			}
			set { setValue(_Activity, value); }
		}
           public string doValidate_Activity() {
               return doValidate_Activity(U_Activity);
           }
           public virtual string doValidate_Activity(object oValue) {
               return base.doValidation(_Activity, oValue);
           }

		/**
		 * Decription: Attachment Entry
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AtcEntry
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _AtcEntry = "U_AtcEntry";
		public int U_AtcEntry { 
			get {
 				return getValueAsInt(_AtcEntry); 
			}
			set { setValue(_AtcEntry, value); }
		}
           public string doValidate_AtcEntry() {
               return doValidate_AtcEntry(U_AtcEntry);
           }
           public virtual string doValidate_AtcEntry(object oValue) {
               return base.doValidation(_AtcEntry, oValue);
           }

		/**
		 * Decription: CA Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CAAddr
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CAAddr = "U_CAAddr";
		public string U_CAAddr { 
			get {
 				return getValueAsString(_CAAddr); 
			}
			set { setValue(_CAAddr, value); }
		}
           public string doValidate_CAAddr() {
               return doValidate_CAAddr(U_CAAddr);
           }
           public virtual string doValidate_CAAddr(object oValue) {
               return base.doValidation(_CAAddr, oValue);
           }

		/**
		 * Decription: CA Block
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CABlock
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CABlock = "U_CABlock";
		public string U_CABlock { 
			get {
 				return getValueAsString(_CABlock); 
			}
			set { setValue(_CABlock, value); }
		}
           public string doValidate_CABlock() {
               return doValidate_CABlock(U_CABlock);
           }
           public virtual string doValidate_CABlock(object oValue) {
               return base.doValidation(_CABlock, oValue);
           }

		/**
		 * Decription: CA City
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CreatedBy
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CACity = "U_CACity";
		public string U_CACity { 
			get {
 				return getValueAsString(_CACity); 
			}
			set { setValue(_CACity, value); }
		}
           public string doValidate_CACity() {
               return doValidate_CACity(U_CACity);
           }
           public virtual string doValidate_CACity(object oValue) {
               return base.doValidation(_CACity, oValue);
           }

		/**
		 * Decription: Create Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CreateDt
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _CAID = "U_CAID";
		public string U_CAID { 
			get {
 				return getValueAsString(_CAID); 
			}
			set { setValue(_CAID, value); }
		}
           public string doValidate_CAID() {
               return doValidate_CAID(U_CAID);
			}
           public virtual string doValidate_CAID(object oValue) {
               return base.doValidation(_CAID, oValue);
		}

		/**
		 * Decription: CA Post Cd
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CAPostCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CAPostCd = "U_CAPostCd";
		public string U_CAPostCd { 
			get {
 				return getValueAsString(_CAPostCd); 
		}
			set { setValue(_CAPostCd, value); }
		}
           public string doValidate_CAPostCd() {
               return doValidate_CAPostCd(U_CAPostCd);
           }
           public virtual string doValidate_CAPostCd(object oValue) {
               return base.doValidation(_CAPostCd, oValue);
           }

		/**
		 * Decription: Disposal Site Cd
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DispSiteCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CarCd = "U_CarCd";
		public string U_CarCd { 
			get {
 				return getValueAsString(_CarCd); 
			}
			set { setValue(_CarCd, value); }
		}
           public string doValidate_CarCd() {
               return doValidate_CarCd(U_CarCd);
           }
           public virtual string doValidate_CarCd(object oValue) {
               return base.doValidation(_CarCd, oValue);
           }

		/**
		 * Decription: Disposal Site Nm
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DispSiteNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CarNm = "U_CarNm";
		public string U_CarNm { 
			get {
 				return getValueAsString(_CarNm); 
			}
			set { setValue(_CarNm, value); }
		}
           public string doValidate_CarNm() {
               return doValidate_CarNm(U_CarNm);
           }
           public virtual string doValidate_CarNm(object oValue) {
               return base.doValidation(_CarNm, oValue);
           }

		/**
		 * Decription: DS Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSAddress
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CASAddr = "U_CASAddr";
		public string U_CASAddr { 
			get {
 				return getValueAsString(_CASAddr); 
			}
			set { setValue(_CASAddr, value); }
		}
           public string doValidate_CASAddr() {
               return doValidate_CASAddr(U_CASAddr);
           }
           public virtual string doValidate_CASAddr(object oValue) {
               return base.doValidation(_CASAddr, oValue);
           }

		/**
		 * Decription: DS Block
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSBlock
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CASBlock = "U_CASBlock";
		public string U_CASBlock { 
			get {
 				return getValueAsString(_CASBlock); 
			}
			set { setValue(_CASBlock, value); }
		}
           public string doValidate_CASBlock() {
               return doValidate_CASBlock(U_CASBlock);
           }
           public virtual string doValidate_CASBlock(object oValue) {
               return base.doValidation(_CASBlock, oValue);
           }

		/**
		 * Decription: DS City
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSCity
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CASCity = "U_CASCity";
		public string U_CASCity { 
			get {
 				return getValueAsString(_CASCity); 
			}
			set { setValue(_CASCity, value); }
		}
           public string doValidate_CASCity() {
               return doValidate_CASCity(U_CASCity);
           }
           public virtual string doValidate_CASCity(object oValue) {
               return base.doValidation(_CASCity, oValue);
           }

		/**
		 * Decription: DS Post Cd
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSPostCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CASPostCd = "U_CASPostCd";
		public string U_CASPostCd { 
			get {
 				return getValueAsString(_CASPostCd); 
			}
			set { setValue(_CASPostCd, value); }
		}
           public string doValidate_CASPostCd() {
               return doValidate_CASPostCd(U_CASPostCd);
           }
           public virtual string doValidate_CASPostCd(object oValue) {
               return base.doValidation(_CASPostCd, oValue);
           }

		/**
		 * Decription: CA ShipTo Street
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CASStrt
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CASStrt = "U_CASStrt";
		public string U_CASStrt { 
			get {
 				return getValueAsString(_CASStrt); 
			}
			set { setValue(_CASStrt, value); }
		}
           public string doValidate_CASStrt() {
               return doValidate_CASStrt(U_CASStrt);
           }
           public virtual string doValidate_CASStrt(object oValue) {
               return base.doValidation(_CASStrt, oValue);
           }

		/**
		 * Decription: DS Street
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSStreet
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CAStrt = "U_CAStrt";
		public string U_CAStrt { 
			get {
 				return getValueAsString(_CAStrt); 
			}
			set { setValue(_CAStrt, value); }
		}
           public string doValidate_CAStrt() {
               return doValidate_CAStrt(U_CAStrt);
           }
           public virtual string doValidate_CAStrt(object oValue) {
               return base.doValidation(_CAStrt, oValue);
           }

		/**
		 * Decription: Created By
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CreatedBy
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CreatedBy = "U_CreatedBy";
		public string U_CreatedBy { 
			get {
 				return getValueAsString(_CreatedBy); 
			}
			set { setValue(_CreatedBy, value); }
		}
           public string doValidate_CreatedBy() {
               return doValidate_CreatedBy(U_CreatedBy);
           }
           public virtual string doValidate_CreatedBy(object oValue) {
               return base.doValidation(_CreatedBy, oValue);
           }

		/**
		 * Decription: Create Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CreateDt
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 12/30/1899 12:00:00 AM
		 * Identity: False
		 */
		public readonly static string _CreateDt = "U_CreateDt";
		public DateTime U_CreateDt { 
			get {
 				return getValueAsDateTime(_CreateDt); 
			}
			set { setValue(_CreateDt, value); }
		}
		public void U_CreateDt_AsString(string value){
			setValue("U_CreateDt", DateTime.Parse(value));
			}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_CreateDt_AsString(){
			DateTime dVal = getValueAsDateTime(_CreateDt);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_CreateDt() {
               return doValidate_CreateDt(U_CreateDt);
           }
           public virtual string doValidate_CreateDt(object oValue) {
               return base.doValidation(_CreateDt, oValue);
           }

		/**
		 * Decription: CU Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CusAddr
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CusAddr = "U_CusAddr";
		public string U_CusAddr { 
			get {
 				return getValueAsString(_CusAddr); 
			}
			set { setValue(_CusAddr, value); }
		}
           public string doValidate_CusAddr() {
               return doValidate_CusAddr(U_CusAddr);
           }
           public virtual string doValidate_CusAddr(object oValue) {
               return base.doValidation(_CusAddr, oValue);
           }

		/**
		 * Decription: CU Block
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CusBlock
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CusBlock = "U_CusBlock";
		public string U_CusBlock { 
			get {
 				return getValueAsString(_CusBlock); 
			}
			set { setValue(_CusBlock, value); }
		}
           public string doValidate_CusBlock() {
               return doValidate_CusBlock(U_CusBlock);
           }
           public virtual string doValidate_CusBlock(object oValue) {
               return base.doValidation(_CusBlock, oValue);
           }

		/**
		 * Decription: Customer Cd
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CusCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CusCd = "U_CusCd";
		public string U_CusCd { 
			get {
 				return getValueAsString(_CusCd); 
			}
			set { setValue(_CusCd, value); }
		}
           public string doValidate_CusCd() {
               return doValidate_CusCd(U_CusCd);
           }
           public virtual string doValidate_CusCd(object oValue) {
               return base.doValidation(_CusCd, oValue);
           }

		/**
		 * Decription: CU City
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CusCity
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CusCity = "U_CusCity";
		public string U_CusCity { 
			get {
 				return getValueAsString(_CusCity); 
			}
			set { setValue(_CusCity, value); }
		}
           public string doValidate_CusCity() {
               return doValidate_CusCity(U_CusCity);
           }
           public virtual string doValidate_CusCity(object oValue) {
               return base.doValidation(_CusCity, oValue);
           }

		/**
		 * Decription: CU ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CusID
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CusID = "U_CusID";
		public string U_CusID { 
			get {
 				return getValueAsString(_CusID); 
			}
			set { setValue(_CusID, value); }
		}
           public string doValidate_CusID() {
               return doValidate_CusID(U_CusID);
           }
           public virtual string doValidate_CusID(object oValue) {
               return base.doValidation(_CusID, oValue);
           }

		/**
		 * Decription: Customer Nm
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CusNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CusNm = "U_CusNm";
		public string U_CusNm { 
			get {
 				return getValueAsString(_CusNm); 
			}
			set { setValue(_CusNm, value); }
		}
           public string doValidate_CusNm() {
               return doValidate_CusNm(U_CusNm);
           }
           public virtual string doValidate_CusNm(object oValue) {
               return base.doValidation(_CusNm, oValue);
           }

		/**
		 * Decription: CU Post Cd
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CusPostCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CusPostCd = "U_CusPostCd";
		public string U_CusPostCd { 
			get {
 				return getValueAsString(_CusPostCd); 
			}
			set { setValue(_CusPostCd, value); }
		}
           public string doValidate_CusPostCd() {
               return doValidate_CusPostCd(U_CusPostCd);
           }
           public virtual string doValidate_CusPostCd(object oValue) {
               return base.doValidation(_CusPostCd, oValue);
           }

		/**
		 * Decription: CU ShipTo Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CusSAddr
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CusSAddr = "U_CusSAddr";
		public string U_CusSAddr { 
			get {
 				return getValueAsString(_CusSAddr); 
			}
			set { setValue(_CusSAddr, value); }
		}
           public string doValidate_CusSAddr() {
               return doValidate_CusSAddr(U_CusSAddr);
           }
           public virtual string doValidate_CusSAddr(object oValue) {
               return base.doValidation(_CusSAddr, oValue);
           }

		/**
		 * Decription: CU ShipTo Block
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CusSBlock
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CusSBlock = "U_CusSBlock";
		public string U_CusSBlock { 
			get {
 				return getValueAsString(_CusSBlock); 
			}
			set { setValue(_CusSBlock, value); }
		}
           public string doValidate_CusSBlock() {
               return doValidate_CusSBlock(U_CusSBlock);
           }
           public virtual string doValidate_CusSBlock(object oValue) {
               return base.doValidation(_CusSBlock, oValue);
           }

		/**
		 * Decription: CU ShipTo City
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CusSCity
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CusSCity = "U_CusSCity";
		public string U_CusSCity { 
			get {
 				return getValueAsString(_CusSCity); 
			}
			set { setValue(_CusSCity, value); }
		}
           public string doValidate_CusSCity() {
               return doValidate_CusSCity(U_CusSCity);
           }
           public virtual string doValidate_CusSCity(object oValue) {
               return base.doValidation(_CusSCity, oValue);
           }

		/**
		 * Decription: CU ShipTo Post Cd
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CusSPostCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CusSPostCd = "U_CusSPostCd";
		public string U_CusSPostCd { 
			get {
 				return getValueAsString(_CusSPostCd); 
			}
			set { setValue(_CusSPostCd, value); }
		}
           public string doValidate_CusSPostCd() {
               return doValidate_CusSPostCd(U_CusSPostCd);
           }
           public virtual string doValidate_CusSPostCd(object oValue) {
               return base.doValidation(_CusSPostCd, oValue);
           }

		/**
		 * Decription: CU ShipTo Street
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CusSStrt
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CusSStrt = "U_CusSStrt";
		public string U_CusSStrt { 
			get {
 				return getValueAsString(_CusSStrt); 
			}
			set { setValue(_CusSStrt, value); }
		}
           public string doValidate_CusSStrt() {
               return doValidate_CusSStrt(U_CusSStrt);
           }
           public virtual string doValidate_CusSStrt(object oValue) {
               return base.doValidation(_CusSStrt, oValue);
           }

		/**
		 * Decription: CU Street
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CusStreet
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CusStreet = "U_CusStreet";
		public string U_CusStreet { 
			get {
 				return getValueAsString(_CusStreet); 
			}
			set { setValue(_CusStreet, value); }
		}
           public string doValidate_CusStreet() {
               return doValidate_CusStreet(U_CusStreet);
           }
           public virtual string doValidate_CusStreet(object oValue) {
               return base.doValidation(_CusStreet, oValue);
           }

		/**
		 * Decription: Declaration Attached YN
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DecAtt
		 * Size: 3
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DecAtt = "U_DecAtt";
		public string U_DecAtt { 
			get {
 				return getValueAsString(_DecAtt); 
			}
			set { setValue(_DecAtt, value); }
		}
           public string doValidate_DecAtt() {
               return doValidate_DecAtt(U_DecAtt);
           }
           public virtual string doValidate_DecAtt(object oValue) {
               return base.doValidation(_DecAtt, oValue);
           }

		/**
		 * Decription: Disposal Site Cd
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DispSiteCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DispSiteCd = "U_DispSiteCd";
		public string U_DispSiteCd { 
			get {
 				return getValueAsString(_DispSiteCd); 
			}
			set { setValue(_DispSiteCd, value); }
		}
           public string doValidate_DispSiteCd() {
               return doValidate_DispSiteCd(U_DispSiteCd);
           }
           public virtual string doValidate_DispSiteCd(object oValue) {
               return base.doValidation(_DispSiteCd, oValue);
           }

		/**
		 * Decription: Disposal Site Nm
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DispSiteNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DispSiteNm = "U_DispSiteNm";
		public string U_DispSiteNm { 
			get {
 				return getValueAsString(_DispSiteNm); 
			}
			set { setValue(_DispSiteNm, value); }
		}
           public string doValidate_DispSiteNm() {
               return doValidate_DispSiteNm(U_DispSiteNm);
           }
           public virtual string doValidate_DispSiteNm(object oValue) {
               return base.doValidation(_DispSiteNm, oValue);
           }

		/**
		 * Decription: DS Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSAddress
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DSAddress = "U_DSAddress";
		public string U_DSAddress { 
			get {
 				return getValueAsString(_DSAddress); 
			}
			set { setValue(_DSAddress, value); }
		}
           public string doValidate_DSAddress() {
               return doValidate_DSAddress(U_DSAddress);
           }
           public virtual string doValidate_DSAddress(object oValue) {
               return base.doValidation(_DSAddress, oValue);
           }

		/**
		 * Decription: DS Block
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSBlock
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DSBlock = "U_DSBlock";
		public string U_DSBlock { 
			get {
 				return getValueAsString(_DSBlock); 
			}
			set { setValue(_DSBlock, value); }
		}
           public string doValidate_DSBlock() {
               return doValidate_DSBlock(U_DSBlock);
           }
           public virtual string doValidate_DSBlock(object oValue) {
               return base.doValidation(_DSBlock, oValue);
           }

		/**
		 * Decription: DS City
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSCity
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DSCity = "U_DSCity";
		public string U_DSCity { 
			get {
 				return getValueAsString(_DSCity); 
			}
			set { setValue(_DSCity, value); }
		}
           public string doValidate_DSCity() {
               return doValidate_DSCity(U_DSCity);
           }
           public virtual string doValidate_DSCity(object oValue) {
               return base.doValidation(_DSCity, oValue);
           }

		/**
		 * Decription: DS Freistellungsnummer
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSPFreistellu
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DSPFreistellu = "U_DSPFreistellu";
		public string U_DSPFreistellu { 
			get {
 				return getValueAsString(_DSPFreistellu); 
			}
			set { setValue(_DSPFreistellu, value); }
		}
           public string doValidate_DSPFreistellu() {
               return doValidate_DSPFreistellu(U_DSPFreistellu);
           }
           public virtual string doValidate_DSPFreistellu(object oValue) {
               return base.doValidation(_DSPFreistellu, oValue);
           }

		/**
		 * Decription: DS Post Cd
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSPostCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DSPostCd = "U_DSPostCd";
		public string U_DSPostCd { 
			get {
 				return getValueAsString(_DSPostCd); 
			}
			set { setValue(_DSPostCd, value); }
		}
           public string doValidate_DSPostCd() {
               return doValidate_DSPostCd(U_DSPostCd);
           }
           public virtual string doValidate_DSPostCd(object oValue) {
               return base.doValidation(_DSPostCd, oValue);
           }

		/**
		 * Decription: DS ShipTo Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSPSAddr
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DSPSAddr = "U_DSPSAddr";
		public string U_DSPSAddr { 
			get {
 				return getValueAsString(_DSPSAddr); 
			}
			set { setValue(_DSPSAddr, value); }
		}
           public string doValidate_DSPSAddr() {
               return doValidate_DSPSAddr(U_DSPSAddr);
           }
           public virtual string doValidate_DSPSAddr(object oValue) {
               return base.doValidation(_DSPSAddr, oValue);
           }

		/**
		 * Decription: DS ShipTo Block
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSPSBlock
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DSPSBlock = "U_DSPSBlock";
		public string U_DSPSBlock { 
			get {
 				return getValueAsString(_DSPSBlock); 
			}
			set { setValue(_DSPSBlock, value); }
		}
           public string doValidate_DSPSBlock() {
               return doValidate_DSPSBlock(U_DSPSBlock);
           }
           public virtual string doValidate_DSPSBlock(object oValue) {
               return base.doValidation(_DSPSBlock, oValue);
           }

		/**
		 * Decription: DS ShipTo City
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSPSCity
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DSPSCity = "U_DSPSCity";
		public string U_DSPSCity { 
			get {
 				return getValueAsString(_DSPSCity); 
			}
			set { setValue(_DSPSCity, value); }
		}
           public string doValidate_DSPSCity() {
               return doValidate_DSPSCity(U_DSPSCity);
           }
           public virtual string doValidate_DSPSCity(object oValue) {
               return base.doValidation(_DSPSCity, oValue);
           }

		/**
		 * Decription: DS ShipTo Post Cd
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSPSPostCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DSPSPostCd = "U_DSPSPostCd";
		public string U_DSPSPostCd { 
			get {
 				return getValueAsString(_DSPSPostCd); 
			}
			set { setValue(_DSPSPostCd, value); }
		}
           public string doValidate_DSPSPostCd() {
               return doValidate_DSPSPostCd(U_DSPSPostCd);
           }
           public virtual string doValidate_DSPSPostCd(object oValue) {
               return base.doValidation(_DSPSPostCd, oValue);
           }

		/**
		 * Decription: DS ShipTo Site ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSPSSID
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DSPSSID = "U_DSPSSID";
		public string U_DSPSSID { 
			get {
 				return getValueAsString(_DSPSSID); 
			}
			set { setValue(_DSPSSID, value); }
		}
           public string doValidate_DSPSSID() {
               return doValidate_DSPSSID(U_DSPSSID);
           }
           public virtual string doValidate_DSPSSID(object oValue) {
               return base.doValidation(_DSPSSID, oValue);
           }

		/**
		 * Decription: DS ShipTo Street
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSPSStrt
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DSPSStrt = "U_DSPSStrt";
		public string U_DSPSStrt { 
			get {
 				return getValueAsString(_DSPSStrt); 
			}
			set { setValue(_DSPSStrt, value); }
		}
           public string doValidate_DSPSStrt() {
               return doValidate_DSPSStrt(U_DSPSStrt);
           }
           public virtual string doValidate_DSPSStrt(object oValue) {
               return base.doValidation(_DSPSStrt, oValue);
           }

		/**
		 * Decription: DS Street
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSStreet
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DSStreet = "U_DSStreet";
		public string U_DSStreet { 
			get {
 				return getValueAsString(_DSStreet); 
			}
			set { setValue(_DSStreet, value); }
		}
           public string doValidate_DSStreet() {
               return doValidate_DSStreet(U_DSStreet);
           }
           public virtual string doValidate_DSStreet(object oValue) {
               return base.doValidation(_DSStreet, oValue);
           }

		/**
		 * Decription: Duration
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Duration
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Duration = "U_Duration";
		public short U_Duration { 
			get {
 				return getValueAsShort(_Duration); 
			}
			set { setValue(_Duration, value); }
		}
           public string doValidate_Duration() {
               return doValidate_Duration(U_Duration);
           }
           public virtual string doValidate_Duration(object oValue) {
               return base.doValidation(_Duration, oValue);
           }

		/**
		 * Decription: End Dt
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_EndDt
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 12/30/1899 12:00:00 AM
		 * Identity: False
		 */
		public readonly static string _EndDt = "U_EndDt";
		public DateTime U_EndDt { 
			get {
 				return getValueAsDateTime(_EndDt); 
			}
			set { setValue(_EndDt, value); }
		}
		public void U_EndDt_AsString(string value){
			setValue("U_EndDt", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_EndDt_AsString(){
			DateTime dVal = getValueAsDateTime(_EndDt);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_EndDt() {
               return doValidate_EndDt(U_EndDt);
           }
           public virtual string doValidate_EndDt(object oValue) {
               return base.doValidation(_EndDt, oValue);
           }

		/**
		 * Decription: Duration
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Duration
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _EnVnSnVs = "U_EnVnSnVs";
		public string U_EnVnSnVs { 
			get {
 				return getValueAsString(_EnVnSnVs); 
			}
			set { setValue(_EnVnSnVs, value); }
		}
           public string doValidate_EnVnSnVs() {
               return doValidate_EnVnSnVs(U_EnVnSnVs);
           }
           public virtual string doValidate_EnVnSnVs(object oValue) {
               return base.doValidation(_EnVnSnVs, oValue);
           }

		/**
		 * Decription: EVN Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_EVNNumber
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _EVNNumber = "U_EVNNumber";
		public string U_EVNNumber { 
			get {
 				return getValueAsString(_EVNNumber); 
			}
			set { setValue(_EVNNumber, value); }
		}
           public string doValidate_EVNNumber() {
               return doValidate_EVNNumber(U_EVNNumber);
           }
           public virtual string doValidate_EVNNumber(object oValue) {
               return base.doValidation(_EVNNumber, oValue);
           }

		/**
		 * Decription: EWC Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_EWCCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _EWCCd = "U_EWCCd";
		public string U_EWCCd { 
			get {
 				return getValueAsString(_EWCCd); 
			}
			set { setValue(_EWCCd, value); }
		}
           public string doValidate_EWCCd() {
               return doValidate_EWCCd(U_EWCCd);
           }
           public virtual string doValidate_EWCCd(object oValue) {
               return base.doValidation(_EWCCd, oValue);
           }

		/**
		 * Decription: EWC Description
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_EWCDsc
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _EWCDsc = "U_EWCDsc";
		public string U_EWCDsc { 
			get {
 				return getValueAsString(_EWCDsc); 
			}
			set { setValue(_EWCDsc, value); }
		}
           public string doValidate_EWCDsc() {
               return doValidate_EWCDsc(U_EWCDsc);
           }
           public virtual string doValidate_EWCDsc(object oValue) {
               return base.doValidation(_EWCDsc, oValue);
           }

		/**
		 * Decription: Is Printed
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IsPrinted
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _IsPrinted = "U_IsPrinted";
		public string U_IsPrinted { 
			get {
 				return getValueAsString(_IsPrinted); 
			}
			set { setValue(_IsPrinted, value); }
		}
           public string doValidate_IsPrinted() {
               return doValidate_IsPrinted(U_IsPrinted);
           }
           public virtual string doValidate_IsPrinted(object oValue) {
               return base.doValidation(_IsPrinted, oValue);
           }

		/**
		 * Decription: PreTreated Waste YN
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PreTre
		 * Size: 3
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PreTre = "U_PreTre";
		public string U_PreTre { 
			get {
 				return getValueAsString(_PreTre); 
			}
			set { setValue(_PreTre, value); }
		}
           public string doValidate_PreTre() {
               return doValidate_PreTre(U_PreTre);
           }
           public virtual string doValidate_PreTre(object oValue) {
               return base.doValidation(_PreTre, oValue);
           }

		/**
		 * Decription: Quantity
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Quantity
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Quantity = "U_Quantity";
		public short U_Quantity { 
			get {
 				return getValueAsShort(_Quantity); 
			}
			set { setValue(_Quantity, value); }
		}
           public string doValidate_Quantity() {
               return doValidate_Quantity(U_Quantity);
           }
           public virtual string doValidate_Quantity(object oValue) {
               return base.doValidation(_Quantity, oValue);
           }

		/**
		 * Decription: Release Number YN
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RelNum
		 * Size: 3
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RelNum = "U_RelNum";
		public string U_RelNum { 
			get {
 				return getValueAsString(_RelNum); 
			}
			set { setValue(_RelNum, value); }
		}
           public string doValidate_RelNum() {
               return doValidate_RelNum(U_RelNum);
           }
           public virtual string doValidate_RelNum(object oValue) {
               return base.doValidation(_RelNum, oValue);
           }

		/**
		 * Decription: Single or Multiple
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SinMul
		 * Size: 3
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SinMul = "U_SinMul";
		public string U_SinMul { 
			get {
 				return getValueAsString(_SinMul); 
			}
			set { setValue(_SinMul, value); }
		}
           public string doValidate_SinMul() {
               return doValidate_SinMul(U_SinMul);
           }
           public virtual string doValidate_SinMul(object oValue) {
               return base.doValidation(_SinMul, oValue);
           }

		/**
		 * Decription: Start Dt
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_StartDt
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _StartDt = "U_StartDt";
		public DateTime U_StartDt { 
			get {
 				return getValueAsDateTime(_StartDt); 
			}
			set { setValue(_StartDt, value); }
		}
		public void U_StartDt_AsString(string value){
			setValue("U_StartDt", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_StartDt_AsString(){
			DateTime dVal = getValueAsDateTime(_StartDt);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_StartDt() {
               return doValidate_StartDt(U_StartDt);
           }
           public virtual string doValidate_StartDt(object oValue) {
               return base.doValidation(_StartDt, oValue);
           }

		/**
		 * Decription: UOM
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UOM
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _UOM = "U_UOM";
		public string U_UOM { 
			get {
 				return getValueAsString(_UOM); 
			}
			set { setValue(_UOM, value); }
		}
           public string doValidate_UOM() {
               return doValidate_UOM(U_UOM);
           }
           public virtual string doValidate_UOM(object oValue) {
               return base.doValidation(_UOM, oValue);
           }

		/**
		 * Decription: Updated By
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UpdatedBy
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _UpdatedBy = "U_UpdatedBy";
		public string U_UpdatedBy { 
			get {
 				return getValueAsString(_UpdatedBy); 
			}
			set { setValue(_UpdatedBy, value); }
		}
           public string doValidate_UpdatedBy() {
               return doValidate_UpdatedBy(U_UpdatedBy);
           }
           public virtual string doValidate_UpdatedBy(object oValue) {
               return base.doValidation(_UpdatedBy, oValue);
           }

		/**
		 * Decription: Update Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UpdateDt
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _UpdateDt = "U_UpdateDt";
		public DateTime U_UpdateDt { 
			get {
 				return getValueAsDateTime(_UpdateDt); 
			}
			set { setValue(_UpdateDt, value); }
		}
		public void U_UpdateDt_AsString(string value){
			setValue("U_UpdateDt", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_UpdateDt_AsString(){
			DateTime dVal = getValueAsDateTime(_UpdateDt);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_UpdateDt() {
               return doValidate_UpdateDt(U_UpdateDt);
           }
           public virtual string doValidate_UpdateDt(object oValue) {
               return base.doValidation(_UpdateDt, oValue);
           }

		/**
		 * Decription: Waste Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WasCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WasCd = "U_WasCd";
		public string U_WasCd { 
			get {
 				return getValueAsString(_WasCd); 
			}
			set { setValue(_WasCd, value); }
		}
           public string doValidate_WasCd() {
               return doValidate_WasCd(U_WasCd);
           }
           public virtual string doValidate_WasCd(object oValue) {
               return base.doValidation(_WasCd, oValue);
           }

		/**
		 * Decription: Waste Description
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WasDsc
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WasDsc = "U_WasDsc";
		public string U_WasDsc { 
			get {
 				return getValueAsString(_WasDsc); 
			}
			set { setValue(_WasDsc, value); }
		}
           public string doValidate_WasDsc() {
               return doValidate_WasDsc(U_WasDsc);
           }
           public virtual string doValidate_WasDsc(object oValue) {
               return base.doValidation(_WasDsc, oValue);
           }

		/**
		 * Decription: Year1
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Year1
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Year1 = "U_Year1";
		public short U_Year1 { 
			get {
 				return getValueAsShort(_Year1); 
			}
			set { setValue(_Year1, value); }
		}
           public string doValidate_Year1() {
               return doValidate_Year1(U_Year1);
           }
           public virtual string doValidate_Year1(object oValue) {
               return base.doValidation(_Year1, oValue);
           }

		/**
		 * Decription: Year2
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Year2
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Year2 = "U_Year2";
		public short U_Year2 { 
			get {
 				return getValueAsShort(_Year2); 
			}
			set { setValue(_Year2, value); }
		}
           public string doValidate_Year2() {
               return doValidate_Year2(U_Year2);
           }
           public virtual string doValidate_Year2(object oValue) {
               return base.doValidation(_Year2, oValue);
           }

		/**
		 * Decription: Year3
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Year3
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Year3 = "U_Year3";
		public short U_Year3 { 
			get {
 				return getValueAsShort(_Year3); 
			}
			set { setValue(_Year3, value); }
		}
           public string doValidate_Year3() {
               return doValidate_Year3(U_Year3);
           }
           public virtual string doValidate_Year3(object oValue) {
               return base.doValidation(_Year3, oValue);
           }

		/**
		 * Decription: Year4
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Year4
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Year4 = "U_Year4";
		public short U_Year4 { 
			get {
 				return getValueAsShort(_Year4); 
			}
			set { setValue(_Year4, value); }
		}
           public string doValidate_Year4() {
               return doValidate_Year4(U_Year4);
           }
           public virtual string doValidate_Year4(object oValue) {
               return base.doValidation(_Year4, oValue);
           }

		/**
		 * Decription: Year5
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Year5
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Year5 = "U_Year5";
		public short U_Year5 { 
			get {
 				return getValueAsShort(_Year5); 
			}
			set { setValue(_Year5, value); }
		}
           public string doValidate_Year5() {
               return doValidate_Year5(U_Year5);
           }
           public virtual string doValidate_Year5(object oValue) {
               return base.doValidation(_Year5, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(69);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_Activity, "Activity", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Activity
			moDBFields.Add(_AtcEntry, "Attachment Entry", 3, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Attachment Entry
			moDBFields.Add(_CAAddr, "CA Address", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //CA Address
			moDBFields.Add(_CABlock, "CA Block", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //CA Block
			moDBFields.Add(_CACity, "CA City", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //CA City
			moDBFields.Add(_CAID, "CA ID", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //CA ID
			moDBFields.Add(_CAPostCd, "CA Post Cd", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //CA Post Cd
			moDBFields.Add(_CarCd, "Carrier Cd", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Carrier Cd
			moDBFields.Add(_CarNm, "Carrier Nm", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Carrier Nm
			moDBFields.Add(_CASAddr, "CA ShipTo Address", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //CA ShipTo Address
			moDBFields.Add(_CASBlock, "CA ShipTo Block", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //CA ShipTo Block
			moDBFields.Add(_CASCity, "CA ShipTo City", 13, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //CA ShipTo City
			moDBFields.Add(_CASPostCd, "CA ShipTo Post Cd", 14, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //CA ShipTo Post Cd
			moDBFields.Add(_CASStrt, "CA ShipTo Street", 15, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //CA ShipTo Street
			moDBFields.Add(_CAStrt, "CA Street", 16, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //CA Street
			moDBFields.Add(_CreatedBy, "Created By", 17, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Created By
			moDBFields.Add(_CreateDt, "Create Date", 18, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Create Date
			moDBFields.Add(_CusAddr, "CU Address", 19, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //CU Address
			moDBFields.Add(_CusBlock, "CU Block", 20, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //CU Block
			moDBFields.Add(_CusCd, "Customer Cd", 21, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Customer Cd
			moDBFields.Add(_CusCity, "CU City", 22, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //CU City
			moDBFields.Add(_CusID, "CU ID", 23, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //CU ID
			moDBFields.Add(_CusNm, "Customer Nm", 24, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Customer Nm
			moDBFields.Add(_CusPostCd, "CU Post Cd", 25, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //CU Post Cd
			moDBFields.Add(_CusSAddr, "CU ShipTo Address", 26, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //CU ShipTo Address
			moDBFields.Add(_CusSBlock, "CU ShipTo Block", 27, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //CU ShipTo Block
			moDBFields.Add(_CusSCity, "CU ShipTo City", 28, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //CU ShipTo City
			moDBFields.Add(_CusSPostCd, "CU ShipTo Post Cd", 29, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //CU ShipTo Post Cd
			moDBFields.Add(_CusSStrt, "CU ShipTo Street", 30, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //CU ShipTo Street
			moDBFields.Add(_CusStreet, "CU Street", 31, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //CU Street
			moDBFields.Add(_DecAtt, "Declaration Attached YN", 32, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //Declaration Attached YN
			moDBFields.Add(_DispSiteCd, "Disposal Site Cd", 33, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Disposal Site Cd
			moDBFields.Add(_DispSiteNm, "Disposal Site Nm", 34, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Disposal Site Nm
			moDBFields.Add(_DSAddress, "DS Address", 35, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //DS Address
			moDBFields.Add(_DSBlock, "DS Block", 36, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //DS Block
			moDBFields.Add(_DSCity, "DS City", 37, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //DS City
			moDBFields.Add(_DSPFreistellu, "DS Freistellungsnummer", 38, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //DS Freistellungsnummer
			moDBFields.Add(_DSPostCd, "DS Post Cd", 39, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //DS Post Cd
			moDBFields.Add(_DSPSAddr, "DS ShipTo Address", 40, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //DS ShipTo Address
			moDBFields.Add(_DSPSBlock, "DS ShipTo Block", 41, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //DS ShipTo Block
			moDBFields.Add(_DSPSCity, "DS ShipTo City", 42, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //DS ShipTo City
			moDBFields.Add(_DSPSPostCd, "DS ShipTo Post Cd", 43, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //DS ShipTo Post Cd
			moDBFields.Add(_DSPSSID, "DS ShipTo Site ID", 44, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //DS ShipTo Site ID
			moDBFields.Add(_DSPSStrt, "DS ShipTo Street", 45, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //DS ShipTo Street
			moDBFields.Add(_DSStreet, "DS Street", 46, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //DS Street
			moDBFields.Add(_Duration, "Duration", 47, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Duration
			moDBFields.Add(_EndDt, "End Dt", 48, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //End Dt
			moDBFields.Add(_EnVnSnVs, "EN VN SN VS", 49, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //EN VN SN VS
			moDBFields.Add(_EVNNumber, "EVN Number", 50, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //EVN Number
			moDBFields.Add(_EWCCd, "EWC Code", 51, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //EWC Code
			moDBFields.Add(_EWCDsc, "EWC Description", 52, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //EWC Description
			moDBFields.Add(_IsPrinted, "Is Printed", 53, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Is Printed
			moDBFields.Add(_PreTre, "PreTreated Waste YN", 54, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //PreTreated Waste YN
			moDBFields.Add(_Quantity, "Quantity", 55, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Quantity
			moDBFields.Add(_RelNum, "Release Number YN", 56, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //Release Number YN
			moDBFields.Add(_SinMul, "Single or Multiple", 57, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //Single or Multiple
			moDBFields.Add(_StartDt, "Start Dt", 58, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Start Dt
			moDBFields.Add(_UOM, "UOM", 59, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //UOM
			moDBFields.Add(_UpdatedBy, "Updated By", 60, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Updated By
			moDBFields.Add(_UpdateDt, "Update Date", 61, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Update Date
			moDBFields.Add(_WasCd, "Waste Code", 62, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Waste Code
			moDBFields.Add(_WasDsc, "Waste Description", 63, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Waste Description
			moDBFields.Add(_Year1, "Year1", 64, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Year1
			moDBFields.Add(_Year2, "Year2", 65, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Year2
			moDBFields.Add(_Year3, "Year3", 66, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Year3
			moDBFields.Add(_Year4, "Year4", 67, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Year4
			moDBFields.Add(_Year5, "Year5", 68, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Year5

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_Activity());
            doBuildValidationString(doValidate_AtcEntry());
            doBuildValidationString(doValidate_CAAddr());
            doBuildValidationString(doValidate_CABlock());
            doBuildValidationString(doValidate_CACity());
            doBuildValidationString(doValidate_CAID());
            doBuildValidationString(doValidate_CAPostCd());
            doBuildValidationString(doValidate_CarCd());
            doBuildValidationString(doValidate_CarNm());
            doBuildValidationString(doValidate_CASAddr());
            doBuildValidationString(doValidate_CASBlock());
            doBuildValidationString(doValidate_CASCity());
            doBuildValidationString(doValidate_CASPostCd());
            doBuildValidationString(doValidate_CASStrt());
            doBuildValidationString(doValidate_CAStrt());
            doBuildValidationString(doValidate_CreatedBy());
            doBuildValidationString(doValidate_CreateDt());
            doBuildValidationString(doValidate_CusAddr());
            doBuildValidationString(doValidate_CusBlock());
            doBuildValidationString(doValidate_CusCd());
            doBuildValidationString(doValidate_CusCity());
            doBuildValidationString(doValidate_CusID());
            doBuildValidationString(doValidate_CusNm());
            doBuildValidationString(doValidate_CusPostCd());
            doBuildValidationString(doValidate_CusSAddr());
            doBuildValidationString(doValidate_CusSBlock());
            doBuildValidationString(doValidate_CusSCity());
            doBuildValidationString(doValidate_CusSPostCd());
            doBuildValidationString(doValidate_CusSStrt());
            doBuildValidationString(doValidate_CusStreet());
            doBuildValidationString(doValidate_DecAtt());
            doBuildValidationString(doValidate_DispSiteCd());
            doBuildValidationString(doValidate_DispSiteNm());
            doBuildValidationString(doValidate_DSAddress());
            doBuildValidationString(doValidate_DSBlock());
            doBuildValidationString(doValidate_DSCity());
            doBuildValidationString(doValidate_DSPFreistellu());
            doBuildValidationString(doValidate_DSPostCd());
            doBuildValidationString(doValidate_DSPSAddr());
            doBuildValidationString(doValidate_DSPSBlock());
            doBuildValidationString(doValidate_DSPSCity());
            doBuildValidationString(doValidate_DSPSPostCd());
            doBuildValidationString(doValidate_DSPSSID());
            doBuildValidationString(doValidate_DSPSStrt());
            doBuildValidationString(doValidate_DSStreet());
            doBuildValidationString(doValidate_Duration());
            doBuildValidationString(doValidate_EndDt());
            doBuildValidationString(doValidate_EnVnSnVs());
            doBuildValidationString(doValidate_EVNNumber());
            doBuildValidationString(doValidate_EWCCd());
            doBuildValidationString(doValidate_EWCDsc());
            doBuildValidationString(doValidate_IsPrinted());
            doBuildValidationString(doValidate_PreTre());
            doBuildValidationString(doValidate_Quantity());
            doBuildValidationString(doValidate_RelNum());
            doBuildValidationString(doValidate_SinMul());
            doBuildValidationString(doValidate_StartDt());
            doBuildValidationString(doValidate_UOM());
            doBuildValidationString(doValidate_UpdatedBy());
            doBuildValidationString(doValidate_UpdateDt());
            doBuildValidationString(doValidate_WasCd());
            doBuildValidationString(doValidate_WasDsc());
            doBuildValidationString(doValidate_Year1());
            doBuildValidationString(doValidate_Year2());
            doBuildValidationString(doValidate_Year3());
            doBuildValidationString(doValidate_Year4());
            doBuildValidationString(doValidate_Year5());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_Activity)) return doValidate_Activity(oValue);
            if (sFieldName.Equals(_AtcEntry)) return doValidate_AtcEntry(oValue);
            if (sFieldName.Equals(_CAAddr)) return doValidate_CAAddr(oValue);
            if (sFieldName.Equals(_CABlock)) return doValidate_CABlock(oValue);
            if (sFieldName.Equals(_CACity)) return doValidate_CACity(oValue);
            if (sFieldName.Equals(_CAID)) return doValidate_CAID(oValue);
            if (sFieldName.Equals(_CAPostCd)) return doValidate_CAPostCd(oValue);
            if (sFieldName.Equals(_CarCd)) return doValidate_CarCd(oValue);
            if (sFieldName.Equals(_CarNm)) return doValidate_CarNm(oValue);
            if (sFieldName.Equals(_CASAddr)) return doValidate_CASAddr(oValue);
            if (sFieldName.Equals(_CASBlock)) return doValidate_CASBlock(oValue);
            if (sFieldName.Equals(_CASCity)) return doValidate_CASCity(oValue);
            if (sFieldName.Equals(_CASPostCd)) return doValidate_CASPostCd(oValue);
            if (sFieldName.Equals(_CASStrt)) return doValidate_CASStrt(oValue);
            if (sFieldName.Equals(_CAStrt)) return doValidate_CAStrt(oValue);
            if (sFieldName.Equals(_CreatedBy)) return doValidate_CreatedBy(oValue);
            if (sFieldName.Equals(_CreateDt)) return doValidate_CreateDt(oValue);
            if (sFieldName.Equals(_CusAddr)) return doValidate_CusAddr(oValue);
            if (sFieldName.Equals(_CusBlock)) return doValidate_CusBlock(oValue);
            if (sFieldName.Equals(_CusCd)) return doValidate_CusCd(oValue);
            if (sFieldName.Equals(_CusCity)) return doValidate_CusCity(oValue);
            if (sFieldName.Equals(_CusID)) return doValidate_CusID(oValue);
            if (sFieldName.Equals(_CusNm)) return doValidate_CusNm(oValue);
            if (sFieldName.Equals(_CusPostCd)) return doValidate_CusPostCd(oValue);
            if (sFieldName.Equals(_CusSAddr)) return doValidate_CusSAddr(oValue);
            if (sFieldName.Equals(_CusSBlock)) return doValidate_CusSBlock(oValue);
            if (sFieldName.Equals(_CusSCity)) return doValidate_CusSCity(oValue);
            if (sFieldName.Equals(_CusSPostCd)) return doValidate_CusSPostCd(oValue);
            if (sFieldName.Equals(_CusSStrt)) return doValidate_CusSStrt(oValue);
            if (sFieldName.Equals(_CusStreet)) return doValidate_CusStreet(oValue);
            if (sFieldName.Equals(_DecAtt)) return doValidate_DecAtt(oValue);
            if (sFieldName.Equals(_DispSiteCd)) return doValidate_DispSiteCd(oValue);
            if (sFieldName.Equals(_DispSiteNm)) return doValidate_DispSiteNm(oValue);
            if (sFieldName.Equals(_DSAddress)) return doValidate_DSAddress(oValue);
            if (sFieldName.Equals(_DSBlock)) return doValidate_DSBlock(oValue);
            if (sFieldName.Equals(_DSCity)) return doValidate_DSCity(oValue);
            if (sFieldName.Equals(_DSPFreistellu)) return doValidate_DSPFreistellu(oValue);
            if (sFieldName.Equals(_DSPostCd)) return doValidate_DSPostCd(oValue);
            if (sFieldName.Equals(_DSPSAddr)) return doValidate_DSPSAddr(oValue);
            if (sFieldName.Equals(_DSPSBlock)) return doValidate_DSPSBlock(oValue);
            if (sFieldName.Equals(_DSPSCity)) return doValidate_DSPSCity(oValue);
            if (sFieldName.Equals(_DSPSPostCd)) return doValidate_DSPSPostCd(oValue);
            if (sFieldName.Equals(_DSPSSID)) return doValidate_DSPSSID(oValue);
            if (sFieldName.Equals(_DSPSStrt)) return doValidate_DSPSStrt(oValue);
            if (sFieldName.Equals(_DSStreet)) return doValidate_DSStreet(oValue);
            if (sFieldName.Equals(_Duration)) return doValidate_Duration(oValue);
            if (sFieldName.Equals(_EndDt)) return doValidate_EndDt(oValue);
            if (sFieldName.Equals(_EnVnSnVs)) return doValidate_EnVnSnVs(oValue);
            if (sFieldName.Equals(_EVNNumber)) return doValidate_EVNNumber(oValue);
            if (sFieldName.Equals(_EWCCd)) return doValidate_EWCCd(oValue);
            if (sFieldName.Equals(_EWCDsc)) return doValidate_EWCDsc(oValue);
            if (sFieldName.Equals(_IsPrinted)) return doValidate_IsPrinted(oValue);
            if (sFieldName.Equals(_PreTre)) return doValidate_PreTre(oValue);
            if (sFieldName.Equals(_Quantity)) return doValidate_Quantity(oValue);
            if (sFieldName.Equals(_RelNum)) return doValidate_RelNum(oValue);
            if (sFieldName.Equals(_SinMul)) return doValidate_SinMul(oValue);
            if (sFieldName.Equals(_StartDt)) return doValidate_StartDt(oValue);
            if (sFieldName.Equals(_UOM)) return doValidate_UOM(oValue);
            if (sFieldName.Equals(_UpdatedBy)) return doValidate_UpdatedBy(oValue);
            if (sFieldName.Equals(_UpdateDt)) return doValidate_UpdateDt(oValue);
            if (sFieldName.Equals(_WasCd)) return doValidate_WasCd(oValue);
            if (sFieldName.Equals(_WasDsc)) return doValidate_WasDsc(oValue);
            if (sFieldName.Equals(_Year1)) return doValidate_Year1(oValue);
            if (sFieldName.Equals(_Year2)) return doValidate_Year2(oValue);
            if (sFieldName.Equals(_Year3)) return doValidate_Year3(oValue);
            if (sFieldName.Equals(_Year4)) return doValidate_Year4(oValue);
            if (sFieldName.Equals(_Year5)) return doValidate_Year5(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_Activity Field to the Form Item.
		 */
		public void doLink_Activity(string sControlName){
			moLinker.doLinkDataToControl(_Activity, sControlName);
		}
		/**
		 * Link the U_AtcEntry Field to the Form Item.
		 */
		public void doLink_AtcEntry(string sControlName){
			moLinker.doLinkDataToControl(_AtcEntry, sControlName);
		}
		/**
		 * Link the U_CAAddr Field to the Form Item.
		 */
		public void doLink_CAAddr(string sControlName){
			moLinker.doLinkDataToControl(_CAAddr, sControlName);
		}
		/**
		 * Link the U_CABlock Field to the Form Item.
		 */
		public void doLink_CABlock(string sControlName){
			moLinker.doLinkDataToControl(_CABlock, sControlName);
		}
		/**
		 * Link the U_CACity Field to the Form Item.
		 */
		public void doLink_CACity(string sControlName){
			moLinker.doLinkDataToControl(_CACity, sControlName);
		}
		/**
		 * Link the U_CAID Field to the Form Item.
		 */
		public void doLink_CAID(string sControlName){
			moLinker.doLinkDataToControl(_CAID, sControlName);
		}
		/**
		 * Link the U_CAPostCd Field to the Form Item.
		 */
		public void doLink_CAPostCd(string sControlName){
			moLinker.doLinkDataToControl(_CAPostCd, sControlName);
		}
		/**
		 * Link the U_CarCd Field to the Form Item.
		 */
		public void doLink_CarCd(string sControlName){
			moLinker.doLinkDataToControl(_CarCd, sControlName);
		}
		/**
		 * Link the U_CarNm Field to the Form Item.
		 */
		public void doLink_CarNm(string sControlName){
			moLinker.doLinkDataToControl(_CarNm, sControlName);
		}
		/**
		 * Link the U_CASAddr Field to the Form Item.
		 */
		public void doLink_CASAddr(string sControlName){
			moLinker.doLinkDataToControl(_CASAddr, sControlName);
		}
		/**
		 * Link the U_CASBlock Field to the Form Item.
		 */
		public void doLink_CASBlock(string sControlName){
			moLinker.doLinkDataToControl(_CASBlock, sControlName);
		}
		/**
		 * Link the U_CASCity Field to the Form Item.
		 */
		public void doLink_CASCity(string sControlName){
			moLinker.doLinkDataToControl(_CASCity, sControlName);
		}
		/**
		 * Link the U_CASPostCd Field to the Form Item.
		 */
		public void doLink_CASPostCd(string sControlName){
			moLinker.doLinkDataToControl(_CASPostCd, sControlName);
		}
		/**
		 * Link the U_CASStrt Field to the Form Item.
		 */
		public void doLink_CASStrt(string sControlName){
			moLinker.doLinkDataToControl(_CASStrt, sControlName);
		}
		/**
		 * Link the U_CAStrt Field to the Form Item.
		 */
		public void doLink_CAStrt(string sControlName){
			moLinker.doLinkDataToControl(_CAStrt, sControlName);
		}
		/**
		 * Link the U_CreatedBy Field to the Form Item.
		 */
		public void doLink_CreatedBy(string sControlName){
			moLinker.doLinkDataToControl(_CreatedBy, sControlName);
		}
		/**
		 * Link the U_CreateDt Field to the Form Item.
		 */
		public void doLink_CreateDt(string sControlName){
			moLinker.doLinkDataToControl(_CreateDt, sControlName);
		}
		/**
		 * Link the U_CusAddr Field to the Form Item.
		 */
		public void doLink_CusAddr(string sControlName){
			moLinker.doLinkDataToControl(_CusAddr, sControlName);
		}
		/**
		 * Link the U_CusBlock Field to the Form Item.
		 */
		public void doLink_CusBlock(string sControlName){
			moLinker.doLinkDataToControl(_CusBlock, sControlName);
		}
		/**
		 * Link the U_CusCd Field to the Form Item.
		 */
		public void doLink_CusCd(string sControlName){
			moLinker.doLinkDataToControl(_CusCd, sControlName);
		}
		/**
		 * Link the U_CusCity Field to the Form Item.
		 */
		public void doLink_CusCity(string sControlName){
			moLinker.doLinkDataToControl(_CusCity, sControlName);
		}
		/**
		 * Link the U_CusID Field to the Form Item.
		 */
		public void doLink_CusID(string sControlName){
			moLinker.doLinkDataToControl(_CusID, sControlName);
		}
		/**
		 * Link the U_CusNm Field to the Form Item.
		 */
		public void doLink_CusNm(string sControlName){
			moLinker.doLinkDataToControl(_CusNm, sControlName);
		}
		/**
		 * Link the U_CusPostCd Field to the Form Item.
		 */
		public void doLink_CusPostCd(string sControlName){
			moLinker.doLinkDataToControl(_CusPostCd, sControlName);
		}
		/**
		 * Link the U_CusSAddr Field to the Form Item.
		 */
		public void doLink_CusSAddr(string sControlName){
			moLinker.doLinkDataToControl(_CusSAddr, sControlName);
		}
		/**
		 * Link the U_CusSBlock Field to the Form Item.
		 */
		public void doLink_CusSBlock(string sControlName){
			moLinker.doLinkDataToControl(_CusSBlock, sControlName);
		}
		/**
		 * Link the U_CusSCity Field to the Form Item.
		 */
		public void doLink_CusSCity(string sControlName){
			moLinker.doLinkDataToControl(_CusSCity, sControlName);
		}
		/**
		 * Link the U_CusSPostCd Field to the Form Item.
		 */
		public void doLink_CusSPostCd(string sControlName){
			moLinker.doLinkDataToControl(_CusSPostCd, sControlName);
		}
		/**
		 * Link the U_CusSStrt Field to the Form Item.
		 */
		public void doLink_CusSStrt(string sControlName){
			moLinker.doLinkDataToControl(_CusSStrt, sControlName);
		}
		/**
		 * Link the U_CusStreet Field to the Form Item.
		 */
		public void doLink_CusStreet(string sControlName){
			moLinker.doLinkDataToControl(_CusStreet, sControlName);
		}
		/**
		 * Link the U_DecAtt Field to the Form Item.
		 */
		public void doLink_DecAtt(string sControlName){
			moLinker.doLinkDataToControl(_DecAtt, sControlName);
		}
		/**
		 * Link the U_DispSiteCd Field to the Form Item.
		 */
		public void doLink_DispSiteCd(string sControlName){
			moLinker.doLinkDataToControl(_DispSiteCd, sControlName);
		}
		/**
		 * Link the U_DispSiteNm Field to the Form Item.
		 */
		public void doLink_DispSiteNm(string sControlName){
			moLinker.doLinkDataToControl(_DispSiteNm, sControlName);
		}
		/**
		 * Link the U_DSAddress Field to the Form Item.
		 */
		public void doLink_DSAddress(string sControlName){
			moLinker.doLinkDataToControl(_DSAddress, sControlName);
		}
		/**
		 * Link the U_DSBlock Field to the Form Item.
		 */
		public void doLink_DSBlock(string sControlName){
			moLinker.doLinkDataToControl(_DSBlock, sControlName);
		}
		/**
		 * Link the U_DSCity Field to the Form Item.
		 */
		public void doLink_DSCity(string sControlName){
			moLinker.doLinkDataToControl(_DSCity, sControlName);
		}
		/**
		 * Link the U_DSPFreistellu Field to the Form Item.
		 */
		public void doLink_DSPFreistellu(string sControlName){
			moLinker.doLinkDataToControl(_DSPFreistellu, sControlName);
		}
		/**
		 * Link the U_DSPostCd Field to the Form Item.
		 */
		public void doLink_DSPostCd(string sControlName){
			moLinker.doLinkDataToControl(_DSPostCd, sControlName);
		}
		/**
		 * Link the U_DSPSAddr Field to the Form Item.
		 */
		public void doLink_DSPSAddr(string sControlName){
			moLinker.doLinkDataToControl(_DSPSAddr, sControlName);
		}
		/**
		 * Link the U_DSPSBlock Field to the Form Item.
		 */
		public void doLink_DSPSBlock(string sControlName){
			moLinker.doLinkDataToControl(_DSPSBlock, sControlName);
		}
		/**
		 * Link the U_DSPSCity Field to the Form Item.
		 */
		public void doLink_DSPSCity(string sControlName){
			moLinker.doLinkDataToControl(_DSPSCity, sControlName);
		}
		/**
		 * Link the U_DSPSPostCd Field to the Form Item.
		 */
		public void doLink_DSPSPostCd(string sControlName){
			moLinker.doLinkDataToControl(_DSPSPostCd, sControlName);
		}
		/**
		 * Link the U_DSPSSID Field to the Form Item.
		 */
		public void doLink_DSPSSID(string sControlName){
			moLinker.doLinkDataToControl(_DSPSSID, sControlName);
		}
		/**
		 * Link the U_DSPSStrt Field to the Form Item.
		 */
		public void doLink_DSPSStrt(string sControlName){
			moLinker.doLinkDataToControl(_DSPSStrt, sControlName);
		}
		/**
		 * Link the U_DSStreet Field to the Form Item.
		 */
		public void doLink_DSStreet(string sControlName){
			moLinker.doLinkDataToControl(_DSStreet, sControlName);
		}
		/**
		 * Link the U_Duration Field to the Form Item.
		 */
		public void doLink_Duration(string sControlName){
			moLinker.doLinkDataToControl(_Duration, sControlName);
		}
		/**
		 * Link the U_EndDt Field to the Form Item.
		 */
		public void doLink_EndDt(string sControlName){
			moLinker.doLinkDataToControl(_EndDt, sControlName);
		}
		/**
		 * Link the U_EnVnSnVs Field to the Form Item.
		 */
		public void doLink_EnVnSnVs(string sControlName){
			moLinker.doLinkDataToControl(_EnVnSnVs, sControlName);
		}
		/**
		 * Link the U_EVNNumber Field to the Form Item.
		 */
		public void doLink_EVNNumber(string sControlName){
			moLinker.doLinkDataToControl(_EVNNumber, sControlName);
		}
		/**
		 * Link the U_EWCCd Field to the Form Item.
		 */
		public void doLink_EWCCd(string sControlName){
			moLinker.doLinkDataToControl(_EWCCd, sControlName);
		}
		/**
		 * Link the U_EWCDsc Field to the Form Item.
		 */
		public void doLink_EWCDsc(string sControlName){
			moLinker.doLinkDataToControl(_EWCDsc, sControlName);
		}
		/**
		 * Link the U_IsPrinted Field to the Form Item.
		 */
		public void doLink_IsPrinted(string sControlName){
			moLinker.doLinkDataToControl(_IsPrinted, sControlName);
		}
		/**
		 * Link the U_PreTre Field to the Form Item.
		 */
		public void doLink_PreTre(string sControlName){
			moLinker.doLinkDataToControl(_PreTre, sControlName);
		}
		/**
		 * Link the U_Quantity Field to the Form Item.
		 */
		public void doLink_Quantity(string sControlName){
			moLinker.doLinkDataToControl(_Quantity, sControlName);
		}
		/**
		 * Link the U_RelNum Field to the Form Item.
		 */
		public void doLink_RelNum(string sControlName){
			moLinker.doLinkDataToControl(_RelNum, sControlName);
		}
		/**
		 * Link the U_SinMul Field to the Form Item.
		 */
		public void doLink_SinMul(string sControlName){
			moLinker.doLinkDataToControl(_SinMul, sControlName);
		}
		/**
		 * Link the U_StartDt Field to the Form Item.
		 */
		public void doLink_StartDt(string sControlName){
			moLinker.doLinkDataToControl(_StartDt, sControlName);
		}
		/**
		 * Link the U_UOM Field to the Form Item.
		 */
		public void doLink_UOM(string sControlName){
			moLinker.doLinkDataToControl(_UOM, sControlName);
		}
		/**
		 * Link the U_UpdatedBy Field to the Form Item.
		 */
		public void doLink_UpdatedBy(string sControlName){
			moLinker.doLinkDataToControl(_UpdatedBy, sControlName);
		}
		/**
		 * Link the U_UpdateDt Field to the Form Item.
		 */
		public void doLink_UpdateDt(string sControlName){
			moLinker.doLinkDataToControl(_UpdateDt, sControlName);
		}
		/**
		 * Link the U_WasCd Field to the Form Item.
		 */
		public void doLink_WasCd(string sControlName){
			moLinker.doLinkDataToControl(_WasCd, sControlName);
		}
		/**
		 * Link the U_WasDsc Field to the Form Item.
		 */
		public void doLink_WasDsc(string sControlName){
			moLinker.doLinkDataToControl(_WasDsc, sControlName);
		}
		/**
		 * Link the U_Year1 Field to the Form Item.
		 */
		public void doLink_Year1(string sControlName){
			moLinker.doLinkDataToControl(_Year1, sControlName);
		}
		/**
		 * Link the U_Year2 Field to the Form Item.
		 */
		public void doLink_Year2(string sControlName){
			moLinker.doLinkDataToControl(_Year2, sControlName);
		}
		/**
		 * Link the U_Year3 Field to the Form Item.
		 */
		public void doLink_Year3(string sControlName){
			moLinker.doLinkDataToControl(_Year3, sControlName);
		}
		/**
		 * Link the U_Year4 Field to the Form Item.
		 */
		public void doLink_Year4(string sControlName){
			moLinker.doLinkDataToControl(_Year4, sControlName);
		}
		/**
		 * Link the U_Year5 Field to the Form Item.
		 */
		public void doLink_Year5(string sControlName){
			moLinker.doLinkDataToControl(_Year5, sControlName);
		}
#endregion

	}
}
