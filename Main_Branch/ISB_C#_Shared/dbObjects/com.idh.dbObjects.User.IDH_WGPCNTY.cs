/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2012/10/19 08:22:43 PM
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.bridge;
using com.idh.dbObjects.numbers;

namespace com.idh.dbObjects.User{
     
    public class IDH_WGPCNTY: com.idh.dbObjects.Base.IDH_WGPCNTY{ 

		public IDH_WGPCNTY() : base() {
            msAutoNumKey = "WGCNTSEQ";
		}

   	    public IDH_WGPCNTY( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
            msAutoNumKey = "WGCNTSEQ";
   	    }

        public void doCopyWGPCNTYItems(string sNewItemCode, string sNewItemName, SAPbobsCOM.Recordset oRecordset)
        {
            try
            {
                IDH_WGPCNTY oNewRow = new IDH_WGPCNTY();
                oNewRow.AutoAddEmptyLine = false;

                while (!oRecordset.EoF)
                {
                    NumbersPair oNumbers = getNewKey();
                    oNewRow.doAddEmptyRow(true, true, false);
                    oNewRow.Code = oNumbers.CodeCode;
                    oNewRow.Name = oNumbers.NameCode;

                    oNewRow.U_WstGpCd = oRecordset.Fields.Item("U_WstGpCd").Value.ToString();
                    oNewRow.U_CntyCd = oRecordset.Fields.Item("U_CntyCd").Value.ToString();
                    oNewRow.U_ItemCd = sNewItemCode; 
                    oNewRow.U_ItemNm = sNewItemName; 

                    oRecordset.MoveNext();
                }
                oNewRow.doProcessData();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
	}
}
