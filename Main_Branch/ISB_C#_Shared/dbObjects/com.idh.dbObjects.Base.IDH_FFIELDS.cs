/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 2014/10/30 11:53:13 AM
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
   [Serializable]
	public class IDH_FFIELDS: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
		protected IDHAddOns.idh.forms.Base moIDHForm;

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_FFIELDS() : base("@IDH_FFIELDS"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_FFIELDS( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_FFIELDS"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_FFIELDS";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Form Type Id
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FTId
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _FTId = "U_FTId";
		public string U_FTId { 
			get {
 				return getValueAsString(_FTId); 
			}
			set { setValue(_FTId, value); }
		}
           public string doValidate_FTId() {
               return doValidate_FTId(U_FTId);
           }
           public virtual string doValidate_FTId(object oValue) {
               return base.doValidation(_FTId, oValue);
           }

		/**
		 * Decription: Branch
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Branch
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Branch = "U_Branch";
		public string U_Branch { 
			get {
 				return getValueAsString(_Branch); 
			}
			set { setValue(_Branch, value); }
		}
           public string doValidate_Branch() {
               return doValidate_Branch(U_Branch);
           }
           public virtual string doValidate_Branch(object oValue) {
               return base.doValidation(_Branch, oValue);
           }

		/**
		 * Decription: Department
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Dep
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Dep = "U_Dep";
		public string U_Dep { 
			get {
 				return getValueAsString(_Dep); 
			}
			set { setValue(_Dep, value); }
		}
           public string doValidate_Dep() {
               return doValidate_Dep(U_Dep);
           }
           public virtual string doValidate_Dep(object oValue) {
               return base.doValidation(_Dep, oValue);
           }

		/**
		 * Decription: User
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_User
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _User = "U_User";
		public string U_User { 
			get {
 				return getValueAsString(_User); 
			}
			set { setValue(_User, value); }
		}
           public string doValidate_User() {
               return doValidate_User(U_User);
           }
           public virtual string doValidate_User(object oValue) {
               return base.doValidation(_User, oValue);
           }

		/**
		 * Decription: Position
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Pos
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Pos = "U_Pos";
		public short U_Pos { 
			get {
 				return getValueAsShort(_Pos); 
			}
			set { setValue(_Pos, value); }
		}
           public string doValidate_Pos() {
               return doValidate_Pos(U_Pos);
           }
           public virtual string doValidate_Pos(object oValue) {
               return base.doValidation(_Pos, oValue);
           }

		/**
		 * Decription: DBField
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DBFld
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DBFld = "U_DBFld";
		public string U_DBFld { 
			get {
 				return getValueAsString(_DBFld); 
			}
			set { setValue(_DBFld, value); }
		}
           public string doValidate_DBFld() {
               return doValidate_DBFld(U_DBFld);
           }
           public virtual string doValidate_DBFld(object oValue) {
               return base.doValidation(_DBFld, oValue);
           }

		/**
		 * Decription: Lable
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Lable
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Lable = "U_Lable";
		public string U_Lable { 
			get {
 				return getValueAsString(_Lable); 
			}
			set { setValue(_Lable, value); }
		}
           public string doValidate_Lable() {
               return doValidate_Lable(U_Lable);
           }
           public virtual string doValidate_Lable(object oValue) {
               return base.doValidation(_Lable, oValue);
           }

		/**
		 * Decription: Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Type
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Type = "U_Type";
		public string U_Type { 
			get {
 				return getValueAsString(_Type); 
			}
			set { setValue(_Type, value); }
		}
           public string doValidate_Type() {
               return doValidate_Type(U_Type);
           }
           public virtual string doValidate_Type(object oValue) {
               return base.doValidation(_Type, oValue);
           }

		/**
		 * Decription: Population Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PopT
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PopT = "U_PopT";
		public string U_PopT { 
			get {
 				return getValueAsString(_PopT); 
			}
			set { setValue(_PopT, value); }
		}
           public string doValidate_PopT() {
               return doValidate_PopT(U_PopT);
           }
           public virtual string doValidate_PopT(object oValue) {
               return base.doValidation(_PopT, oValue);
           }

		/**
		 * Decription: Additional Parameter
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PopPar
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PopPar = "U_PopPar";
		public string U_PopPar { 
			get {
 				return getValueAsString(_PopPar); 
			}
			set { setValue(_PopPar, value); }
		}
           public string doValidate_PopPar() {
               return doValidate_PopPar(U_PopPar);
           }
           public virtual string doValidate_PopPar(object oValue) {
               return base.doValidation(_PopPar, oValue);
           }

		/**
		 * Decription: SAP object Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_oType
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _oType = "U_oType";
		public string U_oType { 
			get {
 				return getValueAsString(_oType); 
			}
			set { setValue(_oType, value); }
		}
           public string doValidate_oType() {
               return doValidate_oType(U_oType);
           }
           public virtual string doValidate_oType(object oValue) {
               return base.doValidation(_oType, oValue);
           }

		/**
		 * Decription: FieldSize
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Length
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Length = "U_Length";
		public int U_Length { 
			get {
 				return getValueAsInt(_Length); 
			}
			set { setValue(_Length, value); }
		}
           public string doValidate_Length() {
               return doValidate_Length(U_Length);
           }
           public virtual string doValidate_Length(object oValue) {
               return base.doValidation(_Length, oValue);
           }

		/**
		 * Decription: Opperation
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Op
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Op = "U_Op";
		public string U_Op { 
			get {
 				return getValueAsString(_Op); 
			}
			set { setValue(_Op, value); }
		}
           public string doValidate_Op() {
               return doValidate_Op(U_Op);
           }
           public virtual string doValidate_Op(object oValue) {
               return base.doValidation(_Op, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(15);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_FTId, "Form Type Id", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Form Type Id
			moDBFields.Add(_Branch, "Branch", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Branch
			moDBFields.Add(_Dep, "Department", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Department
			moDBFields.Add(_User, "User", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //User
			moDBFields.Add(_Pos, "Position", 6, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Position
			moDBFields.Add(_DBFld, "DBField", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //DBField
			moDBFields.Add(_Lable, "Lable", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Lable
			moDBFields.Add(_Type, "Type", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Type
			moDBFields.Add(_PopT, "Population Type", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Population Type
			moDBFields.Add(_PopPar, "Additional Parameter", 11, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Additional Parameter
			moDBFields.Add(_oType, "SAP object Type", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //SAP object Type
			moDBFields.Add(_Length, "FieldSize", 13, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //FieldSize
			moDBFields.Add(_Op, "Opperation", 14, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Opperation

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_FTId());
            doBuildValidationString(doValidate_Branch());
            doBuildValidationString(doValidate_Dep());
            doBuildValidationString(doValidate_User());
            doBuildValidationString(doValidate_Pos());
            doBuildValidationString(doValidate_DBFld());
            doBuildValidationString(doValidate_Lable());
            doBuildValidationString(doValidate_Type());
            doBuildValidationString(doValidate_PopT());
            doBuildValidationString(doValidate_PopPar());
            doBuildValidationString(doValidate_oType());
            doBuildValidationString(doValidate_Length());
            doBuildValidationString(doValidate_Op());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_FTId)) return doValidate_FTId(oValue);
            if (sFieldName.Equals(_Branch)) return doValidate_Branch(oValue);
            if (sFieldName.Equals(_Dep)) return doValidate_Dep(oValue);
            if (sFieldName.Equals(_User)) return doValidate_User(oValue);
            if (sFieldName.Equals(_Pos)) return doValidate_Pos(oValue);
            if (sFieldName.Equals(_DBFld)) return doValidate_DBFld(oValue);
            if (sFieldName.Equals(_Lable)) return doValidate_Lable(oValue);
            if (sFieldName.Equals(_Type)) return doValidate_Type(oValue);
            if (sFieldName.Equals(_PopT)) return doValidate_PopT(oValue);
            if (sFieldName.Equals(_PopPar)) return doValidate_PopPar(oValue);
            if (sFieldName.Equals(_oType)) return doValidate_oType(oValue);
            if (sFieldName.Equals(_Length)) return doValidate_Length(oValue);
            if (sFieldName.Equals(_Op)) return doValidate_Op(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_FTId Field to the Form Item.
		 */
		public void doLink_FTId(string sControlName){
			moLinker.doLinkDataToControl(_FTId, sControlName);
		}
		/**
		 * Link the U_Branch Field to the Form Item.
		 */
		public void doLink_Branch(string sControlName){
			moLinker.doLinkDataToControl(_Branch, sControlName);
		}
		/**
		 * Link the U_Dep Field to the Form Item.
		 */
		public void doLink_Dep(string sControlName){
			moLinker.doLinkDataToControl(_Dep, sControlName);
		}
		/**
		 * Link the U_User Field to the Form Item.
		 */
		public void doLink_User(string sControlName){
			moLinker.doLinkDataToControl(_User, sControlName);
		}
		/**
		 * Link the U_Pos Field to the Form Item.
		 */
		public void doLink_Pos(string sControlName){
			moLinker.doLinkDataToControl(_Pos, sControlName);
		}
		/**
		 * Link the U_DBFld Field to the Form Item.
		 */
		public void doLink_DBFld(string sControlName){
			moLinker.doLinkDataToControl(_DBFld, sControlName);
		}
		/**
		 * Link the U_Lable Field to the Form Item.
		 */
		public void doLink_Lable(string sControlName){
			moLinker.doLinkDataToControl(_Lable, sControlName);
		}
		/**
		 * Link the U_Type Field to the Form Item.
		 */
		public void doLink_Type(string sControlName){
			moLinker.doLinkDataToControl(_Type, sControlName);
		}
		/**
		 * Link the U_PopT Field to the Form Item.
		 */
		public void doLink_PopT(string sControlName){
			moLinker.doLinkDataToControl(_PopT, sControlName);
		}
		/**
		 * Link the U_PopPar Field to the Form Item.
		 */
		public void doLink_PopPar(string sControlName){
			moLinker.doLinkDataToControl(_PopPar, sControlName);
		}
		/**
		 * Link the U_oType Field to the Form Item.
		 */
		public void doLink_oType(string sControlName){
			moLinker.doLinkDataToControl(_oType, sControlName);
		}
		/**
		 * Link the U_Length Field to the Form Item.
		 */
		public void doLink_Length(string sControlName){
			moLinker.doLinkDataToControl(_Length, sControlName);
		}
		/**
		 * Link the U_Op Field to the Form Item.
		 */
		public void doLink_Op(string sControlName){
			moLinker.doLinkDataToControl(_Op, sControlName);
		}
#endregion

	}
}
