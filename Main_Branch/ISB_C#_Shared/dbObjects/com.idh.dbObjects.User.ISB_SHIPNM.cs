/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2013/02/11 03:50:43 PM
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
     
    public class ISB_SHIPNM: com.idh.dbObjects.Base.ISB_SHIPNM{ 

		public ISB_SHIPNM(){
            msAutoNumKey = "GENSEQ";
		}

   	    public ISB_SHIPNM( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
            msAutoNumKey = "GENSEQ";
   	    }
	}
}
