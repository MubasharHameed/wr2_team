/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 04/02/2016 17:41:02
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
   [Serializable] 
	public class IDH_BTCHITM: com.idh.dbObjects.Base.IDH_BTCHITM{ 

		public IDH_BTCHITM() : base() {
			
		}

   	public IDH_BTCHITM( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	}
	}
}
