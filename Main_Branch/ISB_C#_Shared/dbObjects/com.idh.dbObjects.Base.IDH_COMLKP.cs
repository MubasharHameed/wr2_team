/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 2014/11/10 08:52:12 AM
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
   [Serializable]
	public class IDH_COMLKP: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
		protected IDHAddOns.idh.forms.Base moIDHForm;

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_COMLKP() : base("@IDH_COMLKP"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_COMLKP( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_COMLKP"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_COMLKP";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Table
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Table
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Table = "U_Table";
		public string U_Table { 
			get {
 				return getValueAsString(_Table); 
			}
			set { setValue(_Table, value); }
		}
           public string doValidate_Table() {
               return doValidate_Table(U_Table);
           }
           public virtual string doValidate_Table(object oValue) {
               return base.doValidation(_Table, oValue);
           }

		/**
		 * Decription: Field Value
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ValF
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ValF = "U_ValF";
		public string U_ValF { 
			get {
 				return getValueAsString(_ValF); 
			}
			set { setValue(_ValF, value); }
		}
           public string doValidate_ValF() {
               return doValidate_ValF(U_ValF);
           }
           public virtual string doValidate_ValF(object oValue) {
               return base.doValidation(_ValF, oValue);
           }

		/**
		 * Decription: Field Description
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DescF
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DescF = "U_DescF";
		public string U_DescF { 
			get {
 				return getValueAsString(_DescF); 
			}
			set { setValue(_DescF, value); }
		}
           public string doValidate_DescF() {
               return doValidate_DescF(U_DescF);
           }
           public virtual string doValidate_DescF(object oValue) {
               return base.doValidation(_DescF, oValue);
           }

		/**
		 * Decription: WHERE Clause
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WHERE
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WHERE = "U_WHERE";
		public string U_WHERE { 
			get {
 				return getValueAsString(_WHERE); 
			}
			set { setValue(_WHERE, value); }
		}
           public string doValidate_WHERE() {
               return doValidate_WHERE(U_WHERE);
           }
           public virtual string doValidate_WHERE(object oValue) {
               return base.doValidation(_WHERE, oValue);
           }

		/**
		 * Decription: ORDER
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ORDER
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ORDER = "U_ORDER";
		public string U_ORDER { 
			get {
 				return getValueAsString(_ORDER); 
			}
			set { setValue(_ORDER, value); }
		}
           public string doValidate_ORDER() {
               return doValidate_ORDER(U_ORDER);
           }
           public virtual string doValidate_ORDER(object oValue) {
               return base.doValidation(_ORDER, oValue);
           }

		/**
		 * Decription: Empty Value
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_EmptyV
		 * Size: 254
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _EmptyV = "U_EmptyV";
		public string U_EmptyV { 
			get {
 				return getValueAsString(_EmptyV); 
			}
			set { setValue(_EmptyV, value); }
		}
           public string doValidate_EmptyV() {
               return doValidate_EmptyV(U_EmptyV);
           }
           public virtual string doValidate_EmptyV(object oValue) {
               return base.doValidation(_EmptyV, oValue);
           }

		/**
		 * Decription: Empty Description
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_EmptyD
		 * Size: 254
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _EmptyD = "U_EmptyD";
		public string U_EmptyD { 
			get {
 				return getValueAsString(_EmptyD); 
			}
			set { setValue(_EmptyD, value); }
		}
           public string doValidate_EmptyD() {
               return doValidate_EmptyD(U_EmptyD);
           }
           public virtual string doValidate_EmptyD(object oValue) {
               return base.doValidation(_EmptyD, oValue);
           }

		/**
		 * Decription: Combo Catagory
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Cat
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Cat = "U_Cat";
		public string U_Cat { 
			get {
 				return getValueAsString(_Cat); 
			}
			set { setValue(_Cat, value); }
		}
           public string doValidate_Cat() {
               return doValidate_Cat(U_Cat);
           }
           public virtual string doValidate_Cat(object oValue) {
               return base.doValidation(_Cat, oValue);
           }

		/**
		 * Decription: Combo Description
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Disc
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Disc = "U_Disc";
		public string U_Disc { 
			get {
 				return getValueAsString(_Disc); 
			}
			set { setValue(_Disc, value); }
		}
           public string doValidate_Disc() {
               return doValidate_Disc(U_Disc);
           }
           public virtual string doValidate_Disc(object oValue) {
               return base.doValidation(_Disc, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(11);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_Table, "Table", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Table
			moDBFields.Add(_ValF, "Field Value", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Field Value
			moDBFields.Add(_DescF, "Field Description", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Field Description
			moDBFields.Add(_WHERE, "WHERE Clause", 5, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //WHERE Clause
			moDBFields.Add(_ORDER, "ORDER", 6, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //ORDER
			moDBFields.Add(_EmptyV, "Empty Value", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, EMPTYSTR, false, false); //Empty Value
			moDBFields.Add(_EmptyD, "Empty Description", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, EMPTYSTR, false, false); //Empty Description
			moDBFields.Add(_Cat, "Combo Catagory", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Combo Catagory
			moDBFields.Add(_Disc, "Combo Description", 10, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Combo Description

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_Table());
            doBuildValidationString(doValidate_ValF());
            doBuildValidationString(doValidate_DescF());
            doBuildValidationString(doValidate_WHERE());
            doBuildValidationString(doValidate_ORDER());
            doBuildValidationString(doValidate_EmptyV());
            doBuildValidationString(doValidate_EmptyD());
            doBuildValidationString(doValidate_Cat());
            doBuildValidationString(doValidate_Disc());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_Table)) return doValidate_Table(oValue);
            if (sFieldName.Equals(_ValF)) return doValidate_ValF(oValue);
            if (sFieldName.Equals(_DescF)) return doValidate_DescF(oValue);
            if (sFieldName.Equals(_WHERE)) return doValidate_WHERE(oValue);
            if (sFieldName.Equals(_ORDER)) return doValidate_ORDER(oValue);
            if (sFieldName.Equals(_EmptyV)) return doValidate_EmptyV(oValue);
            if (sFieldName.Equals(_EmptyD)) return doValidate_EmptyD(oValue);
            if (sFieldName.Equals(_Cat)) return doValidate_Cat(oValue);
            if (sFieldName.Equals(_Disc)) return doValidate_Disc(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_Table Field to the Form Item.
		 */
		public void doLink_Table(string sControlName){
			moLinker.doLinkDataToControl(_Table, sControlName);
		}
		/**
		 * Link the U_ValF Field to the Form Item.
		 */
		public void doLink_ValF(string sControlName){
			moLinker.doLinkDataToControl(_ValF, sControlName);
		}
		/**
		 * Link the U_DescF Field to the Form Item.
		 */
		public void doLink_DescF(string sControlName){
			moLinker.doLinkDataToControl(_DescF, sControlName);
		}
		/**
		 * Link the U_WHERE Field to the Form Item.
		 */
		public void doLink_WHERE(string sControlName){
			moLinker.doLinkDataToControl(_WHERE, sControlName);
		}
		/**
		 * Link the U_ORDER Field to the Form Item.
		 */
		public void doLink_ORDER(string sControlName){
			moLinker.doLinkDataToControl(_ORDER, sControlName);
		}
		/**
		 * Link the U_EmptyV Field to the Form Item.
		 */
		public void doLink_EmptyV(string sControlName){
			moLinker.doLinkDataToControl(_EmptyV, sControlName);
		}
		/**
		 * Link the U_EmptyD Field to the Form Item.
		 */
		public void doLink_EmptyD(string sControlName){
			moLinker.doLinkDataToControl(_EmptyD, sControlName);
		}
		/**
		 * Link the U_Cat Field to the Form Item.
		 */
		public void doLink_Cat(string sControlName){
			moLinker.doLinkDataToControl(_Cat, sControlName);
		}
		/**
		 * Link the U_Disc Field to the Form Item.
		 */
		public void doLink_Disc(string sControlName){
			moLinker.doLinkDataToControl(_Disc, sControlName);
		}
#endregion

	}
}
