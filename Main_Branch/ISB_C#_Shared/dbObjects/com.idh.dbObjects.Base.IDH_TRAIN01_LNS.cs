/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 2015-07-13 02:55:15 PM
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base
{
    [Serializable]
    public class IDH_TRAIN01_LNS : com.idh.dbObjects.DBBase
    {

        //private Linker moLinker = null;
        //public Linker ControlLinker
        //{
        //    get { return moLinker; }
        //    set { moLinker = value; }
        //}

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm
        {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX
        {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_TRAIN01_LNS()
            : base("@IDH_TRAIN01_LNS")
        {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_TRAIN01_LNS(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oForm, "@IDH_TRAIN01_LNS")
        {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
		* Table name
		*/
        public readonly static string TableName = "@IDH_TRAIN01_LNS";

        /**
         * Decription: Code
         * Mandatory: tYes
         * Name: Code
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Code = "Code";
        public string Code
        {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code()
        {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue)
        {
            return base.doValidation(_Code, oValue);
        }
        /**
         * Decription: Name
         * Mandatory: tYes
         * Name: Name
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Name = "Name";
        public string Name
        {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name()
        {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue)
        {
            return base.doValidation(_Name, oValue);
        }
        /**
         * Decription: Header Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_HDRCode
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _HDRCode = "U_HDRCode";
        public string U_HDRCode
        {
            get
            {
                return getValueAsString(_HDRCode);
            }
            set { setValue(_HDRCode, value); }
        }
        public string doValidate_HDRCode()
        {
            return doValidate_HDRCode(U_HDRCode);
        }
        public virtual string doValidate_HDRCode(object oValue)
        {
            return base.doValidation(_HDRCode, oValue);
        }

        /**
         * Decription: Item Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ItemCode
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ItemCode = "U_ItemCode";
        public string U_ItemCode
        {
            get
            {
                return getValueAsString(_ItemCode);
            }
            set { setValue(_ItemCode, value); }
        }
        public string doValidate_ItemCode()
        {
            return doValidate_ItemCode(U_ItemCode);
        }
        public virtual string doValidate_ItemCode(object oValue)
        {
            return base.doValidation(_ItemCode, oValue);
        }

        /**
         * Decription: Item Description
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ItemDesc
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ItemDesc = "U_ItemDesc";
        public string U_ItemDesc
        {
            get
            {
                return getValueAsString(_ItemDesc);
            }
            set { setValue(_ItemDesc, value); }
        }
        public string doValidate_ItemDesc()
        {
            return doValidate_ItemDesc(U_ItemDesc);
        }
        public virtual string doValidate_ItemDesc(object oValue)
        {
            return base.doValidation(_ItemDesc, oValue);
        }

        /**
         * Decription: Unit Price
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Price
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Price
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Price = "U_Price";
        public double U_Price
        {
            get
            {
                return getValueAsDouble(_Price);
            }
            set { setValue(_Price, value); }
        }
        public string doValidate_Price()
        {
            return doValidate_Price(U_Price);
        }
        public virtual string doValidate_Price(object oValue)
        {
            return base.doValidation(_Price, oValue);
        }

        /**
         * Decription: Quantity
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_QTY
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _QTY = "U_QTY";
        public int U_QTY
        {
            get
            {
                return getValueAsInt(_QTY);
            }
            set { setValue(_QTY, value); }
        }
        public string doValidate_QTY()
        {
            return doValidate_QTY(U_QTY);
        }
        public virtual string doValidate_QTY(object oValue)
        {
            return base.doValidation(_QTY, oValue);
        }

        /**
         * Decription: Line Total
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Total
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Price
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Total = "U_Total";
        public double U_Total
        {
            get
            {
                return getValueAsDouble(_Total);
            }
            set { setValue(_Total, value); }
        }
        public string doValidate_Total()
        {
            return doValidate_Total(U_Total);
        }
        public virtual string doValidate_Total(object oValue)
        {
            return base.doValidation(_Total, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo()
        {
            if (moDBFields == null)
                moDBFields = new DBFields(8);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_HDRCode, "Header Code", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Header Code
            moDBFields.Add(_ItemCode, "Item Code", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Item Code
            moDBFields.Add(_ItemDesc, "Item Description", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Item Description
            moDBFields.Add(_Price, "Unit Price", 5, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Unit Price
            moDBFields.Add(_QTY, "Quantity", 6, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Quantity
            moDBFields.Add(_Total, "Line Total", 7, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Line Total

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation()
        {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_HDRCode());
            doBuildValidationString(doValidate_ItemCode());
            doBuildValidationString(doValidate_ItemDesc());
            doBuildValidationString(doValidate_Price());
            doBuildValidationString(doValidate_QTY());
            doBuildValidationString(doValidate_Total());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue)
        {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_HDRCode)) return doValidate_HDRCode(oValue);
            if (sFieldName.Equals(_ItemCode)) return doValidate_ItemCode(oValue);
            if (sFieldName.Equals(_ItemDesc)) return doValidate_ItemDesc(oValue);
            if (sFieldName.Equals(_Price)) return doValidate_Price(oValue);
            if (sFieldName.Equals(_QTY)) return doValidate_QTY(oValue);
            if (sFieldName.Equals(_Total)) return doValidate_Total(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName)
        {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
         * Link the Name Field to the Form Item.
         */
        public void doLink_Name(string sControlName)
        {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
         * Link the U_HDRCode Field to the Form Item.
         */
        public void doLink_HDRCode(string sControlName)
        {
            moLinker.doLinkDataToControl(_HDRCode, sControlName);
        }
        /**
         * Link the U_ItemCode Field to the Form Item.
         */
        public void doLink_ItemCode(string sControlName)
        {
            moLinker.doLinkDataToControl(_ItemCode, sControlName);
        }
        /**
         * Link the U_ItemDesc Field to the Form Item.
         */
        public void doLink_ItemDesc(string sControlName)
        {
            moLinker.doLinkDataToControl(_ItemDesc, sControlName);
        }
        /**
         * Link the U_Price Field to the Form Item.
         */
        public void doLink_Price(string sControlName)
        {
            moLinker.doLinkDataToControl(_Price, sControlName);
        }
        /**
         * Link the U_QTY Field to the Form Item.
         */
        public void doLink_QTY(string sControlName)
        {
            moLinker.doLinkDataToControl(_QTY, sControlName);
        }
        /**
         * Link the U_Total Field to the Form Item.
         */
        public void doLink_Total(string sControlName)
        {
            moLinker.doLinkDataToControl(_Total, sControlName);
        }
        #endregion

    }
}