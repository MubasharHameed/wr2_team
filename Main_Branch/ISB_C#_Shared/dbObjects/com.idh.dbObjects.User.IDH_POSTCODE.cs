/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2011/11/04 02:00:51 PM
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.bridge.lookups;
using com.idh.utils;
using System.Web;
namespace com.idh.dbObjects.User{
      
	public class IDH_POSTCODE: com.idh.dbObjects.Base.IDH_POSTCODE{
//        private Hashtable mhChangedCustRefs;


        private string msKey;
        private string msSearchTerm;
        private string msLastId="";
        private string msSearchFor = "Everything";
        private string msCountry = "GB";
        private string msLanguagePreference = "en";
        private int msMaxSuggestions = 50;
        private int msMaxResults = 300;

        private string msCapturePlusInteractiveFindServiceURL = "";
        private string msCapturePlusInteractiveRetrieveServiceURL = "";
        

        public string sKey{
            get { return msKey; }
            set { msKey = value; }
        }
        public string SearchTerm
        {
            get { return msSearchTerm; }
            set { msSearchTerm = value; }
        }
        public string LastId 
        {
            get { return msLastId; }
            set { msLastId = value; }
        }

        public string SearchFor
        {
            get { return msSearchFor; }
            set { msSearchFor = value; }
        }

        public string Country
        {
            get { return msCountry; }
            set { msCountry = value; }
        }

        public string LanguagePreference
        {
            get { return msLanguagePreference; }
            set { msLanguagePreference = value; }
        }

        public int MaxSuggestions
        {
            get { return msMaxSuggestions; }
            set { msMaxSuggestions = value; }
        }

        public int MaxResults
        {
            get { return msMaxResults; }
            set { msMaxResults = value; }
        }


		public IDH_POSTCODE() : base(){
            msCapturePlusInteractiveFindServiceURL = (string)Config.ParamaterWithDefault("PCODURL1", "https://services.postcodeanywhere.co.uk/CapturePlus/Interactive/Find/v2.10/dataset.ws?");
            sKey = (string)Config.ParamaterWithDefault("PCODEKEY", "");

            msCapturePlusInteractiveRetrieveServiceURL = (string)Config.ParamaterWithDefault("PCODURL2", "https://services.postcodeanywhere.co.uk/CapturePlus/Interactive/Retrieve/v2.10/dataset.ws?");
        
        }
		
		public IDH_POSTCODE( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
            msCapturePlusInteractiveFindServiceURL = (string)Config.ParamaterWithDefault("PCODURL1", "https://services.postcodeanywhere.co.uk/CapturePlus/Interactive/Find/v2.10/dataset.ws?");
            sKey = (string)Config.ParamaterWithDefault("PCODEKEY", "");

            msCapturePlusInteractiveRetrieveServiceURL = (string)Config.ParamaterWithDefault("PCODURL2", "https://services.postcodeanywhere.co.uk/CapturePlus/Interactive/Retrieve/v2.10/dataset.ws?");
        
        }	
		
		
		//public IDH_POSTCODE(){
		//	base();
		//}

        public DataTable doSearchAddresses(ref string sError) {
            try
            {
                string sDfltLanguage = Config.ParamaterWithDefault("DFTLANG", "en").ToLower();
                //Build the url
                System.Text.StringBuilder url = new System.Text.StringBuilder();
                url.Append(msCapturePlusInteractiveFindServiceURL);
                url.Append("&Key=" + System.Web.HttpUtility.UrlEncode(msKey));
                url.Append("&SearchTerm=" + System.Web.HttpUtility.UrlEncode(msSearchTerm));
                url.Append("&LastId=" + System.Web.HttpUtility.UrlEncode(msLastId));
                url.Append("&SearchFor=" + System.Web.HttpUtility.UrlEncode(msSearchFor));
                url.Append("&Country=" + System.Web.HttpUtility.UrlEncode(msCountry));
                url.Append("&LanguagePreference=" + System.Web.HttpUtility.UrlEncode(sDfltLanguage));
                url.Append("&MaxSuggestions=" + System.Web.HttpUtility.UrlEncode(msMaxSuggestions.ToString(System.Globalization.CultureInfo.InvariantCulture)));
                url.Append("&MaxResults=" + System.Web.HttpUtility.UrlEncode(msMaxResults.ToString(System.Globalization.CultureInfo.InvariantCulture)));

                //Create the dataset
                var dataSet = new System.Data.DataSet();
                dataSet.ReadXml(url.ToString());

                //Check for an error
                if (dataSet.Tables.Count == 1 && dataSet.Tables[0].Columns.Count == 4 && dataSet.Tables[0].Columns[0].ColumnName == "Error")
                {
                    sError = dataSet.Tables[0].Rows[0].ItemArray[1].ToString();
                    return null;
                }
                //Return the dataset
                return dataSet.Tables[0];

                //FYI: The dataset contains the following columns:
                //Id
                //Text
                //Highlight
                //Cursor
                //Description
                //Next
            }
            catch (Exception Ex)
            {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD008", null);
                return null;
            }
            
        }

        public DataTable doRetrieveAddress(string sAddressID, ref string sError)
        {
            try
            {
                //Build the url
                System.Text.StringBuilder url = new System.Text.StringBuilder();
                url.Append(msCapturePlusInteractiveRetrieveServiceURL);
                url.Append("&Key=" + System.Web.HttpUtility.UrlEncode(msKey));
                url.Append("&Id=" + System.Web.HttpUtility.UrlEncode(sAddressID));

                //Create the dataset
                var dataSet = new System.Data.DataSet();
                dataSet.ReadXml(url.ToString());

                //Check for an error
                if (dataSet.Tables.Count == 1 && dataSet.Tables[0].Columns.Count == 4 && dataSet.Tables[0].Columns[0].ColumnName == "Error")
                {
                    sError = dataSet.Tables[0].Rows[0].ItemArray[1].ToString();
                    return null;
                }
                //Return the dataset
                return dataSet.Tables[0];
                //FYI: The dataset contains the following columns:
                //Id
                //DomesticId
                //Language
                //LanguageAlternatives
                //Department
                //Company
                //SubBuilding
                //BuildingNumber
                //BuildingName
                //SecondaryStreet
                //Street
                //Block
                //Neighbourhood
                //District
                //City
                //Line1
                //Line2
                //Line3
                //Line4
                //Line5
                //AdminAreaName
                //AdminAreaCode
                //Province
                //ProvinceName
                //ProvinceCode
                //PostalCode
                //CountryName
                //CountryIso2
                //CountryIso3
                //CountryIsoNumber
                //SortingNumber1
                //SortingNumber2
                //Barcode
                //POBoxNumber
                //Label
                //Type
                //DataLevel
            }
            catch (Exception Ex)
            {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD007", null);
                return null;
            }
            }
        //public override bool doUpdateData(bool bDoValidate, string sComment) {
        public override bool doProcessData(bool bDoValidate, string sComment, bool bDoChildren) {
            return true;
        }

        public void doFillAddresses(DataTable dtDataTable)
        {
            try
            {
                this.doClearAddUpdateBuffers();
                if (dtDataTable == null)
                    return;
                for (int i = 0; i <= dtDataTable.Rows.Count - 1; i++)
                {
                    this.doAddEmptyRow();
                    this.Code = i.ToString();
                    this.Name = i.ToString();
                    this.U_ID = dtDataTable.Rows[i]["Id"].ToString();
                    this.U_Text = dtDataTable.Rows[i]["Text"].ToString();
                    this.U_Highlight = dtDataTable.Rows[i]["Highlight"].ToString();
                    this.U_Cursor = Convert.ToInt32(dtDataTable.Rows[i]["Cursor"]);
                    this.U_Description = dtDataTable.Rows[i]["Description"].ToString();
                    this.U_Next = dtDataTable.Rows[i]["Next"].ToString();
                }
                //Id
                //Text
                //Highlight
                //Cursor
                //Description
                //Next
            }
            catch (Exception Ex)
            {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EXPCD006", null);
            }
         }
         
        public override void doSetFieldHasChanged(int iRow, string sFieldname, object oOldValue, object oNewValue) {
          
        }

	}
}
