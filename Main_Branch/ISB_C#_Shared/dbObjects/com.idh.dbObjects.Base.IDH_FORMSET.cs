/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 2014/10/09 05:29:49 PM
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
   [Serializable]
	public class IDH_FORMSET: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
		protected IDHAddOns.idh.forms.Base moIDHForm;

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_FORMSET() : base("@IDH_FORMSET"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_FORMSET( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_FORMSET"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_FORMSET";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Form ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FormID
		 * Size: 150
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _FormID = "U_FormID";
		public string U_FormID { 
			get {
 				return getValueAsString(_FormID); 
			}
			set { setValue(_FormID, value); }
		}
           public string doValidate_FormID() {
               return doValidate_FormID(U_FormID);
           }
           public virtual string doValidate_FormID(object oValue) {
               return base.doValidation(_FormID, oValue);
           }

		/**
		 * Decription: User Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UserName
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _UserName = "U_UserName";
		public string U_UserName { 
			get {
 				return getValueAsString(_UserName); 
			}
			set { setValue(_UserName, value); }
		}
           public string doValidate_UserName() {
               return doValidate_UserName(U_UserName);
           }
           public virtual string doValidate_UserName(object oValue) {
               return base.doValidation(_UserName, oValue);
           }

		/**
		 * Decription: Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Name
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string __Name = "U_Name";
		public string U_Name { 
			get {
 				return getValueAsString(__Name); 
			}
			set { setValue(__Name, value); }
		}
           public string doValidate__Name() {
               return doValidate__Name(U_Name);
           }
           public virtual string doValidate__Name(object oValue) {
               return base.doValidation(__Name, oValue);
           }

           /**
            * Decription: Field Id
            * DefaultValue: 
            * LinkedTable: 
            * Mandatory: False
            * Name: U_FieldId
            * Size: 4000
            * Type: db_Memo
            * CType: string
            * SubType: st_None
            * ValidValue: 
            * Value: 
            * Identity: False
            */
           public readonly static string _FieldId = "U_FieldId";
		public string U_FieldId { 
			get {
 				return getValueAsString(_FieldId); 
			}
			set { setValue(_FieldId, value); }
		}
           public string doValidate_FieldId() {
               return doValidate_FieldId(U_FieldId);
           }
           public virtual string doValidate_FieldId(object oValue) {
               return base.doValidation(_FieldId, oValue);
           }

		/**
		 * Decription: Title
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Title
		 * Size: 254
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Title = "U_Title";
		public string U_Title { 
			get {
 				return getValueAsString(_Title); 
			}
			set { setValue(_Title, value); }
		}
           public string doValidate_Title() {
               return doValidate_Title(U_Title);
           }
           public virtual string doValidate_Title(object oValue) {
               return base.doValidation(_Title, oValue);
           }

		/**
		 * Decription: Width
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Width
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Width = "U_Width";
		public int U_Width { 
			get {
 				return getValueAsInt(_Width); 
			}
			set { setValue(_Width, value); }
		}
           public string doValidate_Width() {
               return doValidate_Width(U_Width);
           }
           public virtual string doValidate_Width(object oValue) {
               return base.doValidation(_Width, oValue);
           }

		/**
		 * Decription: Editable
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Editable
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Editable = "U_Editable";
		public string U_Editable { 
			get {
 				return getValueAsString(_Editable); 
			}
			set { setValue(_Editable, value); }
		}
           public string doValidate_Editable() {
               return doValidate_Editable(U_Editable);
           }
           public virtual string doValidate_Editable(object oValue) {
               return base.doValidation(_Editable, oValue);
           }

           /**
            * Decription: sType
            * DefaultValue: 
            * LinkedTable: 
            * Mandatory: False
            * Size: 4000
            * Type: db_Memo
            * CType: string
            * SubType: st_None
            * ValidValue: 
            * Value: 
            * Identity: False
            */
           public readonly static string _sType = "U_sType";
		public string U_sType { 
			get {
 				return getValueAsString(_sType); 
			}
			set { setValue(_sType, value); }
		}
           public string doValidate_sType() {
               return doValidate_sType(U_sType);
           }
           public virtual string doValidate_sType(object oValue) {
               return base.doValidation(_sType, oValue);
           }

		/**
		 * Decription: Back Color
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_iBackColor
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _iBackColor = "U_iBackColor";
		public int U_iBackColor { 
			get {
 				return getValueAsInt(_iBackColor); 
			}
			set { setValue(_iBackColor, value); }
		}
           public string doValidate_iBackColor() {
               return doValidate_iBackColor(U_iBackColor);
           }
           public virtual string doValidate_iBackColor(object oValue) {
               return base.doValidation(_iBackColor, oValue);
           }

		/**
		 * Decription: oLinkObject
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_oLinkObject
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _oLinkObject = "U_oLinkObject";
		public int U_oLinkObject { 
			get {
 				return getValueAsInt(_oLinkObject); 
			}
			set { setValue(_oLinkObject, value); }
		}
           public string doValidate_oLinkObject() {
               return doValidate_oLinkObject(U_oLinkObject);
           }
           public virtual string doValidate_oLinkObject(object oValue) {
               return base.doValidation(_oLinkObject, oValue);
           }

		/**
		 * Decription: bWidthIsMax
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_bWidthIsMax
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _bWidthIsMax = "U_bWidthIsMax";
		public string U_bWidthIsMax { 
			get {
 				return getValueAsString(_bWidthIsMax); 
			}
			set { setValue(_bWidthIsMax, value); }
		}
           public string doValidate_bWidthIsMax() {
               return doValidate_bWidthIsMax(U_bWidthIsMax);
           }
           public virtual string doValidate_bWidthIsMax(object oValue) {
               return base.doValidation(_bWidthIsMax, oValue);
           }

		/**
		 * Decription: bPinned
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_bPinned
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _bPinned = "U_bPinned";
		public string U_bPinned { 
			get {
 				return getValueAsString(_bPinned); 
			}
			set { setValue(_bPinned, value); }
		}
           public string doValidate_bPinned() {
               return doValidate_bPinned(U_bPinned);
           }
           public virtual string doValidate_bPinned(object oValue) {
               return base.doValidation(_bPinned, oValue);
           }

		/**
		 * Decription: oSumType
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_oSumType
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _oSumType = "U_oSumType";
		public int U_oSumType { 
			get {
 				return getValueAsInt(_oSumType); 
			}
			set { setValue(_oSumType, value); }
		}
           public string doValidate_oSumType() {
               return doValidate_oSumType(U_oSumType);
           }
           public virtual string doValidate_oSumType(object oValue) {
               return base.doValidation(_oSumType, oValue);
           }

		/**
		 * Decription: oDisplayType
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_oDisplayType
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _oDisplayType = "U_oDisplayType";
		public int U_oDisplayType { 
			get {
 				return getValueAsInt(_oDisplayType); 
			}
			set { setValue(_oDisplayType, value); }
		}
           public string doValidate_oDisplayType() {
               return doValidate_oDisplayType(U_oDisplayType);
           }
           public virtual string doValidate_oDisplayType(object oValue) {
               return base.doValidation(_oDisplayType, oValue);
           }

		/**
		 * Decription: Order Index
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Index
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Index = "U_Index";
		public int U_Index { 
			get {
 				return getValueAsInt(_Index); 
			}
			set { setValue(_Index, value); }
		}
           public string doValidate_Index() {
               return doValidate_Index(U_Index);
           }
           public virtual string doValidate_Index(object oValue) {
               return base.doValidation(_Index, oValue);
           }

		/**
		 * Decription: sUID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_sUID
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _sUID = "U_sUID";
		public string U_sUID { 
			get {
 				return getValueAsString(_sUID); 
			}
			set { setValue(_sUID, value); }
		}
           public string doValidate_sUID() {
               return doValidate_sUID(U_sUID);
           }
           public virtual string doValidate_sUID(object oValue) {
               return base.doValidation(_sUID, oValue);
           }

		/**
		 * Decription: DeptCode
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DeptCode
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _DeptCode = "U_DeptCode";
		public int U_DeptCode { 
			get {
 				return getValueAsInt(_DeptCode); 
			}
			set { setValue(_DeptCode, value); }
		}
           public string doValidate_DeptCode() {
               return doValidate_DeptCode(U_DeptCode);
           }
           public virtual string doValidate_DeptCode(object oValue) {
               return base.doValidation(_DeptCode, oValue);
           }

		/**
		 * Decription: FormName
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FormName
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _FormName = "U_FormName";
		public string U_FormName { 
			get {
 				return getValueAsString(_FormName); 
			}
			set { setValue(_FormName, value); }
		}
           public string doValidate_FormName() {
               return doValidate_FormName(U_FormName);
           }
           public virtual string doValidate_FormName(object oValue) {
               return base.doValidation(_FormName, oValue);
           }

		/**
		 * Decription: Order By
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_OrderBy
		 * Size: 25
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _OrderBy = "U_OrderBy";
		public string U_OrderBy { 
			get {
 				return getValueAsString(_OrderBy); 
			}
			set { setValue(_OrderBy, value); }
		}
           public string doValidate_OrderBy() {
               return doValidate_OrderBy(U_OrderBy);
           }
           public virtual string doValidate_OrderBy(object oValue) {
               return base.doValidation(_OrderBy, oValue);
           }

		/**
		 * Decription: Grid Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_GridName
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _GridName = "U_GridName";
		public string U_GridName { 
			get {
 				return getValueAsString(_GridName); 
			}
			set { setValue(_GridName, value); }
		}
           public string doValidate_GridName() {
               return doValidate_GridName(U_GridName);
           }
           public virtual string doValidate_GridName(object oValue) {
               return base.doValidation(_GridName, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(22);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_FormID, "Form ID", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 150, EMPTYSTR, false, false); //Form ID
			moDBFields.Add(_UserName, "User Name", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //User Name
			moDBFields.Add(__Name, "Name", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Name
			moDBFields.Add(_FieldId, "Field Id", 5, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 4000, EMPTYSTR, false, false); //Field Id
			moDBFields.Add(_Title, "Title", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, EMPTYSTR, false, false); //Title
			moDBFields.Add(_Width, "Width", 7, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Width
			moDBFields.Add(_Editable, "Editable", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Editable
			moDBFields.Add(_sType, "sType", 9, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 4000, EMPTYSTR, false, false); //sType
			moDBFields.Add(_iBackColor, "Back Color", 10, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Back Color
			moDBFields.Add(_oLinkObject, "oLinkObject", 11, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //oLinkObject
			moDBFields.Add(_bWidthIsMax, "bWidthIsMax", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //bWidthIsMax
			moDBFields.Add(_bPinned, "bPinned", 13, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //bPinned
			moDBFields.Add(_oSumType, "oSumType", 14, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //oSumType
			moDBFields.Add(_oDisplayType, "oDisplayType", 15, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //oDisplayType
			moDBFields.Add(_Index, "Order Index", 16, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Order Index
			moDBFields.Add(_sUID, "sUID", 17, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //sUID
			moDBFields.Add(_DeptCode, "DeptCode", 18, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //DeptCode
			moDBFields.Add(_FormName, "FormName", 19, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //FormName
			moDBFields.Add(_OrderBy, "Order By", 20, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 25, EMPTYSTR, false, false); //Order By
			moDBFields.Add(_GridName, "Grid Name", 21, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Grid Name

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_FormID());
            doBuildValidationString(doValidate_UserName());
            doBuildValidationString(doValidate__Name());
            doBuildValidationString(doValidate_FieldId());
            doBuildValidationString(doValidate_Title());
            doBuildValidationString(doValidate_Width());
            doBuildValidationString(doValidate_Editable());
            doBuildValidationString(doValidate_sType());
            doBuildValidationString(doValidate_iBackColor());
            doBuildValidationString(doValidate_oLinkObject());
            doBuildValidationString(doValidate_bWidthIsMax());
            doBuildValidationString(doValidate_bPinned());
            doBuildValidationString(doValidate_oSumType());
            doBuildValidationString(doValidate_oDisplayType());
            doBuildValidationString(doValidate_Index());
            doBuildValidationString(doValidate_sUID());
            doBuildValidationString(doValidate_DeptCode());
            doBuildValidationString(doValidate_FormName());
            doBuildValidationString(doValidate_OrderBy());
            doBuildValidationString(doValidate_GridName());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_FormID)) return doValidate_FormID(oValue);
            if (sFieldName.Equals(_UserName)) return doValidate_UserName(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_FieldId)) return doValidate_FieldId(oValue);
            if (sFieldName.Equals(_Title)) return doValidate_Title(oValue);
            if (sFieldName.Equals(_Width)) return doValidate_Width(oValue);
            if (sFieldName.Equals(_Editable)) return doValidate_Editable(oValue);
            if (sFieldName.Equals(_sType)) return doValidate_sType(oValue);
            if (sFieldName.Equals(_iBackColor)) return doValidate_iBackColor(oValue);
            if (sFieldName.Equals(_oLinkObject)) return doValidate_oLinkObject(oValue);
            if (sFieldName.Equals(_bWidthIsMax)) return doValidate_bWidthIsMax(oValue);
            if (sFieldName.Equals(_bPinned)) return doValidate_bPinned(oValue);
            if (sFieldName.Equals(_oSumType)) return doValidate_oSumType(oValue);
            if (sFieldName.Equals(_oDisplayType)) return doValidate_oDisplayType(oValue);
            if (sFieldName.Equals(_Index)) return doValidate_Index(oValue);
            if (sFieldName.Equals(_sUID)) return doValidate_sUID(oValue);
            if (sFieldName.Equals(_DeptCode)) return doValidate_DeptCode(oValue);
            if (sFieldName.Equals(_FormName)) return doValidate_FormName(oValue);
            if (sFieldName.Equals(_OrderBy)) return doValidate_OrderBy(oValue);
            if (sFieldName.Equals(_GridName)) return doValidate_GridName(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_FormID Field to the Form Item.
		 */
		public void doLink_FormID(string sControlName){
			moLinker.doLinkDataToControl(_FormID, sControlName);
		}
		/**
		 * Link the U_UserName Field to the Form Item.
		 */
		public void doLink_UserName(string sControlName){
			moLinker.doLinkDataToControl(_UserName, sControlName);
		}
		/**
		 * Link the U_Name Field to the Form Item.
		 */
		public void doLink__Name(string sControlName){
			moLinker.doLinkDataToControl(__Name, sControlName);
		}
		/**
		 * Link the U_FieldId Field to the Form Item.
		 */
		public void doLink_FieldId(string sControlName){
			moLinker.doLinkDataToControl(_FieldId, sControlName);
		}
		/**
		 * Link the U_Title Field to the Form Item.
		 */
		public void doLink_Title(string sControlName){
			moLinker.doLinkDataToControl(_Title, sControlName);
		}
		/**
		 * Link the U_Width Field to the Form Item.
		 */
		public void doLink_Width(string sControlName){
			moLinker.doLinkDataToControl(_Width, sControlName);
		}
		/**
		 * Link the U_Editable Field to the Form Item.
		 */
		public void doLink_Editable(string sControlName){
			moLinker.doLinkDataToControl(_Editable, sControlName);
		}
		/**
		 * Link the U_sType Field to the Form Item.
		 */
		public void doLink_sType(string sControlName){
			moLinker.doLinkDataToControl(_sType, sControlName);
		}
		/**
		 * Link the U_iBackColor Field to the Form Item.
		 */
		public void doLink_iBackColor(string sControlName){
			moLinker.doLinkDataToControl(_iBackColor, sControlName);
		}
		/**
		 * Link the U_oLinkObject Field to the Form Item.
		 */
		public void doLink_oLinkObject(string sControlName){
			moLinker.doLinkDataToControl(_oLinkObject, sControlName);
		}
		/**
		 * Link the U_bWidthIsMax Field to the Form Item.
		 */
		public void doLink_bWidthIsMax(string sControlName){
			moLinker.doLinkDataToControl(_bWidthIsMax, sControlName);
		}
		/**
		 * Link the U_bPinned Field to the Form Item.
		 */
		public void doLink_bPinned(string sControlName){
			moLinker.doLinkDataToControl(_bPinned, sControlName);
		}
		/**
		 * Link the U_oSumType Field to the Form Item.
		 */
		public void doLink_oSumType(string sControlName){
			moLinker.doLinkDataToControl(_oSumType, sControlName);
		}
		/**
		 * Link the U_oDisplayType Field to the Form Item.
		 */
		public void doLink_oDisplayType(string sControlName){
			moLinker.doLinkDataToControl(_oDisplayType, sControlName);
		}
		/**
		 * Link the U_Index Field to the Form Item.
		 */
		public void doLink_Index(string sControlName){
			moLinker.doLinkDataToControl(_Index, sControlName);
		}
		/**
		 * Link the U_sUID Field to the Form Item.
		 */
		public void doLink_sUID(string sControlName){
			moLinker.doLinkDataToControl(_sUID, sControlName);
		}
		/**
		 * Link the U_DeptCode Field to the Form Item.
		 */
		public void doLink_DeptCode(string sControlName){
			moLinker.doLinkDataToControl(_DeptCode, sControlName);
		}
		/**
		 * Link the U_FormName Field to the Form Item.
		 */
		public void doLink_FormName(string sControlName){
			moLinker.doLinkDataToControl(_FormName, sControlName);
		}
		/**
		 * Link the U_OrderBy Field to the Form Item.
		 */
		public void doLink_OrderBy(string sControlName){
			moLinker.doLinkDataToControl(_OrderBy, sControlName);
		}
		/**
		 * Link the U_GridName Field to the Form Item.
		 */
		public void doLink_GridName(string sControlName){
			moLinker.doLinkDataToControl(_GridName, sControlName);
		}
#endregion

	}
}
