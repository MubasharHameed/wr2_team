/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 04/02/2016 17:40:38
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
   [Serializable] 
	public class IDH_BITMDTL: com.idh.dbObjects.Base.IDH_BITMDTL{ 

		public IDH_BITMDTL() : base() {
			
		}

   	public IDH_BITMDTL( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	}
	}
}
