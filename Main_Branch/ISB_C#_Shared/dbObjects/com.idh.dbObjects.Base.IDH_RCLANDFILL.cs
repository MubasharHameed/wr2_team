/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 21/07/2015 18:19:29
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
	public class IDH_RCLANDFILL: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
		protected IDHAddOns.idh.forms.Base moIDHForm;

		public IDH_RCLANDFILL() : base("@IDH_RCLANDFILL"){
		}

		public IDH_RCLANDFILL( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_RCLANDFILL"){
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_RCLANDFILL";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: RouteCL Header
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ROUTECL
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ROUTECL = "U_ROUTECL";
		public string U_ROUTECL { 
			get {
 				return getValueAsString(_ROUTECL); 
			}
			set { setValue(_ROUTECL, value); }
		}
           public string doValidate_ROUTECL() {
               return doValidate_ROUTECL(U_ROUTECL);
           }
           public virtual string doValidate_ROUTECL(object oValue) {
               return base.doValidation(_ROUTECL, oValue);
           }

		/**
		 * Decription: On Route
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCONROUTE
		 * Size: 6
		 * Type: db_Date
		 * CType: string
		 * SubType: st_Time
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _RCONROUTE = "U_RCONROUTE";
		public string U_RCONROUTE { 
			get {
 				return getValueAsString(_RCONROUTE); 
			}
			set { setValue(_RCONROUTE, value); }
		}
           public string doValidate_RCONROUTE() {
               return doValidate_RCONROUTE(U_RCONROUTE);
           }
           public virtual string doValidate_RCONROUTE(object oValue) {
               return base.doValidation(_RCONROUTE, oValue);
           }

		/**
		 * Decription: Off Route
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCOFFROUTE
		 * Size: 6
		 * Type: db_Date
		 * CType: string
		 * SubType: st_Time
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _RCOFFROUTE = "U_RCOFFROUTE";
		public string U_RCOFFROUTE { 
			get {
 				return getValueAsString(_RCOFFROUTE); 
			}
			set { setValue(_RCOFFROUTE, value); }
		}
           public string doValidate_RCOFFROUTE() {
               return doValidate_RCOFFROUTE(U_RCOFFROUTE);
           }
           public virtual string doValidate_RCOFFROUTE(object oValue) {
               return base.doValidation(_RCOFFROUTE, oValue);
           }

		/**
		 * Decription: Load Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCLOADTP
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RCLOADTP = "U_RCLOADTP";
		public string U_RCLOADTP { 
			get {
 				return getValueAsString(_RCLOADTP); 
			}
			set { setValue(_RCLOADTP, value); }
		}
           public string doValidate_RCLOADTP() {
               return doValidate_RCLOADTP(U_RCLOADTP);
           }
           public virtual string doValidate_RCLOADTP(object oValue) {
               return base.doValidation(_RCLOADTP, oValue);
           }

		/**
		 * Decription: Site
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCSITE
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RCSITE = "U_RCSITE";
		public string U_RCSITE { 
			get {
 				return getValueAsString(_RCSITE); 
			}
			set { setValue(_RCSITE, value); }
		}
           public string doValidate_RCSITE() {
               return doValidate_RCSITE(U_RCSITE);
           }
           public virtual string doValidate_RCSITE(object oValue) {
               return base.doValidation(_RCSITE, oValue);
           }

		/**
		 * Decription: Time In
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCTIMEIN
		 * Size: 6
		 * Type: db_Date
		 * CType: string
		 * SubType: st_Time
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _RCTIMEIN = "U_RCTIMEIN";
		public string U_RCTIMEIN { 
			get {
 				return getValueAsString(_RCTIMEIN); 
			}
			set { setValue(_RCTIMEIN, value); }
		}
           public string doValidate_RCTIMEIN() {
               return doValidate_RCTIMEIN(U_RCTIMEIN);
           }
           public virtual string doValidate_RCTIMEIN(object oValue) {
               return base.doValidation(_RCTIMEIN, oValue);
           }

		/**
		 * Decription: Ticket #
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCTKTNO
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RCTKTNO = "U_RCTKTNO";
		public string U_RCTKTNO { 
			get {
 				return getValueAsString(_RCTKTNO); 
			}
			set { setValue(_RCTKTNO, value); }
		}
           public string doValidate_RCTKTNO() {
               return doValidate_RCTKTNO(U_RCTKTNO);
           }
           public virtual string doValidate_RCTKTNO(object oValue) {
               return base.doValidation(_RCTKTNO, oValue);
           }

		/**
		 * Decription: Tons
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCTONS
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _RCTONS = "U_RCTONS";
		public short U_RCTONS { 
			get {
 				return getValueAsShort(_RCTONS); 
			}
			set { setValue(_RCTONS, value); }
		}
           public string doValidate_RCTONS() {
               return doValidate_RCTONS(U_RCTONS);
           }
           public virtual string doValidate_RCTONS(object oValue) {
               return base.doValidation(_RCTONS, oValue);
           }

		/**
		 * Decription: Total Cost
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCTCOST
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _RCTCOST = "U_RCTCOST";
		public short U_RCTCOST { 
			get {
 				return getValueAsShort(_RCTCOST); 
			}
			set { setValue(_RCTCOST, value); }
		}
           public string doValidate_RCTCOST() {
               return doValidate_RCTCOST(U_RCTCOST);
           }
           public virtual string doValidate_RCTCOST(object oValue) {
               return base.doValidation(_RCTCOST, oValue);
           }

		/**
		 * Decription: Time Out
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCTIMEOUT
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Percentage
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _RCTIMEOUT = "U_RCTIMEOUT";
		public double U_RCTIMEOUT { 
			get {
 				return getValueAsDouble(_RCTIMEOUT); 
			}
			set { setValue(_RCTIMEOUT, value); }
		}
           public string doValidate_RCTIMEOUT() {
               return doValidate_RCTIMEOUT(U_RCTIMEOUT);
           }
           public virtual string doValidate_RCTIMEOUT(object oValue) {
               return base.doValidation(_RCTIMEOUT, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(12);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_ROUTECL, "RouteCL Header", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //RouteCL Header
			moDBFields.Add(_RCONROUTE, "On Route", 3, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //On Route
			moDBFields.Add(_RCOFFROUTE, "Off Route", 4, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Off Route
			moDBFields.Add(_RCLOADTP, "Load Type", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Load Type
			moDBFields.Add(_RCSITE, "Site", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Site
			moDBFields.Add(_RCTIMEIN, "Time In", 7, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Time In
			moDBFields.Add(_RCTKTNO, "Ticket #", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Ticket #
			moDBFields.Add(_RCTONS, "Tons", 9, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Tons
			moDBFields.Add(_RCTCOST, "Total Cost", 10, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Total Cost
			moDBFields.Add(_RCTIMEOUT, "Time Out", 11, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 20, 0, false, false); //Time Out

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_ROUTECL());
            doBuildValidationString(doValidate_RCONROUTE());
            doBuildValidationString(doValidate_RCOFFROUTE());
            doBuildValidationString(doValidate_RCLOADTP());
            doBuildValidationString(doValidate_RCSITE());
            doBuildValidationString(doValidate_RCTIMEIN());
            doBuildValidationString(doValidate_RCTKTNO());
            doBuildValidationString(doValidate_RCTONS());
            doBuildValidationString(doValidate_RCTCOST());
            doBuildValidationString(doValidate_RCTIMEOUT());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_ROUTECL)) return doValidate_ROUTECL(oValue);
            if (sFieldName.Equals(_RCONROUTE)) return doValidate_RCONROUTE(oValue);
            if (sFieldName.Equals(_RCOFFROUTE)) return doValidate_RCOFFROUTE(oValue);
            if (sFieldName.Equals(_RCLOADTP)) return doValidate_RCLOADTP(oValue);
            if (sFieldName.Equals(_RCSITE)) return doValidate_RCSITE(oValue);
            if (sFieldName.Equals(_RCTIMEIN)) return doValidate_RCTIMEIN(oValue);
            if (sFieldName.Equals(_RCTKTNO)) return doValidate_RCTKTNO(oValue);
            if (sFieldName.Equals(_RCTONS)) return doValidate_RCTONS(oValue);
            if (sFieldName.Equals(_RCTCOST)) return doValidate_RCTCOST(oValue);
            if (sFieldName.Equals(_RCTIMEOUT)) return doValidate_RCTIMEOUT(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_ROUTECL Field to the Form Item.
		 */
		public void doLink_ROUTECL(string sControlName){
			moLinker.doLinkDataToControl(_ROUTECL, sControlName);
		}
		/**
		 * Link the U_RCONROUTE Field to the Form Item.
		 */
		public void doLink_RCONROUTE(string sControlName){
			moLinker.doLinkDataToControl(_RCONROUTE, sControlName);
		}
		/**
		 * Link the U_RCOFFROUTE Field to the Form Item.
		 */
		public void doLink_RCOFFROUTE(string sControlName){
			moLinker.doLinkDataToControl(_RCOFFROUTE, sControlName);
		}
		/**
		 * Link the U_RCLOADTP Field to the Form Item.
		 */
		public void doLink_RCLOADTP(string sControlName){
			moLinker.doLinkDataToControl(_RCLOADTP, sControlName);
		}
		/**
		 * Link the U_RCSITE Field to the Form Item.
		 */
		public void doLink_RCSITE(string sControlName){
			moLinker.doLinkDataToControl(_RCSITE, sControlName);
		}
		/**
		 * Link the U_RCTIMEIN Field to the Form Item.
		 */
		public void doLink_RCTIMEIN(string sControlName){
			moLinker.doLinkDataToControl(_RCTIMEIN, sControlName);
		}
		/**
		 * Link the U_RCTKTNO Field to the Form Item.
		 */
		public void doLink_RCTKTNO(string sControlName){
			moLinker.doLinkDataToControl(_RCTKTNO, sControlName);
		}
		/**
		 * Link the U_RCTONS Field to the Form Item.
		 */
		public void doLink_RCTONS(string sControlName){
			moLinker.doLinkDataToControl(_RCTONS, sControlName);
		}
		/**
		 * Link the U_RCTCOST Field to the Form Item.
		 */
		public void doLink_RCTCOST(string sControlName){
			moLinker.doLinkDataToControl(_RCTCOST, sControlName);
		}
		/**
		 * Link the U_RCTIMEOUT Field to the Form Item.
		 */
		public void doLink_RCTIMEOUT(string sControlName){
			moLinker.doLinkDataToControl(_RCTIMEOUT, sControlName);
		}
#endregion

	}
}
