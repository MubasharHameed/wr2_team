/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 17/03/2015 09:56:48
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base
{
    public class IDH_ROUTECL : com.idh.dbObjects.DBBase
    {

        //private Linker moLinker = null;
        protected IDHAddOns.idh.forms.Base moIDHForm;

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX
        {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }


        public IDH_ROUTECL() : base("@IDH_ROUTECL")
        {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_ROUTECL(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm) : base(oForm, "@IDH_ROUTECL")
        {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
		* Table name
		*/
        public readonly static string TableName = "@IDH_ROUTECL";

        /**
         * Decription: Code
         * Mandatory: tYes
         * Name: Code
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Code = "Code";
        public string Code
        {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code()
        {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue)
        {
            return base.doValidation(_Code, oValue);
        }
        /**
         * Decription: Name
         * Mandatory: tYes
         * Name: Name
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Name = "Name";
        public string Name
        {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name()
        {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue)
        {
            return base.doValidation(_Name, oValue);
        }
        /**
         * Decription: Line of Business
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCLOB
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _RCLOB = "U_RCLOB";
        public string U_RCLOB
        {
            get
            {
                return getValueAsString(_RCLOB);
            }
            set { setValue(_RCLOB, value); }
        }
        public string doValidate_RCLOB()
        {
            return doValidate_RCLOB(U_RCLOB);
        }
        public virtual string doValidate_RCLOB(object oValue)
        {
            return base.doValidation(_RCLOB, oValue);
        }

        /**
         * Decription: Route Sequenced
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCRSEQ
         * Size: 1
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _RCRSEQ = "U_RCRSEQ";
        public string U_RCRSEQ
        {
            get
            {
                return getValueAsString(_RCRSEQ);
            }
            set { setValue(_RCRSEQ, value); }
        }
        public string doValidate_RCRSEQ()
        {
            return doValidate_RCRSEQ(U_RCRSEQ);
        }
        public virtual string doValidate_RCRSEQ(object oValue)
        {
            return base.doValidation(_RCRSEQ, oValue);
        }

        /**
         * Decription: DOT Log Sheet
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCDOTLS
         * Size: 1
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _RCDOTLS = "U_RCDOTLS";
        public string U_RCDOTLS
        {
            get
            {
                return getValueAsString(_RCDOTLS);
            }
            set { setValue(_RCDOTLS, value); }
        }
        public string doValidate_RCDOTLS()
        {
            return doValidate_RCDOTLS(U_RCDOTLS);
        }
        public virtual string doValidate_RCDOTLS(object oValue)
        {
            return base.doValidation(_RCDOTLS, oValue);
        }

        /**
         * Decription: Date
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCDATE
         * Size: 8
         * Type: db_Date
         * CType: DateTime
         * SubType: st_None
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RCDATE = "U_RCDATE";
        public DateTime U_RCDATE
        {
            get
            {
                return getValueAsDateTime(_RCDATE);
            }
            set { setValue(_RCDATE, value); }
        }
        public void U_RCDATE_AsString(string value)
        {
            setValue("U_RCDATE", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_RCDATE_AsString()
        {
            DateTime dVal = getValueAsDateTime(_RCDATE);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_RCDATE()
        {
            return doValidate_RCDATE(U_RCDATE);
        }
        public virtual string doValidate_RCDATE(object oValue)
        {
            return base.doValidation(_RCDATE, oValue);
        }

        /**
         * Decription: Truck #
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCTRUCK
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _RCTRUCK = "U_RCTRUCK";
        public string U_RCTRUCK
        {
            get
            {
                return getValueAsString(_RCTRUCK);
            }
            set { setValue(_RCTRUCK, value); }
        }
        public string doValidate_RCTRUCK()
        {
            return doValidate_RCTRUCK(U_RCTRUCK);
        }
        public virtual string doValidate_RCTRUCK(object oValue)
        {
            return base.doValidation(_RCTRUCK, oValue);
        }

        /**
         * Decription: Route #
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCROUTE
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _RCROUTE = "U_RCROUTE";
        public string U_RCROUTE
        {
            get
            {
                return getValueAsString(_RCROUTE);
            }
            set { setValue(_RCROUTE, value); }
        }
        public string doValidate_RCROUTE()
        {
            return doValidate_RCROUTE(U_RCROUTE);
        }
        public virtual string doValidate_RCROUTE(object oValue)
        {
            return base.doValidation(_RCROUTE, oValue);
        }

        /**
         * Decription: Primary Route
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCPROUTE
         * Size: 1
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _RCPROUTE = "U_RCPROUTE";
        public string U_RCPROUTE
        {
            get
            {
                return getValueAsString(_RCPROUTE);
            }
            set { setValue(_RCPROUTE, value); }
        }
        public string doValidate_RCPROUTE()
        {
            return doValidate_RCPROUTE(U_RCPROUTE);
        }
        public virtual string doValidate_RCPROUTE(object oValue)
        {
            return base.doValidation(_RCPROUTE, oValue);
        }

        /**
         * Decription: Fuel
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCFUEL
         * Size: 6
         * Type: db_Numeric
         * CType: short
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _RCFUEL = "U_RCFUEL";
        public short U_RCFUEL
        {
            get
            {
                return getValueAsShort(_RCFUEL);
            }
            set { setValue(_RCFUEL, value); }
        }
        public string doValidate_RCFUEL()
        {
            return doValidate_RCFUEL(U_RCFUEL);
        }
        public virtual string doValidate_RCFUEL(object oValue)
        {
            return base.doValidation(_RCFUEL, oValue);
        }

        /**
         * Decription: Ext. Yards
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCEXTYD
         * Size: 6
         * Type: db_Numeric
         * CType: short
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _RCEXTYD = "U_RCEXTYD";
        public short U_RCEXTYD
        {
            get
            {
                return getValueAsShort(_RCEXTYD);
            }
            set { setValue(_RCEXTYD, value); }
        }
        public string doValidate_RCEXTYD()
        {
            return doValidate_RCEXTYD(U_RCEXTYD);
        }
        public virtual string doValidate_RCEXTYD(object oValue)
        {
            return base.doValidation(_RCEXTYD, oValue);
        }

        /**
         * Decription: Int. Yards
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCINTYD
         * Size: 6
         * Type: db_Numeric
         * CType: short
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _RCINTYD = "U_RCINTYD";
        public short U_RCINTYD
        {
            get
            {
                return getValueAsShort(_RCINTYD);
            }
            set { setValue(_RCINTYD, value); }
        }
        public string doValidate_RCINTYD()
        {
            return doValidate_RCINTYD(U_RCINTYD);
        }
        public virtual string doValidate_RCINTYD(object oValue)
        {
            return base.doValidation(_RCINTYD, oValue);
        }

        /**
         * Decription: Total Stops
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCTSTOP1
         * Size: 6
         * Type: db_Numeric
         * CType: short
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _RCTSTOP1 = "U_RCTSTOP1";
        public short U_RCTSTOP1
        {
            get
            {
                return getValueAsShort(_RCTSTOP1);
            }
            set { setValue(_RCTSTOP1, value); }
        }
        public string doValidate_RCTSTOP1()
        {
            return doValidate_RCTSTOP1(U_RCTSTOP1);
        }
        public virtual string doValidate_RCTSTOP1(object oValue)
        {
            return base.doValidation(_RCTSTOP1, oValue);
        }

        /**
         * Decription: Planned Homes
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCEXTHOME
         * Size: 6
         * Type: db_Numeric
         * CType: short
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _RCEXTHOME = "U_RCEXTHOME";
        public short U_RCEXTHOME
        {
            get
            {
                return getValueAsShort(_RCEXTHOME);
            }
            set { setValue(_RCEXTHOME, value); }
        }
        public string doValidate_RCEXTHOME()
        {
            return doValidate_RCEXTHOME(U_RCEXTHOME);
        }
        public virtual string doValidate_RCEXTHOME(object oValue)
        {
            return base.doValidation(_RCEXTHOME, oValue);
        }

        /**
         * Decription: Collected Homes
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCINTHOME
         * Size: 6
         * Type: db_Numeric
         * CType: short
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _RCINTHOME = "U_RCINTHOME";
        public short U_RCINTHOME
        {
            get
            {
                return getValueAsShort(_RCINTHOME);
            }
            set { setValue(_RCINTHOME, value); }
        }
        public string doValidate_RCINTHOME()
        {
            return doValidate_RCINTHOME(U_RCINTHOME);
        }
        public virtual string doValidate_RCINTHOME(object oValue)
        {
            return base.doValidation(_RCINTHOME, oValue);
        }

        /**
         * Decription: Tanks
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCTANK
         * Size: 6
         * Type: db_Numeric
         * CType: short
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _RCTANK = "U_RCTANK";
        public short U_RCTANK
        {
            get
            {
                return getValueAsShort(_RCTANK);
            }
            set { setValue(_RCTANK, value); }
        }
        public string doValidate_RCTANK()
        {
            return doValidate_RCTANK(U_RCTANK);
        }
        public virtual string doValidate_RCTANK(object oValue)
        {
            return base.doValidation(_RCTANK, oValue);
        }

        /**
         * Decription: Driver
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCDRIVER
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _RCDRIVER = "U_RCDRIVER";
        public string U_RCDRIVER
        {
            get
            {
                return getValueAsString(_RCDRIVER);
            }
            set { setValue(_RCDRIVER, value); }
        }
        public string doValidate_RCDRIVER()
        {
            return doValidate_RCDRIVER(U_RCDRIVER);
        }
        public virtual string doValidate_RCDRIVER(object oValue)
        {
            return base.doValidation(_RCDRIVER, oValue);
        }

        /**
         * Decription: Clock In
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCCLOCKIN
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RCCLOCKIN = "U_RCCLOCKIN";
        public string U_RCCLOCKIN
        {
            get
            {
                return getValueAsString(_RCCLOCKIN);
            }
            set { setValue(_RCCLOCKIN, value); }
        }
        public string doValidate_RCCLOCKIN()
        {
            return doValidate_RCCLOCKIN(U_RCCLOCKIN);
        }
        public virtual string doValidate_RCCLOCKIN(object oValue)
        {
            return base.doValidation(_RCCLOCKIN, oValue);
        }

        /**
         * Decription: Clock Out
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCCLOCKOUT
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RCCLOCKOUT = "U_RCCLOCKOUT";
        public string U_RCCLOCKOUT
        {
            get
            {
                return getValueAsString(_RCCLOCKOUT);
            }
            set { setValue(_RCCLOCKOUT, value); }
        }
        public string doValidate_RCCLOCKOUT()
        {
            return doValidate_RCCLOCKOUT(U_RCCLOCKOUT);
        }
        public virtual string doValidate_RCCLOCKOUT(object oValue)
        {
            return base.doValidation(_RCCLOCKOUT, oValue);
        }

        /**
         * Decription: Total Hours
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCTHOUR1
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RCTHOUR1 = "U_RCTHOUR1";
        public string U_RCTHOUR1
        {
            get
            {
                return getValueAsString(_RCTHOUR1);
            }
            set { setValue(_RCTHOUR1, value); }
        }
        public string doValidate_RCTHOUR1()
        {
            return doValidate_RCTHOUR1(U_RCTHOUR1);
        }
        public virtual string doValidate_RCTHOUR1(object oValue)
        {
            return base.doValidation(_RCTHOUR1, oValue);
        }

        /**
         * Decription: Plan Clock In Time
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCPLANCIT
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RCPLANCIT = "U_RCPLANCIT";
        public string U_RCPLANCIT
        {
            get
            {
                return getValueAsString(_RCPLANCIT);
            }
            set { setValue(_RCPLANCIT, value); }
        }
        public string doValidate_RCPLANCIT()
        {
            return doValidate_RCPLANCIT(U_RCPLANCIT);
        }
        public virtual string doValidate_RCPLANCIT(object oValue)
        {
            return base.doValidation(_RCPLANCIT, oValue);
        }

        /**
         * Decription: Plan Clock Out Time
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCPLANCOT
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RCPLANCOT = "U_RCPLANCOT";
        public string U_RCPLANCOT
        {
            get
            {
                return getValueAsString(_RCPLANCOT);
            }
            set { setValue(_RCPLANCOT, value); }
        }
        public string doValidate_RCPLANCOT()
        {
            return doValidate_RCPLANCOT(U_RCPLANCOT);
        }
        public virtual string doValidate_RCPLANCOT(object oValue)
        {
            return base.doValidation(_RCPLANCOT, oValue);
        }

        /**
         * Decription: Plan Yards
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCPLANYDS
         * Size: 6
         * Type: db_Numeric
         * CType: short
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _RCPLANYDS = "U_RCPLANYDS";
        public short U_RCPLANYDS
        {
            get
            {
                return getValueAsShort(_RCPLANYDS);
            }
            set { setValue(_RCPLANYDS, value); }
        }
        public string doValidate_RCPLANYDS()
        {
            return doValidate_RCPLANYDS(U_RCPLANYDS);
        }
        public virtual string doValidate_RCPLANYDS(object oValue)
        {
            return base.doValidation(_RCPLANYDS, oValue);
        }

        /**
         * Decription: Total Hours
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCTHOUR2
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RCTHOUR2 = "U_RCTHOUR2";
        public string U_RCTHOUR2
        {
            get
            {
                return getValueAsString(_RCTHOUR2);
            }
            set { setValue(_RCTHOUR2, value); }
        }
        public string doValidate_RCTHOUR2()
        {
            return doValidate_RCTHOUR2(U_RCTHOUR2);
        }
        public virtual string doValidate_RCTHOUR2(object oValue)
        {
            return base.doValidation(_RCTHOUR2, oValue);
        }

        /**
         * Decription: Time Out of Yard
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCTIMEOY
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RCTIMEOY = "U_RCTIMEOY";
        public string U_RCTIMEOY
        {
            get
            {
                return getValueAsString(_RCTIMEOY);
            }
            set { setValue(_RCTIMEOY, value); }
        }
        public string doValidate_RCTIMEOY()
        {
            return doValidate_RCTIMEOY(U_RCTIMEOY);
        }
        public virtual string doValidate_RCTIMEOY(object oValue)
        {
            return base.doValidation(_RCTIMEOY, oValue);
        }

        /**
         * Decription: Time into Yard
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCTIMEIY
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RCTIMEIY = "U_RCTIMEIY";
        public string U_RCTIMEIY
        {
            get
            {
                return getValueAsString(_RCTIMEIY);
            }
            set { setValue(_RCTIMEIY, value); }
        }
        public string doValidate_RCTIMEIY()
        {
            return doValidate_RCTIMEIY(U_RCTIMEIY);
        }
        public virtual string doValidate_RCTIMEIY(object oValue)
        {
            return base.doValidation(_RCTIMEIY, oValue);
        }

        /**
         * Decription: Total Hours
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCTHOUR3
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RCTHOUR3 = "U_RCTHOUR3";
        public string U_RCTHOUR3
        {
            get
            {
                return getValueAsString(_RCTHOUR3);
            }
            set { setValue(_RCTHOUR3, value); }
        }
        public string doValidate_RCTHOUR3()
        {
            return doValidate_RCTHOUR3(U_RCTHOUR3);
        }
        public virtual string doValidate_RCTHOUR3(object oValue)
        {
            return base.doValidation(_RCTHOUR3, oValue);
        }

        /**
         * Decription: Mileage Start
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCMILESRT
         * Size: 6
         * Type: db_Numeric
         * CType: short
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _RCMILESRT = "U_RCMILESRT";
        public short U_RCMILESRT
        {
            get
            {
                return getValueAsShort(_RCMILESRT);
            }
            set { setValue(_RCMILESRT, value); }
        }
        public string doValidate_RCMILESRT()
        {
            return doValidate_RCMILESRT(U_RCMILESRT);
        }
        public virtual string doValidate_RCMILESRT(object oValue)
        {
            return base.doValidation(_RCMILESRT, oValue);
        }

        /**
         * Decription: Mileage End
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCMILEEND
         * Size: 6
         * Type: db_Numeric
         * CType: short
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _RCMILEEND = "U_RCMILEEND";
        public short U_RCMILEEND
        {
            get
            {
                return getValueAsShort(_RCMILEEND);
            }
            set { setValue(_RCMILEEND, value); }
        }
        public string doValidate_RCMILEEND()
        {
            return doValidate_RCMILEEND(U_RCMILEEND);
        }
        public virtual string doValidate_RCMILEEND(object oValue)
        {
            return base.doValidation(_RCMILEEND, oValue);
        }

        /**
         * Decription: Total Mileage
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCTMILE
         * Size: 6
         * Type: db_Numeric
         * CType: short
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _RCTMILE = "U_RCTMILE";
        public short U_RCTMILE
        {
            get
            {
                return getValueAsShort(_RCTMILE);
            }
            set { setValue(_RCTMILE, value); }
        }
        public string doValidate_RCTMILE()
        {
            return doValidate_RCTMILE(U_RCTMILE);
        }
        public virtual string doValidate_RCTMILE(object oValue)
        {
            return base.doValidation(_RCTMILE, oValue);
        }

        /**
         * Decription: Admin Reason
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCADMINR
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _RCADMINR = "U_RCADMINR";
        public string U_RCADMINR
        {
            get
            {
                return getValueAsString(_RCADMINR);
            }
            set { setValue(_RCADMINR, value); }
        }
        public string doValidate_RCADMINR()
        {
            return doValidate_RCADMINR(U_RCADMINR);
        }
        public virtual string doValidate_RCADMINR(object oValue)
        {
            return base.doValidation(_RCADMINR, oValue);
        }

        /**
         * Decription: Unpaid/Admin/Meeting Hrs
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCADMINH
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RCADMINH = "U_RCADMINH";
        public string U_RCADMINH
        {
            get
            {
                return getValueAsString(_RCADMINH);
            }
            set { setValue(_RCADMINH, value); }
        }
        public string doValidate_RCADMINH()
        {
            return doValidate_RCADMINH(U_RCADMINH);
        }
        public virtual string doValidate_RCADMINH(object oValue)
        {
            return base.doValidation(_RCADMINH, oValue);
        }

        /**
         * Decription: Safety Meeting
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCSAFEMT
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RCSAFEMT = "U_RCSAFEMT";
        public string U_RCSAFEMT
        {
            get
            {
                return getValueAsString(_RCSAFEMT);
            }
            set { setValue(_RCSAFEMT, value); }
        }
        public string doValidate_RCSAFEMT()
        {
            return doValidate_RCSAFEMT(U_RCSAFEMT);
        }
        public virtual string doValidate_RCSAFEMT(object oValue)
        {
            return base.doValidation(_RCSAFEMT, oValue);
        }

        /**
         * Decription: Meal Start
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCMEALS
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RCMEALS = "U_RCMEALS";
        public string U_RCMEALS
        {
            get
            {
                return getValueAsString(_RCMEALS);
            }
            set { setValue(_RCMEALS, value); }
        }
        public string doValidate_RCMEALS()
        {
            return doValidate_RCMEALS(U_RCMEALS);
        }
        public virtual string doValidate_RCMEALS(object oValue)
        {
            return base.doValidation(_RCMEALS, oValue);
        }

        /**
         * Decription: Meal End
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCMEALE
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RCMEALE = "U_RCMEALE";
        public string U_RCMEALE
        {
            get
            {
                return getValueAsString(_RCMEALE);
            }
            set { setValue(_RCMEALE, value); }
        }
        public string doValidate_RCMEALE()
        {
            return doValidate_RCMEALE(U_RCMEALE);
        }
        public virtual string doValidate_RCMEALE(object oValue)
        {
            return base.doValidation(_RCMEALE, oValue);
        }

        /**
         * Decription: Break #1
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCBREAK1
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RCBREAK1 = "U_RCBREAK1";
        public string U_RCBREAK1
        {
            get
            {
                return getValueAsString(_RCBREAK1);
            }
            set { setValue(_RCBREAK1, value); }
        }
        public string doValidate_RCBREAK1()
        {
            return doValidate_RCBREAK1(U_RCBREAK1);
        }
        public virtual string doValidate_RCBREAK1(object oValue)
        {
            return base.doValidation(_RCBREAK1, oValue);
        }

        /**
         * Decription: Break #2
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCBREAK2
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RCBREAK2 = "U_RCBREAK2";
        public string U_RCBREAK2
        {
            get
            {
                return getValueAsString(_RCBREAK2);
            }
            set { setValue(_RCBREAK2, value); }
        }
        public string doValidate_RCBREAK2()
        {
            return doValidate_RCBREAK2(U_RCBREAK2);
        }
        public virtual string doValidate_RCBREAK2(object oValue)
        {
            return base.doValidation(_RCBREAK2, oValue);
        }

        /**
         * Decription: Comments
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCCOMM
         * Size: 10
         * Type: db_Memo
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _RCCOMM = "U_RCCOMM";
        public string U_RCCOMM
        {
            get
            {
                return getValueAsString(_RCCOMM);
            }
            set { setValue(_RCCOMM, value); }
        }
        public string doValidate_RCCOMM()
        {
            return doValidate_RCCOMM(U_RCCOMM);
        }
        public virtual string doValidate_RCCOMM(object oValue)
        {
            return base.doValidation(_RCCOMM, oValue);
        }

        /**
         * Decription: Total Stops/Pulls/Hauls
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCTSTOP2
         * Size: 6
         * Type: db_Numeric
         * CType: short
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _RCTSTOP2 = "U_RCTSTOP2";
        public short U_RCTSTOP2
        {
            get
            {
                return getValueAsShort(_RCTSTOP2);
            }
            set { setValue(_RCTSTOP2, value); }
        }
        public string doValidate_RCTSTOP2()
        {
            return doValidate_RCTSTOP2(U_RCTSTOP2);
        }
        public virtual string doValidate_RCTSTOP2(object oValue)
        {
            return base.doValidation(_RCTSTOP2, oValue);
        }

        /**
         * Decription: Total Containers
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCTCONTS
         * Size: 6
         * Type: db_Numeric
         * CType: short
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _RCTCONTS = "U_RCTCONTS";
        public short U_RCTCONTS
        {
            get
            {
                return getValueAsShort(_RCTCONTS);
            }
            set { setValue(_RCTCONTS, value); }
        }
        public string doValidate_RCTCONTS()
        {
            return doValidate_RCTCONTS(U_RCTCONTS);
        }
        public virtual string doValidate_RCTCONTS(object oValue)
        {
            return base.doValidation(_RCTCONTS, oValue);
        }

        /**
         * Decription: Total Yards
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCTYARDS
         * Size: 6
         * Type: db_Numeric
         * CType: short
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _RCTYARDS = "U_RCTYARDS";
        public short U_RCTYARDS
        {
            get
            {
                return getValueAsShort(_RCTYARDS);
            }
            set { setValue(_RCTYARDS, value); }
        }
        public string doValidate_RCTYARDS()
        {
            return doValidate_RCTYARDS(U_RCTYARDS);
        }
        public virtual string doValidate_RCTYARDS(object oValue)
        {
            return base.doValidation(_RCTYARDS, oValue);
        }

        /**
         * Decription: Total Totals
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCTONS
         * Size: 6
         * Type: db_Numeric
         * CType: short
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _RCTONS = "U_RCTONS";
        public short U_RCTONS
        {
            get
            {
                return getValueAsShort(_RCTONS);
            }
            set { setValue(_RCTONS, value); }
        }
        public string doValidate_RCTONS()
        {
            return doValidate_RCTONS(U_RCTONS);
        }
        public virtual string doValidate_RCTONS(object oValue)
        {
            return base.doValidation(_RCTONS, oValue);
        }

        /**
         * Decription: Total Lifts
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RCTLIFT
         * Size: 6
         * Type: db_Numeric
         * CType: short
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _RCTLIFT = "U_RCTLIFT";
        public short U_RCTLIFT
        {
            get
            {
                return getValueAsShort(_RCTLIFT);
            }
            set { setValue(_RCTLIFT, value); }
        }
        public string doValidate_RCTLIFT()
        {
            return doValidate_RCTLIFT(U_RCTLIFT);
        }
        public virtual string doValidate_RCTLIFT(object oValue)
        {
            return base.doValidation(_RCTLIFT, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo()
        {
            if (moDBFields == null)
                moDBFields = new DBFields(43);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_RCLOB, "Line of Business", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Line of Business
            moDBFields.Add(_RCRSEQ, "Route Sequenced", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Route Sequenced
            moDBFields.Add(_RCDOTLS, "DOT Log Sheet", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //DOT Log Sheet
            moDBFields.Add(_RCDATE, "Date", 5, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Date
            moDBFields.Add(_RCTRUCK, "Truck #", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Truck #
            moDBFields.Add(_RCROUTE, "Route #", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Route #
            moDBFields.Add(_RCPROUTE, "Primary Route", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Primary Route
            moDBFields.Add(_RCFUEL, "Fuel", 9, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Fuel
            moDBFields.Add(_RCEXTYD, "Ext. Yards", 10, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Ext. Yards
            moDBFields.Add(_RCINTYD, "Int. Yards", 11, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Int. Yards
            moDBFields.Add(_RCTSTOP1, "Total Stops", 12, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Total Stops
            moDBFields.Add(_RCEXTHOME, "Planned Homes", 13, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Planned Homes
            moDBFields.Add(_RCINTHOME, "Collected Homes", 14, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Collected Homes
            moDBFields.Add(_RCTANK, "Tanks", 15, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Tanks
            moDBFields.Add(_RCDRIVER, "Driver", 16, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Driver
            moDBFields.Add(_RCCLOCKIN, "Clock In", 17, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Clock In
            moDBFields.Add(_RCCLOCKOUT, "Clock Out", 18, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Clock Out
            moDBFields.Add(_RCTHOUR1, "Total Hours", 19, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Total Hours
            moDBFields.Add(_RCPLANCIT, "Plan Clock In Time", 20, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Plan Clock In Time
            moDBFields.Add(_RCPLANCOT, "Plan Clock Out Time", 21, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Plan Clock Out Time
            moDBFields.Add(_RCPLANYDS, "Plan Yards", 22, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Plan Yards
            moDBFields.Add(_RCTHOUR2, "Total Hours", 23, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Total Hours
            moDBFields.Add(_RCTIMEOY, "Time Out of Yard", 24, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Time Out of Yard
            moDBFields.Add(_RCTIMEIY, "Time into Yard", 25, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Time into Yard
            moDBFields.Add(_RCTHOUR3, "Total Hours", 26, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Total Hours
            moDBFields.Add(_RCMILESRT, "Mileage Start", 27, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Mileage Start
            moDBFields.Add(_RCMILEEND, "Mileage End", 28, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Mileage End
            moDBFields.Add(_RCTMILE, "Total Mileage", 29, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Total Mileage
            moDBFields.Add(_RCADMINR, "Admin Reason", 30, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Admin Reason
            moDBFields.Add(_RCADMINH, "Unpaid/Admin/Meeting Hrs", 31, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Unpaid/Admin/Meeting Hrs
            moDBFields.Add(_RCSAFEMT, "Safety Meeting", 32, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Safety Meeting
            moDBFields.Add(_RCMEALS, "Meal Start", 33, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Meal Start
            moDBFields.Add(_RCMEALE, "Meal End", 34, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Meal End
            moDBFields.Add(_RCBREAK1, "Break #1", 35, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Break #1
            moDBFields.Add(_RCBREAK2, "Break #2", 36, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Break #2
            moDBFields.Add(_RCCOMM, "Comments", 37, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Comments
            moDBFields.Add(_RCTSTOP2, "Total Stops/Pulls/Hauls", 38, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Total Stops/Pulls/Hauls
            moDBFields.Add(_RCTCONTS, "Total Containers", 39, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Total Containers
            moDBFields.Add(_RCTYARDS, "Total Yards", 40, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Total Yards
            moDBFields.Add(_RCTONS, "Total Totals", 41, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Total Totals
            moDBFields.Add(_RCTLIFT, "Total Lifts", 42, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Total Lifts

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation()
        {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_RCLOB());
            doBuildValidationString(doValidate_RCRSEQ());
            doBuildValidationString(doValidate_RCDOTLS());
            doBuildValidationString(doValidate_RCDATE());
            doBuildValidationString(doValidate_RCTRUCK());
            doBuildValidationString(doValidate_RCROUTE());
            doBuildValidationString(doValidate_RCPROUTE());
            doBuildValidationString(doValidate_RCFUEL());
            doBuildValidationString(doValidate_RCEXTYD());
            doBuildValidationString(doValidate_RCINTYD());
            doBuildValidationString(doValidate_RCTSTOP1());
            doBuildValidationString(doValidate_RCEXTHOME());
            doBuildValidationString(doValidate_RCINTHOME());
            doBuildValidationString(doValidate_RCTANK());
            doBuildValidationString(doValidate_RCDRIVER());
            doBuildValidationString(doValidate_RCCLOCKIN());
            doBuildValidationString(doValidate_RCCLOCKOUT());
            doBuildValidationString(doValidate_RCTHOUR1());
            doBuildValidationString(doValidate_RCPLANCIT());
            doBuildValidationString(doValidate_RCPLANCOT());
            doBuildValidationString(doValidate_RCPLANYDS());
            doBuildValidationString(doValidate_RCTHOUR2());
            doBuildValidationString(doValidate_RCTIMEOY());
            doBuildValidationString(doValidate_RCTIMEIY());
            doBuildValidationString(doValidate_RCTHOUR3());
            doBuildValidationString(doValidate_RCMILESRT());
            doBuildValidationString(doValidate_RCMILEEND());
            doBuildValidationString(doValidate_RCTMILE());
            doBuildValidationString(doValidate_RCADMINR());
            doBuildValidationString(doValidate_RCADMINH());
            doBuildValidationString(doValidate_RCSAFEMT());
            doBuildValidationString(doValidate_RCMEALS());
            doBuildValidationString(doValidate_RCMEALE());
            doBuildValidationString(doValidate_RCBREAK1());
            doBuildValidationString(doValidate_RCBREAK2());
            doBuildValidationString(doValidate_RCCOMM());
            doBuildValidationString(doValidate_RCTSTOP2());
            doBuildValidationString(doValidate_RCTCONTS());
            doBuildValidationString(doValidate_RCTYARDS());
            doBuildValidationString(doValidate_RCTONS());
            doBuildValidationString(doValidate_RCTLIFT());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue)
        {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_RCLOB)) return doValidate_RCLOB(oValue);
            if (sFieldName.Equals(_RCRSEQ)) return doValidate_RCRSEQ(oValue);
            if (sFieldName.Equals(_RCDOTLS)) return doValidate_RCDOTLS(oValue);
            if (sFieldName.Equals(_RCDATE)) return doValidate_RCDATE(oValue);
            if (sFieldName.Equals(_RCTRUCK)) return doValidate_RCTRUCK(oValue);
            if (sFieldName.Equals(_RCROUTE)) return doValidate_RCROUTE(oValue);
            if (sFieldName.Equals(_RCPROUTE)) return doValidate_RCPROUTE(oValue);
            if (sFieldName.Equals(_RCFUEL)) return doValidate_RCFUEL(oValue);
            if (sFieldName.Equals(_RCEXTYD)) return doValidate_RCEXTYD(oValue);
            if (sFieldName.Equals(_RCINTYD)) return doValidate_RCINTYD(oValue);
            if (sFieldName.Equals(_RCTSTOP1)) return doValidate_RCTSTOP1(oValue);
            if (sFieldName.Equals(_RCEXTHOME)) return doValidate_RCEXTHOME(oValue);
            if (sFieldName.Equals(_RCINTHOME)) return doValidate_RCINTHOME(oValue);
            if (sFieldName.Equals(_RCTANK)) return doValidate_RCTANK(oValue);
            if (sFieldName.Equals(_RCDRIVER)) return doValidate_RCDRIVER(oValue);
            if (sFieldName.Equals(_RCCLOCKIN)) return doValidate_RCCLOCKIN(oValue);
            if (sFieldName.Equals(_RCCLOCKOUT)) return doValidate_RCCLOCKOUT(oValue);
            if (sFieldName.Equals(_RCTHOUR1)) return doValidate_RCTHOUR1(oValue);
            if (sFieldName.Equals(_RCPLANCIT)) return doValidate_RCPLANCIT(oValue);
            if (sFieldName.Equals(_RCPLANCOT)) return doValidate_RCPLANCOT(oValue);
            if (sFieldName.Equals(_RCPLANYDS)) return doValidate_RCPLANYDS(oValue);
            if (sFieldName.Equals(_RCTHOUR2)) return doValidate_RCTHOUR2(oValue);
            if (sFieldName.Equals(_RCTIMEOY)) return doValidate_RCTIMEOY(oValue);
            if (sFieldName.Equals(_RCTIMEIY)) return doValidate_RCTIMEIY(oValue);
            if (sFieldName.Equals(_RCTHOUR3)) return doValidate_RCTHOUR3(oValue);
            if (sFieldName.Equals(_RCMILESRT)) return doValidate_RCMILESRT(oValue);
            if (sFieldName.Equals(_RCMILEEND)) return doValidate_RCMILEEND(oValue);
            if (sFieldName.Equals(_RCTMILE)) return doValidate_RCTMILE(oValue);
            if (sFieldName.Equals(_RCADMINR)) return doValidate_RCADMINR(oValue);
            if (sFieldName.Equals(_RCADMINH)) return doValidate_RCADMINH(oValue);
            if (sFieldName.Equals(_RCSAFEMT)) return doValidate_RCSAFEMT(oValue);
            if (sFieldName.Equals(_RCMEALS)) return doValidate_RCMEALS(oValue);
            if (sFieldName.Equals(_RCMEALE)) return doValidate_RCMEALE(oValue);
            if (sFieldName.Equals(_RCBREAK1)) return doValidate_RCBREAK1(oValue);
            if (sFieldName.Equals(_RCBREAK2)) return doValidate_RCBREAK2(oValue);
            if (sFieldName.Equals(_RCCOMM)) return doValidate_RCCOMM(oValue);
            if (sFieldName.Equals(_RCTSTOP2)) return doValidate_RCTSTOP2(oValue);
            if (sFieldName.Equals(_RCTCONTS)) return doValidate_RCTCONTS(oValue);
            if (sFieldName.Equals(_RCTYARDS)) return doValidate_RCTYARDS(oValue);
            if (sFieldName.Equals(_RCTONS)) return doValidate_RCTONS(oValue);
            if (sFieldName.Equals(_RCTLIFT)) return doValidate_RCTLIFT(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName)
        {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
         * Link the Name Field to the Form Item.
         */
        public void doLink_Name(string sControlName)
        {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
         * Link the U_RCLOB Field to the Form Item.
         */
        public void doLink_RCLOB(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCLOB, sControlName);
        }
        /**
         * Link the U_RCRSEQ Field to the Form Item.
         */
        public void doLink_RCRSEQ(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCRSEQ, sControlName);
        }
        /**
         * Link the U_RCDOTLS Field to the Form Item.
         */
        public void doLink_RCDOTLS(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCDOTLS, sControlName);
        }
        /**
         * Link the U_RCDATE Field to the Form Item.
         */
        public void doLink_RCDATE(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCDATE, sControlName);
        }
        /**
         * Link the U_RCTRUCK Field to the Form Item.
         */
        public void doLink_RCTRUCK(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCTRUCK, sControlName);
        }
        /**
         * Link the U_RCROUTE Field to the Form Item.
         */
        public void doLink_RCROUTE(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCROUTE, sControlName);
        }
        /**
         * Link the U_RCPROUTE Field to the Form Item.
         */
        public void doLink_RCPROUTE(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCPROUTE, sControlName);
        }
        /**
         * Link the U_RCFUEL Field to the Form Item.
         */
        public void doLink_RCFUEL(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCFUEL, sControlName);
        }
        /**
         * Link the U_RCEXTYD Field to the Form Item.
         */
        public void doLink_RCEXTYD(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCEXTYD, sControlName);
        }
        /**
         * Link the U_RCINTYD Field to the Form Item.
         */
        public void doLink_RCINTYD(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCINTYD, sControlName);
        }
        /**
         * Link the U_RCTSTOP1 Field to the Form Item.
         */
        public void doLink_RCTSTOP1(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCTSTOP1, sControlName);
        }
        /**
         * Link the U_RCEXTHOME Field to the Form Item.
         */
        public void doLink_RCEXTHOME(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCEXTHOME, sControlName);
        }
        /**
         * Link the U_RCINTHOME Field to the Form Item.
         */
        public void doLink_RCINTHOME(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCINTHOME, sControlName);
        }
        /**
         * Link the U_RCTANK Field to the Form Item.
         */
        public void doLink_RCTANK(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCTANK, sControlName);
        }
        /**
         * Link the U_RCDRIVER Field to the Form Item.
         */
        public void doLink_RCDRIVER(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCDRIVER, sControlName);
        }
        /**
         * Link the U_RCCLOCKIN Field to the Form Item.
         */
        public void doLink_RCCLOCKIN(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCCLOCKIN, sControlName);
        }
        /**
         * Link the U_RCCLOCKOUT Field to the Form Item.
         */
        public void doLink_RCCLOCKOUT(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCCLOCKOUT, sControlName);
        }
        /**
         * Link the U_RCTHOUR1 Field to the Form Item.
         */
        public void doLink_RCTHOUR1(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCTHOUR1, sControlName);
        }
        /**
         * Link the U_RCPLANCIT Field to the Form Item.
         */
        public void doLink_RCPLANCIT(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCPLANCIT, sControlName);
        }
        /**
         * Link the U_RCPLANCOT Field to the Form Item.
         */
        public void doLink_RCPLANCOT(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCPLANCOT, sControlName);
        }
        /**
         * Link the U_RCPLANYDS Field to the Form Item.
         */
        public void doLink_RCPLANYDS(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCPLANYDS, sControlName);
        }
        /**
         * Link the U_RCTHOUR2 Field to the Form Item.
         */
        public void doLink_RCTHOUR2(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCTHOUR2, sControlName);
        }
        /**
         * Link the U_RCTIMEOY Field to the Form Item.
         */
        public void doLink_RCTIMEOY(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCTIMEOY, sControlName);
        }
        /**
         * Link the U_RCTIMEIY Field to the Form Item.
         */
        public void doLink_RCTIMEIY(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCTIMEIY, sControlName);
        }
        /**
         * Link the U_RCTHOUR3 Field to the Form Item.
         */
        public void doLink_RCTHOUR3(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCTHOUR3, sControlName);
        }
        /**
         * Link the U_RCMILESRT Field to the Form Item.
         */
        public void doLink_RCMILESRT(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCMILESRT, sControlName);
        }
        /**
         * Link the U_RCMILEEND Field to the Form Item.
         */
        public void doLink_RCMILEEND(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCMILEEND, sControlName);
        }
        /**
         * Link the U_RCTMILE Field to the Form Item.
         */
        public void doLink_RCTMILE(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCTMILE, sControlName);
        }
        /**
         * Link the U_RCADMINR Field to the Form Item.
         */
        public void doLink_RCADMINR(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCADMINR, sControlName);
        }
        /**
         * Link the U_RCADMINH Field to the Form Item.
         */
        public void doLink_RCADMINH(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCADMINH, sControlName);
        }
        /**
         * Link the U_RCSAFEMT Field to the Form Item.
         */
        public void doLink_RCSAFEMT(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCSAFEMT, sControlName);
        }
        /**
         * Link the U_RCMEALS Field to the Form Item.
         */
        public void doLink_RCMEALS(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCMEALS, sControlName);
        }
        /**
         * Link the U_RCMEALE Field to the Form Item.
         */
        public void doLink_RCMEALE(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCMEALE, sControlName);
        }
        /**
         * Link the U_RCBREAK1 Field to the Form Item.
         */
        public void doLink_RCBREAK1(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCBREAK1, sControlName);
        }
        /**
         * Link the U_RCBREAK2 Field to the Form Item.
         */
        public void doLink_RCBREAK2(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCBREAK2, sControlName);
        }
        /**
         * Link the U_RCCOMM Field to the Form Item.
         */
        public void doLink_RCCOMM(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCCOMM, sControlName);
        }
        /**
         * Link the U_RCTSTOP2 Field to the Form Item.
         */
        public void doLink_RCTSTOP2(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCTSTOP2, sControlName);
        }
        /**
         * Link the U_RCTCONTS Field to the Form Item.
         */
        public void doLink_RCTCONTS(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCTCONTS, sControlName);
        }
        /**
         * Link the U_RCTYARDS Field to the Form Item.
         */
        public void doLink_RCTYARDS(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCTYARDS, sControlName);
        }
        /**
         * Link the U_RCTONS Field to the Form Item.
         */
        public void doLink_RCTONS(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCTONS, sControlName);
        }
        /**
         * Link the U_RCTLIFT Field to the Form Item.
         */
        public void doLink_RCTLIFT(string sControlName)
        {
            moLinker.doLinkDataToControl(_RCTLIFT, sControlName);
        }
        #endregion

    }
}
