/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 09/03/2016 11:25:10
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
   [Serializable]
	public class IDH_DISPTRT: com.idh.dbObjects.DBBase { 

		private Linker moLinker = null;
       public Linker ControlLinker {
           get { return moLinker; }
           set { moLinker = value; }
       }

       private IDHAddOns.idh.forms.Base moIDHForm;
       public IDHAddOns.idh.forms.Base IDHForm {
           get { return moIDHForm; }
           set { moIDHForm = value; }
       }

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_DISPTRT() : base("@IDH_DISPTRT"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_DISPTRT( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_DISPTRT"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_DISPTRT";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Treatment Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TreatmntCd
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TreatmntCd = "U_TreatmntCd";
		public string U_TreatmntCd { 
			get {
 				return getValueAsString(_TreatmntCd); 
			}
			set { setValue(_TreatmntCd, value); }
		}
           public string doValidate_TreatmntCd() {
               return doValidate_TreatmntCd(U_TreatmntCd);
           }
           public virtual string doValidate_TreatmntCd(object oValue) {
               return base.doValidation(_TreatmntCd, oValue);
           }

		/**
		 * Decription: Treatment Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TreatmntNm
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TreatmntNm = "U_TreatmntNm";
		public string U_TreatmntNm { 
			get {
 				return getValueAsString(_TreatmntNm); 
			}
			set { setValue(_TreatmntNm, value); }
		}
           public string doValidate_TreatmntNm() {
               return doValidate_TreatmntNm(U_TreatmntNm);
           }
           public virtual string doValidate_TreatmntNm(object oValue) {
               return base.doValidation(_TreatmntNm, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(4);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_TreatmntCd, "Treatment Code", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Treatment Code
			moDBFields.Add(_TreatmntNm, "Treatment Name", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Treatment Name

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_TreatmntCd());
            doBuildValidationString(doValidate_TreatmntNm());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_TreatmntCd)) return doValidate_TreatmntCd(oValue);
            if (sFieldName.Equals(_TreatmntNm)) return doValidate_TreatmntNm(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_TreatmntCd Field to the Form Item.
		 */
		public void doLink_TreatmntCd(string sControlName){
			moLinker.doLinkDataToControl(_TreatmntCd, sControlName);
		}
		/**
		 * Link the U_TreatmntNm Field to the Form Item.
		 */
		public void doLink_TreatmntNm(string sControlName){
			moLinker.doLinkDataToControl(_TreatmntNm, sControlName);
		}
#endregion

	}
}
