/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 2015-07-13 02:54:54 PM
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base
{
    [Serializable]
    public class IDH_TRAIN01 : com.idh.dbObjects.DBBase
    {

        //private Linker moLinker = null;
        //public Linker ControlLinker
        //{
        //    get { return moLinker; }
        //    set { moLinker = value; }
        //}

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm
        {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX
        {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_TRAIN01()
            : base("@IDH_TRAIN01")
        {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_TRAIN01(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oForm, "@IDH_TRAIN01")
        {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
		* Table name
		*/
        public readonly static string TableName = "@IDH_TRAIN01";

        /**
         * Decription: Code
         * Mandatory: tYes
         * Name: Code
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Code = "Code";
        public string Code
        {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code()
        {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue)
        {
            return base.doValidation(_Code, oValue);
        }
        /**
         * Decription: Name
         * Mandatory: tYes
         * Name: Name
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Name = "Name";
        public string Name
        {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name()
        {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue)
        {
            return base.doValidation(_Name, oValue);
        }
        /**
         * Decription: Customer Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CardCode
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CardCode = "U_CardCode";
        public string U_CardCode
        {
            get
            {
                return getValueAsString(_CardCode);
            }
            set { setValue(_CardCode, value); }
        }
        public string doValidate_CardCode()
        {
            return doValidate_CardCode(U_CardCode);
        }
        public virtual string doValidate_CardCode(object oValue)
        {
            return base.doValidation(_CardCode, oValue);
        }

        /**
         * Decription: Customer Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CardName
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CardName = "U_CardName";
        public string U_CardName
        {
            get
            {
                return getValueAsString(_CardName);
            }
            set { setValue(_CardName, value); }
        }
        public string doValidate_CardName()
        {
            return doValidate_CardName(U_CardName);
        }
        public virtual string doValidate_CardName(object oValue)
        {
            return base.doValidation(_CardName, oValue);
        }

        /**
         * Decription: Document Amount
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_DocTotal
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Price
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _DocTotal = "U_DocTotal";
        public double U_DocTotal
        {
            get
            {
                return getValueAsDouble(_DocTotal);
            }
            set { setValue(_DocTotal, value); }
        }
        public string doValidate_DocTotal()
        {
            return doValidate_DocTotal(U_DocTotal);
        }
        public virtual string doValidate_DocTotal(object oValue)
        {
            return base.doValidation(_DocTotal, oValue);
        }

        /**
         * Decription: Invoice Date
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_InvDate
         * Size: 8
         * Type: db_Date
         * CType: DateTime
         * SubType: st_None
         * ValidValue: 
         * Value: 1899-12-30 12:00:00 AM
         * Identity: False
         */
        public readonly static string _InvDate = "U_InvDate";
        public DateTime U_InvDate
        {
            get
            {
                return getValueAsDateTime(_InvDate);
            }
            set { setValue(_InvDate, value); }
        }
        public void U_InvDate_AsString(string value)
        {
            setValue("U_InvDate", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_InvDate_AsString()
        {
            DateTime dVal = getValueAsDateTime(_InvDate);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_InvDate()
        {
            return doValidate_InvDate(U_InvDate);
        }
        public virtual string doValidate_InvDate(object oValue)
        {
            return base.doValidation(_InvDate, oValue);
        }

        /**
         * Decription: Invoice Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_InvID
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _InvID = "U_InvID";
        public string U_InvID
        {
            get
            {
                return getValueAsString(_InvID);
            }
            set { setValue(_InvID, value); }
        }
        public string doValidate_InvID()
        {
            return doValidate_InvID(U_InvID);
        }
        public virtual string doValidate_InvID(object oValue)
        {
            return base.doValidation(_InvID, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo()
        {
            if (moDBFields == null)
                moDBFields = new DBFields(7);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_CardCode, "Customer Code", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Customer Code
            moDBFields.Add(_CardName, "Customer Name", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Customer Name
            moDBFields.Add(_DocTotal, "Document Amount", 4, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Document Amount
            moDBFields.Add(_InvDate, "Invoice Date", 5, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Invoice Date
            moDBFields.Add(_InvID, "Invoice Code", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Invoice Code

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation()
        {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_CardCode());
            doBuildValidationString(doValidate_CardName());
            doBuildValidationString(doValidate_DocTotal());
            doBuildValidationString(doValidate_InvDate());
            doBuildValidationString(doValidate_InvID());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue)
        {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_CardCode)) return doValidate_CardCode(oValue);
            if (sFieldName.Equals(_CardName)) return doValidate_CardName(oValue);
            if (sFieldName.Equals(_DocTotal)) return doValidate_DocTotal(oValue);
            if (sFieldName.Equals(_InvDate)) return doValidate_InvDate(oValue);
            if (sFieldName.Equals(_InvID)) return doValidate_InvID(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName)
        {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
         * Link the Name Field to the Form Item.
         */
        public void doLink_Name(string sControlName)
        {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
         * Link the U_CardCode Field to the Form Item.
         */
        public void doLink_CardCode(string sControlName)
        {
            moLinker.doLinkDataToControl(_CardCode, sControlName);
        }
        /**
         * Link the U_CardName Field to the Form Item.
         */
        public void doLink_CardName(string sControlName)
        {
            moLinker.doLinkDataToControl(_CardName, sControlName);
        }
        /**
         * Link the U_DocTotal Field to the Form Item.
         */
        public void doLink_DocTotal(string sControlName)
        {
            moLinker.doLinkDataToControl(_DocTotal, sControlName);
        }
        /**
         * Link the U_InvDate Field to the Form Item.
         */
        public void doLink_InvDate(string sControlName)
        {
            moLinker.doLinkDataToControl(_InvDate, sControlName);
        }
        /**
         * Link the U_InvID Field to the Form Item.
         */
        public void doLink_InvID(string sControlName)
        {
            moLinker.doLinkDataToControl(_InvID, sControlName);
        }
        #endregion

    }
}