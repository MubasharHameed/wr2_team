/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 04/02/2016 17:41:28
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
   [Serializable] 
	public class IDH_SAMPITM: com.idh.dbObjects.Base.IDH_SAMPITM{ 

		public IDH_SAMPITM() : base() {
			
		}

   	public IDH_SAMPITM( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	}
	}
}
