/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 2014/10/09 05:30:01 PM
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
   [Serializable]
	public class IDH_HAULCST: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
		protected IDHAddOns.idh.forms.Base moIDHForm;

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_HAULCST() : base("@IDH_HAULCST"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_HAULCST( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_HAULCST"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_HAULCST";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Lorry Registration
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Lorry
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Lorry = "U_Lorry";
		public string U_Lorry { 
			get {
 				return getValueAsString(_Lorry); 
			}
			set { setValue(_Lorry, value); }
		}
           public string doValidate_Lorry() {
               return doValidate_Lorry(U_Lorry);
           }
           public virtual string doValidate_Lorry(object oValue) {
               return base.doValidation(_Lorry, oValue);
           }

		/**
		 * Decription: Container Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItemCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ItemCd = "U_ItemCd";
		public string U_ItemCd { 
			get {
 				return getValueAsString(_ItemCd); 
			}
			set { setValue(_ItemCd, value); }
		}
           public string doValidate_ItemCd() {
               return doValidate_ItemCd(U_ItemCd);
           }
           public virtual string doValidate_ItemCd(object oValue) {
               return base.doValidation(_ItemCd, oValue);
           }

		/**
		 * Decription: Loads per day
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_LoadPD
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _LoadPD = "U_LoadPD";
		public int U_LoadPD { 
			get {
 				return getValueAsInt(_LoadPD); 
			}
			set { setValue(_LoadPD, value); }
		}
           public string doValidate_LoadPD() {
               return doValidate_LoadPD(U_LoadPD);
           }
           public virtual string doValidate_LoadPD(object oValue) {
               return base.doValidation(_LoadPD, oValue);
           }

		/**
		 * Decription: Loads and tips per day in min
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_LTPDMin
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _LTPDMin = "U_LTPDMin";
		public int U_LTPDMin { 
			get {
 				return getValueAsInt(_LTPDMin); 
			}
			set { setValue(_LTPDMin, value); }
		}
           public string doValidate_LTPDMin() {
               return doValidate_LTPDMin(U_LTPDMin);
           }
           public virtual string doValidate_LTPDMin(object oValue) {
               return base.doValidation(_LTPDMin, oValue);
           }

		/**
		 * Decription: Loads and tips per day in hrs
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_LTPDHrs
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _LTPDHrs = "U_LTPDHrs";
		public double U_LTPDHrs { 
			get {
 				return getValueAsDouble(_LTPDHrs); 
			}
			set { setValue(_LTPDHrs, value); }
		}
           public string doValidate_LTPDHrs() {
               return doValidate_LTPDHrs(U_LTPDHrs);
           }
           public virtual string doValidate_LTPDHrs(object oValue) {
               return base.doValidation(_LTPDHrs, oValue);
           }

		/**
		 * Decription: Running day
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RunDay
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _RunDay = "U_RunDay";
		public int U_RunDay { 
			get {
 				return getValueAsInt(_RunDay); 
			}
			set { setValue(_RunDay, value); }
		}
           public string doValidate_RunDay() {
               return doValidate_RunDay(U_RunDay);
           }
           public virtual string doValidate_RunDay(object oValue) {
               return base.doValidation(_RunDay, oValue);
           }

		/**
		 * Decription: Running time
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RunTime
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _RunTime = "U_RunTime";
		public double U_RunTime { 
			get {
 				return getValueAsDouble(_RunTime); 
			}
			set { setValue(_RunTime, value); }
		}
           public string doValidate_RunTime() {
               return doValidate_RunTime(U_RunTime);
           }
           public virtual string doValidate_RunTime(object oValue) {
               return base.doValidation(_RunTime, oValue);
           }

		/**
		 * Decription: Average Speed Km/h
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AvSpeed
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _AvSpeed = "U_AvSpeed";
		public int U_AvSpeed { 
			get {
 				return getValueAsInt(_AvSpeed); 
			}
			set { setValue(_AvSpeed, value); }
		}
           public string doValidate_AvSpeed() {
               return doValidate_AvSpeed(U_AvSpeed);
           }
           public virtual string doValidate_AvSpeed(object oValue) {
               return base.doValidation(_AvSpeed, oValue);
           }

		/**
		 * Decription: Total Distance
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TotDst
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _TotDst = "U_TotDst";
		public int U_TotDst { 
			get {
 				return getValueAsInt(_TotDst); 
			}
			set { setValue(_TotDst, value); }
		}
           public string doValidate_TotDst() {
               return doValidate_TotDst(U_TotDst);
           }
           public virtual string doValidate_TotDst(object oValue) {
               return base.doValidation(_TotDst, oValue);
           }

		/**
		 * Decription: Approx Distance
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AppDst
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _AppDst = "U_AppDst";
		public int U_AppDst { 
			get {
 				return getValueAsInt(_AppDst); 
			}
			set { setValue(_AppDst, value); }
		}
           public string doValidate_AppDst() {
               return doValidate_AppDst(U_AppDst);
           }
           public virtual string doValidate_AppDst(object oValue) {
               return base.doValidation(_AppDst, oValue);
           }

		/**
		 * Decription: Oneway Distance
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_OneWDst
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _OneWDst = "U_OneWDst";
		public int U_OneWDst { 
			get {
 				return getValueAsInt(_OneWDst); 
			}
			set { setValue(_OneWDst, value); }
		}
           public string doValidate_OneWDst() {
               return doValidate_OneWDst(U_OneWDst);
           }
           public virtual string doValidate_OneWDst(object oValue) {
               return base.doValidation(_OneWDst, oValue);
           }

		/**
		 * Decription: Average Fuel Consumption
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FuelCons
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _FuelCons = "U_FuelCons";
		public double U_FuelCons { 
			get {
 				return getValueAsDouble(_FuelCons); 
			}
			set { setValue(_FuelCons, value); }
		}
           public string doValidate_FuelCons() {
               return doValidate_FuelCons(U_FuelCons);
           }
           public virtual string doValidate_FuelCons(object oValue) {
               return base.doValidation(_FuelCons, oValue);
           }

		/**
		 * Decription: Fuel Liters
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FuelLtrs
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _FuelLtrs = "U_FuelLtrs";
		public double U_FuelLtrs { 
			get {
 				return getValueAsDouble(_FuelLtrs); 
			}
			set { setValue(_FuelLtrs, value); }
		}
           public string doValidate_FuelLtrs() {
               return doValidate_FuelLtrs(U_FuelLtrs);
           }
           public virtual string doValidate_FuelLtrs(object oValue) {
               return base.doValidation(_FuelLtrs, oValue);
           }

		/**
		 * Decription: Fuel Cost
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FuelCst
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _FuelCst = "U_FuelCst";
		public double U_FuelCst { 
			get {
 				return getValueAsDouble(_FuelCst); 
			}
			set { setValue(_FuelCst, value); }
		}
           public string doValidate_FuelCst() {
               return doValidate_FuelCst(U_FuelCst);
           }
           public virtual string doValidate_FuelCst(object oValue) {
               return base.doValidation(_FuelCst, oValue);
           }

		/**
		 * Decription: Driver Cost
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DrvCst
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _DrvCst = "U_DrvCst";
		public double U_DrvCst { 
			get {
 				return getValueAsDouble(_DrvCst); 
			}
			set { setValue(_DrvCst, value); }
		}
           public string doValidate_DrvCst() {
               return doValidate_DrvCst(U_DrvCst);
           }
           public virtual string doValidate_DrvCst(object oValue) {
               return base.doValidation(_DrvCst, oValue);
           }

		/**
		 * Decription: Total Running
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TotRun
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _TotRun = "U_TotRun";
		public double U_TotRun { 
			get {
 				return getValueAsDouble(_TotRun); 
			}
			set { setValue(_TotRun, value); }
		}
           public string doValidate_TotRun() {
               return doValidate_TotRun(U_TotRun);
           }
           public virtual string doValidate_TotRun(object oValue) {
               return base.doValidation(_TotRun, oValue);
           }

		/**
		 * Decription: Plus OH
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PlusOH
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _PlusOH = "U_PlusOH";
		public double U_PlusOH { 
			get {
 				return getValueAsDouble(_PlusOH); 
			}
			set { setValue(_PlusOH, value); }
		}
           public string doValidate_PlusOH() {
               return doValidate_PlusOH(U_PlusOH);
           }
           public virtual string doValidate_PlusOH(object oValue) {
               return base.doValidation(_PlusOH, oValue);
           }

		/**
		 * Decription: Haulage Charge per Load
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HaulChrg
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _HaulChrg = "U_HaulChrg";
		public double U_HaulChrg { 
			get {
 				return getValueAsDouble(_HaulChrg); 
			}
			set { setValue(_HaulChrg, value); }
		}
           public string doValidate_HaulChrg() {
               return doValidate_HaulChrg(U_HaulChrg);
           }
           public virtual string doValidate_HaulChrg(object oValue) {
               return base.doValidation(_HaulChrg, oValue);
           }

		/**
		 * Decription: Unit Of Measure
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UOM
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _UOM = "U_UOM";
		public string U_UOM { 
			get {
 				return getValueAsString(_UOM); 
			}
			set { setValue(_UOM, value); }
		}
           public string doValidate_UOM() {
               return doValidate_UOM(U_UOM);
           }
           public virtual string doValidate_UOM(object oValue) {
               return base.doValidation(_UOM, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(21);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_Lorry, "Lorry Registration", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Lorry Registration
			moDBFields.Add(_ItemCd, "Container Code", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Container Code
			moDBFields.Add(_LoadPD, "Loads per day", 4, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Loads per day
			moDBFields.Add(_LTPDMin, "Loads and tips per day in min", 5, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Loads and tips per day in min
			moDBFields.Add(_LTPDHrs, "Loads and tips per day in hrs", 6, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Loads and tips per day in hrs
			moDBFields.Add(_RunDay, "Running day", 7, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Running day
			moDBFields.Add(_RunTime, "Running time", 8, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Running time
			moDBFields.Add(_AvSpeed, "Average Speed Km/h", 9, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Average Speed Km/h
			moDBFields.Add(_TotDst, "Total Distance", 10, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Total Distance
			moDBFields.Add(_AppDst, "Approx Distance", 11, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Approx Distance
			moDBFields.Add(_OneWDst, "Oneway Distance", 12, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Oneway Distance
			moDBFields.Add(_FuelCons, "Average Fuel Consumption", 13, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Average Fuel Consumption
			moDBFields.Add(_FuelLtrs, "Fuel Liters", 14, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Fuel Liters
			moDBFields.Add(_FuelCst, "Fuel Cost", 15, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Fuel Cost
			moDBFields.Add(_DrvCst, "Driver Cost", 16, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Driver Cost
			moDBFields.Add(_TotRun, "Total Running", 17, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Total Running
			moDBFields.Add(_PlusOH, "Plus OH", 18, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Plus OH
			moDBFields.Add(_HaulChrg, "Haulage Charge per Load", 19, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Haulage Charge per Load
			moDBFields.Add(_UOM, "Unit Of Measure", 20, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Unit Of Measure

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_Lorry());
            doBuildValidationString(doValidate_ItemCd());
            doBuildValidationString(doValidate_LoadPD());
            doBuildValidationString(doValidate_LTPDMin());
            doBuildValidationString(doValidate_LTPDHrs());
            doBuildValidationString(doValidate_RunDay());
            doBuildValidationString(doValidate_RunTime());
            doBuildValidationString(doValidate_AvSpeed());
            doBuildValidationString(doValidate_TotDst());
            doBuildValidationString(doValidate_AppDst());
            doBuildValidationString(doValidate_OneWDst());
            doBuildValidationString(doValidate_FuelCons());
            doBuildValidationString(doValidate_FuelLtrs());
            doBuildValidationString(doValidate_FuelCst());
            doBuildValidationString(doValidate_DrvCst());
            doBuildValidationString(doValidate_TotRun());
            doBuildValidationString(doValidate_PlusOH());
            doBuildValidationString(doValidate_HaulChrg());
            doBuildValidationString(doValidate_UOM());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_Lorry)) return doValidate_Lorry(oValue);
            if (sFieldName.Equals(_ItemCd)) return doValidate_ItemCd(oValue);
            if (sFieldName.Equals(_LoadPD)) return doValidate_LoadPD(oValue);
            if (sFieldName.Equals(_LTPDMin)) return doValidate_LTPDMin(oValue);
            if (sFieldName.Equals(_LTPDHrs)) return doValidate_LTPDHrs(oValue);
            if (sFieldName.Equals(_RunDay)) return doValidate_RunDay(oValue);
            if (sFieldName.Equals(_RunTime)) return doValidate_RunTime(oValue);
            if (sFieldName.Equals(_AvSpeed)) return doValidate_AvSpeed(oValue);
            if (sFieldName.Equals(_TotDst)) return doValidate_TotDst(oValue);
            if (sFieldName.Equals(_AppDst)) return doValidate_AppDst(oValue);
            if (sFieldName.Equals(_OneWDst)) return doValidate_OneWDst(oValue);
            if (sFieldName.Equals(_FuelCons)) return doValidate_FuelCons(oValue);
            if (sFieldName.Equals(_FuelLtrs)) return doValidate_FuelLtrs(oValue);
            if (sFieldName.Equals(_FuelCst)) return doValidate_FuelCst(oValue);
            if (sFieldName.Equals(_DrvCst)) return doValidate_DrvCst(oValue);
            if (sFieldName.Equals(_TotRun)) return doValidate_TotRun(oValue);
            if (sFieldName.Equals(_PlusOH)) return doValidate_PlusOH(oValue);
            if (sFieldName.Equals(_HaulChrg)) return doValidate_HaulChrg(oValue);
            if (sFieldName.Equals(_UOM)) return doValidate_UOM(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_Lorry Field to the Form Item.
		 */
		public void doLink_Lorry(string sControlName){
			moLinker.doLinkDataToControl(_Lorry, sControlName);
		}
		/**
		 * Link the U_ItemCd Field to the Form Item.
		 */
		public void doLink_ItemCd(string sControlName){
			moLinker.doLinkDataToControl(_ItemCd, sControlName);
		}
		/**
		 * Link the U_LoadPD Field to the Form Item.
		 */
		public void doLink_LoadPD(string sControlName){
			moLinker.doLinkDataToControl(_LoadPD, sControlName);
		}
		/**
		 * Link the U_LTPDMin Field to the Form Item.
		 */
		public void doLink_LTPDMin(string sControlName){
			moLinker.doLinkDataToControl(_LTPDMin, sControlName);
		}
		/**
		 * Link the U_LTPDHrs Field to the Form Item.
		 */
		public void doLink_LTPDHrs(string sControlName){
			moLinker.doLinkDataToControl(_LTPDHrs, sControlName);
		}
		/**
		 * Link the U_RunDay Field to the Form Item.
		 */
		public void doLink_RunDay(string sControlName){
			moLinker.doLinkDataToControl(_RunDay, sControlName);
		}
		/**
		 * Link the U_RunTime Field to the Form Item.
		 */
		public void doLink_RunTime(string sControlName){
			moLinker.doLinkDataToControl(_RunTime, sControlName);
		}
		/**
		 * Link the U_AvSpeed Field to the Form Item.
		 */
		public void doLink_AvSpeed(string sControlName){
			moLinker.doLinkDataToControl(_AvSpeed, sControlName);
		}
		/**
		 * Link the U_TotDst Field to the Form Item.
		 */
		public void doLink_TotDst(string sControlName){
			moLinker.doLinkDataToControl(_TotDst, sControlName);
		}
		/**
		 * Link the U_AppDst Field to the Form Item.
		 */
		public void doLink_AppDst(string sControlName){
			moLinker.doLinkDataToControl(_AppDst, sControlName);
		}
		/**
		 * Link the U_OneWDst Field to the Form Item.
		 */
		public void doLink_OneWDst(string sControlName){
			moLinker.doLinkDataToControl(_OneWDst, sControlName);
		}
		/**
		 * Link the U_FuelCons Field to the Form Item.
		 */
		public void doLink_FuelCons(string sControlName){
			moLinker.doLinkDataToControl(_FuelCons, sControlName);
		}
		/**
		 * Link the U_FuelLtrs Field to the Form Item.
		 */
		public void doLink_FuelLtrs(string sControlName){
			moLinker.doLinkDataToControl(_FuelLtrs, sControlName);
		}
		/**
		 * Link the U_FuelCst Field to the Form Item.
		 */
		public void doLink_FuelCst(string sControlName){
			moLinker.doLinkDataToControl(_FuelCst, sControlName);
		}
		/**
		 * Link the U_DrvCst Field to the Form Item.
		 */
		public void doLink_DrvCst(string sControlName){
			moLinker.doLinkDataToControl(_DrvCst, sControlName);
		}
		/**
		 * Link the U_TotRun Field to the Form Item.
		 */
		public void doLink_TotRun(string sControlName){
			moLinker.doLinkDataToControl(_TotRun, sControlName);
		}
		/**
		 * Link the U_PlusOH Field to the Form Item.
		 */
		public void doLink_PlusOH(string sControlName){
			moLinker.doLinkDataToControl(_PlusOH, sControlName);
		}
		/**
		 * Link the U_HaulChrg Field to the Form Item.
		 */
		public void doLink_HaulChrg(string sControlName){
			moLinker.doLinkDataToControl(_HaulChrg, sControlName);
		}
		/**
		 * Link the U_UOM Field to the Form Item.
		 */
		public void doLink_UOM(string sControlName){
			moLinker.doLinkDataToControl(_UOM, sControlName);
		}
#endregion

	}
}
