﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.dbObjects.User;

namespace com.uBC.utils {
   public class DOWrapper {

#region MemberVariables
       private IDH_DISPORD moIDH_DISPORD;
       //private IDH_DISPROW moIDH_DISPROW;
       
       //private string msTNUM;
       //private bool mbCalledFromGUI;

       public static string msDOWrapper = "DOWrapper";
       
       //public static readonly string _TicketNumber = "U_TNum", _Analys1 = IDH_DISPORD._Analys1, _Analys2 = IDH_DISPORD._Analys2,
       //    _Analys3 = IDH_DISPORD._Analys3, _Analys4 = IDH_DISPORD._Analys4, _Analys5 = IDH_DISPORD._Analys5,
       //    _BookingDate = IDH_DISPORD._BDate, _BookingTime = IDH_DISPORD._BTime, _CustomerCd = IDH_DISPORD._CardCd,
       //    _CustomerNM = IDH_DISPORD._CardNM, _CustomerRefNo = IDH_DISPORD._CustRef, _ContactNumber = IDH_DISPORD._CntrNo,
       //    _CarrierCode = IDH_DISPORD._CCardCd, _CarrierName = IDH_DISPORD._CCardNM, _DAAttached = IDH_DISPORD._DAAttach,
       //    _ENNumber = IDH_DISPORD._ENNO, _LockDownPrices = IDH_DISPORD._LckPrc, _OrderType = IDH_DISPORD._ORDTP,
       //    _Origin = IDH_DISPORD._Origin, _ProducerCode = IDH_DISPORD._PCardCd, _ProducerName = IDH_DISPORD._PCardNM,
       //    _ProducerRefNo = IDH_DISPORD._ProRef, _RequestToBeArchived = IDH_DISPORD._ReqArch,
       //    _RequiredTimeFrom = IDH_DISPORD._RTIMEF, _RequiredTimeTo = IDH_DISPORD._RTIMET, _RouteCode = IDH_DISPORD._Route,
       //    _SiteAddress = IDH_DISPORD._SAddress, _SiteReferenceNo = IDH_DISPORD._SiteRef, _SkipLicenseCharge = IDH_DISPORD._SLicCh,
       //    _SkipLicenseExpiryDate = IDH_DISPORD._SLicExp, _SkipLicenseNo = IDH_DISPORD._SLicNr, _SkipLicenseSupplier = IDH_DISPORD._SLicSp,
       //    _SkipLicenseSupplierNM = IDH_DISPORD._SLicNm, _SupplierReferenceNo = IDH_DISPORD._SupRef, _TransactionCode = IDH_DISPORD._TRNCd,
       //    _WasteLicenseRegNo = IDH_DISPORD._WasLic, _ItemGroupCode = IDH_DISPORD._ItemGrp,_User = IDH_DISPORD._User;
       
       //private SortedDictionary<string, string> moPropertyMapper = new SortedDictionary<string, string>() {
       //    {_Analys1,string.Empty},
       //    {_Analys2,string.Empty},
       //    {_Analys3,string.Empty},
       //    {_Analys4,string.Empty},
       //    {_Analys5,string.Empty},
       //    {_BookingDate,string.Empty},
       //    {_BookingTime,string.Empty},
       //    {_CarrierCode,string.Empty},
       //    {_CarrierName,string.Empty},
       //    {_ContactNumber,string.Empty},
       //    {_CustomerCd,string.Empty},
       //    {_CustomerNM,string.Empty},
       //    {_CustomerRefNo,string.Empty},
       //    {_DAAttached,string.Empty},
       //    {_ENNumber,string.Empty},
       //    {_ItemGroupCode,string.Empty},
       //    {_LockDownPrices,string.Empty},
       //    {_OrderType,string.Empty},
       //    {_Origin,string.Empty},
       //    {_ProducerCode,string.Empty},
       //    {_ProducerName,string.Empty},
       //    {_ProducerRefNo,string.Empty},
       //    {_RequestToBeArchived,string.Empty},
       //    {_RequiredTimeFrom,string.Empty},
       //    {_RequiredTimeTo,string.Empty},
       //    {_RouteCode,string.Empty},
       //    {_SiteAddress,string.Empty},
       //    {_SiteReferenceNo,string.Empty},
       //    {_SkipLicenseCharge,string.Empty},
       //    {_SkipLicenseExpiryDate,string.Empty},
       //    {_SkipLicenseNo,string.Empty},
       //    {_SkipLicenseSupplier,string.Empty},
       //    {_SkipLicenseSupplierNM,string.Empty},
       //    {_SupplierReferenceNo,string.Empty},
       //    {_TicketNumber,string.Empty},
       //    {_TransactionCode,string.Empty},
       //    {_User,string.Empty},
       //    {_WasteLicenseRegNo,string.Empty},
       //};
#endregion

       public DOWrapper() {
           moIDH_DISPORD = new IDH_DISPORD();
           //moIDH_DISPORD.OrderRows = new IDH_DISPROW( moIDH_DISPORD);
       }

#region Instance Methods
       public bool doAddDataRow(string sComment) {
           return moIDH_DISPORD.doAddDataRow(sComment);
       }

       public bool doUpdateDataRow(string sComment) {
           return moIDH_DISPORD.doUpdateDataRow(sComment);
       }

       //private string doGetMappedProperty(string sFieldName) {
       //    foreach (string sMappedProperty in moPropertyMapper.Keys) {
       //        if (moPropertyMapper[sMappedProperty].Equals(sFieldName))
       //            return sMappedProperty;
       //    }
       //    return string.Empty;
       //}
       
       //private string doGetReversedMappedProperty(string sFieldName) {
       //    if (moPropertyMapper.ContainsKey(sFieldName))
       //        return moPropertyMapper[sFieldName];
       //    return string.Empty;
       //}
       
       //public void doSetPropertyFromString(string sFieldName, double dVal) {
       //    CalledFromGUI = true;
       //    try {
       //        if (sFieldName.Equals(doGetMappedProperty(_SkipLicenseCharge)))
       //            SkipLicenseCharge = dVal;
       //    } finally {
       //        CalledFromGUI = false;
       //    }
       //}
       
       //public void doSetPropertyFromString(string sFieldName, string sVal) {
       //    try {
       //        CalledFromGUI = true;
       //        if (sFieldName.Equals(doGetMappedProperty(_Analys1)))
       //            Analys1 = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_Analys2)))
       //            Analys2 = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_Analys3)))
       //            Analys3 = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_Analys4)))
       //            Analys4 = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_Analys5)))
       //            Analys5 = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_BookingTime)))
       //            BookingTime = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_CarrierCode)))
       //            CarrierCode = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_CarrierName)))
       //            CarrierName = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_ContactNumber)))
       //            ContactNumber = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_CustomerCd)))
       //            CustomerCd = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_CustomerNM)))
       //            CustomerNM = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_CustomerRefNo)))
       //            CustomerRefNo = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_DAAttached)))
       //            DAAttached = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_ENNumber)))
       //            ENNumber = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_LockDownPrices)))
       //            LockDownPrices = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_OrderType)))
       //            OrderType = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_Origin)))
       //            Origin = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_ProducerCode)))
       //            ProducerCode = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_ProducerName)))
       //            ProducerName = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_ProducerRefNo)))
       //            ProducerRefNo = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_RequestToBeArchived)))
       //            RequestToBeArchived = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_RequiredTimeFrom)))
       //            RequiredTimeFrom = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_RequiredTimeTo)))
       //            RequiredTimeTo = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_RouteCode)))
       //            RouteCode = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_SiteAddress)))
       //            SiteAddress = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_SiteReferenceNo)))
       //            SiteReferenceNo = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_SkipLicenseNo)))
       //            SkipLicenseNo = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_SkipLicenseSupplierNM)))
       //            SkipLicenseSupplierNM = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_SkipLicenseSupplier)))
       //            SkipLicenseSupplier = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_SupplierReferenceNo)))
       //            SupplierReferenceNo = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_TicketNumber)))
       //            TicketNumber = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_TransactionCode)))
       //            TransactionCode = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_WasteLicenseRegNo)))
       //            WasteLicenseRegNo = sVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_ItemGroupCode)))
       //            ItemGroupCode = sVal;
       //    } finally {
       //        CalledFromGUI = false;
       //    }
       //}
       
       //public void doSetPropertyFromString(string sFieldName, DateTime dtVal) {
       //    try {
       //        CalledFromGUI = true;
       //        if (sFieldName.Equals(doGetMappedProperty(_BookingDate)))
       //            BookingDate = dtVal;
       //        else if (sFieldName.Equals(doGetMappedProperty(_SkipLicenseExpiryDate)))
       //            SkipLicenseExpiryDate = dtVal;
       //    } finally {
       //        CalledFromGUI = false;
       //    }
       //}

       //public void doSetDOPropertyFromString(string sFieldName, object oPreviousValue,object oVal) {
       //    try {
       //        CalledFromGUI = true;
       //        DO.doSetFieldHasChanged(sFieldName, oPreviousValue, oVal);
       //    } finally {
       //        CalledFromGUI = false;
       //    }
       //}
       
       //public void doSetDORPropertyFromString(string sFieldName,object oPreviousValue,object oVal) {
       //    try {
       //        CalledFromGUI = true;
       //        DOR.doSetFieldHasChanged(sFieldName, oPreviousValue, oVal);
       //    } finally {
       //        CalledFromGUI = false;
       //    }
       //} 
#endregion

#region Getters & Setters
       //public string TicketNumber {
       //    get { return msTNUM; }
       //    set {
       //        msTNUM = value;
       //        if (!CalledFromGUI)
       //            UpdateGUI(doGetReversedMappedProperty(_TicketNumber), value, ObjectType.STRING);
       //    }
       //}

       //public bool CalledFromGUI {
       //    get { return mbCalledFromGUI; }
       //    private set { mbCalledFromGUI = value; }
       //}
       public IDH_DISPORD DO {
           get { return moIDH_DISPORD; }
       }
       public IDH_DISPROW DOR {
           get { return moIDH_DISPORD.OrderRows; }
       }
       //public SortedDictionary<string, string> PropertyMapper {
       //    get { return moPropertyMapper; }
       //}

        #region CommonFields
        public string Analys1 {
           get { return DO.U_Analys1; }
           set {
               DO.U_Analys1 = value;
               DOR.U_Analys1 = value;
               //if (!CalledFromGUI)
               //    UpdateGUI(doGetReversedMappedProperty(_Analys1), value, ObjectType.STRING);
           }
       }
        public string Analys2 {
            get { return DO.U_Analys2; }
            set {
                DO.U_Analys2 = value;
                DOR.U_Analys2 = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_Analys2), value, ObjectType.STRING);
            }
        }
        public string Analys3 {
            get { return DO.U_Analys3; }
            set {
                DO.U_Analys3 = value;
                DOR.U_Analys3 = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_Analys3), value, ObjectType.STRING);
            }
        }
        public string Analys4 {
            get { return DO.U_Analys4; }
            set {
                DO.U_Analys4 = value;
                DOR.U_Analys4 = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_Analys4), value, ObjectType.STRING);
            }
        }
        public string Analys5 {
            get { return DO.U_Analys5; }
            set {
                DO.U_Analys5 = value;
                DOR.U_Analys5 = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_Analys5), value, ObjectType.STRING);
            }
        }
        public DateTime BookingDate {
            get { return DO.U_BDate; }
            set {
                DO.U_BDate = value;
                DOR.U_BDate = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_BookingDate), value, ObjectType.DATETIME);
            }
        }
        public string BookingTime {
            get { return DO.U_BTime; }
            set {
                DO.U_BTime = value;
                DOR.U_BTime = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_BookingTime), value, ObjectType.STRING);
            }
        }
        public string CustomerCd {
            get { return DO.U_CardCd; }
            set {
                DO.U_CardCd = value;
                DOR.U_CustCd = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_CustomerCd), value, ObjectType.STRING);
            }
        }
        public string CustomerNM {
            get { return DO.U_CardNM; }
            set {
                DO.U_CardNM = value;
                DOR.U_CustNm = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_CustomerNM), value, ObjectType.STRING);
            }
        }
        public string CustomerRefNo {
            get { return DO.U_CustRef; }
            set {
                DO.U_CustRef = value;
                DOR.U_CustRef = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_CustomerRefNo), value, ObjectType.STRING);
            }
        }
        public string ContactNumber {
            get { return DO.U_CntrNo; }
            set {
                DO.U_CntrNo = value;
                DOR.U_CntrNo = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_ContactNumber), value, ObjectType.STRING);
            }
        }
        public string CarrierCode {
            get { return DO.U_CCardCd; }
            set {
                DO.U_CCardCd = value;
                DOR.U_CarrCd = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_CarrierCode), value, ObjectType.STRING);
            }
        }
        public string CarrierName {
            get { return DO.U_CCardNM; }
            set {
                DO.U_CCardNM = value;
                DOR.U_CarrNm = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_CarrierName), value, ObjectType.STRING);
            }
        }
        public string DAAttached {
            get { return DO.U_DAAttach; }
            set {
                DO.U_DAAttach = value;
                DOR.U_DAAttach = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_DAAttached), value, ObjectType.STRING);
            }
        }
        public string ENNumber {
            get { return DO.U_ENNO; }
            set {
                DO.U_ENNO = value;
                DOR.U_ENNO = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_ENNumber), value, ObjectType.STRING);
            }
        }
        public string ItemGroupCode {
            get { return DO.U_ItemGrp; }
            set {
                DO.U_ItemGrp = value;
                DOR.U_ItmGrp = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_ItemGroupCode), value, ObjectType.STRING);
            }
        }
        public string LockDownPrices {
            get { return DO.U_LckPrc; }
            set {
                DO.U_LckPrc = value;
                DOR.U_LckPrc = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_LockDownPrices), value, ObjectType.STRING);
            }
        }
        public string OrderType {
            get { return DO.U_ORDTP; }
            set {
                DO.U_ORDTP = value;
                DOR.U_JobTp = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_OrderType), value, ObjectType.STRING);
            }
        }
        public string Origin {
            get { return DO.U_Origin; }
            set {
                DO.U_Origin = value;
                DOR.U_Origin = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_Origin), value, ObjectType.STRING);
            }
        }
        public string ProducerCode {
            get { return DO.U_PCardCd; }
            set {
                DO.U_PCardCd = value;
                DOR.U_ProCd = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_ProducerCode), value, ObjectType.STRING);
            }
        }
        public string ProducerName {
            get { return DO.U_PCardNM; }
            set {
                DO.U_PCardNM = value;
                DOR.U_ProNm = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_ProducerName), value, ObjectType.STRING);
            }
        }
        public string ProducerRefNo {
            get { return DO.U_ProRef; }
            set {
                DO.U_ProRef = value;
                DOR.U_ProRef = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_ProducerRefNo), value, ObjectType.STRING);
            }
        }
        public string RequestToBeArchived {
            get { return DO.U_ReqArch; }
            set {
                DO.U_ReqArch = value;
                DOR.U_ReqArch = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_RequestToBeArchived), value, ObjectType.STRING);
            }
        }
        public string RequiredTimeFrom {
            get { return DO.U_RTIMEF; }
            set {
                DO.U_RTIMEF = value;
                DOR.U_RTime = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_RequiredTimeFrom), value, ObjectType.STRING);
            }
        }
        public string RequiredTimeTo {
            get { return DO.U_RTIMET; }
            set {
                DO.U_RTIMET = value;
                DOR.U_RTimeT = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_RequiredTimeTo), value, ObjectType.STRING);
            }
        }
        public string RouteCode {
            get { return DO.U_Route; }
            set {
                DO.U_Route = value;
                DOR.U_IDHRTCD = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_RouteCode), value, ObjectType.STRING);
            }
        }
        public string SiteAddress {
            get { return DO.U_SAddress; }
            set {
                DO.U_SAddress = value;
                DOR.U_SAddress = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_SiteAddress), value, ObjectType.STRING);
            }
        }
        public string SiteReferenceNo {
            get { return DO.U_SiteRef; }
            set {
                DO.U_SiteRef = value;
                DOR.U_SiteRef = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_SiteReferenceNo), value, ObjectType.STRING);
            }
        }
        public double SkipLicenseCharge {
            get { return DO.U_SLicCh; }
            set {
                DO.U_SLicCh = value;
                DOR.U_SLicCh = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_SkipLicenseCharge), value, ObjectType.DOUBLE);
            }
        }
        public DateTime SkipLicenseExpiryDate {
            get { return DO.U_SLicExp; }
            set {
                DO.U_SLicExp = value;
                DOR.U_SLicExp = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_SkipLicenseExpiryDate), value, ObjectType.DATETIME);
            }
        }
        public string SkipLicenseNo {
            get { return DO.U_SLicNr; }
            set {
                DO.U_SLicNr = value;
                DOR.U_SLicNr = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_SkipLicenseNo), value, ObjectType.STRING);
            }
        }
        public string SkipLicenseSupplier {
            get { return DO.U_SLicSp; }
            set {
                DO.U_SLicSp = value;
                DOR.U_SLicSp = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_SkipLicenseSupplier), value, ObjectType.STRING);
            }
        }
        public string SkipLicenseSupplierNM {
            get { return DO.U_SLicNm; }
            set {
                DO.U_SLicNm = value;
                DOR.U_SLicNm = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_SkipLicenseSupplierNM), value, ObjectType.STRING);
            }
        }
        public string SupplierReferenceNo {
            get { return DO.U_SupRef; }
            set {
                DO.U_SupRef = value;
                DOR.U_SupRef = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_SupplierReferenceNo), value, ObjectType.STRING);
            }
        }
        public string TransactionCode {
            get { return DO.U_TRNCd; }
            set {
                DO.U_TRNCd = value;
                DOR.U_TrnCode = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_TransactionCode), value, ObjectType.STRING);
            }
        }
        public string WasteLicenseRegNo {
            get { return DO.U_WasLic; }
            set {
                DO.U_WasLic = value;
                DOR.U_WASLIC = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_WasteLicenseRegNo), value, ObjectType.STRING);
            }
        }
        public string User {
            get { return DOR.U_User; }
            set {
                DOR.U_User = value;
                //DO.U_User = value;
                //if (!CalledFromGUI)
                //    UpdateGUI(doGetReversedMappedProperty(_User), value, ObjectType.STRING);
            }
        }
#endregion
#endregion

       //#region Delegates
       //public delegate bool doUpdateGUI(string sFieldName,object sValue,ObjectType oType);
       //#endregion

       //#region Events
       //public event doUpdateGUI UpdateGUI;
       //#endregion

       //#region enums
       //public enum ObjectType {
       //    STRING,
       //    DOUBLE,
       //    DATETIME,
       //}
       //#endregion
   }
}
