/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2013/07/01 08:35:56 AM
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
      
	public class IDH_HAULCST: com.idh.dbObjects.Base.IDH_HAULCST{ 

		public IDH_HAULCST() : base() {
			
		}

   	    public IDH_HAULCST( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	    }

        public double getChrgPriceForDistance(double dDistance) {
            if (getData(dDistance + " < " + _OneWDst, _OneWDst + " ASC") > 0) {
                return U_HaulChrg;
            }
            return -9999;
        }
	}
}
