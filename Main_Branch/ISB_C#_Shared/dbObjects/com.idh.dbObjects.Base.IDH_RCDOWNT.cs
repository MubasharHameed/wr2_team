/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 12/03/2015 12:01:06
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
	public class IDH_RCDOWNT: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
		protected IDHAddOns.idh.forms.Base moIDHForm;

		public IDH_RCDOWNT() : base("@IDH_RCDOWNT"){
		}

		public IDH_RCDOWNT( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_RCDOWNT"){
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_RCDOWNT";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: RouteCL Header
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ROUTECL
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ROUTECL = "U_ROUTECL";
		public string U_ROUTECL { 
			get {
 				return getValueAsString(_ROUTECL); 
			}
			set { setValue(_ROUTECL, value); }
		}
           public string doValidate_ROUTECL() {
               return doValidate_ROUTECL(U_ROUTECL);
           }
           public virtual string doValidate_ROUTECL(object oValue) {
               return base.doValidation(_ROUTECL, oValue);
           }

		/**
		 * Decription: Downtime Operational
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCTIMED
		 * Size: 6
		 * Type: db_Date
		 * CType: string
		 * SubType: st_Time
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _RCTIMED = "U_RCTIMED";
		public string U_RCTIMED { 
			get {
 				return getValueAsString(_RCTIMED); 
			}
			set { setValue(_RCTIMED, value); }
		}
           public string doValidate_RCTIMED() {
               return doValidate_RCTIMED(U_RCTIMED);
           }
           public virtual string doValidate_RCTIMED(object oValue) {
               return base.doValidation(_RCTIMED, oValue);
           }

		/**
		 * Decription: Downtime Mechanical
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCTIMEU
		 * Size: 6
		 * Type: db_Date
		 * CType: string
		 * SubType: st_Time
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _RCTIMEU = "U_RCTIMEU";
		public string U_RCTIMEU { 
			get {
 				return getValueAsString(_RCTIMEU); 
			}
			set { setValue(_RCTIMEU, value); }
		}
           public string doValidate_RCTIMEU() {
               return doValidate_RCTIMEU(U_RCTIMEU);
           }
           public virtual string doValidate_RCTIMEU(object oValue) {
               return base.doValidation(_RCTIMEU, oValue);
           }

		/**
		 * Decription: Spill/Leak
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCSPILL
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RCSPILL = "U_RCSPILL";
		public string U_RCSPILL { 
			get {
 				return getValueAsString(_RCSPILL); 
			}
			set { setValue(_RCSPILL, value); }
		}
           public string doValidate_RCSPILL() {
               return doValidate_RCSPILL(U_RCSPILL);
           }
           public virtual string doValidate_RCSPILL(object oValue) {
               return base.doValidation(_RCSPILL, oValue);
           }

		/**
		 * Decription: Reason
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCREASON
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RCREASON = "U_RCREASON";
		public string U_RCREASON { 
			get {
 				return getValueAsString(_RCREASON); 
			}
			set { setValue(_RCREASON, value); }
		}
           public string doValidate_RCREASON() {
               return doValidate_RCREASON(U_RCREASON);
           }
           public virtual string doValidate_RCREASON(object oValue) {
               return base.doValidation(_RCREASON, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(7);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_ROUTECL, "RouteCL Header", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //RouteCL Header
			moDBFields.Add(_RCTIMED, "Downtime Operational", 3, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Downtime Operational
			moDBFields.Add(_RCTIMEU, "Downtime Mechanical", 4, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Downtime Mechanical
			moDBFields.Add(_RCSPILL, "Spill/Leak", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Spill/Leak
			moDBFields.Add(_RCREASON, "Reason", 6, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Reason

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_ROUTECL());
            doBuildValidationString(doValidate_RCTIMED());
            doBuildValidationString(doValidate_RCTIMEU());
            doBuildValidationString(doValidate_RCSPILL());
            doBuildValidationString(doValidate_RCREASON());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_ROUTECL)) return doValidate_ROUTECL(oValue);
            if (sFieldName.Equals(_RCTIMED)) return doValidate_RCTIMED(oValue);
            if (sFieldName.Equals(_RCTIMEU)) return doValidate_RCTIMEU(oValue);
            if (sFieldName.Equals(_RCSPILL)) return doValidate_RCSPILL(oValue);
            if (sFieldName.Equals(_RCREASON)) return doValidate_RCREASON(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_ROUTECL Field to the Form Item.
		 */
		public void doLink_ROUTECL(string sControlName){
			moLinker.doLinkDataToControl(_ROUTECL, sControlName);
		}
		/**
		 * Link the U_RCTIMED Field to the Form Item.
		 */
		public void doLink_RCTIMED(string sControlName){
			moLinker.doLinkDataToControl(_RCTIMED, sControlName);
		}
		/**
		 * Link the U_RCTIMEU Field to the Form Item.
		 */
		public void doLink_RCTIMEU(string sControlName){
			moLinker.doLinkDataToControl(_RCTIMEU, sControlName);
		}
		/**
		 * Link the U_RCSPILL Field to the Form Item.
		 */
		public void doLink_RCSPILL(string sControlName){
			moLinker.doLinkDataToControl(_RCSPILL, sControlName);
		}
		/**
		 * Link the U_RCREASON Field to the Form Item.
		 */
		public void doLink_RCREASON(string sControlName){
			moLinker.doLinkDataToControl(_RCREASON, sControlName);
		}
#endregion

	}
}
