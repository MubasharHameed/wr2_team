/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2012/01/17 06:20:50 PM
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User
{

    public class IDH_TRAIN01 : com.idh.dbObjects.Base.IDH_TRAIN01
    {
        private IDH_TRAIN01_LNS moLines;
        public IDH_TRAIN01_LNS Lines
        {
            get { return moLines; }
        }

        public IDH_TRAIN01()
            : base()
        {
            msAutoNumKey = "TRN1KEY";
            moLines = new IDH_TRAIN01_LNS(this);

            moLines.Handler_TotalChanged = Hangle_LineTotalChange;
        }

        public IDH_TRAIN01(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oIDHForm, oForm)
        {
            msAutoNumKey = "TRN1KEY";
            moLines = new IDH_TRAIN01_LNS(this);

            moLines.Handler_TotalChanged = Hangle_LineTotalChange;
        }

        public override void doLoadChildren()
        {
            if (Code != null && Code.Length > 0)
            {
                moLines.getByHeaderNumber(Code);
            }
            else
            {
                moLines.doClearBuffers();
            }
        }

        public override void doClearBuffers()
        {
            base.doClearBuffers();
            if (MustLoadChildren)
                moLines.doClearBuffers();
        }

        public override void doApplyDefaults()
        {
            base.doApplyDefaults();

            try
            {
                //doApplyDefaultValue(_Code, "-1");
                //doApplyDefaultValue(_Name, "-1");
                doApplyDefaultValue(_Code, "");
                doApplyDefaultValue(_Name, "");

                doApplyDefaultValue(_CardCode, "");
                doApplyDefaultValue(_CardName, "");
                doApplyDefaultValue(_DocTotal, 0);
                doApplyDefaultValue(_InvID, "");
                doApplyDefaultValue(_InvDate, "");
            }
            catch (Exception) { }
        }

        /*
         * Total change Event
         */
        public void Hangle_LineTotalChange(DBBase oCaller, object dTotal)
        {
            U_DocTotal = (double)dTotal;
        }
    }
}