/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 04/02/2016 17:41:00
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
   [Serializable]
	public class IDH_BTCHITM: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
       //public Linker ControlLinker {
       //    get { return moLinker; }
       //    set { moLinker = value; }
       //}

       private IDHAddOns.idh.forms.Base moIDHForm;
       public IDHAddOns.idh.forms.Base IDHForm {
           get { return moIDHForm; }
           set { moIDHForm = value; }
       }

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_BTCHITM() : base("@IDH_BTCHITM"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_BTCHITM( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_BTCHITM"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_BTCHITM";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Batch Item Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BICode
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BICode = "U_BICode";
		public string U_BICode { 
			get {
 				return getValueAsString(_BICode); 
			}
			set { setValue(_BICode, value); }
		}
           public string doValidate_BICode() {
               return doValidate_BICode(U_BICode);
           }
           public virtual string doValidate_BICode(object oValue) {
               return base.doValidation(_BICode, oValue);
           }

		/**
		 * Decription: Batch Item Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BIName
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BIName = "U_BIName";
		public string U_BIName { 
			get {
 				return getValueAsString(_BIName); 
			}
			set { setValue(_BIName, value); }
		}
           public string doValidate_BIName() {
               return doValidate_BIName(U_BIName);
           }
           public virtual string doValidate_BIName(object oValue) {
               return base.doValidation(_BIName, oValue);
           }

		/**
		 * Decription: New BinLocation
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_NewBinLoc
		 * Size: 228
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _NewBinLoc = "U_NewBinLoc";
		public string U_NewBinLoc { 
			get {
 				return getValueAsString(_NewBinLoc); 
			}
			set { setValue(_NewBinLoc, value); }
		}
           public string doValidate_NewBinLoc() {
               return doValidate_NewBinLoc(U_NewBinLoc);
           }
           public virtual string doValidate_NewBinLoc(object oValue) {
               return base.doValidation(_NewBinLoc, oValue);
           }

		/**
		 * Decription: New Warehouse
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_NewWhse
		 * Size: 18
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _NewWhse = "U_NewWhse";
		public string U_NewWhse { 
			get {
 				return getValueAsString(_NewWhse); 
			}
			set { setValue(_NewWhse, value); }
		}
           public string doValidate_NewWhse() {
               return doValidate_NewWhse(U_NewWhse);
           }
           public virtual string doValidate_NewWhse(object oValue) {
               return base.doValidation(_NewWhse, oValue);
           }

		/**
		 * Decription: Object Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ObjectCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ObjectCd = "U_ObjectCd";
		public string U_ObjectCd { 
			get {
 				return getValueAsString(_ObjectCd); 
			}
			set { setValue(_ObjectCd, value); }
		}
           public string doValidate_ObjectCd() {
               return doValidate_ObjectCd(U_ObjectCd);
           }
           public virtual string doValidate_ObjectCd(object oValue) {
               return base.doValidation(_ObjectCd, oValue);
           }

		/**
		 * Decription: Object Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ObjectType
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ObjectType = "U_ObjectType";
		public string U_ObjectType { 
			get {
 				return getValueAsString(_ObjectType); 
			}
			set { setValue(_ObjectType, value); }
		}
           public string doValidate_ObjectType() {
               return doValidate_ObjectType(U_ObjectType);
           }
           public virtual string doValidate_ObjectType(object oValue) {
               return base.doValidation(_ObjectType, oValue);
           }

		/**
		 * Decription: Quantity
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Quantity
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Quantity = "U_Quantity";
		public double U_Quantity { 
			get {
 				return getValueAsDouble(_Quantity); 
			}
			set { setValue(_Quantity, value); }
		}
           public string doValidate_Quantity() {
               return doValidate_Quantity(U_Quantity);
           }
           public virtual string doValidate_Quantity(object oValue) {
               return base.doValidation(_Quantity, oValue);
           }

		/**
		 * Decription: UOM
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UOM
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _UOM = "U_UOM";
		public string U_UOM { 
			get {
 				return getValueAsString(_UOM); 
			}
			set { setValue(_UOM, value); }
		}
           public string doValidate_UOM() {
               return doValidate_UOM(U_UOM);
           }
           public virtual string doValidate_UOM(object oValue) {
               return base.doValidation(_UOM, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(10);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_BICode, "Batch Item Code", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Batch Item Code
			moDBFields.Add(_BIName, "Batch Item Name", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Batch Item Name
			moDBFields.Add(_NewBinLoc, "New BinLocation", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 228, EMPTYSTR, false, false); //New BinLocation
			moDBFields.Add(_NewWhse, "New Warehouse", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 18, EMPTYSTR, false, false); //New Warehouse
			moDBFields.Add(_ObjectCd, "Object Code", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Object Code
			moDBFields.Add(_ObjectType, "Object Type", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Object Type
			moDBFields.Add(_Quantity, "Quantity", 8, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Quantity
			moDBFields.Add(_UOM, "UOM", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //UOM

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_BICode());
            doBuildValidationString(doValidate_BIName());
            doBuildValidationString(doValidate_NewBinLoc());
            doBuildValidationString(doValidate_NewWhse());
            doBuildValidationString(doValidate_ObjectCd());
            doBuildValidationString(doValidate_ObjectType());
            doBuildValidationString(doValidate_Quantity());
            doBuildValidationString(doValidate_UOM());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_BICode)) return doValidate_BICode(oValue);
            if (sFieldName.Equals(_BIName)) return doValidate_BIName(oValue);
            if (sFieldName.Equals(_NewBinLoc)) return doValidate_NewBinLoc(oValue);
            if (sFieldName.Equals(_NewWhse)) return doValidate_NewWhse(oValue);
            if (sFieldName.Equals(_ObjectCd)) return doValidate_ObjectCd(oValue);
            if (sFieldName.Equals(_ObjectType)) return doValidate_ObjectType(oValue);
            if (sFieldName.Equals(_Quantity)) return doValidate_Quantity(oValue);
            if (sFieldName.Equals(_UOM)) return doValidate_UOM(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_BICode Field to the Form Item.
		 */
		public void doLink_BICode(string sControlName){
			moLinker.doLinkDataToControl(_BICode, sControlName);
		}
		/**
		 * Link the U_BIName Field to the Form Item.
		 */
		public void doLink_BIName(string sControlName){
			moLinker.doLinkDataToControl(_BIName, sControlName);
		}
		/**
		 * Link the U_NewBinLoc Field to the Form Item.
		 */
		public void doLink_NewBinLoc(string sControlName){
			moLinker.doLinkDataToControl(_NewBinLoc, sControlName);
		}
		/**
		 * Link the U_NewWhse Field to the Form Item.
		 */
		public void doLink_NewWhse(string sControlName){
			moLinker.doLinkDataToControl(_NewWhse, sControlName);
		}
		/**
		 * Link the U_ObjectCd Field to the Form Item.
		 */
		public void doLink_ObjectCd(string sControlName){
			moLinker.doLinkDataToControl(_ObjectCd, sControlName);
		}
		/**
		 * Link the U_ObjectType Field to the Form Item.
		 */
		public void doLink_ObjectType(string sControlName){
			moLinker.doLinkDataToControl(_ObjectType, sControlName);
		}
		/**
		 * Link the U_Quantity Field to the Form Item.
		 */
		public void doLink_Quantity(string sControlName){
			moLinker.doLinkDataToControl(_Quantity, sControlName);
		}
		/**
		 * Link the U_UOM Field to the Form Item.
		 */
		public void doLink_UOM(string sControlName){
			moLinker.doLinkDataToControl(_UOM, sControlName);
		}
#endregion

	}
}
