/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 12/03/2015 12:00:41
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
	public class IDH_ROUTECL: com.idh.dbObjects.Base.IDH_ROUTECL{ 

		public IDH_ROUTECL() : base() {
			
		}

   	public IDH_ROUTECL( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	}
	}
}
