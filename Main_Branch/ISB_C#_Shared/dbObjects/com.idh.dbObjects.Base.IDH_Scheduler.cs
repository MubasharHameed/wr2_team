/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2013/03/12 03:31:14 PM
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;

namespace com.idh.dbObjects.Base{
      
	public class IDH_Scheduler: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
		protected IDHAddOns.idh.forms.Base moIDHForm;

		public IDH_Scheduler() : base("IDH_Scheduler"){
		}

		public IDH_Scheduler( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "IDH_Scheduler"){
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "IDH_Scheduler";

		/**
		 * Decription: CurrCount
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: CurrCount
		 * Size: 19
		 * Type: db_Numeric
		 * CType: long
		 * SubType: st_Sum
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CurrCount = "CurrCount";
		public long CurrCount { 
			get {
 				return (long)getValue(_CurrCount); 
			}
			set { setValue(_CurrCount, value); }
		}

		/**
		 * Decription: MaxCount
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: MaxCount
		 * Size: 19
		 * Type: db_Numeric
		 * CType: long
		 * SubType: st_Sum
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _MaxCount = "MaxCount";
		public long MaxCount { 
			get {
 				return (long)getValue(_MaxCount); 
			}
			set { setValue(_MaxCount, value); }
		}

		/**
		 * Decription: SchedId
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: SchedId
		 * Size: 10
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: True
		 */
		public readonly static string _SchedId = "SchedId";
		public int SchedId { 
			get {
 				return getValueAsInt(_SchedId); 
			}
		}

		/**
		 * Decription: Description
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Description
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Description = "Description";
		public string Description { 
			get {
 				return getValueAsString(_Description); 
			}
			set { setValue(_Description, value); }
		}

		/**
		 * Decription: Class
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Class
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Class = "Class";
		public string Class { 
			get {
 				return getValueAsString(_Class); 
			}
			set { setValue(_Class, value); }
		}

		/**
		 * Decription: Processor
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Processor
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Processor = "Processor";
		public string Processor { 
			get {
 				return getValueAsString(_Processor); 
			}
			set { setValue(_Processor, value); }
		}

		/**
		 * Decription: StartDate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: StartDate
		 * Size: 0
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _StartDate = "StartDate";
		public DateTime StartDate { 
			get {
 				return getValueAsDateTime(_StartDate); 
			}
			set { setValue(_StartDate, value); }
		}
		public void StartDate_AsString(string value){
			setValue("StartDate", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string StartDate_AsString(){
			DateTime dVal = getValueAsDateTime(_StartDate);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}

		/**
		 * Decription: Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Status
		 * Size: 5
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Status = "Status";
		public string Status { 
			get {
 				return getValueAsString(_Status); 
			}
			set { setValue(_Status, value); }
		}

		/**
		 * Decription: EndDate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: EndDate
		 * Size: 0
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _EndDate = "EndDate";
		public DateTime EndDate { 
			get {
 				return getValueAsDateTime(_EndDate); 
			}
			set { setValue(_EndDate, value); }
		}
		public void EndDate_AsString(string value){
			setValue("EndDate", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string EndDate_AsString(){
			DateTime dVal = getValueAsDateTime(_EndDate);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}

		/**
		 * Decription: PMonth
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: PMonth
		 * Size: 5
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PMonth = "PMonth";
		public short PMonth { 
			get {
 				return getValueAsShort(_PMonth); 
			}
			set { setValue(_PMonth, value); }
		}

		/**
		 * Decription: MonthDay
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: MonthDay
		 * Size: 5
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _MonthDay = "MonthDay";
		public short MonthDay { 
			get {
 				return getValueAsShort(_MonthDay); 
			}
			set { setValue(_MonthDay, value); }
		}

		/**
		 * Decription: Su
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: Su
		 * Size: 5
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Su = "Su";
		public string Su { 
			get {
 				return getValueAsString(_Su); 
			}
			set { setValue(_Su, value); }
		}

		/**
		 * Decription: Mo
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: Mo
		 * Size: 5
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Mo = "Mo";
		public string Mo { 
			get {
 				return getValueAsString(_Mo); 
			}
			set { setValue(_Mo, value); }
		}

		/**
		 * Decription: Tu
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: Tu
		 * Size: 5
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Tu = "Tu";
		public string Tu { 
			get {
 				return getValueAsString(_Tu); 
			}
			set { setValue(_Tu, value); }
		}

		/**
		 * Decription: We
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: We
		 * Size: 5
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _We = "We";
		public string We { 
			get {
 				return getValueAsString(_We); 
			}
			set { setValue(_We, value); }
		}

		/**
		 * Decription: Th
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: Th
		 * Size: 5
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Th = "Th";
		public string Th { 
			get {
 				return getValueAsString(_Th); 
			}
			set { setValue(_Th, value); }
		}

		/**
		 * Decription: Fr
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: Fr
		 * Size: 5
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Fr = "Fr";
		public string Fr { 
			get {
 				return getValueAsString(_Fr); 
			}
			set { setValue(_Fr, value); }
		}

		/**
		 * Decription: Sa
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: Sa
		 * Size: 5
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Sa = "Sa";
		public string Sa { 
			get {
 				return getValueAsString(_Sa); 
			}
			set { setValue(_Sa, value); }
		}

		/**
		 * Decription: ProcDate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: ProcDate
		 * Size: 0
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ProcDate = "ProcDate";
		public DateTime ProcDate { 
			get {
 				return getValueAsDateTime(_ProcDate); 
			}
			set { setValue(_ProcDate, value); }
		}
		public void ProcDate_AsString(string value){
			setValue("ProcDate", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string ProcDate_AsString(){
			DateTime dVal = getValueAsDateTime(_ProcDate);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}

		/**
		 * Decription: ProcTime
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: ProcTime
		 * Size: 5
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ProcTime = "ProcTime";
		public string ProcTime { 
			get {
 				return getValueAsString(_ProcTime); 
			}
			set { setValue(_ProcTime, value); }
		}

		/**
		 * Decription: Parameters
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: Parameters
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Parameters = "Parameters";
		public string Parameters { 
			get {
 				return getValueAsString(_Parameters); 
			}
			set { setValue(_Parameters, value); }
		}

		/**
		 * Decription: LastRun
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: LastRun
		 * Size: 0
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _LastRun = "LastRun";
		public DateTime LastRun { 
			get {
 				return getValueAsDateTime(_LastRun); 
			}
			set { setValue(_LastRun, value); }
		}
		public void LastRun_AsString(string value){
			setValue("LastRun", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string LastRun_AsString(){
			DateTime dVal = getValueAsDateTime(_LastRun);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}

		/**
		 * Decription: LastRnTm
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: LastRnTm
		 * Size: 5
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _LastRnTm = "LastRnTm";
		public string LastRnTm { 
			get {
 				return getValueAsString(_LastRnTm); 
			}
			set { setValue(_LastRnTm, value); }
		}

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(25);

			moDBFields.Add(_CurrCount, "CurrCount", 0, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_Sum, 19, 0, false); //CurrCount
			moDBFields.Add(_MaxCount, "MaxCount", 1, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_Sum, 19, 0, false); //MaxCount
			moDBFields.Add(_SchedId, "SchedId", 2, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10, 0, true); //SchedId
			RowKeyField = _SchedId;

			moDBFields.Add(_Description, "Description", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false); //Description
			moDBFields.Add(_Class, "Class", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false); //Class
			moDBFields.Add(_Processor, "Processor", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false); //Processor
			moDBFields.Add(_StartDate, "StartDate", 6, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 0, null, false); //StartDate
			moDBFields.Add(_Status, "Status", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 5, EMPTYSTR, false); //Status
			moDBFields.Add(_EndDate, "EndDate", 8, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 0, null, false); //EndDate
			moDBFields.Add(_PMonth, "PMonth", 9, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 5, 0, false); //PMonth
			moDBFields.Add(_MonthDay, "MonthDay", 10, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 5, 0, false); //MonthDay
			moDBFields.Add(_Su, "Su", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 5, EMPTYSTR, false); //Su
			moDBFields.Add(_Mo, "Mo", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 5, EMPTYSTR, false); //Mo
			moDBFields.Add(_Tu, "Tu", 13, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 5, EMPTYSTR, false); //Tu
			moDBFields.Add(_We, "We", 14, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 5, EMPTYSTR, false); //We
			moDBFields.Add(_Th, "Th", 15, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 5, EMPTYSTR, false); //Th
			moDBFields.Add(_Fr, "Fr", 16, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 5, EMPTYSTR, false); //Fr
			moDBFields.Add(_Sa, "Sa", 17, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 5, EMPTYSTR, false); //Sa
			moDBFields.Add(_ProcDate, "ProcDate", 18, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 0, null, false); //ProcDate
			moDBFields.Add(_ProcTime, "ProcTime", 19, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 5, EMPTYSTR, false); //ProcTime
			moDBFields.Add(_Parameters, "Parameters", 20, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR, false); //Parameters
			moDBFields.Add(_LastRun, "LastRun", 21, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 0, null, false); //LastRun
			moDBFields.Add(_LastRnTm, "LastRnTm", 22, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 5, EMPTYSTR, false); //LastRnTm

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			string sVal;
           object oVal;
			msLastValidationError = "";
			oVal = CurrCount;
			if ( oVal == null )
				msLastValidationError += _CurrCount + ',';

			oVal = MaxCount;
			if ( oVal == null )
				msLastValidationError += _MaxCount + ',';

			sVal = Description;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Description + ',' ;

			sVal = Class;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Class + ',' ;

			sVal = Processor;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Processor + ',' ;

			oVal = StartDate;
			if ( oVal == null )
				msLastValidationError += _StartDate + ',';

			sVal = Status;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Status + ',' ;


			if (msLastValidationError.Length > 0) 
			    return false;
			else
			return true;
		}
#endregion

#region LinkDataToControls
		/**
		 * Link the CurrCount Field to the Form Item.
		 */
		public void doLink_CurrCount(string sControlName){
			moLinker.doLinkDataToControl(_CurrCount, sControlName);
		}
		/**
		 * Link the MaxCount Field to the Form Item.
		 */
		public void doLink_MaxCount(string sControlName){
			moLinker.doLinkDataToControl(_MaxCount, sControlName);
		}
		/**
		 * Link the SchedId Field to the Form Item.
		 */
		public void doLink_SchedId(string sControlName){
			moLinker.doLinkDataToControl(_SchedId, sControlName);
		}
		/**
		 * Link the Description Field to the Form Item.
		 */
		public void doLink_Description(string sControlName){
			moLinker.doLinkDataToControl(_Description, sControlName);
		}
		/**
		 * Link the Class Field to the Form Item.
		 */
		public void doLink_Class(string sControlName){
			moLinker.doLinkDataToControl(_Class, sControlName);
		}
		/**
		 * Link the Processor Field to the Form Item.
		 */
		public void doLink_Processor(string sControlName){
			moLinker.doLinkDataToControl(_Processor, sControlName);
		}
		/**
		 * Link the StartDate Field to the Form Item.
		 */
		public void doLink_StartDate(string sControlName){
			moLinker.doLinkDataToControl(_StartDate, sControlName);
		}
		/**
		 * Link the Status Field to the Form Item.
		 */
		public void doLink_Status(string sControlName){
			moLinker.doLinkDataToControl(_Status, sControlName);
		}
		/**
		 * Link the EndDate Field to the Form Item.
		 */
		public void doLink_EndDate(string sControlName){
			moLinker.doLinkDataToControl(_EndDate, sControlName);
		}
		/**
		 * Link the PMonth Field to the Form Item.
		 */
		public void doLink_PMonth(string sControlName){
			moLinker.doLinkDataToControl(_PMonth, sControlName);
		}
		/**
		 * Link the MonthDay Field to the Form Item.
		 */
		public void doLink_MonthDay(string sControlName){
			moLinker.doLinkDataToControl(_MonthDay, sControlName);
		}
		/**
		 * Link the Su Field to the Form Item.
		 */
		public void doLink_Su(string sControlName){
			moLinker.doLinkDataToControl(_Su, sControlName);
		}
		/**
		 * Link the Mo Field to the Form Item.
		 */
		public void doLink_Mo(string sControlName){
			moLinker.doLinkDataToControl(_Mo, sControlName);
		}
		/**
		 * Link the Tu Field to the Form Item.
		 */
		public void doLink_Tu(string sControlName){
			moLinker.doLinkDataToControl(_Tu, sControlName);
		}
		/**
		 * Link the We Field to the Form Item.
		 */
		public void doLink_We(string sControlName){
			moLinker.doLinkDataToControl(_We, sControlName);
		}
		/**
		 * Link the Th Field to the Form Item.
		 */
		public void doLink_Th(string sControlName){
			moLinker.doLinkDataToControl(_Th, sControlName);
		}
		/**
		 * Link the Fr Field to the Form Item.
		 */
		public void doLink_Fr(string sControlName){
			moLinker.doLinkDataToControl(_Fr, sControlName);
		}
		/**
		 * Link the Sa Field to the Form Item.
		 */
		public void doLink_Sa(string sControlName){
			moLinker.doLinkDataToControl(_Sa, sControlName);
		}
		/**
		 * Link the ProcDate Field to the Form Item.
		 */
		public void doLink_ProcDate(string sControlName){
			moLinker.doLinkDataToControl(_ProcDate, sControlName);
		}
		/**
		 * Link the ProcTime Field to the Form Item.
		 */
		public void doLink_ProcTime(string sControlName){
			moLinker.doLinkDataToControl(_ProcTime, sControlName);
		}
		/**
		 * Link the Parameters Field to the Form Item.
		 */
		public void doLink_Parameters(string sControlName){
			moLinker.doLinkDataToControl(_Parameters, sControlName);
		}
		/**
		 * Link the LastRun Field to the Form Item.
		 */
		public void doLink_LastRun(string sControlName){
			moLinker.doLinkDataToControl(_LastRun, sControlName);
		}
		/**
		 * Link the LastRnTm Field to the Form Item.
		 */
		public void doLink_LastRnTm(string sControlName){
			moLinker.doLinkDataToControl(_LastRnTm, sControlName);
		}
#endregion

	}
}
