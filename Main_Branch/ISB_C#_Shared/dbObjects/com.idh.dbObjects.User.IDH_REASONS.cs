/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2015/02/02 07:01:15 AM
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
   [Serializable] 
	public class IDH_REASONS: com.idh.dbObjects.Base.IDH_REASONS{ 

		public IDH_REASONS() : base() {
			
		}

   	public IDH_REASONS( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	}
	}
}
