/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2012/01/30 10:44:33 AM
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;

namespace com.idh.dbObjects.Base{
      
	public class vRR2x: com.idh.dbObjects.DBBase { 

		private static string[] moLocalDBFields = null;
		private static string msLocalSelectList = null;
		//private Linker moLinker = null;
		protected IDHAddOns.idh.forms.Base moIDHForm;

		public vRR2x() : base("vRR2x"){
		}

		public vRR2x( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "vRR2x"){
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
        /**
         * Table name
         */
        public readonly static string TableName = "vRR2x";

		/**
		 * Decription: SiteName
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: SiteName
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _SiteName = "SiteName";
		public string SiteName { 
			get {
           		return getValueAsString(_SiteName); 
           }
			set { setValue(_SiteName, value); }
		}

		/**
		 * Decription: MasterCode
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: MasterCode
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _MasterCode = "MasterCode";
		public string MasterCode { 
			get {
           		return getValueAsString(_MasterCode); 
           }
			set { setValue(_MasterCode, value); }
		}

		/**
		 * Decription: ChildCode
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: ChildCode
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _ChildCode = "ChildCode";
		public string ChildCode { 
			get {
           		return getValueAsString(_ChildCode); 
           }
			set { setValue(_ChildCode, value); }
		}

		/**
		 * Decription: CardCode
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: CardCode
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _CardCode = "CardCode";
		public string CardCode { 
			get {
           		return getValueAsString(_CardCode); 
           }
			set { setValue(_CardCode, value); }
		}

		/**
		 * Decription: CardName
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: CardName
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _CardName = "CardName";
		public string CardName { 
			get {
           		return getValueAsString(_CardName); 
           }
			set { setValue(_CardName, value); }
		}

		/**
		 * Decription: SupplierCode
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: SupplierCode
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _SupplierCode = "SupplierCode";
		public string SupplierCode { 
			get {
           		return getValueAsString(_SupplierCode); 
           }
			set { setValue(_SupplierCode, value); }
		}

		/**
		 * Decription: SupplierName
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: SupplierName
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _SupplierName = "SupplierName";
		public string SupplierName { 
			get {
           		return getValueAsString(_SupplierName); 
           }
			set { setValue(_SupplierName, value); }
		}

		/**
		 * Decription: SiteCode
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: SiteCode
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _SiteCode = "SiteCode";
		public string SiteCode { 
			get {
           		return getValueAsString(_SiteCode); 
           }
			set { setValue(_SiteCode, value); }
		}

		/**
		 * Decription: ParentItemCode
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: ParentItemCode
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _ParentItemCode = "ParentItemCode";
		public string ParentItemCode { 
			get {
           		return getValueAsString(_ParentItemCode); 
           }
			set { setValue(_ParentItemCode, value); }
		}

		/**
		 * Decription: ParentItemName
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: ParentItemName
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _ParentItemName = "ParentItemName";
		public string ParentItemName { 
			get {
           		return getValueAsString(_ParentItemName); 
           }
			set { setValue(_ParentItemName, value); }
		}

		/**
		 * Decription: ChildItemCode
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: ChildItemCode
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _ChildItemCode = "ChildItemCode";
		public string ChildItemCode { 
			get {
           		return getValueAsString(_ChildItemCode); 
           }
			set { setValue(_ChildItemCode, value); }
		}

		/**
		 * Decription: ChildItemName
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: ChildItemName
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _ChildItemName = "ChildItemName";
		public string ChildItemName { 
			get {
           		return getValueAsString(_ChildItemName); 
           }
			set { setValue(_ChildItemName, value); }
		}

		/**
		 * Decription: JobType
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: JobType
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _JobType = "JobType";
		public string JobType { 
			get {
           		return getValueAsString(_JobType); 
           }
			set { setValue(_JobType, value); }
		}

		/**
		 * Decription: JobDate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: JobDate
		 * Size: 0
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _JobDate = "JobDate";
		public DateTime JobDate { 
			get {
           		return getValueAsDateTime(_JobDate); 
           }
			set { setValue(_JobDate, value); }
		}
		public void JobDate_AsString(string value){
			setValue("JobDate", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string JobDate_AsString(){
            DateTime dVal = getValueAsDateTime(_JobDate);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}

		/**
		 * Decription: IsParentItemLandFill
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: IsParentItemLandFill
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _IsParentItemLandFill = "IsParentItemLandFill";
		public string IsParentItemLandFill { 
			get {
           		return getValueAsString(_IsParentItemLandFill); 
           }
			set { setValue(_IsParentItemLandFill, value); }
		}

		/**
		 * Decription: IsChildItemLandFill
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: IsChildItemLandFill
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _IsChildItemLandFill = "IsChildItemLandFill";
		public string IsChildItemLandFill { 
			get {
           		return getValueAsString(_IsChildItemLandFill); 
           }
			set { setValue(_IsChildItemLandFill, value); }
		}

		/**
		 * Decription: SupItmLFPercent
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: SupItmLFPercent
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _SupItmLFPercent = "SupItmLFPercent";
		public double SupItmLFPercent { 
			get {
           		return getValueAsDouble(_SupItmLFPercent); 
           }
			set { setValue(_SupItmLFPercent, value); }
		}

		/**
		 * Decription: SupItmRCPercent
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: SupItmRCPercent
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _SupItmRCPercent = "SupItmRCPercent";
		public double SupItmRCPercent { 
			get {
           		return getValueAsDouble(_SupItmRCPercent); 
           }
			set { setValue(_SupItmRCPercent, value); }
		}

		/**
		 * Decription: U_OrdWgt
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: U_OrdWgt
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _OrdWgt = "U_OrdWgt";
		public double U_OrdWgt { 
			get {
           		return getValueAsDouble(_OrdWgt); 
           }
			set { setValue(_OrdWgt, value); }
		}

		/**
		 * Decription: BinWgt
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: BinWgt
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _BinWgt = "BinWgt";
		public double BinWgt { 
			get {
           		return getValueAsDouble(_BinWgt); 
           }
			set { setValue(_BinWgt, value); }
		}

		/**
		 * Decription: U_CHDICODE
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: U_CHDICODE
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _CHDICODE = "U_CHDICODE";
		public string U_CHDICODE { 
			get {
           		return getValueAsString(_CHDICODE); 
           }
			set { setValue(_CHDICODE, value); }
		}

		/**
		 * Decription: U_CHDPERCENT
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: U_CHDPERCENT
		 * Size: 10
		 * Type: db_Float
		 * CType: int
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _CHDPERCENT = "U_CHDPERCENT";
		public int U_CHDPERCENT { 
			get {
           		return getValueAsInt(_CHDPERCENT); 
           }
			set { setValue(_CHDPERCENT, value); }
		}

		/**
		 * Decription: FinalWgt
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: FinalWgt
		 * Size: 34
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _FinalWgt = "FinalWgt";
		public double FinalWgt { 
			get {
           		return getValueAsDouble(_FinalWgt); 
           }
			set { setValue(_FinalWgt, value); }
		}

		/**
		 * Decription: U_CHDINAME
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: U_CHDINAME
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _CHDINAME = "U_CHDINAME";
		public string U_CHDINAME { 
			get {
           		return getValueAsString(_CHDINAME); 
           }
			set { setValue(_CHDINAME, value); }
		}

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(26);

			moDBFields.Add(_SiteName, "SiteName", 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR); //SiteName
			moDBFields.Add(_MasterCode, "MasterCode", 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR); //MasterCode
			moDBFields.Add(_ChildCode, "ChildCode", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR); //ChildCode
			moDBFields.Add(_CardCode, "CardCode", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR); //CardCode
			moDBFields.Add(_CardName, "CardName", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR); //CardName
			moDBFields.Add(_SupplierCode, "SupplierCode", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR); //SupplierCode
			moDBFields.Add(_SupplierName, "SupplierName", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR); //SupplierName
			moDBFields.Add(_SiteCode, "SiteCode", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR); //SiteCode
			moDBFields.Add(_ParentItemCode, "ParentItemCode", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR); //ParentItemCode
			moDBFields.Add(_ParentItemName, "ParentItemName", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR); //ParentItemName
			moDBFields.Add(_ChildItemCode, "ChildItemCode", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR); //ChildItemCode
			moDBFields.Add(_ChildItemName, "ChildItemName", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR); //ChildItemName
			moDBFields.Add(_JobType, "JobType", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR); //JobType
			moDBFields.Add(_JobDate, "JobDate", 13, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 0, null); //JobDate
			moDBFields.Add(_IsParentItemLandFill, "IsParentItemLandFill", 14, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR); //IsParentItemLandFill
			moDBFields.Add(_IsChildItemLandFill, "IsChildItemLandFill", 15, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR); //IsChildItemLandFill
			moDBFields.Add(_SupItmLFPercent, "SupItmLFPercent", 16, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //SupItmLFPercent
			moDBFields.Add(_SupItmRCPercent, "SupItmRCPercent", 17, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //SupItmRCPercent
			moDBFields.Add(_OrdWgt, "U_OrdWgt", 18, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //U_OrdWgt
			moDBFields.Add(_BinWgt, "BinWgt", 19, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //BinWgt
			moDBFields.Add(_CHDICODE, "U_CHDICODE", 20, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR); //U_CHDICODE
			moDBFields.Add(_CHDPERCENT, "U_CHDPERCENT", 21, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 10, 0); //U_CHDPERCENT
			moDBFields.Add(_FinalWgt, "FinalWgt", 22, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 34, 0); //FinalWgt
			moDBFields.Add(_CHDINAME, "U_CHDINAME", 23, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR); //U_CHDINAME

		if ( moLocalDBFields == null ) {
				moLocalDBFields = new string[] {"SiteName","MasterCode","ChildCode","CardCode","CardName","SupplierCode","SupplierName","SiteCode","ParentItemCode","ParentItemName","ChildItemCode","ChildItemName","JobType","JobDate","IsParentItemLandFill","IsChildItemLandFill","SupItmLFPercent","SupItmRCPercent","U_OrdWgt","BinWgt","U_CHDICODE","U_CHDPERCENT","FinalWgt","U_CHDINAME"};
			}
			if ( msLocalSelectList == null ){
				doBuildSelectionList();
				msLocalSelectList = msSelectionList;
			}
			else 
				msSelectionList = msLocalSelectList;

		}

#endregion

#region validation
		public override bool doValidation(){
			string sVal;
           object oVal;
			msLastValidationError = "";
			sVal = CardName;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _CardName + ',' ;
			sVal = SupplierCode;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _SupplierCode + ',' ;
			sVal = SupplierName;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _SupplierName + ',' ;
			sVal = SiteCode;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _SiteCode + ',' ;
			sVal = ParentItemCode;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _ParentItemCode + ',' ;
			sVal = ParentItemName;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _ParentItemName + ',' ;
			sVal = ChildItemCode;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _ChildItemCode + ',' ;
			sVal = ChildItemName;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _ChildItemName + ',' ;
			sVal = JobType;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _JobType + ',' ;
			oVal = JobDate;
			if ( oVal == null )
				msLastValidationError += _JobDate + ',';

			sVal = IsParentItemLandFill;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _IsParentItemLandFill + ',' ;
			sVal = IsChildItemLandFill;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _IsChildItemLandFill + ',' ;
			oVal = SupItmLFPercent;
			if ( oVal == null )
				msLastValidationError += _SupItmLFPercent + ',';

			oVal = SupItmRCPercent;
			if ( oVal == null )
				msLastValidationError += _SupItmRCPercent + ',';

			oVal = U_OrdWgt;
			if ( oVal == null )
				msLastValidationError += _OrdWgt + ',';

			oVal = BinWgt;
			if ( oVal == null )
				msLastValidationError += _BinWgt + ',';

			sVal = U_CHDICODE;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _CHDICODE + ',' ;
			oVal = U_CHDPERCENT;
			if ( oVal == null )
				msLastValidationError += _CHDPERCENT + ',';

			oVal = FinalWgt;
			if ( oVal == null )
				msLastValidationError += _FinalWgt + ',';

			sVal = U_CHDINAME;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _CHDINAME + ',' ;

			if (msLastValidationError.Length > 0) 
			    return false;
			else
			return true;
		}
#endregion

#region LinkDataToControls
		/**
		 * Link the SiteName Field to the Form Item.
		 */
		public void doLink_SiteName(string sControlName){
			moLinker.doLinkDataToControl(_SiteName, sControlName);
		}
		/**
		 * Link the MasterCode Field to the Form Item.
		 */
		public void doLink_MasterCode(string sControlName){
			moLinker.doLinkDataToControl(_MasterCode, sControlName);
		}
		/**
		 * Link the ChildCode Field to the Form Item.
		 */
		public void doLink_ChildCode(string sControlName){
			moLinker.doLinkDataToControl(_ChildCode, sControlName);
		}
		/**
		 * Link the CardCode Field to the Form Item.
		 */
		public void doLink_CardCode(string sControlName){
			moLinker.doLinkDataToControl(_CardCode, sControlName);
		}
		/**
		 * Link the CardName Field to the Form Item.
		 */
		public void doLink_CardName(string sControlName){
			moLinker.doLinkDataToControl(_CardName, sControlName);
		}
		/**
		 * Link the SupplierCode Field to the Form Item.
		 */
		public void doLink_SupplierCode(string sControlName){
			moLinker.doLinkDataToControl(_SupplierCode, sControlName);
		}
		/**
		 * Link the SupplierName Field to the Form Item.
		 */
		public void doLink_SupplierName(string sControlName){
			moLinker.doLinkDataToControl(_SupplierName, sControlName);
		}
		/**
		 * Link the SiteCode Field to the Form Item.
		 */
		public void doLink_SiteCode(string sControlName){
			moLinker.doLinkDataToControl(_SiteCode, sControlName);
		}
		/**
		 * Link the ParentItemCode Field to the Form Item.
		 */
		public void doLink_ParentItemCode(string sControlName){
			moLinker.doLinkDataToControl(_ParentItemCode, sControlName);
		}
		/**
		 * Link the ParentItemName Field to the Form Item.
		 */
		public void doLink_ParentItemName(string sControlName){
			moLinker.doLinkDataToControl(_ParentItemName, sControlName);
		}
		/**
		 * Link the ChildItemCode Field to the Form Item.
		 */
		public void doLink_ChildItemCode(string sControlName){
			moLinker.doLinkDataToControl(_ChildItemCode, sControlName);
		}
		/**
		 * Link the ChildItemName Field to the Form Item.
		 */
		public void doLink_ChildItemName(string sControlName){
			moLinker.doLinkDataToControl(_ChildItemName, sControlName);
		}
		/**
		 * Link the JobType Field to the Form Item.
		 */
		public void doLink_JobType(string sControlName){
			moLinker.doLinkDataToControl(_JobType, sControlName);
		}
		/**
		 * Link the JobDate Field to the Form Item.
		 */
		public void doLink_JobDate(string sControlName){
			moLinker.doLinkDataToControl(_JobDate, sControlName);
		}
		/**
		 * Link the IsParentItemLandFill Field to the Form Item.
		 */
		public void doLink_IsParentItemLandFill(string sControlName){
			moLinker.doLinkDataToControl(_IsParentItemLandFill, sControlName);
		}
		/**
		 * Link the IsChildItemLandFill Field to the Form Item.
		 */
		public void doLink_IsChildItemLandFill(string sControlName){
			moLinker.doLinkDataToControl(_IsChildItemLandFill, sControlName);
		}
		/**
		 * Link the SupItmLFPercent Field to the Form Item.
		 */
		public void doLink_SupItmLFPercent(string sControlName){
			moLinker.doLinkDataToControl(_SupItmLFPercent, sControlName);
		}
		/**
		 * Link the SupItmRCPercent Field to the Form Item.
		 */
		public void doLink_SupItmRCPercent(string sControlName){
			moLinker.doLinkDataToControl(_SupItmRCPercent, sControlName);
		}
		/**
		 * Link the U_OrdWgt Field to the Form Item.
		 */
		public void doLink_OrdWgt(string sControlName){
			moLinker.doLinkDataToControl(_OrdWgt, sControlName);
		}
		/**
		 * Link the BinWgt Field to the Form Item.
		 */
		public void doLink_BinWgt(string sControlName){
			moLinker.doLinkDataToControl(_BinWgt, sControlName);
		}
		/**
		 * Link the U_CHDICODE Field to the Form Item.
		 */
		public void doLink_CHDICODE(string sControlName){
			moLinker.doLinkDataToControl(_CHDICODE, sControlName);
		}
		/**
		 * Link the U_CHDPERCENT Field to the Form Item.
		 */
		public void doLink_CHDPERCENT(string sControlName){
			moLinker.doLinkDataToControl(_CHDPERCENT, sControlName);
		}
		/**
		 * Link the FinalWgt Field to the Form Item.
		 */
		public void doLink_FinalWgt(string sControlName){
			moLinker.doLinkDataToControl(_FinalWgt, sControlName);
		}
		/**
		 * Link the U_CHDINAME Field to the Form Item.
		 */
		public void doLink_CHDINAME(string sControlName){
			moLinker.doLinkDataToControl(_CHDINAME, sControlName);
		}
#endregion

	}
}
