/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 24/08/2015 15:49:46
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
	public class IDH_TFSANX: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
		protected IDHAddOns.idh.forms.Base moIDHForm;

		public IDH_TFSANX() : base("@IDH_TFSANX"){
		}

		public IDH_TFSANX( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_TFSANX"){
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_TFSANX";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Anex No
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnexNo
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnexNo = "U_AnexNo";
		public string U_AnexNo { 
			get {
 				return getValueAsString(_AnexNo); 
			}
			set { setValue(_AnexNo, value); }
		}
           public string doValidate_AnexNo() {
               return doValidate_AnexNo(U_AnexNo);
           }
           public virtual string doValidate_AnexNo(object oValue) {
               return base.doValidation(_AnexNo, oValue);
           }

		/**
		 * Decription: TFS Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TFSCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TFSCd = "U_TFSCd";
		public string U_TFSCd { 
			get {
 				return getValueAsString(_TFSCd); 
			}
			set { setValue(_TFSCd, value); }
		}
           public string doValidate_TFSCd() {
               return doValidate_TFSCd(U_TFSCd);
           }
           public virtual string doValidate_TFSCd(object oValue) {
               return base.doValidation(_TFSCd, oValue);
           }

		/**
		 * Decription: TFS No
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TFSNo
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TFSNo = "U_TFSNo";
		public string U_TFSNo { 
			get {
 				return getValueAsString(_TFSNo); 
			}
			set { setValue(_TFSNo, value); }
		}
           public string doValidate_TFSNo() {
               return doValidate_TFSNo(U_TFSNo);
           }
           public virtual string doValidate_TFSNo(object oValue) {
               return base.doValidation(_TFSNo, oValue);
           }

		/**
		 * Decription: Editable
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Edtable
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Edtable = "U_Edtable";
		public string U_Edtable { 
			get {
 				return getValueAsString(_Edtable); 
			}
			set { setValue(_Edtable, value); }
		}
           public string doValidate_Edtable() {
               return doValidate_Edtable(U_Edtable);
           }
           public virtual string doValidate_Edtable(object oValue) {
               return base.doValidation(_Edtable, oValue);
           }

		/**
		 * Decription: Detail
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Detail
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Detail = "U_Detail";
		public string U_Detail { 
			get {
 				return getValueAsString(_Detail); 
			}
			set { setValue(_Detail, value); }
		}
           public string doValidate_Detail() {
               return doValidate_Detail(U_Detail);
           }
           public virtual string doValidate_Detail(object oValue) {
               return base.doValidation(_Detail, oValue);
           }

		/**
		 * Decription: Attachment Entry
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AtcEntry
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _AtcEntry = "U_AtcEntry";
		public int U_AtcEntry { 
			get {
 				return getValueAsInt(_AtcEntry); 
			}
			set { setValue(_AtcEntry, value); }
		}
           public string doValidate_AtcEntry() {
               return doValidate_AtcEntry(U_AtcEntry);
           }
           public virtual string doValidate_AtcEntry(object oValue) {
               return base.doValidation(_AtcEntry, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(8);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_AnexNo, "Anex No", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Anex No
			moDBFields.Add(_TFSCd, "TFS Code", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //TFS Code
			moDBFields.Add(_TFSNo, "TFS No", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //TFS No
			moDBFields.Add(_Edtable, "Editable", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Editable
			moDBFields.Add(_Detail, "Detail", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Detail
			moDBFields.Add(_AtcEntry, "Attachment Entry", 7, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Attachment Entry

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_AnexNo());
            doBuildValidationString(doValidate_TFSCd());
            doBuildValidationString(doValidate_TFSNo());
            doBuildValidationString(doValidate_Edtable());
            doBuildValidationString(doValidate_Detail());
            doBuildValidationString(doValidate_AtcEntry());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_AnexNo)) return doValidate_AnexNo(oValue);
            if (sFieldName.Equals(_TFSCd)) return doValidate_TFSCd(oValue);
            if (sFieldName.Equals(_TFSNo)) return doValidate_TFSNo(oValue);
            if (sFieldName.Equals(_Edtable)) return doValidate_Edtable(oValue);
            if (sFieldName.Equals(_Detail)) return doValidate_Detail(oValue);
            if (sFieldName.Equals(_AtcEntry)) return doValidate_AtcEntry(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_AnexNo Field to the Form Item.
		 */
		public void doLink_AnexNo(string sControlName){
			moLinker.doLinkDataToControl(_AnexNo, sControlName);
		}
		/**
		 * Link the U_TFSCd Field to the Form Item.
		 */
		public void doLink_TFSCd(string sControlName){
			moLinker.doLinkDataToControl(_TFSCd, sControlName);
		}
		/**
		 * Link the U_TFSNo Field to the Form Item.
		 */
		public void doLink_TFSNo(string sControlName){
			moLinker.doLinkDataToControl(_TFSNo, sControlName);
		}
		/**
		 * Link the U_Edtable Field to the Form Item.
		 */
		public void doLink_Edtable(string sControlName){
			moLinker.doLinkDataToControl(_Edtable, sControlName);
		}
		/**
		 * Link the U_Detail Field to the Form Item.
		 */
		public void doLink_Detail(string sControlName){
			moLinker.doLinkDataToControl(_Detail, sControlName);
		}
		/**
		 * Link the U_AtcEntry Field to the Form Item.
		 */
		public void doLink_AtcEntry(string sControlName){
			moLinker.doLinkDataToControl(_AtcEntry, sControlName);
		}
#endregion

	}
}
