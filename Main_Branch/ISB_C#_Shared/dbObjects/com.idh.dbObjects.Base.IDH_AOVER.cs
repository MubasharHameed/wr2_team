/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 2014/10/09 05:25:25 PM
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
   [Serializable]
	public class IDH_AOVER: com.idh.dbObjects.DBBase { 

		protected IDHAddOns.idh.forms.Base moIDHForm;

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_AOVER() : base("@IDH_AOVER"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_AOVER( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_AOVER"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_AOVER";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: AddOn Description
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DESCR
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DESCR = "U_DESCR";
		public string U_DESCR { 
			get {
 				return getValueAsString(_DESCR); 
			}
			set { setValue(_DESCR, value); }
		}
           public string doValidate_DESCR() {
               return doValidate_DESCR(U_DESCR);
           }
           public virtual string doValidate_DESCR(object oValue) {
               return base.doValidation(_DESCR, oValue);
           }

		/**
		 * Decription: Help File
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HELP
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _HELP = "U_HELP";
		public string U_HELP { 
			get {
 				return getValueAsString(_HELP); 
			}
			set { setValue(_HELP, value); }
		}
           public string doValidate_HELP() {
               return doValidate_HELP(U_HELP);
           }
           public virtual string doValidate_HELP(object oValue) {
               return base.doValidation(_HELP, oValue);
           }

		/**
		 * Decription: AddOn Version
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_VERSION
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _VERSION = "U_VERSION";
		public string U_VERSION { 
			get {
 				return getValueAsString(_VERSION); 
			}
			set { setValue(_VERSION, value); }
		}
           public string doValidate_VERSION() {
               return doValidate_VERSION(U_VERSION);
           }
           public virtual string doValidate_VERSION(object oValue) {
               return base.doValidation(_VERSION, oValue);
           }

		/**
		 * Decription: AddOn Key
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_KEY
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _KEY = "U_KEY";
		public string U_KEY { 
			get {
 				return getValueAsString(_KEY); 
			}
			set { setValue(_KEY, value); }
		}
           public string doValidate_KEY() {
               return doValidate_KEY(U_KEY);
           }
           public virtual string doValidate_KEY(object oValue) {
               return base.doValidation(_KEY, oValue);
           }

		/**
		 * Decription: Extra Parameters
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_EXTRA
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _EXTRA = "U_EXTRA";
		public string U_EXTRA { 
			get {
 				return getValueAsString(_EXTRA); 
			}
			set { setValue(_EXTRA, value); }
		}
           public string doValidate_EXTRA() {
               return doValidate_EXTRA(U_EXTRA);
           }
           public virtual string doValidate_EXTRA(object oValue) {
               return base.doValidation(_EXTRA, oValue);
           }

		/**
		 * Decription: Release Count
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RELCNT
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _RELCNT = "U_RELCNT";
		public short U_RELCNT { 
			get {
 				return getValueAsShort(_RELCNT); 
			}
			set { setValue(_RELCNT, value); }
		}
           public string doValidate_RELCNT() {
               return doValidate_RELCNT(U_RELCNT);
           }
           public virtual string doValidate_RELCNT(object oValue) {
               return base.doValidation(_RELCNT, oValue);
           }

		/**
		 * Decription: PR Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PRC
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PRC = "U_PRC";
		public string U_PRC { 
			get {
 				return getValueAsString(_PRC); 
			}
			set { setValue(_PRC, value); }
		}
           public string doValidate_PRC() {
               return doValidate_PRC(U_PRC);
           }
           public virtual string doValidate_PRC(object oValue) {
               return base.doValidation(_PRC, oValue);
           }

		/**
		 * Decription: Framework Release Count
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FWVERS
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _FWVERS = "U_FWVERS";
		public short U_FWVERS { 
			get {
 				return getValueAsShort(_FWVERS); 
			}
			set { setValue(_FWVERS, value); }
		}
           public string doValidate_FWVERS() {
               return doValidate_FWVERS(U_FWVERS);
           }
           public virtual string doValidate_FWVERS(object oValue) {
               return base.doValidation(_FWVERS, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(10);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_DESCR, "AddOn Description", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //AddOn Description
			moDBFields.Add(_HELP, "Help File", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Help File
			moDBFields.Add(_VERSION, "AddOn Version", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //AddOn Version
			moDBFields.Add(_KEY, "AddOn Key", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //AddOn Key
			moDBFields.Add(_EXTRA, "Extra Parameters", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Extra Parameters
			moDBFields.Add(_RELCNT, "Release Count", 7, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Release Count
			moDBFields.Add(_PRC, "PR Code", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //PR Code
			moDBFields.Add(_FWVERS, "Framework Release Count", 9, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Framework Release Count

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_DESCR());
            doBuildValidationString(doValidate_HELP());
            doBuildValidationString(doValidate_VERSION());
            doBuildValidationString(doValidate_KEY());
            doBuildValidationString(doValidate_EXTRA());
            doBuildValidationString(doValidate_RELCNT());
            doBuildValidationString(doValidate_PRC());
            doBuildValidationString(doValidate_FWVERS());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_DESCR)) return doValidate_DESCR(oValue);
            if (sFieldName.Equals(_HELP)) return doValidate_HELP(oValue);
            if (sFieldName.Equals(_VERSION)) return doValidate_VERSION(oValue);
            if (sFieldName.Equals(_KEY)) return doValidate_KEY(oValue);
            if (sFieldName.Equals(_EXTRA)) return doValidate_EXTRA(oValue);
            if (sFieldName.Equals(_RELCNT)) return doValidate_RELCNT(oValue);
            if (sFieldName.Equals(_PRC)) return doValidate_PRC(oValue);
            if (sFieldName.Equals(_FWVERS)) return doValidate_FWVERS(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_DESCR Field to the Form Item.
		 */
		public void doLink_DESCR(string sControlName){
			moLinker.doLinkDataToControl(_DESCR, sControlName);
		}
		/**
		 * Link the U_HELP Field to the Form Item.
		 */
		public void doLink_HELP(string sControlName){
			moLinker.doLinkDataToControl(_HELP, sControlName);
		}
		/**
		 * Link the U_VERSION Field to the Form Item.
		 */
		public void doLink_VERSION(string sControlName){
			moLinker.doLinkDataToControl(_VERSION, sControlName);
		}
		/**
		 * Link the U_KEY Field to the Form Item.
		 */
		public void doLink_KEY(string sControlName){
			moLinker.doLinkDataToControl(_KEY, sControlName);
		}
		/**
		 * Link the U_EXTRA Field to the Form Item.
		 */
		public void doLink_EXTRA(string sControlName){
			moLinker.doLinkDataToControl(_EXTRA, sControlName);
		}
		/**
		 * Link the U_RELCNT Field to the Form Item.
		 */
		public void doLink_RELCNT(string sControlName){
			moLinker.doLinkDataToControl(_RELCNT, sControlName);
		}
		/**
		 * Link the U_PRC Field to the Form Item.
		 */
		public void doLink_PRC(string sControlName){
			moLinker.doLinkDataToControl(_PRC, sControlName);
		}
		/**
		 * Link the U_FWVERS Field to the Form Item.
		 */
		public void doLink_FWVERS(string sControlName){
			moLinker.doLinkDataToControl(_FWVERS, sControlName);
		}
#endregion

	}
}
