/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 08/02/2016 10:28:06
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
   [Serializable]
	public class IDH_ENQUS: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
  //     public Linker ControlLinker {
  //         get { return moLinker; }
  //         set { moLinker = value; }
  //     }

       private IDHAddOns.idh.forms.Base moIDHForm;
       public IDHAddOns.idh.forms.Base IDHForm {
           get { return moIDHForm; }
           set { moIDHForm = value; }
       }

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_ENQUS() : base("@IDH_ENQUS"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_ENQUS( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_ENQUS"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_ENQUS";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Answer
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Answer
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Answer = "U_Answer";
		public string U_Answer { 
			get {
 				return getValueAsString(_Answer); 
			}
			set { setValue(_Answer, value); }
		}
           public string doValidate_Answer() {
               return doValidate_Answer(U_Answer);
           }
           public virtual string doValidate_Answer(object oValue) {
               return base.doValidation(_Answer, oValue);
           }

		/**
		 * Decription: Enquiry ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_EnqID
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _EnqID = "U_EnqID";
		public int U_EnqID { 
			get {
 				return getValueAsInt(_EnqID); 
			}
			set { setValue(_EnqID, value); }
		}
           public string doValidate_EnqID() {
               return doValidate_EnqID(U_EnqID);
           }
           public virtual string doValidate_EnqID(object oValue) {
               return base.doValidation(_EnqID, oValue);
           }

		/**
		 * Decription: Question ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_QsID
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _QsID = "U_QsID";
		public string U_QsID { 
			get {
 				return getValueAsString(_QsID); 
			}
			set { setValue(_QsID, value); }
		}
           public string doValidate_QsID() {
               return doValidate_QsID(U_QsID);
           }
           public virtual string doValidate_QsID(object oValue) {
               return base.doValidation(_QsID, oValue);
           }

		/**
		 * Decription: Question
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Question
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Question = "U_Question";
		public string U_Question { 
			get {
 				return getValueAsString(_Question); 
			}
			set { setValue(_Question, value); }
		}
           public string doValidate_Question() {
               return doValidate_Question(U_Question);
           }
           public virtual string doValidate_Question(object oValue) {
               return base.doValidation(_Question, oValue);
           }

		/**
		 * Decription: WOQ ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WOQID
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _WOQID = "U_WOQID";
		public int U_WOQID { 
			get {
 				return getValueAsInt(_WOQID); 
			}
			set { setValue(_WOQID, value); }
		}
           public string doValidate_WOQID() {
               return doValidate_WOQID(U_WOQID);
           }
           public virtual string doValidate_WOQID(object oValue) {
               return base.doValidation(_WOQID, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(7);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_Answer, "Answer", 2, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Answer
			moDBFields.Add(_EnqID, "Enquiry ID", 3, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Enquiry ID
			moDBFields.Add(_QsID, "Question ID", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Question ID
			moDBFields.Add(_Question, "Question", 5, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Question
			moDBFields.Add(_WOQID, "WOQ ID", 6, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //WOQ ID

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_Answer());
            doBuildValidationString(doValidate_EnqID());
            doBuildValidationString(doValidate_QsID());
            doBuildValidationString(doValidate_Question());
            doBuildValidationString(doValidate_WOQID());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_Answer)) return doValidate_Answer(oValue);
            if (sFieldName.Equals(_EnqID)) return doValidate_EnqID(oValue);
            if (sFieldName.Equals(_QsID)) return doValidate_QsID(oValue);
            if (sFieldName.Equals(_Question)) return doValidate_Question(oValue);
            if (sFieldName.Equals(_WOQID)) return doValidate_WOQID(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_Answer Field to the Form Item.
		 */
		public void doLink_Answer(string sControlName){
			moLinker.doLinkDataToControl(_Answer, sControlName);
		}
		/**
		 * Link the U_EnqID Field to the Form Item.
		 */
		public void doLink_EnqID(string sControlName){
			moLinker.doLinkDataToControl(_EnqID, sControlName);
		}
		/**
		 * Link the U_QsID Field to the Form Item.
		 */
		public void doLink_QsID(string sControlName){
			moLinker.doLinkDataToControl(_QsID, sControlName);
		}
		/**
		 * Link the U_Question Field to the Form Item.
		 */
		public void doLink_Question(string sControlName){
			moLinker.doLinkDataToControl(_Question, sControlName);
		}
		/**
		 * Link the U_WOQID Field to the Form Item.
		 */
		public void doLink_WOQID(string sControlName){
			moLinker.doLinkDataToControl(_WOQID, sControlName);
		}
#endregion

	}
}
