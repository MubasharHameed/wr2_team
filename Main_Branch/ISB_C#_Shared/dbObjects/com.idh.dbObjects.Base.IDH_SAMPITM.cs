/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 04/02/2016 17:41:27
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
   [Serializable]
	public class IDH_SAMPITM: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
       //public Linker ControlLinker {
       //    get { return moLinker; }
       //    set { moLinker = value; }
       //}

       private IDHAddOns.idh.forms.Base moIDHForm;
       public IDHAddOns.idh.forms.Base IDHForm {
           get { return moIDHForm; }
           set { moIDHForm = value; }
       }

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_SAMPITM() : base("@IDH_SAMPITM"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_SAMPITM( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_SAMPITM"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_SAMPITM";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: BinLocation
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BinLoc
		 * Size: 228
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BinLoc = "U_BinLoc";
		public string U_BinLoc { 
			get {
 				return getValueAsString(_BinLoc); 
			}
			set { setValue(_BinLoc, value); }
		}
           public string doValidate_BinLoc() {
               return doValidate_BinLoc(U_BinLoc);
           }
           public virtual string doValidate_BinLoc(object oValue) {
               return base.doValidation(_BinLoc, oValue);
           }

		/**
		 * Decription: Comments
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Comments
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Comments = "U_Comments";
		public string U_Comments { 
			get {
 				return getValueAsString(_Comments); 
			}
			set { setValue(_Comments, value); }
		}
           public string doValidate_Comments() {
               return doValidate_Comments(U_Comments);
           }
           public virtual string doValidate_Comments(object oValue) {
               return base.doValidation(_Comments, oValue);
           }

		/**
		 * Decription: Created By
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CreatedBy
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CreatedBy = "U_CreatedBy";
		public string U_CreatedBy { 
			get {
 				return getValueAsString(_CreatedBy); 
			}
			set { setValue(_CreatedBy, value); }
		}
           public string doValidate_CreatedBy() {
               return doValidate_CreatedBy(U_CreatedBy);
           }
           public virtual string doValidate_CreatedBy(object oValue) {
               return base.doValidation(_CreatedBy, oValue);
           }

		/**
		 * Decription: Created On
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CreatedOn
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _CreatedOn = "U_CreatedOn";
		public DateTime U_CreatedOn { 
			get {
 				return getValueAsDateTime(_CreatedOn); 
			}
			set { setValue(_CreatedOn, value); }
		}
		public void U_CreatedOn_AsString(string value){
			setValue("U_CreatedOn", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_CreatedOn_AsString(){
			DateTime dVal = getValueAsDateTime(_CreatedOn);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_CreatedOn() {
               return doValidate_CreatedOn(U_CreatedOn);
           }
           public virtual string doValidate_CreatedOn(object oValue) {
               return base.doValidation(_CreatedOn, oValue);
           }

		/**
		 * Decription: Last Update By
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_LastUpdateBy
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _LastUpdateBy = "U_LastUpdateBy";
		public string U_LastUpdateBy { 
			get {
 				return getValueAsString(_LastUpdateBy); 
			}
			set { setValue(_LastUpdateBy, value); }
		}
           public string doValidate_LastUpdateBy() {
               return doValidate_LastUpdateBy(U_LastUpdateBy);
           }
           public virtual string doValidate_LastUpdateBy(object oValue) {
               return base.doValidation(_LastUpdateBy, oValue);
           }

		/**
		 * Decription: Last Update On
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_LastUpdateOn
		 * Size: 11
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _LastUpdateOn = "U_LastUpdateOn";
		public string U_LastUpdateOn { 
			get {
 				return getValueAsString(_LastUpdateOn); 
			}
			set { setValue(_LastUpdateOn, value); }
		}
           public string doValidate_LastUpdateOn() {
               return doValidate_LastUpdateOn(U_LastUpdateOn);
           }
           public virtual string doValidate_LastUpdateOn(object oValue) {
               return base.doValidation(_LastUpdateOn, oValue);
           }

		/**
		 * Decription: Quantity
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Quantity
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Quantity = "U_Quantity";
		public double U_Quantity { 
			get {
 				return getValueAsDouble(_Quantity); 
			}
			set { setValue(_Quantity, value); }
		}
           public string doValidate_Quantity() {
               return doValidate_Quantity(U_Quantity);
           }
           public virtual string doValidate_Quantity(object oValue) {
               return base.doValidation(_Quantity, oValue);
           }

		/**
		 * Decription: Sample Item Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SICode
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SICode = "U_SICode";
		public string U_SICode { 
			get {
 				return getValueAsString(_SICode); 
			}
			set { setValue(_SICode, value); }
		}
           public string doValidate_SICode() {
               return doValidate_SICode(U_SICode);
           }
           public virtual string doValidate_SICode(object oValue) {
               return base.doValidation(_SICode, oValue);
           }

		/**
		 * Decription: Sample Item Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SIName
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SIName = "U_SIName";
		public string U_SIName { 
			get {
 				return getValueAsString(_SIName); 
			}
			set { setValue(_SIName, value); }
		}
           public string doValidate_SIName() {
               return doValidate_SIName(U_SIName);
           }
           public virtual string doValidate_SIName(object oValue) {
               return base.doValidation(_SIName, oValue);
           }

		/**
		 * Decription: UOM
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UOM
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _UOM = "U_UOM";
		public string U_UOM { 
			get {
 				return getValueAsString(_UOM); 
			}
			set { setValue(_UOM, value); }
		}
           public string doValidate_UOM() {
               return doValidate_UOM(U_UOM);
           }
           public virtual string doValidate_UOM(object oValue) {
               return base.doValidation(_UOM, oValue);
           }

		/**
		 * Decription: Warehouse
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Whse
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Whse = "U_Whse";
		public string U_Whse { 
			get {
 				return getValueAsString(_Whse); 
			}
			set { setValue(_Whse, value); }
		}
           public string doValidate_Whse() {
               return doValidate_Whse(U_Whse);
           }
           public virtual string doValidate_Whse(object oValue) {
               return base.doValidation(_Whse, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(13);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_BinLoc, "BinLocation", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 228, EMPTYSTR, false, false); //BinLocation
			moDBFields.Add(_Comments, "Comments", 3, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Comments
			moDBFields.Add(_CreatedBy, "Created By", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, false); //Created By
			moDBFields.Add(_CreatedOn, "Created On", 5, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Created On
			moDBFields.Add(_LastUpdateBy, "Last Update By", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, false); //Last Update By
			moDBFields.Add(_LastUpdateOn, "Last Update On", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 11, EMPTYSTR, false, false); //Last Update On
			moDBFields.Add(_Quantity, "Quantity", 8, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Quantity
			moDBFields.Add(_SICode, "Sample Item Code", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Sample Item Code
			moDBFields.Add(_SIName, "Sample Item Name", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Sample Item Name
			moDBFields.Add(_UOM, "UOM", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //UOM
			moDBFields.Add(_Whse, "Warehouse", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Warehouse

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_BinLoc());
            doBuildValidationString(doValidate_Comments());
            doBuildValidationString(doValidate_CreatedBy());
            doBuildValidationString(doValidate_CreatedOn());
            doBuildValidationString(doValidate_LastUpdateBy());
            doBuildValidationString(doValidate_LastUpdateOn());
            doBuildValidationString(doValidate_Quantity());
            doBuildValidationString(doValidate_SICode());
            doBuildValidationString(doValidate_SIName());
            doBuildValidationString(doValidate_UOM());
            doBuildValidationString(doValidate_Whse());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_BinLoc)) return doValidate_BinLoc(oValue);
            if (sFieldName.Equals(_Comments)) return doValidate_Comments(oValue);
            if (sFieldName.Equals(_CreatedBy)) return doValidate_CreatedBy(oValue);
            if (sFieldName.Equals(_CreatedOn)) return doValidate_CreatedOn(oValue);
            if (sFieldName.Equals(_LastUpdateBy)) return doValidate_LastUpdateBy(oValue);
            if (sFieldName.Equals(_LastUpdateOn)) return doValidate_LastUpdateOn(oValue);
            if (sFieldName.Equals(_Quantity)) return doValidate_Quantity(oValue);
            if (sFieldName.Equals(_SICode)) return doValidate_SICode(oValue);
            if (sFieldName.Equals(_SIName)) return doValidate_SIName(oValue);
            if (sFieldName.Equals(_UOM)) return doValidate_UOM(oValue);
            if (sFieldName.Equals(_Whse)) return doValidate_Whse(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_BinLoc Field to the Form Item.
		 */
		public void doLink_BinLoc(string sControlName){
			moLinker.doLinkDataToControl(_BinLoc, sControlName);
		}
		/**
		 * Link the U_Comments Field to the Form Item.
		 */
		public void doLink_Comments(string sControlName){
			moLinker.doLinkDataToControl(_Comments, sControlName);
		}
		/**
		 * Link the U_CreatedBy Field to the Form Item.
		 */
		public void doLink_CreatedBy(string sControlName){
			moLinker.doLinkDataToControl(_CreatedBy, sControlName);
		}
		/**
		 * Link the U_CreatedOn Field to the Form Item.
		 */
		public void doLink_CreatedOn(string sControlName){
			moLinker.doLinkDataToControl(_CreatedOn, sControlName);
		}
		/**
		 * Link the U_LastUpdateBy Field to the Form Item.
		 */
		public void doLink_LastUpdateBy(string sControlName){
			moLinker.doLinkDataToControl(_LastUpdateBy, sControlName);
		}
		/**
		 * Link the U_LastUpdateOn Field to the Form Item.
		 */
		public void doLink_LastUpdateOn(string sControlName){
			moLinker.doLinkDataToControl(_LastUpdateOn, sControlName);
		}
		/**
		 * Link the U_Quantity Field to the Form Item.
		 */
		public void doLink_Quantity(string sControlName){
			moLinker.doLinkDataToControl(_Quantity, sControlName);
		}
		/**
		 * Link the U_SICode Field to the Form Item.
		 */
		public void doLink_SICode(string sControlName){
			moLinker.doLinkDataToControl(_SICode, sControlName);
		}
		/**
		 * Link the U_SIName Field to the Form Item.
		 */
		public void doLink_SIName(string sControlName){
			moLinker.doLinkDataToControl(_SIName, sControlName);
		}
		/**
		 * Link the U_UOM Field to the Form Item.
		 */
		public void doLink_UOM(string sControlName){
			moLinker.doLinkDataToControl(_UOM, sControlName);
		}
		/**
		 * Link the U_Whse Field to the Form Item.
		 */
		public void doLink_Whse(string sControlName){
			moLinker.doLinkDataToControl(_Whse, sControlName);
		}
#endregion

	}
}
