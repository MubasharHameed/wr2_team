/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 14/03/2016 12:48:49
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
   [Serializable]
	public class IDH_LTIDETAIL: com.idh.dbObjects.DBBase { 

		private Linker moLinker = null;
       public Linker ControlLinker {
           get { return moLinker; }
           set { moLinker = value; }
       }

       private IDHAddOns.idh.forms.Base moIDHForm;
       public IDHAddOns.idh.forms.Base IDHForm {
           get { return moIDHForm; }
           set { moIDHForm = value; }
       }

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_LTIDETAIL() : base("@IDH_LTIDETAIL"){
			msAutoNumPrefix = msAUTONUMPREFIX;
            
		}

		public IDH_LTIDETAIL( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_LTIDETAIL"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_LTIDETAIL";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: LabTaskImport Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_LTICd
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _LTICd = "U_LTICd";
		public string U_LTICd { 
			get {
 				return getValueAsString(_LTICd); 
			}
			set { setValue(_LTICd, value); }
		}
           public string doValidate_LTICd() {
               return doValidate_LTICd(U_LTICd);
           }
           public virtual string doValidate_LTICd(object oValue) {
               return base.doValidation(_LTICd, oValue);
           }

		/**
		 * Decription: Rep Col 1
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol1
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol1 = "U_RepCol1";
		public string U_RepCol1 { 
			get {
 				return getValueAsString(_RepCol1); 
			}
			set { setValue(_RepCol1, value); }
		}
           public string doValidate_RepCol1() {
               return doValidate_RepCol1(U_RepCol1);
           }
           public virtual string doValidate_RepCol1(object oValue) {
               return base.doValidation(_RepCol1, oValue);
           }

		/**
		 * Decription: Rep Col 10
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol10
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol10 = "U_RepCol10";
		public string U_RepCol10 { 
			get {
 				return getValueAsString(_RepCol10); 
			}
			set { setValue(_RepCol10, value); }
		}
           public string doValidate_RepCol10() {
               return doValidate_RepCol10(U_RepCol10);
           }
           public virtual string doValidate_RepCol10(object oValue) {
               return base.doValidation(_RepCol10, oValue);
           }

		/**
		 * Decription: Rep Col 11
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol11
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol11 = "U_RepCol11";
		public string U_RepCol11 { 
			get {
 				return getValueAsString(_RepCol11); 
			}
			set { setValue(_RepCol11, value); }
		}
           public string doValidate_RepCol11() {
               return doValidate_RepCol11(U_RepCol11);
           }
           public virtual string doValidate_RepCol11(object oValue) {
               return base.doValidation(_RepCol11, oValue);
           }

		/**
		 * Decription: Rep Col 12
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol12
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol12 = "U_RepCol12";
		public string U_RepCol12 { 
			get {
 				return getValueAsString(_RepCol12); 
			}
			set { setValue(_RepCol12, value); }
		}
           public string doValidate_RepCol12() {
               return doValidate_RepCol12(U_RepCol12);
           }
           public virtual string doValidate_RepCol12(object oValue) {
               return base.doValidation(_RepCol12, oValue);
           }

		/**
		 * Decription: Rep Col 13
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol13
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol13 = "U_RepCol13";
		public string U_RepCol13 { 
			get {
 				return getValueAsString(_RepCol13); 
			}
			set { setValue(_RepCol13, value); }
		}
           public string doValidate_RepCol13() {
               return doValidate_RepCol13(U_RepCol13);
           }
           public virtual string doValidate_RepCol13(object oValue) {
               return base.doValidation(_RepCol13, oValue);
           }

		/**
		 * Decription: Rep Col 14
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol14
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol14 = "U_RepCol14";
		public string U_RepCol14 { 
			get {
 				return getValueAsString(_RepCol14); 
			}
			set { setValue(_RepCol14, value); }
		}
           public string doValidate_RepCol14() {
               return doValidate_RepCol14(U_RepCol14);
           }
           public virtual string doValidate_RepCol14(object oValue) {
               return base.doValidation(_RepCol14, oValue);
           }

		/**
		 * Decription: Rep Col 15
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol15
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol15 = "U_RepCol15";
		public string U_RepCol15 { 
			get {
 				return getValueAsString(_RepCol15); 
			}
			set { setValue(_RepCol15, value); }
		}
           public string doValidate_RepCol15() {
               return doValidate_RepCol15(U_RepCol15);
           }
           public virtual string doValidate_RepCol15(object oValue) {
               return base.doValidation(_RepCol15, oValue);
           }

		/**
		 * Decription: Rep Col 16
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol16
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol16 = "U_RepCol16";
		public string U_RepCol16 { 
			get {
 				return getValueAsString(_RepCol16); 
			}
			set { setValue(_RepCol16, value); }
		}
           public string doValidate_RepCol16() {
               return doValidate_RepCol16(U_RepCol16);
           }
           public virtual string doValidate_RepCol16(object oValue) {
               return base.doValidation(_RepCol16, oValue);
           }

		/**
		 * Decription: Rep Col 17
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol17
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol17 = "U_RepCol17";
		public string U_RepCol17 { 
			get {
 				return getValueAsString(_RepCol17); 
			}
			set { setValue(_RepCol17, value); }
		}
           public string doValidate_RepCol17() {
               return doValidate_RepCol17(U_RepCol17);
           }
           public virtual string doValidate_RepCol17(object oValue) {
               return base.doValidation(_RepCol17, oValue);
           }

		/**
		 * Decription: Rep Col 18
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol18
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol18 = "U_RepCol18";
		public string U_RepCol18 { 
			get {
 				return getValueAsString(_RepCol18); 
			}
			set { setValue(_RepCol18, value); }
		}
           public string doValidate_RepCol18() {
               return doValidate_RepCol18(U_RepCol18);
           }
           public virtual string doValidate_RepCol18(object oValue) {
               return base.doValidation(_RepCol18, oValue);
           }

		/**
		 * Decription: Rep Col 19
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol19
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol19 = "U_RepCol19";
		public string U_RepCol19 { 
			get {
 				return getValueAsString(_RepCol19); 
			}
			set { setValue(_RepCol19, value); }
		}
           public string doValidate_RepCol19() {
               return doValidate_RepCol19(U_RepCol19);
           }
           public virtual string doValidate_RepCol19(object oValue) {
               return base.doValidation(_RepCol19, oValue);
           }

		/**
		 * Decription: Rep Col 2
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol2
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol2 = "U_RepCol2";
		public string U_RepCol2 { 
			get {
 				return getValueAsString(_RepCol2); 
			}
			set { setValue(_RepCol2, value); }
		}
           public string doValidate_RepCol2() {
               return doValidate_RepCol2(U_RepCol2);
           }
           public virtual string doValidate_RepCol2(object oValue) {
               return base.doValidation(_RepCol2, oValue);
           }

		/**
		 * Decription: Rep Col 20
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol20
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol20 = "U_RepCol20";
		public string U_RepCol20 { 
			get {
 				return getValueAsString(_RepCol20); 
			}
			set { setValue(_RepCol20, value); }
		}
           public string doValidate_RepCol20() {
               return doValidate_RepCol20(U_RepCol20);
           }
           public virtual string doValidate_RepCol20(object oValue) {
               return base.doValidation(_RepCol20, oValue);
           }

		/**
		 * Decription: Rep Col 3
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol3
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol3 = "U_RepCol3";
		public string U_RepCol3 { 
			get {
 				return getValueAsString(_RepCol3); 
			}
			set { setValue(_RepCol3, value); }
		}
           public string doValidate_RepCol3() {
               return doValidate_RepCol3(U_RepCol3);
           }
           public virtual string doValidate_RepCol3(object oValue) {
               return base.doValidation(_RepCol3, oValue);
           }

		/**
		 * Decription: Rep Col 4
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol4
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol4 = "U_RepCol4";
		public string U_RepCol4 { 
			get {
 				return getValueAsString(_RepCol4); 
			}
			set { setValue(_RepCol4, value); }
		}
           public string doValidate_RepCol4() {
               return doValidate_RepCol4(U_RepCol4);
           }
           public virtual string doValidate_RepCol4(object oValue) {
               return base.doValidation(_RepCol4, oValue);
           }

		/**
		 * Decription: Rep Col 5
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol5
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol5 = "U_RepCol5";
		public string U_RepCol5 { 
			get {
 				return getValueAsString(_RepCol5); 
			}
			set { setValue(_RepCol5, value); }
		}
           public string doValidate_RepCol5() {
               return doValidate_RepCol5(U_RepCol5);
           }
           public virtual string doValidate_RepCol5(object oValue) {
               return base.doValidation(_RepCol5, oValue);
           }

		/**
		 * Decription: Rep Col 6
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol6
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol6 = "U_RepCol6";
		public string U_RepCol6 { 
			get {
 				return getValueAsString(_RepCol6); 
			}
			set { setValue(_RepCol6, value); }
		}
           public string doValidate_RepCol6() {
               return doValidate_RepCol6(U_RepCol6);
           }
           public virtual string doValidate_RepCol6(object oValue) {
               return base.doValidation(_RepCol6, oValue);
           }

		/**
		 * Decription: Rep Col 7
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol7
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol7 = "U_RepCol7";
		public string U_RepCol7 { 
			get {
 				return getValueAsString(_RepCol7); 
			}
			set { setValue(_RepCol7, value); }
		}
           public string doValidate_RepCol7() {
               return doValidate_RepCol7(U_RepCol7);
           }
           public virtual string doValidate_RepCol7(object oValue) {
               return base.doValidation(_RepCol7, oValue);
           }

		/**
		 * Decription: Rep Col 8
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol8
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol8 = "U_RepCol8";
		public string U_RepCol8 { 
			get {
 				return getValueAsString(_RepCol8); 
			}
			set { setValue(_RepCol8, value); }
		}
           public string doValidate_RepCol8() {
               return doValidate_RepCol8(U_RepCol8);
           }
           public virtual string doValidate_RepCol8(object oValue) {
               return base.doValidation(_RepCol8, oValue);
           }

		/**
		 * Decription: Rep Col 9
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RepCol9
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RepCol9 = "U_RepCol9";
		public string U_RepCol9 { 
			get {
 				return getValueAsString(_RepCol9); 
			}
			set { setValue(_RepCol9, value); }
		}
           public string doValidate_RepCol9() {
               return doValidate_RepCol9(U_RepCol9);
           }
           public virtual string doValidate_RepCol9(object oValue) {
               return base.doValidation(_RepCol9, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(23);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_LTICd, "LabTaskImport Code", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //LabTaskImport Code
			moDBFields.Add(_RepCol1, "Rep Col 1", 3, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 1
			moDBFields.Add(_RepCol10, "Rep Col 10", 4, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 10
			moDBFields.Add(_RepCol11, "Rep Col 11", 5, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 11
			moDBFields.Add(_RepCol12, "Rep Col 12", 6, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 12
			moDBFields.Add(_RepCol13, "Rep Col 13", 7, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 13
			moDBFields.Add(_RepCol14, "Rep Col 14", 8, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 14
			moDBFields.Add(_RepCol15, "Rep Col 15", 9, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 15
			moDBFields.Add(_RepCol16, "Rep Col 16", 10, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 16
			moDBFields.Add(_RepCol17, "Rep Col 17", 11, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 17
			moDBFields.Add(_RepCol18, "Rep Col 18", 12, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 18
			moDBFields.Add(_RepCol19, "Rep Col 19", 13, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 19
			moDBFields.Add(_RepCol2, "Rep Col 2", 14, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 2
			moDBFields.Add(_RepCol20, "Rep Col 20", 15, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 20
			moDBFields.Add(_RepCol3, "Rep Col 3", 16, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 3
			moDBFields.Add(_RepCol4, "Rep Col 4", 17, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 4
			moDBFields.Add(_RepCol5, "Rep Col 5", 18, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 5
			moDBFields.Add(_RepCol6, "Rep Col 6", 19, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 6
			moDBFields.Add(_RepCol7, "Rep Col 7", 20, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 7
			moDBFields.Add(_RepCol8, "Rep Col 8", 21, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 8
			moDBFields.Add(_RepCol9, "Rep Col 9", 22, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Rep Col 9

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_LTICd());
            doBuildValidationString(doValidate_RepCol1());
            doBuildValidationString(doValidate_RepCol10());
            doBuildValidationString(doValidate_RepCol11());
            doBuildValidationString(doValidate_RepCol12());
            doBuildValidationString(doValidate_RepCol13());
            doBuildValidationString(doValidate_RepCol14());
            doBuildValidationString(doValidate_RepCol15());
            doBuildValidationString(doValidate_RepCol16());
            doBuildValidationString(doValidate_RepCol17());
            doBuildValidationString(doValidate_RepCol18());
            doBuildValidationString(doValidate_RepCol19());
            doBuildValidationString(doValidate_RepCol2());
            doBuildValidationString(doValidate_RepCol20());
            doBuildValidationString(doValidate_RepCol3());
            doBuildValidationString(doValidate_RepCol4());
            doBuildValidationString(doValidate_RepCol5());
            doBuildValidationString(doValidate_RepCol6());
            doBuildValidationString(doValidate_RepCol7());
            doBuildValidationString(doValidate_RepCol8());
            doBuildValidationString(doValidate_RepCol9());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_LTICd)) return doValidate_LTICd(oValue);
            if (sFieldName.Equals(_RepCol1)) return doValidate_RepCol1(oValue);
            if (sFieldName.Equals(_RepCol10)) return doValidate_RepCol10(oValue);
            if (sFieldName.Equals(_RepCol11)) return doValidate_RepCol11(oValue);
            if (sFieldName.Equals(_RepCol12)) return doValidate_RepCol12(oValue);
            if (sFieldName.Equals(_RepCol13)) return doValidate_RepCol13(oValue);
            if (sFieldName.Equals(_RepCol14)) return doValidate_RepCol14(oValue);
            if (sFieldName.Equals(_RepCol15)) return doValidate_RepCol15(oValue);
            if (sFieldName.Equals(_RepCol16)) return doValidate_RepCol16(oValue);
            if (sFieldName.Equals(_RepCol17)) return doValidate_RepCol17(oValue);
            if (sFieldName.Equals(_RepCol18)) return doValidate_RepCol18(oValue);
            if (sFieldName.Equals(_RepCol19)) return doValidate_RepCol19(oValue);
            if (sFieldName.Equals(_RepCol2)) return doValidate_RepCol2(oValue);
            if (sFieldName.Equals(_RepCol20)) return doValidate_RepCol20(oValue);
            if (sFieldName.Equals(_RepCol3)) return doValidate_RepCol3(oValue);
            if (sFieldName.Equals(_RepCol4)) return doValidate_RepCol4(oValue);
            if (sFieldName.Equals(_RepCol5)) return doValidate_RepCol5(oValue);
            if (sFieldName.Equals(_RepCol6)) return doValidate_RepCol6(oValue);
            if (sFieldName.Equals(_RepCol7)) return doValidate_RepCol7(oValue);
            if (sFieldName.Equals(_RepCol8)) return doValidate_RepCol8(oValue);
            if (sFieldName.Equals(_RepCol9)) return doValidate_RepCol9(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_LTICd Field to the Form Item.
		 */
		public void doLink_LTICd(string sControlName){
			moLinker.doLinkDataToControl(_LTICd, sControlName);
		}
		/**
		 * Link the U_RepCol1 Field to the Form Item.
		 */
		public void doLink_RepCol1(string sControlName){
			moLinker.doLinkDataToControl(_RepCol1, sControlName);
		}
		/**
		 * Link the U_RepCol10 Field to the Form Item.
		 */
		public void doLink_RepCol10(string sControlName){
			moLinker.doLinkDataToControl(_RepCol10, sControlName);
		}
		/**
		 * Link the U_RepCol11 Field to the Form Item.
		 */
		public void doLink_RepCol11(string sControlName){
			moLinker.doLinkDataToControl(_RepCol11, sControlName);
		}
		/**
		 * Link the U_RepCol12 Field to the Form Item.
		 */
		public void doLink_RepCol12(string sControlName){
			moLinker.doLinkDataToControl(_RepCol12, sControlName);
		}
		/**
		 * Link the U_RepCol13 Field to the Form Item.
		 */
		public void doLink_RepCol13(string sControlName){
			moLinker.doLinkDataToControl(_RepCol13, sControlName);
		}
		/**
		 * Link the U_RepCol14 Field to the Form Item.
		 */
		public void doLink_RepCol14(string sControlName){
			moLinker.doLinkDataToControl(_RepCol14, sControlName);
		}
		/**
		 * Link the U_RepCol15 Field to the Form Item.
		 */
		public void doLink_RepCol15(string sControlName){
			moLinker.doLinkDataToControl(_RepCol15, sControlName);
		}
		/**
		 * Link the U_RepCol16 Field to the Form Item.
		 */
		public void doLink_RepCol16(string sControlName){
			moLinker.doLinkDataToControl(_RepCol16, sControlName);
		}
		/**
		 * Link the U_RepCol17 Field to the Form Item.
		 */
		public void doLink_RepCol17(string sControlName){
			moLinker.doLinkDataToControl(_RepCol17, sControlName);
		}
		/**
		 * Link the U_RepCol18 Field to the Form Item.
		 */
		public void doLink_RepCol18(string sControlName){
			moLinker.doLinkDataToControl(_RepCol18, sControlName);
		}
		/**
		 * Link the U_RepCol19 Field to the Form Item.
		 */
		public void doLink_RepCol19(string sControlName){
			moLinker.doLinkDataToControl(_RepCol19, sControlName);
		}
		/**
		 * Link the U_RepCol2 Field to the Form Item.
		 */
		public void doLink_RepCol2(string sControlName){
			moLinker.doLinkDataToControl(_RepCol2, sControlName);
		}
		/**
		 * Link the U_RepCol20 Field to the Form Item.
		 */
		public void doLink_RepCol20(string sControlName){
			moLinker.doLinkDataToControl(_RepCol20, sControlName);
		}
		/**
		 * Link the U_RepCol3 Field to the Form Item.
		 */
		public void doLink_RepCol3(string sControlName){
			moLinker.doLinkDataToControl(_RepCol3, sControlName);
		}
		/**
		 * Link the U_RepCol4 Field to the Form Item.
		 */
		public void doLink_RepCol4(string sControlName){
			moLinker.doLinkDataToControl(_RepCol4, sControlName);
		}
		/**
		 * Link the U_RepCol5 Field to the Form Item.
		 */
		public void doLink_RepCol5(string sControlName){
			moLinker.doLinkDataToControl(_RepCol5, sControlName);
		}
		/**
		 * Link the U_RepCol6 Field to the Form Item.
		 */
		public void doLink_RepCol6(string sControlName){
			moLinker.doLinkDataToControl(_RepCol6, sControlName);
		}
		/**
		 * Link the U_RepCol7 Field to the Form Item.
		 */
		public void doLink_RepCol7(string sControlName){
			moLinker.doLinkDataToControl(_RepCol7, sControlName);
		}
		/**
		 * Link the U_RepCol8 Field to the Form Item.
		 */
		public void doLink_RepCol8(string sControlName){
			moLinker.doLinkDataToControl(_RepCol8, sControlName);
		}
		/**
		 * Link the U_RepCol9 Field to the Form Item.
		 */
		public void doLink_RepCol9(string sControlName){
			moLinker.doLinkDataToControl(_RepCol9, sControlName);
		}
#endregion

	}
}
