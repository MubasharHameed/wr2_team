/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 24/08/2015 14:01:16
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User {
    [Serializable]
    public class IDH_WITMVLD : com.idh.dbObjects.Base.IDH_WITMVLD {

        public IDH_WITMVLD()
            : base() {
            msAutoNumKey = "WITMVLD";
            this.mbDoAuditTrail = true;
        }

        public IDH_WITMVLD(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oIDHForm, oForm) {
        }
    }
}
