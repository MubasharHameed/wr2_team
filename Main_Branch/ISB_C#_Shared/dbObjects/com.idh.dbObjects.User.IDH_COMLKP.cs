/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2014/11/06 11:25:03 AM
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
   [Serializable] 
	public class IDH_COMLKP: com.idh.dbObjects.Base.IDH_COMLKP{ 

		public IDH_COMLKP() : base() {
			
		}

   	public IDH_COMLKP( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	}
	}
}
