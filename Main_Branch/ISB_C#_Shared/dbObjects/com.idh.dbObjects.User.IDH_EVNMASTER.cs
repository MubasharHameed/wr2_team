/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 28/04/2016 12:56:09
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
   [Serializable] 
	public class IDH_EVNMASTER: com.idh.dbObjects.Base.IDH_EVNMASTER{ 
        private IDH_EVNDETAIL moLines;
        public IDH_EVNDETAIL Lines {
            get { return moLines; }
        }
		public IDH_EVNMASTER() : base() {
            msAutoNumKey = "EVNMAS";
            moLines = new IDH_EVNDETAIL(this);
			
		}

   	public IDH_EVNMASTER( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
            msAutoNumKey = "EVNMAS";
            moLines = new IDH_EVNDETAIL(this);
   	}
	}
}
