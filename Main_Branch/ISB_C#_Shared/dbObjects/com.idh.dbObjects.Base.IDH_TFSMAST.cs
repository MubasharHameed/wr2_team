/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 2014/10/09 05:20:37 AM
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
   [Serializable]
	public class IDH_TFSMAST: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
		protected IDHAddOns.idh.forms.Base moIDHForm;

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_TFSMAST() : base("@IDH_TFSMAST"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_TFSMAST( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_TFSMAST"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_TFSMAST";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: TFS Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TFSNum
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TFSNum = "U_TFSNum";
		public string U_TFSNum { 
			get {
 				return getValueAsString(_TFSNum); 
			}
			set { setValue(_TFSNum, value); }
		}
           public string doValidate_TFSNum() {
               return doValidate_TFSNum(U_TFSNum);
           }
           public virtual string doValidate_TFSNum(object oValue) {
               return base.doValidation(_TFSNum, oValue);
           }

		/**
		 * Decription: Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Type
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Type = "U_Type";
		public string U_Type { 
			get {
 				return getValueAsString(_Type); 
			}
			set { setValue(_Type, value); }
		}
           public string doValidate_Type() {
               return doValidate_Type(U_Type);
           }
           public virtual string doValidate_Type(object oValue) {
               return base.doValidation(_Type, oValue);
           }

		/**
		 * Decription: Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Status
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Status = "U_Status";
		public string U_Status { 
			get {
 				return getValueAsString(_Status); 
			}
			set { setValue(_Status, value); }
		}
           public string doValidate_Status() {
               return doValidate_Status(U_Status);
           }
           public virtual string doValidate_Status(object oValue) {
               return base.doValidation(_Status, oValue);
           }

		/**
		 * Decription: Shipment Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ShpType
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ShpType = "U_ShpType";
		public string U_ShpType { 
			get {
 				return getValueAsString(_ShpType); 
			}
			set { setValue(_ShpType, value); }
		}
           public string doValidate_ShpType() {
               return doValidate_ShpType(U_ShpType);
           }
           public virtual string doValidate_ShpType(object oValue) {
               return base.doValidation(_ShpType, oValue);
           }

		/**
		 * Decription: Operation Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_OpType
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _OpType = "U_OpType";
		public string U_OpType { 
			get {
 				return getValueAsString(_OpType); 
			}
			set { setValue(_OpType, value); }
		}
           public string doValidate_OpType() {
               return doValidate_OpType(U_OpType);
           }
           public virtual string doValidate_OpType(object oValue) {
               return base.doValidation(_OpType, oValue);
           }

		/**
		 * Decription: Site Pre-approved
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SPreAp
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SPreAp = "U_SPreAp";
		public string U_SPreAp { 
			get {
 				return getValueAsString(_SPreAp); 
			}
			set { setValue(_SPreAp, value); }
		}
           public string doValidate_SPreAp() {
               return doValidate_SPreAp(U_SPreAp);
           }
           public virtual string doValidate_SPreAp(object oValue) {
               return base.doValidation(_SPreAp, oValue);
           }

		/**
		 * Decription: Intended Shipments
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IntShip
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _IntShip = "U_IntShip";
		public short U_IntShip { 
			get {
 				return getValueAsShort(_IntShip); 
			}
			set { setValue(_IntShip, value); }
		}
           public string doValidate_IntShip() {
               return doValidate_IntShip(U_IntShip);
           }
           public virtual string doValidate_IntShip(object oValue) {
               return base.doValidation(_IntShip, oValue);
           }

		/**
		 * Decription: Actual Shipments
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ActShip
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _ActShip = "U_ActShip";
		public short U_ActShip { 
			get {
 				return getValueAsShort(_ActShip); 
			}
			set { setValue(_ActShip, value); }
		}
           public string doValidate_ActShip() {
               return doValidate_ActShip(U_ActShip);
           }
           public virtual string doValidate_ActShip(object oValue) {
               return base.doValidation(_ActShip, oValue);
           }

		/**
		 * Decription: Intended Shipments
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IntQty
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _IntQty = "U_IntQty";
        public int U_IntQty {
			get {
                return getValueAsInt(_IntQty);
			}
			set { setValue(_IntQty, value); }
		}
           public string doValidate_IntQty() {
               return doValidate_IntQty(U_IntQty);
           }
           public virtual string doValidate_IntQty(object oValue) {
               return base.doValidation(_IntQty, oValue);
           }

		/**
		 * Decription: Intended Qty Tones
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_QtyT
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _QtyT = "U_QtyT";
		public double U_QtyT { 
			get {
 				return getValueAsDouble(_QtyT); 
			}
			set { setValue(_QtyT, value); }
		}
           public string doValidate_QtyT() {
               return doValidate_QtyT(U_QtyT);
           }
           public virtual string doValidate_QtyT(object oValue) {
               return base.doValidation(_QtyT, oValue);
           }

		/**
		 * Decription: Intended Qty Cubic Meter
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_QtyCMtr
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _QtyCMtr = "U_QtyCMtr";
		public double U_QtyCMtr { 
			get {
 				return getValueAsDouble(_QtyCMtr); 
			}
			set { setValue(_QtyCMtr, value); }
		}
           public string doValidate_QtyCMtr() {
               return doValidate_QtyCMtr(U_QtyCMtr);
           }
           public virtual string doValidate_QtyCMtr(object oValue) {
               return base.doValidation(_QtyCMtr, oValue);
           }

		/**
		 * Decription: Intended Ship Dt From
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SDtFrm
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 1899/12/30 12:00:00 AM
		 * Identity: False
		 */
		public readonly static string _SDtFrm = "U_SDtFrm";
		public DateTime U_SDtFrm { 
			get {
 				return getValueAsDateTime(_SDtFrm); 
			}
			set { setValue(_SDtFrm, value); }
		}
		public void U_SDtFrm_AsString(string value){
			setValue("U_SDtFrm", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_SDtFrm_AsString(){
			DateTime dVal = getValueAsDateTime(_SDtFrm);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_SDtFrm() {
               return doValidate_SDtFrm(U_SDtFrm);
           }
           public virtual string doValidate_SDtFrm(object oValue) {
               return base.doValidation(_SDtFrm, oValue);
           }

		/**
		 * Decription: Intended Ship Dt To
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SDtTo
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 1899/12/30 12:00:00 AM
		 * Identity: False
		 */
		public readonly static string _SDtTo = "U_SDtTo";
		public DateTime U_SDtTo { 
			get {
 				return getValueAsDateTime(_SDtTo); 
			}
			set { setValue(_SDtTo, value); }
		}
		public void U_SDtTo_AsString(string value){
			setValue("U_SDtTo", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_SDtTo_AsString(){
			DateTime dVal = getValueAsDateTime(_SDtTo);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_SDtTo() {
               return doValidate_SDtTo(U_SDtTo);
           }
           public virtual string doValidate_SDtTo(object oValue) {
               return base.doValidation(_SDtTo, oValue);
           }

		/**
		 * Decription: Packaging Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PackTyp
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PackTyp = "U_PackTyp";
		public string U_PackTyp { 
			get {
 				return getValueAsString(_PackTyp); 
			}
			set { setValue(_PackTyp, value); }
		}
           public string doValidate_PackTyp() {
               return doValidate_PackTyp(U_PackTyp);
           }
           public virtual string doValidate_PackTyp(object oValue) {
               return base.doValidation(_PackTyp, oValue);
           }

		/**
		 * Decription: Special Handling
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SpHandle
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SpHandle = "U_SpHandle";
		public string U_SpHandle { 
			get {
 				return getValueAsString(_SpHandle); 
			}
			set { setValue(_SpHandle, value); }
		}
           public string doValidate_SpHandle() {
               return doValidate_SpHandle(U_SpHandle);
           }
           public virtual string doValidate_SpHandle(object oValue) {
               return base.doValidation(_SpHandle, oValue);
           }

		/**
		 * Decription: DCode
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DCode
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DCode = "U_DCode";
		public string U_DCode { 
			get {
 				return getValueAsString(_DCode); 
			}
			set { setValue(_DCode, value); }
		}
           public string doValidate_DCode() {
               return doValidate_DCode(U_DCode);
           }
           public virtual string doValidate_DCode(object oValue) {
               return base.doValidation(_DCode, oValue);
           }

		/**
		 * Decription: technology Emlpoyeed
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TechEmp
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TechEmp = "U_TechEmp";
		public string U_TechEmp { 
			get {
 				return getValueAsString(_TechEmp); 
			}
			set { setValue(_TechEmp, value); }
		}
           public string doValidate_TechEmp() {
               return doValidate_TechEmp(U_TechEmp);
           }
           public virtual string doValidate_TechEmp(object oValue) {
               return base.doValidation(_TechEmp, oValue);
           }

		/**
		 * Decription: Export Reason
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ExpRes
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ExpRes = "U_ExpRes";
		public string U_ExpRes { 
			get {
 				return getValueAsString(_ExpRes); 
			}
			set { setValue(_ExpRes, value); }
		}
           public string doValidate_ExpRes() {
               return doValidate_ExpRes(U_ExpRes);
           }
           public virtual string doValidate_ExpRes(object oValue) {
               return base.doValidation(_ExpRes, oValue);
           }

		/**
		 * Decription: Waste Composition
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WasteCmp
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WasteCmp = "U_WasteCmp";
		public string U_WasteCmp { 
			get {
 				return getValueAsString(_WasteCmp); 
			}
			set { setValue(_WasteCmp, value); }
		}
           public string doValidate_WasteCmp() {
               return doValidate_WasteCmp(U_WasteCmp);
           }
           public virtual string doValidate_WasteCmp(object oValue) {
               return base.doValidation(_WasteCmp, oValue);
           }

		/**
		 * Decription: Physical Characteristics
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PhyChtr
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PhyChtr = "U_PhyChtr";
		public string U_PhyChtr { 
			get {
 				return getValueAsString(_PhyChtr); 
			}
			set { setValue(_PhyChtr, value); }
		}
           public string doValidate_PhyChtr() {
               return doValidate_PhyChtr(U_PhyChtr);
           }
           public virtual string doValidate_PhyChtr(object oValue) {
               return base.doValidation(_PhyChtr, oValue);
           }

		/**
		 * Decription: Basel Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BaselCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BaselCd = "U_BaselCd";
		public string U_BaselCd { 
			get {
 				return getValueAsString(_BaselCd); 
			}
			set { setValue(_BaselCd, value); }
		}
           public string doValidate_BaselCd() {
               return doValidate_BaselCd(U_BaselCd);
           }
           public virtual string doValidate_BaselCd(object oValue) {
               return base.doValidation(_BaselCd, oValue);
           }

		/**
		 * Decription: OECD Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_OECDCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _OECDCd = "U_OECDCd";
		public string U_OECDCd { 
			get {
 				return getValueAsString(_OECDCd); 
			}
			set { setValue(_OECDCd, value); }
		}
           public string doValidate_OECDCd() {
               return doValidate_OECDCd(U_OECDCd);
           }
           public virtual string doValidate_OECDCd(object oValue) {
               return base.doValidation(_OECDCd, oValue);
           }

		/**
		 * Decription: EWC Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_EWCCd
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _EWCCd = "U_EWCCd";
		public string U_EWCCd { 
			get {
 				return getValueAsString(_EWCCd); 
			}
			set { setValue(_EWCCd, value); }
		}
           public string doValidate_EWCCd() {
               return doValidate_EWCCd(U_EWCCd);
           }
           public virtual string doValidate_EWCCd(object oValue) {
               return base.doValidation(_EWCCd, oValue);
           }

		/**
		 * Decription: Export National Cd
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ExpNatCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ExpNatCd = "U_ExpNatCd";
		public string U_ExpNatCd { 
			get {
 				return getValueAsString(_ExpNatCd); 
			}
			set { setValue(_ExpNatCd, value); }
		}
           public string doValidate_ExpNatCd() {
               return doValidate_ExpNatCd(U_ExpNatCd);
           }
           public virtual string doValidate_ExpNatCd(object oValue) {
               return base.doValidation(_ExpNatCd, oValue);
           }

		/**
		 * Decription: Import National Cd
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ImpNatCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ImpNatCd = "U_ImpNatCd";
		public string U_ImpNatCd { 
			get {
 				return getValueAsString(_ImpNatCd); 
			}
			set { setValue(_ImpNatCd, value); }
		}
           public string doValidate_ImpNatCd() {
               return doValidate_ImpNatCd(U_ImpNatCd);
           }
           public virtual string doValidate_ImpNatCd(object oValue) {
               return base.doValidation(_ImpNatCd, oValue);
           }

		/**
		 * Decription: Other
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Other
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Other = "U_Other";
		public string U_Other { 
			get {
 				return getValueAsString(_Other); 
			}
			set { setValue(_Other, value); }
		}
           public string doValidate_Other() {
               return doValidate_Other(U_Other);
           }
           public virtual string doValidate_Other(object oValue) {
               return base.doValidation(_Other, oValue);
           }

		/**
		 * Decription: Y-Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_YCode
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _YCode = "U_YCode";
		public string U_YCode { 
			get {
 				return getValueAsString(_YCode); 
			}
			set { setValue(_YCode, value); }
		}
           public string doValidate_YCode() {
               return doValidate_YCode(U_YCode);
           }
           public virtual string doValidate_YCode(object oValue) {
               return base.doValidation(_YCode, oValue);
           }

		/**
		 * Decription: H-Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HCode
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _HCode = "U_HCode";
		public string U_HCode { 
			get {
 				return getValueAsString(_HCode); 
			}
			set { setValue(_HCode, value); }
		}
           public string doValidate_HCode() {
               return doValidate_HCode(U_HCode);
           }
           public virtual string doValidate_HCode(object oValue) {
               return base.doValidation(_HCode, oValue);
           }

		/**
		 * Decription: UN Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UNNumber
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _UNNumber = "U_UNNumber";
		public string U_UNNumber { 
			get {
 				return getValueAsString(_UNNumber); 
			}
			set { setValue(_UNNumber, value); }
		}
           public string doValidate_UNNumber() {
               return doValidate_UNNumber(U_UNNumber);
           }
           public virtual string doValidate_UNNumber(object oValue) {
               return base.doValidation(_UNNumber, oValue);
           }

		/**
		 * Decription: UN Class
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UNClass
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _UNClass = "U_UNClass";
		public string U_UNClass { 
			get {
 				return getValueAsString(_UNClass); 
			}
			set { setValue(_UNClass, value); }
		}
           public string doValidate_UNClass() {
               return doValidate_UNClass(U_UNClass);
           }
           public virtual string doValidate_UNClass(object oValue) {
               return base.doValidation(_UNClass, oValue);
           }

		/**
		 * Decription: Shipping Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UNShipNm
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _UNShipNm = "U_UNShipNm";
		public string U_UNShipNm { 
			get {
 				return getValueAsString(_UNShipNm); 
			}
			set { setValue(_UNShipNm, value); }
		}
           public string doValidate_UNShipNm() {
               return doValidate_UNShipNm(U_UNShipNm);
           }
           public virtual string doValidate_UNShipNm(object oValue) {
               return base.doValidation(_UNShipNm, oValue);
           }

		/**
		 * Decription: Customer Cd
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CustCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CustCd = "U_CustCd";
		public string U_CustCd { 
			get {
 				return getValueAsString(_CustCd); 
			}
			set { setValue(_CustCd, value); }
		}
           public string doValidate_CustCd() {
               return doValidate_CustCd(U_CustCd);
           }
           public virtual string doValidate_CustCd(object oValue) {
               return base.doValidation(_CustCd, oValue);
           }

		/**
		 * Decription: Notifier Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_NOTCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _NOTCd = "U_NOTCd";
		public string U_NOTCd { 
			get {
 				return getValueAsString(_NOTCd); 
			}
			set { setValue(_NOTCd, value); }
		}
           public string doValidate_NOTCd() {
               return doValidate_NOTCd(U_NOTCd);
           }
           public virtual string doValidate_NOTCd(object oValue) {
               return base.doValidation(_NOTCd, oValue);
           }

		/**
		 * Decription: Notifier Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_NOTName
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _NOTName = "U_NOTName";
		public string U_NOTName { 
			get {
 				return getValueAsString(_NOTName); 
			}
			set { setValue(_NOTName, value); }
		}
           public string doValidate_NOTName() {
               return doValidate_NOTName(U_NOTName);
           }
           public virtual string doValidate_NOTName(object oValue) {
               return base.doValidation(_NOTName, oValue);
           }

		/**
		 * Decription: Notifier Reg. No.
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_NotRegNo
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _NotRegNo = "U_NotRegNo";
		public string U_NotRegNo { 
			get {
 				return getValueAsString(_NotRegNo); 
			}
			set { setValue(_NotRegNo, value); }
		}
           public string doValidate_NotRegNo() {
               return doValidate_NotRegNo(U_NotRegNo);
           }
           public virtual string doValidate_NotRegNo(object oValue) {
               return base.doValidation(_NotRegNo, oValue);
           }

		/**
		 * Decription: Notifier Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_NotAdres
		 * Size: 254
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _NotAdres = "U_NotAdres";
		public string U_NotAdres { 
			get {
 				return getValueAsString(_NotAdres); 
			}
			set { setValue(_NotAdres, value); }
		}
           public string doValidate_NotAdres() {
               return doValidate_NotAdres(U_NotAdres);
           }
           public virtual string doValidate_NotAdres(object oValue) {
               return base.doValidation(_NotAdres, oValue);
           }

		/**
		 * Decription: Notifier Contact Person
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_NotCntPr
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _NotCntPr = "U_NotCntPr";
		public string U_NotCntPr { 
			get {
 				return getValueAsString(_NotCntPr); 
			}
			set { setValue(_NotCntPr, value); }
		}
           public string doValidate_NotCntPr() {
               return doValidate_NotCntPr(U_NotCntPr);
           }
           public virtual string doValidate_NotCntPr(object oValue) {
               return base.doValidation(_NotCntPr, oValue);
           }

		/**
		 * Decription: Notifier Phone
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_NotPh
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _NotPh = "U_NotPh";
		public string U_NotPh { 
			get {
 				return getValueAsString(_NotPh); 
			}
			set { setValue(_NotPh, value); }
		}
           public string doValidate_NotPh() {
               return doValidate_NotPh(U_NotPh);
           }
           public virtual string doValidate_NotPh(object oValue) {
               return base.doValidation(_NotPh, oValue);
           }

		/**
		 * Decription: Notifier Fax
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_NotFax
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _NotFax = "U_NotFax";
		public string U_NotFax { 
			get {
 				return getValueAsString(_NotFax); 
			}
			set { setValue(_NotFax, value); }
		}
           public string doValidate_NotFax() {
               return doValidate_NotFax(U_NotFax);
           }
           public virtual string doValidate_NotFax(object oValue) {
               return base.doValidation(_NotFax, oValue);
           }

		/**
		 * Decription: Notifier Email
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_NotEmail
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _NotEmail = "U_NotEmail";
		public string U_NotEmail { 
			get {
 				return getValueAsString(_NotEmail); 
			}
			set { setValue(_NotEmail, value); }
		}
           public string doValidate_NotEmail() {
               return doValidate_NotEmail(U_NotEmail);
           }
           public virtual string doValidate_NotEmail(object oValue) {
               return base.doValidation(_NotEmail, oValue);
           }

		/**
		 * Decription: Consignor Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CONCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CONCd = "U_CONCd";
		public string U_CONCd { 
			get {
 				return getValueAsString(_CONCd); 
			}
			set { setValue(_CONCd, value); }
		}
           public string doValidate_CONCd() {
               return doValidate_CONCd(U_CONCd);
           }
           public virtual string doValidate_CONCd(object oValue) {
               return base.doValidation(_CONCd, oValue);
           }

		/**
		 * Decription: Consignor Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CONName
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CONName = "U_CONName";
		public string U_CONName { 
			get {
 				return getValueAsString(_CONName); 
			}
			set { setValue(_CONName, value); }
		}
           public string doValidate_CONName() {
               return doValidate_CONName(U_CONName);
           }
           public virtual string doValidate_CONName(object oValue) {
               return base.doValidation(_CONName, oValue);
           }

		/**
		 * Decription: Consignor Reg. No.
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ConRegNo
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ConRegNo = "U_ConRegNo";
		public string U_ConRegNo { 
			get {
 				return getValueAsString(_ConRegNo); 
			}
			set { setValue(_ConRegNo, value); }
		}
           public string doValidate_ConRegNo() {
               return doValidate_ConRegNo(U_ConRegNo);
           }
           public virtual string doValidate_ConRegNo(object oValue) {
               return base.doValidation(_ConRegNo, oValue);
           }

		/**
		 * Decription: Consignor Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ConAdres
		 * Size: 254
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ConAdres = "U_ConAdres";
		public string U_ConAdres { 
			get {
 				return getValueAsString(_ConAdres); 
			}
			set { setValue(_ConAdres, value); }
		}
           public string doValidate_ConAdres() {
               return doValidate_ConAdres(U_ConAdres);
           }
           public virtual string doValidate_ConAdres(object oValue) {
               return base.doValidation(_ConAdres, oValue);
           }

		/**
		 * Decription: Consignor Contact Person
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ConCntPr
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ConCntPr = "U_ConCntPr";
		public string U_ConCntPr { 
			get {
 				return getValueAsString(_ConCntPr); 
			}
			set { setValue(_ConCntPr, value); }
		}
           public string doValidate_ConCntPr() {
               return doValidate_ConCntPr(U_ConCntPr);
           }
           public virtual string doValidate_ConCntPr(object oValue) {
               return base.doValidation(_ConCntPr, oValue);
           }

		/**
		 * Decription: Consignor Phone
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ConPh
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ConPh = "U_ConPh";
		public string U_ConPh { 
			get {
 				return getValueAsString(_ConPh); 
			}
			set { setValue(_ConPh, value); }
		}
           public string doValidate_ConPh() {
               return doValidate_ConPh(U_ConPh);
           }
           public virtual string doValidate_ConPh(object oValue) {
               return base.doValidation(_ConPh, oValue);
           }

		/**
		 * Decription: Consignor Fax
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ConFax
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ConFax = "U_ConFax";
		public string U_ConFax { 
			get {
 				return getValueAsString(_ConFax); 
			}
			set { setValue(_ConFax, value); }
		}
           public string doValidate_ConFax() {
               return doValidate_ConFax(U_ConFax);
           }
           public virtual string doValidate_ConFax(object oValue) {
               return base.doValidation(_ConFax, oValue);
           }

		/**
		 * Decription: Consignor Email
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ConEmail
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ConEmail = "U_ConEmail";
		public string U_ConEmail { 
			get {
 				return getValueAsString(_ConEmail); 
			}
			set { setValue(_ConEmail, value); }
		}
           public string doValidate_ConEmail() {
               return doValidate_ConEmail(U_ConEmail);
           }
           public virtual string doValidate_ConEmail(object oValue) {
               return base.doValidation(_ConEmail, oValue);
           }

		/**
		 * Decription: Consignee Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_COECd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _COECd = "U_COECd";
		public string U_COECd { 
			get {
 				return getValueAsString(_COECd); 
			}
			set { setValue(_COECd, value); }
		}
           public string doValidate_COECd() {
               return doValidate_COECd(U_COECd);
           }
           public virtual string doValidate_COECd(object oValue) {
               return base.doValidation(_COECd, oValue);
           }

		/**
		 * Decription: Consignee Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_COEName
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _COEName = "U_COEName";
		public string U_COEName { 
			get {
 				return getValueAsString(_COEName); 
			}
			set { setValue(_COEName, value); }
		}
           public string doValidate_COEName() {
               return doValidate_COEName(U_COEName);
           }
           public virtual string doValidate_COEName(object oValue) {
               return base.doValidation(_COEName, oValue);
           }

		/**
		 * Decription: Consignee Reg. No.
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CoeRegNo
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CoeRegNo = "U_CoeRegNo";
		public string U_CoeRegNo { 
			get {
 				return getValueAsString(_CoeRegNo); 
			}
			set { setValue(_CoeRegNo, value); }
		}
           public string doValidate_CoeRegNo() {
               return doValidate_CoeRegNo(U_CoeRegNo);
           }
           public virtual string doValidate_CoeRegNo(object oValue) {
               return base.doValidation(_CoeRegNo, oValue);
           }

		/**
		 * Decription: Consignee Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CoeAdres
		 * Size: 254
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CoeAdres = "U_CoeAdres";
		public string U_CoeAdres { 
			get {
 				return getValueAsString(_CoeAdres); 
			}
			set { setValue(_CoeAdres, value); }
		}
           public string doValidate_CoeAdres() {
               return doValidate_CoeAdres(U_CoeAdres);
           }
           public virtual string doValidate_CoeAdres(object oValue) {
               return base.doValidation(_CoeAdres, oValue);
           }

		/**
		 * Decription: Consignee Contact Person
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CoeCntPr
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CoeCntPr = "U_CoeCntPr";
		public string U_CoeCntPr { 
			get {
 				return getValueAsString(_CoeCntPr); 
			}
			set { setValue(_CoeCntPr, value); }
		}
           public string doValidate_CoeCntPr() {
               return doValidate_CoeCntPr(U_CoeCntPr);
           }
           public virtual string doValidate_CoeCntPr(object oValue) {
               return base.doValidation(_CoeCntPr, oValue);
           }

		/**
		 * Decription: Consignee Phone
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CoePh
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CoePh = "U_CoePh";
		public string U_CoePh { 
			get {
 				return getValueAsString(_CoePh); 
			}
			set { setValue(_CoePh, value); }
		}
           public string doValidate_CoePh() {
               return doValidate_CoePh(U_CoePh);
           }
           public virtual string doValidate_CoePh(object oValue) {
               return base.doValidation(_CoePh, oValue);
           }

		/**
		 * Decription: Consignee Fax
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CoeFax
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CoeFax = "U_CoeFax";
		public string U_CoeFax { 
			get {
 				return getValueAsString(_CoeFax); 
			}
			set { setValue(_CoeFax, value); }
		}
           public string doValidate_CoeFax() {
               return doValidate_CoeFax(U_CoeFax);
           }
           public virtual string doValidate_CoeFax(object oValue) {
               return base.doValidation(_CoeFax, oValue);
           }

		/**
		 * Decription: Consignee Email
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CoeEmail
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CoeEmail = "U_CoeEmail";
		public string U_CoeEmail { 
			get {
 				return getValueAsString(_CoeEmail); 
			}
			set { setValue(_CoeEmail, value); }
		}
           public string doValidate_CoeEmail() {
               return doValidate_CoeEmail(U_CoeEmail);
           }
           public virtual string doValidate_CoeEmail(object oValue) {
               return base.doValidation(_CoeEmail, oValue);
           }

		/**
		 * Decription: Carrier Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CARCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CARCd = "U_CARCd";
		public string U_CARCd { 
			get {
 				return getValueAsString(_CARCd); 
			}
			set { setValue(_CARCd, value); }
		}
           public string doValidate_CARCd() {
               return doValidate_CARCd(U_CARCd);
           }
           public virtual string doValidate_CARCd(object oValue) {
               return base.doValidation(_CARCd, oValue);
           }

		/**
		 * Decription: Carrier Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CARName
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CARName = "U_CARName";
		public string U_CARName { 
			get {
 				return getValueAsString(_CARName); 
			}
			set { setValue(_CARName, value); }
		}
           public string doValidate_CARName() {
               return doValidate_CARName(U_CARName);
           }
           public virtual string doValidate_CARName(object oValue) {
               return base.doValidation(_CARName, oValue);
           }

		/**
		 * Decription: Generator Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_GENCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _GENCd = "U_GENCd";
		public string U_GENCd { 
			get {
 				return getValueAsString(_GENCd); 
			}
			set { setValue(_GENCd, value); }
		}
           public string doValidate_GENCd() {
               return doValidate_GENCd(U_GENCd);
           }
           public virtual string doValidate_GENCd(object oValue) {
               return base.doValidation(_GENCd, oValue);
           }

		/**
		 * Decription: Generator Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_GENName
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _GENName = "U_GENName";
		public string U_GENName { 
			get {
 				return getValueAsString(_GENName); 
			}
			set { setValue(_GENName, value); }
		}
           public string doValidate_GENName() {
               return doValidate_GENName(U_GENName);
           }
           public virtual string doValidate_GENName(object oValue) {
               return base.doValidation(_GENName, oValue);
           }

		/**
		 * Decription: Generator Reg. No.
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_GenRegNo
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _GenRegNo = "U_GenRegNo";
		public string U_GenRegNo { 
			get {
 				return getValueAsString(_GenRegNo); 
			}
			set { setValue(_GenRegNo, value); }
		}
           public string doValidate_GenRegNo() {
               return doValidate_GenRegNo(U_GenRegNo);
           }
           public virtual string doValidate_GenRegNo(object oValue) {
               return base.doValidation(_GenRegNo, oValue);
           }

		/**
		 * Decription: Generator Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_GenAdres
		 * Size: 254
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _GenAdres = "U_GenAdres";
		public string U_GenAdres { 
			get {
 				return getValueAsString(_GenAdres); 
			}
			set { setValue(_GenAdres, value); }
		}
           public string doValidate_GenAdres() {
               return doValidate_GenAdres(U_GenAdres);
           }
           public virtual string doValidate_GenAdres(object oValue) {
               return base.doValidation(_GenAdres, oValue);
           }

		/**
		 * Decription: Generator Contact Person
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_GenCntPr
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _GenCntPr = "U_GenCntPr";
		public string U_GenCntPr { 
			get {
 				return getValueAsString(_GenCntPr); 
			}
			set { setValue(_GenCntPr, value); }
		}
           public string doValidate_GenCntPr() {
               return doValidate_GenCntPr(U_GenCntPr);
           }
           public virtual string doValidate_GenCntPr(object oValue) {
               return base.doValidation(_GenCntPr, oValue);
           }

		/**
		 * Decription: Generator Phone
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_GenPh
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _GenPh = "U_GenPh";
		public string U_GenPh { 
			get {
 				return getValueAsString(_GenPh); 
			}
			set { setValue(_GenPh, value); }
		}
           public string doValidate_GenPh() {
               return doValidate_GenPh(U_GenPh);
           }
           public virtual string doValidate_GenPh(object oValue) {
               return base.doValidation(_GenPh, oValue);
           }

		/**
		 * Decription: Generator Fax
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_GenFax
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _GenFax = "U_GenFax";
		public string U_GenFax { 
			get {
 				return getValueAsString(_GenFax); 
			}
			set { setValue(_GenFax, value); }
		}
           public string doValidate_GenFax() {
               return doValidate_GenFax(U_GenFax);
           }
           public virtual string doValidate_GenFax(object oValue) {
               return base.doValidation(_GenFax, oValue);
           }

		/**
		 * Decription: Generator Email
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_GenEmail
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _GenEmail = "U_GenEmail";
		public string U_GenEmail { 
			get {
 				return getValueAsString(_GenEmail); 
			}
			set { setValue(_GenEmail, value); }
		}
           public string doValidate_GenEmail() {
               return doValidate_GenEmail(U_GenEmail);
           }
           public virtual string doValidate_GenEmail(object oValue) {
               return base.doValidation(_GenEmail, oValue);
           }

		/**
		 * Decription: Disposal Site Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSPCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DSPCd = "U_DSPCd";
		public string U_DSPCd { 
			get {
 				return getValueAsString(_DSPCd); 
			}
			set { setValue(_DSPCd, value); }
		}
           public string doValidate_DSPCd() {
               return doValidate_DSPCd(U_DSPCd);
           }
           public virtual string doValidate_DSPCd(object oValue) {
               return base.doValidation(_DSPCd, oValue);
           }

		/**
		 * Decription: Disposal Site Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DSPName
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DSPName = "U_DSPName";
		public string U_DSPName { 
			get {
 				return getValueAsString(_DSPName); 
			}
			set { setValue(_DSPName, value); }
		}
           public string doValidate_DSPName() {
               return doValidate_DSPName(U_DSPName);
           }
           public virtual string doValidate_DSPName(object oValue) {
               return base.doValidation(_DSPName, oValue);
           }

		/**
		 * Decription: Disp. Facility Reg. No.
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DOFRegNo
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DOFRegNo = "U_DOFRegNo";
		public string U_DOFRegNo { 
			get {
 				return getValueAsString(_DOFRegNo); 
			}
			set { setValue(_DOFRegNo, value); }
		}
           public string doValidate_DOFRegNo() {
               return doValidate_DOFRegNo(U_DOFRegNo);
           }
           public virtual string doValidate_DOFRegNo(object oValue) {
               return base.doValidation(_DOFRegNo, oValue);
           }

		/**
		 * Decription: Disp. Facility Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DOFAdres
		 * Size: 254
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DOFAdres = "U_DOFAdres";
		public string U_DOFAdres { 
			get {
 				return getValueAsString(_DOFAdres); 
			}
			set { setValue(_DOFAdres, value); }
		}
           public string doValidate_DOFAdres() {
               return doValidate_DOFAdres(U_DOFAdres);
           }
           public virtual string doValidate_DOFAdres(object oValue) {
               return base.doValidation(_DOFAdres, oValue);
           }

		/**
		 * Decription: Disp. Facility Contact Person
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DOFCntPr
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DOFCntPr = "U_DOFCntPr";
		public string U_DOFCntPr { 
			get {
 				return getValueAsString(_DOFCntPr); 
			}
			set { setValue(_DOFCntPr, value); }
		}
           public string doValidate_DOFCntPr() {
               return doValidate_DOFCntPr(U_DOFCntPr);
           }
           public virtual string doValidate_DOFCntPr(object oValue) {
               return base.doValidation(_DOFCntPr, oValue);
           }

		/**
		 * Decription: Disp. Facility Phone
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DOFPh
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DOFPh = "U_DOFPh";
		public string U_DOFPh { 
			get {
 				return getValueAsString(_DOFPh); 
			}
			set { setValue(_DOFPh, value); }
		}
           public string doValidate_DOFPh() {
               return doValidate_DOFPh(U_DOFPh);
           }
           public virtual string doValidate_DOFPh(object oValue) {
               return base.doValidation(_DOFPh, oValue);
           }

		/**
		 * Decription: Disp. Facility Fax
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DOFFax
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DOFFax = "U_DOFFax";
		public string U_DOFFax { 
			get {
 				return getValueAsString(_DOFFax); 
			}
			set { setValue(_DOFFax, value); }
		}
           public string doValidate_DOFFax() {
               return doValidate_DOFFax(U_DOFFax);
           }
           public virtual string doValidate_DOFFax(object oValue) {
               return base.doValidation(_DOFFax, oValue);
           }

		/**
		 * Decription: Disp. Facility Email
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DOFEmail
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DOFEmail = "U_DOFEmail";
		public string U_DOFEmail { 
			get {
 				return getValueAsString(_DOFEmail); 
			}
			set { setValue(_DOFEmail, value); }
		}
           public string doValidate_DOFEmail() {
               return doValidate_DOFEmail(U_DOFEmail);
           }
           public virtual string doValidate_DOFEmail(object oValue) {
               return base.doValidation(_DOFEmail, oValue);
           }

		/**
		 * Decription: Pre Approved
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DOPreAp
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DOPreAp = "U_DOPreAp";
		public string U_DOPreAp { 
			get {
 				return getValueAsString(_DOPreAp); 
			}
			set { setValue(_DOPreAp, value); }
		}
           public string doValidate_DOPreAp() {
               return doValidate_DOPreAp(U_DOPreAp);
           }
           public virtual string doValidate_DOPreAp(object oValue) {
               return base.doValidation(_DOPreAp, oValue);
           }

		/**
		 * Decription: Final Destination Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FDSCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _FDSCd = "U_FDSCd";
		public string U_FDSCd { 
			get {
 				return getValueAsString(_FDSCd); 
			}
			set { setValue(_FDSCd, value); }
		}
           public string doValidate_FDSCd() {
               return doValidate_FDSCd(U_FDSCd);
           }
           public virtual string doValidate_FDSCd(object oValue) {
               return base.doValidation(_FDSCd, oValue);
           }

		/**
		 * Decription: Final Destination Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FDSName
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _FDSName = "U_FDSName";
		public string U_FDSName { 
			get {
 				return getValueAsString(_FDSName); 
			}
			set { setValue(_FDSName, value); }
		}
           public string doValidate_FDSName() {
               return doValidate_FDSName(U_FDSName);
           }
           public virtual string doValidate_FDSName(object oValue) {
               return base.doValidation(_FDSName, oValue);
           }

		/**
		 * Decription: Final Dest. Reg. No.
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FDTRegNo
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _FDTRegNo = "U_FDTRegNo";
		public string U_FDTRegNo { 
			get {
 				return getValueAsString(_FDTRegNo); 
			}
			set { setValue(_FDTRegNo, value); }
		}
           public string doValidate_FDTRegNo() {
               return doValidate_FDTRegNo(U_FDTRegNo);
           }
           public virtual string doValidate_FDTRegNo(object oValue) {
               return base.doValidation(_FDTRegNo, oValue);
           }

		/**
		 * Decription: Final Dest. Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FDTAdres
		 * Size: 254
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _FDTAdres = "U_FDTAdres";
		public string U_FDTAdres { 
			get {
 				return getValueAsString(_FDTAdres); 
			}
			set { setValue(_FDTAdres, value); }
		}
           public string doValidate_FDTAdres() {
               return doValidate_FDTAdres(U_FDTAdres);
           }
           public virtual string doValidate_FDTAdres(object oValue) {
               return base.doValidation(_FDTAdres, oValue);
           }

		/**
		 * Decription: Final Dest. Contact Person
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FDTCntPr
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _FDTCntPr = "U_FDTCntPr";
		public string U_FDTCntPr { 
			get {
 				return getValueAsString(_FDTCntPr); 
			}
			set { setValue(_FDTCntPr, value); }
		}
           public string doValidate_FDTCntPr() {
               return doValidate_FDTCntPr(U_FDTCntPr);
           }
           public virtual string doValidate_FDTCntPr(object oValue) {
               return base.doValidation(_FDTCntPr, oValue);
           }

		/**
		 * Decription: Final Dest. Phone
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FDTPh
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _FDTPh = "U_FDTPh";
		public string U_FDTPh { 
			get {
 				return getValueAsString(_FDTPh); 
			}
			set { setValue(_FDTPh, value); }
		}
           public string doValidate_FDTPh() {
               return doValidate_FDTPh(U_FDTPh);
           }
           public virtual string doValidate_FDTPh(object oValue) {
               return base.doValidation(_FDTPh, oValue);
           }

		/**
		 * Decription: Final Dest. Fax
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FDTFax
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _FDTFax = "U_FDTFax";
		public string U_FDTFax { 
			get {
 				return getValueAsString(_FDTFax); 
			}
			set { setValue(_FDTFax, value); }
		}
           public string doValidate_FDTFax() {
               return doValidate_FDTFax(U_FDTFax);
           }
           public virtual string doValidate_FDTFax(object oValue) {
               return base.doValidation(_FDTFax, oValue);
           }

		/**
		 * Decription: Final Dest. Email
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FDTEmail
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _FDTEmail = "U_FDTEmail";
		public string U_FDTEmail { 
			get {
 				return getValueAsString(_FDTEmail); 
			}
			set { setValue(_FDTEmail, value); }
		}
           public string doValidate_FDTEmail() {
               return doValidate_FDTEmail(U_FDTEmail);
           }
           public virtual string doValidate_FDTEmail(object oValue) {
               return base.doValidation(_FDTEmail, oValue);
           }

		/**
		 * Decription: Multiple
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Multiple
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Multiple = "U_Multiple";
		public string U_Multiple { 
			get {
 				return getValueAsString(_Multiple); 
			}
			set { setValue(_Multiple, value); }
		}
           public string doValidate_Multiple() {
               return doValidate_Multiple(U_Multiple);
           }
           public virtual string doValidate_Multiple(object oValue) {
               return base.doValidation(_Multiple, oValue);
           }

		/**
		 * Decription: Annex Code 1
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxCd1
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxCd1 = "U_AnxCd1";
		public string U_AnxCd1 { 
			get {
 				return getValueAsString(_AnxCd1); 
			}
			set { setValue(_AnxCd1, value); }
		}
           public string doValidate_AnxCd1() {
               return doValidate_AnxCd1(U_AnxCd1);
           }
           public virtual string doValidate_AnxCd1(object oValue) {
               return base.doValidation(_AnxCd1, oValue);
           }

		/**
		 * Decription: Annex Description 1
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxDesc1
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxDesc1 = "U_AnxDesc1";
		public string U_AnxDesc1 { 
			get {
 				return getValueAsString(_AnxDesc1); 
			}
			set { setValue(_AnxDesc1, value); }
		}
           public string doValidate_AnxDesc1() {
               return doValidate_AnxDesc1(U_AnxDesc1);
           }
           public virtual string doValidate_AnxDesc1(object oValue) {
               return base.doValidation(_AnxDesc1, oValue);
           }

		/**
		 * Decription: Annex Code 2
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxCd2
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxCd2 = "U_AnxCd2";
		public string U_AnxCd2 { 
			get {
 				return getValueAsString(_AnxCd2); 
			}
			set { setValue(_AnxCd2, value); }
		}
           public string doValidate_AnxCd2() {
               return doValidate_AnxCd2(U_AnxCd2);
           }
           public virtual string doValidate_AnxCd2(object oValue) {
               return base.doValidation(_AnxCd2, oValue);
           }

		/**
		 * Decription: Annex Description 2
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxDesc2
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxDesc2 = "U_AnxDesc2";
		public string U_AnxDesc2 { 
			get {
 				return getValueAsString(_AnxDesc2); 
			}
			set { setValue(_AnxDesc2, value); }
		}
           public string doValidate_AnxDesc2() {
               return doValidate_AnxDesc2(U_AnxDesc2);
           }
           public virtual string doValidate_AnxDesc2(object oValue) {
               return base.doValidation(_AnxDesc2, oValue);
           }

		/**
		 * Decription: Annex Code 3
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxCd3
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxCd3 = "U_AnxCd3";
		public string U_AnxCd3 { 
			get {
 				return getValueAsString(_AnxCd3); 
			}
			set { setValue(_AnxCd3, value); }
		}
           public string doValidate_AnxCd3() {
               return doValidate_AnxCd3(U_AnxCd3);
           }
           public virtual string doValidate_AnxCd3(object oValue) {
               return base.doValidation(_AnxCd3, oValue);
           }

		/**
		 * Decription: Annex Description 3
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxDesc3
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxDesc3 = "U_AnxDesc3";
		public string U_AnxDesc3 { 
			get {
 				return getValueAsString(_AnxDesc3); 
			}
			set { setValue(_AnxDesc3, value); }
		}
           public string doValidate_AnxDesc3() {
               return doValidate_AnxDesc3(U_AnxDesc3);
           }
           public virtual string doValidate_AnxDesc3(object oValue) {
               return base.doValidation(_AnxDesc3, oValue);
           }

		/**
		 * Decription: Annex Code 4
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxCd4
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxCd4 = "U_AnxCd4";
		public string U_AnxCd4 { 
			get {
 				return getValueAsString(_AnxCd4); 
			}
			set { setValue(_AnxCd4, value); }
		}
           public string doValidate_AnxCd4() {
               return doValidate_AnxCd4(U_AnxCd4);
           }
           public virtual string doValidate_AnxCd4(object oValue) {
               return base.doValidation(_AnxCd4, oValue);
           }

		/**
		 * Decription: Annex Description 4
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxDesc4
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxDesc4 = "U_AnxDesc4";
		public string U_AnxDesc4 { 
			get {
 				return getValueAsString(_AnxDesc4); 
			}
			set { setValue(_AnxDesc4, value); }
		}
           public string doValidate_AnxDesc4() {
               return doValidate_AnxDesc4(U_AnxDesc4);
           }
           public virtual string doValidate_AnxDesc4(object oValue) {
               return base.doValidation(_AnxDesc4, oValue);
           }

		/**
		 * Decription: Annex Code 5
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxCd5
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxCd5 = "U_AnxCd5";
		public string U_AnxCd5 { 
			get {
 				return getValueAsString(_AnxCd5); 
			}
			set { setValue(_AnxCd5, value); }
		}
           public string doValidate_AnxCd5() {
               return doValidate_AnxCd5(U_AnxCd5);
           }
           public virtual string doValidate_AnxCd5(object oValue) {
               return base.doValidation(_AnxCd5, oValue);
           }

		/**
		 * Decription: Annex Description 5
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxDesc5
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxDesc5 = "U_AnxDesc5";
		public string U_AnxDesc5 { 
			get {
 				return getValueAsString(_AnxDesc5); 
			}
			set { setValue(_AnxDesc5, value); }
		}
           public string doValidate_AnxDesc5() {
               return doValidate_AnxDesc5(U_AnxDesc5);
           }
           public virtual string doValidate_AnxDesc5(object oValue) {
               return base.doValidation(_AnxDesc5, oValue);
           }

		/**
		 * Decription: Annex Code 6
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxCd6
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxCd6 = "U_AnxCd6";
		public string U_AnxCd6 { 
			get {
 				return getValueAsString(_AnxCd6); 
			}
			set { setValue(_AnxCd6, value); }
		}
           public string doValidate_AnxCd6() {
               return doValidate_AnxCd6(U_AnxCd6);
           }
           public virtual string doValidate_AnxCd6(object oValue) {
               return base.doValidation(_AnxCd6, oValue);
           }

		/**
		 * Decription: Annex Description 6
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxDesc6
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxDesc6 = "U_AnxDesc6";
		public string U_AnxDesc6 { 
			get {
 				return getValueAsString(_AnxDesc6); 
			}
			set { setValue(_AnxDesc6, value); }
		}
           public string doValidate_AnxDesc6() {
               return doValidate_AnxDesc6(U_AnxDesc6);
           }
           public virtual string doValidate_AnxDesc6(object oValue) {
               return base.doValidation(_AnxDesc6, oValue);
           }

		/**
		 * Decription: Annex Code 7
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxCd7
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxCd7 = "U_AnxCd7";
		public string U_AnxCd7 { 
			get {
 				return getValueAsString(_AnxCd7); 
			}
			set { setValue(_AnxCd7, value); }
		}
           public string doValidate_AnxCd7() {
               return doValidate_AnxCd7(U_AnxCd7);
           }
           public virtual string doValidate_AnxCd7(object oValue) {
               return base.doValidation(_AnxCd7, oValue);
           }

		/**
		 * Decription: Annex Description 7
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxDesc7
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxDesc7 = "U_AnxDesc7";
		public string U_AnxDesc7 { 
			get {
 				return getValueAsString(_AnxDesc7); 
			}
			set { setValue(_AnxDesc7, value); }
		}
           public string doValidate_AnxDesc7() {
               return doValidate_AnxDesc7(U_AnxDesc7);
           }
           public virtual string doValidate_AnxDesc7(object oValue) {
               return base.doValidation(_AnxDesc7, oValue);
           }

		/**
		 * Decription: Annex Code 8
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxCd8
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxCd8 = "U_AnxCd8";
		public string U_AnxCd8 { 
			get {
 				return getValueAsString(_AnxCd8); 
			}
			set { setValue(_AnxCd8, value); }
		}
           public string doValidate_AnxCd8() {
               return doValidate_AnxCd8(U_AnxCd8);
           }
           public virtual string doValidate_AnxCd8(object oValue) {
               return base.doValidation(_AnxCd8, oValue);
           }

		/**
		 * Decription: Annex Description 8
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxDesc8
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxDesc8 = "U_AnxDesc8";
		public string U_AnxDesc8 { 
			get {
 				return getValueAsString(_AnxDesc8); 
			}
			set { setValue(_AnxDesc8, value); }
		}
           public string doValidate_AnxDesc8() {
               return doValidate_AnxDesc8(U_AnxDesc8);
           }
           public virtual string doValidate_AnxDesc8(object oValue) {
               return base.doValidation(_AnxDesc8, oValue);
           }

		/**
		 * Decription: Annex Code 9
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxCd9
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxCd9 = "U_AnxCd9";
		public string U_AnxCd9 { 
			get {
 				return getValueAsString(_AnxCd9); 
			}
			set { setValue(_AnxCd9, value); }
		}
           public string doValidate_AnxCd9() {
               return doValidate_AnxCd9(U_AnxCd9);
           }
           public virtual string doValidate_AnxCd9(object oValue) {
               return base.doValidation(_AnxCd9, oValue);
           }

		/**
		 * Decription: Annex Description 9
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxDesc9
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxDesc9 = "U_AnxDesc9";
		public string U_AnxDesc9 { 
			get {
 				return getValueAsString(_AnxDesc9); 
			}
			set { setValue(_AnxDesc9, value); }
		}
           public string doValidate_AnxDesc9() {
               return doValidate_AnxDesc9(U_AnxDesc9);
           }
           public virtual string doValidate_AnxDesc9(object oValue) {
               return base.doValidation(_AnxDesc9, oValue);
           }

		/**
		 * Decription: Annex Code 10
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxCd10
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxCd10 = "U_AnxCd10";
		public string U_AnxCd10 { 
			get {
 				return getValueAsString(_AnxCd10); 
			}
			set { setValue(_AnxCd10, value); }
		}
           public string doValidate_AnxCd10() {
               return doValidate_AnxCd10(U_AnxCd10);
           }
           public virtual string doValidate_AnxCd10(object oValue) {
               return base.doValidation(_AnxCd10, oValue);
           }

		/**
		 * Decription: Annex Description 10
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnxDesc10
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnxDesc10 = "U_AnxDesc10";
		public string U_AnxDesc10 { 
			get {
 				return getValueAsString(_AnxDesc10); 
			}
			set { setValue(_AnxDesc10, value); }
		}
           public string doValidate_AnxDesc10() {
               return doValidate_AnxDesc10(U_AnxDesc10);
           }
           public virtual string doValidate_AnxDesc10(object oValue) {
               return base.doValidation(_AnxDesc10, oValue);
           }

		/**
		 * Decription: WR Link Order
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WROrd
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WROrd = "U_WROrd";
		public string U_WROrd { 
			get {
 				return getValueAsString(_WROrd); 
			}
			set { setValue(_WROrd, value); }
		}
           public string doValidate_WROrd() {
               return doValidate_WROrd(U_WROrd);
           }
           public virtual string doValidate_WROrd(object oValue) {
               return base.doValidation(_WROrd, oValue);
           }

		/**
		 * Decription: Sp.H. Details
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SpHDetails
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SpHDetails = "U_SpHDetails";
		public string U_SpHDetails { 
			get {
 				return getValueAsString(_SpHDetails); 
			}
			set { setValue(_SpHDetails, value); }
		}
           public string doValidate_SpHDetails() {
               return doValidate_SpHDetails(U_SpHDetails);
           }
           public virtual string doValidate_SpHDetails(object oValue) {
               return base.doValidation(_SpHDetails, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(108);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_TFSNum, "TFS Number", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //TFS Number
			moDBFields.Add(_Type, "Type", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Type
			moDBFields.Add(_Status, "Status", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Status
			moDBFields.Add(_ShpType, "Shipment Type", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Shipment Type
			moDBFields.Add(_OpType, "Operation Type", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Operation Type
			moDBFields.Add(_SPreAp, "Site Pre-approved", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Site Pre-approved
			moDBFields.Add(_IntShip, "Intended Shipments", 8, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Intended Shipments
			moDBFields.Add(_ActShip, "Actual Shipments", 9, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Actual Shipments
            moDBFields.Add(_IntQty, "Intended Shipments", 10, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Intended Shipments
			moDBFields.Add(_QtyT, "Intended Qty Tones", 11, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Intended Qty Tones
			moDBFields.Add(_QtyCMtr, "Intended Qty Cubic Meter", 12, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Intended Qty Cubic Meter
			moDBFields.Add(_SDtFrm, "Intended Ship Dt From", 13, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Intended Ship Dt From
			moDBFields.Add(_SDtTo, "Intended Ship Dt To", 14, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Intended Ship Dt To
			moDBFields.Add(_PackTyp, "Packaging Type", 15, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Packaging Type
			moDBFields.Add(_SpHandle, "Special Handling", 16, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Special Handling
			moDBFields.Add(_DCode, "DCode", 17, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //DCode
			moDBFields.Add(_TechEmp, "technology Emlpoyeed", 18, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //technology Emlpoyeed
			moDBFields.Add(_ExpRes, "Export Reason", 19, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Export Reason
			moDBFields.Add(_WasteCmp, "Waste Composition", 20, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Waste Composition
			moDBFields.Add(_PhyChtr, "Physical Characteristics", 21, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Physical Characteristics
			moDBFields.Add(_BaselCd, "Basel Code", 22, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Basel Code
			moDBFields.Add(_OECDCd, "OECD Code", 23, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //OECD Code
			moDBFields.Add(_EWCCd, "EWC Code", 24, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //EWC Code
			moDBFields.Add(_ExpNatCd, "Export National Cd", 25, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Export National Cd
			moDBFields.Add(_ImpNatCd, "Import National Cd", 26, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Import National Cd
			moDBFields.Add(_Other, "Other", 27, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Other
			moDBFields.Add(_YCode, "Y-Code", 28, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Y-Code
			moDBFields.Add(_HCode, "H-Code", 29, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //H-Code
            moDBFields.Add(_UNNumber, "UN Number", 30, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //UN Number
            moDBFields.Add(_UNClass, "UN Class", 31, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //UN Class
            moDBFields.Add(_UNShipNm, "Shipping Name", 32, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Shipping Name
			moDBFields.Add(_CustCd, "Customer Cd", 33, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Customer Cd
			moDBFields.Add(_NOTCd, "Notifier Code", 34, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Notifier Code
			moDBFields.Add(_NOTName, "Notifier Name", 35, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Notifier Name
			moDBFields.Add(_NotRegNo, "Notifier Reg. No.", 36, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Notifier Reg. No.
			moDBFields.Add(_NotAdres, "Notifier Address", 37, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, EMPTYSTR, false, false); //Notifier Address
			moDBFields.Add(_NotCntPr, "Notifier Contact Person", 38, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Notifier Contact Person
			moDBFields.Add(_NotPh, "Notifier Phone", 39, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Notifier Phone
			moDBFields.Add(_NotFax, "Notifier Fax", 40, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Notifier Fax
			moDBFields.Add(_NotEmail, "Notifier Email", 41, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Notifier Email
			moDBFields.Add(_CONCd, "Consignor Code", 42, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Consignor Code
			moDBFields.Add(_CONName, "Consignor Number", 43, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Consignor Number
			moDBFields.Add(_ConRegNo, "Consignor Reg. No.", 44, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Consignor Reg. No.
			moDBFields.Add(_ConAdres, "Consignor Address", 45, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, EMPTYSTR, false, false); //Consignor Address
			moDBFields.Add(_ConCntPr, "Consignor Contact Person", 46, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Consignor Contact Person
			moDBFields.Add(_ConPh, "Consignor Phone", 47, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Consignor Phone
			moDBFields.Add(_ConFax, "Consignor Fax", 48, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Consignor Fax
			moDBFields.Add(_ConEmail, "Consignor Email", 49, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Consignor Email
			moDBFields.Add(_COECd, "Consignee Code", 50, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Consignee Code
			moDBFields.Add(_COEName, "Consignee Name", 51, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Consignee Name
			moDBFields.Add(_CoeRegNo, "Consignee Reg. No.", 52, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Consignee Reg. No.
			moDBFields.Add(_CoeAdres, "Consignee Address", 53, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, EMPTYSTR, false, false); //Consignee Address
			moDBFields.Add(_CoeCntPr, "Consignee Contact Person", 54, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Consignee Contact Person
			moDBFields.Add(_CoePh, "Consignee Phone", 55, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Consignee Phone
			moDBFields.Add(_CoeFax, "Consignee Fax", 56, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Consignee Fax
			moDBFields.Add(_CoeEmail, "Consignee Email", 57, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Consignee Email
			moDBFields.Add(_CARCd, "Carrier Code", 58, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Carrier Code
			moDBFields.Add(_CARName, "Carrier Name", 59, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Carrier Name
			moDBFields.Add(_GENCd, "Generator Code", 60, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Generator Code
			moDBFields.Add(_GENName, "Generator Name", 61, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Generator Name
			moDBFields.Add(_GenRegNo, "Generator Reg. No.", 62, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Generator Reg. No.
			moDBFields.Add(_GenAdres, "Generator Address", 63, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, EMPTYSTR, false, false); //Generator Address
			moDBFields.Add(_GenCntPr, "Generator Contact Person", 64, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Generator Contact Person
			moDBFields.Add(_GenPh, "Generator Phone", 65, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Generator Phone
			moDBFields.Add(_GenFax, "Generator Fax", 66, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Generator Fax
			moDBFields.Add(_GenEmail, "Generator Email", 67, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Generator Email
			moDBFields.Add(_DSPCd, "Disposal Site Code", 68, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Disposal Site Code
			moDBFields.Add(_DSPName, "Disposal Site Name", 69, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Disposal Site Name
			moDBFields.Add(_DOFRegNo, "Disp. Facility Reg. No.", 70, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Disp. Facility Reg. No.
			moDBFields.Add(_DOFAdres, "Disp. Facility Address", 71, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, EMPTYSTR, false, false); //Disp. Facility Address
			moDBFields.Add(_DOFCntPr, "Disp. Facility Contact Person", 72, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Disp. Facility Contact Person
			moDBFields.Add(_DOFPh, "Disp. Facility Phone", 73, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Disp. Facility Phone
			moDBFields.Add(_DOFFax, "Disp. Facility Fax", 74, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Disp. Facility Fax
			moDBFields.Add(_DOFEmail, "Disp. Facility Email", 75, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Disp. Facility Email
			moDBFields.Add(_DOPreAp, "Pre Approved", 76, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Pre Approved
			moDBFields.Add(_FDSCd, "Final Destination Code", 77, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Final Destination Code
			moDBFields.Add(_FDSName, "Final Destination Name", 78, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Final Destination Name
			moDBFields.Add(_FDTRegNo, "Final Dest. Reg. No.", 79, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Final Dest. Reg. No.
			moDBFields.Add(_FDTAdres, "Final Dest. Address", 80, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, EMPTYSTR, false, false); //Final Dest. Address
			moDBFields.Add(_FDTCntPr, "Final Dest. Contact Person", 81, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Final Dest. Contact Person
			moDBFields.Add(_FDTPh, "Final Dest. Phone", 82, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Final Dest. Phone
			moDBFields.Add(_FDTFax, "Final Dest. Fax", 83, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Final Dest. Fax
			moDBFields.Add(_FDTEmail, "Final Dest. Email", 84, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Final Dest. Email
			moDBFields.Add(_Multiple, "Multiple", 85, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Multiple
			moDBFields.Add(_AnxCd1, "Annex Code 1", 86, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Annex Code 1
			moDBFields.Add(_AnxDesc1, "Annex Description 1", 87, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Annex Description 1
			moDBFields.Add(_AnxCd2, "Annex Code 2", 88, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Annex Code 2
			moDBFields.Add(_AnxDesc2, "Annex Description 2", 89, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Annex Description 2
			moDBFields.Add(_AnxCd3, "Annex Code 3", 90, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Annex Code 3
			moDBFields.Add(_AnxDesc3, "Annex Description 3", 91, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Annex Description 3
			moDBFields.Add(_AnxCd4, "Annex Code 4", 92, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Annex Code 4
			moDBFields.Add(_AnxDesc4, "Annex Description 4", 93, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Annex Description 4
			moDBFields.Add(_AnxCd5, "Annex Code 5", 94, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Annex Code 5
			moDBFields.Add(_AnxDesc5, "Annex Description 5", 95, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Annex Description 5
			moDBFields.Add(_AnxCd6, "Annex Code 6", 96, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Annex Code 6
			moDBFields.Add(_AnxDesc6, "Annex Description 6", 97, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Annex Description 6
			moDBFields.Add(_AnxCd7, "Annex Code 7", 98, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Annex Code 7
			moDBFields.Add(_AnxDesc7, "Annex Description 7", 99, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Annex Description 7
			moDBFields.Add(_AnxCd8, "Annex Code 8", 100, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Annex Code 8
			moDBFields.Add(_AnxDesc8, "Annex Description 8", 101, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Annex Description 8
			moDBFields.Add(_AnxCd9, "Annex Code 9", 102, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Annex Code 9
			moDBFields.Add(_AnxDesc9, "Annex Description 9", 103, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Annex Description 9
			moDBFields.Add(_AnxCd10, "Annex Code 10", 104, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Annex Code 10
			moDBFields.Add(_AnxDesc10, "Annex Description 10", 105, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Annex Description 10
			moDBFields.Add(_WROrd, "WR Link Order", 106, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, false); //WR Link Order
			moDBFields.Add(_SpHDetails, "Sp.H. Details", 107, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Sp.H. Details

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_TFSNum());
            doBuildValidationString(doValidate_Type());
            doBuildValidationString(doValidate_Status());
            doBuildValidationString(doValidate_ShpType());
            doBuildValidationString(doValidate_OpType());
            doBuildValidationString(doValidate_SPreAp());
            doBuildValidationString(doValidate_IntShip());
            doBuildValidationString(doValidate_ActShip());
            doBuildValidationString(doValidate_IntQty());
            doBuildValidationString(doValidate_QtyT());
            doBuildValidationString(doValidate_QtyCMtr());
            doBuildValidationString(doValidate_SDtFrm());
            doBuildValidationString(doValidate_SDtTo());
            doBuildValidationString(doValidate_PackTyp());
            doBuildValidationString(doValidate_SpHandle());
            doBuildValidationString(doValidate_DCode());
            doBuildValidationString(doValidate_TechEmp());
            doBuildValidationString(doValidate_ExpRes());
            doBuildValidationString(doValidate_WasteCmp());
            doBuildValidationString(doValidate_PhyChtr());
            doBuildValidationString(doValidate_BaselCd());
            doBuildValidationString(doValidate_OECDCd());
            doBuildValidationString(doValidate_EWCCd());
            doBuildValidationString(doValidate_ExpNatCd());
            doBuildValidationString(doValidate_ImpNatCd());
            doBuildValidationString(doValidate_Other());
            doBuildValidationString(doValidate_YCode());
            doBuildValidationString(doValidate_HCode());
            doBuildValidationString(doValidate_UNNumber());
            doBuildValidationString(doValidate_UNClass());
            doBuildValidationString(doValidate_UNShipNm());
            doBuildValidationString(doValidate_CustCd());
            doBuildValidationString(doValidate_NOTCd());
            doBuildValidationString(doValidate_NOTName());
            doBuildValidationString(doValidate_NotRegNo());
            doBuildValidationString(doValidate_NotAdres());
            doBuildValidationString(doValidate_NotCntPr());
            doBuildValidationString(doValidate_NotPh());
            doBuildValidationString(doValidate_NotFax());
            doBuildValidationString(doValidate_NotEmail());
            doBuildValidationString(doValidate_CONCd());
            doBuildValidationString(doValidate_CONName());
            doBuildValidationString(doValidate_ConRegNo());
            doBuildValidationString(doValidate_ConAdres());
            doBuildValidationString(doValidate_ConCntPr());
            doBuildValidationString(doValidate_ConPh());
            doBuildValidationString(doValidate_ConFax());
            doBuildValidationString(doValidate_ConEmail());
            doBuildValidationString(doValidate_COECd());
            doBuildValidationString(doValidate_COEName());
            doBuildValidationString(doValidate_CoeRegNo());
            doBuildValidationString(doValidate_CoeAdres());
            doBuildValidationString(doValidate_CoeCntPr());
            doBuildValidationString(doValidate_CoePh());
            doBuildValidationString(doValidate_CoeFax());
            doBuildValidationString(doValidate_CoeEmail());
            doBuildValidationString(doValidate_CARCd());
            doBuildValidationString(doValidate_CARName());
            doBuildValidationString(doValidate_GENCd());
            doBuildValidationString(doValidate_GENName());
            doBuildValidationString(doValidate_GenRegNo());
            doBuildValidationString(doValidate_GenAdres());
            doBuildValidationString(doValidate_GenCntPr());
            doBuildValidationString(doValidate_GenPh());
            doBuildValidationString(doValidate_GenFax());
            doBuildValidationString(doValidate_GenEmail());
            doBuildValidationString(doValidate_DSPCd());
            doBuildValidationString(doValidate_DSPName());
            doBuildValidationString(doValidate_DOFRegNo());
            doBuildValidationString(doValidate_DOFAdres());
            doBuildValidationString(doValidate_DOFCntPr());
            doBuildValidationString(doValidate_DOFPh());
            doBuildValidationString(doValidate_DOFFax());
            doBuildValidationString(doValidate_DOFEmail());
            doBuildValidationString(doValidate_DOPreAp());
            doBuildValidationString(doValidate_FDSCd());
            doBuildValidationString(doValidate_FDSName());
            doBuildValidationString(doValidate_FDTRegNo());
            doBuildValidationString(doValidate_FDTAdres());
            doBuildValidationString(doValidate_FDTCntPr());
            doBuildValidationString(doValidate_FDTPh());
            doBuildValidationString(doValidate_FDTFax());
            doBuildValidationString(doValidate_FDTEmail());
            doBuildValidationString(doValidate_Multiple());
            doBuildValidationString(doValidate_AnxCd1());
            doBuildValidationString(doValidate_AnxDesc1());
            doBuildValidationString(doValidate_AnxCd2());
            doBuildValidationString(doValidate_AnxDesc2());
            doBuildValidationString(doValidate_AnxCd3());
            doBuildValidationString(doValidate_AnxDesc3());
            doBuildValidationString(doValidate_AnxCd4());
            doBuildValidationString(doValidate_AnxDesc4());
            doBuildValidationString(doValidate_AnxCd5());
            doBuildValidationString(doValidate_AnxDesc5());
            doBuildValidationString(doValidate_AnxCd6());
            doBuildValidationString(doValidate_AnxDesc6());
            doBuildValidationString(doValidate_AnxCd7());
            doBuildValidationString(doValidate_AnxDesc7());
            doBuildValidationString(doValidate_AnxCd8());
            doBuildValidationString(doValidate_AnxDesc8());
            doBuildValidationString(doValidate_AnxCd9());
            doBuildValidationString(doValidate_AnxDesc9());
            doBuildValidationString(doValidate_AnxCd10());
            doBuildValidationString(doValidate_AnxDesc10());
            doBuildValidationString(doValidate_WROrd());
            doBuildValidationString(doValidate_SpHDetails());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_TFSNum)) return doValidate_TFSNum(oValue);
            if (sFieldName.Equals(_Type)) return doValidate_Type(oValue);
            if (sFieldName.Equals(_Status)) return doValidate_Status(oValue);
            if (sFieldName.Equals(_ShpType)) return doValidate_ShpType(oValue);
            if (sFieldName.Equals(_OpType)) return doValidate_OpType(oValue);
            if (sFieldName.Equals(_SPreAp)) return doValidate_SPreAp(oValue);
            if (sFieldName.Equals(_IntShip)) return doValidate_IntShip(oValue);
            if (sFieldName.Equals(_ActShip)) return doValidate_ActShip(oValue);
            if (sFieldName.Equals(_IntQty)) return doValidate_IntQty(oValue);
            if (sFieldName.Equals(_QtyT)) return doValidate_QtyT(oValue);
            if (sFieldName.Equals(_QtyCMtr)) return doValidate_QtyCMtr(oValue);
            if (sFieldName.Equals(_SDtFrm)) return doValidate_SDtFrm(oValue);
            if (sFieldName.Equals(_SDtTo)) return doValidate_SDtTo(oValue);
            if (sFieldName.Equals(_PackTyp)) return doValidate_PackTyp(oValue);
            if (sFieldName.Equals(_SpHandle)) return doValidate_SpHandle(oValue);
            if (sFieldName.Equals(_DCode)) return doValidate_DCode(oValue);
            if (sFieldName.Equals(_TechEmp)) return doValidate_TechEmp(oValue);
            if (sFieldName.Equals(_ExpRes)) return doValidate_ExpRes(oValue);
            if (sFieldName.Equals(_WasteCmp)) return doValidate_WasteCmp(oValue);
            if (sFieldName.Equals(_PhyChtr)) return doValidate_PhyChtr(oValue);
            if (sFieldName.Equals(_BaselCd)) return doValidate_BaselCd(oValue);
            if (sFieldName.Equals(_OECDCd)) return doValidate_OECDCd(oValue);
            if (sFieldName.Equals(_EWCCd)) return doValidate_EWCCd(oValue);
            if (sFieldName.Equals(_ExpNatCd)) return doValidate_ExpNatCd(oValue);
            if (sFieldName.Equals(_ImpNatCd)) return doValidate_ImpNatCd(oValue);
            if (sFieldName.Equals(_Other)) return doValidate_Other(oValue);
            if (sFieldName.Equals(_YCode)) return doValidate_YCode(oValue);
            if (sFieldName.Equals(_HCode)) return doValidate_HCode(oValue);
            if (sFieldName.Equals(_UNNumber)) return doValidate_UNNumber(oValue);
            if (sFieldName.Equals(_UNClass)) return doValidate_UNClass(oValue);
            if (sFieldName.Equals(_UNShipNm)) return doValidate_UNShipNm(oValue);
            if (sFieldName.Equals(_CustCd)) return doValidate_CustCd(oValue);
            if (sFieldName.Equals(_NOTCd)) return doValidate_NOTCd(oValue);
            if (sFieldName.Equals(_NOTName)) return doValidate_NOTName(oValue);
            if (sFieldName.Equals(_NotRegNo)) return doValidate_NotRegNo(oValue);
            if (sFieldName.Equals(_NotAdres)) return doValidate_NotAdres(oValue);
            if (sFieldName.Equals(_NotCntPr)) return doValidate_NotCntPr(oValue);
            if (sFieldName.Equals(_NotPh)) return doValidate_NotPh(oValue);
            if (sFieldName.Equals(_NotFax)) return doValidate_NotFax(oValue);
            if (sFieldName.Equals(_NotEmail)) return doValidate_NotEmail(oValue);
            if (sFieldName.Equals(_CONCd)) return doValidate_CONCd(oValue);
            if (sFieldName.Equals(_CONName)) return doValidate_CONName(oValue);
            if (sFieldName.Equals(_ConRegNo)) return doValidate_ConRegNo(oValue);
            if (sFieldName.Equals(_ConAdres)) return doValidate_ConAdres(oValue);
            if (sFieldName.Equals(_ConCntPr)) return doValidate_ConCntPr(oValue);
            if (sFieldName.Equals(_ConPh)) return doValidate_ConPh(oValue);
            if (sFieldName.Equals(_ConFax)) return doValidate_ConFax(oValue);
            if (sFieldName.Equals(_ConEmail)) return doValidate_ConEmail(oValue);
            if (sFieldName.Equals(_COECd)) return doValidate_COECd(oValue);
            if (sFieldName.Equals(_COEName)) return doValidate_COEName(oValue);
            if (sFieldName.Equals(_CoeRegNo)) return doValidate_CoeRegNo(oValue);
            if (sFieldName.Equals(_CoeAdres)) return doValidate_CoeAdres(oValue);
            if (sFieldName.Equals(_CoeCntPr)) return doValidate_CoeCntPr(oValue);
            if (sFieldName.Equals(_CoePh)) return doValidate_CoePh(oValue);
            if (sFieldName.Equals(_CoeFax)) return doValidate_CoeFax(oValue);
            if (sFieldName.Equals(_CoeEmail)) return doValidate_CoeEmail(oValue);
            if (sFieldName.Equals(_CARCd)) return doValidate_CARCd(oValue);
            if (sFieldName.Equals(_CARName)) return doValidate_CARName(oValue);
            if (sFieldName.Equals(_GENCd)) return doValidate_GENCd(oValue);
            if (sFieldName.Equals(_GENName)) return doValidate_GENName(oValue);
            if (sFieldName.Equals(_GenRegNo)) return doValidate_GenRegNo(oValue);
            if (sFieldName.Equals(_GenAdres)) return doValidate_GenAdres(oValue);
            if (sFieldName.Equals(_GenCntPr)) return doValidate_GenCntPr(oValue);
            if (sFieldName.Equals(_GenPh)) return doValidate_GenPh(oValue);
            if (sFieldName.Equals(_GenFax)) return doValidate_GenFax(oValue);
            if (sFieldName.Equals(_GenEmail)) return doValidate_GenEmail(oValue);
            if (sFieldName.Equals(_DSPCd)) return doValidate_DSPCd(oValue);
            if (sFieldName.Equals(_DSPName)) return doValidate_DSPName(oValue);
            if (sFieldName.Equals(_DOFRegNo)) return doValidate_DOFRegNo(oValue);
            if (sFieldName.Equals(_DOFAdres)) return doValidate_DOFAdres(oValue);
            if (sFieldName.Equals(_DOFCntPr)) return doValidate_DOFCntPr(oValue);
            if (sFieldName.Equals(_DOFPh)) return doValidate_DOFPh(oValue);
            if (sFieldName.Equals(_DOFFax)) return doValidate_DOFFax(oValue);
            if (sFieldName.Equals(_DOFEmail)) return doValidate_DOFEmail(oValue);
            if (sFieldName.Equals(_DOPreAp)) return doValidate_DOPreAp(oValue);
            if (sFieldName.Equals(_FDSCd)) return doValidate_FDSCd(oValue);
            if (sFieldName.Equals(_FDSName)) return doValidate_FDSName(oValue);
            if (sFieldName.Equals(_FDTRegNo)) return doValidate_FDTRegNo(oValue);
            if (sFieldName.Equals(_FDTAdres)) return doValidate_FDTAdres(oValue);
            if (sFieldName.Equals(_FDTCntPr)) return doValidate_FDTCntPr(oValue);
            if (sFieldName.Equals(_FDTPh)) return doValidate_FDTPh(oValue);
            if (sFieldName.Equals(_FDTFax)) return doValidate_FDTFax(oValue);
            if (sFieldName.Equals(_FDTEmail)) return doValidate_FDTEmail(oValue);
            if (sFieldName.Equals(_Multiple)) return doValidate_Multiple(oValue);
            if (sFieldName.Equals(_AnxCd1)) return doValidate_AnxCd1(oValue);
            if (sFieldName.Equals(_AnxDesc1)) return doValidate_AnxDesc1(oValue);
            if (sFieldName.Equals(_AnxCd2)) return doValidate_AnxCd2(oValue);
            if (sFieldName.Equals(_AnxDesc2)) return doValidate_AnxDesc2(oValue);
            if (sFieldName.Equals(_AnxCd3)) return doValidate_AnxCd3(oValue);
            if (sFieldName.Equals(_AnxDesc3)) return doValidate_AnxDesc3(oValue);
            if (sFieldName.Equals(_AnxCd4)) return doValidate_AnxCd4(oValue);
            if (sFieldName.Equals(_AnxDesc4)) return doValidate_AnxDesc4(oValue);
            if (sFieldName.Equals(_AnxCd5)) return doValidate_AnxCd5(oValue);
            if (sFieldName.Equals(_AnxDesc5)) return doValidate_AnxDesc5(oValue);
            if (sFieldName.Equals(_AnxCd6)) return doValidate_AnxCd6(oValue);
            if (sFieldName.Equals(_AnxDesc6)) return doValidate_AnxDesc6(oValue);
            if (sFieldName.Equals(_AnxCd7)) return doValidate_AnxCd7(oValue);
            if (sFieldName.Equals(_AnxDesc7)) return doValidate_AnxDesc7(oValue);
            if (sFieldName.Equals(_AnxCd8)) return doValidate_AnxCd8(oValue);
            if (sFieldName.Equals(_AnxDesc8)) return doValidate_AnxDesc8(oValue);
            if (sFieldName.Equals(_AnxCd9)) return doValidate_AnxCd9(oValue);
            if (sFieldName.Equals(_AnxDesc9)) return doValidate_AnxDesc9(oValue);
            if (sFieldName.Equals(_AnxCd10)) return doValidate_AnxCd10(oValue);
            if (sFieldName.Equals(_AnxDesc10)) return doValidate_AnxDesc10(oValue);
            if (sFieldName.Equals(_WROrd)) return doValidate_WROrd(oValue);
            if (sFieldName.Equals(_SpHDetails)) return doValidate_SpHDetails(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_TFSNum Field to the Form Item.
		 */
		public void doLink_TFSNum(string sControlName){
			moLinker.doLinkDataToControl(_TFSNum, sControlName);
		}
		/**
		 * Link the U_Type Field to the Form Item.
		 */
		public void doLink_Type(string sControlName){
			moLinker.doLinkDataToControl(_Type, sControlName);
		}
		/**
		 * Link the U_Status Field to the Form Item.
		 */
		public void doLink_Status(string sControlName){
			moLinker.doLinkDataToControl(_Status, sControlName);
		}
		/**
		 * Link the U_ShpType Field to the Form Item.
		 */
		public void doLink_ShpType(string sControlName){
			moLinker.doLinkDataToControl(_ShpType, sControlName);
		}
		/**
		 * Link the U_OpType Field to the Form Item.
		 */
		public void doLink_OpType(string sControlName){
			moLinker.doLinkDataToControl(_OpType, sControlName);
		}
		/**
		 * Link the U_SPreAp Field to the Form Item.
		 */
		public void doLink_SPreAp(string sControlName){
			moLinker.doLinkDataToControl(_SPreAp, sControlName);
		}
		/**
		 * Link the U_IntShip Field to the Form Item.
		 */
		public void doLink_IntShip(string sControlName){
			moLinker.doLinkDataToControl(_IntShip, sControlName);
		}
		/**
		 * Link the U_ActShip Field to the Form Item.
		 */
		public void doLink_ActShip(string sControlName){
			moLinker.doLinkDataToControl(_ActShip, sControlName);
		}
		/**
		 * Link the U_IntQty Field to the Form Item.
		 */
		public void doLink_IntQty(string sControlName){
			moLinker.doLinkDataToControl(_IntQty, sControlName);
		}
		/**
		 * Link the U_QtyT Field to the Form Item.
		 */
		public void doLink_QtyT(string sControlName){
			moLinker.doLinkDataToControl(_QtyT, sControlName);
		}
		/**
		 * Link the U_QtyCMtr Field to the Form Item.
		 */
		public void doLink_QtyCMtr(string sControlName){
			moLinker.doLinkDataToControl(_QtyCMtr, sControlName);
		}
		/**
		 * Link the U_SDtFrm Field to the Form Item.
		 */
		public void doLink_SDtFrm(string sControlName){
			moLinker.doLinkDataToControl(_SDtFrm, sControlName);
		}
		/**
		 * Link the U_SDtTo Field to the Form Item.
		 */
		public void doLink_SDtTo(string sControlName){
			moLinker.doLinkDataToControl(_SDtTo, sControlName);
		}
		/**
		 * Link the U_PackTyp Field to the Form Item.
		 */
		public void doLink_PackTyp(string sControlName){
			moLinker.doLinkDataToControl(_PackTyp, sControlName);
		}
		/**
		 * Link the U_SpHandle Field to the Form Item.
		 */
		public void doLink_SpHandle(string sControlName){
			moLinker.doLinkDataToControl(_SpHandle, sControlName);
		}
		/**
		 * Link the U_DCode Field to the Form Item.
		 */
		public void doLink_DCode(string sControlName){
			moLinker.doLinkDataToControl(_DCode, sControlName);
		}
		/**
		 * Link the U_TechEmp Field to the Form Item.
		 */
		public void doLink_TechEmp(string sControlName){
			moLinker.doLinkDataToControl(_TechEmp, sControlName);
		}
		/**
		 * Link the U_ExpRes Field to the Form Item.
		 */
		public void doLink_ExpRes(string sControlName){
			moLinker.doLinkDataToControl(_ExpRes, sControlName);
		}
		/**
		 * Link the U_WasteCmp Field to the Form Item.
		 */
		public void doLink_WasteCmp(string sControlName){
			moLinker.doLinkDataToControl(_WasteCmp, sControlName);
		}
		/**
		 * Link the U_PhyChtr Field to the Form Item.
		 */
		public void doLink_PhyChtr(string sControlName){
			moLinker.doLinkDataToControl(_PhyChtr, sControlName);
		}
		/**
		 * Link the U_BaselCd Field to the Form Item.
		 */
		public void doLink_BaselCd(string sControlName){
			moLinker.doLinkDataToControl(_BaselCd, sControlName);
		}
		/**
		 * Link the U_OECDCd Field to the Form Item.
		 */
		public void doLink_OECDCd(string sControlName){
			moLinker.doLinkDataToControl(_OECDCd, sControlName);
		}
		/**
		 * Link the U_EWCCd Field to the Form Item.
		 */
		public void doLink_EWCCd(string sControlName){
			moLinker.doLinkDataToControl(_EWCCd, sControlName);
		}
		/**
		 * Link the U_ExpNatCd Field to the Form Item.
		 */
		public void doLink_ExpNatCd(string sControlName){
			moLinker.doLinkDataToControl(_ExpNatCd, sControlName);
		}
		/**
		 * Link the U_ImpNatCd Field to the Form Item.
		 */
		public void doLink_ImpNatCd(string sControlName){
			moLinker.doLinkDataToControl(_ImpNatCd, sControlName);
		}
		/**
		 * Link the U_Other Field to the Form Item.
		 */
		public void doLink_Other(string sControlName){
			moLinker.doLinkDataToControl(_Other, sControlName);
		}
		/**
		 * Link the U_YCode Field to the Form Item.
		 */
		public void doLink_YCode(string sControlName){
			moLinker.doLinkDataToControl(_YCode, sControlName);
		}
		/**
		 * Link the U_HCode Field to the Form Item.
		 */
		public void doLink_HCode(string sControlName){
			moLinker.doLinkDataToControl(_HCode, sControlName);
		}
		/**
		 * Link the U_UNNumber Field to the Form Item.
		 */
		public void doLink_UNNumber(string sControlName){
			moLinker.doLinkDataToControl(_UNNumber, sControlName);
		}
		/**
		 * Link the U_UNClass Field to the Form Item.
		 */
		public void doLink_UNClass(string sControlName){
			moLinker.doLinkDataToControl(_UNClass, sControlName);
		}
		/**
		 * Link the U_UNShipNm Field to the Form Item.
		 */
		public void doLink_UNShipNm(string sControlName){
			moLinker.doLinkDataToControl(_UNShipNm, sControlName);
		}
		/**
		 * Link the U_CustCd Field to the Form Item.
		 */
		public void doLink_CustCd(string sControlName){
			moLinker.doLinkDataToControl(_CustCd, sControlName);
		}
		/**
		 * Link the U_NOTCd Field to the Form Item.
		 */
		public void doLink_NOTCd(string sControlName){
			moLinker.doLinkDataToControl(_NOTCd, sControlName);
		}
		/**
		 * Link the U_NOTName Field to the Form Item.
		 */
		public void doLink_NOTName(string sControlName){
			moLinker.doLinkDataToControl(_NOTName, sControlName);
		}
		/**
		 * Link the U_NotRegNo Field to the Form Item.
		 */
		public void doLink_NotRegNo(string sControlName){
			moLinker.doLinkDataToControl(_NotRegNo, sControlName);
		}
		/**
		 * Link the U_NotAdres Field to the Form Item.
		 */
		public void doLink_NotAdres(string sControlName){
			moLinker.doLinkDataToControl(_NotAdres, sControlName);
		}
		/**
		 * Link the U_NotCntPr Field to the Form Item.
		 */
		public void doLink_NotCntPr(string sControlName){
			moLinker.doLinkDataToControl(_NotCntPr, sControlName);
		}
		/**
		 * Link the U_NotPh Field to the Form Item.
		 */
		public void doLink_NotPh(string sControlName){
			moLinker.doLinkDataToControl(_NotPh, sControlName);
		}
		/**
		 * Link the U_NotFax Field to the Form Item.
		 */
		public void doLink_NotFax(string sControlName){
			moLinker.doLinkDataToControl(_NotFax, sControlName);
		}
		/**
		 * Link the U_NotEmail Field to the Form Item.
		 */
		public void doLink_NotEmail(string sControlName){
			moLinker.doLinkDataToControl(_NotEmail, sControlName);
		}
		/**
		 * Link the U_CONCd Field to the Form Item.
		 */
		public void doLink_CONCd(string sControlName){
			moLinker.doLinkDataToControl(_CONCd, sControlName);
		}
		/**
		 * Link the U_CONName Field to the Form Item.
		 */
		public void doLink_CONName(string sControlName){
			moLinker.doLinkDataToControl(_CONName, sControlName);
		}
		/**
		 * Link the U_ConRegNo Field to the Form Item.
		 */
		public void doLink_ConRegNo(string sControlName){
			moLinker.doLinkDataToControl(_ConRegNo, sControlName);
		}
		/**
		 * Link the U_ConAdres Field to the Form Item.
		 */
		public void doLink_ConAdres(string sControlName){
			moLinker.doLinkDataToControl(_ConAdres, sControlName);
		}
		/**
		 * Link the U_ConCntPr Field to the Form Item.
		 */
		public void doLink_ConCntPr(string sControlName){
			moLinker.doLinkDataToControl(_ConCntPr, sControlName);
		}
		/**
		 * Link the U_ConPh Field to the Form Item.
		 */
		public void doLink_ConPh(string sControlName){
			moLinker.doLinkDataToControl(_ConPh, sControlName);
		}
		/**
		 * Link the U_ConFax Field to the Form Item.
		 */
		public void doLink_ConFax(string sControlName){
			moLinker.doLinkDataToControl(_ConFax, sControlName);
		}
		/**
		 * Link the U_ConEmail Field to the Form Item.
		 */
		public void doLink_ConEmail(string sControlName){
			moLinker.doLinkDataToControl(_ConEmail, sControlName);
		}
		/**
		 * Link the U_COECd Field to the Form Item.
		 */
		public void doLink_COECd(string sControlName){
			moLinker.doLinkDataToControl(_COECd, sControlName);
		}
		/**
		 * Link the U_COEName Field to the Form Item.
		 */
		public void doLink_COEName(string sControlName){
			moLinker.doLinkDataToControl(_COEName, sControlName);
		}
		/**
		 * Link the U_CoeRegNo Field to the Form Item.
		 */
		public void doLink_CoeRegNo(string sControlName){
			moLinker.doLinkDataToControl(_CoeRegNo, sControlName);
		}
		/**
		 * Link the U_CoeAdres Field to the Form Item.
		 */
		public void doLink_CoeAdres(string sControlName){
			moLinker.doLinkDataToControl(_CoeAdres, sControlName);
		}
		/**
		 * Link the U_CoeCntPr Field to the Form Item.
		 */
		public void doLink_CoeCntPr(string sControlName){
			moLinker.doLinkDataToControl(_CoeCntPr, sControlName);
		}
		/**
		 * Link the U_CoePh Field to the Form Item.
		 */
		public void doLink_CoePh(string sControlName){
			moLinker.doLinkDataToControl(_CoePh, sControlName);
		}
		/**
		 * Link the U_CoeFax Field to the Form Item.
		 */
		public void doLink_CoeFax(string sControlName){
			moLinker.doLinkDataToControl(_CoeFax, sControlName);
		}
		/**
		 * Link the U_CoeEmail Field to the Form Item.
		 */
		public void doLink_CoeEmail(string sControlName){
			moLinker.doLinkDataToControl(_CoeEmail, sControlName);
		}
		/**
		 * Link the U_CARCd Field to the Form Item.
		 */
		public void doLink_CARCd(string sControlName){
			moLinker.doLinkDataToControl(_CARCd, sControlName);
		}
		/**
		 * Link the U_CARName Field to the Form Item.
		 */
		public void doLink_CARName(string sControlName){
			moLinker.doLinkDataToControl(_CARName, sControlName);
		}
		/**
		 * Link the U_GENCd Field to the Form Item.
		 */
		public void doLink_GENCd(string sControlName){
			moLinker.doLinkDataToControl(_GENCd, sControlName);
		}
		/**
		 * Link the U_GENName Field to the Form Item.
		 */
		public void doLink_GENName(string sControlName){
			moLinker.doLinkDataToControl(_GENName, sControlName);
		}
		/**
		 * Link the U_GenRegNo Field to the Form Item.
		 */
		public void doLink_GenRegNo(string sControlName){
			moLinker.doLinkDataToControl(_GenRegNo, sControlName);
		}
		/**
		 * Link the U_GenAdres Field to the Form Item.
		 */
		public void doLink_GenAdres(string sControlName){
			moLinker.doLinkDataToControl(_GenAdres, sControlName);
		}
		/**
		 * Link the U_GenCntPr Field to the Form Item.
		 */
		public void doLink_GenCntPr(string sControlName){
			moLinker.doLinkDataToControl(_GenCntPr, sControlName);
		}
		/**
		 * Link the U_GenPh Field to the Form Item.
		 */
		public void doLink_GenPh(string sControlName){
			moLinker.doLinkDataToControl(_GenPh, sControlName);
		}
		/**
		 * Link the U_GenFax Field to the Form Item.
		 */
		public void doLink_GenFax(string sControlName){
			moLinker.doLinkDataToControl(_GenFax, sControlName);
		}
		/**
		 * Link the U_GenEmail Field to the Form Item.
		 */
		public void doLink_GenEmail(string sControlName){
			moLinker.doLinkDataToControl(_GenEmail, sControlName);
		}
		/**
		 * Link the U_DSPCd Field to the Form Item.
		 */
		public void doLink_DSPCd(string sControlName){
			moLinker.doLinkDataToControl(_DSPCd, sControlName);
		}
		/**
		 * Link the U_DSPName Field to the Form Item.
		 */
		public void doLink_DSPName(string sControlName){
			moLinker.doLinkDataToControl(_DSPName, sControlName);
		}
		/**
		 * Link the U_DOFRegNo Field to the Form Item.
		 */
		public void doLink_DOFRegNo(string sControlName){
			moLinker.doLinkDataToControl(_DOFRegNo, sControlName);
		}
		/**
		 * Link the U_DOFAdres Field to the Form Item.
		 */
		public void doLink_DOFAdres(string sControlName){
			moLinker.doLinkDataToControl(_DOFAdres, sControlName);
		}
		/**
		 * Link the U_DOFCntPr Field to the Form Item.
		 */
		public void doLink_DOFCntPr(string sControlName){
			moLinker.doLinkDataToControl(_DOFCntPr, sControlName);
		}
		/**
		 * Link the U_DOFPh Field to the Form Item.
		 */
		public void doLink_DOFPh(string sControlName){
			moLinker.doLinkDataToControl(_DOFPh, sControlName);
		}
		/**
		 * Link the U_DOFFax Field to the Form Item.
		 */
		public void doLink_DOFFax(string sControlName){
			moLinker.doLinkDataToControl(_DOFFax, sControlName);
		}
		/**
		 * Link the U_DOFEmail Field to the Form Item.
		 */
		public void doLink_DOFEmail(string sControlName){
			moLinker.doLinkDataToControl(_DOFEmail, sControlName);
		}
		/**
		 * Link the U_DOPreAp Field to the Form Item.
		 */
		public void doLink_DOPreAp(string sControlName){
			moLinker.doLinkDataToControl(_DOPreAp, sControlName);
		}
		/**
		 * Link the U_FDSCd Field to the Form Item.
		 */
		public void doLink_FDSCd(string sControlName){
			moLinker.doLinkDataToControl(_FDSCd, sControlName);
		}
		/**
		 * Link the U_FDSName Field to the Form Item.
		 */
		public void doLink_FDSName(string sControlName){
			moLinker.doLinkDataToControl(_FDSName, sControlName);
		}
		/**
		 * Link the U_FDTRegNo Field to the Form Item.
		 */
		public void doLink_FDTRegNo(string sControlName){
			moLinker.doLinkDataToControl(_FDTRegNo, sControlName);
		}
		/**
		 * Link the U_FDTAdres Field to the Form Item.
		 */
		public void doLink_FDTAdres(string sControlName){
			moLinker.doLinkDataToControl(_FDTAdres, sControlName);
		}
		/**
		 * Link the U_FDTCntPr Field to the Form Item.
		 */
		public void doLink_FDTCntPr(string sControlName){
			moLinker.doLinkDataToControl(_FDTCntPr, sControlName);
		}
		/**
		 * Link the U_FDTPh Field to the Form Item.
		 */
		public void doLink_FDTPh(string sControlName){
			moLinker.doLinkDataToControl(_FDTPh, sControlName);
		}
		/**
		 * Link the U_FDTFax Field to the Form Item.
		 */
		public void doLink_FDTFax(string sControlName){
			moLinker.doLinkDataToControl(_FDTFax, sControlName);
		}
		/**
		 * Link the U_FDTEmail Field to the Form Item.
		 */
		public void doLink_FDTEmail(string sControlName){
			moLinker.doLinkDataToControl(_FDTEmail, sControlName);
		}
		/**
		 * Link the U_Multiple Field to the Form Item.
		 */
		public void doLink_Multiple(string sControlName){
			moLinker.doLinkDataToControl(_Multiple, sControlName);
		}
		/**
		 * Link the U_AnxCd1 Field to the Form Item.
		 */
		public void doLink_AnxCd1(string sControlName){
			moLinker.doLinkDataToControl(_AnxCd1, sControlName);
		}
		/**
		 * Link the U_AnxDesc1 Field to the Form Item.
		 */
		public void doLink_AnxDesc1(string sControlName){
			moLinker.doLinkDataToControl(_AnxDesc1, sControlName);
		}
		/**
		 * Link the U_AnxCd2 Field to the Form Item.
		 */
		public void doLink_AnxCd2(string sControlName){
			moLinker.doLinkDataToControl(_AnxCd2, sControlName);
		}
		/**
		 * Link the U_AnxDesc2 Field to the Form Item.
		 */
		public void doLink_AnxDesc2(string sControlName){
			moLinker.doLinkDataToControl(_AnxDesc2, sControlName);
		}
		/**
		 * Link the U_AnxCd3 Field to the Form Item.
		 */
		public void doLink_AnxCd3(string sControlName){
			moLinker.doLinkDataToControl(_AnxCd3, sControlName);
		}
		/**
		 * Link the U_AnxDesc3 Field to the Form Item.
		 */
		public void doLink_AnxDesc3(string sControlName){
			moLinker.doLinkDataToControl(_AnxDesc3, sControlName);
		}
		/**
		 * Link the U_AnxCd4 Field to the Form Item.
		 */
		public void doLink_AnxCd4(string sControlName){
			moLinker.doLinkDataToControl(_AnxCd4, sControlName);
		}
		/**
		 * Link the U_AnxDesc4 Field to the Form Item.
		 */
		public void doLink_AnxDesc4(string sControlName){
			moLinker.doLinkDataToControl(_AnxDesc4, sControlName);
		}
		/**
		 * Link the U_AnxCd5 Field to the Form Item.
		 */
		public void doLink_AnxCd5(string sControlName){
			moLinker.doLinkDataToControl(_AnxCd5, sControlName);
		}
		/**
		 * Link the U_AnxDesc5 Field to the Form Item.
		 */
		public void doLink_AnxDesc5(string sControlName){
			moLinker.doLinkDataToControl(_AnxDesc5, sControlName);
		}
		/**
		 * Link the U_AnxCd6 Field to the Form Item.
		 */
		public void doLink_AnxCd6(string sControlName){
			moLinker.doLinkDataToControl(_AnxCd6, sControlName);
		}
		/**
		 * Link the U_AnxDesc6 Field to the Form Item.
		 */
		public void doLink_AnxDesc6(string sControlName){
			moLinker.doLinkDataToControl(_AnxDesc6, sControlName);
		}
		/**
		 * Link the U_AnxCd7 Field to the Form Item.
		 */
		public void doLink_AnxCd7(string sControlName){
			moLinker.doLinkDataToControl(_AnxCd7, sControlName);
		}
		/**
		 * Link the U_AnxDesc7 Field to the Form Item.
		 */
		public void doLink_AnxDesc7(string sControlName){
			moLinker.doLinkDataToControl(_AnxDesc7, sControlName);
		}
		/**
		 * Link the U_AnxCd8 Field to the Form Item.
		 */
		public void doLink_AnxCd8(string sControlName){
			moLinker.doLinkDataToControl(_AnxCd8, sControlName);
		}
		/**
		 * Link the U_AnxDesc8 Field to the Form Item.
		 */
		public void doLink_AnxDesc8(string sControlName){
			moLinker.doLinkDataToControl(_AnxDesc8, sControlName);
		}
		/**
		 * Link the U_AnxCd9 Field to the Form Item.
		 */
		public void doLink_AnxCd9(string sControlName){
			moLinker.doLinkDataToControl(_AnxCd9, sControlName);
		}
		/**
		 * Link the U_AnxDesc9 Field to the Form Item.
		 */
		public void doLink_AnxDesc9(string sControlName){
			moLinker.doLinkDataToControl(_AnxDesc9, sControlName);
		}
		/**
		 * Link the U_AnxCd10 Field to the Form Item.
		 */
		public void doLink_AnxCd10(string sControlName){
			moLinker.doLinkDataToControl(_AnxCd10, sControlName);
		}
		/**
		 * Link the U_AnxDesc10 Field to the Form Item.
		 */
		public void doLink_AnxDesc10(string sControlName){
			moLinker.doLinkDataToControl(_AnxDesc10, sControlName);
		}
		/**
		 * Link the U_WROrd Field to the Form Item.
		 */
		public void doLink_WROrd(string sControlName){
			moLinker.doLinkDataToControl(_WROrd, sControlName);
		}
		/**
		 * Link the U_SpHDetails Field to the Form Item.
		 */
		public void doLink_SpHDetails(string sControlName){
			moLinker.doLinkDataToControl(_SpHDetails, sControlName);
		}
#endregion

	}
}
