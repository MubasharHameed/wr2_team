/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 06/07/2015 17:11:49
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base
{
    [Serializable]
    public class IDH_POSTCODE : com.idh.dbObjects.DBBase
    {

        //private Linker moLinker = null;
        protected IDHAddOns.idh.forms.Base moIDHForm;

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX
        {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_POSTCODE()
            : base("@IDH_POSTCODE")
        {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_POSTCODE(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oForm, "@IDH_POSTCODE")
        {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
		* Table name
		*/
        public readonly static string TableName = "@IDH_POSTCODE";

        /**
         * Decription: Code
         * Mandatory: tYes
         * Name: Code
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Code = "Code";
        public string Code
        {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code()
        {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue)
        {
            return base.doValidation(_Code, oValue);
        }
        /**
         * Decription: Name
         * Mandatory: tYes
         * Name: Name
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Name = "Name";
        public string Name
        {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name()
        {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue)
        {
            return base.doValidation(_Name, oValue);
        }
        /**
         * Decription: ID
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: True
         * Name: U_ID
         * Size: 200
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ID = "U_ID";
        public string U_ID
        {
            get
            {
                return getValueAsString(_ID);
            }
            set { setValue(_ID, value); }
        }
        public string doValidate_ID()
        {
            return doValidate_ID(U_ID);
        }
        public virtual string doValidate_ID(object oValue)
        {
            return base.doValidation(_ID, oValue);
        }

        /**
         * Decription: Text
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Text
         * Size: 254
         * Type: db_Alpha
         * CType: string
         * SubType: st_Address
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Text = "U_Text";
        public string U_Text
        {
            get
            {
                return getValueAsString(_Text);
            }
            set { setValue(_Text, value); }
        }
        public string doValidate_Text()
        {
            return doValidate_Text(U_Text);
        }
        public virtual string doValidate_Text(object oValue)
        {
            return base.doValidation(_Text, oValue);
        }

        /**
         * Decription: Highlight
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Highlight
         * Size: 200
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Highlight = "U_Highlight";
        public string U_Highlight
        {
            get
            {
                return getValueAsString(_Highlight);
            }
            set { setValue(_Highlight, value); }
        }
        public string doValidate_Highlight()
        {
            return doValidate_Highlight(U_Highlight);
        }
        public virtual string doValidate_Highlight(object oValue)
        {
            return base.doValidation(_Highlight, oValue);
        }

        /**
         * Decription: Cursor
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Cursor
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Cursor = "U_Cursor";
        public int U_Cursor
        {
            get
            {
                return getValueAsInt(_Cursor);
            }
            set { setValue(_Cursor, value); }
        }
        public string doValidate_Cursor()
        {
            return doValidate_Cursor(U_Cursor);
        }
        public virtual string doValidate_Cursor(object oValue)
        {
            return base.doValidation(_Cursor, oValue);
        }

        /**
         * Decription: Description
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Description
         * Size: 200
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Description = "U_Description";
        public string U_Description
        {
            get
            {
                return getValueAsString(_Description);
            }
            set { setValue(_Description, value); }
        }
        public string doValidate_Description()
        {
            return doValidate_Description(U_Description);
        }
        public virtual string doValidate_Description(object oValue)
        {
            return base.doValidation(_Description, oValue);
        }

        /**
         * Decription: Next
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Next
         * Size: 50
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Next = "U_Next";
        public string U_Next
        {
            get
            {
                return getValueAsString(_Next);
            }
            set { setValue(_Next, value); }
        }
        public string doValidate_Next()
        {
            return doValidate_Next(U_Next);
        }
        public virtual string doValidate_Next(object oValue)
        {
            return base.doValidation(_Next, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo()
        {
            if (moDBFields == null)
                moDBFields = new DBFields(8);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_ID, "ID", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, true); //ID
            moDBFields.Add(_Text, "Text", 3, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000, EMPTYSTR, false, false); //Text
            moDBFields.Add(_Highlight, "Highlight", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Highlight
            moDBFields.Add(_Cursor, "Cursor", 5, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Cursor
            moDBFields.Add(_Description, "Description", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Description
            moDBFields.Add(_Next, "Next", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Next

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation()
        {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_ID());
            doBuildValidationString(doValidate_Text());
            doBuildValidationString(doValidate_Highlight());
            doBuildValidationString(doValidate_Cursor());
            doBuildValidationString(doValidate_Description());
            doBuildValidationString(doValidate_Next());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue)
        {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_ID)) return doValidate_ID(oValue);
            if (sFieldName.Equals(_Text)) return doValidate_Text(oValue);
            if (sFieldName.Equals(_Highlight)) return doValidate_Highlight(oValue);
            if (sFieldName.Equals(_Cursor)) return doValidate_Cursor(oValue);
            if (sFieldName.Equals(_Description)) return doValidate_Description(oValue);
            if (sFieldName.Equals(_Next)) return doValidate_Next(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName)
        {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
         * Link the Name Field to the Form Item.
         */
        public void doLink_Name(string sControlName)
        {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
         * Link the U_ID Field to the Form Item.
         */
        public void doLink_ID(string sControlName)
        {
            moLinker.doLinkDataToControl(_ID, sControlName);
        }
        /**
         * Link the U_Text Field to the Form Item.
         */
        public void doLink_Text(string sControlName)
        {
            moLinker.doLinkDataToControl(_Text, sControlName);
        }
        /**
         * Link the U_Highlight Field to the Form Item.
         */
        public void doLink_Highlight(string sControlName)
        {
            moLinker.doLinkDataToControl(_Highlight, sControlName);
        }
        /**
         * Link the U_Cursor Field to the Form Item.
         */
        public void doLink_Cursor(string sControlName)
        {
            moLinker.doLinkDataToControl(_Cursor, sControlName);
        }
        /**
         * Link the U_Description Field to the Form Item.
         */
        public void doLink_Description(string sControlName)
        {
            moLinker.doLinkDataToControl(_Description, sControlName);
        }
        /**
         * Link the U_Next Field to the Form Item.
         */
        public void doLink_Next(string sControlName)
        {
            moLinker.doLinkDataToControl(_Next, sControlName);
        }
        #endregion

    }
}
