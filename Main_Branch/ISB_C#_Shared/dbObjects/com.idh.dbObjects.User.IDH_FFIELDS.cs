/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2014/10/29 04:57:01 PM
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.controls;
//using com.idh.win.controls.SBO;


namespace com.idh.dbObjects.User{
   [Serializable] 
	public class IDH_FFIELDS: com.idh.dbObjects.Base.IDH_FFIELDS{

       private static int miLabelWidth = 80;
       public static int LabelWidth {
           get { return miLabelWidth; }
       }

       private static int miControlWidth = 80;
       public static int ControlWidth {
           get { return miControlWidth; }
       }

       private static string it_STATIC = "it_STATIC";
       public static string StaticText {
           get { return it_STATIC; }
       }

       private static string it_COMBO_BOX = "it_COMBO_BOX";
       public static string ComboBox {
           get { return it_COMBO_BOX; }
       }

       private static string it_EDITTEXT = "it_EDITBOX";
       public static string EditText {
           get { return it_EDITTEXT; }
       }

       private static string pt_TEXT = "pt_TEXT";
       public static string PopType_TEXT {
           get { return pt_TEXT; }
       }

       private static string pt_SQL = "pt_SQL";
       public static string PopType_SQL {
           get { return pt_SQL; }
       }

       private static string pt_LOOKUP = "pt_LOOKUP";
       public static string PopType_LOOKUP {
           get { return pt_LOOKUP; }
       }
       
       public class ComboParams {
           //Array of Key Value pair
           private ArrayList moPairs = new ArrayList();

           public ComboParams(string sParams) {
               string[] saParams = sParams.Split(';');
               string[] saPair;
               foreach (string sPair in saParams) {
                   if (sPair.Contains(",")) {
                       saPair = sPair.Split(',');
                       moPairs.Add(new ValuePair(saPair[0], saPair[1]));
                   } else {
                       moPairs.Add(new ValuePair(sPair, sPair));
                   }
               }
           }
           public ArrayList getPairs {
               get { return moPairs; }
           }

           public static ComboParams doSplit(string sParam) {
               return new ComboParams(sParam);
           }
       }


		public IDH_FFIELDS() : base() {
			
		}

   	    public IDH_FFIELDS( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	    }

        public int getByUser(string sUser, string sFormTypeId) {
            string sWhere =  "(" + _User + " Is NULL Or " + _User + " = '' OR " + _User + " = '" + sUser + "') AND " + _FTId + " = '" + sFormTypeId + '\'';
            return getData(sWhere, _Pos);
        }

        public int getByFormTypeId(string sFormTypeId) {
            string sWhere = _FTId + " = '" + sFormTypeId + '\'';
            return getData(sWhere, _Pos);
        }

       
        
	}
}
