/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 21/07/2015 18:16:13
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
	public class IDH_RCEMPLOYEE: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
		protected IDHAddOns.idh.forms.Base moIDHForm;

		public IDH_RCEMPLOYEE() : base("@IDH_RCEMPLOYEE"){
		}

		public IDH_RCEMPLOYEE( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_RCEMPLOYEE"){
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_RCEMPLOYEE";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: RouteCL Header
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ROUTECL
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ROUTECL = "U_ROUTECL";
		public string U_ROUTECL { 
			get {
 				return getValueAsString(_ROUTECL); 
			}
			set { setValue(_ROUTECL, value); }
		}
           public string doValidate_ROUTECL() {
               return doValidate_ROUTECL(U_ROUTECL);
           }
           public virtual string doValidate_ROUTECL(object oValue) {
               return base.doValidation(_ROUTECL, oValue);
           }

		/**
		 * Decription: Employee
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCEMP
		 * Size: 150
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RCEMP = "U_RCEMP";
		public string U_RCEMP { 
			get {
 				return getValueAsString(_RCEMP); 
			}
			set { setValue(_RCEMP, value); }
		}
           public string doValidate_RCEMP() {
               return doValidate_RCEMP(U_RCEMP);
           }
           public virtual string doValidate_RCEMP(object oValue) {
               return base.doValidation(_RCEMP, oValue);
           }

		/**
		 * Decription: Primary
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCPRIM
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RCPRIM = "U_RCPRIM";
		public string U_RCPRIM { 
			get {
 				return getValueAsString(_RCPRIM); 
			}
			set { setValue(_RCPRIM, value); }
		}
           public string doValidate_RCPRIM() {
               return doValidate_RCPRIM(U_RCPRIM);
           }
           public virtual string doValidate_RCPRIM(object oValue) {
               return base.doValidation(_RCPRIM, oValue);
           }

		/**
		 * Decription: Route Assistant
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCRTASST
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RCRTASST = "U_RCRTASST";
		public string U_RCRTASST { 
			get {
 				return getValueAsString(_RCRTASST); 
			}
			set { setValue(_RCRTASST, value); }
		}
           public string doValidate_RCRTASST() {
               return doValidate_RCRTASST(U_RCRTASST);
           }
           public virtual string doValidate_RCRTASST(object oValue) {
               return base.doValidation(_RCRTASST, oValue);
           }

		/**
		 * Decription: Clock In
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCCLOCKIN
		 * Size: 6
		 * Type: db_Date
		 * CType: string
		 * SubType: st_Time
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _RCCLOCKIN = "U_RCCLOCKIN";
		public string U_RCCLOCKIN { 
			get {
 				return getValueAsString(_RCCLOCKIN); 
			}
			set { setValue(_RCCLOCKIN, value); }
		}
           public string doValidate_RCCLOCKIN() {
               return doValidate_RCCLOCKIN(U_RCCLOCKIN);
           }
           public virtual string doValidate_RCCLOCKIN(object oValue) {
               return base.doValidation(_RCCLOCKIN, oValue);
           }

		/**
		 * Decription: Clock Out
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCCLOCKOUT
		 * Size: 6
		 * Type: db_Date
		 * CType: string
		 * SubType: st_Time
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _RCCLOCKOUT = "U_RCCLOCKOUT";
		public string U_RCCLOCKOUT { 
			get {
 				return getValueAsString(_RCCLOCKOUT); 
			}
			set { setValue(_RCCLOCKOUT, value); }
		}
           public string doValidate_RCCLOCKOUT() {
               return doValidate_RCCLOCKOUT(U_RCCLOCKOUT);
           }
           public virtual string doValidate_RCCLOCKOUT(object oValue) {
               return base.doValidation(_RCCLOCKOUT, oValue);
           }

		/**
		 * Decription: Total Hours
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RCTHOUR1
		 * Size: 5
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RCTHOUR1 = "U_RCTHOUR1";
		public string U_RCTHOUR1 { 
			get {
 				return getValueAsString(_RCTHOUR1); 
			}
			set { setValue(_RCTHOUR1, value); }
		}
           public string doValidate_RCTHOUR1() {
               return doValidate_RCTHOUR1(U_RCTHOUR1);
           }
           public virtual string doValidate_RCTHOUR1(object oValue) {
               return base.doValidation(_RCTHOUR1, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(9);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_ROUTECL, "RouteCL Header", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //RouteCL Header
			moDBFields.Add(_RCEMP, "Employee", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 150, EMPTYSTR, false, false); //Employee
			moDBFields.Add(_RCPRIM, "Primary", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Primary
			moDBFields.Add(_RCRTASST, "Route Assistant", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Route Assistant
			moDBFields.Add(_RCCLOCKIN, "Clock In", 6, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Clock In
			moDBFields.Add(_RCCLOCKOUT, "Clock Out", 7, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Clock Out
			moDBFields.Add(_RCTHOUR1, "Total Hours", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 5, EMPTYSTR, false, false); //Total Hours

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_ROUTECL());
            doBuildValidationString(doValidate_RCEMP());
            doBuildValidationString(doValidate_RCPRIM());
            doBuildValidationString(doValidate_RCRTASST());
            doBuildValidationString(doValidate_RCCLOCKIN());
            doBuildValidationString(doValidate_RCCLOCKOUT());
            doBuildValidationString(doValidate_RCTHOUR1());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_ROUTECL)) return doValidate_ROUTECL(oValue);
            if (sFieldName.Equals(_RCEMP)) return doValidate_RCEMP(oValue);
            if (sFieldName.Equals(_RCPRIM)) return doValidate_RCPRIM(oValue);
            if (sFieldName.Equals(_RCRTASST)) return doValidate_RCRTASST(oValue);
            if (sFieldName.Equals(_RCCLOCKIN)) return doValidate_RCCLOCKIN(oValue);
            if (sFieldName.Equals(_RCCLOCKOUT)) return doValidate_RCCLOCKOUT(oValue);
            if (sFieldName.Equals(_RCTHOUR1)) return doValidate_RCTHOUR1(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_ROUTECL Field to the Form Item.
		 */
		public void doLink_ROUTECL(string sControlName){
			moLinker.doLinkDataToControl(_ROUTECL, sControlName);
		}
		/**
		 * Link the U_RCEMP Field to the Form Item.
		 */
		public void doLink_RCEMP(string sControlName){
			moLinker.doLinkDataToControl(_RCEMP, sControlName);
		}
		/**
		 * Link the U_RCPRIM Field to the Form Item.
		 */
		public void doLink_RCPRIM(string sControlName){
			moLinker.doLinkDataToControl(_RCPRIM, sControlName);
		}
		/**
		 * Link the U_RCRTASST Field to the Form Item.
		 */
		public void doLink_RCRTASST(string sControlName){
			moLinker.doLinkDataToControl(_RCRTASST, sControlName);
		}
		/**
		 * Link the U_RCCLOCKIN Field to the Form Item.
		 */
		public void doLink_RCCLOCKIN(string sControlName){
			moLinker.doLinkDataToControl(_RCCLOCKIN, sControlName);
		}
		/**
		 * Link the U_RCCLOCKOUT Field to the Form Item.
		 */
		public void doLink_RCCLOCKOUT(string sControlName){
			moLinker.doLinkDataToControl(_RCCLOCKOUT, sControlName);
		}
		/**
		 * Link the U_RCTHOUR1 Field to the Form Item.
		 */
		public void doLink_RCTHOUR1(string sControlName){
			moLinker.doLinkDataToControl(_RCTHOUR1, sControlName);
		}
#endregion

	}
}
