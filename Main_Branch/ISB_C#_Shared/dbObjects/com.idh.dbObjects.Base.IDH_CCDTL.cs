/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 2015-09-02 12:07:54 PM
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
   [Serializable]
	public class IDH_CCDTL: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
  //     public Linker ControlLinker {
  //         get { return moLinker; }
  //         set { moLinker = value; }
  //     }

       private IDHAddOns.idh.forms.Base moIDHForm;
       public IDHAddOns.idh.forms.Base IDHForm {
           get { return moIDHForm; }
           set { moIDHForm = value; }
       }

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_CCDTL() : base("@IDH_CCDTL"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_CCDTL( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_CCDTL"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_CCDTL";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Customer
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CardCd
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CardCd = "U_CardCd";
		public string U_CardCd { 
			get {
 				return getValueAsString(_CardCd); 
			}
			set { setValue(_CardCd, value); }
		}
           public string doValidate_CardCd() {
               return doValidate_CardCd(U_CardCd);
           }
           public virtual string doValidate_CardCd(object oValue) {
               return base.doValidation(_CardCd, oValue);
           }

		/**
		 * Decription: Average Delay
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CCAvDe
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CCAvDe = "U_CCAvDe";
		public string U_CCAvDe { 
			get {
 				return getValueAsString(_CCAvDe); 
			}
			set { setValue(_CCAvDe, value); }
		}
           public string doValidate_CCAvDe() {
               return doValidate_CCAvDe(U_CCAvDe);
           }
           public virtual string doValidate_CCAvDe(object oValue) {
               return base.doValidation(_CCAvDe, oValue);
           }

		/**
		 * Decription: Expiration Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CCExp
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 1899-12-30 12:00:00 AM
		 * Identity: False
		 */
		public readonly static string _CCExp = "U_CCExp";
		public DateTime U_CCExp { 
			get {
 				return getValueAsDateTime(_CCExp); 
			}
			set { setValue(_CCExp, value); }
		}
		public void U_CCExp_AsString(string value){
			setValue("U_CCExp", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_CCExp_AsString(){
			DateTime dVal = getValueAsDateTime(_CCExp);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_CCExp() {
               return doValidate_CCExp(U_CCExp);
           }
           public virtual string doValidate_CCExp(object oValue) {
               return base.doValidation(_CCExp, oValue);
           }

		/**
		 * Decription: Creditcard Id Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CCId
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CCId = "U_CCId";
		public string U_CCId { 
			get {
 				return getValueAsString(_CCId); 
			}
			set { setValue(_CCId, value); }
		}
           public string doValidate_CCId() {
               return doValidate_CCId(U_CCId);
           }
           public virtual string doValidate_CCId(object oValue) {
               return base.doValidation(_CCId, oValue);
           }

		/**
		 * Decription: Credit Card No
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CCNum
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CCNum = "U_CCNum";
		public string U_CCNum { 
			get {
 				return getValueAsString(_CCNum); 
			}
			set { setValue(_CCNum, value); }
		}
           public string doValidate_CCNum() {
               return doValidate_CCNum(U_CCNum);
           }
           public virtual string doValidate_CCNum(object oValue) {
               return base.doValidation(_CCNum, oValue);
           }

		/**
		 * Decription: Card Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CCType
		 * Size: 5
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CCType = "U_CCType";
		public string U_CCType { 
			get {
 				return getValueAsString(_CCType); 
			}
			set { setValue(_CCType, value); }
		}
           public string doValidate_CCType() {
               return doValidate_CCType(U_CCType);
           }
           public virtual string doValidate_CCType(object oValue) {
               return base.doValidation(_CCType, oValue);
           }

		/**
		 * Decription: CCard House Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHCCHNm
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _IDHCCHNm = "U_IDHCCHNm";
		public string U_IDHCCHNm { 
			get {
 				return getValueAsString(_IDHCCHNm); 
			}
			set { setValue(_IDHCCHNm, value); }
		}
           public string doValidate_IDHCCHNm() {
               return doValidate_IDHCCHNm(U_IDHCCHNm);
           }
           public virtual string doValidate_IDHCCHNm(object oValue) {
               return base.doValidation(_IDHCCHNm, oValue);
           }

		/**
		 * Decription: CCard Issue #
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHCCIs
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _IDHCCIs = "U_IDHCCIs";
		public string U_IDHCCIs { 
			get {
 				return getValueAsString(_IDHCCIs); 
			}
			set { setValue(_IDHCCIs, value); }
		}
           public string doValidate_IDHCCIs() {
               return doValidate_IDHCCIs(U_IDHCCIs);
           }
           public virtual string doValidate_IDHCCIs(object oValue) {
               return base.doValidation(_IDHCCIs, oValue);
           }

		/**
		 * Decription: CCard PostCode
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHCCPCd
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _IDHCCPCd = "U_IDHCCPCd";
		public string U_IDHCCPCd { 
			get {
 				return getValueAsString(_IDHCCPCd); 
			}
			set { setValue(_IDHCCPCd, value); }
		}
           public string doValidate_IDHCCPCd() {
               return doValidate_IDHCCPCd(U_IDHCCPCd);
           }
           public virtual string doValidate_IDHCCPCd(object oValue) {
               return base.doValidation(_IDHCCPCd, oValue);
           }

		/**
		 * Decription: CCard Security Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHCCSec
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _IDHCCSec = "U_IDHCCSec";
		public string U_IDHCCSec { 
			get {
 				return getValueAsString(_IDHCCSec); 
			}
			set { setValue(_IDHCCSec, value); }
		}
           public string doValidate_IDHCCSec() {
               return doValidate_IDHCCSec(U_IDHCCSec);
           }
           public virtual string doValidate_IDHCCSec(object oValue) {
               return base.doValidation(_IDHCCSec, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(12);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_CardCd, "Customer", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Customer
			moDBFields.Add(_CCAvDe, "Average Delay", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Average Delay
			moDBFields.Add(_CCExp, "Expiration Date", 4, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Expiration Date
			moDBFields.Add(_CCId, "Creditcard Id Number", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Creditcard Id Number
			moDBFields.Add(_CCNum, "Credit Card No", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Credit Card No
			moDBFields.Add(_CCType, "Card Type", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 5, EMPTYSTR, false, false); //Card Type
			moDBFields.Add(_IDHCCHNm, "CCard House Number", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //CCard House Number
			moDBFields.Add(_IDHCCIs, "CCard Issue #", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //CCard Issue #
			moDBFields.Add(_IDHCCPCd, "CCard PostCode", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //CCard PostCode
			moDBFields.Add(_IDHCCSec, "CCard Security Number", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //CCard Security Number

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_CardCd());
            doBuildValidationString(doValidate_CCAvDe());
            doBuildValidationString(doValidate_CCExp());
            doBuildValidationString(doValidate_CCId());
            doBuildValidationString(doValidate_CCNum());
            doBuildValidationString(doValidate_CCType());
            doBuildValidationString(doValidate_IDHCCHNm());
            doBuildValidationString(doValidate_IDHCCIs());
            doBuildValidationString(doValidate_IDHCCPCd());
            doBuildValidationString(doValidate_IDHCCSec());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_CardCd)) return doValidate_CardCd(oValue);
            if (sFieldName.Equals(_CCAvDe)) return doValidate_CCAvDe(oValue);
            if (sFieldName.Equals(_CCExp)) return doValidate_CCExp(oValue);
            if (sFieldName.Equals(_CCId)) return doValidate_CCId(oValue);
            if (sFieldName.Equals(_CCNum)) return doValidate_CCNum(oValue);
            if (sFieldName.Equals(_CCType)) return doValidate_CCType(oValue);
            if (sFieldName.Equals(_IDHCCHNm)) return doValidate_IDHCCHNm(oValue);
            if (sFieldName.Equals(_IDHCCIs)) return doValidate_IDHCCIs(oValue);
            if (sFieldName.Equals(_IDHCCPCd)) return doValidate_IDHCCPCd(oValue);
            if (sFieldName.Equals(_IDHCCSec)) return doValidate_IDHCCSec(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_CardCd Field to the Form Item.
		 */
		public void doLink_CardCd(string sControlName){
			moLinker.doLinkDataToControl(_CardCd, sControlName);
		}
		/**
		 * Link the U_CCAvDe Field to the Form Item.
		 */
		public void doLink_CCAvDe(string sControlName){
			moLinker.doLinkDataToControl(_CCAvDe, sControlName);
		}
		/**
		 * Link the U_CCExp Field to the Form Item.
		 */
		public void doLink_CCExp(string sControlName){
			moLinker.doLinkDataToControl(_CCExp, sControlName);
		}
		/**
		 * Link the U_CCId Field to the Form Item.
		 */
		public void doLink_CCId(string sControlName){
			moLinker.doLinkDataToControl(_CCId, sControlName);
		}
		/**
		 * Link the U_CCNum Field to the Form Item.
		 */
		public void doLink_CCNum(string sControlName){
			moLinker.doLinkDataToControl(_CCNum, sControlName);
		}
		/**
		 * Link the U_CCType Field to the Form Item.
		 */
		public void doLink_CCType(string sControlName){
			moLinker.doLinkDataToControl(_CCType, sControlName);
		}
		/**
		 * Link the U_IDHCCHNm Field to the Form Item.
		 */
		public void doLink_IDHCCHNm(string sControlName){
			moLinker.doLinkDataToControl(_IDHCCHNm, sControlName);
		}
		/**
		 * Link the U_IDHCCIs Field to the Form Item.
		 */
		public void doLink_IDHCCIs(string sControlName){
			moLinker.doLinkDataToControl(_IDHCCIs, sControlName);
		}
		/**
		 * Link the U_IDHCCPCd Field to the Form Item.
		 */
		public void doLink_IDHCCPCd(string sControlName){
			moLinker.doLinkDataToControl(_IDHCCPCd, sControlName);
		}
		/**
		 * Link the U_IDHCCSec Field to the Form Item.
		 */
		public void doLink_IDHCCSec(string sControlName){
			moLinker.doLinkDataToControl(_IDHCCSec, sControlName);
		}
#endregion

	}
}
