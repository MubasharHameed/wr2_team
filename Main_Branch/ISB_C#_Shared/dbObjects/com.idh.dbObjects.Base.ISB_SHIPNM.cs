/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 2014/10/09 05:23:12 AM
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
   [Serializable]
	public class ISB_SHIPNM: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
		protected IDHAddOns.idh.forms.Base moIDHForm;

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public ISB_SHIPNM() : base("@ISB_SHIPNM"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public ISB_SHIPNM( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@ISB_SHIPNM"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@ISB_SHIPNM";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Shipping Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ShipNm
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ShipNm = "U_ShipNm";
		public string U_ShipNm { 
			get {
 				return getValueAsString(_ShipNm); 
			}
			set { setValue(_ShipNm, value); }
		}
           public string doValidate_ShipNm() {
               return doValidate_ShipNm(U_ShipNm);
           }
           public virtual string doValidate_ShipNm(object oValue) {
               return base.doValidation(_ShipNm, oValue);
           }

		/**
		 * Decription: Proper Shipping Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PropShpNm
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PropShpNm = "U_PropShpNm";
		public string U_PropShpNm { 
			get {
 				return getValueAsString(_PropShpNm); 
			}
			set { setValue(_PropShpNm, value); }
		}
           public string doValidate_PropShpNm() {
               return doValidate_PropShpNm(U_PropShpNm);
           }
           public virtual string doValidate_PropShpNm(object oValue) {
               return base.doValidation(_PropShpNm, oValue);
           }

		/**
		 * Decription: Packing Group
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PackGrp
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PackGrp = "U_PackGrp";
		public string U_PackGrp { 
			get {
 				return getValueAsString(_PackGrp); 
			}
			set { setValue(_PackGrp, value); }
		}
           public string doValidate_PackGrp() {
               return doValidate_PackGrp(U_PackGrp);
           }
           public virtual string doValidate_PackGrp(object oValue) {
               return base.doValidation(_PackGrp, oValue);
           }

		/**
		 * Decription: Haz Class
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HazClass
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _HazClass = "U_HazClass";
		public string U_HazClass { 
			get {
 				return getValueAsString(_HazClass); 
			}
			set { setValue(_HazClass, value); }
		}
           public string doValidate_HazClass() {
               return doValidate_HazClass(U_HazClass);
           }
           public virtual string doValidate_HazClass(object oValue) {
               return base.doValidation(_HazClass, oValue);
           }

		/**
		 * Decription: UN NA No.
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UNNANo
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _UNNANo = "U_UNNANo";
		public string U_UNNANo { 
			get {
 				return getValueAsString(_UNNANo); 
			}
			set { setValue(_UNNANo, value); }
		}
           public string doValidate_UNNANo() {
               return doValidate_UNNANo(U_UNNANo);
           }
           public virtual string doValidate_UNNANo(object oValue) {
               return base.doValidation(_UNNANo, oValue);
           }

		/**
		 * Decription: Reportable Quantity
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IsRepQty
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _IsRepQty = "U_IsRepQty";
		public string U_IsRepQty { 
			get {
 				return getValueAsString(_IsRepQty); 
			}
			set { setValue(_IsRepQty, value); }
		}
           public string doValidate_IsRepQty() {
               return doValidate_IsRepQty(U_IsRepQty);
           }
           public virtual string doValidate_IsRepQty(object oValue) {
               return base.doValidation(_IsRepQty, oValue);
           }

		/**
		 * Decription: Waste
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IsWaste
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _IsWaste = "U_IsWaste";
		public string U_IsWaste { 
			get {
 				return getValueAsString(_IsWaste); 
			}
			set { setValue(_IsWaste, value); }
		}
           public string doValidate_IsWaste() {
               return doValidate_IsWaste(U_IsWaste);
           }
           public virtual string doValidate_IsWaste(object oValue) {
               return base.doValidation(_IsWaste, oValue);
           }

		/**
		 * Decription: Constituents
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Constit
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Constit = "U_Constit";
		public string U_Constit { 
			get {
 				return getValueAsString(_Constit); 
			}
			set { setValue(_Constit, value); }
		}
           public string doValidate_Constit() {
               return doValidate_Constit(U_Constit);
           }
           public virtual string doValidate_Constit(object oValue) {
               return base.doValidation(_Constit, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(10);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_ShipNm, "Shipping Name", 2, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Shipping Name
			moDBFields.Add(_PropShpNm, "Proper Shipping Name", 3, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Proper Shipping Name
			moDBFields.Add(_PackGrp, "Packing Group", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Packing Group
			moDBFields.Add(_HazClass, "Haz Class", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Haz Class
			moDBFields.Add(_UNNANo, "UN NA No.", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //UN NA No.
			moDBFields.Add(_IsRepQty, "Reportable Quantity", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Reportable Quantity
			moDBFields.Add(_IsWaste, "Waste", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Waste
			moDBFields.Add(_Constit, "Constituents", 9, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Constituents

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_ShipNm());
            doBuildValidationString(doValidate_PropShpNm());
            doBuildValidationString(doValidate_PackGrp());
            doBuildValidationString(doValidate_HazClass());
            doBuildValidationString(doValidate_UNNANo());
            doBuildValidationString(doValidate_IsRepQty());
            doBuildValidationString(doValidate_IsWaste());
            doBuildValidationString(doValidate_Constit());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_ShipNm)) return doValidate_ShipNm(oValue);
            if (sFieldName.Equals(_PropShpNm)) return doValidate_PropShpNm(oValue);
            if (sFieldName.Equals(_PackGrp)) return doValidate_PackGrp(oValue);
            if (sFieldName.Equals(_HazClass)) return doValidate_HazClass(oValue);
            if (sFieldName.Equals(_UNNANo)) return doValidate_UNNANo(oValue);
            if (sFieldName.Equals(_IsRepQty)) return doValidate_IsRepQty(oValue);
            if (sFieldName.Equals(_IsWaste)) return doValidate_IsWaste(oValue);
            if (sFieldName.Equals(_Constit)) return doValidate_Constit(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_ShipNm Field to the Form Item.
		 */
		public void doLink_ShipNm(string sControlName){
			moLinker.doLinkDataToControl(_ShipNm, sControlName);
		}
		/**
		 * Link the U_PropShpNm Field to the Form Item.
		 */
		public void doLink_PropShpNm(string sControlName){
			moLinker.doLinkDataToControl(_PropShpNm, sControlName);
		}
		/**
		 * Link the U_PackGrp Field to the Form Item.
		 */
		public void doLink_PackGrp(string sControlName){
			moLinker.doLinkDataToControl(_PackGrp, sControlName);
		}
		/**
		 * Link the U_HazClass Field to the Form Item.
		 */
		public void doLink_HazClass(string sControlName){
			moLinker.doLinkDataToControl(_HazClass, sControlName);
		}
		/**
		 * Link the U_UNNANo Field to the Form Item.
		 */
		public void doLink_UNNANo(string sControlName){
			moLinker.doLinkDataToControl(_UNNANo, sControlName);
		}
		/**
		 * Link the U_IsRepQty Field to the Form Item.
		 */
		public void doLink_IsRepQty(string sControlName){
			moLinker.doLinkDataToControl(_IsRepQty, sControlName);
		}
		/**
		 * Link the U_IsWaste Field to the Form Item.
		 */
		public void doLink_IsWaste(string sControlName){
			moLinker.doLinkDataToControl(_IsWaste, sControlName);
		}
		/**
		 * Link the U_Constit Field to the Form Item.
		 */
		public void doLink_Constit(string sControlName){
			moLinker.doLinkDataToControl(_Constit, sControlName);
		}
#endregion

	}
}
