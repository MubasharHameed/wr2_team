/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 05/02/2016 15:02:00
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
   [Serializable] 
	public class IDH_ENQUS: com.idh.dbObjects.Base.IDH_ENQUS{ 

		public IDH_ENQUS() : base() {
            msAutoNumKey = "IDHENQUS";
			
		}

   	public IDH_ENQUS( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
        msAutoNumKey = "IDHENQUS";   	
    }
	}
}
