/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 12/03/2015 12:01:10
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
	public class IDH_RCDOWNT: com.idh.dbObjects.Base.IDH_RCDOWNT{ 

		public IDH_RCDOWNT() : base() {
			
		}

   	public IDH_RCDOWNT( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	}

    private IDH_ROUTECL moRouteCl;
    public IDH_ROUTECL RouteCl
    {
        get { return moRouteCl; }
        set { moRouteCl = value; }
    }

    public int getByRCRow(string sRouteCode)
    {
        return getData(_ROUTECL + " = '" + sRouteCode + "'", null);
    }

    //Set the RouteClosure Code for all rows with a Row Code of -1
    public void doSetRCCode(string sRCCode)
    {
        if (Count > 0)
        {
            int iCurrentRow = CurrentRowIndex;
            first();
            while (next())
            {
                if (U_ROUTECL.Equals("-1"))
                    U_ROUTECL = sRCCode;
            }
            gotoRow(iCurrentRow);
        }
    }

	}
}
