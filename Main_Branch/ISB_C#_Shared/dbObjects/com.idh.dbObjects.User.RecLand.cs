﻿using System;
using System.Collections.Generic;
using System.Text;

using com.idh.bridge;
using com.idh.utils;

namespace com.idh.dbObjects.User {
    public class RecLand : com.idh.dbObjects.DBBase
    {
        public RecLand()
            : base("vRptRecycle1")
        {
		}

        public readonly static string _CardCode = "CardCode";
        public readonly static string _CardName = "CardName";
        public readonly static string _MasterCode = "MasterCode";
        public readonly static string _ChildCode = "ChildCode";
        public readonly static string _ParentItemCode = "ChildCode";
        public readonly static string _ChildItemCode = "ChildItemCode";
        public readonly static string _ChildItemName = "ChildItemName";
        public readonly static string _JobType = "JobType";
        public readonly static string _JobDate = "JobDate";
        public readonly static string _CustLandFillPercent = "CustLandFillPercent";
        public readonly static string _CustRecoveryPercent = "CustRecoveryPercent";
        public readonly static string _IsParentItemLandFill = "IsParentItemLandFill";
        public readonly static string _IsChildItemLandFill = "IsChildItemLandFill";
        public readonly static string _OrdWgt = "U_OrdWgt";
        public readonly static string _RecoveredWgt = "RecoveredWgt";
        public readonly static string _LandFillWgt = "LandFillWgt";
        public readonly static string _Glass = "Glass";
        public readonly static string _GlassWgt = "GlassWgt";
        public readonly static string _PCBCont = "PCBCont";
        public readonly static string _PCBWgt = "PCBWgt";
        public readonly static string _DeflectionCoilsCont = "DeflectionCoilsCont";
        public readonly static string _DeflectionCoilsWgt = "DeflectionCoilsWgt";
        public readonly static string _FerrousMetalCont = "FerrousMetalCont";
        public readonly static string _FerrousMetalWgt = "FerrousMetalWgt";
        public readonly static string _NonFerrousMetalCont = "NonFerrousMetalCont";
        public readonly static string _NonFerrousMetalWgt = "NonFerrousMetalWgt";
        public readonly static string _SpeakerCont = "SpeakerCont";
        public readonly static string _SpeakerWgt = "SpeakerWgt";
        public readonly static string _PlasticCont = "PlasticCont";
        public readonly static string _PlasticWgt = "PlasticWgt";
        public readonly static string _CablePlugCont = "CablePlugCont";
        public readonly static string _CablePlugWgt = "CablePlugWgt";
        public readonly static string _CFCCont = "CFCCont";
        public readonly static string _CFCWgt = "CFCWgt";
        public readonly static string _OilCont = "OilCont";
        public readonly static string _OilWgt = "OilWgt";
        public readonly static string _CompressorsCont = "CompressorsCont";
        public readonly static string _CompressorsWgt = "CompressorsWgt";
        public readonly static string _PURFoamCont = "PURFoamCont";
        public readonly static string _PURFoamWgt = "PURFoamWgt";
        public readonly static string _MixWasteCont = "MixWasteCont";
        public readonly static string _MixWasteWgt = "MixWasteWgt";
        public readonly static string _OtherCont = "OtherCont";
        public readonly static string _OtherWgt = "OtherWgt";
        public readonly static string _FinesCont = "FinesCont";
        public readonly static string _FinesWgt = "FinesWgt";
        public readonly static string _ArcTubesCont = "ArcTubesCont";
        public readonly static string _ArcTubesWgt = "ArcTubesWgt";
        public readonly static string _WireCont = "WireCont";
        public readonly static string _WireWgt = "WireWgt";
        public readonly static string _CathodesCont = "CathodesCont";
        public readonly static string _CathodesWgt = "CathodesWgt";
        public readonly static string _CollarsYokesCont = "CollarsYokesCont";
        public readonly static string _CollarsYokesWgt = "CollarsYokesWgt";
        public readonly static string _TransformersCont = "TransformersCont";
        public readonly static string _TransformersWgt = "TransformersWgt";
        public readonly static string _WoodCont = "WoodCont";
        public readonly static string _WoodWgt = "WoodWgt";
        public readonly static string _PutrescibleCont = "PutrescibleCont";
        public readonly static string _PutrescibleWgt = "PutrescibleWgt";
        public readonly static string _GreenCont = "GreenCont";
        public readonly static string _GreenWgt = "GreenWgt";
        public readonly static string _CardboardCont = "CardboardCont";
        public readonly static string _CardboardWgt = "CardboardWgt";
        public readonly static string _IsLandFill = "IsLandFill";

        protected override void doSetFieldNames()
        {
            string[] soLocalDBFields = new string[] {
                  "CardCode", 
                  "CardName"
                  ,"MasterCode"
                  ,"ChildCode"
                  ,"ParentItemCode"
                  ,"ChildItemCode"
                  ,"ChildItemName"
                  ,"JobType"
                  ,"JobDate"
                  ,"CustLandFillPercent"
                  ,"CustRecoveryPercent"
                  ,"IsParentItemLandFill"
                  ,"IsChildItemLandFill"
                  ,"U_OrdWgt"
                  ,"RecoveredWgt"
                  ,"LandFillWgt"
                  ,"Glass"
                  ,"GlassWgt"
                  ,"PCBCont"
                  ,"PCBWgt"
                  ,"DeflectionCoilsCont"
                  ,"DeflectionCoilsWgt"
                  ,"FerrousMetalCont"
                  ,"FerrousMetalWgt"
                  ,"NonFerrousMetalCont"
                  ,"NonFerrousMetalWgt"
                  ,"SpeakerCont"
                  ,"SpeakerWgt"
                  ,"PlasticCont"
                  ,"PlasticWgt"
                  ,"CablePlugCont"
                  ,"CablePlugWgt"
                  ,"CFCCont"
                  ,"CFCWgt"
                  ,"OilCont"
                  ,"OilWgt"
                  ,"CompressorsCont"
                  ,"CompressorsWgt"
                  ,"PURFoamCont"
                  ,"PURFoamWgt"
                  ,"MixWasteCont"
                  ,"MixWasteWgt"
                  ,"OtherCont"
                  ,"OtherWgt"
                  ,"FinesCont"
                  ,"FinesWgt"
                  ,"ArcTubesCont"
                  ,"ArcTubesWgt"
                  ,"WireCont"
                  ,"WireWgt"
                  ,"CathodesCont"
                  ,"CathodesWgt"
                  ,"CollarsYokesCont"
                  ,"CollarsYokesWgt"
                  ,"TransformersCont"
                  ,"TransformersWgt"
                  ,"WoodCont"
                  ,"WoodWgt"
                  ,"PutrescibleCont"
                  ,"PutrescibleWgt"
                  ,"GreenCont"
                  ,"GreenWgt"
                  ,"CardboardCont"
                  ,"CardboardWgt"
                  ,"IsLandFill" };
            moDBFields = soLocalDBFields;
            doBuildSelectionList();
        }

        public int getSUMForPeriod(string sCardCode, DateTime dFrom, DateTime dTo)
        {
            string sSelectList =
                  "'' AS CardCode, " + 
                  "'' AS CardName, " +
                  "'' AS MasterCode, " +
                  "'' AS ChildCode, " +
                  "'' AS ParentItemCode, " +
                  "'' AS ChildItemCode, " +
                  "'' AS ChildItemName, " +
                  "'' AS JobType, " +
                  "'' AS JobDate, " +
                  "SUM(CustLandFillPercent), " +
                  "SUM(CustRecoveryPercent), " + 
                  "'' As IsParentItemLandFill, " +
                  "'' AS IsChildItemLandFill, " +
                  "SUM(U_OrdWgt), " +
                  "SUM(RecoveredWgt), " +
                  "SUM(LandFillWgt), " +
                  "AVG(Glass), " +
                  "SUM(GlassWgt), " +
                  "AVG(PCBCont), " +
                  "SUM(PCBWgt), " +
                  "AVG(DeflectionCoilsCont), " +
                  "SUM(DeflectionCoilsWgt), " +
                  "AVG(FerrousMetalCont), " +
                  "SUM(FerrousMetalWgt), " +
                  "AVG(NonFerrousMetalCont), " +
                  "SUM(NonFerrousMetalWgt), " +
                  "AVG(SpeakerCont), " +
                  "SUM(SpeakerWgt), " +
                  "AVG(PlasticCont), " +
                  "SUM(PlasticWgt), " +
                  "AVG(CablePlugCont), " +
                  "SUM(CablePlugWgt), " +
                  "AVG(CFCCont), " +
                  "SUM(CFCWgt), " +
                  "AVG(OilCont), " +
                  "SUM(OilWgt), " +
                  "AVG(CompressorsCont), " +
                  "SUM(CompressorsWgt), " +
                  "AVG(PURFoamCont), " +
                  "SUM(PURFoamWgt), " +
                  "AVG(MixWasteCont), " +
                  "SUM(MixWasteWgt), " +
                  "AVG(OtherCont), " +
                  "SUM(OtherWgt), " +
                  "AVG(FinesCont), " +
                  "SUM(FinesWgt), " +
                  "AVG(ArcTubesCont), " +
                  "SUM(ArcTubesWgt), " +
                  "AVG(WireCont), " +
                  "SUM(WireWgt), " +
                  "AVG(CathodesCont), " +
                  "SUM(CathodesWgt), " +
                  "AVG(CollarsYokesCont), " +
                  "SUM(CollarsYokesWgt), " +
                  "AVG(TransformersCont), " +
                  "SUM(TransformersWgt), " +
                  "AVG(WoodCont), " +
                  "SUM(WoodWgt), " +
                  "AVG(PutrescibleCont), " +
                  "SUM(PutrescibleWgt), " +
                  "AVG(GreenCont), " +
                  "SUM(GreenWgt), " +
                  "AVG(CardboardCont), " +
                  "SUM(CardboardWgt), " +
                  "'' AS IsLandFill ";

            string sFromDate = dates.doSimpleSQLDateNoTime(dFrom);
            string sToDate = dates.doSimpleSQLDateNoTime(dTo);
            string sWhereClause = "CardCode = '" + sCardCode + '\'' +
                                "  And CAST(CONVERT(VARCHAR, JobDate,101) AS DATETIME) >= CAST(CONVERT(VARCHAR, '" + sFromDate + "',101) AS DATETIME) " +
                                "  And CAST(CONVERT(VARCHAR, JobDate,101) As DATETIME) <= CAST(CONVERT(VARCHAR, '" + sToDate + "',101) AS DATETIME) ";

            if (getData(sSelectList, msTableName, sWhereClause, null) == 0)
                doAllDefaults();

            return Count;

        }

        public int getPeriodData(string sCardCode, DateTime dFrom, DateTime dTo)
        {
            string sFromDate = dates.doSimpleSQLDateNoTime(dFrom);
            string sToDate = dates.doSimpleSQLDateNoTime(dTo);

            string sWhereClause = "CardCode = '" + sCardCode + '\'' +
                                "  And CAST(CONVERT(VARCHAR, JobDate,101) AS DATETIME) >= CAST(CONVERT(VARCHAR, '" + sFromDate + "',101) AS DATETIME) " +
                                "  And CAST(CONVERT(VARCHAR, JobDate,101) As DATETIME) <= CAST(CONVERT(VARCHAR, '" + sToDate + "',101) AS DATETIME) ";

            if (getData(msSelectionList, msTableName, sWhereClause, "CardCode") == 0)
                doAllDefaults();

            return Count;
        }

        public double getFieldSum(string sFieldName)
        {
            double dVal = 0;
            if (Count > 0)
            {
                first();
                while (next())
                {
                    dVal += getValueAsDouble(sFieldName);
                }
            }
            int iCurrentPos = CurrentRowIndex;
            return dVal;
        }

        protected override void doSetDefaults()
        {
        }

#region validation
        public override bool doValidation()
        {
            return true;
        }
#endregion 

    }


}
