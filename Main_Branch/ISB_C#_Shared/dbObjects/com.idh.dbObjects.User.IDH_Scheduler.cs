/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2013/03/06 05:13:45 PM
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

using com.idh.utils;

namespace com.idh.dbObjects.User{
     
    public class IDH_Scheduler: com.idh.dbObjects.Base.IDH_Scheduler{
        public static string STATUS_ADDED = "Added";
        public static string STATUS_BUSY = "Busy";
        public static string STATUS_DONE = "Done";
        public static string STATUS_DELETED = "Del";
        public static string STATUS_ERROR = "Error";
        public static string STATUS_PAUSED = "Pause";
        public static string STATUS_WAITING = "Wait";
        public static string STATUS_EXCEPTION = "Excep";

        private DateTime moActionDate;
	    public IDH_Scheduler() : base() {
	    }

   	    public IDH_Scheduler( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	    }

        public bool getCurrentEntries( DateTime oActionDate) {
            if (oActionDate == null)
                oActionDate = DateTime.Now;

            moActionDate = oActionDate;

            string sActionDate = Dates.doSimpleSQLDateNoTime(oActionDate);
            string sHour = General.doSetLength(oActionDate.Hour.ToString(), 2, '0', 'l');
            string sMin = General.doSetLength(oActionDate.Minute.ToString(), 2, '0', 'l');
            string sDayOfWeek = "";
            switch (oActionDate.DayOfWeek) {
                case DayOfWeek.Sunday: sDayOfWeek = "Su"; break;
                case DayOfWeek.Monday: sDayOfWeek = "Mo"; break;
                case DayOfWeek.Tuesday: sDayOfWeek = "Tu"; break;
                case DayOfWeek.Wednesday: sDayOfWeek = "We"; break;
                case DayOfWeek.Thursday: sDayOfWeek = "Th"; break;
                case DayOfWeek.Friday: sDayOfWeek = "Fr"; break;
                case DayOfWeek.Saturday: sDayOfWeek = "Sa"; break;
            }
            string sWhere;
            
            /**
             * The Start and End Date Checks
             */
            sWhere = " (NOT " + _Status + " = '" + STATUS_DELETED + "' AND NOT " + _Status + " = '" + STATUS_BUSY + "' AND NOT " + _Status + " = '" + STATUS_PAUSED + "' AND NOT " + _Status + " = '" + STATUS_EXCEPTION + "')" + // Not Deleted and Not Busy
                 " AND (" + _StartDate + " Is NULL OR (CONVERT(DATETIME," + _StartDate + ",126) <= CONVERT(DATETIME,'" + sActionDate + "',126))) " + //StartDate after action Date
                 " AND (" + _EndDate + " Is NULL OR (CONVERT(DATETIME," + _EndDate + ",126) >= CONVERT(DATETIME,'" + sActionDate + "',126))) " + //EndDate Later than action Date
                //" AND " + _MaxCount + " < " + _CurrCount; //Max Count
                 " AND ( " + _MaxCount + " = '-1' OR " + _MaxCount + " > " + _CurrCount + ')';

            /**
             * Check the Actual Date and Time
             */
            sWhere += " AND (ProcDate Is NULL OR (CONVERT(DATETIME,ProcDate,126) = CONVERT(DATETIME,'" + sActionDate + "',126))) " +
                    " AND (ProcTime Is NULL OR '" + sHour + ':' + sMin + "' LIKE ProcTime+'%') ";

            /**
             * Check the Month and Day of Month
             */
            sWhere += "	AND (PMonth Is NULL Or PMonth = 0 Or PMonth = " + oActionDate.Month + ") " +
	                " AND (MonthDay Is NULL Or MonthDay = 0 Or MonthDay = " + oActionDate.Day + ") ";

            /**
             * Check the Weekday Times
             */
            sWhere += "	AND ( ( Su Is NULL " +
			        " AND Mo Is NULL " +
			        " AND Tu Is NULL " +
			        " AND We Is NULL " +
			        " AND Th Is NULL " +
			        " AND Fr Is NULL " +
			        " AND Sa Is NULL ) " +
                    "  Or ( '" + sHour + ':' + sMin + "' LIKE " + sDayOfWeek + "+'%') " +
		            " ) ";

            /**
             * Exclude the Schedules run within the set period
             */
            sWhere += " AND   ( " +
                      "         ( " +
                      "             ProcDate IS NULL" +
                      "             AND (PMonth IS NULL OR PMonth = 0)" +
                      "             AND (MonthDay IS NULL OR MonthDay = 0)" +
                      "             AND ProcTime IS NULL" +
                      "             AND ProcTime IS NULL" +
                      "             AND Su IS NULL" +
                      "             AND Mo IS NULL" +
                      "             AND Tu IS NULL" +
                      "             AND We IS NULL" +
                      "             AND Th IS NULL" +
                      "             AND Fr IS NULL" +
                      "             AND Sa IS NULL" +
                      "         )" +
                      "         OR ( " +
                      "             SchedId Not In ( " + doCompileExclusionList(sActionDate, sDayOfWeek) + ")" +
                      "         ) " +
                      "       )";
            return getData(sWhere, _Class) > 1;
        }

        /**
         * Compile the exclusive - already run Schedules
         */
        private string doCompileExclusionList(string sActionDate, string sDayOfWeek) {
            string sSubQuery = "SELECT SchedId FROM [IDH_Scheduler] WHERE ";

            /**
             * The Start and End Date Checks
             */
            sSubQuery += " (NOT " + _Status + " = '" + STATUS_DELETED + "' AND NOT " + _Status + " = '" + STATUS_BUSY + "' AND NOT " + _Status + " = '" + STATUS_PAUSED + "' AND NOT " + _Status + " = '" + STATUS_EXCEPTION + "')" + // Not Deleted and Not Busy
                 " AND (" + _StartDate + " Is NULL OR (CONVERT(DATETIME," + _StartDate + ",126) <= CONVERT(DATETIME,'" + sActionDate + "',126))) " + //StartDate after action Date
                 " AND (" + _EndDate + " Is NULL OR (CONVERT(DATETIME," + _EndDate + ",126) >= CONVERT(DATETIME,'" + sActionDate + "',126))) " + //EndDate Later than action Date
                 //" AND " + _MaxCount + " < " + _CurrCount; //Max Count
                 " AND ( " + _MaxCount + " = '-1' OR " + _MaxCount + " > " + _CurrCount + ')';

            /**
             * Check the Actual Date and Time
             */
            sSubQuery += " AND (ProcDate Is NULL OR (CONVERT(DATETIME,ProcDate,126) = CONVERT(DATETIME, LastRun, 126))) ";

            sSubQuery += " AND ( " +
                    "       ProcTime Is NULL " +
                    "       OR ( " +
                    "           CONVERT(DATETIME, LastRun, 126) = CONVERT(DATETIME, '" + sActionDate + "', 126) " +
                    "           AND LastRnTm LIKE ProcTime+'%' " +
                    "       ) " +
                    " ) ";

            /**
             * Check the Month and Day of Month
             */
            sSubQuery += " AND ( " +
                    "       PMonth Is NULL " +
                    "       Or PMonth = 0 " +
                    "       Or ( " +
                    "           DATEPART(year, LastRun) = DATEPART(year, + CONVERT(DATETIME, '" + sActionDate + "', 126)) " +
                    "           AND PMonth = DATEPART(month, LastRun) " +
                    "       ) " +
                    " ) " ;

            sSubQuery += " AND ( " +
                    "       MonthDay Is NULL " +
                    "       Or MonthDay = 0 " +
                    "       Or ( " +
                    "           CONVERT(DATETIME, LastRun, 126) = CONVERT(DATETIME, '" + sActionDate + "', 126) " +
                    "           AND MonthDay = DATEPART(day, LastRun) " +
                    "       ) " +
                    " ) ";

            /**
             * Check the Weekday Times
             */
            sSubQuery += "	AND ( " +
                    "   ( Su Is NULL " +
                    "       AND Mo Is NULL " +
                    "       AND Tu Is NULL " +
                    "       AND We Is NULL " +
                    "       AND Th Is NULL " +
                    "       AND Fr Is NULL " +
                    "       AND Sa Is NULL " +
                    "   ) " +
                    "   Or ( " +
                    "       CONVERT(DATETIME, LastRun, 126) = CONVERT(DATETIME, '" + sActionDate + "', 126) " +
					"	    AND LastRnTm LIKE " + sDayOfWeek + "+'%' " +
                    "      ) " +
                    " ) ";

            return sSubQuery;
        }

        public override void doApplyDefaults() {
            base.doApplyDefaults();

            try {
                doApplyDefaultValue(_MaxCount, "-1");
            } catch (Exception) { }
        }

        ///**
        // * Mark All as Busy"
        // */
        //public bool doMarkAllBusy(DateTime oRunDate) {
        //    int iCurrRow = CurrentRowIndex;
        //    first();
        //    while ( next() ) {
        //        doSetStatusWithAction("Busy", oRunDate);
        //    }
        //    gotoRow(iCurrRow);
        //    return doProcessData();
        //}

        /**
         * Set the statuses and LastRun Date for all returned Rows.
        */
        public bool doMarkAllStatuses(string sStatus) {
            return doMarkAllStatuses(sStatus, DateTime.Now);
        }
        public bool doMarkAllStatuses(string sStatus, DateTime oRunDate) {
            int iCurrRow = CurrentRowIndex;
            first();
            while (next()) {
                doSetStatusWithAction(sStatus, oRunDate, false);
            }
            gotoRow(iCurrRow);
            return doProcessData();
        }

        /**
         * Set Status with Action Date"
         */
        public bool doSetStatusWithAction(string sStatus) {
            return doSetStatusWithAction(sStatus, DateTime.Now);
        }
        public bool doSetStatusWithAction(string sStatus, DateTime oRunDate) {
            return doSetStatusWithAction( sStatus, oRunDate, true );
        }
        public bool doSetStatusWithAction(string sStatus, DateTime oRunDate, bool bDoProcess) {
            Status = sStatus;
            if (oRunDate != null && oRunDate != DateTime.MinValue) {
                LastRun = oRunDate;
                LastRnTm = (new Dates.Time(oRunDate.Hour, oRunDate.Minute)).STRTime;
            }
            if (bDoProcess)
                return doProcessData();
            else
                return true;
        }
	}
}
