/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 05/02/2016 15:01:44
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
   [Serializable] 
	public class IDH_ENQSMS: com.idh.dbObjects.Base.IDH_ENQSMS{ 

		public IDH_ENQSMS() : base() {
            msAutoNumKey = "IDHENQSM";
		}

   	public IDH_ENQSMS( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
            msAutoNumKey = "IDHENQSM";
        }
	}
}
