/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 13/08/2015 16:50:05
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
   [Serializable] 
	public class IDH_WGVALDS: com.idh.dbObjects.Base.IDH_WGVALDS{ 

		public IDH_WGVALDS() : base() {
            msAutoNumKey = "WGPVALDS";
            this.mbDoAuditTrail = true;
		}

   	public IDH_WGVALDS( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	}
	}
}
