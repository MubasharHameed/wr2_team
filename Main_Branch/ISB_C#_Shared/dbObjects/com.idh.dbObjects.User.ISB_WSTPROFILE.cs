/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 10/07/2015 09:47:37
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
	public class ISB_WSTPROFILE: com.idh.dbObjects.Base.ISB_WSTPROFILE{ 

		public ISB_WSTPROFILE() : base() {
			
		}

   	public ISB_WSTPROFILE( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	}
	}
}
