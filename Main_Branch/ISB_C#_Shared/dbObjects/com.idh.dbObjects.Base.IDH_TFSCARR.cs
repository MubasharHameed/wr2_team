/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 12/06/2015 11:42:15
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
	public class IDH_TFSCARR: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
		protected IDHAddOns.idh.forms.Base moIDHForm;

		public IDH_TFSCARR() : base("@IDH_TFSCARR"){
		}

		public IDH_TFSCARR( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_TFSCARR"){
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_TFSCARR";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Carrier Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CarrCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CarrCd = "U_CarrCd";
		public string U_CarrCd { 
			get {
 				return getValueAsString(_CarrCd); 
			}
			set { setValue(_CarrCd, value); }
		}
           public string doValidate_CarrCd() {
               return doValidate_CarrCd(U_CarrCd);
           }
           public virtual string doValidate_CarrCd(object oValue) {
               return base.doValidation(_CarrCd, oValue);
           }

		/**
		 * Decription: State Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_StateTyp
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _StateTyp = "U_StateTyp";
		public string U_StateTyp { 
			get {
 				return getValueAsString(_StateTyp); 
			}
			set { setValue(_StateTyp, value); }
		}
           public string doValidate_StateTyp() {
               return doValidate_StateTyp(U_StateTyp);
           }
           public virtual string doValidate_StateTyp(object oValue) {
               return base.doValidation(_StateTyp, oValue);
           }

		/**
		 * Decription: Country
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Country
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Country = "U_Country";
		public string U_Country { 
			get {
 				return getValueAsString(_Country); 
			}
			set { setValue(_Country, value); }
		}
           public string doValidate_Country() {
               return doValidate_Country(U_Country);
           }
           public virtual string doValidate_Country(object oValue) {
               return base.doValidation(_Country, oValue);
           }

		/**
		 * Decription: CompAuth Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CACode
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CACode = "U_CACode";
		public string U_CACode { 
			get {
 				return getValueAsString(_CACode); 
			}
			set { setValue(_CACode, value); }
		}
           public string doValidate_CACode() {
               return doValidate_CACode(U_CACode);
           }
           public virtual string doValidate_CACode(object oValue) {
               return base.doValidation(_CACode, oValue);
           }

		/**
		 * Decription: CompAuth Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CAName
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CAName = "U_CAName";
		public string U_CAName { 
			get {
 				return getValueAsString(_CAName); 
			}
			set { setValue(_CAName, value); }
		}
           public string doValidate_CAName() {
               return doValidate_CAName(U_CAName);
           }
           public virtual string doValidate_CAName(object oValue) {
               return base.doValidation(_CAName, oValue);
           }

		/**
		 * Decription: Entry City
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ENTCity
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ENTCity = "U_ENTCity";
		public string U_ENTCity { 
			get {
 				return getValueAsString(_ENTCity); 
			}
			set { setValue(_ENTCity, value); }
		}
           public string doValidate_ENTCity() {
               return doValidate_ENTCity(U_ENTCity);
           }
           public virtual string doValidate_ENTCity(object oValue) {
               return base.doValidation(_ENTCity, oValue);
           }

		/**
		 * Decription: Entry Country
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ENTCntCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ENTCntCd = "U_ENTCntCd";
		public string U_ENTCntCd { 
			get {
 				return getValueAsString(_ENTCntCd); 
			}
			set { setValue(_ENTCntCd, value); }
		}
           public string doValidate_ENTCntCd() {
               return doValidate_ENTCntCd(U_ENTCntCd);
           }
           public virtual string doValidate_ENTCntCd(object oValue) {
               return base.doValidation(_ENTCntCd, oValue);
           }

		/**
		 * Decription: Exit City
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_EXTCity
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _EXTCity = "U_EXTCity";
		public string U_EXTCity { 
			get {
 				return getValueAsString(_EXTCity); 
			}
			set { setValue(_EXTCity, value); }
		}
           public string doValidate_EXTCity() {
               return doValidate_EXTCity(U_EXTCity);
           }
           public virtual string doValidate_EXTCity(object oValue) {
               return base.doValidation(_EXTCity, oValue);
           }

		/**
		 * Decription: Exit Country
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_EXTCntCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _EXTCntCd = "U_EXTCntCd";
		public string U_EXTCntCd { 
			get {
 				return getValueAsString(_EXTCntCd); 
			}
			set { setValue(_EXTCntCd, value); }
		}
           public string doValidate_EXTCntCd() {
               return doValidate_EXTCntCd(U_EXTCntCd);
           }
           public virtual string doValidate_EXTCntCd(object oValue) {
               return base.doValidation(_EXTCntCd, oValue);
           }

		/**
		 * Decription: TFS Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TFSCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TFSCd = "U_TFSCd";
		public string U_TFSCd { 
			get {
 				return getValueAsString(_TFSCd); 
			}
			set { setValue(_TFSCd, value); }
		}
           public string doValidate_TFSCd() {
               return doValidate_TFSCd(U_TFSCd);
           }
           public virtual string doValidate_TFSCd(object oValue) {
               return base.doValidation(_TFSCd, oValue);
           }

		/**
		 * Decription: TFS No
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TFSNo
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TFSNo = "U_TFSNo";
		public string U_TFSNo { 
			get {
 				return getValueAsString(_TFSNo); 
			}
			set { setValue(_TFSNo, value); }
		}
           public string doValidate_TFSNo() {
               return doValidate_TFSNo(U_TFSNo);
           }
           public virtual string doValidate_TFSNo(object oValue) {
               return base.doValidation(_TFSNo, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(13);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_CarrCd, "Carrier Code", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Carrier Code
			moDBFields.Add(_StateTyp, "State Type", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //State Type
			moDBFields.Add(_Country, "Country", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Country
			moDBFields.Add(_CACode, "CompAuth Code", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //CompAuth Code
			moDBFields.Add(_CAName, "CompAuth Name", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //CompAuth Name
			moDBFields.Add(_ENTCity, "Entry City", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Entry City
			moDBFields.Add(_ENTCntCd, "Entry Country", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Entry Country
			moDBFields.Add(_EXTCity, "Exit City", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Exit City
			moDBFields.Add(_EXTCntCd, "Exit Country", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Exit Country
			moDBFields.Add(_TFSCd, "TFS Code", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //TFS Code
			moDBFields.Add(_TFSNo, "TFS No", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //TFS No

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_CarrCd());
            doBuildValidationString(doValidate_StateTyp());
            doBuildValidationString(doValidate_Country());
            doBuildValidationString(doValidate_CACode());
            doBuildValidationString(doValidate_CAName());
            doBuildValidationString(doValidate_ENTCity());
            doBuildValidationString(doValidate_ENTCntCd());
            doBuildValidationString(doValidate_EXTCity());
            doBuildValidationString(doValidate_EXTCntCd());
            doBuildValidationString(doValidate_TFSCd());
            doBuildValidationString(doValidate_TFSNo());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_CarrCd)) return doValidate_CarrCd(oValue);
            if (sFieldName.Equals(_StateTyp)) return doValidate_StateTyp(oValue);
            if (sFieldName.Equals(_Country)) return doValidate_Country(oValue);
            if (sFieldName.Equals(_CACode)) return doValidate_CACode(oValue);
            if (sFieldName.Equals(_CAName)) return doValidate_CAName(oValue);
            if (sFieldName.Equals(_ENTCity)) return doValidate_ENTCity(oValue);
            if (sFieldName.Equals(_ENTCntCd)) return doValidate_ENTCntCd(oValue);
            if (sFieldName.Equals(_EXTCity)) return doValidate_EXTCity(oValue);
            if (sFieldName.Equals(_EXTCntCd)) return doValidate_EXTCntCd(oValue);
            if (sFieldName.Equals(_TFSCd)) return doValidate_TFSCd(oValue);
            if (sFieldName.Equals(_TFSNo)) return doValidate_TFSNo(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_CarrCd Field to the Form Item.
		 */
		public void doLink_CarrCd(string sControlName){
			moLinker.doLinkDataToControl(_CarrCd, sControlName);
		}
		/**
		 * Link the U_StateTyp Field to the Form Item.
		 */
		public void doLink_StateTyp(string sControlName){
			moLinker.doLinkDataToControl(_StateTyp, sControlName);
		}
		/**
		 * Link the U_Country Field to the Form Item.
		 */
		public void doLink_Country(string sControlName){
			moLinker.doLinkDataToControl(_Country, sControlName);
		}
		/**
		 * Link the U_CACode Field to the Form Item.
		 */
		public void doLink_CACode(string sControlName){
			moLinker.doLinkDataToControl(_CACode, sControlName);
		}
		/**
		 * Link the U_CAName Field to the Form Item.
		 */
		public void doLink_CAName(string sControlName){
			moLinker.doLinkDataToControl(_CAName, sControlName);
		}
		/**
		 * Link the U_ENTCity Field to the Form Item.
		 */
		public void doLink_ENTCity(string sControlName){
			moLinker.doLinkDataToControl(_ENTCity, sControlName);
		}
		/**
		 * Link the U_ENTCntCd Field to the Form Item.
		 */
		public void doLink_ENTCntCd(string sControlName){
			moLinker.doLinkDataToControl(_ENTCntCd, sControlName);
		}
		/**
		 * Link the U_EXTCity Field to the Form Item.
		 */
		public void doLink_EXTCity(string sControlName){
			moLinker.doLinkDataToControl(_EXTCity, sControlName);
		}
		/**
		 * Link the U_EXTCntCd Field to the Form Item.
		 */
		public void doLink_EXTCntCd(string sControlName){
			moLinker.doLinkDataToControl(_EXTCntCd, sControlName);
		}
		/**
		 * Link the U_TFSCd Field to the Form Item.
		 */
		public void doLink_TFSCd(string sControlName){
			moLinker.doLinkDataToControl(_TFSCd, sControlName);
		}
		/**
		 * Link the U_TFSNo Field to the Form Item.
		 */
		public void doLink_TFSNo(string sControlName){
			moLinker.doLinkDataToControl(_TFSNo, sControlName);
		}
#endregion

	}
}
