/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2014/11/12 11:54:02 AM
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
   [Serializable] 
	public class IDH_VMREAS: com.idh.dbObjects.Base.IDH_VMREAS{ 

		public IDH_VMREAS() : base() {
			
		}

   	public IDH_VMREAS( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	}
	}
}
