/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 10/07/2015 09:47:15
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
	public class ISB_WSTPROFILE: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
		protected IDHAddOns.idh.forms.Base moIDHForm;

		public ISB_WSTPROFILE() : base("@ISB_WSTPROFILE"){
		}

		public ISB_WSTPROFILE( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@ISB_WSTPROFILE"){
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@ISB_WSTPROFILE";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Profile No
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ProfileNo
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ProfileNo = "U_ProfileNo";
		public string U_ProfileNo { 
			get {
 				return getValueAsString(_ProfileNo); 
			}
			set { setValue(_ProfileNo, value); }
		}
           public string doValidate_ProfileNo() {
               return doValidate_ProfileNo(U_ProfileNo);
           }
           public virtual string doValidate_ProfileNo(object oValue) {
               return base.doValidation(_ProfileNo, oValue);
           }

		/**
		 * Decription: Description
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Descp
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Descp = "U_Descp";
		public string U_Descp { 
			get {
 				return getValueAsString(_Descp); 
			}
			set { setValue(_Descp, value); }
		}
           public string doValidate_Descp() {
               return doValidate_Descp(U_Descp);
           }
           public virtual string doValidate_Descp(object oValue) {
               return base.doValidation(_Descp, oValue);
           }

		/**
		 * Decription: Valid From
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ValidFm
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _ValidFm = "U_ValidFm";
		public DateTime U_ValidFm { 
			get {
 				return getValueAsDateTime(_ValidFm); 
			}
			set { setValue(_ValidFm, value); }
		}
		public void U_ValidFm_AsString(string value){
			setValue("U_ValidFm", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_ValidFm_AsString(){
			DateTime dVal = getValueAsDateTime(_ValidFm);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_ValidFm() {
               return doValidate_ValidFm(U_ValidFm);
           }
           public virtual string doValidate_ValidFm(object oValue) {
               return base.doValidation(_ValidFm, oValue);
           }

		/**
		 * Decription: Valid To
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ValidTo
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _ValidTo = "U_ValidTo";
		public DateTime U_ValidTo { 
			get {
 				return getValueAsDateTime(_ValidTo); 
			}
			set { setValue(_ValidTo, value); }
		}
		public void U_ValidTo_AsString(string value){
			setValue("U_ValidTo", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_ValidTo_AsString(){
			DateTime dVal = getValueAsDateTime(_ValidTo);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_ValidTo() {
               return doValidate_ValidTo(U_ValidTo);
           }
           public virtual string doValidate_ValidTo(object oValue) {
               return base.doValidation(_ValidTo, oValue);
           }

		/**
		 * Decription: BP Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AddCd
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AddCd = "U_AddCd";
		public string U_AddCd { 
			get {
 				return getValueAsString(_AddCd); 
			}
			set { setValue(_AddCd, value); }
		}
           public string doValidate_AddCd() {
               return doValidate_AddCd(U_AddCd);
           }
           public virtual string doValidate_AddCd(object oValue) {
               return base.doValidation(_AddCd, oValue);
           }

		/**
		 * Decription: BP Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CardCd
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CardCd = "U_CardCd";
		public string U_CardCd { 
			get {
 				return getValueAsString(_CardCd); 
			}
			set { setValue(_CardCd, value); }
		}
           public string doValidate_CardCd() {
               return doValidate_CardCd(U_CardCd);
           }
           public virtual string doValidate_CardCd(object oValue) {
               return base.doValidation(_CardCd, oValue);
           }

		/**
		 * Decription: BP Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CardNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CardNm = "U_CardNm";
		public string U_CardNm { 
			get {
 				return getValueAsString(_CardNm); 
			}
			set { setValue(_CardNm, value); }
		}
           public string doValidate_CardNm() {
               return doValidate_CardNm(U_CardNm);
           }
           public virtual string doValidate_CardNm(object oValue) {
               return base.doValidation(_CardNm, oValue);
           }

		/**
		 * Decription: Item Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItemCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ItemCd = "U_ItemCd";
		public string U_ItemCd { 
			get {
 				return getValueAsString(_ItemCd); 
			}
			set { setValue(_ItemCd, value); }
		}
           public string doValidate_ItemCd() {
               return doValidate_ItemCd(U_ItemCd);
           }
           public virtual string doValidate_ItemCd(object oValue) {
               return base.doValidation(_ItemCd, oValue);
           }

		/**
		 * Decription: Item Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItemNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ItemNm = "U_ItemNm";
		public string U_ItemNm { 
			get {
 				return getValueAsString(_ItemNm); 
			}
			set { setValue(_ItemNm, value); }
		}
           public string doValidate_ItemNm() {
               return doValidate_ItemNm(U_ItemNm);
           }
           public virtual string doValidate_ItemNm(object oValue) {
               return base.doValidation(_ItemNm, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(11);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_ProfileNo, "Profile No", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Profile No
			moDBFields.Add(_Descp, "Description", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Description
			moDBFields.Add(_ValidFm, "Valid From", 4, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Valid From
			moDBFields.Add(_ValidTo, "Valid To", 5, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Valid To
			moDBFields.Add(_AddCd, "BP Address", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //BP Address
			moDBFields.Add(_CardCd, "BP Code", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //BP Code
			moDBFields.Add(_CardNm, "BP Name", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //BP Name
			moDBFields.Add(_ItemCd, "Item Code", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Item Code
			moDBFields.Add(_ItemNm, "Item Name", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Item Name

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_ProfileNo());
            doBuildValidationString(doValidate_Descp());
            doBuildValidationString(doValidate_ValidFm());
            doBuildValidationString(doValidate_ValidTo());
            doBuildValidationString(doValidate_AddCd());
            doBuildValidationString(doValidate_CardCd());
            doBuildValidationString(doValidate_CardNm());
            doBuildValidationString(doValidate_ItemCd());
            doBuildValidationString(doValidate_ItemNm());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_ProfileNo)) return doValidate_ProfileNo(oValue);
            if (sFieldName.Equals(_Descp)) return doValidate_Descp(oValue);
            if (sFieldName.Equals(_ValidFm)) return doValidate_ValidFm(oValue);
            if (sFieldName.Equals(_ValidTo)) return doValidate_ValidTo(oValue);
            if (sFieldName.Equals(_AddCd)) return doValidate_AddCd(oValue);
            if (sFieldName.Equals(_CardCd)) return doValidate_CardCd(oValue);
            if (sFieldName.Equals(_CardNm)) return doValidate_CardNm(oValue);
            if (sFieldName.Equals(_ItemCd)) return doValidate_ItemCd(oValue);
            if (sFieldName.Equals(_ItemNm)) return doValidate_ItemNm(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_ProfileNo Field to the Form Item.
		 */
		public void doLink_ProfileNo(string sControlName){
			moLinker.doLinkDataToControl(_ProfileNo, sControlName);
		}
		/**
		 * Link the U_Descp Field to the Form Item.
		 */
		public void doLink_Descp(string sControlName){
			moLinker.doLinkDataToControl(_Descp, sControlName);
		}
		/**
		 * Link the U_ValidFm Field to the Form Item.
		 */
		public void doLink_ValidFm(string sControlName){
			moLinker.doLinkDataToControl(_ValidFm, sControlName);
		}
		/**
		 * Link the U_ValidTo Field to the Form Item.
		 */
		public void doLink_ValidTo(string sControlName){
			moLinker.doLinkDataToControl(_ValidTo, sControlName);
		}
		/**
		 * Link the U_AddCd Field to the Form Item.
		 */
		public void doLink_AddCd(string sControlName){
			moLinker.doLinkDataToControl(_AddCd, sControlName);
		}
		/**
		 * Link the U_CardCd Field to the Form Item.
		 */
		public void doLink_CardCd(string sControlName){
			moLinker.doLinkDataToControl(_CardCd, sControlName);
		}
		/**
		 * Link the U_CardNm Field to the Form Item.
		 */
		public void doLink_CardNm(string sControlName){
			moLinker.doLinkDataToControl(_CardNm, sControlName);
		}
		/**
		 * Link the U_ItemCd Field to the Form Item.
		 */
		public void doLink_ItemCd(string sControlName){
			moLinker.doLinkDataToControl(_ItemCd, sControlName);
		}
		/**
		 * Link the U_ItemNm Field to the Form Item.
		 */
		public void doLink_ItemNm(string sControlName){
			moLinker.doLinkDataToControl(_ItemNm, sControlName);
		}
#endregion

	}
}
