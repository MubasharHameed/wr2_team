/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2/24/2017 4:00:41 PM
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
   [Serializable] 
	public class IDH_EVNDETAIL: com.idh.dbObjects.Base.IDH_EVNDETAIL{
        private IDH_EVNMASTER moHeader;
        public IDH_EVNMASTER Header {
            get { return moHeader; }
        }

        public IDH_EVNDETAIL() : base() {
            msAutoNumKey = "EVNDET";
        }

        public IDH_EVNDETAIL(IDH_EVNMASTER oParent)
            : base()
        {
            moHeader = oParent;
            msAutoNumKey = "EVNDET";
            OrderByField = _Name;
        }

        public IDH_EVNDETAIL( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
            msAutoNumKey = "EVNDET";
        }
	}
}
