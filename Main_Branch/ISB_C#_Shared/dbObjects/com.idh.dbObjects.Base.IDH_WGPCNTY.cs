/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 2014/10/09 05:21:56 AM
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
   [Serializable]
	public class IDH_WGPCNTY: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
		protected IDHAddOns.idh.forms.Base moIDHForm;

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_WGPCNTY() : base("@IDH_WGPCNTY"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_WGPCNTY( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_WGPCNTY"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_WGPCNTY";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Waste Group Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WstGpCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WstGpCd = "U_WstGpCd";
		public string U_WstGpCd { 
			get {
 				return getValueAsString(_WstGpCd); 
			}
			set { setValue(_WstGpCd, value); }
		}
           public string doValidate_WstGpCd() {
               return doValidate_WstGpCd(U_WstGpCd);
           }
           public virtual string doValidate_WstGpCd(object oValue) {
               return base.doValidation(_WstGpCd, oValue);
           }

		/**
		 * Decription: County Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CntyCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CntyCd = "U_CntyCd";
		public string U_CntyCd { 
			get {
 				return getValueAsString(_CntyCd); 
			}
			set { setValue(_CntyCd, value); }
		}
           public string doValidate_CntyCd() {
               return doValidate_CntyCd(U_CntyCd);
           }
           public virtual string doValidate_CntyCd(object oValue) {
               return base.doValidation(_CntyCd, oValue);
           }

		/**
		 * Decription: Item Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItemCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ItemCd = "U_ItemCd";
		public string U_ItemCd { 
			get {
 				return getValueAsString(_ItemCd); 
			}
			set { setValue(_ItemCd, value); }
		}
           public string doValidate_ItemCd() {
               return doValidate_ItemCd(U_ItemCd);
           }
           public virtual string doValidate_ItemCd(object oValue) {
               return base.doValidation(_ItemCd, oValue);
           }

		/**
		 * Decription: Item Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItemNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ItemNm = "U_ItemNm";
		public string U_ItemNm { 
			get {
 				return getValueAsString(_ItemNm); 
			}
			set { setValue(_ItemNm, value); }
		}
           public string doValidate_ItemNm() {
               return doValidate_ItemNm(U_ItemNm);
           }
           public virtual string doValidate_ItemNm(object oValue) {
               return base.doValidation(_ItemNm, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(6);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_WstGpCd, "Waste Group Code", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Waste Group Code
			moDBFields.Add(_CntyCd, "County Code", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //County Code
			moDBFields.Add(_ItemCd, "Item Code", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Item Code
			moDBFields.Add(_ItemNm, "Item Name", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Item Name

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_WstGpCd());
            doBuildValidationString(doValidate_CntyCd());
            doBuildValidationString(doValidate_ItemCd());
            doBuildValidationString(doValidate_ItemNm());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_WstGpCd)) return doValidate_WstGpCd(oValue);
            if (sFieldName.Equals(_CntyCd)) return doValidate_CntyCd(oValue);
            if (sFieldName.Equals(_ItemCd)) return doValidate_ItemCd(oValue);
            if (sFieldName.Equals(_ItemNm)) return doValidate_ItemNm(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_WstGpCd Field to the Form Item.
		 */
		public void doLink_WstGpCd(string sControlName){
			moLinker.doLinkDataToControl(_WstGpCd, sControlName);
		}
		/**
		 * Link the U_CntyCd Field to the Form Item.
		 */
		public void doLink_CntyCd(string sControlName){
			moLinker.doLinkDataToControl(_CntyCd, sControlName);
		}
		/**
		 * Link the U_ItemCd Field to the Form Item.
		 */
		public void doLink_ItemCd(string sControlName){
			moLinker.doLinkDataToControl(_ItemCd, sControlName);
		}
		/**
		 * Link the U_ItemNm Field to the Form Item.
		 */
		public void doLink_ItemNm(string sControlName){
			moLinker.doLinkDataToControl(_ItemNm, sControlName);
		}
#endregion

	}
}
