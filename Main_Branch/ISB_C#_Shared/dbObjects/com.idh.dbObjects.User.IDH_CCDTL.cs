/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2015-09-02 12:07:56 PM
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
   [Serializable] 
	public class IDH_CCDTL: com.idh.dbObjects.Base.IDH_CCDTL{ 

		public IDH_CCDTL() : base() {
            msAutoNumKey = "CCSEQ";
		}

   	    public IDH_CCDTL( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
            msAutoNumKey = "CCSEQ";
   	    }
	}
}
