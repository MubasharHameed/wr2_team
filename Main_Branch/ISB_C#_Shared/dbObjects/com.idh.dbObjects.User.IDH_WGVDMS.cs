/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 13/08/2015 16:49:35
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.bridge;
namespace com.idh.dbObjects.User{
   [Serializable] 
	public class IDH_WGVDMS: com.idh.dbObjects.Base.IDH_WGVDMS{ 

		public IDH_WGVDMS() : base() {
            msAutoNumKey = "WGPVLDMS";
            this.mbDoAuditTrail = true;
		}

   	public IDH_WGVDMS( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	}
	}

}
