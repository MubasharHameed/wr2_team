/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2014/05/21 03:43:57 PM
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using IDHAddOns.idh.controls;
using com.idh.bridge.lookups;
using com.idh.controls;
using com.idh.controls.strct;

namespace com.idh.dbObjects.User{

	public class IDH_FORMSET: com.idh.dbObjects.Base.IDH_FORMSET{
        private string msGridId = "";
        private string msCapacityQuery = "";

        private int miDeptCode = -1;
        private string msUserName = IDHAddOns.idh.addon.Base.PARENT.gsUserName;
        private string msGridName = "";
        private string msFormTypeId = "";
        private string msFormTitle = "";
        private int miLastIndex = 0;

        private bool mbAllreadyInSynched = false;
        public bool AllreadySynched {
            get { return mbAllreadyInSynched; }
        }

        private bool mbSkipFormSettings = Config.INSTANCE.getParameterAsBool("FORMSET", false);
        public bool SkipFormSettings {
            set { mbSkipFormSettings = value; }
            get { return mbSkipFormSettings; }
        }

		public IDH_FORMSET() : base() {
            msAutoNumKey = "FRMSET";
		}

   	    public IDH_FORMSET( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
            msAutoNumKey = "FRMSET";
        }

        public int getFormGridFormSettings(string sFormTypeID, string sGridId, string sCapacityQuery) {
            mbSkipFormSettings = !Config.INSTANCE.getParameterAsBool("FORMSET", false);
            if (mbSkipFormSettings)
                return 0;

            msCapacityQuery = sCapacityQuery;
            msGridId = sGridId;
            string sGridFilter = " And IsNull(U_GridName,'" + msGridId +"')='" + msGridId + "'";
            string sUserName = IDHAddOns.idh.addon.Base.PARENT.gsUserName;
            string sWhere = "U_FormID='" + sFormTypeID + "' And U_UserName='" + sUserName + "'" + sGridFilter;
            string sOrderBy = "U_Index Asc, Cast(code as Int)";
            if (getData(sWhere, sOrderBy) == 0) {
                sWhere = "U_FormID='" + sFormTypeID + "' And U_UserName='All' And U_DeptCode=(Select Department from OUSR Where OUSR.USER_CODE='" + sUserName + "')" + sGridFilter;
                sOrderBy = "U_Index Asc, Cast(code as Int) Asc";
                if (getData(sWhere, sOrderBy) == 0) {
                    sWhere = "U_FormID='" + sFormTypeID + "' And U_UserName='template'" + sGridFilter;
                    sOrderBy = "U_Index Asc, Cast(code as Int) Asc";
                    getData(sWhere, sOrderBy);
                }
            }
            return Count;
        }

        public int doAddFieldToGrid(IDHAddOns.idh.controls.UpdateGrid oGridN) {
            if (mbSkipFormSettings)
                miLastIndex = -1;
            else {
                SAPbouiCOM.Form oForm = oGridN.getSBOForm();
                msFormTypeId = oForm.TypeEx;
                msFormTitle = oForm.Title;
                miLastIndex = 0;
                if (Count > 0) {
                    string sFieldOrderBy = "";
                    string sCompleteOrderBy = "";
                    first();
                    msUserName = U_UserName;
                    miDeptCode = U_DeptCode;
                    msGridName = U_GridName;
                    while (next()) {
                        sFieldOrderBy = U_OrderBy;

                        if (!Config.INSTANCE.getParameterAsBool("OSM4DCOV", false) && U_FieldId.StartsWith("f.") && (msFormTypeId == "IDHJOBR4" || msFormTypeId == "IDHJOBR5")) {
                            //If goParent.goLookup.getParameterAsBool("OSM4DCOV", False) = False AndAlso sFieldId.StartsWith("f.") Then
                            mbSkipFormSettings = true;
                            continue;
                            //End If
                        }
                        else if (msFormTypeId == "IDHVECR3" && oGridN.GridId == "VEHGRID" && U_FieldId == "doGetCapacityQuery()") {
                            U_FieldId = msCapacityQuery;
                        }

                        ListField oField = oGridN.doAddFormSettingListField(
                            U_FieldId,
                            U_Title,
                            U_Editable == "Y" ? true : false,
                            U_Width,
                            U_sType.Length == 0 ? null : U_sType,
                            U_sUID.Length == 0 ? null : U_sUID,
                            U_iBackColor <= 0 ? -1 : U_iBackColor,
                            (SAPbouiCOM.BoLinkedObject)(U_oLinkObject <= 0 ? -1 : U_oLinkObject),
                            U_bWidthIsMax.Length > 0 && U_bWidthIsMax == "Y" ? true : false,
                            U_bPinned.Length == 0 || U_bPinned == "Y" ? true : false,
                            (SAPbouiCOM.BoColumnSumType)(U_oSumType == -1 ? 0 : U_oSumType),
                            (SAPbouiCOM.BoColumnDisplayType)(U_oDisplayType == -1 ? 0 : U_oDisplayType),
                            U_Index);

                        if (sFieldOrderBy.Length > 0) {
                            sCompleteOrderBy += ", " + U_FieldId + " " + sFieldOrderBy;
                        }
                        miLastIndex = U_Index > miLastIndex ? U_Index : miLastIndex;
                    }

                    miLastIndex++;

                    if (sCompleteOrderBy.Trim().Length > 0) {
                        sCompleteOrderBy = sCompleteOrderBy.Trim().TrimStart(',');
                        oGridN.setOrderValue(sCompleteOrderBy);
                    }
                }
                else {
                    //iIndex = 1;
                    msGridName = msGridId;
                }
            }
            return miLastIndex;
        }

        public bool doSyncDB(IDHAddOns.idh.controls.UpdateGrid oGridN) {
            if (mbSkipFormSettings)
                return false;

            if (miLastIndex < 0)
                return false;
            if (Config.INSTANCE.getParameterAsBool("FSSYNCSP", false))
                return false;

            if (!mbAllreadyInSynched) {
                //Now Find the Grid Columns that was not in the Formsettings
                int ideptCode = -1;
                SAPbobsCOM.Recordset oRecordSet = IDHAddOns.idh.addon.Base.PARENT.goDB.doSelectQuery("Select isnull(Department,-1) as Department from OUSR Where OUSR.USER_CODE='" + msUserName + "'");
                if (oRecordSet!=null && oRecordSet.RecordCount > 0)
                {
                    ideptCode = (int)oRecordSet.Fields.Item("Department").Value;
				}
                IDH_FORMSET objForSetForAll = new IDH_FORMSET();
                objForSetForAll.msGridId = msGridId;
                objForSetForAll.msCapacityQuery = msCapacityQuery;
                objForSetForAll.miDeptCode = ideptCode;
                objForSetForAll.msUserName = "All";
                objForSetForAll.msGridName = msGridName;
                objForSetForAll.msFormTypeId = msFormTypeId;
                objForSetForAll.msFormTitle = msFormTitle;
                objForSetForAll.miLastIndex = miLastIndex;


                ListFields oFields = oGridN.getGridControl().getListFields();
                foreach (ListField oTField in oFields) {
                    //if (!oTField.mbInFormSettings) {
                    if (!oTField.mbInFormSettings) {
                        doAddEmptyRow(true);
                        Code = ""; // "-1";
                        Name = ""; // "-1";
                        U_UserName = msUserName;
                        U_DeptCode = miDeptCode;
                        U_FormID = msFormTypeId;
                        U_FormName = msFormTitle;
                        U_GridName = msGridName == null || msGridName.Length == 0 ? null : msGridName;

                        U_Title = oTField.msFieldTitle;
                        U_Name = oTField.msFieldTitle;
                        if (msFormTypeId == "IDHVECR3" && oGridN.GridId == "VEHGRID" && oTField.msFieldName == msCapacityQuery)
                            U_FieldId = "doGetCapacityQuery()";
                        else {
                            U_FieldId = oTField.msFieldName;
                        }

                        U_Editable = oTField.mbCanEdit ? "Y" : "N";
                        U_Width = oTField.miWidth;
                        U_sType = oTField.msFieldType == null ? "" : oTField.msFieldType;
                        U_sUID = oTField.msFieldId == null || oTField.msFieldId == oTField.msFieldName ? "" : oTField.msFieldId;
                        U_iBackColor = oTField.miBackColour;
                        U_oLinkObject = (int)oTField.moLinkObject;
                        U_bWidthIsMax = oTField.mbIsMaxWidth ? "Y" : "N";
                        U_bPinned = oTField.mbPinned ? "Y" : "N";
                        U_oSumType = oTField.moSumType == 0 ? -1 : (int)oTField.moSumType;
                        U_oDisplayType = oTField.moDisplayType == 0 ? -1 : (int)oTField.moDisplayType;
                        U_Index = miLastIndex++;
                        U_OrderBy = "";

                        if (msUserName == "manager")
                        {
                        objForSetForAll.doAddEmptyRow(true);
                            objForSetForAll.Code = ""; // "-1";
                            objForSetForAll.Name = ""; // "-1";
                        objForSetForAll.U_UserName = "All";
                        objForSetForAll.U_DeptCode = ideptCode;// -2;
                        objForSetForAll.U_FormID = U_FormID;
                        objForSetForAll.U_FormName = U_FormName;
                        objForSetForAll.U_GridName = U_GridName;// msGridName == null || msGridName.Length == 0 ? null : msGridName;

                        objForSetForAll.U_Title = U_Title ;
                        objForSetForAll.U_Name = U_Name;
                        objForSetForAll.U_FieldId = U_FieldId;

                        objForSetForAll.U_Editable = U_Editable;
                        objForSetForAll.U_Width = U_Width;
                        objForSetForAll.U_sType = U_sType;
                        objForSetForAll.U_sUID = U_sUID;
                        objForSetForAll.U_iBackColor = U_iBackColor;
                        objForSetForAll.U_oLinkObject = U_oLinkObject;
                        objForSetForAll.U_bWidthIsMax = U_bWidthIsMax;
                        objForSetForAll.U_bPinned = U_bPinned;
                        objForSetForAll.U_oSumType = U_oSumType;
                        objForSetForAll.U_oDisplayType = U_oDisplayType;
                        objForSetForAll.U_Index = objForSetForAll.miLastIndex++;
                        objForSetForAll.U_OrderBy = "";
                        }
                    }
                }
                if (AddedRows.Count > 0) {
                    if (doProcessData() == true && msUserName == "manager")
                    objForSetForAll.doProcessData();

                }
                mbAllreadyInSynched = true;
            }
            return true;
        }

        public bool doSetGrid(IDHAddOns.idh.controls.UpdateGrid oGridN) {
            if (mbSkipFormSettings)
                return false;

            doAddFieldToGrid(oGridN);

            //SAPbouiCOM.Form oForm = oGridN.getSBOForm();
            //string sFormTypeId = oForm.TypeEx;
            //string sFormTitle = oForm.Title;
            //int iIndex = 0;
            //if (Count > 0) {
            //    string sFieldOrderBy = "";
            //    string sCompleteOrderBy = "";
            //    first();
            //    msUserName = U_UserName;
            //    miDeptCode = U_DeptCode;
            //    msGridName = U_GridName;
            //    while (next()) {
            //        sFieldOrderBy = U_OrderBy;

            //        if (!Config.INSTANCE.getParameterAsBool("OSM4DCOV", false) && U_FieldId.StartsWith("f.") && (sFormTypeId == "IDHJOBR4" || sFormTypeId == "IDHJOBR5")) {
            //            //If goParent.goLookup.getParameterAsBool("OSM4DCOV", False) = False AndAlso sFieldId.StartsWith("f.") Then
            //            mbSkipFormSettings = true;
            //            continue;
            //            //End If
            //        } else if (sFormTypeId == "IDHVECR3" && oGridN.GridId == "VEHGRID" && U_FieldId == "doGetCapacityQuery()") {
            //            U_FieldId = msCapacityQuery;
            //        }

            //        ListField oField = oGridN.doAddFormSettingListField(
            //            U_FieldId,
            //            U_Title,
            //            U_Editable == "Y" ? true : false,
            //            U_Width,
            //            U_sType.Length == 0 ? null : U_sType,
            //            U_sUID.Length == 0 ? null : U_sUID,
            //            U_iBackColor <= 0 ? -1 : U_iBackColor,
            //            (SAPbouiCOM.BoLinkedObject)(U_oLinkObject <= 0 ? -1 : U_oLinkObject),
            //            U_bWidthIsMax.Length > 0 && U_bWidthIsMax == "Y" ? true : false,
            //            U_bPinned.Length == 0 || U_bPinned == "Y" ? true : false,
            //            (SAPbouiCOM.BoColumnSumType)(U_oSumType == -1 ? 0 : U_oSumType),
            //            (SAPbouiCOM.BoColumnDisplayType)(U_oDisplayType == -1 ? 0 : U_oDisplayType),
            //            U_Index);

            //        if (sFieldOrderBy.Length > 0) {
            //            sCompleteOrderBy += ", " + U_FieldId + " " + sFieldOrderBy;
            //        }
            //        iIndex = U_Index > iIndex ? U_Index : iIndex;
            //    }

            //    iIndex++;

            //    if (sCompleteOrderBy.Trim().Length > 0) {
            //        sCompleteOrderBy = sCompleteOrderBy.Trim().TrimStart(',');
            //        oGridN.setOrderValue(sCompleteOrderBy);
            //    }
            //} else {
            //    //iIndex = 1;
            //    msGridName = msGridId;
            //}

            return doSyncDB(oGridN);

            //if (mbSkipFormSettings)
            //    return false;

            //if (!mbAllreadyInSynched) {
            //    //Now Find the Grid Columns that was not in the Formsettings
            //    ListFields oFields = oGridN.getGridControl().getListFields();
            //    foreach (ListField oTField in oFields) {
            //        //if (!oTField.mbInFormSettings) {
            //        if (!oTField.mbInFormSettings)
            //        {
            //            doAddEmptyRow(true);
            //            Code = "-1";
            //            Name = "-1";
            //            U_UserName = msUserName;
            //            U_DeptCode = miDeptCode;
            //            U_FormID = msFormTypeId;
            //            U_FormName = msFormTitle;
            //            U_GridName = msGridName == null || msGridName.Length == 0 ? null : msGridName;

            //            U_Title = oTField.msFieldTitle;
            //            U_Name = oTField.msFieldTitle;
            //            if (msFormTypeId == "IDHVECR3" && oGridN.GridId == "VEHGRID" && oTField.msFieldName == msCapacityQuery)
            //                U_FieldId = "doGetCapacityQuery()";
            //            else {
            //                U_FieldId = oTField.msFieldName;
            //            }
                        
            //            U_Editable = oTField.mbCanEdit ? "Y" : "N";
            //            U_Width = oTField.miWidth;
            //            U_sType = oTField.msFieldType == null ? "" : oTField.msFieldType;
            //            U_sUID = oTField.msFieldId == null || oTField.msFieldId == oTField.msFieldName ? "" : oTField.msFieldId;
            //            U_iBackColor = oTField.miBackColour;
            //            U_oLinkObject = (int)oTField.moLinkObject;
            //            U_bWidthIsMax = oTField.mbIsMaxWidth ? "Y" : "N";
            //            U_bPinned = oTField.mbPinned ? "Y" : "N";
            //            U_oSumType = oTField.moSumType == 0 ? -1 : (int)oTField.moSumType;
            //            U_oDisplayType = oTField.moDisplayType == 0 ? -1 : (int)oTField.moDisplayType;
            //            U_Index = iIndex++;
            //            U_OrderBy = "";
            //        }
            //    }
            //    if (moAddedRows.Count > 0) {
            //        doProcessData();
            //    }
            //    mbAllreadyInSynched = true;
            //}
            //return true;
        }
	}
}
