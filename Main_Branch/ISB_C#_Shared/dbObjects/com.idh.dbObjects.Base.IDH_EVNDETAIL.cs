/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 2/24/2017 4:00:48 PM
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
   [Serializable]
	public class IDH_EVNDETAIL: com.idh.dbObjects.DBBase { 

		private Linker moLinker = null;
       public Linker ControlLinker {
           get { return moLinker; }
           set { moLinker = value; }
       }

       private IDHAddOns.idh.forms.Base moIDHForm;
       public IDHAddOns.idh.forms.Base IDHForm {
           get { return moIDHForm; }
           set { moIDHForm = value; }
       }

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_EVNDETAIL() : base("@IDH_EVNDETAIL"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_EVNDETAIL( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_EVNDETAIL"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_EVNDETAIL";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Bundesland
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BULAND
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BULAND = "U_BULAND";
		public string U_BULAND { 
			get {
 				return getValueAsString(_BULAND); 
			}
			set { setValue(_BULAND, value); }
		}
           public string doValidate_BULAND() {
               return doValidate_BULAND(U_BULAND);
           }
           public virtual string doValidate_BULAND(object oValue) {
               return base.doValidation(_BULAND, oValue);
           }

		/**
		 * Decription: Header Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HDRCode
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _HDRCode = "U_HDRCode";
		public string U_HDRCode { 
			get {
 				return getValueAsString(_HDRCode); 
			}
			set { setValue(_HDRCode, value); }
		}
           public string doValidate_HDRCode() {
               return doValidate_HDRCode(U_HDRCode);
           }
           public virtual string doValidate_HDRCode(object oValue) {
               return base.doValidation(_HDRCode, oValue);
           }

		/**
		 * Decription: Kreis
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_KREIS
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _KREIS = "U_KREIS";
		public string U_KREIS { 
			get {
 				return getValueAsString(_KREIS); 
			}
			set { setValue(_KREIS, value); }
		}
           public string doValidate_KREIS() {
               return doValidate_KREIS(U_KREIS);
           }
           public virtual string doValidate_KREIS(object oValue) {
               return base.doValidation(_KREIS, oValue);
           }

		/**
		 * Decription: Kreiskennung
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_KREISKE
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _KREISKE = "U_KREISKE";
		public string U_KREISKE { 
			get {
 				return getValueAsString(_KREISKE); 
			}
			set { setValue(_KREISKE, value); }
		}
           public string doValidate_KREISKE() {
               return doValidate_KREISKE(U_KREISKE);
           }
           public virtual string doValidate_KREISKE(object oValue) {
               return base.doValidation(_KREISKE, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(6);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_BULAND, "Bundesland", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Bundesland
			moDBFields.Add(_HDRCode, "Header Code", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Header Code
			moDBFields.Add(_KREIS, "Kreis", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Kreis
			moDBFields.Add(_KREISKE, "Kreiskennung", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Kreiskennung

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_BULAND());
            doBuildValidationString(doValidate_HDRCode());
            doBuildValidationString(doValidate_KREIS());
            doBuildValidationString(doValidate_KREISKE());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_BULAND)) return doValidate_BULAND(oValue);
            if (sFieldName.Equals(_HDRCode)) return doValidate_HDRCode(oValue);
            if (sFieldName.Equals(_KREIS)) return doValidate_KREIS(oValue);
            if (sFieldName.Equals(_KREISKE)) return doValidate_KREISKE(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_BULAND Field to the Form Item.
		 */
		public void doLink_BULAND(string sControlName){
			moLinker.doLinkDataToControl(_BULAND, sControlName);
		}
		/**
		 * Link the U_HDRCode Field to the Form Item.
		 */
		public void doLink_HDRCode(string sControlName){
			moLinker.doLinkDataToControl(_HDRCode, sControlName);
		}
		/**
		 * Link the U_KREIS Field to the Form Item.
		 */
		public void doLink_KREIS(string sControlName){
			moLinker.doLinkDataToControl(_KREIS, sControlName);
		}
		/**
		 * Link the U_KREISKE Field to the Form Item.
		 */
		public void doLink_KREISKE(string sControlName){
			moLinker.doLinkDataToControl(_KREISKE, sControlName);
		}
#endregion

	}
}
