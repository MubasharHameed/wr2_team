/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2015-07-13 02:55:17 PM
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.numbers;

namespace com.idh.dbObjects.User
{
    [Serializable]
    public class IDH_TRAIN01_LNS : com.idh.dbObjects.Base.IDH_TRAIN01_LNS
    {
        private IDH_TRAIN01 moHeader;
        public IDH_TRAIN01 Header
        {
            get { return moHeader; }
        }

        public IDH_TRAIN01_LNS()
            : base()
        {
            msAutoNumKey = "TRN1KEYL";
        }

        public IDH_TRAIN01_LNS(IDH_TRAIN01 oParent)
            : base()
        {
            moHeader = oParent;
            msAutoNumKey = "TRN1KEYL";
            OrderByField = _Name;
        }

        public IDH_TRAIN01_LNS(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oIDHForm, oForm)
        {
            msAutoNumKey = "TRN1KEYL";
        }

        public int getByHeaderNumber(string sHeader)
        {
            return getData(IDH_TRAIN01_LNS._HDRCode + " = '" + sHeader + '\'', IDH_TRAIN01_LNS._Code);
        }

        public override void doApplyDefaults()
        {
            base.doApplyDefaults();

            try
            {
                //doApplyDefaultValue(_Code, "-1");
                //doApplyDefaultValue(_Name, "-1");
                NumbersPair oNextNum;
                oNextNum = this.getNewKey();
                doApplyDefaultValue(_Code, oNextNum.CodeNumber.ToString());
                doApplyDefaultValue(_Name, oNextNum.NameNumber.ToString());

                if (Header != null)
                {
                    doApplyDefaultValue(_HDRCode, moHeader.Code);
                }

                doApplyDefaultValue(_ItemCode, "");
                doApplyDefaultValue(_ItemDesc, "");
                doApplyDefaultValue(_QTY, 0);
                doApplyDefaultValue(_Price, 0);
                doApplyDefaultValue(_Total, 0);
            }
            catch (Exception) { }
        }

        public override void doSetFieldHasChanged(int iRow, string sFieldName, object oOldValue, object oNewValue)
        {
            try
            {
                if (sFieldName.Equals(_QTY))
                {
                    U_Total = U_QTY * U_Price;
                }
                else if (sFieldName.Equals(_Price))
                {
                    U_Total = U_QTY * U_Price;
                }
                else if (sFieldName.Equals(_Total))
                {
                    doCalculateTotal();
                }
            }
            catch (Exception e)
            {
                throw new Exception("Field: [" + sFieldName + "] OValue: [" + oOldValue + "] NValue: [" + oNewValue + "] " + e.Message);
            }
        }

        private void doCalculateTotal()
        {
            if (Handler_TotalChanged != null)
            {
                double dTotal = 0;
                for (int r = 0; r < Count; r++)
                {
                    dTotal += getValueAsDouble(r, _Total);
                }
                Handler_TotalChanged(this, dTotal);
            }
        }

        #region ExternalTrigger
        /**
         * et_ExternalTrigger The Total changed. 
         */
        public et_ExternalTriggerWithData Handler_TotalChanged = null;
        #endregion

    }
}
