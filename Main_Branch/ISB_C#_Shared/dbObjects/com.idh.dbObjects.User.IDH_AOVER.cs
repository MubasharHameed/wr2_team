/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2013/06/11 09:17:58 AM
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
      
	public class IDH_AOVER: com.idh.dbObjects.Base.IDH_AOVER{ 

		public IDH_AOVER() : base() {
			
		}

   	    public IDH_AOVER( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	    }
	}
}
