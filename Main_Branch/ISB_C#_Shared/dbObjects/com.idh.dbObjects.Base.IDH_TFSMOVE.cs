/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 12/06/2015 11:41:40
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base{
	public class IDH_TFSMOVE: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
		protected IDHAddOns.idh.forms.Base moIDHForm;

		public IDH_TFSMOVE() : base("@IDH_TFSMOVE"){
		}

		public IDH_TFSMOVE( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_TFSMOVE"){
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_TFSMOVE";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Shipment Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ShpNo
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ShpNo = "U_ShpNo";
		public string U_ShpNo { 
			get {
 				return getValueAsString(_ShpNo); 
			}
			set { setValue(_ShpNo, value); }
		}
           public string doValidate_ShpNo() {
               return doValidate_ShpNo(U_ShpNo);
           }
           public virtual string doValidate_ShpNo(object oValue) {
               return base.doValidation(_ShpNo, oValue);
           }

		/**
		 * Decription: Actual Shipment Dt
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ShpDt
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _ShpDt = "U_ShpDt";
		public DateTime U_ShpDt { 
			get {
 				return getValueAsDateTime(_ShpDt); 
			}
			set { setValue(_ShpDt, value); }
		}
		public void U_ShpDt_AsString(string value){
			setValue("U_ShpDt", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_ShpDt_AsString(){
			DateTime dVal = getValueAsDateTime(_ShpDt);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_ShpDt() {
               return doValidate_ShpDt(U_ShpDt);
           }
           public virtual string doValidate_ShpDt(object oValue) {
               return base.doValidation(_ShpDt, oValue);
           }

		/**
		 * Decription: Carrier Cd
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CarCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CarCd = "U_CarCd";
		public string U_CarCd { 
			get {
 				return getValueAsString(_CarCd); 
			}
			set { setValue(_CarCd, value); }
		}
           public string doValidate_CarCd() {
               return doValidate_CarCd(U_CarCd);
           }
           public virtual string doValidate_CarCd(object oValue) {
               return base.doValidation(_CarCd, oValue);
           }

		/**
		 * Decription: Carrier Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CarNm
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CarNm = "U_CarNm";
		public string U_CarNm { 
			get {
 				return getValueAsString(_CarNm); 
			}
			set { setValue(_CarNm, value); }
		}
           public string doValidate_CarNm() {
               return doValidate_CarNm(U_CarNm);
           }
           public virtual string doValidate_CarNm(object oValue) {
               return base.doValidation(_CarNm, oValue);
           }

		/**
		 * Decription: Shipped
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Shipd
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Shipd = "U_Shipd";
		public string U_Shipd { 
			get {
 				return getValueAsString(_Shipd); 
			}
			set { setValue(_Shipd, value); }
		}
           public string doValidate_Shipd() {
               return doValidate_Shipd(U_Shipd);
           }
           public virtual string doValidate_Shipd(object oValue) {
               return base.doValidation(_Shipd, oValue);
           }

		/**
		 * Decription: UOM
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UOM
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _UOM = "U_UOM";
		public string U_UOM { 
			get {
 				return getValueAsString(_UOM); 
			}
			set { setValue(_UOM, value); }
		}
           public string doValidate_UOM() {
               return doValidate_UOM(U_UOM);
           }
           public virtual string doValidate_UOM(object oValue) {
               return base.doValidation(_UOM, oValue);
           }

		/**
		 * Decription: Balance
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Bal
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Bal = "U_Bal";
		public string U_Bal { 
			get {
 				return getValueAsString(_Bal); 
			}
			set { setValue(_Bal, value); }
		}
           public string doValidate_Bal() {
               return doValidate_Bal(U_Bal);
           }
           public virtual string doValidate_Bal(object oValue) {
               return base.doValidation(_Bal, oValue);
           }

		/**
		 * Decription: Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Status
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Status = "U_Status";
		public string U_Status { 
			get {
 				return getValueAsString(_Status); 
			}
			set { setValue(_Status, value); }
		}
           public string doValidate_Status() {
               return doValidate_Status(U_Status);
           }
           public virtual string doValidate_Status(object oValue) {
               return base.doValidation(_Status, oValue);
           }

		/**
		 * Decription: TFS Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TFSCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TFSCd = "U_TFSCd";
		public string U_TFSCd { 
			get {
 				return getValueAsString(_TFSCd); 
			}
			set { setValue(_TFSCd, value); }
		}
           public string doValidate_TFSCd() {
               return doValidate_TFSCd(U_TFSCd);
           }
           public virtual string doValidate_TFSCd(object oValue) {
               return base.doValidation(_TFSCd, oValue);
           }

		/**
		 * Decription: TFS No
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TFSNo
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TFSNo = "U_TFSNo";
		public string U_TFSNo { 
			get {
 				return getValueAsString(_TFSNo); 
			}
			set { setValue(_TFSNo, value); }
		}
           public string doValidate_TFSNo() {
               return doValidate_TFSNo(U_TFSNo);
           }
           public virtual string doValidate_TFSNo(object oValue) {
               return base.doValidation(_TFSNo, oValue);
           }

		/**
		 * Decription: WR Link Order Row
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WRRow
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WRRow = "U_WRRow";
		public string U_WRRow { 
			get {
 				return getValueAsString(_WRRow); 
			}
			set { setValue(_WRRow, value); }
		}
           public string doValidate_WRRow() {
               return doValidate_WRRow(U_WRRow);
           }
           public virtual string doValidate_WRRow(object oValue) {
               return base.doValidation(_WRRow, oValue);
           }

		/**
		 * Decription: Pre-notification Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PreNotDt
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _PreNotDt = "U_PreNotDt";
		public DateTime U_PreNotDt { 
			get {
 				return getValueAsDateTime(_PreNotDt); 
			}
			set { setValue(_PreNotDt, value); }
		}
		public void U_PreNotDt_AsString(string value){
			setValue("U_PreNotDt", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_PreNotDt_AsString(){
			DateTime dVal = getValueAsDateTime(_PreNotDt);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_PreNotDt() {
               return doValidate_PreNotDt(U_PreNotDt);
           }
           public virtual string doValidate_PreNotDt(object oValue) {
               return base.doValidation(_PreNotDt, oValue);
           }

		/**
		 * Decription: WR Link Order Header
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WRHdr
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WRHdr = "U_WRHdr";
		public string U_WRHdr { 
			get {
 				return getValueAsString(_WRHdr); 
			}
			set { setValue(_WRHdr, value); }
		}
           public string doValidate_WRHdr() {
               return doValidate_WRHdr(U_WRHdr);
           }
           public virtual string doValidate_WRHdr(object oValue) {
               return base.doValidation(_WRHdr, oValue);
           }

		/**
		 * Decription: Receipt Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ReceiptDt
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _ReceiptDt = "U_ReceiptDt";
		public DateTime U_ReceiptDt { 
			get {
 				return getValueAsDateTime(_ReceiptDt); 
			}
			set { setValue(_ReceiptDt, value); }
		}
		public void U_ReceiptDt_AsString(string value){
			setValue("U_ReceiptDt", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_ReceiptDt_AsString(){
			DateTime dVal = getValueAsDateTime(_ReceiptDt);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_ReceiptDt() {
               return doValidate_ReceiptDt(U_ReceiptDt);
           }
           public virtual string doValidate_ReceiptDt(object oValue) {
               return base.doValidation(_ReceiptDt, oValue);
           }

		/**
		 * Decription: Tonnage
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Tonnage
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Tonnage = "U_Tonnage";
		public double U_Tonnage { 
			get {
 				return getValueAsDouble(_Tonnage); 
			}
			set { setValue(_Tonnage, value); }
		}
           public string doValidate_Tonnage() {
               return doValidate_Tonnage(U_Tonnage);
           }
           public virtual string doValidate_Tonnage(object oValue) {
               return base.doValidation(_Tonnage, oValue);
           }

		/**
		 * Decription: Recovery Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RecoveryDt
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _RecoveryDt = "U_RecoveryDt";
		public DateTime U_RecoveryDt { 
			get {
 				return getValueAsDateTime(_RecoveryDt); 
			}
			set { setValue(_RecoveryDt, value); }
		}
		public void U_RecoveryDt_AsString(string value){
			setValue("U_RecoveryDt", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_RecoveryDt_AsString(){
			DateTime dVal = getValueAsDateTime(_RecoveryDt);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_RecoveryDt() {
               return doValidate_RecoveryDt(U_RecoveryDt);
           }
           public virtual string doValidate_RecoveryDt(object oValue) {
               return base.doValidation(_RecoveryDt, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(18);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_ShpNo, "Shipment Number", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Shipment Number
			moDBFields.Add(_ShpDt, "Actual Shipment Dt", 3, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Actual Shipment Dt
			moDBFields.Add(_CarCd, "Carrier Cd", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Carrier Cd
			moDBFields.Add(_CarNm, "Carrier Name", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Carrier Name
			moDBFields.Add(_Shipd, "Shipped", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Shipped
			moDBFields.Add(_UOM, "UOM", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //UOM
			moDBFields.Add(_Bal, "Balance", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Balance
			moDBFields.Add(_Status, "Status", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Status
			moDBFields.Add(_TFSCd, "TFS Code", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //TFS Code
			moDBFields.Add(_TFSNo, "TFS No", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //TFS No
			moDBFields.Add(_WRRow, "WR Link Order Row", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //WR Link Order Row
			moDBFields.Add(_PreNotDt, "Pre-notification Date", 13, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Pre-notification Date
			moDBFields.Add(_WRHdr, "WR Link Order Header", 14, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //WR Link Order Header
			moDBFields.Add(_ReceiptDt, "Receipt Date", 15, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Receipt Date
			moDBFields.Add(_Tonnage, "Tonnage", 16, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Tonnage
			moDBFields.Add(_RecoveryDt, "Recovery Date", 17, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Recovery Date

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_ShpNo());
            doBuildValidationString(doValidate_ShpDt());
            doBuildValidationString(doValidate_CarCd());
            doBuildValidationString(doValidate_CarNm());
            doBuildValidationString(doValidate_Shipd());
            doBuildValidationString(doValidate_UOM());
            doBuildValidationString(doValidate_Bal());
            doBuildValidationString(doValidate_Status());
            doBuildValidationString(doValidate_TFSCd());
            doBuildValidationString(doValidate_TFSNo());
            doBuildValidationString(doValidate_WRRow());
            doBuildValidationString(doValidate_PreNotDt());
            doBuildValidationString(doValidate_WRHdr());
            doBuildValidationString(doValidate_ReceiptDt());
            doBuildValidationString(doValidate_Tonnage());
            doBuildValidationString(doValidate_RecoveryDt());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_ShpNo)) return doValidate_ShpNo(oValue);
            if (sFieldName.Equals(_ShpDt)) return doValidate_ShpDt(oValue);
            if (sFieldName.Equals(_CarCd)) return doValidate_CarCd(oValue);
            if (sFieldName.Equals(_CarNm)) return doValidate_CarNm(oValue);
            if (sFieldName.Equals(_Shipd)) return doValidate_Shipd(oValue);
            if (sFieldName.Equals(_UOM)) return doValidate_UOM(oValue);
            if (sFieldName.Equals(_Bal)) return doValidate_Bal(oValue);
            if (sFieldName.Equals(_Status)) return doValidate_Status(oValue);
            if (sFieldName.Equals(_TFSCd)) return doValidate_TFSCd(oValue);
            if (sFieldName.Equals(_TFSNo)) return doValidate_TFSNo(oValue);
            if (sFieldName.Equals(_WRRow)) return doValidate_WRRow(oValue);
            if (sFieldName.Equals(_PreNotDt)) return doValidate_PreNotDt(oValue);
            if (sFieldName.Equals(_WRHdr)) return doValidate_WRHdr(oValue);
            if (sFieldName.Equals(_ReceiptDt)) return doValidate_ReceiptDt(oValue);
            if (sFieldName.Equals(_Tonnage)) return doValidate_Tonnage(oValue);
            if (sFieldName.Equals(_RecoveryDt)) return doValidate_RecoveryDt(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_ShpNo Field to the Form Item.
		 */
		public void doLink_ShpNo(string sControlName){
			moLinker.doLinkDataToControl(_ShpNo, sControlName);
		}
		/**
		 * Link the U_ShpDt Field to the Form Item.
		 */
		public void doLink_ShpDt(string sControlName){
			moLinker.doLinkDataToControl(_ShpDt, sControlName);
		}
		/**
		 * Link the U_CarCd Field to the Form Item.
		 */
		public void doLink_CarCd(string sControlName){
			moLinker.doLinkDataToControl(_CarCd, sControlName);
		}
		/**
		 * Link the U_CarNm Field to the Form Item.
		 */
		public void doLink_CarNm(string sControlName){
			moLinker.doLinkDataToControl(_CarNm, sControlName);
		}
		/**
		 * Link the U_Shipd Field to the Form Item.
		 */
		public void doLink_Shipd(string sControlName){
			moLinker.doLinkDataToControl(_Shipd, sControlName);
		}
		/**
		 * Link the U_UOM Field to the Form Item.
		 */
		public void doLink_UOM(string sControlName){
			moLinker.doLinkDataToControl(_UOM, sControlName);
		}
		/**
		 * Link the U_Bal Field to the Form Item.
		 */
		public void doLink_Bal(string sControlName){
			moLinker.doLinkDataToControl(_Bal, sControlName);
		}
		/**
		 * Link the U_Status Field to the Form Item.
		 */
		public void doLink_Status(string sControlName){
			moLinker.doLinkDataToControl(_Status, sControlName);
		}
		/**
		 * Link the U_TFSCd Field to the Form Item.
		 */
		public void doLink_TFSCd(string sControlName){
			moLinker.doLinkDataToControl(_TFSCd, sControlName);
		}
		/**
		 * Link the U_TFSNo Field to the Form Item.
		 */
		public void doLink_TFSNo(string sControlName){
			moLinker.doLinkDataToControl(_TFSNo, sControlName);
		}
		/**
		 * Link the U_WRRow Field to the Form Item.
		 */
		public void doLink_WRRow(string sControlName){
			moLinker.doLinkDataToControl(_WRRow, sControlName);
		}
		/**
		 * Link the U_PreNotDt Field to the Form Item.
		 */
		public void doLink_PreNotDt(string sControlName){
			moLinker.doLinkDataToControl(_PreNotDt, sControlName);
		}
		/**
		 * Link the U_WRHdr Field to the Form Item.
		 */
		public void doLink_WRHdr(string sControlName){
			moLinker.doLinkDataToControl(_WRHdr, sControlName);
		}
		/**
		 * Link the U_ReceiptDt Field to the Form Item.
		 */
		public void doLink_ReceiptDt(string sControlName){
			moLinker.doLinkDataToControl(_ReceiptDt, sControlName);
		}
		/**
		 * Link the U_Tonnage Field to the Form Item.
		 */
		public void doLink_Tonnage(string sControlName){
			moLinker.doLinkDataToControl(_Tonnage, sControlName);
		}
		/**
		 * Link the U_RecoveryDt Field to the Form Item.
		 */
		public void doLink_RecoveryDt(string sControlName){
			moLinker.doLinkDataToControl(_RecoveryDt, sControlName);
		}
#endregion

	}
}
