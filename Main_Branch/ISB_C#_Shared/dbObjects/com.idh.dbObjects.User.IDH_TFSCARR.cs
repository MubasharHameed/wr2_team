/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 11/06/2015 23:31:53
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
	public class IDH_TFSCARR: com.idh.dbObjects.Base.IDH_TFSCARR{ 

		public IDH_TFSCARR() : base() {
			
		}

   	public IDH_TFSCARR( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	}

    private IDH_TFSMAST moTFSMaster;
    public IDH_TFSMAST TFSMaster
    {
        get { return moTFSMaster; }
        set { moTFSMaster = value; }
    }

    public int getByTFSNo(string sTFSNo)
    {
        return getData(_TFSNo + " = '" + sTFSNo + "'", null);
    }

    public int getByTFSCode(string sTFSCode)
    {
        return getData(_TFSCd + " = '" + sTFSCode + "'", null);
    }

	}
}
