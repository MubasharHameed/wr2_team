/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 27/01/2016 14:16:52
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

using com.idh.dbObjects.numbers;

namespace com.idh.dbObjects.Base {
    [Serializable]
    public class IDH_WGVDMS : com.idh.dbObjects.DBBase {

        //private Linker moLinker = null;
        //public Linker ControlLinker {
        //    get { return moLinker; }
        //    set { moLinker = value; }
        //}

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_WGVDMS()
            : base("@IDH_WGVDMS") {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_WGVDMS(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oForm, "@IDH_WGVDMS") {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        public NumbersPair getNewKey(bool bSetNewKey) {
            NumbersPair oNumbers = getNewKey();
            Code = oNumbers.CodeCode;
            Name = oNumbers.NameCode;
            return oNumbers;
        }
        #region Properties
        /**
		* Table name
		*/
        public readonly static string TableName = "@IDH_WGVDMS";

        /**
         * Decription: Code
         * Mandatory: tYes
         * Name: Code
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Code = "Code";
        public string Code {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code() {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue) {
            return base.doValidation(_Code, oValue);
        }
        /**
         * Decription: Name
         * Mandatory: tYes
         * Name: Name
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Name = "Name";
        public string Name {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name() {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue) {
            return base.doValidation(_Name, oValue);
        }
        /**
         * Decription: Common Attribute
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Common
         * Size: 1
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Common = "U_Common";
        public string U_Common {
            get {
                return getValueAsString(_Common);
            }
            set { setValue(_Common, value); }
        }
        public string doValidate_Common() {
            return doValidate_Common(U_Common);
        }
        public virtual string doValidate_Common(object oValue) {
            return base.doValidation(_Common, oValue);
        }

        /**
         * Decription: BP Field Names
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_DBField
         * Size: 250
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _DBField = "U_DBField";
        public string U_DBField {
            get {
                return getValueAsString(_DBField);
            }
            set { setValue(_DBField, value); }
        }
        public string doValidate_DBField() {
            return doValidate_DBField(U_DBField);
        }
        public virtual string doValidate_DBField(object oValue) {
            return base.doValidation(_DBField, oValue);
        }

        /**
         * Decription: Data Type
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_DtType
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _DtType = "U_DtType";
        public string U_DtType {
            get {
                return getValueAsString(_DtType);
            }
            set { setValue(_DtType, value); }
        }
        public string doValidate_DtType() {
            return doValidate_DtType(U_DtType);
        }
        public virtual string doValidate_DtType(object oValue) {
            return base.doValidation(_DtType, oValue);
        }

        /**
         * Decription: Expression
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Expression
         * Size: 250
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Expression = "U_Expression";
        public string U_Expression {
            get {
                return getValueAsString(_Expression);
            }
            set { setValue(_Expression, value); }
        }
        public string doValidate_Expression() {
            return doValidate_Expression(U_Expression);
        }
        public virtual string doValidate_Expression(object oValue) {
            return base.doValidation(_Expression, oValue);
        }

        /**
         * Decription: Fields
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Fields
         * Size: 1
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Fields = "U_Fields";
        public string U_Fields {
            get {
                return getValueAsString(_Fields);
            }
            set { setValue(_Fields, value); }
        }
        public string doValidate_Fields() {
            return doValidate_Fields(U_Fields);
        }
        public virtual string doValidate_Fields(object oValue) {
            return base.doValidation(_Fields, oValue);
        }

        /**
         * Decription: Length
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Length
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Length = "U_Length";
        public int U_Length {
            get {
                return getValueAsInt(_Length);
            }
            set { setValue(_Length, value); }
        }
        public string doValidate_Length() {
            return doValidate_Length(U_Length);
        }
        public virtual string doValidate_Length(object oValue) {
            return base.doValidation(_Length, oValue);
        }

        /**
         * Decription: Validation Type
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Type
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Type = "U_Type";
        public string U_Type {
            get {
                return getValueAsString(_Type);
            }
            set { setValue(_Type, value); }
        }
        public string doValidate_Type() {
            return doValidate_Type(U_Type);
        }
        public virtual string doValidate_Type(object oValue) {
            return base.doValidation(_Type, oValue);
        }

        /**
         * Decription: Validation Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ValidCd
         * Size: 250
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ValidCd = "U_ValidCd";
        public string U_ValidCd {
            get {
                return getValueAsString(_ValidCd);
            }
            set { setValue(_ValidCd, value); }
        }
        public string doValidate_ValidCd() {
            return doValidate_ValidCd(U_ValidCd);
        }
        public virtual string doValidate_ValidCd(object oValue) {
            return base.doValidation(_ValidCd, oValue);
        }

        /**
         * Decription: Validation Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ValidNm
         * Size: 250
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ValidNm = "U_ValidNm";
        public string U_ValidNm {
            get {
                return getValueAsString(_ValidNm);
            }
            set { setValue(_ValidNm, value); }
        }
        public string doValidate_ValidNm() {
            return doValidate_ValidNm(U_ValidNm);
        }
        public virtual string doValidate_ValidNm(object oValue) {
            return base.doValidation(_ValidNm, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo() {
            if (moDBFields == null)
                moDBFields = new DBFields(11);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_Common, "Common Attribute", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Common Attribute
            moDBFields.Add(_DBField, "BP Field Names", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //BP Field Names
            moDBFields.Add(_DtType, "Data Type", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Data Type
            moDBFields.Add(_Expression, "Expression", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Expression
            moDBFields.Add(_Fields, "Fields", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Fields
            moDBFields.Add(_Length, "Length", 7, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Length
            moDBFields.Add(_Type, "Validation Type", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Validation Type
            moDBFields.Add(_ValidCd, "Validation Code", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Validation Code
            moDBFields.Add(_ValidNm, "Validation Name", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Validation Name

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation() {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_Common());
            doBuildValidationString(doValidate_DBField());
            doBuildValidationString(doValidate_DtType());
            doBuildValidationString(doValidate_Expression());
            doBuildValidationString(doValidate_Fields());
            doBuildValidationString(doValidate_Length());
            doBuildValidationString(doValidate_Type());
            doBuildValidationString(doValidate_ValidCd());
            doBuildValidationString(doValidate_ValidNm());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_Common)) return doValidate_Common(oValue);
            if (sFieldName.Equals(_DBField)) return doValidate_DBField(oValue);
            if (sFieldName.Equals(_DtType)) return doValidate_DtType(oValue);
            if (sFieldName.Equals(_Expression)) return doValidate_Expression(oValue);
            if (sFieldName.Equals(_Fields)) return doValidate_Fields(oValue);
            if (sFieldName.Equals(_Length)) return doValidate_Length(oValue);
            if (sFieldName.Equals(_Type)) return doValidate_Type(oValue);
            if (sFieldName.Equals(_ValidCd)) return doValidate_ValidCd(oValue);
            if (sFieldName.Equals(_ValidNm)) return doValidate_ValidNm(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName) {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
         * Link the Name Field to the Form Item.
         */
        public void doLink_Name(string sControlName) {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
         * Link the U_Common Field to the Form Item.
         */
        public void doLink_Common(string sControlName) {
            moLinker.doLinkDataToControl(_Common, sControlName);
        }
        /**
         * Link the U_DBField Field to the Form Item.
         */
        public void doLink_DBField(string sControlName) {
            moLinker.doLinkDataToControl(_DBField, sControlName);
        }
        /**
         * Link the U_DtType Field to the Form Item.
         */
        public void doLink_DtType(string sControlName) {
            moLinker.doLinkDataToControl(_DtType, sControlName);
        }
        /**
         * Link the U_Expression Field to the Form Item.
         */
        public void doLink_Expression(string sControlName) {
            moLinker.doLinkDataToControl(_Expression, sControlName);
        }
        /**
         * Link the U_Fields Field to the Form Item.
         */
        public void doLink_Fields(string sControlName) {
            moLinker.doLinkDataToControl(_Fields, sControlName);
        }
        /**
         * Link the U_Length Field to the Form Item.
         */
        public void doLink_Length(string sControlName) {
            moLinker.doLinkDataToControl(_Length, sControlName);
        }
        /**
         * Link the U_Type Field to the Form Item.
         */
        public void doLink_Type(string sControlName) {
            moLinker.doLinkDataToControl(_Type, sControlName);
        }
        /**
         * Link the U_ValidCd Field to the Form Item.
         */
        public void doLink_ValidCd(string sControlName) {
            moLinker.doLinkDataToControl(_ValidCd, sControlName);
        }
        /**
         * Link the U_ValidNm Field to the Form Item.
         */
        public void doLink_ValidNm(string sControlName) {
            moLinker.doLinkDataToControl(_ValidNm, sControlName);
        }
        #endregion

    }
}
