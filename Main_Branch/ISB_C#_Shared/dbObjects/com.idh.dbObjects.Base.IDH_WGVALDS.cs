/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 01/09/2015 17:46:56
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base {
    [Serializable]
    public class IDH_WGVALDS : com.idh.dbObjects.DBBase {

        //private Linker moLinker = null;
        //public Linker ControlLinker {
        //    get { return moLinker; }
        //    set { moLinker = value; }
        //}

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_WGVALDS()
            : base("@IDH_WGVALDS") {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_WGVALDS(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oForm, "@IDH_WGVALDS") {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
		* Table name
		*/
        public readonly static string TableName = "@IDH_WGVALDS";

        /**
         * Decription: Code
         * Mandatory: tYes
         * Name: Code
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Code = "Code";
        public string Code {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code() {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue) {
            return base.doValidation(_Code, oValue);
        }
        /**
         * Decription: Name
         * Mandatory: tYes
         * Name: Name
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Name = "Name";
        public string Name {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name() {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue) {
            return base.doValidation(_Name, oValue);
        }
        /**
         * Decription: Select
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Select
         * Size: 1
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Select = "U_Select";
        public string U_Select {
            get {
                return getValueAsString(_Select);
            }
            set { setValue(_Select, value); }
        }
        public string doValidate_Select() {
            return doValidate_Select(U_Select);
        }
        public virtual string doValidate_Select(object oValue) {
            return base.doValidation(_Select, oValue);
        }

        /**
         * Decription: Sort Order
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Sort
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Sort = "U_Sort";
        public int U_Sort {
            get {
                return getValueAsInt(_Sort);
            }
            set { setValue(_Sort, value); }
        }
        public string doValidate_Sort() {
            return doValidate_Sort(U_Sort);
        }
        public virtual string doValidate_Sort(object oValue) {
            return base.doValidation(_Sort, oValue);
        }

        /**
         * Decription: Validation Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ValidCd
         * Size: 250
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ValidCd = "U_ValidCd";
        public string U_ValidCd {
            get {
                return getValueAsString(_ValidCd);
            }
            set { setValue(_ValidCd, value); }
        }
        public string doValidate_ValidCd() {
            return doValidate_ValidCd(U_ValidCd);
        }
        public virtual string doValidate_ValidCd(object oValue) {
            return base.doValidation(_ValidCd, oValue);
        }

        /**
         * Decription: Validation Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ValidNm
         * Size: 250
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ValidNm = "U_ValidNm";
        public string U_ValidNm {
            get {
                return getValueAsString(_ValidNm);
            }
            set { setValue(_ValidNm, value); }
        }
        public string doValidate_ValidNm() {
            return doValidate_ValidNm(U_ValidNm);
        }
        public virtual string doValidate_ValidNm(object oValue) {
            return base.doValidation(_ValidNm, oValue);
        }

        /**
         * Decription: List of Values
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ValList
         * Size: 10
         * Type: db_Memo
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ValList = "U_ValList";
        public string U_ValList {
            get {
                return getValueAsString(_ValList);
            }
            set { setValue(_ValList, value); }
        }
        public string doValidate_ValList() {
            return doValidate_ValList(U_ValList);
        }
        public virtual string doValidate_ValList(object oValue) {
            return base.doValidation(_ValList, oValue);
        }

        /**
         * Decription: Waste Group Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WstGpCd
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _WstGpCd = "U_WstGpCd";
        public string U_WstGpCd {
            get {
                return getValueAsString(_WstGpCd);
            }
            set { setValue(_WstGpCd, value); }
        }
        public string doValidate_WstGpCd() {
            return doValidate_WstGpCd(U_WstGpCd);
        }
        public virtual string doValidate_WstGpCd(object oValue) {
            return base.doValidation(_WstGpCd, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo() {
            if (moDBFields == null)
                moDBFields = new DBFields(8);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_Select, "Select", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Select
            moDBFields.Add(_Sort, "Sort Order", 3, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Sort Order
            moDBFields.Add(_ValidCd, "Validation Code", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Validation Code
            moDBFields.Add(_ValidNm, "Validation Name", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Validation Name
            moDBFields.Add(_ValList, "List of Values", 6, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //List of Values
            moDBFields.Add(_WstGpCd, "Waste Group Code", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Waste Group Code

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation() {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_Select());
            doBuildValidationString(doValidate_Sort());
            doBuildValidationString(doValidate_ValidCd());
            doBuildValidationString(doValidate_ValidNm());
            doBuildValidationString(doValidate_ValList());
            doBuildValidationString(doValidate_WstGpCd());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_Select)) return doValidate_Select(oValue);
            if (sFieldName.Equals(_Sort)) return doValidate_Sort(oValue);
            if (sFieldName.Equals(_ValidCd)) return doValidate_ValidCd(oValue);
            if (sFieldName.Equals(_ValidNm)) return doValidate_ValidNm(oValue);
            if (sFieldName.Equals(_ValList)) return doValidate_ValList(oValue);
            if (sFieldName.Equals(_WstGpCd)) return doValidate_WstGpCd(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName) {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
         * Link the Name Field to the Form Item.
         */
        public void doLink_Name(string sControlName) {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
         * Link the U_Select Field to the Form Item.
         */
        public void doLink_Select(string sControlName) {
            moLinker.doLinkDataToControl(_Select, sControlName);
        }
        /**
         * Link the U_Sort Field to the Form Item.
         */
        public void doLink_Sort(string sControlName) {
            moLinker.doLinkDataToControl(_Sort, sControlName);
        }
        /**
         * Link the U_ValidCd Field to the Form Item.
         */
        public void doLink_ValidCd(string sControlName) {
            moLinker.doLinkDataToControl(_ValidCd, sControlName);
        }
        /**
         * Link the U_ValidNm Field to the Form Item.
         */
        public void doLink_ValidNm(string sControlName) {
            moLinker.doLinkDataToControl(_ValidNm, sControlName);
        }
        /**
         * Link the U_ValList Field to the Form Item.
         */
        public void doLink_ValList(string sControlName) {
            moLinker.doLinkDataToControl(_ValList, sControlName);
        }
        /**
         * Link the U_WstGpCd Field to the Form Item.
         */
        public void doLink_WstGpCd(string sControlName) {
            moLinker.doLinkDataToControl(_WstGpCd, sControlName);
        }
        #endregion

    }
}
