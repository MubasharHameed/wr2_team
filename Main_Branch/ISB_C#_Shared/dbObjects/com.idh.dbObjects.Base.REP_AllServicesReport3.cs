/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2012/01/30 10:42:12 AM
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;

namespace com.idh.dbObjects.Base{
      
	public class REP_AllServicesReport3: com.idh.dbObjects.DBBase { 

		private static string[] moLocalDBFields = null;
		private static string msLocalSelectList = null;
		//private Linker moLinker = null;
		protected IDHAddOns.idh.forms.Base moIDHForm;

		public REP_AllServicesReport3() : base("REP_AllServicesReport3"){
		}

		public REP_AllServicesReport3( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "REP_AllServicesReport3"){
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
        /**
         * Table name
         */
        public readonly static string TableName = "REP_AllServicesReport3";

		/**
		 * Decription: MON
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: MON
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _MON = "MON";
		public string MON { 
			get {
           		return getValueAsString(_MON); 
           }
			set { setValue(_MON, value); }
		}

		/**
		 * Decription: TUE
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: TUE
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _TUE = "TUE";
		public string TUE { 
			get {
           		return getValueAsString(_TUE); 
           }
			set { setValue(_TUE, value); }
		}

		/**
		 * Decription: WED
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: WED
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _WED = "WED";
		public string WED { 
			get {
           		return getValueAsString(_WED); 
           }
			set { setValue(_WED, value); }
		}

		/**
		 * Decription: THU
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: THU
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _THU = "THU";
		public string THU { 
			get {
           		return getValueAsString(_THU); 
           }
			set { setValue(_THU, value); }
		}

		/**
		 * Decription: FRI
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: FRI
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _FRI = "FRI";
		public string FRI { 
			get {
           		return getValueAsString(_FRI); 
           }
			set { setValue(_FRI, value); }
		}

		/**
		 * Decription: SAT
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: SAT
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _SAT = "SAT";
		public string SAT { 
			get {
           		return getValueAsString(_SAT); 
           }
			set { setValue(_SAT, value); }
		}

		/**
		 * Decription: SUN
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: SUN
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _SUN = "SUN";
		public string SUN { 
			get {
           		return getValueAsString(_SUN); 
           }
			set { setValue(_SUN, value); }
		}

		/**
		 * Decription: Site Clearance
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: Site Clearance
		 * Size: 3
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Site_Clearance = "Site Clearance";
		public string Site_Clearance { 
			get {
           		return getValueAsString(_Site_Clearance); 
           }
			set { setValue(_Site_Clearance, value); }
		}

		/**
		 * Decription: WOHeader
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: WOHeader
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _WOHeader = "WOHeader";
		public string WOHeader { 
			get {
           		return getValueAsString(_WOHeader); 
           }
			set { setValue(_WOHeader, value); }
		}

		/**
		 * Decription: WORow
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: WORow
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _WORow = "WORow";
		public string WORow { 
			get {
           		return getValueAsString(_WORow); 
           }
			set { setValue(_WORow, value); }
		}

		/**
		 * Decription: Lift Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: Lift Type
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Lift_Type = "Lift Type";
		public string Lift_Type { 
			get {
           		return getValueAsString(_Lift_Type); 
           }
			set { setValue(_Lift_Type, value); }
		}

		/**
		 * Decription: Site Fax
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: Site Fax
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Site_Fax = "Site Fax";
		public string Site_Fax { 
			get {
           		return getValueAsString(_Site_Fax); 
           }
			set { setValue(_Site_Fax, value); }
		}

		/**
		 * Decription: JobTrigger
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: JobTrigger
		 * Size: 3
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _JobTrigger = "JobTrigger";
		public string JobTrigger { 
			get {
           		return getValueAsString(_JobTrigger); 
           }
			set { setValue(_JobTrigger, value); }
		}

		/**
		 * Decription: DoneDate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: DoneDate
		 * Size: 0
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _DoneDate = "DoneDate";
		public DateTime DoneDate { 
			get {
           		return getValueAsDateTime(_DoneDate); 
           }
			set { setValue(_DoneDate, value); }
		}
		public void DoneDate_AsString(string value){
			setValue("DoneDate", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string DoneDate_AsString(){
            DateTime dVal = getValueAsDateTime(_DoneDate);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}

		/**
		 * Decription: Customer Reference
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Customer Reference
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Customer_Reference = "Customer Reference";
		public string Customer_Reference { 
			get {
           		return getValueAsString(_Customer_Reference); 
           }
			set { setValue(_Customer_Reference, value); }
		}

		/**
		 * Decription: Site Id
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Site Id
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Site_Id = "Site Id";
		public string Site_Id { 
			get {
           		return getValueAsString(_Site_Id); 
           }
			set { setValue(_Site_Id, value); }
		}

		/**
		 * Decription: Supplier Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Supplier Name
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Supplier_Name = "Supplier Name";
		public string Supplier_Name { 
			get {
           		return getValueAsString(_Supplier_Name); 
           }
			set { setValue(_Supplier_Name, value); }
		}

		/**
		 * Decription: Qty
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Qty
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Qty = "Qty";
		public double Qty { 
			get {
           		return getValueAsDouble(_Qty); 
           }
			set { setValue(_Qty, value); }
		}

		/**
		 * Decription: Txt
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Txt
		 * Size: 54
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Txt = "Txt";
		public string Txt { 
			get {
           		return getValueAsString(_Txt); 
           }
			set { setValue(_Txt, value); }
		}

		/**
		 * Decription: Lifts Per Week
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Lifts Per Week
		 * Size: 38
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Lifts_Per_Week = "Lifts Per Week";
		public double Lifts_Per_Week { 
			get {
           		return getValueAsDouble(_Lifts_Per_Week); 
           }
			set { setValue(_Lifts_Per_Week, value); }
		}

		/**
		 * Decription: Order Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Order Type
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Order_Type = "Order Type";
		public string Order_Type { 
			get {
           		return getValueAsString(_Order_Type); 
           }
			set { setValue(_Order_Type, value); }
		}

		/**
		 * Decription: Container Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Container Code
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Container_Code = "Container Code";
		public string Container_Code { 
			get {
           		return getValueAsString(_Container_Code); 
           }
			set { setValue(_Container_Code, value); }
		}

		/**
		 * Decription: Container Desc
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Container Desc
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Container_Desc = "Container Desc";
		public string Container_Desc { 
			get {
           		return getValueAsString(_Container_Desc); 
           }
			set { setValue(_Container_Desc, value); }
		}

		/**
		 * Decription: Waste Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Waste Code
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Waste_Code = "Waste Code";
		public string Waste_Code { 
			get {
           		return getValueAsString(_Waste_Code); 
           }
			set { setValue(_Waste_Code, value); }
		}

		/**
		 * Decription: Waste Desc
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Waste Desc
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Waste_Desc = "Waste Desc";
		public string Waste_Desc { 
			get {
           		return getValueAsString(_Waste_Desc); 
           }
			set { setValue(_Waste_Desc, value); }
		}

		/**
		 * Decription: Customer Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Customer Code
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Customer_Code = "Customer Code";
		public string Customer_Code { 
			get {
           		return getValueAsString(_Customer_Code); 
           }
			set { setValue(_Customer_Code, value); }
		}

		/**
		 * Decription: Customer Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Customer Name
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Customer_Name = "Customer Name";
		public string Customer_Name { 
			get {
           		return getValueAsString(_Customer_Name); 
           }
			set { setValue(_Customer_Name, value); }
		}

		/**
		 * Decription: Site Address1
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Site Address1
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Site_Address1 = "Site Address1";
		public string Site_Address1 { 
			get {
           		return getValueAsString(_Site_Address1); 
           }
			set { setValue(_Site_Address1, value); }
		}

		/**
		 * Decription: Site Address2
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Site Address2
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Site_Address2 = "Site Address2";
		public string Site_Address2 { 
			get {
           		return getValueAsString(_Site_Address2); 
           }
			set { setValue(_Site_Address2, value); }
		}

		/**
		 * Decription: Site Address3
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Site Address3
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Site_Address3 = "Site Address3";
		public string Site_Address3 { 
			get {
           		return getValueAsString(_Site_Address3); 
           }
			set { setValue(_Site_Address3, value); }
		}

		/**
		 * Decription: Site Address4
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Site Address4
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Site_Address4 = "Site Address4";
		public string Site_Address4 { 
			get {
           		return getValueAsString(_Site_Address4); 
           }
			set { setValue(_Site_Address4, value); }
		}

		/**
		 * Decription: Site Address5
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Site Address5
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Site_Address5 = "Site Address5";
		public string Site_Address5 { 
			get {
           		return getValueAsString(_Site_Address5); 
           }
			set { setValue(_Site_Address5, value); }
		}

		/**
		 * Decription: Post Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Post Code
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Post_Code = "Post Code";
		public string Post_Code { 
			get {
           		return getValueAsString(_Post_Code); 
           }
			set { setValue(_Post_Code, value); }
		}

		/**
		 * Decription: Site Contact
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Site Contact
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Site_Contact = "Site Contact";
		public string Site_Contact { 
			get {
           		return getValueAsString(_Site_Contact); 
           }
			set { setValue(_Site_Contact, value); }
		}

		/**
		 * Decription: Site Tel
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Site Tel
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Site_Tel = "Site Tel";
		public string Site_Tel { 
			get {
           		return getValueAsString(_Site_Tel); 
           }
			set { setValue(_Site_Tel, value); }
		}

		/**
		 * Decription: Cust Disposal Units
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Cust Disposal Units
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Cust_Disposal_Units = "Cust Disposal Units";
		public double Cust_Disposal_Units { 
			get {
           		return getValueAsDouble(_Cust_Disposal_Units); 
           }
			set { setValue(_Cust_Disposal_Units, value); }
		}

		/**
		 * Decription: Cust Read Weight
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Cust Read Weight
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Cust_Read_Weight = "Cust Read Weight";
		public double Cust_Read_Weight { 
			get {
           		return getValueAsDouble(_Cust_Read_Weight); 
           }
			set { setValue(_Cust_Read_Weight, value); }
		}

		/**
		 * Decription: Cust Charge Weight
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Cust Charge Weight
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Cust_Charge_Weight = "Cust Charge Weight";
		public double Cust_Charge_Weight { 
			get {
           		return getValueAsDouble(_Cust_Charge_Weight); 
           }
			set { setValue(_Cust_Charge_Weight, value); }
		}

		/**
		 * Decription: Cust Disposal UOM
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Cust Disposal UOM
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Cust_Disposal_UOM = "Cust Disposal UOM";
		public string Cust_Disposal_UOM { 
			get {
           		return getValueAsString(_Cust_Disposal_UOM); 
           }
			set { setValue(_Cust_Disposal_UOM, value); }
		}

		/**
		 * Decription: Cust Disposal Rate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Cust Disposal Rate
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Cust_Disposal_Rate = "Cust Disposal Rate";
		public double Cust_Disposal_Rate { 
			get {
           		return getValueAsDouble(_Cust_Disposal_Rate); 
           }
			set { setValue(_Cust_Disposal_Rate, value); }
		}

		/**
		 * Decription: Cust Disposal Total
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Cust Disposal Total
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Cust_Disposal_Total = "Cust Disposal Total";
		public double Cust_Disposal_Total { 
			get {
           		return getValueAsDouble(_Cust_Disposal_Total); 
           }
			set { setValue(_Cust_Disposal_Total, value); }
		}

		/**
		 * Decription: Cust Haulage Qty
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Cust Haulage Qty
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Cust_Haulage_Qty = "Cust Haulage Qty";
		public double Cust_Haulage_Qty { 
			get {
           		return getValueAsDouble(_Cust_Haulage_Qty); 
           }
			set { setValue(_Cust_Haulage_Qty, value); }
		}

		/**
		 * Decription: Cust Haulage Rate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Cust Haulage Rate
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Cust_Haulage_Rate = "Cust Haulage Rate";
		public double Cust_Haulage_Rate { 
			get {
           		return getValueAsDouble(_Cust_Haulage_Rate); 
           }
			set { setValue(_Cust_Haulage_Rate, value); }
		}

		/**
		 * Decription: Cust Haulage Total
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Cust Haulage Total
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Cust_Haulage_Total = "Cust Haulage Total";
		public double Cust_Haulage_Total { 
			get {
           		return getValueAsDouble(_Cust_Haulage_Total); 
           }
			set { setValue(_Cust_Haulage_Total, value); }
		}

		/**
		 * Decription: Cust SubTotal Before Discount
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Cust SubTotal Before Discount
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Cust_SubTotal_Before_Discount = "Cust SubTotal Before Discount";
		public double Cust_SubTotal_Before_Discount { 
			get {
           		return getValueAsDouble(_Cust_SubTotal_Before_Discount); 
           }
			set { setValue(_Cust_SubTotal_Before_Discount, value); }
		}

		/**
		 * Decription: Cust Discount Amt
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Cust Discount Amt
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Cust_Discount_Amt = "Cust Discount Amt";
		public double Cust_Discount_Amt { 
			get {
           		return getValueAsDouble(_Cust_Discount_Amt); 
           }
			set { setValue(_Cust_Discount_Amt, value); }
		}

		/**
		 * Decription: Cust SubTotal After Discount
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Cust SubTotal After Discount
		 * Size: 21
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Cust_SubTotal_After_Discount = "Cust SubTotal After Discount";
		public double Cust_SubTotal_After_Discount { 
			get {
           		return getValueAsDouble(_Cust_SubTotal_After_Discount); 
           }
			set { setValue(_Cust_SubTotal_After_Discount, value); }
		}

		/**
		 * Decription: Cust VAT Amount
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Cust VAT Amount
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Cust_VAT_Amount = "Cust VAT Amount";
		public double Cust_VAT_Amount { 
			get {
           		return getValueAsDouble(_Cust_VAT_Amount); 
           }
			set { setValue(_Cust_VAT_Amount, value); }
		}

		/**
		 * Decription: Cust Additional Charges
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Cust Additional Charges
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Cust_Additional_Charges = "Cust Additional Charges";
		public double Cust_Additional_Charges { 
			get {
           		return getValueAsDouble(_Cust_Additional_Charges); 
           }
			set { setValue(_Cust_Additional_Charges, value); }
		}

		/**
		 * Decription: Cust Order Total
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Cust Order Total
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Cust_Order_Total = "Cust Order Total";
		public double Cust_Order_Total { 
			get {
           		return getValueAsDouble(_Cust_Order_Total); 
           }
			set { setValue(_Cust_Order_Total, value); }
		}

		/**
		 * Decription: Supp Disposal Weight
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Supp Disposal Weight
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Supp_Disposal_Weight = "Supp Disposal Weight";
		public double Supp_Disposal_Weight { 
			get {
           		return getValueAsDouble(_Supp_Disposal_Weight); 
           }
			set { setValue(_Supp_Disposal_Weight, value); }
		}

		/**
		 * Decription: Supp Disposal UOM
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Supp Disposal UOM
		 * Size: -1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Supp_Disposal_UOM = "Supp Disposal UOM";
		public string Supp_Disposal_UOM { 
			get {
           		return getValueAsString(_Supp_Disposal_UOM); 
           }
			set { setValue(_Supp_Disposal_UOM, value); }
		}

		/**
		 * Decription: Supp Disposal Rate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Supp Disposal Rate
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Supp_Disposal_Rate = "Supp Disposal Rate";
		public double Supp_Disposal_Rate { 
			get {
           		return getValueAsDouble(_Supp_Disposal_Rate); 
           }
			set { setValue(_Supp_Disposal_Rate, value); }
		}

		/**
		 * Decription: Supp Disposal Total
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Supp Disposal Total
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Supp_Disposal_Total = "Supp Disposal Total";
		public double Supp_Disposal_Total { 
			get {
           		return getValueAsDouble(_Supp_Disposal_Total); 
           }
			set { setValue(_Supp_Disposal_Total, value); }
		}

		/**
		 * Decription: Supp Haulage Qty
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Supp Haulage Qty
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Supp_Haulage_Qty = "Supp Haulage Qty";
		public double Supp_Haulage_Qty { 
			get {
           		return getValueAsDouble(_Supp_Haulage_Qty); 
           }
			set { setValue(_Supp_Haulage_Qty, value); }
		}

		/**
		 * Decription: Supp Haulage Rate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Supp Haulage Rate
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Supp_Haulage_Rate = "Supp Haulage Rate";
		public double Supp_Haulage_Rate { 
			get {
           		return getValueAsDouble(_Supp_Haulage_Rate); 
           }
			set { setValue(_Supp_Haulage_Rate, value); }
		}

		/**
		 * Decription: Supp Haulage Total
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Supp Haulage Total
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Supp_Haulage_Total = "Supp Haulage Total";
		public double Supp_Haulage_Total { 
			get {
           		return getValueAsDouble(_Supp_Haulage_Total); 
           }
			set { setValue(_Supp_Haulage_Total, value); }
		}

		/**
		 * Decription: Supp SubTotal
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Supp SubTotal
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Supp_SubTotal = "Supp SubTotal";
		public double Supp_SubTotal { 
			get {
           		return getValueAsDouble(_Supp_SubTotal); 
           }
			set { setValue(_Supp_SubTotal, value); }
		}

		/**
		 * Decription: Supp VAT Amount
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Supp VAT Amount
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Supp_VAT_Amount = "Supp VAT Amount";
		public double Supp_VAT_Amount { 
			get {
           		return getValueAsDouble(_Supp_VAT_Amount); 
           }
			set { setValue(_Supp_VAT_Amount, value); }
		}

		/**
		 * Decription: Supp Additional Costs
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Supp Additional Costs
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Supp_Additional_Costs = "Supp Additional Costs";
		public double Supp_Additional_Costs { 
			get {
           		return getValueAsDouble(_Supp_Additional_Costs); 
           }
			set { setValue(_Supp_Additional_Costs, value); }
		}

		/**
		 * Decription: Supp Order Total
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Supp Order Total
		 * Size: 19
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Supp_Order_Total = "Supp Order Total";
		public double Supp_Order_Total { 
			get {
           		return getValueAsDouble(_Supp_Order_Total); 
           }
			set { setValue(_Supp_Order_Total, value); }
		}

		/**
		 * Decription: Profit/Loss
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Profit/Loss
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Profit_Loss = "Profit/Loss";
		public double Profit_Loss { 
			get {
           		return getValueAsDouble(_Profit_Loss); 
           }
			set { setValue(_Profit_Loss, value); }
		}

		/**
		 * Decription: Total Cust Charge
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Total Cust Charge
		 * Size: 30
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Total_Cust_Charge = "Total Cust Charge";
		public double Total_Cust_Charge { 
			get {
           		return getValueAsDouble(_Total_Cust_Charge); 
           }
			set { setValue(_Total_Cust_Charge, value); }
		}

		/**
		 * Decription: Total Supp Cost
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: True
		 * Name: Total Supp Cost
		 * Size: 30
		 * Type: db_Float
		 * CType: double
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Total_Supp_Cost = "Total Supp Cost";
		public double Total_Supp_Cost { 
			get {
           		return getValueAsDouble(_Total_Supp_Cost); 
           }
			set { setValue(_Total_Supp_Cost, value); }
		}

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(66);

			moDBFields.Add(_MON, "MON", 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR); //MON
			moDBFields.Add(_TUE, "TUE", 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR); //TUE
			moDBFields.Add(_WED, "WED", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR); //WED
			moDBFields.Add(_THU, "THU", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR); //THU
			moDBFields.Add(_FRI, "FRI", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR); //FRI
			moDBFields.Add(_SAT, "SAT", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR); //SAT
			moDBFields.Add(_SUN, "SUN", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR); //SUN
			moDBFields.Add(_Site_Clearance, "Site Clearance", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR); //Site Clearance
			moDBFields.Add(_WOHeader, "WOHeader", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR); //WOHeader
			moDBFields.Add(_WORow, "WORow", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR); //WORow
			moDBFields.Add(_Lift_Type, "Lift Type", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR); //Lift Type
			moDBFields.Add(_Site_Fax, "Site Fax", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR); //Site Fax
			moDBFields.Add(_JobTrigger, "JobTrigger", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR); //JobTrigger
			moDBFields.Add(_DoneDate, "DoneDate", 13, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 0, null); //DoneDate
			moDBFields.Add(_Customer_Reference, "Customer Reference", 14, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Customer Reference
			moDBFields.Add(_Site_Id, "Site Id", 15, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Site Id
			moDBFields.Add(_Supplier_Name, "Supplier Name", 16, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Supplier Name
			moDBFields.Add(_Qty, "Qty", 17, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Qty
			moDBFields.Add(_Txt, "Txt", 18, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 54, EMPTYSTR); //Txt
			moDBFields.Add(_Lifts_Per_Week, "Lifts Per Week", 19, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 38, 0); //Lifts Per Week
			moDBFields.Add(_Order_Type, "Order Type", 20, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Order Type
			moDBFields.Add(_Container_Code, "Container Code", 21, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Container Code
			moDBFields.Add(_Container_Desc, "Container Desc", 22, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Container Desc
			moDBFields.Add(_Waste_Code, "Waste Code", 23, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Waste Code
			moDBFields.Add(_Waste_Desc, "Waste Desc", 24, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Waste Desc
			moDBFields.Add(_Customer_Code, "Customer Code", 25, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Customer Code
			moDBFields.Add(_Customer_Name, "Customer Name", 26, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Customer Name
			moDBFields.Add(_Site_Address1, "Site Address1", 27, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Site Address1
			moDBFields.Add(_Site_Address2, "Site Address2", 28, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Site Address2
			moDBFields.Add(_Site_Address3, "Site Address3", 29, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Site Address3
			moDBFields.Add(_Site_Address4, "Site Address4", 30, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Site Address4
			moDBFields.Add(_Site_Address5, "Site Address5", 31, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Site Address5
			moDBFields.Add(_Post_Code, "Post Code", 32, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Post Code
			moDBFields.Add(_Site_Contact, "Site Contact", 33, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Site Contact
			moDBFields.Add(_Site_Tel, "Site Tel", 34, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Site Tel
			moDBFields.Add(_Cust_Disposal_Units, "Cust Disposal Units", 35, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Cust Disposal Units
			moDBFields.Add(_Cust_Read_Weight, "Cust Read Weight", 36, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Cust Read Weight
			moDBFields.Add(_Cust_Charge_Weight, "Cust Charge Weight", 37, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Cust Charge Weight
			moDBFields.Add(_Cust_Disposal_UOM, "Cust Disposal UOM", 38, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Cust Disposal UOM
			moDBFields.Add(_Cust_Disposal_Rate, "Cust Disposal Rate", 39, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Cust Disposal Rate
			moDBFields.Add(_Cust_Disposal_Total, "Cust Disposal Total", 40, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Cust Disposal Total
			moDBFields.Add(_Cust_Haulage_Qty, "Cust Haulage Qty", 41, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Cust Haulage Qty
			moDBFields.Add(_Cust_Haulage_Rate, "Cust Haulage Rate", 42, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Cust Haulage Rate
			moDBFields.Add(_Cust_Haulage_Total, "Cust Haulage Total", 43, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Cust Haulage Total
			moDBFields.Add(_Cust_SubTotal_Before_Discount, "Cust SubTotal Before Discount", 44, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 20, 0); //Cust SubTotal Before Discount
			moDBFields.Add(_Cust_Discount_Amt, "Cust Discount Amt", 45, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Cust Discount Amt
			moDBFields.Add(_Cust_SubTotal_After_Discount, "Cust SubTotal After Discount", 46, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 21, 0); //Cust SubTotal After Discount
			moDBFields.Add(_Cust_VAT_Amount, "Cust VAT Amount", 47, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Cust VAT Amount
			moDBFields.Add(_Cust_Additional_Charges, "Cust Additional Charges", 48, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Cust Additional Charges
			moDBFields.Add(_Cust_Order_Total, "Cust Order Total", 49, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Cust Order Total
			moDBFields.Add(_Supp_Disposal_Weight, "Supp Disposal Weight", 50, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Supp Disposal Weight
			moDBFields.Add(_Supp_Disposal_UOM, "Supp Disposal UOM", 51, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, -1, EMPTYSTR); //Supp Disposal UOM
			moDBFields.Add(_Supp_Disposal_Rate, "Supp Disposal Rate", 52, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Supp Disposal Rate
			moDBFields.Add(_Supp_Disposal_Total, "Supp Disposal Total", 53, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Supp Disposal Total
			moDBFields.Add(_Supp_Haulage_Qty, "Supp Haulage Qty", 54, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Supp Haulage Qty
			moDBFields.Add(_Supp_Haulage_Rate, "Supp Haulage Rate", 55, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Supp Haulage Rate
			moDBFields.Add(_Supp_Haulage_Total, "Supp Haulage Total", 56, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Supp Haulage Total
			moDBFields.Add(_Supp_SubTotal, "Supp SubTotal", 57, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 20, 0); //Supp SubTotal
			moDBFields.Add(_Supp_VAT_Amount, "Supp VAT Amount", 58, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Supp VAT Amount
			moDBFields.Add(_Supp_Additional_Costs, "Supp Additional Costs", 59, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Supp Additional Costs
			moDBFields.Add(_Supp_Order_Total, "Supp Order Total", 60, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 19, 0); //Supp Order Total
			moDBFields.Add(_Profit_Loss, "Profit/Loss", 61, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 20, 0); //Profit/Loss
			moDBFields.Add(_Total_Cust_Charge, "Total Cust Charge", 62, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 30, 0); //Total Cust Charge
			moDBFields.Add(_Total_Supp_Cost, "Total Supp Cost", 63, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_None, 30, 0); //Total Supp Cost

		if ( moLocalDBFields == null ) {
				moLocalDBFields = new string[] {"MON","TUE","WED","THU","FRI","SAT","SUN","Site Clearance","WOHeader","WORow","Lift Type","Site Fax","JobTrigger","DoneDate","Customer Reference","Site Id","Supplier Name","Qty","Txt","Lifts Per Week","Order Type","Container Code","Container Desc","Waste Code","Waste Desc","Customer Code","Customer Name","Site Address1","Site Address2","Site Address3","Site Address4","Site Address5","Post Code","Site Contact","Site Tel","Cust Disposal Units","Cust Read Weight","Cust Charge Weight","Cust Disposal UOM","Cust Disposal Rate","Cust Disposal Total","Cust Haulage Qty","Cust Haulage Rate","Cust Haulage Total","Cust SubTotal Before Discount","Cust Discount Amt","Cust SubTotal After Discount","Cust VAT Amount","Cust Additional Charges","Cust Order Total","Supp Disposal Weight","Supp Disposal UOM","Supp Disposal Rate","Supp Disposal Total","Supp Haulage Qty","Supp Haulage Rate","Supp Haulage Total","Supp SubTotal","Supp VAT Amount","Supp Additional Costs","Supp Order Total","Profit/Loss","Total Cust Charge","Total Supp Cost"};
			}
			if ( msLocalSelectList == null ){
				doBuildSelectionList();
				msLocalSelectList = msSelectionList;
			}
			else 
				msSelectionList = msLocalSelectList;

		}

#endregion

#region validation
		public override bool doValidation(){
			string sVal;
           object oVal;
			msLastValidationError = "";
			oVal = DoneDate;
			if ( oVal == null )
				msLastValidationError += _DoneDate + ',';

			sVal = Customer_Reference;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Customer_Reference + ',' ;
			sVal = Site_Id;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Site_Id + ',' ;
			sVal = Supplier_Name;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Supplier_Name + ',' ;
			oVal = Qty;
			if ( oVal == null )
				msLastValidationError += _Qty + ',';

			sVal = Txt;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Txt + ',' ;
			oVal = Lifts_Per_Week;
			if ( oVal == null )
				msLastValidationError += _Lifts_Per_Week + ',';

			sVal = Order_Type;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Order_Type + ',' ;
			sVal = Container_Code;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Container_Code + ',' ;
			sVal = Container_Desc;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Container_Desc + ',' ;
			sVal = Waste_Code;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Waste_Code + ',' ;
			sVal = Waste_Desc;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Waste_Desc + ',' ;
			sVal = Customer_Code;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Customer_Code + ',' ;
			sVal = Customer_Name;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Customer_Name + ',' ;
			sVal = Site_Address1;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Site_Address1 + ',' ;
			sVal = Site_Address2;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Site_Address2 + ',' ;
			sVal = Site_Address3;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Site_Address3 + ',' ;
			sVal = Site_Address4;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Site_Address4 + ',' ;
			sVal = Site_Address5;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Site_Address5 + ',' ;
			sVal = Post_Code;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Post_Code + ',' ;
			sVal = Site_Contact;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Site_Contact + ',' ;
			sVal = Site_Tel;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Site_Tel + ',' ;
			oVal = Cust_Disposal_Units;
			if ( oVal == null )
				msLastValidationError += _Cust_Disposal_Units + ',';

			oVal = Cust_Read_Weight;
			if ( oVal == null )
				msLastValidationError += _Cust_Read_Weight + ',';

			oVal = Cust_Charge_Weight;
			if ( oVal == null )
				msLastValidationError += _Cust_Charge_Weight + ',';

			sVal = Cust_Disposal_UOM;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Cust_Disposal_UOM + ',' ;
			oVal = Cust_Disposal_Rate;
			if ( oVal == null )
				msLastValidationError += _Cust_Disposal_Rate + ',';

			oVal = Cust_Disposal_Total;
			if ( oVal == null )
				msLastValidationError += _Cust_Disposal_Total + ',';

			oVal = Cust_Haulage_Qty;
			if ( oVal == null )
				msLastValidationError += _Cust_Haulage_Qty + ',';

			oVal = Cust_Haulage_Rate;
			if ( oVal == null )
				msLastValidationError += _Cust_Haulage_Rate + ',';

			oVal = Cust_Haulage_Total;
			if ( oVal == null )
				msLastValidationError += _Cust_Haulage_Total + ',';

			oVal = Cust_SubTotal_Before_Discount;
			if ( oVal == null )
				msLastValidationError += _Cust_SubTotal_Before_Discount + ',';

			oVal = Cust_Discount_Amt;
			if ( oVal == null )
				msLastValidationError += _Cust_Discount_Amt + ',';

			oVal = Cust_SubTotal_After_Discount;
			if ( oVal == null )
				msLastValidationError += _Cust_SubTotal_After_Discount + ',';

			oVal = Cust_VAT_Amount;
			if ( oVal == null )
				msLastValidationError += _Cust_VAT_Amount + ',';

			oVal = Cust_Additional_Charges;
			if ( oVal == null )
				msLastValidationError += _Cust_Additional_Charges + ',';

			oVal = Cust_Order_Total;
			if ( oVal == null )
				msLastValidationError += _Cust_Order_Total + ',';

			oVal = Supp_Disposal_Weight;
			if ( oVal == null )
				msLastValidationError += _Supp_Disposal_Weight + ',';

			sVal = Supp_Disposal_UOM;
			if ( sVal == null || sVal.Length == 0 )
				msLastValidationError += _Supp_Disposal_UOM + ',' ;
			oVal = Supp_Disposal_Rate;
			if ( oVal == null )
				msLastValidationError += _Supp_Disposal_Rate + ',';

			oVal = Supp_Disposal_Total;
			if ( oVal == null )
				msLastValidationError += _Supp_Disposal_Total + ',';

			oVal = Supp_Haulage_Qty;
			if ( oVal == null )
				msLastValidationError += _Supp_Haulage_Qty + ',';

			oVal = Supp_Haulage_Rate;
			if ( oVal == null )
				msLastValidationError += _Supp_Haulage_Rate + ',';

			oVal = Supp_Haulage_Total;
			if ( oVal == null )
				msLastValidationError += _Supp_Haulage_Total + ',';

			oVal = Supp_SubTotal;
			if ( oVal == null )
				msLastValidationError += _Supp_SubTotal + ',';

			oVal = Supp_VAT_Amount;
			if ( oVal == null )
				msLastValidationError += _Supp_VAT_Amount + ',';

			oVal = Supp_Additional_Costs;
			if ( oVal == null )
				msLastValidationError += _Supp_Additional_Costs + ',';

			oVal = Supp_Order_Total;
			if ( oVal == null )
				msLastValidationError += _Supp_Order_Total + ',';

			oVal = Profit_Loss;
			if ( oVal == null )
				msLastValidationError += _Profit_Loss + ',';

			oVal = Total_Cust_Charge;
			if ( oVal == null )
				msLastValidationError += _Total_Cust_Charge + ',';

			oVal = Total_Supp_Cost;
			if ( oVal == null )
				msLastValidationError += _Total_Supp_Cost + ',';


			if (msLastValidationError.Length > 0) 
			    return false;
			else
			return true;
		}
#endregion

#region LinkDataToControls
		/**
		 * Link the MON Field to the Form Item.
		 */
		public void doLink_MON(string sControlName){
			moLinker.doLinkDataToControl(_MON, sControlName);
		}
		/**
		 * Link the TUE Field to the Form Item.
		 */
		public void doLink_TUE(string sControlName){
			moLinker.doLinkDataToControl(_TUE, sControlName);
		}
		/**
		 * Link the WED Field to the Form Item.
		 */
		public void doLink_WED(string sControlName){
			moLinker.doLinkDataToControl(_WED, sControlName);
		}
		/**
		 * Link the THU Field to the Form Item.
		 */
		public void doLink_THU(string sControlName){
			moLinker.doLinkDataToControl(_THU, sControlName);
		}
		/**
		 * Link the FRI Field to the Form Item.
		 */
		public void doLink_FRI(string sControlName){
			moLinker.doLinkDataToControl(_FRI, sControlName);
		}
		/**
		 * Link the SAT Field to the Form Item.
		 */
		public void doLink_SAT(string sControlName){
			moLinker.doLinkDataToControl(_SAT, sControlName);
		}
		/**
		 * Link the SUN Field to the Form Item.
		 */
		public void doLink_SUN(string sControlName){
			moLinker.doLinkDataToControl(_SUN, sControlName);
		}
		/**
		 * Link the Site Clearance Field to the Form Item.
		 */
		public void doLink_Site_Clearance(string sControlName){
			moLinker.doLinkDataToControl(_Site_Clearance, sControlName);
		}
		/**
		 * Link the WOHeader Field to the Form Item.
		 */
		public void doLink_WOHeader(string sControlName){
			moLinker.doLinkDataToControl(_WOHeader, sControlName);
		}
		/**
		 * Link the WORow Field to the Form Item.
		 */
		public void doLink_WORow(string sControlName){
			moLinker.doLinkDataToControl(_WORow, sControlName);
		}
		/**
		 * Link the Lift Type Field to the Form Item.
		 */
		public void doLink_Lift_Type(string sControlName){
			moLinker.doLinkDataToControl(_Lift_Type, sControlName);
		}
		/**
		 * Link the Site Fax Field to the Form Item.
		 */
		public void doLink_Site_Fax(string sControlName){
			moLinker.doLinkDataToControl(_Site_Fax, sControlName);
		}
		/**
		 * Link the JobTrigger Field to the Form Item.
		 */
		public void doLink_JobTrigger(string sControlName){
			moLinker.doLinkDataToControl(_JobTrigger, sControlName);
		}
		/**
		 * Link the DoneDate Field to the Form Item.
		 */
		public void doLink_DoneDate(string sControlName){
			moLinker.doLinkDataToControl(_DoneDate, sControlName);
		}
		/**
		 * Link the Customer Reference Field to the Form Item.
		 */
		public void doLink_Customer_Reference(string sControlName){
			moLinker.doLinkDataToControl(_Customer_Reference, sControlName);
		}
		/**
		 * Link the Site Id Field to the Form Item.
		 */
		public void doLink_Site_Id(string sControlName){
			moLinker.doLinkDataToControl(_Site_Id, sControlName);
		}
		/**
		 * Link the Supplier Name Field to the Form Item.
		 */
		public void doLink_Supplier_Name(string sControlName){
			moLinker.doLinkDataToControl(_Supplier_Name, sControlName);
		}
		/**
		 * Link the Qty Field to the Form Item.
		 */
		public void doLink_Qty(string sControlName){
			moLinker.doLinkDataToControl(_Qty, sControlName);
		}
		/**
		 * Link the Txt Field to the Form Item.
		 */
		public void doLink_Txt(string sControlName){
			moLinker.doLinkDataToControl(_Txt, sControlName);
		}
		/**
		 * Link the Lifts Per Week Field to the Form Item.
		 */
		public void doLink_Lifts_Per_Week(string sControlName){
			moLinker.doLinkDataToControl(_Lifts_Per_Week, sControlName);
		}
		/**
		 * Link the Order Type Field to the Form Item.
		 */
		public void doLink_Order_Type(string sControlName){
			moLinker.doLinkDataToControl(_Order_Type, sControlName);
		}
		/**
		 * Link the Container Code Field to the Form Item.
		 */
		public void doLink_Container_Code(string sControlName){
			moLinker.doLinkDataToControl(_Container_Code, sControlName);
		}
		/**
		 * Link the Container Desc Field to the Form Item.
		 */
		public void doLink_Container_Desc(string sControlName){
			moLinker.doLinkDataToControl(_Container_Desc, sControlName);
		}
		/**
		 * Link the Waste Code Field to the Form Item.
		 */
		public void doLink_Waste_Code(string sControlName){
			moLinker.doLinkDataToControl(_Waste_Code, sControlName);
		}
		/**
		 * Link the Waste Desc Field to the Form Item.
		 */
		public void doLink_Waste_Desc(string sControlName){
			moLinker.doLinkDataToControl(_Waste_Desc, sControlName);
		}
		/**
		 * Link the Customer Code Field to the Form Item.
		 */
		public void doLink_Customer_Code(string sControlName){
			moLinker.doLinkDataToControl(_Customer_Code, sControlName);
		}
		/**
		 * Link the Customer Name Field to the Form Item.
		 */
		public void doLink_Customer_Name(string sControlName){
			moLinker.doLinkDataToControl(_Customer_Name, sControlName);
		}
		/**
		 * Link the Site Address1 Field to the Form Item.
		 */
		public void doLink_Site_Address1(string sControlName){
			moLinker.doLinkDataToControl(_Site_Address1, sControlName);
		}
		/**
		 * Link the Site Address2 Field to the Form Item.
		 */
		public void doLink_Site_Address2(string sControlName){
			moLinker.doLinkDataToControl(_Site_Address2, sControlName);
		}
		/**
		 * Link the Site Address3 Field to the Form Item.
		 */
		public void doLink_Site_Address3(string sControlName){
			moLinker.doLinkDataToControl(_Site_Address3, sControlName);
		}
		/**
		 * Link the Site Address4 Field to the Form Item.
		 */
		public void doLink_Site_Address4(string sControlName){
			moLinker.doLinkDataToControl(_Site_Address4, sControlName);
		}
		/**
		 * Link the Site Address5 Field to the Form Item.
		 */
		public void doLink_Site_Address5(string sControlName){
			moLinker.doLinkDataToControl(_Site_Address5, sControlName);
		}
		/**
		 * Link the Post Code Field to the Form Item.
		 */
		public void doLink_Post_Code(string sControlName){
			moLinker.doLinkDataToControl(_Post_Code, sControlName);
		}
		/**
		 * Link the Site Contact Field to the Form Item.
		 */
		public void doLink_Site_Contact(string sControlName){
			moLinker.doLinkDataToControl(_Site_Contact, sControlName);
		}
		/**
		 * Link the Site Tel Field to the Form Item.
		 */
		public void doLink_Site_Tel(string sControlName){
			moLinker.doLinkDataToControl(_Site_Tel, sControlName);
		}
		/**
		 * Link the Cust Disposal Units Field to the Form Item.
		 */
		public void doLink_Cust_Disposal_Units(string sControlName){
			moLinker.doLinkDataToControl(_Cust_Disposal_Units, sControlName);
		}
		/**
		 * Link the Cust Read Weight Field to the Form Item.
		 */
		public void doLink_Cust_Read_Weight(string sControlName){
			moLinker.doLinkDataToControl(_Cust_Read_Weight, sControlName);
		}
		/**
		 * Link the Cust Charge Weight Field to the Form Item.
		 */
		public void doLink_Cust_Charge_Weight(string sControlName){
			moLinker.doLinkDataToControl(_Cust_Charge_Weight, sControlName);
		}
		/**
		 * Link the Cust Disposal UOM Field to the Form Item.
		 */
		public void doLink_Cust_Disposal_UOM(string sControlName){
			moLinker.doLinkDataToControl(_Cust_Disposal_UOM, sControlName);
		}
		/**
		 * Link the Cust Disposal Rate Field to the Form Item.
		 */
		public void doLink_Cust_Disposal_Rate(string sControlName){
			moLinker.doLinkDataToControl(_Cust_Disposal_Rate, sControlName);
		}
		/**
		 * Link the Cust Disposal Total Field to the Form Item.
		 */
		public void doLink_Cust_Disposal_Total(string sControlName){
			moLinker.doLinkDataToControl(_Cust_Disposal_Total, sControlName);
		}
		/**
		 * Link the Cust Haulage Qty Field to the Form Item.
		 */
		public void doLink_Cust_Haulage_Qty(string sControlName){
			moLinker.doLinkDataToControl(_Cust_Haulage_Qty, sControlName);
		}
		/**
		 * Link the Cust Haulage Rate Field to the Form Item.
		 */
		public void doLink_Cust_Haulage_Rate(string sControlName){
			moLinker.doLinkDataToControl(_Cust_Haulage_Rate, sControlName);
		}
		/**
		 * Link the Cust Haulage Total Field to the Form Item.
		 */
		public void doLink_Cust_Haulage_Total(string sControlName){
			moLinker.doLinkDataToControl(_Cust_Haulage_Total, sControlName);
		}
		/**
		 * Link the Cust SubTotal Before Discount Field to the Form Item.
		 */
		public void doLink_Cust_SubTotal_Before_Discount(string sControlName){
			moLinker.doLinkDataToControl(_Cust_SubTotal_Before_Discount, sControlName);
		}
		/**
		 * Link the Cust Discount Amt Field to the Form Item.
		 */
		public void doLink_Cust_Discount_Amt(string sControlName){
			moLinker.doLinkDataToControl(_Cust_Discount_Amt, sControlName);
		}
		/**
		 * Link the Cust SubTotal After Discount Field to the Form Item.
		 */
		public void doLink_Cust_SubTotal_After_Discount(string sControlName){
			moLinker.doLinkDataToControl(_Cust_SubTotal_After_Discount, sControlName);
		}
		/**
		 * Link the Cust VAT Amount Field to the Form Item.
		 */
		public void doLink_Cust_VAT_Amount(string sControlName){
			moLinker.doLinkDataToControl(_Cust_VAT_Amount, sControlName);
		}
		/**
		 * Link the Cust Additional Charges Field to the Form Item.
		 */
		public void doLink_Cust_Additional_Charges(string sControlName){
			moLinker.doLinkDataToControl(_Cust_Additional_Charges, sControlName);
		}
		/**
		 * Link the Cust Order Total Field to the Form Item.
		 */
		public void doLink_Cust_Order_Total(string sControlName){
			moLinker.doLinkDataToControl(_Cust_Order_Total, sControlName);
		}
		/**
		 * Link the Supp Disposal Weight Field to the Form Item.
		 */
		public void doLink_Supp_Disposal_Weight(string sControlName){
			moLinker.doLinkDataToControl(_Supp_Disposal_Weight, sControlName);
		}
		/**
		 * Link the Supp Disposal UOM Field to the Form Item.
		 */
		public void doLink_Supp_Disposal_UOM(string sControlName){
			moLinker.doLinkDataToControl(_Supp_Disposal_UOM, sControlName);
		}
		/**
		 * Link the Supp Disposal Rate Field to the Form Item.
		 */
		public void doLink_Supp_Disposal_Rate(string sControlName){
			moLinker.doLinkDataToControl(_Supp_Disposal_Rate, sControlName);
		}
		/**
		 * Link the Supp Disposal Total Field to the Form Item.
		 */
		public void doLink_Supp_Disposal_Total(string sControlName){
			moLinker.doLinkDataToControl(_Supp_Disposal_Total, sControlName);
		}
		/**
		 * Link the Supp Haulage Qty Field to the Form Item.
		 */
		public void doLink_Supp_Haulage_Qty(string sControlName){
			moLinker.doLinkDataToControl(_Supp_Haulage_Qty, sControlName);
		}
		/**
		 * Link the Supp Haulage Rate Field to the Form Item.
		 */
		public void doLink_Supp_Haulage_Rate(string sControlName){
			moLinker.doLinkDataToControl(_Supp_Haulage_Rate, sControlName);
		}
		/**
		 * Link the Supp Haulage Total Field to the Form Item.
		 */
		public void doLink_Supp_Haulage_Total(string sControlName){
			moLinker.doLinkDataToControl(_Supp_Haulage_Total, sControlName);
		}
		/**
		 * Link the Supp SubTotal Field to the Form Item.
		 */
		public void doLink_Supp_SubTotal(string sControlName){
			moLinker.doLinkDataToControl(_Supp_SubTotal, sControlName);
		}
		/**
		 * Link the Supp VAT Amount Field to the Form Item.
		 */
		public void doLink_Supp_VAT_Amount(string sControlName){
			moLinker.doLinkDataToControl(_Supp_VAT_Amount, sControlName);
		}
		/**
		 * Link the Supp Additional Costs Field to the Form Item.
		 */
		public void doLink_Supp_Additional_Costs(string sControlName){
			moLinker.doLinkDataToControl(_Supp_Additional_Costs, sControlName);
		}
		/**
		 * Link the Supp Order Total Field to the Form Item.
		 */
		public void doLink_Supp_Order_Total(string sControlName){
			moLinker.doLinkDataToControl(_Supp_Order_Total, sControlName);
		}
		/**
		 * Link the Profit/Loss Field to the Form Item.
		 */
		public void doLink_Profit_Loss(string sControlName){
			moLinker.doLinkDataToControl(_Profit_Loss, sControlName);
		}
		/**
		 * Link the Total Cust Charge Field to the Form Item.
		 */
		public void doLink_Total_Cust_Charge(string sControlName){
			moLinker.doLinkDataToControl(_Total_Cust_Charge, sControlName);
		}
		/**
		 * Link the Total Supp Cost Field to the Form Item.
		 */
		public void doLink_Total_Supp_Cost(string sControlName){
			moLinker.doLinkDataToControl(_Total_Supp_Cost, sControlName);
		}
#endregion

	}
}
