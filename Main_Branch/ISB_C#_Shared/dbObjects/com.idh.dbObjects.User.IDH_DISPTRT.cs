/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 09/03/2016 11:25:12
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
   [Serializable] 
	public class IDH_DISPTRT: com.idh.dbObjects.Base.IDH_DISPTRT{ 

		public IDH_DISPTRT() : base() {
            msAutoNumKey = "IDHDISTR";
			
		}

   	public IDH_DISPTRT( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
        msAutoNumKey = "IDHDISTR";
   	
    }
	}
}
