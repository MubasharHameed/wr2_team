/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2012/04/17 12:49:25 PM
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
     
    public class IDH_TFSMOVE: com.idh.dbObjects.Base.IDH_TFSMOVE{ 

		//public IDH_TFSMOVE(){
		//	base();
		//}

   	public IDH_TFSMOVE( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
    }

    private IDH_TFSMAST moTFSMaster;
    public IDH_TFSMAST TFSMaster
    {
        get { return moTFSMaster; }
        set { moTFSMaster = value; }
    }

    public int getByTFSNo(string sTFSNo)
    {
        return getData(_TFSNo + " = '" + sTFSNo + "'", null);
    }

    public int getByTFSCode(string sTFSCode)
    {
        return getData(_TFSCd + " = '" + sTFSCode + "'", null);
    }

	}
}
