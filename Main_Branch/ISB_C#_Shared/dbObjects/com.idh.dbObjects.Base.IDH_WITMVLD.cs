/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 04/11/2015 15:09:24
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.idh.dbObjects.Base {
    [Serializable]
    public class IDH_WITMVLD : com.idh.dbObjects.DBBase {

        //private Linker moLinker = null;
        //public Linker ControlLinker {
        //    get { return moLinker; }
        //    set { moLinker = value; }
        //}

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_WITMVLD()
            : base("@IDH_WITMVLD") {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_WITMVLD(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oForm, "@IDH_WITMVLD") {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
		* Table name
		*/
        public readonly static string TableName = "@IDH_WITMVLD";

        /**
         * Decription: Code
         * Mandatory: tYes
         * Name: Code
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Code = "Code";
        public string Code {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code() {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue) {
            return base.doValidation(_Code, oValue);
        }
        /**
         * Decription: Name
         * Mandatory: tYes
         * Name: Name
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Name = "Name";
        public string Name {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name() {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue) {
            return base.doValidation(_Name, oValue);
        }
        /**
         * Decription: Enquiry ID
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_EnqId
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _EnqId = "U_EnqId";
        public string U_EnqId {
            get {
                return getValueAsString(_EnqId);
            }
            set { setValue(_EnqId, value); }
        }
        public string doValidate_EnqId() {
            return doValidate_EnqId(U_EnqId);
        }
        public virtual string doValidate_EnqId(object oValue) {
            return base.doValidation(_EnqId, oValue);
        }
        /**
         * Decription: Enquiry Line ID
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_EnqLID
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _EnqLID = "U_EnqLID";
        public string U_EnqLID {
            get {
                return getValueAsString(_EnqLID);
            }
            set { setValue(_EnqLID, value); }
        }
        public string doValidate_EnqLID() {
            return doValidate_EnqLID(U_EnqLID);
        }
        public virtual string doValidate_EnqLID(object oValue) {
            return base.doValidation(_EnqLID, oValue);
        }

        /**
         * Decription: Item Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ItemCode
         * Size: 50
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ItemCode = "U_ItemCode";
        public string U_ItemCode {
            get {
                return getValueAsString(_ItemCode);
            }
            set { setValue(_ItemCode, value); }
        }
        public string doValidate_ItemCode() {
            return doValidate_ItemCode(U_ItemCode);
        }
        public virtual string doValidate_ItemCode(object oValue) {
            return base.doValidation(_ItemCode, oValue);
        }

        /**
         * Decription: Item Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ItemFName
         * Size: 200
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ItemFName = "U_ItemFName";
        public string U_ItemFName {
            get {
                return getValueAsString(_ItemFName);
            }
            set { setValue(_ItemFName, value); }
        }
        public string doValidate_ItemFName() {
            return doValidate_ItemFName(U_ItemFName);
        }
        public virtual string doValidate_ItemFName(object oValue) {
            return base.doValidation(_ItemFName, oValue);
        }
        /**
         * Decription: Validation Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ValidCd
         * Size: 250
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ValidCd = "U_ValidCd";
        public string U_ValidCd {
            get {
                return getValueAsString(_ValidCd);
            }
            set { setValue(_ValidCd, value); }
        }
        public string doValidate_ValidCd() {
            return doValidate_ValidCd(U_ValidCd);
        }
        public virtual string doValidate_ValidCd(object oValue) {
            return base.doValidation(_ValidCd, oValue);
        }
        //
        /**
         * Decription: OITM DB Field Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_DBField
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _DBField = "U_DBField";
        public string U_DBField {
            get {
                return getValueAsString(_DBField);
            }
            set { setValue(_DBField, value); }
        }
        public string doValidate_DBField() {
            return doValidate_DBField(U_DBField);
        }
        public virtual string doValidate_DBField(object oValue) {
            return base.doValidation(_DBField, oValue);
        }
        /**
         * Decription: Value1
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Value1
         * Size: 254
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Value1 = "U_Value1";
        public string U_Value1 {
            get {
                return getValueAsString(_Value1);
            }
            set { setValue(_Value1, value); }
        }
        public string doValidate_Value1() {
            return doValidate_Value1(U_Value1);
        }
        public virtual string doValidate_Value1(object oValue) {
            return base.doValidation(_Value1, oValue);
        }

        /**
         * Decription: Value2
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Value2
         * Size: 254
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Value2 = "U_Value2";
        public string U_Value2 {
            get {
                return getValueAsString(_Value2);
            }
            set { setValue(_Value2, value); }
        }
        public string doValidate_Value2() {
            return doValidate_Value2(U_Value2);
        }
        public virtual string doValidate_Value2(object oValue) {
            return base.doValidation(_Value2, oValue);
        }

        /**
         * Decription: WOQ ID
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WOQId
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _WOQId = "U_WOQId";
        public string U_WOQId {
            get {
                return getValueAsString(_WOQId);
            }
            set { setValue(_WOQId, value); }
        }
        public string doValidate_WOQId() {
            return doValidate_WOQId(U_WOQId);
        }
        public virtual string doValidate_WOQId(object oValue) {
            return base.doValidation(_WOQId, oValue);
        }
        /**
         * Decription: WOQ Line ID
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WOQLId
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _WOQLId = "U_WOQLId";
        public string U_WOQLId {
            get {
                return getValueAsString(_WOQLId);
            }
            set { setValue(_WOQLId, value); }
        }
        public string doValidate_WOQLId() {
            return doValidate_WOQLId(U_WOQLId);
        }
        public virtual string doValidate_WOQLId(object oValue) {
            return base.doValidation(_WOQLId, oValue);
        }

        /**
         * Decription: Waste Group Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WstGpCd
         * Size: 250
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _WstGpCd = "U_WstGpCd";
        public string U_WstGpCd {
            get {
                return getValueAsString(_WstGpCd);
            }
            set { setValue(_WstGpCd, value); }
        }
        public string doValidate_WstGpCd() {
            return doValidate_WstGpCd(U_WstGpCd);
        }
        public virtual string doValidate_WstGpCd(object oValue) {
            return base.doValidation(_WstGpCd, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo() {
            if (moDBFields == null)
                moDBFields = new DBFields(13);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_EnqId, "Enquiry ID", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, false); //Enquiry ID
            moDBFields.Add(_ItemCode, "Item Code", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Item Code
            moDBFields.Add(_ItemFName, "Item Name", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Item Name
            moDBFields.Add(_ValidCd, "Validation Code", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Validation Code
            
            moDBFields.Add(_Value1, "Value1", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, EMPTYSTR, false, false); //Value1
            moDBFields.Add(_Value2, "Value2", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, EMPTYSTR, false, false); //Value2
            moDBFields.Add(_WOQId, "WOQ ID", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //WOQ ID
            moDBFields.Add(_WstGpCd, "Waste Group Code", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Waste Group Code

            moDBFields.Add(_WOQLId, "WOQ Line ID", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //WOQ Line ID
            moDBFields.Add(_EnqLID, "WOQ Line ID", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Enq Line ID
            moDBFields.Add(_DBField, "OITM Field Name", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //OITM DB Field Name

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation() {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_EnqId());
            doBuildValidationString(doValidate_ItemCode());
            doBuildValidationString(doValidate_ItemFName());
            doBuildValidationString(doValidate_ValidCd());
            doBuildValidationString(doValidate_Value1());
            doBuildValidationString(doValidate_Value2());
            doBuildValidationString(doValidate_WOQId());
            doBuildValidationString(doValidate_WstGpCd());
            doBuildValidationString(doValidate_EnqLID());
            doBuildValidationString(doValidate_WOQLId());
            doBuildValidationString(doValidate_DBField());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_EnqId)) return doValidate_EnqId(oValue);
            if (sFieldName.Equals(_ItemCode)) return doValidate_ItemCode(oValue);
            if (sFieldName.Equals(_ItemFName)) return doValidate_ItemFName(oValue);
            if (sFieldName.Equals(_ValidCd)) return doValidate_ValidCd(oValue);
            if (sFieldName.Equals(_Value1)) return doValidate_Value1(oValue);
            if (sFieldName.Equals(_Value2)) return doValidate_Value2(oValue);
            if (sFieldName.Equals(_WOQId)) return doValidate_WOQId(oValue);
            if (sFieldName.Equals(_WstGpCd)) return doValidate_WstGpCd(oValue);
            if (sFieldName.Equals(_EnqLID)) return doValidate_EnqLID(oValue);
            if (sFieldName.Equals(_WOQLId)) return doValidate_WOQLId(oValue);
            if (sFieldName.Equals(_DBField)) return doValidate_DBField(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName) {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
         * Link the Name Field to the Form Item.
         */
        public void doLink_Name(string sControlName) {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
         * Link the U_EnqId Field to the Form Item.
         */
        public void doLink_EnqId(string sControlName) {
            moLinker.doLinkDataToControl(_EnqId, sControlName);
        }
        /**
         * Link the U_ItemCode Field to the Form Item.
         */
        public void doLink_ItemCode(string sControlName) {
            moLinker.doLinkDataToControl(_ItemCode, sControlName);
        }
        /**
         * Link the U_ItemFName Field to the Form Item.
         */
        public void doLink_ItemFName(string sControlName) {
            moLinker.doLinkDataToControl(_ItemFName, sControlName);
        }
        /**
         * Link the U_ValidCd Field to the Form Item.
         */
        public void doLink_ValidCd(string sControlName) {
            moLinker.doLinkDataToControl(_ValidCd, sControlName);
        }
        /**
         * Link the U_Value1 Field to the Form Item.
         */
        public void doLink_Value1(string sControlName) {
            moLinker.doLinkDataToControl(_Value1, sControlName);
        }
        /**
         * Link the U_Value2 Field to the Form Item.
         */
        public void doLink_Value2(string sControlName) {
            moLinker.doLinkDataToControl(_Value2, sControlName);
        }
        /**
         * Link the U_WOQId Field to the Form Item.
         */
        public void doLink_WOQId(string sControlName) {
            moLinker.doLinkDataToControl(_WOQId, sControlName);
        }
        /**
         * Link the U_WstGpCd Field to the Form Item.
         */
        public void doLink_WstGpCd(string sControlName) {
            moLinker.doLinkDataToControl(_WstGpCd, sControlName);
        }
        /**
         * Link the U_EnqLID Field to the Form Item.
         */
        public void doLink_EnqLID(string sControlName) {
            moLinker.doLinkDataToControl(_EnqLID, sControlName);
        }
        /**
         * Link the U_WOQLId Field to the Form Item.
         */
        public void doLink_WOQLId(string sControlName) {
            moLinker.doLinkDataToControl(_WOQLId, sControlName);
        }
        /**
         * Link the U_DBField Field to the Form Item.
        */
        public void doLink_DBField(string sControlName) {
            moLinker.doLinkDataToControl(_DBField, sControlName);
        }
        #endregion

    }
}
