/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 17/03/2015 10:00:28
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
	public class IDH_RCEMPLOYEE: com.idh.dbObjects.Base.IDH_RCEMPLOYEE{ 

		public IDH_RCEMPLOYEE() : base() {
			
		}

   	public IDH_RCEMPLOYEE( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	}

    private IDH_ROUTECL moRouteCl;
    public IDH_ROUTECL RouteCl
    {
        get { return moRouteCl; }
        set { moRouteCl = value; }
    }


    //Set the Job Row Code for all rows with a Row Code of -1
    public void doSetRowCode(string sRowCode)
    {
        if (Count > 0)
        {
            int iCurrentRow = CurrentRowIndex;
            first();
            while (next())
            {
                if (U_ROUTECL.Equals("-1"))
                    U_ROUTECL = sRowCode;
            }
            gotoRow(iCurrentRow);
        }
    }

    public int getByRCRow(string sRouteCode)
    {
        return getData(_ROUTECL + " = '" + sRouteCode + "'", null);
    }

    //Set the RouteClosure Code for all rows with a Row Code of -1
    public void doSetRCCode(string sRCCode)
    {
        if (Count > 0)
        {
            int iCurrentRow = CurrentRowIndex;
            first();
            while (next())
            {
                if (U_ROUTECL.Equals("-1"))
                    U_ROUTECL = sRCCode;
            }
            gotoRow(iCurrentRow);
        }
    }

	}
}
