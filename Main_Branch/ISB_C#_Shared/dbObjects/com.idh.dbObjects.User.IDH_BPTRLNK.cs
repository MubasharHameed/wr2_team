/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 2014/09/29 11:46:12 PM
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User{
   [Serializable] 
	public class IDH_BPTRLNK: com.idh.dbObjects.Base.IDH_BPTRLNK{ 

		public IDH_BPTRLNK() : base() {
            msAutoNumKey = "TRBPLNK";
		}

   	    public IDH_BPTRLNK( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
            msAutoNumKey = "TRBPLNK";
   	    }
	}
}
