/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 14/03/2016 12:48:52
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.idh.dbObjects.User {
    [Serializable]
    public class IDH_LTIDETAIL : com.idh.dbObjects.Base.IDH_LTIDETAIL {

        public IDH_LTIDETAIL()
            : base() {
            msAutoNumKey = "SEQLTID";
        }

        public IDH_LTIDETAIL(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oIDHForm, oForm) {
            msAutoNumKey = "SEQLTID";
        }
    }
}
