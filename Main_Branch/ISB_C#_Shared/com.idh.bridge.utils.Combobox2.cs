/*
 * Created by SharpDevelop.
 * User: Louis Viljoen
 * Date: 2009/07/01
 * Time: 04:44 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Windows.Forms;
using com.idh.win.controls;
using com.idh.controls;
using com.idh.win.controls.SBO;

namespace com.idh.bridge.utils
{
	/// <summary>
	/// Description of com_idh_bridge_utils_Combobox.
	/// </summary>
	public class Combobox2
	{
        public static void doFillCombo(object oForm, string sItemID, string sTable, string sKeyVal, string sValDesc, string sWhere, string sOrder, bool bAddAny) {
            if ( bAddAny )
                doFillCombo( oForm, sItemID, sTable, sKeyVal, sValDesc, sWhere, sOrder, "Any" );
            else
                doFillCombo(oForm, sItemID, sTable, sKeyVal, sValDesc, sWhere, sOrder, null);
        }

        public static void doFillCombo(object oForm, string sItemID, string sTable, string sKeyVal, string sValDesc, string sWhere, string sOrder, string sBlankName) {
            if (oForm == null)
                return;

            SAPbouiCOM.Form oSBOForm = null;
            Form oWinForm = null;
            if (oForm is SAPbouiCOM.Form)
                oSBOForm = oForm as SAPbouiCOM.Form;
            else if (oForm is System.Windows.Forms.Form)
                oWinForm = oForm as System.Windows.Forms.Form;

            object oCombo = null;
            if (oSBOForm != null)
                oCombo = oSBOForm.Items.Item(sItemID).Specific;
            else if (oWinForm != null)
                oCombo = oWinForm.Controls.Find(sItemID, true)[0];

            doFillCombo(oCombo, sTable, sKeyVal, sValDesc, sWhere, sOrder, sBlankName);
        }

        public static void doFillCombo( object oCombo, string sTable, string sKeyVal, string sValDesc, string sWhere, string sOrder) {
            doFillCombo( oCombo, sTable, sKeyVal, sValDesc, sWhere, sOrder, null );
        }
        //public static void doFillCombo(object oCombo, string sTable, string sKeyVal, string sValDesc, string sWhere, string sOrder, string sBlankName) {
        //    doFillCombo_(oCombo, sTable, sKeyVal, sValDesc, sWhere, sOrder, sBlankName, false);
        //}
		public static void doFillCombo( object oCombo, string sTable, string sKeyVal, string sValDesc, string sWhere, string sOrder, string sBlankName ){
			string sVal;
            string sDesc;			
			DataRecords oRecords = null;
			
			string sQry = "SELECT " + sKeyVal + (sValDesc != null?", " + sValDesc:"") + " from " + sTable;

            //WHERE
            if ( sWhere != null && sWhere.Length > 0 ) {
            	sQry = sQry + " WHERE " + sWhere;
            }

            //ORDER
            if ( sOrder != null && sOrder.Length > 0 ) {
            	sQry = sQry + " Order by " + sOrder;
            }
            else {
                sQry = sQry + " Order by " + sKeyVal;
            }
            
			try {
                if ( oCombo != null )
	                doClearCombo(oCombo);

	            oRecords = DataHandler.INSTANCE.doBufferedSelectQuery("doFillCombo", sQry);
                if (oRecords != null && oRecords.RecordCount > 0) {
	        		
	        		if ( oCombo != null ) {
  	      				if ( sBlankName!=null )
							doAddValueToCombo( oCombo, "", sBlankName);

                        for (int i = 0; i < oRecords.RecordCount; i++) {
	    					oRecords.gotoRow(i);
	    					sVal = oRecords.getValue(0).ToString();
                            if (sValDesc == null)
                                doAddValueToCombo(oCombo, sVal, sVal, false);
                            else {
                                sDesc = oRecords.getValue(1).ToString();
                                doAddValueToCombo(oCombo, sVal, sDesc, false);
                            }
		        		}
  	      			} 
	            }
			}
	        catch ( Exception ex ) {
            	//DataHandler.INSTANCE.doError("Exception: " + ex.ToString(),"");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
        	}			
		}
		
		public static void doClearCombo( object oCombo ) {
			if ( oCombo is SAPbouiCOM.ComboBox ) {
				SAPbouiCOM.ValidValues oValues = ((SAPbouiCOM.ComboBox)oCombo).ValidValues;
				if ( oValues == null )
					return;

					while ( oValues.Count > 0 ) {
		            	oValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index);
				}
            } else if (oCombo is SAPbouiCOM.ComboBoxColumn) {
                SAPbouiCOM.ValidValues oValues = ((SAPbouiCOM.ComboBoxColumn)oCombo).ValidValues;
                if (oValues == null)
                    return;

                while (oValues.Count > 0) {
                    oValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index);
                }
            } else if (oCombo is ComboBox) {
                ((ComboBox)oCombo).Items.Clear();
            } else if (oCombo is LPVComboBox || oCombo is SBOComboBox) { 
                ((LPVComboBox)oCombo).Items.Clear();
            }
		}

//		/**
//		 * Add a value to a combo
//		 */
//		public static void doAddValueToCombo( ref SAPbouiCOM.ComboBox, string sValue, string sDescription){
//			doAddValueToCombo( oCombo, sValue, sDescription, true );
//		}
//		/**
//		 * Add a value to a combo
//		 */
//		public static void doAddValueToCombo( ref SAPbouiCOM.ComboBox, string sValue, string sDescription, bool bDoTranslate){
//				try {
//					(oCombo as SAPbouiCOM.ComboBox).ValidValues.Add(sValue, Translation.INSTANCE.getTranslatedWord(sDescription));
//                }
//                catch (Exception){
//                }
//		}
		
		/**
		 * Add a value to a combo
		 */
		public static void doAddValueToCombo( object oCombo, string sValue, string sDescription){
			doAddValueToCombo( oCombo, sValue, sDescription, true );
		}
		/**
		 * Add a value to a combo
		 */
		public static void doAddValueToCombo( object oCombo, string sValue, string sDescription, bool bDoTranslate){
			if ( oCombo is SAPbouiCOM.ComboBox ){
				try {
					(oCombo as SAPbouiCOM.ComboBox).ValidValues.Add(sValue, Translation.getTranslatedWord(sDescription));
                }
                catch (Exception){
                }
            } else if (oCombo is SAPbouiCOM.ComboBoxColumn) {
                try {
                    (oCombo as SAPbouiCOM.ComboBoxColumn).ValidValues.Add(sValue, Translation.getTranslatedWord(sDescription));
                } catch (Exception) {
                }
            } else if (oCombo is ComboBox) {
                (oCombo as ComboBox).Items.Add(new ValuePair(sValue, sDescription));
            } else if (oCombo is LPVComboBox || oCombo is SBOComboBox) {
                //((LPVComboBox)oCombo).Items.Add(new ValuePair(sValue, sDescription));
                ((LPVComboBox)oCombo).AddItem(new ValuePair(sValue, sDescription));
            }
		}		
		/**
		 * Select first Record
		 */
		public static void doSelectFirstRow( object oCombo ) {
			if ( oCombo is SAPbouiCOM.ComboBox ){
				try {
                    SAPbouiCOM.ComboBox ooCombo = (oCombo as SAPbouiCOM.ComboBox);
					if ( ooCombo.ValidValues.Count > 0 )
                        ooCombo.Select(0,SAPbouiCOM.BoSearchKey.psk_Index);
                }
                catch (Exception e){
                }
            } else if (oCombo is SAPbouiCOM.ComboBoxColumn) {
                try {
                    SAPbouiCOM.ComboBoxColumn ooCombo;
                    ooCombo = (oCombo as SAPbouiCOM.ComboBoxColumn);
                    //if ( ooCombo.ValidValues.Count > 0 )
                } catch (Exception e) {
                }
            } else if (oCombo is ComboBox) {
                ComboBox ooCombo = (oCombo as ComboBox);
                if ( ooCombo.Items.Count > 0 )
                    ooCombo.SelectedItem = 0;
            } else if (oCombo is LPVComboBox || oCombo is SBOComboBox) {
                LPVComboBox ooCombo = ((LPVComboBox)oCombo);
                if (ooCombo.Items.Count > 0)
                    ooCombo.SelectedIndex = 0;
            }
		}		
	}
}
