﻿using System;
using System.Collections;
using System.Xml;

using com.idh.dbObjects.User;
using com.idh.utils;
using com.idh.bridge.resources;
using com.idh.bridge;
using com.idh.controls;

using System.IO;

namespace com.isb.bridge.lookups {

    public class Config : com.idh.bridge.lookups.Config {

        //public new static Config INSTANCE {
        //    get {
        //        return (Config)com.idh.bridge.lookups.Config.INSTANCE;
        //    }
        //    set { INSTANCE = value; }
        //}

        public Config(SAPbobsCOM.Company oSBOCompany, string sConfigTable) : base(oSBOCompany, sConfigTable) {
            INSTANCE = this;
        }

        public Config(string sConfigTable) : base(sConfigTable) {
            INSTANCE = this;
        }
    }
}
