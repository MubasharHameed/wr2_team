using com.idh.utils;
using com.idh.bridge;
using com.idh.bridge.lookups;

using com.isb.enq.dbObjects.User;
using System;

namespace com.isb.enq {
    public class IDH_PBI_Handler : WR1_PBI.IDH_PBI_Handler {
        public IDH_PBI_Handler(ref SAPbobsCOM.Company SBO_Company) : base( ref SBO_Company, false) {
        }

        public IDH_PBI_Handler(ref SAPbobsCOM.Company SBO_Company, bool bFromService) : base(ref SBO_Company, bFromService) {
        }
        
#region "PBI Executer for Mass Updater"
        public static bool doExecutePBIHandler(ref com.idh.dbObjects.User.IDH_PBI oPBI, IDH_LVSBTH oBatchDetail, SAPbobsCOM.Company SBOCompany, bool bFromService, bool bDoNotRemove, DateTime dActDate) {
            try {
                bool DoAddNewPBI = false;
                IDH_LVSAMD oActionObject = new IDH_LVSAMD();
                oActionObject.UnLockTable = true;

                string sAction = oBatchDetail.U_AmndTp;
                int iRet = oActionObject.getData( IDH_LVSAMD._AmdDesc + "='" + sAction + "' And " + IDH_LVSAMD._Active + "='Y'", "");
                DoAddNewPBI = oActionObject.doCheckAmendListChange(IDH_LVSAMD.AddPBI);

                IDH_PBI_Handler oWOM = new IDH_PBI_Handler(ref SBOCompany, bFromService);
                bool bDoRestOfUpdates = true;
                bool bResult = true;
                bool bDoToEnd = false;
                string sValToEnd = "";

                // a check box labeled To End on PBI; right to Action date getUFValue(oForm, "IDHTOE");
                if (sValToEnd == "Y") bDoToEnd = true;

                if ( !bDoNotRemove && !DoAddNewPBI ) {
                    bool doRemovePBI = false;

                    if (oActionObject.doCheckAmendListChange(IDH_LVSAMD.RemovePBI))
                        doRemovePBI = true;
                    
                    if ( doRemovePBI && bDoRestOfUpdates && oPBI.U_IDHRECEN == 2 && oPBI.U_IDHRECED != DateTime.MinValue) {
                        if (oPBI.U_IDHRECED != DateTime.MinValue && oPBI.U_IDHRECED_AsString().Length >= 6)
                            bResult = oWOM.doClearAfterTerminationDate(oPBI.Code, oPBI.U_IDHRECED);
                        bDoRestOfUpdates = false;
                    }
                }

                if (bDoRestOfUpdates) {
                    if (bDoToEnd || oPBI.U_IDHNEXTR > dActDate) {
                        DateTime dSetNextRun = oPBI.U_IDHNEXTR;
                        bResult = oWOM.ProcessOrderRunToEnd(oPBI.Code, dActDate, dSetNextRun);
                    } else {
                        bResult = oWOM.ProcessOrderRun(oPBI.Code, dActDate);
                        if (bResult && Config.ParameterAsBool("PBIRTDAY", false)) {
                            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            oPBI.getByKey(oPBI.Code);
                            while (bResult && DateTime.Compare(oPBI.U_IDHNEXTR, new DateTime(DateTime.Today.AddMonths(1).Year, DateTime.Today.AddMonths(1).Month, 1)) < 0) {
                                dActDate = dActDate.AddMonths(oPBI.U_IDHRECFQ);
                                bResult = oWOM.ProcessOrderRun(oPBI.Code, dActDate);
                                oPBI.getByKey(oPBI.Code);
                            }
                        }
                    }
                    if (!bResult) return false;
                } else {
                    return bResult;
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "IDH_PBI_Handler.doExecutePBIHandler" });
                return false;
            } finally {
            }
            return true;
        }

        public static bool doExecutePBIHandler(ref idh.dbObjects.User.IDH_PBI oPBI, IDH_LVSBTH oBatchDetail, SAPbobsCOM.Company SBOCompany, bool bFromService, bool bDoNotRemove) {
            try {
                DateTime dActDate = oBatchDetail.U_EffDate;
                if (!(oBatchDetail.U_EffDate.Year == 1 || oBatchDetail.U_EffDate.Year == 1900)) {
                    string sPBIStart = oPBI.U_IDHRECSD_AsString();
                    DateTime dPBIStart = Conversions.ToDateTime(sPBIStart);
                    dActDate = oBatchDetail.U_EffDate;
                    if (DateTime.Compare(dActDate, dPBIStart) < 0)
                        dActDate = dPBIStart;
                }
                return doExecutePBIHandler( ref oPBI, oBatchDetail, SBOCompany, bFromService, bDoNotRemove, dActDate);

                //Return bResult
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "IDH_PBI_Handler.doExecutePBIHandler"});
                return false;
            } finally { 
            }
            return true;
        }
# endregion
    }
}
