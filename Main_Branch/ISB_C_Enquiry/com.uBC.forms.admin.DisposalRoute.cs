using Microsoft.VisualBasic;
using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Data;
//using System.Diagnostics;
//using System.IO;

//using IDHAddOns.idh.controls;
//Imports com.idh.bridge
//using com.idh.bridge.data;
//using com.idh.dbObjects;
using com.idh.controls;
using com.idh.bridge;
using com.idh.bridge.lookups;
//using com.uBC.utils;
using com.idh.dbObjects.numbers;

using com.isb.enq.dbObjects.User;
using IDHAddOns.idh.controls;

namespace com.isb.forms.Enquiry.admin {

    //This is the Base Form object from the Framework
    public class DisposalRoute : com.idh.forms.oo.Form {

        private com.idh.dbObjects.User.IDH_FORMSET moFormSettings;
        //The Result Grid
        private DBOGrid moDispRoutGrid;
        //The CIP Data Object
        private IDH_DISPRTCD moDBObject;

        #region "Form Item Setters"

        public string ItemGroup = "";
        public string DisposalSiteCode {
            get { return getUFValue("IDH_DISPCD").ToString(); }
            set {
                setUFValue("IDH_DISPCD", value);
                setUFValue("IDH_DISPNM", com.idh.bridge.lookups.Config.INSTANCE.doGetBPName(value));
            }
        }
        public string ContainerCode {
            get { return getUFValue("IDH_ITMCD").ToString(); }
            set {
                setUFValue("IDH_ITMCD", value);
                setUFValue("IDH_ITMNM", com.idh.bridge.lookups.Config.INSTANCE.doGetItemDescription(value));
            }
        }
        public string ZipCode {
            get { return getUFValue("IDH_POSCOD").ToString(); }
            set { setUFValue("IDH_POSCOD", value); }
        }
        public string Address {
            get { return getUFValue("IDH_ADDR").ToString(); }
            set { setUFValue("IDH_ADDR", value); }
        }
        public string AddressLineNum {
            get { return getUFValue("IDH_ADLN").ToString(); }
            set { setUFValue("IDH_ADLN", value); }
        }
        public string WasteCode {
            get { return getUFValue("IDH_WASTCD").ToString(); }
            set {
                setUFValue("IDH_WASTCD", value);
                setUFValue("IDH_WASTNM", com.idh.bridge.lookups.Config.INSTANCE.doGetItemDescriptionWithNoBuffer(value));
            }
        }
        public string UOM {
            get { return getUFValue("IDH_UOM").ToString(); }
            set { setUFValue("IDH_UOM", value); }
        }

        #endregion

        //**
        // Register the form controller
        //**
        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_DRT", sParMenu, 5, "DisposalRoute.srf", true, true, false, "Disposal Route", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        public DisposalRoute(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, ref SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
            moDBObject = new IDH_DISPRTCD(IDHForm, SBOForm);
            moDBObject.ApplyDefaultsOnNewRows = false;

            doSetHandlers();
            //## Start if CIP is called from any other form as pop-up then oSBOForm can be set to nothing and we can use this
            if (oSBOForm != null) {
                setSharedData("IDH_BPSRCHOPEND", "");
                setSharedData("IDH_ITMSRCHOPEND", "");
                setSharedData("IDH_ADRSRCHOPEND", "");
            }
            //## End
        }

        //**
        //** This will be called by the base constructure
        //**
        public void doSetHandlers() {
            doStartEventFilterBatch(true);

            addHandler_ITEM_PRESSED("IDH_CFLTIP", new ev_Item_Event(doCustomerLookup));

            addHandler_ITEM_PRESSED("IDH_ITMCH", new ev_Item_Event(doItemLookup));
            addHandler_ITEM_PRESSED("IDH_ADDRCH", new ev_Item_Event(doTIPAddressLookup));
            addHandler_ITEM_PRESSED("IDH_WASTSE", new ev_Item_Event(doWasteCodeLookup));
            addHandler_COMBO_SELECT("IDH_UOM", new ev_Item_Event(doSelectUOM));
            addHandler_VALIDATE_CHANGED("IDH_POSCOD", new ev_Item_Event(doZipCodeChange));
            addHandler_VALIDATE_CHANGED("IDH_DISPCD", new ev_Item_Event(doCustomerChanged));
            addHandler_VALIDATE_CHANGED("IDH_DISPNM", new ev_Item_Event(doCustomerChanged));

            addHandler_VALIDATE_CHANGED("IDH_ITMCD", new ev_Item_Event(doItemChanged));
            addHandler_VALIDATE_CHANGED("IDH_ITMNM", new ev_Item_Event(doItemChanged));

            addHandler_VALIDATE_CHANGED("IDH_ADDR", new ev_Item_Event(doCustomerAddressChanged));
            addHandler_VALIDATE_CHANGED("IDH_WASTCD", new ev_Item_Event(doWasteCodeChanged));
            addHandler_VALIDATE_CHANGED("IDH_WASTNM", new ev_Item_Event(doWasteCodeChanged));

            Handler_VALIDATE_CHANGED = new ev_Item_Event(doTriggerLookup);
            addHandler_ITEM_PRESSED("IDH_ACTIVE", new ev_Item_Event(doShowOnlyActivePrices));
            Handler_Button_Add = new ev_Item_Event(doAddUpdateEvent);
            Handler_Button_Update = new ev_Item_Event(doAddUpdateEvent);

            doCommitEventFilters();
        }

        #region "FormEvents"

        public bool doAddUpdateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            try {
                if (pVal.BeforeAction == true) {
                    Freeze(true);
                    if (doValidate()) {
                        if (moDBObject.doProcessData()) {
                            doLoadData();
                            moDispRoutGrid.doApplyRules();

                        }
                    } else {
                        BubbleEvent = false;
                        return false;
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("Save form") });
                return false;
            } finally {
                Freeze(false);
            }
            return true;
        }



        public override bool doItemEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            //if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED) {
            //    if (pVal.BeforeAction == false) {
            //    }
            //} else {

            //    if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_VALIDATE && pVal.BeforeAction == false) {

            //        if (pVal.ItemUID == "LINESGRID") {
            //        }
            //    }
            //}
            return true;
        }

        //** The Postal Code change event
        //** Return True if the Event must be handled by the other Objects
        public bool doZipCodeChange(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == false) {
                string sZipCode = getUFValue("IDH_POSCOD").ToString();
                //moDBObject.doAddDefaultValue(IDH_DISPRTCD._TZpCd, sZipCode);
                doLoadData();
            }
            return false;
        }


        public bool doSelectUOM(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == false) {
                string sUOM = getUFValue("IDH_UOM").ToString();
                // moDBObject.doAddDefaultValue(IDH_DISPRTCD._UOM, sUOM);
                doLoadData();
            }
            return false;
        }

        //** The doTriggerLookup handler
        //** Return True if the Event must be handled by the other Objects
        public bool doTriggerLookup(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == false) {
                if (!pVal.ItemUID.Equals("LINESGRID") && pVal.ItemChanged) {
                    doLoadData();
                }
            }
            return false;
        }

        //** The customer Choose Button
        //** Return True if the Event must be handled by the other Objects
        public bool doCustomerLookup(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.Before_Action == false) {

                string sCardCode = getUFValue("IDH_DISPCD").ToString();
                string sCardName = getUFValue("IDH_DISPNM").ToString();
                //Config.INSTANCE.getParameterAsBool(msOrderType + "BPFLT", true)
                if (Config.INSTANCE.getParameterAsBool("WOBPFLT", true) == false) {
                    setSharedData("IDH_TYPE", "");
                    setSharedData("IDH_GROUP", "");
                } else {
                    setSharedData("IDH_TYPE", "S");
                    setSharedData("IDH_GROUP", Config.Parameter("BGSDispo"));
                }
                if (pVal.ItemUID == "IDH_DISPCD") {
                    setSharedData("IDH_BPCOD", sCardCode);
                } else if (pVal.ItemUID == "IDH_DISPNM") {
                    setSharedData("IDH_NAME", sCardName);
                } else {
                    setSharedData("IDH_BPCOD", sCardCode);
                    setSharedData("IDH_NAME", sCardName);
                }



                setSharedData("TRG", "ST");
                setSharedData("CALLEDITEM", "IDH_DISPCD");
                //if (false) {
                setSharedData("SILENT", "SHOWMULTI");
                //}
                doOpenModalForm("IDHCSRCH", moDBObject.SBOForm);
                ////##Start 10-07-2013
                //if (sCardCode == "FALLBACK") {
                //    return false;


                //}
                ////##
                //setUFValue("IDH_DISPNM", "");


                //setSharedData("TRG", "CUS");
                //setSharedData("IDH_BPCOD", sCardCode);
                ////##Start 03-07-2013
                //setSharedData("IDH_NAME", "");

                ////## End
                //setSharedData("IDH_TYPE", "C");
                //setSharedData("SILENT", "SHOWMULTI");
                ////##Start 03-07-2013
                ////doOpenModalForm("IDHCSRCH")

                //if (Config.INSTANCE.getParameterAsBool("UNEWSRCH", false) == false) {
                //    doOpenModalForm("IDHCSRCH");
                //    //Actual BP Search Form 
                //} else {
                //    if (getSharedData("IDH_BPSRCHOPEND") == null || getSharedData("IDH_BPSRCHOPEND").ToString()=="") {
                //        doOpenModalForm("IDHNCSRCH", moDBObject.SBOForm);
                //        //NEW BP Search Form 
                //    }
                //}


                ////## End
            }
            return false;
        }

        //** The Customer Changed
        //** Return True if the Event must be handled by the other Objects
        public bool doCustomerChanged(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == false) {
                string sCardCode = getUFValue("IDH_DISPCD").ToString();
                string sCardName = getUFValue("IDH_DISPNM").ToString();
                //mbCalledFromChanged = true;
                if (pVal.ItemUID == "IDH_DISPCD") {
                    if (sCardCode.Trim().Length == 0) {
                        setUFValue("IDH_DISPNM", "");
                        doLoadData();
                    } else {
                        return doCustomerLookup(ref pVal, ref BubbleEvent);
                    }
                } else {
                    if (sCardName.Trim().Length == 0) {
                        setUFValue("IDH_DISPCD", "");
                        doLoadData();
                    } else {
                        return doCustomerLookup(ref pVal, ref BubbleEvent);
                    }
                }

            }
            return false;
        }

        public bool doItemLookup(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.Before_Action == false) {
                string sItemCd = getUFValue("IDH_ITMCD").ToString();
                string sItemDesc = getUFValue("IDH_ITMNM").ToString();

                if (pVal.ItemUID == "IDH_ITMCD") {
                    setSharedData("IDH_ITMNAM", "");
                    setSharedData("IDH_ITMCOD", sItemCd);
                } else if (pVal.ItemUID == "IDH_ITMNM") {
                    setSharedData("IDH_ITMNAM", sItemDesc);
                    setSharedData("IDH_ITMCOD", "");
                } else {
                    setSharedData("IDH_ITMNAM", sItemDesc);
                    setSharedData("IDH_ITMCOD", sItemCd);
                }

                setSharedData("TRG", "ITM");
                setSharedData("SILENT", "SHOWMULTI");
                if (getSharedData("IDH_ITM1SRCHOPEND") == null || getSharedData("IDH_ITM1SRCHOPEND").ToString().Trim() == "") {
                    doOpenModalForm("IDHISRC");
                }
            }
            return false;
        }

        //** The Container Changed
        //** Return True if the Event must be handled by the other Objects
        public bool doItemChanged(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == false) {
                string sItemCd = getUFValue("IDH_ITMCD").ToString();
                string sItemDesc = getUFValue("IDH_ITMNM").ToString();
                if (pVal.ItemUID == "IDH_ITMCD") {
                    if (sItemCd.Trim().Length == 0) {
                        setUFValue("IDH_ITMNM", "");
                        doLoadData();
                    } else {
                        return doItemLookup(ref pVal, ref BubbleEvent);
                    }
                } else {
                    if (sItemDesc.Trim().Length == 0) {
                        setUFValue("IDH_ITMCD", "");
                        doLoadData();
                    } else {
                        return doItemLookup(ref pVal, ref BubbleEvent);
                    }
                }

            }

            return false;
        }

        //** The Customer Address Choose Button
        //** Return True if the Event must be handled by the other Objects
        public bool doTIPAddressLookup(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.Before_Action == false) {
                string sCardCode = getUFValue("IDH_DISPCD").ToString();
                string sAddress = getUFValue("IDH_ADDR").ToString();

                if ((sCardCode != null) && sCardCode.Length > 0) {
                    setSharedData("TRG", "SA");
                    setSharedData("IDH_CUSCOD", sCardCode);
                    setSharedData("IDH_ADRTYP", "S");
                    setSharedData("SILENT", "SHOWMULTI");
                    setSharedData("IDH_ADDRES", sAddress);
                    if (Config.INSTANCE.getParameterAsBool("UNEWSRCH", false) == false) {
                        doOpenModalForm("IDHASRCH");
                        //Actual Address Search Form 
                    } else {
                        if (getSharedData("IDH_ADRSRCHOPEND") == null || getSharedData("IDH_ADRSRCHOPEND").ToString().Trim() == "") {
                            doOpenModalForm("IDHASRCH3", moDBObject.SBOForm);
                            //NEW Address Search Form 
                        }
                    }
                    //## End
                }
            }
            return false;
        }

        //** The Customer Address Changed
        //** Return True if the Event must be handled by the other Objects
        public bool doCustomerAddressChanged(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == false) {
                string sAddress = getUFValue("IDH_ADDR").ToString();
                if (sAddress.Trim().Length == 0) {
                    ZipCode = "";
                    AddressLineNum = "";
                    doLoadData();
                } else {
                    return doTIPAddressLookup(ref pVal, ref BubbleEvent);
                }
            }
            return false;
        }

        public bool doWasteCodeLookup(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.Before_Action == false) {
                string sWasteCode = getUFValue("IDH_WASTCD").ToString();
                string sWastedesc = getUFValue("IDH_WASTNM").ToString();
                //setUFValue("IDH_WASTNM", "");

                setSharedData("TRG", "WC");
                setSharedData("IDH_GRPCOD", com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup());
                if (pVal.ItemUID == "IDH_WASTCD") {
                    setSharedData("IDH_ITMCOD", sWasteCode);
                    setSharedData("IDH_ITMNAM", "");
                } else if (pVal.ItemUID == "IDH_WASTNM") {
                    setSharedData("IDH_ITMCOD", "");
                    setSharedData("IDH_ITMNAM", sWastedesc);
                } else {
                    setSharedData("IDH_ITMCOD", sWasteCode);
                    setSharedData("IDH_ITMNAM", sWastedesc);
                }

                setSharedData("IDH_INVENT", "");
                setSharedData("SILENT", "SHOWMULTI");

                if (getSharedData("IDH_ITMSRCHOPEND") == null || getSharedData("IDH_ITMSRCHOPEND").ToString().Trim() == "") {

                    doOpenModalForm("IDHWISRC");
                }
            }
            return false;
        }

        //** The Waste Code Changed
        //** Return True if the Event must be handled by the other Objects
        public bool doWasteCodeChanged(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == false) {
                string sWasteCode = getUFValue("IDH_WASTCD").ToString();
                string sWasteDesc = getUFValue("IDH_WASTNM").ToString();
                if (pVal.ItemUID == "IDH_WASTCD") {
                    if (sWasteCode.Trim().Length == 0) {
                        setUFValue("IDH_WASTNM", "");
                        doLoadData();
                    } else {
                        return doWasteCodeLookup(ref pVal, ref BubbleEvent);
                    }
                } else {
                    if (sWasteDesc.Trim().Length == 0) {
                        setUFValue("IDH_WASTCD", "");
                        doLoadData();
                    } else {
                        return doWasteCodeLookup(ref pVal, ref BubbleEvent);
                    }
                }

            }
            return false;
        }
        public bool doShowOnlyActivePrices(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == false) {
                doLoadData();
            }
            return false;
        }
        #endregion

        #region "Dialog Handler"

        public override void doHandleModalCanceled(string sModalFormType) {
            base.doHandleModalCanceled(sModalFormType);
            if ((sModalFormType.Equals("IDHCSRCH") || sModalFormType.Equals("IDHNCSRCH") || sModalFormType.Equals("IDHISRC") || sModalFormType.Equals("IDHASRCH") || sModalFormType.Equals("IDHWISRC")) || sModalFormType.Equals("IDHASRCH3")) {
                doLoadData();
            }
            if (sModalFormType.Equals("IDHCSRCH") || sModalFormType.Equals("IDHNCSRCH")) {
                string sTarget = null;
                sTarget = getSharedData("TRG").ToString();
                if (sTarget == null)
                    return;
                if (sTarget.Equals("ST")) {
                    if (!SBOForm.ActiveItem.Equals("IDH_DISPCD")) {
                        setFocus("IDH_DISPCD");
                        setUFValue("IDH_DISPCD", "");
                        setUFValue("IDH_DISPNM", "");
                        doLoadData();
                    }
                } else if (sTarget.Equals("SUP")) {
                    if (!SBOForm.ActiveItem.Equals("IDH_BP2CD")) {
                        setFocus("IDH_BP2CD");
                        setUFValue("IDH_BP2CD", "");
                        doLoadData();
                    }
                }
            } else if (sModalFormType.Equals("IDHASRCH") || sModalFormType.Equals("IDHASRCH3")) {
                string sTarget = null;
                sTarget = getSharedData("TRG").ToString();
                if (sTarget.Equals("SA")) {
                    if (!SBOForm.ActiveItem.Equals("IDH_ADDR")) {
                        setFocus("IDH_ADDR");
                        setUFValue("IDH_ADDR", "");
                        setUFValue("IDH_ADLN", "");
                        setUFValue("IDH_POSCOD", "");
                        doLoadData();
                    }
                } else if (sTarget.Equals("SPA")) {
                    if (!SBOForm.ActiveItem.Equals("IDH_SPADDR")) {
                        setFocus("IDH_SPADDR");
                        setUFValue("IDH_SPADDR", "");
                        setUFValue("IDH_SPADLN", "");
                        setUFValue("IDH_SZIPCD", "");
                        doLoadData();
                    }
                }
            } else if (sModalFormType.Equals("IDHISRC")) {
                string sTarget = getSharedData("TRG").ToString();
                //Container Code (ItemLookup)
                if (sTarget.Equals("ITM")) {
                    if (!SBOForm.ActiveItem.Equals("IDH_ITMCD")) {
                        setFocus("IDH_ITMCD");
                        setUFValue("IDH_ITMCD", "");
                        setUFValue("IDH_ITMNM", "");
                        doLoadData();
                    }
                    // Additional Code Looup
                } else if (sTarget.Equals("ADD")) {
                    if (!SBOForm.ActiveItem.Equals("IDH_ADDCD")) {
                        setFocus("IDH_ADDCD");
                        setUFValue("IDH_ADDCD", "");
                        setUFValue("IDH_ADDNM", "");
                        doLoadData();
                    }

                }
                //WasteCodeLooup
            } else if (sModalFormType.Equals("IDHWISRC")) {
                if (!SBOForm.ActiveItem.Equals("IDH_WASTCD")) {
                    setFocus("IDH_WASTCD");
                    setUFValue("IDH_WASTCD", "");
                    setUFValue("IDH_WASTNM", "");
                    doLoadData();
                }
            }
        }

        //CLOSING MODAL RECEIVING DATA
        public override void doHandleModalResultShared(string sModalFormType, string sLastButton) {
            try {
                if (sModalFormType.Equals("IDHCSRCH") | sModalFormType.Equals("IDHNCSRCH")) {
                    string sTarget = null;
                    string sCardCode = null;
                    string sCardName = null;
                    sTarget = getSharedData("TRG").ToString();
                    if (sTarget.Equals("ST")) {
                        sCardCode = getSharedData("CARDCODE").ToString();
                        sCardName = getSharedData("CARDNAME").ToString();
                        setUFValue("IDH_DISPCD", sCardCode);
                        setUFValue("IDH_DISPNM", sCardName);
                        doLoadData();
                    }
                } else if (sModalFormType.Equals("IDHISRC")) {
                    string sTarget = null;
                    string sItemCode = null;
                    string sItemName = null;
                    sTarget = getSharedData("TRG").ToString();
                    if (sTarget.Equals("ITM")) {
                        sItemCode = getSharedData("ITEMCODE").ToString();
                        sItemName = getSharedData("ITEMNAME").ToString();
                        setUFValue("IDH_ITMCD", sItemCode);
                        setUFValue("IDH_ITMNM", sItemName);
                        doLoadData();
                    }
                } else if (sModalFormType.Equals("IDHASRCH") || sModalFormType.Equals("IDHASRCH3")) {
                    string sTarget = null;
                    sTarget = getSharedData("TRG").ToString();
                    if (sTarget.Equals("SA")) {
                        Address = getSharedData("ADDRESS").ToString();
                        ZipCode = getSharedData("ZIPCODE").ToString();
                        AddressLineNum = getSharedData("ADDRSCD").ToString();
                        doLoadData();
                    }
                } else if (sModalFormType.Equals("IDHWISRC")) {
                    string sTarget = null;
                    string sWasteCd = null;
                    string sWasteName = null;
                    string sUOM = null;
                    sTarget = getSharedData("TRG").ToString();
                    if (sTarget.Equals("WC")) {
                        sWasteCd = getSharedData("ITEMCODE").ToString();
                        sWasteName = getSharedData("ITEMNAME").ToString();
                        sUOM = getSharedData("UOM").ToString();
                        if (sUOM == null || sUOM.Length == 0) {
                            sUOM = Config.INSTANCE.getDefaultUOM();
                        }
                        setUFValue("IDH_WASTCD", sWasteCd);
                        setUFValue("IDH_WASTNM", sWasteName);
                        doLoadData();
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResError(ex.Message, "EREXPMR", new string[] { sModalFormType });
            }
        }
        #endregion

        #region "Form Initializers"
        protected virtual void doTheGridLayout() {
            if (Config.INSTANCE.getParameterAsBool("FORMSET", false)) {
                if (moFormSettings == null) {
                    string sFormTypeId = SBOForm.TypeEx;
                    moFormSettings = new com.idh.dbObjects.User.IDH_FORMSET();
                    moFormSettings.getFormGridFormSettings(sFormTypeId, moDispRoutGrid.GridId, "");
                    moFormSettings.doAddFieldToGrid(moDispRoutGrid);
                    doSetListFields();
                    moFormSettings.doSyncDB(moDispRoutGrid);
                } else {
                    if (moFormSettings.SkipFormSettings) {
                        doSetListFields();
                    } else {
                        moFormSettings.doAddFieldToGrid(moDispRoutGrid);
                    }
                }
            } else {
                doSetListFields();
            }
            moDispRoutGrid.getSBOGrid().SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto;
        }
        //** Create the form
        public override void doCompleteCreate(ref bool BubbleEvent) {
            base.doCompleteCreate(ref BubbleEvent);

            SAPbouiCOM.Item oItem = Items.Item("IDH_CONTA");
            moDispRoutGrid = new DBOGrid(IDHForm, SBOForm, "LINESGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, moDBObject);

            SAPbouiCOM.FormSettings fSettings = default(SAPbouiCOM.FormSettings);
            fSettings = SBOForm.Settings;
            fSettings.Enabled = false;
            SAPbouiCOM.LinkedButton oLink = default(SAPbouiCOM.LinkedButton);
            oLink = (SAPbouiCOM.LinkedButton)Items.Item("IDH_LINKCR").Specific;
            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner;
            oLink = (SAPbouiCOM.LinkedButton)Items.Item("IDH_ITMLK").Specific;
            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_Items;
            oLink = (SAPbouiCOM.LinkedButton)Items.Item("IDH_WASTLK").Specific;
            oLink.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_Items;
            SupportedModes =(SAPbouiCOM.BoAutoFormMode) (Convert.ToInt32( SAPbouiCOM.BoFormMode.fm_OK_MODE )+ Convert.ToInt32(SAPbouiCOM.BoFormMode.fm_UPDATE_MODE));
            AutoManaged = false;
        }

        //'
        // ! - Negate the value - NOT - WHERE NOT x = Value
        // ^ - Null Or value - OR - WHERE x Is NULL Or x='' Or x = value
        // EQUEM - use equal and considerempty strings
        // REPLACE - replace the [@] with the input value
        // AUTO - this will make any "*" filter a LIKE and any "," filter a IN
        // MLIKE - Multiple Likes => (x  LIKE y* OR z LINE u*)
        // IN - will use the "in" option => in ()
        // FNZ_LIKE - The firsttime NULL and Zero Length must be included => IS NULL OR = '' OR LIKE val%
        //'
        protected void doSetFilterFields() {
            doAddUF("IDH_ACTIVE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1).AffectsFormMode = false;

            doAddUF("IDH_DISPCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = false;
            doAddUF("IDH_DISPNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = false;


            doAddUF("IDH_ITMCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = false;
            doAddUF("IDH_ITMNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = false;

            doAddUF("IDH_POSCOD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20).AffectsFormMode = false;
            doAddUF("IDH_ADDR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = false;
            doAddUF("IDH_ADLN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 10).AffectsFormMode = false;

            doAddUF("IDH_WASTCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = false;
            doAddUF("IDH_WASTNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = false;
            doAddUF("IDH_UOM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = false;
            doAddUF("IDH_DISPRT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = false;
            //doAddUF("IDH_DRDSC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 255).AffectsFormMode = false;
            //
            moDispRoutGrid.doAddFilterField("IDH_DISPCD", IDH_DISPRTCD._TipCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 100);
            moDispRoutGrid.doAddFilterField("IDH_DISPNM", IDH_DISPRTCD._TipNm, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 250);

            moDispRoutGrid.doAddFilterField("IDH_ITMCD", IDH_DISPRTCD._ItemCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 50);
            moDispRoutGrid.doAddFilterField("IDH_ITMNM", IDH_DISPRTCD._ItemDsc, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 250);

            moDispRoutGrid.doAddFilterField("IDH_WASTCD", IDH_DISPRTCD._WasCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 50);
            moDispRoutGrid.doAddFilterField("IDH_WASTNM", IDH_DISPRTCD._WasDsc, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 250);

            moDispRoutGrid.doAddFilterField("IDH_POSCOD", IDH_DISPRTCD._TZpCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 50);
            moDispRoutGrid.doAddFilterField("IDH_ADDR", IDH_DISPRTCD._TAddress, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100);
            moDispRoutGrid.doAddFilterField("IDH_ADLN", IDH_DISPRTCD._TAddrssLN, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100);
            moDispRoutGrid.doAddFilterField("IDH_UOM", IDH_DISPRTCD._UOM, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30);
            moDispRoutGrid.doAddFilterField("IDH_DISPRT", IDH_DISPRTCD._DisRCode, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 50);
            //moDispRoutGrid.doAddFilterField("IDH_DRDSC", IDH_DISPRTCD._Descrp, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 255);
        }

        //		SRC:IDHISRC(PROD)[ITEMCODE=U_ItemCd;IDH_GRPCOD=#Test][U_ItemCd=ITEMCODE;U_ItemDs=ITEMNAME]
        //		   :FORMTPE(TARGET)[INPUT][OUTPUT]
        //					 INPUT - %USE VALUE AS IS
        //							 #USE GRID VALUE
        //							 >USE ITEM VALUE
        //					 OUTPUT - SRCFIELDVAL - use this value on as the new value of the current field
        //							  COLID=SRCFIELDVAL - Use the result of the search field val as the new value for the
        //												- Noted Col
        //							  (EQVAL?NEWVAL:VAL) - If the Value = to EQVAL Then Use NEWVAL else use Val
        protected virtual void doSetListFields() {
            //string sAddGrp = com.idh.bridge.lookups.Config.INSTANCE.doGetAdditionalExpGroup();
            string sWastGrp = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup();

            string sDefaultUOM = moDBObject.doGetDefaultValue(IDH_DISPRTCD._UOM).ToString();
            if (sDefaultUOM == null || sDefaultUOM.Length == 0) {
                sDefaultUOM = com.idh.bridge.lookups.Config.INSTANCE.getDefaultUOM();
            }
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._Code, "Code", false, 0, null, null);
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._Name, "Name", false, 0, null, null);
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._DisRCode, "Disposal Route Code", false, -1, null, null);
            //CUS01
            string sDispUIP = "SRC*IDHCSRCH(TIP)[IDH_BPCOD;IDH_TYPE=F-C][CARDCODE;" + IDH_DISPRTCD._TipNm + "=CARDNAME]";
            if (Config.ParameterAsBool("WOBPFLT", true) == false) {
                sDispUIP = "SRC*IDHCSRCH(TIP)[IDH_BPCOD;][CARDCODE;" + IDH_DISPRTCD._TipNm + "=CARDNAME]";
            } else {
                sDispUIP = "SRC*IDHCSRCH(TIP)[IDH_BPCOD;IDH_TYPE=S;IDH_GROUP=" + Config.Parameter("BGSDispo") + "][CARDCODE;" + IDH_DISPRTCD._TipNm + "=CARDNAME]";
            }
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._TipCd, "Disposal Code", true, -1, sDispUIP, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);

            sDispUIP = "SRC*IDHCSRCH(TIP)[IDH_NAME;IDH_TYPE=F-C][CARDNAME;" + IDH_DISPRTCD._TipCd + "=CARDCODE]";
            if (Config.ParameterAsBool("WOBPFLT", true) == false) {
                sDispUIP = "SRC*IDHCSRCH(TIP)[IDH_NAME;][CARDNAME;" + IDH_DISPRTCD._TipCd + "=CARDCODE]";
            } else {
                sDispUIP = "SRC*IDHCSRCH(TIP)[IDH_NAME;IDH_TYPE=S;IDH_GROUP=" + Config.Parameter("BGSDispo") + "][CARDNAME;" + IDH_DISPRTCD._TipCd + "=CARDCODE]";
            }
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._TipNm, "Disposal Name", true, -1, sDispUIP, null);
            //***********final disposal site
            sDispUIP = "SRC*IDHCSRCH(FNTIP)[IDH_BPCOD;IDH_TYPE=F-C][CARDCODE;" + IDH_DISPRTCD._InTipNm + "=CARDNAME]";
            if (Config.ParameterAsBool("WOBPFLT", true) == false) {
                sDispUIP = "SRC*IDHCSRCH(FNTIP)[IDH_BPCOD;][CARDCODE;" + IDH_DISPRTCD._InTipNm + "=CARDNAME]";
            } else {
                sDispUIP = "SRC*IDHCSRCH(FNTIP)[IDH_BPCOD;IDH_TYPE=S;IDH_GROUP=" + Config.Parameter("BGSDispo") + "][CARDCODE;" + IDH_DISPRTCD._InTipNm + "=CARDNAME]";
            }
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._InTipCd, "In-Bound Disposal Site Cd", true, -1, sDispUIP, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);

            sDispUIP = "SRC*IDHCSRCH(FNTIPN)[IDH_NAME;IDH_TYPE=F-C][CARDNAME;" + IDH_DISPRTCD._InTipCd + "=CARDCODE]";
            if (Config.ParameterAsBool("WOBPFLT", true) == false) {
                sDispUIP = "SRC*IDHCSRCH(FNTIPN)[IDH_NAME;][CARDNAME;" + IDH_DISPRTCD._InTipCd + "=CARDCODE]";
            } else {
                sDispUIP = "SRC*IDHCSRCH(FNTIPN)[IDH_NAME;IDH_TYPE=S;IDH_GROUP=" + Config.Parameter("BGSDispo") + "][CARDNAME;" + IDH_DISPRTCD._InTipCd + "=CARDCODE]";
            }
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._InTipNm, "In-Bound Disposal Site", true, -1, sDispUIP, null);

            //***********final disposal site
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._DisProcCd, "Process", true, -1, "COMBOBOX", null);
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._DisTrtCd, "Treatment", true, -1, "COMBOBOX", null);

            //State field? from Excel Check HN
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._State, "State", true, -1, null, null);

            moDispRoutGrid.doAddListField(IDH_DISPRTCD._ItemCd, "Container Code", true, -1, "SRC*IDHISRC(ITM01)[IDH_ITMCOD;SRCHCONT=Yes][ITEMCODE;" + IDH_DISPRTCD._ItemDsc + "=ITEMNAME]", null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._ItemDsc, "Container Desc.", true, -1, null, null);
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._RDCode, "R/D Code Receipt", true, -1, "COMBOBOX", null);
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._RDFrgnCd, "R/D Code Despatch", true, -1, "COMBOBOX", null);
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._TAddress, "Site Address", true, -1, "SRC*IDHASRCH(CADDR01)[IDH_CUSCOD=#" + IDH_DISPRTCD._TipCd + ";IDH_ADRTYP=%S][ADDRESS;" + IDH_DISPRTCD._TAddrssLN + "=ADDRSCD;" + IDH_DISPRTCD._TZpCd + "=ZIPCODE]", null);
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._TAddrssLN, "Address LineNum", false, -1, null, null);
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._TZpCd, "Site Postcode", false, -1, null, null);
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._WasCd, "Waste Code", true, -1, "SRC*IDHWISRC(WST01)[IDH_ITMCOD;IDH_INVENT=;IDH_GRPCOD=" + sWastGrp + "][ITEMCODE;" + IDH_DISPRTCD._WasDsc + "=ITEMNAME;" + IDH_DISPRTCD._UOM + "=UOM]", null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._WasDsc, "Waste Desc.", false, -1, null, null);
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._UOM, "UOM", true, -1, "COMBOBOX", null);
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._doWeight, "Do Weight", true, -1, "COMBOBOX", null);
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._TipCost, "Disposal Cost", true, -1, null, null);
            //moDispRoutGrid.doAddListField(IDH_DISPRTCD._Qty, "Quantity", true, -1, null, null);
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._HaulgCost, "Transport Cost", true, -1, null, null);
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._TotCost, "Total Cost", false, -1, null, null);

            moDispRoutGrid.doAddListField(IDH_DISPRTCD._AvgTngCmt, "Average Tonnage per load and comments", true, -1, null, null);
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._HaulgPCnt, "Transport/ container", true, -1, null, null);
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._CmpBioTrt, "Composting/Biological Treatment Y/N", true, -1, "COMBOBOX", null);//Y/N
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._TFSyn, "TFS Y/N", true, -1, "COMBOBOX", null);//Y/N
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._WhsCode, "Warehouse", true, -1, "COMBOBOX", null);//OWHS
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._COMPrSt, "COMAH pre-set", true, -1, null, null);//
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._HAZCD, "HP Code", true, -1, null, null);//
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._HazClass, "ADR class", true, -1, null, null);//
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._UNNANo, "UN", true, -1, null, null);//
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._PackGrp, "Pkg Gp", true, -1, null, null);//
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._DRTCmts, "Testing Criteria and Spec", true, -1, null, null);//
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._Descrp, "Description", true, -1, null, null);//
            moDispRoutGrid.doAddListField(IDH_DISPRTCD._Active, "Active", true, -1, "CHECKBOX", null);//
        }

        public override void doBeforeLoadData() {
            PaneLevel = 0;
            if (moDispRoutGrid == null) {
                moDispRoutGrid = (DBOGrid)DBOGrid.getInstance(SBOForm, "LINESGRID");
                if (moDispRoutGrid == null) {
                    moDispRoutGrid = new DBOGrid(IDHForm, SBOForm, "LINESGRID", moDBObject);
                }
            }
            if (Config.INSTANCE.doCheckUserAuthorization(Config.USERAUTH_BLOCK_CIP_SIP_DELETE) == false || Config.INSTANCE.doGetIsSupperUser()) {
                moDispRoutGrid.doSetDeleteActive(true);
            } else {
                moDispRoutGrid.doSetDeleteActive(false);
            }
            doTheGridLayout();
            doSetFilterFields();
            if (DisposalSiteCode.Length == 0) {
                DisposalSiteCode = "";
            }
            moDispRoutGrid.Handler_GRID_ROW_ADD_EMPTY = new IDHAddOns.idh.controls.IDHGrid.ev_GRID_EVENTS(doSetLastLine);
            moDispRoutGrid.Handler_GRID_DATA_KEY_EMPTY = new IDHAddOns.idh.controls.IDHGrid.ev_GRID_EVENTS(doCreateKey);
            moDispRoutGrid.Handler_GRID_ROW_DEL = new IDHAddOns.idh.controls.IDHGrid.ev_GRID_EVENTS(doHandleGridDelete);

            

            doSetDBHandlers();
           // doSetGridHandlers();


            doFillCombos(true);
        }


        private void doSetDBHandlers() {
            //BO - Business Object
            moDBObject.Handler_RowDataLoaded += Handle_GridRowsItemsLoaded;
        }
        //protected void doSetGridHandlers() {
        //    doStartEventFilterBatch(true);
        //    //moDispRoutGrid.Handler_GRID_SEARCH = doHandleGridSearch;
        //    //moDispRoutGrid.Handler_GRID_SEARCH_VALUESET = doHandleGridSearchValueSet;
        //    //moDispRoutGrid.Handler_GRID_DOUBLE_CLICK = doHandleGridDoubleClick;
        //    //moDispRoutGrid.Handler_GRID_RIGHT_CLICK = doHandleGridRightClick;
        //    //moDispRoutGrid.Handler_GRID_MENU_EVENT = doHandleGridMenuEvent;
        //    //moDispRoutGrid.Handler_GRID_SORT = doHandleGridSort;
        //    doCommitEventFilters();
        //}

        private void Handle_GridRowsItemsLoaded(idh.dbObjects.DBBase oCaller) {
            //FillCombos.FillState(Items.Item("IDH_STATE").Specific, moEnquiry.U_Country, null);
            if (moDBObject.Count == 0) {
                moDispRoutGrid.doAddEditLine(true);
            }
            moDispRoutGrid.doApplyRules();
            doFillCombos(true);

        }


        public override void doFinalizeShow() {
            base.doFinalizeShow();
        }

        #endregion

        #region "LoadCombo"
        protected void doFillCombos(bool bDoFormCombo) {
            doUOMCombo(bDoFormCombo);
            doProcessCombo(bDoFormCombo);
            doTreatmentCombo(bDoFormCombo);
            doRDCodeCombo(bDoFormCombo);
            doFillYesNoCombo(bDoFormCombo, IDH_DISPRTCD._CmpBioTrt, true, false);
            doFillYesNoCombo(bDoFormCombo, IDH_DISPRTCD._TFSyn, true, false);
            if (!bDoFormCombo)
                doFillYesNoCombo(false, IDH_DISPRTCD._doWeight, false, true);
            doWareHouseCombo(bDoFormCombo);

        }

        private void doUOMCombo(bool bDoFormCombo) {
            try {
                if (bDoFormCombo) {
                    //'FORM COMBO
                    doFillCombo("IDH_UOM", "OWGT", "UnitDisply", "UnitName", null, null, "Any", "");
                } else {
                    //'GRID COMBO
                    SAPbouiCOM.ComboBoxColumn oCombo;//= default(SAPbouiCOM.ComboBoxColumn);
                    oCombo = (SAPbouiCOM.ComboBoxColumn)moDispRoutGrid.Columns.Item(moDispRoutGrid.doIndexFieldWC(IDH_DISPRTCD._UOM));
                    oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                    doFillCombo(oCombo, "OWGT", "UnitDisply", "UnitName", null, null, null, "t");
                    //"Any", "")
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResError(ex.Message, "EREXFC", new string[] { com.idh.bridge.Translation.getTranslatedWord("UOM") });
            }
        }
        private void doProcessCombo(bool bDoFormCombo) {
            try {
                if (bDoFormCombo) {
                } else {
                    //'GRID COMBO
                    SAPbouiCOM.ComboBoxColumn oCombo;//= default(SAPbouiCOM.ComboBoxColumn);
                    oCombo = (SAPbouiCOM.ComboBoxColumn)moDispRoutGrid.Columns.Item(moDispRoutGrid.doIndexFieldWC(IDH_DISPRTCD._DisProcCd));
                    oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;

                    doFillCombo(oCombo, "[@IDH_DISPPRC]", IDH_DISPPRC._ProcessCd, IDH_DISPPRC._ProcessNm, null, null, null, null);
                    //"Any", "")
                }
            } catch (Exception ex) {
                //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error filling the UOM Combo.")
                com.idh.bridge.DataHandler.INSTANCE.doResError(ex.Message, "EREXFC", new string[] { com.idh.bridge.Translation.getTranslatedWord("Disposal Process") });
            }
        }
        private void doTreatmentCombo(bool bDoFormCombo) {
            try {
                if (bDoFormCombo) {
                    //'FORM COMBO
                } else {
                    //'GRID COMBO
                    SAPbouiCOM.ComboBoxColumn oCombo;//= default(SAPbouiCOM.ComboBoxColumn);
                    oCombo = (SAPbouiCOM.ComboBoxColumn)moDispRoutGrid.Columns.Item(moDispRoutGrid.doIndexFieldWC(IDH_DISPRTCD._DisTrtCd));
                    oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                    doFillCombo(oCombo, "[@IDH_DISPTRT]", IDH_DISPTRT._TreatmntCd, IDH_DISPTRT._TreatmntNm, null, null, null, null);
                    //"Any", "")
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResError(ex.Message, "EREXFC", new string[] { com.idh.bridge.Translation.getTranslatedWord("Disposal Treatment") });
            }
        }
        private void doRDCodeCombo(bool bDoFormCombo) {
            try {
                if (bDoFormCombo) {
                } else {
                    //'GRID COMBO
                    SAPbouiCOM.ComboBoxColumn oCombo;//= default(SAPbouiCOM.ComboBoxColumn);
                    oCombo = (SAPbouiCOM.ComboBoxColumn)moDispRoutGrid.Columns.Item(moDispRoutGrid.doIndexFieldWC(IDH_DISPRTCD._RDCode));

                    oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Value;
                    doFillCombo(oCombo, "[@IDH_WTDIRE]", "Code", "U_WTOD", null, null, null, null);
                    //
                    oCombo = (SAPbouiCOM.ComboBoxColumn)moDispRoutGrid.Columns.Item(moDispRoutGrid.doIndexFieldWC(IDH_DISPRTCD._RDFrgnCd));
                    oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Value;
                    doFillCombo(oCombo, "[@IDH_WTDIRE]", "Code", "U_WTOD", null, null, null, null);
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResError(ex.Message, "EREXFC", new string[] { com.idh.bridge.Translation.getTranslatedWord("R/D Code Receipt") });
            }
        }
        private void doFillYesNoCombo(bool bDoFormCombo, string sComboID, bool doAddBlankItem, bool doUseFullName) {
            try {
                if (bDoFormCombo) {
                    //'FORM COMBO
                } else {
                    //'GRID COMBO
                    SAPbouiCOM.ComboBoxColumn oCombo;//= default(SAPbouiCOM.ComboBoxColumn);
                    oCombo = (SAPbouiCOM.ComboBoxColumn)moDispRoutGrid.Columns.Item(moDispRoutGrid.doIndexFieldWC(sComboID));
                    oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                    while (oCombo.ValidValues.Count > 0)
                        oCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index);
                    if (doAddBlankItem)
                        oCombo.ValidValues.Add("", "");
                    if (doUseFullName) {
                        oCombo.ValidValues.Add("Yes", Translation.getTranslatedWord("Yes"));
                        oCombo.ValidValues.Add("No", Translation.getTranslatedWord("No"));
                    } else {
                        oCombo.ValidValues.Add("Y", Translation.getTranslatedWord("Yes"));
                        oCombo.ValidValues.Add("N", Translation.getTranslatedWord("No"));
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResError(ex.Message, "EREXFC", new string[] { com.idh.bridge.Translation.getTranslatedWord("FillYesNoCombo") });
            }
        }

        private void doWareHouseCombo(bool bDoFormCombo) {
            try {
                if (bDoFormCombo) {
                    //'FORM COMBO
                } else {
                    //'GRID COMBO

                    SAPbouiCOM.ComboBoxColumn oCombo;//= default(SAPbouiCOM.ComboBoxColumn);
                    oCombo = (SAPbouiCOM.ComboBoxColumn)moDispRoutGrid.Columns.Item(moDispRoutGrid.doIndexFieldWC(IDH_DISPRTCD._WhsCode));
                    oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;

                    doFillCombo(oCombo, "OWHS", "WhsCode", "WhsName", null, null, null, null);
                    //"Any", "")
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResError(ex.Message, "EREXFC", new string[] { com.idh.bridge.Translation.getTranslatedWord("Warehouse") });
            }
        }

        #endregion
        #region Methods

        private bool doValidate() {
            //try {
            //    for (int i = 0; i <= moDispRoutGrid.getRowCount() - 1; i++) {
            //        for (int j = i + 1; j <= moDispRoutGrid.getRowCount() - 1; j++) {
            //            if (moDispRoutGrid.doGetFieldValue(IDH_DISPRTCD._DisRCode, i).ToString() != "" && moDispRoutGrid.doGetFieldValue(IDH_DISPRTCD._ItemCd, i).ToString() != ""
            //                && moDispRoutGrid.doGetFieldValue(IDH_DISPRTCD._DisRCode, j).ToString() != "" && moDispRoutGrid.doGetFieldValue(IDH_DISPRTCD._ItemCd, j).ToString() != ""
            //                && moDispRoutGrid.doGetFieldValue(IDH_DISPRTCD._DisRCode, i).ToString() == moDispRoutGrid.doGetFieldValue(IDH_DISPRTCD._DisRCode, j).ToString()
            //                && moDispRoutGrid.doGetFieldValue(IDH_DISPRTCD._ItemCd, i).ToString() == moDispRoutGrid.doGetFieldValue(IDH_DISPRTCD._ItemCd, j).ToString()) {
            //                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Duplicate entries are not allowed.", "ERUSDBDU", null);
            //                moDispRoutGrid.setCurrentLineByClick(i);
            //                return false;
            //            }
            //        }
            //    }

            //} catch (Exception ex) {
            //    com.idh.bridge.DataHandler.INSTANCE.doResError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("Validate Form") });
            //    return false;
            //}
            return true;
        }
        #endregion
        private void doUpDateDefaults() {

            string sCustomerCd = getUFValue("IDH_DISPCD").ToString();
            string sCustomerNm = getUFValue("IDH_DISPNM").ToString();

            string sItemCode = getUFValue("IDH_ITMCD").ToString();
            string sItemDesc = getUFValue("IDH_ITMNM").ToString();
            string sWastCd = getUFValue("IDH_ITMNM").ToString();
            string sWastDsc = getUFValue("IDH_WASTNM").ToString();
            string sTipAddCd = getUFValue("IDH_ADDR").ToString();
            string sTipAddressLineNum = getUFValue("IDH_ADLN").ToString();
            string sTipZipCode = getUFValue("IDH_POSCOD").ToString();
            string sUOM = getUFValue("IDH_UOM").ToString();
            string sDispRouteCode = getUFValue("IDH_DISPRT").ToString();
            //string sDispRouteDesc = getUFValue("IDH_DRDSC").ToString();
            moDBObject.doAddDefaultValue(IDH_DISPRTCD._WasCd, sWastCd);
            moDBObject.doAddDefaultValue(IDH_DISPRTCD._WasDsc, sWastDsc);
            moDBObject.doAddDefaultValue(IDH_DISPRTCD._ItemCd, sItemCode);
            moDBObject.doAddDefaultValue(com.isb.dbObjects.User.IDH_SUITPR._ItemDs, sItemDesc);
            moDBObject.doAddDefaultValue(IDH_DISPRTCD._TAddress, sTipAddCd);
            moDBObject.doAddDefaultValue(IDH_DISPRTCD._TAddrssLN, sTipAddressLineNum);
            moDBObject.doAddDefaultValue(IDH_DISPRTCD._TZpCd, sTipZipCode);

            moDBObject.doAddDefaultValue(IDH_DISPRTCD._TipCd, sCustomerCd);
            moDBObject.doAddDefaultValue(IDH_DISPRTCD._TipNm, sCustomerNm);

            if (sUOM == null || sUOM.Length == 0) {
                sUOM = moDBObject.doGetDefaultValue(IDH_DISPRTCD._UOM).ToString();
                if (sUOM == null || sUOM.Length == 0) {
                    sUOM = com.idh.bridge.lookups.Config.INSTANCE.doGetItemSalesUOM(sCustomerCd, sWastCd);
                    moDBObject.doAddDefaultValue(IDH_DISPRTCD._UOM, sUOM);
                }
            } else {
                moDBObject.doAddDefaultValue(IDH_DISPRTCD._UOM, sUOM);
            }
        }

        private bool doCreateKey(ref IDHAddOns.idh.events.Base pVal) {
            if (pVal.BeforeAction == false) {
                NumbersPair oNumbers = moDBObject.getNewKey();
                moDBObject.Code = oNumbers.CodeCode;
                moDBObject.Name = oNumbers.NameCode;
            }
            return true;
        }
        private bool doSetLastLine(ref IDHAddOns.idh.events.Base pVal) {
            if (pVal.BeforeAction == false) {
                int iLastRow = moDBObject.Count;
                moDBObject.doApplyDefaults(iLastRow - 1);
            }
            return true;
        }

        private bool doHandleGridDelete(ref IDHAddOns.idh.events.Base pVal) {
            if (pVal.BeforeAction == false) {
                moDBObject.doAddAuditTrail("REMOVE", string.Concat(IDH_DISPRTCD._Code, ": ", moDBObject.Code), "DBO-[N C]");
            }
            return true;
        }

        public void doLoadNow(int iMode) {
            doLoadData();
        }

        //** The Initializer
        public override void doLoadData() {
            doUpDateDefaults();

            doLoadGridData();
            doFillCombos(false);
        }


        public void doLoadGridData() {
            SBOForm.Freeze(true);

            try {
                moDispRoutGrid.doAutoListFields(true);


                if (getUFValue("IDH_ACTIVE").ToString().Equals("Y")) {
                    moDispRoutGrid.setRequiredFilter("U_Active='Y'");
                } else {
                    moDispRoutGrid.setRequiredFilter("");
                }
                moDispRoutGrid.setOrderValue(IDH_DISPRTCD._DisRCode + ", CAST(" + IDH_DISPRTCD._Code + " As Numeric) ASC");
                moDispRoutGrid.setDoFilter(true);

                moDispRoutGrid.doReloadData("", false, true);

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResError(ex.Message, "EREXDBL", null);
                //  com.idh.bridge.DataHandler.INSTANCE.doResError(ex.Message, "EREXDBL", null);
            }
            SBOForm.Freeze(false);
        }

        public override bool doCustomItemEvent(ref IDHAddOns.idh.events.Base pVal) {
            //if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_SELECT) {
            //    string s = moDBObject.U_TipCd;
            //    s = moDBObject.U_TipCd;
            //}
            if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SORT && pVal.BeforeAction == false) {
                doFillCombos(false);
            }
            bool bret = base.doCustomItemEvent(ref pVal);
            return bret;
        }
    }
}
