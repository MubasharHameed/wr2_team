﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//using System.Collections;
//using System.Data;
//using System.Diagnostics;
using System.IO;
//using IDHAddOns; 
using IDHAddOns.idh.controls;
using com.idh.bridge.data;
using com.idh.bridge;
using com.idh.bridge.lookups; 
//using com.idh.dbObjects; 
//using IDHAddOns.idh.forms;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry
{
    public class LabResults : IDHAddOns.idh.forms.Base
    {

        protected string msKeyGen = "GENSEQ";

        string sLTCd = string.Empty;
        string sImportCd = string.Empty; 


        #region Initialization
        //base(oParent, "IDHLABTANL", 0, "Enq_LabTaskAnalysis.srf", false, true, false, "Lab Task Analysis")
        public LabResults(IDHAddOns.idh.addon.Base oParent)
            : base(oParent, "IDHLABRESULTS", 0, "Enq_LabResults.srf", false, true, false, "Lab Results")
        {
        }

        protected override void doSetEventFilters()
        {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE);

        }

        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent)
        {
            try
            {
                oForm.Title = gsTitle;
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;

                //Report Grid 
                SAPbouiCOM.Item oItem;
                oItem = oForm.Items.Item("IDH_RECRPT");
                UpdateGrid oRPTGrid = new UpdateGrid(this, oForm, "RPTGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane);
                oRPTGrid.getSBOItem().AffectsFormMode = true;

                //Result Grid 
                oItem = oForm.Items.Item("IDH_RECRSL");
                UpdateGrid oResultGridN = new UpdateGrid(this, oForm, "RSLGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane);
                oResultGridN.getSBOItem().AffectsFormMode = true;

                doAddUF(oForm, "LABTSKID", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, false, false);
                doAddUF(oForm, "IMPRSLID", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20, false, false); 
                
            }
            catch (Exception ex)
            {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
                BubbleEvent = false;
            }
        }

        #endregion

        #region LoadingDate

        public override void doBeforeLoadData(SAPbouiCOM.Form oForm)
        {
            //doFillCombos(oForm);

            string sLabTaskId = getParentSharedData(oForm, "LABTSKID").ToString();
            //string sImportResultId = getSharedData(oForm, "IMPRSLID").ToString();
            setUFValue(oForm, "LABTSKID", sLabTaskId);
            //setUFValue(oForm, "IMPRSLID", sImportResultId); 

            //Can use form fields to display Lab Task Cd and Lab Result Cd 
            SAPbouiCOM.Item oItm = oForm.Items.Item("IDH_LTCD");
            SAPbouiCOM.EditText oTxt = (SAPbouiCOM.EditText)oItm.Specific;
            oTxt.Value = sLabTaskId;

            //oItm = oForm.Items.Item("IDH_LRCD");
            //oTxt = (SAPbouiCOM.EditText)oItm.Specific;
            //oTxt.Value = sImportResultId; 


            doManageReportGrid(oForm);
            doManageResultGrid(oForm);
            //doManageNewItemGrid(oForm);

            doSetFilterFields(oForm);
        }

        protected override void doLoadData(SAPbouiCOM.Form oForm)
        {
            string sLabTaskId = getUFValue(oForm, "LABTSKID", false).ToString();
            string sImpResultId = getUFValue(oForm, "IMPRSLID", false).ToString(); 

            FilterGrid oRptGridN = (FilterGrid)FilterGrid.getInstance(oForm, "RPTGRID");
            oRptGridN.setRequiredFilter(" U_LabTskCd = '" + sLabTaskId + "' "); 
            oRptGridN.doReloadData("", false, false);
            oRptGridN.doApplyRules();

            FilterGrid oResultGridN = (FilterGrid)FilterGrid.getInstance(oForm, "RSLGRID");
            oResultGridN.setRequiredFilter(" U_LTICd = '"+ sImpResultId +"' "); 
            oResultGridN.doReloadData("", false, false);
            oResultGridN.doApplyRules();

            //FilterGrid oNIGridN = (FilterGrid)FilterGrid.getInstance(oForm, "NITGRID");
            //oNIGridN.setRequiredFilter("NI.ItemCode = '-987'");
            //oNIGridN.doReloadData("", false, false);
            //oNIGridN.doApplyRules();
        }

        public override void doFinalizeShow(SAPbouiCOM.Form oForm)
        {
            base.doFinalizeShow(oForm);
        }

        protected void doSetFilterFields(SAPbouiCOM.Form oForm)
        {
            //UpdateGrid oItemGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "RSLGRID");
            //if (oItemGridN == null)
            //    oItemGridN = new UpdateGrid(this, oForm, "RSLGRID");

            ////oItemGridN.doAddFilterField("IDH_Date", "LI.Date", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 11, null);
            ////oItemGridN.doAddFilterField("IDH_DTFROM", "LI.FromDate", SAPbouiCOM.BoDataType.dt_DATE, "=", 11, null);
            ////oItemGridN.doAddFilterField("IDH_DTTO", "LI.ToDate", SAPbouiCOM.BoDataType.dt_DATE, "=", 11, null);
            ////oItemGridN.doAddFilterField("IDH_ITMFRM", "i.ItemCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, ">=", 30, "");
            ////oItemGridN.doAddFilterField("IDH_ITMTO", "i.ItemName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "<=", 30, "");
            //oItemGridN.doAddFilterField("IDH_ITMGRP", "i.ItmsGrpCod", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "");
            
            ////oItemGridN.doAddFilterField("IDH_BinLoc", "LI.U_NewBinLoc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "");
        }

        protected void doSetReportGridListFields(IDHAddOns.idh.controls.UpdateGrid oRPTGRIDN)
        {
            //LTI.U_LabTskCd
            oRPTGRIDN.doAddListField("LTI.Code", "LTI Code", false, -1, null, null);
            oRPTGRIDN.doAddListField("LTI.U_LabTskCd", "Lab Task Code", false, 0, null, null);
            oRPTGRIDN.doAddListField("LTI.U_ImportedOn", "Imported On", false, -1, null, null);
            oRPTGRIDN.doAddListField("LTI.U_ImportedBy", "Imported By", false, -1, null, null);
            oRPTGRIDN.doAddListField("LTI.U_Description", "Description", false, 0, null, null);
        }

        protected void doSetResultsGridListFields(IDHAddOns.idh.controls.UpdateGrid oItemGridN)
        {
            oItemGridN.doAddListField("LTID.Code", "Code", false, 0, null, null);
            
            oItemGridN.doAddListField("LTID.U_LTICd", "Lab Task Import Cd", false, 0, null, null);
            oItemGridN.doAddListField("LTID.U_RepCol1", "RepCol1", false, -1, null, null);
            oItemGridN.doAddListField("LTID.U_RepCol2", "RepCol2", false, -1, null, null);
            oItemGridN.doAddListField("LTID.U_RepCol3", "RepCol3", false, -1, null, null);
            oItemGridN.doAddListField("LTID.U_RepCol4", "RepCol4", false, -1, null, null);
            oItemGridN.doAddListField("LTID.U_RepCol5", "RepCol5", false, -1, null, null);
        }

        //protected void doSetNewItemsGridListFields(IDHAddOns.idh.controls.UpdateGrid oNewItemGridN)
        //{
        //    oNewItemGridN.doAddListField("NI.ItemCode", "New Item Code", false, -1, null, null);
        //    oNewItemGridN.doAddListField("NI.ItemName", "New Item Name", false, -1, null, null);
        //    oNewItemGridN.doAddListField("NI.FrgnName", "Parent Whse Code", false, -1, null, null);
        //    oNewItemGridN.doAddListField("NI.OnHand", "Available Qty", false, -1, null, null);
        //    oNewItemGridN.doAddListField("NI.IsCommited", "Qty Using", true, -1, null, null);
        //    oNewItemGridN.doAddListField("NI.SalUnitMsr", "Parent Item UOM", false, -1, null, null);
        //}
        #endregion

        #region UnloadingData

        public override void doClose()
        {
        }

        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent)
        {
            base.doCloseForm(oForm, ref BubbleEvent);
            FilterGrid.doRemoveGrid(oForm, "RSLGRID");
        }

        #endregion
        
        #region EventHandlers
        
        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            if (pVal.BeforeAction)
            {
                if (oForm.Mode == SAPbouiCOM.BoFormMode.fm_OK_MODE)
                {
                    SAPbouiCOM.Form oParentForm = goParent.doGetParentForm(oForm);
                    setSharedData(oParentForm, "LABTSKID", getUFValue(oForm, "LABTSKID")); 
                    doReturnFromModalShared(oForm, true);
                }
            }
            //UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "RSLGRID");
            ////If oGridN.hasFieldChanged("U_Select") Then
            //Int32 _WOQID = getParentSharedDataAsInteger(oForm, "WOQID");
            //Int32 _EnqID = getParentSharedDataAsInteger(oForm, "ENQID");
            //com.idh.dbObjects.User.IDH_ENQUS moIDH_ENQUS = new com.idh.dbObjects.User.IDH_ENQUS();
            //for (Int16 iRow = 0; iRow <= oGridN.getRowCount() - 1; iRow++)
            //{

            //    if (!string.IsNullOrEmpty(oGridN.doGetFieldValue("Code", iRow).ToString()) && moIDH_ENQUS.getByKey(oGridN.doGetFieldValue("Code", iRow).ToString()))
            //    {
            //        moIDH_ENQUS.U_Answer = oGridN.doGetFieldValue("U_Answer", iRow).ToString();
            //    }
            //    else
            //    {
            //        moIDH_ENQUS.doAddEmptyRow(true, true, false);
            //        moIDH_ENQUS.U_Answer = oGridN.doGetFieldValue("U_Answer", iRow).ToString();
            //        if (_EnqID != 0)
            //            moIDH_ENQUS.U_EnqID = Convert.ToInt32(oGridN.doGetFieldValue("U_EnqID", iRow).ToString());
            //        moIDH_ENQUS.U_QsID = oGridN.doGetFieldValue("QID", iRow).ToString();
            //        moIDH_ENQUS.U_Question = oGridN.doGetFieldValue("Question", iRow).ToString();
            //        if (_WOQID != 0)
            //            moIDH_ENQUS.U_WOQID = Convert.ToInt32(oGridN.doGetFieldValue("U_WOQID", iRow).ToString());

            //    }
            //    moIDH_ENQUS.doProcessData();
            //    //End If
            //}
            //doLoadData(oForm);

        }

        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            base.doItemEvent(oForm, ref pVal, ref BubbleEvent);
            if (pVal.BeforeAction == false)
            {
                if (pVal.ItemUID == "IDH_IMPORT") 
                {
                    //start import process
                    try
                    {
                        sLTCd = getUFValue(oForm, "LABTSKID", false).ToString();
                        sImportCd = getUFValue(oForm, "IMPRSLID", false).ToString();

                        System.Threading.Thread newThread = new System.Threading.Thread(doImportLabResults);
                        newThread.SetApartmentState(System.Threading.ApartmentState.STA);
                        newThread.Start();
                        newThread.Join();

                        doLoadData(oForm); 
                    }
                    catch (Exception ex) 
                    {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Error occured while system trying to load selected file." + ex.Message);
                    }
                }
                else if (pVal.ItemUID == "IDH_BATCHI")
                {
                    
                }
                else if (pVal.ItemUID == "IDH_WOHDR")
                {
                    
                }
                else if (pVal.ItemUID == "IDH_RESET")
                {
                    //Reset all filters and grids 
                    //TODO 
                }
            }
            return true;
        }

        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal)
        {
            if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_SELECT && pVal.ItemUID == "RPTGRID") 
            {
                if (pVal.BeforeAction)
                {
                    IDHGrid oGridN = IDHGrid.getInstance(oForm, "RPTGRID");
                    string sLTImportCd = oGridN.doGetFieldValue("LTI.Code", pVal.Row).ToString();
                    setUFValue(oForm, "IMPRSLID", sLTImportCd, false);

                    doLoadData(oForm);
                }
            }
            else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_DESELECT && pVal.ItemUID == "IMPRSLID")
            {
                setUFValue(oForm, "IMPRSLID", "", false);
            }
            return base.doCustomItemEvent(oForm, ref pVal);

        }

        #endregion

        #region CustomFunctions

        protected void doManageReportGrid(SAPbouiCOM.Form oForm)
        {
            UpdateGrid oRPTGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "RPTGRID");
            if (oRPTGridN == null)
                oRPTGridN = new UpdateGrid(this, oForm, "RPTGRID");

            oRPTGridN.doAddGridTable(new GridTable("@IDH_LABTSKIMP", "LTI", "U_LabTskCd", true), true);
            oRPTGridN.setOrderValue("LTI.U_LabTskCd");
            oRPTGridN.doSetDoCount(true);
            doSetReportGridListFields(oRPTGridN);
            oRPTGridN.doApplyRules();
        }

        protected void doManageResultGrid(SAPbouiCOM.Form oForm)
        {
            UpdateGrid oResultGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "RSLGRID");
            if (oResultGridN == null)
                oResultGridN = new UpdateGrid(this, oForm, "RSLGRID");

            oResultGridN.doAddGridTable(new GridTable("@IDH_LTIDETAIL", "LTID", "U_LTICd", true), true);
            oResultGridN.setOrderValue("LTID.U_LTICd");
            oResultGridN.doSetDoCount(true);
            doSetResultsGridListFields(oResultGridN);
            oResultGridN.doApplyRules();
        }

        public void doImportTest()
        {
            
        }

        public void doImportLabResults()
        {
            string sFileName;
            System.Windows.Forms.OpenFileDialog oOpenFile = new System.Windows.Forms.OpenFileDialog();

            try
            {
                string sExt = Config.INSTANCE.getImportExtension();
                if (sExt != null && sExt.Length > 0)
                {
                    if (sExt.IndexOf("|") == -1)
                    {
                        sExt = "(" + sExt + ")|" + sExt;
                    }
                    oOpenFile.Filter = sExt;
                }

            }
            catch (Exception ex)
            {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", new string[] { string.Empty });
            }
            oOpenFile.FileName = "OpenExcelFile";
            oOpenFile.InitialDirectory = Config.INSTANCE.getImportFolder();
            if (com.idh.win.AppForm.OpenFileDialog(oOpenFile) == System.Windows.Forms.DialogResult.OK)
            {
                sFileName = oOpenFile.FileName;
                try
                {
                    //oForm.Freeze(true);
                    //START TRANSACTION 
                    goParent.goDICompany.StartTransaction();
                    
                    IDH_LABTSKIMP oImports;
                    string sNewLabResultCode = string.Empty; 
                    //Add/Save Lab Result 
                    if (sLTCd.Length > 0)
                    {
                        oImports = new IDH_LABTSKIMP();
                        oImports.U_LabTskCd = sLTCd;
                        oImports.U_ImportedOn = DateTime.Now;
                        oImports.U_ImportedBy = goParent.gsUserName;
                        oImports.U_Description = "";
                        oImports.doAddDataRow();
                        sNewLabResultCode = oImports.Code; 
                        //DataHandler.INSTANCE.getLastUsedNumber(); 


                    }
                    com.idh.bridge.DataHandler.INSTANCE.doInfo("Importing the file.");

                    //com.idh.bridge.import.GeneralData oImporter = new com.idh.bridge.import.GeneralData();
                    DataImport oImporter = new DataImport();
                    if (oImporter.doReadFile(sFileName, sNewLabResultCode))
                    {
                        com.idh.bridge.DataHandler.INSTANCE.doInfo("Import done.");
                        if (goParent.goDICompany.InTransaction)
                            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    }
                    else
                    {
                        if (goParent.goDICompany.InTransaction)
                        {
                            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors();
                        }
                    }

                    //END TRANSACTION 
                }
                catch (Exception ex)
                {
                    if (goParent.goDICompany.InTransaction)
                    {
                        goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                        com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors();
                    }
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", new string[] { string.Empty });
                }
                finally
                {
                    string sCurrDirectory = Directory.GetCurrentDirectory();
                    Directory.SetCurrentDirectory(IDHAddOns.idh.addon.Base.CURRENTWORKDIR);
                    //oForm.Freeze(false);
                }
            }
            //return false;
        }

        #endregion






    }
}