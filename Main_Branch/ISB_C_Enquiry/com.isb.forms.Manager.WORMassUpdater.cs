﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
//using System.Diagnostics;
using System.IO;

using IDHAddOns.idh.controls;
//using com.idh.bridge.reports;
//using com.idh.utils.Conversions;
using com.idh.bridge;
//using com.idh.bridge.data;
//using WR1_Grids.idh.controls.grid;
using com.idh.bridge.lookups;
//using com.idh.bridge.action;
using com.idh.controls;
using com.idh.controls.strct;
//using System.Windows.Forms;
//using System.Threading;
//using com.idh.utils;
//using System.Text;
using com.idh.bridge.resources;
//using WR1_PBI;
using SAPbouiCOM;
using System.Linq;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry.PJDORA {
    class WORMassUpdater :
        IDHAddOns.idh.forms.Base {

        public WORMassUpdater(IDHAddOns.idh.addon.Base oParent, string sParent, int iMenuPosition)
            : base(oParent, "IDH_WOMSUPT", sParent, iMenuPosition, "WOR_MassUpdater.srf", true, true, false, "Mass WOR Updater", load_Types.idh_LOAD_NORMAL) {
        }

        protected void doTheGridLayout(DBOGrid oGridN) {
            if (com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", false)) {
                string sFormTypeId = oGridN.getSBOForm().TypeEx;
                idh.dbObjects.User.IDH_FORMSET oFormSettings = (idh.dbObjects.User.IDH_FORMSET)getWFValue("FRMSET", sFormTypeId + "." + oGridN.GridId);
                if (oFormSettings == null) {
                    oFormSettings = new idh.dbObjects.User.IDH_FORMSET();
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, "");
                    setWFValue("FRMSET", sFormTypeId + "." + oGridN.GridId, oFormSettings);
                    oFormSettings.doAddFieldToGrid(oGridN);
                    doSetListFields(oGridN);
                    oFormSettings.doSyncDB(oGridN);
                } else {
                    if (oFormSettings.SkipFormSettings) {
                        doSetListFields(oGridN);
                    } else {
                        oFormSettings.doAddFieldToGrid(oGridN);
                    }
                }
            } else {
                doSetListFields(oGridN);
            }
        }

        protected void doSetListFields(DBOGrid moGrid) {

            moGrid.doAddListField(IDH_WORUHD._Code, "Code", false, -1, null, null);
            moGrid.doAddListField(IDH_WORUHD._BatchNum, "Batch", false, -1, null, null);
            //moGrid.doAddListField(IDH_WORUHD._SrNo, "Sr No.", true, -1, null, null);

            moGrid.doAddListField(IDH_WORUHD._WOR, "WOR#", false, -1, null, null);
            moGrid.doAddListField(IDH_WORUHD._CustCd, "Customer Code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            moGrid.doAddListField(IDH_WORUHD._CustNm, "Customer Name", false, -1, null, null);

            moGrid.doAddListField(IDH_WORUHD._RDate, "Request Date", true, -1, null, null);
            moGrid.doAddListField(IDH_WORUHD._SDate, "Start Date", true, -1, null, null);
            moGrid.doAddListField(IDH_WORUHD._EDate, "End Date", true, -1, null, null);
            moGrid.doAddListField(IDH_WORUHD._Cancelled, "Cancelled", true, -1, null, null);
            moGrid.doAddListField(IDH_WORUHD._VehTyp, "Reason", true, -1, null, null);
            moGrid.doAddListField(IDH_WORUHD._CustRef, "Customer Reference", true, -1, null, null);
            moGrid.doAddListField(IDH_WORUHD._MaximoNum, "Maximo number", true, -1, null, null);
            moGrid.doAddListField(IDH_WORUHD._ExptQty, "Cust Reporting Weight", true, -1, ListFields.LISTTYPE_IGNORE, null);
            moGrid.doAddListField(IDH_WORUHD._UOM, "UOM", true, -1, ListFields.LISTTYPE_IGNORE, null);
            moGrid.doAddListField(IDH_WORUHD._HulgQty, "Haulage Qty", true, -1, ListFields.LISTTYPE_IGNORE, null);
            moGrid.doAddListField(IDH_WORUHD._DispQty, "Disposal Qty", true, -1, ListFields.LISTTYPE_IGNORE, null);
            moGrid.doAddListField(IDH_WORUHD._RebWgt, "Rebate Weight", true, -1, ListFields.LISTTYPE_IGNORE, null);

            moGrid.doAddListField(IDH_WORUHD._CustAdr, "Customer Address", false, -1, null, null);

            moGrid.doAddListField(IDH_WORUHD._CarrCd, "Suplier Code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            moGrid.doAddListField(IDH_WORUHD._CarrNm, "Suplier Name", false, -1, null, null);

            moGrid.doAddListField(IDH_WORUHD._ItemCd, "Container Code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            moGrid.doAddListField(IDH_WORUHD._ItemNm, "Container Type", false, -1, null, null);

            moGrid.doAddListField(IDH_WORUHD._WastCd, "Waste Code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            moGrid.doAddListField(IDH_WORUHD._WasteNm, "Waste Type", false, -1, null, null);
            moGrid.doAddListField(IDH_WORUHD._JbTypeDs, "Job Type", true, -1, null, null);


            moGrid.doAddListField(IDH_WORUHD._ProcSts, "Process Status", false, -1, ListFields.LISTTYPE_COMBOBOX, null);
            moGrid.doAddListField(IDH_WORUHD._VldStatus, "Validation Status", false, -1, ListFields.LISTTYPE_COMBOBOX, null);
            moGrid.doAddListField(IDH_WORUHD._BthStatus, "Batch Status", false, -1, ListFields.LISTTYPE_COMBOBOX, null);

            moGrid.doAddListField(IDH_WORUHD._AddedBy, "Added By", true, 0, null, null);
            moGrid.doAddListField(IDH_WORUHD._VdReason, "Validation Result", false, -1, null, null);
        }

        protected void doSetGridFilters(DBOGrid moGridN) {
            moGridN.doAddFilterField("IDH_CUST", IDH_WORUHD._CustCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_NAME", IDH_WORUHD._CustNm, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_CADDR", IDH_WORUHD._CustAdr, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_SUPCD", IDH_WORUHD._CarrCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_SUPNM", IDH_WORUHD._CarrNm, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_ITEMCD", IDH_WORUHD._ItemCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_ITEMNM", IDH_WORUHD._ItemNm, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WASTCD", IDH_WORUHD._WastCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WASTNM", IDH_WORUHD._WasteNm, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);


            //doAddUF(moGridN.getSBOForm(), "IDH_ACTNAP", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 250).AffectsFormMode = false;

            //moGridN.doAddFilterField("WORCode", IDH_WORUHD._WORow, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            //  moGridN.doAddFilterField("IDH_AMDTYP", IDH_WORUHD._AmndTp, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WOR", IDH_WORUHD._WOR, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_SDATE", IDH_WORUHD._SDate, SAPbouiCOM.BoDataType.dt_DATE, ">=", 10);
            moGridN.doAddFilterField("IDH_EDATE", IDH_WORUHD._EDate, SAPbouiCOM.BoDataType.dt_DATE, "<=", 10);
            moGridN.doAddFilterField("IDH_CUSREF", IDH_WORUHD._CustRef, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_UOM", IDH_WORUHD._UOM, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_JOBTP", IDH_WORUHD._JbTypeDs, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_BATCHF", IDH_WORUHD._BatchNum, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 250);
            //Process Status
            moGridN.doAddFilterField("IDH_BTPRST", IDH_WORUHD._ProcSts, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 2);
            //IDH_VLDSTS Validation Status
            moGridN.doAddFilterField("IDH_VLDSTS", IDH_WORUHD._VldStatus, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 2);
            //IDH_BTCSTS Batch Status Active/Deleted
            moGridN.doAddFilterField("IDH_BTCSTS", IDH_WORUHD._BthStatus, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 1);

        }
        //*** Add event filters to avoid receiving all events from SBO
        protected override void doSetEventFilters() {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE);
        }

        protected override void doReadInputParams(SAPbouiCOM.Form oForm) {

        }

        //** Create the form
        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            try {
                oForm.Title = gsTitle;
                int iWidth = oForm.Width;

                oForm.AutoManaged = false;

                oForm.EnableMenu(Config.NAV_ADD, false);
                oForm.EnableMenu(Config.NAV_FIND, false);
                oForm.EnableMenu(Config.NAV_FIRST, false);
                oForm.EnableMenu(Config.NAV_NEXT, false);
                oForm.EnableMenu(Config.NAV_PREV, false);
                oForm.EnableMenu(Config.NAV_LAST, false);

                oForm.SupportedModes = Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_All);
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);
                base.doCompleteCreate(ref oForm, ref BubbleEvent);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDLF", null);
                BubbleEvent = false;
            }
        }

        protected void doSetMode(SAPbouiCOM.Form oForm, SAPbouiCOM.BoFormMode iMode) {
            try {
                object sFormType = getParentSharedData(oForm, "FTYPE");
                bool bIsSearch = false;
                if ((sFormType != null) && sFormType.Equals("SEARCH") == true) {
                    bIsSearch = true;
                }

                if (iMode == SAPbouiCOM.BoFormMode.fm_FIND_MODE) {
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = Translation.getTranslatedWord("Find");
                    if (bIsSearch) {
                        oForm.Items.Item("2").Visible = true;
                    } else {
                        oForm.Items.Item("2").Visible = false;
                    }
                } else if (iMode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = Translation.getTranslatedWord("Update");
                    oForm.Items.Item("2").Visible = true;
                }
            } catch (Exception ex) {
            }
        }

        public string getListRequiredStr(SAPbouiCOM.Form OForm) {
            return " 1=1 ";
        }

        private void doFillGridCombos(UpdateGrid moGrid) {
            SAPbouiCOM.ComboBoxColumn oCombo;
            int iIndex;

            iIndex = moGrid.doIndexFieldWC(IDH_WORUHD._ProcSts);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            oCombo.ValidValues.Add("0", Translation.getTranslatedWord("Not Processed"));
            oCombo.ValidValues.Add("1", Translation.getTranslatedWord("Sent For Review"));
            oCombo.ValidValues.Add("11", Translation.getTranslatedWord("Ready For Review"));
            oCombo.ValidValues.Add("2", Translation.getTranslatedWord("Ready for Process"));
            oCombo.ValidValues.Add("3", Translation.getTranslatedWord("Processed"));


            iIndex = moGrid.doIndexFieldWC(IDH_WORUHD._VldStatus);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            oCombo.ValidValues.Add("", "");
            oCombo.ValidValues.Add("0", Translation.getTranslatedWord("Failed"));
            oCombo.ValidValues.Add("1", Translation.getTranslatedWord("Passed"));


            iIndex = moGrid.doIndexFieldWC(IDH_WORUHD._BthStatus);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            oCombo.ValidValues.Add("", Translation.getTranslatedWord("All"));
            oCombo.ValidValues.Add("0", Translation.getTranslatedWord("Deleted"));
            oCombo.ValidValues.Add("1", Translation.getTranslatedWord("Active"));


            //iIndex = moGrid.doIndexFieldWC(IDH_WORUHD._UOM);
            //oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            //// oCombo = moPricesGrid.Columns.Item(moPricesGrid.doIndexFieldWC("U_UOM"))
            //oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            //        doFillCombo(moGrid.getSBOForm(), oCombo, "OWGT", "UnitDisply", "UnitName", null, null, null, "t");// '"Any", "");
        }

        private void doFillCombos(SAPbouiCOM.Form oForm) {
            try {
                SAPbouiCOM.ComboBox oCombo;

                //oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_JOBTYP").Specific;
                //oCombo.Item.DisplayDesc = true;
                //doClearValidValues(oCombo.ValidValues);
                //string sItemGrp = "";
                //com.uBC.utils.FillCombos.doFillJobTypeCombo(ref oCombo, sItemGrp);

                //IDH_BTCSTS Batch Status
                oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_BTCSTS").Specific;
                doClearValidValues(oCombo.ValidValues);
                oCombo.Item.DisplayDesc = true;
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
                oCombo.ValidValues.Add("1", Translation.getTranslatedWord("Active"));
                oCombo.ValidValues.Add("0", Translation.getTranslatedWord("Deleted"));
                oCombo.ValidValues.Add("", Translation.getTranslatedWord("All"));
                //oCombo.ValidValues.Add("3", Translation.getTranslatedWord("Processed"));
                oCombo.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index);

                //IDH_BTPRST Process Status
                oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_BTPRST").Specific;
                doClearValidValues(oCombo.ValidValues);

                oCombo.Item.DisplayDesc = true;
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
                oCombo.ValidValues.Add("", Translation.getTranslatedWord("All"));
                oCombo.ValidValues.Add("0", Translation.getTranslatedWord("Not Processed"));
                oCombo.ValidValues.Add("1", Translation.getTranslatedWord("Sent For Review"));
                oCombo.ValidValues.Add("11", Translation.getTranslatedWord("Ready for Validation"));
                oCombo.ValidValues.Add("2", Translation.getTranslatedWord("Ready for Process"));
                oCombo.ValidValues.Add("3", Translation.getTranslatedWord("Processed"));


                //IDH_VLDSTS Validation Status
                oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_VLDSTS").Specific;
                doClearValidValues(oCombo.ValidValues);

                oCombo.Item.DisplayDesc = true;
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
                oCombo.ValidValues.Add("", Translation.getTranslatedWord("All"));
                oCombo.ValidValues.Add("1", Translation.getTranslatedWord("Passed"));
                oCombo.ValidValues.Add("0", Translation.getTranslatedWord("Failed"));


                doFillCombo(oForm, "IDH_UOM", "OWGT", "UnitDisply", "UnitName", null, null, "Any", "");

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }
        }

        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {


            DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");
            if (oGridN == null) {
                oGridN = (DBOGrid)DBOGrid.getInstance(oForm, "LINESGRID");
                if (oGridN == null) {
                    IDH_WORUHD oLiveSrvcBatch = new IDH_WORUHD(this, oForm);
                    oGridN = new DBOGrid(this, oForm, "LINESGRID", 7, 120, oForm.Width - 20, 255, 0, 0, oLiveSrvcBatch);

                }
                setWFValue(oForm, "LINESGRID", oGridN);
            }
            oGridN.doSetDeleteActive(true);
            doSetGridFilters(oGridN);
            oGridN.doSetDoCount(true);
            oGridN.doSetBlankLine(false);

            //Set the required List Fields
            doTheGridLayout(oGridN);
            doFillCombos(oForm);

            oGridN.setInitialFilterValue(IDH_WORUHD._BatchNum + " =\'-999\' ");
            if (DataHandler.INSTANCE.User != "manager") {//&& DataHandler.INSTANCE.User!="manager"
                string sUsersForProcess = Config.INSTANCE.getParameter("WORUPULT");
                if (sUsersForProcess == string.Empty) {
                    setVisible(oForm, "IDH_PRCBTC", false);
                    setVisible(oForm, "IDH_PRCLTR", false);
                }

                string[] aUsersForProcess = sUsersForProcess.Split(',');
                bool bFound = false;
                for (int i = 0; i < aUsersForProcess.Length; i++) {
                    if (aUsersForProcess[i].Trim().Equals(DataHandler.INSTANCE.User, StringComparison.OrdinalIgnoreCase)) {
                        setVisible(oForm, "IDH_PRCBTC", true);
                        setVisible(oForm, "IDH_PRCLTR", true);
                        bFound = true;
                        break;
                    }
                }
                if (!bFound) {
                    setVisible(oForm, "IDH_PRCBTC", false);
                    setVisible(oForm, "IDH_PRCLTR", false);
                }

            }
        }

        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            doReLoadData(oForm, !getHasSharedData(oForm));
        }

        //** The Initializer
        protected void doReLoadData(SAPbouiCOM.Form oForm, bool bIsFirst) {
            try {
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);

                DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");//DBOGrid.getInstance(oForm, "LINESGRID");
                string sFilter = oGridN.getRequiredFilter();
                // doSetCustomeFilter(oGridN);

                //oGridN.setOrderValue("Cast(" + IDH_WOQITEM._JobNr + " as Int)," + IDH_WOQITEM._Sort + " ");
                //oGridN.setOrderValue("Cast(" + IDH_WORUHD._SrNo + " as Int),Cast(" + IDH_WORUHD._Code + " as Int)");
                //#MA 20170301 start Issue=292
                //string BatchNum = getItemValue(oForm, "IDH_BATCHF");
                if (!getItemValue(oForm, "IDH_BATCHF").Contains("*") & getItemValue(oForm, "IDH_BATCHF").Length > 0) {
                    sFilter = IDH_WRADHD._BatchNum + "='" + getItemValue(oForm, "IDH_BATCHF") + "'";
                    //setUFValue(oForm, "IDH_BATCHF", "");
                }
                if (sFilter != "" & sFilter != null) {
                    if (oGridN.getRequiredFilter().Length > 0) {
                        oGridN.setRequiredFilter(oGridN.getRequiredFilter() + " AND " + sFilter);
                    } else {
                        oGridN.setRequiredFilter(sFilter);
                    }
                }
                //#MA 20170301 End
                oGridN.doReloadData("", false, true);
                doFillGridCombos(oGridN);
                oGridN.setRequiredFilter(string.Empty);
                oGridN.getGrid().AutoResizeColumns();
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBL", null);
            } finally {
                oForm.Freeze(false);
                oForm.Refresh();
            }
        }

        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED) {
                if (pVal.ItemUID == "IDH_IMPEXC") {
                    string sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("WRNUSAVE", null);
                    //int iRow = pVal.Row;
                    //if (IDHAddOns.idh.addon.Base.PARENT.doMessage(sResourceMessage, 2, "Yes", "No", null, true) == 1) {
                    if ((((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find")) || (
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption != Translation.getTranslatedWord("Find") &&
                    (IDHAddOns.idh.addon.Base.PARENT.doMessage(sResourceMessage, 2, "Yes", "No", null, true) == 1))
                    ) {
                        doImportExcel(oForm);
                    }
                } else if (pVal.ItemUID == "IDH_DELBAT") {
                    // Delete Batch
                    doPreDeleteBatch(oForm);
                } else if (pVal.ItemUID == "IDH_SNDBTH") {
                    if ((((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find"))) {
                        doSendBatchForReview(oForm);
                    } else {
                        string sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("SAVEFORM", null);
                        com.idh.bridge.DataHandler.INSTANCE.doError(sResourceMessage);
                    }

                } else if (pVal.ItemUID == "IDH_VLDBAC") {
                    if ((((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find"))) {
                        doSendBatchforValidation(oForm);
                    } else {
                        string sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("SAVEFORM", null);
                        com.idh.bridge.DataHandler.INSTANCE.doError(sResourceMessage);
                    }

                } else if (pVal.ItemUID == "IDH_PRCBTC") {
                    if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find"))
                        doProcessBatch(oForm, true);
                } else if (pVal.ItemUID == "IDH_PRCLTR") {
                    if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find"))
                        doProcessBatch(oForm, false);
                }
            }
            return base.doItemEvent(oForm, ref pVal, ref BubbleEvent);
        }
        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == true) {
                if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Save") || ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update")) {
                    doSaveBatch(oForm);
                } else if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find")) {
                    doReLoadData(oForm, true);
                }
                BubbleEvent = false;
            }
        }
        public override void doButtonID2(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == true) {
                if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Save") || ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update")) {
                    doReLoadData(oForm, true);
                    BubbleEvent = false;
                }
            }
        }

        protected override void doHandleModalResultShared(SAPbouiCOM.Form oParentForm, string sModalFormType, string sLastButton = null) {
            if (sModalFormType.Equals("IDH_COMMFRM")) {
                DBOGrid oGridN = (DBOGrid)getWFValue(oParentForm, "LINESGRID");
                if (getSharedData(oParentForm, "CMMNT") == null || getSharedData(oParentForm, "CMMNT").ToString().Trim() == string.Empty) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Please provide reason for deletion of batch(s).");
                    return;
                }
                string sComments = getSharedData(oParentForm, "CMMNT").ToString().Trim();
                doDeleteBatch(oParentForm, sComments);
            }
            base.doHandleModalResultShared(oParentForm, sModalFormType, sLastButton);
        }
        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCloseForm(oForm, ref BubbleEvent);
            UpdateGrid.doRemoveGrid(oForm, "LINESGRID");
        }
        public override void doClose() {
        }
        protected void doGridDoubleClick(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
            if (pVal.Row >= 0) {

                string sRowCode = oGridN.doGetFieldValue(IDH_WORUHD._WOR).ToString();

                ArrayList oData = new ArrayList();
                //if (sWOQID.Length == 0) {
                //   com.idh.bridge.DataHandler.INSTANCE.doResUserError("Valid row must be selected.", "ERUSJOBS", null);
                //}else 
                if (!string.IsNullOrEmpty(sRowCode) && oGridN.doCheckIsSameCol(pVal.ColUID, IDH_WORUHD._WOR)) {
                    //Open WOR
                    ArrayList moData = new ArrayList();
                    moData.Add("DoUpdate");
                    moData.Add(oGridN.doGetFieldValue(IDH_WORUHD._WOR).ToString());
                    setSharedData("IDHJOBS", "ROWCODE", oGridN.doGetFieldValue(IDH_WORUHD._WOR).ToString());
                    goParent.doOpenModalForm("IDHJOBS", oForm, moData);
                } else {//PBI

                }

            }
        }

        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED) {
                if (pVal.ItemUID == "LINESGRID") {
                    if (pVal.BeforeAction == false) {
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE);
                        //  doGridDoubleClick(oForm, ref pVal);
                    }
                }
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK && !pVal.BeforeAction) {
                if (pVal.ItemUID == "LINESGRID") {
                    if (pVal.BeforeAction == false) {
                        //doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE);
                        doGridDoubleClick(oForm, ref pVal);
                    }
                }
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_ADD && !pVal.BeforeAction) {
                pVal.oGrid.getSBOGrid().CommonSetting.SetRowFontColor(pVal.Row + 1, Convert.ToInt32("0000FF", 16));
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SORT && pVal.BeforeAction == false) {
                UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
                doFillGridCombos(oGridN);
                oGridN.doApplyRules();
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK && pVal.Row >= 0 && pVal.BeforeAction) {
                try {
                    //oJobSeq.gotoRow(oJobTypes.Item(iIndex))
                    //sJobType = oJobSeq.U_JobTp
                    if (pVal.oGrid.getSBOGrid().CommonSetting.GetCellFontColor(pVal.Row + 1, pVal.oGrid.doIndexFieldWC(pVal.ColUID) + 1) == Convert.ToInt32("0000FF", 16)) {
                        SAPbouiCOM.MenuItem oMenuItem;
                        oMenuItem = goParent.goApplication.Menus.Item(IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU);

                        SAPbouiCOM.Menus oMenus;
                        SAPbouiCOM.MenuCreationParams oCreationPackage;
                        oCreationPackage = (SAPbouiCOM.MenuCreationParams)goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);

                        oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                        oCreationPackage.UniqueID = "ValidateData";
                        //'oCreationPackage.String = getTranslatedWord("Duplicate Selection")
                        oCreationPackage.String = getTranslatedWord("Validate Data");//
                        oCreationPackage.Enabled = true;
                        oMenus = oMenuItem.SubMenus;
                        oMenus.AddEx(oCreationPackage);

                    }
                } catch (Exception ex) {
                    //com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doApplyNewAmendAction" });
                } finally {
                    //    oForm.Freeze(false);
                }
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT && pVal.Row >= 0 && pVal.BeforeAction) {
                SAPbouiCOM.MenuEvent opVal = (SAPbouiCOM.MenuEvent)pVal.oData;
                if (opVal.MenuUID.Equals("ValidateData")) {

                    doValidateGridCell(oForm, pVal.Row, pVal.ColUID);
                }
            }
            return base.doCustomItemEvent(oForm, ref pVal);
        }

        private void doValidateGridCell(SAPbouiCOM.Form oForm, int r, string sCOlID) {
            try {
                DBOGrid oDBGrid = (DBOGrid)getWFValue(oForm, "LINESGRID");

                //IDH_JOBENTR oWOH = new IDH_JOBENTR();
                //oWOH.UnLockTable = true;

                //               int RGBRed = Convert.ToInt32("0000FF", 16);

                bool bRowValidated = doValidateBatch(oForm, r);
                //               int iLastWOR = 0;
                //               bool bAdditionalItem = false;

                //               if (oDBGrid.doGetFieldValue(IDH_WORUHD._AddtItm, r).ToString() == string.Empty) {
                //                   bAdditionalItem = false;
                //                   iLastWOR = r;
                //               } else {
                //bAdditionalItem = true;
                //               }

                //if (sCOlID == IDH_WORUHD._CustCd && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WORUHD._CustCd, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer Code missing.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._CustCd) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WORUHD._CustNm && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WORUHD._CustNm, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer name missing.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._CustNm) + 1, RGBRed);
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WORUHD._CustCd && bAdditionalItem == false && !Config.INSTANCE.ValidateBPCodenName(oDBGrid.doGetFieldValue(IDH_WORUHD._CustCd, r).ToString(), oDBGrid.doGetFieldValue(IDH_WORUHD._CustNm, r).ToString())) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid Customer Code /Name.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._CustCd) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WORUHD._CustAdr && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WORUHD._CustAdr, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer address is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._CustAdr) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WORUHD._CusPCode && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WORUHD._CusPCode, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer postcode is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._CusPCode) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WORUHD._CustAdr && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WORUHD._CustAdr, r).ToString() != string.Empty
                //    && !Config.INSTANCE.ValidateBPAddress(oDBGrid.doGetFieldValue(IDH_WORUHD._CustCd, r).ToString(), oDBGrid.doGetFieldValue(IDH_WORUHD._CustAdr, r).ToString(), "S")) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer address/postcode is not valid.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._CustAdr) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WORUHD._JbTypeDs && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WORUHD._JbTypeDs, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Order Type is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._JbTypeDs) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WORUHD._SuppCd && oDBGrid.doGetFieldValue(IDH_WORUHD._SuppCd, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Supplier code is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._SuppCd) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WORUHD._SuppNm && oDBGrid.doGetFieldValue(IDH_WORUHD._SuppNm, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Supplier name is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._SuppNm) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WORUHD._SuppCd && !Config.INSTANCE.ValidateBPCodenName(oDBGrid.doGetFieldValue(IDH_WORUHD._SuppCd, r).ToString(), oDBGrid.doGetFieldValue(IDH_WORUHD._SuppNm, r).ToString())) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid Supplier Code /Name.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._SuppCd) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WORUHD._ItemCd && oDBGrid.doGetFieldValue(IDH_WORUHD._ItemCd, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Container code is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._ItemCd) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WORUHD._ItemNm && oDBGrid.doGetFieldValue(IDH_WORUHD._ItemNm, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Container name is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._ItemNm) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WORUHD._WastCd && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WORUHD._WastCd, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Waste item code is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._WastCd) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WORUHD._WasteNm && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WORUHD._WasteNm, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Waste item name is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._WasteNm) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    bRowValidated = false;
                //} else if ((sCOlID == IDH_WORUHD._WasteNm || sCOlID == IDH_WORUHD._WastCd) && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WORUHD._WastCd, r).ToString() != string.Empty  && bAdditionalItem == false&& !Config.INSTANCE.ValidateWasteItem(oDBGrid.doGetFieldValue(IDH_WORUHD._WastCd, r).ToString())) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Waste item: " + oDBGrid.doGetFieldValue(IDH_WORUHD._WastCd, r).ToString() + " is not a valid waste item.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._WastCd) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WORUHD._ItemCd && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WORUHD._ItemCd, r).ToString() != string.Empty && !Config.INSTANCE.ValidateContainerItem(oDBGrid.doGetFieldValue(IDH_WORUHD._ItemCd, r).ToString())) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Container item: " + oDBGrid.doGetFieldValue(IDH_WORUHD._WastCd, r).ToString() + " is not a valid container item.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._ItemCd) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WORUHD._JbTypeDs && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WORUHD._JbTypeDs, r).ToString() != string.Empty && !Config.INSTANCE.ValidateJobTypeWithContainerItem(oDBGrid.doGetFieldValue(IDH_WORUHD._JbTypeDs, r).ToString(), oDBGrid.doGetFieldValue(IDH_WORUHD._ItemCd, r).ToString())) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Order Type : " + oDBGrid.doGetFieldValue(IDH_WORUHD._JbTypeDs, r).ToString() + " is not a valid with attached container.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._JbTypeDs) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WORUHD._UOM && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WORUHD._UOM, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("UOM is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._UOM) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WORUHD._Qty && bAdditionalItem == false && (oDBGrid.doGetFieldValue(IDH_WORUHD._Qty, r).ToString() == string.Empty || (int)oDBGrid.doGetFieldValue(IDH_WORUHD._Qty, r) <= 0)) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Qty must be greater than zero. Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._Qty) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                //    bRowValidated = false;
                //} else {
                //    if (sCOlID == IDH_WORUHD._WOH && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WORUHD._WOH, r).ToString() != string.Empty && !oWOH.getByKey(oDBGrid.doGetFieldValue(IDH_WORUHD._WOH, r).ToString())) {
                //        com.idh.bridge.DataHandler.INSTANCE.doUserError("Failed to load WOH#" + oDBGrid.doGetFieldValue(IDH_WORUHD._WOH, r).ToString() + ".");
                //        oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._WOH) + 1, RGBRed);
                //        bRowValidated = false;
                //    }
                //    if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WORUHD._WOH, r).ToString() != string.Empty && oWOH.Code != string.Empty) {
                //          if (sCOlID == IDH_WORUHD._CustCd && oWOH.U_CardCd != oDBGrid.doGetFieldValue(IDH_WORUHD._CustCd, r).ToString()) {
                //            com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer code is different than on WOH#" + oDBGrid.doGetFieldValue(IDH_WORUHD._WOH, r).ToString() + ".");
                //            oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //            oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._CustCd) + 1, RGBRed);
                //            bRowValidated = false;
                //        } else if (sCOlID == IDH_WORUHD._CustNm && oWOH.U_CardNM != oDBGrid.doGetFieldValue(IDH_WORUHD._CustNm, r).ToString()) {
                //            com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer name is different than on WOH#" + oDBGrid.doGetFieldValue(IDH_WORUHD._WOH, r).ToString() + ".");
                //            oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //            oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._CustNm) + 1, RGBRed);
                //            bRowValidated = false;
                //        } else if (sCOlID == IDH_WORUHD._CustAdr && oWOH.U_CAddress != oDBGrid.doGetFieldValue(IDH_WORUHD._CustAdr, r).ToString()) {
                //            com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer address is different than on WOH#" + oDBGrid.doGetFieldValue(IDH_WORUHD._WOH, r).ToString() + ".");
                //            oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //            oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._CustAdr) + 1, RGBRed);
                //            bRowValidated = false;
                //        } else if (sCOlID == IDH_WORUHD._SuppCd && oWOH.U_PCardCd != oDBGrid.doGetFieldValue(IDH_WORUHD._SuppCd, r).ToString()) {
                //            com.idh.bridge.DataHandler.INSTANCE.doUserError("Supplier code is different than on WOH#" + oDBGrid.doGetFieldValue(IDH_WORUHD._WOH, r).ToString() + ".");
                //            oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //            oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._SuppCd) + 1, RGBRed);
                //            bRowValidated = false;
                //        } else if (sCOlID == IDH_WORUHD._SuppNm && oWOH.U_PCardNM != oDBGrid.doGetFieldValue(IDH_WORUHD._SuppNm, r).ToString()) {
                //            com.idh.bridge.DataHandler.INSTANCE.doUserError("Supplier name is different than on WOH#" + oDBGrid.doGetFieldValue(IDH_WORUHD._WOH, r).ToString() + ".");
                //            oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                //            oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._SuppNm) + 1, RGBRed);
                //            bRowValidated = false;
                //        } 
                //    }

                //}
                //}
                if (bRowValidated) {
                    //oDBGrid.getSBOGrid().CommonSetting.SetRowFontColor(r + 1, 0);
                    //oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "1");
                    if (oDBGrid.doProcessData()) {
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);
                        // GridRows oRows= oDBGrid.getSBOGrid().Rows;//.Columns.Item(1).ce
                        //CellPosition cp = oDBGrid.getSBOGrid().GetCellFocus();
                        //cp.
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                 new string[] { "IDH_WORUHD.doValidateGridCell" });
            }
        }
        private void doSendBatchforValidation(SAPbouiCOM.Form oForm) {
            try {
                DBOGrid oDBGrid = (DBOGrid)getWFValue(oForm, "LINESGRID");
                if (oDBGrid.getRowCount() == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("No batch loaded to \"Send for Validation\"");
                    return;
                }

                bool bRet = true;
                IDH_WORUHD oBatchDetail = (IDH_WORUHD)oDBGrid.DBObject;
               // oBatchDetail.doValidateRows();
               // return;
                DataHandler.INSTANCE.StartTransaction();
                if (oBatchDetail.Count >= 0) {
                    oForm.Freeze(true);
                    oBatchDetail.first();
                    while (oBatchDetail.next() && oBatchDetail.Code != string.Empty) {
                        oBatchDetail.U_ProcSts = ((int)IDH_WORUHD.en_BatchProcessStatus.ReadyForReview).ToString();
                        oBatchDetail.U_VldStatus = "";
                        if (oBatchDetail.doUpdateDataRow() == false) {
                            bRet = false;
                            break;
                        }
                    }

                }
                
                if (DataHandler.INSTANCE.IsInTransaction()) {
                    if (bRet)
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    else
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                }

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doSendBatchforValidation" });
                if (DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
            } finally {
                oForm.Freeze(false);
            }
        }
        private void doSendBatchForReview(SAPbouiCOM.Form oForm) {
            try {
                DBOGrid oDBGrid = (DBOGrid)getWFValue(oForm, "LINESGRID");
                if (oDBGrid.getRowCount() == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("No batch loaded to send for process.");
                    return;
                }

                string sUsersForProcess = Config.INSTANCE.getParameter("WORUPULT");
                if (sUsersForProcess == string.Empty) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("No user configured for batch process. Please update config list.");
                    return;
                }
                string sBatchNum = "";
                for (int r = 0; r <= oDBGrid.getRowCount() - 1; r++) {
                    if (sBatchNum == "")
                        sBatchNum = oDBGrid.doGetFieldValue(IDH_WORUHD._BatchNum, r).ToString();
                    else if (sBatchNum != oDBGrid.doGetFieldValue(IDH_WORUHD._BatchNum, r).ToString() && oDBGrid.doGetFieldValue(IDH_WORUHD._BatchNum, r).ToString() != "") {
                        if (r != oDBGrid.getRowCount() - 1) {
                            com.idh.bridge.DataHandler.INSTANCE.doUserError("Please only one batch to process.");
                            return;
                        }
                    }
                }
                if (sBatchNum == "") {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Please save data to assign batch number.");
                    return;
                }
                IDH_WORUHH obatch = new IDH_WORUHH();
                obatch.UnLockTable = true;
                if (obatch.getByKey(sBatchNum)) {
                    string sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("ALRARDST", new string[] { sBatchNum });
                    if (obatch.U_AlterSend == "Y" && (IDHAddOns.idh.addon.Base.PARENT.doMessage(sResourceMessage, 2, "Yes", "No", null, true) != 1)) {
                        return;
                    }
                }

                if (doValidateBatch(oForm, -1) == false) {
                    oDBGrid.doProcessData();
                    return;
                }

                bool bRet = true;
                obatch = new IDH_WORUHH();
                obatch.UnLockTable = true;
                if (obatch.getByKey(sBatchNum) &&
                    com.idh.bridge.utils.General.SendInternalAlert(sUsersForProcess, Messages.INSTANCE.getMessage("ALRBTMSG", new string[] { "Mass WOR Updater", sBatchNum }), "Mass WOR Updater Batch#" + sBatchNum)
                    ) {
                    DataHandler.INSTANCE.StartTransaction();
                    //if (obatch.getByKey(sBatchNum)) {
                    obatch.U_AlterSdBy = DataHandler.INSTANCE.User;
                    obatch.U_AlterSdDt = DateTime.Today;
                    obatch.U_AlterSdTm = DateTime.Now.ToString("HHmm");
                    obatch.U_AlterSdTo = sUsersForProcess;
                    obatch.U_AlterSend = "Y";
                    if (obatch.doProcessData()) {
                        IDH_WORUHD oBatchDetail = (IDH_WORUHD)oDBGrid.DBObject;

                        if (oBatchDetail.Count >= 0) {
                            oForm.Freeze(true);
                            oBatchDetail.first();
                            while (oBatchDetail.next() && oBatchDetail.Code != string.Empty) {
                                oBatchDetail.U_ProcSts = ((int)IDH_WORUHD.en_BatchProcessStatus.SentToReview).ToString();
                                if (oBatchDetail.doUpdateDataRow() == false) {
                                    bRet = false;
                                    break;
                                }
                            }
                        }
                    } else {
                        bRet = false;
                    }
                    if (DataHandler.INSTANCE.IsInTransaction()) {
                        if (bRet)
                            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                        else
                            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doSendBatchForReview" });
                if (DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
            } finally {
                oForm.Freeze(false);
            }
        }
        #region"Process Batch"
        private void doProcessBatch(SAPbouiCOM.Form oForm, bool bProcessNow) {
            try {
                string sMsg = "Are you sure to process batch now? It may take several minutes to process batch.";
                if (!bProcessNow)
                    sMsg = "Are you sure to send batch to process later by ISB Commander service?";

                if (IDHAddOns.idh.addon.Base.PARENT.doMessage(sMsg, 2, "Yes", "No", null, true) != 1) {
                    return;
                }
                DBOGrid oDBGrid = (DBOGrid)getWFValue(oForm, "LINESGRID");
                if (oDBGrid.getRowCount() == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("No batch loaded for process.");
                    return;
                }
                bool doCheckForMultiBatch = true;
                string sBatchNum = "";
                try {
                    string sLastSQL = oDBGrid.DBObject.LastQuery.ToLower();
                    string[] aLastSQL = sLastSQL.Split(new string[] { "from" }, StringSplitOptions.None);
                    if (aLastSQL.Length > 1) {
                        sLastSQL = aLastSQL[aLastSQL.Length - 1];
                        aLastSQL = sLastSQL.Split(new string[] { "order by" }, StringSplitOptions.None);
                        if (aLastSQL.Length > 0)
                            sLastSQL = aLastSQL[0];

                        sLastSQL = "Select Distinct " + IDH_WORUHD._BatchNum + " From " + sLastSQL;
                        DataRecords oRecs = null;
                        //string sQry = "select DocEntry from " + sDocTable + " where U_FlagToClose = 'Y' AND DocStatus != 'C'";
                        oRecs = DataHandler.INSTANCE.doBufferedSelectQuery("doGetDistinctBatch" + DateTime.Now.ToString("HHmmttss"), sLastSQL);
                        if (oRecs != null && oRecs.RecordCount > 0) {
                            if (oRecs.RecordCount == 1) {
                                sBatchNum = oRecs.getValue(IDH_WORUHD._BatchNum).ToString();

                            } else {
                                com.idh.bridge.DataHandler.INSTANCE.doUserError("Please only one batch to process.");
                                return;
                            }
                            doCheckForMultiBatch = false;
                        }
                    }
                } catch (Exception ex) {
                }
                if (doCheckForMultiBatch) {
                    for (int r = 0; r <= oDBGrid.getRowCount() - 1; r++) {
                        if (sBatchNum == "")
                            sBatchNum = oDBGrid.doGetFieldValue(IDH_WORUHD._BatchNum, r).ToString();
                        else if (sBatchNum != oDBGrid.doGetFieldValue(IDH_WORUHD._BatchNum, r).ToString() && oDBGrid.doGetFieldValue(IDH_WORUHD._BatchNum, r).ToString() != "") {
                            if (r != oDBGrid.getRowCount() - 1) {
                                com.idh.bridge.DataHandler.INSTANCE.doUserError("Please only one batch to process.");
                                return;
                            }
                        }
                    }
                }

                if (sBatchNum == "") {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Please save data to assign batch number.");
                    return;
                }
                IDH_WORUHH obatch = new IDH_WORUHH();
                obatch.UnLockTable = true;
                if (obatch.getByKey(sBatchNum)) {
                    if (obatch.U_AlterSend != "Y") {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Select batch is not ready to process.");
                        // && (IDHAddOns.idh.addon.Base.PARENT.doMessage(sResourceMessage, 2, "Yes", "No", null, true) != 1)) {
                        return;
                    } else if (obatch.U_BtchClose == "Y") {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Select batch is already closed.");
                        return;
                    }
                } else {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Failed to load batch header.");
                    return;
                }
                IDH_WORUHD oBatchDetail = (IDH_WORUHD)oDBGrid.DBObject;
                oBatchDetail.UnLockTable = true;
              
                bool bDone = true;
                oBatchDetail.doBookmark();
                oBatchDetail.first();
                int iRow = 0;
                DataHandler.INSTANCE.doInfo("Please Wait: System is processing your batch request, this may take some time depending on the batch size.");
                while (oBatchDetail.next()) {
                    if (oBatchDetail.Code != string.Empty) {
                        if (oBatchDetail.doProcessBatchItem(!bProcessNow)) {
                            //now process call the PBI Handler
                            DataHandler.INSTANCE.doProgress("PROCEDDADHOCWOR", iRow++, oBatchDetail.Count);
                        } else {
                            bDone = false;
                            //break;
                        }
                    }
                }
                if (bDone) {
                    if (DataHandler.INSTANCE.IsInTransaction())
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    if (bProcessNow)
                        com.idh.bridge.DataHandler.INSTANCE.doInfo("Successfully processed the batch.");
                    else
                        com.idh.bridge.DataHandler.INSTANCE.doInfo("Successfully send the batch for processed later.");
                } else if (bDone == false) {
                    if (DataHandler.INSTANCE.IsInTransaction())
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    DataHandler.INSTANCE.doUserError("Failed to process batch. Please review the error messages.");
                }
                oBatchDetail.doRecallBookmark();
                oDBGrid.doReloadData("", false, true);
                doFillGridCombos(oDBGrid);
            } catch (Exception ex) {
                if (DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doProcessBatch" });

            } finally {
                DataHandler.INSTANCE.doProgressDone("PROCEDDADHOCWOR");
                if (DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
            }
        }

        private bool doValidateBatch(SAPbouiCOM.Form oForm, int row) {
            try {
                DBOGrid oDBGrid = (DBOGrid)getWFValue(oForm, "LINESGRID");


                //IDH_JOBENTR oWOH = new IDH_JOBENTR();
                //oWOH.UnLockTable = true;

                IDH_JOBSHD oWOR = new IDH_JOBSHD();
                oWOR.UnLockTable = true;

                int RGBRed = Convert.ToInt32("0000FF", 16);

                bool bRowValidated = true;
                bool bValidated = true;
                com.idh.bridge.DataHandler.INSTANCE.doInfo("Please wait while system is validating batch.");
                bool doSetUpdateMode = false;

                for (int r = 0; r <= oDBGrid.getRowCount() - 1; r++) {
                    if (row != -1) {
                        r = row;
                        oDBGrid.getSBOGrid().CommonSetting.SetRowFontColor(r + 1, 0);
                    }
                    if (oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString() == string.Empty)
                        continue;
                    DataHandler.INSTANCE.doProgress("IDHVLDPBIBATCH", r, (int)(oDBGrid.getRowCount() - 1));
                    bRowValidated = true;

                    if (oDBGrid.doGetFieldValue(IDH_WORUHD._WOR, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("WOR Code missing.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._WOR) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                        bRowValidated = false;
                    }
                    if (oDBGrid.doGetFieldValue(IDH_WORUHD._UOM, r).ToString() != string.Empty && !Config.INSTANCE.isValidUOM(oDBGrid.doGetFieldValue(IDH_WORUHD._UOM, r).ToString())) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid UOM.Code#" + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._UOM) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                        bRowValidated = false;
                    } else {
                        if (oDBGrid.doGetFieldValue(IDH_WORUHD._WOR, r).ToString() != string.Empty && !oWOR.getByKey(oDBGrid.doGetFieldValue(IDH_WORUHD._WOR, r).ToString())) {
                            com.idh.bridge.DataHandler.INSTANCE.doUserError("Failed to load WOR#" + oDBGrid.doGetFieldValue(IDH_WORUHD._WOR, r).ToString() + ".");
                            oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                            oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._WOR) + 1, RGBRed);
                            bRowValidated = false;
                        } else if (oDBGrid.doGetFieldValue(IDH_WORUHD._WOR, r).ToString() != string.Empty && oWOR.Code != null && oWOR.Code == oDBGrid.doGetFieldValue(IDH_WORUHD._WOR, r).ToString()) {
                            if (!oWOR.doCheckCanEdit(false)) {
                                com.idh.bridge.DataHandler.INSTANCE.doUserError("WOR#" + oWOR.Code + " already billed, " + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                                oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                                oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._WOR) + 1, RGBRed);
                                //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WORUHD._AmndTp));
                                bRowValidated = false;
                            }else if (oDBGrid.doGetFieldValue(IDH_WORUHD._JbTypeDs, r) != null && !string.IsNullOrWhiteSpace(oDBGrid.doGetFieldValue(IDH_WORUHD._JbTypeDs, r).ToString().Trim()) &&
                                     Config.INSTANCE.getValueFromTablebyKey("[@IDH_JOBTYPE]", "Code", "U_JobTp", IDHAddOns.idh.data.Base.doFixForSQL(oDBGrid.doGetFieldValue(IDH_WORUHD._JbTypeDs, r).ToString())) == null) {
                            com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid Job Type '" + oDBGrid.doGetFieldValue(IDH_WORUHD._JbTypeDs, r).ToString() + "', " + oDBGrid.doGetFieldValue(IDH_WORUHD._Code, r).ToString());
                            oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "0");
                            oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WORUHD._JbTypeDs) + 1, RGBRed);
                            bRowValidated = false;
                        }
                        } 
                    }
                    if (!bRowValidated) {
                        bValidated = false;
                        doSetUpdateMode = true;
                    } else { //if (oDBGrid.getSBOGrid().CommonSetting.GetRowFontColor(r + 1)==RGBRed)
                        oDBGrid.getSBOGrid().CommonSetting.SetRowFontColor(r + 1, 0);
                        oDBGrid.doSetFieldValue(IDH_WORUHD._VldStatus, r, "1");
                        doSetUpdateMode = true;
                    }
                    if (row != -1) {
                        break;
                    }
                }
                if (!bValidated)
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Failed to validate batch rows. Please review the errors listed above.");
                else
                    com.idh.bridge.DataHandler.INSTANCE.doInfo("Batch validation completed.");
                return bValidated;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_WORUHD.doValidateAmendmentList" });
                return false;
            } finally {
                DataHandler.INSTANCE.doProgressDone("IDHVLDPBIBATCH");
            }
        }

        #endregion
        #region "Save Batch"
        private void doSaveBatch(SAPbouiCOM.Form oForm) {
            try {
                bool bret;
                DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");
                oGridN.doProcessData();
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);
                oGridN.setInitialFilterValue("");

                //IDH_WORUHD oBatchDetail = (IDH_WORUHD)oGridN.DBObject;
                //idh.dbObjects.numbers.NumbersPair oNextNum;

                //IDH_WORUHH objBatch = new IDH_WORUHH();
                //if (oBatchDetail.Count >= 0) {
                //    oForm.Freeze(true);
                //    oBatchDetail.first();
                //    if (oBatchDetail.U_BatchNum == string.Empty) {
                //        oNextNum = objBatch.getNewKey();
                //        while (oBatchDetail.next() && oBatchDetail.Code!=string.Empty) {
                //            oBatchDetail.U_BatchNum = oNextNum.CodeCode;
                //        }
                //        objBatch.doAddEmptyRow(true);
                //        objBatch.Code = oNextNum.CodeCode;
                //        objBatch.Name = oNextNum.NameCode;
                //    }
                //    bret = oBatchDetail.doProcessData();
                //    bret = objBatch.doProcessData();
                //    doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);
                //    oGridN.setInitialFilterValue("");
                //}
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doSaveBatch" });
            } finally {
                oForm.Freeze(false);
            }
        }
        #endregion
        #region "Delete Batch"
        private void doDeleteBatch(SAPbouiCOM.Form oForm, string sComments) {
            try {
                string sBatches = getItemValue(oForm, "IDH_BATCHF").Trim();
                DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");
                // string sDeleteComments = "";
                string[] aBatches = sBatches.Split(',');
                IDH_WORUHH objBatchHeader = new IDH_WORUHH();
                objBatchHeader.UnLockTable = true;
                IDH_WORUHD objBatch = new IDH_WORUHD();
                objBatch.UnLockTable = true;
                bool bRet = true;
                //DataHandler.INSTANCE.StartTransaction();
                int iRet = objBatchHeader.getData(IDH_WORUHH._Code + " In (" + sBatches + ") ", "");
                if (iRet > 0) {
                    objBatchHeader.first();
                    while (objBatchHeader.next()) {
                        objBatchHeader.U_BthStatus = "0";
                        objBatchHeader.U_Comment = sComments;
                        bRet = objBatchHeader.doUpdateDataRow("Batch Deleted");
                        if (!bRet) {
                            break;
                        }
                    }
                }
                if (bRet) {
                    //delete detailed batch
                    string sSQL = "Delete from ["+ IDH_WORUHD.TableName + "] Where "+ IDH_WORUHD._BatchNum + " In (" + sBatches + ")  And "+ IDH_WORUHD._ProcSts + "!='3' ";
                    PARENT.goDB.doUpdateQuerySkipPatchQueue("DEL WOR BATCH",sSQL);
                    //iRet = objBatch.getData(IDH_WORUHD._BatchNum + " In (" + sBatches + ") ", "");
                    //if (iRet > 0) {
                    //    //objBatch.doMarkForDelete();
                    //   // bRet = objBatch.doProcessData();

                    //    objBatch.first();
                    //    int iR = 0;
                    //    while (objBatch.next()) {
                    //        //objBatch.U_BthStatus = "0";
                    //        //objBatch.U_Comments = sComments;
                    //        bRet = objBatch.doMarkForDelete();
                    //    //  bRet=  objBatch.doProcessData(false, "Batch Deleted");
                    //        //bRet = objBatch.doUpdateDataRow(false,"Batch Deleted");
                    //        if (!bRet) {
                    //            break;
                    //        }
                    //    //  bRet=  objBatch.doProcessData(false, "Batch Deleted");
                    //        DataHandler.INSTANCE.doProgress("BATCHDELETE", iR++, iRet);
                    //    }
                    //    bRet = objBatch.doProcessData(false, "Batch Deleted");

                    //}
                }
                if (!bRet ) {
                    //DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Failed to delete batched.");
                } else if (bRet ) {
                    //DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText("Successfully deleted batches [" + sBatches + "]", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    doReLoadData(oForm, true);
                }


            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doDeleteBatch" });
            } finally {
                oForm.Freeze(false);
                DataHandler.INSTANCE.doProgressDone("BATCHDELETE");
            }
        }
        private void doPreDeleteBatch(SAPbouiCOM.Form oForm) {
            try {
                string sResourceMessage = "";
                if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Save") || ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update")) {
                    sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("SAVEFORM", null);
                    com.idh.bridge.DataHandler.INSTANCE.doError(sResourceMessage);
                    return;
                }
                string sBatches = getItemValue(oForm, "IDH_BATCHF").Trim();
                //  DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");

                if (sBatches == null || sBatches == string.Empty) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Please provide batches to delete.");
                    doSetFocus(oForm, "IDH_BATCHF");
                    return;
                }
                doReLoadData(oForm, true);
                IDH_WORUHD oBatchDetail = new IDH_WORUHD();

                sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("LSRER006", new string[] { sBatches });

                if (IDHAddOns.idh.addon.Base.PARENT.doMessage(sResourceMessage, 2, "Yes", "No", null, true) != 1) {
                    return;
                }
                goParent.doOpenModalForm("IDH_COMMFRM", oForm);


            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doPreDeleteBatch" });
            } finally {
                oForm.Freeze(false);
            }
        }
        #endregion

        #region Import From Excel
        private void doImportExcel(SAPbouiCOM.Form oForm) {
            try {
                DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");
                bool bCancelled = false;
                string sFileName = "";
                doCallWinFormOpener(delegate { BrowserExcelFile(oForm, ref sFileName, ref bCancelled); }, oForm);
                if (bCancelled)
                    return;
                if (sFileName == null || sFileName.Trim() == "") {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Please provide a valid file name.");
                    return;
                }

                com.idh.bridge.DataHandler.INSTANCE.doInfo("Please wait while system is importing file.");//, SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                doReadExcelFile(oForm, sFileName, oGridN);
                doFillGridCombos(oGridN);
                if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update")) {
                    doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE);
                }

                //
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error retrieving the Customers Address - " + sCardCode);
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new String[] { "Exporting All live services to csv." });
            }
        }
        private void doReadExcelFile(SAPbouiCOM.Form oForm, string sFileName, DBOGrid oDBGrid) {
            //  DBOGrid oDBGrid = (DBOGrid)getWFValue(oForm, "LINESGRID");
            try {
                string sNewBatch = "";
                ImportWORSheet oReadFile = new ImportWORSheet();
                oReadFile.doReadFileODBC(oDBGrid, oForm, sFileName, ref sNewBatch);
                FilterFields moFilterFields = oDBGrid.getGridControl().getFilterFields();
                try {
                    for (int i = 0; i <= moFilterFields.Count - 1; i++) {
                        FilterField moFilterFiled = (FilterField)moFilterFields[i];
                        setUFValue(oForm, moFilterFiled.msFieldName, "");
                    }
                } catch (Exception ex) {

                }
                //IDH_BATCHF

                setItemValue(oForm, "IDH_BATCHF", sNewBatch);
                oDBGrid.doReloadData("", false, true);
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error retrieving the Customers Address - " + sCardCode);
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new String[] { "Error in doReadExcelFile." });
            }
        }
        public void BrowserExcelFile(SAPbouiCOM.Form oForm, ref string sFileName, ref bool bCancelled) {
            System.Windows.Forms.OpenFileDialog oOpenFile = new System.Windows.Forms.OpenFileDialog();
            try {
                string sExt = Config.INSTANCE.getImportExtension();
                if (sExt != null && sExt.Length > 0) {
                    if (sExt.IndexOf("|") == -1) {
                        sExt = "(" + sExt + ")|" + sExt;
                    }
                    oOpenFile.Filter = sExt;
                }

            } catch (Exception ex) {
                //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error importing the data.");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", new string[] { string.Empty });
            }
            oOpenFile.FileName = "OpenExcelFile";
            oOpenFile.InitialDirectory = com.isb.enq.lookups.Config.INSTANCE.getImportLiveServicesFolder();
            if (com.idh.win.AppForm.OpenFileDialog(oOpenFile) == System.Windows.Forms.DialogResult.OK) {
                sFileName = oOpenFile.FileName;
            } else
                bCancelled = true;
        }
        #region "Read and Process Excel File"
        public class ImportWORSheet {
            public static int miMinRows = 1;
            public static string SheetName = "WOR Mass Update";
            public static string[] TRIGGERS = {"WOR","RequestDate","StartDate",  "EndDate",    "Cancelled", "Reason",   "CustRef#",    "Maximo#",
                "Cust Reporting Weight", "UOM", "HaulageQty",
                "Disposal Qty",    "Rebate Weights","Job Type" };
            public class ColPos {
                public static int WOR = 0;
                public static int RequestDate = 1;
                public static int StartDate = 2;
                public static int EndDate = 3;

                public static int Cancelled = 4;
                public static int VehType = 5;
                public static int CustRef = 6;

                public static int MaximoNum = 7;
                public static int CustReportingWeight = 8;
                //public static int Supplier Name =  ;
                public static int UOM = 9;
                public static int HaulageQty = 10;
                public static int DisposalQty = 11;
                public static int RebateWeight = 12;
                public static int JobType = 13;
            }


            public ImportWORSheet() { }
            public void doReadFileODBC(DBOGrid oGridN, SAPbouiCOM.Form oForm, string sExcelFile, ref string sNewBatch) {

                bool bItemsFound = false;
                // string sTempWORCSVFile = "";
                string sFilter;
                try {
                    miMinRows = Config.INSTANCE.getParameterAsInt("WORUPFRN", 2, false) + 1;
                    SheetName = Config.INSTANCE.getParameter("WORUSHNM");

                    System.Data.DataTable dtExcel = doReadExcelFile_ExcelReader(sExcelFile);
                    if (dtExcel == null) {
                        return;
                    }
                    //public static int miMinRows = 8;
                    //public static string SheetName = "Service Amendments";
                    if (true) {
                        if (dtExcel.Columns.Count > 10) {
                            if (dtExcel.Columns[ColPos.WOR].ColumnName.Equals(TRIGGERS[0], StringComparison.OrdinalIgnoreCase) &&
                                     dtExcel.Columns[ColPos.RequestDate].ColumnName.Equals(TRIGGERS[1], StringComparison.OrdinalIgnoreCase) &&
                                     dtExcel.Columns[ColPos.StartDate].ColumnName.Equals(TRIGGERS[2], StringComparison.OrdinalIgnoreCase) &&
                                     dtExcel.Columns[ColPos.EndDate].ColumnName.Equals(TRIGGERS[3], StringComparison.OrdinalIgnoreCase) &&
                                      dtExcel.Columns[ColPos.Cancelled].ColumnName.Equals(TRIGGERS[4], StringComparison.OrdinalIgnoreCase) &&
                                       dtExcel.Columns[ColPos.CustRef].ColumnName.Equals(TRIGGERS[6], StringComparison.OrdinalIgnoreCase) &&
                                        dtExcel.Columns[ColPos.MaximoNum].ColumnName.Equals(TRIGGERS[7], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.CustReportingWeight].ColumnName.Equals(TRIGGERS[8], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.UOM].ColumnName.Equals(TRIGGERS[9], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.HaulageQty].ColumnName.Equals(TRIGGERS[10], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.DisposalQty].ColumnName.Equals(TRIGGERS[11], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.RebateWeight].ColumnName.Equals(TRIGGERS[12], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.JobType].ColumnName.Equals(TRIGGERS[13], StringComparison.OrdinalIgnoreCase)
                                ) {

                                oGridN.DBObject.doClearBuffers();
                                sFilter = oGridN.getRequiredFilter();
                                oGridN.setRequiredFilter(IDH_WORUHD._BatchNum + " =\'-999\' ");
                                oGridN.doReloadData("", false, true);
                                oGridN.doApplyRules();
                                oGridN.doSetDoCount(true);
                                oGridN.setRequiredFilter(sFilter);

                                oGridN.AddEditLine = true;
                                DataRecords oRS = null;
                                IDH_WORUHH oBatch = new IDH_WORUHH();
                                /////////////////////////////////
                                idh.dbObjects.numbers.NumbersPair oNextNum;
                                oNextNum = oBatch.getNewKey();
                                sNewBatch = oNextNum.CodeCode;
                                oBatch.doAddEmptyRow(true);
                                oBatch.Code = oNextNum.CodeCode;
                                oBatch.Name = oNextNum.NameCode;

                               
                                bool bret = oBatch.doProcessData();
                                /////////////////////////////////
                                //string sName="",sCode="";
                                IDH_WORUHD oBatchDetail = new IDH_WORUHD();
                                oBatchDetail.UnLockTable = true;
                                com.idh.bridge.DataHandler.INSTANCE.doInfo("Please wait while system is importing WOR sheet using SSIS Package.");
                                dtExcel.Columns.Add("Code", typeof(string));
                                dtExcel.Columns.Add("Name", typeof(string));
                                dtExcel.Columns.Add("BatchNumber", typeof(string));

                                dtExcel.Columns.Add("ProcSts", typeof(string));
                                dtExcel.Columns.Add("VldStatus", typeof(string));
                                dtExcel.Columns.Add("BthStatus", typeof(string));

                                dtExcel.Columns.Add("U_CustCd", typeof(string));
                                dtExcel.Columns.Add("U_CustNm", typeof(string));
                                dtExcel.Columns.Add("U_CustAdr", typeof(string));
                                dtExcel.Columns.Add("U_CarrCd", typeof(string));
                                dtExcel.Columns.Add("U_CarrNm", typeof(string));
                                dtExcel.Columns.Add("U_ItemCd", typeof(string));
                                dtExcel.Columns.Add("U_ItemNm", typeof(string));
                                dtExcel.Columns.Add("U_WastCd", typeof(string));
                                dtExcel.Columns.Add("U_WasteNm", typeof(string));

                                if (!dtExcel.Columns.Contains(IDH_WORUHD._AddedBy))
                                    dtExcel.Columns.Add(IDH_WORUHD._AddedBy, typeof(string));

                                
                                string sBatchNum = sNewBatch;
                                string sLastParent = "";

                                //dtExcel.Select().ToList<DataRow>()
                                //.ForEach(r => {
                                //    r["BatchNumber"] = sBatchNum; r["ProcSts"] = "0"; r["VldStatus"] = ""; r["BthStatus"] = "1";
                                //    r["Code"] = oBatchDetail.getNewKey().CodeCode; r["Name"] = r["Code"];
                                //    r[TRIGGERS[ColPos.CustomerCode]] = doGetSBOKey("OCRD", "CardName", "CardCode", r[TRIGGERS[ColPos.CustomerName]].ToString(), oRS, "CardType", "C");
                                //    r[TRIGGERS[ColPos.SupplierCode]] = doGetSBOKey("OCRD", "CardName", "CardCode", r[TRIGGERS[ColPos.SupplierName]].ToString(), oRS, "CardType", "S");
                                //    r[TRIGGERS[ColPos.ContainerCode]] = doGetSBOKey("OITM", "ItemName", "ItemCode", r[TRIGGERS[ColPos.Container]].ToString(), oRS, "", "");
                                //    r[TRIGGERS[ColPos.WasteMaterialCode]] = doGetSBOKey("OITM", "ItemName", "ItemCode", r[TRIGGERS[ColPos.WasteMaterial]].ToString(), oRS, "", "");
                                //    if (!r[TRIGGERS[ColPos.AdditionalItem]].ToString().Equals("Y", StringComparison.OrdinalIgnoreCase))
                                //        sLastParent = r["Code"].ToString();
                                //    else
                                //        r["ParentID"] = sLastParent;
                                //});
                                string sSQL = "";
                                dtExcel.Select().ToList<DataRow>()
                               .ForEach(r => {
                                   r["BatchNumber"] = sBatchNum; r["ProcSts"] = "0"; r["VldStatus"] = ""; r["BthStatus"] = "1";
                                   r["Code"] = oBatchDetail.getNewKey().CodeCode; r["Name"] = r["Code"];
                                   if (r[TRIGGERS[ColPos.CustReportingWeight]].ToString().Equals("#Remove", StringComparison.OrdinalIgnoreCase))
                                       r[TRIGGERS[ColPos.CustReportingWeight]] = "-999";
                                   if (r[TRIGGERS[ColPos.HaulageQty]].ToString().Equals("#Remove", StringComparison.OrdinalIgnoreCase))
                                       r[TRIGGERS[ColPos.HaulageQty]] = "-999";
                                   if (r[TRIGGERS[ColPos.DisposalQty]].ToString().Equals("#Remove", StringComparison.OrdinalIgnoreCase))
                                       r[TRIGGERS[ColPos.DisposalQty]] = "-999";
                                   if (r[TRIGGERS[ColPos.RebateWeight]].ToString().Equals("#Remove", StringComparison.OrdinalIgnoreCase))
                                       r[TRIGGERS[ColPos.RebateWeight]] = "-999";

                                   if (r[TRIGGERS[ColPos.HaulageQty]] == null || r[TRIGGERS[ColPos.HaulageQty]].ToString().Trim() == "") {
                                       r[TRIGGERS[ColPos.HaulageQty]] = 0;
                                   }
                                   if (r[TRIGGERS[ColPos.DisposalQty]] == null || r[TRIGGERS[ColPos.DisposalQty]].ToString().Trim() == "") {
                                       r[TRIGGERS[ColPos.DisposalQty]] = 0;
                                   }

                                   if (r[TRIGGERS[ColPos.RebateWeight]] == null || r[TRIGGERS[ColPos.RebateWeight]].ToString().Trim() == "") {
                                       r[TRIGGERS[ColPos.RebateWeight]] = 0;
                                   }


                                   if (r[TRIGGERS[ColPos.RequestDate]] != null && r[TRIGGERS[ColPos.RequestDate]].ToString() == "#Remove")
                                       r[TRIGGERS[ColPos.RequestDate]] = "1999/01/01";

                                   if (r[TRIGGERS[ColPos.StartDate]] != null && r[TRIGGERS[ColPos.StartDate]].ToString() == "#Remove")
                                       r[TRIGGERS[ColPos.StartDate]] = "1999/01/01";
                                   //else
                                   //if (r[TRIGGERS[ColPos.StartDate]] != null && r[TRIGGERS[ColPos.StartDate]].ToString().Length > 0)
                                   //    r[TRIGGERS[ColPos.StartDate]] = Convert.ToDateTime(DateTime.Today.Year.ToString().Substring(0, 2) + r[TRIGGERS[ColPos.StartDate]].ToString().Substring(6, 2)
                                   //    + "-" + r[TRIGGERS[ColPos.StartDate]].ToString().Substring(3, 2) + "-" + r[TRIGGERS[ColPos.StartDate]].ToString().Substring(0, 2));

                                   if (r[TRIGGERS[ColPos.EndDate]] != null && r[TRIGGERS[ColPos.EndDate]].ToString() == "#Remove")
                                       r[TRIGGERS[ColPos.EndDate]] = "1999/01/01";
                                   //if (r[TRIGGERS[ColPos.EndDate]] != null && r[TRIGGERS[ColPos.EndDate]].ToString().Length > 0)
                                   //    r[TRIGGERS[ColPos.EndDate]] = Convert.ToDateTime(DateTime.Today.Year.ToString().Substring(0, 2) + r[TRIGGERS[ColPos.EndDate]].ToString().Substring(6, 2)
                                   //    + "-" + r[TRIGGERS[ColPos.EndDate]].ToString().Substring(3, 2) + "-" + r[TRIGGERS[ColPos.EndDate]].ToString().Substring(0, 2));

                                   r[IDH_WORUHD._AddedBy] = "WR1";

                                   sSQL = "Select r.Code ,r.U_CustCd , r.U_CustNm , woh.U_Address ,r.U_CarrCd , r.U_CarrNm ,r.U_ItemCd , r.U_ItemDsc ,r.U_WasCd , " +
                                 " r.U_WasDsc From [@IDH_JOBSHD] r with(nolock)inner join[@IDH_JOBENTR] woh with(nolock) on r.U_JobNr = woh.Code Where r.Code=\'" + r["WOR"] + "\'";
                                   DataRecords oDr = DataHandler.INSTANCE.doBufferedSelectQuery("WORMassUpdate" + r["WOR"].ToString(), sSQL);
                                   if (oDr != null && oDr.RecordCount > 0) {
                                       r["U_CustCd"] = oDr.getValue("U_CustCd").ToString();
                                       r["U_CustNm"] = oDr.getValue("U_CustNm").ToString();
                                       r["U_CustAdr"] = oDr.getValue("U_Address").ToString();
                                       r["U_CarrCd"] = oDr.getValue("U_CarrCd").ToString();
                                       r["U_CarrNm"] = oDr.getValue("U_CarrNm").ToString();
                                       r["U_ItemCd"] = oDr.getValue("U_ItemCd").ToString();
                                       r["U_ItemNm"] = oDr.getValue("U_ItemDsc").ToString();
                                       r["U_WastCd"] = oDr.getValue("U_WasCd").ToString();
                                       r["U_WasteNm"] = oDr.getValue("U_WasDsc").ToString();
                                   }
                               });

                                //dt.ToList<DataRow>()
                                //             .ForEach(r => {
                                //                 r["BatchNumber"] = sNewBatch;
                                //                 r["ProcSts"] = "0";
                                //             });
                                dtExcel.AcceptChanges();
                                StringWriter osw = new StringWriter();
                                int iCol = 1;
                                for (iCol = 0; iCol < dtExcel.Columns.Count; iCol++) {
                                    osw.Write(dtExcel.Columns[iCol].ColumnName + "\t");
                                }
                                int iCRow = 0;
                                osw.Write("\n");
                                foreach (DataRow row in dtExcel.Rows) {
                                    IEnumerable<string> fields = row.ItemArray.Select(field =>
                                      string.Concat("", field.ToString().Replace("\"", "\"\""), ""));
                                    osw.Write(string.Join("\t", fields));
                                    osw.Write("\n");
                                    DataHandler.INSTANCE.doProgress("IDHIMPLSAMD", iCRow++, dtExcel.Rows.Count);
                                }

                                string sODBC = Config.INSTANCE.getParameter("REPDSN");
                                string sODBCUser = Config.INSTANCE.getParameter("REPUSR");
                                string sODBCPwd = Config.INSTANCE.getParameter("REPPAS");
                                string sDBServer = DataHandler.INSTANCE.DBServer;
                                string sDBName = DataHandler.INSTANCE.DBName;

                                //Microsoft.SqlServer.Dts.Runtime.Application app = new Microsoft.SqlServer.Dts.Runtime.Application();
                                //Package package = null;
                                //string message="";

                                //package = app.LoadPackage(sAssemblyPath + "WORBatchUpdaterPackage.dtsx", null);
                                //package.Variables["User::SqlConString"].Value = "Dsn=" + sODBC + ";uid=" + sODBCUser + ";pwd=" + sODBCPwd + ";";
                                //package.Variables["User::CSVFileName"].Value = sTempWORCSVFile;
                                //package.Variables["User::OLEDBConString"].Value = "Data Source="+ sDBServer + ";User ID=" + sODBCUser + ";Initial Catalog="+ sDBName +";pwd="+ sODBCPwd +";Provider=SQLNCLI11.1;Persist Security Info=True;Auto Translate=False;";

                                //Microsoft.SqlServer.Dts.Runtime.DTSExecResult results = package.Execute();
                                ////Check the results for Failure and Success
                                //if (results == Microsoft.SqlServer.Dts.Runtime.DTSExecResult.Failure) {
                                //    string err = "";
                                //    foreach (Microsoft.SqlServer.Dts.Runtime.DtsError local_DtsError in package.Errors) {
                                //        string error = local_DtsError.Description.ToString();
                                //        err = err + error;
                                //    }
                                //    if (err != string.Empty) {
                                //        com.idh.bridge.DataHandler.INSTANCE.doUserError("SSIS Package failed: " + err);
                                //        bItemsFound = false;
                                //    }
                                //}
                                //if (results == Microsoft.SqlServer.Dts.Runtime.DTSExecResult.Success) {
                                //    bItemsFound = true;
                                //    message = "Package Executed Successfully....";
                                //}
                                //System.IO.File.Delete(sTempWORCSVFile);
                                DataHandler.INSTANCE.StartTransaction();
                                using (System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection("Data Source=" + DataHandler.INSTANCE.SBOCompany.Server + ";Initial Catalog=" + DataHandler.INSTANCE.DBName + ";User ID=" + sODBCUser + ";Password=" + sODBCPwd + ";")) {
                                    cn.Open();
                                    using (System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy(cn)) {
                                        bulkCopy.DestinationTableName =
                                            "dbo.[" + IDH_WORUHD.TableName + "]";

                                        try {
                                            bulkCopy.BatchSize = 500;
                                            //////
                                            bulkCopy.ColumnMappings.Add("Code", IDH_WORUHD._Code);
                                            bulkCopy.ColumnMappings.Add("Name", IDH_WORUHD._Name);

                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.WOR], IDH_WORUHD._WOR);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RequestDate], IDH_WORUHD._RDate);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.StartDate], IDH_WORUHD._SDate);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.EndDate], IDH_WORUHD._EDate);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Cancelled], IDH_WORUHD._Cancelled);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.VehType], IDH_WORUHD._VehTyp);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CustRef], IDH_WORUHD._CustRef);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.MaximoNum], IDH_WORUHD._MaximoNum);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CustReportingWeight], IDH_WORUHD._ExptQty);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.UOM], IDH_WORUHD._UOM);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.HaulageQty], IDH_WORUHD._HulgQty);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.DisposalQty], IDH_WORUHD._DispQty);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RebateWeight], IDH_WORUHD._RebWgt);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.JobType], IDH_WORUHD._JbTypeDs);

                                            bulkCopy.ColumnMappings.Add("ProcSts", IDH_WORUHD._ProcSts);
                                            bulkCopy.ColumnMappings.Add("BatchNumber", IDH_WORUHD._BatchNum);
                                            bulkCopy.ColumnMappings.Add("VldStatus", IDH_WORUHD._VldStatus);

                                            bulkCopy.ColumnMappings.Add("BthStatus", IDH_WORUHD._BthStatus);


                                            bulkCopy.ColumnMappings.Add("U_CustCd", IDH_WORUHD._CustCd);
                                            bulkCopy.ColumnMappings.Add("U_CustNm", IDH_WORUHD._CustNm);
                                            bulkCopy.ColumnMappings.Add("U_CustAdr", IDH_WORUHD._CustAdr);
                                            bulkCopy.ColumnMappings.Add("U_CarrCd", IDH_WORUHD._CarrCd);
                                            bulkCopy.ColumnMappings.Add("U_CarrNm", IDH_WORUHD._CarrNm);
                                            bulkCopy.ColumnMappings.Add("U_ItemCd", IDH_WORUHD._ItemCd);
                                            bulkCopy.ColumnMappings.Add("U_ItemNm", IDH_WORUHD._ItemNm);
                                            bulkCopy.ColumnMappings.Add("U_WastCd", IDH_WORUHD._WastCd);
                                            bulkCopy.ColumnMappings.Add("U_WasteNm", IDH_WORUHD._WasteNm);

                                            bulkCopy.ColumnMappings.Add(IDH_WORUHD._AddedBy, IDH_WORUHD._AddedBy);

                                            
                                            //////
                                            // Write from the source to the destination.
                                            bulkCopy.WriteToServer(dtExcel);
                                            bItemsFound = true;
                                            DataHandler.INSTANCE.doProgressDone("IDHIMPLSAMD");
                                            com.idh.bridge.DataHandler.INSTANCE.doInfo("WOR import process completed.");
                                            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                            com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(Translation.getTranslatedMessage("EXPEXLDN", "Successfully import the file"), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                                        } catch (Exception ex) {
                                            bItemsFound = false;
                                            DataHandler.INSTANCE.doResSBOError(ex.Message, "EREXSGEN", new String[] { "Failed Copying Data to SBO Table" });
                                            //Console.WriteLine(ex.Message);
                                        }
                                    }
                                }


                            }// end of col header validation
                            else {
                                com.idh.bridge.DataHandler.INSTANCE.doUserError("Header validation failed");
                                return;
                            }
                        }//end if icol>31
                    }
                } catch (Exception e) {
                    //DataHandler.INSTANCE.doError("Exception: " + e.ToString(), "Error importing the Journals.");
                    com.idh.bridge.DataHandler.INSTANCE.doResSBOError(e.Message, "EREXIMPJ", null);
                    bItemsFound = false;
                    if (DataHandler.INSTANCE.IsInTransaction())
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                } finally {
                    if (DataHandler.INSTANCE.IsInTransaction())
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    if (oGridN != null) {
                        oGridN.AddEditLine = false;
                        DataHandler.INSTANCE.doProgressDone("IDHIMPLSAMD");
                    }

                }
                //try {
                //    if (sTempWORCSVFile != "" && System.IO.File.Exists(sTempWORCSVFile)) {
                //        System.IO.File.Delete(sTempWORCSVFile);
                //    }
                //} catch (Exception ex) {

                //}
                if (!bItemsFound) {
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("This is not a valid Import File - " + sExcelFile, "ERUSINIF", new String[] { sExcelFile });
                }

            }
            private string doGetSBOKey(string sTableName, string sKeyField, string sFiledName, string sKeyValue, DataRecords oRS, string sTypeField, string sType) {
                try {
                    // sName = Conversions.ToString(dt.Rows[r][TRIGGERS[ColPos.CustomerName]]);// oExcel.getValue(r, )).Trim();
                    string sCode = "";
                    if (sKeyValue != string.Empty) {
                        if (sType != "")
                            oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGet" + sTableName + "CodeByName" + sKeyValue, "Select Top 1 " + sFiledName + " from " + sTableName + " with(nolock) Where " + sKeyField + "=\'" + sKeyValue + "\' And " + sTypeField + "=\'" + sType + "\'");
                        else
                            oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGet" + sTableName + "CodeByName" + sKeyValue, "Select Top 1 " + sFiledName + " from " + sTableName + " with(nolock) Where " + sKeyField + "=\'" + sKeyValue + "\' ");

                        if (oRS != null && oRS.RecordCount > 0) {
                            sCode = oRS.getValueAsString(sFiledName).Trim();
                        }
                    }
                    return sCode;
                } catch (Exception e) {
                    //DataHandler.INSTANCE.doError("Exception: " + e.ToString(), "Error importing the Journals.");
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(e.Message, "EREXIMPJ", new string[] { "doGetSBOKey" });
                }
                return "";

            }

            public System.Data.DataTable doReadExcelFile_ExcelReader(string sFileName) {
                try {
                    // int iFirstRow = Config.INSTANCE.getParameterAsInt("WORUPFRN",7,false);
                    // string sSheetName= Config.INSTANCE.getParameter("WORUSHNM");

                    FileStream stream = File.Open(sFileName, FileMode.Open, FileAccess.Read);
                    Excel.IExcelDataReader excelReader;
                    System.Data.DataTable dtdataTable = new System.Data.DataTable();
                    ////3. DataSet - The result of each spreadsheet will be created in the result.Tables
                    //DataSet result = excelReader.AsDataSet();
                    if (sFileName.ToLower().EndsWith(".xlsx")) {
                        //97+ .xlslx
                        //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                        excelReader = Excel.ExcelReaderFactory.CreateOpenXmlReader(stream);
                        //4. DataSet - Create column names from first row
                        excelReader.FirstRow = miMinRows - 1;
                        excelReader.SheetNameToRead = SheetName;
                        excelReader.IsFirstRowAsColumnNames = true;
                        //int r = excelReader.cou[;

                        dtdataTable = excelReader.AsDataSet().Tables[0];

                        //dtdataTable.Columns.Add("Code", new Type() { "string" });
                        excelReader.Close();
                    } else {
                        //2003-97
                        //.xls
                        //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                        excelReader = Excel.ExcelReaderFactory.CreateBinaryReader(stream);
                        //4. DataSet - Create column names from first row
                        excelReader.IsFirstRowAsColumnNames = true;
                        excelReader.FirstRow = miMinRows - 1;
                        excelReader.SheetNameToRead = SheetName;
                        dtdataTable = excelReader.AsDataSet().Tables[0];
                        excelReader.Close();
                    }
                    //using (System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy("CONNECTION_STRING")) {
                    //    bulkCopy.DestinationTableName =
                    //        "dbo.[" + sTableName + "]";

                    //    try {
                    //        // Write from the source to the destination.
                    //        bulkCopy.WriteToServer(dsdataSet.Tables[0]);
                    //    } catch (Exception ex) {
                    //        Console.WriteLine(ex.Message);
                    //    }
                    //}
                    return dtdataTable;
                } catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError(ex.Message, "EREXSGEN", new string[] { "doReadExcelFile_ExcelReader" });
                  //  com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doReadExcelFile_ExcelReader" });
                }
                return null;
            }

            private string noNull(object oValue) {
                return oValue == null ? "" : oValue.ToString();
            }

            private static string doFixStr(object oValue, int iLength) {
                string sVal = oValue == null ? "" : oValue.ToString();
                sVal = (sVal.Length > iLength ? sVal.Substring(0, (iLength - 3)) + "..." : sVal);
                return sVal;
            }
        }
        #endregion
        #endregion

    }//end of class

}//end of namespace
