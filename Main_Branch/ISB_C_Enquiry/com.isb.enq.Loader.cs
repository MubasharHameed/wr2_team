﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.isb.enq {
    public class Loader : com.isb.AModuleLoader {

        public Loader() {
            ReleaseCount = 1079;
        }

        public override void doCreateLookup() { }

        public override bool doSetupDB(IDHAddOns.idh.data.Base oData) {
            com.isb.forms.Enquiry.admin.DataStructures.FORCECREATE = false;
            com.isb.forms.Enquiry.admin.DataStructures.doSetupUserDefined(ReleaseCount, oData); // ReleaseCount, oData);
            return true;
        }

        public override void doRegisterMenus(string sMenuID) {
            com.isb.forms.Enquiry.admin.MainForm.doCreateMenu(sMenuID);
        }

        public override void doRegisterForms(IDHAddOns.idh.addon.Base oParent) {
            com.isb.forms.Enquiry.admin.MainForm.doAddForms(oParent);
        }

        public override void doRegisterClasses() {
        }
    }
}
