﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IDHAddOns.idh.controls;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry.Search {
    class EnqWasteProfileItemSearch : WR1_Search.idh.forms.search.WPWasteItemSearch {
        public EnqWasteProfileItemSearch(IDHAddOns.idh.addon.Base oParent)
            : base(oParent, "IDHWPSRCENQ") {
        }

        public override void doSetGridOptions(IDHAddOns.idh.controls.FilterGrid oGridN) {
            base.doSetGridOptions(oGridN);
        }

        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCompleteCreate(ref oForm, ref BubbleEvent);
           // doAddUF(oForm, "IDH_ADDITMFLG", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, false, false);
            SAPbouiCOM.Item oitm = (SAPbouiCOM.Item)oForm.Items.Item("IDH_ITMNAM");
            doAddEdit(oForm, "IDH_ITMFNM", 0,oitm.Left,oitm.Top+oitm.Height+2,oitm.Width,oitm.Height);
            //SAPbouiCOM.EditText oitm = (SAPbouiCOM.EditText)oForm.Items.Item("IDH_ITMFNM").Specific;
            //oitm.DataBind.SetBound(true,"","IDH_ITMNAM");
            // doAddUF(oForm, "IDH_ITMFNAM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, true);

        }
        protected override void doSetEventFilters() {
            base.doSetEventFilters();
        }
        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {
          //  setUFValue(oForm, "IDH_ADDITMFLG", getParentSharedData(oForm, "IDH_ADDITMFLG"));
           // string s_AdditinalItemSearch = (string)getParentSharedData(oForm, "IDH_ADDITMFLG");//getUFValue(oForm, "IDH_ADDITMFLG", true);
            //IDH_GRPCOD
            base.doBeforeLoadData(oForm);
        }
        protected override void doReadInputParams(SAPbouiCOM.Form oForm) {
            base.doReadInputParams(oForm);
            string s_AdditinalItemSearch = (string)getParentSharedData(oForm, "IDH_ADDITMFLG");//getUFValue(oForm, "IDH_ADDITMFLG", true);            
            if (s_AdditinalItemSearch != null && s_AdditinalItemSearch != "") {
                string sAddGrp = com.idh.bridge.lookups.Config.INSTANCE.doGetAdditionalExpGroup();
                setUFValue(oForm, "IDH_GRPCOD", sAddGrp);
            }
        }

        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            string s_AdditinalItemSearch = (string)getParentSharedData(oForm, "IDH_ADDITMFLG");//getUFValue(oForm, "IDH_ADDITMFLG", true);
           // string sParent=doGetParentID(oForm.UniqueID);
            //if (!string.IsNullOrWhiteSpace(sParent)) {
                SAPbouiCOM.Form oParentForm = PARENT.doGetParentForm(oForm);
                if (oParentForm.TypeEx == "IDH_ENQ") {
                    setUFValue(oForm, "IDH_CRDCD", getDFValue(oParentForm, IDH_ENQUIRY.TableName, IDH_ENQUIRY._CardCode));
                    setUFValue(oForm, "IDH_BPADDR", getDFValue(oParentForm, IDH_ENQUIRY.TableName, IDH_ENQUIRY._Address));
                }else if (oParentForm.TypeEx == "IDHWOQ") {
                    setUFValue(oForm, "IDH_CRDCD", getDFValue(oParentForm, IDH_WOQHD.TableName, IDH_WOQHD._CardCd));
                    setUFValue(oForm, "IDH_BPADDR", getDFValue(oParentForm, IDH_WOQHD.TableName, IDH_WOQHD._Address));
                }

           // }
            //idh.dbObjects.User.IDH_ENQUIRY oEnq=(idh.dbObjects.User.IDH_ENQUIRY)getParentSharedData(oForm, "IDH_POBJECT");//getUFValue(oForm, "IDH_ADDITMFLG", true);
            //IDH_CRDCD="+ moEnquiry.U_CardCode + ";IDH_INVENT=;IDH_BPADDR="+ moEnquiry.U_Address +"
            if (s_AdditinalItemSearch != null && s_AdditinalItemSearch != "") {
                FilterGrid oGridN = FilterGrid.getInstance(oForm, "LINESGRID");

                //Dim sRequiredString As String = "i.ItmsGrpCod = ig.ItmsGrpCod AND i.FrozenFor <> 'Y' "
                string sRequiredString = "i.ItmsGrpCod = ig.ItmsGrpCod ";
                string sWasteGroup = (string)getParentSharedData(oForm, "IDH_ADDITMGRP");

                // string sJobTypeGroup = (string)getUFValue(oForm, "IDH_JOBTP", true);

                if (sWasteGroup != null && sWasteGroup.Length > 0) {
                    setUFValue(oForm, "IDH_WSTGRP", sWasteGroup);
                    sWasteGroup = "'" + sWasteGroup + "'";
                    sRequiredString = sRequiredString + " AND i.ItemCode In ( Select additm.U_ItemCd from [@IDH_ADITVLD] additm with(nolock) Where additm.U_WstGpCd=" + sWasteGroup + ")";
                }
               // else
                 //   sRequiredString = sRequiredString + " AND i.ItemCode In ( Select additm.U_ItemCd from [@IDH_ADITVLD] additm )";
                
                
                oGridN.setRequiredFilter(sRequiredString);

                bClosingFormByOKButton = false;
                oGridN.doReloadData("", false, true);
            } else
                base.doLoadData(oForm);

            //complete re-writre here
            // Load Waste Group here from Additional Item Waste Group Table
        }


    }
}
