﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using IDHAddOns.idh.controls;
using com.idh.bridge.data;


namespace com.isb.forms.Enquiry {
   
    public class EnqQuestions : IDHAddOns.idh.forms.Base {

        protected string msKeyGen = "GENSEQ";
       
        public EnqQuestions(IDHAddOns.idh.addon.Base oParent)
            : base(oParent, "IDH_ENQQS", 0, "Enq_Questions.srf", true) {
        }
        protected string getTable() {
            return "@" + getUserTable();
        }

        protected string getUserTable() {
            return "IDH_ENQUS";
        }

        protected override void doSetEventFilters() {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);

        }
        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent)
		{
			try {
				oForm.Title = gsTitle;

				SAPbouiCOM.Item oItem = default(SAPbouiCOM.Item);
				oItem = oForm.Items.Item("IDH_CONTA");
				UpdateGrid oGridN = new UpdateGrid(this, oForm, "LINESGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane);

			} catch (Exception ex) {
				com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null );
				BubbleEvent = false;
			}
		}

        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCloseForm(oForm,ref BubbleEvent);
            FilterGrid.doRemoveGrid(oForm, "LINESGRID");
        }

        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {
             UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
            if (oGridN == null) {
                oGridN = new UpdateGrid(this, oForm, "LINESGRID");
            }
            oGridN.AddEditLine = false;

            doSetFilterFields(oGridN);
            doSetListFields(oGridN);

            oGridN.doAddGridTable(new GridTable(getTable(), null, "Code", true), true);
            
            oGridN.doSetDoCount(true);
            oGridN.doSetHistory(getUserTable(), "Code");
        }

        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                return;
            }
                UpdateGrid oGridN =(UpdateGrid) UpdateGrid.getInstance(oForm, "LINESGRID");
                //If oGridN.hasFieldChanged("U_Select") Then
                Int32 _WOQID = getParentSharedDataAsInteger(oForm, "WOQID");
                Int32 _EnqID = getParentSharedDataAsInteger(oForm, "ENQID");
                com.idh.dbObjects.User.IDH_ENQUS moIDH_ENQUS = new com.idh.dbObjects.User.IDH_ENQUS();
                for (Int16 iRow = 0; iRow <= oGridN.getRowCount() - 1; iRow++) {
                   
                    if (!string.IsNullOrEmpty(oGridN.doGetFieldValue("Code", iRow).ToString()) && moIDH_ENQUS.getByKey(oGridN.doGetFieldValue("Code", iRow).ToString())) {
                        moIDH_ENQUS.U_Answer = oGridN.doGetFieldValue("U_Answer", iRow).ToString();
                    } 
                    else{
                        moIDH_ENQUS.doAddEmptyRow(true, true, false);
                        moIDH_ENQUS.U_Answer = oGridN.doGetFieldValue("U_Answer", iRow).ToString();
                        if (_EnqID!=0)
                        moIDH_ENQUS.U_EnqID =Convert.ToInt32( oGridN.doGetFieldValue("U_EnqID", iRow).ToString());
                        moIDH_ENQUS.U_QsID = oGridN.doGetFieldValue("QID", iRow).ToString();
                        moIDH_ENQUS.U_Question = oGridN.doGetFieldValue("Question", iRow).ToString();
                        if (_WOQID!= 0)                        
                        moIDH_ENQUS.U_WOQID =Convert.ToInt32(  oGridN.doGetFieldValue("U_WOQID", iRow).ToString());
                        
                    }
                    moIDH_ENQUS.doProcessData();
                    //End If
                }
                doLoadData(oForm);
            
        }

        public override void doFinalizeShow(SAPbouiCOM.Form oForm) {
            base.doFinalizeShow(oForm);
        }

        protected void doSetFilterFields(IDHAddOns.idh.controls.FilterGrid oGridN) {
            //oGridN.doAddFilterField("IDH_DEPT", "U_DeptCode", SAPbouiCOM.BoDataType.dt_SHORT_NUMBER, "=", 10, Nothing)
            //oGridN.doAddFilterField("IDH_USERS", "U_UserName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "")
            //oGridN.doAddFilterField("IDH_FORMS", "U_FormID", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "")
        }

        protected void doSetListFields(IDHAddOns.idh.controls.FilterGrid oGridN) {
            oGridN.doAddListField("Code", "ID", false, 0, null, null);
            oGridN.doAddListField("Name", "Name", false, 0, null, null);
            oGridN.doAddListField("QID", "Name", false, 0, null, null);
            oGridN.doAddListField("Question", "Question", false, 150, null, null);

            oGridN.doAddListField("U_EnqID", "Enquiry ID", false, 0, null, null);
            oGridN.doAddListField("U_WOQID", "WOQ ID", false, 0, null, null);
            oGridN.doAddListField("U_Answer", "Answer", true, 150, null, null);
            oGridN.doAddListField("U_Sort", "Sort", false, 0, null, null);
        }

        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            Int32 _WOQID = getParentSharedDataAsInteger(oForm, "WOQID");
            Int32 _EnqID = getParentSharedDataAsInteger(oForm, "ENQID");
            string sSql = "";
            if (_EnqID != 0) {
                sSql = "Select T0.* From ( " +
                        " Select b.code,b.Name,a.Code as QID,isnull(b.U_Question, a.U_Question) as Question,b.U_EnqID,b.U_WOQID,b.U_Answer,a.U_Sort from  " +
                        " [@IDH_ENQSMS] a with(nolock), [@IDH_ENQUS] b where a.Code=b.U_QsID and (b.U_EnqID='" + _EnqID + "') " +
                        " UNION ALL " +
                        " Select null code,null Name,a.Code as QID,a.U_Question as Question,'" + _EnqID + "' U_EnqID,'" + _WOQID + "' U_WOQID,null U_Answer,a.U_Sort from " +
                        " [@IDH_ENQSMS] a with(nolock) where not a.Code in (Select b.U_QsID from [@IDH_ENQUS] b where (b.U_EnqID='" + _EnqID + "'))  " +
                        " )T0 Order by U_Sort";
            } else {
                sSql = "Select T0.* From ( " +
                        " Select b.code,b.Name,a.Code as QID,isnull(b.U_Question, a.U_Question) as Question,b.U_EnqID,b.U_WOQID,b.U_Answer,a.U_Sort from  " +
                        " [@IDH_ENQSMS] a with(nolock), [@IDH_ENQUS] b where a.Code=b.U_QsID and (b.U_WOQID ='" + _WOQID + "' ) " +
                        " UNION ALL " +
                        " Select null code,null Name,a.Code as QID,a.U_Question as Question,'" + _EnqID + "' U_EnqID,'" + _WOQID + "' U_WOQID,null U_Answer,a.U_Sort from " +
                        " [@IDH_ENQSMS] a with(nolock) where not a.Code in (Select b.U_QsID from [@IDH_ENQUS] b where (b.U_WOQID ='" + _WOQID + "' ))  " +
                        " )T0 Order by U_Sort";
            }
            
            UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
            oGridN.getSBOGrid().DataTable.ExecuteQuery(sSql);
            oGridN.doApplyRules();
            oGridN.Columns.Item("Question").Width =500;
            oGridN.Columns.Item("U_Answer").Width = 450;
        }

        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY) {
                IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");
                oGridN.doSetFieldValue("Name", pVal.Row, "1", true);
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
            }
            return base.doCustomItemEvent(oForm,ref pVal);

        }

        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            base.doItemEvent(oForm,ref pVal,ref BubbleEvent);
            if ((pVal.ItemUID == "IDH_WSTGRP") && pVal.EventType == SAPbouiCOM.BoEventTypes.et_COMBO_SELECT && pVal.BeforeAction == false) {
                doLoadData(oForm);
            }
            return true;
        }

        public override void doClose() {
        }
    }
}