using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.forms.oo;
using com.idh.bridge.data;
using IDHAddOns.idh.controls;
using com.uBC.utils;
using com.idh.controls;
using com.idh.bridge.utils;
using com.uBC.forms.fr3;
using com.idh.dbObjects.strct;
using com.idh.bridge.lookups;
//using com.idh.dbObjects.Base;
using com.idh.bridge;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry {
    public class CreateBP : com.idh.forms.oo.Form {

        public static class SeriesGroups {
          public static int[] Groups ={0, 218, 219, 220, 221,222, 223, 224, 225, 226, 227 };
        }

        bool bCanCreateBP = true;
        private int iSeries = -1;
        private bool bManualCardCode = true;
        //Select IsManual from NNM1,ONNM where ONNM.ObjectCode='2' And NNM1.ObjectCode='2' and ONNM.DfltSeries=NNM1.Series and NNM1.DocSubType='C'
        //private IDH_CREATEBP moCreateNewBP;
        //public IDH_CREATEBP ObjCreateNewBP {
        //    get { return (IDH_CREATEBP)moCreateNewBP; }
        //}
        public CreateBP(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
            doInitialize();
            doSetHandlers();
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_NEWBP", sParMenu, iMenuPos, "Enq_CreateNewBP.srf", false, true, false, "Create New Business Partner", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        protected  void doInitialize() {
           // moCreateNewBP = new IDH_CREATEBP();
        }



        #region Properties
        
        private string _CardCode = "";
        public string CardCode
        {
            get { return _CardCode; }
            //set { _CardCode = value; }
        }
        private bool _bUpdateParentForm = false;
        public bool bUpdateParentForm {
            get { return _bUpdateParentForm; }
            set { _bUpdateParentForm = value; }
        }
        private string _EnqID = "";
        public string EnqID {
            get { return _EnqID; }
            set { _EnqID = value; }
        }

        private string _WOQID = "";
        public string WOQID {
            get { return _WOQID; }
            set { _WOQID = value; }
        }

      /*  private string _SubBuilding;
        public string SubBuilding
        {
            get { return _SubBuilding; }
            set { _SubBuilding = value; }
        }
        private string _BuildingNumber;
        public string BuildingNumber
        {
            get { return _BuildingNumber; }
            set { _BuildingNumber = value; }
        }
        private string _BuildingName;
        public string BuildingName
        {
            get { return _BuildingName; }
            set { _BuildingName = value; }
        }

        private string _SecondaryStreet;
        public string SecondaryStreet
        {
            get { return _SecondaryStreet; }
            set { _SecondaryStreet = value; }
        }
        private string _Street;
        public string Street
        {
            get { return _Street; }
            set { _Street = value; }
        }
        private string _Block;
        public string Block
        {
            get { return _Block; }
            set { _Block = value; }
        }
        //Neighbourhood
        private string _District;
        public string District
        {
            get { return _District; }
            set { _District = value; }
        }
        private string _City;
        public string City
        {
            get { return _City; }
            set { _City = value; }
        }
        private string _Line1;
        public string Line1
        {
            get { return _Line1; }
            set { _Line1 = value; }
        }
        private string _Line2;
        public string Line2
        {
            get { return _Line2; }
            set { _Line2 = value; }
        }
        private string _Line3;
        public string Line3
        {
            get { return _Line3; }
            set { _Line3 = value; }
        }
        private string _Line4;
        public string Line4
        {
            get { return _Line4; }
            set { _Line4 = value; }
        }
        private string _Line5;
        public string Line5
        {
            get { return _Line5; }
            set { _Line5 = value; }
        }
        private string _AdminAreaName;
        public string AdminAreaName
        {
            get { return _AdminAreaName; }
            set { _AdminAreaName = value; }
        }
        //AdminAreaCode
        //Province
        private string _ProvinceName;
        public string ProvinceName
        {
            get { return _ProvinceName; }
            set { _ProvinceName = value; }
        }
        //ProvinceCode
        private string _PostalCode;
        public string PostalCode
        {
            get { return _PostalCode; }
            set { _PostalCode = value; }
        }
        private string _CountryName;
        public string CountryName
        {
            get { return _CountryName; }
            set { _CountryName = value; }
        }
        //CountryIso2
        //CountryIso3
        //CountryIsoNumber
        //SortingNumber1
        //SortingNumber2
        //Barcode
        private string _POBoxNumber;
        public string POBoxNumber
        {
            get { return _POBoxNumber; }
            set { _POBoxNumber = value; }
        }
        private string _Label;
        public string Label
        {
            get { return _Label; }
            set { _Label = value; }
        }
        private string _Type;
        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
        //DataLevel
        private string _AddressToSearch = "";
        /// <summary>
        /// Pass the address string to search, It will be prefilled in address field on form.
        /// </summary>
        public string AddressToSearch
        {
            get { return _AddressToSearch; }
            set { _AddressToSearch = value; }
        }
          */
        #endregion

        #region FormOpenCreateFunctions
        /** 
         * This Function will be called by the controller to allow the class to last minute Form Layout adjustments before it gets displayed
         * All changes made in hete will be cached if this is a cached form, the method will not be called again once the form has benn cached.
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            base.doCompleteCreate(ref BubbleEvent);

            doAddUF("IDH_CARDCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 15);
            doAddUF("IDH_CARDNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100);
            doAddUF("IDH_CARDTY", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);

            doAddUF("IDH_ADRID", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50);//Address ID 50
            doAddUF("IDH_STREET", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100);//Street 100
            doAddUF("IDH_BLOCK", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100);//Block 100
            doAddUF("IDH_CITY", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100);//City 100
            doAddUF("IDH_PCODE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20);//ZIPCODE 20
            doAddUF("IDH_COUNTY", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100);//County 100
            doAddUF("IDH_STATE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 3);//State 3
            doAddUF("IDH_CONTRY", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 3);//Country 3

            doAddUF("IDH_CNTPRS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50);//Contact Person 50
            doAddUF("IDH_EMAIL", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100);//Email 100
            doAddUF("IDH_PHONE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20);//Phone 20
            doAddUF("IDH_INFO", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30);//Label 30
            doAddUF("IDH_INFO2", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30);//Label 30

            AutoManaged = false;
            Items.Item("1").AffectsFormMode = false;
            for (int i = 0; i <= Items.Count - 1; i++) {
                Items.Item(i).AffectsFormMode = false;
            }

        }

        public override void doBeforeLoadData() {

            //  moCreateNewBP.doAddEmptyRow(true, true);

            SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_Ok;

            Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;

            doFillCombos();
            base.doBeforeLoadData();
            //mo.doAddEmptyRow(true, true);
            //setUFValue("IDH_CARDCD", moCreateNewBP.U_CardCode);
            //setUFValue("IDH_CARDNM", moCreateNewBP.U_CardName);
            //setUFValue("IDH_CARDTY", moCreateNewBP.U_CardType);

            //setUFValue("IDH_ADRID", moCreateNewBP.U_Address);//Address ID 50
            //setUFValue("IDH_STREET", moCreateNewBP.U_Street);//Street 100
            //setUFValue("IDH_BLOCK", moCreateNewBP.U_Block);//Block 100
            //setUFValue("IDH_CITY", moCreateNewBP.U_City);//City 100
            //setUFValue("IDH_PCODE", moCreateNewBP.U_ZipCode);//State 3
            //setUFValue("IDH_COUNTY", moCreateNewBP.U_County);//PostCode 20
            //setUFValue("IDH_STATE", moCreateNewBP.U_State);//County 100
            //setUFValue("IDH_CONTRY", moCreateNewBP.U_Country);//Country 3

            //setUFValue("IDH_CNTPRS", moCreateNewBP.U_CName);//Contact Person 50
            //setUFValue("IDH_EMAIL", moCreateNewBP.U_Email);//Email 100
            //setUFValue("IDH_PHONE", moCreateNewBP.U_Phone);//Phone 20
            //setUFValue("IDH_INFO", "Address:");//Phone 20

        }
        /** 
         * Do the final form steps to show after loaddata
         */
        public override void doFinalizeShow() {
            base.doFinalizeShow();

            SAPbobsCOM.SBObob oSBObob = (SAPbobsCOM.SBObob)DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoBridge);
            SAPbobsCOM.Recordset oRecordset;
            oRecordset = (SAPbobsCOM.Recordset)oSBObob.GetObjectPermission(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
            if (oRecordset==null || oRecordset.Fields.Item(0).Value ==null || oRecordset.Fields.Item(0).Value.ToString().ToUpper() != "Y") {
                bCanCreateBP = false;
                DataHandler.INSTANCE.doResUserError("Error: You do not have rights to create Business Partner.", "ERVEQ020", null);
                return;
            }
            string sQry = "Select * from NNM1 Where ObjectCode=2 And isnull(NNM1.Locked,'')<>'Y' and NNM1.DocSubType='C' and IsManual='Y' ";
                //"Select NNM1.IsManual,ONNM.DfltSeries,NNM1.GroupCode from NNM1,ONNM where ONNM.ObjectCode='2' And NNM1.ObjectCode='2' And isnull(NNM1.Locked,'')<>'Y' and ONNM.DfltSeries=NNM1.Series and NNM1.DocSubType='C'";// And NOT NNM1.NextNumber IS NULL";
            DataRecords oRecords = null;
           
            oRecords = DataHandler.INSTANCE.doBufferedSelectQuery("doCheckBPCodeSeries", sQry);

            if (oRecords != null && oRecords.RecordCount > 0 && oRecords.getValue("IsManual").ToString() == "Y") {
                bManualCardCode = true;
                EnableItem(true, "IDH_CARDCD");
                // moCreateNewBP.U_CardCode = "";
                setFocus("IDH_CARDCD");

            } else {
                int iSeriesGroup;
                //
                bManualCardCode = false;
                //for (int i = 0; i < oRecords.RecordCount; i++) {
                //    if (oRecords.getValue("GroupCode") != null && (int)oRecords.getValue("GroupCode")!=0) {
                //        iSeriesGroup = (int)oRecords.getValue("GroupCode");
                //        oRecordset = (SAPbobsCOM.Recordset)oSBObob.GetSystemPermission(DataHandler.INSTANCE.SBOCompany.UserName, SeriesGroups.Groups[iSeriesGroup].ToString());
                //        if (oRecordset.Fields.Item(0).Value != null || (oRecordset.Fields.Item(0).Value.ToString() == "1" || oRecordset.Fields.Item(0).Value.ToString() == "2")) {
                //            iSeries = oRecords.getValueAsInt("DfltSeries");
                //            break;
                //        }
                //    }
                //}
                if (iSeries == -1) {
                    sQry = "Select * from NNM1 Where ObjectCode=2 And isnull(NNM1.Locked,'')<>'Y' and NNM1.DocSubType='C' and IsManual!='Y' ";
                    oRecords = DataHandler.INSTANCE.doBufferedSelectQuery("doCheckBPCodeSeriesNNM1", sQry);
                    for (int i = 0; i < oRecords.RecordCount; i++) {
                        oRecords.gotoRow(i);
                        if (oRecords.getValue("GroupCode") != null) {
                            iSeriesGroup = (int)oRecords.getValue("GroupCode");
                            oRecordset = (SAPbobsCOM.Recordset)oSBObob.GetSystemPermission(DataHandler.INSTANCE.SBOCompany.UserName, SeriesGroups.Groups[iSeriesGroup].ToString());
                            if (oRecordset.Fields.Item(0).Value != null && (oRecordset.Fields.Item(0).Value.ToString() == "1" || oRecordset.Fields.Item(0).Value.ToString() == "2")) {
                                iSeries = oRecords.getValueAsInt("Series");
                                break;
                            }
                        }
                    }
                }
                //iSeries = oRecords.getValueAsInt("DfltSeries");
            }
            

            //SAPbobsCOM.UserPermissionTree oPermissionTree = (SAPbobsCOM.UserPermissionTree)DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserPermissionTree);
            //SAPbobsCOM.UserPermissionForms operm== (SAPbobsCOM.UserPermissionTree)DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.user);
            //operm.PermissionID
            //SAPbobsCOM.Users oUser = (SAPbobsCOM.Users)DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUsers);
            //bool bfound=oUser.GetByKey(3);
            ////oUser.UserPermission.
            //string sPermssion;
            //if (oUser.UserPermission.Count > 0) {
            //    for (int i=0;i< oUser.UserPermission.Count; i++) {
            //        oUser.UserPermission.SetCurrentLine(i);
            //        bfound = oPermissionTree.GetByKey(oUser.UserPermission.PermissionID);
            //        sPermssion = oPermissionTree.Name;
            //    }
            //}
        }
        /* 
         * Load the Form Data
         */
        public override void doLoadData() {
            base.doLoadData();
        }

        
        #endregion

        #region Events
        protected  void doSetHandlers() {
            doStartEventFilterBatch(true);
            Handler_Button_Ok = doCreateBP;
            //   base.doSetHandlers();
            //addHandler_ITEM_PRESSED("1", doCreateBP);
            doCommitEventFilters();

        }

        protected  void doSetGridHandlers() {
            //base.doSetGridHandlers();

            //moAdminGrid.Handler_GRID_DOUBLE_CLICK = new IDHGrid.ev_GRID_EVENTS(doRowDoubleClickEvent);
        }
        #endregion
        #region ItemEventHandlers
        public bool doCreateBP(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                if (!bCanCreateBP)
                    return true;
                string sCardName = "";
                if (bManualCardCode == true && getUFValue("IDH_CARDCD").ToString() == string.Empty) {
                    DataHandler.INSTANCE.doResUserError("Error: Business Partner Code missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Business Partner Code") });
                    BubbleEvent = false;
                    return true;
                }
                if (bManualCardCode == true && doValidateBPbyCode(ref sCardName) == true) {
                    com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage(("ERPKEXIS"), new string[] { getUFValue("IDH_CARDCD").ToString() });
                    BubbleEvent = false;
                    return true;
                }
                try {
                    SAPbobsCOM.BusinessPartners oBP = (SAPbobsCOM.BusinessPartners)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
                    if (bManualCardCode)
                        oBP.CardCode = getUFValue("IDH_CARDCD").ToString();
                    else {
                        oBP.Series = iSeries;
                    }
                    oBP.CardName = getUFValue("IDH_CARDNM").ToString();
                    string BPTye = getUFValue("IDH_CARDTY").ToString();
                    if (BPTye == "C" || BPTye == null || BPTye==string.Empty) {
                        oBP.CardType = SAPbobsCOM.BoCardTypes.cCustomer;
                    } else if (BPTye == "S") {
                        oBP.CardType = SAPbobsCOM.BoCardTypes.cSupplier;
                    } else if (BPTye == "L") {
                        oBP.CardType = SAPbobsCOM.BoCardTypes.cLid;
                    } else
                        oBP.CardType = SAPbobsCOM.BoCardTypes.cCustomer;
                    oBP.Valid = SAPbobsCOM.BoYesNoEnum.tYES;

                    //   oBP.Addresses.Add();
                    oBP.Addresses.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo;
                    oBP.Addresses.AddressName = getUFValue("IDH_ADRID").ToString();//Address ID 50
                    oBP.Addresses.Street = getUFValue("IDH_STREET").ToString();//Street 100
                    oBP.Addresses.Block = getUFValue("IDH_BLOCK").ToString();//Block 100
                    oBP.Addresses.City = getUFValue("IDH_CITY").ToString();//City 100
                    oBP.Addresses.ZipCode = getUFValue("IDH_PCODE").ToString();//State 3
                    oBP.Addresses.County = getUFValue("IDH_COUNTY").ToString();//PostCode 20
                    oBP.Addresses.State = getUFValue("IDH_STATE").ToString();//County 100
                    oBP.Addresses.Country = getUFValue("IDH_CONTRY").ToString();//Country 3
                    oBP.Addresses.UserFields.Fields.Item("U_AddrsCd").Value = Config.INSTANCE.GetAddressNextCode();
                    //oBP.ContactEmployees.Add();
                    oBP.ContactEmployees.Name = getUFValue("IDH_CNTPRS").ToString();//Contact Person 50
                    oBP.ContactEmployees.E_Mail = getUFValue("IDH_EMAIL").ToString();//Email 100
                    oBP.ContactEmployees.Phone1 = getUFValue("IDH_PHONE").ToString();//Phone 20
                    string sAddressCode= Config.INSTANCE.GetAddressNextCode();
                    oBP.Addresses.UserFields.Fields.Item("U_AddrsCd").Value = sAddressCode;
                    int iResult = oBP.Add();
                    if (iResult != 0) {
                        BubbleEvent = false;
                        string sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
                        //DataHandler.INSTANCE.doSystemError("Stocktransfer Error - " + sResult, "Error processing the Wharehouse to Wharehouse stock transfer.");
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Business Partner Add Error - " + sResult + " Error while adding business partner.", "ERSYBPAD",
                            new string[] { "Business Partner", sResult });
                    } else {
                        if (bManualCardCode)
                            _CardCode = oBP.CardCode;
                        else
                            _CardCode = DataHandler.INSTANCE.SBOCompany.GetNewObjectKey();//this line is yet to verify by using a db having BP code serice
                        doUpdateBaseDocs(sAddressCode);
                        string msg = Translation.getTranslatedWord("Business Partner successfully added") + ": " + _CardCode;
                        com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(msg, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                    }
                    //
                } catch (Exception Ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EREXGEN", null);
                    return false;
                }
            }
            return true;
        }
        private void doUpdateBaseDocs(string sAddressCode) {
            string sMessage = "";
            if (_EnqID != "")
              sMessage=  "Update Customer Code based on Enq ID:" + _EnqID;
            else
              sMessage=  "Update Customer Code based on WOQ ID:" + _WOQID;

            IDH_ENQUIRY objEnq = new IDH_ENQUIRY();
            objEnq.MustLoadChildren = false;
            int iret = objEnq.getData(IDH_ENQUIRY._CardName + " Like '" + getUFValue("IDH_CARDNM").ToString() + "' And IsNull(" + IDH_ENQUIRY._CardCode + ",'')=''", IDH_ENQUIRY._Code);
            if (iret > 0) {
                bool result;
                while (objEnq.next()) {
                    objEnq.U_CardCode = _CardCode;
                    objEnq.U_AddrssLN = sAddressCode;
                    result = objEnq.doProcessData(false, sMessage);
                }
            }
            IDH_WOQHD objWOQHd = new IDH_WOQHD();
            objWOQHd.MustLoadChildren = false;
            iret = objWOQHd.getData(IDH_WOQHD._CardNM + " Like '" + getUFValue("IDH_CARDNM").ToString() + "' And IsNull(" + IDH_WOQHD._CardCd+ ",'')=''", IDH_WOQHD._Code);
            if (iret > 0) {
                bool result;
                while (objWOQHd.next()) {
                    objWOQHd.U_CardCd = _CardCode;
                    objWOQHd.U_AddrssLN = sAddressCode;
                    result = objWOQHd.doProcessData(false, sMessage);
                }
            }
        }
        #endregion

        #region doFillCombos
        private void doFillCombos() {
            //FillCombos.FillCountry(Items.Item("IDH_CARDTY").Specific);
            SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)SBOForm.Items.Item("IDH_CARDTY").Specific;
            //string sDfltCountry = Config.INSTANCE.getParameterWithDefault("DFTCNTRY", "GB");
            oCombo.ValidValues.Add("C", "Customer");
            oCombo.ValidValues.Add("S", "Supplier");
            oCombo.ValidValues.Add("L", "Lead");
            string sBPType = Config.INSTANCE.getParameterWithDefault("DFEQBPTP", "C").ToString();
                if (sBPType == null || sBPType == string.Empty)
                sBPType = "C";
            oCombo.SelectExclusive(sBPType, SAPbouiCOM.BoSearchKey.psk_ByValue);
        }
        #endregion

        private bool doValidateBPbyCode(ref string sCardName) {
             
            sCardName = Config.INSTANCE.doGetBPName(getUFValue("IDH_CARDCD").ToString(),false);
            if (sCardName == string.Empty)
                return false;
            else
                return true;
        }
    }
}
