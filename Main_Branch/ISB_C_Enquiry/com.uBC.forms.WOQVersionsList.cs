using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.forms.oo;
using com.idh.bridge.data;
using IDHAddOns.idh.controls;
using com.uBC.utils;
using com.idh.controls;
using com.idh.bridge.utils;
using com.uBC.forms.fr3;
using com.idh.dbObjects.strct;
using com.idh.bridge.lookups;
//using com.idh.dbObjects.Base;
using com.idh.bridge;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry {
    public class WOQVersionsList : com.idh.forms.oo.Form {

        private DBOGrid moGrid;
        private IDH_WOQHVR moWOQVersions;
        public IDH_WOQHVR DBWOQVersions {
            get { return moWOQVersions; }
        }

        public WOQVersionsList(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
            doInitialize();
            doSetHandlers();
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_WOQVRSLST", sParMenu, iMenuPos, "Enq_WOQVersionsList.srf", false, true, false, "WOQ Versions", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            owForm.doEnableHistory(IDH_WOQHVR.TableName);
            return owForm;
        }

        protected  void doInitialize() {
            moWOQVersions= new IDH_WOQHVR(IDHForm, SBOForm);
            moWOQVersions.MustLoadChildren = false;
            moWOQVersions.AutoAddEmptyLine = false;
            
        }



        #region Properties
        private Int32 _WOQID = 0;
        public Int32 WOQID {
            get { return _WOQID; }
            set { _WOQID = value; }
        }

      
        #endregion

        #region FormOpenCreateFunctions
        /** 
         * This Function will be called by the controller to allow the class to last minute Form Layout adjustments before it gets displayed
         * All changes made in hete will be cached if this is a cached form, the method will not be called again once the form has benn cached.
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            base.doCompleteCreate(ref BubbleEvent);
            AutoManaged = false;
            Items.Item("1").AffectsFormMode = false;
        }

        public override void doBeforeLoadData() {
            try{
        
                moWOQVersions.SBOForm = SBOForm;
                SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_Ok;
            moGrid = new DBOGrid(IDHForm, SBOForm, "IDH_CONTA", "LINESGRID", moWOQVersions);
                //moGrid.doAddGridTable(new GridTable("OUSR","U","USERID",false,true),false);
                //moGrid.setRequiredFilter(IDH_WOQHVR._VerCrBy +"=U.USERID" );
                moGrid.doSetDeleteActive(false);
                moGrid.doAddEditLine(false);
                
            doSetGridHandlers();
            doGridLayout();
            moGrid.doApplyRules();


            Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
            base.doBeforeLoadData();
} catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }            
        }
        protected void doGridLayout() {
            
            moGrid.doAddListField(IDH_WOQHVR._Code, "Code", false, 0, null, null);
            moGrid.doAddListField(IDH_WOQHVR._Name, "Name", false, 0, null, null);
            moGrid.doAddListField(IDH_WOQHVR._BasWOQID, "WOQ ID", false, -1, null, null);
            moGrid.doAddListField(IDH_WOQHVR._VerDate, "Version Date", false, -1, "DATE", null);
            //moGrid.doAddListField(IDH_WOQHVR._VerDateTm, "Version Date", false, -1, "DATE", null);
            //moGrid.doAddListField("U.U_NAME", "User", false, -1, null, null);
            moGrid.doAddListField(IDH_WOQHVR._Version, "Version", false, -1, null, null);

            //moGrid.doAddListField("U_EnqID", "Enquiry ID", false, 0, null, null);
            //moGrid.doAddListField("U_WOQID", "WOQ ID", false, 0, null, null);
            //moGrid.doAddListField("U_Answer", "Answer", true, 300, null, null);
            //moGrid.doAddListField("U_Sort", "Sort", false , 0, null, null);
            moGrid.doAutoListFields(false);
            moGrid.doSynchDBandGridFieldPos();
        }
        /** 
         * Do the final form steps to show after loaddata
         */
        public override void doFinalizeShow() {
            base.doFinalizeShow();
        }
        /* 
         * Load the Form Data
         */
        public override void doLoadData() {
            //string sSql = null;
            //sSql = "Select T0.* From ( " +
            //        " Select b.code,b.Name,a.Code as QID,isnull(b.U_Question, a.U_Question) as Question,b.U_EnqID,b.U_WOQID,b.U_Answer,a.U_Sort from  " +
            //        " [@IDH_ENQSMS] a with(nolock), [@IDH_ENQUS] b where a.Code=b.U_QsID and (b.U_WOQID ='" + _WOQID + "' or b.U_EnqID='" + _EnqID + "') " +
            //        " UNION ALL " +
            //        " Select null code,null Name,a.Code as QID,a.U_Question as Question,null U_EnqID,null U_WOQID,null U_Answer,a.U_Sort from " + 
            //        " [@IDH_ENQSMS] a with(nolock) where not a.Code in (Select b.U_QsID from [@IDH_ENQUS] b where (b.U_WOQID ='"+ _WOQID +"' or b.U_EnqID='"+ _EnqID +"'))  " +
            //        " )T0 Order by U_Sort";

            //Dim oRecSet As SAPbobsCOM.Recordset = PARENT.goDB.doSelectQuery(sSql)
           // UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(SBOForm, "LINESGRID");
            moWOQVersions.getByWOQNumber(_WOQID.ToString());
            moGrid.doApplyRules();
        }

        
        #endregion

        #region Events
        protected  void doSetHandlers() {
            //Handler_Button_Add = doSaveQuestions;
           // Handler_Button_Update = doSaveQuestions;
            //   base.doSetHandlers();
            //addHandler_ITEM_PRESSED("1", doCreateBP);

        }

        protected  void doSetGridHandlers() {
            //base.doSetGridHandlers();

            moGrid.Handler_GRID_DOUBLE_CLICK = new IDHGrid.ev_GRID_EVENTS(doRowDoubleClickEvent);
        }
        #endregion
        #region ItemEventHandlers
        public bool doRowDoubleClickEvent(ref IDHAddOns.idh.events.Base pVal) {
            try {
                if (!pVal.BeforeAction) {
                    string scode = moWOQVersions.Code;
                    com.isb.forms.Enquiry.WOQuoteVersions oOOForm = new com.isb.forms.Enquiry.WOQuoteVersions(null, SBOForm.UniqueID, SBOForm);
                    oOOForm.WOQVersionID = scode;
                    oOOForm.bLoadWOQ = true;
                    oOOForm.doOpenForm(oOOForm.IDHForm.gsType);
                }
                return true;
            } catch (Exception Ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EREXGEN", null);
                return false;
            }
        }
         
        #endregion

    }
}
