using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.forms.oo;
using com.idh.bridge.data;
using com.idh.dbObjects.User;
using IDHAddOns.idh.controls;
using com.uBC.utils;
using com.idh.controls;
using com.idh.bridge.utils;
using com.uBC.forms.fr3;
using com.idh.dbObjects.strct;
using com.idh.bridge.lookups;
//using com.idh.dbObjects.Base;
using com.idh.bridge;

namespace com.isb.forms.Enquiry {
    public class EnqQuestions2 : com.idh.forms.oo.Form {

        private DBOGrid moGrid;
        private IDH_ENQUS moEnquiryQuestions;
        public IDH_ENQUS DBEnquiryQuestions {
            get { return moEnquiryQuestions; }
        }
                
        public EnqQuestions2(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
            doInitialize();
            doSetHandlers();
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_ENQQS", sParMenu, iMenuPos, "Enq_Questions.srf", false, true, false, "Enquiry Questioner", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        protected  void doInitialize() {
            moEnquiryQuestions = new IDH_ENQUS(IDHForm, SBOForm);
            moEnquiryQuestions.MustLoadChildren = false;

            moEnquiryQuestions.AutoAddEmptyLine = false;
            
        }



        #region Properties
        private Int32 _EnqID = 0;
        public Int32 EnqID {
            get { return _EnqID; }
            set { _EnqID = value; }
        }

        private Int32 _WOQID = 0;
        public Int32 WOQID {
            get { return _WOQID; }
            set { _WOQID = value; }
        }

      
        #endregion

        #region FormOpenCreateFunctions
        /** 
         * This Function will be called by the controller to allow the class to last minute Form Layout adjustments before it gets displayed
         * All changes made in hete will be cached if this is a cached form, the method will not be called again once the form has benn cached.
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            base.doCompleteCreate(ref BubbleEvent);
            AutoManaged = false;
            Items.Item("1").AffectsFormMode = false;
        }

        public override void doBeforeLoadData() {
            try{
            SupportedModes = (int)SAPbouiCOM.BoFormMode.fm_OK_MODE;
            moGrid = new DBOGrid(IDHForm, SBOForm, "LINESGRID", moEnquiryQuestions);
            //if (getSharedData("IDH_OPNENQ") == null || getSharedData("IDH_OPNENQ").ToString() != "Y") {
                moGrid.doSetDeleteActive(false);
                moGrid.doAddEditLine(false);
            //}
            //  moEnquiry.EnquiryItems.AutoAddEmptyLine = false;
            doSetGridHandlers();
            doGridLayout();
            moGrid.doApplyRules();


            Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
            base.doBeforeLoadData();
} catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }            
        }
        protected void doGridLayout() {
            
            moGrid.doAddListField("Code", "ID", false, 0, null, null);
            moGrid.doAddListField("Name", "Name", false, 0, null, null);
            moGrid.doAddListField("QID", "Name", false, 0, null, null);
            moGrid.doAddListField("Question", "Question", true, 350, null, null);

            moGrid.doAddListField("U_EnqID", "Enquiry ID", false, 0, null, null);
            moGrid.doAddListField("U_WOQID", "WOQ ID", false, 0, null, null);
            moGrid.doAddListField("U_Answer", "Answer", true, 300, null, null);
            moGrid.doAddListField("U_Sort", "Sort", false , 0, null, null);
            moGrid.doSynchDBandGridFieldPos();
        }
        /** 
         * Do the final form steps to show after loaddata
         */
        public override void doFinalizeShow() {
            base.doFinalizeShow();
        }
        /* 
         * Load the Form Data
         */
        public override void doLoadData() {
            string sSql = null;
            sSql = "Select T0.* From ( " +
                    " Select b.code,b.Name,a.Code as QID,isnull(b.U_Question, a.U_Question) as Question,b.U_EnqID,b.U_WOQID,b.U_Answer,a.U_Sort from  " +
                    " [@IDH_ENQSMS] a with(nolock), [@IDH_ENQUS] b where a.Code=b.U_QsID and (b.U_WOQID ='" + _WOQID + "' or b.U_EnqID='" + _EnqID + "') " +
                    " UNION ALL " +
                    " Select null code,null Name,a.Code as QID,a.U_Question as Question,null U_EnqID,null U_WOQID,null U_Answer,a.U_Sort from " + 
                    " [@IDH_ENQSMS] a with(nolock) where not a.Code in (Select b.U_QsID from [@IDH_ENQUS] b where (b.U_WOQID ='"+ _WOQID +"' or b.U_EnqID='"+ _EnqID +"'))  " +
                    " )T0 Order by U_Sort";

            //Dim oRecSet As SAPbobsCOM.Recordset = PARENT.goDB.doSelectQuery(sSql)
           // UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(SBOForm, "LINESGRID");
            moGrid.getSBOGrid().DataTable.ExecuteQuery(sSql);
            moGrid.doApplyRules();
        }

        
        #endregion

        #region Events
        protected  void doSetHandlers() {
            Handler_Button_Add = doSaveQuestions;
            Handler_Button_Update = doSaveQuestions;
            //   base.doSetHandlers();
            //addHandler_ITEM_PRESSED("1", doCreateBP);

        }

        protected  void doSetGridHandlers() {
            //base.doSetGridHandlers();

            //moAdminGrid.Handler_GRID_DOUBLE_CLICK = new IDHGrid.ev_GRID_EVENTS(doRowDoubleClickEvent);
        }
        #endregion
        #region ItemEventHandlers
        public bool doSaveQuestions(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            try {
                string Code, Name, Answer, QID;
                Int32 EnqID;
                moEnquiryQuestions.doBookmark();
                for (int i = 0; i <= moEnquiryQuestions.Count - 1; i++) {
                    moEnquiryQuestions.gotoRow(i);
                    Code = moEnquiryQuestions.Code;
                    Name= moEnquiryQuestions.Name;
                    QID = moEnquiryQuestions.U_QsID;
                    Answer= moEnquiryQuestions.U_Answer;
                    EnqID = moEnquiryQuestions.U_EnqID;
                    EnqID = moEnquiryQuestions.U_EnqID;

                    //moEnquiryQuestions.doProcessData();
                }
                moEnquiryQuestions.doRecallBookmark();
                return true;
            } catch (Exception Ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EREXGEN", null);
                return false;
            }
        }
        //public bool doCreateBP(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (pVal.BeforeAction) {
        //        string sCardName = "";
        //        if (bManualCardCode == true && getUFValue("IDH_CARDCD").ToString() == string.Empty) {
        //            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Business Partner Code missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Business Partner Code") });
        //            BubbleEvent = false;
        //            return true;
        //        }
        //        if (bManualCardCode == true && doValidateBPbyCode(ref sCardName) == true) {
        //            com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage(("ERPKEXIS"), new string[] { getUFValue("IDH_CARDCD").ToString() });
        //            BubbleEvent = false;
        //            return true;
        //        }
        //        try {
        //            SAPbobsCOM.BusinessPartners oBP = (SAPbobsCOM.BusinessPartners)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
        //            if (bManualCardCode)
        //                oBP.CardCode = getUFValue("IDH_CARDCD").ToString();

        //            oBP.CardName = getUFValue("IDH_CARDNM").ToString();
        //            string BPTye = getUFValue("IDH_CARDTY").ToString();
        //            if (BPTye == "C") {
        //                oBP.CardType = SAPbobsCOM.BoCardTypes.cCustomer;
        //            } else if (BPTye == "S") {
        //                oBP.CardType = SAPbobsCOM.BoCardTypes.cSupplier;
        //            } else if (BPTye == "L") {
        //                oBP.CardType = SAPbobsCOM.BoCardTypes.cLid;
        //            } else
        //                oBP.CardType = SAPbobsCOM.BoCardTypes.cCustomer;
        //            oBP.Valid = SAPbobsCOM.BoYesNoEnum.tYES;

        //            //   oBP.Addresses.Add();
        //            oBP.Addresses.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo;
        //            oBP.Addresses.AddressName = getUFValue("IDH_ADRID").ToString();//Address ID 50
        //            oBP.Addresses.Street = getUFValue("IDH_STREET").ToString();//Street 100
        //            oBP.Addresses.Block = getUFValue("IDH_BLOCK").ToString();//Block 100
        //            oBP.Addresses.City = getUFValue("IDH_CITY").ToString();//City 100
        //            oBP.Addresses.ZipCode = getUFValue("IDH_PCODE").ToString();//State 3
        //            oBP.Addresses.County = getUFValue("IDH_COUNTY").ToString();//PostCode 20
        //            oBP.Addresses.State = getUFValue("IDH_STATE").ToString();//County 100
        //            oBP.Addresses.Country = getUFValue("IDH_CONTRY").ToString();//Country 3

        //            //oBP.ContactEmployees.Add();
        //            oBP.ContactEmployees.Name = getUFValue("IDH_CNTPRS").ToString();//Contact Person 50
        //            oBP.ContactEmployees.E_Mail = getUFValue("IDH_EMAIL").ToString();//Email 100
        //            oBP.ContactEmployees.Phone1 = getUFValue("IDH_PHONE").ToString();//Phone 20
        //            int iResult = oBP.Add();
        //            if (iResult != 0) {
        //                BubbleEvent = false;
        //                string sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
        //                //DataHandler.INSTANCE.doSystemError("Stocktransfer Error - " + sResult, "Error processing the Wharehouse to Wharehouse stock transfer.");
        //                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Business Partner Add Error - " + sResult + " Error while adding business partner.", "ERSYBPAD",
        //                    new string[] { "Business Partner", sResult });
        //            } else {
        //                if (bManualCardCode)
        //                    _CardCode = oBP.CardCode;
        //                else
        //                    _CardCode = DataHandler.INSTANCE.SBOCompany.GetNewObjectKey();//this line is yet to verify by using a db having BP code serice
        //                doUpdateBaseDocs();
        //                string msg = Translation.getTranslatedWord("Business Partner successfully added") + ": " + _CardCode;
        //                com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(msg, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
        //            }
        //            //
        //        } catch (Exception Ex) {
        //            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EREXGEN", null);
        //            return false;
        //        }
        //    }
        //    return true;
        //}
        //private void doUpdateBaseDocs() {
        //    string sMessage = "";
        //    if (_EnqID != "")
        //      sMessage=  "Update Customer Code based on Enq ID:" + _EnqID;
        //    else
        //      sMessage=  "Update Customer Code based on WOQ ID:" + _WOQID;

        //    IDH_ENQUIRY objEnq = new IDH_ENQUIRY();
        //    objEnq.MustLoadChildren = false;
        //    int iret = objEnq.getData(IDH_ENQUIRY._CardName + " Like '" + getUFValue("IDH_CARDNM").ToString() + "' And IsNull(" + IDH_ENQUIRY._CardCode + ",'')=''", IDH_ENQUIRY._Code);
        //    if (iret > 0) {
        //        bool result;
        //        while (objEnq.next()) {
        //            objEnq.U_CardCode = _CardCode;
        //            result = objEnq.doProcessData(false, sMessage);
        //        }
        //    }
        //    IDH_WOQHD objWOQHd = new IDH_WOQHD();
        //    objWOQHd.MustLoadChildren = false;
        //    iret = objWOQHd.getData(IDH_WOQHD._CardName + " Like '" + getUFValue("IDH_CARDNM").ToString() + "' And IsNull(" + IDH_WOQHD._CardCode + ",'')=''", IDH_WOQHD._Code);
        //    if (iret > 0) {
        //        bool result;
        //        while (objWOQHd.next()) {
        //            objWOQHd.U_CardCode = _CardCode;
        //            result = objWOQHd.doProcessData(false, sMessage);
        //        }
        //    }
        //}
        #endregion

    }
}
