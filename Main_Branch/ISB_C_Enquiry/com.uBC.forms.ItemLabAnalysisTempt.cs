﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using IDHAddOns.idh.controls;
using com.idh.bridge.data;
using com.idh.dbObjects; 
using com.idh.dbObjects.numbers;
using com.idh.bridge;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry {
    public class ItemLabAnalysisTempt : IDHAddOns.idh.forms.Base {

        #region Initialization

        public ItemLabAnalysisTempt(IDHAddOns.idh.addon.Base oParent)
            : base(oParent, "IDHITMLBAN", 0, "Enq_LabTaskAnalysis.srf", true, true, false, "Lab Analysis Template For Item Master") {
        }
        protected override void doSetEventFilters() {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE);

        }
        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            try {
                oForm.Title = gsTitle;
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;

                //Report Grid 
                SAPbouiCOM.Item oItem;
                oItem = oForm.Items.Item("IDH_RECLTA");
                FilterGrid oANLGRID = new FilterGrid(this, oForm, "ANLGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane, true);
                oANLGRID.getSBOItem().AffectsFormMode = false;
                oANLGRID.AddEditLine = false;
                
                doSetAffectsMode(oForm, false);
                base.doCompleteCreate(ref oForm,ref BubbleEvent);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
                BubbleEvent = false;
            }
        }

        #endregion

        #region LoadingDate

        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {
            doAddUF(oForm, "ItemCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50, false, false);
            if (getHasSharedData(oForm)) {
                string sTaskAnalysisCodes = getParentSharedData(oForm, "ITEM_CODE").ToString();
                setUFValue(oForm, "ItemCode", sTaskAnalysisCodes);
            }
            doManageAnalysisGrid(oForm);
           
           // doSetFilterFields(oForm);
        }

        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            FilterGrid oAnlyGridN = (FilterGrid)FilterGrid.getInstance(oForm, "ANLGRID");
            oAnlyGridN.AddEditLine = false;
            oAnlyGridN.doReloadData("", false, false);
            oAnlyGridN.doApplyRules();

            string sItemCode = getUFValue(oForm, "ItemCode", false).ToString();
            
            if (sItemCode.Length > 0) {
                IDH_ITMLANL objLabItemTemp = new IDH_ITMLANL();
                objLabItemTemp.UnLockTable = true;
                int iRet = 0;
                for (int i = 0; i < oAnlyGridN.getRowCount(); i++) {
                    iRet = objLabItemTemp.getData(IDH_ITMLANL._ItemCode + "=\'" + sItemCode + "\' AND " + IDH_ITMLANL._AnalysisCd + "=\'" + oAnlyGridN.doGetFieldValue("LTA.U_AnalysisCd", i).ToString() + "\'", "");
                   if (iRet>0)
                        oAnlyGridN.doSetFieldValue("Select", i, "Y");
                }
            }
            oAnlyGridN.getSBOItem().AffectsFormMode = false;
        }

        public override void doFinalizeShow(SAPbouiCOM.Form oForm) {
            base.doFinalizeShow(oForm);
        }

        protected void doSetAnalysisGridListFields(IDHAddOns.idh.controls.FilterGrid oAnlyGridN) {
            //LTI.U_LabTskCd
            oAnlyGridN.doAddListField("'Select'", "Select", true, -1, "CHECKBOX", null);
            oAnlyGridN.doAddListField("LTA.Code", "LTA Code", false, 0, null, null);
            oAnlyGridN.doAddListField("LTA.U_AnalysisCd", "Analysis Code", false, -1, null, null);
            oAnlyGridN.doAddListField("LTA.U_AnalysisDesc", "Analysis Description", false, -1, null, null);
        }

        #endregion

        #region UnloadingData

        public override void doClose() {
        }

        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCloseForm(oForm, ref BubbleEvent);
            FilterGrid.doRemoveGrid(oForm, "ANLGRID");
        }

        #endregion

        #region EventHandlers

        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == false) {
                doSaveData(oForm);

                //doPrepareModalData(oForm);
                //doReturnFromModalShared(oForm, true);
            
            }

        }

        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            //if (pVal.BeforeAction) {
            //    if (pVal.ItemUID == "ANLGRID") {
            //        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            //            oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
            //    }
            //}
            return base.doItemEvent(oForm,ref pVal,ref BubbleEvent);
        }
        //override dopre
        #endregion

        #region CustomFunctions

        protected void doManageAnalysisGrid(SAPbouiCOM.Form oForm) {
            FilterGrid oAnlyGridN = (FilterGrid)FilterGrid.getInstance(oForm, "ANLGRID");
            if (oAnlyGridN == null)
                oAnlyGridN = new FilterGrid(this, oForm, "ANLGRID", true);

            oAnlyGridN.doAddGridTable(new GridTable("@IDH_LABANLYS", "LTA", "U_AnalysisCd", true), true);
            oAnlyGridN.setOrderValue("LTA.U_AnalysisCd");
            oAnlyGridN.doSetDoCount(true);
            doSetAnalysisGridListFields(oAnlyGridN);
            oAnlyGridN.doApplyRules();

            oAnlyGridN.getSBOItem().AffectsFormMode = false;
        }

        public void doSaveData(SAPbouiCOM.Form oForm) {
            FilterGrid oAnlyGridN = (FilterGrid)FilterGrid.getInstance(oForm, "ANLGRID");
           string sItemCode = getUFValue(oForm, "ItemCode", false).ToString();
            int iRet;
            IDH_ITMLANL obj;
            string sSelect = "";
            for (int i = 0; i < oAnlyGridN.getRowCount(); i++) {
                if (oAnlyGridN.doGetFieldValue("LTA.U_AnalysisCd", i).ToString() == string.Empty)
                    continue;
                obj=new IDH_ITMLANL();
                obj.UnLockTable=true;
                sSelect = oAnlyGridN.doGetFieldValue("Select", i).ToString();
                
              iRet=  obj.getData(IDH_ITMLANL._ItemCode + "=\'" + sItemCode + "\' AND "+ IDH_ITMLANL._AnalysisCd+ "=\'" +  oAnlyGridN.doGetFieldValue("LTA.U_AnalysisCd", i).ToString() +  "\'", "");
              if (sSelect.Equals("Y") && iRet == 0) {
                  obj.doAddEmptyRow();
                  NumbersPair oNextNum = obj.getNewKey();
                  obj.Code = oNextNum.CodeCode;
                  obj.Name = oNextNum.NameCode;
                  obj.U_AnalysisCd = oAnlyGridN.doGetFieldValue("LTA.U_AnalysisCd", i).ToString();
                  obj.U_ItemCode = sItemCode;
                  obj.doProcessData();

              } else if (!sSelect.Equals("Y") && iRet > 0) {
                  obj.doMarkForDelete(0);
                  obj.doProcessData();
                 // obj.doDeleteData();

              }
               // if (oAnlyGridN.doGetFieldValue("Select", i).ToString().Equals("Y"))
                   // obj.
                    //sTaskAnalysisCodes = sTaskAnalysisCodes + "," + oAnlyGridN.doGetFieldValue("LTA.U_AnalysisCd", i).ToString();
            }
           // sTaskAnalysisCodes = sTaskAnalysisCodes.Trim(',');
            //SAPbouiCOM.Form oParentForm = goParent.doGetParentForm(oForm.UniqueID);
            doClearParentSharedData(oForm);
         //   setParentSharedData(oForm, "NEWTSKANLCD", sTaskAnalysisCodes);

        }

        #endregion
         
    }
}