﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using IDHAddOns.idh.controls;
using com.idh.bridge.data;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.uBC.utils;
using com.idh.controls;
using SAPbouiCOM;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry {
    public class WOQuoteVersions : com.idh.forms.oo.Form {
        
        private IDH_WOQHVR moWOQ;
        public IDH_WOQHVR DBWOQ {
            get { return moWOQ; }
        }
        private DBOGrid moGrid;
        private string _WOQVersionCode = "-1";
        private string _EnqID = "";
        private bool mbAddUpdateOk = false;
        private bool mbLoadWOQById = false;
        private bool mbLoadReadOnly = false;
        private bool mbENQRESULT = false;
        protected string msOrdCat = "Waste Order";//com.idh.bridge.lookups.FixedValues.getStatusOrder();
        private idh.dbObjects.User.IDH_FORMSET moFormSettings;
        
        public WOQuoteVersions(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
            doInitialize();
            doSetHandlers();
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuePos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDHWOQVVIW", sParMenu, iMenuePos, "Enq_WorkOrderQuote_Version.srf", true, true, false, "Work Order Quote", 
                IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            owForm.doEnableHistory(IDH_WOQHVR.TableName);
            return owForm;
        }

        static string msMainTable = IDH_WOQHVR.TableName;
        static string msGridTable = IDH_WOQITVR.TableName;

 protected void doInitialize() {
            moWOQ= new IDH_WOQHVR(IDHForm, SBOForm);
            moWOQ.MustLoadChildren = true;

            moWOQ.AutoAddEmptyLine = false;
            moWOQ.WOQItems.AutoAddEmptyLine = false;
       
 }


        #region Properties

        public string EnquiryID {
            get { return _EnqID; }
            set { _EnqID = value; }
        }
        public string WOQVersionID {
            get { return _WOQVersionCode; }
            set { _WOQVersionCode = value; }
        }

        public bool bLoadWOQ {
            get { return mbLoadWOQById; }
            set { mbLoadWOQById = value; }
        }
        public bool bAddUpdateOk {
            get { return mbAddUpdateOk; }
            set { mbAddUpdateOk = value; }
        }
        public bool bLoadReadOnly {
            get { return mbLoadReadOnly; }
            set { mbLoadReadOnly = value; }
        }
        public bool ENQRESULT {
            get { return mbENQRESULT; }
            set { mbENQRESULT = value; }
        }
        #endregion      
        
        
        #region Override methods
        public override void doCompleteCreate(ref bool BubbleEvent) {
            {
                try {

                    DataBrowser.BrowseBy = "IDH_DOCNUM";
                    doAddUF("IDH_LBLTOT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 30, false, false);

                    SAPbouiCOM.EditText otxt = (SAPbouiCOM.EditText)SBOForm.Items.Item("IDH_DOCNUM").Specific;
                    otxt.DataBind.SetBound (true,msMainTable, "U_BasWOQID");
                    if (base.IDHForm.goParent.doCheckModal(UniqueID)) {
                        SBOForm.EnableMenu("1281", false);
                        SBOForm.EnableMenu("1282", false);
                        SBOForm.EnableMenu("1290", false);
                        SBOForm.EnableMenu("1288", false);
                        SBOForm.EnableMenu("1289", false);
                        SBOForm.EnableMenu("1291", false);
                    }
                } catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
                    BubbleEvent = false;
                }
            }
        }

        public override void doCloseForm(ref bool BubbleEvent) {
            DBOGrid.doRemoveGrid(SBOForm, "LINESGRID");
            base.doCloseForm(ref BubbleEvent);
        }
       /** 
         * Do the final form steps to show before loaddata
         */
        public override void doBeforeLoadData() {

            try {
                moWOQ.SBOForm = SBOForm;
                moWOQ.doAddEmptyRow(true, false);

                Title = IDHForm.gsTitle + " (" + moWOQ.U_Version +")";
                moGrid = new DBOGrid(IDHForm, SBOForm, "LINESGRID", moWOQ.WOQItems);
                doTheGridLayout();
                
                moGrid.doSetDeleteActive(true);
                moGrid.doAddEditLine(true);
            

                setUFValue("IDH_LBLTOT", Translation.getTranslatedWord("Totals"));
                doSetGridHandlers();
                

                moGrid.doApplyRules();
                doFillCombos();

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }
        }

        public override void doFinalizeShow() {
            try {
                EnableItems(false, new string[] { "IDH_LBLTOT", "IDH_LPRICE", "IDH_COST", "IDH_LPPnL", "IDH_LPMRGN", "IDH_CHARGE", "IDH_COST2", "IDH_CHGPnL", "IDH_CHMRGN",
                "IDH_VERSN","IDH_CHARGE","IDH_CHGVAT","IDH_TCHARG"});
                Items.Item("IDH_DOCNUM").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable,
          (int)SAPbouiCOM.BoFormMode.fm_ADD_MODE + (int)SAPbouiCOM.BoFormMode.fm_EDIT_MODE +
          (int)SAPbouiCOM.BoFormMode.fm_OK_MODE + (int)SAPbouiCOM.BoFormMode.fm_PRINT_MODE +
          (int)SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + (int)SAPbouiCOM.BoFormMode.fm_VIEW_MODE,
          SAPbouiCOM.BoModeVisualBehavior.mvb_False);
                Items.Item("IDH_DOCNUM").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, (int)SAPbouiCOM.BoFormMode.fm_FIND_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_True);

                Items.Item("IDH_CPYENQ").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible,
              (int)SAPbouiCOM.BoFormMode.fm_ADD_MODE + (int)SAPbouiCOM.BoFormMode.fm_EDIT_MODE +
              (int)SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + (int)SAPbouiCOM.BoFormMode.fm_OK_MODE,
              SAPbouiCOM.BoModeVisualBehavior.mvb_True);
                Items.Item("IDH_CPYENQ").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, (int)SAPbouiCOM.BoFormMode.fm_FIND_MODE + (int)SAPbouiCOM.BoFormMode.fm_VIEW_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False);

                Items.Item("IDH_DOCS").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible,
(int)SAPbouiCOM.BoFormMode.fm_ADD_MODE + (int)SAPbouiCOM.BoFormMode.fm_EDIT_MODE +
(int)SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + (int)SAPbouiCOM.BoFormMode.fm_OK_MODE,
SAPbouiCOM.BoModeVisualBehavior.mvb_True);
                Items.Item("IDH_DOCS").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, (int)SAPbouiCOM.BoFormMode.fm_FIND_MODE + (int)SAPbouiCOM.BoFormMode.fm_VIEW_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False);


                if (mbLoadWOQById) {
                    Freeze(true);

                    if (_WOQVersionCode != "-1") {
                        if (moWOQ.getByKey(_WOQVersionCode)) {
                            FillCombos.FillState(Items.Item("IDH_STATE").Specific, moWOQ.U_Country, null);

                            //doSwitchToEdit();
                            moGrid.doApplyRules();
                            doFillGridCombos();
                           // if (bLoadReadOnly)
                                Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE;
                                Title = IDHForm.gsTitle + " (" + moWOQ.U_Version + ")";
                            //else
                              //  HandleFormStatus();
                        }
                    } else if (_EnqID != "") {
                        //Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE;
                        //IDH_ENQUIRY oEnquiry = new IDH_ENQUIRY();
                        //oEnquiry.MustLoadChildren = true;
                        //oEnquiry.getByKey(_EnqID);
                        //int iWOQRow = 0;
                        //string sPrvEnqRowCode = "", sPrvWOQRowCode = "";

                        //doSetHeaderByEnquiry(oEnquiry);
                        //for (int iEnqRow = 0; iEnqRow <= oEnquiry.EnquiryItems.Count - 1; iEnqRow++) {
                        //    oEnquiry.EnquiryItems.gotoRow(iEnqRow);
                        //    if (oEnquiry.EnquiryItems.U_Status != "1")
                        //        continue;
                        //    moWOQ.WOQItems.DoBlockUpdateTrigger = true;
                        //    moWOQ.WOQItems.U_AdtnlItm = oEnquiry.EnquiryItems.U_AddItm;
                        //    moWOQ.WOQItems.U_EnqID = oEnquiry.EnquiryItems.U_EnqId;
                        //    moWOQ.WOQItems.U_EnqLID = oEnquiry.EnquiryItems.Code;
                        //    moWOQ.WOQItems.U_ExpLdWgt = oEnquiry.EnquiryItems.U_EstQty;
                        //    moWOQ.WOQItems.U_WasCd = oEnquiry.EnquiryItems.U_ItemCode;
                        //    moWOQ.WOQItems.U_WasFDsc= oEnquiry.EnquiryItems.U_ItemName;
                        //    moWOQ.WOQItems.U_ItmGrp = moWOQ.U_ItemGrp;

                        //    if (oEnquiry.EnquiryItems.U_AddItm == "A" && oEnquiry.EnquiryItems.U_PCode != "" && iWOQRow > 0) {
                        //        if (sPrvEnqRowCode == oEnquiry.EnquiryItems.U_PCode)
                        //            moWOQ.WOQItems.U_PLineID = sPrvWOQRowCode;

                        //    } else {
                        //        moWOQ.WOQItems.U_PLineID = "";
                        //        sPrvEnqRowCode = oEnquiry.EnquiryItems.Code;
                        //        sPrvWOQRowCode = moWOQ.WOQItems.Code;
                        //    }
                        //    moWOQ.WOQItems.U_Status = "1";
                        //    moWOQ.WOQItems.U_UOM = oEnquiry.EnquiryItems.U_UOM;
                        //    moWOQ.WOQItems.U_WstGpCd = oEnquiry.EnquiryItems.U_WstGpCd;
                        //    moWOQ.WOQItems.U_WstGpNm = oEnquiry.EnquiryItems.U_WstGpNm;
                        //    moWOQ.WOQItems.DoBlockUpdateTrigger = false;
                        //    moWOQ.WOQItems.doGetAllPrices();

                        //    moWOQ.WOQItems.doAddEmptyRow(true, true, false);
                        //    iWOQRow++;
                        //}
                        //moWOQ.WOQItems.doCalculateAllRowsTotal(false);
                    }

                } else {
                    Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE;
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
            } finally {
                Freeze(false);
            }
            base.doFinalizeShow();
             
        }

        public override void doLoadData() {
            doFillGridCombos();
        }

       
        #endregion
        #region General Methods
        //private void doSetHeaderByEnquiry(IDH_ENQUIRY oEnquiry) {
            
        //    moWOQ.DoBlockUpdateTrigger = true;
        //    moWOQ.U_Address = oEnquiry.U_Address;
        //    moWOQ.U_AddrssLN = oEnquiry.U_AddrssLN;
        //    //moWOQ.U_AttendUser=idh.bridge.DataHandler.INSTANCE.User;
        //    moWOQ.U_Block=oEnquiry.U_Block;
        //    moWOQ.U_Branch=Convert.ToInt16( Config.INSTANCE.doGetBranch(idh.bridge.DataHandler.INSTANCE.User));//GetUserBranch GetBranch Get User Branch GetloginUserBranch
        //    moWOQ.U_CardCd=oEnquiry.U_CardCode;
        //    moWOQ.U_CardNM=oEnquiry.U_CardName;
        //    moWOQ.U_ChgPrice=0;
        //    moWOQ.U_City=oEnquiry.U_City;
        //    moWOQ.U_ClgCode=Convert.ToInt16(oEnquiry.U_ClgCode.ToString());//Activity id
        //    moWOQ.U_Contact=oEnquiry.U_CName;
        //    moWOQ.U_Cost=0;
        //    moWOQ.U_Country=oEnquiry.U_Country;
        //    moWOQ.U_County=oEnquiry.U_County;
        //    moWOQ.U_CPGrossM=0;
        //    moWOQ.U_CPProfit=0;
        //    moWOQ.U_E_Mail=oEnquiry.U_E_Mail;
        //    moWOQ.U_LPGrossM=0;
        //    moWOQ.U_LPProfit=0;
        //    moWOQ.U_LstPrice=0;
        //    moWOQ.U_Notes=oEnquiry.U_Notes;
        //    moWOQ.U_Phone1=oEnquiry.U_Phone1;
        //    moWOQ.U_SpInst=oEnquiry.U_Desc;
        //    moWOQ.U_BDate= DateTime.Now;// idh.utils.Dates.doDateToSBODateStr(DateTime.Now);
        //    moWOQ.U_RSDate=DateTime.Now;//idh.utils.Dates.doDateToSBODateStr(DateTime.Now);
        //    moWOQ.U_Source=1;
        //    moWOQ.U_State="";
        //    moWOQ.U_Status = ((int)IDH_WOQHVR.en_WOQSTATUS.New).ToString();// "1";
        //    moWOQ.U_Street=oEnquiry.U_Street;
        //    string UserID = com.idh.bridge.lookups.Config.INSTANCE.doGetUserId(idh.bridge.DataHandler.INSTANCE.User);

        //    moWOQ.U_User = Convert.ToInt16( UserID);
        //        //idh.bridge.DataHandler.INSTANCE.SBOCompany.UserSignature;// idh.bridge.DataHandler.INSTANCE.User;
        //    moWOQ.U_Version=1;
        //    moWOQ.U_ZpCd=oEnquiry.U_ZipCode;
        //    moWOQ.DoBlockUpdateTrigger = false;
        //    EnableItem(false, "IDH_DOCNUM");
        //}

        private void doSetHandlers() {
            //Handler_Button_Add = doAddUpdateEvent;
            //Handler_Button_Update = doAddUpdateEvent;
            //Handler_Button_Find = doFindEvent;
            //Handler_Button_Ok = doHandleOKEvent;
            //Handler_FORM_DATA_LOAD = doHandleFormDataLoad;
            //addHandler_ITEM_PRESSED("2", doCancelButton);
            //addHandler_CLICK("IDH_VERSN", doShowVersionList);
            
            //addHandler_ITEM_PRESSED("IDH_SNDAPR", doHandleApprovalButton);
            //addHandler_ITEM_PRESSED("IDH_CRTWO", doHandleCreateWO);
            //addHandler_ITEM_PRESSED("IDH_FINDAD", doFindAddress);
            //addHandler_ITEM_PRESSED("IDH_CRETBP", doCreateBP);
            //addHandler_ITEM_PRESSED("IDH_CPYENQ", doCopyFromENQ);
            //addHandler_ITEM_PRESSED("IDH_ADRCFL", doSearchBPAddressCFL);
            //addHandler_VALIDATE("IDH_CardNM", dovalidateBPEvent);
            //addHandler_VALIDATE("IDH_ADR1", dovalidateFieldsForPriceChange);
            //addHandler_VALIDATE("IDH_PCODE", dovalidateFieldsForPriceChange);
            //addHandler_VALIDATE("IDH_CardCD", dovalidateFieldsForPriceChange);
            //addHandler_VALIDATE("IDH_BRANCH", dovalidateFieldsForPriceChange);
            ////addHandler_VALIDATE("IDH_CardCD", dovalidateFieldsForPriceChange);
            //addHandler_COMBO_SELECT("IDH_BRANCH", doHandleBranchCombo);
            //addHandler_COMBO_SELECT("IDH_ITMGRP", doHandleItemGroupCombo);
            
            //addHandler_VALIDATE_CHANGED("IDH_CardNM", dovalidateBPChangeEvent);
            //addHandler_COMBO_SELECT("IDH_CUNTRY", doHandleCountryCombo);
            //addHandler_ITEM_PRESSED("IDH_NOVAT", doHanleNoVATCheckBox);
            ////Menu Events
            //Handler_Menu_NAV_ADD = doMenuAddEvent;
            //Handler_Menu_NAV_FIRST = doMenuBrowseEvents;
            //Handler_Menu_NAV_LAST = doMenuBrowseEvents;
            //Handler_Menu_NAV_NEXT = doMenuBrowseEvents;
            //Handler_Menu_NAV_PREV = doMenuBrowseEvents;
            //Handler_Menu_NAV_FIND = doMenuFindBUttonEvent;
            //Handler_FormMode_Changed = doHandleModeChange;
        }
        protected void doSetGridHandlers() {
            ////moGrid.Handler_GRID_ALL = doHandleGRIDAll;
            //moGrid.Handler_GRID_SEARCH = doHandleGridSearch;
            //moGrid.Handler_GRID_SEARCH_VALUESET = doHandleGridSearchValueSet;
            //moGrid.Handler_GRID_DOUBLE_CLICK = doHandleGridDoubleClick;
            //moGrid.Handler_GRID_RIGHT_CLICK = doHandleGridRightClick;
            //moGrid.Handler_GRID_MENU_EVENT = doHandleGridMenuEvent;
            //moGrid.Handler_GRID_SORT = doHandleGridSort;
            //moGrid.Handler_GRID_FIELD_CHANGED = doHandleGRIDFIELDCHANGED;
        }

        private void HandleFormStatus() {
            moGrid.doEnabled(false);
            //if (moWOQ.U_Status == ((int)IDH_WOQHVR.en_WOQSTATUS.WaitingForApproval).ToString() || moWOQ.U_Status == ((int)IDH_WOQHVR.en_WOQSTATUS.Rejected).ToString() ||
            //    moWOQ.U_Status == ((int)IDH_WOQHVR.en_WOQSTATUS.Cancelled).ToString() || moWOQ.U_Status == ((int)IDH_WOQHVR.en_WOQSTATUS.Closed).ToString())
            //    Mode = BoFormMode.fm_VIEW_MODE;
            //else if (moWOQ.U_Status == ((int)IDH_WOQHVR.en_WOQSTATUS.Approved).ToString() || moWOQ.U_Status == ((int)IDH_WOQHVR.en_WOQSTATUS.WOCreated).ToString()) {
            //    Mode = BoFormMode.fm_OK_MODE;
            //    string[] oExl = { "IDH_CRETBP", "IDH_CRTWO","2","1","IDH_DOCS" };
            //    DisableAllEditAndButtonItems(ref oExl);
            //    moGrid.doEnabled(false);

            
            //} else {
            //    Mode = BoFormMode.fm_OK_MODE;
            //    moGrid.doEnabled(true);
            //}
        }
        #endregion
        #region ItemEventHandlers


        /** 
         * Update the Row Header Ref
         */
        private void doUpdateLineHeaderRef() {
            moWOQ.WOQItems.setValueForAllRows(IDH_WOQITVR._JobNr, moWOQ.Code, true);
        }
        public bool doHandleOKEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction)
                return false;
            return true;
        }
        //public bool doAddUpdateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (!pVal.BeforeAction)
        //        return true;
        //    if (!doValidate(SBOForm)) {
        //        BubbleEvent = false;
        //        return false;
        //    }
        //    try {

        //        bool bDoContinue = true;
        //        BoFormMode CurrentMode = Mode;
        //        Freeze(true);
        //        DataHandler.INSTANCE.StartTransaction();

        //        bDoContinue = doAutoSave(IDH_WOQHVR.AUTONUMPREFIX, moWOQ.AutoNumKey);//doAutoSave(SBOForm, msMainTable, IDH_WOQHVR.AUTONUMPREFIX, msHeaderKeyGen);

        //        if (bDoContinue) {
        //            //here update grid
        //            doUpdateLineHeaderRef();
        //            if (moGrid.getDeletedRowList() != null) {
        //                ArrayList aDeletedRows = moGrid.getDeletedRowList();
        //                for (int i = 0; i <= aDeletedRows.Count - 1; i++) {
        //                    IDHAddOns.idh.data.AuditObject oAuditObjGrid = new IDHAddOns.idh.data.AuditObject(IDHAddOns.idh.addon.Base.PARENT, IDH_WOQITVR.TableName);
        //                    if (oAuditObjGrid.setKeyPair(aDeletedRows[i].ToString(), IDHAddOns.idh.data.AuditObject.ac_Types.idh_REMOVE) == true) {
        //                        oAuditObjGrid.Commit();
        //                    }
        //                }

        //            }
        //            int iwResult = 0;
        //            for (int i = 0; i <= moWOQ.WOQItems.Count - 1; i++) {

        //                moWOQ.WOQItems.gotoRow(i);
        //                IDHAddOns.idh.data.AuditObject oAuditObjGrid = new IDHAddOns.idh.data.AuditObject(base.IDHForm.goParent, IDH_WOQITVR.TableName);
        //                if (oAuditObjGrid.setKeyPair(moWOQ.WOQItems.Code, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) == false) {
        //                    oAuditObjGrid.setKeyPair(moWOQ.WOQItems.Code, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD);
        //                }
        //                if (moWOQ.WOQItems.U_WasFDsc.Trim() == string.Empty) {
        //                    continue;
        //                }
        //                oAuditObjGrid.setName((string)moWOQ.WOQItems.Name);//(IDH_WOQITVR._Name, i));
        //                for (int iCols = 0; iCols <= moWOQ.WOQItems.getColumnCount() - 1; iCols++) {
        //                    if (moWOQ.WOQItems.getFieldInfo(iCols).FieldName != "Code" && moWOQ.WOQItems.getFieldInfo(iCols).FieldName != "Name")
        //                        oAuditObjGrid.setFieldValue(moWOQ.WOQItems.getFieldInfo(iCols).FieldName, moWOQ.WOQItems.getValue(moWOQ.WOQItems.getFieldInfo(iCols).FieldName));                        
        //                }
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._AdtnlItm, moWOQ.WOQItems.U_AdtnlItm);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._ChgPrice, moWOQ.WOQItems.U_ChgPrice);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._Cost, moWOQ.WOQItems.U_Cost);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._EnqID, moWOQ.WOQItems.U_EnqID);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._EnqLID, moWOQ.WOQItems.U_EnqLID);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._ExpLdWgt, moWOQ.WOQItems.U_ExpLdWgt);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._GMargin, moWOQ.WOQItems.U_GMargin);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._WasCd, moWOQ.WOQItems.U_WasCd);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._WasFDsc, moWOQ.WOQItems.U_WasFDsc);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._LstPrice, moWOQ.WOQItems.U_LstPrice);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._MnMargin, moWOQ.WOQItems.U_MnMargin);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._PLineID, moWOQ.WOQItems.U_PLineID);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._Sort, moWOQ.WOQItems.U_Sort);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._Status, moWOQ.WOQItems.U_Status);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._UOM, moWOQ.WOQItems.U_UOM);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._JobNr, moWOQ.WOQItems.U_JobNr);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._WstGpCd, moWOQ.WOQItems.U_WstGpCd);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._WstGpNm, moWOQ.WOQItems.U_WstGpNm);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._MANPRC, moWOQ.WOQItems.U_MANPRC);

        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._AdtnlItm, moWOQ.WOQItems.U_AdtnlItm);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._PLineID, moWOQ.WOQItems.U_PLineID);

        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._CarrCd, moWOQ.WOQItems.U_CarrCd);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._CarrNm, moWOQ.WOQItems.U_CarrNm);

        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._Tip, moWOQ.WOQItems.U_Tip);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._TipNm, moWOQ.WOQItems.U_TipNm);


        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._WstGpCd, moWOQ.WOQItems.U_WstGpCd);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._WstGpNm, moWOQ.WOQItems.U_WstGpNm);

        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._EnqID, moWOQ.WOQItems.U_EnqID);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._EnqLID, moWOQ.WOQItems.U_EnqLID);

        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._JobTp, moWOQ.WOQItems.U_JobTp);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._ItemCd, moWOQ.WOQItems.U_ItemCd);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._ItemDsc, moWOQ.WOQItems.U_ItemDsc);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._WasCd, moWOQ.WOQItems.U_WasCd);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._WasFDsc, moWOQ.WOQItems.U_WasFDsc);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._ExpLdWgt, moWOQ.WOQItems.U_ExpLdWgt);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._UOM, moWOQ.WOQItems.U_UOM);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._TipCost, moWOQ.WOQItems.U_TipCost);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._MnMargin, moWOQ.WOQItems.U_MnMargin);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._LstPrice, moWOQ.WOQItems.U_LstPrice);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._TCharge, moWOQ.WOQItems.U_TCharge);

        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._OrdCost, moWOQ.WOQItems.U_OrdCost);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._OrdTot, moWOQ.WOQItems.U_OrdTot);

        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._TipTot, moWOQ.WOQItems.U_TipTot);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._TChrgVtGrp, moWOQ.WOQItems.U_TChrgVtGrp);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._TaxAmt, moWOQ.WOQItems.U_TaxAmt);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._Total, moWOQ.WOQItems.U_Total);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._JCost, moWOQ.WOQItems.U_JCost);

        //                ////
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._CusChr, moWOQ.WOQItems.U_CusChr);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._TCTotal, moWOQ.WOQItems.U_TCTotal);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._Price, moWOQ.WOQItems.U_Price);
                        
        //                ////

        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._GMargin, moWOQ.WOQItems.U_GMargin);

        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._Sample, moWOQ.WOQItems.U_Sample);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._MSDS, moWOQ.WOQItems.U_MSDS);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._RouteCd, moWOQ.WOQItems.U_RouteCd);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._SampleRef, moWOQ.WOQItems.U_SampleRef);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._SampStatus, moWOQ.WOQItems.U_SampStatus);


        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._Status, moWOQ.WOQItems.U_Status);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._Sort, moWOQ.WOQItems.U_Sort);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._JobNr, moWOQ.WOQItems.U_JobNr);
        //                //oAuditObjGrid.setFieldValue(IDH_WOQITVR._MANPRC, moWOQ.WOQItems.U_MANPRC);

        //                iwResult = oAuditObjGrid.Commit();
        //                if (iwResult != 0) {
        //                    bDoContinue = false;
        //                    break;
        //                }
        //            }
        //            //here update Enquiry
        //        }

        //        if (bDoContinue) {
        //            moWOQ.WOQItems.AddedRows.Clear();
        //            moWOQ.WOQItems.ChangedRows.Clear();

        //            IDH_ENQITEM moEnquiryItem = new IDH_ENQITEM();
        //            for (int i = 0; i <= moWOQ.WOQItems.Count - 1; i++) {
        //                moEnquiryItem.DoNotSetDefault = true;
        //                moEnquiryItem.DoNotSetParent = true;
        //                moWOQ.WOQItems.gotoRow(i);
        //                if (moWOQ.WOQItems.U_EnqLID == string.Empty)
        //                    continue;
        //                if (moEnquiryItem.getByKey(moWOQ.WOQItems.U_EnqLID)) {
        //                    moEnquiryItem.U_Status = "2";
        //                    moEnquiryItem.U_WOQID = moWOQ.WOQItems.U_JobNr;
        //                    moEnquiryItem.U_WstGpCd = moWOQ.WOQItems.U_WstGpCd;
        //                    moEnquiryItem.U_WstGpNm = moWOQ.WOQItems.U_WstGpNm;
        //                    moEnquiryItem.U_WOQLineID = moWOQ.WOQItems.Code;
        //                    bDoContinue = moEnquiryItem.doUpdateDataRow("Linked with WOQ:" + moWOQ.WOQItems.U_JobNr+ "." + moWOQ.WOQItems.Code);
        //                    if (bDoContinue == false) {
        //                        break;
        //                    }
        //                }
        //            }

        //        }
        //        if (bDoContinue) {
        //            if (DataHandler.INSTANCE.IsInTransaction()) {
        //                DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
        //                com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors();
        //            } else {
        //                //'Something went wrong...
        //                com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors();
        //                SBOForm.Update();
        //                moGrid.doLoadLastChangeIndicators();
        //                return false;
        //            }
        //            if (base.IDHForm.goParent.doCheckModal(SBOForm.UniqueID)) {
        //                mbENQRESULT = true;
        //                Mode = BoFormMode.fm_OK_MODE;
        //                return true;
        //            } else {
        //                if (CurrentMode == BoFormMode.fm_ADD_MODE ) {
        //                    string msg = com.idh.bridge.resources.Messages.getGMessage("INFDOCNO", new string[] { "WOQ", moWOQ.Code });
        //                    com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(msg, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
        //                    Mode = BoFormMode.fm_ADD_MODE;
        //                } else
        //                    Mode = BoFormMode.fm_OK_MODE;
        //                BubbleEvent = false;
        //            }
        //        } else {
        //            if (DataHandler.INSTANCE.IsInTransaction()) {
        //                DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
        //                com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors();
        //                SBOForm.Update();
        //                moGrid.doLoadLastChangeIndicators();
        //                if (base.IDHForm.goParent.doCheckModal(SBOForm.UniqueID)) {
        //                    return true;
        //                }
        //                return true;
        //            }
        //        }
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "Error in saving WOQ." });
        //        if (DataHandler.INSTANCE.IsInTransaction()) {
        //            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
        //            com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors();
        //            SBOForm.Update();
        //            moGrid.doLoadLastChangeIndicators();
        //            if (base.IDHForm.goParent.doCheckModal(SBOForm.UniqueID)) {
        //                mbENQRESULT = false;
        //            }
        //            return true;
        //        }
        //    } finally {
        //        Freeze( false);
        //    }
        //    return true;
        //}
        //private bool doHandleGRIDFIELDCHANGED(ref IDHAddOns.idh.events.Base pVal) {
        //    try {
        //        if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED && !pVal.BeforeAction) {
        //            if (pVal.ColUID == IDH_WOQITVR._TCharge || pVal.ColUID == IDH_WOQITVR._TipCost || pVal.ColUID == IDH_WOQITVR._CusChr
        //                || pVal.ColUID == IDH_WOQITVR._OrdCost) {
        //                moWOQ.WOQItems.doMarkUserUpdate(pVal.ColUID);
        //                //return false;
        //            }

        //            //if user clicks Sample Check Box 
        //            if (pVal.ColUID == IDH_WOQITVR._Sample) { 
        //                //Following action required 
        //                // 1 - Check Validations to proceed 
        //                // 2 - Create a Sample Item 
        //                // 3 - Created a Lab Task against the Sample Item 

        //                // 2 - Creating Sample: doCreateLabItem(...) 
        //                // 3 - Creating Lab Task: doCreateLabTask(...) 

        //                IDH_LABITM oLabItem = new IDH_LABITM();
        //                IDH_LITMDTL oLIDetail = new IDH_LITMDTL();
        //                bool bResult = false;
        //                IDHAddOns.idh.addon.Base.PARENT.goDICompany.StartTransaction();
                        
        //                bResult = doCreateLabItem(ref pVal, ref oLabItem, ref oLIDetail); //Adding new record in Lab Item and LI Details 
        //                if (bResult)
        //                    bResult = doCreateLabTask(ref oLabItem);

        //                if (bResult)
        //                {
        //                    //Need to update LabItem as the Object code has been updated by new Lab Task Code 
        //                    oLabItem.doUpdateDataRow();

        //                    if (IDHAddOns.idh.addon.Base.PARENT.goDICompany.InTransaction)
        //                        IDHAddOns.idh.addon.Base.PARENT.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
        //                    IDHAddOns.idh.addon.Base.STATUSBAR.SetText("Process completed successfully.", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
        //                }
        //                else
        //                    if (IDHAddOns.idh.addon.Base.PARENT.goDICompany.InTransaction)
        //                    {
        //                        IDHAddOns.idh.addon.Base.PARENT.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
        //                        com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors();
        //                    }
        //            }
        //        }
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "Error in doGRIDFIELDCHANGED." });            
        //    }
        //    return true;
        //}
        //private bool doHandleGridSort(ref IDHAddOns.idh.events.Base pVal) {
        //    return false;
        //}
        //private bool doHandleGridMenuEvent(ref IDHAddOns.idh.events.Base pVal) {
        //    SAPbouiCOM.MenuEvent opVal = (SAPbouiCOM.MenuEvent)pVal.oData;

        //    if ((Mode == BoFormMode.fm_VIEW_MODE && (opVal.MenuUID.Equals("IDHGMADD") || opVal.MenuUID.Equals("IDHGMREML") || opVal.MenuUID.Equals("AddAdItm")))
        //        || ((opVal.MenuUID.Equals("IDHGMADD") || opVal.MenuUID.Equals("IDHGMREML") || opVal.MenuUID.Equals("AddAdItm")) &&
        //              (moWOQ.U_Status != ((int)IDH_WOQHVR.en_WOQSTATUS.New).ToString() && moWOQ.U_Status != ((int)IDH_WOQHVR.en_WOQSTATUS.ReviewAdvised).ToString()))
        //                        ) {
        //        setSharedData("ROWDELTED", "True");
        //        return false;
        //    }

        //    if (!pVal.BeforeAction && opVal.MenuUID.Equals("AddAdItm")) {
        //        SAPbouiCOM.SelectedRows oSelected = moGrid.getGrid().Rows.SelectedRows;
        //        int iSelectionCount = oSelected.Count;
        //        if (iSelectionCount > 0) {
        //            moWOQ.DoBlockUpdateTrigger = true;
        //            string sPCode = (string)moGrid.doGetFieldValue("Code", pVal.Row);
        //            int iSort = (int)moGrid.doGetFieldValue(IDH_WOQITVR._Sort, pVal.Row);
        //            Freeze(true);
        //            moWOQ.WOQItems.doAddEmptyRow(false, true);
        //            string iCode = (string)moGrid.doGetFieldValue("Code", moGrid.getRowCount() - 1);
        //            string sName = (string)moGrid.doGetFieldValue("Name", moGrid.getRowCount() - 1);
        //            for (int i = moGrid.getRowCount() - 1; i >= pVal.Row + 2; i--) {

        //                for (int icol = 0; icol <= moGrid.Columns.Count - 1; icol++) {
        //                    moGrid.doSetFieldValue(icol, i, moGrid.doGetFieldValue(icol, i - 1));
        //                }
        //            }
        //            for (int icol = 0; icol <= moGrid.Columns.Count - 1; icol++) {
        //                if ((moGrid.Columns.Item(icol).UniqueID == "Code"))
        //                    moGrid.doSetFieldValue(icol, pVal.Row + 1, iCode);
        //                else if ((moGrid.Columns.Item(icol).UniqueID == "Name"))
        //                    moGrid.doSetFieldValue(icol, pVal.Row + 1, sName);
        //                else
        //                    moGrid.doSetFieldValue(icol, pVal.Row + 1, moGrid.doGetFieldValue(icol, moGrid.getRowCount() - 1));
        //            }
        //            for (int i = pVal.Row + 1; i <= moGrid.getRowCount() - 1; i++) {
        //                moGrid.doSetFieldValue(IDH_WOQITVR._Sort, i, i + 1);
        //            }
        //            moGrid.doSetFieldValue(IDH_WOQITVR._PLineID, pVal.Row + 1, sPCode);
        //            moGrid.doSetFieldValue(IDH_WOQITVR._AdtnlItm, pVal.Row + 1, "A");
        //            if (IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("AddAdItm"))
        //                IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("AddAdItm");

        //            for (int i = 0; i <= moGrid.getRowCount() - 1; i++) {
        //                if (moGrid.doGetFieldValue(IDH_WOQITVR._Status, i).ToString() == "1")
        //                    moGrid.Settings.SetRowEditable(i + 1, true);
        //                else
        //                    moGrid.Settings.SetRowEditable(i + 1, false);
        //            }
        //            moWOQ.DoBlockUpdateTrigger = false;
        //            moGrid.doApplyRules();
        //            bool ceditable = moGrid.Columns.Item(IDH_WOQITVR._EnqID).Editable;

        //            Freeze(false);
        //        }
        //    } else if (pVal.BeforeAction && opVal.MenuUID.Equals("IDHGMADD") || opVal.MenuUID.Equals("IDHGMREML")) {
        //        setSharedData("ROWDELTED", "False");
        //        SAPbouiCOM.SelectedRows oSelected = moGrid.getGrid().Rows.SelectedRows;
        //        int iSelectionCount = oSelected.Count;
        //        if (iSelectionCount == 0)
        //            return true;
        //        if (moGrid.doGetFieldValue(IDH_WOQITVR._WasFDsc, pVal.Row).ToString().Trim() != string.Empty && (moGrid.doGetFieldValue(IDH_WOQITVR._Status, pVal.Row).ToString() != "1")) {
        //            return false;
        //        } else if (opVal.MenuUID.Equals("IDHGMREML") &&
        //          moGrid.doGetFieldValue(IDH_WOQITVR._WasFDsc, pVal.Row).ToString().Trim() != string.Empty
        //          && (moGrid.doGetFieldValue(IDH_WOQITVR._Status, pVal.Row).ToString() == "1")
        //          ) {
        //            bool bHasLinkedItems = false;
        //            string sLineCode = moGrid.doGetFieldValue(IDH_WOQITVR._Code, pVal.Row).ToString();
        //            for (int i = pVal.Row; i <= moGrid.getRowCount() - 1; i++) {
        //                if (moGrid.doGetFieldValue(IDH_WOQITVR._PLineID, i).ToString() == sLineCode) {
        //                    bHasLinkedItems = true;
        //                    break;
        //                }
        //            }
        //            if (!bHasLinkedItems)
        //                return true;
        //            string sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("ERVEQ003", null);
        //            int iRow = pVal.Row;
        //            if (IDHAddOns.idh.addon.Base.PARENT.doMessage(sResourceMessage, 2, "Yes", "No", null, true) == 1) {
        //                Freeze(true);
        //                moWOQ.DoBlockUpdateTrigger = true;
        //                moGrid.doRemoveRow(iRow, false, true);
        //                for (int i = moGrid.getRowCount() - 1; i >= iRow; i--) {
        //                    if (moGrid.doGetFieldValue(IDH_WOQITVR._PLineID, i).ToString() == sLineCode) {
        //                        moGrid.doRemoveRow(i, false, true);
        //                        setSharedData("ROWDELTED", "True");
        //                    }
        //                }
        //                for (int i = 0; i <= moGrid.getRowCount() - 1; i++) {
        //                    moGrid.doSetFieldValue(IDH_WOQITVR._Sort, i, i + 1);
        //                    if (moGrid.doGetFieldValue(IDH_WOQITVR._Status, i).ToString() == "1")
        //                        moGrid.Settings.SetRowEditable(i + 1, true);
        //                    else
        //                        moGrid.Settings.SetRowEditable(i + 1, false);
        //                }
        //                moWOQ.DoBlockUpdateTrigger = false;
        //                moWOQ.WOQItems.doRefreashAllRowPrices(false, true);
        //                if (Mode == BoFormMode.fm_OK_MODE)
        //                    Mode = BoFormMode.fm_UPDATE_MODE;
        //                moGrid.doApplyRules();
        //                Freeze(false);
        //                return false;
        //            } else {
        //                setSharedData("ROWDELTED", "True");
        //                return false;
        //            }
        //        }
        //    }
        //    return true;
        //}
       
        //private bool doHandleGridRightClick(ref IDHAddOns.idh.events.Base pVal) {
        //    if (Mode != BoFormMode.fm_VIEW_MODE && pVal.BeforeAction && (moWOQ.U_Status == ((int)IDH_WOQHVR.en_WOQSTATUS.New).ToString()) || moWOQ.U_Status == ((int)IDH_WOQHVR.en_WOQSTATUS.ReviewAdvised).ToString() ) {
        //        if (moGrid.getSBOGrid().Rows.IsLeaf(pVal.Row)) {
        //            SAPbouiCOM.SelectedRows oSelected = moGrid.getGrid().Rows.SelectedRows;
        //            int iSelectionCount = oSelected.Count;
        //            if (iSelectionCount > 0 && moGrid.doGetFieldValue(IDH_WOQITVR._WasFDsc, pVal.Row).ToString() != string.Empty
        //                && moGrid.doGetFieldValue(IDH_WOQITVR._WstGpCd, pVal.Row).ToString() != string.Empty
        //                && moGrid.doGetFieldValue(IDH_WOQITVR._AdtnlItm, pVal.Row).ToString() == string.Empty
        //                && moGrid.doGetFieldValue(IDH_WOQITVR._Status, pVal.Row).ToString() == "1") {
        //                SAPbouiCOM.MenuItem oMenuItem;
        //                string sRMenuId = IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU;
        //                oMenuItem = IDHAddOns.idh.addon.Base.APPLICATION.Menus.Item(sRMenuId);
        //                if (IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("AddAdItm"))
        //                    IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("AddAdItm");
        //                if (moGrid.CanEdit) {
        //                    SAPbouiCOM.Menus oMenus;
        //                    int iMenuPos = 1;
        //                    SAPbouiCOM.MenuCreationParams oCreationPackage;
        //                    oCreationPackage = (SAPbouiCOM.MenuCreationParams)IDHAddOns.idh.addon.Base.APPLICATION.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);
        //                    oCreationPackage.Enabled = true;
        //                    oMenus = oMenuItem.SubMenus;
        //                    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;

        //                    oCreationPackage.UniqueID = "AddAdItm";
        //                    oCreationPackage.String = Translation.getTranslatedWord("Add Additional Item");
        //                    oCreationPackage.Position = iMenuPos;
        //                    iMenuPos += 1;
        //                    oMenus.AddEx(oCreationPackage);
        //                }
        //            }
                        
        //        else {
        //                if (IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("AddAdItm"))
        //                    IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("AddAdItm");
        //                setWFValue("LASTJOBMENU", null);
        //            }

        //        } else {
        //            if (IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("AddAdItm"))
        //                IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("AddAdItm");
        //            setWFValue("LASTJOBMENU", null);
        //        }
        //    }
        //    return true;
        //}
        //private bool doHandleGridSearch(ref IDHAddOns.idh.events.Base pVal) {
        //    if (!pVal.BeforeAction) {
        //        if (pVal.ColUID == IDH_WOQITVR._WasFDsc && moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITVR._WasCd) != string.Empty) {
        //            moWOQ.WOQItems.setValue(IDH_WOQITVR._WasCd, "", false);
        //            moWOQ.WOQItems.setValue(IDH_WOQITVR._WasDsc, "", false);
        //        } else if (pVal.ColUID == IDH_WOQITVR._WstGpNm && moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITVR._WstGpCd) != string.Empty) {
        //            moWOQ.WOQItems.setValue(IDH_WOQITVR._WstGpCd, "", false);
        //        }
        //    } else {
        //        if (pVal.ColUID == IDH_WOQITVR._RouteCd && moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITVR._AdtnlItm) == "A") {
        //            moWOQ.WOQItems.setValue(IDH_WOQITVR._RouteCd, "", false);
        //            return false; }
        //    }
        //    return true;
        //}

        //private bool doHandleGridSearchValueSet(ref IDHAddOns.idh.events.Base pVal) {
        //    if (!pVal.BeforeAction) {
        //        if (pVal.ColUID == IDH_WOQITVR._WasFDsc && moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITVR._WasCd) != string.Empty) {
        //            IDH_ADITVLD obj = new IDH_ADITVLD();
        //            string sWG = "";
        //            if (moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITVR._WstGpCd) != string.Empty) {
        //                sWG = " And U_WstGpCd='" + moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITVR._WstGpCd) + "'";
        //            }
        //            int ret = obj.getData("U_ItemCd='" + moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITVR._WasCd) + "' " + sWG, "");
        //            if (ret != 0) {
        //                moWOQ.WOQItems.setValue(IDH_WOQITVR._AdtnlItm, "A");
        //                moWOQ.WOQItems.setValue(IDH_WOQITVR._WstGpCd, obj.U_WstGpCd);
        //                if (obj.U_WstGpCd != string.Empty) {
        //                    object oVal = com.idh.bridge.lookups.Config.INSTANCE.doLookupTableField("@ISB_WASTEGRP", "U_WstGpNm", "U_WstGpCd = '" + obj.U_WstGpCd + '\'');
        //                    moWOQ.WOQItems.setValue(IDH_WOQITVR._WstGpNm, com.idh.utils.Conversions.ToString(oVal));
        //                }
        //            } else {
        //                string sAddGrp = com.idh.bridge.lookups.Config.INSTANCE.doGetAdditionalExpGroup();
        //                if (getSharedData("ITMSGRPCOD") != null && getSharedData("ITMSGRPCOD").ToString() == sAddGrp) {
        //                    moWOQ.WOQItems.setValue(IDH_WOQITVR._AdtnlItm, "A");
        //                }
        //            }
        //        } //else if (pVal.ColUID == IDH_WOQITVR._RouteCd && moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITVR._WasCd) != string.Empty) {
        //            //string sItemFName = (string)Config.INSTANCE.getValueFromOITM(moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITVR._WasCd), "FrgnName");
        //            //if (sItemFName != null) {
        //            //    moWOQ.WOQItems.setValue(IDH_WOQITVR._WasFDsc, sItemFName);
        //            //}
        //        //}
        //    } 
        //    return true;
        //}
        //private bool doHandleGridDoubleClick(ref IDHAddOns.idh.events.Base pVal) {
        //    if (pVal.BeforeAction) {
        //        if ((moGrid.doGetFieldValue(IDH_WOQITVR._WasCd, pVal.Row).ToString().Trim() != string.Empty || Mode!=BoFormMode.fm_ADD_MODE) && (pVal.ColUID == IDH_WOQITVR._WasCd || pVal.ColUID == IDH_WOQITVR._WasFDsc) && moGrid.doGetFieldValue(IDH_WOQITVR._WasFDsc, pVal.Row).ToString().Trim() != string.Empty) {//&& moEnquiry.EnquiryItems.getValueAsString(pVal.Row, IDH_ENQITEM._WasCd) == string.Empty) {
                    
        //            com.isb.forms.Enquiry.admin.WasteItemValidations oOOForm = new com.isb.forms.Enquiry.admin.WasteItemValidations(null, SBOForm.UniqueID, null);
        //            oOOForm.iParentRow = pVal.Row;
        //            oOOForm.sEnquiryLineID = moGrid.doGetFieldValue(IDH_WOQITVR._EnqLID, pVal.Row).ToString();
        //            oOOForm.sWOQLineID = moGrid.doGetFieldValue(IDH_WOQITVR._Code, pVal.Row).ToString();

        //            oOOForm.setUFValue("IDH_ITEMCD", moGrid.doGetFieldValue(IDH_WOQITVR._WasCd, pVal.Row));
        //            oOOForm.setUFValue("IDH_ITEMNM", "");

        //            oOOForm.setUFValue("IDH_FRNNAM", moGrid.doGetFieldValue(IDH_WOQITVR._WasFDsc, pVal.Row));

        //            oOOForm.setUFValue("IDH_WSTGRP", moGrid.doGetFieldValue(IDH_WOQITVR._WstGpCd, pVal.Row));//Address ID 50
        //            oOOForm.WasteGroupCode = (string)moGrid.doGetFieldValue(IDH_WOQITVR._WstGpCd, pVal.Row);
        //            oOOForm.EnquiryID = (string)moGrid.doGetFieldValue(IDH_WOQITVR._EnqID, pVal.Row);
        //            oOOForm.WOQID = (string)moGrid.doGetFieldValue(IDH_WOQITVR._JobNr, pVal.Row);
        //            oOOForm.ItemCode = (string)moGrid.doGetFieldValue(IDH_WOQITVR._WasCd, pVal.Row);
        //            oOOForm.ItemFName= (string)moGrid.doGetFieldValue(IDH_WOQITVR._WasFDsc, pVal.Row);
        //            oOOForm.AddItm = (string)moGrid.doGetFieldValue(IDH_WOQITVR._AdtnlItm, pVal.Row);
        //            oOOForm.CardCode = getDFValue(msMainTable, IDH_WOQHVR._CardCd).ToString();
        //            oOOForm.ParentFormMode = Mode;
        //            oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleCreateWasteItemOKReturn);
        //            oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleCreateWasteItemCancelReturn);
        //            oOOForm.doShowModal(SBOForm);

        //        }
        //        if (pVal.BeforeAction && (pVal.ColUID == IDH_WOQITVR._EnqID || pVal.ColUID == IDH_WOQITVR._EnqLID) && moGrid.doGetFieldValue(IDH_WOQITVR._EnqID, pVal.Row).ToString().Trim() != string.Empty) {
        //            com.isb.forms.Enquiry.Enquiry oOOForm = new com.isb.forms.Enquiry.Enquiry(null, SBOForm.UniqueID, null);
        //            oOOForm.EnquiryID = moGrid.doGetFieldValue(IDH_WOQITVR._EnqID, pVal.Row).ToString().Trim();
        //            oOOForm.bLoadEnquiry = true;
        //            oOOForm.bLoadReadOnly = true;
        //            oOOForm.doShowModal(SBOForm);
        //            oOOForm.DBEnquiry.SBOForm = oOOForm.SBOForm;
        //        }
        //        if (pVal.BeforeAction && pVal.ColUID == IDH_WOQITVR._SampleRef && moGrid.doGetFieldValue(IDH_WOQITVR._SampleRef, pVal.Row).ToString() != string.Empty)
        //        {
        //            //open Lab Item Form here 
        //            //IDHAddOns.idh.addon.Base.PARENT.doOpenForm("IDHLABTODO", oForm, false);
        //            setSharedData("LABITEM", (string)moGrid.doGetFieldValue(IDH_WOQITVR._SampleRef, pVal.Row));
        //            doOpenModalForm("IDHLABITM");
        //        }
        //    }
            
        //    return true;
        //}

        #endregion
        #region Handlers
        private bool doFindEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                Freeze(true);
                try {
                    string sHeader = (string)getFormDFValue(IDH_WOQHVR._Code, true);
                    if (!moWOQ.getByKey(sHeader)) {
                        moGrid.doAddEditLine(true);
                        moGrid.doApplyRules();
                        Mode = BoFormMode.fm_FIND_MODE;
                        doSwitchToFind();
                        doSetWidths();
                        BubbleEvent = false;
                        return false;
                    } else {
                        Mode = BoFormMode.fm_OK_MODE;
                        FillCombos.FillState(Items.Item("IDH_STATE").Specific, moWOQ.U_Country, null);
                        doSwitchToEdit();
                        moGrid.doApplyRules();
                        doFillGridCombos();
                        doSetWidths();
                        HandleFormStatus();
                        BubbleEvent = false;
                        return false;
                    }
                } catch (Exception ex) {
                    throw (ex);
                } finally {
                    Freeze(false);
                }
            }

            return true;
        }
        //private bool doSearchBPAddressCFL(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (!pVal.BeforeAction && moWOQ.U_CardCd.Trim() != string.Empty) {
        //        doChooseBPAddress();
        //    }
        //    return true;
        //}

        private bool doHandleCountryCombo(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {

                FillCombos.FillState(Items.Item("IDH_STATE").Specific, moWOQ.U_Country, null);

            }
            return true;
        }
        //private bool dovalidateBPEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (pVal.BeforeAction && moWOQ.U_CardNM.Trim().IndexOf("*") > -1) {
        //      //  doChooseBPCode();
        //    }
        //    return true;
        //}
        //private bool dovalidateBPChangeEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (pVal.BeforeAction && moWOQ.U_CardNM.Trim() != string.Empty) {
        //        doChooseBPCode();
        //    } else if (pVal.BeforeAction && moWOQ.U_CardNM.Trim() == string.Empty) {
        //        moWOQ.U_CardCd= "";
        //    }
        //    return true;
        //}
        
        //public bool doCreateBP(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (pVal.BeforeAction) {
        //        if (Mode != SAPbouiCOM.BoFormMode.fm_OK_MODE) {
        //            com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage("UNSAVED");
        //            BubbleEvent = false;
        //        } else if (moWOQ.U_CardCd.Trim() != string.Empty &&
        //            Config.INSTANCE.doGetBPName(moWOQ.U_CardCd.Trim()) != string.Empty) {
        //            com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage(("ERPKEXIS"), new string[] { moWOQ.U_CardCd.Trim() });
        //            BubbleEvent = false;
        //        }
        //    } else {
        //        setFocus("IDH_ADR1");
        //        com.isb.forms.Enquiry.CreateBP oOOForm = new com.isb.forms.Enquiry.CreateBP(null, SBOForm.UniqueID, null);
        //        //string sAddress = (getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._Address).ToString() + " " +
        //        //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._Street).ToString() + " " +
        //        //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._City).ToString() + " " +
        //        //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._Country).ToString() + " " +
        //        //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._State).ToString() + " " +
        //        //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._ZipCode).ToString()).Replace("  ", " ").Trim();
        //        oOOForm.WOQID = moWOQ.Code.Trim();
        //        oOOForm.setUFValue("IDH_CARDCD", moWOQ.U_CardCd.Trim());
        //        oOOForm.setUFValue("IDH_CARDNM", moWOQ.U_CardNM.Trim());

        //        oOOForm.setUFValue("IDH_CARDTY", "C");

        //        oOOForm.setUFValue("IDH_ADRID", moWOQ.U_Address.Trim());//Address ID 50
        //        oOOForm.setUFValue("IDH_STREET", moWOQ.U_Street.Trim());//Street 100
        //        oOOForm.setUFValue("IDH_BLOCK", moWOQ.U_Block.Trim());//Block 100
        //        oOOForm.setUFValue("IDH_CITY", moWOQ.U_City.Trim());//City 100
        //        oOOForm.setUFValue("IDH_PCODE", moWOQ.U_ZpCd.Trim());//State 3
        //        oOOForm.setUFValue("IDH_COUNTY", moWOQ.U_County.Trim());//PostCode 20
        //        oOOForm.setUFValue("IDH_STATE", moWOQ.U_State.Trim());//County 100
        //        oOOForm.setUFValue("IDH_CONTRY", moWOQ.U_Country.Trim());//Country 3

        //        oOOForm.setUFValue("IDH_CNTPRS", moWOQ.U_Contact.Trim());//Contact Person 50
        //        oOOForm.setUFValue("IDH_EMAIL", moWOQ.U_E_Mail.Trim());//Email 100
        //        oOOForm.setUFValue("IDH_PHONE", moWOQ.U_Phone1.Trim());//Phone 20
        //        oOOForm.setUFValue("IDH_INFO", "Address:");//Phone 20
        //        oOOForm.setUFValue("IDH_INFO2", "Contact:");//Phone 20
        //        oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleCreateBPOKReturn);
        //        oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleCreateBPCancelReturn);
        //        oOOForm.doShowModal(SBOForm);
        //    }
        //    return true;
        //}
        //public bool doCopyFromENQ(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //        setSharedData("IDH_BPNAME", moWOQ.U_CardNM);
        //        setSharedData("IDH_BPCODE", moWOQ.U_CardCd);
        //        if (moGrid.getRowCount() > 0) {
        //            string aExistingEnquiryRows = "";// new ArrayList();
        //            if (moGrid.doGetFieldValue(IDH_WOQITVR._EnqLID, 0).ToString() != string.Empty)
        //                aExistingEnquiryRows += (moGrid.doGetFieldValue(IDH_WOQITVR._EnqLID, 0));
        //            for (int i = 1; i <= moGrid.getRowCount() - 1; i++) {
        //                if (moGrid.doGetFieldValue(IDH_WOQITVR._EnqLID, i).ToString() != string.Empty)
        //                    aExistingEnquiryRows += " ," + (moGrid.doGetFieldValue(IDH_WOQITVR._EnqLID, i));
        //            }

        //            setSharedData("IDH_EXISTINGENQLINES", aExistingEnquiryRows);
        //        }
        //        doOpenModalForm("IDHSRENQ");
            
        //    return true;
        //}
        
        //public bool doFindAddress(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (!pVal.BeforeAction) {
        //        com.uBC.forms.fr3.PostCode oOOForm = new com.uBC.forms.fr3.PostCode(null, SBOForm.UniqueID, null);
        //        string sAddress = (moWOQ.U_Address + " " +
        //                            moWOQ.U_Street + " " +
        //                            moWOQ.U_City + " " +
        //                            moWOQ.U_Country + " " +
        //                            moWOQ.U_State + " " +
        //                            moWOQ.U_ZpCd).Replace("  ", " ").Trim();

        //        oOOForm.setUFValue("IDH_ADDRES", sAddress);
        //        oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandlePostCodeOKReturn);
        //        oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doHandlePostCodeCancelReturn);
        //        oOOForm.doShowModal(SBOForm);
        //    }
        //    return true;
        //}

        //public bool doCancelButton(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (!pVal.BeforeAction) {
        //        return false;
        //    }
        //    return true;
        //}

        private bool doHandleFormDataLoad(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.ActionSuccess && pVal.BeforeAction == false) {
                Mode = BoFormMode.fm_OK_MODE;
                setFocus("IDH_ADR1");
                EnableItem(false, "IDH_DOCNUM");
                doSetWidths();
            }
            return true;
        }

        //private bool dovalidateFieldsForPriceChange(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    try {
        //        if (!pVal.BeforeAction && pVal.ItemChanged && moWOQ.WOQItems.Count > 0 && (moWOQ.U_CardCd != string.Empty || moWOQ.WOQItems.U_WasCd != string.Empty)) {
        //            Freeze(true);
        //            moWOQ.WOQItems.doRefreashAllRowPrices(false, true);
        //        }
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("dovalidateFieldsForPriceChange") });
        //    } finally {
        //        Freeze(false);
        //    }
        //    return true;
        //}
        //private bool doHanleNoVATCheckBox(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    try {
        //        if (!pVal.BeforeAction && moWOQ.WOQItems.Count > 0 && (moWOQ.U_CardCd != string.Empty || moWOQ.WOQItems.U_WasCd != string.Empty)) {
        //            Freeze(true);
        //            moWOQ.WOQItems.doCalculateAllRowsTotal(true);
        //        }

        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doHanleNoVATCheckBox") });
        //    } finally {
        //        Freeze(false);
        //    }
        //    return true;
        //}

        //private bool doHandleBranchCombo(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    try {
        //        if (!pVal.BeforeAction && pVal.ItemChanged && moWOQ.WOQItems.Count > 0) {
        //            //IDH_BRANCH
        //            //moWOQ.WOQItems.doRefreashAllRowValueforField(sFieldname, oNewValue, false);
        //            //moWOQ.WOQItems.doRefreashAllRowValueforField(IDH_WOQITVR._Branch, moWOQ.U_Branch, false);
        //            Freeze(true);
        //            moWOQ.WOQItems.setValueForAllRows(IDH_WOQITVR._Branch, moWOQ.U_Branch);
        //            moWOQ.WOQItems.doRefreashAllRowPrices(false, true);
        //        }
        //    } catch (Exception ex) {              
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doHandleBranchCombo") });
        //    } finally {
        //        Freeze(false);
        //    }
        //    return true;
        //}
        //private bool doHandleItemGroupCombo(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    try {
        //        if (!pVal.BeforeAction && pVal.ItemChanged && moWOQ.WOQItems.Count > 0) {
        //            //IDH_BRANCH
        //            //moWOQ.WOQItems.doRefreashAllRowValueforField(sFieldname, oNewValue, false);
        //            Freeze(true);
        //            doFillJobTypeCombo();
        //            //string sContainerUID = "SRC*IDHISRC(CONTCD)[IDH_ITMCOD;IDH_ITMNAM=;IDH_GRPCOD=" + moWOQ.U_ItemGrp + "][ITEMCODE;" + IDH_WOQITVR._ItemDsc + "=ITEMNAME]";               
        //            //moGrid.doAddListField(IDH_WOQITVR._ItemCd, "Container Code", true, 100, sContainerUID, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
        //            moWOQ.WOQItems.setValueForAllRows(IDH_WOQITVR._ItmGrp, moWOQ.U_ItemGrp);
        //            // moWOQ.WOQItems.doRefreashAllRowValueforField(IDH_WOQITVR._ItmGrp, moWOQ.U_ItemGrp, false);
        //            moWOQ.WOQItems.doRefreashAllRowPrices(false, true);
        //        }
        //    } catch (Exception ex) {               
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doHandleItemGroupCombo") });
        //    } finally {
        //        Freeze(false);
        //    }
        //    return true;
        //}

        //public override bool doMenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
        //    if (getSharedData("ROWDELTED") != null && getSharedData("ROWDELTED").ToString() == "True") {
        //        BubbleEvent = false;
        //        setSharedData("ROWDELTED", "False");
        //        return false;
        //    }
        //    return true;
        //}
        
        #endregion

        //#region CreateBP
        //public bool doHandleCreateBPOKReturn(com.idh.forms.oo.Form oDialogForm) {
        //    try {
        //        com.isb.forms.Enquiry.CreateBP oOOForm = (com.isb.forms.Enquiry.CreateBP)oDialogForm;
        //        SAPbouiCOM.Form oForm = oOOForm.SBOParentForm;
        //        Freeze(true);
        //        moWOQ.U_CardCd = oOOForm.CardCode;
        //        setFocus("IDH_ADR1");

        //        //oForm.Mode =SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
        //        EnableItem(false, "IDH_CRETBP");
        //        EnableItem(false, "IDH_CardNM");
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "Error in HandleCreateBP OK" });

        //    } finally {
        //        Freeze(false);
        //    }
        //    return true;
        //}
        //public bool doHandleCreateBPCancelReturn(com.idh.forms.oo.Form oDialogForm) {
        //    com.isb.forms.Enquiry.CreateBP oOOForm = (com.isb.forms.Enquiry.CreateBP)oDialogForm;
        //    if (oOOForm.CardCode.Trim() != string.Empty) {
        //        SAPbouiCOM.Form oForm = oOOForm.SBOParentForm;
        //        moWOQ.U_CardCd= oOOForm.CardCode;
        //        setFocus("IDH_ADR1");
        //        //oForm.Mode =SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
        //        EnableItem(false, "IDH_CRETBP");
        //        EnableItem(false, "IDH_CardNM");
        //    }
        //    return true;
        //}
        //#endregion
        //#region Postcode
        //public bool doHandlePostCodeOKReturn(com.idh.forms.oo.Form oDialogForm) {
        //    com.uBC.forms.fr3.PostCode oOOForm = (com.uBC.forms.fr3.PostCode)oDialogForm;
        //    string Company = oOOForm.Company;
        //    string SubBuilding = oOOForm.SubBuilding;
        //    string BuildingNumber = oOOForm.BuildingNumber;
        //    string BuildingName = oOOForm.BuildingName;
        //    string SecondaryStreet = oOOForm.SecondaryStreet;
        //    string Street = oOOForm.Street;
        //    string Block = oOOForm.Block;
        //    ////Neighbourhood
        //    string District = oOOForm.District;
        //    string City = oOOForm.City;
        //    string Line1 = oOOForm.Line1;
        //    string Line2 = oOOForm.Line2;
        //    string Line3 = oOOForm.Line3;
        //    string Line4 = oOOForm.Line4;
        //    string Line5 = oOOForm.Line5;
        //    string AdminAreaName = oOOForm.AdminAreaName;
        //    ////AdminAreaCode
        //    ////Province
        //    string ProvinceName = oOOForm.ProvinceName;
        //    string ProvinceCode = oOOForm.ProvinceCode;
        //    string PostalCode = oOOForm.PostalCode;
        //    string CountryName = oOOForm.CountryName;
        //    string CountryIso2 = oOOForm.CountryIso2;
        //    ////CountryIso3
        //    ////CountryIsoNumber
        //    ////SortingNumber1
        //    ////SortingNumber2
        //    ////Barcode
        //    string POBoxNumber = oOOForm.POBoxNumber;
        //    string Label = oOOForm.Label;
        //    string Type = oOOForm.Type;
        //    SAPbouiCOM.Form oForm = oOOForm.SBOParentForm;

        //    moWOQ.U_Address = (SubBuilding + ' ' + BuildingNumber + ' ' + BuildingName).Replace("  ", " ");
        //    moWOQ.U_AddrssLN = "";
        //    moWOQ.U_Street= (SecondaryStreet + Street);
        //    moWOQ.U_Block= Block;
        //    moWOQ.U_City= City;
        //    moWOQ.U_ZpCd= PostalCode;
        //    moWOQ.U_County= AdminAreaName;


        //    moWOQ.U_State= ProvinceCode;
        //    moWOQ.U_Country= CountryIso2;
            
        //    com.idh.bridge.DataRecords oRecords = null;
        //    try {
        //        string sQry = "Select Code from OCRY where Code Like '" + CountryIso2 + "' ";
        //        if (CountryName != string.Empty) {
        //            oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("dogetcountry" + CountryName, sQry);
        //            if (oRecords != null && oRecords.RecordCount > 0) {
        //                SAPbouiCOM.ComboBox oCOmbo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_CUNTRY").Specific;
        //                if (oCOmbo.Selected == null || (oCOmbo.Selected != null && oCOmbo.Selected.Value != CountryIso2)) {
        //                    oCOmbo.SelectExclusive(CountryIso2, SAPbouiCOM.BoSearchKey.psk_ByValue);
        //                    FillCombos.FillState(oForm.Items.Item("IDH_STATE").Specific, moWOQ.U_Country, null);
        //                }
        //            }
        //            if (ProvinceCode != string.Empty) {
        //                sQry = "Select Code from OCST where Code Like '" + ProvinceCode + "' And Country='" + CountryIso2 + "' ";
        //                oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("dogetstate" + ProvinceCode, sQry);
        //                if (oRecords != null && oRecords.RecordCount > 0) {
        //                    SAPbouiCOM.ComboBox oCOmbo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_STATE").Specific;
        //                    if ((oCOmbo.Selected == null) || (oCOmbo.Selected != null && oCOmbo.Selected.Value != ProvinceCode)) {
        //                        oCOmbo.SelectExclusive(ProvinceCode, SAPbouiCOM.BoSearchKey.psk_ByValue);
        //                    }
        //                }
        //            }
        //        }
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
        //    }
        //    if (Mode == BoFormMode.fm_OK_MODE)
        //        Mode = BoFormMode.fm_UPDATE_MODE;
        //    return true;
        //}
        //public bool doHandlePostCodeCancelReturn(com.idh.forms.oo.Form oDeialogForm) {
        //    com.uBC.forms.fr3.PostCode oOOForm = (com.uBC.forms.fr3.PostCode)oDeialogForm;

        //    return true;
        //}
        //#endregion
        //#region CreateWasteItem
        //public bool doHandleCreateWasteItemOKReturn(com.idh.forms.oo.Form oDialogForm) {
        //    com.isb.forms.Enquiry.admin.WasteItemValidations oOOForm = (com.isb.forms.Enquiry.admin.WasteItemValidations)oDialogForm;

        //    moGrid.doSetFieldValue(IDH_WOQITVR._WasCd, oOOForm.iParentRow, oOOForm.ItemCode);
        //    moGrid.doSetFieldValue(IDH_WOQITVR._WasDsc, oOOForm.iParentRow, oOOForm.ItemName);
        //    mbENQRESULT = true;

        //    //if (Mode != BoFormMode.fm_ADD_MODE)
        //    //   Mode = BoFormMode.fm_UPDATE_MODE;
        //    return true;
        //}
        //public bool doHandleCreateWasteItemCancelReturn(com.idh.forms.oo.Form oDialogForm) {
        //    com.isb.forms.Enquiry.admin.WasteItemValidations oOOForm = (com.isb.forms.Enquiry.admin.WasteItemValidations)oDialogForm;

        //    if (oOOForm.bUpdateParent) {
        //        {
        //            moGrid.doSetFieldValue(IDH_WOQITVR._WasCd, oOOForm.iParentRow, oOOForm.ItemCode);
        //            moGrid.doSetFieldValue(IDH_WOQITVR._WasDsc, oOOForm.iParentRow, oOOForm.ItemName);
        //        } mbENQRESULT = true;
        //        //if (Mode != BoFormMode.fm_ADD_MODE)
        //        //    Mode = BoFormMode.fm_UPDATE_MODE;
        //    }
        //    return true;
        //}
        //#endregion

        #region EventHandlers

        public bool doHandleModeChange(SAPbouiCOM.BoFormMode oSBOMode) {
            Freeze(true);
            if (oSBOMode == BoFormMode.fm_ADD_MODE) {
                Freeze(true);
                doSwitchToEdit();
                EnableItem(true, "IDH_CPYENQ");
                doClearFormDFValues();
                moGrid.doEnabled(true);
                moWOQ.doClearBuffers();
                moWOQ.doAddEmptyRow(true, true);
                //string sContainerUID = "SRC*IDHISRC(CONTCD)[IDH_ITMCOD;IDH_ITMNAM=;IDH_GRPCOD=" + moWOQ.U_ItemGrp + "][ITEMCODE;" + IDH_WOQITVR._ItemDsc + "=ITEMNAME]";
                //moGrid.setUFFilterValue(IDH_WOQITVR._ItemCd, sContainerUID);
                moGrid.doApplyRules();
                doFillGridCombos();
                FillCombos.FillState(Items.Item("IDH_STATE").Specific, moWOQ.U_Country, null);
                setFocus("IDH_CardNM");
                setVisible("IDH_SNDAPR", (moWOQ.U_AppRequired == "Y"));
                doSetWidths();
            } else if (oSBOMode == BoFormMode.fm_FIND_MODE) {
                doSwitchToFind();
                setVisible("IDH_SNDAPR", false);
                doClearFormDFValues();
                //doClearFormUFValues();
                doSetWidths();
                ////moGrid.doApplyRules();
            } else if (oSBOMode == BoFormMode.fm_OK_MODE || oSBOMode == BoFormMode.fm_UPDATE_MODE) {

                if (moWOQ.U_CardCd!= string.Empty) EnableItem(false, "IDH_CardNM");
                //EnableItem(false, "IDH_DOCNUM");
                
                EnableItem(true, "IDH_CPYENQ");
                setVisible("IDH_SNDAPR", (moWOQ.U_AppRequired == "Y"));
            } else if (oSBOMode == BoFormMode.fm_VIEW_MODE) {
                SBOForm.EnableMenu("1282", true);
            }

            Freeze(false);
            return true;
        }
        public bool doMenuAddEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction ) {
                moWOQ.doClearBuffers();
                Mode = BoFormMode.fm_ADD_MODE;
            }
            return true;
        }

        public bool doMenuFindBUttonEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                Mode = BoFormMode.fm_FIND_MODE;
            }
            return true;
        }
        public bool doMenuBrowseEvents(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                //Freeze(true);
                //try {
                //    //doClearFormUFValues();
                //} catch (Exception ex) {
                //    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHANB", null);
                //} finally {
                //    Freeze(false);
                //}
            } else {
                FillCombos.FillState(Items.Item("IDH_STATE").Specific, moWOQ.U_Country, null);
                moWOQ.doLoadChildren();
                if (moWOQ.WOQItems.Count == 0) {
                    moGrid.doAddEditLine(true);
                }
                moGrid.doApplyRules();
                doFillGridCombos();
                EnableItem(false, "IDH_DOCNUM");
                string sCardName = "";
                if (doValidateBPbyCode(ref sCardName) == true) {
                    EnableItem(false, "IDH_CRETBP");
                } else
                    EnableItem(true, "IDH_CRETBP");
                doSetWidths();
                HandleFormStatus();
               
            }
            return true;
        }
        public override void doHandleModalCanceled(string sModalFormType) {
            if (sModalFormType == "IDHCSRCH" && getSharedData("TRG")!=null && getSharedData("TRG").ToString() == "BP")//BP Search
            {
                if (moWOQ.U_CardCd.Trim() != string.Empty) {
                    string sCardName = Config.INSTANCE.doGetBPName(moWOQ.U_CardCd.Trim(),false);
                    if (!moWOQ.U_CardNM.Trim().Equals(sCardName))
                        moWOQ.U_CardCd = "";
                }

            }
            else if (sModalFormType == "IDHASRCH" || sModalFormType == "IDHASRCH3") {
                setFocus("IDH_ADR1");

            }
        }
        //public override void doHandleModalResultShared(string sModalFormType, string sLastButton) {
        //    if (sModalFormType == "IDHCSRCH" && getSharedData("TRG").ToString() == "BP")//BP
        //    {
        //        moWOQ.U_CardCd = getSharedData("CARDCODE").ToString();
        //        moWOQ.U_CardNM = getSharedData("CARDNAME").ToString();
        //        ArrayList oData = Config.INSTANCE.doGetBPContactEmployee(getSharedData("CARDCODE").ToString(), "DEFAULTCONTACT");
        //        if (oData != null) {
        //            moWOQ.U_Contact = oData[0].ToString();
        //            moWOQ.U_Phone1 = oData[4].ToString();
        //            moWOQ.U_E_Mail = oData[7].ToString();
        //        }
        //        if (Mode == BoFormMode.fm_OK_MODE)
        //            Mode = BoFormMode.fm_UPDATE_MODE;

        //    }
        //    else if (sModalFormType == "IDHASRCH" || sModalFormType == "IDHASRCH3")//BP Address
        //    {
        //        moWOQ.U_Address = (string)getSharedData("ADDRESS");
        //        moWOQ.U_AddrssLN = getSharedData("LINENUM").ToString();
        //        moWOQ.U_Block = (string)getSharedData("BLOCK");
        //        moWOQ.U_Street = (string)getSharedData("STREET");
        //        moWOQ.U_City = (string)getSharedData("CITY");
        //        moWOQ.U_State = (string)getSharedData("STATE");
        //        moWOQ.U_ZpCd = (string)getSharedData("ZIPCODE");
        //        moWOQ.U_Country = (string)getSharedData("COUNTRY");
        //        moWOQ.U_County = (string)getSharedData("COUNTY");
        //        if (Mode == BoFormMode.fm_OK_MODE)
        //            Mode = BoFormMode.fm_UPDATE_MODE;

        //    }
        //    else if (sModalFormType == "IDHSRENQ")//Enquiry Search
        //    {
        //        System.Collections.ArrayList aSelectItems = (ArrayList)getSharedData("IDH_SELENQIDS");
        //        if (aSelectItems == null || aSelectItems.Count == 0)
        //            return;
        //        if (Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE) {
        //            IDH_ENQUIRY oEnq = new IDH_ENQUIRY();
        //            oEnq.MustLoadChildren = false;
        //            if (oEnq.getByKey(aSelectItems[0].ToString().Split(':')[0])) {
        //                doSetHeaderByEnquiry(oEnq);
        //            }
        //        }
        //        IDH_ENQITEM oEnqItems = new IDH_ENQITEM();
        //        oEnqItems.DoNotSetDefault = true;
        //        oEnqItems.DoNotSetParent = true;
        //        if (Mode == SAPbouiCOM.BoFormMode.fm_OK_MODE) {
        //            Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
        //        }

        //        moGrid.AddEditLine = false;
        //        int iWOQRow = 0;
        //        string sPrvEnqRowCode = "", sPrvWOQRowCode = "";
        //        string sEnqLineID = "";
        //        int iRet = 0;
        //        moWOQ.WOQItems.gotoRow(moWOQ.WOQItems.Count - 1);
        //        for (int iSelRecodrs = 0; iSelRecodrs <= aSelectItems.Count - 1; iSelRecodrs++) {
        //            sEnqLineID = aSelectItems[iSelRecodrs].ToString().Split(':')[1];
        //            iRet = oEnqItems.getData("(" + IDH_ENQITEM._Code + "='" + sEnqLineID + "' OR " + IDH_ENQITEM._PCode + "='" + sEnqLineID + "') And " + IDH_ENQITEM._Status + " ='1'", IDH_ENQITEM._Sort);
        //            if (iRet == 0)
        //                continue;
        //            while (oEnqItems.next()) {
        //                if (oEnqItems.U_EnqId == string.Empty)
        //                    continue;
        //                iWOQRow = moWOQ.WOQItems.Count - 1;
        //                moWOQ.WOQItems.DoBlockUpdateTrigger = true;
        //                moWOQ.WOQItems.U_AdtnlItm = oEnqItems.U_AddItm;
        //                moWOQ.WOQItems.U_EnqID = oEnqItems.U_EnqId;
        //                moWOQ.WOQItems.U_EnqLID = oEnqItems.Code;
        //                moWOQ.WOQItems.U_ExpLdWgt = oEnqItems.U_EstQty;
        //                moWOQ.WOQItems.U_WasCd = oEnqItems.U_ItemCode;
        //                moWOQ.WOQItems.U_WasFDsc = oEnqItems.U_ItemName;
        //                if (oEnqItems.U_AddItm == "A" && oEnqItems.U_PCode != "" && iWOQRow > 0) {
        //                    if (sPrvEnqRowCode == oEnqItems.U_PCode)
        //                        moWOQ.WOQItems.U_PLineID = sPrvWOQRowCode;

        //                }
        //                else {
        //                    moWOQ.WOQItems.U_PLineID = "";
        //                    sPrvEnqRowCode = oEnqItems.Code;
        //                    sPrvWOQRowCode = moWOQ.WOQItems.Code;
        //                }
        //                moWOQ.WOQItems.U_Status = "1";
        //                moWOQ.WOQItems.U_UOM = oEnqItems.U_UOM;
        //                moWOQ.WOQItems.U_WstGpCd = oEnqItems.U_WstGpCd;
        //                moWOQ.WOQItems.U_WstGpNm = oEnqItems.U_WstGpNm;
        //                moWOQ.WOQItems.DoBlockUpdateTrigger = false;
        //                moWOQ.WOQItems.doGetAllPrices();
        //                moWOQ.WOQItems.doAddEmptyRow(true, true, false);
        //                iWOQRow++;
        //            }
        //        }
        //        doFillGridCombos();
        //    }
        //}
        
        public void HandleFormDataEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.BusinessObjectInfo BusinessObjectInfo, ref bool BubbleEvent) {
            try {
                if (BusinessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_LOAD && BusinessObjectInfo.ActionSuccess && BusinessObjectInfo.BeforeAction == false) {
                    Mode = BoFormMode.fm_OK_MODE;
                    setFocus("IDH_ADR1");
                    EnableItem(false, "IDH_DOCNUM");
                    if (moWOQ.U_CardCd != string.Empty) EnableItem(false, "IDH_CardNM");
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHFE", null);
            }

        }

        #endregion

        
        private void doClearAll() {
            doClearFormDFValues();
        }

        #region "Form Save/Update"
        
        //private bool doValidate(SAPbouiCOM.Form oForm) {
        //  try{ 
        //    if (moWOQ.U_CardNM.Trim() == string.Empty) {

        //        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Business Partner is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Business Partner") });
        //        return false;
        //    }
        //    if (moWOQ.U_Address.Trim() == string.Empty) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Address is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Address") });
        //        return false;
        //    }
        //    if (moWOQ.U_ZpCd.Trim() == string.Empty) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Postcode is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Postcode") });
        //        //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar("Please enter value for Address field.");
        //        return false;
        //    }
        //    if (moWOQ.U_City.Trim() == string.Empty) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: City is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("City") });
        //        //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar("Please enter value for City field.");
        //        return false;
        //    }

        //    if (moWOQ.U_Country.Trim() == string.Empty) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Country is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Country") });
        //        //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar("Please enter value for Country field.");
        //        return false;
        //    }
        //    if (moWOQ.U_Contact.Trim() == string.Empty) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Contact person is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Contact person") });
        //        //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar("Please enter value(s) for contact details.");
        //        return false;
        //    }
        //    if (moWOQ.U_Phone1.Trim() == string.Empty && moWOQ.U_E_Mail.Trim() == string.Empty) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Contact detail(s) is missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Contact detail(s)") });
        //        //com.idh.bridge.DataHandler.INSTANCE.doErrorToStatusbar("Please enter value(s) for contact details.");
        //        return false;
        //    }
        //    for (int i = 0; i <= moGrid.getRowCount() - 2; i++) {
        //        if ((string)moGrid.doGetFieldValue(IDH_WOQITVR._WasFDsc, i) == string.Empty) {
        //            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Item is missing in row " + (i+1).ToString() , "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Item") });
        //            moGrid.getSBOGrid().SetCellFocus(i, moGrid.doFieldIndex(IDH_WOQITVR._WasFDsc));
        //            return false;
        //        } else if ((string)moGrid.doGetFieldValue(IDH_WOQITVR._UOM, i) == string.Empty) {
        //            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: UOM in row " + (i + 1).ToString(), "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("UOM") });
        //            moGrid.getSBOGrid().SetCellFocus(i, moGrid.doFieldIndex(IDH_WOQITVR._UOM));
        //            return false;
        //        } 
        //    }
        //  } catch (Exception ex) {
        //      com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "Error in Validating WOQ." });
        //      return false;
        //  }
        //    return true;
        //}

        #endregion
        #region "Form specific methods"
        protected void doSetFilterFields() {
            moGrid.doAddFilterField("IDH_DOCNUM", IDH_WOQITVR._EnqID, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30);
        }

        protected virtual void doTheGridLayout() {
            if (Config.INSTANCE.getParameterAsBool("FORMSET", false)) {
                if (moFormSettings == null) {
                    string sFormTypeId = "IDHWOQ";   //SBOForm.TypeEx;
                    //Dim sGridID As String = sFormTypeId & "." & moDispRoutGrid.GridId
                    moFormSettings = new idh.dbObjects.User.IDH_FORMSET();
                    moFormSettings.getFormGridFormSettings(sFormTypeId, moGrid.GridId, "");
                    moFormSettings.doAddFieldToGrid(moGrid);

                    doSetListFields();
                    moFormSettings.doSyncDB(moGrid);
                } else {
                    if (moFormSettings.SkipFormSettings) {
                        doSetListFields();
                    } else {
                        moFormSettings.doAddFieldToGrid(moGrid);
                    }
                }
            } else {
                doSetListFields();
            }
            moGrid.getSBOGrid().SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto;
        }

        protected virtual void doSetListFields() {

            string sWastGrp = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup();

            moGrid.doAddListField(IDH_WOQITVR._Code, "Code", false, 0, null, null);
            moGrid.doAddListField(IDH_WOQITVR._Name, "Name", false, 0, null, null);
            moGrid.doAddListField(IDH_WOQITVR._BasWQRID, "WOQ Row ID", false, -1, null, null);

            moGrid.doAddListField(IDH_WOQITVR._AdtnlItm, "A", false, 15, null, null);
            moGrid.doAddListField(IDH_WOQITVR._PLineID, "Linked ID", false, 40, null, null);

            //string sCarrierUIP = "SRC*IDHCSRCH(WC)[IDH_BPCOD;IDH_TYPE=F-C][CARDCODE;" + IDH_WOQITVR._CarrNm + "=CARDNAME]";
            //if (Config.ParameterAsBool("WOBPFLT", true) == false) {
            //    sCarrierUIP = "SRC*IDHCSRCH(WC)[IDH_BPCOD;][CARDCODE;" + IDH_WOQITVR._CarrNm + "=CARDNAME]";
            //} else {
            //    sCarrierUIP = "SRC*IDHCSRCH(WC)[IDH_BPCOD;IDH_TYPE=S;IDH_GROUP=" + Config.Parameter("BGSWCarr") + "][CARDCODE;" + IDH_WOQITVR._CarrNm + "=CARDNAME]";
            //}
            moGrid.doAddListField(IDH_WOQITVR._CarrCd, "Carrier Code", false, 80, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            moGrid.doAddListField(IDH_WOQITVR._CarrNm, "Carrier Name", false, 100, null, null);
            
            //string sDispUIP = "SRC*IDHCSRCH(TIP)[IDH_BPCOD;IDH_TYPE=F-C][CARDCODE;" + IDH_WOQITVR._TipNm + "=CARDNAME]";
            //if (Config.ParameterAsBool("WOBPFLT", false) == false) {
            //    sDispUIP = "SRC*IDHCSRCH(TIP)[IDH_BPCOD;][CARDCODE;" + IDH_WOQITVR._TipNm + "=CARDNAME]";
            //} else {
            //    sDispUIP = "SRC*IDHCSRCH(TIP)[IDH_BPCOD;IDH_TYPE=S;IDH_GROUP=" + Config.Parameter("BGSDispo") + "][CARDCODE;" + IDH_WOQITVR._TipNm + "=CARDNAME]";
            //}
            moGrid.doAddListField(IDH_WOQITVR._Tip, "Disposal Code", false, 80,null, null,-1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            moGrid.doAddListField(IDH_WOQITVR._TipNm, "Disposal Name", false, 100, null, null);

            moGrid.doAddListField(IDH_WOQITVR._WstGpCd, "Waste Group", false, 120,null, null);
            moGrid.doAddListField(IDH_WOQITVR._WstGpNm, "Waste Group Name", false, 0,null, null);

            moGrid.doAddListField(IDH_WOQITVR._EnqID, "Enquiry", false, 50, null, null);
            moGrid.doAddListField(IDH_WOQITVR._EnqLID, "Enquiry Line", false, 50, null, null);

            moGrid.doAddListField(IDH_WOQITVR._JobTp, "Order Type", false, 100, "COMBOBOX", null);


          //  string sContainerUID = "SRC*IDHISRC(CONTCD)[IDH_ITMCOD;IDH_ITMNAM=;IDH_GRPCOD=" + moWOQ.U_ItemGrp + "][ITEMCODE;" + IDH_WOQITVR._ItemDsc + "=ITEMNAME]";
          //  sContainerUID = "SRC*IDHISRC(CONTCD)[IDH_ITMCOD;IDH_ITMNAM=;IDH_GRPCOD=#" + IDH_WOQITVR._ItmGrp + "][ITEMCODE;" + IDH_WOQITVR._ItemDsc + "=ITEMNAME]";


            moGrid.doAddListField(IDH_WOQITVR._ItemCd, "Container Code", false, 100,null, null, -1,SAPbouiCOM.BoLinkedObject.lf_Items);
            moGrid.doAddListField(IDH_WOQITVR._ItemDsc, "Container Name", false, 100, null, null);

            moGrid.doAddListField(IDH_WOQITVR._WasCd, "Item Code", false, 120, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            moGrid.doAddListField(IDH_WOQITVR._WasFDsc, "Description", false, 140, null, null);
            moGrid.doAddListField(IDH_WOQITVR._WasDsc, "Item Name", false, 120, null, null);
            
            moGrid.doAddListField(IDH_WOQITVR._ExpLdWgt, "Est Qty", false, 40, null, null);
            moGrid.doAddListField(IDH_WOQITVR._UOM, "UOM", false, 40, "COMBOBOX", null);
            moGrid.doAddListField(IDH_WOQITVR._TipCost, "Disposal Cost", false, 70, null, null);
            moGrid.doAddListField(IDH_WOQITVR._MnMargin, "MU%", false, 40, null, null);
            moGrid.doAddListField(IDH_WOQITVR._LstPrice, "List Price", false, 60, null, null);
            moGrid.doAddListField(IDH_WOQITVR._TCharge, "Charge Price", false, 70, null, null);

            moGrid.doAddListField(IDH_WOQITVR._CusChr, "Carrier Charge", false, 70, null, null);
            moGrid.doAddListField(IDH_WOQITVR._Price, "Carrier Chg Total", false, 90, null, null);


            moGrid.doAddListField(IDH_WOQITVR._OrdCost, "Carrier Cost", false, 70, null, null);
            moGrid.doAddListField(IDH_WOQITVR._OrdTot, "Carrier Cost Total", false, 90, null, null);

            
            moGrid.doAddListField(IDH_WOQITVR._TipTot, "Disposal Total", false, 70, null, null);
            
            moGrid.doAddListField(IDH_WOQITVR._TChrgVtGrp, "Tax Charge Group", false, 0, null, null);
            moGrid.doAddListField(IDH_WOQITVR._TaxAmt, "Tax Amount", false, 60, null, null);
            moGrid.doAddListField(IDH_WOQITVR._Total, "Total Charge", false, 60, null, null);

            moGrid.doAddListField(IDH_WOQITVR._JCost, "Total Cost", false, 60, null, null);
            

            moGrid.doAddListField(IDH_WOQITVR._GMargin, "GM %", false, 45, null, null);

            moGrid.doAddListField(IDH_WOQITVR._Sample, "Sample", false, 40, "CHECKBOX", null);
            moGrid.doAddListField(IDH_WOQITVR._MSDS, "MSDS", false, 40, "CHECKBOX", null);
            //"SRC*IDH_WGSRCH(WGID)[IDH_WGCOD][WGCODE;" + IDH_WOQITVR._WstGpNm + "=WGNAME]"
            //DisposalRoute
            //string sDispRouteUID = IDH_WOQITVR._TipCost + "=" + IDH_DISPRTCD._TipCost + ";" +
            //    IDH_WOQITVR._Tip + "=" + IDH_DISPRTCD._TipCd + ";" + IDH_WOQITVR._TipNm + "=" + IDH_DISPRTCD._TipNm + ";"
            //    + IDH_WOQITVR._ItemCd + "=" + IDH_DISPRTCD._ItemCd+ ";"
            //    +IDH_WOQITVR._ItemDsc+ "=" + IDH_DISPRTCD._ItemDsc+ ";"
            //    +IDH_WOQITVR._WasCd+ "=" + IDH_DISPRTCD._WasCd+ ";"
            //    + IDH_WOQITVR._WasDsc + "=" + IDH_DISPRTCD._WasDsc + ";"
            //    + IDH_WOQITVR._Quantity + "=" + IDH_DISPRTCD._Qty + ";"
            //    + IDH_WOQITVR._WasFDsc + "=FrgnName;"
            //+IDH_WOQITVR._UOM + "=" + IDH_DISPRTCD._UOM;
            moGrid.doAddListField(IDH_WOQITVR._RouteCd, "Disposal Route", false, 60, null, null);
            moGrid.doAddListField(IDH_WOQITVR._SampleRef, "Sample Ref.", false, 60, null, null);
            moGrid.doAddListField(IDH_WOQITVR._SampStatus, "Sample Status", false, 65, null, null);
            moGrid.doAddListField(IDH_WOQITVR._WhsCode, "Warehouse", false, 40, null, null);


            moGrid.doAddListField(IDH_WOQITVR._Status, "Status", false, 50, "COMBOBOX", null);
            moGrid.doAddListField(IDH_WOQITVR._Sort, "Sort", false, 0, null, null);
            moGrid.doAddListField(IDH_WOQITVR._JobNr, "WOQ ID", false, 0, null, null);
            moGrid.doAddListField(IDH_WOQITVR._MANPRC, "Manual Price Change", false, 0, null, null);

            moGrid.doAutoListFields(false);

            //moGrid.DBObject.getFieldInfo(0).FieldName.in
            moGrid.doSynchDBandGridFieldPos();

            moGrid.doApplyRules();
           // moGrid.doSizeColumns();
            moGrid.setOrderValue("U_Sort Asc,Cast(Code as int) Asc");
        }

        private void doHandleCountryCombo(ref SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal) {
            if (!pVal.BeforeAction) {
                   SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_CUNTRY").Specific;
                   if (oCombo.Selected == null)
                       return;
                   FillCombos.FillState(oForm.Items.Item("IDH_STATE").Specific, oCombo.Selected.Value, null);
            }
        }

        private void doChooseBPCode() {
            setSharedData("IDH_BPCOD", "");
            setSharedData("IDH_NAME", moWOQ.U_CardNM.Trim());
            setSharedData("TRG", "BP");
            doOpenModalForm("IDHCSRCH");
        }
        private void doChooseBPAddress() {
            string sCardCode = moWOQ.U_CardCd.Trim();
            string sAddress = moWOQ.U_Address.Trim();
            setSharedData("TRG", "IDH_ADR1");
            setSharedData("IDH_CUSCOD", sCardCode);
            setSharedData("IDH_ADRTYP", "S");
            setSharedData("CALLEDITEM", "IDH_ADR1");
            setSharedData("IDH_ADDRES", sAddress);
            if (Config.ParameterAsBool("UNEWSRCH", false) == false) {
                doOpenModalForm("IDHASRCH");
            } else {
                doOpenModalForm("IDHASRCH3");
            }
        }

        private void doSetWidths() {
            return;
            //if (moGrid == null || moGrid.Columns.Count == 0)

            //    return;
            //ListFields oFields = moGrid.getGridControl().getListFields();
            //foreach (com.idh.controls.strct.ListField oTField in oFields) {
            //    if (oTField.miWidth > 0)
            //        moGrid.Columns.Item(oTField.msFieldId).Width = oTField.miWidth;

            //}
        }
        private void doUpdateGridCells() {
            if (Mode == BoFormMode.fm_ADD_MODE || Mode == BoFormMode.fm_FIND_MODE)
                return;
            try {
                for (int i = 0; i <= moGrid.getRowCount() - 1; i++) {
                    if (moGrid.doGetFieldValue(IDH_WOQITVR._Status, i).ToString() != "" && moGrid.doGetFieldValue(IDH_WOQITVR._Status, i).ToString() != "1") {
                        moGrid.Settings.SetRowEditable(i + 1, false);
                    }
                }
            } catch (Exception ex) {
                //com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doUpdateGridCells") });
            }

        }
        private bool doValidateBPbyCode(ref string sCardName) {
            if (moWOQ.U_CardCd.Trim() == string.Empty) {
                sCardName = "";
                return false;
            }
            sCardName = Config.INSTANCE.doGetBPName(moWOQ.U_CardCd,false);
            if (sCardName == string.Empty)
                return false;
            else
                return true;
        }

        private void doSwitchToFind() {
            Items.Item("IDH_DOCNUM").Enabled = true;
            setFocus("IDH_DOCNUM");
            EnableItem(false, "IDH_CRETBP");
            EnableItem(false, "IDH_CRTWOQ");
            string[] oExl = { "IDH_DOCNUM" };
            DisableAllEditItems(oExl);

        }

        private void doSwitchToEdit() {
            setFocus("IDH_ADR1");
            string[] oExl = { "IDH_DOCNUM", "IDH_ACTVTY", "IDH_STATUS", "IDH_USER", "IDH_CardCD", 
                                "IDH_LBLTOT", "IDH_LPRICE", "IDH_COST", "IDH_LPPnL", "IDH_LPMRGN", "IDH_CHARGE", "IDH_COST2", "IDH_CHGPnL", "IDH_CHMRGN",
                            "IDH_VERSN","IDH_CHARGE","IDH_CHGVAT","IDH_TCHARG"};
           // concatinate
            EnableAllEditItems(oExl);
            EnableItem(false, "IDH_DOCNUM");
            setFocus("IDH_ADR1");
            EnableItem(true, "IDH_CRETBP");

        }

        #region Fill Combos
        private void doFillCombos() {
            try {
                //   FillCombos.UsersCombo(Items.Item("IDH_ASGNTO").Specific, idh.bridge.DataHandler.INSTANCE.User, null);
                SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)SBOForm.Items.Item("IDH_ASGNTO").Specific;
                doFillCombo(oCombo, "OUSR", "USERID", "U_NAME", "", "U_NAME", false);

                FillCombos.FillCountry(Items.Item("IDH_CUNTRY").Specific);


                FillCombos.FillState(Items.Item("IDH_STATE").Specific, com.idh.bridge.lookups.Config.INSTANCE.getParameterWithDefault("DFTCNTRY", "GB"), null);
                FillCombos.FillWR1StatusNew(Items.Item("IDH_STATUS").Specific, 106);
                FillCombos.FillWR1StatusNew(Items.Item("IDH_SOURCE").Specific, 108);
                FillCombos.BranchCombo(Items.Item("IDH_BRANCH").Specific,null, null);

                //oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_ASGNTO").Specific;
                //    oCombo.SelectExclusive(idh.bridge.DataHandler.INSTANCE.SBOCompany.UserSignature, SAPbouiCOM.BoSearchKey.psk_ByValue);


                oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_STATUS").Specific;
                oCombo.SelectExclusive("1", SAPbouiCOM.BoSearchKey.psk_ByValue);


                oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_CUNTRY").Specific;
                oCombo.SelectExclusive(Config.INSTANCE.getParameterWithDefault("DFTCNTRY", "GB"), SAPbouiCOM.BoSearchKey.psk_ByValue);

                oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_ITMGRP").Specific;
                doFillCombo(oCombo, "OITB g, [@IDH_JOBTYPE] jt", "ItmsGrpCod", "ItmsGrpNam", "g.ItmsGrpCod=jt.U_ItemGrp AND jt.U_OrdCat='" + msOrdCat + "'");
               // oCombo.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index);
               
                //doFillCombo(oCombo, "OUSR", "USERID", "U_NAME", "", "U_NAME", false);

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }

        }
        private void doFillGridCombos() {
            try {
                SAPbouiCOM.ComboBoxColumn oCombo;

                int iIndex = moGrid.doIndexFieldWC(IDH_WOQITVR._UOM);
                oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                FillCombos.UOMCombo(oCombo);

                iIndex = moGrid.doIndexFieldWC(IDH_WOQITVR._Status);
                oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                FillCombos.FillWR1StatusNew(oCombo, 107);

                doFillJobTypeCombo();
            

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }
        }

        private void doFillJobTypeCombo() {
            try {
                SAPbouiCOM.ComboBoxColumn oCombo;
                int iIndex = moGrid.doIndexFieldWC(IDH_WOQITVR._JobTp);
                oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;

                doClearValidValues(oCombo.ValidValues);

                string sItemGrp = moWOQ.U_ItemGrp;
                //string sJobNumber = moWOQ.Code;
               // double dQty = 0;

                ////JobEntries oJobs = getWFValue(oForm, "JOBENTRIES");
                ArrayList oJobTypes = default(ArrayList);
                idh.dbObjects.User.IDH_JOBTYPE_SEQ oJobSeq = idh.dbObjects.User.IDH_JOBTYPE_SEQ.getInstance();
                //if (oJobs == null) {
                //sLastJobType = getFormDFValue("U_JobTp");
                oJobTypes = idh.dbObjects.User.IDH_JOBTYPE_SEQ.getAvailJobs(Config.CAT_WO, sItemGrp, "");
                //} 
                //else {
                //    sItemGrp = getFormDFValue(oForm, "U_ItmGrp");
                //    sLastJobType = oJobs.LastJobType;
                //    sJobNumber = getFormDFValue(oForm, "U_JobNr");
                //    dQty = oJobs.getOnSiteQty(sJobNumber);

                //    if (getWFValue(oForm, "ISNEW") == true) {
                //        oJobTypes = IDH_JOBTYPE_SEQ.getNextAvailJobs(Config.CAT_WO, sItemGrp, sLastJobType, dQty);
                //    } else {
                //        oJobTypes = IDH_JOBTYPE_SEQ.getAvailJobs(Config.CAT_WO, sItemGrp, sLastJobType);
                //    }
                //}
                oCombo.ValidValues.Add("", "");
                if (oJobTypes.Count > 0) {
                    iIndex = 0;
                    for (iIndex = 0; iIndex <= oJobTypes.Count - 1; iIndex++) {
                        try {
                            oJobSeq.gotoRow((int)oJobTypes[iIndex]);
                            oCombo.ValidValues.Add(oJobSeq.U_JobTp, oJobSeq.U_JobTp);
                        } catch {
                        }
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }
        }

        #endregion

        #endregion 
        #region "Form Setup Methods"
      
        public override bool doItemEvent(ref ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction && IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("AddAdItm"))
                IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("AddAdItm");

            return base.doItemEvent(ref pVal, ref BubbleEvent);
        }
        public override bool doRightClickEvent(ref ContextMenuInfo pVal, ref bool BubbleEvent) {
            if (pVal.ItemUID != "LINESGRID" && pVal.BeforeAction && IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("AddAdItm")) {
                IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("AddAdItm");
            }
            return base.doRightClickEvent(ref pVal, ref BubbleEvent);
        }
        #endregion

        //#region Create WO
        //private bool doValidateNCreateAddressNContact() {
        //try{
        //    bool bAddressExists = false;
        //    if (moWOQ.U_Address == "") {
        //        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Please provide customer address.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Container Address") });
        //        return false;
        //    }
        //    if (moWOQ.U_Contact == "" || (moWOQ.U_Phone1 == "" && moWOQ.U_E_Mail=="")) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Please provide customer contact.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Container Contact") });
        //        return false;
        //    }
        //    string sAddressLN =Config.INSTANCE.getValueFromBPShipToAddress(moWOQ.U_CardCd, moWOQ.U_Address, "LineNum").ToString();
        //    if ((sAddressLN == null || sAddressLN == string.Empty )) {
        //        //create address
        //        bAddressExists = false;
        //    } else { 
        //        moWOQ.U_AddrssLN = sAddressLN;
        //        bAddressExists = true;
        //    }
        //    bool bContactExists = false;
        //    string sContact = (string)Config.INSTANCE.getValueFromTablebyKey("OCPR","Name","Name", moWOQ.U_Contact);
        //    if ((sContact == null || sContact == string.Empty)) {
        //        //create contact
        //        bContactExists= false;
        //    } else {
        //        bContactExists = true;
        //    }
        //    try {
        //        SAPbobsCOM.BusinessPartners oBP = (SAPbobsCOM.BusinessPartners)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
        //        if (oBP.GetByKey(moWOQ.U_CardCd) == false)
        //            return false;
        //        if (bAddressExists) {
        //            oBP.Addresses.SetCurrentLine(0);
        //            int iLine = 0;
        //            while (true) {
        //                if (iLine>=oBP.Addresses.Count)
        //                    break;
        //                oBP.Addresses.SetCurrentLine(iLine);
        //                iLine++;
        //                if (oBP.Addresses.AddressName == moWOQ.U_Address)
        //                    break;
        //            }
        //        } 
                
        //        oBP.Addresses.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo;
        //        oBP.Addresses.AddressName = moWOQ.U_Address;
        //        oBP.Addresses.Street = moWOQ.U_SStreet;//Street 100
        //        oBP.Addresses.Block = moWOQ.U_Block;//Block 100
        //        oBP.Addresses.City = moWOQ.U_City;//City 100
        //        oBP.Addresses.ZipCode = moWOQ.U_ZpCd;//State 3
        //        oBP.Addresses.County = moWOQ.U_County;//PostCode 20
        //        oBP.Addresses.State = moWOQ.U_State;//County 100
        //        oBP.Addresses.Country = moWOQ.U_Country;//Country 3


        //        if (bContactExists) {
        //            oBP.ContactEmployees.SetCurrentLine(0);
        //            int iLine = 0;
        //            while (true) {
        //                if (iLine>=oBP.ContactEmployees.Count )
        //                    break;
        //                oBP.ContactEmployees.SetCurrentLine(iLine);
        //                iLine++;
        //                if (oBP.ContactEmployees.Name == moWOQ.U_Contact)
        //                    break;
        //            }
        //        } 
        //        ////oBP.ContactEmployees.Add();
        //        oBP.ContactEmployees.Name = moWOQ.U_Contact;//Contact Person 50
        //        oBP.ContactEmployees.E_Mail = moWOQ.U_E_Mail;//Email 100
        //        oBP.ContactEmployees.Phone1 = moWOQ.U_Phone1;//Phone 20
        //        int iResult = oBP.Update();
        //        if (iResult != 0) {
                   
        //            string sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
        //            //DataHandler.INSTANCE.doSystemError("Stocktransfer Error - " + sResult, "Error processing the Wharehouse to Wharehouse stock transfer.");
        //            com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Business Partner Address Error - " + sResult + " Error while adding business partner aadress.", "ERSYBPAD",
        //                new string[] { "Business Partner Address", sResult });
        //            return false;
        //        } else {
        //            return true;
        //        }
        //    } catch (Exception Ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EREXGEN", null);
        //        return false;
        //    }

        //} catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doValidateForWO") });
        //        return false;
        //    } finally {
        //        Freeze(false);
        //    }
        //    //return true;
        //}
        ////
        //public bool doValidateForWO() {
        //    try {
        //        if (Mode != BoFormMode.fm_OK_MODE) {
        //            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Please save form before creating WO.", "ERSAVEFR", new string[] { com.idh.bridge.Translation.getTranslatedWord("Container Group") });
        //            return false;
                
        //        }
        //        string sCardName = "";
        //        if (doValidateBPbyCode(ref sCardName) == false) {
        //            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: You must create customer.", "ERCRETBP", null);
        //            //com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: You must create customer.", "ERCRETBP", new string[] { com.idh.bridge.Translation.getTranslatedWord("Item") });
        //            return false;
        //        } else if (moWOQ.U_ItemGrp==string.Empty) {
        //           // com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: You must create customer.", "ERCRETBP", null);
        //            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Please select Container Group.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Container Group") });
        //            return false;
        //        }
        //        if (doValidateNCreateAddressNContact() == false) {
        //            return false;
        //        }
        //        bool ret = moWOQ.WOQItems.doValidateAllRowsforWO();
        //        return ret;
                
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doValidateForWO") });
        //        return false;
        //    } finally {
        //        Freeze(false);
        //    }
        //    //return true;
        //}

        //public bool doHandleCreateWO(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    try{
        //        if (pVal.BeforeAction)
        //            return true;
        //        Freeze(true);
        //        if (doValidateForWO() ) 
        //            moWOQ.doCreateWObyWOQ();
        //      //  return true;
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doHandleCreateWO") });
                
        //    } finally {
        //        Freeze(false);
        //    }
        //    return true;
        //}
        //#endregion
        //#region Approvals And Versions
        ////
        //public bool doHandleApprovalButton(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        //{

        //    try
        //    {
        //        if (pVal.BeforeAction)
        //            return true;
        //        Freeze(true);
        //        if (Mode != BoFormMode.fm_OK_MODE) {
        //            com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage("UNSAVED");
        //            return true;
        //        }
        //        string sErrorMsgID = "";
        //        if (moWOQ.doCreateApprovalRequest(ref sErrorMsgID)) {
        //            Mode = BoFormMode.fm_VIEW_MODE;
        //            string msg = com.idh.bridge.resources.Messages.getGMessage("WRNBPA12", null);
        //            com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(msg, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);

        //        } else if (sErrorMsgID!="") {
        //            com.idh.bridge.DataHandler.INSTANCE.doResUserError("You cannot send approval request as your name is not is not listed in approvals", sErrorMsgID, null);
        //            //com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(msg, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);                
        //        }
        //        //if (moWOQ.U_AppRequired == "Y") {
        //        //    moWOQ.U_Status = "2";
        //        //    moWOQ.doCreateApprovalRequest();
        //        //    moWOQ.doProcessData();
        //        //}
        //        //  return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doHandleCreateWO") });

        //    }
        //    finally
        //    {
        //        Freeze(false);
        //    }
        //    return true;
        //}

        //public bool doShowVersionList(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (!pVal.BeforeAction && moWOQ.U_Version>1) {
        //        com.isb.forms.Enquiry.WOQVersionsList oOOForm = new com.isb.forms.Enquiry.WOQVersionsList(null, SBOForm.UniqueID, null);

        //        oOOForm.WOQID = Convert.ToInt32(moWOQ.Code);

        //        oOOForm.doShowModal(SBOForm);
        //        return false;
        //    }
        //    return true;
        //}

        //#endregion

        //#region La Tasks
        //protected bool doCreateLabItem(ref IDHAddOns.idh.events.Base pVal, ref IDH_LABITM oLabItem, ref IDH_LITMDTL oLIDetail)
        //{
        //    try
        //    {
        //        //Validate selected row 
        //        string sICode = moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITVR._WasCd);
        //        string sIName = moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITVR._WasDsc);

        //        if (sICode.Length <= 0)
        //        {
        //            sICode = moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITVR._WasFDsc);
        //            sIName = sICode;

        //            if (sICode.Length <= 0)
        //                return false;
        //        }

        //        sIName = (sIName.Length <= 0 ? sIName = sICode : sIName);

        //        oLabItem = new IDH_LABITM();
        //        oLIDetail = new IDH_LITMDTL();

        //        string sUOM = moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITVR._UOM);
        //        string sQty = moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITVR._Quantity);
        //        //string sWhse = moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITVR._wse);
        //        string sComments = moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITVR._Comment);

        //        com.idh.bridge.DataHandler.NextNumbers oNextLINo = oLabItem.getNewKey();
        //        oLabItem.Code = oNextLINo.CodeCode;
        //        oLabItem.Name = oNextLINo.NameCode;
        //        oLabItem.U_ObjectCd = moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITVR._Code); 
        //        oLabItem.U_ObjectType = "WOQ";
        //        oLabItem.U_LICode = sICode;
        //        oLabItem.U_LIName = sIName;

        //        if (sUOM.Length > 0)
        //            oLabItem.U_UOM = sUOM;

        //        if (sQty.Length > 0)
        //            oLabItem.U_Quantity = Convert.ToDouble(sQty);

        //        //if (sWhse.Length > 0)
        //        //    oLabItem.U_NewWhse = sWhse;

        //        if (sComments.Length > 0)
        //            oLabItem.U_Comments = sComments;

        //        oLabItem.U_CreatedBy = com.idh.bridge.DataHandler.INSTANCE.SBOCompany.UserName;
        //        oLabItem.U_CreatedOn = DateTime.Now;

        //        //Add Lab Item Details 
        //        com.idh.bridge.DataHandler.NextNumbers oNextLIDNo = oLIDetail.getNewKey();
        //        oLIDetail.Code = oNextLIDNo.CodeCode;
        //        oLIDetail.Name = oNextLIDNo.NameCode;
        //        oLIDetail.U_LabCd = oLabItem.Code;

        //        oLIDetail.U_PItemCd = sICode;
        //        oLIDetail.U_PItemNm = sIName;
        //        oLIDetail.U_UOM = sUOM;
        //        oLIDetail.U_Quantity = sQty;
        //        //oLIDetail.U_Whse = oNewItemGridN.doGetFieldValue("NI.FrgnName", i).ToString();
        //        oLIDetail.doAddDataRow();


        //        oLabItem.doAddDataRow();
        //        //moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITVR._WasCd);

        //        moWOQ.WOQItems.setValue(IDH_WOQITVR._SampleRef, oLabItem.Code);
        //        moWOQ.WOQItems.setValue(IDH_WOQITVR._SampStatus, "Pending"); 
        //        //iLabItemCd = Convert.ToInt32(oLabItem.Code);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        com.idh.bridge.DataHandler.INSTANCE.doError("doCreateLabItem()", "Error adding Lab Item: " + ex.Message);
        //        return false;
        //    }

        //}

        //protected bool doCreateLabTask(ref IDH_LABITM oLabItem)
        //{
        //    try
        //    {
        //        IDH_LABTASK oLabTask = new IDH_LABTASK();
        //        com.idh.bridge.DataHandler.NextNumbers oNxtNo = oLabTask.getNewKey();
        //        oLabTask.Code = oNxtNo.CodeCode;
        //        oLabTask.Name = oNxtNo.NameCode;

        //        oLabTask.U_LabItemRCd = oLabItem.Code;
        //        oLabTask.U_LabICd = oLabItem.U_LICode;
        //        oLabTask.U_LabINm = oLabItem.U_LIName;

        //        //oLabTask.U_BPCode = ""; //No BP while creating the Lab Task 
        //        //oLabTask.U_BPName = "";

        //        oLabTask.U_RowStatus = "Requested"; 
        //        oLabTask.U_DtReceived = DateTime.Now;

        //        //oLabTask.U_DtAnalysed = DateTime.Now; //will be updated by user 

        //        //oLabTask.U_Department = "";
        //        oLabTask.U_AssignedTo = "";
        //        oLabTask.U_AnalysisReq = "";
        //        oLabTask.U_ExtStatus = "";
        //        oLabTask.U_ExtComments = "";
        //        //oLabTask.U_AtcAbsEntry = -1;
        //        oLabTask.U_ImpResults = "";
        //        oLabTask.U_Decision = "Pending";
        //        oLabTask.doAddDataRow();
                
        //        oLabItem.U_LabTask = oLabTask.Code; 
        //        //iLabTaskCd = Convert.ToInt32(oLabTask.Code);

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        com.idh.bridge.DataHandler.INSTANCE.doError("doCreateLabTask()", "Error adding Lab Task row: " + ex.Message);
        //        //iLabTaskCd = -1;
        //        return false;
        //    }
        //}
        //#endregion


    }
}