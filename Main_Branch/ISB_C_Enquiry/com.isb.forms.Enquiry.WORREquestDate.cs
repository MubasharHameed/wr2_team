﻿
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;

//Needed for the Framework controls
using IDHAddOns.idh.controls;
using IDHAddOns;
using com.idh.bridge.data;
using SAPbouiCOM;
//Imports idh.const.Lookup

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry {
    //public class WOQRequestDates : ArrayList {
    //    private int miCurrentIndex = 0;
    //    public int CurrentIndex {
    //        get { return miCurrentIndex; }
    //        set { miCurrentIndex = value; }
    //    }
    //    public void Add(WOQRequestDated oRDate) {
    //        base.Add(oRDate);
    //    }

    //    public int Add() {
    //        Add(new WOQRequestDated());
    //        miCurrentIndex = Count - 1;
    //        return miCurrentIndex;
    //    }

    //    public new WOQRequestDated this[int iIndex] {
    //        get {
    //            return (WOQRequestDated)base[iIndex];
    //        }
    //        set {
    //            base[iIndex] = value;
    //        }
    //    }

    //    private WOQRequestDated getCurrentItem() {
    //        if (Count == 0) {
    //            WOQRequestDated oRDate = new WOQRequestDated();
    //            Add(oRDate);
    //            return oRDate;
    //        } else
    //            return this[miCurrentIndex];
    //    }

    //    public string WOR { //Haulage Price
    //        get { return this[miCurrentIndex].moWOR; }
    //        set { getCurrentItem().moWOR = value; }
    //    }

    //    public DateTime RDate { //Haulage Price
    //        get { return this[miCurrentIndex].moRDate; }
    //        set { getCurrentItem().moRDate = value; }
    //    }

    //    public class WOQRequestDated {
    //        public string moWOR;
    //        public DateTime moRDate;
    //    }

    //}

    class WORREquestDate : com.idh.forms.oo.Form {
       
       // private CollectionBase Requestdates;// = new CollectionBase();
        private IDH_WOQITEM moWOQItems;
        //public string IDH_RDate() {
        //    get { return getUFValue("IDH_RDate").ToString(); }
        //    set { setUFValue("IDH_RDate", value); }
        //}

        public IDH_WOQITEM WOQItems {
            get { return moWOQItems; }
            set { moWOQItems = value; }
        }

        //public WORREquestDate(IDHAddOns.idh.addon.Base oParent, string sParent, int iMenuPosition)
        //    : base(oParent, "IDH_WORRDATE", sParent, iMenuPosition, "WORRequestDate.srf", true, true) {
        //}

        public static IDHAddOns.idh.forms.Base doRegisterFormClass() {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_WORRDATE", 0, "WORRequestDate.srf", false);
            return owForm;
        }


        public WORREquestDate(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
        }


        //** Create the form
        public override void doCompleteCreate(ref bool BubbleEvent) {

            int iTop = getItem("IDH_Top").Top;
            int iWidth = getItem("IDH_Top").Width;
            int iLeft = getItem("IDH_Top").Left;
            int iHeight = getItem("IDH_Top").Height;
            SAPbouiCOM.Item owItem;
            SAPbouiCOM.EditText owEdit;
            SAPbouiCOM.StaticText owStatic=null;
            moWOQItems.first();
            for (int i = 0; i <= moWOQItems.Count - 1; i++) {
                moWOQItems.next();
                if (moWOQItems.U_WORID == string.Empty)
                    continue;
                owItem = SBOForm.Items.Add(moWOQItems.Code.ToString(), SAPbouiCOM.BoFormItemTypes.it_EDIT);
                owItem.Left = iLeft;
                owItem.Top = iTop;// + iHeight + 1;
                owItem.Width = iWidth;
                owItem.Height = iHeight;
                owItem.Visible = true;
                owItem.AffectsFormMode = false;
                owEdit = (SAPbouiCOM.EditText)owItem.Specific;
                doAddUF(moWOQItems.Code.ToString(), SAPbouiCOM.BoDataType.dt_DATE, 10, false, false);
                setUFValue(moWOQItems.Code.ToString(), com.idh.utils.Dates.doDateToSBODateStr(moWOQItems.U_RDate));

                owItem = SBOForm.Items.Add("IDH_LB" + i.ToString(), SAPbouiCOM.BoFormItemTypes.it_STATIC);
                owItem.Left = 5;
                owItem.Top = iTop;// + iHeight + 1;
                owItem.Width = 195;
                owItem.Height = iHeight;
                owItem.Visible = true;
                owItem.LinkTo = "IDH_RD" + i.ToString();
                owItem.AffectsFormMode = false;
                owStatic = (SAPbouiCOM.StaticText)owItem.Specific;
                owStatic.Caption = "Request Date for WOQ Line#" + moWOQItems.Code;
                iTop = owItem.Height + owItem.Top + 1;
                //doAddUF("IDH_RD" + i.ToString(), SAPbouiCOM.BoDataType.dt_DATE, 10, false, false);
                //SBOForm.Height + 
                // SBOForm.Height = SBOForm.Height + iSizeChange + 10
            }
            iTop += 5;    
            owItem = getItem("1");
            //int isizechanged = iTop - owItem.Top;
            SBOForm.Height = (int)((iTop + owItem.Height + 5 + 1)+((iTop + owItem.Height + 5 + 1)*0.4));

            owItem.Top = iTop;// + owStatic.Item.Height + 5+1;

            owItem = getItem("2");
            owItem.Top = iTop;// + owStatic.Item.Height + 5+1;
            
            SBOForm.AutoManaged = false;

        }


        public override void doBeforeLoadData() {
            string sString = "";
            //sString = getPriceTypeName(PriceType);

            //SBOForm.Title = sString + " Price Update / Create Option";
        }

        private WOQRequestDates moRDates = null;
        public WOQRequestDates RDates {
            get { return moRDates; }
            set { moRDates = value; }
        }

        public override void doButtonID1(ref ItemEvent pVal, ref bool BubbleEvent) {
            moWOQItems.first();
            moRDates = new WOQRequestDates();

            for (int i = 0; i <= moWOQItems.Count - 1; i++) {
                moWOQItems.next();
                if (moWOQItems.U_WORID == string.Empty)
                    continue;
                moRDates.Add();
                moRDates.WOR = moWOQItems.Code;
                moRDates.RDate= com.idh.utils.Dates.doStrToDate(getUFValue(moWOQItems.Code.ToString()).ToString());
                
                //  moWOQItems.U_RDate= com.idh.utils.Dates.doStrToDate( getUFValue(moWOQItems.Code.ToString()).ToString());
            }
                base.doButtonID1(ref pVal, ref BubbleEvent);
        }
    }
}
