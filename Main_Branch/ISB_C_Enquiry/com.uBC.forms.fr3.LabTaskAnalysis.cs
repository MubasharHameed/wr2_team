﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using IDHAddOns.idh.controls;
using com.idh.bridge.data;
using com.idh.dbObjects; 
using com.idh.dbObjects.numbers;
using com.idh.bridge;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry {
    public class LabTaskAnalysis : IDHAddOns.idh.forms.Base {

        #region Initialization
        public LabTaskAnalysis(IDHAddOns.idh.addon.Base oParent)
            : base(oParent, "IDHLABTANL", 0, "Enq_LabTaskAnalysis.srf", false, true, false, "Lab Task Analysis") {
        }


        protected override void doSetEventFilters() {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE);

        }

        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            try {
                oForm.Title = gsTitle;
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;

                //Report Grid 
                SAPbouiCOM.Item oItem;
                oItem = oForm.Items.Item("IDH_RECLTA");
                FilterGrid oANLGRID = new FilterGrid(this, oForm, "ANLGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane, true);
                oANLGRID.getSBOItem().AffectsFormMode = false;
                oANLGRID.AddEditLine = false;
                doAddUF(oForm, "IDHTSKANLCDS", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 250, false, false);
                doAddUF(oForm, "TASKCODE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50, false, false);
                doAddUF(oForm, "WASTECODE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50, false, false);
                 
                doSetAffectsMode(oForm, false);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
                BubbleEvent = false;
            }
        }

        #endregion

        #region LoadingDate

        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {
            if (getHasSharedData(oForm)) {
                string sTaskAnalysisCodes = getParentSharedData(oForm, "TSKANLCD").ToString();
                setUFValue(oForm, "IDHTSKANLCDS", sTaskAnalysisCodes);
                setUFValue(oForm, "TASKCODE", getParentSharedData(oForm, "TASKCODE").ToString());
                setUFValue(oForm, "WASTECODE", getParentSharedData(oForm, "WASTECODE").ToString());
            }
            doManageAnalysisGrid(oForm);
        }

        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            FilterGrid oAnlyGridN = (FilterGrid)FilterGrid.getInstance(oForm, "ANLGRID");
            oAnlyGridN.doReloadData("", false, false);
            oAnlyGridN.doApplyRules();
            string sTaskAnalysisCodes = getUFValue(oForm, "IDHTSKANLCDS", false).ToString();
            string[] aTA={""};
            if (sTaskAnalysisCodes.Length > 0)
                aTA = sTaskAnalysisCodes.Split(',');
            if (sTaskAnalysisCodes.Trim()== string.Empty || aTA == null || aTA.Length == 0) {
                string sWastecode = getUFValue(oForm, "WASTECODE").ToString();
                int iRet;
                IDH_ITMLANL obj;
                //select the default test for waste code; user just need to press update to committ the selection
                for (int i = 0; i < oAnlyGridN.getRowCount(); i++) {
                    obj = new IDH_ITMLANL();
                    obj.UnLockTable = true;
                    iRet = obj.getData(IDH_ITMLANL._ItemCode + "=\'" + sWastecode + "\' AND " + IDH_ITMLANL._AnalysisCd + "=\'" + oAnlyGridN.doGetFieldValue("LTA.U_AnalysisCd", i).ToString() + "\'  ", "");
                    if (iRet > 0) {
                        oAnlyGridN.doSetFieldValue("LTPRG." + IDH_LBTKPRG._Select, i, "Y");
                    }
                }
            }
            if (aTA!=null && aTA.Length > 0) {
                for (int i = 0; i < oAnlyGridN.getRowCount(); i++) {
                    if (sTaskAnalysisCodes.Contains(oAnlyGridN.doGetFieldValue("LTA.U_AnalysisCd", i).ToString()))
                        oAnlyGridN.doSetFieldValue("LTPRG." + IDH_LBTKPRG._Select, i, "Y");
                }
            }
            oAnlyGridN.getSBOItem().AffectsFormMode = false;
        }

        public override void doFinalizeShow(SAPbouiCOM.Form oForm) {
            base.doFinalizeShow(oForm);
        }

        protected void doSetAnalysisGridListFields(IDHAddOns.idh.controls.FilterGrid oAnlyGridN) {
            //LTI.U_LabTskCd
            //oAnlyGridN.doAddListField("'Select'", "Select", true, -1, "CHECKBOX", null);
            oAnlyGridN.doAddListField("LTPRG." + IDH_LBTKPRG._Select, "Select", true, -1, "CHECKBOX", null);
            oAnlyGridN.doAddListField("LTA.Code", "LTA Code", false, 0, null, null);
            oAnlyGridN.doAddListField("LTA.U_AnalysisCd", "Analysis Code", false, -1, null, null);
            oAnlyGridN.doAddListField("LTA.U_AnalysisDesc", "Analysis Description", false, -1, null, null);
            oAnlyGridN.doAddListField("LTPRG." + IDH_LBTKPRG._Status, "Completed", true, -1, "CHECKBOX", null);
            oAnlyGridN.doAddListField("LTPRG."+ IDH_LBTKPRG._Comments, "Comments", true, -1, null, null);
        }
         
        #endregion

        #region UnloadingData

        public override void doClose() {
        }

        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCloseForm(oForm, ref BubbleEvent);
            FilterGrid.doRemoveGrid(oForm, "ANLGRID");
        }

        #endregion

        #region EventHandlers

        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == true) {
                doPrepareModalData(oForm);
                doReturnFromModalShared(oForm, true);
            }
        }

        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                if (pVal.ItemUID == "ANLGRID") {
                    if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
                }
            }
            return true;
        }
        #endregion

        #region CustomFunctions

        protected void doManageAnalysisGrid(SAPbouiCOM.Form oForm) {
            FilterGrid oAnlyGridN = (FilterGrid)FilterGrid.getInstance(oForm, "ANLGRID");
            if (oAnlyGridN == null)
                oAnlyGridN = new FilterGrid(this, oForm, "ANLGRID", true);

            oAnlyGridN.doAddGridTable(new GridTable("@IDH_LABANLYS", "LTA", "U_AnalysisCd", true), true);
           // oAnlyGridN.doAddGridTable(new GridTable("@IDH_LBTKPRG", "LTPRG", "U_AnalysisCd", true), false);
            oAnlyGridN.setOrderValue("LTA.U_AnalysisCd");
            GridJoin oJoin = new GridJoin("[@IDH_LBTKPRG]", GridJoin.TYPE_JOIN.LEFT, "LTPRG", "U_AnalysisCd", "LTA.U_AnalysisCd=LTPRG.U_AnalysisCd And (LTPRG.U_TaskCd is null or LTPRG.U_TaskCd=\'" + getUFValue(oForm, "TASKCODE").ToString() + "\')", true);
            oAnlyGridN.doAddGridJoin(oJoin);
            oAnlyGridN.doSetDoCount(true);
            doSetAnalysisGridListFields(oAnlyGridN);
            oAnlyGridN.doApplyRules();
            oAnlyGridN.getSBOItem().AffectsFormMode = false;
        }

        public void doPrepareModalData(SAPbouiCOM.Form oForm) {
            FilterGrid oAnlyGridN = (FilterGrid)FilterGrid.getInstance(oForm, "ANLGRID");
            string sTaskAnalysisCodes = "";
            IDH_LBTKPRG objProgress;
            for (int i = 0; i < oAnlyGridN.getRowCount(); i++) {
                /*
                             oAnlyGridN.doAddListField("LTPRG." + IDH_LBTKPRG._Status, "Completed", true, -1, "CHECKBOX", null);
            oAnlyGridN.doAddListField("LTPRG."+ IDH_LBTKPRG._Comments, "Comments", true, -1, null, null);
                 */
                objProgress = new IDH_LBTKPRG();
                objProgress.UnLockTable = true;
                if (objProgress.getData(IDH_LBTKPRG._AnalysisCd + "=\'" + oAnlyGridN.doGetFieldValue("LTA.U_AnalysisCd", i).ToString() + "\' And " +
                    IDH_LBTKPRG._TaskCd + "=\'" + getUFValue(oForm, "TASKCODE").ToString() + "\'", "") == 0) {
                    NumbersPair oNextNum = objProgress.getNewKey();
                    objProgress.doAddEmptyRow();
                    objProgress.Code = oNextNum.CodeCode;
                    objProgress.Name = oNextNum.NameCode;
                }
                objProgress.U_TaskCd = getUFValue(oForm, "TASKCODE").ToString();
                objProgress.U_AnalysisCd = oAnlyGridN.doGetFieldValue("LTA.U_AnalysisCd", i).ToString();
                objProgress.U_Status = oAnlyGridN.doGetFieldValue("LTPRG." + IDH_LBTKPRG._Status, i).ToString();
                objProgress.U_Comments = oAnlyGridN.doGetFieldValue("LTPRG." + IDH_LBTKPRG._Comments, i).ToString();
                objProgress.U_Select = "";
                if (oAnlyGridN.doGetFieldValue("LTPRG." + IDH_LBTKPRG._Select, i).ToString().Equals("Y")) {
                    sTaskAnalysisCodes = sTaskAnalysisCodes + "," + oAnlyGridN.doGetFieldValue("LTA.U_AnalysisCd", i).ToString();
                    objProgress.U_Select = "Y";
                }
                IDH_LBTKPRG.oDataToSave.Add(objProgress);
            }
            sTaskAnalysisCodes = sTaskAnalysisCodes.Trim(',');
            //SAPbouiCOM.Form oParentForm = goParent.doGetParentForm(oForm.UniqueID);
            doClearParentSharedData(oForm);
            setParentSharedData(oForm, "NEWTSKANLCD", sTaskAnalysisCodes);

        }
        #endregion

    }
}