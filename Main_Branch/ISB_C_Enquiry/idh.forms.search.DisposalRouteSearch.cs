﻿
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using IDHAddOns.idh.controls;

using com.isb.enq.dbObjects.User;

using com.idh.bridge;
using com.idh.bridge.lookups;



namespace com.isb.forms.Enquiry.Search {
    public class DisposalRouteSearch : IDHAddOns.idh.forms.Search {

        protected bool bClosingFormByOKButton = false;
        public DisposalRouteSearch(IDHAddOns.idh.addon.Base oParent)
            : base(oParent, "IDH_DRTSR", "DisposalRouteSearch.srf", 5, 70, 1050, 320, "Disposal Route Search") {
        }
        public DisposalRouteSearch(IDHAddOns.idh.addon.Base oParent, string sUNIQUEID)
            : base(oParent, sUNIQUEID, "DisposalRouteSearch.srf", 5, 70, 1050, 320, "Disposal Route Search") {
        }

        public override void doSetGridOptions(FilterGrid oGridN) {
            oGridN.doAddGridTable(new com.idh.bridge.data.GridTable(IDH_DISPRTCD.TableName, "dr", "Code", false, true));
            //oGridN.doAddGridTable(new com.idh.bridge.data.GridTable("OITM", "i", "ItemCode", false, true));

            oGridN.setOrderValue(IDH_DISPRTCD._WasCd + ", " + IDH_DISPRTCD._WasDsc);

            oGridN.doAddFilterField("IDH_FDISPC", "dr." + IDH_DISPRTCD._InTipCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100);
            oGridN.doAddFilterField("IDH_FDISPN", "dr." + IDH_DISPRTCD._InTipNm, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100);
            oGridN.doAddFilterField("IDH_ITMCD", "dr." + IDH_DISPRTCD._ItemCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30);
            oGridN.doAddFilterField("IDH_ITMNM", "dr." + IDH_DISPRTCD._ItemDsc, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30);

            oGridN.doAddFilterField("IDH_POSCOD", "dr." + IDH_DISPRTCD._TZpCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30);
            oGridN.doAddFilterField("IDH_ADDR", "dr." + IDH_DISPRTCD._TAddress, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100);
            oGridN.doAddFilterField("IDH_ADLN", "dr." + IDH_DISPRTCD._TAddrssLN, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100);

            oGridN.doAddFilterField("IDH_UOM", "dr." + IDH_DISPRTCD._UOM, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30);
            oGridN.doAddFilterField("IDH_DISPRT", "dr." + IDH_DISPRTCD._DisRCode, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 50);
            oGridN.doAddFilterField("IDH_DRDSC", "dr." + IDH_DISPRTCD._Descrp, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 255);

            oGridN.doAddFilterField("IDH_WASTCD", "dr." + IDH_DISPRTCD._WasCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100);
            oGridN.doAddFilterField("IDH_WASTNM", "dr." + IDH_DISPRTCD._WasDsc, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100);
            //


            oGridN.doAddListField("dr." + IDH_DISPRTCD._Code, "Code", false, 0, null, IDH_DISPRTCD._Code);
            oGridN.doAddListField("dr." + IDH_DISPRTCD._Name, "Name", false, 0, null, IDH_DISPRTCD._Name);

            oGridN.doAddListField("dr." + IDH_DISPRTCD._DisRCode, "Disposal Route Code", false, -1, null, IDH_DISPRTCD._DisRCode);
            oGridN.doAddListField("dr." + IDH_DISPRTCD._Descrp, "Description", false, -1, null, IDH_DISPRTCD._Descrp);

            oGridN.doAddListField("dr." + IDH_DISPRTCD._TipCd, "Disposal Code", false, -1, null, IDH_DISPRTCD._TipCd, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            oGridN.doAddListField("dr." + IDH_DISPRTCD._TipNm, "Disposal Name", false, -1, null, IDH_DISPRTCD._TipNm);

            oGridN.doAddListField("dr." + IDH_DISPRTCD._InTipCd, "In-Bound Disposal Site Cd", false, -1, null, IDH_DISPRTCD._InTipCd, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            oGridN.doAddListField("dr." + IDH_DISPRTCD._InTipNm, "In-Bound Disposal Site", false, -1, null, IDH_DISPRTCD._InTipNm);

            //oGridN.doAddListField("dr." + IDH_DISPRTCD._FnlTipCd, "Final Disposal Code", false, 0, null, IDH_DISPRTCD._FnlTipCd, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            //oGridN.doAddListField("dr." + IDH_DISPRTCD._FnlTipNm, "Final Disposal Name", false, 0, null, IDH_DISPRTCD._FnlTipNm);

            oGridN.doAddListField("dr." + IDH_DISPRTCD._DisProcCd, "Process", false, -1, null, IDH_DISPRTCD._DisProcCd);
            oGridN.doAddListField("dr." + IDH_DISPRTCD._DisTrtCd, "Treatment", false, -1, null, IDH_DISPRTCD._DisTrtCd);

            oGridN.doAddListField("dr." + IDH_DISPRTCD._State, "State", false, -1, null, IDH_DISPRTCD._State);


            oGridN.doAddListField("dr." + IDH_DISPRTCD._ItemCd, "Container Code", false, -1, null, IDH_DISPRTCD._ItemCd, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            oGridN.doAddListField("dr." + IDH_DISPRTCD._ItemDsc, "Container Desc.", false, -1, null, IDH_DISPRTCD._ItemDsc);

            oGridN.doAddListField("dr." + IDH_DISPRTCD._RDCode, "R/D Code Receipt", false, -1, null, IDH_DISPRTCD._RDCode);
            oGridN.doAddListField("dr." + IDH_DISPRTCD._RDFrgnCd, "R/D Code Despatch", false, -1, null, IDH_DISPRTCD._RDFrgnCd);

            oGridN.doAddListField("dr." + IDH_DISPRTCD._TAddress, "Site Address", false, -1, null, IDH_DISPRTCD._TAddress);

            oGridN.doAddListField("dr." + IDH_DISPRTCD._TAddrssLN, "Address LineNum", false, 0, null, IDH_DISPRTCD._TAddrssLN);
            oGridN.doAddListField("dr." + IDH_DISPRTCD._TZpCd, "Site Postcode", false, -1, null, IDH_DISPRTCD._TZpCd);

            oGridN.doAddListField("dr." + IDH_DISPRTCD._WasCd, "Waste Code", false, -1, null, IDH_DISPRTCD._WasCd, -1, SAPbouiCOM.BoLinkedObject.lf_Items);

            oGridN.doAddListField("dr." + IDH_DISPRTCD._WasDsc, "Waste Desc.", false, -1, null, IDH_DISPRTCD._WasDsc);
            oGridN.doAddListField("dr." + IDH_DISPRTCD._UOM, "UOM", false, -1, null, IDH_DISPRTCD._UOM);

            oGridN.doAddListField("dr." + IDH_DISPRTCD._TipCost, "Disposal Cost", false, -1, null, IDH_DISPRTCD._TipCost);

            oGridN.doAddListField("dr." + IDH_DISPRTCD._Qty, "Quantity", false, -1, null, IDH_DISPRTCD._Qty);

            oGridN.doAddListField("dr." + IDH_DISPRTCD._HaulgCost, "Transport Cost", false, -1, null, IDH_DISPRTCD._HaulgCost);
            oGridN.doAddListField("dr." + IDH_DISPRTCD._TotCost, "Total Cost", false, -1, null, IDH_DISPRTCD._TotCost);

            oGridN.doAddListField("dr." + IDH_DISPRTCD._AvgTngCmt, "Average Tonnage per load and comments", false, -1, null, IDH_DISPRTCD._AvgTngCmt);

            oGridN.doAddListField("dr." + IDH_DISPRTCD._HaulgPCnt, "Transport/ container", false, -1, null, IDH_DISPRTCD._HaulgPCnt);

            oGridN.doAddListField("dr." + IDH_DISPRTCD._CmpBioTrt, "Composting/Biological Treatment Y/N", false, -1, null, IDH_DISPRTCD._CmpBioTrt);//Y/N
            oGridN.doAddListField("dr." + IDH_DISPRTCD._TFSyn, "TFS Y/N", false, -1, null, IDH_DISPRTCD._TFSyn);//Y/N

            oGridN.doAddListField("dr." + IDH_DISPRTCD._WhsCode, "Warehouse", false, -1, null, IDH_DISPRTCD._WhsCode);//OWHS

            oGridN.doAddListField("dr." + IDH_DISPRTCD._COMPrSt, "COMAH pre-set", false, -1, null, IDH_DISPRTCD._COMPrSt);//

            oGridN.doAddListField("dr." + IDH_DISPRTCD._HAZCD, "HP Code", false, -1, null, IDH_DISPRTCD._HAZCD);//
            oGridN.doAddListField("dr." + IDH_DISPRTCD._HazClass, "ADR class", false, -1, null, IDH_DISPRTCD._HazClass);//
            oGridN.doAddListField("dr." + IDH_DISPRTCD._UNNANo, "UN", false, -1, null, IDH_DISPRTCD._UNNANo);//
            oGridN.doAddListField("dr." + IDH_DISPRTCD._PackGrp, "Pkg Gp", false, -1, null, IDH_DISPRTCD._PackGrp);//
            oGridN.doAddListField("dr." + IDH_DISPRTCD._DRTCmts, "Testing Criteria and Spec", false, -1, null, IDH_DISPRTCD._DRTCmts);//
            //oGridN.doAddListField("dr.ItemName", "Item Name", false, 0, null, "ItemName");//
            //oGridN.doAddListField("i.FrgnName", "Item Foreign Name", false, 0, null, "FrgnName");//

        }

        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCompleteCreate(ref oForm, ref BubbleEvent);
            doAddUF(oForm, "IDH_FDISPC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = false;
            doAddUF(oForm, "IDH_FDISPN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = false;

            doAddUF(oForm, "IDH_ITMCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = false;
            doAddUF(oForm, "IDH_ITMNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = false;

            doAddUF(oForm, "IDH_POSCOD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20).AffectsFormMode = false;
            doAddUF(oForm, "IDH_ADDR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = false;
            doAddUF(oForm, "IDH_ADLN", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 10).AffectsFormMode = false;

            doAddUF(oForm, "IDH_WASTCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = false;
            doAddUF(oForm, "IDH_WASTNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = false;


            doAddUF(oForm, "IDH_UOM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = false;
            doAddUF(oForm, "IDH_DISPRT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100).AffectsFormMode = false;
            //IDH_DRDSC'
            doAddUF(oForm, "IDH_DRDSC", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254).AffectsFormMode = false;

        }

        protected override void doSetEventFilters() {
            base.doSetEventFilters();
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD);
        }

        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {
            doUOMCombo(oForm);

            //setUFValue(oForm, "IDH_DISPCD", getParentSharedData(oForm, IDH_DISPRTCD._fTipCd));//getParentSharedData(oForm, "IDH_DISPCD"));
            //setUFValue(oForm, "IDH_DISPNM", getParentSharedData(oForm, IDH_DISPRTCD._TipNm));//getParentSharedData(oForm, "IDH_DISPNM"));
            setUFValue(oForm, "IDH_FDISPC", getParentSharedData(oForm, IDH_DISPRTCD._TipCd));//getParentSharedData(oForm, "IDH_DISPCD"));
            setUFValue(oForm, "IDH_FDISPN", getParentSharedData(oForm, IDH_DISPRTCD._TipNm));//getParentSharedData(oForm, "IDH_DISPNM"));

            setUFValue(oForm, "IDH_ITMCD", getParentSharedData(oForm, IDH_DISPRTCD._ItemCd));// "IDH_ITMCD"));
            setUFValue(oForm, "IDH_ITMNM", getParentSharedData(oForm, IDH_DISPRTCD._ItemDsc));// "IDH_ITMNM"));

            setUFValue(oForm, "IDH_WASTCD", getParentSharedData(oForm, IDH_DISPRTCD._WasCd));// "IDH_WASTCD"));
            setUFValue(oForm, "IDH_WASTNM", getParentSharedData(oForm, IDH_DISPRTCD._WasDsc));// "IDH_WASTNM"));

            setUFValue(oForm, "IDH_DISPRT", getParentSharedData(oForm, "IDH_DISPRT"));
            setUFValue(oForm, "IDH_UOM", getParentSharedData(oForm, IDH_DISPRTCD._UOM));//getParentSharedData(oForm, "IDH_UOM"));


            base.doBeforeLoadData(oForm);
        }
        //** The Initializer
        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            FilterGrid oGridN = FilterGrid.getInstance(oForm, "LINESGRID");

            //string sRequiredString = "i.ItmsGrpCod = ig.ItmsGrpCod ";

            //    string sWasteGroup = Convert.ToString(getUFValue(oForm, "IDH_WSTGRP", true));
            //  string sJobTypeGroup = Convert.ToString(getUFValue(oForm, "IDH_JOBTP", true));

            //if (sWasteGroup.Length > 0) {
            //    sWasteGroup = "'" + sWasteGroup + "'";
            //} else if (sJobTypeGroup.Length > 0) {
            //    SAPbobsCOM.Recordset oRecordSet = default(SAPbobsCOM.Recordset);
            //    string sQry = "SELECT U_WstGpCd FROM [@IDH_WGRPJLK] WHERE U_JobTp = '" + sJobTypeGroup + "'";
            //    oRecordSet = goParent.goDB.doSelectQuery(sQry);

            //    int iRecords = oRecordSet.RecordCount;
            //    if (iRecords > 0) {
            //        while (!oRecordSet.EoF) {
            //            if (sWasteGroup.Length > 0) {
            //                sWasteGroup = sWasteGroup + ",";
            //            }
            //            sWasteGroup = "'" + Convert.ToString(oRecordSet.Fields.Item(0).Value()) + "'";
            //            oRecordSet.MoveNext();
            //        }
            //    }
            //    DataHandler.INSTANCE.doReleaseRecordset(oRecordSet);
            //}

            //if (sWasteGroup.Length > 0) {
            //    sRequiredString = sRequiredString + " AND i.ItemCode In ( SELECT wg.U_ItemCd FROM [@IDH_WGPCNTY] wg, [@IDH_WGRPJLK] wlj WHERE wg.U_WstGpCd = wlj.U_WstGpCd AND wlj.U_WstGpCd In (" + sWasteGroup + "))";
            //}

            //string sRequiredString = "dr." + IDH_DISPRTCD._WasCd + "=i.ItemCode And dr." + IDH_DISPRTCD._Active + "='Y'";
            string sRequiredString = " dr." + IDH_DISPRTCD._Active + "='Y'";
            oGridN.setRequiredFilter(sRequiredString);

            bClosingFormByOKButton = false;
            this.setParentSharedData(oForm, "IDH_ITMSRCHOPEND", "LOADED");
            base.doLoadData(oForm);
        }

        private void doUOMCombo(SAPbouiCOM.Form oForm) {
            try {
                doFillCombo(oForm, "IDH_UOM", "OWGT", "UnitDisply", "UnitName", null, null, "Any", "");
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", new string[] { com.idh.bridge.Translation.getTranslatedWord("UOM") });
            }
        }

        //'## Start 05-07-2013
        public override void doFinalizeShow(SAPbouiCOM.Form oForm) {
            setParentSharedData(oForm, "SILENT", "");
            base.doFinalizeShow(oForm);
            if (oForm.Visible == false) {
                this.setParentSharedData(oForm, "IDH_ITMSRCHOPEND", "");
            } else {
                FilterGrid oGridN = FilterGrid.getInstance(oForm, "LINESGRID");
                if (oGridN == null) {
                    oGridN = new FilterGrid(this, oForm, "IDH_CONTA", "LINESGRID", true);
                }
                oGridN.doReloadData("", false, true);
            }
            doSetFocus(oForm, "IDH_DISPRT");

        }
        //'## End

        protected override void doPrepareModalData(SAPbouiCOM.Form oForm, int iSel = -1) {
            //MyBase.doPrepareModalData(oForm, iSel)

            FilterGrid oGridN = FilterGrid.getInstance(oForm, "LINESGRID");
            int iIndex = 0;
            int iCurrentDataRow = 0;
            SAPbouiCOM.SelectedRows oSelectedRows = default(SAPbouiCOM.SelectedRows);
            oSelectedRows = oGridN.getSBOGrid().Rows.SelectedRows;
            if (oSelectedRows.Count == 0) {
                oSelectedRows.Add(0);
            }

            if (iSel == -1) {
                iCurrentDataRow = oGridN.getSBOGrid().GetDataTableRowIndex(oSelectedRows.Item(iIndex, SAPbouiCOM.BoOrderType.ot_SelectionOrder));
            }

            if (iCurrentDataRow > -1) {
                oGridN.setCurrentDataRowIndex(iCurrentDataRow);

                string sWasteCode = oGridN.doGetFieldValue("dr." + IDH_DISPRTCD._WasCd).ToString().Trim();
                string sParentFormWasteValue = "";
                if (!string.IsNullOrEmpty(getParentSharedDataAsString(oForm, IDH_DISPRTCD._WasCd))) {
                    sParentFormWasteValue = getParentSharedDataAsString(oForm, IDH_DISPRTCD._WasCd);
                }
                string sWasteDesc = oGridN.doGetFieldValue("dr." + IDH_DISPRTCD._WasCd).ToString().Trim();
                string sParentFormWasteDesc = "";
                if (!string.IsNullOrEmpty(getParentSharedDataAsString(oForm, IDH_DISPRTCD._WasDsc))) {
                    sParentFormWasteDesc = getParentSharedDataAsString(oForm, IDH_DISPRTCD._WasDsc);
                }

                int iFields = 0;
                com.idh.controls.strct.ListField oField;//= default(com.idh.controls.strct.ListField);
                com.idh.controls.ListFields oFields = oGridN.getListfields();
                for (iFields = 0; iFields <= oGridN.getListfields().Count - 1; iFields++) {

                    oField = oFields.getListField(iFields);
                    setParentSharedData(oForm, oField.msFieldId, oGridN.doGetFieldValue(oField.msFieldName));
                }
                if (Config.ParameterAsBool("REFRSWOQ", true) == true) {
                    if (!string.IsNullOrEmpty(sParentFormWasteValue)) {
                        sWasteCode = sParentFormWasteValue;
                        setParentSharedData(oForm, IDH_DISPRTCD._WasCd, sParentFormWasteValue);
                    }
                    if (!string.IsNullOrEmpty(sParentFormWasteDesc)) {
                        setParentSharedData(oForm, IDH_DISPRTCD._WasDsc, sParentFormWasteDesc);
                    }
                    if (sWasteCode != string.Empty)
                        sWasteCode = (string)Config.INSTANCE.getValueFromOITM(sWasteCode, "FrgnName");
                    setParentSharedData(oForm, "FrgnName", sWasteCode);
                }

            }
            doReturnFromModalShared(oForm, true);
        }

        //** The ItemSubEvent handler
        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            //'## Start 05-07-2013
            if (pVal.BeforeAction == true) {
                if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE || pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD || (pVal.EventType == SAPbouiCOM.BoEventTypes.et_KEY_DOWN && pVal.CharPressed.ToString() == "27")) {
                    this.setParentSharedData(oForm, "IDH_ITMSRCHOPEND", "");
                    if (bClosingFormByOKButton == false) {
                        base.doButtonID2(oForm, ref pVal, ref BubbleEvent);
                    }
                }
            }
            //'## End
            base.doItemEvent(oForm, ref pVal, ref BubbleEvent);
            if (!pVal.BeforeAction && pVal.EventType == SAPbouiCOM.BoEventTypes.et_VALIDATE) {
                if (pVal.ItemUID == "IDH_WASTCD" && string.IsNullOrEmpty(getUFValue(oForm, "IDH_WASTCD").ToString())) {
                    setUFValue(oForm, "IDH_WASTNM", "");
                } else if (pVal.ItemUID == "IDH_FDISPC" && string.IsNullOrEmpty(getUFValue(oForm, "IDH_FDISPC").ToString())) {
                    setUFValue(oForm, "IDH_FDISPN", "");
                } else if (pVal.ItemUID == "IDH_ITMCD" && string.IsNullOrEmpty(getUFValue(oForm, "IDH_ITMCD").ToString())) {
                    setUFValue(oForm, "IDH_ITMNM", "");
                }
            } else if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_COMBO_SELECT && pVal.BeforeAction == false) {
                string sItem = pVal.ItemUID;
                if (sItem == "IDH_UOM") {
                    doLoadData(oForm);
                }
            }
            return true;
        }

        protected override void doReadInputParams(SAPbouiCOM.Form oForm) {
            setParentSharedData(oForm, "CALLEDITEM", getParentSharedData(oForm, "CALLEDITEM"));
            //base.doReadInputParams(oForm);
        }

    }
}
