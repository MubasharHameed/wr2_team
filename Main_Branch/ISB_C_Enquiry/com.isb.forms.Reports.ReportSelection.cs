﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;

using IDHAddOns.idh.controls;
//using com.idh.bridge.reports;
//using com.idh.utils.Conversions;
using com.idh.bridge;
using com.idh.bridge.data;
//using WR1_Grids.idh.controls.grid;
using com.idh.bridge.lookups;
using com.idh.dbObjects.User;
using com.idh.bridge.action;
using com.idh.controls;
using com.idh.controls.strct;

namespace com.isb.forms.Enquiry.Reports {
    class ReportSelection : 
        IDHAddOns.idh.forms.Base {

        public ReportSelection(IDHAddOns.idh.addon.Base oParent,string sParent, int iMenuPosition)
            : base(oParent, "IDHWOQRPTS", sParent, iMenuPosition, "ENQ_WOQReportSelection.srf",true,true) {
        }

        protected virtual void doTheGridLayout(UpdateGrid oGridN) {
                doSetListFields(oGridN);
        }

        protected void doSetListFields(UpdateGrid moGrid) {
            moGrid.doAddListField("r." + IDH_WRCONFIG._Code, "Config Code", false, -1, null, null);
            moGrid.doAddListField("r." + IDH_WRCONFIG._Val, "Report Name", false, 20, null, null);
            moGrid.doAddListField("cast('' as NVARCHAR(1))" , "Print", true, -1, ListFields.LISTTYPE_CHECKBOX, null);
        }


        protected void doSetGridFilters(UpdateGrid moGridN) {

            moGridN.doAddFilterField("IDH_DATEF", "h.U_Recontact", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10);
            moGridN.doAddFilterField("IDH_DATET", "h.U_Recontact", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10);
            moGridN.doAddFilterField("IDH_CUST", "h.U_CardCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_NAME", "h.U_CardName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_ENQSTS", "h.U_Status", SAPbouiCOM.BoDataType.dt_SHORT_NUMBER, "=", 11);
            moGridN.doAddFilterField("IDH_ACTVTY", "h.U_ClgCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_ASGNTO", "h.U_User", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_USER", "h.U_AttendUser", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_ENQID", "h.Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_ROWNO", "r.Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_ADDR", "h.U_Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_POSTCD", "h.U_ZipCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_CONPH", "h.U_Phone1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_CONNM", "h.U_CName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_EMAIL", "h.U_E_Mail", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_STREET", "h.U_Street", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WSCD", "r.U_ItemCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WSFDC", "r.U_ItemDesc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WSDC", "r.U_ItemName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WSGPCD", "r.U_WstGpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WSGPNM", "r.U_WstGpNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            doAddUF(moGridN.getSBOForm(), "IDH_SHADIT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, false, false);
        }

        //*** Add event filters to avoid receiving all events from SBO
        protected override void doSetEventFilters() {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE);
        }

        protected override void doReadInputParams(SAPbouiCOM.Form oForm) {
            ////base.doReadInputParams(oForm);
            //if (getHasSharedData(oForm)) {
            //    UpdateGrid oGridN = default(UpdateGrid);
            //    oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");

            //    int iIndex = 0;
            //    for (iIndex = 0; iIndex <= oGridN.getGridControl().getFilterFields().Count - 1; iIndex++) {
            //        com.idh.controls.strct.FilterField oField = (com.idh.controls.strct.FilterField)(oGridN.getGridControl().getFilterFields()[iIndex]);
            //        string sFieldName = oField.msFieldName;
            //        string sFieldValue = getParentSharedData(oForm, sFieldName).ToString();
            //        try {
            //            setUFValue(oForm, sFieldName, sFieldValue);
            //        } catch (Exception ex) {
            //        }
            //    }
            //}
        }
        public string getListRequiredStr(SAPbouiCOM.Form oForm) {
            string sWRCode = "'WOQDOC'";
            if (getHasSharedData(oForm)) {
                if (!string.IsNullOrEmpty(getParentSharedDataAsString(oForm, "WOQDOCCODES")))
                    sWRCode = getParentSharedDataAsString(oForm, "WOQDOCCODES");
                sWRCode ="'"+ (sWRCode.Replace(",", "','"))+ "'";

                //string[] sVals = sWRCode.Split(',');
                //for (int i=0;sVals.Length-1,i++){
                //sWRCode +="'"+ sVals
                //}
            }
            return IDH_WRCONFIG._Code + " IN ( "+ sWRCode +" ) ";          
        }
        //** Create the form
        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            try {
                oForm.Title = gsTitle;
                int iWidth = oForm.Width;

                UpdateGrid oGridN = new UpdateGrid(this, oForm, "IDH_CONTA", "LINESGRID");//, 5, 5, iWidth - 20, 180, 0,0);
                
                oGridN.getSBOGrid().SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto;
                oForm.AutoManaged = false;
                oGridN.getSBOItem().AffectsFormMode = false;

                oForm.EnableMenu(Config.NAV_ADD, false);
                oForm.EnableMenu(Config.NAV_FIND, false);
                oForm.EnableMenu(Config.NAV_FIRST, false);
                oForm.EnableMenu(Config.NAV_NEXT, false);
                oForm.EnableMenu(Config.NAV_PREV, false);
                oForm.EnableMenu(Config.NAV_LAST, false);

                
                oForm.SupportedModes = Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_Ok);
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_OK_MODE);
                base.doCompleteCreate(ref oForm, ref BubbleEvent);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDLF", null);
                BubbleEvent = false;
            }
        }

        protected void doSetMode(SAPbouiCOM.Form oForm, SAPbouiCOM.BoFormMode iMode) {
            try {
                object sFormType = getParentSharedData(oForm, "FTYPE");
                bool bIsSearch = false;
                if ((sFormType != null) && sFormType.Equals("SEARCH") == true) {
                    bIsSearch = true;
                }

                if (iMode == SAPbouiCOM.BoFormMode.fm_FIND_MODE) {
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = Translation.getTranslatedWord("Find");
                    if (bIsSearch) {
                        oForm.Items.Item("2").Visible = true;
                    } else {
                        oForm.Items.Item("2").Visible = false;
                    }
                } else if (iMode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = Translation.getTranslatedWord("Update");
                    oForm.Items.Item("2").Visible = true;
                }
            } catch (Exception ex) {
            }
        }
        
        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {

            //oForm.Top = goParent.goApplication.Desktop.Height - (oForm.Height + 130);
            //oForm.Left = 200;
            
            UpdateGrid oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");

            if (oGridN == null) {
                //int iWidth = oForm.Width;
                oGridN = new UpdateGrid(this, oForm, "LINESGRID"); //, 7, 100, iWidth - 20, 255, 0, 0);
            }

            //Required tables
            oGridN.doAddGridTable(new GridTable(IDH_WRCONFIG.TableName, "r", "Code", true, true), true);
            
            doSetGridFilters(oGridN);
            oGridN.setRequiredFilter(getListRequiredStr(oForm));
            oGridN.doSetDoCount(true);
            oGridN.doSetBlankLine(false);

            //Set the required List Fields
            doTheGridLayout(oGridN);



            //bool bHasParams = false;
            //if (getHasSharedData(oForm)) {
            //    doReadInputParams(oForm);
            //    bHasParams = true;

            //    string sCardCode = getParentSharedData(oForm, "CARDCODE").ToString();
            //    if ((sCardCode != null) && sCardCode.Length > 0) {
            //        string sExtraReqFilter = oGridN.getRequiredFilter();

            //        if (sExtraReqFilter.Length > 0) {
            //            sExtraReqFilter = sExtraReqFilter + " AND ";
            //        }

            //        sExtraReqFilter = sExtraReqFilter + " (h."+IDH_ENQUIRY._CardCode+" LIKE '" + sCardCode + "%') ";

            //        oGridN.setRequiredFilter(sExtraReqFilter);
            //    }
            //}

            //if (bHasParams == false) {
            //    oGridN.setInitialFilterValue("r.Code = '-100'");
            //}
        }

        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            doReLoadData(oForm, !getHasSharedData(oForm));
        }

        
        //** The Initializer
        protected void doReLoadData(SAPbouiCOM.Form oForm, bool bIsFirst)
		{
			try {
				doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_OK_MODE);

                UpdateGrid oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");
				string sFilter = oGridN.getRequiredFilter();
				//doSetCustomeFilter(oGridN);
				//doUpdateStatuses(oGridN);
				//doSetFormSpecificFilters(oGridN);
                //if (Config.ParameterAsBool("ENBODRWO", false) && !Config.ParameterAsBool("FORMSET", false))
                //oGridN.setOrderValue("Cast(r." + IDH_ENQITEM._EnqId + " as Int),r." + IDH_ENQITEM._Sort  + " ");
                
                oGridN.doReloadData("", false, true);
                //doFillGridCombos(oGridN);
                oGridN.setRequiredFilter(sFilter);
				
			} catch (Exception ex) {
				com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBL",  null );
			}
		}
        //protected void doSetCustomeFilter(UpdateGrid oGridN) {
        //    SAPbouiCOM.Form oForm = oGridN.getSBOForm();
        //    if (getItemValue(oForm, "IDH_SHADIT").ToString().Trim()=="Y") {
        //        //string sStatusQry = IDH_ENQITEM._AddItm + "='A'";//doGetStatusFilterQueryDynamic(getItemValue(oForm, "IDH_PROG").ToString.Trim.ToUpper);
        //        //oGridN.setRequiredFilter(oGridN.getRequiredFilter() + " And " + sStatusQry);
        //    } else {
        //        string sStatusQry = IDH_ENQITEM._AddItm + "=''";//doGetStatusFilterQueryDynamic(getItemValue(oForm, "IDH_PROG").ToString.Trim.ToUpper);
        //        oGridN.setRequiredFilter(oGridN.getRequiredFilter() + " And " + sStatusQry);
        //    }
        //}

        //public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (pVal.BeforeAction == true) {
        //        if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update") || oForm.Mode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
        //            ////Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
        //            //UpdateGrid oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");
        //            //ArrayList aRows = oGridN.doGetChangedRows();
        //            //ArrayList oCloseList = new ArrayList();
        //            //if (doSaveOrderRows(oForm) == true) {
        //            //    string sHead = null;
        //            //    string sRow = null;
        //            //    string sSOStatus = null;
        //            //    string sPOStatus = null;

        //            //    if ((aRows != null)) {
        //            //        bool bDoInBack = Config.INSTANCE.getParameterAsBool("MDCMIBG", false);
        //            //        if (bDoInBack == false || oGridN.mbForceRealTimeBilling == true) {
        //            //            for (int iIndex = 0; iIndex <= aRows.Count - 1; iIndex++) {
        //            //                oGridN.setCurrentDataRowIndex(aRows(iIndex));
        //            //                sHead = oGridN.doGetFieldValue("r." + IDH_JOBSHD._JobNr);
        //            //                sRow = oGridN.doGetFieldValue("r." + IDH_JOBSHD._Code);
        //            //                sSOStatus = oGridN.doGetFieldValue("r." + IDH_JOBSHD._Status);
        //            //                sPOStatus = oGridN.doGetFieldValue("r." + IDH_JOBSHD._PStat);

        //            //                if (sSOStatus.StartsWith(FixedValues.getDoOrderStatus()) || sPOStatus.StartsWith(FixedValues.getDoOrderStatus())) {
        //            //                    MarketingDocs.doOrders("@IDH_JOBENTR", "@IDH_JOBSHD", "WO", sHead, sRow);
        //            //                }

        //            //                if (sSOStatus.StartsWith(FixedValues.getDoInvoiceStatus()) || sSOStatus.StartsWith(FixedValues.getDoPInvoiceStatus())) {
        //            //                    MarketingDocs.doInvoicesAR("@IDH_JOBENTR", "@IDH_JOBSHD", "WO", sHead, sRow);
        //            //                }

        //            //                if (sSOStatus.StartsWith(FixedValues.getDoRebateStatus()) || sPOStatus.StartsWith(FixedValues.getDoRebateStatus())) {
        //            //                    MarketingDocs.doRebates("@IDH_JOBENTR", "@IDH_JOBSHD", "WO", sHead, sRow);
        //            //                }

        //            //                if (sSOStatus.StartsWith(FixedValues.getDoFocStatus())) {
        //            //                    MarketingDocs.doFoc("@IDH_JOBENTR", "@IDH_JOBSHD", "WO", sHead, sRow);
        //            //                }

        //            //                MarketingDocs.doPayment("@IDH_JOBENTR", "@IDH_JOBSHD", "WO", sHead, sRow);

        //            //                //CR 20150601: The DO Journals functionality build for USA Clients can now be used for all under new WR Config Key "NEWDOJRN" 
        //            //                //'For USA release 
        //            //                //If Config.INSTANCE.getParameterAsBool("USAREL", False) = True Then
        //            //                if (Config.INSTANCE.getParameterAsBool("NEWDOJRN", false) == true) {
        //            //                    bool sResult = MarketingDocs.doJournals("@IDH_JOBENTR", "@IDH_JOBSHD", "WO", sHead, sRow);
        //            //                    if (sResult) {
        //            //                        doWarnMess("The Journal entry for selected row(s) was posted successfully.");
        //            //                    }
        //            //                }

        //            //                if (oCloseList.Contains(sHead) == false) {
        //            //                    ArrayList aFields = oGridN.doGetChangedFields(aRows(iIndex));
        //            //                    if (aFields.Contains("r.U_AEDate")) {
        //            //                        object oEDate = oGridN.doGetFieldValue("r.U_AEDate");
        //            //                        if (com.idh.utils.Dates.isValidDate(oEDate)) {
        //            //                            IDH_JOBENTR oWOH = new IDH_JOBENTR();
        //            //                            if (oWOH.doCheckAndCloseWO(sHead, sRow) == true) {
        //            //                                oCloseList.Add(sHead);
        //            //                            }
        //            //                        }
        //            //                    }
        //            //                }
        //            //            }
        //            //        }
        //            //    }
        //            //    doReLoadData(oForm, true);
        //            //}
        //        } else if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find")) {
        //            doReLoadData(oForm, true);
        //        }
        //        BubbleEvent = false;
        //    }
        //}
        //public override void doButtonID2(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (pVal.BeforeAction == true) {
        //        doReLoadData(oForm, true);
        //    }
        //}

        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCloseForm(oForm,ref BubbleEvent);
            UpdateGrid.doRemoveGrid(oForm, "LINESGRID");
        }
        public override void doClose() {
        }
        protected void doGridDoubleClick(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            //UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
            //if (pVal.Row >= 0) {

            //    string sRowCode = oGridN.doGetFieldValue("r." + IDH_ENQITEM._Code).ToString();
            //    string sEnqID = oGridN.doGetFieldValue("r." + IDH_ENQITEM._EnqId).ToString();

            //    ArrayList oData = new ArrayList();
            //    if (sEnqID.Length == 0) {
            //        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Valid row must be selected.", "ERUSJOBS", null);
            //    } else {
            //        if (oGridN.doCheckIsSameCol(pVal.ColUID, "r." + IDH_ENQITEM._WOQID) || oGridN.doCheckIsSameCol(pVal.ColUID, "r." + IDH_ENQITEM._WOQLineID)) {
            //            //Open WOQ                      
            //            com.isb.forms.Enquiry.WOQuote oOOForm = new com.isb.forms.Enquiry.WOQuote(null, oForm.UniqueID, null);
            //            oOOForm.WOQID= oGridN.doGetFieldValue("r." + IDH_ENQITEM._WOQID).ToString();
            //            oOOForm.bLoadWOQ= true;
            //           // oOOForm.bLoadWOQ
            //            oOOForm.bLoadReadOnly = false;
            //            oOOForm.Handler_DialogButton1Return = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQButton1Return);
            //            oOOForm.doShowModal(oForm);
            //            oOOForm.DBWOQ.SBOForm = oOOForm.SBOForm;
            //        } else {//if (oGridN.doCheckIsSameCol(pVal.ColUID, "r." + IDH_ENQITEM._WOQID) || oGridN.doCheckIsSameCol(pVal.ColUID, "r." + IDH_ENQITEM._WOQLineID)) {
            //            //Open Enquiry
            //            com.isb.forms.Enquiry.Enquiry oOOForm = new com.isb.forms.Enquiry.Enquiry(null, oForm.UniqueID, null);
            //            oOOForm.EnquiryID = oGridN.doGetFieldValue("r." + IDH_ENQITEM._EnqId).ToString();
            //            oOOForm.bLoadEnquiry = true;
            //            oOOForm.bLoadReadOnly = false;
            //            oOOForm.Handler_DialogButton1Return = new com.idh.forms.oo.Form.DialogReturn(doHandleEnquiryButton1Return);
            //            oOOForm.doShowModal(oForm);
            //            oOOForm.DBEnquiry.SBOForm = oOOForm.SBOForm;

            //        }
            //    }
            //}
        }

        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            //if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK) {
            //    if (pVal.ItemUID == "LINESGRID") {
            //        if (pVal.BeforeAction == false) {
            //            doGridDoubleClick(oForm, ref pVal);
            //        }
            //    }
            //} else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT && !pVal.BeforeAction) {
            //    //		oData.GetType()	{Name = "MenuEventClass" FullName = "SAPbouiCOM.MenuEventClass"}	System.Type {System.RuntimeType}
            //    SAPbouiCOM.MenuEventClass oData = (SAPbouiCOM.MenuEventClass)pVal.oData;
            //    if (oData.MenuUID == IDHGrid.GRIDMENUSORTASC) {
            //        string sField = pVal.oGrid.doFindDBField(pVal.ColUID).Trim();
            //        if (sField.Length > 0 && (sField == "r." + IDH_ENQITEM._Code || sField == "r." + IDH_ENQITEM._EnqId || sField == "r." + IDH_ENQITEM._WOQID || sField == "r." + IDH_ENQITEM._WOQLineID)) {
            //            UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
            //            string sFilter = oGridN.getRequiredFilter();
            //            doSetCustomeFilter(oGridN);

            //            oGridN.setOrderValue("Cast( " + sField + " As Int) ");
            //            oGridN.doReloadData("ASC", false);
            //            doFillGridCombos(oGridN);
            //            oGridN.doApplyRules();
            //            oGridN.setRequiredFilter(sFilter);                       
            //            return false;
            //        }
            //    } else if (oData.MenuUID == IDHGrid.GRIDMENUSORTDESC) {
            //        string sField = pVal.oGrid.doFindDBField(pVal.ColUID).Trim();
            //        if (sField.Length > 0 && (sField == "r." + IDH_ENQITEM._Code || sField == "r." + IDH_ENQITEM._EnqId || sField == "r." + IDH_ENQITEM._WOQID || sField == "r." + IDH_ENQITEM._WOQLineID)) {
            //            UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
            //            string sFilter = oGridN.getRequiredFilter();
            //            doSetCustomeFilter(oGridN);

            //            oGridN.setOrderValue("Cast( " + sField + " As Int) ");
            //            oGridN.doReloadData("DESC", false);
            //            doFillGridCombos(oGridN);
            //            oGridN.doApplyRules();
            //            oGridN.setRequiredFilter(sFilter);
            //            return false;
            //        }
            //    }
            //} else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SORT && pVal.BeforeAction == false) {
            //    UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
            //    doFillGridCombos(oGridN);
            //    oGridN.doApplyRules();
            //}
            return base.doCustomItemEvent(oForm, ref pVal);
        }
        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "IDH_SHWRPS" && !pVal.BeforeAction) {
                UpdateGrid oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");
                if (oGridN == null) {
                    oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");
                }
                 Hashtable oParams =new Hashtable();
                        //oParams.Add("OrdNum", sCode)
                 oParams.Add("RowNum", getParentSharedDataAsString(oForm,"ROWNUM"));
                string sCallerForm = oForm.TypeEx;
                string sReportName = "";
                for (int iRows = 0; iRows <= oGridN.getRowCount() - 1; iRows++) {
                    string sSelected = oGridN.doGetFieldValue(3, iRows).ToString();
                    if (sSelected == "Y") {
                        sReportName = oGridN.doGetFieldValue("r." + IDH_WRCONFIG._Val, iRows).ToString();
                        IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportName, "TRUE", "FALSE", "1", ref oParams, ref sCallerForm);
                    }
                }
            }
            return base.doItemEvent(oForm, ref pVal, ref BubbleEvent);
        }
        //public bool doHandleEnquiryButton1Return(com.idh.forms.oo.Form oDialogForm) {
        //    try {
        //        com.isb.forms.Enquiry.Enquiry oOOForm = (com.isb.forms.Enquiry.Enquiry)oDialogForm;
        //        doReLoadData(oOOForm.SBOParentForm, true);
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doHandleEnquiryButton1Return" });
        //    }
        //    return true;
        //}
        //public bool doHandleWOQButton1Return(com.idh.forms.oo.Form oDialogForm) {
        //    try {
        //        com.isb.forms.Enquiry.WOQuote oOOForm = (com.isb.forms.Enquiry.WOQuote)oDialogForm;
        //        doReLoadData(oOOForm.SBOParentForm, true);
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doHandleWOQButton1Return" });
        //    }
        //    return true;
        //}
    }//end of class
}//end of namespace
