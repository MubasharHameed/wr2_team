﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;

using IDHAddOns.idh.controls;
//using com.idh.bridge.reports;
//using com.idh.utils.Conversions;
using com.idh.bridge;
using com.idh.bridge.data;
//using WR1_Grids.idh.controls.grid;
using com.idh.bridge.lookups;
using com.idh.bridge.action;
using com.idh.controls;
using com.idh.controls.strct;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry.Manager {
    class EnquiryManager : 
        IDHAddOns.idh.forms.Base {

        public EnquiryManager(IDHAddOns.idh.addon.Base oParent,string sParent, int iMenuPosition)
            : base(oParent, "IDHENQMGR", sParent, iMenuPosition, "ENQ_EnquiryManager.srf", true, true, false, "Enquiry Manager", load_Types.idh_LOAD_NORMAL) {
        }

        protected virtual void doTheGridLayout(UpdateGrid oGridN) {
            if (com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", false)) {
                string sFormTypeId = oGridN.getSBOForm().TypeEx;
                idh.dbObjects.User.IDH_FORMSET oFormSettings = (idh.dbObjects.User.IDH_FORMSET)getWFValue("FRMSET", sFormTypeId + "." + oGridN.GridId);
                if (oFormSettings == null) {
                    oFormSettings = new idh.dbObjects.User.IDH_FORMSET();
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, "");
                    setWFValue("FRMSET", sFormTypeId + "." + oGridN.GridId, oFormSettings);
                    oFormSettings.doAddFieldToGrid(oGridN);
                    doSetListFields(oGridN);
                    oFormSettings.doSyncDB(oGridN);
                } else {
                    if (oFormSettings.SkipFormSettings) {
                        doSetListFields(oGridN);
                    } else {
                        oFormSettings.doAddFieldToGrid(oGridN);
                    }
                }
            } else {
                doSetListFields(oGridN);
            }
        }

        protected void doSetListFields(UpdateGrid moGrid) {

            moGrid.doAddListField( IDH_ENQITEM._EnqId, "Enquiry ID", false, -1, null, null);            
            moGrid.doAddListField( IDH_ENQITEM._Code, "Row ID", false, 20, null, null);
            //moGrid.doAddListField( IDH_ENQITEM._Name, "Name", false, 0, null, null);

            //moGrid.doAddListField( IDH_ENQITEM._Code, "Enquiry ID", false, -1, null, null);
            moGrid.doAddListField( IDH_ENQUIRY._CardCode, "Customer code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            moGrid.doAddListField( IDH_ENQUIRY._CardName, "Customer Name", false, -1, null, null);

            moGrid.doAddListField( IDH_ENQUIRY._Recontact, "Date", false, -1, null, null);
            
            
            moGrid.doAddListField( IDH_ENQUIRY._Address, "Address", false, -1, null, null);
            moGrid.doAddListField( IDH_ENQUIRY._Street, "Street", false, -1, null, null);
            moGrid.doAddListField( IDH_ENQUIRY._Block, "Block", false, 0, null, null);
            moGrid.doAddListField( IDH_ENQUIRY._City, "City", false, -1, null, null);
            moGrid.doAddListField( IDH_ENQUIRY._Country, "Country", false, -1, null, null);
            moGrid.doAddListField( IDH_ENQUIRY._County, "County", false, -1, null, null);
            moGrid.doAddListField( IDH_ENQUIRY._State, "State", false, 0, null, null);
            moGrid.doAddListField( IDH_ENQUIRY._ZipCode, "Postcode", false, -1, null, null);

            moGrid.doAddListField( IDH_ENQUIRY._Desc, "Description", false, -1, null, null);
            
            moGrid.doAddListField( IDH_ENQUIRY._CName, "Contact Name", false, -1, null, null);
            moGrid.doAddListField( IDH_ENQUIRY._E_Mail, "Email", false, -1, null, null);
            moGrid.doAddListField( IDH_ENQUIRY._Phone1, "Phone", false, -1, null, null);
            moGrid.doAddListField( IDH_ENQUIRY._Notes, "Notes", false, -1, null, null);

            moGrid.doAddListField( IDH_ENQUIRY._Source, "Source", false, -1, ListFields.LISTTYPE_COMBOBOX, null);

            moGrid.doAddListField( IDH_ENQUIRY._User, "User", false, -1, null, null);

            moGrid.doAddListField( IDH_ENQUIRY._ClgCode, "Activity", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_ContactWithCustAndVend);
            moGrid.doAddListField( IDH_ENQUIRY._AttendUser, "Assigned to", false, -1, null, null);


            moGrid.doAddListField( IDH_ENQITEM._AddItm, "A", false, 30, null, null);
            moGrid.doAddListField( IDH_ENQITEM._PCode, "Linked ID", false, 30, null, null);

            moGrid.doAddListField( IDH_ENQITEM._ItemCode, "Item Code", false, 120, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            moGrid.doAddListField( IDH_ENQITEM._ItemDesc, "Item Name", false, 200, null, null);

            moGrid.doAddListField( IDH_ENQITEM._ItemName, "Description(F)", false, 200, null, null);
            moGrid.doAddListField( IDH_ENQITEM._WstGpCd, "Waste Group", false, 120, null, null);
            moGrid.doAddListField( IDH_ENQITEM._WstGpNm, "Waste Group Name", true, 0, null, null);
            moGrid.doAddListField( IDH_ENQITEM._EstQty, "Estimated Qty", false, 100, null, null);
            moGrid.doAddListField( IDH_ENQITEM._UOM, "UOM", false, 50, ListFields.LISTTYPE_COMBOBOX, null);
            moGrid.doAddListField( "H"+ IDH_ENQUIRY._Status, "Doc Status", false, 70, ListFields.LISTTYPE_COMBOBOX, null);
            moGrid.doAddListField( IDH_ENQITEM._Status, "Row Status", false, 70, ListFields.LISTTYPE_COMBOBOX, null);

            moGrid.doAddListField( IDH_ENQITEM._WOQID, "WOQ", false, 30, null, null);
            moGrid.doAddListField( IDH_ENQITEM._WOQLineID, "WOQ Line Id", false, 0, null, null);

            moGrid.doAddListField( IDH_ENQITEM._Sort, "Sort", false, 0, null, null);

        }


        protected void doSetGridFilters(UpdateGrid moGridN) {

            moGridN.doAddFilterField("IDH_DATEF", "U_Recontact", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10);
            moGridN.doAddFilterField("IDH_DATET", "U_Recontact", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10);
            moGridN.doAddFilterField("IDH_CUST", "U_CardCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_NAME", "U_CardName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_ENQSTS", "U_Status", SAPbouiCOM.BoDataType.dt_SHORT_NUMBER, "=", 11);
            moGridN.doAddFilterField("IDH_ACTVTY", "U_ClgCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_ASGNTO", "U_User", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_USER", "U_AttendUser", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_ENQID", "U_EnqID", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_ROWNO", "Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_ADDR", "U_Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_POSTCD", "U_ZipCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_CONPH", "U_Phone1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_CONNM", "U_CName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_EMAIL", "U_E_Mail", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_STREET", "U_Street", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WSCD", "U_ItemCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WSFDC", "U_ItemDesc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WSDC", "U_ItemName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WSGPCD", "U_WstGpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WSGPNM", "U_WstGpNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            doAddUF(moGridN.getSBOForm(), "IDH_SHADIT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, false, false);
            doAddUF(moGridN.getSBOForm(), "IDH_SEARCH", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 250, false, false);
        
        }

        //*** Add event filters to avoid receiving all events from SBO
        protected override void doSetEventFilters() {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE);
        }

        protected override void doReadInputParams(SAPbouiCOM.Form oForm) {
            //base.doReadInputParams(oForm);
            if (getHasSharedData(oForm)) {
                UpdateGrid oGridN = default(UpdateGrid);
                oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");

                int iIndex = 0;
                for (iIndex = 0; iIndex <= oGridN.getGridControl().getFilterFields().Count - 1; iIndex++) {
                    com.idh.controls.strct.FilterField oField = (com.idh.controls.strct.FilterField)(oGridN.getGridControl().getFilterFields()[iIndex]);
                    string sFieldName = oField.msFieldName;
                    string sFieldValue = getParentSharedData(oForm, sFieldName).ToString();
                    try {
                        setUFValue(oForm, sFieldName, sFieldValue);
                    } catch (Exception ex) {
                    }
                }
            }
        }

        //** Create the form
        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            try {
                oForm.Title = gsTitle;
                int iWidth = oForm.Width;

                UpdateGrid oGridN = new UpdateGrid(this, oForm, "LINESGRID", 7, 100, iWidth - 20, 255, 0,0);
                
                oGridN.getSBOGrid().SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto;
                oForm.AutoManaged = false;
                oGridN.getSBOItem().AffectsFormMode = false;

                oForm.EnableMenu(Config.NAV_ADD, false);
                oForm.EnableMenu(Config.NAV_FIND, false);
                oForm.EnableMenu(Config.NAV_FIRST, false);
                oForm.EnableMenu(Config.NAV_NEXT, false);
                oForm.EnableMenu(Config.NAV_PREV, false);
                oForm.EnableMenu(Config.NAV_LAST, false);

                //Set Documents button caption on Job Managers
                SAPbouiCOM.Item oItm = default(SAPbouiCOM.Item);
                SAPbouiCOM.Button oBtn = default(SAPbouiCOM.Button);
                try {
                    oItm = oForm.Items.Item("IDH_DOCS");
                    if (oItm != null) {
                        oBtn = (SAPbouiCOM.Button)oItm.Specific;
                        oBtn.Caption = Config.ParameterWithDefault("BTDOCCAP", "Documents");
                    }
                } catch (Exception ex) {
                }

                

                oForm.SupportedModes = Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_All);
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);
                base.doCompleteCreate(ref oForm, ref BubbleEvent);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDLF", null);
                BubbleEvent = false;
            }
        }

        protected void doSetMode(SAPbouiCOM.Form oForm, SAPbouiCOM.BoFormMode iMode) {
            try {
                object sFormType = getParentSharedData(oForm, "FTYPE");
                bool bIsSearch = false;
                if ((sFormType != null) && sFormType.Equals("SEARCH") == true) {
                    bIsSearch = true;
                }

                if (iMode == SAPbouiCOM.BoFormMode.fm_FIND_MODE) {
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = Translation.getTranslatedWord("Find");
                    if (bIsSearch) {
                        oForm.Items.Item("2").Visible = true;
                    } else {
                        oForm.Items.Item("2").Visible = false;
                    }
                } else if (iMode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = Translation.getTranslatedWord("Update");
                    oForm.Items.Item("2").Visible = true;
                }
            } catch (Exception ex) {
            }
        }
        private void doFillHeaderCombos(SAPbouiCOM.Form oForm) {
            com.uBC.utils.FillCombos.FillWR1StatusNew(oForm.Items.Item("IDH_ENQSTS").Specific, 101);
        }

        public  string getListRequiredStr(SAPbouiCOM.Form OForm) {
            return " 1=1 ";
            //return "r." + IDH_ENQITEM._EnqId + " = h.Code And hstatus.U_ObjType=101 And hstatus.U_Value=h." + IDH_ENQUIRY._Status +
            //    " And rstatus.U_ObjType=102 And rstatus.U_Value=r." + IDH_ENQITEM._Status;// +
               // " And hsource.U_ObjType=108 And (hsource.U_Value=h." + IDH_ENQUIRY._Source + " Or h." + IDH_ENQUIRY._Source+ " Is Null";
        }

        private void doFillGridCombos(UpdateGrid moGrid) {
            SAPbouiCOM.ComboBoxColumn oCombo;

            int iIndex = moGrid.doIndexFieldWC(IDH_ENQITEM._UOM);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            com.uBC.utils.FillCombos.UOMCombo(oCombo);

            iIndex = moGrid.doIndexFieldWC( IDH_ENQITEM._Status);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            com.uBC.utils.FillCombos.FillWR1StatusNew(oCombo, 102);

            iIndex = moGrid.doIndexFieldWC("H" + IDH_ENQUIRY._Status);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            com.uBC.utils.FillCombos.FillWR1StatusNew(oCombo, 101);

            iIndex = moGrid.doIndexFieldWC( IDH_ENQUIRY._Source);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            com.uBC.utils.FillCombos.FillWR1StatusNew(oCombo, 108);

            //FillCombos.FillWR1StatusNew(Items.Item("IDH_STATUS").Specific, 101);
        }
        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {

            oForm.Top = goParent.goApplication.Desktop.Height - (oForm.Height + 130);
            oForm.Left = 200;
            
            UpdateGrid oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");

            if (oGridN == null) {
                //int iWidth = oForm.Width;
                oGridN = new UpdateGrid(this, oForm, "LINESGRID"); //, 7, 100, iWidth - 20, 255, 0, 0);
            }

            //Required tables
            oGridN.doAddGridTable(new GridTable("ENQ_MANAGER", "", "Code", true, true), true);
            /*
            oGridN.doAddGridTable(new GridTable(IDH_ENQITEM.TableName, "r", "Code", true, true), true);
            oGridN.doAddGridTable(new GridTable(IDH_ENQUIRY.TableName, "h", "Code", true, true));
            oGridN.doAddGridTable(new GridTable("@IDH_WRSTUSNW", "hstatus", "Code", true, true));
            oGridN.doAddGridTable(new GridTable("@IDH_WRSTUSNW", "rstatus", "Code", true, true));
            */
            doSetGridFilters(oGridN);
            oGridN.setRequiredFilter(getListRequiredStr(oForm));
            oGridN.doSetDoCount(true);
            oGridN.doSetBlankLine(false);

            //Set the required List Fields
            doTheGridLayout(oGridN);


            doFillHeaderCombos(oForm);

            bool bHasParams = false;
            if (getHasSharedData(oForm)) {
                doReadInputParams(oForm);
                bHasParams = true;

                string sCardCode = getParentSharedData(oForm, "CARDCODE").ToString();
                if ((sCardCode != null) && sCardCode.Length > 0) {
                    string sExtraReqFilter = oGridN.getRequiredFilter();

                    if (sExtraReqFilter.Length > 0) {
                        sExtraReqFilter = sExtraReqFilter + " AND ";
                    }

                    sExtraReqFilter = sExtraReqFilter + " ("+IDH_ENQUIRY._CardCode+" LIKE '" + sCardCode + "%') ";

                    oGridN.setRequiredFilter(sExtraReqFilter);
                }
            }

            if (bHasParams == false) {
                oGridN.setInitialFilterValue("Code = '-100'");
            }
        }

        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            doReLoadData(oForm, !getHasSharedData(oForm));
        }

        
        //** The Initializer
        protected void doReLoadData(SAPbouiCOM.Form oForm, bool bIsFirst)
		{
			try {
				doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);

                UpdateGrid oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");
				string sFilter = oGridN.getRequiredFilter();
				doSetCustomeFilter(oGridN);
				//doUpdateStatuses(oGridN);
				//doSetFormSpecificFilters(oGridN);
                //if (Config.ParameterAsBool("ENBODRWO", false) && !Config.ParameterAsBool("FORMSET", false))
                oGridN.setOrderValue("Cast(" + IDH_ENQITEM._EnqId + " as Int)," + IDH_ENQITEM._Sort  + " ");
                
                oGridN.doReloadData("", false, true);
                doFillGridCombos(oGridN);
                oGridN.setRequiredFilter(sFilter);
				
			} catch (Exception ex) {
				com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBL",  null );
			}
		}
        protected void doSetCustomeFilter(UpdateGrid oGridN) {
            SAPbouiCOM.Form oForm = oGridN.getSBOForm();
            if (getItemValue(oForm, "IDH_SHADIT").ToString().Trim() == "Y") {
                //string sStatusQry = IDH_ENQITEM._AddItm + "='A'";//doGetStatusFilterQueryDynamic(getItemValue(oForm, "IDH_PROG").ToString.Trim.ToUpper);
                //oGridN.setRequiredFilter(oGridN.getRequiredFilter() + " And " + sStatusQry);
            } else {
                string sStatusQry = IDH_ENQITEM._AddItm + "=''";//doGetStatusFilterQueryDynamic(getItemValue(oForm, "IDH_PROG").ToString.Trim.ToUpper);
                oGridN.setRequiredFilter(oGridN.getRequiredFilter() + " And " + sStatusQry);
            }
            if (getItemValue(oForm, "IDH_SEARCH").ToString().Trim() != string.Empty) {
                string sSearchWord = getItemValue(oForm, "IDH_SEARCH").ToString().Trim().Replace("*", "%").Replace("'", "''");
                string sExistingReqFilter = oGridN.getRequiredFilter();
                string sSQL = "";
                bool bAddNot = false;
                string sNotOp = "";

                if (sSearchWord.StartsWith("!")) {
                    bAddNot = true;
                    sSearchWord = sSearchWord.TrimStart('!');
                    sNotOp = " NOT ";
                }
                if (sSearchWord.Contains(",")) {
                    string[] aList = sSearchWord.Split(',');
                    sSearchWord = "";
                    int i = 0;
                    for (i = 0; i < aList.Length; i++) {
                      //  sSearchWord += "'" + aList[i].Trim() + "', ";
                        
                        sSQL += "¬  (" +
                                    "" + IDH_ENQITEM._Code + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQITEM._EnqId + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQUIRY._Address + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQUIRY._AttendUser + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQUIRY._Block + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQUIRY._CardCode + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQUIRY._CardName + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQUIRY._City + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_ENQUIRY._ClgCode + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQUIRY._CName + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQUIRY._Desc + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQUIRY._E_Mail + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQUIRY._Notes + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQUIRY._Phone1 + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + "HSourceDesc"  + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQUIRY._Phone1 + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    " HStatusDesc " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQUIRY._Street + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQUIRY._User + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQUIRY._ZipCode + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQITEM._ItemCode + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQITEM._ItemDesc + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQITEM._ItemName + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQITEM._PCode + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    " RStatusDesc " + sNotOp + "Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQITEM._UOM + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "(" + IDH_ENQITEM._UOM + sNotOp + "  IN  (Select UnitDisply from OWGT WITH(NOLOCK) where UnitName Like " + "'" + aList[i].Trim() + "'" + ") ) OR " +
                                    "" + IDH_ENQITEM._WOQID + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQITEM._WOQLineID + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQITEM._WstGpCd + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_ENQITEM._EstQty +" As NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "" + IDH_ENQITEM._WstGpNm + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + "  " +
                                     ")";
                    }
                    sSQL = sSQL.Trim().TrimStart('¬').Trim();
                    sSQL = sSQL.Replace("¬", " OR ");
                    //sSearchWord ="("+ sSearchWord.Trim().TrimEnd(',').Trim() +")";
                    //sSQL = "  (" +
                    //    "" + IDH_ENQUIRY._Address + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQUIRY._AttendUser + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQUIRY._AttendUser + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQUIRY._Block + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQUIRY._CardCode + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQUIRY._CardName + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQUIRY._City + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "Cast(" + IDH_ENQUIRY._ClgCode + " As NVARCHAR(MAX)) "+ sNotOp +" IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQUIRY._CName + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQUIRY._Desc + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQUIRY._E_Mail + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    //"" + IDH_ENQUIRY._Notes + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQUIRY._Phone1 + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQUIRY._Source + sNotOp + "  IN  (Select hsource.U_Value from [@IDH_WRSTUSNW] hsource WITH(NOLOCK) where hsource.U_ObjType = 108 And hsource.U_Desc  in " + sSearchWord + ") OR " +
                    //    "" + IDH_ENQUIRY._Phone1 + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    " HStatusDesc "+ sNotOp +" IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQUIRY._Phone1 + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQUIRY._Street + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQUIRY._User + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQUIRY._ZipCode + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQITEM._ItemCode + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQITEM._ItemDesc + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQITEM._ItemName + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQITEM._PCode + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    " RStatusDesc "+ sNotOp +" IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQITEM._UOM + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "(" + IDH_ENQITEM._UOM + sNotOp + "  IN  (Select UnitDisply from OWGT where UnitName In " + sSearchWord + ") ) OR " +
                    //    "" + IDH_ENQITEM._WOQID + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQITEM._WOQLineID + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQITEM._WstGpCd + sNotOp + "  IN  " + sSearchWord + " OR " +
                    //    "" + IDH_ENQITEM._WstGpNm + sNotOp + "  IN  " + sSearchWord + " " +
                    //        ")";

                    if (bAddNot)
                        sSQL = sSQL.Replace(" OR ", " AND "); 
                    sSQL = " And " + sSQL;
                
                } else {
                    sSearchWord = "'" + sSearchWord + "'";
                    sSQL = "  (" +
                                "" + IDH_ENQITEM._Code + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQITEM._EnqId + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQUIRY._Address + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQUIRY._AttendUser  + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQUIRY._Block + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQUIRY._CardCode  + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQUIRY._CardName  + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQUIRY._City  + sNotOp + " Like " + sSearchWord + " OR " +
                                "Cast(" + IDH_ENQUIRY._ClgCode + " AS NVARCHAR(MAX)) "+ sNotOp +" Like " + sSearchWord + " OR " +
                                "" + IDH_ENQUIRY._CName  + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQUIRY._Desc  + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQUIRY._E_Mail  + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQUIRY._Notes  + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQUIRY._Phone1  + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQUIRY._Source  + sNotOp + "  IN  (Select hsource.U_Value from [@IDH_WRSTUSNW] hsource WITH(NOLOCK) where hsource.U_ObjType = 108 And hsource.U_Desc  Like " + sSearchWord + ") OR " +
                                "" + IDH_ENQUIRY._Phone1  + sNotOp + " Like " + sSearchWord + " OR " +
                                " HStatusDesc " + sNotOp +" Like " + sSearchWord + " OR " +
                                "" + IDH_ENQUIRY._Street  + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQUIRY._User  + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQUIRY._ZipCode  + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQITEM._ItemCode  + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQITEM._ItemDesc  + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQITEM._ItemName  + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQITEM._PCode  + sNotOp + " Like " + sSearchWord + " OR " +
                                " RStatusDesc "+ sNotOp +"Like " + sSearchWord + " OR " +
                                "" + IDH_ENQITEM._UOM + sNotOp + " Like " + sSearchWord + " OR " +
                                "(" + IDH_ENQITEM._UOM + sNotOp + "  IN  (Select UnitDisply from OWGT where UnitName Like " + sSearchWord + ") ) OR " +
                                "" + IDH_ENQITEM._WOQID + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQITEM._WOQLineID + sNotOp + " Like " + sSearchWord + " OR " +
                                "" + IDH_ENQITEM._WstGpCd + sNotOp + " Like " + sSearchWord + " OR " +
                                "Cast(" + IDH_ENQITEM._EstQty + " As NVARCHAR(MAX)) " + sNotOp + " Like " +  sSearchWord +  " OR " +
                                "" + IDH_ENQITEM._WstGpNm + sNotOp + " Like " + sSearchWord + "  " +
                                 ")";


                    if (bAddNot)
                        sSQL = sSQL.Replace(" OR ", " AND ");
                        sSQL = " And " + sSQL;
                }
                oGridN.setRequiredFilter(oGridN.getRequiredFilter() + " " + sSQL);
            }
        }

        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == true) {
                if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update") || oForm.Mode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
                    ////Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    //UpdateGrid oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");
                    //ArrayList aRows = oGridN.doGetChangedRows();
                    //ArrayList oCloseList = new ArrayList();
                    //if (doSaveOrderRows(oForm) == true) {
                    //    string sHead = null;
                    //    string sRow = null;
                    //    string sSOStatus = null;
                    //    string sPOStatus = null;

                    //    if ((aRows != null)) {
                    //        bool bDoInBack = Config.INSTANCE.getParameterAsBool("MDCMIBG", false);
                    //        if (bDoInBack == false || oGridN.mbForceRealTimeBilling == true) {
                    //            for (int iIndex = 0; iIndex <= aRows.Count - 1; iIndex++) {
                    //                oGridN.setCurrentDataRowIndex(aRows(iIndex));
                    //                sHead = oGridN.doGetFieldValue("r." + IDH_JOBSHD._JobNr);
                    //                sRow = oGridN.doGetFieldValue("r." + IDH_JOBSHD._Code);
                    //                sSOStatus = oGridN.doGetFieldValue("r." + IDH_JOBSHD._Status);
                    //                sPOStatus = oGridN.doGetFieldValue("r." + IDH_JOBSHD._PStat);

                    //                if (sSOStatus.StartsWith(FixedValues.getDoOrderStatus()) || sPOStatus.StartsWith(FixedValues.getDoOrderStatus())) {
                    //                    MarketingDocs.doOrders("@IDH_JOBENTR", "@IDH_JOBSHD", "WO", sHead, sRow);
                    //                }

                    //                if (sSOStatus.StartsWith(FixedValues.getDoInvoiceStatus()) || sSOStatus.StartsWith(FixedValues.getDoPInvoiceStatus())) {
                    //                    MarketingDocs.doInvoicesAR("@IDH_JOBENTR", "@IDH_JOBSHD", "WO", sHead, sRow);
                    //                }

                    //                if (sSOStatus.StartsWith(FixedValues.getDoRebateStatus()) || sPOStatus.StartsWith(FixedValues.getDoRebateStatus())) {
                    //                    MarketingDocs.doRebates("@IDH_JOBENTR", "@IDH_JOBSHD", "WO", sHead, sRow);
                    //                }

                    //                if (sSOStatus.StartsWith(FixedValues.getDoFocStatus())) {
                    //                    MarketingDocs.doFoc("@IDH_JOBENTR", "@IDH_JOBSHD", "WO", sHead, sRow);
                    //                }

                    //                MarketingDocs.doPayment("@IDH_JOBENTR", "@IDH_JOBSHD", "WO", sHead, sRow);

                    //                //CR 20150601: The DO Journals functionality build for USA Clients can now be used for all under new WR Config Key "NEWDOJRN" 
                    //                //'For USA release 
                    //                //If Config.INSTANCE.getParameterAsBool("USAREL", False) = True Then
                    //                if (Config.INSTANCE.getParameterAsBool("NEWDOJRN", false) == true) {
                    //                    bool sResult = MarketingDocs.doJournals("@IDH_JOBENTR", "@IDH_JOBSHD", "WO", sHead, sRow);
                    //                    if (sResult) {
                    //                        doWarnMess("The Journal entry for selected row(s) was posted successfully.");
                    //                    }
                    //                }

                    //                if (oCloseList.Contains(sHead) == false) {
                    //                    ArrayList aFields = oGridN.doGetChangedFields(aRows(iIndex));
                    //                    if (aFields.Contains("r.U_AEDate")) {
                    //                        object oEDate = oGridN.doGetFieldValue("r.U_AEDate");
                    //                        if (com.idh.utils.Dates.isValidDate(oEDate)) {
                    //                            IDH_JOBENTR oWOH = new IDH_JOBENTR();
                    //                            if (oWOH.doCheckAndCloseWO(sHead, sRow) == true) {
                    //                                oCloseList.Add(sHead);
                    //                            }
                    //                        }
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //    doReLoadData(oForm, true);
                    //}
                } else if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find")) {
                    doReLoadData(oForm, true);
                }
                BubbleEvent = false;
            }
        }
        public override void doButtonID2(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == true) {
                doReLoadData(oForm, true);
            }
        }

        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCloseForm(oForm,ref BubbleEvent);
            UpdateGrid.doRemoveGrid(oForm, "LINESGRID");
        }
        public override void doClose() {
        }
        protected void doGridDoubleClick(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
            if (pVal.Row >= 0) {

                string sRowCode = oGridN.doGetFieldValue("" + IDH_ENQITEM._Code).ToString();
                string sEnqID = oGridN.doGetFieldValue("" + IDH_ENQITEM._EnqId).ToString();

                ArrayList oData = new ArrayList();
                if (sEnqID.Length == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Valid row must be selected.", "ERUSJOBS", null);
                } else {
                    if (oGridN.doCheckIsSameCol(pVal.ColUID, "" + IDH_ENQITEM._WOQID) || oGridN.doCheckIsSameCol(pVal.ColUID, "" + IDH_ENQITEM._WOQLineID)) {
                        //Open WOQ                      
                        //com.isb.forms.Enquiry.WOQuote oOOForm = new com.isb.forms.Enquiry.WOQuote(null, oForm.UniqueID, null);
                        com.isb.forms.Enquiry.WOQuote oOOForm = new com.isb.forms.Enquiry.WOQuote(oForm.UniqueID, oGridN.doGetFieldValue("" + IDH_ENQITEM._WOQID).ToString());
                        oOOForm.WOQID= oGridN.doGetFieldValue("" + IDH_ENQITEM._WOQID).ToString();
                        oOOForm.bLoadWOQ= true;
                       // oOOForm.bLoadWOQ
                        oOOForm.bLoadReadOnly = false;
                        oOOForm.Handler_DialogButton1Return = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQButton1Return);
                        oOOForm.doShowModal(oForm);
                        oOOForm.DBWOQ.SBOForm = oOOForm.SBOForm;
                    } else {//if (oGridN.doCheckIsSameCol(pVal.ColUID, "r." + IDH_ENQITEM._WOQID) || oGridN.doCheckIsSameCol(pVal.ColUID, "r." + IDH_ENQITEM._WOQLineID)) {
                        //Open Enquiry
                        //com.isb.forms.Enquiry.Enquiry oOOForm = new com.isb.forms.Enquiry.Enquiry(null, oForm.UniqueID, null);
                        com.isb.forms.Enquiry.Enquiry oOOForm = new com.isb.forms.Enquiry.Enquiry(oForm.UniqueID, oGridN.doGetFieldValue("" + IDH_ENQITEM._EnqId).ToString());
                        //oOOForm.EnquiryID = oGridN.doGetFieldValue("" + IDH_ENQITEM._EnqId).ToString();
                        //oOOForm.bLoadEnquiry = true;
                        //oOOForm.bLoadReadOnly = false;
                        oOOForm.Handler_DialogButton1Return = new com.idh.forms.oo.Form.DialogReturn(doHandleEnquiryButton1Return);
                        oOOForm.doShowModal(oForm);
                        //oOOForm.DBEnquiry.SBOForm = oOOForm.SBOForm;
                    }
                }
            }
        }
         
        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK) {
                if (pVal.ItemUID == "LINESGRID") {
                    if (pVal.BeforeAction == false) {
                        doGridDoubleClick(oForm, ref pVal);
                    }
                }
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT && !pVal.BeforeAction) {
                //		oData.GetType()	{Name = "MenuEventClass" FullName = "SAPbouiCOM.MenuEventClass"}	System.Type {System.RuntimeType}
                SAPbouiCOM.MenuEventClass oData = (SAPbouiCOM.MenuEventClass)pVal.oData;
                if (oData.MenuUID == IDHGrid.GRIDMENUSORTASC) {
                    string sField = pVal.oGrid.doFindDBField(pVal.ColUID).Trim();
                    if (sField.Length > 0 && (sField == "" + IDH_ENQITEM._Code || sField == "" + IDH_ENQITEM._EnqId || sField == "" + IDH_ENQITEM._WOQID || sField == "" + IDH_ENQITEM._WOQLineID)) {
                        UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
                        string sFilter = oGridN.getRequiredFilter();
                        doSetCustomeFilter(oGridN);

                        oGridN.setOrderValue("Cast( " + sField + " As Int) ");
                        oGridN.doReloadData("ASC", false);
                        doFillGridCombos(oGridN);
                        oGridN.doApplyRules();
                        oGridN.setRequiredFilter(sFilter);
                        return false;
                    } else {
                        //string sField = pVal.oGrid.doFindDBField(pVal.ColUID).Trim();
                        //if (sField.Length > 0 && (sField == "" + IDH_ENQITEM._Code || sField == "" + IDH_ENQITEM._EnqId || sField == "" + IDH_ENQITEM._WOQID || sField == "" + IDH_ENQITEM._WOQLineID)) {
                        UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
                        string sFilter = oGridN.getRequiredFilter();
                        doSetCustomeFilter(oGridN);

                        oGridN.setOrderValue(sField );
                        oGridN.doReloadData("ASC", false);
                        doFillGridCombos(oGridN);
                        oGridN.doApplyRules();
                        oGridN.setRequiredFilter(sFilter);
                        return false;
                    }
                } else if (oData.MenuUID == IDHGrid.GRIDMENUSORTDESC) {
                    string sField = pVal.oGrid.doFindDBField(pVal.ColUID).Trim();
                    if (sField.Length > 0 && (sField == "" + IDH_ENQITEM._Code || sField == "" + IDH_ENQITEM._EnqId || sField == "" + IDH_ENQITEM._WOQID || sField == "" + IDH_ENQITEM._WOQLineID)) {
                        UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
                        string sFilter = oGridN.getRequiredFilter();
                        doSetCustomeFilter(oGridN);

                        oGridN.setOrderValue("Cast( " + sField + " As Int) ");
                        oGridN.doReloadData("DESC", false);
                        doFillGridCombos(oGridN);
                        oGridN.doApplyRules();
                        oGridN.setRequiredFilter(sFilter);
                        return false;
                    } else {
                        //string sField = pVal.oGrid.doFindDBField(pVal.ColUID).Trim();
                        //if (sField.Length > 0 && (sField == "" + IDH_ENQITEM._Code || sField == "" + IDH_ENQITEM._EnqId || sField == "" + IDH_ENQITEM._WOQID || sField == "" + IDH_ENQITEM._WOQLineID)) {
                        UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
                        string sFilter = oGridN.getRequiredFilter();
                        doSetCustomeFilter(oGridN);

                        oGridN.setOrderValue(sField);
                        oGridN.doReloadData("DESC", false);
                        doFillGridCombos(oGridN);
                        oGridN.doApplyRules();
                        oGridN.setRequiredFilter(sFilter);
                        return false;
                    }
                } 

            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SORT && pVal.BeforeAction == false) {
                UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
                doFillGridCombos(oGridN);
                oGridN.doApplyRules();
            }
            return true;
            //return base.doCustomItemEvent(oForm, ref pVal);
        }

        public bool doHandleEnquiryButton1Return(com.idh.forms.oo.Form oDialogForm) {
            try {
                com.isb.forms.Enquiry.Enquiry oOOForm = (com.isb.forms.Enquiry.Enquiry)oDialogForm;
                doReLoadData(oOOForm.SBOParentForm, true);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doHandleEnquiryButton1Return" });
            }
            return true;
        }
        public bool doHandleWOQButton1Return(com.idh.forms.oo.Form oDialogForm) {
            try {
                com.isb.forms.Enquiry.WOQuote oOOForm = (com.isb.forms.Enquiry.WOQuote)oDialogForm;
                doReLoadData(oOOForm.SBOParentForm, true);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doHandleWOQButton1Return" });
            }
            return true;
        }
    }//end of class
}//end of namespace
