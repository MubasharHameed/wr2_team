﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

using System.Collections;
///using System.Data;
//using System.Diagnostics;
//using System.IO;
using IDHAddOns.idh.controls;
using com.idh.bridge.data;
//using com.idh.dbObjects;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry
{
    public class LabItem : IDHAddOns.idh.forms.Base
    {

        protected string msKeyGen ="SEQLBITM";// "GENSEQ";

        #region Initialization

        public LabItem(IDHAddOns.idh.addon.Base oParent, string sParMenu, int iMenuPos)
            : base(oParent, "IDHLABITM", sParMenu, iMenuPos, "Enq_LabItem.srf", false, true, false, "Lab Items", load_Types.idh_LOAD_NORMAL)
        {
        }

        protected override void doSetEventFilters()
        {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);

        }

        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent)
        {
            try
            {
                oForm.Title = gsTitle;
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE;

                doAddUF(oForm, "IDHLABITM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100, false, false); 

                SAPbouiCOM.Item oItem = oForm.Items.Item("IDH_RECLIT");
                UpdateGrid oGridN = new UpdateGrid(this, oForm, "LINESGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane);
                //oGridN.getSBOGrid().CollapseLevel = 15;
                oGridN.getSBOItem().AffectsFormMode = false;
            }
            catch (Exception ex)
            {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
                BubbleEvent = false;
            }
        }

        #endregion

        #region LoadingDate

        public override void doBeforeLoadData(SAPbouiCOM.Form oForm)
        {
            string sLabItemId = string.Empty;
            if (getHasSharedData(oForm))
            {
                sLabItemId = getParentSharedData(oForm, "LABITEM").ToString();
                setUFValue(oForm, "IDHLABITM", sLabItemId);
                doClearParentSharedData(oForm);
            }

            UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
            if (oGridN == null)
            {
                oGridN = new UpdateGrid(this, oForm, "LINESGRID");
            }

            oGridN.doAddGridTable(new GridTable(IDH_LABITM.TableName, "LI", "Code", true), true);
            oGridN.doAddGridTable(new GridTable(IDH_LITMDTL.TableName, "LID", "U_LabCd", true), true);
            oGridN.setRequiredFilter("LI.Code = LID.U_LabCd And LI.U_Saved=1");
            oGridN.setOrderValue("LI.Code");
            oGridN.doSetDoCount(true);

            doSetFilterFields(oGridN);
            doSetListFields(oGridN);
            oGridN.doApplyRules();
        }

        protected override void doLoadData(SAPbouiCOM.Form oForm)
        {
            oForm.Freeze(true); 
            FilterGrid oGridN = (FilterGrid)FilterGrid.getInstance(oForm, "LINESGRID");

            string sLabItemCd = getUFValue(oForm, "IDHLABITM", true).ToString();
            if (sLabItemCd.Length > 0)
            {
                string sReqFilter = oGridN.getRequiredFilter();
                sReqFilter = sReqFilter + " AND LI.Code = '" + sLabItemCd + "' ";
                oGridN.setRequiredFilter(sReqFilter); 
                setUFValue(oForm, "IDHLABITM", "");
            }
            else
            {
                //oGridN.setRequiredFilter(string.Empty);
            }

            oGridN.doReloadData("", false, false);
            oGridN.doApplyRules();
            oForm.Freeze(false); 
        }

        public override void doFinalizeShow(SAPbouiCOM.Form oForm)
        {
            base.doFinalizeShow(oForm);
        }

        protected void doSetFilterFields(IDHAddOns.idh.controls.UpdateGrid oGridN)
        {
            //oGridN.doAddFilterField("IDH_DEPT", "U_DeptCode", SAPbouiCOM.BoDataType.dt_SHORT_NUMBER, "=", 10, Nothing)
            //oGridN.doAddFilterField("IDH_USERS", "U_UserName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "")
            //oGridN.doAddFilterField("IDH_FORMS", "U_FormID", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "")

            oGridN.doAddFilterField("IDH_ICode", "LI.U_LICode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, "");
            oGridN.doAddFilterField("IDH_IName", "LI.U_LIName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, "");
            oGridN.doAddFilterField("IDH_Whse", "LI.U_NewWhse", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, "");
            oGridN.doAddFilterField("IDH_UOM", "LI.U_UOM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, "");
            oGridN.doAddFilterField("IDH_LBTSK", "LI.U_LabTask", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, "");
            oGridN.doAddFilterField("IDH_BTCD", "LI.U_ItemCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, "");
            oGridN.doAddFilterField("IDH_GISSUE", "LI.U_GIssue", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "");
            oGridN.doAddFilterField("IDH_GRECPT", "LI.U_GReceipt", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "");
            oGridN.doAddFilterField("IDH_SRCTP", "LI.U_ObjectType", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, "");
            oGridN.doAddFilterField("IDH_SRCCD", "LI.U_ObjectCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 30, "");
        }

        protected void doSetListFields(IDHAddOns.idh.controls.UpdateGrid oGridN)
        {

            oGridN.doAddListField("LI.Code", "ID", false, -1, null, null);
            oGridN.doAddListField("LI.Name", "Name", false, 0, null, null);
            oGridN.doAddListField("LI.U_LICode", "Lab Item Code", false, -1, null, null);
            oGridN.doAddListField("LI.U_LIName", "Lab Item Name", false, -1, null, null);
            
            oGridN.doAddListField("LI.U_ObjectType", "Source Type", false, -1, null, null);
            //oGridN.doAddListField("(case when LI.U_ObjectType = 'WOQ' then 'WOQuote' else (case when LI.U_ObjectType = 'LPP' then 'Lab Pre-process' else (LI.U_ObjectType) end) end)", "Source Type", false, -1, null, null);

            //oGridN.doAddListField("(CASE WHEN LI.U_ObjectCd = '-1' THEN '-' ELSE LI.U_ObjectCd END )", "Source ID", false, -1, null, null);
            oGridN.doAddListField("LI.U_ObjectCd", "Source ID", false, -1, null, null);

            //Releated documents 
            oGridN.doAddListField("LI.U_LabTask", "Lab Task", false, -1, null, null);
            oGridN.doAddListField("LI.U_GIssue", "Goods Issue", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_GoodsIssue);
            oGridN.doAddListField("LI.U_GReceipt", "Goods Receipt", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_GoodsReceipt);
            oGridN.doAddListField("LI.U_ItemCode", "New Batch", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);

            oGridN.doAddListField("LI.U_UOM", "UOM", false, -1, null, null);
            oGridN.doAddListField("LI.U_Quantity", "Quantity", false, -1, null, null);
            oGridN.doAddListField("LI.U_NewWhse", "New Warehouse", false, -1, null, null);
            oGridN.doAddListField("LI.U_NewBinLoc", "New Bin Location", false, -1, null, null);
            oGridN.doAddListField("LI.U_Comments", "Comments", false, -1, null, null);
            oGridN.doAddListField("LI.U_CreatedBy", "Created By", false, -1, null, null);
            oGridN.doAddListField("LI.U_CreatedOn", "Created On", false, -1, null, null);
            oGridN.doAddListField("LI.U_LastUpdateBy", "Last Updated By", false, -1, null, null);
            oGridN.doAddListField("LI.U_LastUpdateOn", "Last Updated On", false, -1, null, null);
            oGridN.doAddListField("LID.U_LabCd", "LID Lab Code2", false, -1, null, null);
            oGridN.doAddListField("LID.U_PItemCd", "LID Parent ICode", false, -1, null, null);
            oGridN.doAddListField("LID.U_PItemNm", "LID Parent IName", false, -1, null, null);
            oGridN.doAddListField("LID.U_UOM", "LID UOM", false, -1, null, null);
            oGridN.doAddListField("LID.U_Quantity", "LID Quantity", false, -1, null, null);
            oGridN.doAddListField("LID.U_Whse", "LID Warehouse", false, -1, null, null);
            oGridN.doAddListField("LID.U_BinLoc", "LID BinLocation", false, -1, null, null);
        }
        
        #endregion

        #region UnloadingData

        public override void doClose()
        {
        }

        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent)
        {
            base.doCloseForm(oForm, ref BubbleEvent);
            FilterGrid.doRemoveGrid(oForm, "LINESGRID");
        }

        #endregion


        #region EventHandlers
        
        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            if (pVal.BeforeAction)
            {
                return;
            }
            //UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
            ////If oGridN.hasFieldChanged("U_Select") Then
            //Int32 _WOQID = getParentSharedDataAsInteger(oForm, "WOQID");
            //Int32 _EnqID = getParentSharedDataAsInteger(oForm, "ENQID");
            //com.idh.dbObjects.User.IDH_ENQUS moIDH_ENQUS = new com.idh.dbObjects.User.IDH_ENQUS();
            //for (Int16 iRow = 0; iRow <= oGridN.getRowCount() - 1; iRow++)
            //{

            //    if (!string.IsNullOrEmpty(oGridN.doGetFieldValue("Code", iRow).ToString()) && moIDH_ENQUS.getByKey(oGridN.doGetFieldValue("Code", iRow).ToString()))
            //    {
            //        moIDH_ENQUS.U_Answer = oGridN.doGetFieldValue("U_Answer", iRow).ToString();
            //    }
            //    else
            //    {
            //        moIDH_ENQUS.doAddEmptyRow(true, true, false);
            //        moIDH_ENQUS.U_Answer = oGridN.doGetFieldValue("U_Answer", iRow).ToString();
            //        if (_EnqID != 0)
            //            moIDH_ENQUS.U_EnqID = Convert.ToInt32(oGridN.doGetFieldValue("U_EnqID", iRow).ToString());
            //        moIDH_ENQUS.U_QsID = oGridN.doGetFieldValue("QID", iRow).ToString();
            //        moIDH_ENQUS.U_Question = oGridN.doGetFieldValue("Question", iRow).ToString();
            //        if (_WOQID != 0)
            //            moIDH_ENQUS.U_WOQID = Convert.ToInt32(oGridN.doGetFieldValue("U_WOQID", iRow).ToString());

            //    }
            //    moIDH_ENQUS.doProcessData();
            //    //End If
            //}
            doLoadData(oForm);

        }

        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            base.doItemEvent(oForm, ref pVal, ref BubbleEvent);
            if ((pVal.ItemUID == "IDH_WSTGRP") && pVal.EventType == SAPbouiCOM.BoEventTypes.et_COMBO_SELECT && pVal.BeforeAction == false)
            {
                doLoadData(oForm);
            }
            return true;
        }

        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal)
        {
            if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY)
            {
                IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");
                oGridN.doSetFieldValue("Name", pVal.Row, "1", true);
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
            }
            else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK)
            {
                if (pVal.ItemUID == "LINESGRID")
                {
                    if (pVal.BeforeAction == false)
                    {
                        IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");

                        if (pVal.ColUID.Equals("LI.U_LabTask"))
                        {
                            setSharedData(oForm, "LABTASK", oGridN.doGetFieldValue("LI.U_LabTask", pVal.Row));
                            goParent.doOpenForm("IDHLABTODO", oForm, false);
                        }
                        else if (pVal.ColUID.Contains("GridDotBtn2.png"))
                            goParent.doOpenModalForm("IDHATTACH", oForm);
                        else if (pVal.ColUID.Contains("GridDotBtn3.png"))
                            goParent.doOpenModalForm("IDHLABRESULTS", oForm);
                        else if (pVal.ColUID.Equals("LI.U_ObjectCd")) 
                        {
                            string sObjectType = oGridN.doGetFieldValue("LI.U_ObjectType", pVal.Row).ToString();
                            string sObjectCd = oGridN.doGetFieldValue("LI.U_ObjectCd", pVal.Row).ToString();
                            ArrayList oData = new ArrayList();
                            oData.Add("DoUpdate");
                            oData.Add(sObjectCd);
                            setSharedData(oForm, "ROWCODE", sObjectCd);
                            switch (sObjectType)
                            {
                                case "LPP": 
                                    //goParent.doOpenModalForm("IDHLABRESULTS", oForm);
                                    break; 
                                case "WOQ":
                                    //Get WOQ Header Code from WOQRow 
                                    IDH_WOQITEM oWOQRow = new IDH_WOQITEM();
                                    oWOQRow.getData(" Code = '" + sObjectCd + "'", oWOQRow.OrderByField); 

                                    //com.isb.forms.Enquiry.WOQuote oOOForm = new com.isb.forms.Enquiry.WOQuote(null, oForm.UniqueID, null);
                                    com.isb.forms.Enquiry.WOQuote oOOForm = new com.isb.forms.Enquiry.WOQuote(oForm.UniqueID, oWOQRow.U_JobNr);
                                    oOOForm.WOQID = oWOQRow.U_JobNr;
                                    oOOForm.bLoadWOQ = true;
                                    oOOForm.ENQRESULT = false;
                                    //oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQOKReturn);
                                    //oOOForm.Handler_DialogButton1Return = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQOKReturn);
                                    //oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQCancelReturn);
                                    oOOForm.doShowModal(oForm);
                                    break;
                                case "WOR":
                                    goParent.doOpenModalForm("IDHJOBS", oForm, oData);
                                    break; 
                                case "DPO":
                                    goParent.doOpenModalForm("IDH_DISPORD", oForm, oData);
                                    break;
                                default:
                                    break;
                            }
                        }

                    }
                }
            }
            return base.doCustomItemEvent(oForm, ref pVal);

        }

        #endregion

        #region CustomFunctions
        #endregion






    }
}