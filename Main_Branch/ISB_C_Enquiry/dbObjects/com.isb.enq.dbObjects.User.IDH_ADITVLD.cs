/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 01/10/2015 16:32:55
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User{
   [Serializable] 
	public class IDH_ADITVLD: com.isb.enq.dbObjects.Base.IDH_ADITVLD{ 

		public IDH_ADITVLD() : base() {
            msAutoNumKey = "WGADITVL";
		}
       
   	public IDH_ADITVLD( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	}
	}
}
