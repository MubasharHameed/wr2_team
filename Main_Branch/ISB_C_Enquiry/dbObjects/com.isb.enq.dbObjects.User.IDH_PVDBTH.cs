/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 28/09/2016 15:49:31
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_PVDBTH : com.isb.enq.dbObjects.Base.IDH_PVDBTH {

        public IDH_PVDBTH()
            : base() {
                msAutoNumKey = "SEQPVDBTH";
        }

        public IDH_PVDBTH(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oIDHForm, oForm) {
                msAutoNumKey = "SEQPVDBTH";
        }
    }
}
