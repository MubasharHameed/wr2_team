/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 29/03/2016 15:34:43
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User{
    [Serializable]
    public class IDH_WOQHVR : com.isb.enq.dbObjects.Base.IDH_WOQHVR {

        private IDH_WOQITVR moWOQItems;
        public IDH_WOQITVR WOQItems {
            get { return moWOQItems; }
            //set { moEnquiryItems = value; }
        }

        public IDH_WOQHVR()
            : base() {
            msAutoNumKey = "IDHWOQHV";
            moWOQItems = new IDH_WOQITVR(this);
        }

        public IDH_WOQHVR(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oIDHForm, oForm) {
            msAutoNumKey = "IDHWOQHV";
            moWOQItems = new IDH_WOQITVR(this);

        }


        public bool hasRows {
            get { return moWOQItems != null && moWOQItems.Count > 0; }
        }

        public IDH_WOQITVR getRowDB() {
            return moWOQItems;
        }

        public int getByWOQNumber(string sParentNo) {
            return getData(IDH_WOQHVR._BasWOQID + " = '" + sParentNo + '\'', IDH_WOQHVR._Version);
        }

        #region rowLoader
        public override void doClearBuffers() {
            base.doClearBuffers();
            if (MustLoadChildren)
                moWOQItems.doClearBuffers();
        }
        public override void doLoadChildren() {
            if (Code != null && Code.Length > 0) {
                moWOQItems.getByParentNumber(Code);
            } else {
                moWOQItems.doClearBuffers();
            }
        }
        #endregion
    }
}
