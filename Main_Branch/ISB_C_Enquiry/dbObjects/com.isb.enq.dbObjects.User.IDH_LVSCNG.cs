/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 02/11/2016 11:33:45
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_LVSCNG : com.isb.enq.dbObjects.Base.IDH_LVSCNG {

        public IDH_LVSCNG() : base() {
            msAutoNumKey = "SEQLVSCN";
        }

        public IDH_LVSCNG(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm) : base(oIDHForm, oForm) {
            msAutoNumKey = "SEQLVSCN";
        }
    }
}
