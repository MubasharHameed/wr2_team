/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 02/05/2017 09:12:34
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base {
    [Serializable]
    public class IDH_DISPRTCD : com.idh.dbObjects.DBBase {

        private Linker moLinker = null;
        public Linker ControlLinker {
            get { return moLinker; }
            set { moLinker = value; }
        }

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_DISPRTCD() : base("@IDH_DISPRTCD") {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_DISPRTCD(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm) : base(oForm, "@IDH_DISPRTCD") {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
         * Table name
         */
        public readonly static string TableName = "@IDH_DISPRTCD";

        /**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
        public readonly static string _Code = "Code";
        public string Code {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code() {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue) {
            return base.doValidation(_Code, oValue);
        }
        /**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
        public readonly static string _Name = "Name";
        public string Name {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name() {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue) {
            return base.doValidation(_Name, oValue);
        }
        /**
		 * Decription: Active
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Active
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Active = "U_Active";
        public string U_Active {
            get {
                return getValueAsString(_Active);
            }
            set { setValue(_Active, value); }
        }
        public string doValidate_Active() {
            return doValidate_Active(U_Active);
        }
        public virtual string doValidate_Active(object oValue) {
            return base.doValidation(_Active, oValue);
        }

        /**
		 * Decription: Average Tonnage per load and c
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AvgTngCmt
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _AvgTngCmt = "U_AvgTngCmt";
        public string U_AvgTngCmt {
            get {
                return getValueAsString(_AvgTngCmt);
            }
            set { setValue(_AvgTngCmt, value); }
        }
        public string doValidate_AvgTngCmt() {
            return doValidate_AvgTngCmt(U_AvgTngCmt);
        }
        public virtual string doValidate_AvgTngCmt(object oValue) {
            return base.doValidation(_AvgTngCmt, oValue);
        }

        /**
		 * Decription: Composting/Biological Treatme
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CmpBioTrt
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CmpBioTrt = "U_CmpBioTrt";
        public string U_CmpBioTrt {
            get {
                return getValueAsString(_CmpBioTrt);
            }
            set { setValue(_CmpBioTrt, value); }
        }
        public string doValidate_CmpBioTrt() {
            return doValidate_CmpBioTrt(U_CmpBioTrt);
        }
        public virtual string doValidate_CmpBioTrt(object oValue) {
            return base.doValidation(_CmpBioTrt, oValue);
        }

        /**
		 * Decription: COMAH pre-set
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_COMPrSt
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _COMPrSt = "U_COMPrSt";
        public string U_COMPrSt {
            get {
                return getValueAsString(_COMPrSt);
            }
            set { setValue(_COMPrSt, value); }
        }
        public string doValidate_COMPrSt() {
            return doValidate_COMPrSt(U_COMPrSt);
        }
        public virtual string doValidate_COMPrSt(object oValue) {
            return base.doValidation(_COMPrSt, oValue);
        }

        /**
		 * Decription: Description
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Descrp
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Descrp = "U_Descrp";
        public string U_Descrp {
            get {
                return getValueAsString(_Descrp);
            }
            set { setValue(_Descrp, value); }
        }
        public string doValidate_Descrp() {
            return doValidate_Descrp(U_Descrp);
        }
        public virtual string doValidate_Descrp(object oValue) {
            return base.doValidation(_Descrp, oValue);
        }

        /**
		 * Decription: Process Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DisProcCd
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _DisProcCd = "U_DisProcCd";
        public string U_DisProcCd {
            get {
                return getValueAsString(_DisProcCd);
            }
            set { setValue(_DisProcCd, value); }
        }
        public string doValidate_DisProcCd() {
            return doValidate_DisProcCd(U_DisProcCd);
        }
        public virtual string doValidate_DisProcCd(object oValue) {
            return base.doValidation(_DisProcCd, oValue);
        }

        /**
		 * Decription: Disposal Route Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DisRCode
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _DisRCode = "U_DisRCode";
        public string U_DisRCode {
            get {
                return getValueAsString(_DisRCode);
            }
            set { setValue(_DisRCode, value); }
        }
        public string doValidate_DisRCode() {
            return doValidate_DisRCode(U_DisRCode);
        }
        public virtual string doValidate_DisRCode(object oValue) {
            return base.doValidation(_DisRCode, oValue);
        }

        /**
		 * Decription: Treatment Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DisTrtCd
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _DisTrtCd = "U_DisTrtCd";
        public string U_DisTrtCd {
            get {
                return getValueAsString(_DisTrtCd);
            }
            set { setValue(_DisTrtCd, value); }
        }
        public string doValidate_DisTrtCd() {
            return doValidate_DisTrtCd(U_DisTrtCd);
        }
        public virtual string doValidate_DisTrtCd(object oValue) {
            return base.doValidation(_DisTrtCd, oValue);
        }

        /**
		 * Decription: Do Weight
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_doWeight
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _doWeight = "U_doWeight";
        public string U_doWeight {
            get {
                return getValueAsString(_doWeight);
            }
            set { setValue(_doWeight, value); }
        }
        public string doValidate_doWeight() {
            return doValidate_doWeight(U_doWeight);
        }
        public virtual string doValidate_doWeight(object oValue) {
            return base.doValidation(_doWeight, oValue);
        }

        /**
		 * Decription: Testing Criteria and Spec
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DRTCmts
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _DRTCmts = "U_DRTCmts";
        public string U_DRTCmts {
            get {
                return getValueAsString(_DRTCmts);
            }
            set { setValue(_DRTCmts, value); }
        }
        public string doValidate_DRTCmts() {
            return doValidate_DRTCmts(U_DRTCmts);
        }
        public virtual string doValidate_DRTCmts(object oValue) {
            return base.doValidation(_DRTCmts, oValue);
        }

        /**
		 * Decription: Haulage Cost
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HaulgCost
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _HaulgCost = "U_HaulgCost";
        public double U_HaulgCost {
            get {
                return getValueAsDouble(_HaulgCost);
            }
            set { setValue(_HaulgCost, value); }
        }
        public string doValidate_HaulgCost() {
            return doValidate_HaulgCost(U_HaulgCost);
        }
        public virtual string doValidate_HaulgCost(object oValue) {
            return base.doValidation(_HaulgCost, oValue);
        }

        /**
		 * Decription: Transport/ container
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HaulgPCnt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _HaulgPCnt = "U_HaulgPCnt";
        public double U_HaulgPCnt {
            get {
                return getValueAsDouble(_HaulgPCnt);
            }
            set { setValue(_HaulgPCnt, value); }
        }
        public string doValidate_HaulgPCnt() {
            return doValidate_HaulgPCnt(U_HaulgPCnt);
        }
        public virtual string doValidate_HaulgPCnt(object oValue) {
            return base.doValidation(_HaulgPCnt, oValue);
        }

        /**
		 * Decription: HP Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HAZCD
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _HAZCD = "U_HAZCD";
        public string U_HAZCD {
            get {
                return getValueAsString(_HAZCD);
            }
            set { setValue(_HAZCD, value); }
        }
        public string doValidate_HAZCD() {
            return doValidate_HAZCD(U_HAZCD);
        }
        public virtual string doValidate_HAZCD(object oValue) {
            return base.doValidation(_HAZCD, oValue);
        }

        /**
		 * Decription: ADR class
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HazClass
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _HazClass = "U_HazClass";
        public string U_HazClass {
            get {
                return getValueAsString(_HazClass);
            }
            set { setValue(_HazClass, value); }
        }
        public string doValidate_HazClass() {
            return doValidate_HazClass(U_HazClass);
        }
        public virtual string doValidate_HazClass(object oValue) {
            return base.doValidation(_HazClass, oValue);
        }

        /**
		 * Decription: In-Bound Dispoal Site
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_InTipCd
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _InTipCd = "U_InTipCd";
        public string U_InTipCd {
            get {
                return getValueAsString(_InTipCd);
            }
            set { setValue(_InTipCd, value); }
        }
        public string doValidate_InTipCd() {
            return doValidate_InTipCd(U_InTipCd);
        }
        public virtual string doValidate_InTipCd(object oValue) {
            return base.doValidation(_InTipCd, oValue);
        }

        /**
		 * Decription: In-Bound Dispoal Site Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_InTipNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _InTipNm = "U_InTipNm";
        public string U_InTipNm {
            get {
                return getValueAsString(_InTipNm);
            }
            set { setValue(_InTipNm, value); }
        }
        public string doValidate_InTipNm() {
            return doValidate_InTipNm(U_InTipNm);
        }
        public virtual string doValidate_InTipNm(object oValue) {
            return base.doValidation(_InTipNm, oValue);
        }

        /**
		 * Decription: Container Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItemCd
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _ItemCd = "U_ItemCd";
        public string U_ItemCd {
            get {
                return getValueAsString(_ItemCd);
            }
            set { setValue(_ItemCd, value); }
        }
        public string doValidate_ItemCd() {
            return doValidate_ItemCd(U_ItemCd);
        }
        public virtual string doValidate_ItemCd(object oValue) {
            return base.doValidation(_ItemCd, oValue);
        }

        /**
		 * Decription: Container Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItemDsc
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _ItemDsc = "U_ItemDsc";
        public string U_ItemDsc {
            get {
                return getValueAsString(_ItemDsc);
            }
            set { setValue(_ItemDsc, value); }
        }
        public string doValidate_ItemDsc() {
            return doValidate_ItemDsc(U_ItemDsc);
        }
        public virtual string doValidate_ItemDsc(object oValue) {
            return base.doValidation(_ItemDsc, oValue);
        }

        /**
		 * Decription: Pkg Gp
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PackGrp
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _PackGrp = "U_PackGrp";
        public string U_PackGrp {
            get {
                return getValueAsString(_PackGrp);
            }
            set { setValue(_PackGrp, value); }
        }
        public string doValidate_PackGrp() {
            return doValidate_PackGrp(U_PackGrp);
        }
        public virtual string doValidate_PackGrp(object oValue) {
            return base.doValidation(_PackGrp, oValue);
        }

        /**
		 * Decription: Qunatity
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Qty
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _Qty = "U_Qty";
        public int U_Qty {
            get {
                return getValueAsInt(_Qty);
            }
            set { setValue(_Qty, value); }
        }
        public string doValidate_Qty() {
            return doValidate_Qty(U_Qty);
        }
        public virtual string doValidate_Qty(object oValue) {
            return base.doValidation(_Qty, oValue);
        }

        /**
		 * Decription: R/D Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RDCode
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _RDCode = "U_RDCode";
        public string U_RDCode {
            get {
                return getValueAsString(_RDCode);
            }
            set { setValue(_RDCode, value); }
        }
        public string doValidate_RDCode() {
            return doValidate_RDCode(U_RDCode);
        }
        public virtual string doValidate_RDCode(object oValue) {
            return base.doValidation(_RDCode, oValue);
        }

        /**
		 * Decription: R/D Foreign Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RDFrgnCd
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _RDFrgnCd = "U_RDFrgnCd";
        public string U_RDFrgnCd {
            get {
                return getValueAsString(_RDFrgnCd);
            }
            set { setValue(_RDFrgnCd, value); }
        }
        public string doValidate_RDFrgnCd() {
            return doValidate_RDFrgnCd(U_RDFrgnCd);
        }
        public virtual string doValidate_RDFrgnCd(object oValue) {
            return base.doValidation(_RDFrgnCd, oValue);
        }

        /**
		 * Decription: State
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_State
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _State = "U_State";
        public string U_State {
            get {
                return getValueAsString(_State);
            }
            set { setValue(_State, value); }
        }
        public string doValidate_State() {
            return doValidate_State(U_State);
        }
        public virtual string doValidate_State(object oValue) {
            return base.doValidation(_State, oValue);
        }

        /**
		 * Decription: Tip Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TAddress
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _TAddress = "U_TAddress";
        public string U_TAddress {
            get {
                return getValueAsString(_TAddress);
            }
            set { setValue(_TAddress, value); }
        }
        public string doValidate_TAddress() {
            return doValidate_TAddress(U_TAddress);
        }
        public virtual string doValidate_TAddress(object oValue) {
            return base.doValidation(_TAddress, oValue);
        }

        /**
		 * Decription: Tip Address LineNum
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TAddrssLN
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _TAddrssLN = "U_TAddrssLN";
        public string U_TAddrssLN {
            get {
                return getValueAsString(_TAddrssLN);
            }
            set { setValue(_TAddrssLN, value); }
        }
        public string doValidate_TAddrssLN() {
            return doValidate_TAddrssLN(U_TAddrssLN);
        }
        public virtual string doValidate_TAddrssLN(object oValue) {
            return base.doValidation(_TAddrssLN, oValue);
        }

        /**
		 * Decription: TFS Y/N
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TFSyn
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _TFSyn = "U_TFSyn";
        public string U_TFSyn {
            get {
                return getValueAsString(_TFSyn);
            }
            set { setValue(_TFSyn, value); }
        }
        public string doValidate_TFSyn() {
            return doValidate_TFSyn(U_TFSyn);
        }
        public virtual string doValidate_TFSyn(object oValue) {
            return base.doValidation(_TFSyn, oValue);
        }

        /**
		 * Decription: Disposal Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TipCd
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _TipCd = "U_TipCd";
        public string U_TipCd {
            get {
                return getValueAsString(_TipCd);
            }
            set { setValue(_TipCd, value); }
        }
        public string doValidate_TipCd() {
            return doValidate_TipCd(U_TipCd);
        }
        public virtual string doValidate_TipCd(object oValue) {
            return base.doValidation(_TipCd, oValue);
        }

        /**
		 * Decription: Disposal Cost
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TipCost
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _TipCost = "U_TipCost";
        public double U_TipCost {
            get {
                return getValueAsDouble(_TipCost);
            }
            set { setValue(_TipCost, value); }
        }
        public string doValidate_TipCost() {
            return doValidate_TipCost(U_TipCost);
        }
        public virtual string doValidate_TipCost(object oValue) {
            return base.doValidation(_TipCost, oValue);
        }

        /**
		 * Decription: Disposal Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TipNm
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _TipNm = "U_TipNm";
        public string U_TipNm {
            get {
                return getValueAsString(_TipNm);
            }
            set { setValue(_TipNm, value); }
        }
        public string doValidate_TipNm() {
            return doValidate_TipNm(U_TipNm);
        }
        public virtual string doValidate_TipNm(object oValue) {
            return base.doValidation(_TipNm, oValue);
        }

        /**
		 * Decription: Total Cost
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TotCost
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _TotCost = "U_TotCost";
        public double U_TotCost {
            get {
                return getValueAsDouble(_TotCost);
            }
            set { setValue(_TotCost, value); }
        }
        public string doValidate_TotCost() {
            return doValidate_TotCost(U_TotCost);
        }
        public virtual string doValidate_TotCost(object oValue) {
            return base.doValidation(_TotCost, oValue);
        }

        /**
		 * Decription: Tip Postal Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TZpCd
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _TZpCd = "U_TZpCd";
        public string U_TZpCd {
            get {
                return getValueAsString(_TZpCd);
            }
            set { setValue(_TZpCd, value); }
        }
        public string doValidate_TZpCd() {
            return doValidate_TZpCd(U_TZpCd);
        }
        public virtual string doValidate_TZpCd(object oValue) {
            return base.doValidation(_TZpCd, oValue);
        }

        /**
		 * Decription: UN
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UNNANo
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _UNNANo = "U_UNNANo";
        public string U_UNNANo {
            get {
                return getValueAsString(_UNNANo);
            }
            set { setValue(_UNNANo, value); }
        }
        public string doValidate_UNNANo() {
            return doValidate_UNNANo(U_UNNANo);
        }
        public virtual string doValidate_UNNANo(object oValue) {
            return base.doValidation(_UNNANo, oValue);
        }

        /**
		 * Decription: Unit of Measurment
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UOM
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _UOM = "U_UOM";
        public string U_UOM {
            get {
                return getValueAsString(_UOM);
            }
            set { setValue(_UOM, value); }
        }
        public string doValidate_UOM() {
            return doValidate_UOM(U_UOM);
        }
        public virtual string doValidate_UOM(object oValue) {
            return base.doValidation(_UOM, oValue);
        }

        /**
		 * Decription: Waste Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WasCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _WasCd = "U_WasCd";
        public string U_WasCd {
            get {
                return getValueAsString(_WasCd);
            }
            set { setValue(_WasCd, value); }
        }
        public string doValidate_WasCd() {
            return doValidate_WasCd(U_WasCd);
        }
        public virtual string doValidate_WasCd(object oValue) {
            return base.doValidation(_WasCd, oValue);
        }

        /**
		 * Decription: Waste Description
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WasDsc
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _WasDsc = "U_WasDsc";
        public string U_WasDsc {
            get {
                return getValueAsString(_WasDsc);
            }
            set { setValue(_WasDsc, value); }
        }
        public string doValidate_WasDsc() {
            return doValidate_WasDsc(U_WasDsc);
        }
        public virtual string doValidate_WasDsc(object oValue) {
            return base.doValidation(_WasDsc, oValue);
        }

        /**
		 * Decription: Bay
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WhsCode
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _WhsCode = "U_WhsCode";
        public string U_WhsCode {
            get {
                return getValueAsString(_WhsCode);
            }
            set { setValue(_WhsCode, value); }
        }
        public string doValidate_WhsCode() {
            return doValidate_WhsCode(U_WhsCode);
        }
        public virtual string doValidate_WhsCode(object oValue) {
            return base.doValidation(_WhsCode, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo() {
            if (moDBFields == null)
                moDBFields = new DBFields(38);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_Active, "Active", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Active
            moDBFields.Add(_AvgTngCmt, "Average Tonnage per load and c", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Average Tonnage per load and c
            moDBFields.Add(_CmpBioTrt, "Composting/Biological Treatme", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Composting/Biological Treatme
            moDBFields.Add(_COMPrSt, "COMAH pre-set", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //COMAH pre-set
            moDBFields.Add(_Descrp, "Description", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Description
            moDBFields.Add(_DisProcCd, "Process Code", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Process Code
            moDBFields.Add(_DisRCode, "Disposal Route Code", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Disposal Route Code
            moDBFields.Add(_DisTrtCd, "Treatment Code", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Treatment Code
            moDBFields.Add(_doWeight, "Do Weight", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Do Weight
            moDBFields.Add(_DRTCmts, "Testing Criteria and Spec", 11, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Testing Criteria and Spec
            moDBFields.Add(_HaulgCost, "Haulage Cost", 12, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Haulage Cost
            moDBFields.Add(_HaulgPCnt, "Transport/ container", 13, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Transport/ container
            moDBFields.Add(_HAZCD, "HP Code", 14, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //HP Code
            moDBFields.Add(_HazClass, "ADR class", 15, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //ADR class
            moDBFields.Add(_InTipCd, "In-Bound Dispoal Site", 16, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //In-Bound Dispoal Site
            moDBFields.Add(_InTipNm, "In-Bound Dispoal Site Name", 17, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //In-Bound Dispoal Site Name
            moDBFields.Add(_ItemCd, "Container Code", 18, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Container Code
            moDBFields.Add(_ItemDsc, "Container Name", 19, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Container Name
            moDBFields.Add(_PackGrp, "Pkg Gp", 20, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Pkg Gp
            moDBFields.Add(_Qty, "Qunatity", 21, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Qunatity
            moDBFields.Add(_RDCode, "R/D Code", 22, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //R/D Code
            moDBFields.Add(_RDFrgnCd, "R/D Foreign Code", 23, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //R/D Foreign Code
            moDBFields.Add(_State, "State", 24, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //State
            moDBFields.Add(_TAddress, "Tip Address", 25, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Tip Address
            moDBFields.Add(_TAddrssLN, "Tip Address LineNum", 26, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Tip Address LineNum
            moDBFields.Add(_TFSyn, "TFS Y/N", 27, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //TFS Y/N
            moDBFields.Add(_TipCd, "Disposal Code", 28, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Disposal Code
            moDBFields.Add(_TipCost, "Disposal Cost", 29, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Disposal Cost
            moDBFields.Add(_TipNm, "Disposal Name", 30, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Disposal Name
            moDBFields.Add(_TotCost, "Total Cost", 31, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Total Cost
            moDBFields.Add(_TZpCd, "Tip Postal Code", 32, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Tip Postal Code
            moDBFields.Add(_UNNANo, "UN", 33, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //UN
            moDBFields.Add(_UOM, "Unit of Measurment", 34, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Unit of Measurment
            moDBFields.Add(_WasCd, "Waste Code", 35, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Waste Code
            moDBFields.Add(_WasDsc, "Waste Description", 36, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Waste Description
            moDBFields.Add(_WhsCode, "Bay", 37, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Bay

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation() {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_Active());
            doBuildValidationString(doValidate_AvgTngCmt());
            doBuildValidationString(doValidate_CmpBioTrt());
            doBuildValidationString(doValidate_COMPrSt());
            doBuildValidationString(doValidate_Descrp());
            doBuildValidationString(doValidate_DisProcCd());
            doBuildValidationString(doValidate_DisRCode());
            doBuildValidationString(doValidate_DisTrtCd());
            doBuildValidationString(doValidate_doWeight());
            doBuildValidationString(doValidate_DRTCmts());
            doBuildValidationString(doValidate_HaulgCost());
            doBuildValidationString(doValidate_HaulgPCnt());
            doBuildValidationString(doValidate_HAZCD());
            doBuildValidationString(doValidate_HazClass());
            doBuildValidationString(doValidate_InTipCd());
            doBuildValidationString(doValidate_InTipNm());
            doBuildValidationString(doValidate_ItemCd());
            doBuildValidationString(doValidate_ItemDsc());
            doBuildValidationString(doValidate_PackGrp());
            doBuildValidationString(doValidate_Qty());
            doBuildValidationString(doValidate_RDCode());
            doBuildValidationString(doValidate_RDFrgnCd());
            doBuildValidationString(doValidate_State());
            doBuildValidationString(doValidate_TAddress());
            doBuildValidationString(doValidate_TAddrssLN());
            doBuildValidationString(doValidate_TFSyn());
            doBuildValidationString(doValidate_TipCd());
            doBuildValidationString(doValidate_TipCost());
            doBuildValidationString(doValidate_TipNm());
            doBuildValidationString(doValidate_TotCost());
            doBuildValidationString(doValidate_TZpCd());
            doBuildValidationString(doValidate_UNNANo());
            doBuildValidationString(doValidate_UOM());
            doBuildValidationString(doValidate_WasCd());
            doBuildValidationString(doValidate_WasDsc());
            doBuildValidationString(doValidate_WhsCode());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_Active)) return doValidate_Active(oValue);
            if (sFieldName.Equals(_AvgTngCmt)) return doValidate_AvgTngCmt(oValue);
            if (sFieldName.Equals(_CmpBioTrt)) return doValidate_CmpBioTrt(oValue);
            if (sFieldName.Equals(_COMPrSt)) return doValidate_COMPrSt(oValue);
            if (sFieldName.Equals(_Descrp)) return doValidate_Descrp(oValue);
            if (sFieldName.Equals(_DisProcCd)) return doValidate_DisProcCd(oValue);
            if (sFieldName.Equals(_DisRCode)) return doValidate_DisRCode(oValue);
            if (sFieldName.Equals(_DisTrtCd)) return doValidate_DisTrtCd(oValue);
            if (sFieldName.Equals(_doWeight)) return doValidate_doWeight(oValue);
            if (sFieldName.Equals(_DRTCmts)) return doValidate_DRTCmts(oValue);
            if (sFieldName.Equals(_HaulgCost)) return doValidate_HaulgCost(oValue);
            if (sFieldName.Equals(_HaulgPCnt)) return doValidate_HaulgPCnt(oValue);
            if (sFieldName.Equals(_HAZCD)) return doValidate_HAZCD(oValue);
            if (sFieldName.Equals(_HazClass)) return doValidate_HazClass(oValue);
            if (sFieldName.Equals(_InTipCd)) return doValidate_InTipCd(oValue);
            if (sFieldName.Equals(_InTipNm)) return doValidate_InTipNm(oValue);
            if (sFieldName.Equals(_ItemCd)) return doValidate_ItemCd(oValue);
            if (sFieldName.Equals(_ItemDsc)) return doValidate_ItemDsc(oValue);
            if (sFieldName.Equals(_PackGrp)) return doValidate_PackGrp(oValue);
            if (sFieldName.Equals(_Qty)) return doValidate_Qty(oValue);
            if (sFieldName.Equals(_RDCode)) return doValidate_RDCode(oValue);
            if (sFieldName.Equals(_RDFrgnCd)) return doValidate_RDFrgnCd(oValue);
            if (sFieldName.Equals(_State)) return doValidate_State(oValue);
            if (sFieldName.Equals(_TAddress)) return doValidate_TAddress(oValue);
            if (sFieldName.Equals(_TAddrssLN)) return doValidate_TAddrssLN(oValue);
            if (sFieldName.Equals(_TFSyn)) return doValidate_TFSyn(oValue);
            if (sFieldName.Equals(_TipCd)) return doValidate_TipCd(oValue);
            if (sFieldName.Equals(_TipCost)) return doValidate_TipCost(oValue);
            if (sFieldName.Equals(_TipNm)) return doValidate_TipNm(oValue);
            if (sFieldName.Equals(_TotCost)) return doValidate_TotCost(oValue);
            if (sFieldName.Equals(_TZpCd)) return doValidate_TZpCd(oValue);
            if (sFieldName.Equals(_UNNANo)) return doValidate_UNNANo(oValue);
            if (sFieldName.Equals(_UOM)) return doValidate_UOM(oValue);
            if (sFieldName.Equals(_WasCd)) return doValidate_WasCd(oValue);
            if (sFieldName.Equals(_WasDsc)) return doValidate_WasDsc(oValue);
            if (sFieldName.Equals(_WhsCode)) return doValidate_WhsCode(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName) {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
		 * Link the Name Field to the Form Item.
		 */
        public void doLink_Name(string sControlName) {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
		 * Link the U_Active Field to the Form Item.
		 */
        public void doLink_Active(string sControlName) {
            moLinker.doLinkDataToControl(_Active, sControlName);
        }
        /**
		 * Link the U_AvgTngCmt Field to the Form Item.
		 */
        public void doLink_AvgTngCmt(string sControlName) {
            moLinker.doLinkDataToControl(_AvgTngCmt, sControlName);
        }
        /**
		 * Link the U_CmpBioTrt Field to the Form Item.
		 */
        public void doLink_CmpBioTrt(string sControlName) {
            moLinker.doLinkDataToControl(_CmpBioTrt, sControlName);
        }
        /**
		 * Link the U_COMPrSt Field to the Form Item.
		 */
        public void doLink_COMPrSt(string sControlName) {
            moLinker.doLinkDataToControl(_COMPrSt, sControlName);
        }
        /**
		 * Link the U_Descrp Field to the Form Item.
		 */
        public void doLink_Descrp(string sControlName) {
            moLinker.doLinkDataToControl(_Descrp, sControlName);
        }
        /**
		 * Link the U_DisProcCd Field to the Form Item.
		 */
        public void doLink_DisProcCd(string sControlName) {
            moLinker.doLinkDataToControl(_DisProcCd, sControlName);
        }
        /**
		 * Link the U_DisRCode Field to the Form Item.
		 */
        public void doLink_DisRCode(string sControlName) {
            moLinker.doLinkDataToControl(_DisRCode, sControlName);
        }
        /**
		 * Link the U_DisTrtCd Field to the Form Item.
		 */
        public void doLink_DisTrtCd(string sControlName) {
            moLinker.doLinkDataToControl(_DisTrtCd, sControlName);
        }
        /**
		 * Link the U_doWeight Field to the Form Item.
		 */
        public void doLink_doWeight(string sControlName) {
            moLinker.doLinkDataToControl(_doWeight, sControlName);
        }
        /**
		 * Link the U_DRTCmts Field to the Form Item.
		 */
        public void doLink_DRTCmts(string sControlName) {
            moLinker.doLinkDataToControl(_DRTCmts, sControlName);
        }
        /**
		 * Link the U_HaulgCost Field to the Form Item.
		 */
        public void doLink_HaulgCost(string sControlName) {
            moLinker.doLinkDataToControl(_HaulgCost, sControlName);
        }
        /**
		 * Link the U_HaulgPCnt Field to the Form Item.
		 */
        public void doLink_HaulgPCnt(string sControlName) {
            moLinker.doLinkDataToControl(_HaulgPCnt, sControlName);
        }
        /**
		 * Link the U_HAZCD Field to the Form Item.
		 */
        public void doLink_HAZCD(string sControlName) {
            moLinker.doLinkDataToControl(_HAZCD, sControlName);
        }
        /**
		 * Link the U_HazClass Field to the Form Item.
		 */
        public void doLink_HazClass(string sControlName) {
            moLinker.doLinkDataToControl(_HazClass, sControlName);
        }
        /**
		 * Link the U_InTipCd Field to the Form Item.
		 */
        public void doLink_InTipCd(string sControlName) {
            moLinker.doLinkDataToControl(_InTipCd, sControlName);
        }
        /**
		 * Link the U_InTipNm Field to the Form Item.
		 */
        public void doLink_InTipNm(string sControlName) {
            moLinker.doLinkDataToControl(_InTipNm, sControlName);
        }
        /**
		 * Link the U_ItemCd Field to the Form Item.
		 */
        public void doLink_ItemCd(string sControlName) {
            moLinker.doLinkDataToControl(_ItemCd, sControlName);
        }
        /**
		 * Link the U_ItemDsc Field to the Form Item.
		 */
        public void doLink_ItemDsc(string sControlName) {
            moLinker.doLinkDataToControl(_ItemDsc, sControlName);
        }
        /**
		 * Link the U_PackGrp Field to the Form Item.
		 */
        public void doLink_PackGrp(string sControlName) {
            moLinker.doLinkDataToControl(_PackGrp, sControlName);
        }
        /**
		 * Link the U_Qty Field to the Form Item.
		 */
        public void doLink_Qty(string sControlName) {
            moLinker.doLinkDataToControl(_Qty, sControlName);
        }
        /**
		 * Link the U_RDCode Field to the Form Item.
		 */
        public void doLink_RDCode(string sControlName) {
            moLinker.doLinkDataToControl(_RDCode, sControlName);
        }
        /**
		 * Link the U_RDFrgnCd Field to the Form Item.
		 */
        public void doLink_RDFrgnCd(string sControlName) {
            moLinker.doLinkDataToControl(_RDFrgnCd, sControlName);
        }
        /**
		 * Link the U_State Field to the Form Item.
		 */
        public void doLink_State(string sControlName) {
            moLinker.doLinkDataToControl(_State, sControlName);
        }
        /**
		 * Link the U_TAddress Field to the Form Item.
		 */
        public void doLink_TAddress(string sControlName) {
            moLinker.doLinkDataToControl(_TAddress, sControlName);
        }
        /**
		 * Link the U_TAddrssLN Field to the Form Item.
		 */
        public void doLink_TAddrssLN(string sControlName) {
            moLinker.doLinkDataToControl(_TAddrssLN, sControlName);
        }
        /**
		 * Link the U_TFSyn Field to the Form Item.
		 */
        public void doLink_TFSyn(string sControlName) {
            moLinker.doLinkDataToControl(_TFSyn, sControlName);
        }
        /**
		 * Link the U_TipCd Field to the Form Item.
		 */
        public void doLink_TipCd(string sControlName) {
            moLinker.doLinkDataToControl(_TipCd, sControlName);
        }
        /**
		 * Link the U_TipCost Field to the Form Item.
		 */
        public void doLink_TipCost(string sControlName) {
            moLinker.doLinkDataToControl(_TipCost, sControlName);
        }
        /**
		 * Link the U_TipNm Field to the Form Item.
		 */
        public void doLink_TipNm(string sControlName) {
            moLinker.doLinkDataToControl(_TipNm, sControlName);
        }
        /**
		 * Link the U_TotCost Field to the Form Item.
		 */
        public void doLink_TotCost(string sControlName) {
            moLinker.doLinkDataToControl(_TotCost, sControlName);
        }
        /**
		 * Link the U_TZpCd Field to the Form Item.
		 */
        public void doLink_TZpCd(string sControlName) {
            moLinker.doLinkDataToControl(_TZpCd, sControlName);
        }
        /**
		 * Link the U_UNNANo Field to the Form Item.
		 */
        public void doLink_UNNANo(string sControlName) {
            moLinker.doLinkDataToControl(_UNNANo, sControlName);
        }
        /**
		 * Link the U_UOM Field to the Form Item.
		 */
        public void doLink_UOM(string sControlName) {
            moLinker.doLinkDataToControl(_UOM, sControlName);
        }
        /**
		 * Link the U_WasCd Field to the Form Item.
		 */
        public void doLink_WasCd(string sControlName) {
            moLinker.doLinkDataToControl(_WasCd, sControlName);
        }
        /**
		 * Link the U_WasDsc Field to the Form Item.
		 */
        public void doLink_WasDsc(string sControlName) {
            moLinker.doLinkDataToControl(_WasDsc, sControlName);
        }
        /**
		 * Link the U_WhsCode Field to the Form Item.
		 */
        public void doLink_WhsCode(string sControlName) {
            moLinker.doLinkDataToControl(_WhsCode, sControlName);
        }
        #endregion

    }
}
