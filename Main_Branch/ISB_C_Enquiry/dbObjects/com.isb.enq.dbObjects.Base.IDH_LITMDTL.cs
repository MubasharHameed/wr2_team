/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 05/10/2016 15:22:00
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base {
    [Serializable]
    public class IDH_LITMDTL : com.idh.dbObjects.DBBase {

        private Linker moLinker = null;
        public Linker ControlLinker {
            get { return moLinker; }
            set { moLinker = value; }
        }

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_LITMDTL()
            : base("@IDH_LITMDTL") {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_LITMDTL(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oForm, "@IDH_LITMDTL") {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
		* Table name
		*/
        public readonly static string TableName = "@IDH_LITMDTL";

        /**
         * Decription: Code
         * Mandatory: tYes
         * Name: Code
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Code = "Code";
        public string Code {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code() {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue) {
            return base.doValidation(_Code, oValue);
        }
        /**
         * Decription: Name
         * Mandatory: tYes
         * Name: Name
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Name = "Name";
        public string Name {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name() {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue) {
            return base.doValidation(_Name, oValue);
        }
        /**
         * Decription: Batch AbsEntry
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_AbsEntry
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _AbsEntry = "U_AbsEntry";
        public int U_AbsEntry {
            get {
                return getValueAsInt(_AbsEntry);
            }
            set { setValue(_AbsEntry, value); }
        }
        public string doValidate_AbsEntry() {
            return doValidate_AbsEntry(U_AbsEntry);
        }
        public virtual string doValidate_AbsEntry(object oValue) {
            return base.doValidation(_AbsEntry, oValue);
        }

        /**
         * Decription: Batch Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Batch
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Batch = "U_Batch";
        public string U_Batch {
            get {
                return getValueAsString(_Batch);
            }
            set { setValue(_Batch, value); }
        }
        public string doValidate_Batch() {
            return doValidate_Batch(U_Batch);
        }
        public virtual string doValidate_Batch(object oValue) {
            return base.doValidation(_Batch, oValue);
        }

        /**
         * Decription: BinLocation
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_BinLoc
         * Size: 15
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _BinLoc = "U_BinLoc";
        public string U_BinLoc {
            get {
                return getValueAsString(_BinLoc);
            }
            set { setValue(_BinLoc, value); }
        }
        public string doValidate_BinLoc() {
            return doValidate_BinLoc(U_BinLoc);
        }
        public virtual string doValidate_BinLoc(object oValue) {
            return base.doValidation(_BinLoc, oValue);
        }

        /**
         * Decription: Lab Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_LabCd
         * Size: 15
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _LabCd = "U_LabCd";
        public string U_LabCd {
            get {
                return getValueAsString(_LabCd);
            }
            set { setValue(_LabCd, value); }
        }
        public string doValidate_LabCd() {
            return doValidate_LabCd(U_LabCd);
        }
        public virtual string doValidate_LabCd(object oValue) {
            return base.doValidation(_LabCd, oValue);
        }

        /**
         * Decription: Parent Item Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PItemCd
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PItemCd = "U_PItemCd";
        public string U_PItemCd {
            get {
                return getValueAsString(_PItemCd);
            }
            set { setValue(_PItemCd, value); }
        }
        public string doValidate_PItemCd() {
            return doValidate_PItemCd(U_PItemCd);
        }
        public virtual string doValidate_PItemCd(object oValue) {
            return base.doValidation(_PItemCd, oValue);
        }

        /**
         * Decription: Parent Item Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PItemNm
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PItemNm = "U_PItemNm";
        public string U_PItemNm {
            get {
                return getValueAsString(_PItemNm);
            }
            set { setValue(_PItemNm, value); }
        }
        public string doValidate_PItemNm() {
            return doValidate_PItemNm(U_PItemNm);
        }
        public virtual string doValidate_PItemNm(object oValue) {
            return base.doValidation(_PItemNm, oValue);
        }

        /**
         * Decription: Provisional Batch Detail Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PrvBtDCd
         * Size: 50
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PrvBtDCd = "U_PrvBtDCd";
        public string U_PrvBtDCd {
            get {
                return getValueAsString(_PrvBtDCd);
            }
            set { setValue(_PrvBtDCd, value); }
        }
        public string doValidate_PrvBtDCd() {
            return doValidate_PrvBtDCd(U_PrvBtDCd);
        }
        public virtual string doValidate_PrvBtDCd(object oValue) {
            return base.doValidation(_PrvBtDCd, oValue);
        }

        /**
         * Decription: Provisional Batch Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PrvBtHCd
         * Size: 50
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PrvBtHCd = "U_PrvBtHCd";
        public string U_PrvBtHCd {
            get {
                return getValueAsString(_PrvBtHCd);
            }
            set { setValue(_PrvBtHCd, value); }
        }
        public string doValidate_PrvBtHCd() {
            return doValidate_PrvBtHCd(U_PrvBtHCd);
        }
        public virtual string doValidate_PrvBtHCd(object oValue) {
            return base.doValidation(_PrvBtHCd, oValue);
        }

        /**
         * Decription: Quantity
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Quantity
         * Size: 15
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Quantity = "U_Quantity";
        public string U_Quantity {
            get {
                return getValueAsString(_Quantity);
            }
            set { setValue(_Quantity, value); }
        }
        public string doValidate_Quantity() {
            return doValidate_Quantity(U_Quantity);
        }
        public virtual string doValidate_Quantity(object oValue) {
            return base.doValidation(_Quantity, oValue);
        }

        /**
         * Decription: UOM
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_UOM
         * Size: 15
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _UOM = "U_UOM";
        public string U_UOM {
            get {
                return getValueAsString(_UOM);
            }
            set { setValue(_UOM, value); }
        }
        public string doValidate_UOM() {
            return doValidate_UOM(U_UOM);
        }
        public virtual string doValidate_UOM(object oValue) {
            return base.doValidation(_UOM, oValue);
        }

        /**
         * Decription: Warehouse
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Whse
         * Size: 15
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Whse = "U_Whse";
        public string U_Whse {
            get {
                return getValueAsString(_Whse);
            }
            set { setValue(_Whse, value); }
        }
        public string doValidate_Whse() {
            return doValidate_Whse(U_Whse);
        }
        public virtual string doValidate_Whse(object oValue) {
            return base.doValidation(_Whse, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo() {
            if (moDBFields == null)
                moDBFields = new DBFields(13);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_AbsEntry, "Batch AbsEntry", 2, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Batch AbsEntry
            moDBFields.Add(_Batch, "Batch Name", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Batch Name
            moDBFields.Add(_BinLoc, "BinLocation", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //BinLocation
            moDBFields.Add(_LabCd, "Lab Code", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Lab Code
            moDBFields.Add(_PItemCd, "Parent Item Code", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Parent Item Code
            moDBFields.Add(_PItemNm, "Parent Item Name", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Parent Item Name
            moDBFields.Add(_PrvBtDCd, "Provisional Batch Detail Code", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Provisional Batch Detail Code
            moDBFields.Add(_PrvBtHCd, "Provisional Batch Code", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Provisional Batch Code
            moDBFields.Add(_Quantity, "Quantity", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Quantity
            moDBFields.Add(_UOM, "UOM", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //UOM
            moDBFields.Add(_Whse, "Warehouse", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Warehouse

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation() {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_AbsEntry());
            doBuildValidationString(doValidate_Batch());
            doBuildValidationString(doValidate_BinLoc());
            doBuildValidationString(doValidate_LabCd());
            doBuildValidationString(doValidate_PItemCd());
            doBuildValidationString(doValidate_PItemNm());
            doBuildValidationString(doValidate_PrvBtDCd());
            doBuildValidationString(doValidate_PrvBtHCd());
            doBuildValidationString(doValidate_Quantity());
            doBuildValidationString(doValidate_UOM());
            doBuildValidationString(doValidate_Whse());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_AbsEntry)) return doValidate_AbsEntry(oValue);
            if (sFieldName.Equals(_Batch)) return doValidate_Batch(oValue);
            if (sFieldName.Equals(_BinLoc)) return doValidate_BinLoc(oValue);
            if (sFieldName.Equals(_LabCd)) return doValidate_LabCd(oValue);
            if (sFieldName.Equals(_PItemCd)) return doValidate_PItemCd(oValue);
            if (sFieldName.Equals(_PItemNm)) return doValidate_PItemNm(oValue);
            if (sFieldName.Equals(_PrvBtDCd)) return doValidate_PrvBtDCd(oValue);
            if (sFieldName.Equals(_PrvBtHCd)) return doValidate_PrvBtHCd(oValue);
            if (sFieldName.Equals(_Quantity)) return doValidate_Quantity(oValue);
            if (sFieldName.Equals(_UOM)) return doValidate_UOM(oValue);
            if (sFieldName.Equals(_Whse)) return doValidate_Whse(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName) {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
         * Link the Name Field to the Form Item.
         */
        public void doLink_Name(string sControlName) {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
         * Link the U_AbsEntry Field to the Form Item.
         */
        public void doLink_AbsEntry(string sControlName) {
            moLinker.doLinkDataToControl(_AbsEntry, sControlName);
        }
        /**
         * Link the U_Batch Field to the Form Item.
         */
        public void doLink_Batch(string sControlName) {
            moLinker.doLinkDataToControl(_Batch, sControlName);
        }
        /**
         * Link the U_BinLoc Field to the Form Item.
         */
        public void doLink_BinLoc(string sControlName) {
            moLinker.doLinkDataToControl(_BinLoc, sControlName);
        }
        /**
         * Link the U_LabCd Field to the Form Item.
         */
        public void doLink_LabCd(string sControlName) {
            moLinker.doLinkDataToControl(_LabCd, sControlName);
        }
        /**
         * Link the U_PItemCd Field to the Form Item.
         */
        public void doLink_PItemCd(string sControlName) {
            moLinker.doLinkDataToControl(_PItemCd, sControlName);
        }
        /**
         * Link the U_PItemNm Field to the Form Item.
         */
        public void doLink_PItemNm(string sControlName) {
            moLinker.doLinkDataToControl(_PItemNm, sControlName);
        }
        /**
         * Link the U_PrvBtDCd Field to the Form Item.
         */
        public void doLink_PrvBtDCd(string sControlName) {
            moLinker.doLinkDataToControl(_PrvBtDCd, sControlName);
        }
        /**
         * Link the U_PrvBtHCd Field to the Form Item.
         */
        public void doLink_PrvBtHCd(string sControlName) {
            moLinker.doLinkDataToControl(_PrvBtHCd, sControlName);
        }
        /**
         * Link the U_Quantity Field to the Form Item.
         */
        public void doLink_Quantity(string sControlName) {
            moLinker.doLinkDataToControl(_Quantity, sControlName);
        }
        /**
         * Link the U_UOM Field to the Form Item.
         */
        public void doLink_UOM(string sControlName) {
            moLinker.doLinkDataToControl(_UOM, sControlName);
        }
        /**
         * Link the U_Whse Field to the Form Item.
         */
        public void doLink_Whse(string sControlName) {
            moLinker.doLinkDataToControl(_Whse, sControlName);
        }
        #endregion

    }
}
