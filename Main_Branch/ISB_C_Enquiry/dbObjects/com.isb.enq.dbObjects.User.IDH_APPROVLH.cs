/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 11/02/2016 17:35:17
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User{
   [Serializable] 
	public class IDH_APPROVLH: com.isb.enq.dbObjects.Base.IDH_APPROVLH{ 

		public IDH_APPROVLH() : base() {
            msAutoNumKey = "IDHAPDCH";
		}

   	public IDH_APPROVLH( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
        msAutoNumKey = "IDHAPDCH";
        }

    public enum en_APPROVALSTATUS {
        Draft = 1,
        Approve = 2,
        Reject = 3,
        ReviewAdvised = 4
    };

	}
}
