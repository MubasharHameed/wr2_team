/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 08/03/2017 18:59:03
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_LBTDPLG : com.isb.enq.dbObjects.Base.IDH_LBTDPLG {

        public IDH_LBTDPLG() : base() {
            msAutoNumKey = "SEQLTDLG";
        }

        public IDH_LBTDPLG(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm) : base(oIDHForm, oForm) {
            msAutoNumKey = "SEQLTDLG";
        }

        //public void getNextSuffext() {
        //    if (U_OrgTaskCd == "") {
        //        U_OrgTaskCd = "A";
        //    }else {
        //        U_OrgTaskCd+= U_OrgTaskCd
        //    }

        //    doProcessData();
        //}
    }
}
