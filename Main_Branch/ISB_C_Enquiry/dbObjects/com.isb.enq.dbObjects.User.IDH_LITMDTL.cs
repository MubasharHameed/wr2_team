/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 09/02/2016 12:22:49
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.bridge; 
using com.idh.bridge.lookups;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_LITMDTL : com.isb.enq.dbObjects.Base.IDH_LITMDTL {
        protected IDH_LABITM moParentLabItem;
        public IDH_LITMDTL()
            : base() {
            msAutoNumKey = "SEQLIDTL";
        }

        public IDH_LITMDTL(IDH_LABITM oParent)
            : base() {
            msAutoNumKey = "SEQLIDTL";
            moParentLabItem = oParent;
        }

        public IDH_LITMDTL(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oIDHForm, oForm) {
            msAutoNumKey = "SEQLIDTL";
        }

        public int getByParentNumber(string sParentNo) {
            return getData(IDH_LITMDTL._LabCd + " = '" + sParentNo + "'", "");
        }
    }
}
