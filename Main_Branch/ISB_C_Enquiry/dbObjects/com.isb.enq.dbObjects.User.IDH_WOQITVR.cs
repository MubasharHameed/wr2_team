/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 29/03/2016 15:35:19
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.bridge.lookups;
using com.idh.bridge;
using com.idh.dbObjects.numbers;

namespace com.isb.enq.dbObjects.User {
   [Serializable] 
	public class IDH_WOQITVR: com.isb.enq.dbObjects.Base.IDH_WOQITVR{ 

		public IDH_WOQITVR() : base() {
    msAutoNumKey = "IDHWOQIV";
        			
		}

   	public IDH_WOQITVR( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	    msAutoNumKey = "IDHWOQIV";
        }
    public IDH_WOQITVR(IDH_WOQHVR oParent)
            : base() {
            moParentWOQ = oParent;
            msAutoNumKey = "IDHWOQIV";
            OrderByField = _Sort;
        }

    private int miDoHandleChange = 0;
    public bool DoBlockUpdateTrigger {
        get { return miDoHandleChange > 0; }
        set { miDoHandleChange += (value ? 1 : -1); }
    }

    protected IDH_WOQHVR moParentWOQ;
        public IDH_WOQHVR WOQ {
            get {
                if (moParentWOQ == null && U_BasWOHID != null && U_BasWOHID.Length > 0) {
                    moParentWOQ = new IDH_WOQHVR();
                    if (!moParentWOQ.getByKey(U_BasWOHID)) {
                        moParentWOQ = null;
                    }

                }
                if (moParentWOQ == null) {
                    //DataHandler.INSTANCE.doError("The Parent Disposal Order [" + U_JobNr + "] could not be retrieved for Disposal Order Row [" + Code + "].");
                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("The Parent WOQV  [" + U_BasWOHID  + "] could not be retrieved for WOQ Item [" + Code + "].",
                        "ERSYFPDO", new string[] { U_BasWOHID, Code });
                }
                return moParentWOQ;
            }
        }

        public int getByParentNumber(string sParentNo) {
            return getData(IDH_WOQITVR._BasWOHID + " = '" + sParentNo + '\'', IDH_WOQITVR._Sort);
        }

        public bool doLoadParent() {
            bool bOk = false;
            bool bLoadCh = moParentWOQ.MustLoadChildren;
            moParentWOQ.MustLoadChildren = false;
            try {
                bOk = moParentWOQ.getByKey(U_BasWOHID);
            } catch (Exception ex) {
                throw (ex);
            } finally {
                moParentWOQ.MustLoadChildren = bLoadCh;
            }
            return bOk;
        }

        public override void doApplyDefaults() {
            //if (mbDoNotSetDefault == true)
            //    return;
            base.doApplyDefaults();

            try {
                // setDefaultValue(_Status, "1");

                //if (WOQ != null) {
                //    setDefaultValue(_JobNr, WOQ.Code);
                //}


                NumbersPair oNextNum;
                oNextNum = this.getNewKey();
                doApplyDefaultValue(_Code, oNextNum.CodeNumber.ToString());
                doApplyDefaultValue(_Name, oNextNum.NameNumber.ToString());
                //setDefaultValue(_Sort, this.Count);

            } catch (Exception) { }
        }

        public override void doClearBuffers() {
            base.doClearBuffers();
        }
    }
}
