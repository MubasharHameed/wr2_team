/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 11/02/2016 17:35:53
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User{
   [Serializable] 
	public class IDH_APPROVLD: com.isb.enq.dbObjects.Base.IDH_APPROVLD{ 

		public IDH_APPROVLD() : base() {
            msAutoNumKey = "IDHAPDCD";
		}

   	public IDH_APPROVLD( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
        msAutoNumKey = "IDHAPDCD";   	
    }

    public enum en_APPROVALSTATUS {
        Waiting = 1,
        Approve = 2,
        Reject = 3,
        ReviewAdvised = 4
    };


   }

   
}
