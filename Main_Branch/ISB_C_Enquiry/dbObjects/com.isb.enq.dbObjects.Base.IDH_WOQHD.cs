/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 01/04/2016 11:59:48
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base {
    [Serializable]
    public class IDH_WOQHD : com.idh.dbObjects.DBBase {

        private Linker moLinker = null;
        public Linker ControlLinker {
            get { return moLinker; }
            set { moLinker = value; }
        }

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_WOQHD()
            : base("@IDH_WOQHD") {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_WOQHD(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oForm, "@IDH_WOQHD") {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
		* Table name
		*/
        public readonly static string TableName = "@IDH_WOQHD";

        /**
         * Decription: Code
         * Mandatory: tYes
         * Name: Code
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Code = "Code";
        public string Code {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code() {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue) {
            return base.doValidation(_Code, oValue);
        }
        /**
         * Decription: Name
         * Mandatory: tYes
         * Name: Name
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Name = "Name";
        public string Name {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name() {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue) {
            return base.doValidation(_Name, oValue);
        }
        /**
         * Decription: Address
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Address
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Address = "U_Address";
        public string U_Address {
            get {
                return getValueAsString(_Address);
            }
            set { setValue(_Address, value); }
        }
        public string doValidate_Address() {
            return doValidate_Address(U_Address);
        }
        public virtual string doValidate_Address(object oValue) {
            return base.doValidation(_Address, oValue);
        }

        /**
         * Decription: Customer Address LineNum
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_AddrssLN
         * Size: 10
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _AddrssLN = "U_AddrssLN";
        public string U_AddrssLN {
            get {
                return getValueAsString(_AddrssLN);
            }
            set { setValue(_AddrssLN, value); }
        }
        public string doValidate_AddrssLN() {
            return doValidate_AddrssLN(U_AddrssLN);
        }
        public virtual string doValidate_AddrssLN(object oValue) {
            return base.doValidation(_AddrssLN, oValue);
        }

        /**
         * Decription: Approval Required
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_AppRequired
         * Size: 1
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _AppRequired = "U_AppRequired";
        public string U_AppRequired {
            get {
                return getValueAsString(_AppRequired);
            }
            set { setValue(_AppRequired, value); }
        }
        public string doValidate_AppRequired() {
            return doValidate_AppRequired(U_AppRequired);
        }
        public virtual string doValidate_AppRequired(object oValue) {
            return base.doValidation(_AppRequired, oValue);
        }

        /**
         * Decription: Booking Date
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_BDate
         * Size: 8
         * Type: db_Date
         * CType: DateTime
         * SubType: st_None
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _BDate = "U_BDate";
        public DateTime U_BDate {
            get {
                return getValueAsDateTime(_BDate);
            }
            set { setValue(_BDate, value); }
        }
        public void U_BDate_AsString(string value) {
            setValue("U_BDate", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_BDate_AsString() {
            DateTime dVal = getValueAsDateTime(_BDate);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_BDate() {
            return doValidate_BDate(U_BDate);
        }
        public virtual string doValidate_BDate(object oValue) {
            return base.doValidation(_BDate, oValue);
        }

        /**
         * Decription: Block
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Block
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Block = "U_Block";
        public string U_Block {
            get {
                return getValueAsString(_Block);
            }
            set { setValue(_Block, value); }
        }
        public string doValidate_Block() {
            return doValidate_Block(U_Block);
        }
        public virtual string doValidate_Block(object oValue) {
            return base.doValidation(_Block, oValue);
        }

        /**
         * Decription: Branch
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Branch
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Branch = "U_Branch";
        public int U_Branch {
            get {
                return getValueAsInt(_Branch);
            }
            set { setValue(_Branch, value); }
        }
        public string doValidate_Branch() {
            return doValidate_Branch(U_Branch);
        }
        public virtual string doValidate_Branch(object oValue) {
            return base.doValidation(_Branch, oValue);
        }

        /**
         * Decription: Booking Time
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_BTime
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _BTime = "U_BTime";
        public string U_BTime {
            get {
                return getValueAsString(_BTime);
            }
            set { setValue(_BTime, value); }
        }
        public string doValidate_BTime() {
            return doValidate_BTime(U_BTime);
        }
        public virtual string doValidate_BTime(object oValue) {
            return base.doValidation(_BTime, oValue);
        }

        /**
         * Decription: Carrier Address
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CAddress
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CAddress = "U_CAddress";
        public string U_CAddress {
            get {
                return getValueAsString(_CAddress);
            }
            set { setValue(_CAddress, value); }
        }
        public string doValidate_CAddress() {
            return doValidate_CAddress(U_CAddress);
        }
        public virtual string doValidate_CAddress(object oValue) {
            return base.doValidation(_CAddress, oValue);
        }

        /**
         * Decription: Haulier Address LineNum
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CAddrsLN
         * Size: 10
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CAddrsLN = "U_CAddrsLN";
        public string U_CAddrsLN {
            get {
                return getValueAsString(_CAddrsLN);
            }
            set { setValue(_CAddrsLN, value); }
        }
        public string doValidate_CAddrsLN() {
            return doValidate_CAddrsLN(U_CAddrsLN);
        }
        public virtual string doValidate_CAddrsLN(object oValue) {
            return base.doValidation(_CAddrsLN, oValue);
        }

        /**
         * Decription: Customer
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CardCd
         * Size: 15
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CardCd = "U_CardCd";
        public string U_CardCd {
            get {
                return getValueAsString(_CardCd);
            }
            set { setValue(_CardCd, value); }
        }
        public string doValidate_CardCd() {
            return doValidate_CardCd(U_CardCd);
        }
        public virtual string doValidate_CardCd(object oValue) {
            return base.doValidation(_CardCd, oValue);
        }

        /**
         * Decription: Customer Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CardNM
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CardNM = "U_CardNM";
        public string U_CardNM {
            get {
                return getValueAsString(_CardNM);
            }
            set { setValue(_CardNM, value); }
        }
        public string doValidate_CardNM() {
            return doValidate_CardNM(U_CardNM);
        }
        public virtual string doValidate_CardNM(object oValue) {
            return base.doValidation(_CardNM, oValue);
        }

        /**
         * Decription: Carrier Block
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CBlock
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CBlock = "U_CBlock";
        public string U_CBlock {
            get {
                return getValueAsString(_CBlock);
            }
            set { setValue(_CBlock, value); }
        }
        public string doValidate_CBlock() {
            return doValidate_CBlock(U_CBlock);
        }
        public virtual string doValidate_CBlock(object oValue) {
            return base.doValidation(_CBlock, oValue);
        }

        /**
         * Decription: Carrier
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CCardCd
         * Size: 15
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CCardCd = "U_CCardCd";
        public string U_CCardCd {
            get {
                return getValueAsString(_CCardCd);
            }
            set { setValue(_CCardCd, value); }
        }
        public string doValidate_CCardCd() {
            return doValidate_CCardCd(U_CCardCd);
        }
        public virtual string doValidate_CCardCd(object oValue) {
            return base.doValidation(_CCardCd, oValue);
        }

        /**
         * Decription: Carrier Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CCardNM
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CCardNM = "U_CCardNM";
        public string U_CCardNM {
            get {
                return getValueAsString(_CCardNM);
            }
            set { setValue(_CCardNM, value); }
        }
        public string doValidate_CCardNM() {
            return doValidate_CCardNM(U_CCardNM);
        }
        public virtual string doValidate_CCardNM(object oValue) {
            return base.doValidation(_CCardNM, oValue);
        }

        /**
         * Decription: Carrier City
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CCity
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CCity = "U_CCity";
        public string U_CCity {
            get {
                return getValueAsString(_CCity);
            }
            set { setValue(_CCity, value); }
        }
        public string doValidate_CCity() {
            return doValidate_CCity(U_CCity);
        }
        public virtual string doValidate_CCity(object oValue) {
            return base.doValidation(_CCity, oValue);
        }

        /**
         * Decription: Carrier Contact Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CContact
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CContact = "U_CContact";
        public string U_CContact {
            get {
                return getValueAsString(_CContact);
            }
            set { setValue(_CContact, value); }
        }
        public string doValidate_CContact() {
            return doValidate_CContact(U_CContact);
        }
        public virtual string doValidate_CContact(object oValue) {
            return base.doValidation(_CContact, oValue);
        }

        /**
         * Decription: Charge Price
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ChgPrice
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Price
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _ChgPrice = "U_ChgPrice";
        public double U_ChgPrice {
            get {
                return getValueAsDouble(_ChgPrice);
            }
            set { setValue(_ChgPrice, value); }
        }
        public string doValidate_ChgPrice() {
            return doValidate_ChgPrice(U_ChgPrice);
        }
        public virtual string doValidate_ChgPrice(object oValue) {
            return base.doValidation(_ChgPrice, oValue);
        }

        /**
         * Decription: Tax on Charge Price
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ChgVAT
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Price
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _ChgVAT = "U_ChgVAT";
        public double U_ChgVAT {
            get {
                return getValueAsDouble(_ChgVAT);
            }
            set { setValue(_ChgVAT, value); }
        }
        public string doValidate_ChgVAT() {
            return doValidate_ChgVAT(U_ChgVAT);
        }
        public virtual string doValidate_ChgVAT(object oValue) {
            return base.doValidation(_ChgVAT, oValue);
        }

        /**
         * Decription: City
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_City
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _City = "U_City";
        public string U_City {
            get {
                return getValueAsString(_City);
            }
            set { setValue(_City, value); }
        }
        public string doValidate_City() {
            return doValidate_City(U_City);
        }
        public virtual string doValidate_City(object oValue) {
            return base.doValidation(_City, oValue);
        }

        /**
         * Decription: Number
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ClgCode
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _ClgCode = "U_ClgCode";
        public int U_ClgCode {
            get {
                return getValueAsInt(_ClgCode);
            }
            set { setValue(_ClgCode, value); }
        }
        public string doValidate_ClgCode() {
            return doValidate_ClgCode(U_ClgCode);
        }
        public virtual string doValidate_ClgCode(object oValue) {
            return base.doValidation(_ClgCode, oValue);
        }

        /**
         * Decription: Contract No.
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CntrNo
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CntrNo = "U_CntrNo";
        public string U_CntrNo {
            get {
                return getValueAsString(_CntrNo);
            }
            set { setValue(_CntrNo, value); }
        }
        public string doValidate_CntrNo() {
            return doValidate_CntrNo(U_CntrNo);
        }
        public virtual string doValidate_CntrNo(object oValue) {
            return base.doValidation(_CntrNo, oValue);
        }

        /**
         * Decription: Contact Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Contact
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Contact = "U_Contact";
        public string U_Contact {
            get {
                return getValueAsString(_Contact);
            }
            set { setValue(_Contact, value); }
        }
        public string doValidate_Contact() {
            return doValidate_Contact(U_Contact);
        }
        public virtual string doValidate_Contact(object oValue) {
            return base.doValidation(_Contact, oValue);
        }

        /**
         * Decription: Copy Instruction to rows
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CopTRw
         * Size: 5
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CopTRw = "U_CopTRw";
        public string U_CopTRw {
            get {
                return getValueAsString(_CopTRw);
            }
            set { setValue(_CopTRw, value); }
        }
        public string doValidate_CopTRw() {
            return doValidate_CopTRw(U_CopTRw);
        }
        public virtual string doValidate_CopTRw(object oValue) {
            return base.doValidation(_CopTRw, oValue);
        }

        /**
         * Decription: Cost
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Cost
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Price
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Cost = "U_Cost";
        public double U_Cost {
            get {
                return getValueAsDouble(_Cost);
            }
            set { setValue(_Cost, value); }
        }
        public string doValidate_Cost() {
            return doValidate_Cost(U_Cost);
        }
        public virtual string doValidate_Cost(object oValue) {
            return base.doValidation(_Cost, oValue);
        }

        /**
         * Decription: Country
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Country
         * Size: 3
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Country = "U_Country";
        public string U_Country {
            get {
                return getValueAsString(_Country);
            }
            set { setValue(_Country, value); }
        }
        public string doValidate_Country() {
            return doValidate_Country(U_Country);
        }
        public virtual string doValidate_Country(object oValue) {
            return base.doValidation(_Country, oValue);
        }

        /**
         * Decription: County
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_County
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _County = "U_County";
        public string U_County {
            get {
                return getValueAsString(_County);
            }
            set { setValue(_County, value); }
        }
        public string doValidate_County() {
            return doValidate_County(U_County);
        }
        public virtual string doValidate_County(object oValue) {
            return base.doValidation(_County, oValue);
        }

        /**
         * Decription: Charge Price Gross Margin
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CPGrossM
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Percentage
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _CPGrossM = "U_CPGrossM";
        public double U_CPGrossM {
            get {
                return getValueAsDouble(_CPGrossM);
            }
            set { setValue(_CPGrossM, value); }
        }
        public string doValidate_CPGrossM() {
            return doValidate_CPGrossM(U_CPGrossM);
        }
        public virtual string doValidate_CPGrossM(object oValue) {
            return base.doValidation(_CPGrossM, oValue);
        }

        /**
         * Decription: Carrier Phone Number
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CPhone1
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CPhone1 = "U_CPhone1";
        public string U_CPhone1 {
            get {
                return getValueAsString(_CPhone1);
            }
            set { setValue(_CPhone1, value); }
        }
        public string doValidate_CPhone1() {
            return doValidate_CPhone1(U_CPhone1);
        }
        public virtual string doValidate_CPhone1(object oValue) {
            return base.doValidation(_CPhone1, oValue);
        }

        /**
         * Decription: Charge Price Profit
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CPProfit
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Price
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _CPProfit = "U_CPProfit";
        public double U_CPProfit {
            get {
                return getValueAsDouble(_CPProfit);
            }
            set { setValue(_CPProfit, value); }
        }
        public string doValidate_CPProfit() {
            return doValidate_CPProfit(U_CPProfit);
        }
        public virtual string doValidate_CPProfit(object oValue) {
            return base.doValidation(_CPProfit, oValue);
        }

        /**
         * Decription: Carrier State
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CState
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CState = "U_CState";
        public string U_CState {
            get {
                return getValueAsString(_CState);
            }
            set { setValue(_CState, value); }
        }
        public string doValidate_CState() {
            return doValidate_CState(U_CState);
        }
        public virtual string doValidate_CState(object oValue) {
            return base.doValidation(_CState, oValue);
        }

        /**
         * Decription: Carrier Street
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CStreet
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CStreet = "U_CStreet";
        public string U_CStreet {
            get {
                return getValueAsString(_CStreet);
            }
            set { setValue(_CStreet, value); }
        }
        public string doValidate_CStreet() {
            return doValidate_CStreet(U_CStreet);
        }
        public virtual string doValidate_CStreet(object oValue) {
            return base.doValidation(_CStreet, oValue);
        }

        /**
         * Decription: Customer Foreign Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CusFrnNm
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CusFrnNm = "U_CusFrnNm";
        public string U_CusFrnNm {
            get {
                return getValueAsString(_CusFrnNm);
            }
            set { setValue(_CusFrnNm, value); }
        }
        public string doValidate_CusFrnNm() {
            return doValidate_CusFrnNm(U_CusFrnNm);
        }
        public virtual string doValidate_CusFrnNm(object oValue) {
            return base.doValidation(_CusFrnNm, oValue);
        }

        /**
         * Decription: Customer Ref
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CustRef
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CustRef = "U_CustRef";
        public string U_CustRef {
            get {
                return getValueAsString(_CustRef);
            }
            set { setValue(_CustRef, value); }
        }
        public string doValidate_CustRef() {
            return doValidate_CustRef(U_CustRef);
        }
        public virtual string doValidate_CustRef(object oValue) {
            return base.doValidation(_CustRef, oValue);
        }

        /**
         * Decription: Carr Waste Lic. Reg. No.
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CWasLic
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CWasLic = "U_CWasLic";
        public string U_CWasLic {
            get {
                return getValueAsString(_CWasLic);
            }
            set { setValue(_CWasLic, value); }
        }
        public string doValidate_CWasLic() {
            return doValidate_CWasLic(U_CWasLic);
        }
        public virtual string doValidate_CWasLic(object oValue) {
            return base.doValidation(_CWasLic, oValue);
        }

        /**
         * Decription: Carrier Postal Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CZpCd
         * Size: 10
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CZpCd = "U_CZpCd";
        public string U_CZpCd {
            get {
                return getValueAsString(_CZpCd);
            }
            set { setValue(_CZpCd, value); }
        }
        public string doValidate_CZpCd() {
            return doValidate_CZpCd(U_CZpCd);
        }
        public virtual string doValidate_CZpCd(object oValue) {
            return base.doValidation(_CZpCd, oValue);
        }

        /**
         * Decription: EN DA Attached
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_DAAttach
         * Size: 1
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _DAAttach = "U_DAAttach";
        public string U_DAAttach {
            get {
                return getValueAsString(_DAAttach);
            }
            set { setValue(_DAAttach, value); }
        }
        public string doValidate_DAAttach() {
            return doValidate_DAAttach(U_DAAttach);
        }
        public virtual string doValidate_DAAttach(object oValue) {
            return base.doValidation(_DAAttach, oValue);
        }

        /**
         * Decription: Email Address
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_E_Mail
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _E_Mail = "U_E_Mail";
        public string U_E_Mail {
            get {
                return getValueAsString(_E_Mail);
            }
            set { setValue(_E_Mail, value); }
        }
        public string doValidate_E_Mail() {
            return doValidate_E_Mail(U_E_Mail);
        }
        public virtual string doValidate_E_Mail(object oValue) {
            return base.doValidation(_E_Mail, oValue);
        }

        /**
         * Decription: EN Number
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ENNO
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ENNO = "U_ENNO";
        public string U_ENNO {
            get {
                return getValueAsString(_ENNO);
            }
            set { setValue(_ENNO, value); }
        }
        public string doValidate_ENNO() {
            return doValidate_ENNO(U_ENNO);
        }
        public virtual string doValidate_ENNO(object oValue) {
            return base.doValidation(_ENNO, oValue);
        }

        /**
         * Decription: EN Period Applied From Date
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ENPApFDt
         * Size: 8
         * Type: db_Date
         * CType: DateTime
         * SubType: st_None
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _ENPApFDt = "U_ENPApFDt";
        public DateTime U_ENPApFDt {
            get {
                return getValueAsDateTime(_ENPApFDt);
            }
            set { setValue(_ENPApFDt, value); }
        }
        public void U_ENPApFDt_AsString(string value) {
            setValue("U_ENPApFDt", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_ENPApFDt_AsString() {
            DateTime dVal = getValueAsDateTime(_ENPApFDt);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_ENPApFDt() {
            return doValidate_ENPApFDt(U_ENPApFDt);
        }
        public virtual string doValidate_ENPApFDt(object oValue) {
            return base.doValidation(_ENPApFDt, oValue);
        }

        /**
         * Decription: EN Period Applied To Date
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ENPApTDt
         * Size: 8
         * Type: db_Date
         * CType: DateTime
         * SubType: st_None
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _ENPApTDt = "U_ENPApTDt";
        public DateTime U_ENPApTDt {
            get {
                return getValueAsDateTime(_ENPApTDt);
            }
            set { setValue(_ENPApTDt, value); }
        }
        public void U_ENPApTDt_AsString(string value) {
            setValue("U_ENPApTDt", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_ENPApTDt_AsString() {
            DateTime dVal = getValueAsDateTime(_ENPApTDt);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_ENPApTDt() {
            return doValidate_ENPApTDt(U_ENPApTDt);
        }
        public virtual string doValidate_ENPApTDt(object oValue) {
            return base.doValidation(_ENPApTDt, oValue);
        }

        /**
         * Decription: First BP Selected
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_FirstBP
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _FirstBP = "U_FirstBP";
        public string U_FirstBP {
            get {
                return getValueAsString(_FirstBP);
            }
            set { setValue(_FirstBP, value); }
        }
        public string doValidate_FirstBP() {
            return doValidate_FirstBP(U_FirstBP);
        }
        public virtual string doValidate_FirstBP(object oValue) {
            return base.doValidation(_FirstBP, oValue);
        }

        /**
         * Decription: Forecast Number
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ForeCS
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ForeCS = "U_ForeCS";
        public string U_ForeCS {
            get {
                return getValueAsString(_ForeCS);
            }
            set { setValue(_ForeCS, value); }
        }
        public string doValidate_ForeCS() {
            return doValidate_ForeCS(U_ForeCS);
        }
        public virtual string doValidate_ForeCS(object oValue) {
            return base.doValidation(_ForeCS, oValue);
        }

        /**
         * Decription: Item Group
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ItemGrp
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ItemGrp = "U_ItemGrp";
        public string U_ItemGrp {
            get {
                return getValueAsString(_ItemGrp);
            }
            set { setValue(_ItemGrp, value); }
        }
        public string doValidate_ItemGrp() {
            return doValidate_ItemGrp(U_ItemGrp);
        }
        public virtual string doValidate_ItemGrp(object oValue) {
            return base.doValidation(_ItemGrp, oValue);
        }

        /**
         * Decription: Lock down Prices
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_LckPrc
         * Size: 1
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _LckPrc = "U_LckPrc";
        public string U_LckPrc {
            get {
                return getValueAsString(_LckPrc);
            }
            set { setValue(_LckPrc, value); }
        }
        public string doValidate_LckPrc() {
            return doValidate_LckPrc(U_LckPrc);
        }
        public virtual string doValidate_LckPrc(object oValue) {
            return base.doValidation(_LckPrc, oValue);
        }

        /**
         * Decription: List Price Gross Margin
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_LPGrossM
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Percentage
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _LPGrossM = "U_LPGrossM";
        public double U_LPGrossM {
            get {
                return getValueAsDouble(_LPGrossM);
            }
            set { setValue(_LPGrossM, value); }
        }
        public string doValidate_LPGrossM() {
            return doValidate_LPGrossM(U_LPGrossM);
        }
        public virtual string doValidate_LPGrossM(object oValue) {
            return base.doValidation(_LPGrossM, oValue);
        }

        /**
         * Decription: List Price Profit
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_LPProfit
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Price
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _LPProfit = "U_LPProfit";
        public double U_LPProfit {
            get {
                return getValueAsDouble(_LPProfit);
            }
            set { setValue(_LPProfit, value); }
        }
        public string doValidate_LPProfit() {
            return doValidate_LPProfit(U_LPProfit);
        }
        public virtual string doValidate_LPProfit(object oValue) {
            return base.doValidation(_LPProfit, oValue);
        }

        /**
         * Decription: List Price
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_LstPrice
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Price
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _LstPrice = "U_LstPrice";
        public double U_LstPrice {
            get {
                return getValueAsDouble(_LstPrice);
            }
            set { setValue(_LstPrice, value); }
        }
        public string doValidate_LstPrice() {
            return doValidate_LstPrice(U_LstPrice);
        }
        public virtual string doValidate_LstPrice(object oValue) {
            return base.doValidation(_LstPrice, oValue);
        }

        /**
         * Decription: Mailing Address
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_MAddress
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _MAddress = "U_MAddress";
        public string U_MAddress {
            get {
                return getValueAsString(_MAddress);
            }
            set { setValue(_MAddress, value); }
        }
        public string doValidate_MAddress() {
            return doValidate_MAddress(U_MAddress);
        }
        public virtual string doValidate_MAddress(object oValue) {
            return base.doValidation(_MAddress, oValue);
        }

        /**
         * Decription: Mailing Address LineNum
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_MAddrsLN
         * Size: 10
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _MAddrsLN = "U_MAddrsLN";
        public string U_MAddrsLN {
            get {
                return getValueAsString(_MAddrsLN);
            }
            set { setValue(_MAddrsLN, value); }
        }
        public string doValidate_MAddrsLN() {
            return doValidate_MAddrsLN(U_MAddrsLN);
        }
        public virtual string doValidate_MAddrsLN(object oValue) {
            return base.doValidation(_MAddrsLN, oValue);
        }

        /**
         * Decription: Mailing Block
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_MBlock
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _MBlock = "U_MBlock";
        public string U_MBlock {
            get {
                return getValueAsString(_MBlock);
            }
            set { setValue(_MBlock, value); }
        }
        public string doValidate_MBlock() {
            return doValidate_MBlock(U_MBlock);
        }
        public virtual string doValidate_MBlock(object oValue) {
            return base.doValidation(_MBlock, oValue);
        }

        /**
         * Decription: Mailing City
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_MCity
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _MCity = "U_MCity";
        public string U_MCity {
            get {
                return getValueAsString(_MCity);
            }
            set { setValue(_MCity, value); }
        }
        public string doValidate_MCity() {
            return doValidate_MCity(U_MCity);
        }
        public virtual string doValidate_MCity(object oValue) {
            return base.doValidation(_MCity, oValue);
        }

        /**
         * Decription: Mailing Contact Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_MContact
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _MContact = "U_MContact";
        public string U_MContact {
            get {
                return getValueAsString(_MContact);
            }
            set { setValue(_MContact, value); }
        }
        public string doValidate_MContact() {
            return doValidate_MContact(U_MContact);
        }
        public virtual string doValidate_MContact(object oValue) {
            return base.doValidation(_MContact, oValue);
        }

        /**
         * Decription: Min. Job Required in a Day
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_MinJob
         * Size: 6
         * Type: db_Numeric
         * CType: short
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _MinJob = "U_MinJob";
        public short U_MinJob {
            get {
                return getValueAsShort(_MinJob);
            }
            set { setValue(_MinJob, value); }
        }
        public string doValidate_MinJob() {
            return doValidate_MinJob(U_MinJob);
        }
        public virtual string doValidate_MinJob(object oValue) {
            return base.doValidation(_MinJob, oValue);
        }

        /**
         * Decription: Mailing Phone Number
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_MPhone1
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _MPhone1 = "U_MPhone1";
        public string U_MPhone1 {
            get {
                return getValueAsString(_MPhone1);
            }
            set { setValue(_MPhone1, value); }
        }
        public string doValidate_MPhone1() {
            return doValidate_MPhone1(U_MPhone1);
        }
        public virtual string doValidate_MPhone1(object oValue) {
            return base.doValidation(_MPhone1, oValue);
        }

        /**
         * Decription: Mailing State
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_MState
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _MState = "U_MState";
        public string U_MState {
            get {
                return getValueAsString(_MState);
            }
            set { setValue(_MState, value); }
        }
        public string doValidate_MState() {
            return doValidate_MState(U_MState);
        }
        public virtual string doValidate_MState(object oValue) {
            return base.doValidation(_MState, oValue);
        }

        /**
         * Decription: Mailing Street
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_MStreet
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _MStreet = "U_MStreet";
        public string U_MStreet {
            get {
                return getValueAsString(_MStreet);
            }
            set { setValue(_MStreet, value); }
        }
        public string doValidate_MStreet() {
            return doValidate_MStreet(U_MStreet);
        }
        public virtual string doValidate_MStreet(object oValue) {
            return base.doValidation(_MStreet, oValue);
        }

        /**
         * Decription: Mailing Postal Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_MZpCd
         * Size: 10
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _MZpCd = "U_MZpCd";
        public string U_MZpCd {
            get {
                return getValueAsString(_MZpCd);
            }
            set { setValue(_MZpCd, value); }
        }
        public string doValidate_MZpCd() {
            return doValidate_MZpCd(U_MZpCd);
        }
        public virtual string doValidate_MZpCd(object oValue) {
            return base.doValidation(_MZpCd, oValue);
        }

        /**
         * Decription: Do not Calculate Tax
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_NoChgTax
         * Size: 1
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _NoChgTax = "U_NoChgTax";
        public string U_NoChgTax {
            get {
                return getValueAsString(_NoChgTax);
            }
            set { setValue(_NoChgTax, value); }
        }
        public string doValidate_NoChgTax() {
            return doValidate_NoChgTax(U_NoChgTax);
        }
        public virtual string doValidate_NoChgTax(object oValue) {
            return base.doValidation(_NoChgTax, oValue);
        }

        /**
         * Decription: Remarks
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Notes
         * Size: 10
         * Type: db_Memo
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Notes = "U_Notes";
        public string U_Notes {
            get {
                return getValueAsString(_Notes);
            }
            set { setValue(_Notes, value); }
        }
        public string doValidate_Notes() {
            return doValidate_Notes(U_Notes);
        }
        public virtual string doValidate_Notes(object oValue) {
            return base.doValidation(_Notes, oValue);
        }

        /**
         * Decription: Order Type (WO,DO)
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ORDTP
         * Size: 5
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ORDTP = "U_ORDTP";
        public string U_ORDTP {
            get {
                return getValueAsString(_ORDTP);
            }
            set { setValue(_ORDTP, value); }
        }
        public string doValidate_ORDTP() {
            return doValidate_ORDTP(U_ORDTP);
        }
        public virtual string doValidate_ORDTP(object oValue) {
            return base.doValidation(_ORDTP, oValue);
        }

        /**
         * Decription: Origin
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Origin
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Origin = "U_Origin";
        public string U_Origin {
            get {
                return getValueAsString(_Origin);
            }
            set { setValue(_Origin, value); }
        }
        public string doValidate_Origin() {
            return doValidate_Origin(U_Origin);
        }
        public virtual string doValidate_Origin(object oValue) {
            return base.doValidation(_Origin, oValue);
        }

        /**
         * Decription: Off Road
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ORoad
         * Size: 1
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ORoad = "U_ORoad";
        public string U_ORoad {
            get {
                return getValueAsString(_ORoad);
            }
            set { setValue(_ORoad, value); }
        }
        public string doValidate_ORoad() {
            return doValidate_ORoad(U_ORoad);
        }
        public virtual string doValidate_ORoad(object oValue) {
            return base.doValidation(_ORoad, oValue);
        }

        /**
         * Decription: Producer Address
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PAddress
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PAddress = "U_PAddress";
        public string U_PAddress {
            get {
                return getValueAsString(_PAddress);
            }
            set { setValue(_PAddress, value); }
        }
        public string doValidate_PAddress() {
            return doValidate_PAddress(U_PAddress);
        }
        public virtual string doValidate_PAddress(object oValue) {
            return base.doValidation(_PAddress, oValue);
        }

        /**
         * Decription: Supplier Address LineNum
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PAddrsLN
         * Size: 10
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PAddrsLN = "U_PAddrsLN";
        public string U_PAddrsLN {
            get {
                return getValueAsString(_PAddrsLN);
            }
            set { setValue(_PAddrsLN, value); }
        }
        public string doValidate_PAddrsLN() {
            return doValidate_PAddrsLN(U_PAddrsLN);
        }
        public virtual string doValidate_PAddrsLN(object oValue) {
            return base.doValidation(_PAddrsLN, oValue);
        }

        /**
         * Decription: PreBook Instruction Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PBICODE
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PBICODE = "U_PBICODE";
        public string U_PBICODE {
            get {
                return getValueAsString(_PBICODE);
            }
            set { setValue(_PBICODE, value); }
        }
        public string doValidate_PBICODE() {
            return doValidate_PBICODE(U_PBICODE);
        }
        public virtual string doValidate_PBICODE(object oValue) {
            return base.doValidation(_PBICODE, oValue);
        }

        /**
         * Decription: Producer Block
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PBlock
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PBlock = "U_PBlock";
        public string U_PBlock {
            get {
                return getValueAsString(_PBlock);
            }
            set { setValue(_PBlock, value); }
        }
        public string doValidate_PBlock() {
            return doValidate_PBlock(U_PBlock);
        }
        public virtual string doValidate_PBlock(object oValue) {
            return base.doValidation(_PBlock, oValue);
        }

        /**
         * Decription: Producer
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PCardCd
         * Size: 15
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PCardCd = "U_PCardCd";
        public string U_PCardCd {
            get {
                return getValueAsString(_PCardCd);
            }
            set { setValue(_PCardCd, value); }
        }
        public string doValidate_PCardCd() {
            return doValidate_PCardCd(U_PCardCd);
        }
        public virtual string doValidate_PCardCd(object oValue) {
            return base.doValidation(_PCardCd, oValue);
        }

        /**
         * Decription: Producer Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PCardNM
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PCardNM = "U_PCardNM";
        public string U_PCardNM {
            get {
                return getValueAsString(_PCardNM);
            }
            set { setValue(_PCardNM, value); }
        }
        public string doValidate_PCardNM() {
            return doValidate_PCardNM(U_PCardNM);
        }
        public virtual string doValidate_PCardNM(object oValue) {
            return base.doValidation(_PCardNM, oValue);
        }

        /**
         * Decription: Producer City
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PCity
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PCity = "U_PCity";
        public string U_PCity {
            get {
                return getValueAsString(_PCity);
            }
            set { setValue(_PCity, value); }
        }
        public string doValidate_PCity() {
            return doValidate_PCity(U_PCity);
        }
        public virtual string doValidate_PCity(object oValue) {
            return base.doValidation(_PCity, oValue);
        }

        /**
         * Decription: Producer Contact Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PContact
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PContact = "U_PContact";
        public string U_PContact {
            get {
                return getValueAsString(_PContact);
            }
            set { setValue(_PContact, value); }
        }
        public string doValidate_PContact() {
            return doValidate_PContact(U_PContact);
        }
        public virtual string doValidate_PContact(object oValue) {
            return base.doValidation(_PContact, oValue);
        }

        /**
         * Decription: Phone Number
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Phone1
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Phone1 = "U_Phone1";
        public string U_Phone1 {
            get {
                return getValueAsString(_Phone1);
            }
            set { setValue(_Phone1, value); }
        }
        public string doValidate_Phone1() {
            return doValidate_Phone1(U_Phone1);
        }
        public virtual string doValidate_Phone1(object oValue) {
            return base.doValidation(_Phone1, oValue);
        }

        /**
         * Decription: Producer Phone Number
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PPhone1
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PPhone1 = "U_PPhone1";
        public string U_PPhone1 {
            get {
                return getValueAsString(_PPhone1);
            }
            set { setValue(_PPhone1, value); }
        }
        public string doValidate_PPhone1() {
            return doValidate_PPhone1(U_PPhone1);
        }
        public virtual string doValidate_PPhone1(object oValue) {
            return base.doValidation(_PPhone1, oValue);
        }

        /**
         * Decription: Premises Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PremCd
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PremCd = "U_PremCd";
        public string U_PremCd {
            get {
                return getValueAsString(_PremCd);
            }
            set { setValue(_PremCd, value); }
        }
        public string doValidate_PremCd() {
            return doValidate_PremCd(U_PremCd);
        }
        public virtual string doValidate_PremCd(object oValue) {
            return base.doValidation(_PremCd, oValue);
        }

        /**
         * Decription: Producer Ref. No.
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ProRef
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ProRef = "U_ProRef";
        public string U_ProRef {
            get {
                return getValueAsString(_ProRef);
            }
            set { setValue(_ProRef, value); }
        }
        public string doValidate_ProRef() {
            return doValidate_ProRef(U_ProRef);
        }
        public virtual string doValidate_ProRef(object oValue) {
            return base.doValidation(_ProRef, oValue);
        }

        /**
         * Decription: Producer State
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PState
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PState = "U_PState";
        public string U_PState {
            get {
                return getValueAsString(_PState);
            }
            set { setValue(_PState, value); }
        }
        public string doValidate_PState() {
            return doValidate_PState(U_PState);
        }
        public virtual string doValidate_PState(object oValue) {
            return base.doValidation(_PState, oValue);
        }

        /**
         * Decription: Producer Street
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PStreet
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PStreet = "U_PStreet";
        public string U_PStreet {
            get {
                return getValueAsString(_PStreet);
            }
            set { setValue(_PStreet, value); }
        }
        public string doValidate_PStreet() {
            return doValidate_PStreet(U_PStreet);
        }
        public virtual string doValidate_PStreet(object oValue) {
            return base.doValidation(_PStreet, oValue);
        }

        /**
         * Decription: Producer Postal Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PZpCd
         * Size: 10
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PZpCd = "U_PZpCd";
        public string U_PZpCd {
            get {
                return getValueAsString(_PZpCd);
            }
            set { setValue(_PZpCd, value); }
        }
        public string doValidate_PZpCd() {
            return doValidate_PZpCd(U_PZpCd);
        }
        public virtual string doValidate_PZpCd(object oValue) {
            return base.doValidation(_PZpCd, oValue);
        }

        /**
         * Decription: Quantity
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Qty
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Measurement
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Qty = "U_Qty";
        public double U_Qty {
            get {
                return getValueAsDouble(_Qty);
            }
            set { setValue(_Qty, value); }
        }
        public string doValidate_Qty() {
            return doValidate_Qty(U_Qty);
        }
        public virtual string doValidate_Qty(object oValue) {
            return base.doValidation(_Qty, oValue);
        }

        /**
         * Decription: Request End Date
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_REDate
         * Size: 8
         * Type: db_Date
         * CType: DateTime
         * SubType: st_None
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _REDate = "U_REDate";
        public DateTime U_REDate {
            get {
                return getValueAsDateTime(_REDate);
            }
            set { setValue(_REDate, value); }
        }
        public void U_REDate_AsString(string value) {
            setValue("U_REDate", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_REDate_AsString() {
            DateTime dVal = getValueAsDateTime(_REDate);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_REDate() {
            return doValidate_REDate(U_REDate);
        }
        public virtual string doValidate_REDate(object oValue) {
            return base.doValidation(_REDate, oValue);
        }

        /**
         * Decription: Request End Time
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RETime
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RETime = "U_RETime";
        public string U_RETime {
            get {
                return getValueAsString(_RETime);
            }
            set { setValue(_RETime, value); }
        }
        public string doValidate_RETime() {
            return doValidate_RETime(U_RETime);
        }
        public virtual string doValidate_RETime(object oValue) {
            return base.doValidation(_RETime, oValue);
        }

        /**
         * Decription: Review Notes
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ReviewNotes
         * Size: 10
         * Type: db_Memo
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ReviewNotes = "U_ReviewNotes";
        public string U_ReviewNotes {
            get {
                return getValueAsString(_ReviewNotes);
            }
            set { setValue(_ReviewNotes, value); }
        }
        public string doValidate_ReviewNotes() {
            return doValidate_ReviewNotes(U_ReviewNotes);
        }
        public virtual string doValidate_ReviewNotes(object oValue) {
            return base.doValidation(_ReviewNotes, oValue);
        }

        /**
         * Decription: Route Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Route
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Route = "U_Route";
        public string U_Route {
            get {
                return getValueAsString(_Route);
            }
            set { setValue(_Route, value); }
        }
        public string doValidate_Route() {
            return doValidate_Route(U_Route);
        }
        public virtual string doValidate_Route(object oValue) {
            return base.doValidation(_Route, oValue);
        }

        /**
         * Decription: Request Start Date
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RSDate
         * Size: 8
         * Type: db_Date
         * CType: DateTime
         * SubType: st_None
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RSDate = "U_RSDate";
        public DateTime U_RSDate {
            get {
                return getValueAsDateTime(_RSDate);
            }
            set { setValue(_RSDate, value); }
        }
        public void U_RSDate_AsString(string value) {
            setValue("U_RSDate", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_RSDate_AsString() {
            DateTime dVal = getValueAsDateTime(_RSDate);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_RSDate() {
            return doValidate_RSDate(U_RSDate);
        }
        public virtual string doValidate_RSDate(object oValue) {
            return base.doValidation(_RSDate, oValue);
        }

        /**
         * Decription: Request End Time
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RSTime
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RSTime = "U_RSTime";
        public string U_RSTime {
            get {
                return getValueAsString(_RSTime);
            }
            set { setValue(_RSTime, value); }
        }
        public string doValidate_RSTime() {
            return doValidate_RSTime(U_RSTime);
        }
        public virtual string doValidate_RSTime(object oValue) {
            return base.doValidation(_RSTime, oValue);
        }

        /**
         * Decription: Required Time From
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RTIMEF
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RTIMEF = "U_RTIMEF";
        public string U_RTIMEF {
            get {
                return getValueAsString(_RTIMEF);
            }
            set { setValue(_RTIMEF, value); }
        }
        public string doValidate_RTIMEF() {
            return doValidate_RTIMEF(U_RTIMEF);
        }
        public virtual string doValidate_RTIMEF(object oValue) {
            return base.doValidation(_RTIMEF, oValue);
        }

        /**
         * Decription: Required Time To
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RTIMET
         * Size: 6
         * Type: db_Date
         * CType: string
         * SubType: st_Time
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RTIMET = "U_RTIMET";
        public string U_RTIMET {
            get {
                return getValueAsString(_RTIMET);
            }
            set { setValue(_RTIMET, value); }
        }
        public string doValidate_RTIMET() {
            return doValidate_RTIMET(U_RTIMET);
        }
        public virtual string doValidate_RTIMET(object oValue) {
            return base.doValidation(_RTIMET, oValue);
        }

        /**
         * Decription: Site Address
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SAddress
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SAddress = "U_SAddress";
        public string U_SAddress {
            get {
                return getValueAsString(_SAddress);
            }
            set { setValue(_SAddress, value); }
        }
        public string doValidate_SAddress() {
            return doValidate_SAddress(U_SAddress);
        }
        public virtual string doValidate_SAddress(object oValue) {
            return base.doValidation(_SAddress, oValue);
        }

        /**
         * Decription: Disposal Address LineNum
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SAddrsLN
         * Size: 10
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SAddrsLN = "U_SAddrsLN";
        public string U_SAddrsLN {
            get {
                return getValueAsString(_SAddrsLN);
            }
            set { setValue(_SAddrsLN, value); }
        }
        public string doValidate_SAddrsLN() {
            return doValidate_SAddrsLN(U_SAddrsLN);
        }
        public virtual string doValidate_SAddrsLN(object oValue) {
            return base.doValidation(_SAddrsLN, oValue);
        }

        /**
         * Decription: Site Block
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SBlock
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SBlock = "U_SBlock";
        public string U_SBlock {
            get {
                return getValueAsString(_SBlock);
            }
            set { setValue(_SBlock, value); }
        }
        public string doValidate_SBlock() {
            return doValidate_SBlock(U_SBlock);
        }
        public virtual string doValidate_SBlock(object oValue) {
            return base.doValidation(_SBlock, oValue);
        }

        /**
         * Decription: Site
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SCardCd
         * Size: 15
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SCardCd = "U_SCardCd";
        public string U_SCardCd {
            get {
                return getValueAsString(_SCardCd);
            }
            set { setValue(_SCardCd, value); }
        }
        public string doValidate_SCardCd() {
            return doValidate_SCardCd(U_SCardCd);
        }
        public virtual string doValidate_SCardCd(object oValue) {
            return base.doValidation(_SCardCd, oValue);
        }

        /**
         * Decription: Site Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SCardNM
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SCardNM = "U_SCardNM";
        public string U_SCardNM {
            get {
                return getValueAsString(_SCardNM);
            }
            set { setValue(_SCardNM, value); }
        }
        public string doValidate_SCardNM() {
            return doValidate_SCardNM(U_SCardNM);
        }
        public virtual string doValidate_SCardNM(object oValue) {
            return base.doValidation(_SCardNM, oValue);
        }

        /**
         * Decription: Site City
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SCity
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SCity = "U_SCity";
        public string U_SCity {
            get {
                return getValueAsString(_SCity);
            }
            set { setValue(_SCity, value); }
        }
        public string doValidate_SCity() {
            return doValidate_SCity(U_SCity);
        }
        public virtual string doValidate_SCity(object oValue) {
            return base.doValidation(_SCity, oValue);
        }

        /**
         * Decription: Site Contact Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SContact
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SContact = "U_SContact";
        public string U_SContact {
            get {
                return getValueAsString(_SContact);
            }
            set { setValue(_SContact, value); }
        }
        public string doValidate_SContact() {
            return doValidate_SContact(U_SContact);
        }
        public virtual string doValidate_SContact(object oValue) {
            return base.doValidation(_SContact, oValue);
        }

        /**
         * Decription: Seperate Tipping and Transport
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SepOrder
         * Size: 1
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SepOrder = "U_SepOrder";
        public string U_SepOrder {
            get {
                return getValueAsString(_SepOrder);
            }
            set { setValue(_SepOrder, value); }
        }
        public string doValidate_SepOrder() {
            return doValidate_SepOrder(U_SepOrder);
        }
        public virtual string doValidate_SepOrder(object oValue) {
            return base.doValidation(_SepOrder, oValue);
        }

        /**
         * Decription: Route Sequence
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Seq
         * Size: 6
         * Type: db_Numeric
         * CType: short
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Seq = "U_Seq";
        public short U_Seq {
            get {
                return getValueAsShort(_Seq);
            }
            set { setValue(_Seq, value); }
        }
        public string doValidate_Seq() {
            return doValidate_Seq(U_Seq);
        }
        public virtual string doValidate_Seq(object oValue) {
            return base.doValidation(_Seq, oValue);
        }

        /**
         * Decription: Site Licence Numbers
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SiteLic
         * Size: 25
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SiteLic = "U_SiteLic";
        public string U_SiteLic {
            get {
                return getValueAsString(_SiteLic);
            }
            set { setValue(_SiteLic, value); }
        }
        public string doValidate_SiteLic() {
            return doValidate_SiteLic(U_SiteLic);
        }
        public virtual string doValidate_SiteLic(object oValue) {
            return base.doValidation(_SiteLic, oValue);
        }

        /**
         * Decription: Site Ref. No.
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SiteRef
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SiteRef = "U_SiteRef";
        public string U_SiteRef {
            get {
                return getValueAsString(_SiteRef);
            }
            set { setValue(_SiteRef, value); }
        }
        public string doValidate_SiteRef() {
            return doValidate_SiteRef(U_SiteRef);
        }
        public virtual string doValidate_SiteRef(object oValue) {
            return base.doValidation(_SiteRef, oValue);
        }

        /**
         * Decription: Site Phone Number
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SiteTl
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SiteTl = "U_SiteTl";
        public string U_SiteTl {
            get {
                return getValueAsString(_SiteTl);
            }
            set { setValue(_SiteTl, value); }
        }
        public string doValidate_SiteTl() {
            return doValidate_SiteTl(U_SiteTl);
        }
        public virtual string doValidate_SiteTl(object oValue) {
            return base.doValidation(_SiteTl, oValue);
        }

        /**
         * Decription: Skip License Charge
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SLicCh
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Price
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _SLicCh = "U_SLicCh";
        public double U_SLicCh {
            get {
                return getValueAsDouble(_SLicCh);
            }
            set { setValue(_SLicCh, value); }
        }
        public string doValidate_SLicCh() {
            return doValidate_SLicCh(U_SLicCh);
        }
        public virtual string doValidate_SLicCh(object oValue) {
            return base.doValidation(_SLicCh, oValue);
        }

        /**
         * Decription: Skip License Expiry Date
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SLicExp
         * Size: 8
         * Type: db_Date
         * CType: DateTime
         * SubType: st_None
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _SLicExp = "U_SLicExp";
        public DateTime U_SLicExp {
            get {
                return getValueAsDateTime(_SLicExp);
            }
            set { setValue(_SLicExp, value); }
        }
        public void U_SLicExp_AsString(string value) {
            setValue("U_SLicExp", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_SLicExp_AsString() {
            DateTime dVal = getValueAsDateTime(_SLicExp);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_SLicExp() {
            return doValidate_SLicExp(U_SLicExp);
        }
        public virtual string doValidate_SLicExp(object oValue) {
            return base.doValidation(_SLicExp, oValue);
        }

        /**
         * Decription: Skip License Supplier Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SLicNm
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SLicNm = "U_SLicNm";
        public string U_SLicNm {
            get {
                return getValueAsString(_SLicNm);
            }
            set { setValue(_SLicNm, value); }
        }
        public string doValidate_SLicNm() {
            return doValidate_SLicNm(U_SLicNm);
        }
        public virtual string doValidate_SLicNm(object oValue) {
            return base.doValidation(_SLicNm, oValue);
        }

        /**
         * Decription: Skip License No
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SLicNr
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SLicNr = "U_SLicNr";
        public string U_SLicNr {
            get {
                return getValueAsString(_SLicNr);
            }
            set { setValue(_SLicNr, value); }
        }
        public string doValidate_SLicNr() {
            return doValidate_SLicNr(U_SLicNr);
        }
        public virtual string doValidate_SLicNr(object oValue) {
            return base.doValidation(_SLicNr, oValue);
        }

        /**
         * Decription: Skip License Supplier
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SLicSp
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SLicSp = "U_SLicSp";
        public string U_SLicSp {
            get {
                return getValueAsString(_SLicSp);
            }
            set { setValue(_SLicSp, value); }
        }
        public string doValidate_SLicSp() {
            return doValidate_SLicSp(U_SLicSp);
        }
        public virtual string doValidate_SLicSp(object oValue) {
            return base.doValidation(_SLicSp, oValue);
        }

        /**
         * Decription: Source
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Source
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Source = "U_Source";
        public int U_Source {
            get {
                return getValueAsInt(_Source);
            }
            set { setValue(_Source, value); }
        }
        public string doValidate_Source() {
            return doValidate_Source(U_Source);
        }
        public virtual string doValidate_Source(object oValue) {
            return base.doValidation(_Source, oValue);
        }

        /**
         * Decription: Site Phone Number
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SPhone1
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SPhone1 = "U_SPhone1";
        public string U_SPhone1 {
            get {
                return getValueAsString(_SPhone1);
            }
            set { setValue(_SPhone1, value); }
        }
        public string doValidate_SPhone1() {
            return doValidate_SPhone1(U_SPhone1);
        }
        public virtual string doValidate_SPhone1(object oValue) {
            return base.doValidation(_SPhone1, oValue);
        }

        /**
         * Decription: Special Instruction
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SpInst
         * Size: 230
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SpInst = "U_SpInst";
        public string U_SpInst {
            get {
                return getValueAsString(_SpInst);
            }
            set { setValue(_SpInst, value); }
        }
        public string doValidate_SpInst() {
            return doValidate_SpInst(U_SpInst);
        }
        public virtual string doValidate_SpInst(object oValue) {
            return base.doValidation(_SpInst, oValue);
        }

        /**
         * Decription: Site State
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SState
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SState = "U_SState";
        public string U_SState {
            get {
                return getValueAsString(_SState);
            }
            set { setValue(_SState, value); }
        }
        public string doValidate_SState() {
            return doValidate_SState(U_SState);
        }
        public virtual string doValidate_SState(object oValue) {
            return base.doValidation(_SState, oValue);
        }

        /**
         * Decription: Site Street
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SStreet
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SStreet = "U_SStreet";
        public string U_SStreet {
            get {
                return getValueAsString(_SStreet);
            }
            set { setValue(_SStreet, value); }
        }
        public string doValidate_SStreet() {
            return doValidate_SStreet(U_SStreet);
        }
        public virtual string doValidate_SStreet(object oValue) {
            return base.doValidation(_SStreet, oValue);
        }

        /**
         * Decription: State
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_State
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _State = "U_State";
        public string U_State {
            get {
                return getValueAsString(_State);
            }
            set { setValue(_State, value); }
        }
        public string doValidate_State() {
            return doValidate_State(U_State);
        }
        public virtual string doValidate_State(object oValue) {
            return base.doValidation(_State, oValue);
        }

        /**
         * Decription: Order Status
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Status
         * Size: 10
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Status = "U_Status";
        public string U_Status {
            get {
                return getValueAsString(_Status);
            }
            set { setValue(_Status, value); }
        }
        public string doValidate_Status() {
            return doValidate_Status(U_Status);
        }
        public virtual string doValidate_Status(object oValue) {
            return base.doValidation(_Status, oValue);
        }

        /**
         * Decription: Site Id
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SteId
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SteId = "U_SteId";
        public string U_SteId {
            get {
                return getValueAsString(_SteId);
            }
            set { setValue(_SteId, value); }
        }
        public string doValidate_SteId() {
            return doValidate_SteId(U_SteId);
        }
        public virtual string doValidate_SteId(object oValue) {
            return base.doValidation(_SteId, oValue);
        }

        /**
         * Decription: Street
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Street
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Street = "U_Street";
        public string U_Street {
            get {
                return getValueAsString(_Street);
            }
            set { setValue(_Street, value); }
        }
        public string doValidate_Street() {
            return doValidate_Street(U_Street);
        }
        public virtual string doValidate_Street(object oValue) {
            return base.doValidation(_Street, oValue);
        }

        /**
         * Decription: Supplier Ref
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SupRef
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SupRef = "U_SupRef";
        public string U_SupRef {
            get {
                return getValueAsString(_SupRef);
            }
            set { setValue(_SupRef, value); }
        }
        public string doValidate_SupRef() {
            return doValidate_SupRef(U_SupRef);
        }
        public virtual string doValidate_SupRef(object oValue) {
            return base.doValidation(_SupRef, oValue);
        }

        /**
         * Decription: Site Postal Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_SZpCd
         * Size: 10
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _SZpCd = "U_SZpCd";
        public string U_SZpCd {
            get {
                return getValueAsString(_SZpCd);
            }
            set { setValue(_SZpCd, value); }
        }
        public string doValidate_SZpCd() {
            return doValidate_SZpCd(U_SZpCd);
        }
        public virtual string doValidate_SZpCd(object oValue) {
            return base.doValidation(_SZpCd, oValue);
        }

        /**
         * Decription: List Price Gross Margin
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_TCharge
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Price
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _TCharge = "U_TCharge";
        public double U_TCharge {
            get {
                return getValueAsDouble(_TCharge);
            }
            set { setValue(_TCharge, value); }
        }
        public string doValidate_TCharge() {
            return doValidate_TCharge(U_TCharge);
        }
        public virtual string doValidate_TCharge(object oValue) {
            return base.doValidation(_TCharge, oValue);
        }

        /**
         * Decription: Transaction Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_TRNCd
         * Size: 10
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _TRNCd = "U_TRNCd";
        public string U_TRNCd {
            get {
                return getValueAsString(_TRNCd);
            }
            set { setValue(_TRNCd, value); }
        }
        public string doValidate_TRNCd() {
            return doValidate_TRNCd(U_TRNCd);
        }
        public virtual string doValidate_TRNCd(object oValue) {
            return base.doValidation(_TRNCd, oValue);
        }

        /**
         * Decription: Signed In User
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_User
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _User = "U_User";
        public int U_User {
            get {
                return getValueAsInt(_User);
            }
            set { setValue(_User, value); }
        }
        public string doValidate_User() {
            return doValidate_User(U_User);
        }
        public virtual string doValidate_User(object oValue) {
            return base.doValidation(_User, oValue);
        }

        /**
         * Decription: Version
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Version
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Version = "U_Version";
        public int U_Version {
            get {
                return getValueAsInt(_Version);
            }
            set { setValue(_Version, value); }
        }
        public string doValidate_Version() {
            return doValidate_Version(U_Version);
        }
        public virtual string doValidate_Version(object oValue) {
            return base.doValidation(_Version, oValue);
        }

        /**
         * Decription: Cus Waste Lic. Reg. No.
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WasLic
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _WasLic = "U_WasLic";
        public string U_WasLic {
            get {
                return getValueAsString(_WasLic);
            }
            set { setValue(_WasLic, value); }
        }
        public string doValidate_WasLic() {
            return doValidate_WasLic(U_WasLic);
        }
        public virtual string doValidate_WasLic(object oValue) {
            return base.doValidation(_WasLic, oValue);
        }

        /**
         * Decription: WOH ID
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WOHID
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _WOHID = "U_WOHID";
        public string U_WOHID {
            get {
                return getValueAsString(_WOHID);
            }
            set { setValue(_WOHID, value); }
        }
        public string doValidate_WOHID() {
            return doValidate_WOHID(U_WOHID);
        }
        public virtual string doValidate_WOHID(object oValue) {
            return base.doValidation(_WOHID, oValue);
        }

        /**
         * Decription: Postal Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ZpCd
         * Size: 10
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ZpCd = "U_ZpCd";
        public string U_ZpCd {
            get {
                return getValueAsString(_ZpCd);
            }
            set { setValue(_ZpCd, value); }
        }
        public string doValidate_ZpCd() {
            return doValidate_ZpCd(U_ZpCd);
        }
        public virtual string doValidate_ZpCd(object oValue) {
            return base.doValidation(_ZpCd, oValue);
        }



        /**
         * Decription: Original WOQ Number
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_OrgDocNm
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _OrgDocNm = "U_OrgDocNm";
        public string U_OrgDocNm {
            get {
                return getValueAsString(_OrgDocNm);
            }
            set { setValue(_OrgDocNm, value); }
        }
        public string doValidate_OrgDocNm() {
            return doValidate_OrgDocNm(U_OrgDocNm);
        }
        public virtual string doValidate_OrgDocNm(object oValue) {
            return base.doValidation(_OrgDocNm, oValue);
        }

        #endregion


        #region FieldInfoSetup
        protected override void doSetFieldInfo() {
            if (moDBFields == null)
                moDBFields = new DBFields(124);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_Address, "Address", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Address
            moDBFields.Add(_AddrssLN, "Customer Address LineNum", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Customer Address LineNum
            moDBFields.Add(_AppRequired, "Approval Required", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Approval Required
            moDBFields.Add(_BDate, "Booking Date", 5, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Booking Date
            moDBFields.Add(_Block, "Block", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Block
            moDBFields.Add(_Branch, "Branch", 7, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Branch
            moDBFields.Add(_BTime, "Booking Time", 8, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Booking Time
            moDBFields.Add(_CAddress, "Carrier Address", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Carrier Address
            moDBFields.Add(_CAddrsLN, "Haulier Address LineNum", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Haulier Address LineNum
            moDBFields.Add(_CardCd, "Customer", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Customer
            moDBFields.Add(_CardNM, "Customer Name", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Customer Name
            moDBFields.Add(_CBlock, "Carrier Block", 13, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Carrier Block
            moDBFields.Add(_CCardCd, "Carrier", 14, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Carrier
            moDBFields.Add(_CCardNM, "Carrier Name", 15, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Carrier Name
            moDBFields.Add(_CCity, "Carrier City", 16, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Carrier City
            moDBFields.Add(_CContact, "Carrier Contact Name", 17, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Carrier Contact Name
            moDBFields.Add(_ChgPrice, "Charge Price", 18, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Charge Price
            moDBFields.Add(_ChgVAT, "Tax on Charge Price", 19, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Tax on Charge Price
            moDBFields.Add(_City, "City", 20, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //City
            moDBFields.Add(_ClgCode, "Number", 21, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Number
            moDBFields.Add(_CntrNo, "Contract No.", 22, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Contract No.
            moDBFields.Add(_Contact, "Contact Name", 23, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Contact Name
            moDBFields.Add(_CopTRw, "Copy Instruction to rows", 24, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 5, EMPTYSTR, false, false); //Copy Instruction to rows
            moDBFields.Add(_Cost, "Cost", 25, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Cost
            moDBFields.Add(_Country, "Country", 26, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //Country
            moDBFields.Add(_County, "County", 27, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //County
            moDBFields.Add(_CPGrossM, "Charge Price Gross Margin", 28, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 20, 0, false, false); //Charge Price Gross Margin
            moDBFields.Add(_CPhone1, "Carrier Phone Number", 29, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Carrier Phone Number
            moDBFields.Add(_CPProfit, "Charge Price Profit", 30, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Charge Price Profit
            moDBFields.Add(_CState, "Carrier State", 31, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Carrier State
            moDBFields.Add(_CStreet, "Carrier Street", 32, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Carrier Street
            moDBFields.Add(_CusFrnNm, "Customer Foreign Name", 33, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Customer Foreign Name
            moDBFields.Add(_CustRef, "Customer Ref", 34, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Customer Ref
            moDBFields.Add(_CWasLic, "Carr Waste Lic. Reg. No.", 35, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Carr Waste Lic. Reg. No.
            moDBFields.Add(_CZpCd, "Carrier Postal Code", 36, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Carrier Postal Code
            moDBFields.Add(_DAAttach, "EN DA Attached", 37, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //EN DA Attached
            moDBFields.Add(_E_Mail, "Email Address", 38, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Email Address
            moDBFields.Add(_ENNO, "EN Number", 39, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //EN Number
            moDBFields.Add(_ENPApFDt, "EN Period Applied From Date", 40, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //EN Period Applied From Date
            moDBFields.Add(_ENPApTDt, "EN Period Applied To Date", 41, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //EN Period Applied To Date
            moDBFields.Add(_FirstBP, "First BP Selected", 42, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //First BP Selected
            moDBFields.Add(_ForeCS, "Forecast Number", 43, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Forecast Number
            moDBFields.Add(_ItemGrp, "Item Group", 44, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Item Group
            moDBFields.Add(_LckPrc, "Lock down Prices", 45, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Lock down Prices
            moDBFields.Add(_LPGrossM, "List Price Gross Margin", 46, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 20, 0, false, false); //List Price Gross Margin
            moDBFields.Add(_LPProfit, "List Price Profit", 47, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //List Price Profit
            moDBFields.Add(_LstPrice, "List Price", 48, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //List Price
            moDBFields.Add(_MAddress, "Mailing Address", 49, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Mailing Address
            moDBFields.Add(_MAddrsLN, "Mailing Address LineNum", 50, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Mailing Address LineNum
            moDBFields.Add(_MBlock, "Mailing Block", 51, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Mailing Block
            moDBFields.Add(_MCity, "Mailing City", 52, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Mailing City
            moDBFields.Add(_MContact, "Mailing Contact Name", 53, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Mailing Contact Name
            moDBFields.Add(_MinJob, "Min. Job Required in a Day", 54, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Min. Job Required in a Day
            moDBFields.Add(_MPhone1, "Mailing Phone Number", 55, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Mailing Phone Number
            moDBFields.Add(_MState, "Mailing State", 56, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Mailing State
            moDBFields.Add(_MStreet, "Mailing Street", 57, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Mailing Street
            moDBFields.Add(_MZpCd, "Mailing Postal Code", 58, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Mailing Postal Code
            moDBFields.Add(_NoChgTax, "Do not Calculate Tax", 59, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Do not Calculate Tax
            moDBFields.Add(_Notes, "Remarks", 60, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Remarks
            moDBFields.Add(_ORDTP, "Order Type (WO,DO)", 61, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 5, EMPTYSTR, false, false); //Order Type (WO,DO)
            moDBFields.Add(_Origin, "Origin", 62, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Origin
            moDBFields.Add(_ORoad, "Off Road", 63, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Off Road
            moDBFields.Add(_PAddress, "Producer Address", 64, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Producer Address
            moDBFields.Add(_PAddrsLN, "Supplier Address LineNum", 65, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Supplier Address LineNum
            moDBFields.Add(_PBICODE, "PreBook Instruction Code", 66, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //PreBook Instruction Code
            moDBFields.Add(_PBlock, "Producer Block", 67, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Producer Block
            moDBFields.Add(_PCardCd, "Producer", 68, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Producer
            moDBFields.Add(_PCardNM, "Producer Name", 69, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Producer Name
            moDBFields.Add(_PCity, "Producer City", 70, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Producer City
            moDBFields.Add(_PContact, "Producer Contact Name", 71, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Producer Contact Name
            moDBFields.Add(_Phone1, "Phone Number", 72, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Phone Number
            moDBFields.Add(_PPhone1, "Producer Phone Number", 73, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Producer Phone Number
            moDBFields.Add(_PremCd, "Premises Code", 74, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Premises Code
            moDBFields.Add(_ProRef, "Producer Ref. No.", 75, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Producer Ref. No.
            moDBFields.Add(_PState, "Producer State", 76, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Producer State
            moDBFields.Add(_PStreet, "Producer Street", 77, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Producer Street
            moDBFields.Add(_PZpCd, "Producer Postal Code", 78, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Producer Postal Code
            moDBFields.Add(_Qty, "Quantity", 79, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 20, 0, false, false); //Quantity
            moDBFields.Add(_REDate, "Request End Date", 80, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Request End Date
            moDBFields.Add(_RETime, "Request End Time", 81, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Request End Time
            moDBFields.Add(_ReviewNotes, "Review Notes", 82, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Review Notes
            moDBFields.Add(_Route, "Route Code", 83, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Route Code
            moDBFields.Add(_RSDate, "Request Start Date", 84, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Request Start Date
            moDBFields.Add(_RSTime, "Request End Time", 85, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Request End Time
            moDBFields.Add(_RTIMEF, "Required Time From", 86, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Required Time From
            moDBFields.Add(_RTIMET, "Required Time To", 87, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Required Time To
            moDBFields.Add(_SAddress, "Site Address", 88, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Site Address
            moDBFields.Add(_SAddrsLN, "Disposal Address LineNum", 89, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Disposal Address LineNum
            moDBFields.Add(_SBlock, "Site Block", 90, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Site Block
            moDBFields.Add(_SCardCd, "Site", 91, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Site
            moDBFields.Add(_SCardNM, "Site Name", 92, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Site Name
            moDBFields.Add(_SCity, "Site City", 93, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Site City
            moDBFields.Add(_SContact, "Site Contact Name", 94, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Site Contact Name
            moDBFields.Add(_SepOrder, "Seperate Tipping and Transport", 95, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Seperate Tipping and Transport
            moDBFields.Add(_Seq, "Route Sequence", 96, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Route Sequence
            moDBFields.Add(_SiteLic, "Site Licence Numbers", 97, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 25, EMPTYSTR, false, false); //Site Licence Numbers
            moDBFields.Add(_SiteRef, "Site Ref. No.", 98, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Site Ref. No.
            moDBFields.Add(_SiteTl, "Site Phone Number", 99, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Site Phone Number
            moDBFields.Add(_SLicCh, "Skip License Charge", 100, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Skip License Charge
            moDBFields.Add(_SLicExp, "Skip License Expiry Date", 101, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Skip License Expiry Date
            moDBFields.Add(_SLicNm, "Skip License Supplier Name", 102, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Skip License Supplier Name
            moDBFields.Add(_SLicNr, "Skip License No", 103, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Skip License No
            moDBFields.Add(_SLicSp, "Skip License Supplier", 104, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Skip License Supplier
            moDBFields.Add(_Source, "Source", 105, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Source
            moDBFields.Add(_SPhone1, "Site Phone Number", 106, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Site Phone Number
            moDBFields.Add(_SpInst, "Special Instruction", 107, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 230, EMPTYSTR, false, false); //Special Instruction
            moDBFields.Add(_SState, "Site State", 108, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Site State
            moDBFields.Add(_SStreet, "Site Street", 109, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Site Street
            moDBFields.Add(_State, "State", 110, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //State
            moDBFields.Add(_Status, "Order Status", 111, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Order Status
            moDBFields.Add(_SteId, "Site Id", 112, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Site Id
            moDBFields.Add(_Street, "Street", 113, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Street
            moDBFields.Add(_SupRef, "Supplier Ref", 114, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Supplier Ref
            moDBFields.Add(_SZpCd, "Site Postal Code", 115, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Site Postal Code
            moDBFields.Add(_TCharge, "List Price Gross Margin", 116, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //List Price Gross Margin
            moDBFields.Add(_TRNCd, "Transaction Code", 117, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Transaction Code
            moDBFields.Add(_User, "Signed In User", 118, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Signed In User
            moDBFields.Add(_Version, "Version", 119, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Version
            moDBFields.Add(_WasLic, "Cus Waste Lic. Reg. No.", 120, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Cus Waste Lic. Reg. No.
            moDBFields.Add(_WOHID, "WOH ID", 121, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //WOH ID
            moDBFields.Add(_ZpCd, "Postal Code", 122, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Postal Code
            moDBFields.Add(_OrgDocNm, "Original WOQ Number", 123, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Original WOQ Number

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation() {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_Address());
            doBuildValidationString(doValidate_AddrssLN());
            doBuildValidationString(doValidate_AppRequired());
            doBuildValidationString(doValidate_BDate());
            doBuildValidationString(doValidate_Block());
            doBuildValidationString(doValidate_Branch());
            doBuildValidationString(doValidate_BTime());
            doBuildValidationString(doValidate_CAddress());
            doBuildValidationString(doValidate_CAddrsLN());
            doBuildValidationString(doValidate_CardCd());
            doBuildValidationString(doValidate_CardNM());
            doBuildValidationString(doValidate_CBlock());
            doBuildValidationString(doValidate_CCardCd());
            doBuildValidationString(doValidate_CCardNM());
            doBuildValidationString(doValidate_CCity());
            doBuildValidationString(doValidate_CContact());
            doBuildValidationString(doValidate_ChgPrice());
            doBuildValidationString(doValidate_ChgVAT());
            doBuildValidationString(doValidate_City());
            doBuildValidationString(doValidate_ClgCode());
            doBuildValidationString(doValidate_CntrNo());
            doBuildValidationString(doValidate_Contact());
            doBuildValidationString(doValidate_CopTRw());
            doBuildValidationString(doValidate_Cost());
            doBuildValidationString(doValidate_Country());
            doBuildValidationString(doValidate_County());
            doBuildValidationString(doValidate_CPGrossM());
            doBuildValidationString(doValidate_CPhone1());
            doBuildValidationString(doValidate_CPProfit());
            doBuildValidationString(doValidate_CState());
            doBuildValidationString(doValidate_CStreet());
            doBuildValidationString(doValidate_CusFrnNm());
            doBuildValidationString(doValidate_CustRef());
            doBuildValidationString(doValidate_CWasLic());
            doBuildValidationString(doValidate_CZpCd());
            doBuildValidationString(doValidate_DAAttach());
            doBuildValidationString(doValidate_E_Mail());
            doBuildValidationString(doValidate_ENNO());
            doBuildValidationString(doValidate_ENPApFDt());
            doBuildValidationString(doValidate_ENPApTDt());
            doBuildValidationString(doValidate_FirstBP());
            doBuildValidationString(doValidate_ForeCS());
            doBuildValidationString(doValidate_ItemGrp());
            doBuildValidationString(doValidate_LckPrc());
            doBuildValidationString(doValidate_LPGrossM());
            doBuildValidationString(doValidate_LPProfit());
            doBuildValidationString(doValidate_LstPrice());
            doBuildValidationString(doValidate_MAddress());
            doBuildValidationString(doValidate_MAddrsLN());
            doBuildValidationString(doValidate_MBlock());
            doBuildValidationString(doValidate_MCity());
            doBuildValidationString(doValidate_MContact());
            doBuildValidationString(doValidate_MinJob());
            doBuildValidationString(doValidate_MPhone1());
            doBuildValidationString(doValidate_MState());
            doBuildValidationString(doValidate_MStreet());
            doBuildValidationString(doValidate_MZpCd());
            doBuildValidationString(doValidate_NoChgTax());
            doBuildValidationString(doValidate_Notes());
            doBuildValidationString(doValidate_ORDTP());
            doBuildValidationString(doValidate_Origin());
            doBuildValidationString(doValidate_ORoad());
            doBuildValidationString(doValidate_PAddress());
            doBuildValidationString(doValidate_PAddrsLN());
            doBuildValidationString(doValidate_PBICODE());
            doBuildValidationString(doValidate_PBlock());
            doBuildValidationString(doValidate_PCardCd());
            doBuildValidationString(doValidate_PCardNM());
            doBuildValidationString(doValidate_PCity());
            doBuildValidationString(doValidate_PContact());
            doBuildValidationString(doValidate_Phone1());
            doBuildValidationString(doValidate_PPhone1());
            doBuildValidationString(doValidate_PremCd());
            doBuildValidationString(doValidate_ProRef());
            doBuildValidationString(doValidate_PState());
            doBuildValidationString(doValidate_PStreet());
            doBuildValidationString(doValidate_PZpCd());
            doBuildValidationString(doValidate_Qty());
            doBuildValidationString(doValidate_REDate());
            doBuildValidationString(doValidate_RETime());
            doBuildValidationString(doValidate_ReviewNotes());
            doBuildValidationString(doValidate_Route());
            doBuildValidationString(doValidate_RSDate());
            doBuildValidationString(doValidate_RSTime());
            doBuildValidationString(doValidate_RTIMEF());
            doBuildValidationString(doValidate_RTIMET());
            doBuildValidationString(doValidate_SAddress());
            doBuildValidationString(doValidate_SAddrsLN());
            doBuildValidationString(doValidate_SBlock());
            doBuildValidationString(doValidate_SCardCd());
            doBuildValidationString(doValidate_SCardNM());
            doBuildValidationString(doValidate_SCity());
            doBuildValidationString(doValidate_SContact());
            doBuildValidationString(doValidate_SepOrder());
            doBuildValidationString(doValidate_Seq());
            doBuildValidationString(doValidate_SiteLic());
            doBuildValidationString(doValidate_SiteRef());
            doBuildValidationString(doValidate_SiteTl());
            doBuildValidationString(doValidate_SLicCh());
            doBuildValidationString(doValidate_SLicExp());
            doBuildValidationString(doValidate_SLicNm());
            doBuildValidationString(doValidate_SLicNr());
            doBuildValidationString(doValidate_SLicSp());
            doBuildValidationString(doValidate_Source());
            doBuildValidationString(doValidate_SPhone1());
            doBuildValidationString(doValidate_SpInst());
            doBuildValidationString(doValidate_SState());
            doBuildValidationString(doValidate_SStreet());
            doBuildValidationString(doValidate_State());
            doBuildValidationString(doValidate_Status());
            doBuildValidationString(doValidate_SteId());
            doBuildValidationString(doValidate_Street());
            doBuildValidationString(doValidate_SupRef());
            doBuildValidationString(doValidate_SZpCd());
            doBuildValidationString(doValidate_TCharge());
            doBuildValidationString(doValidate_TRNCd());
            doBuildValidationString(doValidate_User());
            doBuildValidationString(doValidate_Version());
            doBuildValidationString(doValidate_WasLic());
            doBuildValidationString(doValidate_WOHID());
            doBuildValidationString(doValidate_ZpCd());
            doBuildValidationString(doValidate_OrgDocNm());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_Address)) return doValidate_Address(oValue);
            if (sFieldName.Equals(_AddrssLN)) return doValidate_AddrssLN(oValue);
            if (sFieldName.Equals(_AppRequired)) return doValidate_AppRequired(oValue);
            if (sFieldName.Equals(_BDate)) return doValidate_BDate(oValue);
            if (sFieldName.Equals(_Block)) return doValidate_Block(oValue);
            if (sFieldName.Equals(_Branch)) return doValidate_Branch(oValue);
            if (sFieldName.Equals(_BTime)) return doValidate_BTime(oValue);
            if (sFieldName.Equals(_CAddress)) return doValidate_CAddress(oValue);
            if (sFieldName.Equals(_CAddrsLN)) return doValidate_CAddrsLN(oValue);
            if (sFieldName.Equals(_CardCd)) return doValidate_CardCd(oValue);
            if (sFieldName.Equals(_CardNM)) return doValidate_CardNM(oValue);
            if (sFieldName.Equals(_CBlock)) return doValidate_CBlock(oValue);
            if (sFieldName.Equals(_CCardCd)) return doValidate_CCardCd(oValue);
            if (sFieldName.Equals(_CCardNM)) return doValidate_CCardNM(oValue);
            if (sFieldName.Equals(_CCity)) return doValidate_CCity(oValue);
            if (sFieldName.Equals(_CContact)) return doValidate_CContact(oValue);
            if (sFieldName.Equals(_ChgPrice)) return doValidate_ChgPrice(oValue);
            if (sFieldName.Equals(_ChgVAT)) return doValidate_ChgVAT(oValue);
            if (sFieldName.Equals(_City)) return doValidate_City(oValue);
            if (sFieldName.Equals(_ClgCode)) return doValidate_ClgCode(oValue);
            if (sFieldName.Equals(_CntrNo)) return doValidate_CntrNo(oValue);
            if (sFieldName.Equals(_Contact)) return doValidate_Contact(oValue);
            if (sFieldName.Equals(_CopTRw)) return doValidate_CopTRw(oValue);
            if (sFieldName.Equals(_Cost)) return doValidate_Cost(oValue);
            if (sFieldName.Equals(_Country)) return doValidate_Country(oValue);
            if (sFieldName.Equals(_County)) return doValidate_County(oValue);
            if (sFieldName.Equals(_CPGrossM)) return doValidate_CPGrossM(oValue);
            if (sFieldName.Equals(_CPhone1)) return doValidate_CPhone1(oValue);
            if (sFieldName.Equals(_CPProfit)) return doValidate_CPProfit(oValue);
            if (sFieldName.Equals(_CState)) return doValidate_CState(oValue);
            if (sFieldName.Equals(_CStreet)) return doValidate_CStreet(oValue);
            if (sFieldName.Equals(_CusFrnNm)) return doValidate_CusFrnNm(oValue);
            if (sFieldName.Equals(_CustRef)) return doValidate_CustRef(oValue);
            if (sFieldName.Equals(_CWasLic)) return doValidate_CWasLic(oValue);
            if (sFieldName.Equals(_CZpCd)) return doValidate_CZpCd(oValue);
            if (sFieldName.Equals(_DAAttach)) return doValidate_DAAttach(oValue);
            if (sFieldName.Equals(_E_Mail)) return doValidate_E_Mail(oValue);
            if (sFieldName.Equals(_ENNO)) return doValidate_ENNO(oValue);
            if (sFieldName.Equals(_ENPApFDt)) return doValidate_ENPApFDt(oValue);
            if (sFieldName.Equals(_ENPApTDt)) return doValidate_ENPApTDt(oValue);
            if (sFieldName.Equals(_FirstBP)) return doValidate_FirstBP(oValue);
            if (sFieldName.Equals(_ForeCS)) return doValidate_ForeCS(oValue);
            if (sFieldName.Equals(_ItemGrp)) return doValidate_ItemGrp(oValue);
            if (sFieldName.Equals(_LckPrc)) return doValidate_LckPrc(oValue);
            if (sFieldName.Equals(_LPGrossM)) return doValidate_LPGrossM(oValue);
            if (sFieldName.Equals(_LPProfit)) return doValidate_LPProfit(oValue);
            if (sFieldName.Equals(_LstPrice)) return doValidate_LstPrice(oValue);
            if (sFieldName.Equals(_MAddress)) return doValidate_MAddress(oValue);
            if (sFieldName.Equals(_MAddrsLN)) return doValidate_MAddrsLN(oValue);
            if (sFieldName.Equals(_MBlock)) return doValidate_MBlock(oValue);
            if (sFieldName.Equals(_MCity)) return doValidate_MCity(oValue);
            if (sFieldName.Equals(_MContact)) return doValidate_MContact(oValue);
            if (sFieldName.Equals(_MinJob)) return doValidate_MinJob(oValue);
            if (sFieldName.Equals(_MPhone1)) return doValidate_MPhone1(oValue);
            if (sFieldName.Equals(_MState)) return doValidate_MState(oValue);
            if (sFieldName.Equals(_MStreet)) return doValidate_MStreet(oValue);
            if (sFieldName.Equals(_MZpCd)) return doValidate_MZpCd(oValue);
            if (sFieldName.Equals(_NoChgTax)) return doValidate_NoChgTax(oValue);
            if (sFieldName.Equals(_Notes)) return doValidate_Notes(oValue);
            if (sFieldName.Equals(_ORDTP)) return doValidate_ORDTP(oValue);
            if (sFieldName.Equals(_Origin)) return doValidate_Origin(oValue);
            if (sFieldName.Equals(_ORoad)) return doValidate_ORoad(oValue);
            if (sFieldName.Equals(_PAddress)) return doValidate_PAddress(oValue);
            if (sFieldName.Equals(_PAddrsLN)) return doValidate_PAddrsLN(oValue);
            if (sFieldName.Equals(_PBICODE)) return doValidate_PBICODE(oValue);
            if (sFieldName.Equals(_PBlock)) return doValidate_PBlock(oValue);
            if (sFieldName.Equals(_PCardCd)) return doValidate_PCardCd(oValue);
            if (sFieldName.Equals(_PCardNM)) return doValidate_PCardNM(oValue);
            if (sFieldName.Equals(_PCity)) return doValidate_PCity(oValue);
            if (sFieldName.Equals(_PContact)) return doValidate_PContact(oValue);
            if (sFieldName.Equals(_Phone1)) return doValidate_Phone1(oValue);
            if (sFieldName.Equals(_PPhone1)) return doValidate_PPhone1(oValue);
            if (sFieldName.Equals(_PremCd)) return doValidate_PremCd(oValue);
            if (sFieldName.Equals(_ProRef)) return doValidate_ProRef(oValue);
            if (sFieldName.Equals(_PState)) return doValidate_PState(oValue);
            if (sFieldName.Equals(_PStreet)) return doValidate_PStreet(oValue);
            if (sFieldName.Equals(_PZpCd)) return doValidate_PZpCd(oValue);
            if (sFieldName.Equals(_Qty)) return doValidate_Qty(oValue);
            if (sFieldName.Equals(_REDate)) return doValidate_REDate(oValue);
            if (sFieldName.Equals(_RETime)) return doValidate_RETime(oValue);
            if (sFieldName.Equals(_ReviewNotes)) return doValidate_ReviewNotes(oValue);
            if (sFieldName.Equals(_Route)) return doValidate_Route(oValue);
            if (sFieldName.Equals(_RSDate)) return doValidate_RSDate(oValue);
            if (sFieldName.Equals(_RSTime)) return doValidate_RSTime(oValue);
            if (sFieldName.Equals(_RTIMEF)) return doValidate_RTIMEF(oValue);
            if (sFieldName.Equals(_RTIMET)) return doValidate_RTIMET(oValue);
            if (sFieldName.Equals(_SAddress)) return doValidate_SAddress(oValue);
            if (sFieldName.Equals(_SAddrsLN)) return doValidate_SAddrsLN(oValue);
            if (sFieldName.Equals(_SBlock)) return doValidate_SBlock(oValue);
            if (sFieldName.Equals(_SCardCd)) return doValidate_SCardCd(oValue);
            if (sFieldName.Equals(_SCardNM)) return doValidate_SCardNM(oValue);
            if (sFieldName.Equals(_SCity)) return doValidate_SCity(oValue);
            if (sFieldName.Equals(_SContact)) return doValidate_SContact(oValue);
            if (sFieldName.Equals(_SepOrder)) return doValidate_SepOrder(oValue);
            if (sFieldName.Equals(_Seq)) return doValidate_Seq(oValue);
            if (sFieldName.Equals(_SiteLic)) return doValidate_SiteLic(oValue);
            if (sFieldName.Equals(_SiteRef)) return doValidate_SiteRef(oValue);
            if (sFieldName.Equals(_SiteTl)) return doValidate_SiteTl(oValue);
            if (sFieldName.Equals(_SLicCh)) return doValidate_SLicCh(oValue);
            if (sFieldName.Equals(_SLicExp)) return doValidate_SLicExp(oValue);
            if (sFieldName.Equals(_SLicNm)) return doValidate_SLicNm(oValue);
            if (sFieldName.Equals(_SLicNr)) return doValidate_SLicNr(oValue);
            if (sFieldName.Equals(_SLicSp)) return doValidate_SLicSp(oValue);
            if (sFieldName.Equals(_Source)) return doValidate_Source(oValue);
            if (sFieldName.Equals(_SPhone1)) return doValidate_SPhone1(oValue);
            if (sFieldName.Equals(_SpInst)) return doValidate_SpInst(oValue);
            if (sFieldName.Equals(_SState)) return doValidate_SState(oValue);
            if (sFieldName.Equals(_SStreet)) return doValidate_SStreet(oValue);
            if (sFieldName.Equals(_State)) return doValidate_State(oValue);
            if (sFieldName.Equals(_Status)) return doValidate_Status(oValue);
            if (sFieldName.Equals(_SteId)) return doValidate_SteId(oValue);
            if (sFieldName.Equals(_Street)) return doValidate_Street(oValue);
            if (sFieldName.Equals(_SupRef)) return doValidate_SupRef(oValue);
            if (sFieldName.Equals(_SZpCd)) return doValidate_SZpCd(oValue);
            if (sFieldName.Equals(_TCharge)) return doValidate_TCharge(oValue);
            if (sFieldName.Equals(_TRNCd)) return doValidate_TRNCd(oValue);
            if (sFieldName.Equals(_User)) return doValidate_User(oValue);
            if (sFieldName.Equals(_Version)) return doValidate_Version(oValue);
            if (sFieldName.Equals(_WasLic)) return doValidate_WasLic(oValue);
            if (sFieldName.Equals(_WOHID)) return doValidate_WOHID(oValue);
            if (sFieldName.Equals(_ZpCd)) return doValidate_ZpCd(oValue);
            if (sFieldName.Equals(_OrgDocNm)) return doValidate_OrgDocNm(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName) {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
         * Link the Name Field to the Form Item.
         */
        public void doLink_Name(string sControlName) {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
         * Link the U_Address Field to the Form Item.
         */
        public void doLink_Address(string sControlName) {
            moLinker.doLinkDataToControl(_Address, sControlName);
        }
        /**
         * Link the U_AddrssLN Field to the Form Item.
         */
        public void doLink_AddrssLN(string sControlName) {
            moLinker.doLinkDataToControl(_AddrssLN, sControlName);
        }
        /**
         * Link the U_AppRequired Field to the Form Item.
         */
        public void doLink_AppRequired(string sControlName) {
            moLinker.doLinkDataToControl(_AppRequired, sControlName);
        }
        /**
         * Link the U_BDate Field to the Form Item.
         */
        public void doLink_BDate(string sControlName) {
            moLinker.doLinkDataToControl(_BDate, sControlName);
        }
        /**
         * Link the U_Block Field to the Form Item.
         */
        public void doLink_Block(string sControlName) {
            moLinker.doLinkDataToControl(_Block, sControlName);
        }
        /**
         * Link the U_Branch Field to the Form Item.
         */
        public void doLink_Branch(string sControlName) {
            moLinker.doLinkDataToControl(_Branch, sControlName);
        }
        /**
         * Link the U_BTime Field to the Form Item.
         */
        public void doLink_BTime(string sControlName) {
            moLinker.doLinkDataToControl(_BTime, sControlName);
        }
        /**
         * Link the U_CAddress Field to the Form Item.
         */
        public void doLink_CAddress(string sControlName) {
            moLinker.doLinkDataToControl(_CAddress, sControlName);
        }
        /**
         * Link the U_CAddrsLN Field to the Form Item.
         */
        public void doLink_CAddrsLN(string sControlName) {
            moLinker.doLinkDataToControl(_CAddrsLN, sControlName);
        }
        /**
         * Link the U_CardCd Field to the Form Item.
         */
        public void doLink_CardCd(string sControlName) {
            moLinker.doLinkDataToControl(_CardCd, sControlName);
        }
        /**
         * Link the U_CardNM Field to the Form Item.
         */
        public void doLink_CardNM(string sControlName) {
            moLinker.doLinkDataToControl(_CardNM, sControlName);
        }
        /**
         * Link the U_CBlock Field to the Form Item.
         */
        public void doLink_CBlock(string sControlName) {
            moLinker.doLinkDataToControl(_CBlock, sControlName);
        }
        /**
         * Link the U_CCardCd Field to the Form Item.
         */
        public void doLink_CCardCd(string sControlName) {
            moLinker.doLinkDataToControl(_CCardCd, sControlName);
        }
        /**
         * Link the U_CCardNM Field to the Form Item.
         */
        public void doLink_CCardNM(string sControlName) {
            moLinker.doLinkDataToControl(_CCardNM, sControlName);
        }
        /**
         * Link the U_CCity Field to the Form Item.
         */
        public void doLink_CCity(string sControlName) {
            moLinker.doLinkDataToControl(_CCity, sControlName);
        }
        /**
         * Link the U_CContact Field to the Form Item.
         */
        public void doLink_CContact(string sControlName) {
            moLinker.doLinkDataToControl(_CContact, sControlName);
        }
        /**
         * Link the U_ChgPrice Field to the Form Item.
         */
        public void doLink_ChgPrice(string sControlName) {
            moLinker.doLinkDataToControl(_ChgPrice, sControlName);
        }
        /**
         * Link the U_ChgVAT Field to the Form Item.
         */
        public void doLink_ChgVAT(string sControlName) {
            moLinker.doLinkDataToControl(_ChgVAT, sControlName);
        }
        /**
         * Link the U_City Field to the Form Item.
         */
        public void doLink_City(string sControlName) {
            moLinker.doLinkDataToControl(_City, sControlName);
        }
        /**
         * Link the U_ClgCode Field to the Form Item.
         */
        public void doLink_ClgCode(string sControlName) {
            moLinker.doLinkDataToControl(_ClgCode, sControlName);
        }
        /**
         * Link the U_CntrNo Field to the Form Item.
         */
        public void doLink_CntrNo(string sControlName) {
            moLinker.doLinkDataToControl(_CntrNo, sControlName);
        }
        /**
         * Link the U_Contact Field to the Form Item.
         */
        public void doLink_Contact(string sControlName) {
            moLinker.doLinkDataToControl(_Contact, sControlName);
        }
        /**
         * Link the U_CopTRw Field to the Form Item.
         */
        public void doLink_CopTRw(string sControlName) {
            moLinker.doLinkDataToControl(_CopTRw, sControlName);
        }
        /**
         * Link the U_Cost Field to the Form Item.
         */
        public void doLink_Cost(string sControlName) {
            moLinker.doLinkDataToControl(_Cost, sControlName);
        }
        /**
         * Link the U_Country Field to the Form Item.
         */
        public void doLink_Country(string sControlName) {
            moLinker.doLinkDataToControl(_Country, sControlName);
        }
        /**
         * Link the U_County Field to the Form Item.
         */
        public void doLink_County(string sControlName) {
            moLinker.doLinkDataToControl(_County, sControlName);
        }
        /**
         * Link the U_CPGrossM Field to the Form Item.
         */
        public void doLink_CPGrossM(string sControlName) {
            moLinker.doLinkDataToControl(_CPGrossM, sControlName);
        }
        /**
         * Link the U_CPhone1 Field to the Form Item.
         */
        public void doLink_CPhone1(string sControlName) {
            moLinker.doLinkDataToControl(_CPhone1, sControlName);
        }
        /**
         * Link the U_CPProfit Field to the Form Item.
         */
        public void doLink_CPProfit(string sControlName) {
            moLinker.doLinkDataToControl(_CPProfit, sControlName);
        }
        /**
         * Link the U_CState Field to the Form Item.
         */
        public void doLink_CState(string sControlName) {
            moLinker.doLinkDataToControl(_CState, sControlName);
        }
        /**
         * Link the U_CStreet Field to the Form Item.
         */
        public void doLink_CStreet(string sControlName) {
            moLinker.doLinkDataToControl(_CStreet, sControlName);
        }
        /**
         * Link the U_CusFrnNm Field to the Form Item.
         */
        public void doLink_CusFrnNm(string sControlName) {
            moLinker.doLinkDataToControl(_CusFrnNm, sControlName);
        }
        /**
         * Link the U_CustRef Field to the Form Item.
         */
        public void doLink_CustRef(string sControlName) {
            moLinker.doLinkDataToControl(_CustRef, sControlName);
        }
        /**
         * Link the U_CWasLic Field to the Form Item.
         */
        public void doLink_CWasLic(string sControlName) {
            moLinker.doLinkDataToControl(_CWasLic, sControlName);
        }
        /**
         * Link the U_CZpCd Field to the Form Item.
         */
        public void doLink_CZpCd(string sControlName) {
            moLinker.doLinkDataToControl(_CZpCd, sControlName);
        }
        /**
         * Link the U_DAAttach Field to the Form Item.
         */
        public void doLink_DAAttach(string sControlName) {
            moLinker.doLinkDataToControl(_DAAttach, sControlName);
        }
        /**
         * Link the U_E_Mail Field to the Form Item.
         */
        public void doLink_E_Mail(string sControlName) {
            moLinker.doLinkDataToControl(_E_Mail, sControlName);
        }
        /**
         * Link the U_ENNO Field to the Form Item.
         */
        public void doLink_ENNO(string sControlName) {
            moLinker.doLinkDataToControl(_ENNO, sControlName);
        }
        /**
         * Link the U_ENPApFDt Field to the Form Item.
         */
        public void doLink_ENPApFDt(string sControlName) {
            moLinker.doLinkDataToControl(_ENPApFDt, sControlName);
        }
        /**
         * Link the U_ENPApTDt Field to the Form Item.
         */
        public void doLink_ENPApTDt(string sControlName) {
            moLinker.doLinkDataToControl(_ENPApTDt, sControlName);
        }
        /**
         * Link the U_FirstBP Field to the Form Item.
         */
        public void doLink_FirstBP(string sControlName) {
            moLinker.doLinkDataToControl(_FirstBP, sControlName);
        }
        /**
         * Link the U_ForeCS Field to the Form Item.
         */
        public void doLink_ForeCS(string sControlName) {
            moLinker.doLinkDataToControl(_ForeCS, sControlName);
        }
        /**
         * Link the U_ItemGrp Field to the Form Item.
         */
        public void doLink_ItemGrp(string sControlName) {
            moLinker.doLinkDataToControl(_ItemGrp, sControlName);
        }
        /**
         * Link the U_LckPrc Field to the Form Item.
         */
        public void doLink_LckPrc(string sControlName) {
            moLinker.doLinkDataToControl(_LckPrc, sControlName);
        }
        /**
         * Link the U_LPGrossM Field to the Form Item.
         */
        public void doLink_LPGrossM(string sControlName) {
            moLinker.doLinkDataToControl(_LPGrossM, sControlName);
        }
        /**
         * Link the U_LPProfit Field to the Form Item.
         */
        public void doLink_LPProfit(string sControlName) {
            moLinker.doLinkDataToControl(_LPProfit, sControlName);
        }
        /**
         * Link the U_LstPrice Field to the Form Item.
         */
        public void doLink_LstPrice(string sControlName) {
            moLinker.doLinkDataToControl(_LstPrice, sControlName);
        }
        /**
         * Link the U_MAddress Field to the Form Item.
         */
        public void doLink_MAddress(string sControlName) {
            moLinker.doLinkDataToControl(_MAddress, sControlName);
        }
        /**
         * Link the U_MAddrsLN Field to the Form Item.
         */
        public void doLink_MAddrsLN(string sControlName) {
            moLinker.doLinkDataToControl(_MAddrsLN, sControlName);
        }
        /**
         * Link the U_MBlock Field to the Form Item.
         */
        public void doLink_MBlock(string sControlName) {
            moLinker.doLinkDataToControl(_MBlock, sControlName);
        }
        /**
         * Link the U_MCity Field to the Form Item.
         */
        public void doLink_MCity(string sControlName) {
            moLinker.doLinkDataToControl(_MCity, sControlName);
        }
        /**
         * Link the U_MContact Field to the Form Item.
         */
        public void doLink_MContact(string sControlName) {
            moLinker.doLinkDataToControl(_MContact, sControlName);
        }
        /**
         * Link the U_MinJob Field to the Form Item.
         */
        public void doLink_MinJob(string sControlName) {
            moLinker.doLinkDataToControl(_MinJob, sControlName);
        }
        /**
         * Link the U_MPhone1 Field to the Form Item.
         */
        public void doLink_MPhone1(string sControlName) {
            moLinker.doLinkDataToControl(_MPhone1, sControlName);
        }
        /**
         * Link the U_MState Field to the Form Item.
         */
        public void doLink_MState(string sControlName) {
            moLinker.doLinkDataToControl(_MState, sControlName);
        }
        /**
         * Link the U_MStreet Field to the Form Item.
         */
        public void doLink_MStreet(string sControlName) {
            moLinker.doLinkDataToControl(_MStreet, sControlName);
        }
        /**
         * Link the U_MZpCd Field to the Form Item.
         */
        public void doLink_MZpCd(string sControlName) {
            moLinker.doLinkDataToControl(_MZpCd, sControlName);
        }
        /**
         * Link the U_NoChgTax Field to the Form Item.
         */
        public void doLink_NoChgTax(string sControlName) {
            moLinker.doLinkDataToControl(_NoChgTax, sControlName);
        }
        /**
         * Link the U_Notes Field to the Form Item.
         */
        public void doLink_Notes(string sControlName) {
            moLinker.doLinkDataToControl(_Notes, sControlName);
        }
        /**
         * Link the U_ORDTP Field to the Form Item.
         */
        public void doLink_ORDTP(string sControlName) {
            moLinker.doLinkDataToControl(_ORDTP, sControlName);
        }
        /**
         * Link the U_Origin Field to the Form Item.
         */
        public void doLink_Origin(string sControlName) {
            moLinker.doLinkDataToControl(_Origin, sControlName);
        }
        /**
         * Link the U_ORoad Field to the Form Item.
         */
        public void doLink_ORoad(string sControlName) {
            moLinker.doLinkDataToControl(_ORoad, sControlName);
        }
        /**
         * Link the U_PAddress Field to the Form Item.
         */
        public void doLink_PAddress(string sControlName) {
            moLinker.doLinkDataToControl(_PAddress, sControlName);
        }
        /**
         * Link the U_PAddrsLN Field to the Form Item.
         */
        public void doLink_PAddrsLN(string sControlName) {
            moLinker.doLinkDataToControl(_PAddrsLN, sControlName);
        }
        /**
         * Link the U_PBICODE Field to the Form Item.
         */
        public void doLink_PBICODE(string sControlName) {
            moLinker.doLinkDataToControl(_PBICODE, sControlName);
        }
        /**
         * Link the U_PBlock Field to the Form Item.
         */
        public void doLink_PBlock(string sControlName) {
            moLinker.doLinkDataToControl(_PBlock, sControlName);
        }
        /**
         * Link the U_PCardCd Field to the Form Item.
         */
        public void doLink_PCardCd(string sControlName) {
            moLinker.doLinkDataToControl(_PCardCd, sControlName);
        }
        /**
         * Link the U_PCardNM Field to the Form Item.
         */
        public void doLink_PCardNM(string sControlName) {
            moLinker.doLinkDataToControl(_PCardNM, sControlName);
        }
        /**
         * Link the U_PCity Field to the Form Item.
         */
        public void doLink_PCity(string sControlName) {
            moLinker.doLinkDataToControl(_PCity, sControlName);
        }
        /**
         * Link the U_PContact Field to the Form Item.
         */
        public void doLink_PContact(string sControlName) {
            moLinker.doLinkDataToControl(_PContact, sControlName);
        }
        /**
         * Link the U_Phone1 Field to the Form Item.
         */
        public void doLink_Phone1(string sControlName) {
            moLinker.doLinkDataToControl(_Phone1, sControlName);
        }
        /**
         * Link the U_PPhone1 Field to the Form Item.
         */
        public void doLink_PPhone1(string sControlName) {
            moLinker.doLinkDataToControl(_PPhone1, sControlName);
        }
        /**
         * Link the U_PremCd Field to the Form Item.
         */
        public void doLink_PremCd(string sControlName) {
            moLinker.doLinkDataToControl(_PremCd, sControlName);
        }
        /**
         * Link the U_ProRef Field to the Form Item.
         */
        public void doLink_ProRef(string sControlName) {
            moLinker.doLinkDataToControl(_ProRef, sControlName);
        }
        /**
         * Link the U_PState Field to the Form Item.
         */
        public void doLink_PState(string sControlName) {
            moLinker.doLinkDataToControl(_PState, sControlName);
        }
        /**
         * Link the U_PStreet Field to the Form Item.
         */
        public void doLink_PStreet(string sControlName) {
            moLinker.doLinkDataToControl(_PStreet, sControlName);
        }
        /**
         * Link the U_PZpCd Field to the Form Item.
         */
        public void doLink_PZpCd(string sControlName) {
            moLinker.doLinkDataToControl(_PZpCd, sControlName);
        }
        /**
         * Link the U_Qty Field to the Form Item.
         */
        public void doLink_Qty(string sControlName) {
            moLinker.doLinkDataToControl(_Qty, sControlName);
        }
        /**
         * Link the U_REDate Field to the Form Item.
         */
        public void doLink_REDate(string sControlName) {
            moLinker.doLinkDataToControl(_REDate, sControlName);
        }
        /**
         * Link the U_RETime Field to the Form Item.
         */
        public void doLink_RETime(string sControlName) {
            moLinker.doLinkDataToControl(_RETime, sControlName);
        }
        /**
         * Link the U_ReviewNotes Field to the Form Item.
         */
        public void doLink_ReviewNotes(string sControlName) {
            moLinker.doLinkDataToControl(_ReviewNotes, sControlName);
        }
        /**
         * Link the U_Route Field to the Form Item.
         */
        public void doLink_Route(string sControlName) {
            moLinker.doLinkDataToControl(_Route, sControlName);
        }
        /**
         * Link the U_RSDate Field to the Form Item.
         */
        public void doLink_RSDate(string sControlName) {
            moLinker.doLinkDataToControl(_RSDate, sControlName);
        }
        /**
         * Link the U_RSTime Field to the Form Item.
         */
        public void doLink_RSTime(string sControlName) {
            moLinker.doLinkDataToControl(_RSTime, sControlName);
        }
        /**
         * Link the U_RTIMEF Field to the Form Item.
         */
        public void doLink_RTIMEF(string sControlName) {
            moLinker.doLinkDataToControl(_RTIMEF, sControlName);
        }
        /**
         * Link the U_RTIMET Field to the Form Item.
         */
        public void doLink_RTIMET(string sControlName) {
            moLinker.doLinkDataToControl(_RTIMET, sControlName);
        }
        /**
         * Link the U_SAddress Field to the Form Item.
         */
        public void doLink_SAddress(string sControlName) {
            moLinker.doLinkDataToControl(_SAddress, sControlName);
        }
        /**
         * Link the U_SAddrsLN Field to the Form Item.
         */
        public void doLink_SAddrsLN(string sControlName) {
            moLinker.doLinkDataToControl(_SAddrsLN, sControlName);
        }
        /**
         * Link the U_SBlock Field to the Form Item.
         */
        public void doLink_SBlock(string sControlName) {
            moLinker.doLinkDataToControl(_SBlock, sControlName);
        }
        /**
         * Link the U_SCardCd Field to the Form Item.
         */
        public void doLink_SCardCd(string sControlName) {
            moLinker.doLinkDataToControl(_SCardCd, sControlName);
        }
        /**
         * Link the U_SCardNM Field to the Form Item.
         */
        public void doLink_SCardNM(string sControlName) {
            moLinker.doLinkDataToControl(_SCardNM, sControlName);
        }
        /**
         * Link the U_SCity Field to the Form Item.
         */
        public void doLink_SCity(string sControlName) {
            moLinker.doLinkDataToControl(_SCity, sControlName);
        }
        /**
         * Link the U_SContact Field to the Form Item.
         */
        public void doLink_SContact(string sControlName) {
            moLinker.doLinkDataToControl(_SContact, sControlName);
        }
        /**
         * Link the U_SepOrder Field to the Form Item.
         */
        public void doLink_SepOrder(string sControlName) {
            moLinker.doLinkDataToControl(_SepOrder, sControlName);
        }
        /**
         * Link the U_Seq Field to the Form Item.
         */
        public void doLink_Seq(string sControlName) {
            moLinker.doLinkDataToControl(_Seq, sControlName);
        }
        /**
         * Link the U_SiteLic Field to the Form Item.
         */
        public void doLink_SiteLic(string sControlName) {
            moLinker.doLinkDataToControl(_SiteLic, sControlName);
        }
        /**
         * Link the U_SiteRef Field to the Form Item.
         */
        public void doLink_SiteRef(string sControlName) {
            moLinker.doLinkDataToControl(_SiteRef, sControlName);
        }
        /**
         * Link the U_SiteTl Field to the Form Item.
         */
        public void doLink_SiteTl(string sControlName) {
            moLinker.doLinkDataToControl(_SiteTl, sControlName);
        }
        /**
         * Link the U_SLicCh Field to the Form Item.
         */
        public void doLink_SLicCh(string sControlName) {
            moLinker.doLinkDataToControl(_SLicCh, sControlName);
        }
        /**
         * Link the U_SLicExp Field to the Form Item.
         */
        public void doLink_SLicExp(string sControlName) {
            moLinker.doLinkDataToControl(_SLicExp, sControlName);
        }
        /**
         * Link the U_SLicNm Field to the Form Item.
         */
        public void doLink_SLicNm(string sControlName) {
            moLinker.doLinkDataToControl(_SLicNm, sControlName);
        }
        /**
         * Link the U_SLicNr Field to the Form Item.
         */
        public void doLink_SLicNr(string sControlName) {
            moLinker.doLinkDataToControl(_SLicNr, sControlName);
        }
        /**
         * Link the U_SLicSp Field to the Form Item.
         */
        public void doLink_SLicSp(string sControlName) {
            moLinker.doLinkDataToControl(_SLicSp, sControlName);
        }
        /**
         * Link the U_Source Field to the Form Item.
         */
        public void doLink_Source(string sControlName) {
            moLinker.doLinkDataToControl(_Source, sControlName);
        }
        /**
         * Link the U_SPhone1 Field to the Form Item.
         */
        public void doLink_SPhone1(string sControlName) {
            moLinker.doLinkDataToControl(_SPhone1, sControlName);
        }
        /**
         * Link the U_SpInst Field to the Form Item.
         */
        public void doLink_SpInst(string sControlName) {
            moLinker.doLinkDataToControl(_SpInst, sControlName);
        }
        /**
         * Link the U_SState Field to the Form Item.
         */
        public void doLink_SState(string sControlName) {
            moLinker.doLinkDataToControl(_SState, sControlName);
        }
        /**
         * Link the U_SStreet Field to the Form Item.
         */
        public void doLink_SStreet(string sControlName) {
            moLinker.doLinkDataToControl(_SStreet, sControlName);
        }
        /**
         * Link the U_State Field to the Form Item.
         */
        public void doLink_State(string sControlName) {
            moLinker.doLinkDataToControl(_State, sControlName);
        }
        /**
         * Link the U_Status Field to the Form Item.
         */
        public void doLink_Status(string sControlName) {
            moLinker.doLinkDataToControl(_Status, sControlName);
        }
        /**
         * Link the U_SteId Field to the Form Item.
         */
        public void doLink_SteId(string sControlName) {
            moLinker.doLinkDataToControl(_SteId, sControlName);
        }
        /**
         * Link the U_Street Field to the Form Item.
         */
        public void doLink_Street(string sControlName) {
            moLinker.doLinkDataToControl(_Street, sControlName);
        }
        /**
         * Link the U_SupRef Field to the Form Item.
         */
        public void doLink_SupRef(string sControlName) {
            moLinker.doLinkDataToControl(_SupRef, sControlName);
        }
        /**
         * Link the U_SZpCd Field to the Form Item.
         */
        public void doLink_SZpCd(string sControlName) {
            moLinker.doLinkDataToControl(_SZpCd, sControlName);
        }
        /**
         * Link the U_TCharge Field to the Form Item.
         */
        public void doLink_TCharge(string sControlName) {
            moLinker.doLinkDataToControl(_TCharge, sControlName);
        }
        /**
         * Link the U_TRNCd Field to the Form Item.
         */
        public void doLink_TRNCd(string sControlName) {
            moLinker.doLinkDataToControl(_TRNCd, sControlName);
        }
        /**
         * Link the U_User Field to the Form Item.
         */
        public void doLink_User(string sControlName) {
            moLinker.doLinkDataToControl(_User, sControlName);
        }
        /**
         * Link the U_Version Field to the Form Item.
         */
        public void doLink_Version(string sControlName) {
            moLinker.doLinkDataToControl(_Version, sControlName);
        }
        /**
         * Link the U_WasLic Field to the Form Item.
         */
        public void doLink_WasLic(string sControlName) {
            moLinker.doLinkDataToControl(_WasLic, sControlName);
        }
        /**
         * Link the U_WOHID Field to the Form Item.
         */
        public void doLink_WOHID(string sControlName) {
            moLinker.doLinkDataToControl(_WOHID, sControlName);
        }
        /**
         * Link the U_ZpCd Field to the Form Item.
         */
        public void doLink_ZpCd(string sControlName) {
            moLinker.doLinkDataToControl(_ZpCd, sControlName);
        }
        /**
         * Link the U_OrgDocNm Field to the Form Item.
         */
        public void doLink_OrgDocNm(string sControlName) {
            moLinker.doLinkDataToControl(_OrgDocNm, sControlName);
        }
        #endregion

    }
}
