/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 13/04/2016 17:24:24
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;
using System.Windows.Forms;

namespace com.isb.enq.dbObjects.Base {
    [Serializable]
    public class IDH_ENQUIRY : com.idh.dbObjects.DBBase {

        //private Linker moLinker = null;
        //public Linker ControlLinker {
        //    get { return moLinker; }
        //    set { moLinker = value; }
        //}

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private Control moParentControl;
        public Control ParentControl {
            get { return moParentControl; }
            set { moParentControl = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_ENQUIRY()
            : base("@IDH_ENQUIRY") {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_ENQUIRY(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oForm, "@IDH_ENQUIRY") {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
		* Table name
		*/
        public readonly static string TableName = "@IDH_ENQUIRY";

        /**
         * Decription: Code
         * Mandatory: tYes
         * Name: Code
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Code = "Code";
        public string Code {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code() {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue) {
            return base.doValidation(_Code, oValue);
        }
        /**
         * Decription: Name
         * Mandatory: tYes
         * Name: Name
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Name = "Name";
        public string Name {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name() {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue) {
            return base.doValidation(_Name, oValue);
        }
        /**
         * Decription: Address ID
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Address
         * Size: 50
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Address = "U_Address";
        public string U_Address {
            get {
                return getValueAsString(_Address);
            }
            set { setValue(_Address, value); }
        }
        public string doValidate_Address() {
            return doValidate_Address(U_Address);
        }
        public virtual string doValidate_Address(object oValue) {
            return base.doValidation(_Address, oValue);
        }

        /**
         * Decription: Address LineNum
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_AddrssLN
         * Size: 10
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _AddrssLN = "U_AddrssLN";
        public string U_AddrssLN {
            get {
                return getValueAsString(_AddrssLN);
            }
            set { setValue(_AddrssLN, value); }
        }
        public string doValidate_AddrssLN() {
            return doValidate_AddrssLN(U_AddrssLN);
        }
        public virtual string doValidate_AddrssLN(object oValue) {
            return base.doValidation(_AddrssLN, oValue);
        }

        /**
         * Decription: Assigned To
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_AttendUser
         * Size: 155
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _AttendUser = "U_AttendUser";
        public string U_AttendUser {
            get {
                return getValueAsString(_AttendUser);
            }
            set { setValue(_AttendUser, value); }
        }
        public string doValidate_AttendUser() {
            return doValidate_AttendUser(U_AttendUser);
        }
        public virtual string doValidate_AttendUser(object oValue) {
            return base.doValidation(_AttendUser, oValue);
        }

        /**
         * Decription: Block
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Block
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Block = "U_Block";
        public string U_Block {
            get {
                return getValueAsString(_Block);
            }
            set { setValue(_Block, value); }
        }
        public string doValidate_Block() {
            return doValidate_Block(U_Block);
        }
        public virtual string doValidate_Block(object oValue) {
            return base.doValidation(_Block, oValue);
        }

        /**
         * Decription: BP Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CardCode
         * Size: 15
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CardCode = "U_CardCode";
        public string U_CardCode {
            get {
                return getValueAsString(_CardCode);
            }
            set { setValue(_CardCode, value); }
        }
        public string doValidate_CardCode() {
            return doValidate_CardCode(U_CardCode);
        }
        public virtual string doValidate_CardCode(object oValue) {
            return base.doValidation(_CardCode, oValue);
        }

        /**
         * Decription: BP Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CardName
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CardName = "U_CardName";
        public string U_CardName {
            get {
                return getValueAsString(_CardName);
            }
            set { setValue(_CardName, value); }
        }
        public string doValidate_CardName() {
            return doValidate_CardName(U_CardName);
        }
        public virtual string doValidate_CardName(object oValue) {
            return base.doValidation(_CardName, oValue);
        }

        /**
         * Decription: City
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_City
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _City = "U_City";
        public string U_City {
            get {
                return getValueAsString(_City);
            }
            set { setValue(_City, value); }
        }
        public string doValidate_City() {
            return doValidate_City(U_City);
        }
        public virtual string doValidate_City(object oValue) {
            return base.doValidation(_City, oValue);
        }

        /**
         * Decription: Number
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ClgCode
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _ClgCode = "U_ClgCode";
        public int U_ClgCode {
            get {
                return getValueAsInt(_ClgCode);
            }
            set { setValue(_ClgCode, value); }
        }
        public string doValidate_ClgCode() {
            return doValidate_ClgCode(U_ClgCode);
        }
        public virtual string doValidate_ClgCode(object oValue) {
            return base.doValidation(_ClgCode, oValue);
        }

        /**
         * Decription: Contact Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CName
         * Size: 50
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CName = "U_CName";
        public string U_CName {
            get {
                return getValueAsString(_CName);
            }
            set { setValue(_CName, value); }
        }
        public string doValidate_CName() {
            return doValidate_CName(U_CName);
        }
        public virtual string doValidate_CName(object oValue) {
            return base.doValidation(_CName, oValue);
        }

        /**
         * Decription: Country
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Country
         * Size: 3
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Country = "U_Country";
        public string U_Country {
            get {
                return getValueAsString(_Country);
            }
            set { setValue(_Country, value); }
        }
        public string doValidate_Country() {
            return doValidate_Country(U_Country);
        }
        public virtual string doValidate_Country(object oValue) {
            return base.doValidation(_Country, oValue);
        }

        /**
         * Decription: County
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_County
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _County = "U_County";
        public string U_County {
            get {
                return getValueAsString(_County);
            }
            set { setValue(_County, value); }
        }
        public string doValidate_County() {
            return doValidate_County(U_County);
        }
        public virtual string doValidate_County(object oValue) {
            return base.doValidation(_County, oValue);
        }

        /**
         * Decription: Description
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Desc
         * Size: 250
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Desc = "U_Desc";
        public string U_Desc {
            get {
                return getValueAsString(_Desc);
            }
            set { setValue(_Desc, value); }
        }
        public string doValidate_Desc() {
            return doValidate_Desc(U_Desc);
        }
        public virtual string doValidate_Desc(object oValue) {
            return base.doValidation(_Desc, oValue);
        }

        /**
         * Decription: Email Address
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_E_Mail
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _E_Mail = "U_E_Mail";
        public string U_E_Mail {
            get {
                return getValueAsString(_E_Mail);
            }
            set { setValue(_E_Mail, value); }
        }
        public string doValidate_E_Mail() {
            return doValidate_E_Mail(U_E_Mail);
        }
        public virtual string doValidate_E_Mail(object oValue) {
            return base.doValidation(_E_Mail, oValue);
        }

        /**
         * Decription: Remarks
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Notes
         * Size: 10
         * Type: db_Memo
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Notes = "U_Notes";
        public string U_Notes {
            get {
                return getValueAsString(_Notes);
            }
            set { setValue(_Notes, value); }
        }
        public string doValidate_Notes() {
            return doValidate_Notes(U_Notes);
        }
        public virtual string doValidate_Notes(object oValue) {
            return base.doValidation(_Notes, oValue);
        }

        /**
         * Decription: Telephone
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Phone1
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_Phone
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Phone1 = "U_Phone1";
        public string U_Phone1 {
            get {
                return getValueAsString(_Phone1);
            }
            set { setValue(_Phone1, value); }
        }
        public string doValidate_Phone1() {
            return doValidate_Phone1(U_Phone1);
        }
        public virtual string doValidate_Phone1(object oValue) {
            return base.doValidation(_Phone1, oValue);
        }

        /**
         * Decription: Date
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Recontact
         * Size: 8
         * Type: db_Date
         * CType: DateTime
         * SubType: st_None
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _Recontact = "U_Recontact";
        public DateTime U_Recontact {
            get {
                return getValueAsDateTime(_Recontact);
            }
            set { setValue(_Recontact, value); }
        }
        public void U_Recontact_AsString(string value) {
            setValue("U_Recontact", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_Recontact_AsString() {
            DateTime dVal = getValueAsDateTime(_Recontact);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_Recontact() {
            return doValidate_Recontact(U_Recontact);
        }
        public virtual string doValidate_Recontact(object oValue) {
            return base.doValidation(_Recontact, oValue);
        }

        /**
         * Decription: Source
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Source
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Source = "U_Source";
        public int U_Source {
            get {
                return getValueAsInt(_Source);
            }
            set { setValue(_Source, value); }
        }
        public string doValidate_Source() {
            return doValidate_Source(U_Source);
        }
        public virtual string doValidate_Source(object oValue) {
            return base.doValidation(_Source, oValue);
        }

        /**
         * Decription: State
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_State
         * Size: 3
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _State = "U_State";
        public string U_State {
            get {
                return getValueAsString(_State);
            }
            set { setValue(_State, value); }
        }
        public string doValidate_State() {
            return doValidate_State(U_State);
        }
        public virtual string doValidate_State(object oValue) {
            return base.doValidation(_State, oValue);
        }

        /**
         * Decription: Status
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Status
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Status = "U_Status";
        public int U_Status {
            get {
                return getValueAsInt(_Status);
            }
            set { setValue(_Status, value); }
        }
        public string doValidate_Status() {
            return doValidate_Status(U_Status);
        }
        public virtual string doValidate_Status(object oValue) {
            return base.doValidation(_Status, oValue);
        }

        /**
         * Decription: Street
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Street
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Street = "U_Street";
        public string U_Street {
            get {
                return getValueAsString(_Street);
            }
            set { setValue(_Street, value); }
        }
        public string doValidate_Street() {
            return doValidate_Street(U_Street);
        }
        public virtual string doValidate_Street(object oValue) {
            return base.doValidation(_Street, oValue);
        }

        /**
         * Decription: Created By
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_User
         * Size: 155
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _User = "U_User";
        public string U_User {
            get {
                return getValueAsString(_User);
            }
            set { setValue(_User, value); }
        }
        public string doValidate_User() {
            return doValidate_User(U_User);
        }
        public virtual string doValidate_User(object oValue) {
            return base.doValidation(_User, oValue);
        }

        /**
         * Decription: Post Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ZipCode
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ZipCode = "U_ZipCode";
        public string U_ZipCode {
            get {
                return getValueAsString(_ZipCode);
            }
            set { setValue(_ZipCode, value); }
        }
        public string doValidate_ZipCode() {
            return doValidate_ZipCode(U_ZipCode);
        }
        public virtual string doValidate_ZipCode(object oValue) {
            return base.doValidation(_ZipCode, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo() {
            if (moDBFields == null)
                moDBFields = new DBFields(24);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_Address, "Address ID", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Address ID
            moDBFields.Add(_AddrssLN, "Address LineNum", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Address LineNum
            moDBFields.Add(_AttendUser, "Assigned To", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 155, EMPTYSTR, false, false); //Assigned To
            moDBFields.Add(_Block, "Block", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Block
            moDBFields.Add(_CardCode, "BP Code", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //BP Code
            moDBFields.Add(_CardName, "BP Name", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //BP Name
            moDBFields.Add(_City, "City", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //City
            moDBFields.Add(_ClgCode, "Number", 9, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Number
            moDBFields.Add(_CName, "Contact Name", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Contact Name
            moDBFields.Add(_Country, "Country", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //Country
            moDBFields.Add(_County, "County", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //County
            moDBFields.Add(_Desc, "Description", 13, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Description
            moDBFields.Add(_E_Mail, "Email Address", 14, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Email Address
            moDBFields.Add(_Notes, "Remarks", 15, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Remarks
            moDBFields.Add(_Phone1, "Telephone", 16, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_Phone, 20, EMPTYSTR, false, false); //Telephone
            moDBFields.Add(_Recontact, "Date", 17, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Date
            moDBFields.Add(_Source, "Source", 18, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Source
            moDBFields.Add(_State, "State", 19, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //State
            moDBFields.Add(_Status, "Status", 20, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Status
            moDBFields.Add(_Street, "Street", 21, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Street
            moDBFields.Add(_User, "Created By", 22, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 155, EMPTYSTR, false, false); //Created By
            moDBFields.Add(_ZipCode, "Post Code", 23, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Post Code

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation() {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_Address());
            doBuildValidationString(doValidate_AddrssLN());
            doBuildValidationString(doValidate_AttendUser());
            doBuildValidationString(doValidate_Block());
            doBuildValidationString(doValidate_CardCode());
            doBuildValidationString(doValidate_CardName());
            doBuildValidationString(doValidate_City());
            doBuildValidationString(doValidate_ClgCode());
            doBuildValidationString(doValidate_CName());
            doBuildValidationString(doValidate_Country());
            doBuildValidationString(doValidate_County());
            doBuildValidationString(doValidate_Desc());
            doBuildValidationString(doValidate_E_Mail());
            doBuildValidationString(doValidate_Notes());
            doBuildValidationString(doValidate_Phone1());
            doBuildValidationString(doValidate_Recontact());
            doBuildValidationString(doValidate_Source());
            doBuildValidationString(doValidate_State());
            doBuildValidationString(doValidate_Status());
            doBuildValidationString(doValidate_Street());
            doBuildValidationString(doValidate_User());
            doBuildValidationString(doValidate_ZipCode());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_Address)) return doValidate_Address(oValue);
            if (sFieldName.Equals(_AddrssLN)) return doValidate_AddrssLN(oValue);
            if (sFieldName.Equals(_AttendUser)) return doValidate_AttendUser(oValue);
            if (sFieldName.Equals(_Block)) return doValidate_Block(oValue);
            if (sFieldName.Equals(_CardCode)) return doValidate_CardCode(oValue);
            if (sFieldName.Equals(_CardName)) return doValidate_CardName(oValue);
            if (sFieldName.Equals(_City)) return doValidate_City(oValue);
            if (sFieldName.Equals(_ClgCode)) return doValidate_ClgCode(oValue);
            if (sFieldName.Equals(_CName)) return doValidate_CName(oValue);
            if (sFieldName.Equals(_Country)) return doValidate_Country(oValue);
            if (sFieldName.Equals(_County)) return doValidate_County(oValue);
            if (sFieldName.Equals(_Desc)) return doValidate_Desc(oValue);
            if (sFieldName.Equals(_E_Mail)) return doValidate_E_Mail(oValue);
            if (sFieldName.Equals(_Notes)) return doValidate_Notes(oValue);
            if (sFieldName.Equals(_Phone1)) return doValidate_Phone1(oValue);
            if (sFieldName.Equals(_Recontact)) return doValidate_Recontact(oValue);
            if (sFieldName.Equals(_Source)) return doValidate_Source(oValue);
            if (sFieldName.Equals(_State)) return doValidate_State(oValue);
            if (sFieldName.Equals(_Status)) return doValidate_Status(oValue);
            if (sFieldName.Equals(_Street)) return doValidate_Street(oValue);
            if (sFieldName.Equals(_User)) return doValidate_User(oValue);
            if (sFieldName.Equals(_ZipCode)) return doValidate_ZipCode(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName) {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
         * Link the Name Field to the Form Item.
         */
        public void doLink_Name(string sControlName) {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
         * Link the U_Address Field to the Form Item.
         */
        public void doLink_Address(string sControlName) {
            moLinker.doLinkDataToControl(_Address, sControlName);
        }
        /**
         * Link the U_AddrssLN Field to the Form Item.
         */
        public void doLink_AddrssLN(string sControlName) {
            moLinker.doLinkDataToControl(_AddrssLN, sControlName);
        }
        /**
         * Link the U_AttendUser Field to the Form Item.
         */
        public void doLink_AttendUser(string sControlName) {
            moLinker.doLinkDataToControl(_AttendUser, sControlName);
        }
        /**
         * Link the U_Block Field to the Form Item.
         */
        public void doLink_Block(string sControlName) {
            moLinker.doLinkDataToControl(_Block, sControlName);
        }
        /**
         * Link the U_CardCode Field to the Form Item.
         */
        public void doLink_CardCode(string sControlName) {
            moLinker.doLinkDataToControl(_CardCode, sControlName);
        }
        /**
         * Link the U_CardName Field to the Form Item.
         */
        public void doLink_CardName(string sControlName) {
            moLinker.doLinkDataToControl(_CardName, sControlName);
        }
        /**
         * Link the U_City Field to the Form Item.
         */
        public void doLink_City(string sControlName) {
            moLinker.doLinkDataToControl(_City, sControlName);
        }
        /**
         * Link the U_ClgCode Field to the Form Item.
         */
        public void doLink_ClgCode(string sControlName) {
            moLinker.doLinkDataToControl(_ClgCode, sControlName);
        }
        /**
         * Link the U_CName Field to the Form Item.
         */
        public void doLink_CName(string sControlName) {
            moLinker.doLinkDataToControl(_CName, sControlName);
        }
        /**
         * Link the U_Country Field to the Form Item.
         */
        public void doLink_Country(string sControlName) {
            moLinker.doLinkDataToControl(_Country, sControlName);
        }
        /**
         * Link the U_County Field to the Form Item.
         */
        public void doLink_County(string sControlName) {
            moLinker.doLinkDataToControl(_County, sControlName);
        }
        /**
         * Link the U_Desc Field to the Form Item.
         */
        public void doLink_Desc(string sControlName) {
            moLinker.doLinkDataToControl(_Desc, sControlName);
        }
        /**
         * Link the U_E_Mail Field to the Form Item.
         */
        public void doLink_E_Mail(string sControlName) {
            moLinker.doLinkDataToControl(_E_Mail, sControlName);
        }
        /**
         * Link the U_Notes Field to the Form Item.
         */
        public void doLink_Notes(string sControlName) {
            moLinker.doLinkDataToControl(_Notes, sControlName);
        }
        /**
         * Link the U_Phone1 Field to the Form Item.
         */
        public void doLink_Phone1(string sControlName) {
            moLinker.doLinkDataToControl(_Phone1, sControlName);
        }
        /**
         * Link the U_Recontact Field to the Form Item.
         */
        public void doLink_Recontact(string sControlName) {
            moLinker.doLinkDataToControl(_Recontact, sControlName);
        }
        /**
         * Link the U_Source Field to the Form Item.
         */
        public void doLink_Source(string sControlName) {
            moLinker.doLinkDataToControl(_Source, sControlName);
        }
        /**
         * Link the U_State Field to the Form Item.
         */
        public void doLink_State(string sControlName) {
            moLinker.doLinkDataToControl(_State, sControlName);
        }
        /**
         * Link the U_Status Field to the Form Item.
         */
        public void doLink_Status(string sControlName) {
            moLinker.doLinkDataToControl(_Status, sControlName);
        }
        /**
         * Link the U_Street Field to the Form Item.
         */
        public void doLink_Street(string sControlName) {
            moLinker.doLinkDataToControl(_Street, sControlName);
        }
        /**
         * Link the U_User Field to the Form Item.
         */
        public void doLink_User(string sControlName) {
            moLinker.doLinkDataToControl(_User, sControlName);
        }
        /**
         * Link the U_ZipCode Field to the Form Item.
         */
        public void doLink_ZipCode(string sControlName) {
            moLinker.doLinkDataToControl(_ZipCode, sControlName);
        }
        #endregion

    }
}
