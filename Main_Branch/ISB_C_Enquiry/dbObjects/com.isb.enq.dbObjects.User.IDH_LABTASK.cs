/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 09/02/2016 12:22:13
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.bridge.lookups;
using com.idh.bridge;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_LABTASK : com.isb.enq.dbObjects.Base.IDH_LABTASK {

        public IDH_LABTASK()
            : base() {
            msAutoNumKey = "SEQLBT";
        }

        public IDH_LABTASK(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oIDHForm, oForm) {
            msAutoNumKey = "SEQLBT";
        }
        public static ArrayList MessagesToSent = new ArrayList();
        private int miDoHandleChange = 0;
        public bool DoBlockUpdateTrigger {
            get { return miDoHandleChange > 0; }
            set { miDoHandleChange += (value ? 1 : -1); }
        }
        public static bool dosendAllAlerttoUser() {
            if (DataHandler.INSTANCE.SBOCompany == null) {
                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("You have to be connected to SBO for this to work.", "ERSYCSBO", null);
                return false;
            }
            if (MessagesToSent.Count == 0)
                return false;
            for (int i = 0; i <= MessagesToSent.Count - 1; i++) {
                SAPbobsCOM.Messages msg = (SAPbobsCOM.Messages)MessagesToSent[i];
                int iRet = msg.Add();
                if (iRet != 0) {
                    string sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error sending the lab task alert - " + sResult + ".", "LAB00002",
                        new string[] { sResult });
                    return false;
                }
            }
            MessagesToSent.Clear();
            return true;
        }
        public static bool dosendAlerttoUser(string sUsers, string sMessage, string sSubject, bool bAddtoListForLater, string sLabTaskID) {
            if (DataHandler.INSTANCE.SBOCompany == null) {
                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("You have to be connected to SBO for this to work.", "ERSYCSBO", null);
                return false;
            }

            string[] aReceipients = sUsers.Split(',');
            SAPbobsCOM.Messages msg;
            msg = (SAPbobsCOM.Messages)DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oMessages);
            if (sLabTaskID != string.Empty) {
                msg.MessageText = "Lab Task: " + sLabTaskID + "::" + sMessage;
                msg.Subject = "Lab Task: " + sLabTaskID + "::" + sSubject;
            } else {
                msg.MessageText = sMessage;
                msg.Subject = sSubject;
            }
            bool bValidUsers = false;
            for (int i = 0; i <= aReceipients.Length - 1; i++) {
                if (aReceipients[i].Trim() == "") {
                    continue;
                }
                bValidUsers = true;
                msg.Recipients.Add();
                msg.Recipients.SetCurrentLine(i);
                msg.Recipients.UserCode = aReceipients[i];
                msg.Recipients.NameTo = aReceipients[i];
                msg.Recipients.SendInternal = SAPbobsCOM.BoYesNoEnum.tYES;
                msg.Recipients.UserType = SAPbobsCOM.BoMsgRcpTypes.rt_InternalUser;
                object sEMail = Config.INSTANCE.getValueFromTablebyKey("OUSR", "E_Mail", "USER_CODE", aReceipients[i]);
                if (sEMail != null && sEMail.ToString() != string.Empty) {
                    msg.Recipients.EmailAddress = sEMail.ToString();//Config.INSTANCE.getValueFromTablebyKey("OUSR", "E_Mail", "USER_CODE", sUserList[i]);
                    msg.Recipients.SendEmail = SAPbobsCOM.BoYesNoEnum.tYES;
                }
                //    msg.Priority=SAPbobsCOM.BoMsgPriorities.pr_Low;
                //msg.Recipients.UserType=SAPbobsCOM.BoMsgRcpTypes.
            }
            if (bValidUsers == false)
                return true;
            //Here I added false in condition so Louis can test and see
            if ((bAddtoListForLater || DataHandler.INSTANCE.IsInTransaction())) {
                IDH_LABTASK.MessagesToSent.Add(msg);
                return true;
            } else {
                int iRet = msg.Add();
                if (iRet != 0) {
                    string sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error sending the lab task alert - " + sResult + ".", "LAB00002",
                        new string[] { sResult });
                    return false;
                } else
                    return true;
            }
        }

        public override void doSetFieldHasChanged(int iRow, string sFieldname, object oOldValue, object oNewValue) {
            base.doSetFieldHasChanged(iRow, sFieldname, oOldValue, oNewValue);
            if (miDoHandleChange > 0)
                return;
            DoBlockUpdateTrigger = true;
            try {
                if (sFieldname == IDH_LABTASK._Decision && oOldValue != oNewValue && oNewValue != null && oNewValue.ToString() != "Pending") {//part of work flow if Decision updated
                    U_AssignedTo = "";
                    U_Department = "";
                } else if (sFieldname == IDH_LABTASK._DtAnalysed && oOldValue != oNewValue && oNewValue != null && oNewValue.ToString() != string.Empty) {//part of work flow if Analysis Date updated
                    //Analysis date provided
                    //update user and its dept
                    string sUser = "";
                    if (U_ObjectType.ToString().Contains("WOR"))
                        sUser = Config.ParamaterWithDefault("LABUSR11", "");
                    else
                        sUser = Config.ParamaterWithDefault("LABUSR04", "");
                    if (sUser != string.Empty) {
                        string sDept = Config.INSTANCE.getValueFromTablebyKey("OUSR", "Department", "USER_CODE", sUser).ToString();
                        U_AssignedTo = sUser;
                        U_Department = sDept;
                    }
                } else if (sFieldname == IDH_LABTASK._AnalysisReq && oOldValue != oNewValue && oNewValue != null
                    && oNewValue.ToString() != string.Empty && !U_ObjectType.Contains("WOR")) {//part of work flow if Analysis Required updated
                    //receieve date provided
                    //update user and its dept
                    string sUser = Config.ParamaterWithDefault("LABUSR03", "");
                    if (sUser != string.Empty) {
                        string sDept = Config.INSTANCE.getValueFromTablebyKey("OUSR", "Department", "USER_CODE", sUser).ToString();
                        U_AssignedTo = sUser;
                        U_Department = sDept;
                    }
                } else if (sFieldname == IDH_LABTASK._DtReceived && oOldValue != oNewValue && oNewValue != null
                    && oNewValue.ToString() != string.Empty) {//part of work flow if Received date is entered
                        //receieve date provided
                        //update user and its dept
                        string sUser = "";
                        if (U_ObjectType.Contains("WOR"))
                            sUser = Config.ParamaterWithDefault("LABUSR10", "");
                        else
                            sUser = Config.ParamaterWithDefault("LABUSR02", "");
                        if (sUser != string.Empty) {
                            string sDept = Config.INSTANCE.getValueFromTablebyKey("OUSR", "Department", "USER_CODE", sUser).ToString();
                            U_AssignedTo= sUser;
                            U_Department= sDept;
                        }
                }    
            } catch (Exception ex) {
                throw new Exception("Field: [" + sFieldname + "] OValue: [" + oOldValue + "] NValue: [" + oNewValue + "] " + ex.Message);
                //throw new Exception("Field: [" + sFieldName + "] OValue: [" + oOldValue + "] NValue: [" + oNewValue + "] ExtraFlag: [" + sExtraFlag + "] " + e.Message);
            } finally {
                miDoHandleChange--;
            }
        }
    }
}
