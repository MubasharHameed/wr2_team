/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 15/09/2016 14:33:51
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_LBTKPRG : com.isb.enq.dbObjects.Base.IDH_LBTKPRG {

        public IDH_LBTKPRG()
            : base() {
            msAutoNumKey = "SEQLBTPRG";
        }

        public IDH_LBTKPRG(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oIDHForm, oForm) {
            msAutoNumKey = "SEQLBTPRG";
        }
        public static ArrayList oDataToSave = new ArrayList();

        public static bool doProcessPendingData() {
            if (oDataToSave.Count == 0)
                return true;
            for (int i = 0; i <= oDataToSave.Count - 1; i++) {
                IDH_LBTKPRG obj = (IDH_LBTKPRG)oDataToSave[i];
                obj.UnLockTable = true;
                 IDH_LBTKPRG obj2 = new IDH_LBTKPRG();
                obj2.UnLockTable = true;
                int iRet = obj2.getData(IDH_LBTKPRG._TaskCd + " =\'" + obj.U_TaskCd + "\' And " + IDH_LBTKPRG._AnalysisCd + "=\'" + obj.U_AnalysisCd + "\' ", "");
                if (iRet == 0) {
                    if (obj.doProcessData() == false)
                        return false;
                } else {
                    for (int icol = 0; icol<= obj2.getColumnCount() - 1; icol++) {
                        if (obj2.getFieldInfo(icol).FieldName!=IDH_LBTKPRG._Code && obj2.getFieldInfo(icol).FieldName!=IDH_LBTKPRG._Name)
                        obj2.setValue(obj2.getFieldInfo(icol).FieldName,obj.getValue( obj.getFieldInfo(icol).FieldName));
                        if (obj2.doProcessData() == false)
                            return false;
                    }
                }
            }
            oDataToSave.Clear();
            return true;
        }
    }
}
