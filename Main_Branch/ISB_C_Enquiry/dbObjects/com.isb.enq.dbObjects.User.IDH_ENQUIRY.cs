/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 08/07/2015 16:21:30
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.bridge.lookups;
using com.idh.dbObjects.numbers;
using com.idh.bridge;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_ENQUIRY : com.isb.enq.dbObjects.Base.IDH_ENQUIRY {

        private IDH_ENQITEM moEnquiryItems;
        public IDH_ENQITEM EnquiryItems {
            get { return moEnquiryItems; }
            //set { moEnquiryItems = value; }
        }

        public IDH_ENQUIRY()
            : base() {
            msAutoNumKey = "ENQUIRYH";
            //moEnquiryItems = new IDH_ENQITEM(this);
        }

        public IDH_ENQUIRY(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oIDHForm, oForm) {
            msAutoNumKey = "ENQUIRYH";
            //moEnquiryItems = new IDH_ENQITEM(this);
        }
        private int miDoHandleChange = 0;

        private bool mbAddressChanged = false;
        public bool DoAddressChanged {
            get { return mbAddressChanged; }
            set { mbAddressChanged = value; }
        }
        private bool mbFormHasDatatoSave = false;
        public bool FormHasDatatoSave {
            set {
                mbFormHasDatatoSave = value;
            }
            get {
                return mbFormHasDatatoSave;
            }
        }

        private bool mbFromActivity = false;

        public bool bFromActivity {
            get { return mbFromActivity; }
            set { mbFromActivity = value; }
        }
        public bool hasRows {
            get { return moEnquiryItems != null && moEnquiryItems.Count > 0; }
        }

        public IDH_ENQITEM getRowDB() {
            return moEnquiryItems;
        }

        public bool doFormHaveSomeData() {
            if (mbFormHasDatatoSave || U_CardCode != string.Empty || U_CardName != string.Empty || U_Address != string.Empty || U_City != string.Empty || U_ZipCode != string.Empty) {
                return true;
            } else 
                return false;
        }

        //public void doMarkUserUpdate(string sFieldName) {
        //    doMarkUserUpdate(sFieldName, null, getValue(sFieldName), false);
        //}
        //public void doMarkUserUpdate(string sFieldName, bool bDoPreSet) {
        //    doMarkUserUpdate(sFieldName, null, getValue(sFieldName), bDoPreSet);
        //}
        //private void doMarkUserUpdate(string sFieldName, object oOldValue, object oNewValue, bool bDoPreSet) {
        //    doSetFieldHasChanged(sFieldName, oOldValue, oNewValue);
        //}

        #region overrides

        public override void doRegisterChildren() {
            if (moEnquiryItems == null) {
                //if (ParentControl != null)
                //    moEnquiryItems = new IDH_ENQITEM(ParentControl, this);
                //else
                moEnquiryItems = new IDH_ENQITEM(this);
            }

            addChild("EnquiryItem", moEnquiryItems, _Code, IDH_ENQITEM._EnqId);
        }

        public override void doSetFieldHasChanged(int iRow, string sFieldname, object oOldValue, object oNewValue) {
            base.doSetFieldHasChanged(iRow, sFieldname, oOldValue, oNewValue);
            if (miDoHandleChange > 0)
                return;
            if (sFieldname == _CardName) {
                if ((oNewValue == null || oNewValue.ToString().Trim() == string.Empty) && U_CardCode != string.Empty) {
                    miDoHandleChange++;
                    U_CardCode = "";
                    U_Address = "";
                    U_Street = "";
                    U_Block = "";
                    U_AddrssLN = "";
                    U_ZipCode = "";
                    U_City = "";
                    U_County = "";
                    U_CName = "";
                    U_E_Mail = "";
                    U_Phone1 = "";
                    miDoHandleChange--;
                }
            } else if (sFieldname == _CardCode) {
                if (oNewValue != null && oNewValue.ToString().Trim() != string.Empty) {
                    ArrayList oContactData = Config.INSTANCE.doGetBPContactEmployee(U_CardCode, "DEFAULTCONTACT");
                    if (oContactData != null) {
                        U_CName = oContactData[0].ToString();
                        U_Phone1 = oContactData[4].ToString();
                        U_E_Mail = oContactData[7].ToString();
                    }
                    if (U_AddrssLN != "" || U_Address.Trim() == string.Empty) {
                        ArrayList oData = Config.INSTANCE.doGetAddress(U_CardCode, "S", null); //WR1_Search.idh.forms.search.AddressSearch.doGetAddress(, val, "S");
                        if (oData != null && oData.Count > 0) {
                            miDoHandleChange++;
                            U_Address = oData[0].ToString();
                            U_Street = oData[1].ToString();
                            U_Block = oData[2].ToString();
                            U_ZipCode = oData[3].ToString();
                            U_City = oData[4].ToString();
                            U_Country = oData[5].ToString();
                            U_County = oData[17].ToString();
                            U_AddrssLN = oData[30].ToString();
                            miDoHandleChange--;
                        } else {
                            miDoHandleChange++;
                            U_Address = "";
                            U_Street = "";
                            U_Block = "";
                            U_ZipCode = "";
                            U_City = "";
                            U_County = "";
                            U_AddrssLN = "";
                            miDoHandleChange--;
                        }
                    }
                } else {
                    miDoHandleChange++;
                    U_Address = "";
                    U_Street = "";
                    U_Block = "";
                    U_AddrssLN = "";
                    U_ZipCode = "";
                    U_City = "";
                    U_County = "";
                    U_CName = "";
                    U_E_Mail = "";
                    U_Phone1 = "";
                    miDoHandleChange--;
                }
            } else if (sFieldname == _Address || sFieldname == _Street || sFieldname == _Block || sFieldname == _ZipCode || sFieldname == _City || sFieldname == _County
                  || sFieldname == _Country) {
                mbAddressChanged = true;
            }
            //if (sFieldname==_Country && oNewValue!=null && oNewValue.ToString().Trim()!=string.Empty){

            //}
        }
        
        public override void doApplyDefaults() {
            base.doApplyDefaults();

            doApplyDefaultValue(_User, idh.bridge.DataHandler.INSTANCE.User);
            doApplyDefaultValue(_AttendUser, idh.bridge.DataHandler.INSTANCE.User);
            //NumbersPair oNextNum;
            //oNextNum = getNewKey();
            //Code = oNextNum.CodeNumber.ToString();
            //Name = oNextNum.NameNumber.ToString();
            //doApplyDefaultValue(_Code, Code);
            //doApplyDefaultValue(_Name, Name);
            DateTime oDate = DateTime.Now;
            doApplyDefaultValue(_Recontact, idh.utils.Dates.doDateToSBODateStr(oDate));
            doApplyDefaultValue(_Desc, "");
            doApplyDefaultValue(_Address, "");
            doApplyDefaultValue(_Block, "");
            doApplyDefaultValue(_CardCode, "");
            doApplyDefaultValue(_CardName, "");
            doApplyDefaultValue(_City, "");
            doApplyDefaultValue(_ClgCode, 0);
            doApplyDefaultValue(_CName, "");
            doApplyDefaultValue(_Country, com.idh.bridge.lookups.Config.INSTANCE.getParameterWithDefault("DFTCNTRY", "GB"));
            doApplyDefaultValue(_County, "");
            doApplyDefaultValue(_E_Mail, "");
            doApplyDefaultValue(_Notes, "");
            doApplyDefaultValue(_Phone1, "");
            doApplyDefaultValue(_State, "");
            doApplyDefaultValue(_Status, "1");
            doApplyDefaultValue(_Street, "");
            doApplyDefaultValue(_ZipCode, "");
            mbAddressChanged = false;
            mbFormHasDatatoSave = false;
        }

        public override bool doAddDataRow(bool bDoValidation, string sComment, bool bDoChildren) {
            if (doPreAddUpdateRow()) {
                if (base.doAddDataRow(bDoValidation, sComment, bDoChildren)) {
                    if (!mbFromActivity) {
                        string msg = com.idh.bridge.resources.Messages.getGMessage("INFDOCNO", new string[] { "Enquiry", Code });
                        com.idh.bridge.DataHandler.INSTANCE.doInfo((msg));
                        doCreateActivity(false);
                    }else {
                        doCreateActivity(true);
                    }
                    DoAddressChanged = false;
                    return true;
                } else
                    return false;
            } else
                return false;

        }

        public override bool doUpdateDataRow(bool bDoValidation, string sComment, bool bDoChildren) {
            if (doPreAddUpdateRow()) {
                if (base.doUpdateDataRow(bDoValidation, sComment, bDoChildren)) {
                    doCreateActivity(true);
                    DoAddressChanged = false;
                    return true;
                } else
                    return false;
            }
            else
                return false;
        }
        public override bool doProcessData(bool bDoValidation, string sComment, bool bDoChildren) {
            return base.doProcessData(bDoValidation, sComment, bDoChildren);
        }
        #endregion
        #region rowLoader
        public override void doLoadRowData(bool bLoadChildren) {
            base.doLoadRowData(bLoadChildren);
            mbAddressChanged = false;
            mbFormHasDatatoSave = false;
        }
        public override void doClearBuffers() {
            base.doClearBuffers();
            //if (MustLoadChildren)
            //    moEnquiryItems.doClearBuffers();
            mbAddressChanged = false;
            mbFormHasDatatoSave = false;
        }
		public override void doLoadChildren() {
            base.doLoadChildren();
        }
        //public override void doLoadChildren() {
        //    if (Code != null && Code.Length > 0) {
        //        moEnquiryItems.getByParentNumber(Code);
        //    } else {
        //        moEnquiryItems.doClearBuffers();
        //    }
        //}
        #endregion
        #region "Validation"
        public override string doValidate_CardName(object oValue) {
            if (U_CardName.Trim() == string.Empty) {
                return com.idh.bridge.Translation.getTranslatedWord("Error: Business Partner is missing.");
            }
            return "";
        }
        public override string doValidate_Address(object oValue) {
            if (U_Address.Trim() == string.Empty) {
                return com.idh.bridge.Translation.getTranslatedWord("Error: Address is missing.");
            }
            return "";
        }
        public override string  doValidate_ZipCode(object oValue){
            if (U_ZipCode.Trim() == string.Empty) {
                return com.idh.bridge.Translation.getTranslatedWord("Error: Postcode is missing.");
            }
            return "";
        }
        public override string doValidate_City(object oValue) {
            if (U_City.Trim() == string.Empty) {
                return com.idh.bridge.Translation.getTranslatedWord("Error: City is missing.");
            }
            return "";
        }

        public override string doValidate_Country(object oValue) {
            if (U_Country.Trim() == string.Empty) {
                return com.idh.bridge.Translation.getTranslatedWord("Error: Country is missing.");
            }
            return "";
        }
        public override string doValidate_CName(object oValue) {
            if (U_CName.Trim() == string.Empty) {
                return com.idh.bridge.Translation.getTranslatedWord("Error: Contact person is missing.");
            }
            return "";
        }
        public override string doValidate_Phone1(object oValue) {
            if (U_Phone1.Trim() == string.Empty && U_E_Mail == string.Empty) {
                return com.idh.bridge.Translation.getTranslatedWord("Error: Contact detail(s) is missing.");
            }
            return "";
        }
        public override string doValidate_E_Mail(object oValue) {
            if (U_Phone1.Trim() == string.Empty && U_E_Mail == string.Empty) {
                return com.idh.bridge.Translation.getTranslatedWord("Error: Contact detail(s) is missing.");
            }
            return "";
        }
        #endregion
        #region "Add Address and contact persons"
        public bool doAddUpdateAddress() {
            SAPbobsCOM.BusinessPartners oBP = null;
            SAPbobsCOM.BPAddresses oAddress = null;
            try {
                if (U_CardCode != string.Empty && U_Address != string.Empty) {
                    object oAddressLine = null;
                    oAddressLine = Config.INSTANCE.doLookupTableField("CRD1", "U_AddrsCd", "CardCode='" + U_CardCode.Replace("'", "''") + "' And Address='" + U_Address.Replace("'", "''") + "' And AdresType='S'", "", true);
                    if (DoAddressChanged || oAddressLine == null) {
                        //Add Address
                        //If sCardCode.Length > 0 Then
                        //  Try
                        oBP = (SAPbobsCOM.BusinessPartners)DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
                        if (oBP.GetByKey(U_CardCode)) {
                            oAddress = oBP.Addresses;
                            if (oAddressLine == null) {
                                oAddress.Add();
                                oAddress.SetCurrentLine(oAddress.Count - 1);
                                oAddress.UserFields.Fields.Item("U_AddrsCd").Value = bridge.lookups.Config.INSTANCE.GetAddressNextCode();
                            } else if (DoAddressChanged) {
                                int i = 0;
                                for (i = 0; i < oAddress.Count; i++) {
                                    oAddress.SetCurrentLine(i);
                                    if (oAddress.AddressName == U_Address && oAddress.AddressType == SAPbobsCOM.BoAddressType.bo_ShipTo)
                                        break;
                                }
                                if (i == oAddress.Count) {
                                    oAddress.Add();
                                    oAddress.SetCurrentLine(oAddress.Count - 1);
                                    oAddress.UserFields.Fields.Item("U_AddrsCd").Value = bridge.lookups.Config.INSTANCE.GetAddressNextCode();
                                }
                            }
                            oAddress.AddressName = U_Address;
                            oAddress.Street = U_Street;
                            oAddress.Block = U_Block;
                            oAddress.ZipCode = U_ZipCode;
                            oAddress.City = U_City;
                            oAddress.County = U_County;
                            oAddress.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo;
                            oAddress.State = U_State;
                            oAddress.Country = U_Country;
                            int iwResult;
                            string swResult;
                            iwResult = oBP.Update();
                            oAddressLine = Config.INSTANCE.doLookupTableField("CRD1", "U_AddrsCd", "CardCode Like '" + U_CardCode.Replace("'", "''") + "' And Address Like '" + U_Address.Replace("'", "''") + "' And AdresType='S'", "U_AddrsCd", true);
                            if (oAddressLine != null)
                                U_AddrssLN = Convert.ToString(oAddressLine);
                            if (iwResult != 0) {
                                DataHandler.INSTANCE.SBOCompany.GetLastError(out iwResult, out swResult);
                                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Adding Address: " + swResult, "ERSYADDA", new string[] { swResult });
                                return false;
                            }
                            return true;
                        }

                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doAddUpdateAddress:Add/Update BP's Address" });
                return false;
            } finally {
                if (oBP != null) {
                    object oObj = (object)oBP;
                    IDHAddOns.idh.data.Base.doReleaseObject(ref oObj);
                    oObj = (object)oAddress;
                    IDHAddOns.idh.data.Base.doReleaseObject(ref oObj);
                }
            }
            return true;
        }
        #endregion
        #region Create Activity
        public void doCreateActivity(bool bdoUpdate) {
            com.idh.bridge.DataRecords oRecords = null;
            try {
                if (bdoUpdate) {
                    object oBPActivity = Config.INSTANCE.doLookupTableField("OCLG", "ClgCode", "ClgCode='" + U_ClgCode + "'", null, true);
                    if (oBPActivity == null || oBPActivity.ToString() == string.Empty)
                        bdoUpdate = false;
                }
                if (bdoUpdate == false) {
                    int iActivityType = -1;
                    SAPbobsCOM.ActivityTypes oActivityType = (SAPbobsCOM.ActivityTypes)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oActivityTypes);
                    string sQry = "Select * from OCLT Where Name Like 'Enquiry'";
                    oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("doCheckActivityofEnquiryType", sQry);
                    if (oRecords != null && oRecords.RecordCount > 0) {
                        if (oRecords.getValue("Name").ToString().ToLower().Trim() == "enquiry") {
                            oActivityType.GetByKey(Convert.ToInt16(oRecords.getValue("Code")));
                            iActivityType = oActivityType.Code;
                        }
                    } else {
                        oActivityType.Name = "Enquiry";
                        oActivityType.Add();
                        iActivityType = Convert.ToInt16(com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetNewObjectKey());

                    }

                    SAPbobsCOM.CompanyService oCompanyService = com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetCompanyService();
                    SAPbobsCOM.ActivitiesService oActivityService = (SAPbobsCOM.ActivitiesService)oCompanyService.GetBusinessService(SAPbobsCOM.ServiceTypes.ActivitiesService);
                    SAPbobsCOM.Activity oActivity = (SAPbobsCOM.Activity)oActivityService.GetDataInterface(SAPbobsCOM.ActivitiesServiceDataInterfaces.asActivity);
                    SAPbobsCOM.ActivityParams oActParams;
                    if (U_CardCode != string.Empty && doValidateBPbyCode(U_CardCode)) {
                        oActivity.CardCode = U_CardCode;
                        oActivity.Personalflag = SAPbobsCOM.BoYesNoEnum.tNO;
                        object oContactPerson = Config.INSTANCE.doLookupTableField("OCPR", "CntctCode", "CardCode='" + U_CardCode + "' And [Name] Like '" + U_CName + "'", null, true);
                        if (oContactPerson != null)
                            oActivity.ContactPersonCode = Convert.ToInt32(oContactPerson);
                    } else {
                        oActivity.Personalflag = SAPbobsCOM.BoYesNoEnum.tYES;
                    }
                    oActivity.Notes = U_Notes;
                    oActivity.Details = U_Desc.Length > 100 ? U_Desc.Substring(0, 100) : U_Desc;
                    oActivity.Activity = SAPbobsCOM.BoActivities.cn_Conversation;
                    oActivity.ActivityType = iActivityType;
                    oActivity.Phone = U_Phone1;
                    string UserName = U_AttendUser == string.Empty ? idh.bridge.DataHandler.INSTANCE.User : U_AttendUser;
                    int UserID = Convert.ToInt32(com.idh.bridge.lookups.Config.INSTANCE.doGetUserId(UserName));
                    //object oUserCode = Config.INSTANCE.doLookupTableField("OUSR", "USERID", "USER_CODE='" + UserName + "'");
                    // if (UserID != null)
                    oActivity.HandledBy = UserID;// Convert.ToInt16(oUserCode);

                    oActivity.ActivityDate = DateTime.Today.Date;//idh.utils.Dates.doDateTo( oDate)  
                    oActivity.ActivityTime = DateTime.Now;
                    oActivity.EndDuedate = DateTime.Today;
                    oActivity.EndTime = DateTime.Now.AddMinutes(15);
                    oActivity.UserFields.Item("U_IDHEnqID").Value = Code;
                    oActParams = oActivityService.AddActivity(oActivity);
                    if (hasDataToProcess())
                        doClearAddUpdateBuffers();
                    U_ClgCode = oActParams.ActivityCode;
                    if(hasDataToProcess())
                        doProcessData();
                } else if (U_ClgCode != 0) {
                    SAPbobsCOM.CompanyService oCompanyService = com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetCompanyService();
                    SAPbobsCOM.ActivitiesService oActivityService = (SAPbobsCOM.ActivitiesService)oCompanyService.GetBusinessService(SAPbobsCOM.ServiceTypes.ActivitiesService);
                    SAPbobsCOM.Activity oActivity = (SAPbobsCOM.Activity)oActivityService.GetDataInterface(SAPbobsCOM.ActivitiesServiceDataInterfaces.asActivity);
                    SAPbobsCOM.ActivityParams oActParam;
                    oActParam = (SAPbobsCOM.ActivityParams)oActivityService.GetDataInterface(SAPbobsCOM.ActivitiesServiceDataInterfaces.asActivityParams);
                    oActParam.ActivityCode = U_ClgCode;
                    oActivity = oActivityService.GetActivity(oActParam);

                    string UserName = U_AttendUser == string.Empty ? idh.bridge.DataHandler.INSTANCE.User : U_AttendUser;
                    int UserID = Convert.ToInt32(com.idh.bridge.lookups.Config.INSTANCE.doGetUserId(UserName));
                    //object oUserCode = Config.INSTANCE.doLookupTableField("OUSR", "USERID", "USER_CODE='" + UserName + "'");
                    // if (UserID != null)
                    bool bUserUpdated = false;
                    if (oActivity.HandledBy != UserID) {
                        oActivity.HandledBy = UserID;
                        bUserUpdated = true;
                    }
                    if (oActivity.CardCode == string.Empty && !string.IsNullOrWhiteSpace(U_CardCode)) {
                        oActivity.Personalflag = SAPbobsCOM.BoYesNoEnum.tNO;
                        oActivity.CardCode = U_CardCode;
                        object oContactPerson = Config.INSTANCE.doLookupTableField("OCPR", "CntctCode", "CardCode='" + U_CardCode + "' And [Name] Like '" + U_CName + "'", null, true);
                        if (oContactPerson != null)
                            oActivity.ContactPersonCode = Convert.ToInt32(oContactPerson);
                        bUserUpdated = true;
                    }
                    if (bUserUpdated)
                        oActivityService.UpdateActivity(oActivity);
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
            }
        }
        private bool doValidateBPbyCode(string sCardCode) {
            string sCardName = Config.INSTANCE.doGetBPName(sCardCode, false);
            if (sCardName == string.Empty)
                return false;
            else
                return true;
        }
        #endregion
        public bool doPreAddUpdateRow() {
			//THIS IS YOUR VALIDATION ISSUE....
            //if (doValidation() == false) {
            //    com.idh.bridge.DataHandler.INSTANCE.doUserError(LastValidationError);
            //    return false;
            //}
            if (doAddUpdateAddress()) {
                return true;
            } else
                return false;
        }
        
    }
}
