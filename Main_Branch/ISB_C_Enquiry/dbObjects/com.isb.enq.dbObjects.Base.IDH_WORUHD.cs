/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 22/09/2017 11:49:40
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base {
    [Serializable]
    public class IDH_WORUHD : com.idh.dbObjects.DBBase {

        private Linker moLinker = null;
        public Linker ControlLinker {
            get { return moLinker; }
            set { moLinker = value; }
        }

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_WORUHD() : base("@IDH_WORUHD") {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_WORUHD(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm) : base(oForm, "@IDH_WORUHD") {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
         * Table name
         */
        public readonly static string TableName = "@IDH_WORUHD";

        /**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
        public readonly static string _Code = "Code";
        public string Code {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code() {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue) {
            return base.doValidation(_Code, oValue);
        }
        /**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
        public readonly static string _Name = "Name";
        public string Name {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name() {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue) {
            return base.doValidation(_Name, oValue);
        }
        /**
		 * Decription: Record Added By
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AddedBy
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _AddedBy = "U_AddedBy";
        public string U_AddedBy {
            get {
                return getValueAsString(_AddedBy);
            }
            set { setValue(_AddedBy, value); }
        }
        public string doValidate_AddedBy() {
            return doValidate_AddedBy(U_AddedBy);
        }
        public virtual string doValidate_AddedBy(object oValue) {
            return base.doValidation(_AddedBy, oValue);
        }

        /**
		 * Decription: BatchNum
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BatchNum
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _BatchNum = "U_BatchNum";
        public string U_BatchNum {
            get {
                return getValueAsString(_BatchNum);
            }
            set { setValue(_BatchNum, value); }
        }
        public string doValidate_BatchNum() {
            return doValidate_BatchNum(U_BatchNum);
        }
        public virtual string doValidate_BatchNum(object oValue) {
            return base.doValidation(_BatchNum, oValue);
        }

        /**
		 * Decription: Batch Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BthStatus
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _BthStatus = "U_BthStatus";
        public string U_BthStatus {
            get {
                return getValueAsString(_BthStatus);
            }
            set { setValue(_BthStatus, value); }
        }
        public string doValidate_BthStatus() {
            return doValidate_BthStatus(U_BthStatus);
        }
        public virtual string doValidate_BthStatus(object oValue) {
            return base.doValidation(_BthStatus, oValue);
        }

        /**
		 * Decription: Cancelled
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Cancelled
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Cancelled = "U_Cancelled";
        public string U_Cancelled {
            get {
                return getValueAsString(_Cancelled);
            }
            set { setValue(_Cancelled, value); }
        }
        public string doValidate_Cancelled() {
            return doValidate_Cancelled(U_Cancelled);
        }
        public virtual string doValidate_Cancelled(object oValue) {
            return base.doValidation(_Cancelled, oValue);
        }

        /**
		 * Decription: Carrier Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CarrCd
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CarrCd = "U_CarrCd";
        public string U_CarrCd {
            get {
                return getValueAsString(_CarrCd);
            }
            set { setValue(_CarrCd, value); }
        }
        public string doValidate_CarrCd() {
            return doValidate_CarrCd(U_CarrCd);
        }
        public virtual string doValidate_CarrCd(object oValue) {
            return base.doValidation(_CarrCd, oValue);
        }

        /**
		 * Decription: Carrier Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CarrNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CarrNm = "U_CarrNm";
        public string U_CarrNm {
            get {
                return getValueAsString(_CarrNm);
            }
            set { setValue(_CarrNm, value); }
        }
        public string doValidate_CarrNm() {
            return doValidate_CarrNm(U_CarrNm);
        }
        public virtual string doValidate_CarrNm(object oValue) {
            return base.doValidation(_CarrNm, oValue);
        }

        /**
		 * Decription: Customer Site Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CustAdr
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CustAdr = "U_CustAdr";
        public string U_CustAdr {
            get {
                return getValueAsString(_CustAdr);
            }
            set { setValue(_CustAdr, value); }
        }
        public string doValidate_CustAdr() {
            return doValidate_CustAdr(U_CustAdr);
        }
        public virtual string doValidate_CustAdr(object oValue) {
            return base.doValidation(_CustAdr, oValue);
        }

        /**
		 * Decription: Customer-Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CustCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CustCd = "U_CustCd";
        public string U_CustCd {
            get {
                return getValueAsString(_CustCd);
            }
            set { setValue(_CustCd, value); }
        }
        public string doValidate_CustCd() {
            return doValidate_CustCd(U_CustCd);
        }
        public virtual string doValidate_CustCd(object oValue) {
            return base.doValidation(_CustCd, oValue);
        }

        /**
		 * Decription: Customer-Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CustNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CustNm = "U_CustNm";
        public string U_CustNm {
            get {
                return getValueAsString(_CustNm);
            }
            set { setValue(_CustNm, value); }
        }
        public string doValidate_CustNm() {
            return doValidate_CustNm(U_CustNm);
        }
        public virtual string doValidate_CustNm(object oValue) {
            return base.doValidation(_CustNm, oValue);
        }

        /**
		 * Decription: Customer Reference
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CustRef
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CustRef = "U_CustRef";
        public string U_CustRef {
            get {
                return getValueAsString(_CustRef);
            }
            set { setValue(_CustRef, value); }
        }
        public string doValidate_CustRef() {
            return doValidate_CustRef(U_CustRef);
        }
        public virtual string doValidate_CustRef(object oValue) {
            return base.doValidation(_CustRef, oValue);
        }

        /**
		 * Decription: Disposal Qty
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DispQty
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _DispQty = "U_DispQty";
        public double U_DispQty {
            get {
                return getValueAsDouble(_DispQty);
            }
            set { setValue(_DispQty, value); }
        }
        public string doValidate_DispQty() {
            return doValidate_DispQty(U_DispQty);
        }
        public virtual string doValidate_DispQty(object oValue) {
            return base.doValidation(_DispQty, oValue);
        }

        /**
		 * Decription: EDate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_EDate
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
        public readonly static string _EDate = "U_EDate";
        public DateTime U_EDate {
            get {
                return getValueAsDateTime(_EDate);
            }
            set { setValue(_EDate, value); }
        }
        public void U_EDate_AsString(string value) {
            setValue("U_EDate", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_EDate_AsString() {
            DateTime dVal = getValueAsDateTime(_EDate);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_EDate() {
            return doValidate_EDate(U_EDate);
        }
        public virtual string doValidate_EDate(object oValue) {
            return base.doValidation(_EDate, oValue);
        }

        /**
		 * Decription: Cust Reporting Weight
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ExptQty
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _ExptQty = "U_ExptQty";
        public double U_ExptQty {
            get {
                return getValueAsDouble(_ExptQty);
            }
            set { setValue(_ExptQty, value); }
        }
        public string doValidate_ExptQty() {
            return doValidate_ExptQty(U_ExptQty);
        }
        public virtual string doValidate_ExptQty(object oValue) {
            return base.doValidation(_ExptQty, oValue);
        }

        /**
		 * Decription: Haulage Qty
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HulgQty
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _HulgQty = "U_HulgQty";
        public double U_HulgQty {
            get {
                return getValueAsDouble(_HulgQty);
            }
            set { setValue(_HulgQty, value); }
        }
        public string doValidate_HulgQty() {
            return doValidate_HulgQty(U_HulgQty);
        }
        public virtual string doValidate_HulgQty(object oValue) {
            return base.doValidation(_HulgQty, oValue);
        }

        /**
		 * Decription: Container Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItemCd
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _ItemCd = "U_ItemCd";
        public string U_ItemCd {
            get {
                return getValueAsString(_ItemCd);
            }
            set { setValue(_ItemCd, value); }
        }
        public string doValidate_ItemCd() {
            return doValidate_ItemCd(U_ItemCd);
        }
        public virtual string doValidate_ItemCd(object oValue) {
            return base.doValidation(_ItemCd, oValue);
        }

        /**
		 * Decription: Container
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItemNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _ItemNm = "U_ItemNm";
        public string U_ItemNm {
            get {
                return getValueAsString(_ItemNm);
            }
            set { setValue(_ItemNm, value); }
        }
        public string doValidate_ItemNm() {
            return doValidate_ItemNm(U_ItemNm);
        }
        public virtual string doValidate_ItemNm(object oValue) {
            return base.doValidation(_ItemNm, oValue);
        }

        /**
		 * Decription: Order Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_JbTypeDs
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _JbTypeDs = "U_JbTypeDs";
        public string U_JbTypeDs {
            get {
                return getValueAsString(_JbTypeDs);
            }
            set { setValue(_JbTypeDs, value); }
        }
        public string doValidate_JbTypeDs() {
            return doValidate_JbTypeDs(U_JbTypeDs);
        }
        public virtual string doValidate_JbTypeDs(object oValue) {
            return base.doValidation(_JbTypeDs, oValue);
        }

        /**
		 * Decription: Maximo Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_MaximoNum
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _MaximoNum = "U_MaximoNum";
        public string U_MaximoNum {
            get {
                return getValueAsString(_MaximoNum);
            }
            set { setValue(_MaximoNum, value); }
        }
        public string doValidate_MaximoNum() {
            return doValidate_MaximoNum(U_MaximoNum);
        }
        public virtual string doValidate_MaximoNum(object oValue) {
            return base.doValidation(_MaximoNum, oValue);
        }

        /**
		 * Decription: Process Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ProcSts
		 * Size: 2
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _ProcSts = "U_ProcSts";
        public string U_ProcSts {
            get {
                return getValueAsString(_ProcSts);
            }
            set { setValue(_ProcSts, value); }
        }
        public string doValidate_ProcSts() {
            return doValidate_ProcSts(U_ProcSts);
        }
        public virtual string doValidate_ProcSts(object oValue) {
            return base.doValidation(_ProcSts, oValue);
        }

        /**
		 * Decription: Requested Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RDate
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
        public readonly static string _RDate = "U_RDate";
        public DateTime U_RDate {
            get {
                return getValueAsDateTime(_RDate);
            }
            set { setValue(_RDate, value); }
        }
        public void U_RDate_AsString(string value) {
            setValue("U_RDate", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_RDate_AsString() {
            DateTime dVal = getValueAsDateTime(_RDate);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_RDate() {
            return doValidate_RDate(U_RDate);
        }
        public virtual string doValidate_RDate(object oValue) {
            return base.doValidation(_RDate, oValue);
        }

        /**
		 * Decription: Rebate Weights
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RebWgt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _RebWgt = "U_RebWgt";
        public double U_RebWgt {
            get {
                return getValueAsDouble(_RebWgt);
            }
            set { setValue(_RebWgt, value); }
        }
        public string doValidate_RebWgt() {
            return doValidate_RebWgt(U_RebWgt);
        }
        public virtual string doValidate_RebWgt(object oValue) {
            return base.doValidation(_RebWgt, oValue);
        }

        /**
		 * Decription: SDate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SDate
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
        public readonly static string _SDate = "U_SDate";
        public DateTime U_SDate {
            get {
                return getValueAsDateTime(_SDate);
            }
            set { setValue(_SDate, value); }
        }
        public void U_SDate_AsString(string value) {
            setValue("U_SDate", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_SDate_AsString() {
            DateTime dVal = getValueAsDateTime(_SDate);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_SDate() {
            return doValidate_SDate(U_SDate);
        }
        public virtual string doValidate_SDate(object oValue) {
            return base.doValidation(_SDate, oValue);
        }

        /**
		 * Decription: Unit of Measure (UOM)
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UOM
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _UOM = "U_UOM";
        public string U_UOM {
            get {
                return getValueAsString(_UOM);
            }
            set { setValue(_UOM, value); }
        }
        public string doValidate_UOM() {
            return doValidate_UOM(U_UOM);
        }
        public virtual string doValidate_UOM(object oValue) {
            return base.doValidation(_UOM, oValue);
        }

        /**
		 * Decription: Validation Reason
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_VdReason
		 * Size: 254
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _VdReason = "U_VdReason";
        public string U_VdReason {
            get {
                return getValueAsString(_VdReason);
            }
            set { setValue(_VdReason, value); }
        }
        public string doValidate_VdReason() {
            return doValidate_VdReason(U_VdReason);
        }
        public virtual string doValidate_VdReason(object oValue) {
            return base.doValidation(_VdReason, oValue);
        }

        /**
		 * Decription: Vehicle Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_VehTyp
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _VehTyp = "U_VehTyp";
        public string U_VehTyp {
            get {
                return getValueAsString(_VehTyp);
            }
            set { setValue(_VehTyp, value); }
        }
        public string doValidate_VehTyp() {
            return doValidate_VehTyp(U_VehTyp);
        }
        public virtual string doValidate_VehTyp(object oValue) {
            return base.doValidation(_VehTyp, oValue);
        }

        /**
		 * Decription: Validation Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_VldStatus
		 * Size: 2
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _VldStatus = "U_VldStatus";
        public string U_VldStatus {
            get {
                return getValueAsString(_VldStatus);
            }
            set { setValue(_VldStatus, value); }
        }
        public string doValidate_VldStatus() {
            return doValidate_VldStatus(U_VldStatus);
        }
        public virtual string doValidate_VldStatus(object oValue) {
            return base.doValidation(_VldStatus, oValue);
        }

        /**
		 * Decription: Waste Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WastCd
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _WastCd = "U_WastCd";
        public string U_WastCd {
            get {
                return getValueAsString(_WastCd);
            }
            set { setValue(_WastCd, value); }
        }
        public string doValidate_WastCd() {
            return doValidate_WastCd(U_WastCd);
        }
        public virtual string doValidate_WastCd(object oValue) {
            return base.doValidation(_WastCd, oValue);
        }

        /**
		 * Decription: Waste Material
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WasteNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _WasteNm = "U_WasteNm";
        public string U_WasteNm {
            get {
                return getValueAsString(_WasteNm);
            }
            set { setValue(_WasteNm, value); }
        }
        public string doValidate_WasteNm() {
            return doValidate_WasteNm(U_WasteNm);
        }
        public virtual string doValidate_WasteNm(object oValue) {
            return base.doValidation(_WasteNm, oValue);
        }

        /**
		 * Decription: WOR
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WOR
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _WOR = "U_WOR";
        public string U_WOR {
            get {
                return getValueAsString(_WOR);
            }
            set { setValue(_WOR, value); }
        }
        public string doValidate_WOR() {
            return doValidate_WOR(U_WOR);
        }
        public virtual string doValidate_WOR(object oValue) {
            return base.doValidation(_WOR, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo() {
            if (moDBFields == null)
                moDBFields = new DBFields(31);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_AddedBy, "Record Added By", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Record Added By
            moDBFields.Add(_BatchNum, "BatchNum", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //BatchNum
            moDBFields.Add(_BthStatus, "Batch Status", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Batch Status
            moDBFields.Add(_Cancelled, "Cancelled", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Cancelled
            moDBFields.Add(_CarrCd, "Carrier Code", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //Carrier Code
            moDBFields.Add(_CarrNm, "Carrier Name", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Carrier Name
            moDBFields.Add(_CustAdr, "Customer Site Address", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Customer Site Address
            moDBFields.Add(_CustCd, "Customer-Code", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Customer-Code
            moDBFields.Add(_CustNm, "Customer-Name", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Customer-Name
            moDBFields.Add(_CustRef, "Customer Reference", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Customer Reference
            moDBFields.Add(_DispQty, "Disposal Qty", 12, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Disposal Qty
            moDBFields.Add(_EDate, "EDate", 13, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //EDate
            moDBFields.Add(_ExptQty, "Cust Reporting Weight", 14, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Cust Reporting Weight
            moDBFields.Add(_HulgQty, "Haulage Qty", 15, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Haulage Qty
            moDBFields.Add(_ItemCd, "Container Code", 16, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //Container Code
            moDBFields.Add(_ItemNm, "Container", 17, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Container
            moDBFields.Add(_JbTypeDs, "Order Type", 18, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Order Type
            moDBFields.Add(_MaximoNum, "Maximo Number", 19, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Maximo Number
            moDBFields.Add(_ProcSts, "Process Status", 20, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2, EMPTYSTR, false, false); //Process Status
            moDBFields.Add(_RDate, "Requested Date", 21, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Requested Date
            moDBFields.Add(_RebWgt, "Rebate Weights", 22, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Rebate Weights
            moDBFields.Add(_SDate, "SDate", 23, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //SDate
            moDBFields.Add(_UOM, "Unit of Measure (UOM)", 24, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Unit of Measure (UOM)
            moDBFields.Add(_VdReason, "Validation Reason", 25, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, EMPTYSTR, false, false); //Validation Reason
            moDBFields.Add(_VehTyp, "Vehicle Type", 26, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Vehicle Type
            moDBFields.Add(_VldStatus, "Validation Status", 27, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2, EMPTYSTR, false, false); //Validation Status
            moDBFields.Add(_WastCd, "Waste Code", 28, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //Waste Code
            moDBFields.Add(_WasteNm, "Waste Material", 29, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Waste Material
            moDBFields.Add(_WOR, "WOR", 30, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //WOR

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation() {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_AddedBy());
            doBuildValidationString(doValidate_BatchNum());
            doBuildValidationString(doValidate_BthStatus());
            doBuildValidationString(doValidate_Cancelled());
            doBuildValidationString(doValidate_CarrCd());
            doBuildValidationString(doValidate_CarrNm());
            doBuildValidationString(doValidate_CustAdr());
            doBuildValidationString(doValidate_CustCd());
            doBuildValidationString(doValidate_CustNm());
            doBuildValidationString(doValidate_CustRef());
            doBuildValidationString(doValidate_DispQty());
            doBuildValidationString(doValidate_EDate());
            doBuildValidationString(doValidate_ExptQty());
            doBuildValidationString(doValidate_HulgQty());
            doBuildValidationString(doValidate_ItemCd());
            doBuildValidationString(doValidate_ItemNm());
            doBuildValidationString(doValidate_JbTypeDs());
            doBuildValidationString(doValidate_MaximoNum());
            doBuildValidationString(doValidate_ProcSts());
            doBuildValidationString(doValidate_RDate());
            doBuildValidationString(doValidate_RebWgt());
            doBuildValidationString(doValidate_SDate());
            doBuildValidationString(doValidate_UOM());
            doBuildValidationString(doValidate_VdReason());
            doBuildValidationString(doValidate_VehTyp());
            doBuildValidationString(doValidate_VldStatus());
            doBuildValidationString(doValidate_WastCd());
            doBuildValidationString(doValidate_WasteNm());
            doBuildValidationString(doValidate_WOR());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_AddedBy)) return doValidate_AddedBy(oValue);
            if (sFieldName.Equals(_BatchNum)) return doValidate_BatchNum(oValue);
            if (sFieldName.Equals(_BthStatus)) return doValidate_BthStatus(oValue);
            if (sFieldName.Equals(_Cancelled)) return doValidate_Cancelled(oValue);
            if (sFieldName.Equals(_CarrCd)) return doValidate_CarrCd(oValue);
            if (sFieldName.Equals(_CarrNm)) return doValidate_CarrNm(oValue);
            if (sFieldName.Equals(_CustAdr)) return doValidate_CustAdr(oValue);
            if (sFieldName.Equals(_CustCd)) return doValidate_CustCd(oValue);
            if (sFieldName.Equals(_CustNm)) return doValidate_CustNm(oValue);
            if (sFieldName.Equals(_CustRef)) return doValidate_CustRef(oValue);
            if (sFieldName.Equals(_DispQty)) return doValidate_DispQty(oValue);
            if (sFieldName.Equals(_EDate)) return doValidate_EDate(oValue);
            if (sFieldName.Equals(_ExptQty)) return doValidate_ExptQty(oValue);
            if (sFieldName.Equals(_HulgQty)) return doValidate_HulgQty(oValue);
            if (sFieldName.Equals(_ItemCd)) return doValidate_ItemCd(oValue);
            if (sFieldName.Equals(_ItemNm)) return doValidate_ItemNm(oValue);
            if (sFieldName.Equals(_JbTypeDs)) return doValidate_JbTypeDs(oValue);
            if (sFieldName.Equals(_MaximoNum)) return doValidate_MaximoNum(oValue);
            if (sFieldName.Equals(_ProcSts)) return doValidate_ProcSts(oValue);
            if (sFieldName.Equals(_RDate)) return doValidate_RDate(oValue);
            if (sFieldName.Equals(_RebWgt)) return doValidate_RebWgt(oValue);
            if (sFieldName.Equals(_SDate)) return doValidate_SDate(oValue);
            if (sFieldName.Equals(_UOM)) return doValidate_UOM(oValue);
            if (sFieldName.Equals(_VdReason)) return doValidate_VdReason(oValue);
            if (sFieldName.Equals(_VehTyp)) return doValidate_VehTyp(oValue);
            if (sFieldName.Equals(_VldStatus)) return doValidate_VldStatus(oValue);
            if (sFieldName.Equals(_WastCd)) return doValidate_WastCd(oValue);
            if (sFieldName.Equals(_WasteNm)) return doValidate_WasteNm(oValue);
            if (sFieldName.Equals(_WOR)) return doValidate_WOR(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName) {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
		 * Link the Name Field to the Form Item.
		 */
        public void doLink_Name(string sControlName) {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
		 * Link the U_AddedBy Field to the Form Item.
		 */
        public void doLink_AddedBy(string sControlName) {
            moLinker.doLinkDataToControl(_AddedBy, sControlName);
        }
        /**
		 * Link the U_BatchNum Field to the Form Item.
		 */
        public void doLink_BatchNum(string sControlName) {
            moLinker.doLinkDataToControl(_BatchNum, sControlName);
        }
        /**
		 * Link the U_BthStatus Field to the Form Item.
		 */
        public void doLink_BthStatus(string sControlName) {
            moLinker.doLinkDataToControl(_BthStatus, sControlName);
        }
        /**
		 * Link the U_Cancelled Field to the Form Item.
		 */
        public void doLink_Cancelled(string sControlName) {
            moLinker.doLinkDataToControl(_Cancelled, sControlName);
        }
        /**
		 * Link the U_CarrCd Field to the Form Item.
		 */
        public void doLink_CarrCd(string sControlName) {
            moLinker.doLinkDataToControl(_CarrCd, sControlName);
        }
        /**
		 * Link the U_CarrNm Field to the Form Item.
		 */
        public void doLink_CarrNm(string sControlName) {
            moLinker.doLinkDataToControl(_CarrNm, sControlName);
        }
        /**
		 * Link the U_CustAdr Field to the Form Item.
		 */
        public void doLink_CustAdr(string sControlName) {
            moLinker.doLinkDataToControl(_CustAdr, sControlName);
        }
        /**
		 * Link the U_CustCd Field to the Form Item.
		 */
        public void doLink_CustCd(string sControlName) {
            moLinker.doLinkDataToControl(_CustCd, sControlName);
        }
        /**
		 * Link the U_CustNm Field to the Form Item.
		 */
        public void doLink_CustNm(string sControlName) {
            moLinker.doLinkDataToControl(_CustNm, sControlName);
        }
        /**
		 * Link the U_CustRef Field to the Form Item.
		 */
        public void doLink_CustRef(string sControlName) {
            moLinker.doLinkDataToControl(_CustRef, sControlName);
        }
        /**
		 * Link the U_DispQty Field to the Form Item.
		 */
        public void doLink_DispQty(string sControlName) {
            moLinker.doLinkDataToControl(_DispQty, sControlName);
        }
        /**
		 * Link the U_EDate Field to the Form Item.
		 */
        public void doLink_EDate(string sControlName) {
            moLinker.doLinkDataToControl(_EDate, sControlName);
        }
        /**
		 * Link the U_ExptQty Field to the Form Item.
		 */
        public void doLink_ExptQty(string sControlName) {
            moLinker.doLinkDataToControl(_ExptQty, sControlName);
        }
        /**
		 * Link the U_HulgQty Field to the Form Item.
		 */
        public void doLink_HulgQty(string sControlName) {
            moLinker.doLinkDataToControl(_HulgQty, sControlName);
        }
        /**
		 * Link the U_ItemCd Field to the Form Item.
		 */
        public void doLink_ItemCd(string sControlName) {
            moLinker.doLinkDataToControl(_ItemCd, sControlName);
        }
        /**
		 * Link the U_ItemNm Field to the Form Item.
		 */
        public void doLink_ItemNm(string sControlName) {
            moLinker.doLinkDataToControl(_ItemNm, sControlName);
        }
        /**
		 * Link the U_JbTypeDs Field to the Form Item.
		 */
        public void doLink_JbTypeDs(string sControlName) {
            moLinker.doLinkDataToControl(_JbTypeDs, sControlName);
        }
        /**
		 * Link the U_MaximoNum Field to the Form Item.
		 */
        public void doLink_MaximoNum(string sControlName) {
            moLinker.doLinkDataToControl(_MaximoNum, sControlName);
        }
        /**
		 * Link the U_ProcSts Field to the Form Item.
		 */
        public void doLink_ProcSts(string sControlName) {
            moLinker.doLinkDataToControl(_ProcSts, sControlName);
        }
        /**
		 * Link the U_RDate Field to the Form Item.
		 */
        public void doLink_RDate(string sControlName) {
            moLinker.doLinkDataToControl(_RDate, sControlName);
        }
        /**
		 * Link the U_RebWgt Field to the Form Item.
		 */
        public void doLink_RebWgt(string sControlName) {
            moLinker.doLinkDataToControl(_RebWgt, sControlName);
        }
        /**
		 * Link the U_SDate Field to the Form Item.
		 */
        public void doLink_SDate(string sControlName) {
            moLinker.doLinkDataToControl(_SDate, sControlName);
        }
        /**
		 * Link the U_UOM Field to the Form Item.
		 */
        public void doLink_UOM(string sControlName) {
            moLinker.doLinkDataToControl(_UOM, sControlName);
        }
        /**
		 * Link the U_VdReason Field to the Form Item.
		 */
        public void doLink_VdReason(string sControlName) {
            moLinker.doLinkDataToControl(_VdReason, sControlName);
        }
        /**
		 * Link the U_VehTyp Field to the Form Item.
		 */
        public void doLink_VehTyp(string sControlName) {
            moLinker.doLinkDataToControl(_VehTyp, sControlName);
        }
        /**
		 * Link the U_VldStatus Field to the Form Item.
		 */
        public void doLink_VldStatus(string sControlName) {
            moLinker.doLinkDataToControl(_VldStatus, sControlName);
        }
        /**
		 * Link the U_WastCd Field to the Form Item.
		 */
        public void doLink_WastCd(string sControlName) {
            moLinker.doLinkDataToControl(_WastCd, sControlName);
        }
        /**
		 * Link the U_WasteNm Field to the Form Item.
		 */
        public void doLink_WasteNm(string sControlName) {
            moLinker.doLinkDataToControl(_WasteNm, sControlName);
        }
        /**
		 * Link the U_WOR Field to the Form Item.
		 */
        public void doLink_WOR(string sControlName) {
            moLinker.doLinkDataToControl(_WOR, sControlName);
        }
        #endregion

    }
}
