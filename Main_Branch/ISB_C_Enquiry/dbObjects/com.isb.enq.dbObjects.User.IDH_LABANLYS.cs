/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 09/02/2016 12:21:43
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User{
   [Serializable] 
	public class IDH_LABANLYS: com.isb.enq.dbObjects.Base.IDH_LABANLYS{ 

		public IDH_LABANLYS() : base() {
			
		}

   	public IDH_LABANLYS( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	}
	}
}
