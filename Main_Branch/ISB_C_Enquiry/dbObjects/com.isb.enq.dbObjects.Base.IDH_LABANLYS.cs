/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 09/02/2016 12:21:39
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base{
   [Serializable]
	public class IDH_LABANLYS: com.idh.dbObjects.DBBase { 

		private Linker moLinker = null;
		protected IDHAddOns.idh.forms.Base moIDHForm;

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_LABANLYS() : base("@IDH_LABANLYS"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_LABANLYS( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_LABANLYS"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_LABANLYS";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Analysis Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnalysisCd
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnalysisCd = "U_AnalysisCd";
		public string U_AnalysisCd { 
			get {
 				return getValueAsString(_AnalysisCd); 
			}
			set { setValue(_AnalysisCd, value); }
		}
           public string doValidate_AnalysisCd() {
               return doValidate_AnalysisCd(U_AnalysisCd);
           }
           public virtual string doValidate_AnalysisCd(object oValue) {
               return base.doValidation(_AnalysisCd, oValue);
           }

		/**
		 * Decription: Analysis Description
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnalysisDesc
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnalysisDesc = "U_AnalysisDesc";
		public string U_AnalysisDesc { 
			get {
 				return getValueAsString(_AnalysisDesc); 
			}
			set { setValue(_AnalysisDesc, value); }
		}
           public string doValidate_AnalysisDesc() {
               return doValidate_AnalysisDesc(U_AnalysisDesc);
           }
           public virtual string doValidate_AnalysisDesc(object oValue) {
               return base.doValidation(_AnalysisDesc, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(4);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_AnalysisCd, "Analysis Code", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Analysis Code
			moDBFields.Add(_AnalysisDesc, "Analysis Description", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Analysis Description

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_AnalysisCd());
            doBuildValidationString(doValidate_AnalysisDesc());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_AnalysisCd)) return doValidate_AnalysisCd(oValue);
            if (sFieldName.Equals(_AnalysisDesc)) return doValidate_AnalysisDesc(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_AnalysisCd Field to the Form Item.
		 */
		public void doLink_AnalysisCd(string sControlName){
			moLinker.doLinkDataToControl(_AnalysisCd, sControlName);
		}
		/**
		 * Link the U_AnalysisDesc Field to the Form Item.
		 */
		public void doLink_AnalysisDesc(string sControlName){
			moLinker.doLinkDataToControl(_AnalysisDesc, sControlName);
		}
#endregion

	}
}
