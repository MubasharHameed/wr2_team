/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 11/11/2016 11:46:33
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_LVSBHH : com.isb.enq.dbObjects.Base.IDH_LVSBHH {

        public IDH_LVSBHH() : base() {
            msAutoNumKey = "SEQLVSBH";
        }

        public IDH_LVSBHH(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm) : base(oIDHForm, oForm) {
            msAutoNumKey = "SEQLVSBH";
        }
    }
}
