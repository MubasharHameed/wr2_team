/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 19/05/2017 12:40:31
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base {
    [Serializable]
    public class IDH_LVSAMD : com.idh.dbObjects.DBBase {

        private Linker moLinker = null;
        public Linker ControlLinker {
            get { return moLinker; }
            set { moLinker = value; }
        }

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_LVSAMD() : base("@IDH_LVSAMD") {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_LVSAMD(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm) : base(oForm, "@IDH_LVSAMD") {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
         * Table name
         */
        public readonly static string TableName = "@IDH_LVSAMD";

        /**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
        public readonly static string _Code = "Code";
        public string Code {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code() {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue) {
            return base.doValidation(_Code, oValue);
        }
        /**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
        public readonly static string _Name = "Name";
        public string Name {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name() {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue) {
            return base.doValidation(_Name, oValue);
        }
        /**
		 * Decription: Active
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Active
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Active = "U_Active";
        public string U_Active {
            get {
                return getValueAsString(_Active);
            }
            set { setValue(_Active, value); }
        }
        public string doValidate_Active() {
            return doValidate_Active(U_Active);
        }
        public virtual string doValidate_Active(object oValue) {
            return base.doValidation(_Active, oValue);
        }

        /**
		 * Decription: Add PBI
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AddPBI
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _AddPBI = "U_AddPBI";
        public string U_AddPBI {
            get {
                return getValueAsString(_AddPBI);
            }
            set { setValue(_AddPBI, value); }
        }
        public string doValidate_AddPBI() {
            return doValidate_AddPBI(U_AddPBI);
        }
        public virtual string doValidate_AddPBI(object oValue) {
            return base.doValidation(_AddPBI, oValue);
        }

        /**
		 * Decription: Amendment Binary Value
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AmdBnVal
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _AmdBnVal = "U_AmdBnVal";
        public int U_AmdBnVal {
            get {
                return getValueAsInt(_AmdBnVal);
            }
            set { setValue(_AmdBnVal, value); }
        }
        public string doValidate_AmdBnVal() {
            return doValidate_AmdBnVal(U_AmdBnVal);
        }
        public virtual string doValidate_AmdBnVal(object oValue) {
            return base.doValidation(_AmdBnVal, oValue);
        }

        /**
		 * Decription: Amendment Group
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AmdDesc
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _AmdDesc = "U_AmdDesc";
        public string U_AmdDesc {
            get {
                return getValueAsString(_AmdDesc);
            }
            set { setValue(_AmdDesc, value); }
        }
        public string doValidate_AmdDesc() {
            return doValidate_AmdDesc(U_AmdDesc);
        }
        public virtual string doValidate_AmdDesc(object oValue) {
            return base.doValidation(_AmdDesc, oValue);
        }

        /**
		 * Decription: Auto Complete
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AutoCmp
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _AutoCmp = "U_AutoCmp";
        public string U_AutoCmp {
            get {
                return getValueAsString(_AutoCmp);
            }
            set { setValue(_AutoCmp, value); }
        }
        public string doValidate_AutoCmp() {
            return doValidate_AutoCmp(U_AutoCmp);
        }
        public virtual string doValidate_AutoCmp(object oValue) {
            return base.doValidation(_AutoCmp, oValue);
        }

        /**
		 * Decription: Days
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Days
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Days = "U_Days";
        public string U_Days {
            get {
                return getValueAsString(_Days);
            }
            set { setValue(_Days, value); }
        }
        public string doValidate_Days() {
            return doValidate_Days(U_Days);
        }
        public virtual string doValidate_Days(object oValue) {
            return base.doValidation(_Days, oValue);
        }

        /**
		 * Decription: Driver Comments
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DrvCmt
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _DrvCmt = "U_DrvCmt";
        public string U_DrvCmt {
            get {
                return getValueAsString(_DrvCmt);
            }
            set { setValue(_DrvCmt, value); }
        }
        public string doValidate_DrvCmt() {
            return doValidate_DrvCmt(U_DrvCmt);
        }
        public virtual string doValidate_DrvCmt(object oValue) {
            return base.doValidation(_DrvCmt, oValue);
        }

        /**
		 * Decription: Frequency Count
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FrqCount
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _FrqCount = "U_FrqCount";
        public string U_FrqCount {
            get {
                return getValueAsString(_FrqCount);
            }
            set { setValue(_FrqCount, value); }
        }
        public string doValidate_FrqCount() {
            return doValidate_FrqCount(U_FrqCount);
        }
        public virtual string doValidate_FrqCount(object oValue) {
            return base.doValidation(_FrqCount, oValue);
        }

        /**
		 * Decription: Frequency Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FrqTyp
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _FrqTyp = "U_FrqTyp";
        public string U_FrqTyp {
            get {
                return getValueAsString(_FrqTyp);
            }
            set { setValue(_FrqTyp, value); }
        }
        public string doValidate_FrqTyp() {
            return doValidate_FrqTyp(U_FrqTyp);
        }
        public virtual string doValidate_FrqTyp(object oValue) {
            return base.doValidation(_FrqTyp, oValue);
        }

        /**
		 * Decription: Job Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_JbType
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _JbType = "U_JbType";
        public string U_JbType {
            get {
                return getValueAsString(_JbType);
            }
            set { setValue(_JbType, value); }
        }
        public string doValidate_JbType() {
            return doValidate_JbType(U_JbType);
        }
        public virtual string doValidate_JbType(object oValue) {
            return base.doValidation(_JbType, oValue);
        }

        /**
		 * Decription: Period type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PERTYP
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _PERTYP = "U_PERTYP";
        public string U_PERTYP {
            get {
                return getValueAsString(_PERTYP);
            }
            set { setValue(_PERTYP, value); }
        }
        public string doValidate_PERTYP() {
            return doValidate_PERTYP(U_PERTYP);
        }
        public virtual string doValidate_PERTYP(object oValue) {
            return base.doValidation(_PERTYP, oValue);
        }

        /**
		 * Decription: Qty
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Qty
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Qty = "U_Qty";
        public string U_Qty {
            get {
                return getValueAsString(_Qty);
            }
            set { setValue(_Qty, value); }
        }
        public string doValidate_Qty() {
            return doValidate_Qty(U_Qty);
        }
        public virtual string doValidate_Qty(object oValue) {
            return base.doValidation(_Qty, oValue);
        }

        /**
		 * Decription: Rebate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Rebate
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Rebate = "U_Rebate";
        public string U_Rebate {
            get {
                return getValueAsString(_Rebate);
            }
            set { setValue(_Rebate, value); }
        }
        public string doValidate_Rebate() {
            return doValidate_Rebate(U_Rebate);
        }
        public virtual string doValidate_Rebate(object oValue) {
            return base.doValidation(_Rebate, oValue);
        }

        /**
		 * Decription: Stop PBI
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Removal
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Removal = "U_Removal";
        public string U_Removal {
            get {
                return getValueAsString(_Removal);
            }
            set { setValue(_Removal, value); }
        }
        public string doValidate_Removal() {
            return doValidate_Removal(U_Removal);
        }
        public virtual string doValidate_Removal(object oValue) {
            return base.doValidation(_Removal, oValue);
        }

        /**
		 * Decription: Route Comments
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RoutCmt
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _RoutCmt = "U_RoutCmt";
        public string U_RoutCmt {
            get {
                return getValueAsString(_RoutCmt);
            }
            set { setValue(_RoutCmt, value); }
        }
        public string doValidate_RoutCmt() {
            return doValidate_RoutCmt(U_RoutCmt);
        }
        public virtual string doValidate_RoutCmt(object oValue) {
            return base.doValidation(_RoutCmt, oValue);
        }

        /**
		 * Decription: Start Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SDate
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _SDate = "U_SDate";
        public string U_SDate {
            get {
                return getValueAsString(_SDate);
            }
            set { setValue(_SDate, value); }
        }
        public string doValidate_SDate() {
            return doValidate_SDate(U_SDate);
        }
        public virtual string doValidate_SDate(object oValue) {
            return base.doValidation(_SDate, oValue);
        }

        /**
		 * Decription: SupRef
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SupRef
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _SupRef = "U_SupRef";
        public string U_SupRef {
            get {
                return getValueAsString(_SupRef);
            }
            set { setValue(_SupRef, value); }
        }
        public string doValidate_SupRef() {
            return doValidate_SupRef(U_SupRef);
        }
        public virtual string doValidate_SupRef(object oValue) {
            return base.doValidation(_SupRef, oValue);
        }

        /**
		 * Decription: UOM
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UOM
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _UOM = "U_UOM";
        public string U_UOM {
            get {
                return getValueAsString(_UOM);
            }
            set { setValue(_UOM, value); }
        }
        public string doValidate_UOM() {
            return doValidate_UOM(U_UOM);
        }
        public virtual string doValidate_UOM(object oValue) {
            return base.doValidation(_UOM, oValue);
        }

        /**
		 * Decription: WTN From Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WTNFDt
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _WTNFDt = "U_WTNFDt";
        public string U_WTNFDt {
            get {
                return getValueAsString(_WTNFDt);
            }
            set { setValue(_WTNFDt, value); }
        }
        public string doValidate_WTNFDt() {
            return doValidate_WTNFDt(U_WTNFDt);
        }
        public virtual string doValidate_WTNFDt(object oValue) {
            return base.doValidation(_WTNFDt, oValue);
        }

        /**
		 * Decription: WTN To Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WTNTDt
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _WTNTDt = "U_WTNTDt";
        public string U_WTNTDt {
            get {
                return getValueAsString(_WTNTDt);
            }
            set { setValue(_WTNTDt, value); }
        }
        public string doValidate_WTNTDt() {
            return doValidate_WTNTDt(U_WTNTDt);
        }
        public virtual string doValidate_WTNTDt(object oValue) {
            return base.doValidation(_WTNTDt, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo() {
            if (moDBFields == null)
                moDBFields = new DBFields(22);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_Active, "Active", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Active
            moDBFields.Add(_AddPBI, "Add PBI", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Add PBI
            moDBFields.Add(_AmdBnVal, "Amendment Binary Value", 4, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Amendment Binary Value
            moDBFields.Add(_AmdDesc, "Amendment Group", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Amendment Group
            moDBFields.Add(_AutoCmp, "Auto Complete", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Auto Complete
            moDBFields.Add(_Days, "Days", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Days
            moDBFields.Add(_DrvCmt, "Driver Comments", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Driver Comments
            moDBFields.Add(_FrqCount, "Frequency Count", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Frequency Count
            moDBFields.Add(_FrqTyp, "Frequency Type", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Frequency Type
            moDBFields.Add(_JbType, "Job Type", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Job Type
            moDBFields.Add(_PERTYP, "Period type", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Period type
            moDBFields.Add(_Qty, "Qty", 13, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Qty
            moDBFields.Add(_Rebate, "Rebate", 14, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Rebate
            moDBFields.Add(_Removal, "Stop PBI", 15, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Stop PBI
            moDBFields.Add(_RoutCmt, "Route Comments", 16, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Route Comments
            moDBFields.Add(_SDate, "Start Date", 17, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Start Date
            moDBFields.Add(_SupRef, "SupRef", 18, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //SupRef
            moDBFields.Add(_UOM, "UOM", 19, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //UOM
            moDBFields.Add(_WTNFDt, "WTN From Date", 20, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //WTN From Date
            moDBFields.Add(_WTNTDt, "WTN To Date", 21, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //WTN To Date

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation() {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_Active());
            doBuildValidationString(doValidate_AddPBI());
            doBuildValidationString(doValidate_AmdBnVal());
            doBuildValidationString(doValidate_AmdDesc());
            doBuildValidationString(doValidate_AutoCmp());
            doBuildValidationString(doValidate_Days());
            doBuildValidationString(doValidate_DrvCmt());
            doBuildValidationString(doValidate_FrqCount());
            doBuildValidationString(doValidate_FrqTyp());
            doBuildValidationString(doValidate_JbType());
            doBuildValidationString(doValidate_PERTYP());
            doBuildValidationString(doValidate_Qty());
            doBuildValidationString(doValidate_Rebate());
            doBuildValidationString(doValidate_Removal());
            doBuildValidationString(doValidate_RoutCmt());
            doBuildValidationString(doValidate_SDate());
            doBuildValidationString(doValidate_SupRef());
            doBuildValidationString(doValidate_UOM());
            doBuildValidationString(doValidate_WTNFDt());
            doBuildValidationString(doValidate_WTNTDt());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_Active)) return doValidate_Active(oValue);
            if (sFieldName.Equals(_AddPBI)) return doValidate_AddPBI(oValue);
            if (sFieldName.Equals(_AmdBnVal)) return doValidate_AmdBnVal(oValue);
            if (sFieldName.Equals(_AmdDesc)) return doValidate_AmdDesc(oValue);
            if (sFieldName.Equals(_AutoCmp)) return doValidate_AutoCmp(oValue);
            if (sFieldName.Equals(_Days)) return doValidate_Days(oValue);
            if (sFieldName.Equals(_DrvCmt)) return doValidate_DrvCmt(oValue);
            if (sFieldName.Equals(_FrqCount)) return doValidate_FrqCount(oValue);
            if (sFieldName.Equals(_FrqTyp)) return doValidate_FrqTyp(oValue);
            if (sFieldName.Equals(_JbType)) return doValidate_JbType(oValue);
            if (sFieldName.Equals(_PERTYP)) return doValidate_PERTYP(oValue);
            if (sFieldName.Equals(_Qty)) return doValidate_Qty(oValue);
            if (sFieldName.Equals(_Rebate)) return doValidate_Rebate(oValue);
            if (sFieldName.Equals(_Removal)) return doValidate_Removal(oValue);
            if (sFieldName.Equals(_RoutCmt)) return doValidate_RoutCmt(oValue);
            if (sFieldName.Equals(_SDate)) return doValidate_SDate(oValue);
            if (sFieldName.Equals(_SupRef)) return doValidate_SupRef(oValue);
            if (sFieldName.Equals(_UOM)) return doValidate_UOM(oValue);
            if (sFieldName.Equals(_WTNFDt)) return doValidate_WTNFDt(oValue);
            if (sFieldName.Equals(_WTNTDt)) return doValidate_WTNTDt(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName) {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
		 * Link the Name Field to the Form Item.
		 */
        public void doLink_Name(string sControlName) {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
		 * Link the U_Active Field to the Form Item.
		 */
        public void doLink_Active(string sControlName) {
            moLinker.doLinkDataToControl(_Active, sControlName);
        }
        /**
		 * Link the U_AddPBI Field to the Form Item.
		 */
        public void doLink_AddPBI(string sControlName) {
            moLinker.doLinkDataToControl(_AddPBI, sControlName);
        }
        /**
		 * Link the U_AmdBnVal Field to the Form Item.
		 */
        public void doLink_AmdBnVal(string sControlName) {
            moLinker.doLinkDataToControl(_AmdBnVal, sControlName);
        }
        /**
		 * Link the U_AmdDesc Field to the Form Item.
		 */
        public void doLink_AmdDesc(string sControlName) {
            moLinker.doLinkDataToControl(_AmdDesc, sControlName);
        }
        /**
		 * Link the U_AutoCmp Field to the Form Item.
		 */
        public void doLink_AutoCmp(string sControlName) {
            moLinker.doLinkDataToControl(_AutoCmp, sControlName);
        }
        /**
		 * Link the U_Days Field to the Form Item.
		 */
        public void doLink_Days(string sControlName) {
            moLinker.doLinkDataToControl(_Days, sControlName);
        }
        /**
		 * Link the U_DrvCmt Field to the Form Item.
		 */
        public void doLink_DrvCmt(string sControlName) {
            moLinker.doLinkDataToControl(_DrvCmt, sControlName);
        }
        /**
		 * Link the U_FrqCount Field to the Form Item.
		 */
        public void doLink_FrqCount(string sControlName) {
            moLinker.doLinkDataToControl(_FrqCount, sControlName);
        }
        /**
		 * Link the U_FrqTyp Field to the Form Item.
		 */
        public void doLink_FrqTyp(string sControlName) {
            moLinker.doLinkDataToControl(_FrqTyp, sControlName);
        }
        /**
		 * Link the U_JbType Field to the Form Item.
		 */
        public void doLink_JbType(string sControlName) {
            moLinker.doLinkDataToControl(_JbType, sControlName);
        }
        /**
		 * Link the U_PERTYP Field to the Form Item.
		 */
        public void doLink_PERTYP(string sControlName) {
            moLinker.doLinkDataToControl(_PERTYP, sControlName);
        }
        /**
		 * Link the U_Qty Field to the Form Item.
		 */
        public void doLink_Qty(string sControlName) {
            moLinker.doLinkDataToControl(_Qty, sControlName);
        }
        /**
		 * Link the U_Rebate Field to the Form Item.
		 */
        public void doLink_Rebate(string sControlName) {
            moLinker.doLinkDataToControl(_Rebate, sControlName);
        }
        /**
		 * Link the U_Removal Field to the Form Item.
		 */
        public void doLink_Removal(string sControlName) {
            moLinker.doLinkDataToControl(_Removal, sControlName);
        }
        /**
		 * Link the U_RoutCmt Field to the Form Item.
		 */
        public void doLink_RoutCmt(string sControlName) {
            moLinker.doLinkDataToControl(_RoutCmt, sControlName);
        }
        /**
		 * Link the U_SDate Field to the Form Item.
		 */
        public void doLink_SDate(string sControlName) {
            moLinker.doLinkDataToControl(_SDate, sControlName);
        }
        /**
		 * Link the U_SupRef Field to the Form Item.
		 */
        public void doLink_SupRef(string sControlName) {
            moLinker.doLinkDataToControl(_SupRef, sControlName);
        }
        /**
		 * Link the U_UOM Field to the Form Item.
		 */
        public void doLink_UOM(string sControlName) {
            moLinker.doLinkDataToControl(_UOM, sControlName);
        }
        /**
		 * Link the U_WTNFDt Field to the Form Item.
		 */
        public void doLink_WTNFDt(string sControlName) {
            moLinker.doLinkDataToControl(_WTNFDt, sControlName);
        }
        /**
		 * Link the U_WTNTDt Field to the Form Item.
		 */
        public void doLink_WTNTDt(string sControlName) {
            moLinker.doLinkDataToControl(_WTNTDt, sControlName);
        }
        #endregion

    }
}
