/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 10/03/2016 11:27:15
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User{
   [Serializable] 
	public class IDH_APPRSTUP: com.isb.enq.dbObjects.Base.IDH_APPRSTUP{ 

		public IDH_APPRSTUP() : base() {
            msAutoNumKey = "IDHAPSTP";
		}

   	public IDH_APPRSTUP( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
        msAutoNumKey = "IDHAPSTP";
        }
	}
}
