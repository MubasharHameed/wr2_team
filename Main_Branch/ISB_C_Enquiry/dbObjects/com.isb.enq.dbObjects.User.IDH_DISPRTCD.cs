/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 18/03/2016 11:26:04
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.bridge.lookups;
using com.idh.bridge;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_DISPRTCD : com.isb.enq.dbObjects.Base.IDH_DISPRTCD {

        public IDH_DISPRTCD()
        : base() {
            msAutoNumKey = "IDHDSPRT";
        }

        public IDH_DISPRTCD(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oIDHForm, oForm) {
            msAutoNumKey = "IDHDSPRT";
        }

        string sDisposalRoutFormat = "";

        public override void doSetFieldHasChanged(int iRow, string sFieldname, object oOldValue, object oNewValue) {
            base.doSetFieldHasChanged(iRow, sFieldname, oOldValue, oNewValue);
            if (sDisposalRoutFormat == "") {
                sDisposalRoutFormat = Config.Parameter("DRTFRMAT", true).Trim();

            }
            if (sDisposalRoutFormat == string.Empty && (sFieldname == _TipCd || sFieldname == _TipNm || sFieldname == _DisTrtCd || sFieldname == _DisProcCd)) {
                if (!string.IsNullOrEmpty(U_TipCd) && U_TipCd != "*") {
                    string sBPDispCode = (string)idh.bridge.lookups.Config.INSTANCE.getValueFromBP(U_TipCd, "U_DispRtID");
                    if (sBPDispCode != null && sBPDispCode.Length < 3)
                        sBPDispCode = sBPDispCode.PadLeft(3, '0');
                    U_DisRCode = sBPDispCode + U_DisProcCd + U_DisTrtCd;
                }
            } else if (sDisposalRoutFormat != string.Empty && (sDisposalRoutFormat.Contains(sFieldname))) {
                string[] sFields = sDisposalRoutFormat.Split(',');
                string sDispRoutCode = "";
                for (int i = 0; i < sFields.Length; i++) {
                    if ((sFields[i].Contains(_TipCd)) && (!string.IsNullOrEmpty(U_TipCd) && U_TipCd != "*")) {
                        string[] sSubFields = sFields[i].Split('.');
                        if (sSubFields.Length == 3) {
                            string sBPDispCode = (string)idh.bridge.lookups.Config.INSTANCE.getValueFromBP(U_TipCd, sSubFields[1]);
                            if (sBPDispCode != null && sBPDispCode.Length < Convert.ToInt16(sSubFields[2]))
                                sBPDispCode = sBPDispCode.PadLeft(Convert.ToInt16(sSubFields[2]), '0');
                            sDispRoutCode += sBPDispCode;
                        } else if (sSubFields.Length == 2) {
                            string sBPDispCode = (string)idh.bridge.lookups.Config.INSTANCE.getValueFromBP(U_TipCd, sSubFields[1]);
                            if (sBPDispCode != null && sBPDispCode.Length < Convert.ToInt16(sSubFields[2]))
                                sBPDispCode = sBPDispCode.Trim();//.PadLeft(Convert.ToInt16(sSubFields[2]), '0');
                            sDispRoutCode += sBPDispCode;
                        }
                    } else if (sFields[i].Contains(".")) {
                        string[] sSubFields = sFields[i].Split('.');
                        string sValue = getValueAsString(sSubFields[0]);
                        if (!string.IsNullOrEmpty(sValue) && sValue != "*") {
                            if (sValue != null && sValue.Length < Convert.ToInt16(sSubFields[2]))
                                sValue = sValue.PadLeft(Convert.ToInt16(sSubFields[2]), '0');
                            if (!string.IsNullOrEmpty(sValue) && sValue != "*")
                                sDispRoutCode += sValue;
                        }
                    } else
                        sDispRoutCode += getValueAsString(sFields[i]);//sBPDispCode;

                }
                U_DisRCode = sDispRoutCode;
            }
            if (sFieldname == _InTipNm && (oNewValue == null || oNewValue.ToString() == "")) {
                U_InTipCd = "";
            } else if (sFieldname == _InTipCd && (oNewValue == null || oNewValue.ToString() == "")) {
                U_InTipNm = "";
            } else if (sFieldname == _TipCd && (oNewValue == null || oNewValue.ToString() == "")) {
                U_TipNm = "";
                U_TAddress = "";
                U_TZpCd = "";
                U_TAddrssLN = "";
            } else if (sFieldname == _WasCd && (oNewValue == null || oNewValue.ToString() == "")) {
                U_WasDsc = "";
            } else if (sFieldname == _ItemCd && (oNewValue == null || oNewValue.ToString() == "")) {
                U_ItemDsc = "";
            } else if ((sFieldname == _HaulgCost || sFieldname == _TipCost) && oNewValue != null && !string.IsNullOrEmpty(oNewValue.ToString().Trim())) {
                U_TotCost = U_HaulgCost + U_TipCost;
            } else if (sFieldname == _TAddress) {
                string sAddress = U_TAddress;
                if (sAddress.Trim() == "") {
                    U_TAddrssLN = "";
                } else {
                    string sCardCode = U_TipCd;
                    if (sCardCode.Trim() != "" && sAddress.Trim() != "") {
                        string sAddressAddrsCd = Convert.ToString(Config.INSTANCE.getValueFromBPShipToAddress(sCardCode, sAddress, "U_AddrsCd"));
                        if (sAddressAddrsCd == null)
                            sAddressAddrsCd = "";
                        U_TAddrssLN = sAddressAddrsCd;
                    } else {
                        U_TAddrssLN = "";
                    }
                }
            }
        }

        //public override void doLoadRowData() {
        //    base.doLoadRowData();
        //}
        public override bool doProcessData(bool bDoValidation, string sComment, bool bDoChildren) {
            doBookmark();
            bool bRet = false;
            first();
            while (next()) {
                if (isChangedRow()) {
                    doUpdateOITMDIspRout();
                    bRet = this.doUpdateDataRow();
                } else if (IsAddedRow()) {
                    doUpdateOITMDIspRout();
                    bRet = this.doAddDataRow();
                }
            }
            doRecallBookmark();

            return base.doProcessData(bDoValidation, sComment, bDoChildren);
        }

        private void doUpdateOITMDIspRout() {
            try {
                if (string.IsNullOrEmpty(U_WasCd))
                    return;
                if (DataHandler.INSTANCE.SBOCompany == null) {
                    string sUQry = "Update OITM set U_ROUTECD='" + U_DisRCode + "' WHERE ItemCode = '" + U_WasCd + "'";
                    DataHandler.INSTANCE.doUpdateQuery("Update disposal route code", sUQry);
                } else {
                    SAPbobsCOM.Items oItem = (SAPbobsCOM.Items)DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
                    if (oItem.GetByKey(U_WasCd) && oItem.UserFields.Fields.Item("U_ROUTECD").Value != U_DisRCode) {
                        oItem.UserFields.Fields.Item("U_ROUTECD").Value = U_DisRCode;
                        oItem.Update();
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Updating the Item Disposal Route: " + ex.Message, "ERSYUROW", new string[] { ex.Message });
            }
        }
    }
}
