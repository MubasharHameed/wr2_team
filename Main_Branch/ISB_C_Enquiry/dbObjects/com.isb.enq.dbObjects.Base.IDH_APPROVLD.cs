/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 11/03/2016 17:09:46
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base {
    [Serializable]
    public class IDH_APPROVLD : com.idh.dbObjects.DBBase {

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_APPROVLD()
            : base("@IDH_APPROVLD") {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_APPROVLD(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oForm, "@IDH_APPROVLD") {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
		* Table name
		*/
        public readonly static string TableName = "@IDH_APPROVLD";

        /**
         * Decription: Code
         * Mandatory: tYes
         * Name: Code
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Code = "Code";
        public string Code {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code() {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue) {
            return base.doValidation(_Code, oValue);
        }
        /**
         * Decription: Name
         * Mandatory: tYes
         * Name: Name
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Name = "Name";
        public string Name {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name() {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue) {
            return base.doValidation(_Name, oValue);
        }
        /**
         * Decription: Approval Header Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_APPHID
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _APPHID = "U_APPHID";
        public string U_APPHID {
            get {
                return getValueAsString(_APPHID);
            }
            set { setValue(_APPHID, value); }
        }
        public string doValidate_APPHID() {
            return doValidate_APPHID(U_APPHID);
        }
        public virtual string doValidate_APPHID(object oValue) {
            return base.doValidation(_APPHID, oValue);
        }

        /**
         * Decription: Approver Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ApprvNm
         * Size: 200
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ApprvNm = "U_ApprvNm";
        public string U_ApprvNm {
            get {
                return getValueAsString(_ApprvNm);
            }
            set { setValue(_ApprvNm, value); }
        }
        public string doValidate_ApprvNm() {
            return doValidate_ApprvNm(U_ApprvNm);
        }
        public virtual string doValidate_ApprvNm(object oValue) {
            return base.doValidation(_ApprvNm, oValue);
        }

        /**
         * Decription: Approver User ID
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ApprvUId
         * Size: 200
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ApprvUId = "U_ApprvUId";
        public string U_ApprvUId {
            get {
                return getValueAsString(_ApprvUId);
            }
            set { setValue(_ApprvUId, value); }
        }
        public string doValidate_ApprvUId() {
            return doValidate_ApprvUId(U_ApprvUId);
        }
        public virtual string doValidate_ApprvUId(object oValue) {
            return base.doValidation(_ApprvUId, oValue);
        }

        /**
         * Decription: Document number
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_DocNum
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _DocNum = "U_DocNum";
        public string U_DocNum {
            get {
                return getValueAsString(_DocNum);
            }
            set { setValue(_DocNum, value); }
        }
        public string doValidate_DocNum() {
            return doValidate_DocNum(U_DocNum);
        }
        public virtual string doValidate_DocNum(object oValue) {
            return base.doValidation(_DocNum, oValue);
        }

        /**
         * Decription: Form Type
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_FormTyp
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _FormTyp = "U_FormTyp";
        public string U_FormTyp {
            get {
                return getValueAsString(_FormTyp);
            }
            set { setValue(_FormTyp, value); }
        }
        public string doValidate_FormTyp() {
            return doValidate_FormTyp(U_FormTyp);
        }
        public virtual string doValidate_FormTyp(object oValue) {
            return base.doValidation(_FormTyp, oValue);
        }

        /**
         * Decription: Last Update Date
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_LstUpdtOn
         * Size: 8
         * Type: db_Date
         * CType: DateTime
         * SubType: st_None
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _LstUpdtOn = "U_LstUpdtOn";
        public DateTime U_LstUpdtOn {
            get {
                return getValueAsDateTime(_LstUpdtOn);
            }
            set { setValue(_LstUpdtOn, value); }
        }
        public void U_LstUpdtOn_AsString(string value) {
            setValue("U_LstUpdtOn", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_LstUpdtOn_AsString() {
            DateTime dVal = getValueAsDateTime(_LstUpdtOn);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_LstUpdtOn() {
            return doValidate_LstUpdtOn(U_LstUpdtOn);
        }
        public virtual string doValidate_LstUpdtOn(object oValue) {
            return base.doValidation(_LstUpdtOn, oValue);
        }

        /**
         * Decription: Remarks
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Remarks
         * Size: 10
         * Type: db_Memo
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Remarks = "U_Remarks";
        public string U_Remarks {
            get {
                return getValueAsString(_Remarks);
            }
            set { setValue(_Remarks, value); }
        }
        public string doValidate_Remarks() {
            return doValidate_Remarks(U_Remarks);
        }
        public virtual string doValidate_Remarks(object oValue) {
            return base.doValidation(_Remarks, oValue);
        }

        /**
         * Decription: Status
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Status
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Status = "U_Status";
        public string U_Status {
            get {
                return getValueAsString(_Status);
            }
            set { setValue(_Status, value); }
        }
        public string doValidate_Status() {
            return doValidate_Status(U_Status);
        }
        public virtual string doValidate_Status(object oValue) {
            return base.doValidation(_Status, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo() {
            if (moDBFields == null)
                moDBFields = new DBFields(10);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_APPHID, "Approval Header Code", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Approval Header Code
            moDBFields.Add(_ApprvNm, "Approver Name", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Approver Name
            moDBFields.Add(_ApprvUId, "Approver User ID", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Approver User ID
            moDBFields.Add(_DocNum, "Document number", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Document number
            moDBFields.Add(_FormTyp, "Form Type", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Form Type
            moDBFields.Add(_LstUpdtOn, "Last Update Date", 7, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Last Update Date
            moDBFields.Add(_Remarks, "Remarks", 8, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Remarks
            moDBFields.Add(_Status, "Status", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Status

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation() {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_APPHID());
            doBuildValidationString(doValidate_ApprvNm());
            doBuildValidationString(doValidate_ApprvUId());
            doBuildValidationString(doValidate_DocNum());
            doBuildValidationString(doValidate_FormTyp());
            doBuildValidationString(doValidate_LstUpdtOn());
            doBuildValidationString(doValidate_Remarks());
            doBuildValidationString(doValidate_Status());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_APPHID)) return doValidate_APPHID(oValue);
            if (sFieldName.Equals(_ApprvNm)) return doValidate_ApprvNm(oValue);
            if (sFieldName.Equals(_ApprvUId)) return doValidate_ApprvUId(oValue);
            if (sFieldName.Equals(_DocNum)) return doValidate_DocNum(oValue);
            if (sFieldName.Equals(_FormTyp)) return doValidate_FormTyp(oValue);
            if (sFieldName.Equals(_LstUpdtOn)) return doValidate_LstUpdtOn(oValue);
            if (sFieldName.Equals(_Remarks)) return doValidate_Remarks(oValue);
            if (sFieldName.Equals(_Status)) return doValidate_Status(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName) {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
         * Link the Name Field to the Form Item.
         */
        public void doLink_Name(string sControlName) {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
         * Link the U_APPHID Field to the Form Item.
         */
        public void doLink_APPHID(string sControlName) {
            moLinker.doLinkDataToControl(_APPHID, sControlName);
        }
        /**
         * Link the U_ApprvNm Field to the Form Item.
         */
        public void doLink_ApprvNm(string sControlName) {
            moLinker.doLinkDataToControl(_ApprvNm, sControlName);
        }
        /**
         * Link the U_ApprvUId Field to the Form Item.
         */
        public void doLink_ApprvUId(string sControlName) {
            moLinker.doLinkDataToControl(_ApprvUId, sControlName);
        }
        /**
         * Link the U_DocNum Field to the Form Item.
         */
        public void doLink_DocNum(string sControlName) {
            moLinker.doLinkDataToControl(_DocNum, sControlName);
        }
        /**
         * Link the U_FormTyp Field to the Form Item.
         */
        public void doLink_FormTyp(string sControlName) {
            moLinker.doLinkDataToControl(_FormTyp, sControlName);
        }
        /**
         * Link the U_LstUpdtOn Field to the Form Item.
         */
        public void doLink_LstUpdtOn(string sControlName) {
            moLinker.doLinkDataToControl(_LstUpdtOn, sControlName);
        }
        /**
         * Link the U_Remarks Field to the Form Item.
         */
        public void doLink_Remarks(string sControlName) {
            moLinker.doLinkDataToControl(_Remarks, sControlName);
        }
        /**
         * Link the U_Status Field to the Form Item.
         */
        public void doLink_Status(string sControlName) {
            moLinker.doLinkDataToControl(_Status, sControlName);
        }
        #endregion

    }
}
