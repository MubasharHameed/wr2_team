/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 28/09/2016 15:49:13
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.bridge;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_PVHBTH : com.isb.enq.dbObjects.Base.IDH_PVHBTH {

        public IDH_PVHBTH()
            : base() {
            msAutoNumKey = "SEQPVHBTH";
        }

        public IDH_PVHBTH(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oIDHForm, oForm) {
            msAutoNumKey = "SEQPVHBTH";
        }
        public static void doUpdateContainerQtyOfWORs(string sWOHCode) {

            string sFinalBatchCode = "";
            string sQry = "Select distinct " + IDH_JOBSHD._LABFBTCD + "  From [" + IDH_JOBSHD.TableName + "] With(nolock) Where Isnull(" + IDH_JOBSHD._LABFBTCD + ",'')<>''  And " + IDH_JOBSHD._JobNr + "=\'" + sWOHCode + "\' And isnull(" + IDH_JOBSHD._RowSta + ",'')<>'Deleted'  ";
            DataRecords oResult = null;
            oResult = DataHandler.INSTANCE.doBufferedSelectQuery("", sQry);
            if (oResult != null && oResult.RecordCount > 0) {
                for (int i = 0; i <= oResult.RecordCount - 1; i++) {
                    oResult.gotoRow(i);
                    sFinalBatchCode = (string)oResult.getValue(IDH_JOBSHD._LABFBTCD);
                    doUpdateWOR_ContainerQty(sFinalBatchCode);
                }
            }
        }
        public static void doUpdateWOR_ContainerQty(string sFinalBatchCd){
            double dCarWgt= 0;
            string sQry = "Select Sum(" + IDH_JOBSHD._CarWgt + ")  As CarWgt From [" + IDH_JOBSHD.TableName + "] With(nolock) Where Isnull(" + IDH_JOBSHD._LABFBTCD + ",'')<>''  And " + IDH_JOBSHD._LABFBTCD + "=\'" + sFinalBatchCd + "\' And isnull(" + IDH_JOBSHD._RowSta + ",'')<>'Deleted'  ";
            DataRecords oResult = null;
            IDH_PVHBTH oFinalBatch= new IDH_PVHBTH();
            oFinalBatch.UnLockTable = true;
            oResult = DataHandler.INSTANCE.doBufferedSelectQuery("", sQry);
            if (oResult != null && oResult.RecordCount > 0) {
                for (int i = 0; i <= oResult.RecordCount - 1; i++) {
                    oResult.gotoRow(i);
                    dCarWgt = (double)oResult.getValue("CarWgt");
                    //doUpdateContainerQty(sFinalBatchCd, dCarWgt);
                    if (oFinalBatch.getByKey(sFinalBatchCd)) {
                        oFinalBatch.U_WORCntQty = dCarWgt;
                        oFinalBatch.doProcessData();
                    }
                }
            }
        }

        //public static void doUpdateContainerQty(string sFinalBatchCd,double CarWgt) {
          
        //}
    }
}
