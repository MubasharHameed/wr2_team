﻿using System.Linq;
using System.Text;

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

//using com.idh.bridge;
//using com.idh.utils;
using com.idh.dbObjects.strct;
using com.idh.bridge.error;
using com.idh.bridge.lookups;

using com.idh.bridge.action;
using com.idh.bridge.data;

using com.idh.dbObjects.User.Shared;
using com.isb.enq.dbObjects.User;
using com.isb.enq.utils;

namespace com.isb.enq.dbObjects.User {
    public class IDH_JOBSHD : com.isb.dbObjects.User.IDH_JOBSHD {
        public IDH_JOBSHD() : base() {
        }

        public IDH_JOBSHD(com.idh.dbObjects.User.IDH_JOBENTR oParent) : base(oParent) {
        }

        public IDH_JOBSHD(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm, com.idh.dbObjects.User.IDH_JOBENTR oParent)
            : base(oIDHForm, oForm, oParent) { }

        public IDH_JOBSHD(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm, com.idh.dbObjects.User.IDH_JOBENTR oParent, bool bASSBODataTable)
            : base(oIDHForm, oForm, oParent, bASSBODataTable) {
        }

        public IDH_JOBSHD(System.Windows.Forms.Control oParentControl, com.idh.dbObjects.User.IDH_JOBENTR oParent)
            : base(oParentControl, oParent) {
        }

#region Lab Module
        private void doSetDisposalRoute() {
            try {
                if (!string.IsNullOrWhiteSpace(U_WasCd) && !string.IsNullOrWhiteSpace(U_ItemCd)) {
                    IDH_DISPRTCD oDispRtCode = new IDH_DISPRTCD();
                    int iRet = oDispRtCode.getData(IDH_DISPRTCD._WasCd + "=\'" + 
                        com.idh.utils.Formatter.doFixForSQL(U_WasCd.Trim()) + "\' And " + IDH_DISPRTCD._ItemCd + "=\'" + 
                        com.idh.utils.Formatter.doFixForSQL(U_ItemCd) + "\' And " + IDH_DISPRTCD._Active + "=\'Y\'", "");
                    if (iRet > 0) {
                        U_RtCdCode = oDispRtCode.Code;
                        U_RouteCd = oDispRtCode.U_DisRCode;
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                   new string[] { "doSetDisposalRoute" });
            }
        }

#endregion

        public override string getWharehouse(string sRouteCd, string sWasteCd, string sItemCd) {
            string sWH = "";
            IDH_DISPRTCD oDispRtCd = new IDH_DISPRTCD();
            int iRet = oDispRtCd.getData(IDH_DISPRTCD._DisRCode + "=\'" + sRouteCd + "\' And " + sWasteCd + "=\'" +
                com.idh.utils.Formatter.doFixForSQL(U_WasCd) + "\' AND " + sItemCd + "=\'" +
                com.idh.utils.Formatter.doFixForSQL(U_ItemCd) + "\' And " + IDH_DISPRTCD._Active + "=\'Y\'", "");
            if (iRet > 0) {
                if (oDispRtCd.U_WhsCode != null && oDispRtCd.U_WhsCode != string.Empty)
                    sWH = oDispRtCd.U_WhsCode;
            }
            return sWH;
        }

        public override string doDuplicateRow(int iSrcRow, bool bCopyComments, bool bCopyPrice, bool bCopyQty, string sJob, bool bClearHaulage, bool bSetStartDate, bool bCopyAdditems, bool bCopyVehReg, bool bCopyDriver, string sReqDate, bool bFromOptions) {
            string sNewCode = base.doDuplicateRow(iSrcRow, bCopyComments, bCopyPrice, bCopyQty, sJob, bClearHaulage, bSetStartDate, bCopyAdditems, bCopyVehReg, bCopyDriver, sReqDate, bFromOptions);

            if (sNewCode != null) {
                BlockChangeNotif = true;
                try {
                    if (U_WasCd != string.Empty && Config.INSTANCE.getParameterAsBool("CPSMRWOR", true))
                        AddLabTaskFlagIfRequired(U_WasCd);

                } catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDDDR", null);
                    sNewCode = null;
                } finally {
                    BlockChangeNotif = false;
                }
            }
            return sNewCode;
        }

        public override bool doPostAddUpdateProcess() {
            bool bResult = false;
            try {
                if (!com.idh.bridge.DataHandler.INSTANCE.IsInTransaction() && U_Status != FixedValues.getStatusCanceled() && !mbFromDelink)
                    bResult = doMarketingDocuments();
                doSaveSampleTask();
                doSentNCRAlert();
                doUpdateFinalBatchQty();
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBUP", null);
                bResult = false;
            } finally {
            }

            return bResult;
        }

        public override void doSetFieldHasChanged(int iRow, string sFieldName, object oOldValue, object oNewValue) {
            base.doSetFieldHasChanged(iRow, sFieldName, oOldValue, oNewValue);

            if (BlockChangeNotif)
                return;

            BlockChangeNotif = true;
            try {
                if (sFieldName.Equals(_ItemCd)) { //Container
                    if (!string.IsNullOrEmpty(U_ItemCd)) {
                        doSetDisposalRoute();
                    }
                } else if (sFieldName.Equals(_WasCd)) { //Waste Code
                    if (!string.IsNullOrEmpty(U_WasCd)) {

                        doSetDisposalRoute();

                        string sVal = oNewValue.ToString();
                        if (!string.IsNullOrWhiteSpace(sVal) && sVal != "*" && U_Sample != "Y")
                            AddLabTaskFlagIfRequired(oNewValue.ToString());

                    }
                } else if (sFieldName.Equals(_Sample) && (oOldValue != oNewValue)) {
                    if ((oOldValue == null || oOldValue.ToString() == "" || oOldValue.ToString() == "N") && oNewValue.ToString() == "Y" && U_SampleRef == string.Empty) {
                        //create sample ref
                        doCreateSampleTask(0);
                    } else if (((oOldValue == null || oOldValue.ToString() == "Y") && oNewValue.ToString() == "N" && U_SampleRef != string.Empty)) {
                        BlockChangeNotif = true;
                        IDH_LABTASK oLabTask = new IDH_LABTASK();
                        if (oLabTask.getByKey(U_SampleRef.Trim()) && (oLabTask.U_Saved == 1)) {
                            if (Handler_ExternalTriggerWithData_ShowMessage != null)
                                Handler_ExternalTriggerWithData_ShowMessage(this, "LAB00001");
                            U_Sample = "Y";
                            //send alert to make it cancel if they want from lab module
                        }
                        BlockChangeNotif = false;

                    }
                }
            } catch (Exception e) {
                throw new Exception("Field: [" + sFieldName + "] OValue: [" + oOldValue + "] NValue: [" + oNewValue + "] " + e.Message);
            } finally {
                BlockChangeNotif = false;
            }
        }

        public bool AddLabTaskFlagIfRequired(string sWasCode) {
            if (General.doCheckIfLabTaskRequired(sWasCode)) {
                BlockChangeNotif = true;
                try {
                    U_Sample = "Y";
                    doCreateSampleTask(0);
                } finally {
                    BlockChangeNotif = false;
                }
                return true;
            } else
                return false;
        }
        public static bool doCreateSampleTask(int iSave, string WasCd, string WasDsc, string WasFDsc, string UOM, int Quantity, string Comment,
            string sLineNum, string sDocNum, string sCardCode, string CardName, ref string sSampleRef, ref string sSampleStatus, string sDispRouteCode) {
            bool bResult = false;
            //string sSampleRef = "", sSampleStatus = "";
            bResult = General.doCreateLabItemNTask(WasCd,
                WasDsc,
                WasFDsc,
                UOM,
                Quantity.ToString(), Comment,
                sLineNum, sDocNum, "WOR",
                sCardCode, CardName, "", false, iSave, ref sSampleRef, ref sSampleStatus, "", sDispRouteCode);
            return bResult;
        }
        public bool doCreateSampleTask(int iSave) {
            try {
                BlockChangeNotif = true;
                string sSampleRef = "", sSampleStatus = "";
                if (doCreateSampleTask(iSave, U_WasCd, U_WasDsc, "", U_UOM, U_Quantity, U_Comment, Code, U_JobNr, U_CustCd,
                    U_CustNm, ref sSampleRef, ref sSampleStatus, U_RouteCd)) {
                    U_SampleRef = sSampleRef;
                    U_SampStatus = sSampleStatus;
                    return true;
                }
                U_Sample = "N";
                return false;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", null);
                return false;
            } finally {
                BlockChangeNotif = false;
            }
        }

        public void doSaveSampleTask() {
            if (U_Sample == "Y") {
                if (string.IsNullOrEmpty(U_SampleRef)) {
                    doCreateSampleTask(1);
                } else {
                    IDH_LABTASK oLabTask = new IDH_LABTASK();
                    oLabTask.UnLockTable = true;
                    if (oLabTask.getByKey(U_SampleRef.Trim()) && (oLabTask.U_Saved == 0)) {
                        oLabTask.U_Saved = 1;
                        string sUser = oLabTask.U_AssignedTo;
                        oLabTask.doProcessData();

                        IDH_LABITM oLabItem = new IDH_LABITM();
                        oLabItem.UnLockTable = true;
                        oLabItem.getByKey(oLabTask.U_LabItemRCd);
                        oLabItem.U_Saved = 1;
                        oLabItem.doProcessData();
                        if (!string.IsNullOrEmpty(sUser))
                            IDH_LABTASK.dosendAlerttoUser(sUser, com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTM1", null), com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTS1", null), false, U_SampleRef);
                    }
                }
            }
        }
        public void doUpdateFinalBatchQty() {
            try {
                if (U_LABFBTCD != null && U_LABFBTCD != string.Empty) {
                    IDH_PVHBTH.doUpdateWOR_ContainerQty(U_LABFBTCD);
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", null);
            } finally {
            }
        }
    }
}
