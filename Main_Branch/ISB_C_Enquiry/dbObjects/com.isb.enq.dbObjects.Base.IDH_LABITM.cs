/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 05/10/2016 17:29:08
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base {
    [Serializable]
    public class IDH_LABITM : com.idh.dbObjects.DBBase {

        private Linker moLinker = null;
        public Linker ControlLinker {
            get { return moLinker; }
            set { moLinker = value; }
        }

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_LABITM()
            : base("@IDH_LABITM") {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_LABITM(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oForm, "@IDH_LABITM") {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
		* Table name
		*/
        public readonly static string TableName = "@IDH_LABITM";

        /**
         * Decription: Code
         * Mandatory: tYes
         * Name: Code
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Code = "Code";
        public string Code {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code() {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue) {
            return base.doValidation(_Code, oValue);
        }
        /**
         * Decription: Name
         * Mandatory: tYes
         * Name: Name
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Name = "Name";
        public string Name {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name() {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue) {
            return base.doValidation(_Name, oValue);
        }
        /**
         * Decription: Container Qty
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CntnrQty
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Quantity
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _CntnrQty = "U_CntnrQty";
        public double U_CntnrQty {
            get {
                return getValueAsDouble(_CntnrQty);
            }
            set { setValue(_CntnrQty, value); }
        }
        public string doValidate_CntnrQty() {
            return doValidate_CntnrQty(U_CntnrQty);
        }
        public virtual string doValidate_CntnrQty(object oValue) {
            return base.doValidation(_CntnrQty, oValue);
        }

        /**
         * Decription: Comments
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Comments
         * Size: 10
         * Type: db_Memo
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Comments = "U_Comments";
        public string U_Comments {
            get {
                return getValueAsString(_Comments);
            }
            set { setValue(_Comments, value); }
        }
        public string doValidate_Comments() {
            return doValidate_Comments(U_Comments);
        }
        public virtual string doValidate_Comments(object oValue) {
            return base.doValidation(_Comments, oValue);
        }

        /**
         * Decription: Created By
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CreatedBy
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _CreatedBy = "U_CreatedBy";
        public string U_CreatedBy {
            get {
                return getValueAsString(_CreatedBy);
            }
            set { setValue(_CreatedBy, value); }
        }
        public string doValidate_CreatedBy() {
            return doValidate_CreatedBy(U_CreatedBy);
        }
        public virtual string doValidate_CreatedBy(object oValue) {
            return base.doValidation(_CreatedBy, oValue);
        }

        /**
         * Decription: Created On
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CreatedOn
         * Size: 8
         * Type: db_Date
         * CType: DateTime
         * SubType: st_None
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _CreatedOn = "U_CreatedOn";
        public DateTime U_CreatedOn {
            get {
                return getValueAsDateTime(_CreatedOn);
            }
            set { setValue(_CreatedOn, value); }
        }
        public void U_CreatedOn_AsString(string value) {
            setValue("U_CreatedOn", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_CreatedOn_AsString() {
            DateTime dVal = getValueAsDateTime(_CreatedOn);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_CreatedOn() {
            return doValidate_CreatedOn(U_CreatedOn);
        }
        public virtual string doValidate_CreatedOn(object oValue) {
            return base.doValidation(_CreatedOn, oValue);
        }

        /**
         * Decription: Goods Issue
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_GIssue
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _GIssue = "U_GIssue";
        public string U_GIssue {
            get {
                return getValueAsString(_GIssue);
            }
            set { setValue(_GIssue, value); }
        }
        public string doValidate_GIssue() {
            return doValidate_GIssue(U_GIssue);
        }
        public virtual string doValidate_GIssue(object oValue) {
            return base.doValidation(_GIssue, oValue);
        }

        /**
         * Decription: Goods Receipt
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_GReceipt
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _GReceipt = "U_GReceipt";
        public string U_GReceipt {
            get {
                return getValueAsString(_GReceipt);
            }
            set { setValue(_GReceipt, value); }
        }
        public string doValidate_GReceipt() {
            return doValidate_GReceipt(U_GReceipt);
        }
        public virtual string doValidate_GReceipt(object oValue) {
            return base.doValidation(_GReceipt, oValue);
        }

        /**
         * Decription: Item Master
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ItemCode
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ItemCode = "U_ItemCode";
        public string U_ItemCode {
            get {
                return getValueAsString(_ItemCode);
            }
            set { setValue(_ItemCode, value); }
        }
        public string doValidate_ItemCode() {
            return doValidate_ItemCode(U_ItemCode);
        }
        public virtual string doValidate_ItemCode(object oValue) {
            return base.doValidation(_ItemCode, oValue);
        }

        /**
         * Decription: Lab Task
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_LabTask
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _LabTask = "U_LabTask";
        public string U_LabTask {
            get {
                return getValueAsString(_LabTask);
            }
            set { setValue(_LabTask, value); }
        }
        public string doValidate_LabTask() {
            return doValidate_LabTask(U_LabTask);
        }
        public virtual string doValidate_LabTask(object oValue) {
            return base.doValidation(_LabTask, oValue);
        }

        /**
         * Decription: Last Update By
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_LastUpdateBy
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _LastUpdateBy = "U_LastUpdateBy";
        public string U_LastUpdateBy {
            get {
                return getValueAsString(_LastUpdateBy);
            }
            set { setValue(_LastUpdateBy, value); }
        }
        public string doValidate_LastUpdateBy() {
            return doValidate_LastUpdateBy(U_LastUpdateBy);
        }
        public virtual string doValidate_LastUpdateBy(object oValue) {
            return base.doValidation(_LastUpdateBy, oValue);
        }

        /**
         * Decription: Last Update On
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_LastUpdateOn
         * Size: 11
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _LastUpdateOn = "U_LastUpdateOn";
        public string U_LastUpdateOn {
            get {
                return getValueAsString(_LastUpdateOn);
            }
            set { setValue(_LastUpdateOn, value); }
        }
        public string doValidate_LastUpdateOn() {
            return doValidate_LastUpdateOn(U_LastUpdateOn);
        }
        public virtual string doValidate_LastUpdateOn(object oValue) {
            return base.doValidation(_LastUpdateOn, oValue);
        }

        /**
         * Decription: Lab Item Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_LICode
         * Size: 50
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _LICode = "U_LICode";
        public string U_LICode {
            get {
                return getValueAsString(_LICode);
            }
            set { setValue(_LICode, value); }
        }
        public string doValidate_LICode() {
            return doValidate_LICode(U_LICode);
        }
        public virtual string doValidate_LICode(object oValue) {
            return base.doValidation(_LICode, oValue);
        }

        /**
         * Decription: Lab Item Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_LIName
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _LIName = "U_LIName";
        public string U_LIName {
            get {
                return getValueAsString(_LIName);
            }
            set { setValue(_LIName, value); }
        }
        public string doValidate_LIName() {
            return doValidate_LIName(U_LIName);
        }
        public virtual string doValidate_LIName(object oValue) {
            return base.doValidation(_LIName, oValue);
        }

        /**
         * Decription: New BinLocation
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_NewBinLoc
         * Size: 228
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _NewBinLoc = "U_NewBinLoc";
        public string U_NewBinLoc {
            get {
                return getValueAsString(_NewBinLoc);
            }
            set { setValue(_NewBinLoc, value); }
        }
        public string doValidate_NewBinLoc() {
            return doValidate_NewBinLoc(U_NewBinLoc);
        }
        public virtual string doValidate_NewBinLoc(object oValue) {
            return base.doValidation(_NewBinLoc, oValue);
        }

        /**
         * Decription: New Warehouse
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_NewWhse
         * Size: 18
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _NewWhse = "U_NewWhse";
        public string U_NewWhse {
            get {
                return getValueAsString(_NewWhse);
            }
            set { setValue(_NewWhse, value); }
        }
        public string doValidate_NewWhse() {
            return doValidate_NewWhse(U_NewWhse);
        }
        public virtual string doValidate_NewWhse(object oValue) {
            return base.doValidation(_NewWhse, oValue);
        }

        /**
         * Decription: Object Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ObjectCd
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ObjectCd = "U_ObjectCd";
        public string U_ObjectCd {
            get {
                return getValueAsString(_ObjectCd);
            }
            set { setValue(_ObjectCd, value); }
        }
        public string doValidate_ObjectCd() {
            return doValidate_ObjectCd(U_ObjectCd);
        }
        public virtual string doValidate_ObjectCd(object oValue) {
            return base.doValidation(_ObjectCd, oValue);
        }

        /**
         * Decription: Object Type
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ObjectType
         * Size: 50
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ObjectType = "U_ObjectType";
        public string U_ObjectType {
            get {
                return getValueAsString(_ObjectType);
            }
            set { setValue(_ObjectType, value); }
        }
        public string doValidate_ObjectType() {
            return doValidate_ObjectType(U_ObjectType);
        }
        public virtual string doValidate_ObjectType(object oValue) {
            return base.doValidation(_ObjectType, oValue);
        }

        /**
         * Decription: Quantity
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Quantity
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Quantity
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Quantity = "U_Quantity";
        public double U_Quantity {
            get {
                return getValueAsDouble(_Quantity);
            }
            set { setValue(_Quantity, value); }
        }
        public string doValidate_Quantity() {
            return doValidate_Quantity(U_Quantity);
        }
        public virtual string doValidate_Quantity(object oValue) {
            return base.doValidation(_Quantity, oValue);
        }

        /**
         * Decription: Saved
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Saved
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Saved = "U_Saved";
        public int U_Saved {
            get {
                return getValueAsInt(_Saved);
            }
            set { setValue(_Saved, value); }
        }
        public string doValidate_Saved() {
            return doValidate_Saved(U_Saved);
        }
        public virtual string doValidate_Saved(object oValue) {
            return base.doValidation(_Saved, oValue);
        }

        /**
         * Decription: UOM
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_UOM
         * Size: 10
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _UOM = "U_UOM";
        public string U_UOM {
            get {
                return getValueAsString(_UOM);
            }
            set { setValue(_UOM, value); }
        }
        public string doValidate_UOM() {
            return doValidate_UOM(U_UOM);
        }
        public virtual string doValidate_UOM(object oValue) {
            return base.doValidation(_UOM, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo() {
            if (moDBFields == null)
                moDBFields = new DBFields(21);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_CntnrQty, "Container Qty", 2, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Container Qty
            moDBFields.Add(_Comments, "Comments", 3, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Comments
            moDBFields.Add(_CreatedBy, "Created By", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, false); //Created By
            moDBFields.Add(_CreatedOn, "Created On", 5, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Created On
            moDBFields.Add(_GIssue, "Goods Issue", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Goods Issue
            moDBFields.Add(_GReceipt, "Goods Receipt", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Goods Receipt
            moDBFields.Add(_ItemCode, "Item Master", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Item Master
            moDBFields.Add(_LabTask, "Lab Task", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Lab Task
            moDBFields.Add(_LastUpdateBy, "Last Update By", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, false); //Last Update By
            moDBFields.Add(_LastUpdateOn, "Last Update On", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 11, EMPTYSTR, false, false); //Last Update On
            moDBFields.Add(_LICode, "Lab Item Code", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Lab Item Code
            moDBFields.Add(_LIName, "Lab Item Name", 13, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Lab Item Name
            moDBFields.Add(_NewBinLoc, "New BinLocation", 14, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 228, EMPTYSTR, false, false); //New BinLocation
            moDBFields.Add(_NewWhse, "New Warehouse", 15, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 18, EMPTYSTR, false, false); //New Warehouse
            moDBFields.Add(_ObjectCd, "Object Code", 16, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Object Code
            moDBFields.Add(_ObjectType, "Object Type", 17, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Object Type
            moDBFields.Add(_Quantity, "Quantity", 18, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Quantity
            moDBFields.Add(_Saved, "Saved", 19, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Saved
            moDBFields.Add(_UOM, "UOM", 20, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //UOM

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation() {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_CntnrQty());
            doBuildValidationString(doValidate_Comments());
            doBuildValidationString(doValidate_CreatedBy());
            doBuildValidationString(doValidate_CreatedOn());
            doBuildValidationString(doValidate_GIssue());
            doBuildValidationString(doValidate_GReceipt());
            doBuildValidationString(doValidate_ItemCode());
            doBuildValidationString(doValidate_LabTask());
            doBuildValidationString(doValidate_LastUpdateBy());
            doBuildValidationString(doValidate_LastUpdateOn());
            doBuildValidationString(doValidate_LICode());
            doBuildValidationString(doValidate_LIName());
            doBuildValidationString(doValidate_NewBinLoc());
            doBuildValidationString(doValidate_NewWhse());
            doBuildValidationString(doValidate_ObjectCd());
            doBuildValidationString(doValidate_ObjectType());
            doBuildValidationString(doValidate_Quantity());
            doBuildValidationString(doValidate_Saved());
            doBuildValidationString(doValidate_UOM());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_CntnrQty)) return doValidate_CntnrQty(oValue);
            if (sFieldName.Equals(_Comments)) return doValidate_Comments(oValue);
            if (sFieldName.Equals(_CreatedBy)) return doValidate_CreatedBy(oValue);
            if (sFieldName.Equals(_CreatedOn)) return doValidate_CreatedOn(oValue);
            if (sFieldName.Equals(_GIssue)) return doValidate_GIssue(oValue);
            if (sFieldName.Equals(_GReceipt)) return doValidate_GReceipt(oValue);
            if (sFieldName.Equals(_ItemCode)) return doValidate_ItemCode(oValue);
            if (sFieldName.Equals(_LabTask)) return doValidate_LabTask(oValue);
            if (sFieldName.Equals(_LastUpdateBy)) return doValidate_LastUpdateBy(oValue);
            if (sFieldName.Equals(_LastUpdateOn)) return doValidate_LastUpdateOn(oValue);
            if (sFieldName.Equals(_LICode)) return doValidate_LICode(oValue);
            if (sFieldName.Equals(_LIName)) return doValidate_LIName(oValue);
            if (sFieldName.Equals(_NewBinLoc)) return doValidate_NewBinLoc(oValue);
            if (sFieldName.Equals(_NewWhse)) return doValidate_NewWhse(oValue);
            if (sFieldName.Equals(_ObjectCd)) return doValidate_ObjectCd(oValue);
            if (sFieldName.Equals(_ObjectType)) return doValidate_ObjectType(oValue);
            if (sFieldName.Equals(_Quantity)) return doValidate_Quantity(oValue);
            if (sFieldName.Equals(_Saved)) return doValidate_Saved(oValue);
            if (sFieldName.Equals(_UOM)) return doValidate_UOM(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName) {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
         * Link the Name Field to the Form Item.
         */
        public void doLink_Name(string sControlName) {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
         * Link the U_CntnrQty Field to the Form Item.
         */
        public void doLink_CntnrQty(string sControlName) {
            moLinker.doLinkDataToControl(_CntnrQty, sControlName);
        }
        /**
         * Link the U_Comments Field to the Form Item.
         */
        public void doLink_Comments(string sControlName) {
            moLinker.doLinkDataToControl(_Comments, sControlName);
        }
        /**
         * Link the U_CreatedBy Field to the Form Item.
         */
        public void doLink_CreatedBy(string sControlName) {
            moLinker.doLinkDataToControl(_CreatedBy, sControlName);
        }
        /**
         * Link the U_CreatedOn Field to the Form Item.
         */
        public void doLink_CreatedOn(string sControlName) {
            moLinker.doLinkDataToControl(_CreatedOn, sControlName);
        }
        /**
         * Link the U_GIssue Field to the Form Item.
         */
        public void doLink_GIssue(string sControlName) {
            moLinker.doLinkDataToControl(_GIssue, sControlName);
        }
        /**
         * Link the U_GReceipt Field to the Form Item.
         */
        public void doLink_GReceipt(string sControlName) {
            moLinker.doLinkDataToControl(_GReceipt, sControlName);
        }
        /**
         * Link the U_ItemCode Field to the Form Item.
         */
        public void doLink_ItemCode(string sControlName) {
            moLinker.doLinkDataToControl(_ItemCode, sControlName);
        }
        /**
         * Link the U_LabTask Field to the Form Item.
         */
        public void doLink_LabTask(string sControlName) {
            moLinker.doLinkDataToControl(_LabTask, sControlName);
        }
        /**
         * Link the U_LastUpdateBy Field to the Form Item.
         */
        public void doLink_LastUpdateBy(string sControlName) {
            moLinker.doLinkDataToControl(_LastUpdateBy, sControlName);
        }
        /**
         * Link the U_LastUpdateOn Field to the Form Item.
         */
        public void doLink_LastUpdateOn(string sControlName) {
            moLinker.doLinkDataToControl(_LastUpdateOn, sControlName);
        }
        /**
         * Link the U_LICode Field to the Form Item.
         */
        public void doLink_LICode(string sControlName) {
            moLinker.doLinkDataToControl(_LICode, sControlName);
        }
        /**
         * Link the U_LIName Field to the Form Item.
         */
        public void doLink_LIName(string sControlName) {
            moLinker.doLinkDataToControl(_LIName, sControlName);
        }
        /**
         * Link the U_NewBinLoc Field to the Form Item.
         */
        public void doLink_NewBinLoc(string sControlName) {
            moLinker.doLinkDataToControl(_NewBinLoc, sControlName);
        }
        /**
         * Link the U_NewWhse Field to the Form Item.
         */
        public void doLink_NewWhse(string sControlName) {
            moLinker.doLinkDataToControl(_NewWhse, sControlName);
        }
        /**
         * Link the U_ObjectCd Field to the Form Item.
         */
        public void doLink_ObjectCd(string sControlName) {
            moLinker.doLinkDataToControl(_ObjectCd, sControlName);
        }
        /**
         * Link the U_ObjectType Field to the Form Item.
         */
        public void doLink_ObjectType(string sControlName) {
            moLinker.doLinkDataToControl(_ObjectType, sControlName);
        }
        /**
         * Link the U_Quantity Field to the Form Item.
         */
        public void doLink_Quantity(string sControlName) {
            moLinker.doLinkDataToControl(_Quantity, sControlName);
        }
        /**
         * Link the U_Saved Field to the Form Item.
         */
        public void doLink_Saved(string sControlName) {
            moLinker.doLinkDataToControl(_Saved, sControlName);
        }
        /**
         * Link the U_UOM Field to the Form Item.
         */
        public void doLink_UOM(string sControlName) {
            moLinker.doLinkDataToControl(_UOM, sControlName);
        }
        #endregion

    }
}
