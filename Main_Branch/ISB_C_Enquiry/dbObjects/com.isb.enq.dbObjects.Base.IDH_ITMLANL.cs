/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 14/09/2016 16:37:35
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base{
   [Serializable]
	public class IDH_ITMLANL: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
  //     public Linker ControlLinker {
  //         get { return moLinker; }
  //         set { moLinker = value; }
  //     }

       private IDHAddOns.idh.forms.Base moIDHForm;
       public IDHAddOns.idh.forms.Base IDHForm {
           get { return moIDHForm; }
           set { moIDHForm = value; }
       }

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_ITMLANL() : base("@IDH_ITMLANL"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_ITMLANL( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_ITMLANL"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_ITMLANL";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Analysis Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnalysisCd
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AnalysisCd = "U_AnalysisCd";
		public string U_AnalysisCd { 
			get {
 				return getValueAsString(_AnalysisCd); 
			}
			set { setValue(_AnalysisCd, value); }
		}
           public string doValidate_AnalysisCd() {
               return doValidate_AnalysisCd(U_AnalysisCd);
           }
           public virtual string doValidate_AnalysisCd(object oValue) {
               return base.doValidation(_AnalysisCd, oValue);
           }

		/**
		 * Decription: Item Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItemCode
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ItemCode = "U_ItemCode";
		public string U_ItemCode { 
			get {
 				return getValueAsString(_ItemCode); 
			}
			set { setValue(_ItemCode, value); }
		}
           public string doValidate_ItemCode() {
               return doValidate_ItemCode(U_ItemCode);
           }
           public virtual string doValidate_ItemCode(object oValue) {
               return base.doValidation(_ItemCode, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(4);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_AnalysisCd, "Analysis Code", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Analysis Code
			moDBFields.Add(_ItemCode, "Item Code", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Item Code

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_AnalysisCd());
            doBuildValidationString(doValidate_ItemCode());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_AnalysisCd)) return doValidate_AnalysisCd(oValue);
            if (sFieldName.Equals(_ItemCode)) return doValidate_ItemCode(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_AnalysisCd Field to the Form Item.
		 */
		public void doLink_AnalysisCd(string sControlName){
			moLinker.doLinkDataToControl(_AnalysisCd, sControlName);
		}
		/**
		 * Link the U_ItemCode Field to the Form Item.
		 */
		public void doLink_ItemCode(string sControlName){
			moLinker.doLinkDataToControl(_ItemCode, sControlName);
		}
#endregion

	}
}
