/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 06/10/2015 17:49:53
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.bridge.lookups;
using com.idh.utils;
using com.idh.bridge;
using com.idh.dbObjects.numbers;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_WOQHD : com.isb.enq.dbObjects.Base.IDH_WOQHD {

        public enum en_WOQSTATUS {
            New = 1,
            WaitingForApproval = 2,
            ReviewAdvised = 3,
            Rejected = 4,
            Approved = 5,
            WOCreated = 6,
            Cancelled = 7,
            Closed = 8
        };

        private IDH_WOQITEM moWOQItems;
        public IDH_WOQITEM WOQItems {
            get { return moWOQItems; }
            //set { moEnquiryItems = value; }
        }

        public IDH_WOQHD()
            : base() {
            msAutoNumKey = "WOQUOTEH";
            //moWOQItems = new IDH_WOQITEM(this);
        }

        public IDH_WOQHD(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oIDHForm, oForm) {
            msAutoNumKey = "WOQUOTEH";
            //moWOQItems = new IDH_WOQITEM(this);

        }


        private bool mbCalculateTotals = false;
        public bool CalculateTotals {
            set {
                mbCalculateTotals = value;
            }
            get {
                return mbCalculateTotals;
            }
        }
        //public string AutoNumKey {
        //    get { return msAutoNumKey; }
        //    //set { moEnquiryItems = value; }
        //}
        /**
         * et_ExternalYNOption The external YN Option handler
         */

        public et_ExternalTriggerWithData Handler_ExternalTriggerWithData = null;
        //public et_ExternalTriggerWithDataAtRow Handler_ExternalTriggerWithData = null;
        public et_ExternalYNOption Handler_ExternalTriggerWithData_ShowYesNO = null;
        // public et_ExternalYNOption Handler_ExternalTriggerWithData_Show3Button = null;
        //  public et_ExternalTriggerWithData 

        public bool hasRows {
            get { return moWOQItems != null && moWOQItems.Count > 0; }
        }

        public IDH_WOQITEM getRowDB() {
            return moWOQItems;
        }


        private int miDoHandleChange = 0;
        public bool DoBlockUpdateTrigger {
            get { return miDoHandleChange > 0; }
            set { miDoHandleChange += (value ? 1 : -1); }
        }
        private bool mbDoNotSetDefault = false;
        public bool DoNotSetDefault {
            set {
                mbDoNotSetDefault = value;
            }
            get {
                return mbDoNotSetDefault;
            }
        }

        private bool mbAddressChanged = false;
        public bool DoAddressChanged {
            get { return mbAddressChanged; }
            set { mbAddressChanged = value; }
        }

        private bool mbFormHasDatatoSave = false;
        public bool FormHasDatatoSave {
            set {
                mbFormHasDatatoSave = value;
            }
            get {
                return mbFormHasDatatoSave;
            }
        }

        private bool moFromActivity=false;
        public bool FromActivity {
            set {
                moFromActivity = value;
            }
            get {
                return moFromActivity;
            }
        }
        //private string mbObligated = "";
        //public string Obligated {
        //    set {
        //        mbObligated = value;
        //    }
        //    get {
        //        return mbObligated;
        //    }
        //}
        public bool doFormHaveSomeData() {
            if (mbFormHasDatatoSave || U_CardCd != string.Empty || U_CardNM != string.Empty || U_Address != string.Empty || U_City != string.Empty || U_ZpCd != string.Empty) {
                return true;
            } else
                return false;
        }
        #region overrides

        public override void doRegisterChildren() {
            if (moWOQItems == null) {
                //if (ParentControl != null)
                //    moEnquiryItems = new IDH_ENQITEM(ParentControl, this);
                //else
                moWOQItems = new IDH_WOQITEM(this);
            }

            addChild("WOQItem", moWOQItems, _Code, IDH_WOQITEM._JobNr);
        }

        //public void doMarkUserUpdate(string sFieldName) {
        //    doMarkUserUpdate(sFieldName, null, getValue(sFieldName), false);
        //}
        //public void doMarkUserUpdate(string sFieldName, bool bDoPreSet) {
        //    doMarkUserUpdate(sFieldName, null, getValue(sFieldName), bDoPreSet);
        //}
        //private void doMarkUserUpdate(string sFieldName, object oOldValue, object oNewValue, bool bDoPreSet) {
        //    doSetFieldHasChanged(sFieldName, oOldValue, oNewValue);
        //}

        public override void doSetFieldHasChanged(int iRow, string sFieldname, object oOldValue, object oNewValue) {
            base.doSetFieldHasChanged(iRow, sFieldname, oOldValue, oNewValue);
            if (miDoHandleChange > 0)
                return;
            if (sFieldname == _CardNM) {
                if ((oNewValue == null || oNewValue.ToString().Trim() == string.Empty) && U_CardCd != string.Empty) {
                    miDoHandleChange++;
                    U_CardCd = "";
                    U_Address = "";
                    U_Street = "";
                    U_Block = "";
                    U_AddrssLN = "";
                    U_ZpCd = "";
                    U_City = "";
                    U_County = "";
                    U_Contact = "";
                    U_E_Mail = "";
                    U_Phone1 = "";
                    miDoHandleChange--;
                }
            } else if ((sFieldname == _CardCd || sFieldname == _Address || sFieldname == _ZpCd) ) {
                if (moWOQItems.Count > 0)
                moWOQItems.doRefreashAllRowPrices(false, true);
                if (sFieldname == _CardCd && oNewValue != null && oNewValue.ToString().Trim() != string.Empty) {
                    //mbObligated = (string)Config.INSTANCE.getValueFromBP(U_CardCd, OCRD_ReadOnly._IDHOBLGT);
                    //if (mbObligated == null)
                    //    mbObligated = "";
                    ArrayList oContactData = Config.INSTANCE.doGetBPContactEmployee(U_CardCd, "DEFAULTCONTACT");
                    if (oContactData != null) {
                        U_Contact = oContactData[0].ToString();
                        U_Phone1 = oContactData[4].ToString();
                        U_E_Mail = oContactData[7].ToString();
                    }
                    if (U_AddrssLN != "" || U_Address.Trim() == string.Empty) {
                        ArrayList oData = Config.INSTANCE.doGetAddress(U_CardCd, "S", null); //WR1_Search.idh.forms.search.AddressSearch.doGetAddress(, val, "S");
                        miDoHandleChange++;
                        if (oData != null && oData.Count > 0) {
                            U_Address = oData[0].ToString();
                            U_Street = oData[1].ToString();
                            U_Block = oData[2].ToString();
                            U_ZpCd = oData[3].ToString();
                            U_City = oData[4].ToString();
                            U_Country = oData[5].ToString();
                            U_County = oData[17].ToString();
                            U_AddrssLN = oData[30].ToString();
                        } else {
                            U_Address = "";
                            U_Street = "";
                            U_Block = "";
                            U_ZpCd = "";
                            U_City = "";
                            //U_Country = "";
                            U_County = "";
                            U_AddrssLN = "";
                        }
                        miDoHandleChange--;
                    }
                } else if (sFieldname == _CardCd && (oNewValue == null || oNewValue.ToString().Trim() == string.Empty)) {
                    miDoHandleChange++;
                    U_Address = "";
                    U_Street = "";
                    U_Block = "";
                    U_AddrssLN = "";
                    U_ZpCd = "";
                    U_City = "";
                    U_County = "";
                    U_Contact = "";
                    U_E_Mail = "";
                    U_Phone1 = "";
                    //mbObligated = "";
                    miDoHandleChange--;
                }
                //if (sFieldname == _CardCd) {
                //    moWOQItems.doBookmark();
                //    moWOQItems.first();
                //    moWOQItems.DoBlockUpdateTrigger = true;
                //    while (moWOQItems.next()) {
                //        moWOQItems.U_Obligated = mbObligated;
                //    }
                //    moWOQItems.DoBlockUpdateTrigger = false;
                //    moWOQItems.doRecallBookmark();
                //}
            } else if ((sFieldname == _Branch || sFieldname == _ItemGrp) && (oOldValue == null || oOldValue != oNewValue)) {
                moWOQItems.setValueForAllRows(sFieldname, oNewValue);
                //moWOQItems.doRefreashAllRowValueforField(sFieldname, oNewValue, false);
                moWOQItems.doRefreashAllRowPrices(false, true);
            } else if (sFieldname == _NoChgTax && moWOQItems.Count > 0 && (oOldValue == null || oOldValue != oNewValue)) {
                moWOQItems.doCalculateAllRowsTotal(true);
            } else if (sFieldname == _Address || sFieldname == _Street || sFieldname == _Block || sFieldname == _ZpCd || sFieldname == _City || sFieldname == _County
               || sFieldname == _Country) {
                mbAddressChanged = true;
            }
        }

        public override void doApplyDefaults() {
            if (mbDoNotSetDefault == true)
                return;
            base.doApplyDefaults();
            string UserID = com.idh.bridge.lookups.Config.INSTANCE.doGetUserId(idh.bridge.DataHandler.INSTANCE.User);
            doApplyDefaultValue(_User, UserID);

            doApplyDefaultValue(_Branch, Config.INSTANCE.doGetBranch(idh.bridge.DataHandler.INSTANCE.User));

            //NumbersPair oNextNum;
            //oNextNum = getNewKey();
            //Code = oNextNum.CodeNumber.ToString();
            //Name = oNextNum.NameNumber.ToString();





            //doApplyDefaultValue(_Code, Code);
            //doApplyDefaultValue(_Name, Name);
            doApplyDefaultValue(_Version, "1");







            DateTime oDate = DateTime.Now;
            doApplyDefaultValue(_BDate, idh.utils.Dates.doDateToSBODateStr(oDate));
            doApplyDefaultValue(_RSDate, idh.utils.Dates.doDateToSBODateStr(oDate));
            doApplyDefaultValue(_SpInst, "");
            doApplyDefaultValue(_Address, "");
            doApplyDefaultValue(_Block, "");
            doApplyDefaultValue(_CardNM, "");
            doApplyDefaultValue(_CardNM, "");
            doApplyDefaultValue(_City, "");
            doApplyDefaultValue(_ClgCode, 0);
            doApplyDefaultValue(_Contact, "");








            doApplyDefaultValue(_Country, com.idh.bridge.lookups.Config.INSTANCE.getParameterWithDefault("DFTCNTRY", "GB"));
            doApplyDefaultValue(_County, "");
            doApplyDefaultValue(_E_Mail, "");
            doApplyDefaultValue(_Notes, "");
            doApplyDefaultValue(_Phone1, "");
            doApplyDefaultValue(_State, "");





            doApplyDefaultValue(_Status, ((int)en_WOQSTATUS.New).ToString());
            //doApplyDefaultValue(_AppRequired, "N");
            dosetApprovalRequiredFlag();
            doApplyDefaultValue(_Street, "");
            doApplyDefaultValue(_ZpCd, "");
            doApplyDefaultValue(_LPGrossM, 0);
            doApplyDefaultValue(_LPProfit, 0);




            doApplyDefaultValue(_ItemGrp, Config.INSTANCE.getParameterWithDefault("WODCOG", ""));
            mbAddressChanged = false;
            mbFormHasDatatoSave = false;
            //mbObligated = "";
        }

        public override bool doUpdateDataRow(bool bDoValidation, string sComment, bool bDoChildren) {
            if (doPreAddUpdateRow()) {
                if (base.doUpdateDataRow(bDoValidation, sComment, bDoChildren)) {
                    doCreateActivity(true);
                    DoAddressChanged = false;
                    return doCreateLabTasks(false);
                } else
                    return false;
            } else
                return false;
        }
        
        public override bool doAddDataRow(bool bDoValidation, string sComment, bool bDoChildren) {
            if (doPreAddUpdateRow()) {
                if (!DataHandler.INSTANCE.IsInTransaction())
                    IDH_LABTASK.dosendAllAlerttoUser();
                if (base.doAddDataRow(bDoValidation, sComment, bDoChildren)) {
                    if (!moFromActivity) {
                        string msg = com.idh.bridge.resources.Messages.getGMessage("INFDOCNO", new string[] { "Enquiry", Code });
                        com.idh.bridge.DataHandler.INSTANCE.doInfo((msg));
                        doCreateActivity(false);
                    } else {
                        doCreateActivity(true);
                    }
                    DoAddressChanged = false;
                    return doCreateLabTasks(true); ;
                } else
                    return false;
            } else
                return false;
        }
        public bool doPreAddUpdateRow() {
            if (doAddUpdateAddress()) {
                return true;
            } else
                return false;
        }

        public bool doPostAddUpdateProcess() {
            bool doContinue = true;
            doContinue = doUpdateEnquiry();
            if (!doContinue) {
                return false;
            }
            if (DataHandler.INSTANCE.IsInTransaction()) {
                DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                DoAddressChanged = false;
                DataHandler.INSTANCE.doBufferedErrors();
            }
            if (!DataHandler.INSTANCE.IsInTransaction())
                IDH_LABTASK.dosendAllAlerttoUser();
            return doContinue;
        }
        public override bool doProcessData(bool bDoValidation, string sComment, bool bDoChildren) {
            if (this.CalculateTotals) {
                doCalculateAllRowsTotal(true);
            }
            bool doContinue = true;
            if (base.doProcessData(bDoValidation, sComment, bDoChildren)) {
                if (doPostAddUpdateProcess()) {
                    doContinue = true;
                    if (DataHandler.INSTANCE.IsInTransaction()) {
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                        DoAddressChanged = false;
                        DataHandler.INSTANCE.doBufferedErrors();
                    }
                    //return true;
                } else {
                    doContinue = false;
                }
            } else {
                doContinue = false;
            }
            if (!doContinue) {
                if (DataHandler.INSTANCE.IsInTransaction()) {
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    DoAddressChanged = false;
                    DataHandler.INSTANCE.doBufferedErrors();
                    IDH_LABTASK.MessagesToSent.Clear();
                }
            }
            return doContinue;
        }

        public bool doCreateLabTasks(bool bInAddMode) {
            WOQItems.doBookmark();
            bool bDoContinue = true;
            try {
                WOQItems.first();
                while (WOQItems.next()) {
                    //Start 
                    //Adding Lab Task for the WOQ Row 
                    if (bInAddMode) {

                        //Check if Sample Require is checked 
                        if (WOQItems.U_AdtnlItm != "A" && WOQItems.U_Sample == "Y") {
                            if (string.IsNullOrWhiteSpace(WOQItems.U_SampleRef)) {
                                bDoContinue = WOQItems.doCreateSampleTask(1, "");
                            } else {
                                WOQItems.doSaveSampleTask();
                            }
                        }
                    } else if (!bInAddMode && WOQItems.U_Sample == "Y") {
                        if (WOQItems.U_SampleRef.Trim() == string.Empty) {
                            bDoContinue = WOQItems.doCreateSampleTask(1, "");
                        } else {
                            WOQItems.doSaveSampleTask();
                        }
                    }
                    if (string.IsNullOrWhiteSpace(WOQItems.U_WasFDsc) && string.IsNullOrWhiteSpace(WOQItems.U_WasDsc) && string.IsNullOrWhiteSpace(WOQItems.U_WasCd)) {
                        continue;
                    }
                }
                return bDoContinue;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("docreateLabTasks") });
                return false;
            } finally {
                WOQItems.doRecallBookmark();
            }
        }

        public bool doUpdateEnquiry() {
            bool bDoContinue = true;
            IDH_ENQITEM moEnquiryItem = new IDH_ENQITEM();

            WOQItems.doBookmark();
            WOQItems.first();

            while (WOQItems.next())
            {
                moEnquiryItem.DoNotSetDefault = true;
                moEnquiryItem.DoNotSetParent = true;
                if (string.IsNullOrWhiteSpace( WOQItems.U_EnqLID ))
                    continue;
                if (moEnquiryItem.getByKey(WOQItems.U_EnqLID)) {
                    moEnquiryItem.U_Status = "2";
                    moEnquiryItem.U_WOQID = WOQItems.U_JobNr;
                    moEnquiryItem.U_WstGpCd = WOQItems.U_WstGpCd;
                    moEnquiryItem.U_WstGpNm = WOQItems.U_WstGpNm;
                    moEnquiryItem.U_WOQLineID = WOQItems.Code;
                    bDoContinue = moEnquiryItem.doUpdateDataRow("Linked with WOQ:" + WOQItems.U_JobNr + "." + WOQItems.Code);
                    if (bDoContinue == false) {
                        break;
                    }
                }
            }
            return bDoContinue;
        }

        #region Validations
        public override string doValidate_CardNM(object oValue) {
            if (oValue ==null || string.IsNullOrWhiteSpace((string)oValue)) {
                return Translation.getTranslatedMessage("ERVLDFLD", "Error: Business Partner is missing.");
            }
            return base.doValidate_CardNM(oValue);
        }
        public override string doValidate_Address(object oValue) {
            if (oValue == null || string.IsNullOrWhiteSpace((string)oValue)) {
                return Translation.getTranslatedMessage("ERVLDFLD", "Error: Address is missing.");
            }
            return base.doValidate_Address(oValue);
        }
        public override string doValidate_ZpCd(object oValue) {
            if (oValue == null || string.IsNullOrWhiteSpace((string)oValue)) {
                return Translation.getTranslatedMessage("ERVLDFLD", "Error: Postcode is missing.");
            }
            return base.doValidate_ZpCd(oValue);
        }
        public override string doValidate_City(object oValue) {
            if (oValue == null || string.IsNullOrWhiteSpace((string)oValue)) {
                return Translation.getTranslatedMessage("ERVLDFLD", "Error: City is missing.");
            }
            return base.doValidate_City(oValue);
        }
        public override string doValidate_Country(object oValue) {
            if (oValue == null || string.IsNullOrWhiteSpace((string)oValue)) {
                return Translation.getTranslatedMessage("ERVLDFLD", "Error: Country is missing.");
            }
            return base.doValidate_Country(oValue);
        }
        public override string doValidate_Contact(object oValue) {
            if (oValue == null || string.IsNullOrWhiteSpace((string)oValue)) {
                return Translation.getTranslatedMessage("ERVLDFLD", "Error: Contact person is missing.");
            }
            return base.doValidate_Contact(oValue);
        }
        public override string doValidate_Phone1(object oValue) {
            if ((oValue == null || string.IsNullOrWhiteSpace((string)oValue)) || (U_E_Mail == null && string.IsNullOrWhiteSpace(U_E_Mail))) {
                return Translation.getTranslatedMessage("ERVLDFLD", "Error: Phone number is missing.");
            }
            return base.doValidate_Phone1(oValue);
        }
        public override string doValidate_E_Mail(object oValue) {
            if ((oValue == null || string.IsNullOrWhiteSpace((string)oValue)) && (U_Phone1 == null || string.IsNullOrWhiteSpace(U_Phone1))) {
                return Translation.getTranslatedMessage("ERVLDFLD", "Error: E-mail address is missing.");
            }
            return base.doValidate_E_Mail(oValue);
        }
        #endregion
        #endregion
        #region rowLoader

        public override void doLoadRowData(bool bLoadChildren) {
            base.doLoadRowData(bLoadChildren);
            mbAddressChanged = false;
            mbFormHasDatatoSave = false;
        }
        public override void doClearBuffers() {
            base.doClearBuffers();
            //if (MustLoadChildren)
            //    moWOQItems.doClearBuffers();
            mbAddressChanged = false;
            mbFormHasDatatoSave = false;
        }
        public override void doLoadChildren() {
            if (Code != null && Code.Length > 0) {
                if (U_Status == ((int)en_WOQSTATUS.Approved).ToString() ||
                    U_Status == ((int)en_WOQSTATUS.Cancelled).ToString() ||
                    U_Status == ((int)en_WOQSTATUS.Closed).ToString() ||
                    U_Status == ((int)en_WOQSTATUS.Rejected).ToString() ||
                    U_Status == ((int)en_WOQSTATUS.WaitingForApproval).ToString() ||
                    U_Status == ((int)en_WOQSTATUS.WOCreated).ToString()
                    )
                    moWOQItems.AutoAddEmptyLine = false;
                else
                    moWOQItems.AutoAddEmptyLine = true;
                base.doLoadChildren();
                //moWOQItems.getByParentNumber(Code);
            } else {
                moWOQItems.doClearBuffers();
            }
        }
        #endregion
        #region "Duplication"
        ///// <summary>
        ///// Use this to call from SBO form
        ///// </summary>
        ///// <returns></returns>
        //public bool duplicateDocument() {
        //    try {

        //        string UserID = com.idh.bridge.lookups.Config.INSTANCE.doGetUserId(idh.bridge.DataHandler.INSTANCE.User);

        //        mbDoNotSetDefault = true;
        //        miDoHandleChange++;

        //        IDH_WOQITEM _WOQItems = new IDH_WOQITEM();
        //        _WOQItems.DoNotSetDefault = true;
        //        _WOQItems.DoNotSetParent = true;
        //        _WOQItems.UnLockTable = true;
        //        _WOQItems.getByParentNumber(Code);

        //        DataHandler.NextNumbers oNextNum;
        //        oNextNum = getNewKey();
        //        Code = oNextNum.CodeNumber.ToString();
        //        Name = oNextNum.NameNumber.ToString();
        //        setDefaultValue(_Version, "1");
        //        DateTime oDate = DateTime.Now;
        //        U_BDate = oDate;
        //        U_RSDate = oDate;
        //        U_Status = ((int)en_WOQSTATUS.New).ToString();
        //        U_ClgCode = 0;
        //        U_WOHID = "";
        //        WOQItems.first();
        //        string sParentRowID = "";
        //        int sParentRowIndex = 0;

        //        //while (WOQItems.next()) {
        //        //    //moWOQItems.dod
        //        //    moWOQItems.DoBlockUpdateTrigger = true;
        //        //    DataHandler.NextNumbers oWOQRowNextNum = moWOQItems.getNewKey();
        //        //    //moWOQItems.doClearBuffers();

        //        //    if (moWOQItems.U_AdtnlItm == string.Empty) {
        //        //        sParentRowID = moWOQItems.Code;
        //        //        sParentRowIndex = moWOQItems.CurrentRowIndex;
        //        //    } else if (moWOQItems.U_AdtnlItm == "A" && moWOQItems.U_PLineID != string.Empty) {
        //        //        moWOQItems.U_PLineID = sParentRowID;
        //        //    }
        //        //    moWOQItems.Code = oWOQRowNextNum.CodeCode;
        //        //    moWOQItems.Name = oWOQRowNextNum.NameCode;
        //        //    moWOQItems.U_Status = ((int)IDH_WOQITEM.en_WOQITMSTATUS.New).ToString();
        //        //    moWOQItems.U_JobNr = Code;
        //        //    moWOQItems.U_WOHID = "";
        //        //    moWOQItems.U_WORID = "";
        //        //    moWOQItems.U_SampleRef = "";
        //        //    moWOQItems.U_SampStatus = "";
        //        //    string sSampleVal = moWOQItems.U_Sample;
        //        //    if (sSampleVal == "Y") {
        //        //        if (!moWOQItems.AddLabTaskFlagIfRequired(moWOQItems.U_WasCd, null)) {
        //        //            moWOQItems.doCreateSampleTask(0);
        //        //        }
        //        //    }
        //        //    moWOQItems.DoBlockUpdateTrigger = false;
        //        //}
        //        if (MustLoadChildren) {
        //            moWOQItems.doClearBuffers();
        //            while (_WOQItems.next()) {
        //                moWOQItems.DoNotSetDefault = true;
        //                moWOQItems.doAddEmptyRow();
        //                _WOQItems.doDuplicateRow(ref moWOQItems, Code);

        //                //moWOQItems.U_WasFDsc = "Demineralised Water";
        //                //moWOQItems.U_WasDsc = "Demineralised WaterF";
        //                //_WOQItems.U_WasDsc = "Demineralised WaterF";
        //                //_WOQItems.U_WasDsc  = "Demineralised WaterFF4";
        //                //int w = moWOQItems.getFieldInfo(IDH_WOQITEM._WasDsc).Width;
        //                //moWOQItems.U_WasDsc = "Demineralised WaterFF";
        //                //moWOQItems.U_WasFDsc = "FFDemineralisedWater";
        //                //moWOQItems.U_WasFDsc = _WOQItems.U_WasFDsc;

        //                moWOQItems.DoBlockUpdateTrigger = true;
        //                if (WOQItems.U_AdtnlItm == string.Empty) {
        //                    sParentRowID = WOQItems.Code;
        //                    sParentRowIndex = _WOQItems.CurrentRowIndex;
        //                } else if (WOQItems.U_AdtnlItm == "A" && WOQItems.U_PLineID != string.Empty) {
        //                    moWOQItems.U_PLineID = sParentRowID;
        //                }
        //                moWOQItems.U_Sort = _WOQItems.U_Sort;
        //                moWOQItems.DoNotSetDefault = false;
        //                moWOQItems.DoBlockUpdateTrigger = false;

        //            }
        //        }
        //        DoBlockUpdateTrigger = false;
        //        mbDoNotSetDefault = false;

        //        return true;

        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doDuplication") });
        //    } finally {
        //        mbDoNotSetDefault = false;
        //        miDoHandleChange--;
        //    }
        //    return true;
        //}
        /// <summary>
        /// Use this to call from SBO form
        /// </summary>
        /// <returns></returns>
        public bool duplicateDocument(string sCode) {
            mbDoNotSetDefault = true;
            miDoHandleChange++;
            try {
                string SourceDocCode = sCode;
                string UserID = com.idh.bridge.lookups.Config.INSTANCE.doGetUserId(idh.bridge.DataHandler.INSTANCE.User);
                IDH_WOQHD _WOQ = new IDH_WOQHD();
                _WOQ.UnLockTable = true;
                _WOQ.MustLoadChildren = true;
                _WOQ.getByKey(sCode);

                for (int i = 0; i <= this.moDBFields.Count - 1; i++) {
                    this.setValue(_WOQ.getFieldInfo(i).FieldName, _WOQ.getValue(_WOQ.getFieldInfo(i).FieldName));
                }
                //for (int icol = 0; icol < this.getColumnCount(); icol++) {
                //    this.setValue(_WOQ.getFieldInfo(icol).FieldName, this.getValue(_WOQ.getFieldInfo(icol).FieldName));
                //}
                //IDH_WOQITEM _WOQItems = new IDH_WOQITEM();
                //_WOQItems.DoNotSetDefault = true;
                //_WOQItems.DoNotSetParent = true;
                //_WOQItems.UnLockTable = true;
                //_WOQItems.getByParentNumber(Code);

                NumbersPair oNextNum;
                oNextNum = getNewKey();
                Code = oNextNum.CodeNumber.ToString();
                Name = oNextNum.NameNumber.ToString();
                doApplyDefaultValue(_Version, "1");

                DateTime oDate = DateTime.Now;
                U_BDate = oDate;
                U_RSDate = oDate;
                U_Status = ((int)en_WOQSTATUS.New).ToString();
                U_ClgCode = 0;
                U_WOHID = "";
                U_OrgDocNm = SourceDocCode;
                _WOQ.moWOQItems.first();
                string sParentRowID = "";
                int sParentRowIndex = 0;
                if (MustLoadChildren) {
                    while (_WOQ.moWOQItems.next()) {
                        moWOQItems.DoNotSetDefault = true;
                        moWOQItems.doAddEmptyRow();
                       _WOQ.moWOQItems.doDuplicateRow(ref moWOQItems, Code);
                        moWOQItems.U_Sort =_WOQ.moWOQItems.U_Sort;
                        moWOQItems.DoBlockUpdateTrigger = true;
                        if (string.IsNullOrWhiteSpace( moWOQItems.U_AdtnlItm )) {
                            sParentRowID = moWOQItems.Code;
                            sParentRowIndex = moWOQItems.CurrentRowIndex;
                        } else if (moWOQItems.U_AdtnlItm == "A" && moWOQItems.U_PLineID != string.Empty) {
                            moWOQItems.U_PLineID = sParentRowID;
                        }
                        moWOQItems.DoBlockUpdateTrigger = false;
                    }
                    //while (_WOQ.moWOQItems.next()) {
                    //    //moWOQItems.dod
                    //    moWOQItems.doAddEmptyRow(true);
                    //    moWOQItems.DoBlockUpdateTrigger = true;

                    //    for (int i = 0; i <= _WOQ.moWOQItems.DBFields.Count - 1; i++) {
                    //        try {
                    //            moWOQItems.setValue(_WOQ.moWOQItems.getFieldInfo(i).FieldName, _WOQ.moWOQItems.getValue(_WOQ.moWOQItems.getFieldInfo(i).FieldName));
                    //        }catch(Exception ex) {
                    //            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doDuplication") });
                    //        }
                    //    }
                    //    NumbersPair oWOQRowNextNum = moWOQItems.getNewKey();
                    //    //moWOQItems.doClearBuffers();
                    //    moWOQItems.Code = oWOQRowNextNum.CodeCode;
                    //    moWOQItems.Name = oWOQRowNextNum.NameCode;

                    //    if (moWOQItems.U_AdtnlItm == string.Empty) {
                    //        sParentRowID = moWOQItems.Code;
                    //        sParentRowIndex = moWOQItems.CurrentRowIndex;
                    //    } else if (moWOQItems.U_AdtnlItm == "A" && moWOQItems.U_PLineID != string.Empty) {
                    //        moWOQItems.U_PLineID = sParentRowID;
                    //    }
                    //    moWOQItems.U_Status = ((int)IDH_WOQITEM.en_WOQITMSTATUS.New).ToString();

                    //    string sSampleVal = moWOQItems.U_Sample;
                    //    string sSampleRefNum = moWOQItems.U_SampleRef;
                    //    string sSampleStatus = moWOQItems.U_SampStatus;
                    //    moWOQItems.U_JobNr = Code;
                    //    moWOQItems.U_WOHID = "";
                    //    moWOQItems.U_WORID = "";
                    //    moWOQItems.U_SampleRef = "";
                    //    moWOQItems.U_SampStatus = "";
                    //    moWOQItems.U_Sample = "";

                    //    if (sSampleVal == "Y" && Config.INSTANCE.getParameterAsBool("CPSMPDUP", true)) {
                    //        if (com.isb.enq.utils.General.doCheckIfLabTaskRequired(moWOQItems.U_WasCd.ToString())) {
                    //            string[] sParm = { sSampleRefNum, moWOQItems.U_WasCd == "" ? moWOQItems.U_WasDsc : moWOQItems.U_WasCd };
                    //            if (sSampleRefNum != string.Empty && Handler_ExternalTriggerWithData_ShowYesNO != null) {
                    //                int iMsgRet = (Handler_ExternalTriggerWithData_ShowYesNO(this, "ERVEQ019", sParm, 3));
                    //                if (iMsgRet == 1) {
                    //                    moWOQItems.BlockChangeNotif = true;
                    //                    moWOQItems.U_Sample = "Y";
                    //                    moWOQItems.U_SampleRef = sSampleRefNum;
                    //                    moWOQItems.U_SampStatus = "Accepted";
                    //                    moWOQItems.BlockChangeNotif = false;
                    //                } else if (iMsgRet == 2) {
                    //                    if (moWOQItems.AddLabTaskFlagIfRequired(moWOQItems.U_WasCd)) {
                    //                        moWOQItems.doCreateSampleTask(0, sSampleRefNum);
                    //                    }
                    //                } else if (moWOQItems.AddLabTaskFlagIfRequired(moWOQItems.U_WasCd)) {
                    //                    moWOQItems.doCreateSampleTask(0, "");
                    //                }
                    //                //if (Handler_ExternalTriggerWithData_ShowYesNO(this, "ERVEQ019", null, 1) == 1) {
                    //                //} else if (moWOQItems.AddLabTaskFlagIfRequired(moWOQItems.U_WasCd, null)) {
                    //                //    moWOQItems.doCreateSampleTask(0);
                    //                //}
                    //            } else if (moWOQItems.AddLabTaskFlagIfRequired(moWOQItems.U_WasCd)) {
                    //                moWOQItems.doCreateSampleTask(0, "");
                    //            }
                    //        }
                    //        //if (moWOQItems.AddLabTaskFlagIfRequired(moWOQItems.U_WasCd, null)) {
                    //        //    moWOQItems.doCreateSampleTask(0);
                    //        //}
                    //    }
                    //    moWOQItems.DoBlockUpdateTrigger = false;
                    //}
                }
                
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doDuplication") });
            } finally {
                mbDoNotSetDefault = false;
                miDoHandleChange--;
            }
            return true;
        }
        /// <summary>
        /// Duplicate the current object and will return a new object. Use in NON-SBO environment
        /// </summary>
        /// <returns></returns>
        public IDH_WOQHD duplicateObject() {
            try {
                DataHandler.INSTANCE.StartTransaction();
                IDH_WOQHD _moWOQ = new IDH_WOQHD();
                _moWOQ.DoBlockUpdateTrigger = true;
                _moWOQ.mbDoNotSetDefault = true;
                for (int icol = 0; icol < this.getColumnCount(); icol++) {
                    _moWOQ.setValue(_moWOQ.getFieldInfo(icol).FieldName, this.getValue(_moWOQ.getFieldInfo(icol).FieldName));
                }

                NumbersPair oNextNum;
                oNextNum = _moWOQ.getNewKey();
                _moWOQ.Code = oNextNum.CodeNumber.ToString();
                _moWOQ.Name = oNextNum.NameNumber.ToString();
                _moWOQ.U_Version = 1;
                _moWOQ.U_ClgCode = 0;
                DateTime oDate = DateTime.Now;
                _moWOQ.U_BDate = oDate;
                _moWOQ.U_RSDate = oDate;
                _moWOQ.U_Status = ((int)en_WOQSTATUS.New).ToString();
                _moWOQ.U_WOHID = "";
                _moWOQ.U_OrgDocNm = Code;


                _moWOQ.MustLoadChildren = true;
                moWOQItems.doBookmark();
                moWOQItems.first();
                string sParentRowID = "";
                int sParentRowIndex = 0;
                while (moWOQItems.next()) {
                    _moWOQ.moWOQItems.DoNotSetDefault = true;
                    _moWOQ.moWOQItems.doAddEmptyRow();
                    moWOQItems.doDuplicateRow(ref _moWOQ.moWOQItems, _moWOQ.Code);
                    _moWOQ.moWOQItems.U_Sort = moWOQItems.U_Sort;
                    _moWOQ.moWOQItems.DoBlockUpdateTrigger = true;
                    if (_moWOQ.moWOQItems.U_AdtnlItm == string.Empty) {
                        sParentRowID = _moWOQ.moWOQItems.Code;
                        sParentRowIndex = _moWOQ.moWOQItems.CurrentRowIndex;
                    } else if (_moWOQ.moWOQItems.U_AdtnlItm == "A" && _moWOQ.moWOQItems.U_PLineID != string.Empty) {
                        _moWOQ.moWOQItems.U_PLineID = sParentRowID;
                    }
                    _moWOQ.moWOQItems.DoBlockUpdateTrigger = false;

                    
                }
                _moWOQ.DoBlockUpdateTrigger = false;
                _moWOQ.mbDoNotSetDefault = false;
                moWOQItems.doRecallBookmark();
                _moWOQ.WOQItems.first();
                bool bRet = _moWOQ.doAddDataRow();
                if (bRet == false) {
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    return null;
                }
                while (_moWOQ.WOQItems.next()) {
                    bRet = _moWOQ.WOQItems.doAddDataRow();
                    if (bRet == false) {
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                        return null;
                    }
                }
                if (DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                return _moWOQ;

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doDuplication") });
                if (DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
            } finally {
                mbDoNotSetDefault = false;
                miDoHandleChange--;
            }
            return null;
        }
        #endregion
        #region Create Activity
        public void doCreateActivity(bool bDoUpdate) {
            com.idh.bridge.DataRecords oRecords = null;
            try {
                if (bDoUpdate) {
                    object oActivity = Config.INSTANCE.doLookupTableField("OCLG", "ClgCode", "ClgCode='" + U_ClgCode + "'", "", true);
                    if (oActivity == null || oActivity.ToString() == string.Empty)
                        bDoUpdate = false;
                }
                if (bDoUpdate == false) {
                    int iActivityType = -1;
                    SAPbobsCOM.ActivityTypes oActivityType = (SAPbobsCOM.ActivityTypes)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oActivityTypes);
                    string sQry = "Select * from OCLT Where Name Like 'Enquiry'";
                    oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("doCheckActivityofEnquiryType", sQry);
                    if (oRecords != null && oRecords.RecordCount > 0) {
                        if (oRecords.getValue("Name").ToString().ToLower().Trim() == "enquiry") {
                            oActivityType.GetByKey(Convert.ToInt16(oRecords.getValue("Code")));
                            iActivityType = oActivityType.Code;
                        }
                    } else {
                        oActivityType.Name = "Enquiry";
                        oActivityType.Add();
                        iActivityType = Convert.ToInt16(com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetNewObjectKey());

                    }

                    SAPbobsCOM.CompanyService oCompanyService = com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetCompanyService();
                    SAPbobsCOM.ActivitiesService oActivityService = (SAPbobsCOM.ActivitiesService)oCompanyService.GetBusinessService(SAPbobsCOM.ServiceTypes.ActivitiesService);
                    SAPbobsCOM.Activity oActivity = (SAPbobsCOM.Activity)oActivityService.GetDataInterface(SAPbobsCOM.ActivitiesServiceDataInterfaces.asActivity);

                    SAPbobsCOM.ActivityParams oActParams;

                    if (U_CardCd != string.Empty && doValidateBPbyCode(U_CardCd)) {
                        oActivity.CardCode = U_CardCd;
                        oActivity.Personalflag = SAPbobsCOM.BoYesNoEnum.tNO;
                        object oContactPerson = Config.INSTANCE.doLookupTableField("OCPR", "CntctCode", "CardCode='" + U_CardCd + "' And [Name] Like '" + U_Contact + "' And Active='Y'");
                        if (oContactPerson != null)
                            oActivity.ContactPersonCode = Convert.ToInt32(oContactPerson);
                    } else {
                        oActivity.Personalflag = SAPbobsCOM.BoYesNoEnum.tYES;
                    }
                    oActivity.Notes = U_Notes;
                    oActivity.Details = U_SpInst.Length > 100 ? U_SpInst.Substring(0, 100) : U_SpInst;
                    oActivity.Activity = SAPbobsCOM.BoActivities.cn_Conversation;
                    oActivity.ActivityType = iActivityType;
                    oActivity.Phone = U_Phone1;
                    int UserID = Convert.ToInt16(com.idh.bridge.lookups.Config.INSTANCE.doGetUserId(idh.bridge.DataHandler.INSTANCE.User));

                    //UserID = U_User == 0 || U_User == null ? UserID : U_User;
                    UserID = U_User == 0 ? UserID : U_User;

                    // object oUserCode = Config.INSTANCE.doLookupTableField("OUSR", "USERID", "USER_CODE='" + UserName + "'");
                    if (UserID != 0)
                        oActivity.HandledBy = UserID;//Convert.ToInt16(oUserCode);

                    oActivity.ActivityDate = DateTime.Today.Date;//idh.utils.Dates.doDateTo( oDate)  
                    oActivity.ActivityTime = DateTime.Now;
                    oActivity.EndDuedate = DateTime.Today;
                    oActivity.EndTime = DateTime.Now.AddMinutes(15);
                    oActivity.UserFields.Item("U_IDHWOQID").Value = Code;
                    oActParams = oActivityService.AddActivity(oActivity);
                    if (hasDataToProcess())
                        doClearAddUpdateBuffers();
                    U_ClgCode = oActParams.ActivityCode;
                    if (hasAddedDataToProcess())
                        doProcessData();
                } else if (U_ClgCode != 0) {
                    SAPbobsCOM.CompanyService oCompanyService = com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetCompanyService();
                    SAPbobsCOM.ActivitiesService oActivityService = (SAPbobsCOM.ActivitiesService)oCompanyService.GetBusinessService(SAPbobsCOM.ServiceTypes.ActivitiesService);
                    SAPbobsCOM.Activity oActivity = (SAPbobsCOM.Activity)oActivityService.GetDataInterface(SAPbobsCOM.ActivitiesServiceDataInterfaces.asActivity);
                    SAPbobsCOM.ActivityParams oActParams;
                    oActParams = (SAPbobsCOM.ActivityParams)oActivityService.GetDataInterface(SAPbobsCOM.ActivitiesServiceDataInterfaces.asActivityParams);
                    oActParams.ActivityCode = U_ClgCode;
                    oActivity = oActivityService.GetActivity(oActParams);

                    bool doUpdate = false;
                    int UserID = Convert.ToInt16(com.idh.bridge.lookups.Config.INSTANCE.doGetUserId(idh.bridge.DataHandler.INSTANCE.User));
                    UserID = U_User == 0 ? UserID : U_User;
                    if (UserID != 0 && oActivity.HandledBy != UserID) {
                        oActivity.HandledBy = UserID;//Convert.ToInt16(oUserCode);
                        doUpdate = true;
                    }

                    if (oActivity.CardCode == string.Empty && !string.IsNullOrWhiteSpace(U_CardCd)) {
                        oActivity.Personalflag = SAPbobsCOM.BoYesNoEnum.tNO;
                        oActivity.CardCode = U_CardCd;
                        oActivity.UserFields.Item("U_IDHWOQID").Value = Code;
                        object oContactPerson = Config.INSTANCE.doLookupTableField("OCPR", "CntctCode", "CardCode='" + U_CardCd + "' And [Name] Like '" + U_Contact + "'");
                        if (oContactPerson != null)
                            oActivity.ContactPersonCode = Convert.ToInt32(oContactPerson);
                        //oGet.Notes = "Discuss next year's financial plan and training plan";
                        //update a single activity, or an already modified activity from a series 
                        doUpdate = true;
                    }
                    if (doUpdate)
                        oActivityService.UpdateActivity(oActivity);
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
            }
        }

        private void doUpdateActivity() {
            //com.idh.bridge.DataRecords oRecords = null;
            try {
                //    int iActivityType = -1;
                SAPbobsCOM.ActivityTypes oActivityType = (SAPbobsCOM.ActivityTypes)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oActivityTypes);
                //  string sActivityCode = U_ClgCode.ToString();
                //string sQry = "Select * from OCLT Where Name Like 'Enquiry'";
                //oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("doCheckActivityofEnquiryType", sQry);
                //if (oRecords != null && oRecords.RecordCount > 0) {
                //    if (oRecords.getValue("Name").ToString().ToLower().Trim() == "enquiry") {
                //        oActivityType.GetByKey(Convert.ToInt16(oRecords.getValue("Code")));
                //        iActivityType = oActivityType.Code;
                //    }
                //} else {
                //    oActivityType.Name = "Enquiry";
                //    oActivityType.Add();
                //    iActivityType = Convert.ToInt16(com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetNewObjectKey());

                //}

                SAPbobsCOM.CompanyService oCompanyService = com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetCompanyService();
                SAPbobsCOM.ActivitiesService oActivityService = (SAPbobsCOM.ActivitiesService)oCompanyService.GetBusinessService(SAPbobsCOM.ServiceTypes.ActivitiesService);
                SAPbobsCOM.Activity oActivity = (SAPbobsCOM.Activity)oActivityService.GetDataInterface(SAPbobsCOM.ActivitiesServiceDataInterfaces.asActivity);

                SAPbobsCOM.ActivityParams oActParams;


                oActParams = (SAPbobsCOM.ActivityParams)oActivityService.GetDataInterface(SAPbobsCOM.ActivitiesServiceDataInterfaces.asActivityParams);
                oActParams.ActivityCode = U_ClgCode;
                oActivity = oActivityService.GetActivity(oActParams);
                if (oActivity.CardCode == string.Empty && U_CardCd != string.Empty && doValidateBPbyCode(U_CardCd)) {
                    oActivity.CardCode = U_CardCd;
                    oActivity.Personalflag = SAPbobsCOM.BoYesNoEnum.tNO;
                    object oContactPerson = Config.INSTANCE.doLookupTableField("OCPR", "CntctCode", "CardCode='" + U_CardCd + "' And [Name] Like '" + U_Contact + "'");
                    if (oContactPerson != null)
                        oActivity.ContactPersonCode = Convert.ToInt32(oContactPerson);
                } else {
                    oActivity.Personalflag = SAPbobsCOM.BoYesNoEnum.tYES;
                }
                oActivity.UserFields.Item("U_IDHWOQID").Value = Code;
                int UserID = Convert.ToInt16(com.idh.bridge.lookups.Config.INSTANCE.doGetUserId(idh.bridge.DataHandler.INSTANCE.User));
                UserID = U_User == 0 ? UserID : U_User;
                if (UserID != 0 && oActivity.HandledBy != UserID) {
                    oActivity.HandledBy = UserID;//Convert.ToInt16(oUserCode);                    
                }

                oActivityService.UpdateActivity(oActivity);

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
            }
        }
        private bool doValidateBPbyCode(string sCardCode) {
            string sCardName = Config.INSTANCE.doGetBPName(sCardCode, false);
            if (sCardName == string.Empty)
                return false;
            else
                return true;
        }
        #endregion
        #region functions and methods
        public DataRecords dogetWOQApproveres() {
            string sQry = "SELECT apstg.WstCode,apstg.Name,apstg.MaxReqr,apstg.MaxRejReqr,appstg_dt.UserID as Approver ,apr_user.USER_CODE "
                + " FROM [dbo].[@IDH_APPRSTUP] apd With(nolock), OWST apstg With(nolock),WST1 appstg_dt With(nolock), OUSR apr_user with(nolock) "
                + " where [U_Origintr]='" + idh.bridge.DataHandler.INSTANCE.User + "'"
                + " And apstg.WstCode=apd.U_ApStagCd"
                + " And appstg_dt.WstCode=apstg.WstCode"
                + " And U_FormTyp='IDHWOQ' "
                + " And appstg_dt.UserID=apr_user.USERID ";
            DataRecords oDataRecord = Config.INSTANCE.doCachedQuery("doGetWOQApprovalReqired" + idh.bridge.DataHandler.INSTANCE.User, sQry);
            return oDataRecord;
        }
        public void dosetApprovalRequiredFlag() {
            try {
                string sQry = "SELECT apstg.WstCode,apstg.Name,apstg.MaxReqr,apstg.MaxRejReqr,appstg_dt.UserID as Approver ,apr_user.USER_CODE "
                    + " FROM [dbo].[@IDH_APPRSTUP] apd With(nolock), OWST apstg With(nolock),WST1 appstg_dt With(nolock), OUSR apr_user with(nolock) "
                    + " where [U_Origintr]='" + idh.bridge.DataHandler.INSTANCE.User + "'"
                    + " And apstg.WstCode=apd.U_ApStagCd"
                    + " And appstg_dt.WstCode=apstg.WstCode"
                    + " And U_FormTyp='IDHWOQ' "
                    + " And appstg_dt.UserID=apr_user.USERID ";
                DataRecords oDataRecord = Config.INSTANCE.doCachedQuery("doGetWOQApprovalReqired" + idh.bridge.DataHandler.INSTANCE.User, sQry);
                //DataRecords oDataRecord = DataHandler.INSTANCE.doBufferedSelectQuery("doSetItemsGrp", sQry );
                if (oDataRecord != null && oDataRecord.RecordCount > 0)
                    doApplyDefaultValue(IDH_WOQHD._AppRequired, "Y");
                else
                    doApplyDefaultValue(IDH_WOQHD._AppRequired, "N");
            } catch (Exception ex) {
                doApplyDefaultValue(_AppRequired, "N");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("dosetApprovalRequiredFlag") });
            }
        }
        public bool doCreateApprovalRequest(ref string sErrorMsgID, ref string ApproverUserNames) {
            try {
                DataRecords oDataRecord = dogetWOQApproveres();
                if (oDataRecord == null || oDataRecord.RecordCount == 0) {
                    sErrorMsgID = "WRNBPA13";
                    return false;
                }
                if (U_AppRequired == "Y") {
                    IDH_APPROVLH moAppH = new IDH_APPROVLH();
                    moAppH.getData(IDH_APPROVLH._DocNum + "='" + Code + "' And " + IDH_APPROVLH._DocVer + "='" + U_Version + "' And (U_NmAprovd>=U_NmAprReq	or U_NmRejted>=U_NmRejReq)", "");
                    if (moAppH.Count > 0) { //Document version is already in Approval
                        return false;
                    }
                    U_Status = ((int)en_WOQSTATUS.WaitingForApproval).ToString();// "2";
                    if (doSaveApprovalRequest(ref ApproverUserNames)) {
                        this.doUpdateDataRow();
                    } else
                        return false;
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doCreateApprovalRequest") });
                return false;
            }
            return true;
        }
        public bool doSaveApprovalRequest(ref string ApproverUserNames) {
            try {
                //string sQry = "SELECT apstg.WstCode,apstg.Name,apstg.MaxReqr,apstg.MaxRejReqr,appstg_dt.UserID as Approver ,apr_user.USER_CODE "
                //    + " FROM [dbo].[@IDH_APPRSTUP] apd With(nolock), OWST apstg With(nolock),WST1 appstg_dt With(nolock), OUSR apr_user with(nolock) "
                //    + " where [U_Origintr]='" + idh.bridge.DataHandler.INSTANCE.User + "'"
                //    + " And apstg.WstCode=apd.U_ApStagCd"
                //    + " And appstg_dt.WstCode=apstg.WstCode"
                //    + " And U_FormTyp='IDHWOQ' "
                //    + " And appstg_dt.UserID=apr_user.USERID ";
                DataRecords oDataRecord = dogetWOQApproveres(); //Config.INSTANCE.doCachedQuery("doGetWOQApprovalReqired" + idh.bridge.DataHandler.INSTANCE.User, sQry);
                if (oDataRecord != null && oDataRecord.RecordCount > 0) {
                    IDH_APPROVLH moAppH = new IDH_APPROVLH();
                    moAppH.U_DocAddDt = DateTime.Now;
                    moAppH.U_DocDt = U_BDate;
                    moAppH.U_DocNum = Code;
                    moAppH.U_DocVer = U_Version;
                    moAppH.U_FormTyp = "IDHWOQ";
                    moAppH.U_NmAprReq = oDataRecord.getValueAsInt("MaxReqr");
                    moAppH.U_NmRejReq = oDataRecord.getValueAsInt("MaxRejReqr");
                    moAppH.U_Origintr = idh.bridge.DataHandler.INSTANCE.User;//oDataRecord.getValueAsInt("MaxRejReqr");
                    moAppH.U_Stage = oDataRecord.getValueAsInt("WstCode");
                    moAppH.U_Status = Convert.ToString((int)IDH_APPROVLH.en_APPROVALSTATUS.Draft);
                    moAppH.U_NmAprovd = 0;
                    moAppH.U_NmRejted = 0;
                    if (moAppH.doAddDataRow()) {
                        string hcode = moAppH.Code;
                        IDH_APPROVLD moAppD = new IDH_APPROVLD();
                        for (int i = 0; i <= oDataRecord.RecordCount - 1; i++) {
                            oDataRecord.gotoRow(i);
                            moAppD.doAddEmptyRow(true);
                            moAppD.U_APPHID = moAppH.Code;
                            moAppD.U_ApprvNm = oDataRecord.getValueAsString("USER_CODE");
                            moAppD.U_ApprvUId = oDataRecord.getValueAsString("Approver");
                            moAppD.U_DocNum = Code;
                            moAppD.U_FormTyp = "IDHWOQ";
                            moAppD.U_Status = ((int)IDH_APPROVLD.en_APPROVALSTATUS.Waiting).ToString();
                            ApproverUserNames += oDataRecord.getValueAsString("USER_CODE") + ",";
                        }
                        moAppD.doProcessData();
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doSaveApprovalRequest") });
                return false;
            }
            return true;
        }

        public bool doCalculateAllRowsTotal(bool bReCalsVAT) {

            bool bResult = true;

            //CalculateTotals
            double Tot_CostPrice = 0;

            double Tot_ListPrice = 0;
            double Tot_LPProfitnLose = 0;
            double Tot_LPGMargin = 0;

            double Tot_ChargesPrice = 0;
            double Tot_CPProfitnLose = 0;
            double Tot_CPMNMargin = 0;
            double Tot_VatCharge = 0;
            double Tot_VatChargeonListPrice = 0;
            double Tot_VatCost = 0;

            this.moWOQItems.doBookmark();
            try {
                this.moWOQItems.first();
                while (moWOQItems.next()) {
                    try {
                        if (U_NoChgTax == "Y") {
                            moWOQItems.DoBlockUpdateTrigger = true;
                            moWOQItems.U_TaxAmt = 0;
                            moWOQItems.U_VtCostAmt = 0;
                            moWOQItems.doCalculateCostTotal();
                            moWOQItems.doCalculateChargeTotal();
                            moWOQItems.DoBlockUpdateTrigger = false;
                        } else if (U_NoChgTax != "Y" && bReCalsVAT) {
                            moWOQItems.doCalculateCostVat();
                            moWOQItems.doCalculateChargeVat();
                        }
                        Tot_CostPrice += moWOQItems.U_JCost;// U_PCTotal + U_TipTot+U_OrdTot+U_SLicCTo; //;

                        Tot_ListPrice += (moWOQItems.U_LstPrice * moWOQItems.U_ExpLdWgt) + moWOQItems.U_Price;

                        Tot_ChargesPrice += (moWOQItems.U_TCTotal + moWOQItems.U_Price);
                        Tot_VatChargeonListPrice += (moWOQItems.U_TChrgVtRt / 100.0 * ((moWOQItems.U_LstPrice * moWOQItems.U_ExpLdWgt))) + (moWOQItems.U_HChrgVtRt / 100.0 * moWOQItems.U_Price);

                        Tot_VatCharge += moWOQItems.U_TaxAmt;
                        Tot_VatCost += moWOQItems.U_VtCostAmt;

                    } catch (Exception ex) {
                        //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error updating the Row [" + Code +"] Prices.");
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXEURP", new string[] { this.moWOQItems.Code });
                    }
                }
            } catch (Exception ex) {
                throw (ex);
            } finally {
                moWOQItems.doRecallBookmark();
                //gotoRow(iCurrIndex);
            }
            Tot_LPProfitnLose = Tot_ListPrice + Tot_VatChargeonListPrice - Tot_CostPrice;//Tot_ListPrice -Tot_CostPrice;
            Tot_LPGMargin = Tot_LPProfitnLose / (Tot_ListPrice + Tot_VatChargeonListPrice) * 100.0;//Tot_LPProfitnLose / (Tot_ListPrice + Tot_VatCharge) * 100.0;

            Tot_CPProfitnLose = Tot_ChargesPrice + Tot_VatCharge - Tot_CostPrice; //; Tot_ChargesPrice + Tot_VatCharge - Tot_CostPrice - Tot_VatCost;
            Tot_CPMNMargin = Tot_CPProfitnLose / (Tot_ChargesPrice + Tot_VatCharge) * 100.0; // Tot_CPProfitnLose / (Tot_ChargesPrice + Tot_VatCharge) * 100.0;

            this.U_LstPrice = Tot_ListPrice + Tot_VatChargeonListPrice;
            this.U_Cost = Tot_CostPrice;
            this.U_LPProfit = Tot_LPProfitnLose;
            this.U_LPGrossM = Tot_LPGMargin;

            this.U_ChgPrice = Tot_ChargesPrice;
            this.U_ChgVAT = Tot_VatCharge;
            this.U_TCharge = Tot_ChargesPrice + Tot_VatCharge;

            this.U_CPProfit = Tot_CPProfitnLose;
            this.U_CPGrossM = Tot_CPMNMargin;

            this.CalculateTotals = false;
            Handler_ExternalTriggerWithData(this, "IDH_CALTOT:False");
            //doUpdateTotals();
            return bResult;
        }

        public void doSetByEnquiry(IDH_ENQUIRY oEnquiry) {
            //setItemValue("IDH_CardNM", oEnquiry.U_CardName);
            DoBlockUpdateTrigger = true;
           // AutoAddEmptyLine = false;
            U_Address = oEnquiry.U_Address;
            if (!IsActiveRow())
                doActivateRow(CurrentRowIndex);
            U_AddrssLN = oEnquiry.U_AddrssLN;
            //moWOQ.U_AttendUser=idh.bridge.DataHandler.INSTANCE.User;
            U_Block = oEnquiry.U_Block;
            U_Branch = Convert.ToInt16(Config.INSTANCE.doGetBranch(idh.bridge.DataHandler.INSTANCE.User));//GetUserBranch GetBranch Get User Branch GetloginUserBranch
            U_CardCd = oEnquiry.U_CardCode;
            U_CardNM = oEnquiry.U_CardName;
            U_ChgPrice = 0;
            U_City = oEnquiry.U_City;
            U_ClgCode = Convert.ToInt16(oEnquiry.U_ClgCode.ToString());//Activity id
            U_Contact = oEnquiry.U_CName;
            U_Cost = 0;
            U_Country = oEnquiry.U_Country;
            U_County = oEnquiry.U_County;
            U_CPGrossM = 0;
            U_CPProfit = 0;
            U_E_Mail = oEnquiry.U_E_Mail;
            U_LPGrossM = 0;
            U_LPProfit = 0;
            U_LstPrice = 0;
            U_Notes = oEnquiry.U_Notes;
            U_Phone1 = oEnquiry.U_Phone1;
            U_SpInst = oEnquiry.U_Desc;
            U_BDate = DateTime.Now;// idh.utils.Dates.doDateToSBODateStr(DateTime.Now);
            U_RSDate = DateTime.Now;//idh.utils.Dates.doDateToSBODateStr(DateTime.Now);
            U_Source = oEnquiry.U_Source; ;
            U_State = "";
            U_Status = ((int)IDH_WOQHD.en_WOQSTATUS.New).ToString();// "1";
            U_Street = oEnquiry.U_Street;
            string UserID = com.idh.bridge.lookups.Config.INSTANCE.doGetUserId(idh.bridge.DataHandler.INSTANCE.User);

            U_User = Convert.ToInt16(UserID);
            //idh.bridge.DataHandler.INSTANCE.SBOCompany.UserSignature;// idh.bridge.DataHandler.INSTANCE.User;
            U_Version = 1;
            U_ZpCd = oEnquiry.U_ZipCode;
            DoBlockUpdateTrigger = false;
            ///EnableItem(false, "IDH_DOCNUM");
            //AutoAddEmptyLine = true;

        }

        private bool doAddUpdateAddress() {
            SAPbobsCOM.BusinessPartners oBP = null;
            SAPbobsCOM.BPAddresses oAddress = null;
            try {
                if (U_CardCd != string.Empty && U_Address != string.Empty) {
                    object oAddressLine = null;
                    oAddressLine = Config.INSTANCE.doLookupTableField("CRD1", "Address", "CardCode='" + U_CardCd.Replace("'", "''") + "' And Address='" + U_Address.Replace("'", "''") + "' And AdresType='S'", "", true);
                    if (DoAddressChanged || oAddressLine == null) {
                        //Add Address

                        //If sCardCode.Length > 0 Then
                        //  Try
                        oBP = (SAPbobsCOM.BusinessPartners)IDHAddOns.idh.addon.Base.PARENT.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
                        if (oBP.GetByKey(U_CardCd)) {
                            oAddress = oBP.Addresses;
                            if (oAddressLine == null) {
                                oAddress.Add();
                                oAddress.SetCurrentLine(oAddress.Count - 1);
                                oAddress.UserFields.Fields.Item("U_AddrsCd").Value = Config.INSTANCE.GetAddressNextCode();
                            } else if (DoAddressChanged) {
                                int i = 0;
                                for (i = 0; i < oAddress.Count; i++) {
                                    oAddress.SetCurrentLine(i);
                                    if (oAddress.AddressName == U_Address && oAddress.AddressType == SAPbobsCOM.BoAddressType.bo_ShipTo)
                                        break;
                                }
                                if (i == oAddress.Count) {
                                    oAddress.Add();
                                    oAddress.SetCurrentLine(oAddress.Count - 1);
                                    oAddress.UserFields.Fields.Item("U_AddrsCd").Value = Config.INSTANCE.GetAddressNextCode();
                                }
                            }

                            oAddress.AddressName = U_Address;
                            oAddress.Street = U_Street;
                            oAddress.Block = U_Block;
                            oAddress.ZipCode = U_ZpCd;
                            oAddress.City = U_City;
                            oAddress.County = U_County;
                            oAddress.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo;
                            oAddress.State = U_State;
                            oAddress.Country = U_Country;
                            int iwResult;
                            string swResult;
                            iwResult = oBP.Update();
                            oAddressLine = Config.INSTANCE.doLookupTableField("CRD1", "U_AddrsCd", "CardCode Like '" + U_CardCd.Replace("'", "''") + "' And Address Like '" + U_Address.Replace("'", "''") + "' And AdresType='S'", "", true);
                            if (oAddressLine != null)
                                U_AddrssLN = Convert.ToString(oAddressLine);
                            if (iwResult != 0) {
                                IDHAddOns.idh.addon.Base.PARENT.goDICompany.GetLastError(out iwResult, out swResult);
                                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Adding Address: " + swResult, "ERSYADDA", new string[] { swResult });
                                return false;
                            }
                            return true;
                        }

                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doAddUpdateAddress:Add/Update BP's Address" });
                return false;
            } finally {
                if (oBP != null) {
                    object oObj = (object)oBP;
                    IDHAddOns.idh.data.Base.doReleaseObject(ref oObj);
                    oObj = (object)oAddress;
                    IDHAddOns.idh.data.Base.doReleaseObject(ref oObj);
                }
            }
            return true;
        }
        #endregion
        #region Create WO
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sOption">One= One WOH, Multiple=Each row will have its own WOH</param>
        /// <param name="iOptionoverride">1=Override, 2=skip rows having WOH Ids, 3 do not process at all</param>
        /// <param name="sWOHNums"></param>
        /// <param name="WOQ"></param>
        /// <returns></returns>
        public bool doCreateWObyWOQ(string sOption, int iOptionoverride, ref string sWOHNums, ref IDH_WOQHD WOQ, WOQRequestDates oRDates) {
            bool bContinue = true;
            try {
                IDH_WOQHD _moWOQ = new IDH_WOQHD();
                _moWOQ.MustLoadChildren = true;
                _moWOQ.getByKey(Code);
                DataHandler.INSTANCE.StartTransaction();
                //if (!IDHAddOns.idh.addon.Base.PARENT.goDICompany.InTransaction)
                //  IDHAddOns.idh.addon.Base.PARENT.goDICompany.StartTransaction();
                int iWOQRow = 0;
                _moWOQ.moWOQItems.doBookmark();
                _moWOQ.moWOQItems.first();
                while (true) {
                    com.idh.dbObjects.User.IDH_JOBENTR moWOH = new com.idh.dbObjects.User.IDH_JOBENTR();


                    moWOH.U_CardCd = _moWOQ.U_CardCd;
                    moWOH.U_CardNM = _moWOQ.U_CardNM;

                    moWOH.U_WOQID = _moWOQ.Code;


                    moWOH.U_Address = _moWOQ.U_Address;
                    moWOH.U_Street = _moWOQ.U_Street;
                    moWOH.U_Block = _moWOQ.U_Block;
                    moWOH.U_City = _moWOQ.U_City;
                    moWOH.U_ZpCd = _moWOQ.U_ZpCd;
                    moWOH.U_County = _moWOQ.U_County;
                    moWOH.U_State = _moWOQ.U_State;
                    //moWOH.U_Country =_moWOQ.U_Country;

                    moWOH.U_SpInst = _moWOQ.U_SpInst + '\n' + _moWOQ.U_Notes;


                    moWOH.U_Contact = _moWOQ.U_Contact;
                    moWOH.U_Phone1 = _moWOQ.U_Phone1;
                    //moWOH.U_Email =_moWOQ.U_E_Mail;

                    moWOH.U_BDate = _moWOQ.U_BDate;
                    moWOH.U_BTime = Dates.doTimeToNumStr();
                    moWOH.U_RSDate = _moWOQ.U_RSDate;
                    moWOH.U_User = Convert.ToInt16(_moWOQ.U_User);
                    moWOH.U_ItemGrp = _moWOQ.U_ItemGrp;
                    moWOH.U_TRNCd = Config.Parameter("DFTCDWQ");

                    if (moWOH.doAddDataRow("By WOQ") == false)
                        bContinue = false;
                    if (bContinue) {
                        DateTime moRDate;
                        if (sOption != "Multiple") {
                            if (_moWOQ.moWOQItems.Count > 0) {
                                // moWOQItems.doBookmark();
                                // moWOQItems.first();
                                while (_moWOQ.moWOQItems.next() && bContinue) {
                                    moRDate = _moWOQ.moWOQItems.U_RDate;
                                    if (oRDates != null) {
                                        for (int i = 0; i < oRDates.Count; i++) {
                                            if (oRDates[i].moWOR == _moWOQ.moWOQItems.Code) {
                                                moRDate = oRDates[i].moRDate; //_moWOQ.moWOQItems.U_RDate;
                                                break;
                                            }
                                        }
                                    }
                                    if ((_moWOQ.moWOQItems.U_Sample == "Y" && _moWOQ.moWOQItems.U_SampStatus != "Accepted" && _moWOQ.moWOQItems.U_SampStatus != "Cancelled")
                                       || (_moWOQ.moWOQItems.U_MSDS == "Y" && Config.INSTANCE.getValueFromOITMWithNoBuffer(_moWOQ.moWOQItems.U_WasCd, "U_LOFile").ToString() != "Y")
                                       || (iOptionoverride == 2 && _moWOQ.moWOQItems.U_WORID != string.Empty)) {
                                        //skipp the lines as these are not ready for WOR
                                        //  bool b = Config.INSTANCE.getValueFromOITMWithNoBuffer(_moWOQ.moWOQItems.U_WasCd, "U_LOFile").ToString() != "Y";
                                        //  string stemp = "";
                                    } else if (_moWOQ.moWOQItems.U_Status != ((int)IDH_WOQITEM.en_WOQITMSTATUS.Cancel).ToString() && _moWOQ.moWOQItems.U_Status != ((int)IDH_WOQITEM.en_WOQITMSTATUS.Close).ToString()
                                          && _moWOQ.moWOQItems.U_AdtnlItm == "" &&
                                         _moWOQ.moWOQItems.U_WasCd != string.Empty
                                         && _moWOQ.moWOQItems.doCreateWOR(moWOH, moRDate) == false) {
                                        bContinue = false;
                                    }
                                }
                                // _moWOQ.doRecallBookmark();
                            }
                        } else {//create separate WOs for each quote row
                            if (_moWOQ.moWOQItems.Count > 0) {
                                //   moWOQItems.doBookmark();
                                //  moWOQItems.first();
                                _moWOQ.moWOQItems.gotoRow(iWOQRow);
                                if (bContinue) {
                                    moRDate = _moWOQ.moWOQItems.U_RDate;
                                    if (oRDates != null) {
                                        for (int i = 0; i < oRDates.Count; i++) {
                                            if (oRDates[i].moWOR == _moWOQ.moWOQItems.Code) {
                                                moRDate = oRDates[i].moRDate; //_moWOQ.moWOQItems.U_RDate;
                                                break;
                                            }
                                        }
                                    }
                                    if ((_moWOQ.moWOQItems.U_Sample == "Y" && _moWOQ.moWOQItems.U_SampStatus != "Accepted" && _moWOQ.moWOQItems.U_SampStatus != "Cancelled")
                                       || (_moWOQ.moWOQItems.U_MSDS == "Y" && Config.INSTANCE.getValueFromOITMWithNoBuffer(_moWOQ.moWOQItems.U_WasCd, "U_LOFile").ToString() != "Y")
                                       || (iOptionoverride == 2 && _moWOQ.moWOQItems.U_WORID != string.Empty)) {
                                        //skipp the lines as these are not ready for WOR
                                    } else if (_moWOQ.moWOQItems.U_Status != ((int)IDH_WOQITEM.en_WOQITMSTATUS.Cancel).ToString() && _moWOQ.moWOQItems.U_Status != ((int)IDH_WOQITEM.en_WOQITMSTATUS.Close).ToString()
                                        && _moWOQ.moWOQItems.U_AdtnlItm == ""
                                        && _moWOQ.moWOQItems.U_WasCd != string.Empty
                                        && _moWOQ.moWOQItems.doCreateWOR(moWOH, moRDate) == false) {
                                        bContinue = false;
                                        //   doRecallBookmark();
                                    }
                                }
                            }
                        }
                    }
                    if (bContinue) {
                        _moWOQ.U_WOHID = moWOH.Code;
                        _moWOQ.U_Status = ((int)en_WOQSTATUS.WOCreated).ToString();
                        sWOHNums += _moWOQ.U_WOHID + ", ";
                    } else
                        break;
                    if (sOption != "Multiple" || iWOQRow == _moWOQ.moWOQItems.Count - 1)
                        break;

                    iWOQRow++;
                    _moWOQ.moWOQItems.gotoRow(iWOQRow);

                    while ((_moWOQ.moWOQItems.U_AdtnlItm == "A" || _moWOQ.moWOQItems.U_WasCd == string.Empty) && iWOQRow < _moWOQ.moWOQItems.Count - 1) {
                        iWOQRow++;
                        _moWOQ.moWOQItems.gotoRow(iWOQRow);
                    }
                    if (iWOQRow > _moWOQ.moWOQItems.Count - 1)
                        break;
                    if ((_moWOQ.moWOQItems.U_AdtnlItm == "A" || _moWOQ.moWOQItems.U_WasCd == string.Empty) && iWOQRow == _moWOQ.moWOQItems.Count - 1) {
                        break;
                    }
                }

                if (bContinue == false && DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);

                else if (bContinue && DataHandler.INSTANCE.IsInTransaction()) {

                    bool ddone = _moWOQ.doProcessData();//true,"",);
                    //if (ddone )
                    //    WOQ.doReLoadData();//= _WOHID;
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    if (WOQ == null) {
                        //doLoadRowData
                        this.getByKey(Code);
                        // this.doReLoadData();
                        // this.doLoadChildren();
                    }

                }
                //doRecallBookmark();
                //moWOR.U_WOQLID = moWOH.Code;

            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: [Code:" + Code + "]" + e.ToString(), "Error Adding the WOR [Code:" + Code + "].");
                sWOHNums = "";
                if (DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doCreateWObyWOQ") });
                return false;

            } finally {
            }
            return bContinue;
        }
        #endregion

    }
}