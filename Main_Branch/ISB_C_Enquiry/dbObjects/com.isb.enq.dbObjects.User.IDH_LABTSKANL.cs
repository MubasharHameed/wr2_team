/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 09/02/2016 12:22:24
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User{
   [Serializable] 
	public class IDH_LABTSKANL: com.isb.enq.dbObjects.Base.IDH_LABTSKANL{ 

		public IDH_LABTSKANL() : base() {
			
		}

   	public IDH_LABTSKANL( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	}
	}
}
