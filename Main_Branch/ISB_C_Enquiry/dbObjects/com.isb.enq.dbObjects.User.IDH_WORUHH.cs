/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 13/12/2016 16:30:53
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_WORUHH : com.isb.enq.dbObjects.Base.IDH_WORUHH {

        public IDH_WORUHH() : base() {
            msAutoNumKey = "SEQWORBH";
        }

        public IDH_WORUHH(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm) : base(oIDHForm, oForm) {
            msAutoNumKey = "SEQWORBH";
        }
    }
}
