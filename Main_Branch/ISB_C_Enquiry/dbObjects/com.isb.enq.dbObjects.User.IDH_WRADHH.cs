/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 06/12/2016 13:18:02
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_WRADHH : com.isb.enq.dbObjects.Base.IDH_WRADHH {

        public IDH_WRADHH() : base() {
            msAutoNumKey = "SEQWRDBH";
        }

        public IDH_WRADHH(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm) : base(oIDHForm, oForm) {
            msAutoNumKey = "SEQWRDBH";
        }
    }
}
