/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 09/11/2016 17:15:19
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_LVSAMD : com.isb.enq.dbObjects.Base.IDH_LVSAMD {

        public static int None = 0;
        public static int FrquencyType = 2;
        public static int FrquencyCount= 4;
        public static int Days = 8;
        public static int Qty = 16;
        public static int RemovePBI = 32;
        public static int AutoStartDate = 64;
        public static int JobType = 128;
        public static int AutoComplete = 256;
        public static int WTNDateFrom = 512;
        public static int WTNDateTo= 1024;
        public static int AddPBI= 2048;
        public static int Rebate= 4096;
        public static int UOM= 8192;
        public static int SupplierRef = 16384;
        public static int RouteComments = 32768;
        public static int DriverComments = 65536;
        public static int Periodtype= 131072;
       

        private int miDoHandleChange = 0;
        public bool DoBlockUpdateTrigger
        {
            get { return miDoHandleChange > 0; }
            set { miDoHandleChange += (value ? 1 : -1); }
        }

        public IDH_LVSAMD() : base() {
            msAutoNumKey = "SEQLSAMD";
        }

        public IDH_LVSAMD(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm) : base(oIDHForm, oForm) {
            msAutoNumKey = "SEQLSAMD";
        }

        public override void doSetFieldHasChanged(int iRow, string sFieldname, object oOldValue, object oNewValue) {
            base.doSetFieldHasChanged(iRow, sFieldname, oOldValue, oNewValue);
            if (miDoHandleChange > 0)
                return;
            miDoHandleChange++;
            int iValue = 0;
            if (U_FrqTyp == "Y") {
                iValue += (int)Math.Pow(2,1);
            }
            if (U_FrqCount== "Y") {
                iValue += (int)Math.Pow(2, 2);
            }
            if (U_Days== "Y") {
                iValue += (int)Math.Pow(2, 3);
            }
            if (U_Qty== "Y") {
                iValue += (int)Math.Pow(2, 4);
            }
            if (U_Removal== "Y") {
                iValue += (int)Math.Pow(2, 5);
            }
            if (U_SDate== "Y") {
                iValue += (int)Math.Pow(2, 6);
            }
            if (U_JbType== "Y") {
                iValue += (int)Math.Pow(2, 7);
            }
            if (U_AutoCmp== "Y") {
                iValue += (int)Math.Pow(2, 8);
            }
            if (U_WTNFDt== "Y") {
                iValue += (int)Math.Pow(2, 9);
            }
            if (U_WTNTDt == "Y") {
                iValue += (int)Math.Pow(2, 10);
            }
            if (U_AddPBI == "Y") {
                iValue += (int)Math.Pow(2, 11);
            }
            if (U_Rebate== "Y") {
                iValue += (int)Math.Pow(2, 12);
            }
            if (U_UOM== "Y") {
                iValue += (int)Math.Pow(2, 13);
            }
            if (U_SupRef== "Y") {
                iValue += (int)Math.Pow(2, 14);
            }
            if (U_RoutCmt == "Y") {
                iValue += (int)Math.Pow(2, 15);
            }
            if (U_DrvCmt== "Y") {
                iValue += (int)Math.Pow(2, 16);
            }
            if (U_PERTYP == "Y") {
                iValue += (int)Math.Pow(2, 17);
            }
            U_AmdBnVal = iValue;
            miDoHandleChange--;
        }

        public bool doCheckAmendListChange(int iAmendOption) {
            int iAuth = U_AmdBnVal;
            if (iAuth < 0)
                return true;

            bool bTest = ((iAuth & iAmendOption) == iAmendOption);
            return bTest;
        }

        public static bool doCheckAmendListChange(int _iAmendOption,int _AmdBnVal) {
            int iAuth = _AmdBnVal;
            if (iAuth < 0)
                return true;

            bool bTest = ((iAuth & _iAmendOption) == _iAmendOption);
            return bTest;
        }

        public static bool bIsAddNewPBIAction(string sAction) {
            IDH_LVSAMD oActionObject = new IDH_LVSAMD();
            oActionObject.UnLockTable = true;
            //string sAction = U_AmndTp;
            int iRet = oActionObject.getData(IDH_LVSAMD._AmdDesc + "=\'" + sAction + "\' And " + IDH_LVSAMD._Active + "=\'Y\'", "");
            if (iRet > 0) {
                return  (oActionObject.doCheckAmendListChange(IDH_LVSAMD.AddPBI));
            } else
                return false;
        }
    }
}
