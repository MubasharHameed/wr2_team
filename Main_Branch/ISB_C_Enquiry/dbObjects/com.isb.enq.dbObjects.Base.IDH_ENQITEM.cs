/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 29/10/2015 11:02:10
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base {
    [Serializable]
    public class IDH_ENQITEM : com.idh.dbObjects.DBBase {

        //private Linker moLinker = null;
        //public Linker ControlLinker {
        //    get { return moLinker; }
        //    set { moLinker = value; }
        //}

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_ENQITEM()
            : base("@IDH_ENQITEM") {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_ENQITEM(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oForm, "@IDH_ENQITEM") {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
		* Table name
		*/
        public readonly static string TableName = "@IDH_ENQITEM";

        /**
         * Decription: Code
         * Mandatory: tYes
         * Name: Code
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Code = "Code";
        public string Code {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code() {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue) {
            return base.doValidation(_Code, oValue);
        }
        /**
         * Decription: Name
         * Mandatory: tYes
         * Name: Name
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Name = "Name";
        public string Name {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name() {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue) {
            return base.doValidation(_Name, oValue);
        }
        /**
         * Decription: Additional Item
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_AddItm
         * Size: 1
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _AddItm = "U_AddItm";
        public string U_AddItm {
            get {
                return getValueAsString(_AddItm);
            }
            set { setValue(_AddItm, value); }
        }
        public string doValidate_AddItm() {
            return doValidate_AddItm(U_AddItm);
        }
        public virtual string doValidate_AddItm(object oValue) {
            return base.doValidation(_AddItm, oValue);
        }

        /**
         * Decription: Enquiry ID
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_EnqId
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _EnqId = "U_EnqId";
        public string U_EnqId {
            get {
                return getValueAsString(_EnqId);
            }
            set { setValue(_EnqId, value); }
        }
        public string doValidate_EnqId() {
            return doValidate_EnqId(U_EnqId);
        }
        public virtual string doValidate_EnqId(object oValue) {
            return base.doValidation(_EnqId, oValue);
        }

        /**
         * Decription: Estimated Qty
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_EstQty
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Quantity
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _EstQty = "U_EstQty";
        public double U_EstQty {
            get {
                return getValueAsDouble(_EstQty);
            }
            set { setValue(_EstQty, value); }
        }
        public string doValidate_EstQty() {
            return doValidate_EstQty(U_EstQty);
        }
        public virtual string doValidate_EstQty(object oValue) {
            return base.doValidation(_EstQty, oValue);
        }

        /**
         * Decription: Material Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ItemCode
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ItemCode = "U_ItemCode";
        public string U_ItemCode {
            get {
                return getValueAsString(_ItemCode);
            }
            set { setValue(_ItemCode, value); }
        }
        public string doValidate_ItemCode() {
            return doValidate_ItemCode(U_ItemCode);
        }
        public virtual string doValidate_ItemCode(object oValue) {
            return base.doValidation(_ItemCode, oValue);
        }

        /**
         * Decription: Material Description(F)
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ItemName
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ItemName = "U_ItemName";
        public string U_ItemName {
            get {
                return getValueAsString(_ItemName);
            }
            set { setValue(_ItemName, value); }
        }
        public string doValidate_ItemName() {
            return doValidate_ItemName(U_ItemName);
        }
        public virtual string doValidate_ItemName(object oValue) {
            return base.doValidation(_ItemName, oValue);
        }
        /**
        * Decription: Material Description
        * DefaultValue: 
        * LinkedTable: 
        * Mandatory: False
        * Name: U_ItemDesc
        * Size: 100
        * Type: db_Alpha
        * CType: string
        * SubType: st_None
        * ValidValue: 
        * Value: 
        * Identity: False
        */
        public readonly static string _ItemDesc = "U_ItemDesc";
        public string U_ItemDesc {
            get {
                return getValueAsString(_ItemDesc);
            }
            set { setValue(_ItemDesc, value); }
        }
        public string doValidate_ItemDesc() {
            return doValidate_ItemDesc(U_ItemDesc);
        }
        public virtual string doValidate_ItemDesc(object oValue) {
            return base.doValidation(_ItemDesc, oValue);
        }

        /**
         * Decription: PCode
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PCode
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PCode = "U_PCode";
        public string U_PCode {
            get {
                return getValueAsString(_PCode);
            }
            set { setValue(_PCode, value); }
        }
        public string doValidate_PCode() {
            return doValidate_PCode(U_PCode);
        }
        public virtual string doValidate_PCode(object oValue) {
            return base.doValidation(_PCode, oValue);
        }

        /**
         * Decription: Sort Order
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Sort
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Sort = "U_Sort";
        public int U_Sort {
            get {
                return getValueAsInt(_Sort);
            }
            set { setValue(_Sort, value); }
        }
        public string doValidate_Sort() {
            return doValidate_Sort(U_Sort);
        }
        public virtual string doValidate_Sort(object oValue) {
            return base.doValidation(_Sort, oValue);
        }

        /**
         * Decription: Status
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Status
         * Size: 2
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Status = "U_Status";
        public string U_Status {
            get {
                return getValueAsString(_Status);
            }
            set { setValue(_Status, value); }
        }
        public string doValidate_Status() {
            return doValidate_Status(U_Status);
        }
        public virtual string doValidate_Status(object oValue) {
            return base.doValidation(_Status, oValue);
        }

        /**
         * Decription: Unit of Measurement
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_UOM
         * Size: 7
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _UOM = "U_UOM";
        public string U_UOM {
            get {
                return getValueAsString(_UOM);
            }
            set { setValue(_UOM, value); }
        }
        public string doValidate_UOM() {
            return doValidate_UOM(U_UOM);
        }
        public virtual string doValidate_UOM(object oValue) {
            return base.doValidation(_UOM, oValue);
        }

        /**
         * Decription: WOQ ID
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WOQID
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _WOQID = "U_WOQID";
        public string U_WOQID {
            get {
                return getValueAsString(_WOQID);
            }
            set { setValue(_WOQID, value); }
        }
        public string doValidate_WOQID() {
            return doValidate_WOQID(U_WOQID);
        }
        public virtual string doValidate_WOQID(object oValue) {
            return base.doValidation(_WOQID, oValue);
        }

        /**
         * Decription: WOQ Line ID
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WOQLineID
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _WOQLineID = "U_WOQLineID";
        public string U_WOQLineID {
            get {
                return getValueAsString(_WOQLineID);
            }
            set { setValue(_WOQLineID, value); }
        }
        public string doValidate_WOQLineID() {
            return doValidate_WOQLineID(U_WOQLineID);
        }
        public virtual string doValidate_WOQLineID(object oValue) {
            return base.doValidation(_WOQLineID, oValue);
        }

        /**
         * Decription: Waste Group Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WstGpCd
         * Size: 20
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _WstGpCd = "U_WstGpCd";
        public string U_WstGpCd {
            get {
                return getValueAsString(_WstGpCd);
            }
            set { setValue(_WstGpCd, value); }
        }
        public string doValidate_WstGpCd() {
            return doValidate_WstGpCd(U_WstGpCd);
        }
        public virtual string doValidate_WstGpCd(object oValue) {
            return base.doValidation(_WstGpCd, oValue);
        }

        /**
         * Decription: Waste Group Name
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WstGpNm
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _WstGpNm = "U_WstGpNm";
        public string U_WstGpNm {
            get {
                return getValueAsString(_WstGpNm);
            }
            set { setValue(_WstGpNm, value); }
        }
        public string doValidate_WstGpNm() {
            return doValidate_WstGpNm(U_WstGpNm);
        }
        public virtual string doValidate_WstGpNm(object oValue) {
            return base.doValidation(_WstGpNm, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo() {
            if (moDBFields == null)
                moDBFields = new DBFields(16);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_AddItm, "Additional Item", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Additional Item
            moDBFields.Add(_EnqId, "Enquiry ID", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, false); //Enquiry ID
            moDBFields.Add(_EstQty, "Estimated Qty", 4, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Estimated Qty
            moDBFields.Add(_ItemCode, "Material Code", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Material Code
            moDBFields.Add(_ItemName, "Material Description(F)", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Material Description(F)
            moDBFields.Add(_PCode, "PCode", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //PCode
            moDBFields.Add(_Sort, "Sort Order", 8, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Sort Order
            moDBFields.Add(_Status, "Status", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2, EMPTYSTR, false, false); //Status
            moDBFields.Add(_UOM, "Unit of Measurement", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 7, EMPTYSTR, false, false); //Unit of Measurement
            moDBFields.Add(_WOQID, "WOQ ID", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //WOQ ID
            moDBFields.Add(_WOQLineID, "WOQ Line ID", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //WOQ Line ID
            moDBFields.Add(_WstGpCd, "Waste Group Code", 13, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Waste Group Code
            moDBFields.Add(_WstGpNm, "Waste Group Name", 14, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Waste Group Name
            moDBFields.Add(_ItemDesc, "Material Description", 15, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Material Description

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation() {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_AddItm());
            doBuildValidationString(doValidate_EnqId());
            doBuildValidationString(doValidate_EstQty());
            doBuildValidationString(doValidate_ItemCode());
            doBuildValidationString(doValidate_ItemName());
            doBuildValidationString(doValidate_PCode());
            doBuildValidationString(doValidate_Sort());
            doBuildValidationString(doValidate_Status());
            doBuildValidationString(doValidate_UOM());
            doBuildValidationString(doValidate_WOQID());
            doBuildValidationString(doValidate_WOQLineID());
            doBuildValidationString(doValidate_WstGpCd());
            doBuildValidationString(doValidate_WstGpNm());
            doBuildValidationString(doValidate_ItemDesc());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_AddItm)) return doValidate_AddItm(oValue);
            if (sFieldName.Equals(_EnqId)) return doValidate_EnqId(oValue);
            if (sFieldName.Equals(_EstQty)) return doValidate_EstQty(oValue);
            if (sFieldName.Equals(_ItemCode)) return doValidate_ItemCode(oValue);
            if (sFieldName.Equals(_ItemName)) return doValidate_ItemName(oValue);
            if (sFieldName.Equals(_PCode)) return doValidate_PCode(oValue);
            if (sFieldName.Equals(_Sort)) return doValidate_Sort(oValue);
            if (sFieldName.Equals(_Status)) return doValidate_Status(oValue);
            if (sFieldName.Equals(_UOM)) return doValidate_UOM(oValue);
            if (sFieldName.Equals(_WOQID)) return doValidate_WOQID(oValue);
            if (sFieldName.Equals(_WOQLineID)) return doValidate_WOQLineID(oValue);
            if (sFieldName.Equals(_WstGpCd)) return doValidate_WstGpCd(oValue);
            if (sFieldName.Equals(_WstGpNm)) return doValidate_WstGpNm(oValue);
            if (sFieldName.Equals(_ItemDesc)) return doValidate_ItemDesc(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName) {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
         * Link the Name Field to the Form Item.
         */
        public void doLink_Name(string sControlName) {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
         * Link the U_AddItm Field to the Form Item.
         */
        public void doLink_AddItm(string sControlName) {
            moLinker.doLinkDataToControl(_AddItm, sControlName);
        }
        /**
         * Link the U_EnqId Field to the Form Item.
         */
        public void doLink_EnqId(string sControlName) {
            moLinker.doLinkDataToControl(_EnqId, sControlName);
        }
        /**
         * Link the U_EstQty Field to the Form Item.
         */
        public void doLink_EstQty(string sControlName) {
            moLinker.doLinkDataToControl(_EstQty, sControlName);
        }
        /**
         * Link the U_ItemCode Field to the Form Item.
         */
        public void doLink_ItemCode(string sControlName) {
            moLinker.doLinkDataToControl(_ItemCode, sControlName);
        }
        /**
         * Link the U_ItemName Field to the Form Item.
         */
        public void doLink_ItemName(string sControlName) {
            moLinker.doLinkDataToControl(_ItemName, sControlName);
        }
        /**
         * Link the U_PCode Field to the Form Item.
         */
        public void doLink_PCode(string sControlName) {
            moLinker.doLinkDataToControl(_PCode, sControlName);
        }
        /**
         * Link the U_Sort Field to the Form Item.
         */
        public void doLink_Sort(string sControlName) {
            moLinker.doLinkDataToControl(_Sort, sControlName);
        }
        /**
         * Link the U_Status Field to the Form Item.
         */
        public void doLink_Status(string sControlName) {
            moLinker.doLinkDataToControl(_Status, sControlName);
        }
        /**
         * Link the U_UOM Field to the Form Item.
         */
        public void doLink_UOM(string sControlName) {
            moLinker.doLinkDataToControl(_UOM, sControlName);
        }
        /**
         * Link the U_WOQID Field to the Form Item.
         */
        public void doLink_WOQID(string sControlName) {
            moLinker.doLinkDataToControl(_WOQID, sControlName);
        }
        /**
         * Link the U_WOQLineID Field to the Form Item.
         */
        public void doLink_WOQLineID(string sControlName) {
            moLinker.doLinkDataToControl(_WOQLineID, sControlName);
        }
        /**
         * Link the U_WstGpCd Field to the Form Item.
         */
        public void doLink_WstGpCd(string sControlName) {
            moLinker.doLinkDataToControl(_WstGpCd, sControlName);
        }
        /**
         * Link the U_WstGpNm Field to the Form Item.
         */
        public void doLink_WstGpNm(string sControlName) {
            moLinker.doLinkDataToControl(_WstGpNm, sControlName);
        }
        /**
         * Link the U_ItemDesc Field to the Form Item.
         */
        public void doLink_ItemDesc(string sControlName) {
            moLinker.doLinkDataToControl(_ItemDesc, sControlName);
        }
        #endregion

    }
}
