/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 06/10/2015 17:50:10
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.bridge.lookups;
using com.idh.bridge;
using com.idh.dbObjects.numbers;
using com.idh.dbObjects.User;

using com.isb.enq.utils;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_WOQITEM : com.isb.enq.dbObjects.Base.IDH_WOQITEM {

        public enum en_WOQITMSTATUS {
            New = 1,
            Started = 2,
            Quoted = 3,
            WOCreated = 4,
            Close = 5,
            Cancel = 6
        };

        //com.idh.dbObjects.DBFields _DBFields;        
        public com.idh.dbObjects.DBFields DBFields {
            get {
                return moDBFields;
            }
        }

        protected IDH_WOQHD moParentWOQ;
        public IDH_WOQHD WOQ {
            get {
                if (moParentWOQ == null && U_JobNr != null && U_JobNr.Length > 0) {
                    moParentWOQ = new IDH_WOQHD();
                    if (!moParentWOQ.getByKey(U_JobNr)) {
                        moParentWOQ = null;
                    }

                }
                if (moParentWOQ == null) {
                    //DataHandler.INSTANCE.doError("The Parent Disposal Order [" + U_JobNr + "] could not be retrieved for Disposal Order Row [" + Code + "].");
                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("The Parent WOQ [" + U_JobNr + "] could not be retrieved for WOQ Item [" + Code + "].",
                        "ERSYFPDO", new string[] { U_JobNr, Code });
                }
                return moParentWOQ;
            }
        }

        private int miDoHandleChange = 0;
        public bool DoBlockUpdateTrigger {
            get { return miDoHandleChange > 0; }
            set { miDoHandleChange += (value ? 1 : -1); }
        }

        private bool mbDoNotSetDefault = false;
        public bool DoNotSetDefault {
            set {
                mbDoNotSetDefault = value;
            }
            get {
                return mbDoNotSetDefault;
            }
        }
        private bool mbDoNotLoadParent = false;
        public bool DoNotSetParent {
            set {
                mbDoNotLoadParent = value;
            }
            get {
                return mbDoNotLoadParent;
            }
        }

        public IDH_WOQITEM()
            : base() {
            msAutoNumKey = "WOQUOTED";
        }

        public IDH_WOQITEM(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oIDHForm, oForm) {
            msAutoNumKey = "WOQUOTED";
        }

        public IDH_WOQITEM(IDH_WOQHD oParent)
            : base() {
            moParentWOQ = oParent;
            msAutoNumKey = "WOQUOTED";
            OrderByField = _Sort;
        }

        //Disposal Charge
        private IDH_CSITPR moCIP = new IDH_CSITPR();
        public IDH_CSITPR CIP {
            get { return moCIP; }
        }
        //Producer Cost
        private IDH_SUITPR moSIPP = new IDH_SUITPR();
        public IDH_SUITPR SIPP {
            get { return moSIPP; }
        }
        //Haulier Cost        
        private IDH_SUITPR moSIPH = new IDH_SUITPR();
        public IDH_SUITPR SIPH {
            get { return moSIPH; }
        }

        private string msHCCTCos = "STANDARD";// = "STANDARD";  
        private string msCCHChr = "STANDARD";
        private string msCCHCos = "VARIABLE";
        private string msCCTCos = "VARIABLE";
        private string msCCTChr = "VARIABLE";

        private double mdTOffset = 0;
        private double mdHOffset = 0;
        private double mdTCstOffset = 0;
        private double mdHCstOffset = 0;

        /**
         * et_ExternalYNOption The external YN Option handler
         */
        //public et_ExternalYNOption Handler_ExternalYNOption = null;

        private string dtNewRDate = "";
        public string F_dtNewRDate {
            get {
                return dtNewRDate;
            }
            set { dtNewRDate = value; }
        }
        #region userFriendlyProperties
        public string F_ContainerGroup {
            get {
                return U_ItmGrp;
            }
            set { U_ItmGrp = value; }
        }
        public string F_ContainerCode {
            get {
                return U_ItemCd;
            }
            set { U_ItemCd = value; }
        }
        public string F_ContainerDescription {
            get {
                return U_ItemDsc;
            }
            set { U_ItemDsc = value; }
        }
        public string F_CustomerCode {
            get {
                return moParentWOQ.U_CardCd;// U_CustCd;
            }
            //set { U_CustCd = value; }
        }
        public string F_CustomerName {
            get {
                return U_CustNm;
            }
            set { U_CustNm = value; }
        }
        //public string F_ProducerCode {
        //    get {
        //        return U_ProCd;
        //    }
        //    set { U_ProCd = value; }
        //}
        //public string F_ProducerName {
        //    get {
        //        return U_ProNm;
        //    }
        //    set { U_ProNm = value; }
        //}
        public string F_DisposalSiteCode {
            get {
                return U_Tip;
            }
            set { U_Tip = value; }
        }
        public string F_DisposalSiteName {
            get {
                return U_TipNm;
            }
            set { U_TipNm = value; }
        }
        public string F_CarrierCode {
            get {
                return U_CarrCd;
            }
            set { U_CarrCd = value; }
        }
        public string F_CarrierName {
            get {
                return U_CarrNm;
            }
            set { U_CarrNm = value; }
        }
        public string F_WasteCode {
            get {
                return U_WasCd;
            }
            set { U_WasCd = value; }
        }
        public string F_WasteDescription {
            get {
                return U_WasFDsc;
            }
            set { U_WasFDsc = value; }
        }

        public string F_CustomerAddress {
            get {
                return moParentWOQ.U_Address;
            }
        }
        public string F_CustomerZipCode {
            get {
                return moParentWOQ.U_ZpCd;
            }
        }
        //public string F_DisposalSiteAddress {
        //    get {
        //        return Order.U_SAddress;
        //    }
        //}
        //public string F_Origin {
        //    get {
        //        return Order.U_Origin;
        //    }
        //}
        #endregion

        public int getByParentNumber(string sParentNo) {
            return getData(IDH_WOQITEM._JobNr + " = '" + sParentNo + '\'', IDH_WOQITEM._Sort);
        }

        public int getBySampleRef(string sSampleRef) {
            return getData(IDH_WOQITEM._SampleRef + " = '" + sSampleRef + "'", IDH_WOQITEM._Sort);
        }

        #region overrides


        public bool doLoadParent() {
            bool bOk = false;
            bool bLoadCh = moParentWOQ.MustLoadChildren;
            moParentWOQ.MustLoadChildren = false;
            try {
                bOk = moParentWOQ.getByKey(U_JobNr);
            } catch (Exception ex) {
                throw (ex);
            } finally {
                moParentWOQ.MustLoadChildren = bLoadCh;
            }
            return bOk;
        }

        protected void doSetDefaultsForAdd() {
            doApplyDefaults();
        }
        public override void doApplyDefaults() {
            if (mbDoNotSetDefault == true)
                return;
            //base.doApplyDefaults();

            try {
                doApplyDefaultValue(_Status, "1");

                //doApplyDefaultValue(_PStat, FixedValues.getStatusOpen());

                if (WOQ != null) {
                    doApplyDefaultValue(_JobNr, WOQ.Code);
                    doApplyDefaultValue(_Status, "1");


                }


                string sUOM = Config.INSTANCE.getDefaultUOM();
                doApplyDefaultValue(_UOM, sUOM);

                //NumbersPair oNextNum;
                //oNextNum = this.getNewKey();
                //doApplyDefaultValue(_Code, oNextNum.CodeNumber.ToString());
                //doApplyDefaultValue(_Name, oNextNum.NameNumber.ToString());
                doApplyDefaultValue(_Sort, this.Count);
                doApplyDefaultValue(_EnqID, "");
                doApplyDefaultValue(_EnqLID, "");
                doApplyDefaultValue(_MANPRC, 0);



                doApplyDefaultValue(_ExpLdWgt, Config.INSTANCE.getParameterAsInt("WOQDFQTY", 1, true));
                doApplyDefaultValue(_HlSQty, 1);
                doApplyDefaultValue(_CusQty, 1);
                doApplyDefaultValue(_CarWgt, 1);
                doApplyDefaultValue(_OrdWgt, 1);




                doApplyDefaultValue(_Branch, moParentWOQ.U_Branch);
                doApplyDefaultValue(_ItmGrp, moParentWOQ.U_ItemGrp);
                doApplyDefaultValue(_Obligated, "");

                dtNewRDate = "";
                //string sdefaultJobType = "";
                //if (this.Count > 1) {
                //    doBookmark();
                //    while (this.previous()) {
                //        if (U_AdtnlItm == "" && U_JobTp != string.Empty) {
                //            sdefaultJobType = U_JobTp;
                //            break;
                //        }
                //    }
                //    doRecallBookmark();
                //}
                //if (sdefaultJobType != string.Empty)
                //    U_JobTp = sdefaultJobType;

                //doApplyDefaultValue(_PCode, "");
                // doApplyDefaultValue(_Created, "N");


            } catch (Exception) { }
        }

        public override void doClearBuffers() {
            base.doClearBuffers();
        }

        public void doMarkUserUpdate(string sFieldName) {
            doMarkUserUpdate(sFieldName, null, getValue(sFieldName), false);
        }

        public void doMarkUserUpdate(string sFieldName, bool bDoPreSet) {
            doMarkUserUpdate(sFieldName, null, getValue(sFieldName), bDoPreSet);
        }

        private void doMarkUserUpdate(string sFieldName, object oOldValue, object oNewValue, bool bDoPreSet) {
            if (bDoPreSet) {
                doActualMarked(sFieldName);
            }

            doSetFieldHasChanged(sFieldName, oOldValue, oNewValue);

            if (!bDoPreSet) {
                doActualMarked(sFieldName);
            }
        }

        private void doActualMarked(string sFieldName) {
            if (sFieldName == _TCharge)
                setUserChanged(Config.MASK_TIPCHRG, true);
            else if (sFieldName == _CusChr)
                setUserChanged(Config.MASK_HAULCHRG, true);
            else if (sFieldName == _TipCost)
                setUserChanged(Config.MASK_TIPCST, true);
            else if (sFieldName == _OrdCost)
                setUserChanged(Config.MASK_HAULCST, true);
            else if (sFieldName == _PCost)
                setUserChanged(Config.MASK_PRODUCERCST, true);
            else if (sFieldName == _UOM)
                setUserChanged(Config.MASK_CUSTUOM, true);
            else if (sFieldName == _PUOM)
                setUserChanged(Config.MASK_TIPUOM, true);
            else if (sFieldName == _ProUOM)
                setUserChanged(Config.MASK_PRODUCERUOM, true);
            else if (sFieldName == _CstWgt)
                setUserChanged(Config.MASK_TIPCHRGWGT, true);
            else if (sFieldName == _HlSQty)
                setUserChanged(Config.MASK_HAULCHRQTY, true);
            else if (sFieldName == _CusQty)
                setUserChanged(Config.MASK_HAULCHRQTY, true);
            else if (sFieldName == _TipWgt)
                setUserChanged(Config.MASK_TIPCSTWGT, true);
            else if (sFieldName == _OrdWgt)
                setUserChanged(Config.MASK_HAULCSTQTY, true);
            else if (sFieldName == _ProWgt)
                setUserChanged(Config.MASK_PRODUCERCSTWGT, true);
            else if (sFieldName == _TRdWgt)
                setUserChanged(Config.MASK_TIPCSTREADWGT, U_TRdWgt != 0);
            else if (sFieldName == _TAUOMQt)
                setUserChanged(Config.MASK_TIPCSTAUOMQt, true);
            //MA Start 20-10-2014
            else if (sFieldName == _CarWgt)
                setUserChanged(Config.MASK_HAULCSTQTY, true);
            //MA End 20-10-2014

        }


        public override void doSetFieldHasChanged(int iRow, string sFieldname, object oOldValue, object oNewValue) {
            base.doSetFieldHasChanged(iRow, sFieldname, oOldValue, oNewValue);
            if (miDoHandleChange > 0 || (moParentWOQ != null && moParentWOQ.DoBlockUpdateTrigger))
                return;
            DoBlockUpdateTrigger = true;
            try {
                if (this.moParentWOQ != null) {
                    if (sFieldname == _WasFDsc && (oNewValue == null || oNewValue.ToString() == string.Empty) && U_WasCd != string.Empty) {
                        U_WasCd = "";
                        U_WasDsc = "";
                        //doGetAllPrices();
                        //doCalculateAllRowsTotal(false);
                    } else if (sFieldname == _AdtnlItm && oNewValue.ToString() != string.Empty && !oOldValue.Equals(oNewValue) && U_WasCd != string.Empty) {
                        doGetAllPrices();
                        doCalculateAllRowsTotal(false);

                    } else if (sFieldname == _WasCd && !oOldValue.Equals(oNewValue) && oOldValue.ToString() != "*") {
                        if (oNewValue == null || oNewValue.ToString() == string.Empty) {
                            U_WasFDsc = "";
                            U_WasDsc = "";
                        } else {
                            /*if (doFillByDisposalRoute("")) {
                                doGetAllPrices();
                                doCalculateAllRowsTotal(false);
                            }*/
                            if (string.IsNullOrWhiteSpace(U_RouteCd)) {
                                doGetAllPrices(true, true);
                            } else {
                                doGetAllPrices(false, true);
                            }

                            doCalculateAllRowsTotal(false);
                            AddLabTaskFlagIfRequired(oNewValue);
                        }
                    } else if (sFieldname == _WasDsc && !oOldValue.Equals(oNewValue)) {
                        if (oNewValue == null || oNewValue.ToString() == string.Empty) {
                            U_WasCd = "";
                            U_WasFDsc = "";
                        }
                    } else if (sFieldname == _CarrCd && !oOldValue.Equals(oNewValue)) {
                        if (oNewValue == null || oNewValue.ToString().Trim() == string.Empty) {
                            //DoBlockUpdateTrigger=true;
                            U_CarrNm = "";
                            //doGetAllPrices();
                            //doCalculateAllRowsTotal(false);
                            //miDoHandleChange--;
                        } else if (!oNewValue.ToString().Contains("*")) {
                            doGetAllPrices();
                            DoBlockUpdateTrigger = true;
                            string moCurrentCarrierCd = oNewValue.ToString();
                            object moCurrentCarrierNm = Config.INSTANCE.getValueFromBP(moCurrentCarrierCd, "CardName");
                            if (!string.IsNullOrWhiteSpace((string)moCurrentCarrierNm)) {


                                doBookmark();
                                while (next()) {
                                    if (U_AdtnlItm == "") {

                                        if (U_CarrCd == "") {
                                            //if (CurrentRowIndex < Count-1) {





                                            U_CarrCd = moCurrentCarrierCd;
                                            U_CarrNm = moCurrentCarrierNm.ToString();

                                            doGetAllPrices();
                                            //}
                                        }


                                    }
                                }
                                last();

                                doSetDefaultValue(_CarrCd, U_CarrCd);
                                doSetDefaultValue(_CarrNm, U_CarrNm);
                                doRecallBookmark();
                            }








                            DoBlockUpdateTrigger = false;
                            doCalculateAllRowsTotal(false);

                        }
                    } else if (sFieldname == _CarrNm && !oOldValue.Equals(oNewValue)) {
                        if (oNewValue == null || oNewValue.ToString().Trim() == string.Empty) {
                            //DoBlockUpdateTrigger=true;
                            U_CarrCd = "";
                            //doGetAllPrices();
                            //doCalculateAllRowsTotal(false);
                            //miDoHandleChange--;
                        } else if (oNewValue != null && oNewValue.ToString() != string.Empty && !oNewValue.ToString().Contains("*")) {
                            //    doGetAllPrices();
                            //    doCalculateAllRowsTotal(false);
                            DoBlockUpdateTrigger = true;
                            string moCurrentCarrier = oNewValue.ToString();
                            doBookmark();
                            while (next()) {
                                if (U_AdtnlItm == "") {
                                    if (U_CarrNm == "") {
                                        U_CarrNm = moCurrentCarrier;
                                    }
                                }
                            }
                            last();

                            doSetDefaultValue(_CarrNm, U_CarrNm);

                            doRecallBookmark();
                            miDoHandleChange--;
                        }
                    } else if (sFieldname == _Tip && !oOldValue.Equals(oNewValue)) {
                        if (oNewValue == null || oNewValue.ToString().Trim() == string.Empty) {
                            U_TipNm = "";
                        } else if (!oNewValue.ToString().Contains("*")) {
                            doGetAllPrices();
                            string moCurrentDisposalCd = oNewValue.ToString();
                            object moCurrentDisposalNm = Config.INSTANCE.getValueFromBP(moCurrentDisposalCd, "CardName");


                            if (!string.IsNullOrWhiteSpace((string)moCurrentDisposalNm)) {
                                doBookmark();
                                while (next()) {
                                    if (U_AdtnlItm == "") {

                                        if (U_Tip == "") {




                                            U_Tip = moCurrentDisposalCd;
                                            U_TipNm = moCurrentDisposalNm.ToString();

                                            doGetAllPrices();
                                        }


                                    }
                                }
                                last();
                                doSetDefaultValue(_Tip, U_Tip);
                                doSetDefaultValue(_TipNm, U_TipNm);
                                doRecallBookmark();
                            }









                            DoBlockUpdateTrigger = false;

                            doCalculateAllRowsTotal(false);
                        }
                    } else if (sFieldname == _TipNm && !oOldValue.Equals(oNewValue)) {
                        if (oNewValue == null || oNewValue.ToString().Trim() == string.Empty) {
                            U_Tip = "";
                        } else if (oNewValue != null && oNewValue.ToString() != string.Empty && !oNewValue.ToString().Contains("*")) {
                            DoBlockUpdateTrigger = true;
                            string moCurrentDisposal = oNewValue.ToString();
                            doBookmark();
                            while (next()) {
                                if (U_AdtnlItm == "") {
                                    if (U_TipNm == "") {
                                        U_TipNm = moCurrentDisposal;
                                    }
                                }
                            }
                            last();

                            doSetDefaultValue(_TipNm, U_TipNm);

                            doRecallBookmark();
                            miDoHandleChange--;
                        }
                    } else if (sFieldname == _UOM && oNewValue != null && oNewValue.ToString() != string.Empty && !oOldValue.Equals(oNewValue)) {
                        doGetAllPrices();
                        //docalculateRowTotalCharge();
                        //docalculateRowTotalCost();
                        doCalculateAllRowsTotal(false);

                        //doUpdateMargins();
                        //doUpdateTotals();
                    } else if (sFieldname == _RDate && oNewValue != null && oNewValue.ToString() != string.Empty && oOldValue != oNewValue) {
                        DoBlockUpdateTrigger = true;
                        DateTime moCurrentJobType = (DateTime)oNewValue;
                        doBookmark();
                        while (next()) {
                            if (U_AdtnlItm == "") {
                                if (U_RDate == null || U_RDate.Year < 2000) {
                                    U_RDate = moCurrentJobType;
                                }
                            }
                        }
                        last();

                        doSetDefaultValue(_RDate, U_RDate);

                        doRecallBookmark();
                        miDoHandleChange--;

                    } else if (sFieldname == _JobTp && oNewValue != null && oNewValue.ToString() != string.Empty && !oOldValue.Equals(oNewValue)) {
                        doGetAllPrices();





                        doCalculateAllRowsTotal(false);
                        DoBlockUpdateTrigger = true;
                        string moCurrentJobType = oNewValue.ToString();
                        doBookmark();

                        while (next()) {
                            if (U_AdtnlItm == "") {
                                if (U_JobTp == "") {
                                    U_JobTp = moCurrentJobType;
                                }
                            }
                        }
                        //if (mbAutoAddEmptyLine == true) {
                        last();










                        doSetDefaultValue(_JobTp, U_JobTp);











                        doRecallBookmark();
                        miDoHandleChange--;





                    } else if (sFieldname == _ItemCd) {
                        if (oNewValue == null || oNewValue.ToString().Trim() == string.Empty) {
                            U_ItemDsc = "";
                        } else if (oNewValue.ToString() != string.Empty && !oOldValue.Equals(oNewValue) && oNewValue.ToString() != "*") {
                            doGetAllPrices();




                            doCalculateAllRowsTotal(false);





                        }
                    } else if (sFieldname == _ItemDsc) {
                        if (oNewValue == null || oNewValue.ToString().Trim() == string.Empty) {
                            U_ItemCd = "";
                        }

















                    } else if (sFieldname == _TipCost && oOldValue != null && !oOldValue.Equals(oNewValue)) {
                        //doUpdateMargins();
                        doCalculateTipCostTotal();
                        doCalculateMUMargins();
                        doCalculateGMargins();
                        //docalculateRowTotalCost();
                        doCalculateAllRowsTotal(false);
                        //doUpdateTotals();
                    } else if (sFieldname == _OrdCost && oOldValue != null && !oOldValue.Equals(oNewValue)) {
                        //doUpdateMargins();
                        //doCalculateTipCostTotal();
                        doCalculateHaulageCostTotals();
                        doCalculateMUMargins();
                        doCalculateGMargins();
                        //docalculateRowTotalCost();
                        doCalculateAllRowsTotal(false);
                        //doUpdateTotals();

                    } else if (sFieldname == _TCharge && oOldValue != null && !oOldValue.Equals(oNewValue)) {
                        //Update margins and totals
                        docalculateTipChargeTotals();
                        doCalculateMUMargins();
                        doCalculateGMargins();
                        //docalculateRowTotalCharge();
                        doCalculateAllRowsTotal(false);
                        //doUpdateMargins();
                        //doUpdateTotals();
                    } else if (sFieldname == _CusChr && oOldValue != null && !oOldValue.Equals(oNewValue)) {
                        //Update margins and totals
                        doCalculateHaulageChargeTotals();
                        doCalculateMUMargins();
                        doCalculateGMargins();
                        //docalculateRowTotalCharge();
                        doCalculateAllRowsTotal(false);
                        //doUpdateMargins();
                        //doUpdateTotals();
                    } else if (sFieldname == _LstPrice && oOldValue != null && !oOldValue.Equals(oNewValue)) {
                        //Update margins and totals
                        doCalculateMUMargins();
                        doCalculateGMargins();
                        doCalculateAllRowsTotal(false);

                        //doUpdateMargins();
                        //doUpdateTotals();
                    } else if (sFieldname == _TChrgVtRt && !oOldValue.Equals(oNewValue)) {
                        doCalculateChargeVat();
                        doCalculateAllRowsTotal(false);
                    } else if (sFieldname == _TCostVtRt && !oOldValue.Equals(oNewValue)) {
                        doCalculateCostVat();
                        doCalculateAllRowsTotal(false);
                    } else if (sFieldname == _ExpLdWgt && oOldValue!=oNewValue) {
                        doGetAllPrices();
                        // docalculateRowTotalCharge();                        
                        doCalculateAllRowsTotal(false);

                    } /*else if (sFieldname == _MnMargin && !oOldValue.Equals(oNewValue)) {
                        //Update margins and totals
                        doCalculateChargePriceFromManualMargin();
                        doCalculateGMargins();

                        docalculateTipChargeTotals();
                        doCalculateTipCostTotal();
                        doCalculateAllRowsTotal(false);

                        //doUpdateMargins();
                        //doUpdateTotals();
                    } */
                    else if (sFieldname == _RouteCd && oOldValue != (oNewValue)) {

                        if (oNewValue == null || string.IsNullOrWhiteSpace(oNewValue.ToString())) {
                            DoBlockUpdateTrigger = true;
                            U_RtCdCode = "";
                            doGetAllPrices(true, false);
                            miDoHandleChange--;

                        } //else if (oNewValue.ToString().IndexOf("*")==-1)
                        //    doFillByDisposalRoute(U_RouteCd);
                        //else if (sFieldname == _RtCdCode && oNewValue!=null && oNewValue.ToString()!=string.Empty && oOldValue != (oNewValue)) {


                    } else if (sFieldname == _RtCdCode && oOldValue != (oNewValue)) {
                        if (string.IsNullOrWhiteSpace(U_RouteCd)) {

                            DoBlockUpdateTrigger = true;
                            U_RtCdCode = "";
                            miDoHandleChange--;
                        } else if (!string.IsNullOrWhiteSpace(U_RouteCd) && !string.IsNullOrWhiteSpace(U_RtCdCode)) {
                            DoBlockUpdateTrigger = true;
                            doFillByDisposalRouteCode(U_RtCdCode);
                            doCalculateAllRowsTotal(false);
                            DoBlockUpdateTrigger = false;
                        }


                        //else if (sFieldname == _RtCdCode && oNewValue!=null && oNewValue.ToString()!=string.Empty && oOldValue != (oNewValue)) {


                    } else if (sFieldname == _Sample && (oOldValue != oNewValue)) {// || oOldValue.ToString()==string.Empty)){
                        if ((oOldValue == null || oOldValue.ToString() == "" || oOldValue.ToString() == "N") && oNewValue.ToString() == "Y" && U_SampleRef == string.Empty) {
                            //create sample ref
                            doCreateSampleTask(0, "");
                        } else if ((oOldValue != null && oOldValue.ToString() == "Y" && oNewValue.ToString() == "N" && U_SampleRef != string.Empty)) {
                            DoBlockUpdateTrigger = true;
                            IDH_LABTASK oLabTask = new IDH_LABTASK();
                            if (oLabTask.getByKey(U_SampleRef.Trim()) && (oLabTask.U_Saved == 1)) {
                                if (moParentWOQ.Handler_ExternalTriggerWithData != null)
                                    moParentWOQ.Handler_ExternalTriggerWithData(this, "LAB00001");
                                U_Sample = "Y";
                                //send alert to make it cancel if they want from lab module
                            }
                            miDoHandleChange--;

                        }
                    }
                    if (moParentWOQ != null && (sFieldname == _ItemCd || sFieldname == _WasCd || sFieldname == _WasDsc || sFieldname == _WasFDsc
                        || sFieldname == _Tip || sFieldname == _TipNm || sFieldname == _CarrCd || sFieldname == _WstGpNm) && oNewValue != null && !string.IsNullOrWhiteSpace(oNewValue.ToString())) {
                        moParentWOQ.FormHasDatatoSave = true;
                    }
                }
            } catch (Exception ex) {
                throw new Exception("Field: [" + sFieldname + "] OValue: [" + oOldValue + "] NValue: [" + oNewValue + "] " + ex.Message);
                //throw new Exception("Field: [" + sFieldName + "] OValue: [" + oOldValue + "] NValue: [" + oNewValue + "] ExtraFlag: [" + sExtraFlag + "] " + e.Message);
            } finally {
                miDoHandleChange--;
            }

        }
        public override string doValidate_WasCd(object oValue) {
            try {
                if (Config.INSTANCE.getParameterAsBool("DRREQWOQ", false) && !string.IsNullOrWhiteSpace(U_WasCd) && string.IsNullOrWhiteSpace(U_RouteCd)) {
                    if (!Config.INSTANCE.isWasteMaterialItem(U_WasCd)) {
                        //com.idh.bridge.DataHandler.INSTANCE.doResExceptionError("Disposal route must be set for the row.", "ERUSDRVE",
                        //                   new string[] { idh.bridge.Translation.getTranslatedWord("Disposal Route:") + '[' + Code + ']' });
                        return Translation.getTranslatedMessage("WOQ0001", "Disposal route must be set for the row" + Code);
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "ERUSDRVE",
                  new string[] { idh.bridge.Translation.getTranslatedWord("Disposal Route:") + '[' + Code + ']' });
                // return false;
            }
            return base.doValidate_WasCd(oValue);
        }
        public override bool doAddDataRow(bool bDoValidation, string sComment, bool bDoChildren) {
            doSaveSampleTask();
            return base.doAddDataRow(bDoValidation, sComment, bDoChildren);
        }

        public override bool doUpdateDataRow(bool bDoValidation, string sComment, bool bDoChildren) {
            doSaveSampleTask();
            return base.doUpdateDataRow(bDoValidation, sComment, bDoChildren);
        }

        public bool doPostAddUpdateProcess() {
            if (!DataHandler.INSTANCE.IsInTransaction())
                IDH_LABTASK.dosendAllAlerttoUser();
            return true;
        }
        #endregion

        #region Pricing
        public void doGetAllPrices() {
            doGetAllPrices(true, true);
        }
        public void doGetAllPrices(bool doCalcCostPrices, bool doCalcChargePrices) {
            try {
                DoBlockUpdateTrigger = true;
                if (doCalcChargePrices)
                    doGetChargePrice();
                if (doCalcCostPrices == true) {
                    doGetCostPrice();
                    doGetHaulageCost();
                }

                docalculateTipChargeTotals();
                doCalculateHaulageChargeTotals();

                doCalculateTipCostTotal();
                doCalculateHaulageCostTotals();


                //docalculateRowTotalCharge();
                //docalculateRowTotalCost();
                //doGetHaulageChargePrice();
                //doGetHaulageCostPrice();
                doCalculateMUMargins();
                doCalculateGMargins();
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXEURP", new string[] { Code });
            } finally {
                miDoHandleChange--;
            }

        }

        //public bool doRefreashAllRowValueforField(string sFieldName, object sValue, bool bTriggerFieldChaage) {
        //    bool bResult = true;
        //    if (!bTriggerFieldChaage)
        //        DoBlockUpdateTrigger=true;
        //    doBookmark();
        //    try {
        //        first();
        //        while (next()) {
        //            try {
        //                dodoApplyDefaultValue(sFieldName, sValue);

        //                setValueForAllRows(
        //            } catch (Exception ex) {
        //                //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error updating the Row [" + Code +"] Prices.");
        //                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXEURP", new string[] { Code });
        //            }
        //        }
        //    } catch (Exception ex) {
        //        throw (ex);
        //    } finally {
        //        doRecallBookmark();
        //        //gotoRow(iCurrIndex);
        //        if (!bTriggerFieldChaage)
        //            miDoHandleChange--;
        //    }
        //    //doUpdateTotals();
        //    return bResult;
        //}
        public bool doRefreashAllRowPrices(bool bUpdateEachRowData, bool bForcePriceUpdate) {
            bool bResult = true;
            doBookmark();
            try {
                first();
                while (next()) {
                    try {
                        doGetAllPrices();
                        //doUpdateMargins();

                        //for now no need; in future we may require to enhance customizations
                        //if (bUpdateEachRowData) {
                        //    if (isChangedRow())
                        //        bResult &= doUpdateDataRow("Price Refreshed");
                        //    else if (mbDoUpdateInfo) {
                        //        DataHandler.INSTANCE.doInfo("doRefreashAllRowPrices", "Info", "WOR Up to Date - " + Code);
                        //    }
                        //}
                    } catch (Exception ex) {
                        //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error updating the Row [" + Code +"] Prices.");
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXEURP", new string[] { Code });
                    }
                }
            } catch (Exception ex) {
                throw (ex);
            } finally {
                doRecallBookmark();
                //gotoRow(iCurrIndex);
            }
            //doUpdateTotals();
            return bResult;
        }
        public void doCalculateMUMargins() {
            if (U_TipCost == 0 || U_TipCost == U_TCharge)
                U_MnMargin = 0.0;
            if (U_TipCost != 0.0) {
                U_MnMargin = ((U_TCharge - U_TipCost) / U_TipCost * 100.0);
            }
        }
        public void doCalculateGMargins() {
            if (U_LstPrice == 0 || U_TipCost == U_TCharge)
                U_MnMargin = 0.0;
            if (U_TCharge != 0.0 || U_CusChr != 0.0) {
                U_GMargin = ((U_TCharge + U_CusChr - U_TipCost - U_OrdCost) / (U_TCharge + U_CusChr) * 100.0);

            } else
                U_GMargin = 0.0;

        }

        #region Charge Pricing
        protected void doGetChargePrice() {
            if ((U_WasCd == string.Empty) || (moParentWOQ.U_CardNM == string.Empty))//&& U_WasFDsc == string.Empty (U_RouteCd != string.Empty) || 
                return;

            if (U_AdtnlItm == "") {

                bool bDoTPrice = true;
                bool bDoHPrice = true;
                if (getUserChanged(Config.MASK_TIPCHRG)) {
                    string sPriceLookUpControl = Config.ParameterWithDefault("PRLUCO", "P");
                    if (sPriceLookUpControl == "U") {
                        bDoTPrice = false;
                    } else if (sPriceLookUpControl == "P") {

                        if (moParentWOQ.Handler_ExternalTriggerWithData_ShowYesNO != null) {
                            if (moParentWOQ.Handler_ExternalTriggerWithData_ShowYesNO(this, "PRCTPCST", null, 1) == 2) {


                                bDoTPrice = false;
                            }
                        } /*else {
                            docalculateTipChargeTotals();//bDoAdjustWeights ? 'S' : 'R');
                            bDoTPrice = false;
                        }*/
                    }
                }


                if (getUserChanged(Config.MASK_HAULCHRG)) {
                    string sPriceLookUpControl = Config.ParameterWithDefault("PRLUCO", "P");
                    if (sPriceLookUpControl == "U") {
                        bDoHPrice = false;
                    } else if (sPriceLookUpControl == "P") {

                        if (moParentWOQ.Handler_ExternalTriggerWithData_ShowYesNO != null) {
                            if (moParentWOQ.Handler_ExternalTriggerWithData_ShowYesNO(this, "PRCHACST", null, 1) == 2) {


                                bDoHPrice = false;
                            }
                        } /*else {
                            doCalculateHaulageChargeTotals();//bDoAdjustWeights ? 'S' : 'R');
                            bDoHPrice = false;
                        }*/
                    }
                }

                Prices oPrices = null;
                oPrices = moCIP.doGetJobChargePrices("WO", U_JobTp, U_ItmGrp, U_ItemCd,
                                         this.moParentWOQ.U_CardCd, U_ExpLdWgt, U_UOM, U_WasCd, U_Tip,
                                         this.moParentWOQ.U_Address, this.moParentWOQ.U_RSDate, this.moParentWOQ.U_Branch.ToString(),
                                         this.moParentWOQ.U_ZpCd);
                if (oPrices != null) {
                    DoBlockUpdateTrigger = true;
                    try {
                        U_LstPrice = oPrices.TipPrice;
                        if (U_TChrgVtRt != oPrices.TipVat)
                            U_TChrgVtRt = oPrices.TipVat;
                        U_TChrgVtGrp = oPrices.TipVatGroup == null ? "" : oPrices.TipVatGroup;

                        if (bDoTPrice) {
                            U_TCharge = U_LstPrice;
                            msCCTChr = oPrices.TipChargeCalc;
                            mdTOffset = oPrices.TipOffsetWeight;
                            setUserChanged(Config.MASK_TIPCHRG, false);
                        }


                        if (U_HChrgVtRt != oPrices.HaulageVat)
                            U_HChrgVtRt = oPrices.HaulageVat;
                        //    bVatChanged = true;
                        //} else
                        //    bVatChanged = false;
                        U_HChrgVtGrp = oPrices.HaulageVatGroup == null ? "" : oPrices.HaulageVatGroup;
                        if (bDoHPrice) {
                            double dPrice = oPrices.HaulPrice;
                            if (oPrices.Band == true) {
                                dPrice = dPrice * doGetFactor();
                            }
                            U_CusChr = dPrice;
                            msCCHChr = oPrices.HaulageChargeCalc;
                            mdHOffset = oPrices.HaulageOffsetWeight;
                            setUserChanged(Config.MASK_HAULCHRG, false);
                            if (msCCHChr == "WEIGHT")
                                U_HaulAC = "Y";
                        }


                    } catch (Exception ex) {
                        throw ex;
                    } finally {
                        miDoHandleChange--;
                    }

                }
            } else if (U_AdtnlItm == "A") {
                if (getUserChanged(Config.MASK_TIPCHRG))
                    return;
                AdditionalPrice oPrice;

                oPrice = Config.INSTANCE.doGetAdditionalChargePrices("WO", U_JobTp, U_ItmGrp, U_ItemCd,
                    this.moParentWOQ.U_CardCd, U_ExpLdWgt, U_UOM, doGetWasteCodeOfAdditionalItem(), this.moParentWOQ.U_Address,
                    this.moParentWOQ.U_RSDate, this.moParentWOQ.U_Branch.ToString(), U_WasCd,
                    this.moParentWOQ.U_ZpCd);
                DoBlockUpdateTrigger = true;
                try {

                    if (oPrice != null) {
                        U_LstPrice = oPrice.mdPrice;
                        U_TCharge = U_LstPrice;

                        setUserChanged(Config.MASK_TIPCHRG, false);

                    }
                } catch (Exception ex) {
                    throw ex;
                } finally {
                    miDoHandleChange--;
                }
            }
        }

        //public void doCalculateChargePriceFromManualMargin() {
        //    if (U_MnMargin == 0) {
        //        U_TCharge = U_LstPrice;

        //    } else {




        //        U_TCharge = U_TipCost + (U_TipCost * U_MnMargin / 100.0);

        //    }

        //    setUserChanged(Config.MASK_TIPCHRG, true);

        //}


        private void docalculateTipChargeTotals() {
            DoBlockUpdateTrigger = true;
            try {
                if (U_AdtnlItm == "A") {
                    U_TCTotal = U_ExpLdWgt * U_TCharge;
                } else {
                    if (msCCTChr.ToUpper().Equals("QTY1")) {
                        U_CstWgt = 1;
                    } else if (U_ExpLdWgt > 0 && msCCTChr.ToUpper().Equals("OFFSET")) {
                        if (mdTOffset < U_ExpLdWgt) {
                            U_CstWgt = U_ExpLdWgt - mdTOffset;
                        } else {
                            U_CstWgt = 0;
                        }
                    } else {
                        U_CstWgt = U_ExpLdWgt;
                    }

                    if (msCCTChr.ToUpper().Equals("FIXED")) {
                        if (U_CstWgt == 0)
                            U_TCTotal = 0;
                        else
                            U_TCTotal = U_TCharge;
                    } else {
                        U_TCTotal = U_CstWgt * U_TCharge;
                    }
                }
            } catch (Exception ex) {
                throw ex;
            } finally {
                miDoHandleChange--;
            }
            doCalculateChargeVat();
        }

        public void doCalculateHaulageChargeTotals() {
            DoBlockUpdateTrigger = true;
            try {
                if (U_AdtnlItm == "A") {
                    U_Price = U_HlSQty * U_CusChr;
                } else {
                    if (U_HaulAC != null && U_HaulAC.Equals("Y"))
                        U_HlSQty = U_ExpLdWgt;//U_RdWgt;
                    else {
                        if (msCCHChr == "QTY1") {
                            U_HlSQty = 1;
                        } else if (msCCHChr == "WEIGHT") {
                            U_HlSQty = U_CstWgt;//U_CstWgt;
                        } else
                            U_HlSQty = U_CusQty;
                    }
                    U_Price = U_HlSQty * U_CusChr;
                }
            } catch (Exception ex) {
                throw ex;
            } finally {
                miDoHandleChange--;
            }
            doCalculateChargeVat();
        }
        public void doCalculateChargeVat() {
            DoBlockUpdateTrigger = true;
            try {
                if (moParentWOQ.U_NoChgTax == "Y") {
                    U_TaxAmt = 0;
                } else {
                    double dChargeTSubTotal;
                    double dChargeHSubTotal;

                    if (U_Discnt == 0) {
                        dChargeTSubTotal = U_TCTotal;
                        dChargeHSubTotal = U_Price;
                    } else {
                        dChargeTSubTotal = U_TCTotal - (U_TCTotal * (U_Discnt / 100));
                        dChargeHSubTotal = U_Price - (U_Price * (U_Discnt / 100));
                    }

                    /**
                     * Lookup the Vat if it is not set
                     */
                    if (F_CustomerCode != null && F_CustomerCode.Length > 0) {
                        if (F_WasteCode != null && F_WasteCode.Length > 0 && U_TCTotal != 0) {
                            if (U_TChrgVtGrp == null || U_TChrgVtGrp.Length == 0) {
                                U_TChrgVtGrp = Config.INSTANCE.doGetSalesVatGroup(F_CustomerCode, F_WasteCode);
                                U_TChrgVtRt = Config.INSTANCE.doGetVatRate(U_TChrgVtGrp, U_RDate);
                            }
                        }
                        if (F_ContainerCode != null && F_ContainerCode.Length > 0 && U_Price != 0) {
                            if (U_HChrgVtGrp == null || U_HChrgVtGrp.Length == 0) {
                                U_HChrgVtGrp = Config.INSTANCE.doGetSalesVatGroup(F_CustomerCode, F_ContainerCode);
                                U_HChrgVtRt = Config.INSTANCE.doGetVatRate(U_HChrgVtGrp, U_RDate);
                            }
                        }
                    }

                    U_TaxAmt = 0;

                    if (U_TChrgVtRt != 0)
                        U_TaxAmt = (dChargeTSubTotal * (U_TChrgVtRt / 100));

                    if (U_HChrgVtRt != 0)
                        U_TaxAmt = U_TaxAmt + (dChargeHSubTotal * (U_HChrgVtRt / 100));
                    //}
                }
            } catch (Exception ex) {
                throw ex;
            } finally {
                miDoHandleChange--;
            }
            U_TaxAmt += U_TAddChVat;
            doCalculateChargeTotal();
        }

        public void doCalculateChargeTotal() {
            U_Total = ((U_TCTotal + U_Price) - U_DisAmt) + U_AddCharge + U_TaxAmt - U_ValDed;
        }
        #endregion

        #region Cost Pricing
        protected void doGetCostPrice() {
            //if ((U_TipCost != 0) || (U_WasCd == string.Empty) || (moParentWOQ.U_CardNM == string.Empty ) ||
            //   !((U_Tip != null && U_Tip.Length > 0) && (U_ItemCd != null && U_ItemCd.Length > 0)))//&& U_WasFDsc == string.Empty
            //   return;
            if ((U_WasCd == string.Empty) || (moParentWOQ.U_CardNM == string.Empty) || !string.IsNullOrWhiteSpace(U_RouteCd))//&& U_WasFDsc == string.Empty
                return;
            if (U_AdtnlItm == "") {









































                Prices oPrices = null;

                oPrices = moSIPP.doGetJobCostPrice(
                    "WO", U_JobTp, U_ItmGrp, U_ItemCd, U_Tip, U_ExpLdWgt,
                    U_UOM, U_WasCd, this.moParentWOQ.U_CardCd,
                    this.moParentWOQ.U_Address, "", this.moParentWOQ.U_RSDate,
                    this.moParentWOQ.U_Branch.ToString(), this.moParentWOQ.U_ZpCd);
                DoBlockUpdateTrigger = true;
                try {
                    if (oPrices != null) {
                        if (U_Tip != string.Empty) {
                            msCCTCos = oPrices.TipChargeCalc;
                            U_TCostVtRt = oPrices.TipVat;
                            U_TCostVtGrp = oPrices.TipVatGroup;
                            if (!getUserChanged(Config.MASK_TIPCST)) {
                                U_TipCost = oPrices.TipPrice;
                                U_DispCoCc = (oPrices.TipCurrency == null) ? "" : oPrices.TipCurrency;
                                mdTCstOffset = oPrices.TipOffsetWeight;
                                msCCTCos = "VARIABLE";
                            }

                        } else {
                            if (!getUserChanged(Config.MASK_TIPCST)) {
                                U_TipCost = 0;
                                mdTCstOffset = 0;
                            }
                            U_TCostVtRt = 0;
                            U_TCostVtGrp = "";
                            msCCTCos = "VARIABLE";
                            U_DispCoCc = "";
                        }
                        if (U_CarrCd != string.Empty)
                            msHCCTCos = oPrices.HaulageChargeCalc;
                        else
                            msHCCTCos = "STANDARD";

                    } else {
                        U_TCostVtRt = 0;
                        U_TCostVtGrp = "";
                        msCCTCos = "VARIABLE";
                        mdTCstOffset = 0;
                        U_DispCoCc = "";
                        msHCCTCos = "STANDARD";
                        // if (!getUserChanged(Config.MASK_TIPCST)) {
                        U_TipCost = 0;

                        //}



                    }
                } catch (Exception ex) {
                    throw ex;
                } finally {
                    miDoHandleChange--;
                }
            } else if (U_AdtnlItm == "A") {
                AdditionalPrice oPrice = null;
                if (U_Tip != string.Empty)
                    oPrice = Config.INSTANCE.doGetAdditionalCostPrices("WO", U_JobTp, U_ItmGrp, U_ItemCd,
                        U_Tip, U_ExpLdWgt, U_UOM, doGetWasteCodeOfAdditionalItem(), this.moParentWOQ.U_Address,
                        this.moParentWOQ.U_RSDate, this.moParentWOQ.U_Branch.ToString(), U_WasCd,
                        this.moParentWOQ.U_ZpCd, this.moParentWOQ.U_CardCd);
                DoBlockUpdateTrigger = true;
                try {
                    if (oPrice != null) {
                        U_TipCost = oPrice.mdPrice;
                        //msCCTCos = oPrices.TipChargeCalc;
                        U_TCostVtRt = oPrice.mdVat;
                        U_TCostVtGrp = oPrice.msVatGroup;
                        //msHCCTCos = oPrices.HaulageChargeCalc;
                        //msCCTCos = oPrices.TipChargeCalc;
                        //mdTCstOffset = oPrices.TipOffsetWeight;
                        U_DispCoCc = "";// oPrice.TipCurrency;
                    } else {
                        U_TipCost = 0;
                        U_TCostVtRt = 0;
                        U_TCostVtGrp = "";
                        U_DispCoCc = "";
                    }
                } catch (Exception ex) {
                    throw ex;
                } finally {
                    miDoHandleChange--;
                }
            }
        }
        private void doGetHaulageCost() {
            try {
                //if ((U_CarrCd!=null && U_CarrCd.Length > 0) && (U_ItemCd!=null && U_ItemCd.Length > 0) && (U_CustCd!=null && U_CustCd.Length > 0)) {
                //if ((U_RouteCd != string.Empty))
                //    return;

                //if (U_AdtnlItm == "" && (U_CarrCd != null && U_CarrCd.Length > 0) && (U_ItemCd != null && U_ItemCd.Length > 0) && (U_WasCd.Length > 0 || moParentWOQ.U_CardCd.Length > 0) && string.IsNullOrWhiteSpace( U_RouteCd)) {
                if (U_AdtnlItm == "" && (U_WasCd.Length > 0 || moParentWOQ.U_CardCd.Length > 0) && string.IsNullOrWhiteSpace(U_RouteCd)) {
                    Prices oPrices = null;
                    if (U_CarrCd != string.Empty)
                        oPrices = moSIPH.doGetJobCostPrice(
                            "WO",
                            U_JobTp,
                            U_ItmGrp, U_ItemCd,
                            U_CarrCd,
                            U_ExpLdWgt, //(bIsOneLine ? U_TipWgt : U_OrdWgt),
                            U_UOM, //(bIsOneLine ? U_PUOM : null), 
                            U_WasCd, moParentWOQ.U_CardCd,
                            moParentWOQ.U_Address, this.moParentWOQ.U_RSDate,
                            U_Branch, moParentWOQ.U_ZpCd);
                    DoBlockUpdateTrigger = true;
                    try {
                        if (oPrices != null) {
                            U_OrdCost = oPrices.HaulPrice;
                            msCCHCos = oPrices.HaulageChargeCalc;
                            U_HCostVtRt = oPrices.HaulageVat;
                            U_HCostVtGrp = oPrices.HaulageVatGroup;

                            //if (U_CarrCd == U_Tip) {
                            //    msCCTCos = oPrices.TipChargeCalc;
                            //}
                            mdHCstOffset = oPrices.HaulageOffsetWeight;
                            if (oPrices.HaulCurrency != null)
                                U_CarrCoCc = oPrices.HaulCurrency;
                            else
                                U_CarrCoCc = "";
                        } else {
                            U_OrdCost = 0;
                            U_HCostVtRt = 0;
                            U_HCostVtGrp = "";

                            msCCHCos = "VARIABLE";
                            mdHCstOffset = 0;
                            U_CarrCoCc = "";
                        }
                    } catch (Exception ex) {
                        throw ex;
                    } finally {
                        miDoHandleChange--;
                    }

                    //doCalculateHaulageCostTotals(bDoAdjustWeights ? 'S' : 'R');
                    setUserChanged(Config.MASK_HAULCST, false);
                    //return true;
                } else {
                    //doCalculateHaulageCostTotals(bDoAdjustWeights ? 'S' : 'R');
                }
            } catch (Exception exx) {
                throw exx;
            } finally {
                //if (Handler_CostHaulagePricesChanged != null) {
                //    if (SBOForm != null) {
                //        Handler_CostHaulagePricesChanged(this);
                //    }
                //}
            }
        }

        public void doCalculateTipCostTotal() {
            DoBlockUpdateTrigger = true;
            try {
                if (U_AdtnlItm == "A") {
                    U_TipTot = U_TipCost * U_ExpLdWgt;
                } else {
                    if (msCCTCos.ToUpper().Equals("QTY1")) {
                        U_TipWgt = 1;
                    } else if (FixedValues.isFoc(U_Status)) {
                        U_TipWgt = U_ExpLdWgt;
                    } else {
                        if (U_ExpLdWgt > 0 && msCCTCos.ToUpper().Equals("OFFSET")) {
                            if (mdTCstOffset < U_ExpLdWgt) {
                                U_TipWgt = U_ExpLdWgt - mdTCstOffset;
                            } else {
                                U_TipWgt = 0;
                            }
                        } else {
                            U_TipWgt = U_ExpLdWgt;
                        }
                    }

                    if (msCCTCos.Equals("FIXED")) {
                        if (U_TipWgt == 0) {
                            U_TipTot = 0;
                        } else {
                            U_TipTot = U_TipCost;
                        }
                    } else {
                        U_TipTot = U_TipCost * U_TipWgt;
                    }
                }
            } catch (Exception ex) {
                throw ex;
            } finally {
                miDoHandleChange--;
            }
            doCalculateCostVat();
        }

        public void doCalculateHaulageCostTotals() {
            DoBlockUpdateTrigger = true;
            try {
                if (U_AdtnlItm == "A") {
                    U_OrdTot = U_OrdCost * U_OrdWgt;
                } else {
                    if (U_HaulAC != null && U_HaulAC.Equals("Y"))
                        U_OrdWgt = U_ExpLdWgt;// U_TRdWgt;
                    else {
                        if (msCCHCos.Equals("FIXED")) {
                            if (U_OrdWgt == 0) {
                                U_OrdTot = 0;
                            } else {
                                U_OrdTot = U_OrdCost;
                            }
                        } else {
                            if (msCCHCos == "QTY1") {
                                U_OrdWgt = 1;
                            } else if (msCCHCos == "WEIGHT") {
                                U_OrdWgt = U_ExpLdWgt;// U_TipWgt;
                            } else
                                U_OrdWgt = 1;
                        }
                    }
                    U_OrdTot = U_OrdCost * U_OrdWgt;
                }
            } catch (Exception ex) {
                throw ex;
            } finally {
                miDoHandleChange--;
            }
            doCalculateCostVat();
        }

        public void doCalculateCostVat() {
            DoBlockUpdateTrigger = true;
            try {
                U_VtCostAmt = 0;
                if (string.IsNullOrWhiteSpace(moParentWOQ.U_NoChgTax) || moParentWOQ.U_NoChgTax != "Y") {


                    /**t
                    * Look up the vats if it is not set
                    */
                    if (U_CarrCd != null && U_CarrCd.Length > 0 && F_ContainerCode != null && F_ContainerCode.Length > 0 && U_OrdTot != 0) {
                        if (U_HCostVtGrp == null || U_HCostVtGrp.Length == 0) {
                            U_HCostVtGrp = Config.INSTANCE.doGetPurchaceVatGroup(U_CarrCd, F_ContainerCode);
                            U_HCostVtRt = Config.INSTANCE.doGetVatRate(U_HCostVtGrp, U_RDate);
                        }
                    }


                    if (U_Tip != null && U_Tip.Length > 0 && F_WasteCode != null && F_WasteCode.Length > 0 && U_TipTot != 0) {
                        if (U_TCostVtGrp == null || U_TCostVtGrp.Length == 0) {
                            U_TCostVtGrp = Config.INSTANCE.doGetSalesVatGroup(U_Tip, F_WasteCode);
                            U_TCostVtRt = Config.INSTANCE.doGetVatRate(U_TCostVtGrp, U_RDate);
                        }
                    }


                    if (U_ProCd != null && U_ProCd.Length > 0 && F_WasteCode != null && F_WasteCode.Length > 0 && U_PCTotal != 0) {
                        if (U_TCostVtGrp == null || U_TCostVtGrp.Length == 0) {
                            U_TCostVtGrp = Config.INSTANCE.doGetSalesVatGroup(U_ProCd, F_WasteCode);
                            U_TCostVtRt = Config.INSTANCE.doGetVatRate(U_TCostVtGrp, U_RDate);
                        }
                    }


                    if (U_TCostVtRt != 0) {
                        U_VtCostAmt = (U_PCTotal * (U_TCostVtRt / 100)) +
                                        (U_TipTot * (U_TCostVtRt / 100));

                    }

                    if (U_HCostVtRt != 0) {
                        U_VtCostAmt = U_VtCostAmt + (U_OrdTot * (U_HCostVtRt / 100));
                    }
                }
                //			}
            } catch (Exception ex) {
                throw ex;
            } finally {
                miDoHandleChange--;
            }
            U_VtCostAmt += U_TAddCsVat;
            doCalculateCostTotal(); //LP Viljoen follow normal core logic - false);	
        }

        public void doCalculateCostTotal() {
            U_JCost = U_TipTot + U_OrdTot + U_PCTotal + U_AddCost + U_VtCostAmt + U_SLicCTo;

        }
        #endregion

        public bool doCalculateAllRowsTotal(bool bReCalsVAT) {

            bool bResult = true;
            if (this.moParentWOQ == null)
                return false;
            this.moParentWOQ.CalculateTotals = true;
            moParentWOQ.Handler_ExternalTriggerWithData(this, "IDH_CALTOT:True");
            ////CalculateTotals
            //double Tot_CostPrice = 0;


            //double Tot_ListPrice = 0;
            //double Tot_LPProfitnLose = 0;
            //double Tot_LPGMargin = 0;

            //double Tot_ChargesPrice = 0;
            //double Tot_CPProfitnLose = 0;
            //double Tot_CPMNMargin = 0;
            //double Tot_VatCharge = 0;
            //double Tot_VatChargeonListPrice = 0;
            //double Tot_VatCost = 0;


            //doBookmark();


            //try {
            //    first();
            //    while (next()) {

            //        try {
            //            if (moParentWOQ.U_NoChgTax == "Y") {
            //                DoBlockUpdateTrigger = true;



            //                U_TaxAmt = 0;
            //                U_VtCostAmt = 0;
            //                miDoHandleChange--;
            //            } else if (moParentWOQ.U_NoChgTax != "Y" && bReCalsVAT) {
            //                doCalculateCostVat();
            //                doCalculateChargeVat();

            //            }
            //            Tot_CostPrice += U_JCost;// U_PCTotal + U_TipTot+U_OrdTot+U_SLicCTo; //;

            //            Tot_ListPrice += (U_LstPrice * U_ExpLdWgt) + U_Price;

            //            Tot_ChargesPrice += (U_TCTotal + U_Price);
            //            Tot_VatChargeonListPrice += (U_TChrgVtRt / 100.0 * ((U_LstPrice * U_ExpLdWgt))) + (U_HChrgVtRt / 100.0 * U_Price);

            //            Tot_VatCharge += U_TaxAmt;
            //            Tot_VatCost += U_VtCostAmt;




            //        } catch (Exception ex) {
            //            //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error updating the Row [" + Code +"] Prices.");
            //            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXEURP", new string[] { Code });








            //        }
            //    }
            //} catch (Exception ex) {
            //    throw (ex);
















            //} finally {
            //    doRecallBookmark();
            //    //gotoRow(iCurrIndex);


            //}
            //Tot_LPProfitnLose = Tot_ListPrice + Tot_VatChargeonListPrice - Tot_CostPrice;//Tot_ListPrice -Tot_CostPrice;
            //Tot_LPGMargin = Tot_LPProfitnLose / (Tot_ListPrice + Tot_VatChargeonListPrice) * 100.0;//Tot_LPProfitnLose / (Tot_ListPrice + Tot_VatCharge) * 100.0;



            //Tot_CPProfitnLose = Tot_ChargesPrice + Tot_VatCharge - Tot_CostPrice; //; Tot_ChargesPrice + Tot_VatCharge - Tot_CostPrice - Tot_VatCost;
            //Tot_CPMNMargin = Tot_CPProfitnLose / (Tot_ChargesPrice + Tot_VatCharge) * 100.0; // Tot_CPProfitnLose / (Tot_ChargesPrice + Tot_VatCharge) * 100.0;














            //this.moParentWOQ.U_LstPrice = Tot_ListPrice + Tot_VatChargeonListPrice;
            //this.moParentWOQ.U_Cost = Tot_CostPrice;
            //this.moParentWOQ.U_LPProfit = Tot_LPProfitnLose;
            //this.moParentWOQ.U_LPGrossM = Tot_LPGMargin;


            //this.moParentWOQ.U_ChgPrice = Tot_ChargesPrice;
            //this.moParentWOQ.U_ChgVAT = Tot_VatCharge;

            //this.moParentWOQ.U_TCharge = Tot_ChargesPrice + Tot_VatCharge;

            //this.moParentWOQ.U_CPProfit = Tot_CPProfitnLose;
            //this.moParentWOQ.U_CPGrossM = Tot_CPMNMargin;












            return bResult;
        }

        private double doGetFactor() {
            return Config.INSTANCE.doGetFactor(moParentWOQ.U_ZpCd, this.moParentWOQ.U_CCardCd, U_ItemCd, U_ItmGrp);
        }

        public void setUserChanged(int iKey, bool bUserSet) {
            if (!bUserSet)
                U_MANPRC &= (short)(~iKey);
            else
                U_MANPRC |= (short)iKey;
        }

        public bool getUserChanged(int iKey) {
            return (U_MANPRC & iKey) > 0;
        }
        private string doGetWasteCodeOfAdditionalItem() {
            string sParentWasteCode = "";
            string sParentID = U_PLineID;
            doBookmark();
            try {
                first();
                while (next()) {
                    try {
                        if (sParentID == Code && U_AdtnlItm == "") {
                            sParentWasteCode = U_WasCd;
                            break;
                        }

                    } catch (Exception ex) {
                        //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error updating the Row [" + Code +"] Prices.");
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXEURP", new string[] { Code });
                    }
                }
            } catch (Exception ex) {
                throw (ex);
            } finally {
                doRecallBookmark();
                //gotoRow(iCurrIndex);
            }
            return sParentWasteCode;
        }
        #endregion
        #region Methods
        public bool AddLabTaskFlagIfRequired(object oNewValue) {//, object oOldValue
            if (oNewValue != null && U_AdtnlItm == "" && Config.INSTANCE.getParameterAsBool("ACTSMPRQ", false) && oNewValue.ToString() != string.Empty && oNewValue.ToString() != "*" && U_Sample != "Y") {
                //  OITM oitm = new OITM();
                //string sSampleReq = Config.INSTANCE.getValueFromOITMWithNoBuffer(oNewValue.ToString(), "U_SAMPLEREQ").ToString();
                // int iRet = oitm.getData("ItemCode='" + oNewValue.ToString() + "' And U_SAMPLEREQ='Y'", "");
                if (com.isb.enq.utils.General.doCheckIfLabTaskRequired(oNewValue.ToString())) {
                    miDoHandleChange++;
                    U_Sample = "Y";
                    //doCreateSampleTask();
                    miDoHandleChange--;
                    return true;
                }
            }
            return false;
        }
        public void doSaveSampleTask() {
            if (U_Sample == "Y") {
                if (string.IsNullOrEmpty(U_SampleRef)) {
                    doCreateSampleTask(1, "");
                } else {
                    IDH_LABTASK oLabTask = new IDH_LABTASK();
                    oLabTask.UnLockTable = true;
                    if (oLabTask.getByKey(U_SampleRef.Trim()) && ((oLabTask.U_Saved == 0 || oLabTask.U_DipRtCd != U_RouteCd) && oLabTask.U_ObjectCd == Code)) {
                        oLabTask.U_Saved = 1;
                        oLabTask.U_DipRtCd = U_RouteCd;
                        string sUser = oLabTask.U_AssignedTo;
                        oLabTask.doProcessData();
                        //here I am sending alert message
                        if (!string.IsNullOrEmpty(sUser))
                            IDH_LABTASK.dosendAlerttoUser(sUser, com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTM1", null), com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTS1", null), false, U_SampleRef);

                        IDH_LABITM oLabItem = new IDH_LABITM();
                        oLabItem.UnLockTable = true;
                        oLabItem.getByKey(oLabTask.U_LabItemRCd);
                        oLabItem.U_Saved = 1;
                        oLabItem.doProcessData();

                    }
                }
            }
        }

        public static bool doCreateSampleTask(int iSave, string WasCd, string WasDsc, string WasFDsc, string UOM, int Quantity, string Comment,
            string WOQLineNum, string sWOQID, string sCardCode, string CardName, ref string sSampleRef, ref string sSampleStatus, string sLabTasktoDuplicate,
            string sDispRouteCode) {
            bool bResult = false;
            //string sSampleRef = "", sSampleStatus = "";
            bResult = com.isb.enq.utils.General.doCreateLabItemNTask(WasCd,
                WasDsc,
                WasFDsc,
                UOM,
                Quantity.ToString(), Comment,
                WOQLineNum, sWOQID, "WOQ",
                sCardCode, CardName, "", false, iSave, ref sSampleRef, ref sSampleStatus, sLabTasktoDuplicate, sDispRouteCode);
            return bResult;
        }
        public bool doCreateSampleTask(int iSave, string sLabTasktoDuplicate) {
            try {
                DoBlockUpdateTrigger = true;
                //bool bResult = false;
                string sSampleRef = "", sSampleStatus = "";
                if (doCreateSampleTask(iSave, U_WasCd, U_WasDsc, U_WasFDsc, U_UOM, U_Quantity, U_Comment, Code, moParentWOQ.Code, moParentWOQ.U_CardCd,
                    moParentWOQ.U_CardNM, ref sSampleRef, ref sSampleStatus, sLabTasktoDuplicate, U_RouteCd)) {
                    //bResult = com.idh.bridge.utils.General.doCreateLabItemNTask(U_WasCd,
                    //    U_WasDsc,
                    //    U_WasFDsc,
                    //    U_UOM,
                    //    U_Quantity.ToString(), U_Comment,
                    //    Code, moParentWOQ.Code, "WOQ",
                    //    moParentWOQ.U_CardCd, moParentWOQ.U_CardNM, false, iSave, ref sSampleRef, ref sSampleStatus);
                    //if (bResult) {
                    U_SampleRef = sSampleRef;
                    U_SampStatus = sSampleStatus;
                    //}
                    return true;
                }
                return false;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", null);
                return false;
            } finally {
                miDoHandleChange--;
            }
        }
        public bool doDuplicateRow(ref IDH_WOQITEM _WOQRow, string WOQCode) {
            _WOQRow.DoBlockUpdateTrigger = true;
            for (int iRow = 0; iRow < this.getColumnCount(); iRow++) {
                try {
                    if (this.getValue(iRow) != null) {
                        if (iRow == 260) {
                            _WOQRow.U_WhsCode = this.U_WhsCode;
                            _WOQRow.setValue(this.getFieldInfo(iRow).FieldName, this.getValue(this.getFieldInfo(iRow).FieldName));
                        }
                        if (this.getFieldInfo(iRow).Type != SAPbobsCOM.BoFieldTypes.db_Date)
                            _WOQRow.setValue(this.getFieldInfo(iRow).FieldName, this.getValue(this.getFieldInfo(iRow).FieldName));
                    }
                } catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doDuplicateRow") + " Row ID=" + Code });
                }
            }

            _WOQRow.U_AEDate = U_AEDate;
            // _WOQRow.U_AETime = U_AETime;
            _WOQRow.U_ASDate = U_ASDate;
            //  _WOQRow.U_ASTime = U_ASTime;
            _WOQRow.U_BDate = U_BDate;
            //  _WOQRow.U_BTime = U_BTime;
            //  _WOQRow.U_DrivrOfSitDt= U_DrivrOfSitDt;
            //  _WOQRow.U_DrivrOfSitTm = U_DrivrOfSitTm;
            //  _WOQRow.U_DrivrOnSitDt= U_DrivrOnSitDt;
            //  _WOQRow.U_DrivrOnSitTm = U_DrivrOnSitTm;
            //  _WOQRow.U_JobRmD = U_JobRmD;
            //  _WOQRow.U_JobRmT = U_JobRmT;
            _WOQRow.U_RDate = U_RDate;
            //  _WOQRow.U_RTime = U_RTime;
            //_WOQRow.U_RTimeT = U_RTimeT;

            NumbersPair oWOQRowNextNum = _WOQRow.getNewKey();
            //if (_WOQRow.U_AdtnlItm == string.Empty) {
            //    sParentRowID = _WOQRow.Code;
            //    sParentRowIndex = _WOQRow.CurrentRowIndex;
            //} else if (_WOQRow.U_AdtnlItm == "A" && _WOQRow.U_PLineID != string.Empty) {
            //    _WOQRow.U_PLineID = sParentRowID;
            //}
            _WOQRow.Code = oWOQRowNextNum.CodeCode;
            _WOQRow.Name = oWOQRowNextNum.NameCode;
            _WOQRow.U_Status = ((int)IDH_WOQITEM.en_WOQITMSTATUS.New).ToString();
            _WOQRow.U_JobNr = WOQCode;

            _WOQRow.U_EnqID = "";
            _WOQRow.U_EnqLID = "";

            string sSampleVal = _WOQRow.U_Sample;
            string sSampleRefNum = _WOQRow.U_SampleRef;


            _WOQRow.U_WOHID = "";
            _WOQRow.U_WORID = "";
            _WOQRow.U_SampleRef = "";
            _WOQRow.U_SampStatus = "";
            _WOQRow.U_Sample = "";
            if (sSampleVal == "Y" && Config.INSTANCE.getParameterAsBool("CPSMPDUP", true)) {
                if (com.isb.enq.utils.General.doCheckIfLabTaskRequired(_WOQRow.U_WasCd.ToString())) {
                    if (sSampleRefNum != string.Empty && moParentWOQ.Handler_ExternalTriggerWithData_ShowYesNO != null
                            && (moParentWOQ.Handler_ExternalTriggerWithData_ShowYesNO(this, "ERVEQ019", null, 2) == 1)) {
                        if (_WOQRow.AddLabTaskFlagIfRequired(_WOQRow.U_WasCd)) {
                            _WOQRow.doCreateSampleTask(0, sSampleRefNum);
                        }
                        //if (Handler_ExternalTriggerWithData_ShowYesNO(this, "ERVEQ019", null, 1) == 1) {
                        //} else if (moWOQItems.AddLabTaskFlagIfRequired(moWOQItems.U_WasCd, null)) {
                        //    moWOQItems.doCreateSampleTask(0);
                        //}
                    } else if (_WOQRow.AddLabTaskFlagIfRequired(_WOQRow.U_WasCd)) {
                        _WOQRow.doCreateSampleTask(0, "");
                    }
                }
                //if (_WOQRow.AddLabTaskFlagIfRequired(_WOQRow.U_WasCd, null)) {
                //    _WOQRow.doCreateSampleTask(0,"");
                //}
            }
            _WOQRow.U_Sort = this.Count + 1;
            _WOQRow.DoBlockUpdateTrigger = false;
            return true;
        }
        public bool doFillByDisposalRouteCode(string RtCdCode) {
            try {
                miDoHandleChange++;
                if (string.IsNullOrWhiteSpace(RtCdCode))
                    return true;
                IDH_DISPRTCD oDisposalRoute = new IDH_DISPRTCD();
                bool bRet = false;
                bRet = oDisposalRoute.getByKey(RtCdCode);
                if (bRet) {
                    doSetWOQByDisposalRout(oDisposalRoute);
                }
            } catch (Exception ex) {
                throw ex;
            } finally {
                miDoHandleChange--;
            }
            return true;
        }
        public bool doFillByDisposalRoute(string sDipRouteCode) {
            try {
                miDoHandleChange++;
                if (string.IsNullOrWhiteSpace(U_WasCd) || string.IsNullOrWhiteSpace(U_ItemCd))
                    return true;
                IDH_DISPRTCD oDisposalRoute = new IDH_DISPRTCD();
                int iRet = 0;
                if (!string.IsNullOrWhiteSpace(sDipRouteCode) && sDipRouteCode.IndexOf("*") == -1 && !string.IsNullOrWhiteSpace(U_WasCd) && !string.IsNullOrWhiteSpace(U_ItemCd)) {
                    iRet = oDisposalRoute.getData(IDH_DISPRTCD._DisRCode + "=\'" + sDipRouteCode + "\' And " + IDH_DISPRTCD._WasCd + "=\'" + com.idh.utils.Formatter.doFixForSQL(U_WasCd.Trim()) + "\' And " + IDH_DISPRTCD._ItemCd + "=\'" + com.idh.utils.Formatter.doFixForSQL(U_ItemCd) + "\' And " + IDH_DISPRTCD._Active + "=\'Y\'", "");
                } else
                    iRet = oDisposalRoute.getData(IDH_DISPRTCD._WasCd + "=\'" + com.idh.utils.Formatter.doFixForSQL(U_WasCd.Trim()) + "\' And " + IDH_DISPRTCD._ItemCd + "=\'" + com.idh.utils.Formatter.doFixForSQL(U_ItemCd) + "\' And " + IDH_DISPRTCD._Active + "=\'Y\'", "");
                if (iRet > 0) {
                    doSetWOQByDisposalRout(oDisposalRoute);
                }
            } catch (Exception ex) {
                throw ex;
            } finally {
                miDoHandleChange--;
            }

            return true;
        }
        public void doSetWOQByDisposalRout(IDH_DISPRTCD oDisposalRoute) {
            try {

                if (oDisposalRoute.U_doWeight != null || oDisposalRoute.U_doWeight != string.Empty)
                    U_Obligated = oDisposalRoute.U_doWeight;
                else
                    U_Obligated = "";
                //DoBlockUpdateTrigger = true;
                U_Tip = oDisposalRoute.U_InTipCd;
                U_RouteCd = oDisposalRoute.U_DisRCode;
                U_TipNm = oDisposalRoute.U_InTipNm;
                U_FnlTipCd = oDisposalRoute.U_TipCd;
                U_FnlTipNm = oDisposalRoute.U_TipNm;
                U_ItemCd = oDisposalRoute.U_ItemCd;
                U_ItemDsc = oDisposalRoute.U_ItemDsc;
                U_UOM = oDisposalRoute.U_UOM;
                U_TipCost = oDisposalRoute.U_TotCost;// oDisposalRoute.U_TipCost+ oDisposalRoute.U_HaulgPCnt;
                if (string.IsNullOrWhiteSpace(U_WasCd)) {
                    U_WasCd = oDisposalRoute.U_WasCd;
                    U_WasDsc = oDisposalRoute.U_WasDsc;
                    AddLabTaskFlagIfRequired(U_WasCd);
                }

                U_TipTot = U_TipCost * U_ExpLdWgt;
                //U_ExpLdWgt
                if (string.IsNullOrWhiteSpace(U_RtCdCode) || oDisposalRoute.Code != U_RtCdCode)
                    U_RtCdCode = oDisposalRoute.Code;
                //U_OrdCost = oDisposalRoute.U_HaulgCost;

                //U_cos= oDisposalRoute.U_TipCost;
                doMarkUserUpdate(_TipCost);
                U_OrdCost = 0;
                //DoBlockUpdateTrigger = false;
                doGetAllPrices(false, true);
                //return false;
                //
                //doMarkUserUpdate(_OrdCost);

            } catch (Exception ex) {
                throw ex;
            }
        }
        #endregion
        #region Validate
        protected bool doValidateRowsforWO(ref string sNotAllRowPassed) {
            try {
                if (U_WasCd == string.Empty && U_WasFDsc == string.Empty) {
                    return true;
                }
                if (U_WasCd == string.Empty) {
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Item code is missing", "ERVLDFLD",
                    new string[] { idh.bridge.Translation.getTranslatedWord("Item Code:") + '[' + Code + ']' });
                    return false;
                }
                if (U_Sample.ToUpper().Trim().Equals("Y") && U_SampStatus.ToLower().Trim() == Translation.getTranslatedWord("pending").ToLower().Trim()) {
                    string sErrorMsg = Translation.getTranslatedWord("Sample for item ") + U_WasCd + Translation.getTranslatedWord(" is not accepted.");
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError(sErrorMsg, "ERVLDFLD",
                    new string[] { idh.bridge.Translation.getTranslatedWord("Sample for item:") + '[' + U_WasCd + ']' });
                    sNotAllRowPassed = sErrorMsg + "\n";
                    return true;
                }
                if (U_MSDS.ToUpper().Trim().Equals("Y") && U_WasCd.ToLower().Trim() != string.Empty && (Config.INSTANCE.getValueFromOITMWithNoBuffer(U_WasCd, "U_LOFile").ToString() != "Y")) {
                    string sErrorMsg = Translation.getTranslatedWord("MSDS for item ") + U_WasCd + Translation.getTranslatedWord(" not received.");
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError(sErrorMsg, "ERSDSMIS",
                     new string[] { idh.bridge.Translation.getTranslatedWord("MSDS for item:") + '[' + U_WasCd + ']' });
                    sNotAllRowPassed = sErrorMsg + "\n";
                    return true;
                }
                if (U_AdtnlItm == "") {
                    if (U_ItemCd == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Container code is missing", "ERVLDFLD",
                        new string[] { idh.bridge.Translation.getTranslatedWord("Container code:") + '[' + Code + ']' });
                        return false;
                    }
                    if (U_JobTp == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Job type is missing", "ERVLDFLD",
                        new string[] { idh.bridge.Translation.getTranslatedWord("Job Type:") + '[' + U_WasCd + ']' });
                        return false;
                    }
                    if (U_RDate == null || U_RDate.Year < 2000) {
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Request Date is missing", "ERVLDFLD",
                        new string[] { idh.bridge.Translation.getTranslatedWord("Request Date:") + '[' + Code + ']' });
                        return false;
                    }
                }




            } catch (Exception e) {
                //DataHandler.INSTANCE.doError("Exception: [Code:" + Code + "]" + e.ToString(), "Error Adding the WOR [Code:" + Code + "].");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(e.Message, "ERVLDFLD",
                    new string[] { idh.bridge.Translation.getTranslatedWord("Item Code:") + '[' + Code + ']' });
                return false;
            } finally {
            }

            return true;
        }

        public bool doValidateAllRowsforWO() {
            doBookmark();
            bool bAllValided = true;
            string sErrorMsgs = "";
            try {
                first();
                while (next()) {
                    try {
                        if (U_Status != "5" && U_Status != "4")
                            bAllValided = doValidateRowsforWO(ref sErrorMsgs);
                    } catch (Exception ex) {
                        //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error updating the Row [" + Code +"] Prices.");
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXEURP", new string[] { Code });
                        bAllValided = false;
                    }
                    if (!bAllValided)
                        break;
                }
            } catch (Exception e) {
                //DataHandler.INSTANCE.doError("Exception: [Code:" + Code + "]" + e.ToString(), "Error Adding the WOR [Code:" + Code + "].");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(e.Message, "EREXSGEN",
                    new string[] { "doValidateAllRowsforWO" });
                bAllValided = false;
            } finally {
                doRecallBookmark();
            }
            if (bAllValided == true && sErrorMsgs != string.Empty) {
                sErrorMsgs = "Follwing rows failed the validations, Do you want to continue?\n" + sErrorMsgs;
                //string sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("ERVEQ003", null);
                //int iRow = pVal.Row;
                if (IDHAddOns.idh.addon.Base.PARENT.doMessage(sErrorMsgs, 2, "Yes", "No", null, true) != 1) {
                    return false;
                }
            }
            return bAllValided;
        }
        #endregion
        #region Create WOR From WOQ Row
        public bool doAddAdditionalItems(ref com.isb.enq.dbObjects.User.IDH_JOBSHD moWOR) {
            IDH_WOQITEM moAddItems = new IDH_WOQITEM();
            moAddItems.mbDoNotSetDefault = true;
            int iRet = moAddItems.getData(IDH_WOQITEM._JobNr + "='" + U_JobNr + "' And " + IDH_WOQITEM._AdtnlItm + "='A' And " + IDH_WOQITEM._PLineID + "='" + Code + "'", "Cast(Code as Int)");
            moAddItems.first();
            while (moAddItems.next()) {

                moWOR.AdditionalExpenses.doAddEmptyRow();
                moWOR.AdditionalExpenses.U_JobNr = moWOR.U_JobNr;

                moWOR.AdditionalExpenses.U_RowNr = moWOR.Code;
                moWOR.AdditionalExpenses.U_ItemCd = moAddItems.U_WasCd;
                moWOR.AdditionalExpenses.U_ItemDsc = moAddItems.U_WasDsc;
                moWOR.AdditionalExpenses.U_SuppCd = moAddItems.U_CarrCd;
                moWOR.AdditionalExpenses.U_SuppNm = moAddItems.U_CarrNm;
                moWOR.AdditionalExpenses.U_UOM = moAddItems.U_UOM;
                moWOR.AdditionalExpenses.U_Quantity = moAddItems.U_ExpLdWgt;
                moWOR.AdditionalExpenses.U_ItmCost = moAddItems.U_TipCost;
                moWOR.AdditionalExpenses.U_ItmChrg = moAddItems.U_TCharge;
                moWOR.AdditionalExpenses.U_ChrgVatGrp = U_TChrgVtGrp;//Config.INSTANCE.doGetSalesVatGroup(oWOR.U_CustCd, oBPAC.U_ItemCode);
                moWOR.AdditionalExpenses.U_AutoAdd = "N";
                moWOR.AdditionalExpenses.U_BPAutoAdd = "N";

                moWOR.AdditionalExpenses.U_WOQLID = moAddItems.Code;

                NumbersPair oNextNum;
                oNextNum = moWOR.AdditionalExpenses.getNewKey();
                moWOR.AdditionalExpenses.Code = oNextNum.CodeCode;
                moWOR.AdditionalExpenses.Name = oNextNum.NameCode;

                moAddItems.U_AddItmID = moWOR.AdditionalExpenses.Code;
                moAddItems.U_WORID = moWOR.Code;
                moAddItems.U_WOHID = moWOR.U_JobNr;
                moAddItems.U_Status = ((int)IDH_WOQITEM.en_WOQITMSTATUS.WOCreated).ToString();
                if (moAddItems.doUpdateDataRow() == false)
                    return false;
                //oWOR.AdditionalExpenses = oWOAExp;
                //return  moWOR.AdditionalExpenses.doAddDataRow();
            }

            return true;
        }
        public bool doCreateWOR(IDH_JOBENTR moWOH, DateTime moRDate) {

            bool bDOContinue = true;
            try {

                string sTime = com.idh.utils.Dates.doTimeToNumStr();

                com.isb.enq.dbObjects.User.IDH_JOBSHD moWOR = new com.isb.enq.dbObjects.User.IDH_JOBSHD(moWOH);

                moWOR.doAddEmptyRow(true, true);
                string msNextRowNum = "JOBSCHE";
                NumbersPair oNumbers = DataHandler.INSTANCE.doGenerateCode(IDH_JOBSHD.AUTONUMPREFIX, msNextRowNum);
                moWOR.Code = oNumbers.CodeCode;
                moWOR.Name = oNumbers.NameCode;
                //moWOR.getNewKey();
                moWOR.BlockChangeNotif = true;

                moWOR.U_JobNr = moWOH.Code;
                //moWOR.doApplyDefaults();
                //moWOR.U_JobNr = moWOH.Code;

                moWOR.U_WOQID = moParentWOQ.Code;
                moWOR.U_WOQLID = Code;
                moWOR.U_RouteCd = U_RouteCd;
                moWOR.U_RtCdCode = U_RtCdCode;

                moWOR.U_User = Config.INSTANCE.doGetUserCode(moParentWOQ.U_User.ToString());
                moWOR.U_Branch = U_Branch;
                moWOR.U_JobTp = U_JobTp;
                moWOR.U_ItemCd = U_ItemCd;
                moWOR.U_ItemDsc = U_ItemDsc;
                moWOR.U_ItmGrp = U_ItmGrp;
                moWOR.U_BDate = DateTime.Now;//moParentWOQ.U_BDate;
                moWOR.U_BTime = sTime;
                if ((moRDate == null || moRDate.Year < 2000) && (U_RDate == null || U_RDate.Year < 2000))
                    moWOR.U_RDate = DateTime.Now;//moParentWOQ.U_BDate;
                else if ((moRDate == null || moRDate.Year < 2000) && (U_RDate != null || U_RDate.Year > 2000))
                    moWOR.U_RDate = U_RDate;// U_RDate;//moParentWOQ.U_BDate;
                else
                    moWOR.U_RDate = moRDate;

                moWOR.U_RTime = sTime;
                moWOR.U_RTimeT = sTime;

                if (Config.Parameter("CTOFJBHR") != "" && Config.Parameter("CTOFJBHR") != "0" && Config.ParameterWithDefault("MAXJOBHR", "0") != "0") {
                    DateTime cuttoffhrs = Convert.ToDateTime(DateTime.Today.ToString("yyyy/MM/dd") + " " + Config.INSTANCE.getParameter("CTOFJBHR"));
                    DateTime BTime = DateTime.Now;
                    if (BTime.TimeOfDay <= cuttoffhrs.TimeOfDay) {
                        BTime = BTime.AddHours(Convert.ToInt32(Config.ParamaterWithDefault("MAXJOBHR", "0")));
                        U_JbToBeDoneTm = com.idh.utils.Dates.doTimeToNumStr(BTime);
                        U_JbToBeDoneDt = BTime;
                    } else {
                        //BTime = "08:00";
                        //if BDate is beyond the cuttof time then set to 8AM of next Day( Hayden to be confirm from Mike)
                        U_JbToBeDoneTm = "800";
                        DateTime BDate = DateTime.Today.AddDays(1);
                        U_JbToBeDoneDt = BDate;
                    }
                    //Else
                }
                if (moParentWOQ.U_NoChgTax == "Y") {
                    moWOR.DoChargeVat = false;
                }
                moWOR.U_ExpLdWgt = U_ExpLdWgt;
                moWOR.U_MANPRC = U_MANPRC;
                moWOR.U_HaulAC = U_HaulAC;

                //moWOR.U_not= U_ExpLdWgt;

                moWOR.U_UOM = U_UOM;
                moWOR.U_TCharge = U_TCharge;
                moWOR.U_CusChr = U_CusChr;

                moWOR.U_Tip = U_Tip;
                moWOR.U_TipNm = U_TipNm;
                moWOR.U_SAddress = U_SAddress;
                moWOR.U_SAddrsLN = U_SAddrsLN;

                moWOR.U_CarrCd = U_CarrCd;
                moWOR.U_CarrNm = U_CarrNm;
                moWOR.U_CarrNm = U_CarrNm;

                moWOR.U_PUOM = U_UOM;

                moWOR.U_TipCost = U_TipCost;
                moWOR.U_OrdCost = U_OrdCost;
                doAddAdditionalItems(ref moWOR);


                // Add Rest of Values
                //moWOR.U_ASDate=U_ASDate
                //moWOR.U_ASTime=U_ASTime
                //moWOR.U_AEDate=U_AEDate
                //moWOR.U_AETime=U_AETime
                //moWOR.U_VehTyp=U_VehTyp
                //moWOR.U_Lorry=U_Lorry
                //moWOR.U_Driver=U_Driver
                //moWOR.U_DocNum=U_DocNum

                //moWOR.U_SupRef=U_SupRef
                //moWOR.U_TipWgt=U_TipWgt
                //moWOR.U_TipCost=U_TipCost
                //moWOR.U_TipTot=U_TipTot
                //moWOR.U_CstWgt=U_CstWgt
                //moWOR.U_TCharge=U_TCharge
                //moWOR.U_TCTotal=U_TCTotal
                //moWOR.U_CongCh=U_CongCh
                //moWOR.U_Weight=U_Weight
                //moWOR.U_Price=U_Price
                //moWOR.U_Discnt=U_Discnt
                //moWOR.U_DisAmt=U_DisAmt
                //moWOR.U_TaxAmt=U_TaxAmt
                //moWOR.U_Total=U_Total

                //moWOR.U_SLicNr=U_SLicNr
                //moWOR.U_SLicExp=U_SLicExp
                //moWOR.U_SLicCh=U_SLicCh
                //moWOR.U_ItmGrp=U_ItmGrp
                //moWOR.U_ItemCd=U_ItemCd
                //moWOR.U_ItemDsc=U_ItemDsc
                if (string.IsNullOrEmpty(U_WasDsc) && !string.IsNullOrEmpty(U_WasCd))
                    U_WasDsc = Config.INSTANCE.doGetItemDescriptionWithNoBuffer(U_WasCd);
                moWOR.U_WasCd = U_WasCd;
                moWOR.U_WasDsc = U_WasDsc;
                //moWOR.U_Serial=U_Serial
                moWOR.U_Status = com.idh.bridge.lookups.FixedValues.getStatusOpen();
                //moWOR.U_Quantity=U_Quantity
                //moWOR.U_JCost=U_JCost

                //moWOR.U_SAINV=U_SAINV
                //moWOR.U_SAORD=U_SAORD
                //moWOR.U_TIPPO=U_TIPPO
                //moWOR.U_JOBPO=U_JOBPO
                //moWOR.U_SLicSp=U_SLicSp
                //moWOR.U_Tip=U_Tip
                //moWOR.U_CustCd=U_CustCd
                //moWOR.U_CarrCd=U_CarrCd
                //moWOR.U_CongCd=U_CongCd
                //moWOR.U_TipNm=U_TipNm
                //moWOR.U_CustNm=U_CustNm
                //moWOR.U_CarrNm=U_CarrNm
                //moWOR.U_SLicNm=U_SLicNm
                //moWOR.U_SCngNm=U_SCngNm
                //moWOR.U_Dista=U_Dista
                //moWOR.U_CntrNo=U_CntrNo
                //moWOR.U_Wei1=U_Wei1
                //moWOR.U_Wei2=U_Wei2
                //moWOR.U_Ser1=U_Ser1
                //moWOR.U_Ser2=U_Ser2
                //moWOR.U_WDt1=U_WDt1
                //moWOR.U_WDt2=U_WDt2
                //moWOR.U_AddEx=U_AddEx
                moWOR.U_PStat = com.idh.bridge.lookups.FixedValues.getStatusOpen();
                //moWOR.U_OrdWgt=U_OrdWgt
                //moWOR.U_OrdCost=U_OrdCost
                //moWOR.U_OrdTot=U_OrdTot
                //moWOR.U_TRLReg=U_TRLReg
                //moWOR.U_TRLNM=U_TRLNM
                //moWOR.U_UOM=U_UOM
                //moWOR.U_WASLIC=U_WASLIC
                //moWOR.U_SLPO=U_SLPO
                //moWOR.U_SLicCst=U_SLicCst
                //moWOR.U_SLicCTo=U_SLicCTo
                //moWOR.U_SLicCQt=U_SLicCQt
                //moWOR.U_WROrd=U_WROrd
                //moWOR.U_WRRow=U_WRRow
                //moWOR.U_RTime=U_RTime
                //moWOR.U_RTimeT=U_RTimeT
                //moWOR.U_CusQty=U_CusQty
                //moWOR.U_CusChr=U_CusChr
                //moWOR.U_PayMeth=U_PayMeth
                //moWOR.U_CustRef=U_CustRef
                //moWOR.U_RdWgt=U_RdWgt
                //moWOR.U_LorryCd=U_LorryCd
                //moWOR.U_Covera=U_Covera
                //moWOR.U_Comment=U_Comment
                //moWOR.U_PUOM=U_PUOM
                moWOR.U_AUOM = U_UOM;
                //moWOR.U_AUOMQt=U_AUOMQt
                //moWOR.U_CCNum=U_CCNum
                //moWOR.U_CCStat=U_CCStat
                //moWOR.U_PayStat=U_PayStat
                //moWOR.U_CCType=U_CCType
                //moWOR.U_PARCPT=U_PARCPT
                //moWOR.U_ENREF=U_ENREF
                //moWOR.U_JobRmD=U_JobRmD
                //moWOR.U_JobRmT=U_JobRmT
                //moWOR.U_RemNot=U_RemNot
                //moWOR.U_RemCnt=U_RemCnt
                //moWOR.U_TZone=U_TZone
                //moWOR.U_CoverHst=U_CoverHst
                //moWOR.U_ProPO=U_ProPO
                //moWOR.U_ProCd=U_ProCd
                //moWOR.U_ProNm=U_ProNm
                //moWOR.U_User=U_User
                //moWOR.U_Branch=U_Branch
                //moWOR.U_TAddChrg=U_TAddChrg
                //moWOR.U_TAddCost=U_TAddCost
                //moWOR.U_IssQty=U_IssQty
                moWOR.U_Obligated = U_Obligated;
                //moWOR.U_Origin=U_Origin
                moWOR.U_TChrgVtRt = U_TChrgVtRt;
                moWOR.U_HChrgVtRt = U_HChrgVtRt;
                moWOR.U_TCostVtRt = U_TCostVtRt;
                moWOR.U_HCostVtRt = U_HCostVtRt;
                moWOR.U_TChrgVtGrp = U_TChrgVtGrp;
                moWOR.U_HChrgVtGrp = U_HChrgVtGrp;
                moWOR.U_TCostVtGrp = U_TCostVtGrp;
                moWOR.U_HCostVtGrp = U_HCostVtGrp;

                //moWOR.U_VtCostAmt=U_VtCostAmt
                //moWOR.U_CustCs=U_CustCs
                //moWOR.U_ConNum=U_ConNum
                //moWOR.U_CCExp=U_CCExp
                //moWOR.U_CCIs=U_CCIs
                //moWOR.U_CCSec=U_CCSec
                //moWOR.U_CCHNum=U_CCHNum
                //moWOR.U_CCPCd=U_CCPCd
                //moWOR.U_IntComm=U_IntComm
                //moWOR.U_ContNr=U_ContNr
                //moWOR.U_SealNr=U_SealNr
                //moWOR.U_FPCoH=U_FPCoH
                //moWOR.U_FPCoT=U_FPCoT
                //moWOR.U_FPChH=U_FPChH
                //moWOR.U_FPChT=U_FPChT
                //moWOR.U_WastTNN=U_WastTNN
                //moWOR.U_HazWCNN=U_HazWCNN
                moWOR.U_SiteRef = U_SiteRef;
                //moWOR.U_ExtWeig=U_ExtWeig
                //moWOR.U_BDate=U_BDate
                //moWOR.U_BTime=U_BTime
                //moWOR.U_CarrReb=U_CarrReb
                //moWOR.U_TPCN=U_TPCN
                //moWOR.U_MANPRC=U_MANPRC
                //moWOR.U_ENNO=U_ENNO
                //moWOR.U_SemSolid=U_SemSolid
                //moWOR.U_PaMuPu=U_PaMuPu
                //moWOR.U_Dusty=U_Dusty
                //moWOR.U_Fluid=U_Fluid
                //moWOR.U_Firm=U_Firm
                //moWOR.U_Consiste=U_Consiste
                //moWOR.U_TypPTreat=U_TypPTreat
                //moWOR.U_DAAttach=U_DAAttach
                //moWOR.U_LnkPBI=U_LnkPBI
                //moWOR.U_DPRRef=U_DPRRef
                //moWOR.U_MDChngd=U_MDChngd
                //moWOR.U_Sched=U_Sched
                //moWOR.U_USched=U_USched
                //moWOR.U_Analys1=U_Analys1
                //moWOR.U_Analys2=U_Analys2
                //moWOR.U_Analys3=U_Analys3
                //moWOR.U_Analys4=U_Analys4
                //moWOR.U_Analys5=U_Analys5
                moWOR.U_CustReb = U_CustReb;
                moWOR.U_TCCN = U_TCCN;
                moWOR.U_RowSta = com.idh.bridge.lookups.FixedValues.getStatusOpen();
                //moWOR.U_ActComm=U_ActComm
                //moWOR.U_HlSQty=U_HlSQty
                //moWOR.U_HaulAC=U_HaulAC
                //moWOR.U_ClgCode=U_ClgCode
                //moWOR.U_ProRef=U_ProRef
                //moWOR.U_SerialC=U_SerialC
                //moWOR.U_PAUOMQt=U_PAUOMQt
                //moWOR.U_PRdWgt=U_PRdWgt
                //moWOR.U_ProWgt=U_ProWgt
                //moWOR.U_ProUOM=U_ProUOM
                //moWOR.U_PCost=U_PCost
                //moWOR.U_PCTotal=U_PCTotal
                //moWOR.U_WgtDed=U_WgtDed
                //moWOR.U_AdjWgt=U_AdjWgt
                //moWOR.U_ValDed=U_ValDed
                //moWOR.U_BookIn=U_BookIn
                //moWOR.U_ProGRPO=U_ProGRPO
                //moWOR.U_GRIn=U_GRIn
                //moWOR.U_CarrRef=U_CarrRef
                //moWOR.U_PCharge=U_PCharge
                //moWOR.U_CustGR=U_CustGR
                //moWOR.U_IDHSEQ=U_IDHSEQ
                //moWOR.U_IDHCMT=U_IDHCMT
                //moWOR.U_IDHDRVI=U_IDHDRVI
                //moWOR.U_IDHCHK=U_IDHCHK
                //moWOR.U_IDHPRTD=U_IDHPRTD
                //moWOR.U_IDHRTCD=U_IDHRTCD
                //moWOR.U_IDHRTDT=U_IDHRTDT
                //moWOR.U_LoadSht=U_LoadSht
                //moWOR.U_SODlvNot=U_SODlvNot
                //moWOR.U_ExpLdWgt=U_ExpLdWgt
                //moWOR.U_IsTrl=U_IsTrl
                //moWOR.U_UseWgt=U_UseWgt
                //moWOR.U_TFSMov=U_TFSMov
                //moWOR.U_PayTrm=U_PayTrm
                //moWOR.U_JStat=U_JStat
                //moWOR.U_Jrnl=U_Jrnl
                //moWOR.U_SAddress=U_SAddress
                //moWOR.U_LckPrc=U_LckPrc
                //moWOR.U_CBICont=U_CBICont
                //moWOR.U_CBIManQty=U_CBIManQty
                //moWOR.U_CBIManUOM=U_CBIManUOM
                //moWOR.U_ExtComm1=U_ExtComm1
                //moWOR.U_ExtComm2=U_ExtComm2
                //moWOR.U_MaximoNum=U_MaximoNum
                //moWOR.U_TFSReq=U_TFSReq
                //moWOR.U_TFSNum=U_TFSNum
                //moWOR.U_TRdWgt=U_TRdWgt
                //moWOR.U_PrcLink=U_PrcLink
                //moWOR.U_TAUOMQt=U_TAUOMQt
                moWOR.U_Rebate = U_Rebate;
                //moWOR.U_TAddChVat=U_TAddChVat
                //moWOR.U_TAddCsVat=U_TAddCsVat
                //moWOR.U_AddCharge=U_AddCharge
                //moWOR.U_AddCost=U_AddCost
                //moWOR.U_ReqArch=U_ReqArch
                //moWOR.U_JbToBeDoneDt=U_JbToBeDoneDt
                //moWOR.U_JbToBeDoneTm=U_JbToBeDoneTm
                //moWOR.U_DrivrOnSitDt=U_DrivrOnSitDt
                //moWOR.U_DrivrOnSitTm=U_DrivrOnSitTm
                //moWOR.U_DrivrOfSitDt=U_DrivrOfSitDt
                //moWOR.U_DrivrOfSitTm=U_DrivrOfSitTm
                //moWOR.U_AltWasDsc=U_AltWasDsc
                //moWOR.U_OTComments=U_OTComments
                //moWOR.U_TrnCode=U_TrnCode
                //moWOR.U_UseBPWgt=U_UseBPWgt
                //moWOR.U_BPWgt=U_BPWgt
                //moWOR.U_CarWgt=U_CarWgt
                //moWOR.U_PROINV=U_PROINV
                //moWOR.U_WHTRNF=U_WHTRNF
                //moWOR.U_ProPay=U_ProPay
                //moWOR.U_CustPay=U_CustPay
                //moWOR.U_WROrd2=U_WROrd2
                //moWOR.U_WRRow2=U_WRRow2
                moWOR.U_DispCgCc = U_DispCgCc;
                moWOR.U_CarrCgCc = U_CarrCgCc;
                moWOR.U_PurcCoCc = U_PurcCoCc;
                moWOR.U_DispCoCc = U_DispCoCc;
                moWOR.U_CarrCoCc = U_CarrCoCc;
                moWOR.U_LiscCoCc = U_LiscCoCc;
                //moWOR.U_SAddrsLN=U_SAddrsLN
                //moWOR.U_FTOrig=U_FTOrig
                //moWOR.U_WOQID=U_WOQID
                //moWOR.U_WOQLID=U_WOQLID
                moWOR.doSetLongituteLatitute();
                moWOR.doSetWasteItemGroup();

                moWOR.BlockChangeNotif = false;


                moWOR.doGetChargePrices(true);
                moWOR.doGetCostPrices(true, true, true, true);

                //moWOR.doCalculateChargeVat('R');
                //moWOR.doCalculateCostVat('R');
                //moWOR.doCalculateTotals();
                moWOR.doCalculateProfit();
                moWOR.AddLabTaskFlagIfRequired(U_WasCd);
                bDOContinue = moWOR.doAddDataRow();
                if (bDOContinue) {
                    U_WOHID = moWOH.Code;
                    U_WORID = moWOR.Code;
                    U_Status = ((int)en_WOQITMSTATUS.WOCreated).ToString();
                    bDOContinue = doUpdateDataRow(false, "");
                }
            } catch (Exception e) {
                //DataHandler.INSTANCE.doError("Exception: [Code:" + Code + "]" + e.ToString(), "Error Adding the WOR [Code:" + Code + "].");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(e.Message, "EREXSGEN",
                    new string[] { "IDH_WOQITM.doCreateWOR" });
                bDOContinue = false;
            } finally {

            }
            return bDOContinue;
        }
        #endregion
    }
}
