﻿using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User {
    public class WOQRequestDates : ArrayList {
        private int miCurrentIndex = 0;
        public int CurrentIndex {
            get { return miCurrentIndex; }
            set { miCurrentIndex = value; }
        }
        public void Add(WOQRequestDated oRDate) {
            base.Add(oRDate);
        }

        public int Add() {
            Add(new WOQRequestDated());
            miCurrentIndex = Count - 1;
            return miCurrentIndex;
        }

        public new WOQRequestDated this[int iIndex] {
            get {
                return (WOQRequestDated)base[iIndex];
            }
            set {
                base[iIndex] = value;
            }
        }

        private WOQRequestDated getCurrentItem() {
            if (Count == 0) {
                WOQRequestDated oRDate = new WOQRequestDated();
                Add(oRDate);
                return oRDate;
            } else
                return this[miCurrentIndex];
        }

        public string WOR { //Haulage Price
            get { return this[miCurrentIndex].moWOR; }
            set { getCurrentItem().moWOR = value; }
        }

        public DateTime RDate { //Haulage Price
            get { return this[miCurrentIndex].moRDate; }
            set { getCurrentItem().moRDate = value; }
        }

        public class WOQRequestDated {
            public string moWOR;
            public DateTime moRDate;
        }

    }

}
