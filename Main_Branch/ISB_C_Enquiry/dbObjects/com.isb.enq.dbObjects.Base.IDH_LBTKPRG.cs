/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 15/09/2016 17:15:40
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base {
    [Serializable]
    public class IDH_LBTKPRG : com.idh.dbObjects.DBBase {

        private Linker moLinker = null;
        public Linker ControlLinker {
            get { return moLinker; }
            set { moLinker = value; }
        }

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_LBTKPRG()
            : base("@IDH_LBTKPRG") {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_LBTKPRG(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oForm, "@IDH_LBTKPRG") {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
		* Table name
		*/
        public readonly static string TableName = "@IDH_LBTKPRG";

        /**
         * Decription: Code
         * Mandatory: tYes
         * Name: Code
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Code = "Code";
        public string Code {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code() {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue) {
            return base.doValidation(_Code, oValue);
        }
        /**
         * Decription: Name
         * Mandatory: tYes
         * Name: Name
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Name = "Name";
        public string Name {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name() {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue) {
            return base.doValidation(_Name, oValue);
        }
        /**
         * Decription: Analysis Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_AnalysisCd
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _AnalysisCd = "U_AnalysisCd";
        public string U_AnalysisCd {
            get {
                return getValueAsString(_AnalysisCd);
            }
            set { setValue(_AnalysisCd, value); }
        }
        public string doValidate_AnalysisCd() {
            return doValidate_AnalysisCd(U_AnalysisCd);
        }
        public virtual string doValidate_AnalysisCd(object oValue) {
            return base.doValidation(_AnalysisCd, oValue);
        }

        /**
         * Decription: Comments
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Comments
         * Size: 10
         * Type: db_Memo
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Comments = "U_Comments";
        public string U_Comments {
            get {
                return getValueAsString(_Comments);
            }
            set { setValue(_Comments, value); }
        }
        public string doValidate_Comments() {
            return doValidate_Comments(U_Comments);
        }
        public virtual string doValidate_Comments(object oValue) {
            return base.doValidation(_Comments, oValue);
        }

        /**
         * Decription: Select
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Select
         * Size: 1
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Select = "U_Select";
        public string U_Select {
            get {
                return getValueAsString(_Select);
            }
            set { setValue(_Select, value); }
        }
        public string doValidate_Select() {
            return doValidate_Select(U_Select);
        }
        public virtual string doValidate_Select(object oValue) {
            return base.doValidation(_Select, oValue);
        }

        /**
         * Decription: Status
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Status
         * Size: 1
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Status = "U_Status";
        public string U_Status {
            get {
                return getValueAsString(_Status);
            }
            set { setValue(_Status, value); }
        }
        public string doValidate_Status() {
            return doValidate_Status(U_Status);
        }
        public virtual string doValidate_Status(object oValue) {
            return base.doValidation(_Status, oValue);
        }

        /**
         * Decription: Lab Task Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_TaskCd
         * Size: 50
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _TaskCd = "U_TaskCd";
        public string U_TaskCd {
            get {
                return getValueAsString(_TaskCd);
            }
            set { setValue(_TaskCd, value); }
        }
        public string doValidate_TaskCd() {
            return doValidate_TaskCd(U_TaskCd);
        }
        public virtual string doValidate_TaskCd(object oValue) {
            return base.doValidation(_TaskCd, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo() {
            if (moDBFields == null)
                moDBFields = new DBFields(7);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_AnalysisCd, "Analysis Code", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Analysis Code
            moDBFields.Add(_Comments, "Comments", 3, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Comments
            moDBFields.Add(_Select, "Select", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Select
            moDBFields.Add(_Status, "Status", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Status
            moDBFields.Add(_TaskCd, "Lab Task Code", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Lab Task Code

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation() {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_AnalysisCd());
            doBuildValidationString(doValidate_Comments());
            doBuildValidationString(doValidate_Select());
            doBuildValidationString(doValidate_Status());
            doBuildValidationString(doValidate_TaskCd());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_AnalysisCd)) return doValidate_AnalysisCd(oValue);
            if (sFieldName.Equals(_Comments)) return doValidate_Comments(oValue);
            if (sFieldName.Equals(_Select)) return doValidate_Select(oValue);
            if (sFieldName.Equals(_Status)) return doValidate_Status(oValue);
            if (sFieldName.Equals(_TaskCd)) return doValidate_TaskCd(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName) {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
         * Link the Name Field to the Form Item.
         */
        public void doLink_Name(string sControlName) {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
         * Link the U_AnalysisCd Field to the Form Item.
         */
        public void doLink_AnalysisCd(string sControlName) {
            moLinker.doLinkDataToControl(_AnalysisCd, sControlName);
        }
        /**
         * Link the U_Comments Field to the Form Item.
         */
        public void doLink_Comments(string sControlName) {
            moLinker.doLinkDataToControl(_Comments, sControlName);
        }
        /**
         * Link the U_Select Field to the Form Item.
         */
        public void doLink_Select(string sControlName) {
            moLinker.doLinkDataToControl(_Select, sControlName);
        }
        /**
         * Link the U_Status Field to the Form Item.
         */
        public void doLink_Status(string sControlName) {
            moLinker.doLinkDataToControl(_Status, sControlName);
        }
        /**
         * Link the U_TaskCd Field to the Form Item.
         */
        public void doLink_TaskCd(string sControlName) {
            moLinker.doLinkDataToControl(_TaskCd, sControlName);
        }
        #endregion

    }
}
