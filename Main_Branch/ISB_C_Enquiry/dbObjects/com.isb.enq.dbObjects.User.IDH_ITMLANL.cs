/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 14/09/2016 16:37:43
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_ITMLANL : com.isb.enq.dbObjects.Base.IDH_ITMLANL {

        public IDH_ITMLANL()
            : base() {
            msAutoNumKey = "SEQITLBTM";
        }

        public IDH_ITMLANL(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oIDHForm, oForm) {
            msAutoNumKey = "SEQITLBTM";
        }
    }
}
