/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 11/03/2016 17:09:00
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base {
    [Serializable]
    public class IDH_APPROVLH : com.idh.dbObjects.DBBase {

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_APPROVLH()
            : base("@IDH_APPROVLH") {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_APPROVLH(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oForm, "@IDH_APPROVLH") {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
		* Table name
		*/
        public readonly static string TableName = "@IDH_APPROVLH";

        /**
         * Decription: Code
         * Mandatory: tYes
         * Name: Code
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Code = "Code";
        public string Code {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code() {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue) {
            return base.doValidation(_Code, oValue);
        }
        /**
         * Decription: Name
         * Mandatory: tYes
         * Name: Name
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Name = "Name";
        public string Name {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name() {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue) {
            return base.doValidation(_Name, oValue);
        }
        /**
         * Decription: Approve Date
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ApprDt
         * Size: 8
         * Type: db_Date
         * CType: DateTime
         * SubType: st_None
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _ApprDt = "U_ApprDt";
        public DateTime U_ApprDt {
            get {
                return getValueAsDateTime(_ApprDt);
            }
            set { setValue(_ApprDt, value); }
        }
        public void U_ApprDt_AsString(string value) {
            setValue("U_ApprDt", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_ApprDt_AsString() {
            DateTime dVal = getValueAsDateTime(_ApprDt);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_ApprDt() {
            return doValidate_ApprDt(U_ApprDt);
        }
        public virtual string doValidate_ApprDt(object oValue) {
            return base.doValidation(_ApprDt, oValue);
        }

        /**
         * Decription: Doc Add Date
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_DocAddDt
         * Size: 8
         * Type: db_Date
         * CType: DateTime
         * SubType: st_None
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _DocAddDt = "U_DocAddDt";
        public DateTime U_DocAddDt {
            get {
                return getValueAsDateTime(_DocAddDt);
            }
            set { setValue(_DocAddDt, value); }
        }
        public void U_DocAddDt_AsString(string value) {
            setValue("U_DocAddDt", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_DocAddDt_AsString() {
            DateTime dVal = getValueAsDateTime(_DocAddDt);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_DocAddDt() {
            return doValidate_DocAddDt(U_DocAddDt);
        }
        public virtual string doValidate_DocAddDt(object oValue) {
            return base.doValidation(_DocAddDt, oValue);
        }

        /**
         * Decription: Document Date
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_DocDt
         * Size: 8
         * Type: db_Date
         * CType: DateTime
         * SubType: st_None
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _DocDt = "U_DocDt";
        public DateTime U_DocDt {
            get {
                return getValueAsDateTime(_DocDt);
            }
            set { setValue(_DocDt, value); }
        }
        public void U_DocDt_AsString(string value) {
            setValue("U_DocDt", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_DocDt_AsString() {
            DateTime dVal = getValueAsDateTime(_DocDt);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_DocDt() {
            return doValidate_DocDt(U_DocDt);
        }
        public virtual string doValidate_DocDt(object oValue) {
            return base.doValidation(_DocDt, oValue);
        }

        /**
         * Decription: Document number
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_DocNum
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _DocNum = "U_DocNum";
        public string U_DocNum {
            get {
                return getValueAsString(_DocNum);
            }
            set { setValue(_DocNum, value); }
        }
        public string doValidate_DocNum() {
            return doValidate_DocNum(U_DocNum);
        }
        public virtual string doValidate_DocNum(object oValue) {
            return base.doValidation(_DocNum, oValue);
        }

        /**
         * Decription: Document Version
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_DocVer
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _DocVer = "U_DocVer";
        public int U_DocVer {
            get {
                return getValueAsInt(_DocVer);
            }
            set { setValue(_DocVer, value); }
        }
        public string doValidate_DocVer() {
            return doValidate_DocVer(U_DocVer);
        }
        public virtual string doValidate_DocVer(object oValue) {
            return base.doValidation(_DocVer, oValue);
        }

        /**
         * Decription: Form Type
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_FormTyp
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _FormTyp = "U_FormTyp";
        public string U_FormTyp {
            get {
                return getValueAsString(_FormTyp);
            }
            set { setValue(_FormTyp, value); }
        }
        public string doValidate_FormTyp() {
            return doValidate_FormTyp(U_FormTyp);
        }
        public virtual string doValidate_FormTyp(object oValue) {
            return base.doValidation(_FormTyp, oValue);
        }

        /**
         * Decription: Last Updated By
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_LastUpBy
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _LastUpBy = "U_LastUpBy";
        public int U_LastUpBy {
            get {
                return getValueAsInt(_LastUpBy);
            }
            set { setValue(_LastUpBy, value); }
        }
        public string doValidate_LastUpBy() {
            return doValidate_LastUpBy(U_LastUpBy);
        }
        public virtual string doValidate_LastUpBy(object oValue) {
            return base.doValidation(_LastUpBy, oValue);
        }

        /**
         * Decription: Total Approvals
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_NmAprovd
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _NmAprovd = "U_NmAprovd";
        public int U_NmAprovd {
            get {
                return getValueAsInt(_NmAprovd);
            }
            set { setValue(_NmAprovd, value); }
        }
        public string doValidate_NmAprovd() {
            return doValidate_NmAprovd(U_NmAprovd);
        }
        public virtual string doValidate_NmAprovd(object oValue) {
            return base.doValidation(_NmAprovd, oValue);
        }

        /**
         * Decription: Number of Approvals Required
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_NmAprReq
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _NmAprReq = "U_NmAprReq";
        public int U_NmAprReq {
            get {
                return getValueAsInt(_NmAprReq);
            }
            set { setValue(_NmAprReq, value); }
        }
        public string doValidate_NmAprReq() {
            return doValidate_NmAprReq(U_NmAprReq);
        }
        public virtual string doValidate_NmAprReq(object oValue) {
            return base.doValidation(_NmAprReq, oValue);
        }

        /**
         * Decription: Number of Reject Required
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_NmRejReq
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _NmRejReq = "U_NmRejReq";
        public int U_NmRejReq {
            get {
                return getValueAsInt(_NmRejReq);
            }
            set { setValue(_NmRejReq, value); }
        }
        public string doValidate_NmRejReq() {
            return doValidate_NmRejReq(U_NmRejReq);
        }
        public virtual string doValidate_NmRejReq(object oValue) {
            return base.doValidation(_NmRejReq, oValue);
        }

        /**
         * Decription: Total Rejected
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_NmRejted
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _NmRejted = "U_NmRejted";
        public int U_NmRejted {
            get {
                return getValueAsInt(_NmRejted);
            }
            set { setValue(_NmRejted, value); }
        }
        public string doValidate_NmRejted() {
            return doValidate_NmRejted(U_NmRejted);
        }
        public virtual string doValidate_NmRejted(object oValue) {
            return base.doValidation(_NmRejted, oValue);
        }

        /**
         * Decription: Originator
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Origintr
         * Size: 200
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Origintr = "U_Origintr";
        public string U_Origintr {
            get {
                return getValueAsString(_Origintr);
            }
            set { setValue(_Origintr, value); }
        }
        public string doValidate_Origintr() {
            return doValidate_Origintr(U_Origintr);
        }
        public virtual string doValidate_Origintr(object oValue) {
            return base.doValidation(_Origintr, oValue);
        }

        /**
         * Decription: Reject Date
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_RejDt
         * Size: 8
         * Type: db_Date
         * CType: DateTime
         * SubType: st_None
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _RejDt = "U_RejDt";
        public DateTime U_RejDt {
            get {
                return getValueAsDateTime(_RejDt);
            }
            set { setValue(_RejDt, value); }
        }
        public void U_RejDt_AsString(string value) {
            setValue("U_RejDt", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_RejDt_AsString() {
            DateTime dVal = getValueAsDateTime(_RejDt);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_RejDt() {
            return doValidate_RejDt(U_RejDt);
        }
        public virtual string doValidate_RejDt(object oValue) {
            return base.doValidation(_RejDt, oValue);
        }

        /**
         * Decription: Stage
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Stage
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Stage = "U_Stage";
        public int U_Stage {
            get {
                return getValueAsInt(_Stage);
            }
            set { setValue(_Stage, value); }
        }
        public string doValidate_Stage() {
            return doValidate_Stage(U_Stage);
        }
        public virtual string doValidate_Stage(object oValue) {
            return base.doValidation(_Stage, oValue);
        }

        /**
         * Decription: Status
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Status
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Status = "U_Status";
        public string U_Status {
            get {
                return getValueAsString(_Status);
            }
            set { setValue(_Status, value); }
        }
        public string doValidate_Status() {
            return doValidate_Status(U_Status);
        }
        public virtual string doValidate_Status(object oValue) {
            return base.doValidation(_Status, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo() {
            if (moDBFields == null)
                moDBFields = new DBFields(17);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_ApprDt, "Approve Date", 2, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Approve Date
            moDBFields.Add(_DocAddDt, "Doc Add Date", 3, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Doc Add Date
            moDBFields.Add(_DocDt, "Document Date", 4, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Document Date
            moDBFields.Add(_DocNum, "Document number", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Document number
            moDBFields.Add(_DocVer, "Document Version", 6, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Document Version
            moDBFields.Add(_FormTyp, "Form Type", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Form Type
            moDBFields.Add(_LastUpBy, "Last Updated By", 8, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Last Updated By
            moDBFields.Add(_NmAprovd, "Total Approvals", 9, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Total Approvals
            moDBFields.Add(_NmAprReq, "Number of Approvals Required", 10, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Number of Approvals Required
            moDBFields.Add(_NmRejReq, "Number of Reject Required", 11, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Number of Reject Required
            moDBFields.Add(_NmRejted, "Total Rejected", 12, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Total Rejected
            moDBFields.Add(_Origintr, "Originator", 13, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Originator
            moDBFields.Add(_RejDt, "Reject Date", 14, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Reject Date
            moDBFields.Add(_Stage, "Stage", 15, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Stage
            moDBFields.Add(_Status, "Status", 16, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Status

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation() {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_ApprDt());
            doBuildValidationString(doValidate_DocAddDt());
            doBuildValidationString(doValidate_DocDt());
            doBuildValidationString(doValidate_DocNum());
            doBuildValidationString(doValidate_DocVer());
            doBuildValidationString(doValidate_FormTyp());
            doBuildValidationString(doValidate_LastUpBy());
            doBuildValidationString(doValidate_NmAprovd());
            doBuildValidationString(doValidate_NmAprReq());
            doBuildValidationString(doValidate_NmRejReq());
            doBuildValidationString(doValidate_NmRejted());
            doBuildValidationString(doValidate_Origintr());
            doBuildValidationString(doValidate_RejDt());
            doBuildValidationString(doValidate_Stage());
            doBuildValidationString(doValidate_Status());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_ApprDt)) return doValidate_ApprDt(oValue);
            if (sFieldName.Equals(_DocAddDt)) return doValidate_DocAddDt(oValue);
            if (sFieldName.Equals(_DocDt)) return doValidate_DocDt(oValue);
            if (sFieldName.Equals(_DocNum)) return doValidate_DocNum(oValue);
            if (sFieldName.Equals(_DocVer)) return doValidate_DocVer(oValue);
            if (sFieldName.Equals(_FormTyp)) return doValidate_FormTyp(oValue);
            if (sFieldName.Equals(_LastUpBy)) return doValidate_LastUpBy(oValue);
            if (sFieldName.Equals(_NmAprovd)) return doValidate_NmAprovd(oValue);
            if (sFieldName.Equals(_NmAprReq)) return doValidate_NmAprReq(oValue);
            if (sFieldName.Equals(_NmRejReq)) return doValidate_NmRejReq(oValue);
            if (sFieldName.Equals(_NmRejted)) return doValidate_NmRejted(oValue);
            if (sFieldName.Equals(_Origintr)) return doValidate_Origintr(oValue);
            if (sFieldName.Equals(_RejDt)) return doValidate_RejDt(oValue);
            if (sFieldName.Equals(_Stage)) return doValidate_Stage(oValue);
            if (sFieldName.Equals(_Status)) return doValidate_Status(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName) {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
         * Link the Name Field to the Form Item.
         */
        public void doLink_Name(string sControlName) {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
         * Link the U_ApprDt Field to the Form Item.
         */
        public void doLink_ApprDt(string sControlName) {
            moLinker.doLinkDataToControl(_ApprDt, sControlName);
        }
        /**
         * Link the U_DocAddDt Field to the Form Item.
         */
        public void doLink_DocAddDt(string sControlName) {
            moLinker.doLinkDataToControl(_DocAddDt, sControlName);
        }
        /**
         * Link the U_DocDt Field to the Form Item.
         */
        public void doLink_DocDt(string sControlName) {
            moLinker.doLinkDataToControl(_DocDt, sControlName);
        }
        /**
         * Link the U_DocNum Field to the Form Item.
         */
        public void doLink_DocNum(string sControlName) {
            moLinker.doLinkDataToControl(_DocNum, sControlName);
        }
        /**
         * Link the U_DocVer Field to the Form Item.
         */
        public void doLink_DocVer(string sControlName) {
            moLinker.doLinkDataToControl(_DocVer, sControlName);
        }
        /**
         * Link the U_FormTyp Field to the Form Item.
         */
        public void doLink_FormTyp(string sControlName) {
            moLinker.doLinkDataToControl(_FormTyp, sControlName);
        }
        /**
         * Link the U_LastUpBy Field to the Form Item.
         */
        public void doLink_LastUpBy(string sControlName) {
            moLinker.doLinkDataToControl(_LastUpBy, sControlName);
        }
        /**
         * Link the U_NmAprovd Field to the Form Item.
         */
        public void doLink_NmAprovd(string sControlName) {
            moLinker.doLinkDataToControl(_NmAprovd, sControlName);
        }
        /**
         * Link the U_NmAprReq Field to the Form Item.
         */
        public void doLink_NmAprReq(string sControlName) {
            moLinker.doLinkDataToControl(_NmAprReq, sControlName);
        }
        /**
         * Link the U_NmRejReq Field to the Form Item.
         */
        public void doLink_NmRejReq(string sControlName) {
            moLinker.doLinkDataToControl(_NmRejReq, sControlName);
        }
        /**
         * Link the U_NmRejted Field to the Form Item.
         */
        public void doLink_NmRejted(string sControlName) {
            moLinker.doLinkDataToControl(_NmRejted, sControlName);
        }
        /**
         * Link the U_Origintr Field to the Form Item.
         */
        public void doLink_Origintr(string sControlName) {
            moLinker.doLinkDataToControl(_Origintr, sControlName);
        }
        /**
         * Link the U_RejDt Field to the Form Item.
         */
        public void doLink_RejDt(string sControlName) {
            moLinker.doLinkDataToControl(_RejDt, sControlName);
        }
        /**
         * Link the U_Stage Field to the Form Item.
         */
        public void doLink_Stage(string sControlName) {
            moLinker.doLinkDataToControl(_Stage, sControlName);
        }
        /**
         * Link the U_Status Field to the Form Item.
         */
        public void doLink_Status(string sControlName) {
            moLinker.doLinkDataToControl(_Status, sControlName);
        }
        #endregion

    }
}
