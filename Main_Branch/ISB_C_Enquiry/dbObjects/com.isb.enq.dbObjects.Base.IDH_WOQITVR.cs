/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 29/03/2016 15:35:16
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base{
   [Serializable]
	public class IDH_WOQITVR: com.idh.dbObjects.DBBase { 

		private Linker moLinker = null;
       public Linker ControlLinker {
           get { return moLinker; }
           set { moLinker = value; }
       }

       private IDHAddOns.idh.forms.Base moIDHForm;
       public IDHAddOns.idh.forms.Base IDHForm {
           get { return moIDHForm; }
           set { moIDHForm = value; }
       }

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_WOQITVR() : base("@IDH_WOQITVR"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_WOQITVR( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_WOQITVR"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_WOQITVR";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Action Comment
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ActComm
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ActComm = "U_ActComm";
		public string U_ActComm { 
			get {
 				return getValueAsString(_ActComm); 
			}
			set { setValue(_ActComm, value); }
		}
           public string doValidate_ActComm() {
               return doValidate_ActComm(U_ActComm);
           }
           public virtual string doValidate_ActComm(object oValue) {
               return base.doValidation(_ActComm, oValue);
           }

		/**
		 * Decription: Additional Charge
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AddCharge
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _AddCharge = "U_AddCharge";
		public double U_AddCharge { 
			get {
 				return getValueAsDouble(_AddCharge); 
			}
			set { setValue(_AddCharge, value); }
		}
           public string doValidate_AddCharge() {
               return doValidate_AddCharge(U_AddCharge);
           }
           public virtual string doValidate_AddCharge(object oValue) {
               return base.doValidation(_AddCharge, oValue);
           }

		/**
		 * Decription: Additional Cost
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AddCost
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _AddCost = "U_AddCost";
		public double U_AddCost { 
			get {
 				return getValueAsDouble(_AddCost); 
			}
			set { setValue(_AddCost, value); }
		}
           public string doValidate_AddCost() {
               return doValidate_AddCost(U_AddCost);
           }
           public virtual string doValidate_AddCost(object oValue) {
               return base.doValidation(_AddCost, oValue);
           }

		/**
		 * Decription: Additional Expences
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AddEx
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Measurement
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _AddEx = "U_AddEx";
		public double U_AddEx { 
			get {
 				return getValueAsDouble(_AddEx); 
			}
			set { setValue(_AddEx, value); }
		}
           public string doValidate_AddEx() {
               return doValidate_AddEx(U_AddEx);
           }
           public virtual string doValidate_AddEx(object oValue) {
               return base.doValidation(_AddEx, oValue);
           }

		/**
		 * Decription: WOR Additional Item ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AddItmID
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AddItmID = "U_AddItmID";
		public string U_AddItmID { 
			get {
 				return getValueAsString(_AddItmID); 
			}
			set { setValue(_AddItmID, value); }
		}
           public string doValidate_AddItmID() {
               return doValidate_AddItmID(U_AddItmID);
           }
           public virtual string doValidate_AddItmID(object oValue) {
               return base.doValidation(_AddItmID, oValue);
           }

		/**
		 * Decription: Adjusted Weight
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AdjWgt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Measurement
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _AdjWgt = "U_AdjWgt";
		public double U_AdjWgt { 
			get {
 				return getValueAsDouble(_AdjWgt); 
			}
			set { setValue(_AdjWgt, value); }
		}
           public string doValidate_AdjWgt() {
               return doValidate_AdjWgt(U_AdjWgt);
           }
           public virtual string doValidate_AdjWgt(object oValue) {
               return base.doValidation(_AdjWgt, oValue);
           }

		/**
		 * Decription: Additional Item
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AdtnlItm
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AdtnlItm = "U_AdtnlItm";
		public string U_AdtnlItm { 
			get {
 				return getValueAsString(_AdtnlItm); 
			}
			set { setValue(_AdtnlItm, value); }
		}
           public string doValidate_AdtnlItm() {
               return doValidate_AdtnlItm(U_AdtnlItm);
           }
           public virtual string doValidate_AdtnlItm(object oValue) {
               return base.doValidation(_AdtnlItm, oValue);
           }

		/**
		 * Decription: Action End Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AEDate
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _AEDate = "U_AEDate";
		public DateTime U_AEDate { 
			get {
 				return getValueAsDateTime(_AEDate); 
			}
			set { setValue(_AEDate, value); }
		}
		public void U_AEDate_AsString(string value){
			setValue("U_AEDate", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_AEDate_AsString(){
			DateTime dVal = getValueAsDateTime(_AEDate);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_AEDate() {
               return doValidate_AEDate(U_AEDate);
           }
           public virtual string doValidate_AEDate(object oValue) {
               return base.doValidation(_AEDate, oValue);
           }

		/**
		 * Decription: Action End Time
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AETime
		 * Size: 6
		 * Type: db_Date
		 * CType: string
		 * SubType: st_Time
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _AETime = "U_AETime";
		public string U_AETime { 
			get {
 				return getValueAsString(_AETime); 
			}
			set { setValue(_AETime, value); }
		}
           public string doValidate_AETime() {
               return doValidate_AETime(U_AETime);
           }
           public virtual string doValidate_AETime(object oValue) {
               return base.doValidation(_AETime, oValue);
           }

		/**
		 * Decription: Alternate Waste Descrp
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AltWasDsc
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AltWasDsc = "U_AltWasDsc";
		public string U_AltWasDsc { 
			get {
 				return getValueAsString(_AltWasDsc); 
			}
			set { setValue(_AltWasDsc, value); }
		}
           public string doValidate_AltWasDsc() {
               return doValidate_AltWasDsc(U_AltWasDsc);
           }
           public virtual string doValidate_AltWasDsc(object oValue) {
               return base.doValidation(_AltWasDsc, oValue);
           }

		/**
		 * Decription: Action Start Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ASDate
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _ASDate = "U_ASDate";
		public DateTime U_ASDate { 
			get {
 				return getValueAsDateTime(_ASDate); 
			}
			set { setValue(_ASDate, value); }
		}
		public void U_ASDate_AsString(string value){
			setValue("U_ASDate", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_ASDate_AsString(){
			DateTime dVal = getValueAsDateTime(_ASDate);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_ASDate() {
               return doValidate_ASDate(U_ASDate);
           }
           public virtual string doValidate_ASDate(object oValue) {
               return base.doValidation(_ASDate, oValue);
           }

		/**
		 * Decription: Action Start Time
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ASTime
		 * Size: 6
		 * Type: db_Date
		 * CType: string
		 * SubType: st_Time
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _ASTime = "U_ASTime";
		public string U_ASTime { 
			get {
 				return getValueAsString(_ASTime); 
			}
			set { setValue(_ASTime, value); }
		}
           public string doValidate_ASTime() {
               return doValidate_ASTime(U_ASTime);
           }
           public virtual string doValidate_ASTime(object oValue) {
               return base.doValidation(_ASTime, oValue);
           }

		/**
		 * Decription: Purchase Unit Of Measure
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AUOM
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AUOM = "U_AUOM";
		public string U_AUOM { 
			get {
 				return getValueAsString(_AUOM); 
			}
			set { setValue(_AUOM, value); }
		}
           public string doValidate_AUOM() {
               return doValidate_AUOM(U_AUOM);
           }
           public virtual string doValidate_AUOM(object oValue) {
               return base.doValidation(_AUOM, oValue);
           }

		/**
		 * Decription: Overriding Qty
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AUOMQt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _AUOMQt = "U_AUOMQt";
		public double U_AUOMQt { 
			get {
 				return getValueAsDouble(_AUOMQt); 
			}
			set { setValue(_AUOMQt, value); }
		}
           public string doValidate_AUOMQt() {
               return doValidate_AUOMQt(U_AUOMQt);
           }
           public virtual string doValidate_AUOMQt(object oValue) {
               return base.doValidation(_AUOMQt, oValue);
           }

		/**
		 * Decription: Base WOQ ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BasWOHID
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BasWOHID = "U_BasWOHID";
		public string U_BasWOHID { 
			get {
 				return getValueAsString(_BasWOHID); 
			}
			set { setValue(_BasWOHID, value); }
		}
           public string doValidate_BasWOHID() {
               return doValidate_BasWOHID(U_BasWOHID);
           }
           public virtual string doValidate_BasWOHID(object oValue) {
               return base.doValidation(_BasWOHID, oValue);
           }

		/**
		 * Decription: Base WOQ Row ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BasWQRID
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BasWQRID = "U_BasWQRID";
		public string U_BasWQRID { 
			get {
 				return getValueAsString(_BasWQRID); 
			}
			set { setValue(_BasWQRID, value); }
		}
           public string doValidate_BasWQRID() {
               return doValidate_BasWQRID(U_BasWQRID);
           }
           public virtual string doValidate_BasWQRID(object oValue) {
               return base.doValidation(_BasWQRID, oValue);
           }

		/**
		 * Decription: Booking Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BDate
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _BDate = "U_BDate";
		public DateTime U_BDate { 
			get {
 				return getValueAsDateTime(_BDate); 
			}
			set { setValue(_BDate, value); }
		}
		public void U_BDate_AsString(string value){
			setValue("U_BDate", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_BDate_AsString(){
			DateTime dVal = getValueAsDateTime(_BDate);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_BDate() {
               return doValidate_BDate(U_BDate);
           }
           public virtual string doValidate_BDate(object oValue) {
               return base.doValidation(_BDate, oValue);
           }

		/**
		 * Decription: Book In Stock
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BookIn
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BookIn = "U_BookIn";
		public string U_BookIn { 
			get {
 				return getValueAsString(_BookIn); 
			}
			set { setValue(_BookIn, value); }
		}
           public string doValidate_BookIn() {
               return doValidate_BookIn(U_BookIn);
           }
           public virtual string doValidate_BookIn(object oValue) {
               return base.doValidation(_BookIn, oValue);
           }

		/**
		 * Decription: BP Weight
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BPWgt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _BPWgt = "U_BPWgt";
		public double U_BPWgt { 
			get {
 				return getValueAsDouble(_BPWgt); 
			}
			set { setValue(_BPWgt, value); }
		}
           public string doValidate_BPWgt() {
               return doValidate_BPWgt(U_BPWgt);
           }
           public virtual string doValidate_BPWgt(object oValue) {
               return base.doValidation(_BPWgt, oValue);
           }

		/**
		 * Decription: Branch
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Branch
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Branch = "U_Branch";
		public string U_Branch { 
			get {
 				return getValueAsString(_Branch); 
			}
			set { setValue(_Branch, value); }
		}
           public string doValidate_Branch() {
               return doValidate_Branch(U_Branch);
           }
           public virtual string doValidate_Branch(object oValue) {
               return base.doValidation(_Branch, oValue);
           }

		/**
		 * Decription: Booking Time
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BTime
		 * Size: 6
		 * Type: db_Date
		 * CType: string
		 * SubType: st_Time
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _BTime = "U_BTime";
		public string U_BTime { 
			get {
 				return getValueAsString(_BTime); 
			}
			set { setValue(_BTime, value); }
		}
           public string doValidate_BTime() {
               return doValidate_BTime(U_BTime);
           }
           public virtual string doValidate_BTime(object oValue) {
               return base.doValidation(_BTime, oValue);
           }

		/**
		 * Decription: CarrierCode
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CarrCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CarrCd = "U_CarrCd";
		public string U_CarrCd { 
			get {
 				return getValueAsString(_CarrCd); 
			}
			set { setValue(_CarrCd, value); }
		}
           public string doValidate_CarrCd() {
               return doValidate_CarrCd(U_CarrCd);
           }
           public virtual string doValidate_CarrCd(object oValue) {
               return base.doValidation(_CarrCd, oValue);
           }

		/**
		 * Decription: Carrier Charge Currency
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CarrCgCc
		 * Size: 3
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CarrCgCc = "U_CarrCgCc";
		public string U_CarrCgCc { 
			get {
 				return getValueAsString(_CarrCgCc); 
			}
			set { setValue(_CarrCgCc, value); }
		}
           public string doValidate_CarrCgCc() {
               return doValidate_CarrCgCc(U_CarrCgCc);
           }
           public virtual string doValidate_CarrCgCc(object oValue) {
               return base.doValidation(_CarrCgCc, oValue);
           }

		/**
		 * Decription: Carrier Cost Currency
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CarrCoCc
		 * Size: 3
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CarrCoCc = "U_CarrCoCc";
		public string U_CarrCoCc { 
			get {
 				return getValueAsString(_CarrCoCc); 
			}
			set { setValue(_CarrCoCc, value); }
		}
           public string doValidate_CarrCoCc() {
               return doValidate_CarrCoCc(U_CarrCoCc);
           }
           public virtual string doValidate_CarrCoCc(object oValue) {
               return base.doValidation(_CarrCoCc, oValue);
           }

		/**
		 * Decription: Carrier Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CarrNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CarrNm = "U_CarrNm";
		public string U_CarrNm { 
			get {
 				return getValueAsString(_CarrNm); 
			}
			set { setValue(_CarrNm, value); }
		}
           public string doValidate_CarrNm() {
               return doValidate_CarrNm(U_CarrNm);
           }
           public virtual string doValidate_CarrNm(object oValue) {
               return base.doValidation(_CarrNm, oValue);
           }

		/**
		 * Decription: Carrier Rebate Item
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CarrReb
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CarrReb = "U_CarrReb";
		public string U_CarrReb { 
			get {
 				return getValueAsString(_CarrReb); 
			}
			set { setValue(_CarrReb, value); }
		}
           public string doValidate_CarrReb() {
               return doValidate_CarrReb(U_CarrReb);
           }
           public virtual string doValidate_CarrReb(object oValue) {
               return base.doValidation(_CarrReb, oValue);
           }

		/**
		 * Decription: Carrier Ref. No.
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CarrRef
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CarrRef = "U_CarrRef";
		public string U_CarrRef { 
			get {
 				return getValueAsString(_CarrRef); 
			}
			set { setValue(_CarrRef, value); }
		}
           public string doValidate_CarrRef() {
               return doValidate_CarrRef(U_CarrRef);
           }
           public virtual string doValidate_CarrRef(object oValue) {
               return base.doValidation(_CarrRef, oValue);
           }

		/**
		 * Decription: Carrier Weight
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CarWgt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _CarWgt = "U_CarWgt";
		public double U_CarWgt { 
			get {
 				return getValueAsDouble(_CarWgt); 
			}
			set { setValue(_CarWgt, value); }
		}
           public string doValidate_CarWgt() {
               return doValidate_CarWgt(U_CarWgt);
           }
           public virtual string doValidate_CarWgt(object oValue) {
               return base.doValidation(_CarWgt, oValue);
           }

		/**
		 * Decription: Containers
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CBICont
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _CBICont = "U_CBICont";
		public int U_CBICont { 
			get {
 				return getValueAsInt(_CBICont); 
			}
			set { setValue(_CBICont, value); }
		}
           public string doValidate_CBICont() {
               return doValidate_CBICont(U_CBICont);
           }
           public virtual string doValidate_CBICont(object oValue) {
               return base.doValidation(_CBICont, oValue);
           }

		/**
		 * Decription: Manifest Quantity
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CBIManQty
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _CBIManQty = "U_CBIManQty";
		public double U_CBIManQty { 
			get {
 				return getValueAsDouble(_CBIManQty); 
			}
			set { setValue(_CBIManQty, value); }
		}
           public string doValidate_CBIManQty() {
               return doValidate_CBIManQty(U_CBIManQty);
           }
           public virtual string doValidate_CBIManQty(object oValue) {
               return base.doValidation(_CBIManQty, oValue);
           }

		/**
		 * Decription: Manifest UOM
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CBIManUOM
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CBIManUOM = "U_CBIManUOM";
		public string U_CBIManUOM { 
			get {
 				return getValueAsString(_CBIManUOM); 
			}
			set { setValue(_CBIManUOM, value); }
		}
           public string doValidate_CBIManUOM() {
               return doValidate_CBIManUOM(U_CBIManUOM);
           }
           public virtual string doValidate_CBIManUOM(object oValue) {
               return base.doValidation(_CBIManUOM, oValue);
           }

		/**
		 * Decription: CCard Expiry
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CCExp
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _CCExp = "U_CCExp";
		public DateTime U_CCExp { 
			get {
 				return getValueAsDateTime(_CCExp); 
			}
			set { setValue(_CCExp, value); }
		}
		public void U_CCExp_AsString(string value){
			setValue("U_CCExp", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_CCExp_AsString(){
			DateTime dVal = getValueAsDateTime(_CCExp);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_CCExp() {
               return doValidate_CCExp(U_CCExp);
           }
           public virtual string doValidate_CCExp(object oValue) {
               return base.doValidation(_CCExp, oValue);
           }

		/**
		 * Decription: CCard House Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CCHNum
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CCHNum = "U_CCHNum";
		public string U_CCHNum { 
			get {
 				return getValueAsString(_CCHNum); 
			}
			set { setValue(_CCHNum, value); }
		}
           public string doValidate_CCHNum() {
               return doValidate_CCHNum(U_CCHNum);
           }
           public virtual string doValidate_CCHNum(object oValue) {
               return base.doValidation(_CCHNum, oValue);
           }

		/**
		 * Decription: CCard Issue #
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CCIs
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CCIs = "U_CCIs";
		public string U_CCIs { 
			get {
 				return getValueAsString(_CCIs); 
			}
			set { setValue(_CCIs, value); }
		}
           public string doValidate_CCIs() {
               return doValidate_CCIs(U_CCIs);
           }
           public virtual string doValidate_CCIs(object oValue) {
               return base.doValidation(_CCIs, oValue);
           }

		/**
		 * Decription: Credit Card No
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CCNum
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CCNum = "U_CCNum";
		public string U_CCNum { 
			get {
 				return getValueAsString(_CCNum); 
			}
			set { setValue(_CCNum, value); }
		}
           public string doValidate_CCNum() {
               return doValidate_CCNum(U_CCNum);
           }
           public virtual string doValidate_CCNum(object oValue) {
               return base.doValidation(_CCNum, oValue);
           }

		/**
		 * Decription: CCard PostCode
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CCPCd
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CCPCd = "U_CCPCd";
		public string U_CCPCd { 
			get {
 				return getValueAsString(_CCPCd); 
			}
			set { setValue(_CCPCd, value); }
		}
           public string doValidate_CCPCd() {
               return doValidate_CCPCd(U_CCPCd);
           }
           public virtual string doValidate_CCPCd(object oValue) {
               return base.doValidation(_CCPCd, oValue);
           }

		/**
		 * Decription: CCard Security Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CCSec
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CCSec = "U_CCSec";
		public string U_CCSec { 
			get {
 				return getValueAsString(_CCSec); 
			}
			set { setValue(_CCSec, value); }
		}
           public string doValidate_CCSec() {
               return doValidate_CCSec(U_CCSec);
           }
           public virtual string doValidate_CCSec(object oValue) {
               return base.doValidation(_CCSec, oValue);
           }

		/**
		 * Decription: Credit Card Approval Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CCStat
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CCStat = "U_CCStat";
		public string U_CCStat { 
			get {
 				return getValueAsString(_CCStat); 
			}
			set { setValue(_CCStat, value); }
		}
           public string doValidate_CCStat() {
               return doValidate_CCStat(U_CCStat);
           }
           public virtual string doValidate_CCStat(object oValue) {
               return base.doValidation(_CCStat, oValue);
           }

		/**
		 * Decription: Credit Card Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CCType
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _CCType = "U_CCType";
		public short U_CCType { 
			get {
 				return getValueAsShort(_CCType); 
			}
			set { setValue(_CCType, value); }
		}
           public string doValidate_CCType() {
               return doValidate_CCType(U_CCType);
           }
           public virtual string doValidate_CCType(object oValue) {
               return base.doValidation(_CCType, oValue);
           }

		/**
		 * Decription: Activity Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ClgCode
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ClgCode = "U_ClgCode";
		public string U_ClgCode { 
			get {
 				return getValueAsString(_ClgCode); 
			}
			set { setValue(_ClgCode, value); }
		}
           public string doValidate_ClgCode() {
               return doValidate_ClgCode(U_ClgCode);
           }
           public virtual string doValidate_ClgCode(object oValue) {
               return base.doValidation(_ClgCode, oValue);
           }

		/**
		 * Decription: Contract Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CntrNo
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CntrNo = "U_CntrNo";
		public string U_CntrNo { 
			get {
 				return getValueAsString(_CntrNo); 
			}
			set { setValue(_CntrNo, value); }
		}
           public string doValidate_CntrNo() {
               return doValidate_CntrNo(U_CntrNo);
           }
           public virtual string doValidate_CntrNo(object oValue) {
               return base.doValidation(_CntrNo, oValue);
           }

		/**
		 * Decription: Comment
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Comment
		 * Size: 230
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Comment = "U_Comment";
		public string U_Comment { 
			get {
 				return getValueAsString(_Comment); 
			}
			set { setValue(_Comment, value); }
		}
           public string doValidate_Comment() {
               return doValidate_Comment(U_Comment);
           }
           public virtual string doValidate_Comment(object oValue) {
               return base.doValidation(_Comment, oValue);
           }

		/**
		 * Decription: Congestion Supp Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CongCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CongCd = "U_CongCd";
		public string U_CongCd { 
			get {
 				return getValueAsString(_CongCd); 
			}
			set { setValue(_CongCd, value); }
		}
           public string doValidate_CongCd() {
               return doValidate_CongCd(U_CongCd);
           }
           public virtual string doValidate_CongCd(object oValue) {
               return base.doValidation(_CongCd, oValue);
           }

		/**
		 * Decription: Congestion Charge
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CongCh
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _CongCh = "U_CongCh";
		public double U_CongCh { 
			get {
 				return getValueAsDouble(_CongCh); 
			}
			set { setValue(_CongCh, value); }
		}
           public string doValidate_CongCh() {
               return doValidate_CongCh(U_CongCh);
           }
           public virtual string doValidate_CongCh(object oValue) {
               return base.doValidation(_CongCh, oValue);
           }

		/**
		 * Decription: Consignment Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ConNum
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ConNum = "U_ConNum";
		public string U_ConNum { 
			get {
 				return getValueAsString(_ConNum); 
			}
			set { setValue(_ConNum, value); }
		}
           public string doValidate_ConNum() {
               return doValidate_ConNum(U_ConNum);
           }
           public virtual string doValidate_ConNum(object oValue) {
               return base.doValidation(_ConNum, oValue);
           }

		/**
		 * Decription: Consistency
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Consiste
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Consiste = "U_Consiste";
		public string U_Consiste { 
			get {
 				return getValueAsString(_Consiste); 
			}
			set { setValue(_Consiste, value); }
		}
           public string doValidate_Consiste() {
               return doValidate_Consiste(U_Consiste);
           }
           public virtual string doValidate_Consiste(object oValue) {
               return base.doValidation(_Consiste, oValue);
           }

		/**
		 * Decription: Container Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ContNr
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ContNr = "U_ContNr";
		public string U_ContNr { 
			get {
 				return getValueAsString(_ContNr); 
			}
			set { setValue(_ContNr, value); }
		}
           public string doValidate_ContNr() {
               return doValidate_ContNr(U_ContNr);
           }
           public virtual string doValidate_ContNr(object oValue) {
               return base.doValidation(_ContNr, oValue);
           }

		/**
		 * Decription: Coverage Used
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Covera
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Covera = "U_Covera";
		public string U_Covera { 
			get {
 				return getValueAsString(_Covera); 
			}
			set { setValue(_Covera, value); }
		}
           public string doValidate_Covera() {
               return doValidate_Covera(U_Covera);
           }
           public virtual string doValidate_Covera(object oValue) {
               return base.doValidation(_Covera, oValue);
           }

		/**
		 * Decription: Coverage History
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CoverHst
		 * Size: 230
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CoverHst = "U_CoverHst";
		public string U_CoverHst { 
			get {
 				return getValueAsString(_CoverHst); 
			}
			set { setValue(_CoverHst, value); }
		}
           public string doValidate_CoverHst() {
               return doValidate_CoverHst(U_CoverHst);
           }
           public virtual string doValidate_CoverHst(object oValue) {
               return base.doValidation(_CoverHst, oValue);
           }

		/**
		 * Decription: Customer Weight
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CstWgt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Measurement
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _CstWgt = "U_CstWgt";
		public double U_CstWgt { 
			get {
 				return getValueAsDouble(_CstWgt); 
			}
			set { setValue(_CstWgt, value); }
		}
           public string doValidate_CstWgt() {
               return doValidate_CstWgt(U_CstWgt);
           }
           public virtual string doValidate_CstWgt(object oValue) {
               return base.doValidation(_CstWgt, oValue);
           }

		/**
		 * Decription: Customer Charge
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CusChr
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _CusChr = "U_CusChr";
		public double U_CusChr { 
			get {
 				return getValueAsDouble(_CusChr); 
			}
			set { setValue(_CusChr, value); }
		}
           public string doValidate_CusChr() {
               return doValidate_CusChr(U_CusChr);
           }
           public virtual string doValidate_CusChr(object oValue) {
               return base.doValidation(_CusChr, oValue);
           }

		/**
		 * Decription: Customer Quantity
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CusQty
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Measurement
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _CusQty = "U_CusQty";
		public double U_CusQty { 
			get {
 				return getValueAsDouble(_CusQty); 
			}
			set { setValue(_CusQty, value); }
		}
           public string doValidate_CusQty() {
               return doValidate_CusQty(U_CusQty);
           }
           public virtual string doValidate_CusQty(object oValue) {
               return base.doValidation(_CusQty, oValue);
           }

		/**
		 * Decription: Customer Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CustCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CustCd = "U_CustCd";
		public string U_CustCd { 
			get {
 				return getValueAsString(_CustCd); 
			}
			set { setValue(_CustCd, value); }
		}
           public string doValidate_CustCd() {
               return doValidate_CustCd(U_CustCd);
           }
           public virtual string doValidate_CustCd(object oValue) {
               return base.doValidation(_CustCd, oValue);
           }

		/**
		 * Decription: Customer Compliance Scheme
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CustCs
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CustCs = "U_CustCs";
		public string U_CustCs { 
			get {
 				return getValueAsString(_CustCs); 
			}
			set { setValue(_CustCs, value); }
		}
           public string doValidate_CustCs() {
               return doValidate_CustCs(U_CustCs);
           }
           public virtual string doValidate_CustCs(object oValue) {
               return base.doValidation(_CustCs, oValue);
           }

		/**
		 * Decription: Customer Goods Receipt
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CustGR
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CustGR = "U_CustGR";
		public string U_CustGR { 
			get {
 				return getValueAsString(_CustGR); 
			}
			set { setValue(_CustGR, value); }
		}
           public string doValidate_CustGR() {
               return doValidate_CustGR(U_CustGR);
           }
           public virtual string doValidate_CustGR(object oValue) {
               return base.doValidation(_CustGR, oValue);
           }

		/**
		 * Decription: Customer Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CustNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CustNm = "U_CustNm";
		public string U_CustNm { 
			get {
 				return getValueAsString(_CustNm); 
			}
			set { setValue(_CustNm, value); }
		}
           public string doValidate_CustNm() {
               return doValidate_CustNm(U_CustNm);
           }
           public virtual string doValidate_CustNm(object oValue) {
               return base.doValidation(_CustNm, oValue);
           }

		/**
		 * Decription: Customer In Payment
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CustPay
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CustPay = "U_CustPay";
		public string U_CustPay { 
			get {
 				return getValueAsString(_CustPay); 
			}
			set { setValue(_CustPay, value); }
		}
           public string doValidate_CustPay() {
               return doValidate_CustPay(U_CustPay);
           }
           public virtual string doValidate_CustPay(object oValue) {
               return base.doValidation(_CustPay, oValue);
           }

		/**
		 * Decription: Customer Rebate Item
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CustReb
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CustReb = "U_CustReb";
		public string U_CustReb { 
			get {
 				return getValueAsString(_CustReb); 
			}
			set { setValue(_CustReb, value); }
		}
           public string doValidate_CustReb() {
               return doValidate_CustReb(U_CustReb);
           }
           public virtual string doValidate_CustReb(object oValue) {
               return base.doValidation(_CustReb, oValue);
           }

		/**
		 * Decription: Customer Ref. No.
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CustRef
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _CustRef = "U_CustRef";
		public string U_CustRef { 
			get {
 				return getValueAsString(_CustRef); 
			}
			set { setValue(_CustRef, value); }
		}
           public string doValidate_CustRef() {
               return doValidate_CustRef(U_CustRef);
           }
           public virtual string doValidate_CustRef(object oValue) {
               return base.doValidation(_CustRef, oValue);
           }

		/**
		 * Decription: DA Attached
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DAAttach
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DAAttach = "U_DAAttach";
		public string U_DAAttach { 
			get {
 				return getValueAsString(_DAAttach); 
			}
			set { setValue(_DAAttach, value); }
		}
           public string doValidate_DAAttach() {
               return doValidate_DAAttach(U_DAAttach);
           }
           public virtual string doValidate_DAAttach(object oValue) {
               return base.doValidation(_DAAttach, oValue);
           }

		/**
		 * Decription: Discount Amount
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DisAmt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _DisAmt = "U_DisAmt";
		public double U_DisAmt { 
			get {
 				return getValueAsDouble(_DisAmt); 
			}
			set { setValue(_DisAmt, value); }
		}
           public string doValidate_DisAmt() {
               return doValidate_DisAmt(U_DisAmt);
           }
           public virtual string doValidate_DisAmt(object oValue) {
               return base.doValidation(_DisAmt, oValue);
           }

		/**
		 * Decription: Discount
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Discnt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Discnt = "U_Discnt";
		public double U_Discnt { 
			get {
 				return getValueAsDouble(_Discnt); 
			}
			set { setValue(_Discnt, value); }
		}
           public string doValidate_Discnt() {
               return doValidate_Discnt(U_Discnt);
           }
           public virtual string doValidate_Discnt(object oValue) {
               return base.doValidation(_Discnt, oValue);
           }

		/**
		 * Decription: Disposal Charge Currency
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DispCgCc
		 * Size: 3
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DispCgCc = "U_DispCgCc";
		public string U_DispCgCc { 
			get {
 				return getValueAsString(_DispCgCc); 
			}
			set { setValue(_DispCgCc, value); }
		}
           public string doValidate_DispCgCc() {
               return doValidate_DispCgCc(U_DispCgCc);
           }
           public virtual string doValidate_DispCgCc(object oValue) {
               return base.doValidation(_DispCgCc, oValue);
           }

		/**
		 * Decription: Disposal Cost Currency
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DispCoCc
		 * Size: 3
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DispCoCc = "U_DispCoCc";
		public string U_DispCoCc { 
			get {
 				return getValueAsString(_DispCoCc); 
			}
			set { setValue(_DispCoCc, value); }
		}
           public string doValidate_DispCoCc() {
               return doValidate_DispCoCc(U_DispCoCc);
           }
           public virtual string doValidate_DispCoCc(object oValue) {
               return base.doValidation(_DispCoCc, oValue);
           }

		/**
		 * Decription: Distance
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Dista
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Dista = "U_Dista";
		public double U_Dista { 
			get {
 				return getValueAsDouble(_Dista); 
			}
			set { setValue(_Dista, value); }
		}
           public string doValidate_Dista() {
               return doValidate_Dista(U_Dista);
           }
           public virtual string doValidate_Dista(object oValue) {
               return base.doValidation(_Dista, oValue);
           }

		/**
		 * Decription: Docket Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DocNum
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DocNum = "U_DocNum";
		public string U_DocNum { 
			get {
 				return getValueAsString(_DocNum); 
			}
			set { setValue(_DocNum, value); }
		}
           public string doValidate_DocNum() {
               return doValidate_DocNum(U_DocNum);
           }
           public virtual string doValidate_DocNum(object oValue) {
               return base.doValidation(_DocNum, oValue);
           }

		/**
		 * Decription: DPR Referance
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DPRRef
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _DPRRef = "U_DPRRef";
		public string U_DPRRef { 
			get {
 				return getValueAsString(_DPRRef); 
			}
			set { setValue(_DPRRef, value); }
		}
           public string doValidate_DPRRef() {
               return doValidate_DPRRef(U_DPRRef);
           }
           public virtual string doValidate_DPRRef(object oValue) {
               return base.doValidation(_DPRRef, oValue);
           }

		/**
		 * Decription: Driver
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Driver
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Driver = "U_Driver";
		public string U_Driver { 
			get {
 				return getValueAsString(_Driver); 
			}
			set { setValue(_Driver, value); }
		}
           public string doValidate_Driver() {
               return doValidate_Driver(U_Driver);
           }
           public virtual string doValidate_Driver(object oValue) {
               return base.doValidation(_Driver, oValue);
           }

		/**
		 * Decription: Driver Off Site Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DrivrOfSitDt
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _DrivrOfSitDt = "U_DrivrOfSitDt";
		public DateTime U_DrivrOfSitDt { 
			get {
 				return getValueAsDateTime(_DrivrOfSitDt); 
			}
			set { setValue(_DrivrOfSitDt, value); }
		}
		public void U_DrivrOfSitDt_AsString(string value){
			setValue("U_DrivrOfSitDt", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_DrivrOfSitDt_AsString(){
			DateTime dVal = getValueAsDateTime(_DrivrOfSitDt);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_DrivrOfSitDt() {
               return doValidate_DrivrOfSitDt(U_DrivrOfSitDt);
           }
           public virtual string doValidate_DrivrOfSitDt(object oValue) {
               return base.doValidation(_DrivrOfSitDt, oValue);
           }

		/**
		 * Decription: Driver Off Site Time
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DrivrOfSitTm
		 * Size: 6
		 * Type: db_Date
		 * CType: string
		 * SubType: st_Time
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _DrivrOfSitTm = "U_DrivrOfSitTm";
		public string U_DrivrOfSitTm { 
			get {
 				return getValueAsString(_DrivrOfSitTm); 
			}
			set { setValue(_DrivrOfSitTm, value); }
		}
           public string doValidate_DrivrOfSitTm() {
               return doValidate_DrivrOfSitTm(U_DrivrOfSitTm);
           }
           public virtual string doValidate_DrivrOfSitTm(object oValue) {
               return base.doValidation(_DrivrOfSitTm, oValue);
           }

		/**
		 * Decription: Drive On Site Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DrivrOnSitDt
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _DrivrOnSitDt = "U_DrivrOnSitDt";
		public DateTime U_DrivrOnSitDt { 
			get {
 				return getValueAsDateTime(_DrivrOnSitDt); 
			}
			set { setValue(_DrivrOnSitDt, value); }
		}
		public void U_DrivrOnSitDt_AsString(string value){
			setValue("U_DrivrOnSitDt", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_DrivrOnSitDt_AsString(){
			DateTime dVal = getValueAsDateTime(_DrivrOnSitDt);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_DrivrOnSitDt() {
               return doValidate_DrivrOnSitDt(U_DrivrOnSitDt);
           }
           public virtual string doValidate_DrivrOnSitDt(object oValue) {
               return base.doValidation(_DrivrOnSitDt, oValue);
           }

		/**
		 * Decription: Drive On Site Time
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DrivrOnSitTm
		 * Size: 6
		 * Type: db_Date
		 * CType: string
		 * SubType: st_Time
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _DrivrOnSitTm = "U_DrivrOnSitTm";
		public string U_DrivrOnSitTm { 
			get {
 				return getValueAsString(_DrivrOnSitTm); 
			}
			set { setValue(_DrivrOnSitTm, value); }
		}
           public string doValidate_DrivrOnSitTm() {
               return doValidate_DrivrOnSitTm(U_DrivrOnSitTm);
           }
           public virtual string doValidate_DrivrOnSitTm(object oValue) {
               return base.doValidation(_DrivrOnSitTm, oValue);
           }

		/**
		 * Decription: Dusty
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Dusty
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Dusty = "U_Dusty";
		public string U_Dusty { 
			get {
 				return getValueAsString(_Dusty); 
			}
			set { setValue(_Dusty, value); }
		}
           public string doValidate_Dusty() {
               return doValidate_Dusty(U_Dusty);
           }
           public virtual string doValidate_Dusty(object oValue) {
               return base.doValidation(_Dusty, oValue);
           }

		/**
		 * Decription: EN Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ENNO
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ENNO = "U_ENNO";
		public string U_ENNO { 
			get {
 				return getValueAsString(_ENNO); 
			}
			set { setValue(_ENNO, value); }
		}
           public string doValidate_ENNO() {
               return doValidate_ENNO(U_ENNO);
           }
           public virtual string doValidate_ENNO(object oValue) {
               return base.doValidation(_ENNO, oValue);
           }

		/**
		 * Decription: Enquiry ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_EnqID
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _EnqID = "U_EnqID";
		public string U_EnqID { 
			get {
 				return getValueAsString(_EnqID); 
			}
			set { setValue(_EnqID, value); }
		}
           public string doValidate_EnqID() {
               return doValidate_EnqID(U_EnqID);
           }
           public virtual string doValidate_EnqID(object oValue) {
               return base.doValidation(_EnqID, oValue);
           }

		/**
		 * Decription: Enquiry Line ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_EnqLID
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _EnqLID = "U_EnqLID";
		public string U_EnqLID { 
			get {
 				return getValueAsString(_EnqLID); 
			}
			set { setValue(_EnqLID, value); }
		}
           public string doValidate_EnqLID() {
               return doValidate_EnqLID(U_EnqLID);
           }
           public virtual string doValidate_EnqLID(object oValue) {
               return base.doValidation(_EnqLID, oValue);
           }

		/**
		 * Decription: Evidence Reference Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ENREF
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ENREF = "U_ENREF";
		public string U_ENREF { 
			get {
 				return getValueAsString(_ENREF); 
			}
			set { setValue(_ENREF, value); }
		}
           public string doValidate_ENREF() {
               return doValidate_ENREF(U_ENREF);
           }
           public virtual string doValidate_ENREF(object oValue) {
               return base.doValidation(_ENREF, oValue);
           }

		/**
		 * Decription: Load Weight
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ExpLdWgt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Measurement
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _ExpLdWgt = "U_ExpLdWgt";
		public double U_ExpLdWgt { 
			get {
 				return getValueAsDouble(_ExpLdWgt); 
			}
			set { setValue(_ExpLdWgt, value); }
		}
           public string doValidate_ExpLdWgt() {
               return doValidate_ExpLdWgt(U_ExpLdWgt);
           }
           public virtual string doValidate_ExpLdWgt(object oValue) {
               return base.doValidation(_ExpLdWgt, oValue);
           }

		/**
		 * Decription: Ext. Comment1
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ExtComm1
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ExtComm1 = "U_ExtComm1";
		public string U_ExtComm1 { 
			get {
 				return getValueAsString(_ExtComm1); 
			}
			set { setValue(_ExtComm1, value); }
		}
           public string doValidate_ExtComm1() {
               return doValidate_ExtComm1(U_ExtComm1);
           }
           public virtual string doValidate_ExtComm1(object oValue) {
               return base.doValidation(_ExtComm1, oValue);
           }

		/**
		 * Decription: Ext. Comment2
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ExtComm2
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ExtComm2 = "U_ExtComm2";
		public string U_ExtComm2 { 
			get {
 				return getValueAsString(_ExtComm2); 
			}
			set { setValue(_ExtComm2, value); }
		}
           public string doValidate_ExtComm2() {
               return doValidate_ExtComm2(U_ExtComm2);
           }
           public virtual string doValidate_ExtComm2(object oValue) {
               return base.doValidation(_ExtComm2, oValue);
           }

		/**
		 * Decription: External Weighbridge Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ExtWeig
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ExtWeig = "U_ExtWeig";
		public string U_ExtWeig { 
			get {
 				return getValueAsString(_ExtWeig); 
			}
			set { setValue(_ExtWeig, value); }
		}
           public string doValidate_ExtWeig() {
               return doValidate_ExtWeig(U_ExtWeig);
           }
           public virtual string doValidate_ExtWeig(object oValue) {
               return base.doValidation(_ExtWeig, oValue);
           }

		/**
		 * Decription: Firm
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Firm
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Firm = "U_Firm";
		public string U_Firm { 
			get {
 				return getValueAsString(_Firm); 
			}
			set { setValue(_Firm, value); }
		}
           public string doValidate_Firm() {
               return doValidate_Firm(U_Firm);
           }
           public virtual string doValidate_Firm(object oValue) {
               return base.doValidation(_Firm, oValue);
           }

		/**
		 * Decription: Fluid
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Fluid
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Fluid = "U_Fluid";
		public string U_Fluid { 
			get {
 				return getValueAsString(_Fluid); 
			}
			set { setValue(_Fluid, value); }
		}
           public string doValidate_Fluid() {
               return doValidate_Fluid(U_Fluid);
           }
           public virtual string doValidate_Fluid(object oValue) {
               return base.doValidation(_Fluid, oValue);
           }

		/**
		 * Decription: Freeze Price Charge Haulage
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FPChH
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _FPChH = "U_FPChH";
		public string U_FPChH { 
			get {
 				return getValueAsString(_FPChH); 
			}
			set { setValue(_FPChH, value); }
		}
           public string doValidate_FPChH() {
               return doValidate_FPChH(U_FPChH);
           }
           public virtual string doValidate_FPChH(object oValue) {
               return base.doValidation(_FPChH, oValue);
           }

		/**
		 * Decription: Freeze Price Charge Tipping
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FPChT
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _FPChT = "U_FPChT";
		public string U_FPChT { 
			get {
 				return getValueAsString(_FPChT); 
			}
			set { setValue(_FPChT, value); }
		}
           public string doValidate_FPChT() {
               return doValidate_FPChT(U_FPChT);
           }
           public virtual string doValidate_FPChT(object oValue) {
               return base.doValidation(_FPChT, oValue);
           }

		/**
		 * Decription: Freeze Price Cost Haulage
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FPCoH
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _FPCoH = "U_FPCoH";
		public string U_FPCoH { 
			get {
 				return getValueAsString(_FPCoH); 
			}
			set { setValue(_FPCoH, value); }
		}
           public string doValidate_FPCoH() {
               return doValidate_FPCoH(U_FPCoH);
           }
           public virtual string doValidate_FPCoH(object oValue) {
               return base.doValidation(_FPCoH, oValue);
           }

		/**
		 * Decription: Freeze Price Cost Tipping
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FPCoT
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _FPCoT = "U_FPCoT";
		public string U_FPCoT { 
			get {
 				return getValueAsString(_FPCoT); 
			}
			set { setValue(_FPCoT, value); }
		}
           public string doValidate_FPCoT() {
               return doValidate_FPCoT(U_FPCoT);
           }
           public virtual string doValidate_FPCoT(object oValue) {
               return base.doValidation(_FPCoT, oValue);
           }

		/**
		 * Decription: Gross Margin
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_GMargin
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Percentage
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _GMargin = "U_GMargin";
		public double U_GMargin { 
			get {
 				return getValueAsDouble(_GMargin); 
			}
			set { setValue(_GMargin, value); }
		}
           public string doValidate_GMargin() {
               return doValidate_GMargin(U_GMargin);
           }
           public virtual string doValidate_GMargin(object oValue) {
               return base.doValidation(_GMargin, oValue);
           }

		/**
		 * Decription: Goods Receipt Incomming
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_GRIn
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _GRIn = "U_GRIn";
		public string U_GRIn { 
			get {
 				return getValueAsString(_GRIn); 
			}
			set { setValue(_GRIn, value); }
		}
           public string doValidate_GRIn() {
               return doValidate_GRIn(U_GRIn);
           }
           public virtual string doValidate_GRIn(object oValue) {
               return base.doValidation(_GRIn, oValue);
           }

		/**
		 * Decription: Haulage Automatic Calculation
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HaulAC
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _HaulAC = "U_HaulAC";
		public string U_HaulAC { 
			get {
 				return getValueAsString(_HaulAC); 
			}
			set { setValue(_HaulAC, value); }
		}
           public string doValidate_HaulAC() {
               return doValidate_HaulAC(U_HaulAC);
           }
           public virtual string doValidate_HaulAC(object oValue) {
               return base.doValidation(_HaulAC, oValue);
           }

		/**
		 * Decription: Hazardous Waste Consignment No
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HazWCNN
		 * Size: 12
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _HazWCNN = "U_HazWCNN";
		public string U_HazWCNN { 
			get {
 				return getValueAsString(_HazWCNN); 
			}
			set { setValue(_HazWCNN, value); }
		}
           public string doValidate_HazWCNN() {
               return doValidate_HazWCNN(U_HazWCNN);
           }
           public virtual string doValidate_HazWCNN(object oValue) {
               return base.doValidation(_HazWCNN, oValue);
           }

		/**
		 * Decription: Haulage Sales Vat Group
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HChrgVtGrp
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _HChrgVtGrp = "U_HChrgVtGrp";
		public string U_HChrgVtGrp { 
			get {
 				return getValueAsString(_HChrgVtGrp); 
			}
			set { setValue(_HChrgVtGrp, value); }
		}
           public string doValidate_HChrgVtGrp() {
               return doValidate_HChrgVtGrp(U_HChrgVtGrp);
           }
           public virtual string doValidate_HChrgVtGrp(object oValue) {
               return base.doValidation(_HChrgVtGrp, oValue);
           }

		/**
		 * Decription: Haulage Sales Vat Rate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HChrgVtRt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Percentage
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _HChrgVtRt = "U_HChrgVtRt";
		public double U_HChrgVtRt { 
			get {
 				return getValueAsDouble(_HChrgVtRt); 
			}
			set { setValue(_HChrgVtRt, value); }
		}
           public string doValidate_HChrgVtRt() {
               return doValidate_HChrgVtRt(U_HChrgVtRt);
           }
           public virtual string doValidate_HChrgVtRt(object oValue) {
               return base.doValidation(_HChrgVtRt, oValue);
           }

		/**
		 * Decription: Tipping Buying Vat Group
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HCostVtGrp
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _HCostVtGrp = "U_HCostVtGrp";
		public string U_HCostVtGrp { 
			get {
 				return getValueAsString(_HCostVtGrp); 
			}
			set { setValue(_HCostVtGrp, value); }
		}
           public string doValidate_HCostVtGrp() {
               return doValidate_HCostVtGrp(U_HCostVtGrp);
           }
           public virtual string doValidate_HCostVtGrp(object oValue) {
               return base.doValidation(_HCostVtGrp, oValue);
           }

		/**
		 * Decription: Tipping Buying Vat Rate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HCostVtRt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Percentage
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _HCostVtRt = "U_HCostVtRt";
		public double U_HCostVtRt { 
			get {
 				return getValueAsDouble(_HCostVtRt); 
			}
			set { setValue(_HCostVtRt, value); }
		}
           public string doValidate_HCostVtRt() {
               return doValidate_HCostVtRt(U_HCostVtRt);
           }
           public virtual string doValidate_HCostVtRt(object oValue) {
               return base.doValidation(_HCostVtRt, oValue);
           }

		/**
		 * Decription: Haulage Selling Qty
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HlSQty
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Measurement
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _HlSQty = "U_HlSQty";
		public double U_HlSQty { 
			get {
 				return getValueAsDouble(_HlSQty); 
			}
			set { setValue(_HlSQty, value); }
		}
           public string doValidate_HlSQty() {
               return doValidate_HlSQty(U_HlSQty);
           }
           public virtual string doValidate_HlSQty(object oValue) {
               return base.doValidation(_HlSQty, oValue);
           }

		/**
		 * Decription: Check
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHCHK
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _IDHCHK = "U_IDHCHK";
		public string U_IDHCHK { 
			get {
 				return getValueAsString(_IDHCHK); 
			}
			set { setValue(_IDHCHK, value); }
		}
           public string doValidate_IDHCHK() {
               return doValidate_IDHCHK(U_IDHCHK);
           }
           public virtual string doValidate_IDHCHK(object oValue) {
               return base.doValidation(_IDHCHK, oValue);
           }

		/**
		 * Decription: Route Comments
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHCMT
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _IDHCMT = "U_IDHCMT";
		public string U_IDHCMT { 
			get {
 				return getValueAsString(_IDHCMT); 
			}
			set { setValue(_IDHCMT, value); }
		}
           public string doValidate_IDHCMT() {
               return doValidate_IDHCMT(U_IDHCMT);
           }
           public virtual string doValidate_IDHCMT(object oValue) {
               return base.doValidation(_IDHCMT, oValue);
           }

		/**
		 * Decription: Driver Instructions
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHDRVI
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _IDHDRVI = "U_IDHDRVI";
		public string U_IDHDRVI { 
			get {
 				return getValueAsString(_IDHDRVI); 
			}
			set { setValue(_IDHDRVI, value); }
		}
           public string doValidate_IDHDRVI() {
               return doValidate_IDHDRVI(U_IDHDRVI);
           }
           public virtual string doValidate_IDHDRVI(object oValue) {
               return base.doValidation(_IDHDRVI, oValue);
           }

		/**
		 * Decription: Printed
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHPRTD
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _IDHPRTD = "U_IDHPRTD";
		public string U_IDHPRTD { 
			get {
 				return getValueAsString(_IDHPRTD); 
			}
			set { setValue(_IDHPRTD, value); }
		}
           public string doValidate_IDHPRTD() {
               return doValidate_IDHPRTD(U_IDHPRTD);
           }
           public virtual string doValidate_IDHPRTD(object oValue) {
               return base.doValidation(_IDHPRTD, oValue);
           }

		/**
		 * Decription: Route Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHRTCD
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _IDHRTCD = "U_IDHRTCD";
		public string U_IDHRTCD { 
			get {
 				return getValueAsString(_IDHRTCD); 
			}
			set { setValue(_IDHRTCD, value); }
		}
           public string doValidate_IDHRTCD() {
               return doValidate_IDHRTCD(U_IDHRTCD);
           }
           public virtual string doValidate_IDHRTCD(object oValue) {
               return base.doValidation(_IDHRTCD, oValue);
           }

		/**
		 * Decription: Route Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHRTDT
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _IDHRTDT = "U_IDHRTDT";
		public DateTime U_IDHRTDT { 
			get {
 				return getValueAsDateTime(_IDHRTDT); 
			}
			set { setValue(_IDHRTDT, value); }
		}
		public void U_IDHRTDT_AsString(string value){
			setValue("U_IDHRTDT", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_IDHRTDT_AsString(){
			DateTime dVal = getValueAsDateTime(_IDHRTDT);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_IDHRTDT() {
               return doValidate_IDHRTDT(U_IDHRTDT);
           }
           public virtual string doValidate_IDHRTDT(object oValue) {
               return base.doValidation(_IDHRTDT, oValue);
           }

		/**
		 * Decription: Sequence
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHSEQ
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _IDHSEQ = "U_IDHSEQ";
		public int U_IDHSEQ { 
			get {
 				return getValueAsInt(_IDHSEQ); 
			}
			set { setValue(_IDHSEQ, value); }
		}
           public string doValidate_IDHSEQ() {
               return doValidate_IDHSEQ(U_IDHSEQ);
           }
           public virtual string doValidate_IDHSEQ(object oValue) {
               return base.doValidation(_IDHSEQ, oValue);
           }

		/**
		 * Decription: Internal Comment
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IntComm
		 * Size: 230
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _IntComm = "U_IntComm";
		public string U_IntComm { 
			get {
 				return getValueAsString(_IntComm); 
			}
			set { setValue(_IntComm, value); }
		}
           public string doValidate_IntComm() {
               return doValidate_IntComm(U_IntComm);
           }
           public virtual string doValidate_IntComm(object oValue) {
               return base.doValidation(_IntComm, oValue);
           }

		/**
		 * Decription: Issue Qty
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IssQty
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _IssQty = "U_IssQty";
		public double U_IssQty { 
			get {
 				return getValueAsDouble(_IssQty); 
			}
			set { setValue(_IssQty, value); }
		}
           public string doValidate_IssQty() {
               return doValidate_IssQty(U_IssQty);
           }
           public virtual string doValidate_IssQty(object oValue) {
               return base.doValidation(_IssQty, oValue);
           }

		/**
		 * Decription: Trail Load
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IsTrl
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _IsTrl = "U_IsTrl";
		public string U_IsTrl { 
			get {
 				return getValueAsString(_IsTrl); 
			}
			set { setValue(_IsTrl, value); }
		}
           public string doValidate_IsTrl() {
               return doValidate_IsTrl(U_IsTrl);
           }
           public virtual string doValidate_IsTrl(object oValue) {
               return base.doValidation(_IsTrl, oValue);
           }

		/**
		 * Decription: Item Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItemCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ItemCd = "U_ItemCd";
		public string U_ItemCd { 
			get {
 				return getValueAsString(_ItemCd); 
			}
			set { setValue(_ItemCd, value); }
		}
           public string doValidate_ItemCd() {
               return doValidate_ItemCd(U_ItemCd);
           }
           public virtual string doValidate_ItemCd(object oValue) {
               return base.doValidation(_ItemCd, oValue);
           }

		/**
		 * Decription: Item Description
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItemDsc
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ItemDsc = "U_ItemDsc";
		public string U_ItemDsc { 
			get {
 				return getValueAsString(_ItemDsc); 
			}
			set { setValue(_ItemDsc, value); }
		}
           public string doValidate_ItemDsc() {
               return doValidate_ItemDsc(U_ItemDsc);
           }
           public virtual string doValidate_ItemDsc(object oValue) {
               return base.doValidation(_ItemDsc, oValue);
           }

		/**
		 * Decription: Item Group Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItmGrp
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ItmGrp = "U_ItmGrp";
		public string U_ItmGrp { 
			get {
 				return getValueAsString(_ItmGrp); 
			}
			set { setValue(_ItmGrp, value); }
		}
           public string doValidate_ItmGrp() {
               return doValidate_ItmGrp(U_ItmGrp);
           }
           public virtual string doValidate_ItmGrp(object oValue) {
               return base.doValidation(_ItmGrp, oValue);
           }

		/**
		 * Decription: Jobs To Be Done Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_JbToBeDoneDt
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _JbToBeDoneDt = "U_JbToBeDoneDt";
		public DateTime U_JbToBeDoneDt { 
			get {
 				return getValueAsDateTime(_JbToBeDoneDt); 
			}
			set { setValue(_JbToBeDoneDt, value); }
		}
		public void U_JbToBeDoneDt_AsString(string value){
			setValue("U_JbToBeDoneDt", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_JbToBeDoneDt_AsString(){
			DateTime dVal = getValueAsDateTime(_JbToBeDoneDt);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_JbToBeDoneDt() {
               return doValidate_JbToBeDoneDt(U_JbToBeDoneDt);
           }
           public virtual string doValidate_JbToBeDoneDt(object oValue) {
               return base.doValidation(_JbToBeDoneDt, oValue);
           }

		/**
		 * Decription: Jobs To Be Done Time
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_JbToBeDoneTm
		 * Size: 6
		 * Type: db_Date
		 * CType: string
		 * SubType: st_Time
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _JbToBeDoneTm = "U_JbToBeDoneTm";
		public string U_JbToBeDoneTm { 
			get {
 				return getValueAsString(_JbToBeDoneTm); 
			}
			set { setValue(_JbToBeDoneTm, value); }
		}
           public string doValidate_JbToBeDoneTm() {
               return doValidate_JbToBeDoneTm(U_JbToBeDoneTm);
           }
           public virtual string doValidate_JbToBeDoneTm(object oValue) {
               return base.doValidation(_JbToBeDoneTm, oValue);
           }

		/**
		 * Decription: Order Row Cost Total
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_JCost
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _JCost = "U_JCost";
		public double U_JCost { 
			get {
 				return getValueAsDouble(_JCost); 
			}
			set { setValue(_JCost, value); }
		}
           public string doValidate_JCost() {
               return doValidate_JCost(U_JCost);
           }
           public virtual string doValidate_JCost(object oValue) {
               return base.doValidation(_JCost, oValue);
           }

		/**
		 * Decription: Order Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_JobNr
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _JobNr = "U_JobNr";
		public string U_JobNr { 
			get {
 				return getValueAsString(_JobNr); 
			}
			set { setValue(_JobNr, value); }
		}
           public string doValidate_JobNr() {
               return doValidate_JobNr(U_JobNr);
           }
           public virtual string doValidate_JobNr(object oValue) {
               return base.doValidation(_JobNr, oValue);
           }

		/**
		 * Decription: Job PO
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_JOBPO
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _JOBPO = "U_JOBPO";
		public string U_JOBPO { 
			get {
 				return getValueAsString(_JOBPO); 
			}
			set { setValue(_JOBPO, value); }
		}
           public string doValidate_JOBPO() {
               return doValidate_JOBPO(U_JOBPO);
           }
           public virtual string doValidate_JOBPO(object oValue) {
               return base.doValidation(_JOBPO, oValue);
           }

		/**
		 * Decription: Reminder Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_JobRmD
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _JobRmD = "U_JobRmD";
		public DateTime U_JobRmD { 
			get {
 				return getValueAsDateTime(_JobRmD); 
			}
			set { setValue(_JobRmD, value); }
		}
		public void U_JobRmD_AsString(string value){
			setValue("U_JobRmD", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_JobRmD_AsString(){
			DateTime dVal = getValueAsDateTime(_JobRmD);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_JobRmD() {
               return doValidate_JobRmD(U_JobRmD);
           }
           public virtual string doValidate_JobRmD(object oValue) {
               return base.doValidation(_JobRmD, oValue);
           }

		/**
		 * Decription: Reminder Time
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_JobRmT
		 * Size: 6
		 * Type: db_Date
		 * CType: string
		 * SubType: st_Time
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _JobRmT = "U_JobRmT";
		public string U_JobRmT { 
			get {
 				return getValueAsString(_JobRmT); 
			}
			set { setValue(_JobRmT, value); }
		}
           public string doValidate_JobRmT() {
               return doValidate_JobRmT(U_JobRmT);
           }
           public virtual string doValidate_JobRmT(object oValue) {
               return base.doValidation(_JobRmT, oValue);
           }

		/**
		 * Decription: Order Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_JobTp
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _JobTp = "U_JobTp";
		public string U_JobTp { 
			get {
 				return getValueAsString(_JobTp); 
			}
			set { setValue(_JobTp, value); }
		}
           public string doValidate_JobTp() {
               return doValidate_JobTp(U_JobTp);
           }
           public virtual string doValidate_JobTp(object oValue) {
               return base.doValidation(_JobTp, oValue);
           }

		/**
		 * Decription: Journal Entry
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Jrnl
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Jrnl = "U_Jrnl";
		public string U_Jrnl { 
			get {
 				return getValueAsString(_Jrnl); 
			}
			set { setValue(_Jrnl, value); }
		}
           public string doValidate_Jrnl() {
               return doValidate_Jrnl(U_Jrnl);
           }
           public virtual string doValidate_Jrnl(object oValue) {
               return base.doValidation(_Jrnl, oValue);
           }

		/**
		 * Decription: Journals Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_JStat
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _JStat = "U_JStat";
		public string U_JStat { 
			get {
 				return getValueAsString(_JStat); 
			}
			set { setValue(_JStat, value); }
		}
           public string doValidate_JStat() {
               return doValidate_JStat(U_JStat);
           }
           public virtual string doValidate_JStat(object oValue) {
               return base.doValidation(_JStat, oValue);
           }

		/**
		 * Decription: Lock down Prices
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_LckPrc
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _LckPrc = "U_LckPrc";
		public string U_LckPrc { 
			get {
 				return getValueAsString(_LckPrc); 
			}
			set { setValue(_LckPrc, value); }
		}
           public string doValidate_LckPrc() {
               return doValidate_LckPrc(U_LckPrc);
           }
           public virtual string doValidate_LckPrc(object oValue) {
               return base.doValidation(_LckPrc, oValue);
           }

		/**
		 * Decription: Liscence Cost Currency
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_LiscCoCc
		 * Size: 3
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _LiscCoCc = "U_LiscCoCc";
		public string U_LiscCoCc { 
			get {
 				return getValueAsString(_LiscCoCc); 
			}
			set { setValue(_LiscCoCc, value); }
		}
           public string doValidate_LiscCoCc() {
               return doValidate_LiscCoCc(U_LiscCoCc);
           }
           public virtual string doValidate_LiscCoCc(object oValue) {
               return base.doValidation(_LiscCoCc, oValue);
           }

		/**
		 * Decription: Linked PBI Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_LnkPBI
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _LnkPBI = "U_LnkPBI";
		public string U_LnkPBI { 
			get {
 				return getValueAsString(_LnkPBI); 
			}
			set { setValue(_LnkPBI, value); }
		}
           public string doValidate_LnkPBI() {
               return doValidate_LnkPBI(U_LnkPBI);
           }
           public virtual string doValidate_LnkPBI(object oValue) {
               return base.doValidation(_LnkPBI, oValue);
           }

		/**
		 * Decription: Load Sheet Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_LoadSht
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _LoadSht = "U_LoadSht";
		public string U_LoadSht { 
			get {
 				return getValueAsString(_LoadSht); 
			}
			set { setValue(_LoadSht, value); }
		}
           public string doValidate_LoadSht() {
               return doValidate_LoadSht(U_LoadSht);
           }
           public virtual string doValidate_LoadSht(object oValue) {
               return base.doValidation(_LoadSht, oValue);
           }

		/**
		 * Decription: Lorry Registration
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Lorry
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Lorry = "U_Lorry";
		public string U_Lorry { 
			get {
 				return getValueAsString(_Lorry); 
			}
			set { setValue(_Lorry, value); }
		}
           public string doValidate_Lorry() {
               return doValidate_Lorry(U_Lorry);
           }
           public virtual string doValidate_Lorry(object oValue) {
               return base.doValidation(_Lorry, oValue);
           }

		/**
		 * Decription: Lorry Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_LorryCd
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _LorryCd = "U_LorryCd";
		public string U_LorryCd { 
			get {
 				return getValueAsString(_LorryCd); 
			}
			set { setValue(_LorryCd, value); }
		}
           public string doValidate_LorryCd() {
               return doValidate_LorryCd(U_LorryCd);
           }
           public virtual string doValidate_LorryCd(object oValue) {
               return base.doValidation(_LorryCd, oValue);
           }

		/**
		 * Decription: List Price
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_LstPrice
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _LstPrice = "U_LstPrice";
		public double U_LstPrice { 
			get {
 				return getValueAsDouble(_LstPrice); 
			}
			set { setValue(_LstPrice, value); }
		}
           public string doValidate_LstPrice() {
               return doValidate_LstPrice(U_LstPrice);
           }
           public virtual string doValidate_LstPrice(object oValue) {
               return base.doValidation(_LstPrice, oValue);
           }

		/**
		 * Decription: Manual prices entered ( 1 - Ti
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_MANPRC
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _MANPRC = "U_MANPRC";
		public short U_MANPRC { 
			get {
 				return getValueAsShort(_MANPRC); 
			}
			set { setValue(_MANPRC, value); }
		}
           public string doValidate_MANPRC() {
               return doValidate_MANPRC(U_MANPRC);
           }
           public virtual string doValidate_MANPRC(object oValue) {
               return base.doValidation(_MANPRC, oValue);
           }

		/**
		 * Decription: Maximo Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_MaximoNum
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _MaximoNum = "U_MaximoNum";
		public string U_MaximoNum { 
			get {
 				return getValueAsString(_MaximoNum); 
			}
			set { setValue(_MaximoNum, value); }
		}
           public string doValidate_MaximoNum() {
               return doValidate_MaximoNum(U_MaximoNum);
           }
           public virtual string doValidate_MaximoNum(object oValue) {
               return base.doValidation(_MaximoNum, oValue);
           }

		/**
		 * Decription: Marketing Document Changed
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_MDChngd
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _MDChngd = "U_MDChngd";
		public string U_MDChngd { 
			get {
 				return getValueAsString(_MDChngd); 
			}
			set { setValue(_MDChngd, value); }
		}
           public string doValidate_MDChngd() {
               return doValidate_MDChngd(U_MDChngd);
           }
           public virtual string doValidate_MDChngd(object oValue) {
               return base.doValidation(_MDChngd, oValue);
           }

		/**
		 * Decription: Manual Margin
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_MnMargin
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Percentage
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _MnMargin = "U_MnMargin";
		public double U_MnMargin { 
			get {
 				return getValueAsDouble(_MnMargin); 
			}
			set { setValue(_MnMargin, value); }
		}
           public string doValidate_MnMargin() {
               return doValidate_MnMargin(U_MnMargin);
           }
           public virtual string doValidate_MnMargin(object oValue) {
               return base.doValidation(_MnMargin, oValue);
           }

		/**
		 * Decription: MSDS
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_MSDS
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _MSDS = "U_MSDS";
		public string U_MSDS { 
			get {
 				return getValueAsString(_MSDS); 
			}
			set { setValue(_MSDS, value); }
		}
           public string doValidate_MSDS() {
               return doValidate_MSDS(U_MSDS);
           }
           public virtual string doValidate_MSDS(object oValue) {
               return base.doValidation(_MSDS, oValue);
           }

		/**
		 * Decription: Obligated
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Obligated
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Obligated = "U_Obligated";
		public string U_Obligated { 
			get {
 				return getValueAsString(_Obligated); 
			}
			set { setValue(_Obligated, value); }
		}
           public string doValidate_Obligated() {
               return doValidate_Obligated(U_Obligated);
           }
           public virtual string doValidate_Obligated(object oValue) {
               return base.doValidation(_Obligated, oValue);
           }

		/**
		 * Decription: Order (Haulage) Cost
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_OrdCost
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _OrdCost = "U_OrdCost";
		public double U_OrdCost { 
			get {
 				return getValueAsDouble(_OrdCost); 
			}
			set { setValue(_OrdCost, value); }
		}
           public string doValidate_OrdCost() {
               return doValidate_OrdCost(U_OrdCost);
           }
           public virtual string doValidate_OrdCost(object oValue) {
               return base.doValidation(_OrdCost, oValue);
           }

		/**
		 * Decription: Order (Haulage) Total Cost
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_OrdTot
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _OrdTot = "U_OrdTot";
		public double U_OrdTot { 
			get {
 				return getValueAsDouble(_OrdTot); 
			}
			set { setValue(_OrdTot, value); }
		}
           public string doValidate_OrdTot() {
               return doValidate_OrdTot(U_OrdTot);
           }
           public virtual string doValidate_OrdTot(object oValue) {
               return base.doValidation(_OrdTot, oValue);
           }

		/**
		 * Decription: Order (Haulage) Weight
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_OrdWgt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Measurement
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _OrdWgt = "U_OrdWgt";
		public double U_OrdWgt { 
			get {
 				return getValueAsDouble(_OrdWgt); 
			}
			set { setValue(_OrdWgt, value); }
		}
           public string doValidate_OrdWgt() {
               return doValidate_OrdWgt(U_OrdWgt);
           }
           public virtual string doValidate_OrdWgt(object oValue) {
               return base.doValidation(_OrdWgt, oValue);
           }

		/**
		 * Decription: Origin
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Origin
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Origin = "U_Origin";
		public string U_Origin { 
			get {
 				return getValueAsString(_Origin); 
			}
			set { setValue(_Origin, value); }
		}
           public string doValidate_Origin() {
               return doValidate_Origin(U_Origin);
           }
           public virtual string doValidate_Origin(object oValue) {
               return base.doValidation(_Origin, oValue);
           }

		/**
		 * Decription: Order Type Comments
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_OTComments
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _OTComments = "U_OTComments";
		public string U_OTComments { 
			get {
 				return getValueAsString(_OTComments); 
			}
			set { setValue(_OTComments, value); }
		}
           public string doValidate_OTComments() {
               return doValidate_OTComments(U_OTComments);
           }
           public virtual string doValidate_OTComments(object oValue) {
               return base.doValidation(_OTComments, oValue);
           }

		/**
		 * Decription: Pasty/ Muddy/ pulpy
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PaMuPu
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PaMuPu = "U_PaMuPu";
		public string U_PaMuPu { 
			get {
 				return getValueAsString(_PaMuPu); 
			}
			set { setValue(_PaMuPu, value); }
		}
           public string doValidate_PaMuPu() {
               return doValidate_PaMuPu(U_PaMuPu);
           }
           public virtual string doValidate_PaMuPu(object oValue) {
               return base.doValidation(_PaMuPu, oValue);
           }

		/**
		 * Decription: Payment Receipt
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PARCPT
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PARCPT = "U_PARCPT";
		public string U_PARCPT { 
			get {
 				return getValueAsString(_PARCPT); 
			}
			set { setValue(_PARCPT, value); }
		}
           public string doValidate_PARCPT() {
               return doValidate_PARCPT(U_PARCPT);
           }
           public virtual string doValidate_PARCPT(object oValue) {
               return base.doValidation(_PARCPT, oValue);
           }

		/**
		 * Decription: Purchase Overriding Qty
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PAUOMQt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _PAUOMQt = "U_PAUOMQt";
		public double U_PAUOMQt { 
			get {
 				return getValueAsDouble(_PAUOMQt); 
			}
			set { setValue(_PAUOMQt, value); }
		}
           public string doValidate_PAUOMQt() {
               return doValidate_PAUOMQt(U_PAUOMQt);
           }
           public virtual string doValidate_PAUOMQt(object oValue) {
               return base.doValidation(_PAUOMQt, oValue);
           }

		/**
		 * Decription: Payment Method
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PayMeth
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PayMeth = "U_PayMeth";
		public string U_PayMeth { 
			get {
 				return getValueAsString(_PayMeth); 
			}
			set { setValue(_PayMeth, value); }
		}
           public string doValidate_PayMeth() {
               return doValidate_PayMeth(U_PayMeth);
           }
           public virtual string doValidate_PayMeth(object oValue) {
               return base.doValidation(_PayMeth, oValue);
           }

		/**
		 * Decription: Payment Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PayStat
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PayStat = "U_PayStat";
		public string U_PayStat { 
			get {
 				return getValueAsString(_PayStat); 
			}
			set { setValue(_PayStat, value); }
		}
           public string doValidate_PayStat() {
               return doValidate_PayStat(U_PayStat);
           }
           public virtual string doValidate_PayStat(object oValue) {
               return base.doValidation(_PayStat, oValue);
           }

		/**
		 * Decription: Payment Term
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PayTrm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PayTrm = "U_PayTrm";
		public string U_PayTrm { 
			get {
 				return getValueAsString(_PayTrm); 
			}
			set { setValue(_PayTrm, value); }
		}
           public string doValidate_PayTrm() {
               return doValidate_PayTrm(U_PayTrm);
           }
           public virtual string doValidate_PayTrm(object oValue) {
               return base.doValidation(_PayTrm, oValue);
           }

		/**
		 * Decription: Producer Charge
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PCharge
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _PCharge = "U_PCharge";
		public double U_PCharge { 
			get {
 				return getValueAsDouble(_PCharge); 
			}
			set { setValue(_PCharge, value); }
		}
           public string doValidate_PCharge() {
               return doValidate_PCharge(U_PCharge);
           }
           public virtual string doValidate_PCharge(object oValue) {
               return base.doValidation(_PCharge, oValue);
           }

		/**
		 * Decription: Producer Cost
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PCost
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _PCost = "U_PCost";
		public double U_PCost { 
			get {
 				return getValueAsDouble(_PCost); 
			}
			set { setValue(_PCost, value); }
		}
           public string doValidate_PCost() {
               return doValidate_PCost(U_PCost);
           }
           public virtual string doValidate_PCost(object oValue) {
               return base.doValidation(_PCost, oValue);
           }

		/**
		 * Decription: Producer Coharge Total
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PCTotal
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _PCTotal = "U_PCTotal";
		public double U_PCTotal { 
			get {
 				return getValueAsDouble(_PCTotal); 
			}
			set { setValue(_PCTotal, value); }
		}
           public string doValidate_PCTotal() {
               return doValidate_PCTotal(U_PCTotal);
           }
           public virtual string doValidate_PCTotal(object oValue) {
               return base.doValidation(_PCTotal, oValue);
           }

		/**
		 * Decription: Parent Row
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PLineID
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PLineID = "U_PLineID";
		public string U_PLineID { 
			get {
 				return getValueAsString(_PLineID); 
			}
			set { setValue(_PLineID, value); }
		}
           public string doValidate_PLineID() {
               return doValidate_PLineID(U_PLineID);
           }
           public virtual string doValidate_PLineID(object oValue) {
               return base.doValidation(_PLineID, oValue);
           }

		/**
		 * Decription: CIPSIP Link
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PrcLink
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PrcLink = "U_PrcLink";
		public string U_PrcLink { 
			get {
 				return getValueAsString(_PrcLink); 
			}
			set { setValue(_PrcLink, value); }
		}
           public string doValidate_PrcLink() {
               return doValidate_PrcLink(U_PrcLink);
           }
           public virtual string doValidate_PrcLink(object oValue) {
               return base.doValidation(_PrcLink, oValue);
           }

		/**
		 * Decription: Producer Read Weight
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PRdWgt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Measurement
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _PRdWgt = "U_PRdWgt";
		public double U_PRdWgt { 
			get {
 				return getValueAsDouble(_PRdWgt); 
			}
			set { setValue(_PRdWgt, value); }
		}
           public string doValidate_PRdWgt() {
               return doValidate_PRdWgt(U_PRdWgt);
           }
           public virtual string doValidate_PRdWgt(object oValue) {
               return base.doValidation(_PRdWgt, oValue);
           }

		/**
		 * Decription: Price
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Price
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Price = "U_Price";
		public double U_Price { 
			get {
 				return getValueAsDouble(_Price); 
			}
			set { setValue(_Price, value); }
		}
           public string doValidate_Price() {
               return doValidate_Price(U_Price);
           }
           public virtual string doValidate_Price(object oValue) {
               return base.doValidation(_Price, oValue);
           }

		/**
		 * Decription: Producer Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ProCd
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ProCd = "U_ProCd";
		public string U_ProCd { 
			get {
 				return getValueAsString(_ProCd); 
			}
			set { setValue(_ProCd, value); }
		}
           public string doValidate_ProCd() {
               return doValidate_ProCd(U_ProCd);
           }
           public virtual string doValidate_ProCd(object oValue) {
               return base.doValidation(_ProCd, oValue);
           }

		/**
		 * Decription: Producer Goods Receipt PO
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ProGRPO
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ProGRPO = "U_ProGRPO";
		public string U_ProGRPO { 
			get {
 				return getValueAsString(_ProGRPO); 
			}
			set { setValue(_ProGRPO, value); }
		}
           public string doValidate_ProGRPO() {
               return doValidate_ProGRPO(U_ProGRPO);
           }
           public virtual string doValidate_ProGRPO(object oValue) {
               return base.doValidation(_ProGRPO, oValue);
           }

		/**
		 * Decription: Producer Invoice
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PROINV
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PROINV = "U_PROINV";
		public string U_PROINV { 
			get {
 				return getValueAsString(_PROINV); 
			}
			set { setValue(_PROINV, value); }
		}
           public string doValidate_PROINV() {
               return doValidate_PROINV(U_PROINV);
           }
           public virtual string doValidate_PROINV(object oValue) {
               return base.doValidation(_PROINV, oValue);
           }

		/**
		 * Decription: Producer Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ProNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ProNm = "U_ProNm";
		public string U_ProNm { 
			get {
 				return getValueAsString(_ProNm); 
			}
			set { setValue(_ProNm, value); }
		}
           public string doValidate_ProNm() {
               return doValidate_ProNm(U_ProNm);
           }
           public virtual string doValidate_ProNm(object oValue) {
               return base.doValidation(_ProNm, oValue);
           }

		/**
		 * Decription: Producer OUT Payment
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ProPay
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ProPay = "U_ProPay";
		public string U_ProPay { 
			get {
 				return getValueAsString(_ProPay); 
			}
			set { setValue(_ProPay, value); }
		}
           public string doValidate_ProPay() {
               return doValidate_ProPay(U_ProPay);
           }
           public virtual string doValidate_ProPay(object oValue) {
               return base.doValidation(_ProPay, oValue);
           }

		/**
		 * Decription: Producer PO
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ProPO
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ProPO = "U_ProPO";
		public string U_ProPO { 
			get {
 				return getValueAsString(_ProPO); 
			}
			set { setValue(_ProPO, value); }
		}
           public string doValidate_ProPO() {
               return doValidate_ProPO(U_ProPO);
           }
           public virtual string doValidate_ProPO(object oValue) {
               return base.doValidation(_ProPO, oValue);
           }

		/**
		 * Decription: Producer Ref. No.
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ProRef
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ProRef = "U_ProRef";
		public string U_ProRef { 
			get {
 				return getValueAsString(_ProRef); 
			}
			set { setValue(_ProRef, value); }
		}
           public string doValidate_ProRef() {
               return doValidate_ProRef(U_ProRef);
           }
           public virtual string doValidate_ProRef(object oValue) {
               return base.doValidation(_ProRef, oValue);
           }

		/**
		 * Decription: Producer Unit Of Measure
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ProUOM
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ProUOM = "U_ProUOM";
		public string U_ProUOM { 
			get {
 				return getValueAsString(_ProUOM); 
			}
			set { setValue(_ProUOM, value); }
		}
           public string doValidate_ProUOM() {
               return doValidate_ProUOM(U_ProUOM);
           }
           public virtual string doValidate_ProUOM(object oValue) {
               return base.doValidation(_ProUOM, oValue);
           }

		/**
		 * Decription: Producer Weight
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ProWgt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Measurement
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _ProWgt = "U_ProWgt";
		public double U_ProWgt { 
			get {
 				return getValueAsDouble(_ProWgt); 
			}
			set { setValue(_ProWgt, value); }
		}
           public string doValidate_ProWgt() {
               return doValidate_ProWgt(U_ProWgt);
           }
           public virtual string doValidate_ProWgt(object oValue) {
               return base.doValidation(_ProWgt, oValue);
           }

		/**
		 * Decription: Purchase Order Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PStat
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PStat = "U_PStat";
		public string U_PStat { 
			get {
 				return getValueAsString(_PStat); 
			}
			set { setValue(_PStat, value); }
		}
           public string doValidate_PStat() {
               return doValidate_PStat(U_PStat);
           }
           public virtual string doValidate_PStat(object oValue) {
               return base.doValidation(_PStat, oValue);
           }

		/**
		 * Decription: Purchase Unit Of Measure
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PUOM
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PUOM = "U_PUOM";
		public string U_PUOM { 
			get {
 				return getValueAsString(_PUOM); 
			}
			set { setValue(_PUOM, value); }
		}
           public string doValidate_PUOM() {
               return doValidate_PUOM(U_PUOM);
           }
           public virtual string doValidate_PUOM(object oValue) {
               return base.doValidation(_PUOM, oValue);
           }

		/**
		 * Decription: Purchase Cost Currency
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PurcCoCc
		 * Size: 3
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _PurcCoCc = "U_PurcCoCc";
		public string U_PurcCoCc { 
			get {
 				return getValueAsString(_PurcCoCc); 
			}
			set { setValue(_PurcCoCc, value); }
		}
           public string doValidate_PurcCoCc() {
               return doValidate_PurcCoCc(U_PurcCoCc);
           }
           public virtual string doValidate_PurcCoCc(object oValue) {
               return base.doValidation(_PurcCoCc, oValue);
           }

		/**
		 * Decription: Order Quantity
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Quantity
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Quantity = "U_Quantity";
		public int U_Quantity { 
			get {
 				return getValueAsInt(_Quantity); 
			}
			set { setValue(_Quantity, value); }
		}
           public string doValidate_Quantity() {
               return doValidate_Quantity(U_Quantity);
           }
           public virtual string doValidate_Quantity(object oValue) {
               return base.doValidation(_Quantity, oValue);
           }

		/**
		 * Decription: Required Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RDate
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _RDate = "U_RDate";
		public DateTime U_RDate { 
			get {
 				return getValueAsDateTime(_RDate); 
			}
			set { setValue(_RDate, value); }
		}
		public void U_RDate_AsString(string value){
			setValue("U_RDate", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_RDate_AsString(){
			DateTime dVal = getValueAsDateTime(_RDate);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_RDate() {
               return doValidate_RDate(U_RDate);
           }
           public virtual string doValidate_RDate(object oValue) {
               return base.doValidation(_RDate, oValue);
           }

		/**
		 * Decription: Read Weight
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RdWgt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Measurement
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _RdWgt = "U_RdWgt";
		public double U_RdWgt { 
			get {
 				return getValueAsDouble(_RdWgt); 
			}
			set { setValue(_RdWgt, value); }
		}
           public string doValidate_RdWgt() {
               return doValidate_RdWgt(U_RdWgt);
           }
           public virtual string doValidate_RdWgt(object oValue) {
               return base.doValidation(_RdWgt, oValue);
           }

		/**
		 * Decription: Rebate Item
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Rebate
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Rebate = "U_Rebate";
		public string U_Rebate { 
			get {
 				return getValueAsString(_Rebate); 
			}
			set { setValue(_Rebate, value); }
		}
           public string doValidate_Rebate() {
               return doValidate_Rebate(U_Rebate);
           }
           public virtual string doValidate_Rebate(object oValue) {
               return base.doValidation(_Rebate, oValue);
           }

		/**
		 * Decription: Reminder Sent Count
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RemCnt
		 * Size: 6
		 * Type: db_Numeric
		 * CType: short
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _RemCnt = "U_RemCnt";
		public short U_RemCnt { 
			get {
 				return getValueAsShort(_RemCnt); 
			}
			set { setValue(_RemCnt, value); }
		}
           public string doValidate_RemCnt() {
               return doValidate_RemCnt(U_RemCnt);
           }
           public virtual string doValidate_RemCnt(object oValue) {
               return base.doValidation(_RemCnt, oValue);
           }

		/**
		 * Decription: Order Job Reminder Text
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RemNot
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RemNot = "U_RemNot";
		public string U_RemNot { 
			get {
 				return getValueAsString(_RemNot); 
			}
			set { setValue(_RemNot, value); }
		}
           public string doValidate_RemNot() {
               return doValidate_RemNot(U_RemNot);
           }
           public virtual string doValidate_RemNot(object oValue) {
               return base.doValidation(_RemNot, oValue);
           }

		/**
		 * Decription: Request to be archived
		 * DefaultValue: N
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ReqArch
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ReqArch = "U_ReqArch";
		public string U_ReqArch { 
			get {
 				return getValueAsString(_ReqArch); 
			}
			set { setValue(_ReqArch, value); }
		}
           public string doValidate_ReqArch() {
               return doValidate_ReqArch(U_ReqArch);
           }
           public virtual string doValidate_ReqArch(object oValue) {
               return base.doValidation(_ReqArch, oValue);
           }

		/**
		 * Decription: Route Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RouteCd
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RouteCd = "U_RouteCd";
		public string U_RouteCd { 
			get {
 				return getValueAsString(_RouteCd); 
			}
			set { setValue(_RouteCd, value); }
		}
           public string doValidate_RouteCd() {
               return doValidate_RouteCd(U_RouteCd);
           }
           public virtual string doValidate_RouteCd(object oValue) {
               return base.doValidation(_RouteCd, oValue);
           }

		/**
		 * Decription: Row Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RowSta
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _RowSta = "U_RowSta";
		public string U_RowSta { 
			get {
 				return getValueAsString(_RowSta); 
			}
			set { setValue(_RowSta, value); }
		}
           public string doValidate_RowSta() {
               return doValidate_RowSta(U_RowSta);
           }
           public virtual string doValidate_RowSta(object oValue) {
               return base.doValidation(_RowSta, oValue);
           }

		/**
		 * Decription: Required Time From
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RTime
		 * Size: 6
		 * Type: db_Date
		 * CType: string
		 * SubType: st_Time
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _RTime = "U_RTime";
		public string U_RTime { 
			get {
 				return getValueAsString(_RTime); 
			}
			set { setValue(_RTime, value); }
		}
           public string doValidate_RTime() {
               return doValidate_RTime(U_RTime);
           }
           public virtual string doValidate_RTime(object oValue) {
               return base.doValidation(_RTime, oValue);
           }

		/**
		 * Decription: Required Time To
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RTimeT
		 * Size: 6
		 * Type: db_Date
		 * CType: string
		 * SubType: st_Time
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _RTimeT = "U_RTimeT";
		public string U_RTimeT { 
			get {
 				return getValueAsString(_RTimeT); 
			}
			set { setValue(_RTimeT, value); }
		}
           public string doValidate_RTimeT() {
               return doValidate_RTimeT(U_RTimeT);
           }
           public virtual string doValidate_RTimeT(object oValue) {
               return base.doValidation(_RTimeT, oValue);
           }

		/**
		 * Decription: Site Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SAddress
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SAddress = "U_SAddress";
		public string U_SAddress { 
			get {
 				return getValueAsString(_SAddress); 
			}
			set { setValue(_SAddress, value); }
		}
           public string doValidate_SAddress() {
               return doValidate_SAddress(U_SAddress);
           }
           public virtual string doValidate_SAddress(object oValue) {
               return base.doValidation(_SAddress, oValue);
           }

		/**
		 * Decription: Disposal Address LineNum
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SAddrsLN
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SAddrsLN = "U_SAddrsLN";
		public string U_SAddrsLN { 
			get {
 				return getValueAsString(_SAddrsLN); 
			}
			set { setValue(_SAddrsLN, value); }
		}
           public string doValidate_SAddrsLN() {
               return doValidate_SAddrsLN(U_SAddrsLN);
           }
           public virtual string doValidate_SAddrsLN(object oValue) {
               return base.doValidation(_SAddrsLN, oValue);
           }

		/**
		 * Decription: Sales Invoice
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SAINV
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SAINV = "U_SAINV";
		public string U_SAINV { 
			get {
 				return getValueAsString(_SAINV); 
			}
			set { setValue(_SAINV, value); }
		}
           public string doValidate_SAINV() {
               return doValidate_SAINV(U_SAINV);
           }
           public virtual string doValidate_SAINV(object oValue) {
               return base.doValidation(_SAINV, oValue);
           }

		/**
		 * Decription: Sample
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Sample
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Sample = "U_Sample";
		public string U_Sample { 
			get {
 				return getValueAsString(_Sample); 
			}
			set { setValue(_Sample, value); }
		}
           public string doValidate_Sample() {
               return doValidate_Sample(U_Sample);
           }
           public virtual string doValidate_Sample(object oValue) {
               return base.doValidation(_Sample, oValue);
           }

		/**
		 * Decription: Sample Ref.
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SampleRef
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SampleRef = "U_SampleRef";
		public string U_SampleRef { 
			get {
 				return getValueAsString(_SampleRef); 
			}
			set { setValue(_SampleRef, value); }
		}
           public string doValidate_SampleRef() {
               return doValidate_SampleRef(U_SampleRef);
           }
           public virtual string doValidate_SampleRef(object oValue) {
               return base.doValidation(_SampleRef, oValue);
           }

		/**
		 * Decription: Sample Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SampStatus
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SampStatus = "U_SampStatus";
		public string U_SampStatus { 
			get {
 				return getValueAsString(_SampStatus); 
			}
			set { setValue(_SampStatus, value); }
		}
           public string doValidate_SampStatus() {
               return doValidate_SampStatus(U_SampStatus);
           }
           public virtual string doValidate_SampStatus(object oValue) {
               return base.doValidation(_SampStatus, oValue);
           }

		/**
		 * Decription: Sales Order
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SAORD
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SAORD = "U_SAORD";
		public string U_SAORD { 
			get {
 				return getValueAsString(_SAORD); 
			}
			set { setValue(_SAORD, value); }
		}
           public string doValidate_SAORD() {
               return doValidate_SAORD(U_SAORD);
           }
           public virtual string doValidate_SAORD(object oValue) {
               return base.doValidation(_SAORD, oValue);
           }

		/**
		 * Decription: Scheduled Job
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Sched
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Sched = "U_Sched";
		public string U_Sched { 
			get {
 				return getValueAsString(_Sched); 
			}
			set { setValue(_Sched, value); }
		}
           public string doValidate_Sched() {
               return doValidate_Sched(U_Sched);
           }
           public virtual string doValidate_Sched(object oValue) {
               return base.doValidation(_Sched, oValue);
           }

		/**
		 * Decription: Congestion Supplier Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SCngNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SCngNm = "U_SCngNm";
		public string U_SCngNm { 
			get {
 				return getValueAsString(_SCngNm); 
			}
			set { setValue(_SCngNm, value); }
		}
           public string doValidate_SCngNm() {
               return doValidate_SCngNm(U_SCngNm);
           }
           public virtual string doValidate_SCngNm(object oValue) {
               return base.doValidation(_SCngNm, oValue);
           }

		/**
		 * Decription: Seal Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SealNr
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SealNr = "U_SealNr";
		public string U_SealNr { 
			get {
 				return getValueAsString(_SealNr); 
			}
			set { setValue(_SealNr, value); }
		}
           public string doValidate_SealNr() {
               return doValidate_SealNr(U_SealNr);
           }
           public virtual string doValidate_SealNr(object oValue) {
               return base.doValidation(_SealNr, oValue);
           }

		/**
		 * Decription: Semi Solid
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SemSolid
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SemSolid = "U_SemSolid";
		public string U_SemSolid { 
			get {
 				return getValueAsString(_SemSolid); 
			}
			set { setValue(_SemSolid, value); }
		}
           public string doValidate_SemSolid() {
               return doValidate_SemSolid(U_SemSolid);
           }
           public virtual string doValidate_SemSolid(object oValue) {
               return base.doValidation(_SemSolid, oValue);
           }

		/**
		 * Decription: Flash Serial 1
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Ser1
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Ser1 = "U_Ser1";
		public string U_Ser1 { 
			get {
 				return getValueAsString(_Ser1); 
			}
			set { setValue(_Ser1, value); }
		}
           public string doValidate_Ser1() {
               return doValidate_Ser1(U_Ser1);
           }
           public virtual string doValidate_Ser1(object oValue) {
               return base.doValidation(_Ser1, oValue);
           }

		/**
		 * Decription: Flash Serial 2
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Ser2
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Ser2 = "U_Ser2";
		public string U_Ser2 { 
			get {
 				return getValueAsString(_Ser2); 
			}
			set { setValue(_Ser2, value); }
		}
           public string doValidate_Ser2() {
               return doValidate_Ser2(U_Ser2);
           }
           public virtual string doValidate_Ser2(object oValue) {
               return base.doValidation(_Ser2, oValue);
           }

		/**
		 * Decription: Serial Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Serial
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Serial = "U_Serial";
		public string U_Serial { 
			get {
 				return getValueAsString(_Serial); 
			}
			set { setValue(_Serial, value); }
		}
           public string doValidate_Serial() {
               return doValidate_Serial(U_Serial);
           }
           public virtual string doValidate_Serial(object oValue) {
               return base.doValidation(_Serial, oValue);
           }

		/**
		 * Decription: Serial Number Collection
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SerialC
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SerialC = "U_SerialC";
		public string U_SerialC { 
			get {
 				return getValueAsString(_SerialC); 
			}
			set { setValue(_SerialC, value); }
		}
           public string doValidate_SerialC() {
               return doValidate_SerialC(U_SerialC);
           }
           public virtual string doValidate_SerialC(object oValue) {
               return base.doValidation(_SerialC, oValue);
           }

		/**
		 * Decription: Site Reference Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SiteRef
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SiteRef = "U_SiteRef";
		public string U_SiteRef { 
			get {
 				return getValueAsString(_SiteRef); 
			}
			set { setValue(_SiteRef, value); }
		}
           public string doValidate_SiteRef() {
               return doValidate_SiteRef(U_SiteRef);
           }
           public virtual string doValidate_SiteRef(object oValue) {
               return base.doValidation(_SiteRef, oValue);
           }

		/**
		 * Decription: Skip License Charge
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SLicCh
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _SLicCh = "U_SLicCh";
		public double U_SLicCh { 
			get {
 				return getValueAsDouble(_SLicCh); 
			}
			set { setValue(_SLicCh, value); }
		}
           public string doValidate_SLicCh() {
               return doValidate_SLicCh(U_SLicCh);
           }
           public virtual string doValidate_SLicCh(object oValue) {
               return base.doValidation(_SLicCh, oValue);
           }

		/**
		 * Decription: Skip Licence Quantity
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SLicCQt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _SLicCQt = "U_SLicCQt";
		public double U_SLicCQt { 
			get {
 				return getValueAsDouble(_SLicCQt); 
			}
			set { setValue(_SLicCQt, value); }
		}
           public string doValidate_SLicCQt() {
               return doValidate_SLicCQt(U_SLicCQt);
           }
           public virtual string doValidate_SLicCQt(object oValue) {
               return base.doValidation(_SLicCQt, oValue);
           }

		/**
		 * Decription: Skip Licence Cost
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SLicCst
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _SLicCst = "U_SLicCst";
		public double U_SLicCst { 
			get {
 				return getValueAsDouble(_SLicCst); 
			}
			set { setValue(_SLicCst, value); }
		}
           public string doValidate_SLicCst() {
               return doValidate_SLicCst(U_SLicCst);
           }
           public virtual string doValidate_SLicCst(object oValue) {
               return base.doValidation(_SLicCst, oValue);
           }

		/**
		 * Decription: Skip Licence Cost Total
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SLicCTo
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _SLicCTo = "U_SLicCTo";
		public double U_SLicCTo { 
			get {
 				return getValueAsDouble(_SLicCTo); 
			}
			set { setValue(_SLicCTo, value); }
		}
           public string doValidate_SLicCTo() {
               return doValidate_SLicCTo(U_SLicCTo);
           }
           public virtual string doValidate_SLicCTo(object oValue) {
               return base.doValidation(_SLicCTo, oValue);
           }

		/**
		 * Decription: Skip License Expiry Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SLicExp
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _SLicExp = "U_SLicExp";
		public DateTime U_SLicExp { 
			get {
 				return getValueAsDateTime(_SLicExp); 
			}
			set { setValue(_SLicExp, value); }
		}
		public void U_SLicExp_AsString(string value){
			setValue("U_SLicExp", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_SLicExp_AsString(){
			DateTime dVal = getValueAsDateTime(_SLicExp);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_SLicExp() {
               return doValidate_SLicExp(U_SLicExp);
           }
           public virtual string doValidate_SLicExp(object oValue) {
               return base.doValidation(_SLicExp, oValue);
           }

		/**
		 * Decription: Skip License Supplier Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SLicNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SLicNm = "U_SLicNm";
		public string U_SLicNm { 
			get {
 				return getValueAsString(_SLicNm); 
			}
			set { setValue(_SLicNm, value); }
		}
           public string doValidate_SLicNm() {
               return doValidate_SLicNm(U_SLicNm);
           }
           public virtual string doValidate_SLicNm(object oValue) {
               return base.doValidation(_SLicNm, oValue);
           }

		/**
		 * Decription: Skip License No
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SLicNr
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SLicNr = "U_SLicNr";
		public string U_SLicNr { 
			get {
 				return getValueAsString(_SLicNr); 
			}
			set { setValue(_SLicNr, value); }
		}
           public string doValidate_SLicNr() {
               return doValidate_SLicNr(U_SLicNr);
           }
           public virtual string doValidate_SLicNr(object oValue) {
               return base.doValidation(_SLicNr, oValue);
           }

		/**
		 * Decription: Skip License Supplier
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SLicSp
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SLicSp = "U_SLicSp";
		public string U_SLicSp { 
			get {
 				return getValueAsString(_SLicSp); 
			}
			set { setValue(_SLicSp, value); }
		}
           public string doValidate_SLicSp() {
               return doValidate_SLicSp(U_SLicSp);
           }
           public virtual string doValidate_SLicSp(object oValue) {
               return base.doValidation(_SLicSp, oValue);
           }

		/**
		 * Decription: Skip licence PO
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SLPO
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SLPO = "U_SLPO";
		public string U_SLPO { 
			get {
 				return getValueAsString(_SLPO); 
			}
			set { setValue(_SLPO, value); }
		}
           public string doValidate_SLPO() {
               return doValidate_SLPO(U_SLPO);
           }
           public virtual string doValidate_SLPO(object oValue) {
               return base.doValidation(_SLPO, oValue);
           }

		/**
		 * Decription: SO Delivery Note
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SODlvNot
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SODlvNot = "U_SODlvNot";
		public string U_SODlvNot { 
			get {
 				return getValueAsString(_SODlvNot); 
			}
			set { setValue(_SODlvNot, value); }
		}
           public string doValidate_SODlvNot() {
               return doValidate_SODlvNot(U_SODlvNot);
           }
           public virtual string doValidate_SODlvNot(object oValue) {
               return base.doValidation(_SODlvNot, oValue);
           }

		/**
		 * Decription: Sort Order
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Sort
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Sort = "U_Sort";
		public int U_Sort { 
			get {
 				return getValueAsInt(_Sort); 
			}
			set { setValue(_Sort, value); }
		}
           public string doValidate_Sort() {
               return doValidate_Sort(U_Sort);
           }
           public virtual string doValidate_Sort(object oValue) {
               return base.doValidation(_Sort, oValue);
           }

		/**
		 * Decription: Order Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Status
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Status = "U_Status";
		public string U_Status { 
			get {
 				return getValueAsString(_Status); 
			}
			set { setValue(_Status, value); }
		}
           public string doValidate_Status() {
               return doValidate_Status(U_Status);
           }
           public virtual string doValidate_Status(object oValue) {
               return base.doValidation(_Status, oValue);
           }

		/**
		 * Decription: Supplier Ref No
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SupRef
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _SupRef = "U_SupRef";
		public string U_SupRef { 
			get {
 				return getValueAsString(_SupRef); 
			}
			set { setValue(_SupRef, value); }
		}
           public string doValidate_SupRef() {
               return doValidate_SupRef(U_SupRef);
           }
           public virtual string doValidate_SupRef(object oValue) {
               return base.doValidation(_SupRef, oValue);
           }

		/**
		 * Decription: Additional Charge
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TAddChrg
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _TAddChrg = "U_TAddChrg";
		public double U_TAddChrg { 
			get {
 				return getValueAsDouble(_TAddChrg); 
			}
			set { setValue(_TAddChrg, value); }
		}
           public string doValidate_TAddChrg() {
               return doValidate_TAddChrg(U_TAddChrg);
           }
           public virtual string doValidate_TAddChrg(object oValue) {
               return base.doValidation(_TAddChrg, oValue);
           }

		/**
		 * Decription: Additional Charge VAT
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TAddChVat
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _TAddChVat = "U_TAddChVat";
		public double U_TAddChVat { 
			get {
 				return getValueAsDouble(_TAddChVat); 
			}
			set { setValue(_TAddChVat, value); }
		}
           public string doValidate_TAddChVat() {
               return doValidate_TAddChVat(U_TAddChVat);
           }
           public virtual string doValidate_TAddChVat(object oValue) {
               return base.doValidation(_TAddChVat, oValue);
           }

		/**
		 * Decription: Additional Cost
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TAddCost
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _TAddCost = "U_TAddCost";
		public double U_TAddCost { 
			get {
 				return getValueAsDouble(_TAddCost); 
			}
			set { setValue(_TAddCost, value); }
		}
           public string doValidate_TAddCost() {
               return doValidate_TAddCost(U_TAddCost);
           }
           public virtual string doValidate_TAddCost(object oValue) {
               return base.doValidation(_TAddCost, oValue);
           }

		/**
		 * Decription: Additional Cost VAT
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TAddCsVat
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _TAddCsVat = "U_TAddCsVat";
		public double U_TAddCsVat { 
			get {
 				return getValueAsDouble(_TAddCsVat); 
			}
			set { setValue(_TAddCsVat, value); }
		}
           public string doValidate_TAddCsVat() {
               return doValidate_TAddCsVat(U_TAddCsVat);
           }
           public virtual string doValidate_TAddCsVat(object oValue) {
               return base.doValidation(_TAddCsVat, oValue);
           }

		/**
		 * Decription: Disposal Cost Overriding Qty
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TAUOMQt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _TAUOMQt = "U_TAUOMQt";
		public double U_TAUOMQt { 
			get {
 				return getValueAsDouble(_TAUOMQt); 
			}
			set { setValue(_TAUOMQt, value); }
		}
           public string doValidate_TAUOMQt() {
               return doValidate_TAUOMQt(U_TAUOMQt);
           }
           public virtual string doValidate_TAUOMQt(object oValue) {
               return base.doValidation(_TAUOMQt, oValue);
           }

		/**
		 * Decription: Tax Amount
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TaxAmt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _TaxAmt = "U_TaxAmt";
		public double U_TaxAmt { 
			get {
 				return getValueAsDouble(_TaxAmt); 
			}
			set { setValue(_TaxAmt, value); }
		}
           public string doValidate_TaxAmt() {
               return doValidate_TaxAmt(U_TaxAmt);
           }
           public virtual string doValidate_TaxAmt(object oValue) {
               return base.doValidation(_TaxAmt, oValue);
           }

		/**
		 * Decription: Customer Credit Note
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TCCN
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TCCN = "U_TCCN";
		public string U_TCCN { 
			get {
 				return getValueAsString(_TCCN); 
			}
			set { setValue(_TCCN, value); }
		}
           public string doValidate_TCCN() {
               return doValidate_TCCN(U_TCCN);
           }
           public virtual string doValidate_TCCN(object oValue) {
               return base.doValidation(_TCCN, oValue);
           }

		/**
		 * Decription: Tipping Charge
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TCharge
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _TCharge = "U_TCharge";
		public double U_TCharge { 
			get {
 				return getValueAsDouble(_TCharge); 
			}
			set { setValue(_TCharge, value); }
		}
           public string doValidate_TCharge() {
               return doValidate_TCharge(U_TCharge);
           }
           public virtual string doValidate_TCharge(object oValue) {
               return base.doValidation(_TCharge, oValue);
           }

		/**
		 * Decription: Tipping Sales Vat Group
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TChrgVtGrp
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TChrgVtGrp = "U_TChrgVtGrp";
		public string U_TChrgVtGrp { 
			get {
 				return getValueAsString(_TChrgVtGrp); 
			}
			set { setValue(_TChrgVtGrp, value); }
		}
           public string doValidate_TChrgVtGrp() {
               return doValidate_TChrgVtGrp(U_TChrgVtGrp);
           }
           public virtual string doValidate_TChrgVtGrp(object oValue) {
               return base.doValidation(_TChrgVtGrp, oValue);
           }

		/**
		 * Decription: Tipping Sales Vat Rate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TChrgVtRt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Percentage
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _TChrgVtRt = "U_TChrgVtRt";
		public double U_TChrgVtRt { 
			get {
 				return getValueAsDouble(_TChrgVtRt); 
			}
			set { setValue(_TChrgVtRt, value); }
		}
           public string doValidate_TChrgVtRt() {
               return doValidate_TChrgVtRt(U_TChrgVtRt);
           }
           public virtual string doValidate_TChrgVtRt(object oValue) {
               return base.doValidation(_TChrgVtRt, oValue);
           }

		/**
		 * Decription: Tipping Buying Vat Group
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TCostVtGrp
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TCostVtGrp = "U_TCostVtGrp";
		public string U_TCostVtGrp { 
			get {
 				return getValueAsString(_TCostVtGrp); 
			}
			set { setValue(_TCostVtGrp, value); }
		}
           public string doValidate_TCostVtGrp() {
               return doValidate_TCostVtGrp(U_TCostVtGrp);
           }
           public virtual string doValidate_TCostVtGrp(object oValue) {
               return base.doValidation(_TCostVtGrp, oValue);
           }

		/**
		 * Decription: Tipping Buying Vat Rate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TCostVtRt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Percentage
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _TCostVtRt = "U_TCostVtRt";
		public double U_TCostVtRt { 
			get {
 				return getValueAsDouble(_TCostVtRt); 
			}
			set { setValue(_TCostVtRt, value); }
		}
           public string doValidate_TCostVtRt() {
               return doValidate_TCostVtRt(U_TCostVtRt);
           }
           public virtual string doValidate_TCostVtRt(object oValue) {
               return base.doValidation(_TCostVtRt, oValue);
           }

		/**
		 * Decription: Tipping Charge Total
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TCTotal
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _TCTotal = "U_TCTotal";
		public double U_TCTotal { 
			get {
 				return getValueAsDouble(_TCTotal); 
			}
			set { setValue(_TCTotal, value); }
		}
           public string doValidate_TCTotal() {
               return doValidate_TCTotal(U_TCTotal);
           }
           public virtual string doValidate_TCTotal(object oValue) {
               return base.doValidation(_TCTotal, oValue);
           }

		/**
		 * Decription: TFS Movement
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TFSMov
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TFSMov = "U_TFSMov";
		public string U_TFSMov { 
			get {
 				return getValueAsString(_TFSMov); 
			}
			set { setValue(_TFSMov, value); }
		}
           public string doValidate_TFSMov() {
               return doValidate_TFSMov(U_TFSMov);
           }
           public virtual string doValidate_TFSMov(object oValue) {
               return base.doValidation(_TFSMov, oValue);
           }

		/**
		 * Decription: TFS Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TFSNum
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TFSNum = "U_TFSNum";
		public string U_TFSNum { 
			get {
 				return getValueAsString(_TFSNum); 
			}
			set { setValue(_TFSNum, value); }
		}
           public string doValidate_TFSNum() {
               return doValidate_TFSNum(U_TFSNum);
           }
           public virtual string doValidate_TFSNum(object oValue) {
               return base.doValidation(_TFSNum, oValue);
           }

		/**
		 * Decription: TFS Required
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TFSReq
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TFSReq = "U_TFSReq";
		public string U_TFSReq { 
			get {
 				return getValueAsString(_TFSReq); 
			}
			set { setValue(_TFSReq, value); }
		}
           public string doValidate_TFSReq() {
               return doValidate_TFSReq(U_TFSReq);
           }
           public virtual string doValidate_TFSReq(object oValue) {
               return base.doValidation(_TFSReq, oValue);
           }

		/**
		 * Decription: Disposal Site
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Tip
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Tip = "U_Tip";
		public string U_Tip { 
			get {
 				return getValueAsString(_Tip); 
			}
			set { setValue(_Tip, value); }
		}
           public string doValidate_Tip() {
               return doValidate_Tip(U_Tip);
           }
           public virtual string doValidate_Tip(object oValue) {
               return base.doValidation(_Tip, oValue);
           }

		/**
		 * Decription: Tipping Cost
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TipCost
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _TipCost = "U_TipCost";
		public double U_TipCost { 
			get {
 				return getValueAsDouble(_TipCost); 
			}
			set { setValue(_TipCost, value); }
		}
           public string doValidate_TipCost() {
               return doValidate_TipCost(U_TipCost);
           }
           public virtual string doValidate_TipCost(object oValue) {
               return base.doValidation(_TipCost, oValue);
           }

		/**
		 * Decription: Dispoal Site Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TipNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TipNm = "U_TipNm";
		public string U_TipNm { 
			get {
 				return getValueAsString(_TipNm); 
			}
			set { setValue(_TipNm, value); }
		}
           public string doValidate_TipNm() {
               return doValidate_TipNm(U_TipNm);
           }
           public virtual string doValidate_TipNm(object oValue) {
               return base.doValidation(_TipNm, oValue);
           }

		/**
		 * Decription: Tipping PO
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TIPPO
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TIPPO = "U_TIPPO";
		public string U_TIPPO { 
			get {
 				return getValueAsString(_TIPPO); 
			}
			set { setValue(_TIPPO, value); }
		}
           public string doValidate_TIPPO() {
               return doValidate_TIPPO(U_TIPPO);
           }
           public virtual string doValidate_TIPPO(object oValue) {
               return base.doValidation(_TIPPO, oValue);
           }

		/**
		 * Decription: Tipping Total Cost
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TipTot
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _TipTot = "U_TipTot";
		public double U_TipTot { 
			get {
 				return getValueAsDouble(_TipTot); 
			}
			set { setValue(_TipTot, value); }
		}
           public string doValidate_TipTot() {
               return doValidate_TipTot(U_TipTot);
           }
           public virtual string doValidate_TipTot(object oValue) {
               return base.doValidation(_TipTot, oValue);
           }

		/**
		 * Decription: Tipping Weight
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TipWgt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Measurement
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _TipWgt = "U_TipWgt";
		public double U_TipWgt { 
			get {
 				return getValueAsDouble(_TipWgt); 
			}
			set { setValue(_TipWgt, value); }
		}
           public string doValidate_TipWgt() {
               return doValidate_TipWgt(U_TipWgt);
           }
           public virtual string doValidate_TipWgt(object oValue) {
               return base.doValidation(_TipWgt, oValue);
           }

		/**
		 * Decription: Total
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Total
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Total = "U_Total";
		public double U_Total { 
			get {
 				return getValueAsDouble(_Total); 
			}
			set { setValue(_Total, value); }
		}
           public string doValidate_Total() {
               return doValidate_Total(U_Total);
           }
           public virtual string doValidate_Total(object oValue) {
               return base.doValidation(_Total, oValue);
           }

		/**
		 * Decription: Disposal Purchase Credit Note
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TPCN
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TPCN = "U_TPCN";
		public string U_TPCN { 
			get {
 				return getValueAsString(_TPCN); 
			}
			set { setValue(_TPCN, value); }
		}
           public string doValidate_TPCN() {
               return doValidate_TPCN(U_TPCN);
           }
           public virtual string doValidate_TPCN(object oValue) {
               return base.doValidation(_TPCN, oValue);
           }

		/**
		 * Decription: Tip Read Weight
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TRdWgt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Measurement
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _TRdWgt = "U_TRdWgt";
		public double U_TRdWgt { 
			get {
 				return getValueAsDouble(_TRdWgt); 
			}
			set { setValue(_TRdWgt, value); }
		}
           public string doValidate_TRdWgt() {
               return doValidate_TRdWgt(U_TRdWgt);
           }
           public virtual string doValidate_TRdWgt(object oValue) {
               return base.doValidation(_TRdWgt, oValue);
           }

		/**
		 * Decription: Trailer Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TRLNM
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TRLNM = "U_TRLNM";
		public string U_TRLNM { 
			get {
 				return getValueAsString(_TRLNM); 
			}
			set { setValue(_TRLNM, value); }
		}
           public string doValidate_TRLNM() {
               return doValidate_TRLNM(U_TRLNM);
           }
           public virtual string doValidate_TRLNM(object oValue) {
               return base.doValidation(_TRLNM, oValue);
           }

		/**
		 * Decription: Trailer Reg. No.
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TRLReg
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TRLReg = "U_TRLReg";
		public string U_TRLReg { 
			get {
 				return getValueAsString(_TRLReg); 
			}
			set { setValue(_TRLReg, value); }
		}
           public string doValidate_TRLReg() {
               return doValidate_TRLReg(U_TRLReg);
           }
           public virtual string doValidate_TRLReg(object oValue) {
               return base.doValidation(_TRLReg, oValue);
           }

		/**
		 * Decription: Transaction Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TrnCode
		 * Size: 40
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TrnCode = "U_TrnCode";
		public string U_TrnCode { 
			get {
 				return getValueAsString(_TrnCode); 
			}
			set { setValue(_TrnCode, value); }
		}
           public string doValidate_TrnCode() {
               return doValidate_TrnCode(U_TrnCode);
           }
           public virtual string doValidate_TrnCode(object oValue) {
               return base.doValidation(_TrnCode, oValue);
           }

		/**
		 * Decription: Type of pre-treatment
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TypPTreat
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TypPTreat = "U_TypPTreat";
		public string U_TypPTreat { 
			get {
 				return getValueAsString(_TypPTreat); 
			}
			set { setValue(_TypPTreat, value); }
		}
           public string doValidate_TypPTreat() {
               return doValidate_TypPTreat(U_TypPTreat);
           }
           public virtual string doValidate_TypPTreat(object oValue) {
               return base.doValidation(_TypPTreat, oValue);
           }

		/**
		 * Decription: Tip Zone
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TZone
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _TZone = "U_TZone";
		public string U_TZone { 
			get {
 				return getValueAsString(_TZone); 
			}
			set { setValue(_TZone, value); }
		}
           public string doValidate_TZone() {
               return doValidate_TZone(U_TZone);
           }
           public virtual string doValidate_TZone(object oValue) {
               return base.doValidation(_TZone, oValue);
           }

		/**
		 * Decription: Unit Of Measure
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UOM
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _UOM = "U_UOM";
		public string U_UOM { 
			get {
 				return getValueAsString(_UOM); 
			}
			set { setValue(_UOM, value); }
		}
           public string doValidate_UOM() {
               return doValidate_UOM(U_UOM);
           }
           public virtual string doValidate_UOM(object oValue) {
               return base.doValidation(_UOM, oValue);
           }

		/**
		 * Decription: Un-Scheduled Job
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_USched
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _USched = "U_USched";
		public string U_USched { 
			get {
 				return getValueAsString(_USched); 
			}
			set { setValue(_USched, value); }
		}
           public string doValidate_USched() {
               return doValidate_USched(U_USched);
           }
           public virtual string doValidate_USched(object oValue) {
               return base.doValidation(_USched, oValue);
           }

		/**
		 * Decription: Use BP Weight
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UseBPWgt
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _UseBPWgt = "U_UseBPWgt";
		public string U_UseBPWgt { 
			get {
 				return getValueAsString(_UseBPWgt); 
			}
			set { setValue(_UseBPWgt, value); }
		}
           public string doValidate_UseBPWgt() {
               return doValidate_UseBPWgt(U_UseBPWgt);
           }
           public virtual string doValidate_UseBPWgt(object oValue) {
               return base.doValidation(_UseBPWgt, oValue);
           }

		/**
		 * Decription: Created By
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_User
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _User = "U_User";
		public string U_User { 
			get {
 				return getValueAsString(_User); 
			}
			set { setValue(_User, value); }
		}
           public string doValidate_User() {
               return doValidate_User(U_User);
           }
           public virtual string doValidate_User(object oValue) {
               return base.doValidation(_User, oValue);
           }

		/**
		 * Decription: Which Weight to Use
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UseWgt
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _UseWgt = "U_UseWgt";
		public string U_UseWgt { 
			get {
 				return getValueAsString(_UseWgt); 
			}
			set { setValue(_UseWgt, value); }
		}
           public string doValidate_UseWgt() {
               return doValidate_UseWgt(U_UseWgt);
           }
           public virtual string doValidate_UseWgt(object oValue) {
               return base.doValidation(_UseWgt, oValue);
           }

		/**
		 * Decription: Value Deduction
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ValDed
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Measurement
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _ValDed = "U_ValDed";
		public double U_ValDed { 
			get {
 				return getValueAsDouble(_ValDed); 
			}
			set { setValue(_ValDed, value); }
		}
           public string doValidate_ValDed() {
               return doValidate_ValDed(U_ValDed);
           }
           public virtual string doValidate_ValDed(object oValue) {
               return base.doValidation(_ValDed, oValue);
           }

		/**
		 * Decription: Vehicle Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_VehTyp
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _VehTyp = "U_VehTyp";
		public string U_VehTyp { 
			get {
 				return getValueAsString(_VehTyp); 
			}
			set { setValue(_VehTyp, value); }
		}
           public string doValidate_VehTyp() {
               return doValidate_VehTyp(U_VehTyp);
           }
           public virtual string doValidate_VehTyp(object oValue) {
               return base.doValidation(_VehTyp, oValue);
           }

		/**
		 * Decription: Purchasing Vat Amount
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_VtCostAmt
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _VtCostAmt = "U_VtCostAmt";
		public double U_VtCostAmt { 
			get {
 				return getValueAsDouble(_VtCostAmt); 
			}
			set { setValue(_VtCostAmt, value); }
		}
           public string doValidate_VtCostAmt() {
               return doValidate_VtCostAmt(U_VtCostAmt);
           }
           public virtual string doValidate_VtCostAmt(object oValue) {
               return base.doValidation(_VtCostAmt, oValue);
           }

		/**
		 * Decription: Waste Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WasCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WasCd = "U_WasCd";
		public string U_WasCd { 
			get {
 				return getValueAsString(_WasCd); 
			}
			set { setValue(_WasCd, value); }
		}
           public string doValidate_WasCd() {
               return doValidate_WasCd(U_WasCd);
           }
           public virtual string doValidate_WasCd(object oValue) {
               return base.doValidation(_WasCd, oValue);
           }

		/**
		 * Decription: Waste Description
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WasDsc
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WasDsc = "U_WasDsc";
		public string U_WasDsc { 
			get {
 				return getValueAsString(_WasDsc); 
			}
			set { setValue(_WasDsc, value); }
		}
           public string doValidate_WasDsc() {
               return doValidate_WasDsc(U_WasDsc);
           }
           public virtual string doValidate_WasDsc(object oValue) {
               return base.doValidation(_WasDsc, oValue);
           }

		/**
		 * Decription: Waste Cd Foreign Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WasFDsc
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WasFDsc = "U_WasFDsc";
		public string U_WasFDsc { 
			get {
 				return getValueAsString(_WasFDsc); 
			}
			set { setValue(_WasFDsc, value); }
		}
           public string doValidate_WasFDsc() {
               return doValidate_WasFDsc(U_WasFDsc);
           }
           public virtual string doValidate_WasFDsc(object oValue) {
               return base.doValidation(_WasFDsc, oValue);
           }

		/**
		 * Decription: Waste Lic. Reg. No.
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WASLIC
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WASLIC = "U_WASLIC";
		public string U_WASLIC { 
			get {
 				return getValueAsString(_WASLIC); 
			}
			set { setValue(_WASLIC, value); }
		}
           public string doValidate_WASLIC() {
               return doValidate_WASLIC(U_WASLIC);
           }
           public virtual string doValidate_WASLIC(object oValue) {
               return base.doValidation(_WASLIC, oValue);
           }

		/**
		 * Decription: Waste Transfer Note Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WastTNN
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WastTNN = "U_WastTNN";
		public string U_WastTNN { 
			get {
 				return getValueAsString(_WastTNN); 
			}
			set { setValue(_WastTNN, value); }
		}
           public string doValidate_WastTNN() {
               return doValidate_WastTNN(U_WastTNN);
           }
           public virtual string doValidate_WastTNN(object oValue) {
               return base.doValidation(_WastTNN, oValue);
           }

		/**
		 * Decription: Weigh Date 1
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WDt1
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _WDt1 = "U_WDt1";
		public DateTime U_WDt1 { 
			get {
 				return getValueAsDateTime(_WDt1); 
			}
			set { setValue(_WDt1, value); }
		}
		public void U_WDt1_AsString(string value){
			setValue("U_WDt1", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_WDt1_AsString(){
			DateTime dVal = getValueAsDateTime(_WDt1);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_WDt1() {
               return doValidate_WDt1(U_WDt1);
           }
           public virtual string doValidate_WDt1(object oValue) {
               return base.doValidation(_WDt1, oValue);
           }

		/**
		 * Decription: Weigh Date 2
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WDt2
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _WDt2 = "U_WDt2";
		public DateTime U_WDt2 { 
			get {
 				return getValueAsDateTime(_WDt2); 
			}
			set { setValue(_WDt2, value); }
		}
		public void U_WDt2_AsString(string value){
			setValue("U_WDt2", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_WDt2_AsString(){
			DateTime dVal = getValueAsDateTime(_WDt2);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_WDt2() {
               return doValidate_WDt2(U_WDt2);
           }
           public virtual string doValidate_WDt2(object oValue) {
               return base.doValidation(_WDt2, oValue);
           }

		/**
		 * Decription: Weighridge Weigh 1
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Wei1
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Measurement
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Wei1 = "U_Wei1";
		public double U_Wei1 { 
			get {
 				return getValueAsDouble(_Wei1); 
			}
			set { setValue(_Wei1, value); }
		}
           public string doValidate_Wei1() {
               return doValidate_Wei1(U_Wei1);
           }
           public virtual string doValidate_Wei1(object oValue) {
               return base.doValidation(_Wei1, oValue);
           }

		/**
		 * Decription: Weighbridge Weigh 2
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Wei2
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Measurement
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Wei2 = "U_Wei2";
		public double U_Wei2 { 
			get {
 				return getValueAsDouble(_Wei2); 
			}
			set { setValue(_Wei2, value); }
		}
           public string doValidate_Wei2() {
               return doValidate_Wei2(U_Wei2);
           }
           public virtual string doValidate_Wei2(object oValue) {
               return base.doValidation(_Wei2, oValue);
           }

		/**
		 * Decription: Weight
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Weight
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Measurement
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _Weight = "U_Weight";
		public double U_Weight { 
			get {
 				return getValueAsDouble(_Weight); 
			}
			set { setValue(_Weight, value); }
		}
           public string doValidate_Weight() {
               return doValidate_Weight(U_Weight);
           }
           public virtual string doValidate_Weight(object oValue) {
               return base.doValidation(_Weight, oValue);
           }

		/**
		 * Decription: Weight Deduction
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WgtDed
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Measurement
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
		public readonly static string _WgtDed = "U_WgtDed";
		public double U_WgtDed { 
			get {
 				return getValueAsDouble(_WgtDed); 
			}
			set { setValue(_WgtDed, value); }
		}
           public string doValidate_WgtDed() {
               return doValidate_WgtDed(U_WgtDed);
           }
           public virtual string doValidate_WgtDed(object oValue) {
               return base.doValidation(_WgtDed, oValue);
           }

		/**
		 * Decription: Warehouse Transfer
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WHTRNF
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WHTRNF = "U_WHTRNF";
		public string U_WHTRNF { 
			get {
 				return getValueAsString(_WHTRNF); 
			}
			set { setValue(_WHTRNF, value); }
		}
           public string doValidate_WHTRNF() {
               return doValidate_WHTRNF(U_WHTRNF);
           }
           public virtual string doValidate_WHTRNF(object oValue) {
               return base.doValidation(_WHTRNF, oValue);
           }

		/**
		 * Decription: WOH ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WOHID
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WOHID = "U_WOHID";
		public string U_WOHID { 
			get {
 				return getValueAsString(_WOHID); 
			}
			set { setValue(_WOHID, value); }
		}
           public string doValidate_WOHID() {
               return doValidate_WOHID(U_WOHID);
           }
           public virtual string doValidate_WOHID(object oValue) {
               return base.doValidation(_WOHID, oValue);
           }

		/**
		 * Decription: WOR ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WORID
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WORID = "U_WORID";
		public string U_WORID { 
			get {
 				return getValueAsString(_WORID); 
			}
			set { setValue(_WORID, value); }
		}
           public string doValidate_WORID() {
               return doValidate_WORID(U_WORID);
           }
           public virtual string doValidate_WORID(object oValue) {
               return base.doValidation(_WORID, oValue);
           }

		/**
		 * Decription: WR Link Order
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WROrd
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WROrd = "U_WROrd";
		public string U_WROrd { 
			get {
 				return getValueAsString(_WROrd); 
			}
			set { setValue(_WROrd, value); }
		}
           public string doValidate_WROrd() {
               return doValidate_WROrd(U_WROrd);
           }
           public virtual string doValidate_WROrd(object oValue) {
               return base.doValidation(_WROrd, oValue);
           }

		/**
		 * Decription: Extra WR Link Order
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WROrd2
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WROrd2 = "U_WROrd2";
		public string U_WROrd2 { 
			get {
 				return getValueAsString(_WROrd2); 
			}
			set { setValue(_WROrd2, value); }
		}
           public string doValidate_WROrd2() {
               return doValidate_WROrd2(U_WROrd2);
           }
           public virtual string doValidate_WROrd2(object oValue) {
               return base.doValidation(_WROrd2, oValue);
           }

		/**
		 * Decription: WR Link Order Row
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WRRow
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WRRow = "U_WRRow";
		public string U_WRRow { 
			get {
 				return getValueAsString(_WRRow); 
			}
			set { setValue(_WRRow, value); }
		}
           public string doValidate_WRRow() {
               return doValidate_WRRow(U_WRRow);
           }
           public virtual string doValidate_WRRow(object oValue) {
               return base.doValidation(_WRRow, oValue);
           }

		/**
		 * Decription: Extra WR Link Order Row
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WRRow2
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WRRow2 = "U_WRRow2";
		public string U_WRRow2 { 
			get {
 				return getValueAsString(_WRRow2); 
			}
			set { setValue(_WRRow2, value); }
		}
           public string doValidate_WRRow2() {
               return doValidate_WRRow2(U_WRRow2);
           }
           public virtual string doValidate_WRRow2(object oValue) {
               return base.doValidation(_WRRow2, oValue);
           }

		/**
		 * Decription: Waste Group Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WstGpCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WstGpCd = "U_WstGpCd";
		public string U_WstGpCd { 
			get {
 				return getValueAsString(_WstGpCd); 
			}
			set { setValue(_WstGpCd, value); }
		}
           public string doValidate_WstGpCd() {
               return doValidate_WstGpCd(U_WstGpCd);
           }
           public virtual string doValidate_WstGpCd(object oValue) {
               return base.doValidation(_WstGpCd, oValue);
           }

		/**
		 * Decription: Waste Group Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WstGpNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _WstGpNm = "U_WstGpNm";
		public string U_WstGpNm { 
			get {
 				return getValueAsString(_WstGpNm); 
			}
			set { setValue(_WstGpNm, value); }
		}
           public string doValidate_WstGpNm() {
               return doValidate_WstGpNm(U_WstGpNm);
           }
           public virtual string doValidate_WstGpNm(object oValue) {
               return base.doValidation(_WstGpNm, oValue);
           }

           /**
             * Decription: Warehouse
             * DefaultValue: 
             * LinkedTable: 
             * Mandatory: False
             * Name: U_WhsCode
             * Size: 30
             * Type: db_Alpha
             * CType: string
             * SubType: st_None
             * ValidValue: 
             * Value: 
             * Identity: False
             */
           public readonly static string _WhsCode = "U_WhsCode";
           public string U_WhsCode  {
               get {
                   return getValueAsString(_WhsCode);
               }
               set { setValue(_WhsCode, value); }
           }
           public string doValidate_WhsCode() {
               return doValidate_WhsCode(U_WhsCode);
           }
           public virtual string doValidate_WhsCode(object oValue) {
               return base.doValidation(_WhsCode, oValue);
           }
#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(263);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_ActComm, "Action Comment", 2, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Action Comment
			moDBFields.Add(_AddCharge, "Additional Charge", 3, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Additional Charge
			moDBFields.Add(_AddCost, "Additional Cost", 4, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Additional Cost
			moDBFields.Add(_AddEx, "Additional Expences", 5, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 20, 0, false, false); //Additional Expences
			moDBFields.Add(_AddItmID, "WOR Additional Item ID", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //WOR Additional Item ID
			moDBFields.Add(_AdjWgt, "Adjusted Weight", 7, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 20, 0, false, false); //Adjusted Weight
			moDBFields.Add(_AdtnlItm, "Additional Item", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Additional Item
			moDBFields.Add(_AEDate, "Action End Date", 9, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Action End Date
			moDBFields.Add(_AETime, "Action End Time", 10, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Action End Time
			moDBFields.Add(_AltWasDsc, "Alternate Waste Descrp", 11, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Alternate Waste Descrp
			moDBFields.Add(_ASDate, "Action Start Date", 12, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Action Start Date
			moDBFields.Add(_ASTime, "Action Start Time", 13, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Action Start Time
			moDBFields.Add(_AUOM, "Purchase Unit Of Measure", 14, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Purchase Unit Of Measure
			moDBFields.Add(_AUOMQt, "Overriding Qty", 15, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Overriding Qty
			moDBFields.Add(_BasWOHID, "Base WOQ ID", 16, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Base WOQ ID
			moDBFields.Add(_BasWQRID, "Base WOQ Row ID", 17, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Base WOQ Row ID
			moDBFields.Add(_BDate, "Booking Date", 18, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Booking Date
			moDBFields.Add(_BookIn, "Book In Stock", 19, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Book In Stock
			moDBFields.Add(_BPWgt, "BP Weight", 20, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //BP Weight
			moDBFields.Add(_Branch, "Branch", 21, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Branch
			moDBFields.Add(_BTime, "Booking Time", 22, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Booking Time
			moDBFields.Add(_CarrCd, "CarrierCode", 23, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //CarrierCode
			moDBFields.Add(_CarrCgCc, "Carrier Charge Currency", 24, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //Carrier Charge Currency
			moDBFields.Add(_CarrCoCc, "Carrier Cost Currency", 25, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //Carrier Cost Currency
			moDBFields.Add(_CarrNm, "Carrier Name", 26, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Carrier Name
			moDBFields.Add(_CarrReb, "Carrier Rebate Item", 27, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Carrier Rebate Item
			moDBFields.Add(_CarrRef, "Carrier Ref. No.", 28, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Carrier Ref. No.
			moDBFields.Add(_CarWgt, "Carrier Weight", 29, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Carrier Weight
			moDBFields.Add(_CBICont, "Containers", 30, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Containers
			moDBFields.Add(_CBIManQty, "Manifest Quantity", 31, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Manifest Quantity
			moDBFields.Add(_CBIManUOM, "Manifest UOM", 32, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Manifest UOM
			moDBFields.Add(_CCExp, "CCard Expiry", 33, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //CCard Expiry
			moDBFields.Add(_CCHNum, "CCard House Number", 34, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //CCard House Number
			moDBFields.Add(_CCIs, "CCard Issue #", 35, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //CCard Issue #
			moDBFields.Add(_CCNum, "Credit Card No", 36, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Credit Card No
			moDBFields.Add(_CCPCd, "CCard PostCode", 37, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //CCard PostCode
			moDBFields.Add(_CCSec, "CCard Security Number", 38, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //CCard Security Number
			moDBFields.Add(_CCStat, "Credit Card Approval Status", 39, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Credit Card Approval Status
			moDBFields.Add(_CCType, "Credit Card Type", 40, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Credit Card Type
			moDBFields.Add(_ClgCode, "Activity Code", 41, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Activity Code
			moDBFields.Add(_CntrNo, "Contract Number", 42, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Contract Number
			moDBFields.Add(_Comment, "Comment", 43, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 230, EMPTYSTR, false, false); //Comment
			moDBFields.Add(_CongCd, "Congestion Supp Code", 44, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Congestion Supp Code
			moDBFields.Add(_CongCh, "Congestion Charge", 45, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Congestion Charge
			moDBFields.Add(_ConNum, "Consignment Number", 46, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Consignment Number
			moDBFields.Add(_Consiste, "Consistency", 47, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Consistency
			moDBFields.Add(_ContNr, "Container Number", 48, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Container Number
			moDBFields.Add(_Covera, "Coverage Used", 49, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Coverage Used
			moDBFields.Add(_CoverHst, "Coverage History", 50, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 230, EMPTYSTR, false, false); //Coverage History
			moDBFields.Add(_CstWgt, "Customer Weight", 51, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 20, 0, false, false); //Customer Weight
			moDBFields.Add(_CusChr, "Customer Charge", 52, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Customer Charge
			moDBFields.Add(_CusQty, "Customer Quantity", 53, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 20, 0, false, false); //Customer Quantity
			moDBFields.Add(_CustCd, "Customer Code", 54, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Customer Code
			moDBFields.Add(_CustCs, "Customer Compliance Scheme", 55, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Customer Compliance Scheme
			moDBFields.Add(_CustGR, "Customer Goods Receipt", 56, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Customer Goods Receipt
			moDBFields.Add(_CustNm, "Customer Name", 57, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Customer Name
			moDBFields.Add(_CustPay, "Customer In Payment", 58, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Customer In Payment
			moDBFields.Add(_CustReb, "Customer Rebate Item", 59, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Customer Rebate Item
			moDBFields.Add(_CustRef, "Customer Ref. No.", 60, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Customer Ref. No.
			moDBFields.Add(_DAAttach, "DA Attached", 61, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //DA Attached
			moDBFields.Add(_DisAmt, "Discount Amount", 62, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Discount Amount
			moDBFields.Add(_Discnt, "Discount", 63, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Discount
			moDBFields.Add(_DispCgCc, "Disposal Charge Currency", 64, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //Disposal Charge Currency
			moDBFields.Add(_DispCoCc, "Disposal Cost Currency", 65, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //Disposal Cost Currency
			moDBFields.Add(_Dista, "Distance", 66, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Distance
			moDBFields.Add(_DocNum, "Docket Number", 67, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Docket Number
			moDBFields.Add(_DPRRef, "DPR Referance", 68, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //DPR Referance
			moDBFields.Add(_Driver, "Driver", 69, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Driver
			moDBFields.Add(_DrivrOfSitDt, "Driver Off Site Date", 70, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Driver Off Site Date
			moDBFields.Add(_DrivrOfSitTm, "Driver Off Site Time", 71, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Driver Off Site Time
			moDBFields.Add(_DrivrOnSitDt, "Drive On Site Date", 72, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Drive On Site Date
			moDBFields.Add(_DrivrOnSitTm, "Drive On Site Time", 73, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Drive On Site Time
			moDBFields.Add(_Dusty, "Dusty", 74, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Dusty
			moDBFields.Add(_ENNO, "EN Number", 75, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //EN Number
			moDBFields.Add(_EnqID, "Enquiry ID", 76, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Enquiry ID
			moDBFields.Add(_EnqLID, "Enquiry Line ID", 77, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Enquiry Line ID
			moDBFields.Add(_ENREF, "Evidence Reference Number", 78, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Evidence Reference Number
			moDBFields.Add(_ExpLdWgt, "Load Weight", 79, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 20, 0, false, false); //Load Weight
			moDBFields.Add(_ExtComm1, "Ext. Comment1", 80, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Ext. Comment1
			moDBFields.Add(_ExtComm2, "Ext. Comment2", 81, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Ext. Comment2
			moDBFields.Add(_ExtWeig, "External Weighbridge Number", 82, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //External Weighbridge Number
			moDBFields.Add(_Firm, "Firm", 83, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Firm
			moDBFields.Add(_Fluid, "Fluid", 84, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Fluid
			moDBFields.Add(_FPChH, "Freeze Price Charge Haulage", 85, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Freeze Price Charge Haulage
			moDBFields.Add(_FPChT, "Freeze Price Charge Tipping", 86, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Freeze Price Charge Tipping
			moDBFields.Add(_FPCoH, "Freeze Price Cost Haulage", 87, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Freeze Price Cost Haulage
			moDBFields.Add(_FPCoT, "Freeze Price Cost Tipping", 88, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Freeze Price Cost Tipping
			moDBFields.Add(_GMargin, "Gross Margin", 89, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 20, 0, false, false); //Gross Margin
			moDBFields.Add(_GRIn, "Goods Receipt Incomming", 90, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Goods Receipt Incomming
			moDBFields.Add(_HaulAC, "Haulage Automatic Calculation", 91, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Haulage Automatic Calculation
			moDBFields.Add(_HazWCNN, "Hazardous Waste Consignment No", 92, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 12, EMPTYSTR, false, false); //Hazardous Waste Consignment No
			moDBFields.Add(_HChrgVtGrp, "Haulage Sales Vat Group", 93, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Haulage Sales Vat Group
			moDBFields.Add(_HChrgVtRt, "Haulage Sales Vat Rate", 94, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 20, 0, false, false); //Haulage Sales Vat Rate
			moDBFields.Add(_HCostVtGrp, "Tipping Buying Vat Group", 95, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Tipping Buying Vat Group
			moDBFields.Add(_HCostVtRt, "Tipping Buying Vat Rate", 96, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 20, 0, false, false); //Tipping Buying Vat Rate
			moDBFields.Add(_HlSQty, "Haulage Selling Qty", 97, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 20, 0, false, false); //Haulage Selling Qty
			moDBFields.Add(_IDHCHK, "Check", 98, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Check
			moDBFields.Add(_IDHCMT, "Route Comments", 99, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Route Comments
			moDBFields.Add(_IDHDRVI, "Driver Instructions", 100, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Driver Instructions
			moDBFields.Add(_IDHPRTD, "Printed", 101, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Printed
			moDBFields.Add(_IDHRTCD, "Route Code", 102, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, false); //Route Code
			moDBFields.Add(_IDHRTDT, "Route Date", 103, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Route Date
			moDBFields.Add(_IDHSEQ, "Sequence", 104, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Sequence
			moDBFields.Add(_IntComm, "Internal Comment", 105, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 230, EMPTYSTR, false, false); //Internal Comment
			moDBFields.Add(_IssQty, "Issue Qty", 106, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Issue Qty
			moDBFields.Add(_IsTrl, "Trail Load", 107, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Trail Load
			moDBFields.Add(_ItemCd, "Item Code", 108, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Item Code
			moDBFields.Add(_ItemDsc, "Item Description", 109, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Item Description
			moDBFields.Add(_ItmGrp, "Item Group Code", 110, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Item Group Code
			moDBFields.Add(_JbToBeDoneDt, "Jobs To Be Done Date", 111, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Jobs To Be Done Date
			moDBFields.Add(_JbToBeDoneTm, "Jobs To Be Done Time", 112, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Jobs To Be Done Time
			moDBFields.Add(_JCost, "Order Row Cost Total", 113, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Order Row Cost Total
			moDBFields.Add(_JobNr, "Order Number", 114, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Order Number
			moDBFields.Add(_JOBPO, "Job PO", 115, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Job PO
			moDBFields.Add(_JobRmD, "Reminder Date", 116, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Reminder Date
			moDBFields.Add(_JobRmT, "Reminder Time", 117, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Reminder Time
			moDBFields.Add(_JobTp, "Order Type", 118, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Order Type
			moDBFields.Add(_Jrnl, "Journal Entry", 119, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Journal Entry
			moDBFields.Add(_JStat, "Journals Status", 120, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Journals Status
			moDBFields.Add(_LckPrc, "Lock down Prices", 121, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Lock down Prices
			moDBFields.Add(_LiscCoCc, "Liscence Cost Currency", 122, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //Liscence Cost Currency
			moDBFields.Add(_LnkPBI, "Linked PBI Code", 123, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Linked PBI Code
			moDBFields.Add(_LoadSht, "Load Sheet Number", 124, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Load Sheet Number
			moDBFields.Add(_Lorry, "Lorry Registration", 125, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Lorry Registration
			moDBFields.Add(_LorryCd, "Lorry Code", 126, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, false); //Lorry Code
			moDBFields.Add(_LstPrice, "List Price", 127, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //List Price
			moDBFields.Add(_MANPRC, "Manual prices entered ( 1 - Ti", 128, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Manual prices entered ( 1 - Ti
			moDBFields.Add(_MaximoNum, "Maximo Number", 129, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Maximo Number
			moDBFields.Add(_MDChngd, "Marketing Document Changed", 130, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Marketing Document Changed
			moDBFields.Add(_MnMargin, "Manual Margin", 131, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 20, 0, false, false); //Manual Margin
			moDBFields.Add(_MSDS, "MSDS", 132, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //MSDS
			moDBFields.Add(_Obligated, "Obligated", 133, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Obligated
			moDBFields.Add(_OrdCost, "Order (Haulage) Cost", 134, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Order (Haulage) Cost
			moDBFields.Add(_OrdTot, "Order (Haulage) Total Cost", 135, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Order (Haulage) Total Cost
			moDBFields.Add(_OrdWgt, "Order (Haulage) Weight", 136, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 20, 0, false, false); //Order (Haulage) Weight
			moDBFields.Add(_Origin, "Origin", 137, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Origin
			moDBFields.Add(_OTComments, "Order Type Comments", 138, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Order Type Comments
			moDBFields.Add(_PaMuPu, "Pasty/ Muddy/ pulpy", 139, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Pasty/ Muddy/ pulpy
			moDBFields.Add(_PARCPT, "Payment Receipt", 140, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Payment Receipt
			moDBFields.Add(_PAUOMQt, "Purchase Overriding Qty", 141, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Purchase Overriding Qty
			moDBFields.Add(_PayMeth, "Payment Method", 142, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Payment Method
			moDBFields.Add(_PayStat, "Payment Status", 143, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Payment Status
			moDBFields.Add(_PayTrm, "Payment Term", 144, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Payment Term
			moDBFields.Add(_PCharge, "Producer Charge", 145, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Producer Charge
			moDBFields.Add(_PCost, "Producer Cost", 146, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Producer Cost
			moDBFields.Add(_PCTotal, "Producer Coharge Total", 147, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Producer Coharge Total
			moDBFields.Add(_PLineID, "Parent Row", 148, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Parent Row
			moDBFields.Add(_PrcLink, "CIPSIP Link", 149, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //CIPSIP Link
			moDBFields.Add(_PRdWgt, "Producer Read Weight", 150, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 20, 0, false, false); //Producer Read Weight
			moDBFields.Add(_Price, "Price", 151, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Price
			moDBFields.Add(_ProCd, "Producer Code", 152, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Producer Code
			moDBFields.Add(_ProGRPO, "Producer Goods Receipt PO", 153, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Producer Goods Receipt PO
			moDBFields.Add(_PROINV, "Producer Invoice", 154, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Producer Invoice
			moDBFields.Add(_ProNm, "Producer Name", 155, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Producer Name
			moDBFields.Add(_ProPay, "Producer OUT Payment", 156, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Producer OUT Payment
			moDBFields.Add(_ProPO, "Producer PO", 157, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Producer PO
			moDBFields.Add(_ProRef, "Producer Ref. No.", 158, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Producer Ref. No.
			moDBFields.Add(_ProUOM, "Producer Unit Of Measure", 159, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Producer Unit Of Measure
			moDBFields.Add(_ProWgt, "Producer Weight", 160, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 20, 0, false, false); //Producer Weight
			moDBFields.Add(_PStat, "Purchase Order Status", 161, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Purchase Order Status
			moDBFields.Add(_PUOM, "Purchase Unit Of Measure", 162, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Purchase Unit Of Measure
			moDBFields.Add(_PurcCoCc, "Purchase Cost Currency", 163, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3, EMPTYSTR, false, false); //Purchase Cost Currency
			moDBFields.Add(_Quantity, "Order Quantity", 164, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Order Quantity
			moDBFields.Add(_RDate, "Required Date", 165, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Required Date
			moDBFields.Add(_RdWgt, "Read Weight", 166, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 20, 0, false, false); //Read Weight
			moDBFields.Add(_Rebate, "Rebate Item", 167, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Rebate Item
			moDBFields.Add(_RemCnt, "Reminder Sent Count", 168, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 6, 0, false, false); //Reminder Sent Count
			moDBFields.Add(_RemNot, "Order Job Reminder Text", 169, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Order Job Reminder Text
			moDBFields.Add(_ReqArch, "Request to be archived", 170, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", false, false); //Request to be archived
			moDBFields.Add(_RouteCd, "Route Code", 171, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Route Code
			moDBFields.Add(_RowSta, "Row Status", 172, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Row Status
			moDBFields.Add(_RTime, "Required Time From", 173, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Required Time From
			moDBFields.Add(_RTimeT, "Required Time To", 174, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Required Time To
			moDBFields.Add(_SAddress, "Site Address", 175, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Site Address
			moDBFields.Add(_SAddrsLN, "Disposal Address LineNum", 176, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Disposal Address LineNum
			moDBFields.Add(_SAINV, "Sales Invoice", 177, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Sales Invoice
			moDBFields.Add(_Sample, "Sample", 178, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Sample
			moDBFields.Add(_SampleRef, "Sample Ref.", 179, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Sample Ref.
			moDBFields.Add(_SampStatus, "Sample Status", 180, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Sample Status
			moDBFields.Add(_SAORD, "Sales Order", 181, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Sales Order
			moDBFields.Add(_Sched, "Scheduled Job", 182, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Scheduled Job
			moDBFields.Add(_SCngNm, "Congestion Supplier Name", 183, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Congestion Supplier Name
			moDBFields.Add(_SealNr, "Seal Number", 184, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Seal Number
			moDBFields.Add(_SemSolid, "Semi Solid", 185, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Semi Solid
			moDBFields.Add(_Ser1, "Flash Serial 1", 186, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Flash Serial 1
			moDBFields.Add(_Ser2, "Flash Serial 2", 187, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Flash Serial 2
			moDBFields.Add(_Serial, "Serial Number", 188, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Serial Number
			moDBFields.Add(_SerialC, "Serial Number Collection", 189, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Serial Number Collection
			moDBFields.Add(_SiteRef, "Site Reference Number", 190, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Site Reference Number
			moDBFields.Add(_SLicCh, "Skip License Charge", 191, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Skip License Charge
			moDBFields.Add(_SLicCQt, "Skip Licence Quantity", 192, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Skip Licence Quantity
			moDBFields.Add(_SLicCst, "Skip Licence Cost", 193, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Skip Licence Cost
			moDBFields.Add(_SLicCTo, "Skip Licence Cost Total", 194, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Skip Licence Cost Total
			moDBFields.Add(_SLicExp, "Skip License Expiry Date", 195, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Skip License Expiry Date
			moDBFields.Add(_SLicNm, "Skip License Supplier Name", 196, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Skip License Supplier Name
			moDBFields.Add(_SLicNr, "Skip License No", 197, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Skip License No
			moDBFields.Add(_SLicSp, "Skip License Supplier", 198, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Skip License Supplier
			moDBFields.Add(_SLPO, "Skip licence PO", 199, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Skip licence PO
			moDBFields.Add(_SODlvNot, "SO Delivery Note", 200, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //SO Delivery Note
			moDBFields.Add(_Sort, "Sort Order", 201, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Sort Order
			moDBFields.Add(_Status, "Order Status", 202, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Order Status
			moDBFields.Add(_SupRef, "Supplier Ref No", 203, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Supplier Ref No
			moDBFields.Add(_TAddChrg, "Additional Charge", 204, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Additional Charge
			moDBFields.Add(_TAddChVat, "Additional Charge VAT", 205, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Additional Charge VAT
			moDBFields.Add(_TAddCost, "Additional Cost", 206, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Additional Cost
			moDBFields.Add(_TAddCsVat, "Additional Cost VAT", 207, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Additional Cost VAT
			moDBFields.Add(_TAUOMQt, "Disposal Cost Overriding Qty", 208, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Disposal Cost Overriding Qty
			moDBFields.Add(_TaxAmt, "Tax Amount", 209, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Tax Amount
			moDBFields.Add(_TCCN, "Customer Credit Note", 210, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Customer Credit Note
			moDBFields.Add(_TCharge, "Tipping Charge", 211, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Tipping Charge
			moDBFields.Add(_TChrgVtGrp, "Tipping Sales Vat Group", 212, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Tipping Sales Vat Group
			moDBFields.Add(_TChrgVtRt, "Tipping Sales Vat Rate", 213, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 20, 0, false, false); //Tipping Sales Vat Rate
			moDBFields.Add(_TCostVtGrp, "Tipping Buying Vat Group", 214, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Tipping Buying Vat Group
			moDBFields.Add(_TCostVtRt, "Tipping Buying Vat Rate", 215, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 20, 0, false, false); //Tipping Buying Vat Rate
			moDBFields.Add(_TCTotal, "Tipping Charge Total", 216, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Tipping Charge Total
			moDBFields.Add(_TFSMov, "TFS Movement", 217, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, false); //TFS Movement
			moDBFields.Add(_TFSNum, "TFS Number", 218, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //TFS Number
			moDBFields.Add(_TFSReq, "TFS Required", 219, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //TFS Required
			moDBFields.Add(_Tip, "Disposal Site", 220, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Disposal Site
			moDBFields.Add(_TipCost, "Tipping Cost", 221, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Tipping Cost
			moDBFields.Add(_TipNm, "Dispoal Site Name", 222, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Dispoal Site Name
			moDBFields.Add(_TIPPO, "Tipping PO", 223, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Tipping PO
			moDBFields.Add(_TipTot, "Tipping Total Cost", 224, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Tipping Total Cost
			moDBFields.Add(_TipWgt, "Tipping Weight", 225, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 20, 0, false, false); //Tipping Weight
			moDBFields.Add(_Total, "Total", 226, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Total
			moDBFields.Add(_TPCN, "Disposal Purchase Credit Note", 227, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Disposal Purchase Credit Note
			moDBFields.Add(_TRdWgt, "Tip Read Weight", 228, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 20, 0, false, false); //Tip Read Weight
			moDBFields.Add(_TRLNM, "Trailer Name", 229, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Trailer Name
			moDBFields.Add(_TRLReg, "Trailer Reg. No.", 230, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Trailer Reg. No.
			moDBFields.Add(_TrnCode, "Transaction Code", 231, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 40, EMPTYSTR, false, false); //Transaction Code
			moDBFields.Add(_TypPTreat, "Type of pre-treatment", 232, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Type of pre-treatment
			moDBFields.Add(_TZone, "Tip Zone", 233, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Tip Zone
			moDBFields.Add(_UOM, "Unit Of Measure", 234, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Unit Of Measure
			moDBFields.Add(_USched, "Un-Scheduled Job", 235, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Un-Scheduled Job
			moDBFields.Add(_UseBPWgt, "Use BP Weight", 236, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Use BP Weight
			moDBFields.Add(_User, "Created By", 237, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Created By
			moDBFields.Add(_UseWgt, "Which Weight to Use", 238, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Which Weight to Use
			moDBFields.Add(_ValDed, "Value Deduction", 239, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 20, 0, false, false); //Value Deduction
			moDBFields.Add(_VehTyp, "Vehicle Type", 240, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Vehicle Type
			moDBFields.Add(_VtCostAmt, "Purchasing Vat Amount", 241, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //Purchasing Vat Amount
			moDBFields.Add(_WasCd, "Waste Code", 242, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Waste Code
			moDBFields.Add(_WasDsc, "Waste Description", 243, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Waste Description
			moDBFields.Add(_WasFDsc, "Waste Cd Foreign Name", 244, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Waste Cd Foreign Name
			moDBFields.Add(_WASLIC, "Waste Lic. Reg. No.", 245, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Waste Lic. Reg. No.
			moDBFields.Add(_WastTNN, "Waste Transfer Note Number", 246, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Waste Transfer Note Number
			moDBFields.Add(_WDt1, "Weigh Date 1", 247, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Weigh Date 1
			moDBFields.Add(_WDt2, "Weigh Date 2", 248, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Weigh Date 2
			moDBFields.Add(_Wei1, "Weighridge Weigh 1", 249, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 20, 0, false, false); //Weighridge Weigh 1
			moDBFields.Add(_Wei2, "Weighbridge Weigh 2", 250, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 20, 0, false, false); //Weighbridge Weigh 2
			moDBFields.Add(_Weight, "Weight", 251, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 20, 0, false, false); //Weight
			moDBFields.Add(_WgtDed, "Weight Deduction", 252, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 20, 0, false, false); //Weight Deduction
			moDBFields.Add(_WHTRNF, "Warehouse Transfer", 253, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Warehouse Transfer
			moDBFields.Add(_WOHID, "WOH ID", 254, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //WOH ID
			moDBFields.Add(_WORID, "WOR ID", 255, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //WOR ID
			moDBFields.Add(_WROrd, "WR Link Order", 256, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //WR Link Order
			moDBFields.Add(_WROrd2, "Extra WR Link Order", 257, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Extra WR Link Order
			moDBFields.Add(_WRRow, "WR Link Order Row", 258, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //WR Link Order Row
			moDBFields.Add(_WRRow2, "Extra WR Link Order Row", 259, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Extra WR Link Order Row
			moDBFields.Add(_WstGpCd, "Waste Group Code", 260, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Waste Group Code
            moDBFields.Add(_WstGpNm, "Waste Group Name", 261, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Waste Group Name
            moDBFields.Add(_WhsCode, "Warehouse", 262, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Warehouse

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_ActComm());
            doBuildValidationString(doValidate_AddCharge());
            doBuildValidationString(doValidate_AddCost());
            doBuildValidationString(doValidate_AddEx());
            doBuildValidationString(doValidate_AddItmID());
            doBuildValidationString(doValidate_AdjWgt());
            doBuildValidationString(doValidate_AdtnlItm());
            doBuildValidationString(doValidate_AEDate());
            doBuildValidationString(doValidate_AETime());
            doBuildValidationString(doValidate_AltWasDsc());
            doBuildValidationString(doValidate_ASDate());
            doBuildValidationString(doValidate_ASTime());
            doBuildValidationString(doValidate_AUOM());
            doBuildValidationString(doValidate_AUOMQt());
            doBuildValidationString(doValidate_BasWOHID());
            doBuildValidationString(doValidate_BasWQRID());
            doBuildValidationString(doValidate_BDate());
            doBuildValidationString(doValidate_BookIn());
            doBuildValidationString(doValidate_BPWgt());
            doBuildValidationString(doValidate_Branch());
            doBuildValidationString(doValidate_BTime());
            doBuildValidationString(doValidate_CarrCd());
            doBuildValidationString(doValidate_CarrCgCc());
            doBuildValidationString(doValidate_CarrCoCc());
            doBuildValidationString(doValidate_CarrNm());
            doBuildValidationString(doValidate_CarrReb());
            doBuildValidationString(doValidate_CarrRef());
            doBuildValidationString(doValidate_CarWgt());
            doBuildValidationString(doValidate_CBICont());
            doBuildValidationString(doValidate_CBIManQty());
            doBuildValidationString(doValidate_CBIManUOM());
            doBuildValidationString(doValidate_CCExp());
            doBuildValidationString(doValidate_CCHNum());
            doBuildValidationString(doValidate_CCIs());
            doBuildValidationString(doValidate_CCNum());
            doBuildValidationString(doValidate_CCPCd());
            doBuildValidationString(doValidate_CCSec());
            doBuildValidationString(doValidate_CCStat());
            doBuildValidationString(doValidate_CCType());
            doBuildValidationString(doValidate_ClgCode());
            doBuildValidationString(doValidate_CntrNo());
            doBuildValidationString(doValidate_Comment());
            doBuildValidationString(doValidate_CongCd());
            doBuildValidationString(doValidate_CongCh());
            doBuildValidationString(doValidate_ConNum());
            doBuildValidationString(doValidate_Consiste());
            doBuildValidationString(doValidate_ContNr());
            doBuildValidationString(doValidate_Covera());
            doBuildValidationString(doValidate_CoverHst());
            doBuildValidationString(doValidate_CstWgt());
            doBuildValidationString(doValidate_CusChr());
            doBuildValidationString(doValidate_CusQty());
            doBuildValidationString(doValidate_CustCd());
            doBuildValidationString(doValidate_CustCs());
            doBuildValidationString(doValidate_CustGR());
            doBuildValidationString(doValidate_CustNm());
            doBuildValidationString(doValidate_CustPay());
            doBuildValidationString(doValidate_CustReb());
            doBuildValidationString(doValidate_CustRef());
            doBuildValidationString(doValidate_DAAttach());
            doBuildValidationString(doValidate_DisAmt());
            doBuildValidationString(doValidate_Discnt());
            doBuildValidationString(doValidate_DispCgCc());
            doBuildValidationString(doValidate_DispCoCc());
            doBuildValidationString(doValidate_Dista());
            doBuildValidationString(doValidate_DocNum());
            doBuildValidationString(doValidate_DPRRef());
            doBuildValidationString(doValidate_Driver());
            doBuildValidationString(doValidate_DrivrOfSitDt());
            doBuildValidationString(doValidate_DrivrOfSitTm());
            doBuildValidationString(doValidate_DrivrOnSitDt());
            doBuildValidationString(doValidate_DrivrOnSitTm());
            doBuildValidationString(doValidate_Dusty());
            doBuildValidationString(doValidate_ENNO());
            doBuildValidationString(doValidate_EnqID());
            doBuildValidationString(doValidate_EnqLID());
            doBuildValidationString(doValidate_ENREF());
            doBuildValidationString(doValidate_ExpLdWgt());
            doBuildValidationString(doValidate_ExtComm1());
            doBuildValidationString(doValidate_ExtComm2());
            doBuildValidationString(doValidate_ExtWeig());
            doBuildValidationString(doValidate_Firm());
            doBuildValidationString(doValidate_Fluid());
            doBuildValidationString(doValidate_FPChH());
            doBuildValidationString(doValidate_FPChT());
            doBuildValidationString(doValidate_FPCoH());
            doBuildValidationString(doValidate_FPCoT());
            doBuildValidationString(doValidate_GMargin());
            doBuildValidationString(doValidate_GRIn());
            doBuildValidationString(doValidate_HaulAC());
            doBuildValidationString(doValidate_HazWCNN());
            doBuildValidationString(doValidate_HChrgVtGrp());
            doBuildValidationString(doValidate_HChrgVtRt());
            doBuildValidationString(doValidate_HCostVtGrp());
            doBuildValidationString(doValidate_HCostVtRt());
            doBuildValidationString(doValidate_HlSQty());
            doBuildValidationString(doValidate_IDHCHK());
            doBuildValidationString(doValidate_IDHCMT());
            doBuildValidationString(doValidate_IDHDRVI());
            doBuildValidationString(doValidate_IDHPRTD());
            doBuildValidationString(doValidate_IDHRTCD());
            doBuildValidationString(doValidate_IDHRTDT());
            doBuildValidationString(doValidate_IDHSEQ());
            doBuildValidationString(doValidate_IntComm());
            doBuildValidationString(doValidate_IssQty());
            doBuildValidationString(doValidate_IsTrl());
            doBuildValidationString(doValidate_ItemCd());
            doBuildValidationString(doValidate_ItemDsc());
            doBuildValidationString(doValidate_ItmGrp());
            doBuildValidationString(doValidate_JbToBeDoneDt());
            doBuildValidationString(doValidate_JbToBeDoneTm());
            doBuildValidationString(doValidate_JCost());
            doBuildValidationString(doValidate_JobNr());
            doBuildValidationString(doValidate_JOBPO());
            doBuildValidationString(doValidate_JobRmD());
            doBuildValidationString(doValidate_JobRmT());
            doBuildValidationString(doValidate_JobTp());
            doBuildValidationString(doValidate_Jrnl());
            doBuildValidationString(doValidate_JStat());
            doBuildValidationString(doValidate_LckPrc());
            doBuildValidationString(doValidate_LiscCoCc());
            doBuildValidationString(doValidate_LnkPBI());
            doBuildValidationString(doValidate_LoadSht());
            doBuildValidationString(doValidate_Lorry());
            doBuildValidationString(doValidate_LorryCd());
            doBuildValidationString(doValidate_LstPrice());
            doBuildValidationString(doValidate_MANPRC());
            doBuildValidationString(doValidate_MaximoNum());
            doBuildValidationString(doValidate_MDChngd());
            doBuildValidationString(doValidate_MnMargin());
            doBuildValidationString(doValidate_MSDS());
            doBuildValidationString(doValidate_Obligated());
            doBuildValidationString(doValidate_OrdCost());
            doBuildValidationString(doValidate_OrdTot());
            doBuildValidationString(doValidate_OrdWgt());
            doBuildValidationString(doValidate_Origin());
            doBuildValidationString(doValidate_OTComments());
            doBuildValidationString(doValidate_PaMuPu());
            doBuildValidationString(doValidate_PARCPT());
            doBuildValidationString(doValidate_PAUOMQt());
            doBuildValidationString(doValidate_PayMeth());
            doBuildValidationString(doValidate_PayStat());
            doBuildValidationString(doValidate_PayTrm());
            doBuildValidationString(doValidate_PCharge());
            doBuildValidationString(doValidate_PCost());
            doBuildValidationString(doValidate_PCTotal());
            doBuildValidationString(doValidate_PLineID());
            doBuildValidationString(doValidate_PrcLink());
            doBuildValidationString(doValidate_PRdWgt());
            doBuildValidationString(doValidate_Price());
            doBuildValidationString(doValidate_ProCd());
            doBuildValidationString(doValidate_ProGRPO());
            doBuildValidationString(doValidate_PROINV());
            doBuildValidationString(doValidate_ProNm());
            doBuildValidationString(doValidate_ProPay());
            doBuildValidationString(doValidate_ProPO());
            doBuildValidationString(doValidate_ProRef());
            doBuildValidationString(doValidate_ProUOM());
            doBuildValidationString(doValidate_ProWgt());
            doBuildValidationString(doValidate_PStat());
            doBuildValidationString(doValidate_PUOM());
            doBuildValidationString(doValidate_PurcCoCc());
            doBuildValidationString(doValidate_Quantity());
            doBuildValidationString(doValidate_RDate());
            doBuildValidationString(doValidate_RdWgt());
            doBuildValidationString(doValidate_Rebate());
            doBuildValidationString(doValidate_RemCnt());
            doBuildValidationString(doValidate_RemNot());
            doBuildValidationString(doValidate_ReqArch());
            doBuildValidationString(doValidate_RouteCd());
            doBuildValidationString(doValidate_RowSta());
            doBuildValidationString(doValidate_RTime());
            doBuildValidationString(doValidate_RTimeT());
            doBuildValidationString(doValidate_SAddress());
            doBuildValidationString(doValidate_SAddrsLN());
            doBuildValidationString(doValidate_SAINV());
            doBuildValidationString(doValidate_Sample());
            doBuildValidationString(doValidate_SampleRef());
            doBuildValidationString(doValidate_SampStatus());
            doBuildValidationString(doValidate_SAORD());
            doBuildValidationString(doValidate_Sched());
            doBuildValidationString(doValidate_SCngNm());
            doBuildValidationString(doValidate_SealNr());
            doBuildValidationString(doValidate_SemSolid());
            doBuildValidationString(doValidate_Ser1());
            doBuildValidationString(doValidate_Ser2());
            doBuildValidationString(doValidate_Serial());
            doBuildValidationString(doValidate_SerialC());
            doBuildValidationString(doValidate_SiteRef());
            doBuildValidationString(doValidate_SLicCh());
            doBuildValidationString(doValidate_SLicCQt());
            doBuildValidationString(doValidate_SLicCst());
            doBuildValidationString(doValidate_SLicCTo());
            doBuildValidationString(doValidate_SLicExp());
            doBuildValidationString(doValidate_SLicNm());
            doBuildValidationString(doValidate_SLicNr());
            doBuildValidationString(doValidate_SLicSp());
            doBuildValidationString(doValidate_SLPO());
            doBuildValidationString(doValidate_SODlvNot());
            doBuildValidationString(doValidate_Sort());
            doBuildValidationString(doValidate_Status());
            doBuildValidationString(doValidate_SupRef());
            doBuildValidationString(doValidate_TAddChrg());
            doBuildValidationString(doValidate_TAddChVat());
            doBuildValidationString(doValidate_TAddCost());
            doBuildValidationString(doValidate_TAddCsVat());
            doBuildValidationString(doValidate_TAUOMQt());
            doBuildValidationString(doValidate_TaxAmt());
            doBuildValidationString(doValidate_TCCN());
            doBuildValidationString(doValidate_TCharge());
            doBuildValidationString(doValidate_TChrgVtGrp());
            doBuildValidationString(doValidate_TChrgVtRt());
            doBuildValidationString(doValidate_TCostVtGrp());
            doBuildValidationString(doValidate_TCostVtRt());
            doBuildValidationString(doValidate_TCTotal());
            doBuildValidationString(doValidate_TFSMov());
            doBuildValidationString(doValidate_TFSNum());
            doBuildValidationString(doValidate_TFSReq());
            doBuildValidationString(doValidate_Tip());
            doBuildValidationString(doValidate_TipCost());
            doBuildValidationString(doValidate_TipNm());
            doBuildValidationString(doValidate_TIPPO());
            doBuildValidationString(doValidate_TipTot());
            doBuildValidationString(doValidate_TipWgt());
            doBuildValidationString(doValidate_Total());
            doBuildValidationString(doValidate_TPCN());
            doBuildValidationString(doValidate_TRdWgt());
            doBuildValidationString(doValidate_TRLNM());
            doBuildValidationString(doValidate_TRLReg());
            doBuildValidationString(doValidate_TrnCode());
            doBuildValidationString(doValidate_TypPTreat());
            doBuildValidationString(doValidate_TZone());
            doBuildValidationString(doValidate_UOM());
            doBuildValidationString(doValidate_USched());
            doBuildValidationString(doValidate_UseBPWgt());
            doBuildValidationString(doValidate_User());
            doBuildValidationString(doValidate_UseWgt());
            doBuildValidationString(doValidate_ValDed());
            doBuildValidationString(doValidate_VehTyp());
            doBuildValidationString(doValidate_VtCostAmt());
            doBuildValidationString(doValidate_WasCd());
            doBuildValidationString(doValidate_WasDsc());
            doBuildValidationString(doValidate_WasFDsc());
            doBuildValidationString(doValidate_WASLIC());
            doBuildValidationString(doValidate_WastTNN());
            doBuildValidationString(doValidate_WDt1());
            doBuildValidationString(doValidate_WDt2());
            doBuildValidationString(doValidate_Wei1());
            doBuildValidationString(doValidate_Wei2());
            doBuildValidationString(doValidate_Weight());
            doBuildValidationString(doValidate_WgtDed());
            doBuildValidationString(doValidate_WHTRNF());
            doBuildValidationString(doValidate_WOHID());
            doBuildValidationString(doValidate_WORID());
            doBuildValidationString(doValidate_WROrd());
            doBuildValidationString(doValidate_WROrd2());
            doBuildValidationString(doValidate_WRRow());
            doBuildValidationString(doValidate_WRRow2());
            doBuildValidationString(doValidate_WstGpCd());
            doBuildValidationString(doValidate_WstGpNm());
            doBuildValidationString(doValidate_WhsCode());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_ActComm)) return doValidate_ActComm(oValue);
            if (sFieldName.Equals(_AddCharge)) return doValidate_AddCharge(oValue);
            if (sFieldName.Equals(_AddCost)) return doValidate_AddCost(oValue);
            if (sFieldName.Equals(_AddEx)) return doValidate_AddEx(oValue);
            if (sFieldName.Equals(_AddItmID)) return doValidate_AddItmID(oValue);
            if (sFieldName.Equals(_AdjWgt)) return doValidate_AdjWgt(oValue);
            if (sFieldName.Equals(_AdtnlItm)) return doValidate_AdtnlItm(oValue);
            if (sFieldName.Equals(_AEDate)) return doValidate_AEDate(oValue);
            if (sFieldName.Equals(_AETime)) return doValidate_AETime(oValue);
            if (sFieldName.Equals(_AltWasDsc)) return doValidate_AltWasDsc(oValue);
            if (sFieldName.Equals(_ASDate)) return doValidate_ASDate(oValue);
            if (sFieldName.Equals(_ASTime)) return doValidate_ASTime(oValue);
            if (sFieldName.Equals(_AUOM)) return doValidate_AUOM(oValue);
            if (sFieldName.Equals(_AUOMQt)) return doValidate_AUOMQt(oValue);
            if (sFieldName.Equals(_BasWOHID)) return doValidate_BasWOHID(oValue);
            if (sFieldName.Equals(_BasWQRID)) return doValidate_BasWQRID(oValue);
            if (sFieldName.Equals(_BDate)) return doValidate_BDate(oValue);
            if (sFieldName.Equals(_BookIn)) return doValidate_BookIn(oValue);
            if (sFieldName.Equals(_BPWgt)) return doValidate_BPWgt(oValue);
            if (sFieldName.Equals(_Branch)) return doValidate_Branch(oValue);
            if (sFieldName.Equals(_BTime)) return doValidate_BTime(oValue);
            if (sFieldName.Equals(_CarrCd)) return doValidate_CarrCd(oValue);
            if (sFieldName.Equals(_CarrCgCc)) return doValidate_CarrCgCc(oValue);
            if (sFieldName.Equals(_CarrCoCc)) return doValidate_CarrCoCc(oValue);
            if (sFieldName.Equals(_CarrNm)) return doValidate_CarrNm(oValue);
            if (sFieldName.Equals(_CarrReb)) return doValidate_CarrReb(oValue);
            if (sFieldName.Equals(_CarrRef)) return doValidate_CarrRef(oValue);
            if (sFieldName.Equals(_CarWgt)) return doValidate_CarWgt(oValue);
            if (sFieldName.Equals(_CBICont)) return doValidate_CBICont(oValue);
            if (sFieldName.Equals(_CBIManQty)) return doValidate_CBIManQty(oValue);
            if (sFieldName.Equals(_CBIManUOM)) return doValidate_CBIManUOM(oValue);
            if (sFieldName.Equals(_CCExp)) return doValidate_CCExp(oValue);
            if (sFieldName.Equals(_CCHNum)) return doValidate_CCHNum(oValue);
            if (sFieldName.Equals(_CCIs)) return doValidate_CCIs(oValue);
            if (sFieldName.Equals(_CCNum)) return doValidate_CCNum(oValue);
            if (sFieldName.Equals(_CCPCd)) return doValidate_CCPCd(oValue);
            if (sFieldName.Equals(_CCSec)) return doValidate_CCSec(oValue);
            if (sFieldName.Equals(_CCStat)) return doValidate_CCStat(oValue);
            if (sFieldName.Equals(_CCType)) return doValidate_CCType(oValue);
            if (sFieldName.Equals(_ClgCode)) return doValidate_ClgCode(oValue);
            if (sFieldName.Equals(_CntrNo)) return doValidate_CntrNo(oValue);
            if (sFieldName.Equals(_Comment)) return doValidate_Comment(oValue);
            if (sFieldName.Equals(_CongCd)) return doValidate_CongCd(oValue);
            if (sFieldName.Equals(_CongCh)) return doValidate_CongCh(oValue);
            if (sFieldName.Equals(_ConNum)) return doValidate_ConNum(oValue);
            if (sFieldName.Equals(_Consiste)) return doValidate_Consiste(oValue);
            if (sFieldName.Equals(_ContNr)) return doValidate_ContNr(oValue);
            if (sFieldName.Equals(_Covera)) return doValidate_Covera(oValue);
            if (sFieldName.Equals(_CoverHst)) return doValidate_CoverHst(oValue);
            if (sFieldName.Equals(_CstWgt)) return doValidate_CstWgt(oValue);
            if (sFieldName.Equals(_CusChr)) return doValidate_CusChr(oValue);
            if (sFieldName.Equals(_CusQty)) return doValidate_CusQty(oValue);
            if (sFieldName.Equals(_CustCd)) return doValidate_CustCd(oValue);
            if (sFieldName.Equals(_CustCs)) return doValidate_CustCs(oValue);
            if (sFieldName.Equals(_CustGR)) return doValidate_CustGR(oValue);
            if (sFieldName.Equals(_CustNm)) return doValidate_CustNm(oValue);
            if (sFieldName.Equals(_CustPay)) return doValidate_CustPay(oValue);
            if (sFieldName.Equals(_CustReb)) return doValidate_CustReb(oValue);
            if (sFieldName.Equals(_CustRef)) return doValidate_CustRef(oValue);
            if (sFieldName.Equals(_DAAttach)) return doValidate_DAAttach(oValue);
            if (sFieldName.Equals(_DisAmt)) return doValidate_DisAmt(oValue);
            if (sFieldName.Equals(_Discnt)) return doValidate_Discnt(oValue);
            if (sFieldName.Equals(_DispCgCc)) return doValidate_DispCgCc(oValue);
            if (sFieldName.Equals(_DispCoCc)) return doValidate_DispCoCc(oValue);
            if (sFieldName.Equals(_Dista)) return doValidate_Dista(oValue);
            if (sFieldName.Equals(_DocNum)) return doValidate_DocNum(oValue);
            if (sFieldName.Equals(_DPRRef)) return doValidate_DPRRef(oValue);
            if (sFieldName.Equals(_Driver)) return doValidate_Driver(oValue);
            if (sFieldName.Equals(_DrivrOfSitDt)) return doValidate_DrivrOfSitDt(oValue);
            if (sFieldName.Equals(_DrivrOfSitTm)) return doValidate_DrivrOfSitTm(oValue);
            if (sFieldName.Equals(_DrivrOnSitDt)) return doValidate_DrivrOnSitDt(oValue);
            if (sFieldName.Equals(_DrivrOnSitTm)) return doValidate_DrivrOnSitTm(oValue);
            if (sFieldName.Equals(_Dusty)) return doValidate_Dusty(oValue);
            if (sFieldName.Equals(_ENNO)) return doValidate_ENNO(oValue);
            if (sFieldName.Equals(_EnqID)) return doValidate_EnqID(oValue);
            if (sFieldName.Equals(_EnqLID)) return doValidate_EnqLID(oValue);
            if (sFieldName.Equals(_ENREF)) return doValidate_ENREF(oValue);
            if (sFieldName.Equals(_ExpLdWgt)) return doValidate_ExpLdWgt(oValue);
            if (sFieldName.Equals(_ExtComm1)) return doValidate_ExtComm1(oValue);
            if (sFieldName.Equals(_ExtComm2)) return doValidate_ExtComm2(oValue);
            if (sFieldName.Equals(_ExtWeig)) return doValidate_ExtWeig(oValue);
            if (sFieldName.Equals(_Firm)) return doValidate_Firm(oValue);
            if (sFieldName.Equals(_Fluid)) return doValidate_Fluid(oValue);
            if (sFieldName.Equals(_FPChH)) return doValidate_FPChH(oValue);
            if (sFieldName.Equals(_FPChT)) return doValidate_FPChT(oValue);
            if (sFieldName.Equals(_FPCoH)) return doValidate_FPCoH(oValue);
            if (sFieldName.Equals(_FPCoT)) return doValidate_FPCoT(oValue);
            if (sFieldName.Equals(_GMargin)) return doValidate_GMargin(oValue);
            if (sFieldName.Equals(_GRIn)) return doValidate_GRIn(oValue);
            if (sFieldName.Equals(_HaulAC)) return doValidate_HaulAC(oValue);
            if (sFieldName.Equals(_HazWCNN)) return doValidate_HazWCNN(oValue);
            if (sFieldName.Equals(_HChrgVtGrp)) return doValidate_HChrgVtGrp(oValue);
            if (sFieldName.Equals(_HChrgVtRt)) return doValidate_HChrgVtRt(oValue);
            if (sFieldName.Equals(_HCostVtGrp)) return doValidate_HCostVtGrp(oValue);
            if (sFieldName.Equals(_HCostVtRt)) return doValidate_HCostVtRt(oValue);
            if (sFieldName.Equals(_HlSQty)) return doValidate_HlSQty(oValue);
            if (sFieldName.Equals(_IDHCHK)) return doValidate_IDHCHK(oValue);
            if (sFieldName.Equals(_IDHCMT)) return doValidate_IDHCMT(oValue);
            if (sFieldName.Equals(_IDHDRVI)) return doValidate_IDHDRVI(oValue);
            if (sFieldName.Equals(_IDHPRTD)) return doValidate_IDHPRTD(oValue);
            if (sFieldName.Equals(_IDHRTCD)) return doValidate_IDHRTCD(oValue);
            if (sFieldName.Equals(_IDHRTDT)) return doValidate_IDHRTDT(oValue);
            if (sFieldName.Equals(_IDHSEQ)) return doValidate_IDHSEQ(oValue);
            if (sFieldName.Equals(_IntComm)) return doValidate_IntComm(oValue);
            if (sFieldName.Equals(_IssQty)) return doValidate_IssQty(oValue);
            if (sFieldName.Equals(_IsTrl)) return doValidate_IsTrl(oValue);
            if (sFieldName.Equals(_ItemCd)) return doValidate_ItemCd(oValue);
            if (sFieldName.Equals(_ItemDsc)) return doValidate_ItemDsc(oValue);
            if (sFieldName.Equals(_ItmGrp)) return doValidate_ItmGrp(oValue);
            if (sFieldName.Equals(_JbToBeDoneDt)) return doValidate_JbToBeDoneDt(oValue);
            if (sFieldName.Equals(_JbToBeDoneTm)) return doValidate_JbToBeDoneTm(oValue);
            if (sFieldName.Equals(_JCost)) return doValidate_JCost(oValue);
            if (sFieldName.Equals(_JobNr)) return doValidate_JobNr(oValue);
            if (sFieldName.Equals(_JOBPO)) return doValidate_JOBPO(oValue);
            if (sFieldName.Equals(_JobRmD)) return doValidate_JobRmD(oValue);
            if (sFieldName.Equals(_JobRmT)) return doValidate_JobRmT(oValue);
            if (sFieldName.Equals(_JobTp)) return doValidate_JobTp(oValue);
            if (sFieldName.Equals(_Jrnl)) return doValidate_Jrnl(oValue);
            if (sFieldName.Equals(_JStat)) return doValidate_JStat(oValue);
            if (sFieldName.Equals(_LckPrc)) return doValidate_LckPrc(oValue);
            if (sFieldName.Equals(_LiscCoCc)) return doValidate_LiscCoCc(oValue);
            if (sFieldName.Equals(_LnkPBI)) return doValidate_LnkPBI(oValue);
            if (sFieldName.Equals(_LoadSht)) return doValidate_LoadSht(oValue);
            if (sFieldName.Equals(_Lorry)) return doValidate_Lorry(oValue);
            if (sFieldName.Equals(_LorryCd)) return doValidate_LorryCd(oValue);
            if (sFieldName.Equals(_LstPrice)) return doValidate_LstPrice(oValue);
            if (sFieldName.Equals(_MANPRC)) return doValidate_MANPRC(oValue);
            if (sFieldName.Equals(_MaximoNum)) return doValidate_MaximoNum(oValue);
            if (sFieldName.Equals(_MDChngd)) return doValidate_MDChngd(oValue);
            if (sFieldName.Equals(_MnMargin)) return doValidate_MnMargin(oValue);
            if (sFieldName.Equals(_MSDS)) return doValidate_MSDS(oValue);
            if (sFieldName.Equals(_Obligated)) return doValidate_Obligated(oValue);
            if (sFieldName.Equals(_OrdCost)) return doValidate_OrdCost(oValue);
            if (sFieldName.Equals(_OrdTot)) return doValidate_OrdTot(oValue);
            if (sFieldName.Equals(_OrdWgt)) return doValidate_OrdWgt(oValue);
            if (sFieldName.Equals(_Origin)) return doValidate_Origin(oValue);
            if (sFieldName.Equals(_OTComments)) return doValidate_OTComments(oValue);
            if (sFieldName.Equals(_PaMuPu)) return doValidate_PaMuPu(oValue);
            if (sFieldName.Equals(_PARCPT)) return doValidate_PARCPT(oValue);
            if (sFieldName.Equals(_PAUOMQt)) return doValidate_PAUOMQt(oValue);
            if (sFieldName.Equals(_PayMeth)) return doValidate_PayMeth(oValue);
            if (sFieldName.Equals(_PayStat)) return doValidate_PayStat(oValue);
            if (sFieldName.Equals(_PayTrm)) return doValidate_PayTrm(oValue);
            if (sFieldName.Equals(_PCharge)) return doValidate_PCharge(oValue);
            if (sFieldName.Equals(_PCost)) return doValidate_PCost(oValue);
            if (sFieldName.Equals(_PCTotal)) return doValidate_PCTotal(oValue);
            if (sFieldName.Equals(_PLineID)) return doValidate_PLineID(oValue);
            if (sFieldName.Equals(_PrcLink)) return doValidate_PrcLink(oValue);
            if (sFieldName.Equals(_PRdWgt)) return doValidate_PRdWgt(oValue);
            if (sFieldName.Equals(_Price)) return doValidate_Price(oValue);
            if (sFieldName.Equals(_ProCd)) return doValidate_ProCd(oValue);
            if (sFieldName.Equals(_ProGRPO)) return doValidate_ProGRPO(oValue);
            if (sFieldName.Equals(_PROINV)) return doValidate_PROINV(oValue);
            if (sFieldName.Equals(_ProNm)) return doValidate_ProNm(oValue);
            if (sFieldName.Equals(_ProPay)) return doValidate_ProPay(oValue);
            if (sFieldName.Equals(_ProPO)) return doValidate_ProPO(oValue);
            if (sFieldName.Equals(_ProRef)) return doValidate_ProRef(oValue);
            if (sFieldName.Equals(_ProUOM)) return doValidate_ProUOM(oValue);
            if (sFieldName.Equals(_ProWgt)) return doValidate_ProWgt(oValue);
            if (sFieldName.Equals(_PStat)) return doValidate_PStat(oValue);
            if (sFieldName.Equals(_PUOM)) return doValidate_PUOM(oValue);
            if (sFieldName.Equals(_PurcCoCc)) return doValidate_PurcCoCc(oValue);
            if (sFieldName.Equals(_Quantity)) return doValidate_Quantity(oValue);
            if (sFieldName.Equals(_RDate)) return doValidate_RDate(oValue);
            if (sFieldName.Equals(_RdWgt)) return doValidate_RdWgt(oValue);
            if (sFieldName.Equals(_Rebate)) return doValidate_Rebate(oValue);
            if (sFieldName.Equals(_RemCnt)) return doValidate_RemCnt(oValue);
            if (sFieldName.Equals(_RemNot)) return doValidate_RemNot(oValue);
            if (sFieldName.Equals(_ReqArch)) return doValidate_ReqArch(oValue);
            if (sFieldName.Equals(_RouteCd)) return doValidate_RouteCd(oValue);
            if (sFieldName.Equals(_RowSta)) return doValidate_RowSta(oValue);
            if (sFieldName.Equals(_RTime)) return doValidate_RTime(oValue);
            if (sFieldName.Equals(_RTimeT)) return doValidate_RTimeT(oValue);
            if (sFieldName.Equals(_SAddress)) return doValidate_SAddress(oValue);
            if (sFieldName.Equals(_SAddrsLN)) return doValidate_SAddrsLN(oValue);
            if (sFieldName.Equals(_SAINV)) return doValidate_SAINV(oValue);
            if (sFieldName.Equals(_Sample)) return doValidate_Sample(oValue);
            if (sFieldName.Equals(_SampleRef)) return doValidate_SampleRef(oValue);
            if (sFieldName.Equals(_SampStatus)) return doValidate_SampStatus(oValue);
            if (sFieldName.Equals(_SAORD)) return doValidate_SAORD(oValue);
            if (sFieldName.Equals(_Sched)) return doValidate_Sched(oValue);
            if (sFieldName.Equals(_SCngNm)) return doValidate_SCngNm(oValue);
            if (sFieldName.Equals(_SealNr)) return doValidate_SealNr(oValue);
            if (sFieldName.Equals(_SemSolid)) return doValidate_SemSolid(oValue);
            if (sFieldName.Equals(_Ser1)) return doValidate_Ser1(oValue);
            if (sFieldName.Equals(_Ser2)) return doValidate_Ser2(oValue);
            if (sFieldName.Equals(_Serial)) return doValidate_Serial(oValue);
            if (sFieldName.Equals(_SerialC)) return doValidate_SerialC(oValue);
            if (sFieldName.Equals(_SiteRef)) return doValidate_SiteRef(oValue);
            if (sFieldName.Equals(_SLicCh)) return doValidate_SLicCh(oValue);
            if (sFieldName.Equals(_SLicCQt)) return doValidate_SLicCQt(oValue);
            if (sFieldName.Equals(_SLicCst)) return doValidate_SLicCst(oValue);
            if (sFieldName.Equals(_SLicCTo)) return doValidate_SLicCTo(oValue);
            if (sFieldName.Equals(_SLicExp)) return doValidate_SLicExp(oValue);
            if (sFieldName.Equals(_SLicNm)) return doValidate_SLicNm(oValue);
            if (sFieldName.Equals(_SLicNr)) return doValidate_SLicNr(oValue);
            if (sFieldName.Equals(_SLicSp)) return doValidate_SLicSp(oValue);
            if (sFieldName.Equals(_SLPO)) return doValidate_SLPO(oValue);
            if (sFieldName.Equals(_SODlvNot)) return doValidate_SODlvNot(oValue);
            if (sFieldName.Equals(_Sort)) return doValidate_Sort(oValue);
            if (sFieldName.Equals(_Status)) return doValidate_Status(oValue);
            if (sFieldName.Equals(_SupRef)) return doValidate_SupRef(oValue);
            if (sFieldName.Equals(_TAddChrg)) return doValidate_TAddChrg(oValue);
            if (sFieldName.Equals(_TAddChVat)) return doValidate_TAddChVat(oValue);
            if (sFieldName.Equals(_TAddCost)) return doValidate_TAddCost(oValue);
            if (sFieldName.Equals(_TAddCsVat)) return doValidate_TAddCsVat(oValue);
            if (sFieldName.Equals(_TAUOMQt)) return doValidate_TAUOMQt(oValue);
            if (sFieldName.Equals(_TaxAmt)) return doValidate_TaxAmt(oValue);
            if (sFieldName.Equals(_TCCN)) return doValidate_TCCN(oValue);
            if (sFieldName.Equals(_TCharge)) return doValidate_TCharge(oValue);
            if (sFieldName.Equals(_TChrgVtGrp)) return doValidate_TChrgVtGrp(oValue);
            if (sFieldName.Equals(_TChrgVtRt)) return doValidate_TChrgVtRt(oValue);
            if (sFieldName.Equals(_TCostVtGrp)) return doValidate_TCostVtGrp(oValue);
            if (sFieldName.Equals(_TCostVtRt)) return doValidate_TCostVtRt(oValue);
            if (sFieldName.Equals(_TCTotal)) return doValidate_TCTotal(oValue);
            if (sFieldName.Equals(_TFSMov)) return doValidate_TFSMov(oValue);
            if (sFieldName.Equals(_TFSNum)) return doValidate_TFSNum(oValue);
            if (sFieldName.Equals(_TFSReq)) return doValidate_TFSReq(oValue);
            if (sFieldName.Equals(_Tip)) return doValidate_Tip(oValue);
            if (sFieldName.Equals(_TipCost)) return doValidate_TipCost(oValue);
            if (sFieldName.Equals(_TipNm)) return doValidate_TipNm(oValue);
            if (sFieldName.Equals(_TIPPO)) return doValidate_TIPPO(oValue);
            if (sFieldName.Equals(_TipTot)) return doValidate_TipTot(oValue);
            if (sFieldName.Equals(_TipWgt)) return doValidate_TipWgt(oValue);
            if (sFieldName.Equals(_Total)) return doValidate_Total(oValue);
            if (sFieldName.Equals(_TPCN)) return doValidate_TPCN(oValue);
            if (sFieldName.Equals(_TRdWgt)) return doValidate_TRdWgt(oValue);
            if (sFieldName.Equals(_TRLNM)) return doValidate_TRLNM(oValue);
            if (sFieldName.Equals(_TRLReg)) return doValidate_TRLReg(oValue);
            if (sFieldName.Equals(_TrnCode)) return doValidate_TrnCode(oValue);
            if (sFieldName.Equals(_TypPTreat)) return doValidate_TypPTreat(oValue);
            if (sFieldName.Equals(_TZone)) return doValidate_TZone(oValue);
            if (sFieldName.Equals(_UOM)) return doValidate_UOM(oValue);
            if (sFieldName.Equals(_USched)) return doValidate_USched(oValue);
            if (sFieldName.Equals(_UseBPWgt)) return doValidate_UseBPWgt(oValue);
            if (sFieldName.Equals(_User)) return doValidate_User(oValue);
            if (sFieldName.Equals(_UseWgt)) return doValidate_UseWgt(oValue);
            if (sFieldName.Equals(_ValDed)) return doValidate_ValDed(oValue);
            if (sFieldName.Equals(_VehTyp)) return doValidate_VehTyp(oValue);
            if (sFieldName.Equals(_VtCostAmt)) return doValidate_VtCostAmt(oValue);
            if (sFieldName.Equals(_WasCd)) return doValidate_WasCd(oValue);
            if (sFieldName.Equals(_WasDsc)) return doValidate_WasDsc(oValue);
            if (sFieldName.Equals(_WasFDsc)) return doValidate_WasFDsc(oValue);
            if (sFieldName.Equals(_WASLIC)) return doValidate_WASLIC(oValue);
            if (sFieldName.Equals(_WastTNN)) return doValidate_WastTNN(oValue);
            if (sFieldName.Equals(_WDt1)) return doValidate_WDt1(oValue);
            if (sFieldName.Equals(_WDt2)) return doValidate_WDt2(oValue);
            if (sFieldName.Equals(_Wei1)) return doValidate_Wei1(oValue);
            if (sFieldName.Equals(_Wei2)) return doValidate_Wei2(oValue);
            if (sFieldName.Equals(_Weight)) return doValidate_Weight(oValue);
            if (sFieldName.Equals(_WgtDed)) return doValidate_WgtDed(oValue);
            if (sFieldName.Equals(_WHTRNF)) return doValidate_WHTRNF(oValue);
            if (sFieldName.Equals(_WOHID)) return doValidate_WOHID(oValue);
            if (sFieldName.Equals(_WORID)) return doValidate_WORID(oValue);
            if (sFieldName.Equals(_WROrd)) return doValidate_WROrd(oValue);
            if (sFieldName.Equals(_WROrd2)) return doValidate_WROrd2(oValue);
            if (sFieldName.Equals(_WRRow)) return doValidate_WRRow(oValue);
            if (sFieldName.Equals(_WRRow2)) return doValidate_WRRow2(oValue);
            if (sFieldName.Equals(_WstGpCd)) return doValidate_WstGpCd(oValue);
            if (sFieldName.Equals(_WstGpNm)) return doValidate_WstGpNm(oValue);
            if (sFieldName.Equals(_WhsCode)) return doValidate_WhsCode(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_ActComm Field to the Form Item.
		 */
		public void doLink_ActComm(string sControlName){
			moLinker.doLinkDataToControl(_ActComm, sControlName);
		}
		/**
		 * Link the U_AddCharge Field to the Form Item.
		 */
		public void doLink_AddCharge(string sControlName){
			moLinker.doLinkDataToControl(_AddCharge, sControlName);
		}
		/**
		 * Link the U_AddCost Field to the Form Item.
		 */
		public void doLink_AddCost(string sControlName){
			moLinker.doLinkDataToControl(_AddCost, sControlName);
		}
		/**
		 * Link the U_AddEx Field to the Form Item.
		 */
		public void doLink_AddEx(string sControlName){
			moLinker.doLinkDataToControl(_AddEx, sControlName);
		}
		/**
		 * Link the U_AddItmID Field to the Form Item.
		 */
		public void doLink_AddItmID(string sControlName){
			moLinker.doLinkDataToControl(_AddItmID, sControlName);
		}
		/**
		 * Link the U_AdjWgt Field to the Form Item.
		 */
		public void doLink_AdjWgt(string sControlName){
			moLinker.doLinkDataToControl(_AdjWgt, sControlName);
		}
		/**
		 * Link the U_AdtnlItm Field to the Form Item.
		 */
		public void doLink_AdtnlItm(string sControlName){
			moLinker.doLinkDataToControl(_AdtnlItm, sControlName);
		}
		/**
		 * Link the U_AEDate Field to the Form Item.
		 */
		public void doLink_AEDate(string sControlName){
			moLinker.doLinkDataToControl(_AEDate, sControlName);
		}
		/**
		 * Link the U_AETime Field to the Form Item.
		 */
		public void doLink_AETime(string sControlName){
			moLinker.doLinkDataToControl(_AETime, sControlName);
		}
		/**
		 * Link the U_AltWasDsc Field to the Form Item.
		 */
		public void doLink_AltWasDsc(string sControlName){
			moLinker.doLinkDataToControl(_AltWasDsc, sControlName);
		}
		/**
		 * Link the U_ASDate Field to the Form Item.
		 */
		public void doLink_ASDate(string sControlName){
			moLinker.doLinkDataToControl(_ASDate, sControlName);
		}
		/**
		 * Link the U_ASTime Field to the Form Item.
		 */
		public void doLink_ASTime(string sControlName){
			moLinker.doLinkDataToControl(_ASTime, sControlName);
		}
		/**
		 * Link the U_AUOM Field to the Form Item.
		 */
		public void doLink_AUOM(string sControlName){
			moLinker.doLinkDataToControl(_AUOM, sControlName);
		}
		/**
		 * Link the U_AUOMQt Field to the Form Item.
		 */
		public void doLink_AUOMQt(string sControlName){
			moLinker.doLinkDataToControl(_AUOMQt, sControlName);
		}
		/**
		 * Link the U_BasWOHID Field to the Form Item.
		 */
		public void doLink_BasWOHID(string sControlName){
			moLinker.doLinkDataToControl(_BasWOHID, sControlName);
		}
		/**
		 * Link the U_BasWQRID Field to the Form Item.
		 */
		public void doLink_BasWQRID(string sControlName){
			moLinker.doLinkDataToControl(_BasWQRID, sControlName);
		}
		/**
		 * Link the U_BDate Field to the Form Item.
		 */
		public void doLink_BDate(string sControlName){
			moLinker.doLinkDataToControl(_BDate, sControlName);
		}
		/**
		 * Link the U_BookIn Field to the Form Item.
		 */
		public void doLink_BookIn(string sControlName){
			moLinker.doLinkDataToControl(_BookIn, sControlName);
		}
		/**
		 * Link the U_BPWgt Field to the Form Item.
		 */
		public void doLink_BPWgt(string sControlName){
			moLinker.doLinkDataToControl(_BPWgt, sControlName);
		}
		/**
		 * Link the U_Branch Field to the Form Item.
		 */
		public void doLink_Branch(string sControlName){
			moLinker.doLinkDataToControl(_Branch, sControlName);
		}
		/**
		 * Link the U_BTime Field to the Form Item.
		 */
		public void doLink_BTime(string sControlName){
			moLinker.doLinkDataToControl(_BTime, sControlName);
		}
		/**
		 * Link the U_CarrCd Field to the Form Item.
		 */
		public void doLink_CarrCd(string sControlName){
			moLinker.doLinkDataToControl(_CarrCd, sControlName);
		}
		/**
		 * Link the U_CarrCgCc Field to the Form Item.
		 */
		public void doLink_CarrCgCc(string sControlName){
			moLinker.doLinkDataToControl(_CarrCgCc, sControlName);
		}
		/**
		 * Link the U_CarrCoCc Field to the Form Item.
		 */
		public void doLink_CarrCoCc(string sControlName){
			moLinker.doLinkDataToControl(_CarrCoCc, sControlName);
		}
		/**
		 * Link the U_CarrNm Field to the Form Item.
		 */
		public void doLink_CarrNm(string sControlName){
			moLinker.doLinkDataToControl(_CarrNm, sControlName);
		}
		/**
		 * Link the U_CarrReb Field to the Form Item.
		 */
		public void doLink_CarrReb(string sControlName){
			moLinker.doLinkDataToControl(_CarrReb, sControlName);
		}
		/**
		 * Link the U_CarrRef Field to the Form Item.
		 */
		public void doLink_CarrRef(string sControlName){
			moLinker.doLinkDataToControl(_CarrRef, sControlName);
		}
		/**
		 * Link the U_CarWgt Field to the Form Item.
		 */
		public void doLink_CarWgt(string sControlName){
			moLinker.doLinkDataToControl(_CarWgt, sControlName);
		}
		/**
		 * Link the U_CBICont Field to the Form Item.
		 */
		public void doLink_CBICont(string sControlName){
			moLinker.doLinkDataToControl(_CBICont, sControlName);
		}
		/**
		 * Link the U_CBIManQty Field to the Form Item.
		 */
		public void doLink_CBIManQty(string sControlName){
			moLinker.doLinkDataToControl(_CBIManQty, sControlName);
		}
		/**
		 * Link the U_CBIManUOM Field to the Form Item.
		 */
		public void doLink_CBIManUOM(string sControlName){
			moLinker.doLinkDataToControl(_CBIManUOM, sControlName);
		}
		/**
		 * Link the U_CCExp Field to the Form Item.
		 */
		public void doLink_CCExp(string sControlName){
			moLinker.doLinkDataToControl(_CCExp, sControlName);
		}
		/**
		 * Link the U_CCHNum Field to the Form Item.
		 */
		public void doLink_CCHNum(string sControlName){
			moLinker.doLinkDataToControl(_CCHNum, sControlName);
		}
		/**
		 * Link the U_CCIs Field to the Form Item.
		 */
		public void doLink_CCIs(string sControlName){
			moLinker.doLinkDataToControl(_CCIs, sControlName);
		}
		/**
		 * Link the U_CCNum Field to the Form Item.
		 */
		public void doLink_CCNum(string sControlName){
			moLinker.doLinkDataToControl(_CCNum, sControlName);
		}
		/**
		 * Link the U_CCPCd Field to the Form Item.
		 */
		public void doLink_CCPCd(string sControlName){
			moLinker.doLinkDataToControl(_CCPCd, sControlName);
		}
		/**
		 * Link the U_CCSec Field to the Form Item.
		 */
		public void doLink_CCSec(string sControlName){
			moLinker.doLinkDataToControl(_CCSec, sControlName);
		}
		/**
		 * Link the U_CCStat Field to the Form Item.
		 */
		public void doLink_CCStat(string sControlName){
			moLinker.doLinkDataToControl(_CCStat, sControlName);
		}
		/**
		 * Link the U_CCType Field to the Form Item.
		 */
		public void doLink_CCType(string sControlName){
			moLinker.doLinkDataToControl(_CCType, sControlName);
		}
		/**
		 * Link the U_ClgCode Field to the Form Item.
		 */
		public void doLink_ClgCode(string sControlName){
			moLinker.doLinkDataToControl(_ClgCode, sControlName);
		}
		/**
		 * Link the U_CntrNo Field to the Form Item.
		 */
		public void doLink_CntrNo(string sControlName){
			moLinker.doLinkDataToControl(_CntrNo, sControlName);
		}
		/**
		 * Link the U_Comment Field to the Form Item.
		 */
		public void doLink_Comment(string sControlName){
			moLinker.doLinkDataToControl(_Comment, sControlName);
		}
		/**
		 * Link the U_CongCd Field to the Form Item.
		 */
		public void doLink_CongCd(string sControlName){
			moLinker.doLinkDataToControl(_CongCd, sControlName);
		}
		/**
		 * Link the U_CongCh Field to the Form Item.
		 */
		public void doLink_CongCh(string sControlName){
			moLinker.doLinkDataToControl(_CongCh, sControlName);
		}
		/**
		 * Link the U_ConNum Field to the Form Item.
		 */
		public void doLink_ConNum(string sControlName){
			moLinker.doLinkDataToControl(_ConNum, sControlName);
		}
		/**
		 * Link the U_Consiste Field to the Form Item.
		 */
		public void doLink_Consiste(string sControlName){
			moLinker.doLinkDataToControl(_Consiste, sControlName);
		}
		/**
		 * Link the U_ContNr Field to the Form Item.
		 */
		public void doLink_ContNr(string sControlName){
			moLinker.doLinkDataToControl(_ContNr, sControlName);
		}
		/**
		 * Link the U_Covera Field to the Form Item.
		 */
		public void doLink_Covera(string sControlName){
			moLinker.doLinkDataToControl(_Covera, sControlName);
		}
		/**
		 * Link the U_CoverHst Field to the Form Item.
		 */
		public void doLink_CoverHst(string sControlName){
			moLinker.doLinkDataToControl(_CoverHst, sControlName);
		}
		/**
		 * Link the U_CstWgt Field to the Form Item.
		 */
		public void doLink_CstWgt(string sControlName){
			moLinker.doLinkDataToControl(_CstWgt, sControlName);
		}
		/**
		 * Link the U_CusChr Field to the Form Item.
		 */
		public void doLink_CusChr(string sControlName){
			moLinker.doLinkDataToControl(_CusChr, sControlName);
		}
		/**
		 * Link the U_CusQty Field to the Form Item.
		 */
		public void doLink_CusQty(string sControlName){
			moLinker.doLinkDataToControl(_CusQty, sControlName);
		}
		/**
		 * Link the U_CustCd Field to the Form Item.
		 */
		public void doLink_CustCd(string sControlName){
			moLinker.doLinkDataToControl(_CustCd, sControlName);
		}
		/**
		 * Link the U_CustCs Field to the Form Item.
		 */
		public void doLink_CustCs(string sControlName){
			moLinker.doLinkDataToControl(_CustCs, sControlName);
		}
		/**
		 * Link the U_CustGR Field to the Form Item.
		 */
		public void doLink_CustGR(string sControlName){
			moLinker.doLinkDataToControl(_CustGR, sControlName);
		}
		/**
		 * Link the U_CustNm Field to the Form Item.
		 */
		public void doLink_CustNm(string sControlName){
			moLinker.doLinkDataToControl(_CustNm, sControlName);
		}
		/**
		 * Link the U_CustPay Field to the Form Item.
		 */
		public void doLink_CustPay(string sControlName){
			moLinker.doLinkDataToControl(_CustPay, sControlName);
		}
		/**
		 * Link the U_CustReb Field to the Form Item.
		 */
		public void doLink_CustReb(string sControlName){
			moLinker.doLinkDataToControl(_CustReb, sControlName);
		}
		/**
		 * Link the U_CustRef Field to the Form Item.
		 */
		public void doLink_CustRef(string sControlName){
			moLinker.doLinkDataToControl(_CustRef, sControlName);
		}
		/**
		 * Link the U_DAAttach Field to the Form Item.
		 */
		public void doLink_DAAttach(string sControlName){
			moLinker.doLinkDataToControl(_DAAttach, sControlName);
		}
		/**
		 * Link the U_DisAmt Field to the Form Item.
		 */
		public void doLink_DisAmt(string sControlName){
			moLinker.doLinkDataToControl(_DisAmt, sControlName);
		}
		/**
		 * Link the U_Discnt Field to the Form Item.
		 */
		public void doLink_Discnt(string sControlName){
			moLinker.doLinkDataToControl(_Discnt, sControlName);
		}
		/**
		 * Link the U_DispCgCc Field to the Form Item.
		 */
		public void doLink_DispCgCc(string sControlName){
			moLinker.doLinkDataToControl(_DispCgCc, sControlName);
		}
		/**
		 * Link the U_DispCoCc Field to the Form Item.
		 */
		public void doLink_DispCoCc(string sControlName){
			moLinker.doLinkDataToControl(_DispCoCc, sControlName);
		}
		/**
		 * Link the U_Dista Field to the Form Item.
		 */
		public void doLink_Dista(string sControlName){
			moLinker.doLinkDataToControl(_Dista, sControlName);
		}
		/**
		 * Link the U_DocNum Field to the Form Item.
		 */
		public void doLink_DocNum(string sControlName){
			moLinker.doLinkDataToControl(_DocNum, sControlName);
		}
		/**
		 * Link the U_DPRRef Field to the Form Item.
		 */
		public void doLink_DPRRef(string sControlName){
			moLinker.doLinkDataToControl(_DPRRef, sControlName);
		}
		/**
		 * Link the U_Driver Field to the Form Item.
		 */
		public void doLink_Driver(string sControlName){
			moLinker.doLinkDataToControl(_Driver, sControlName);
		}
		/**
		 * Link the U_DrivrOfSitDt Field to the Form Item.
		 */
		public void doLink_DrivrOfSitDt(string sControlName){
			moLinker.doLinkDataToControl(_DrivrOfSitDt, sControlName);
		}
		/**
		 * Link the U_DrivrOfSitTm Field to the Form Item.
		 */
		public void doLink_DrivrOfSitTm(string sControlName){
			moLinker.doLinkDataToControl(_DrivrOfSitTm, sControlName);
		}
		/**
		 * Link the U_DrivrOnSitDt Field to the Form Item.
		 */
		public void doLink_DrivrOnSitDt(string sControlName){
			moLinker.doLinkDataToControl(_DrivrOnSitDt, sControlName);
		}
		/**
		 * Link the U_DrivrOnSitTm Field to the Form Item.
		 */
		public void doLink_DrivrOnSitTm(string sControlName){
			moLinker.doLinkDataToControl(_DrivrOnSitTm, sControlName);
		}
		/**
		 * Link the U_Dusty Field to the Form Item.
		 */
		public void doLink_Dusty(string sControlName){
			moLinker.doLinkDataToControl(_Dusty, sControlName);
		}
		/**
		 * Link the U_ENNO Field to the Form Item.
		 */
		public void doLink_ENNO(string sControlName){
			moLinker.doLinkDataToControl(_ENNO, sControlName);
		}
		/**
		 * Link the U_EnqID Field to the Form Item.
		 */
		public void doLink_EnqID(string sControlName){
			moLinker.doLinkDataToControl(_EnqID, sControlName);
		}
		/**
		 * Link the U_EnqLID Field to the Form Item.
		 */
		public void doLink_EnqLID(string sControlName){
			moLinker.doLinkDataToControl(_EnqLID, sControlName);
		}
		/**
		 * Link the U_ENREF Field to the Form Item.
		 */
		public void doLink_ENREF(string sControlName){
			moLinker.doLinkDataToControl(_ENREF, sControlName);
		}
		/**
		 * Link the U_ExpLdWgt Field to the Form Item.
		 */
		public void doLink_ExpLdWgt(string sControlName){
			moLinker.doLinkDataToControl(_ExpLdWgt, sControlName);
		}
		/**
		 * Link the U_ExtComm1 Field to the Form Item.
		 */
		public void doLink_ExtComm1(string sControlName){
			moLinker.doLinkDataToControl(_ExtComm1, sControlName);
		}
		/**
		 * Link the U_ExtComm2 Field to the Form Item.
		 */
		public void doLink_ExtComm2(string sControlName){
			moLinker.doLinkDataToControl(_ExtComm2, sControlName);
		}
		/**
		 * Link the U_ExtWeig Field to the Form Item.
		 */
		public void doLink_ExtWeig(string sControlName){
			moLinker.doLinkDataToControl(_ExtWeig, sControlName);
		}
		/**
		 * Link the U_Firm Field to the Form Item.
		 */
		public void doLink_Firm(string sControlName){
			moLinker.doLinkDataToControl(_Firm, sControlName);
		}
		/**
		 * Link the U_Fluid Field to the Form Item.
		 */
		public void doLink_Fluid(string sControlName){
			moLinker.doLinkDataToControl(_Fluid, sControlName);
		}
		/**
		 * Link the U_FPChH Field to the Form Item.
		 */
		public void doLink_FPChH(string sControlName){
			moLinker.doLinkDataToControl(_FPChH, sControlName);
		}
		/**
		 * Link the U_FPChT Field to the Form Item.
		 */
		public void doLink_FPChT(string sControlName){
			moLinker.doLinkDataToControl(_FPChT, sControlName);
		}
		/**
		 * Link the U_FPCoH Field to the Form Item.
		 */
		public void doLink_FPCoH(string sControlName){
			moLinker.doLinkDataToControl(_FPCoH, sControlName);
		}
		/**
		 * Link the U_FPCoT Field to the Form Item.
		 */
		public void doLink_FPCoT(string sControlName){
			moLinker.doLinkDataToControl(_FPCoT, sControlName);
		}
		/**
		 * Link the U_GMargin Field to the Form Item.
		 */
		public void doLink_GMargin(string sControlName){
			moLinker.doLinkDataToControl(_GMargin, sControlName);
		}
		/**
		 * Link the U_GRIn Field to the Form Item.
		 */
		public void doLink_GRIn(string sControlName){
			moLinker.doLinkDataToControl(_GRIn, sControlName);
		}
		/**
		 * Link the U_HaulAC Field to the Form Item.
		 */
		public void doLink_HaulAC(string sControlName){
			moLinker.doLinkDataToControl(_HaulAC, sControlName);
		}
		/**
		 * Link the U_HazWCNN Field to the Form Item.
		 */
		public void doLink_HazWCNN(string sControlName){
			moLinker.doLinkDataToControl(_HazWCNN, sControlName);
		}
		/**
		 * Link the U_HChrgVtGrp Field to the Form Item.
		 */
		public void doLink_HChrgVtGrp(string sControlName){
			moLinker.doLinkDataToControl(_HChrgVtGrp, sControlName);
		}
		/**
		 * Link the U_HChrgVtRt Field to the Form Item.
		 */
		public void doLink_HChrgVtRt(string sControlName){
			moLinker.doLinkDataToControl(_HChrgVtRt, sControlName);
		}
		/**
		 * Link the U_HCostVtGrp Field to the Form Item.
		 */
		public void doLink_HCostVtGrp(string sControlName){
			moLinker.doLinkDataToControl(_HCostVtGrp, sControlName);
		}
		/**
		 * Link the U_HCostVtRt Field to the Form Item.
		 */
		public void doLink_HCostVtRt(string sControlName){
			moLinker.doLinkDataToControl(_HCostVtRt, sControlName);
		}
		/**
		 * Link the U_HlSQty Field to the Form Item.
		 */
		public void doLink_HlSQty(string sControlName){
			moLinker.doLinkDataToControl(_HlSQty, sControlName);
		}
		/**
		 * Link the U_IDHCHK Field to the Form Item.
		 */
		public void doLink_IDHCHK(string sControlName){
			moLinker.doLinkDataToControl(_IDHCHK, sControlName);
		}
		/**
		 * Link the U_IDHCMT Field to the Form Item.
		 */
		public void doLink_IDHCMT(string sControlName){
			moLinker.doLinkDataToControl(_IDHCMT, sControlName);
		}
		/**
		 * Link the U_IDHDRVI Field to the Form Item.
		 */
		public void doLink_IDHDRVI(string sControlName){
			moLinker.doLinkDataToControl(_IDHDRVI, sControlName);
		}
		/**
		 * Link the U_IDHPRTD Field to the Form Item.
		 */
		public void doLink_IDHPRTD(string sControlName){
			moLinker.doLinkDataToControl(_IDHPRTD, sControlName);
		}
		/**
		 * Link the U_IDHRTCD Field to the Form Item.
		 */
		public void doLink_IDHRTCD(string sControlName){
			moLinker.doLinkDataToControl(_IDHRTCD, sControlName);
		}
		/**
		 * Link the U_IDHRTDT Field to the Form Item.
		 */
		public void doLink_IDHRTDT(string sControlName){
			moLinker.doLinkDataToControl(_IDHRTDT, sControlName);
		}
		/**
		 * Link the U_IDHSEQ Field to the Form Item.
		 */
		public void doLink_IDHSEQ(string sControlName){
			moLinker.doLinkDataToControl(_IDHSEQ, sControlName);
		}
		/**
		 * Link the U_IntComm Field to the Form Item.
		 */
		public void doLink_IntComm(string sControlName){
			moLinker.doLinkDataToControl(_IntComm, sControlName);
		}
		/**
		 * Link the U_IssQty Field to the Form Item.
		 */
		public void doLink_IssQty(string sControlName){
			moLinker.doLinkDataToControl(_IssQty, sControlName);
		}
		/**
		 * Link the U_IsTrl Field to the Form Item.
		 */
		public void doLink_IsTrl(string sControlName){
			moLinker.doLinkDataToControl(_IsTrl, sControlName);
		}
		/**
		 * Link the U_ItemCd Field to the Form Item.
		 */
		public void doLink_ItemCd(string sControlName){
			moLinker.doLinkDataToControl(_ItemCd, sControlName);
		}
		/**
		 * Link the U_ItemDsc Field to the Form Item.
		 */
		public void doLink_ItemDsc(string sControlName){
			moLinker.doLinkDataToControl(_ItemDsc, sControlName);
		}
		/**
		 * Link the U_ItmGrp Field to the Form Item.
		 */
		public void doLink_ItmGrp(string sControlName){
			moLinker.doLinkDataToControl(_ItmGrp, sControlName);
		}
		/**
		 * Link the U_JbToBeDoneDt Field to the Form Item.
		 */
		public void doLink_JbToBeDoneDt(string sControlName){
			moLinker.doLinkDataToControl(_JbToBeDoneDt, sControlName);
		}
		/**
		 * Link the U_JbToBeDoneTm Field to the Form Item.
		 */
		public void doLink_JbToBeDoneTm(string sControlName){
			moLinker.doLinkDataToControl(_JbToBeDoneTm, sControlName);
		}
		/**
		 * Link the U_JCost Field to the Form Item.
		 */
		public void doLink_JCost(string sControlName){
			moLinker.doLinkDataToControl(_JCost, sControlName);
		}
		/**
		 * Link the U_JobNr Field to the Form Item.
		 */
		public void doLink_JobNr(string sControlName){
			moLinker.doLinkDataToControl(_JobNr, sControlName);
		}
		/**
		 * Link the U_JOBPO Field to the Form Item.
		 */
		public void doLink_JOBPO(string sControlName){
			moLinker.doLinkDataToControl(_JOBPO, sControlName);
		}
		/**
		 * Link the U_JobRmD Field to the Form Item.
		 */
		public void doLink_JobRmD(string sControlName){
			moLinker.doLinkDataToControl(_JobRmD, sControlName);
		}
		/**
		 * Link the U_JobRmT Field to the Form Item.
		 */
		public void doLink_JobRmT(string sControlName){
			moLinker.doLinkDataToControl(_JobRmT, sControlName);
		}
		/**
		 * Link the U_JobTp Field to the Form Item.
		 */
		public void doLink_JobTp(string sControlName){
			moLinker.doLinkDataToControl(_JobTp, sControlName);
		}
		/**
		 * Link the U_Jrnl Field to the Form Item.
		 */
		public void doLink_Jrnl(string sControlName){
			moLinker.doLinkDataToControl(_Jrnl, sControlName);
		}
		/**
		 * Link the U_JStat Field to the Form Item.
		 */
		public void doLink_JStat(string sControlName){
			moLinker.doLinkDataToControl(_JStat, sControlName);
		}
		/**
		 * Link the U_LckPrc Field to the Form Item.
		 */
		public void doLink_LckPrc(string sControlName){
			moLinker.doLinkDataToControl(_LckPrc, sControlName);
		}
		/**
		 * Link the U_LiscCoCc Field to the Form Item.
		 */
		public void doLink_LiscCoCc(string sControlName){
			moLinker.doLinkDataToControl(_LiscCoCc, sControlName);
		}
		/**
		 * Link the U_LnkPBI Field to the Form Item.
		 */
		public void doLink_LnkPBI(string sControlName){
			moLinker.doLinkDataToControl(_LnkPBI, sControlName);
		}
		/**
		 * Link the U_LoadSht Field to the Form Item.
		 */
		public void doLink_LoadSht(string sControlName){
			moLinker.doLinkDataToControl(_LoadSht, sControlName);
		}
		/**
		 * Link the U_Lorry Field to the Form Item.
		 */
		public void doLink_Lorry(string sControlName){
			moLinker.doLinkDataToControl(_Lorry, sControlName);
		}
		/**
		 * Link the U_LorryCd Field to the Form Item.
		 */
		public void doLink_LorryCd(string sControlName){
			moLinker.doLinkDataToControl(_LorryCd, sControlName);
		}
		/**
		 * Link the U_LstPrice Field to the Form Item.
		 */
		public void doLink_LstPrice(string sControlName){
			moLinker.doLinkDataToControl(_LstPrice, sControlName);
		}
		/**
		 * Link the U_MANPRC Field to the Form Item.
		 */
		public void doLink_MANPRC(string sControlName){
			moLinker.doLinkDataToControl(_MANPRC, sControlName);
		}
		/**
		 * Link the U_MaximoNum Field to the Form Item.
		 */
		public void doLink_MaximoNum(string sControlName){
			moLinker.doLinkDataToControl(_MaximoNum, sControlName);
		}
		/**
		 * Link the U_MDChngd Field to the Form Item.
		 */
		public void doLink_MDChngd(string sControlName){
			moLinker.doLinkDataToControl(_MDChngd, sControlName);
		}
		/**
		 * Link the U_MnMargin Field to the Form Item.
		 */
		public void doLink_MnMargin(string sControlName){
			moLinker.doLinkDataToControl(_MnMargin, sControlName);
		}
		/**
		 * Link the U_MSDS Field to the Form Item.
		 */
		public void doLink_MSDS(string sControlName){
			moLinker.doLinkDataToControl(_MSDS, sControlName);
		}
		/**
		 * Link the U_Obligated Field to the Form Item.
		 */
		public void doLink_Obligated(string sControlName){
			moLinker.doLinkDataToControl(_Obligated, sControlName);
		}
		/**
		 * Link the U_OrdCost Field to the Form Item.
		 */
		public void doLink_OrdCost(string sControlName){
			moLinker.doLinkDataToControl(_OrdCost, sControlName);
		}
		/**
		 * Link the U_OrdTot Field to the Form Item.
		 */
		public void doLink_OrdTot(string sControlName){
			moLinker.doLinkDataToControl(_OrdTot, sControlName);
		}
		/**
		 * Link the U_OrdWgt Field to the Form Item.
		 */
		public void doLink_OrdWgt(string sControlName){
			moLinker.doLinkDataToControl(_OrdWgt, sControlName);
		}
		/**
		 * Link the U_Origin Field to the Form Item.
		 */
		public void doLink_Origin(string sControlName){
			moLinker.doLinkDataToControl(_Origin, sControlName);
		}
		/**
		 * Link the U_OTComments Field to the Form Item.
		 */
		public void doLink_OTComments(string sControlName){
			moLinker.doLinkDataToControl(_OTComments, sControlName);
		}
		/**
		 * Link the U_PaMuPu Field to the Form Item.
		 */
		public void doLink_PaMuPu(string sControlName){
			moLinker.doLinkDataToControl(_PaMuPu, sControlName);
		}
		/**
		 * Link the U_PARCPT Field to the Form Item.
		 */
		public void doLink_PARCPT(string sControlName){
			moLinker.doLinkDataToControl(_PARCPT, sControlName);
		}
		/**
		 * Link the U_PAUOMQt Field to the Form Item.
		 */
		public void doLink_PAUOMQt(string sControlName){
			moLinker.doLinkDataToControl(_PAUOMQt, sControlName);
		}
		/**
		 * Link the U_PayMeth Field to the Form Item.
		 */
		public void doLink_PayMeth(string sControlName){
			moLinker.doLinkDataToControl(_PayMeth, sControlName);
		}
		/**
		 * Link the U_PayStat Field to the Form Item.
		 */
		public void doLink_PayStat(string sControlName){
			moLinker.doLinkDataToControl(_PayStat, sControlName);
		}
		/**
		 * Link the U_PayTrm Field to the Form Item.
		 */
		public void doLink_PayTrm(string sControlName){
			moLinker.doLinkDataToControl(_PayTrm, sControlName);
		}
		/**
		 * Link the U_PCharge Field to the Form Item.
		 */
		public void doLink_PCharge(string sControlName){
			moLinker.doLinkDataToControl(_PCharge, sControlName);
		}
		/**
		 * Link the U_PCost Field to the Form Item.
		 */
		public void doLink_PCost(string sControlName){
			moLinker.doLinkDataToControl(_PCost, sControlName);
		}
		/**
		 * Link the U_PCTotal Field to the Form Item.
		 */
		public void doLink_PCTotal(string sControlName){
			moLinker.doLinkDataToControl(_PCTotal, sControlName);
		}
		/**
		 * Link the U_PLineID Field to the Form Item.
		 */
		public void doLink_PLineID(string sControlName){
			moLinker.doLinkDataToControl(_PLineID, sControlName);
		}
		/**
		 * Link the U_PrcLink Field to the Form Item.
		 */
		public void doLink_PrcLink(string sControlName){
			moLinker.doLinkDataToControl(_PrcLink, sControlName);
		}
		/**
		 * Link the U_PRdWgt Field to the Form Item.
		 */
		public void doLink_PRdWgt(string sControlName){
			moLinker.doLinkDataToControl(_PRdWgt, sControlName);
		}
		/**
		 * Link the U_Price Field to the Form Item.
		 */
		public void doLink_Price(string sControlName){
			moLinker.doLinkDataToControl(_Price, sControlName);
		}
		/**
		 * Link the U_ProCd Field to the Form Item.
		 */
		public void doLink_ProCd(string sControlName){
			moLinker.doLinkDataToControl(_ProCd, sControlName);
		}
		/**
		 * Link the U_ProGRPO Field to the Form Item.
		 */
		public void doLink_ProGRPO(string sControlName){
			moLinker.doLinkDataToControl(_ProGRPO, sControlName);
		}
		/**
		 * Link the U_PROINV Field to the Form Item.
		 */
		public void doLink_PROINV(string sControlName){
			moLinker.doLinkDataToControl(_PROINV, sControlName);
		}
		/**
		 * Link the U_ProNm Field to the Form Item.
		 */
		public void doLink_ProNm(string sControlName){
			moLinker.doLinkDataToControl(_ProNm, sControlName);
		}
		/**
		 * Link the U_ProPay Field to the Form Item.
		 */
		public void doLink_ProPay(string sControlName){
			moLinker.doLinkDataToControl(_ProPay, sControlName);
		}
		/**
		 * Link the U_ProPO Field to the Form Item.
		 */
		public void doLink_ProPO(string sControlName){
			moLinker.doLinkDataToControl(_ProPO, sControlName);
		}
		/**
		 * Link the U_ProRef Field to the Form Item.
		 */
		public void doLink_ProRef(string sControlName){
			moLinker.doLinkDataToControl(_ProRef, sControlName);
		}
		/**
		 * Link the U_ProUOM Field to the Form Item.
		 */
		public void doLink_ProUOM(string sControlName){
			moLinker.doLinkDataToControl(_ProUOM, sControlName);
		}
		/**
		 * Link the U_ProWgt Field to the Form Item.
		 */
		public void doLink_ProWgt(string sControlName){
			moLinker.doLinkDataToControl(_ProWgt, sControlName);
		}
		/**
		 * Link the U_PStat Field to the Form Item.
		 */
		public void doLink_PStat(string sControlName){
			moLinker.doLinkDataToControl(_PStat, sControlName);
		}
		/**
		 * Link the U_PUOM Field to the Form Item.
		 */
		public void doLink_PUOM(string sControlName){
			moLinker.doLinkDataToControl(_PUOM, sControlName);
		}
		/**
		 * Link the U_PurcCoCc Field to the Form Item.
		 */
		public void doLink_PurcCoCc(string sControlName){
			moLinker.doLinkDataToControl(_PurcCoCc, sControlName);
		}
		/**
		 * Link the U_Quantity Field to the Form Item.
		 */
		public void doLink_Quantity(string sControlName){
			moLinker.doLinkDataToControl(_Quantity, sControlName);
		}
		/**
		 * Link the U_RDate Field to the Form Item.
		 */
		public void doLink_RDate(string sControlName){
			moLinker.doLinkDataToControl(_RDate, sControlName);
		}
		/**
		 * Link the U_RdWgt Field to the Form Item.
		 */
		public void doLink_RdWgt(string sControlName){
			moLinker.doLinkDataToControl(_RdWgt, sControlName);
		}
		/**
		 * Link the U_Rebate Field to the Form Item.
		 */
		public void doLink_Rebate(string sControlName){
			moLinker.doLinkDataToControl(_Rebate, sControlName);
		}
		/**
		 * Link the U_RemCnt Field to the Form Item.
		 */
		public void doLink_RemCnt(string sControlName){
			moLinker.doLinkDataToControl(_RemCnt, sControlName);
		}
		/**
		 * Link the U_RemNot Field to the Form Item.
		 */
		public void doLink_RemNot(string sControlName){
			moLinker.doLinkDataToControl(_RemNot, sControlName);
		}
		/**
		 * Link the U_ReqArch Field to the Form Item.
		 */
		public void doLink_ReqArch(string sControlName){
			moLinker.doLinkDataToControl(_ReqArch, sControlName);
		}
		/**
		 * Link the U_RouteCd Field to the Form Item.
		 */
		public void doLink_RouteCd(string sControlName){
			moLinker.doLinkDataToControl(_RouteCd, sControlName);
		}
		/**
		 * Link the U_RowSta Field to the Form Item.
		 */
		public void doLink_RowSta(string sControlName){
			moLinker.doLinkDataToControl(_RowSta, sControlName);
		}
		/**
		 * Link the U_RTime Field to the Form Item.
		 */
		public void doLink_RTime(string sControlName){
			moLinker.doLinkDataToControl(_RTime, sControlName);
		}
		/**
		 * Link the U_RTimeT Field to the Form Item.
		 */
		public void doLink_RTimeT(string sControlName){
			moLinker.doLinkDataToControl(_RTimeT, sControlName);
		}
		/**
		 * Link the U_SAddress Field to the Form Item.
		 */
		public void doLink_SAddress(string sControlName){
			moLinker.doLinkDataToControl(_SAddress, sControlName);
		}
		/**
		 * Link the U_SAddrsLN Field to the Form Item.
		 */
		public void doLink_SAddrsLN(string sControlName){
			moLinker.doLinkDataToControl(_SAddrsLN, sControlName);
		}
		/**
		 * Link the U_SAINV Field to the Form Item.
		 */
		public void doLink_SAINV(string sControlName){
			moLinker.doLinkDataToControl(_SAINV, sControlName);
		}
		/**
		 * Link the U_Sample Field to the Form Item.
		 */
		public void doLink_Sample(string sControlName){
			moLinker.doLinkDataToControl(_Sample, sControlName);
		}
		/**
		 * Link the U_SampleRef Field to the Form Item.
		 */
		public void doLink_SampleRef(string sControlName){
			moLinker.doLinkDataToControl(_SampleRef, sControlName);
		}
		/**
		 * Link the U_SampStatus Field to the Form Item.
		 */
		public void doLink_SampStatus(string sControlName){
			moLinker.doLinkDataToControl(_SampStatus, sControlName);
		}
		/**
		 * Link the U_SAORD Field to the Form Item.
		 */
		public void doLink_SAORD(string sControlName){
			moLinker.doLinkDataToControl(_SAORD, sControlName);
		}
		/**
		 * Link the U_Sched Field to the Form Item.
		 */
		public void doLink_Sched(string sControlName){
			moLinker.doLinkDataToControl(_Sched, sControlName);
		}
		/**
		 * Link the U_SCngNm Field to the Form Item.
		 */
		public void doLink_SCngNm(string sControlName){
			moLinker.doLinkDataToControl(_SCngNm, sControlName);
		}
		/**
		 * Link the U_SealNr Field to the Form Item.
		 */
		public void doLink_SealNr(string sControlName){
			moLinker.doLinkDataToControl(_SealNr, sControlName);
		}
		/**
		 * Link the U_SemSolid Field to the Form Item.
		 */
		public void doLink_SemSolid(string sControlName){
			moLinker.doLinkDataToControl(_SemSolid, sControlName);
		}
		/**
		 * Link the U_Ser1 Field to the Form Item.
		 */
		public void doLink_Ser1(string sControlName){
			moLinker.doLinkDataToControl(_Ser1, sControlName);
		}
		/**
		 * Link the U_Ser2 Field to the Form Item.
		 */
		public void doLink_Ser2(string sControlName){
			moLinker.doLinkDataToControl(_Ser2, sControlName);
		}
		/**
		 * Link the U_Serial Field to the Form Item.
		 */
		public void doLink_Serial(string sControlName){
			moLinker.doLinkDataToControl(_Serial, sControlName);
		}
		/**
		 * Link the U_SerialC Field to the Form Item.
		 */
		public void doLink_SerialC(string sControlName){
			moLinker.doLinkDataToControl(_SerialC, sControlName);
		}
		/**
		 * Link the U_SiteRef Field to the Form Item.
		 */
		public void doLink_SiteRef(string sControlName){
			moLinker.doLinkDataToControl(_SiteRef, sControlName);
		}
		/**
		 * Link the U_SLicCh Field to the Form Item.
		 */
		public void doLink_SLicCh(string sControlName){
			moLinker.doLinkDataToControl(_SLicCh, sControlName);
		}
		/**
		 * Link the U_SLicCQt Field to the Form Item.
		 */
		public void doLink_SLicCQt(string sControlName){
			moLinker.doLinkDataToControl(_SLicCQt, sControlName);
		}
		/**
		 * Link the U_SLicCst Field to the Form Item.
		 */
		public void doLink_SLicCst(string sControlName){
			moLinker.doLinkDataToControl(_SLicCst, sControlName);
		}
		/**
		 * Link the U_SLicCTo Field to the Form Item.
		 */
		public void doLink_SLicCTo(string sControlName){
			moLinker.doLinkDataToControl(_SLicCTo, sControlName);
		}
		/**
		 * Link the U_SLicExp Field to the Form Item.
		 */
		public void doLink_SLicExp(string sControlName){
			moLinker.doLinkDataToControl(_SLicExp, sControlName);
		}
		/**
		 * Link the U_SLicNm Field to the Form Item.
		 */
		public void doLink_SLicNm(string sControlName){
			moLinker.doLinkDataToControl(_SLicNm, sControlName);
		}
		/**
		 * Link the U_SLicNr Field to the Form Item.
		 */
		public void doLink_SLicNr(string sControlName){
			moLinker.doLinkDataToControl(_SLicNr, sControlName);
		}
		/**
		 * Link the U_SLicSp Field to the Form Item.
		 */
		public void doLink_SLicSp(string sControlName){
			moLinker.doLinkDataToControl(_SLicSp, sControlName);
		}
		/**
		 * Link the U_SLPO Field to the Form Item.
		 */
		public void doLink_SLPO(string sControlName){
			moLinker.doLinkDataToControl(_SLPO, sControlName);
		}
		/**
		 * Link the U_SODlvNot Field to the Form Item.
		 */
		public void doLink_SODlvNot(string sControlName){
			moLinker.doLinkDataToControl(_SODlvNot, sControlName);
		}
		/**
		 * Link the U_Sort Field to the Form Item.
		 */
		public void doLink_Sort(string sControlName){
			moLinker.doLinkDataToControl(_Sort, sControlName);
		}
		/**
		 * Link the U_Status Field to the Form Item.
		 */
		public void doLink_Status(string sControlName){
			moLinker.doLinkDataToControl(_Status, sControlName);
		}
		/**
		 * Link the U_SupRef Field to the Form Item.
		 */
		public void doLink_SupRef(string sControlName){
			moLinker.doLinkDataToControl(_SupRef, sControlName);
		}
		/**
		 * Link the U_TAddChrg Field to the Form Item.
		 */
		public void doLink_TAddChrg(string sControlName){
			moLinker.doLinkDataToControl(_TAddChrg, sControlName);
		}
		/**
		 * Link the U_TAddChVat Field to the Form Item.
		 */
		public void doLink_TAddChVat(string sControlName){
			moLinker.doLinkDataToControl(_TAddChVat, sControlName);
		}
		/**
		 * Link the U_TAddCost Field to the Form Item.
		 */
		public void doLink_TAddCost(string sControlName){
			moLinker.doLinkDataToControl(_TAddCost, sControlName);
		}
		/**
		 * Link the U_TAddCsVat Field to the Form Item.
		 */
		public void doLink_TAddCsVat(string sControlName){
			moLinker.doLinkDataToControl(_TAddCsVat, sControlName);
		}
		/**
		 * Link the U_TAUOMQt Field to the Form Item.
		 */
		public void doLink_TAUOMQt(string sControlName){
			moLinker.doLinkDataToControl(_TAUOMQt, sControlName);
		}
		/**
		 * Link the U_TaxAmt Field to the Form Item.
		 */
		public void doLink_TaxAmt(string sControlName){
			moLinker.doLinkDataToControl(_TaxAmt, sControlName);
		}
		/**
		 * Link the U_TCCN Field to the Form Item.
		 */
		public void doLink_TCCN(string sControlName){
			moLinker.doLinkDataToControl(_TCCN, sControlName);
		}
		/**
		 * Link the U_TCharge Field to the Form Item.
		 */
		public void doLink_TCharge(string sControlName){
			moLinker.doLinkDataToControl(_TCharge, sControlName);
		}
		/**
		 * Link the U_TChrgVtGrp Field to the Form Item.
		 */
		public void doLink_TChrgVtGrp(string sControlName){
			moLinker.doLinkDataToControl(_TChrgVtGrp, sControlName);
		}
		/**
		 * Link the U_TChrgVtRt Field to the Form Item.
		 */
		public void doLink_TChrgVtRt(string sControlName){
			moLinker.doLinkDataToControl(_TChrgVtRt, sControlName);
		}
		/**
		 * Link the U_TCostVtGrp Field to the Form Item.
		 */
		public void doLink_TCostVtGrp(string sControlName){
			moLinker.doLinkDataToControl(_TCostVtGrp, sControlName);
		}
		/**
		 * Link the U_TCostVtRt Field to the Form Item.
		 */
		public void doLink_TCostVtRt(string sControlName){
			moLinker.doLinkDataToControl(_TCostVtRt, sControlName);
		}
		/**
		 * Link the U_TCTotal Field to the Form Item.
		 */
		public void doLink_TCTotal(string sControlName){
			moLinker.doLinkDataToControl(_TCTotal, sControlName);
		}
		/**
		 * Link the U_TFSMov Field to the Form Item.
		 */
		public void doLink_TFSMov(string sControlName){
			moLinker.doLinkDataToControl(_TFSMov, sControlName);
		}
		/**
		 * Link the U_TFSNum Field to the Form Item.
		 */
		public void doLink_TFSNum(string sControlName){
			moLinker.doLinkDataToControl(_TFSNum, sControlName);
		}
		/**
		 * Link the U_TFSReq Field to the Form Item.
		 */
		public void doLink_TFSReq(string sControlName){
			moLinker.doLinkDataToControl(_TFSReq, sControlName);
		}
		/**
		 * Link the U_Tip Field to the Form Item.
		 */
		public void doLink_Tip(string sControlName){
			moLinker.doLinkDataToControl(_Tip, sControlName);
		}
		/**
		 * Link the U_TipCost Field to the Form Item.
		 */
		public void doLink_TipCost(string sControlName){
			moLinker.doLinkDataToControl(_TipCost, sControlName);
		}
		/**
		 * Link the U_TipNm Field to the Form Item.
		 */
		public void doLink_TipNm(string sControlName){
			moLinker.doLinkDataToControl(_TipNm, sControlName);
		}
		/**
		 * Link the U_TIPPO Field to the Form Item.
		 */
		public void doLink_TIPPO(string sControlName){
			moLinker.doLinkDataToControl(_TIPPO, sControlName);
		}
		/**
		 * Link the U_TipTot Field to the Form Item.
		 */
		public void doLink_TipTot(string sControlName){
			moLinker.doLinkDataToControl(_TipTot, sControlName);
		}
		/**
		 * Link the U_TipWgt Field to the Form Item.
		 */
		public void doLink_TipWgt(string sControlName){
			moLinker.doLinkDataToControl(_TipWgt, sControlName);
		}
		/**
		 * Link the U_Total Field to the Form Item.
		 */
		public void doLink_Total(string sControlName){
			moLinker.doLinkDataToControl(_Total, sControlName);
		}
		/**
		 * Link the U_TPCN Field to the Form Item.
		 */
		public void doLink_TPCN(string sControlName){
			moLinker.doLinkDataToControl(_TPCN, sControlName);
		}
		/**
		 * Link the U_TRdWgt Field to the Form Item.
		 */
		public void doLink_TRdWgt(string sControlName){
			moLinker.doLinkDataToControl(_TRdWgt, sControlName);
		}
		/**
		 * Link the U_TRLNM Field to the Form Item.
		 */
		public void doLink_TRLNM(string sControlName){
			moLinker.doLinkDataToControl(_TRLNM, sControlName);
		}
		/**
		 * Link the U_TRLReg Field to the Form Item.
		 */
		public void doLink_TRLReg(string sControlName){
			moLinker.doLinkDataToControl(_TRLReg, sControlName);
		}
		/**
		 * Link the U_TrnCode Field to the Form Item.
		 */
		public void doLink_TrnCode(string sControlName){
			moLinker.doLinkDataToControl(_TrnCode, sControlName);
		}
		/**
		 * Link the U_TypPTreat Field to the Form Item.
		 */
		public void doLink_TypPTreat(string sControlName){
			moLinker.doLinkDataToControl(_TypPTreat, sControlName);
		}
		/**
		 * Link the U_TZone Field to the Form Item.
		 */
		public void doLink_TZone(string sControlName){
			moLinker.doLinkDataToControl(_TZone, sControlName);
		}
		/**
		 * Link the U_UOM Field to the Form Item.
		 */
		public void doLink_UOM(string sControlName){
			moLinker.doLinkDataToControl(_UOM, sControlName);
		}
		/**
		 * Link the U_USched Field to the Form Item.
		 */
		public void doLink_USched(string sControlName){
			moLinker.doLinkDataToControl(_USched, sControlName);
		}
		/**
		 * Link the U_UseBPWgt Field to the Form Item.
		 */
		public void doLink_UseBPWgt(string sControlName){
			moLinker.doLinkDataToControl(_UseBPWgt, sControlName);
		}
		/**
		 * Link the U_User Field to the Form Item.
		 */
		public void doLink_User(string sControlName){
			moLinker.doLinkDataToControl(_User, sControlName);
		}
		/**
		 * Link the U_UseWgt Field to the Form Item.
		 */
		public void doLink_UseWgt(string sControlName){
			moLinker.doLinkDataToControl(_UseWgt, sControlName);
		}
		/**
		 * Link the U_ValDed Field to the Form Item.
		 */
		public void doLink_ValDed(string sControlName){
			moLinker.doLinkDataToControl(_ValDed, sControlName);
		}
		/**
		 * Link the U_VehTyp Field to the Form Item.
		 */
		public void doLink_VehTyp(string sControlName){
			moLinker.doLinkDataToControl(_VehTyp, sControlName);
		}
		/**
		 * Link the U_VtCostAmt Field to the Form Item.
		 */
		public void doLink_VtCostAmt(string sControlName){
			moLinker.doLinkDataToControl(_VtCostAmt, sControlName);
		}
		/**
		 * Link the U_WasCd Field to the Form Item.
		 */
		public void doLink_WasCd(string sControlName){
			moLinker.doLinkDataToControl(_WasCd, sControlName);
		}
		/**
		 * Link the U_WasDsc Field to the Form Item.
		 */
		public void doLink_WasDsc(string sControlName){
			moLinker.doLinkDataToControl(_WasDsc, sControlName);
		}
		/**
		 * Link the U_WasFDsc Field to the Form Item.
		 */
		public void doLink_WasFDsc(string sControlName){
			moLinker.doLinkDataToControl(_WasFDsc, sControlName);
		}
		/**
		 * Link the U_WASLIC Field to the Form Item.
		 */
		public void doLink_WASLIC(string sControlName){
			moLinker.doLinkDataToControl(_WASLIC, sControlName);
		}
		/**
		 * Link the U_WastTNN Field to the Form Item.
		 */
		public void doLink_WastTNN(string sControlName){
			moLinker.doLinkDataToControl(_WastTNN, sControlName);
		}
		/**
		 * Link the U_WDt1 Field to the Form Item.
		 */
		public void doLink_WDt1(string sControlName){
			moLinker.doLinkDataToControl(_WDt1, sControlName);
		}
		/**
		 * Link the U_WDt2 Field to the Form Item.
		 */
		public void doLink_WDt2(string sControlName){
			moLinker.doLinkDataToControl(_WDt2, sControlName);
		}
		/**
		 * Link the U_Wei1 Field to the Form Item.
		 */
		public void doLink_Wei1(string sControlName){
			moLinker.doLinkDataToControl(_Wei1, sControlName);
		}
		/**
		 * Link the U_Wei2 Field to the Form Item.
		 */
		public void doLink_Wei2(string sControlName){
			moLinker.doLinkDataToControl(_Wei2, sControlName);
		}
		/**
		 * Link the U_Weight Field to the Form Item.
		 */
		public void doLink_Weight(string sControlName){
			moLinker.doLinkDataToControl(_Weight, sControlName);
		}
		/**
		 * Link the U_WgtDed Field to the Form Item.
		 */
		public void doLink_WgtDed(string sControlName){
			moLinker.doLinkDataToControl(_WgtDed, sControlName);
		}
		/**
		 * Link the U_WHTRNF Field to the Form Item.
		 */
		public void doLink_WHTRNF(string sControlName){
			moLinker.doLinkDataToControl(_WHTRNF, sControlName);
		}
		/**
		 * Link the U_WOHID Field to the Form Item.
		 */
		public void doLink_WOHID(string sControlName){
			moLinker.doLinkDataToControl(_WOHID, sControlName);
		}
		/**
		 * Link the U_WORID Field to the Form Item.
		 */
		public void doLink_WORID(string sControlName){
			moLinker.doLinkDataToControl(_WORID, sControlName);
		}
		/**
		 * Link the U_WROrd Field to the Form Item.
		 */
		public void doLink_WROrd(string sControlName){
			moLinker.doLinkDataToControl(_WROrd, sControlName);
		}
		/**
		 * Link the U_WROrd2 Field to the Form Item.
		 */
		public void doLink_WROrd2(string sControlName){
			moLinker.doLinkDataToControl(_WROrd2, sControlName);
		}
		/**
		 * Link the U_WRRow Field to the Form Item.
		 */
		public void doLink_WRRow(string sControlName){
			moLinker.doLinkDataToControl(_WRRow, sControlName);
		}
		/**
		 * Link the U_WRRow2 Field to the Form Item.
		 */
		public void doLink_WRRow2(string sControlName){
			moLinker.doLinkDataToControl(_WRRow2, sControlName);
		}
		/**
		 * Link the U_WstGpCd Field to the Form Item.
		 */
		public void doLink_WstGpCd(string sControlName){
			moLinker.doLinkDataToControl(_WstGpCd, sControlName);
		}
		/**
		 * Link the U_WstGpNm Field to the Form Item.
		 */
		public void doLink_WstGpNm(string sControlName){
			moLinker.doLinkDataToControl(_WstGpNm, sControlName);
		}
        /**
          * Link the U_WhsCode Field to the Form Item.
          */
        public void doLink_WhsCode(string sControlName) {
            moLinker.doLinkDataToControl(_WhsCode, sControlName);
        }
#endregion

	}
}
