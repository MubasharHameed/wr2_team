/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 13/12/2016 16:31:23
 * DBObject user template
 *
 */

using System;
//using System.Data;
//using System.Collections;
//using System.Data.SqlClient;
using com.idh.dbObjects.numbers;
using com.idh.bridge;
using com.idh.bridge.lookups;
using System.Net;
using System.IO;
//using Newtonsoft.Json;
//using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_WORUHD : com.isb.enq.dbObjects.Base.IDH_WORUHD {


        public enum en_BatchProcessStatus {
            NotSent = 0,
            SentToReview = 1,
            ReadyForReview = 11,
            ReadyForProcessByService = 2,
            Processed = 3,

        };

        public IDH_WORUHD() : base() {
            msAutoNumKey = "SEQWORBD";
        }

        public IDH_WORUHD(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm) : base(oIDHForm, oForm) {
            msAutoNumKey = "SEQWORBD";
        }

        #region "Validate Rows"
        public bool doValidateRows() {
            try {
                string sURL = "", sCompanyID = "", sCountryCode = "";
                sURL = Config.INSTANCE.getParameter("WMWEBURL");
                sCompanyID = Config.INSTANCE.getParameter("WMCMCODE");
                sCountryCode = Config.INSTANCE.getParameter("WMCNCODE");

                // bool bRet = true;
                int iRecs = 0;
                iRecs = getData(IDH_WORUHD._ProcSts + " = " + ((int)en_BatchProcessStatus.ReadyForReview).ToString() + " And isnull(" + IDH_WORUHD._BthStatus + ",'1')!='0' And isnull(" + IDH_WORUHD._VldStatus + ",'')='' ", "");
                DataHandler.INSTANCE.doInfo("MassWOR.doProcessBatch", "Info", ">>> No. of rows found to process=" + iRecs.ToString());
                if (iRecs == 0)
                    return true;
                first();
                while (next()) {
                    //bRet = true;
                    if (U_ProcSts == ((int)en_BatchProcessStatus.ReadyForReview).ToString()) {
                        doValidateRow(sURL, sCompanyID, sCountryCode);
                    }
                }
                return true;
            } catch (Exception ex) {
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_WORUHD.doValidateRows" });
                return false;
            }

        }

        //public async Task<HttpResponseMessage> PostResult(string url, HttpContent resultObject) {
        //    using (var client = new HttpClient()) {
        //        HttpResponseMessage response = new HttpResponseMessage();
        //        try {
        //            response = await client.PostAsync(url, resultObject);
        //        } catch (Exception ex) {
        //            throw ex;
        //          }
        //        return response;
        //    }
        //}
        private bool doUpdateValidationToWasteManager(string sURL, string sCompanyID, string sCountryCode, bool bResult, string _Code, string sReason) {
            try {

                if (!string.IsNullOrWhiteSpace(U_AddedBy) && U_AddedBy == "WR1") {
                    //do update status only
                } else if (string.IsNullOrWhiteSpace(sURL) || string.IsNullOrWhiteSpace(sCompanyID) || string.IsNullOrWhiteSpace(sCountryCode)) {
                    //no config for web service found so return
                    DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Mass WOR Updater Code=" + Code + ", Web service configuration missing"));
                    return true;
                } else {

                    // //      sURL= @"https://isbsmartsolutions-dev.outsystemsenterprise.com/UploadersIntegration/rest/MassUploaders/WORValidation?";
                    // string sJSONToSend = "TenantId=" + sCompanyID + "&CountryCode=" + sCountryCode + "&BatchNumber=" + U_BatchNum + "&DetailCode=" + Code + "&IsSuccess=" + bResult.ToString() + "&ErrorMsg=" + System.Web.HttpUtility.UrlEncode(sReason) + "";
                    // var request = (HttpWebRequest)WebRequest.Create(sURL + sJSONToSend);
                    // var response = (HttpWebResponse)request.GetResponse();
                    // var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    //// response.GetResponseHeader()
                    // if (responseString == null || responseString.ToString().Trim() != string.Empty) {
                    //     DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Mass WOR Updater Code=" + Code + ", failed to update status on SW server.Response=" + responseString == null ? "Null" : responseString.ToString()));
                    //     return false;
                    // }
                    //HttpContent ocontect;// = new HttpContent();

                    //HttpResponseMessage oresponse =await PostResult(sURL, ocontect);
                    //                    HttpClient client = new HttpClient();
                    //                    var values = new Dictionary<string, string>
                    //            {
                    //   { "thing1", "hello" },
                    //   { "thing2", "world" }
                    //};

                    //                    var content = new FormUrlEncodedContent(values);

                    //                    var response = await client.PostAsync(sURL, content);

                    //                    var responseString = await response.Content.ReadAsStringAsync();
                    string sJSONToSend = "TenantId=" + sCompanyID + "&CountryCode=" + sCountryCode + "&BatchNumber=" + U_BatchNum + "&DetailCode=" + Code + "&IsSuccess=" + bResult.ToString() + "&ErrorMsg=" + System.Web.HttpUtility.UrlEncode(sReason) + "";
                    bool bWebResponse = false;
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(sURL + "?" + sJSONToSend);
                    httpWebRequest.ContentType = "application/json; charset=UTF-8";
                    httpWebRequest.Method = "POST";
                    //Temp out as I kept on getting a compiler error
                    //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream())) {
                    //    string json = JsonConvert.SerializeObject(new {
                    //        TenantId = Convert.ToInt32(sCompanyID),
                    //        CountryCode = Convert.ToInt32(sCountryCode),
                    //        BatchNumber = U_BatchNum,
                    //        DetailCode = Code,
                    //        IsSuccess = bResult,
                    //        ErrorMsg = sReason
                    //    });
                    //    streamWriter.Write(json);
                    //}
                    try {
                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream())) {
                            var responseString = streamReader.ReadToEnd();
                            bWebResponse = Convert.ToBoolean(httpResponse.GetResponseHeader("Success"));
                            
                            if (responseString != null && responseString.ToString().Trim() == "No Record Found") {
                                DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Mass WOR Updater Code=" + Code + ",Batch#"+ U_BatchNum +"WOR#"+ U_WOR +", failed to update status on server.Response=" + responseString == null ? "Null" : responseString.ToString()));
                                sReason += "," + responseString.ToString();
                                bResult = false;
                                U_ProcSts = Convert.ToString((int)en_BatchProcessStatus.ReadyForReview);
                            } else if (responseString == null || responseString.ToString().Trim() != string.Empty) {
                                DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Mass WOR Updater Code=" + Code + "Batch#" + U_BatchNum + "WOR#" + U_WOR + ", failed to update status on server.Response=" + responseString == null ? "Null" : responseString.ToString()));
                                bResult = false;
                                sReason += "," + responseString.ToString();
                                U_ProcSts = Convert.ToString((int)en_BatchProcessStatus.ReadyForReview);
                                // return false;
                            }
                        }
                    } catch (WebException ex) {
                        using (WebResponse response = ex.Response) {
                            var httpResponse = (HttpWebResponse)response;

                            using (Stream data = response.GetResponseStream()) {
                                StreamReader sr = new StreamReader(data);
                                throw new Exception(sr.ReadToEnd());
                            }
                        }
                    }

                }
                U_VldStatus = bResult ? "1" : "0";
                U_VdReason = sReason;
                doUpdateDataRow();
                return true;
            } catch (Exception ex) {
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_WORUHD.doUpdateValidationToWasteManager" });
                return false;
            }
        }

        private bool doValidateRow(string sURL, string sCompanyID, string sCountryCode) {
            try {
               
            //   bool bValidated = true;
             //   bool doSetUpdateMode = false;

                if (string.IsNullOrWhiteSpace(Code))
                    return true; ;
                IDH_JOBSHD oWOR = new IDH_JOBSHD();
                oWOR.UnLockTable = true;
                bool bRowValidated = true;

                if (string.IsNullOrWhiteSpace(U_WOR)) {
                    doUpdateValidationToWasteManager(sURL, sCompanyID, sCountryCode, false, Code, "WOR Code missing.Code#" + Code);
                    return false;
                }
                if (!string.IsNullOrWhiteSpace(U_UOM) && !Config.INSTANCE.isValidUOM(U_UOM)) {
                    doUpdateValidationToWasteManager(sURL, sCompanyID, sCountryCode, false, Code, "Invalid UOM.Code#" + Code);
                    bRowValidated = false;
                } else {
                    if (!string.IsNullOrWhiteSpace(U_WOR) && !oWOR.getByKey(U_WOR)) {
                        doUpdateValidationToWasteManager(sURL, sCompanyID, sCountryCode, false, Code, "Failed to load WOR#" + U_WOR + ".");
                        bRowValidated = false;
                    } else if (!string.IsNullOrWhiteSpace(U_WOR) && oWOR.Code != null && oWOR.Code == U_WOR) {
                        if (!oWOR.doCheckCanEdit(false)) {
                            doUpdateValidationToWasteManager(sURL, sCompanyID, sCountryCode, false, Code, "WOR#" + oWOR.Code + " already billed, " + Code);
                            bRowValidated = false;
                        } else if (!string.IsNullOrWhiteSpace(U_JbTypeDs) &&
                                  Config.INSTANCE.getValueFromTablebyKey("[@IDH_JOBTYPE]", "Code", "U_JobTp", com.idh.utils.Formatter.doFixForSQL(U_JbTypeDs)) == null) {
                            doUpdateValidationToWasteManager(sURL, sCompanyID, sCountryCode, false, Code, "Invalid Job Type '" + U_JbTypeDs + "', " + Code);
                            bRowValidated = false;
                        }
                    }
                }

                if (!bRowValidated) {
                    DataHandler.INSTANCE.doUserError("Failed to validate WOR Updater row " + Code + ".Batch# "+ U_BatchNum +", WOR# "+ U_WOR +". Please review the errors.");

                } else {
                    DataHandler.INSTANCE.doInfo("WOR Updater row " + Code + " validated.Batch# " + U_BatchNum + ", WOR# " + U_WOR + ".");
                    U_ProcSts =Convert.ToString( (int)en_BatchProcessStatus.ReadyForProcessByService);
                    doUpdateValidationToWasteManager(sURL, sCompanyID, sCountryCode, true, Code, "");
                    //U_VldStatus = "1";
                //    doUpdateDataRow();
                }
              
                return bRowValidated;
            } catch (Exception ex) {
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_WORUHD.doValidateRow" });
                doUpdateValidationToWasteManager(sURL, sCompanyID, sCountryCode, false, Code, "Exception in Validation: " + ex.Message + ".Please contact system admin");
                return false;
            } finally {
                // return true;
            }
            return true;
        }
        #endregion
        #region "Process Batch"
        public bool doProcessBatch(string sProcessParm) {
            try {
                bool bRet = true;
                int iRecs = 0;
                if (string.IsNullOrWhiteSpace( sProcessParm ))
                iRecs = getData(IDH_WORUHD._ProcSts + " = " + ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString() + " And isnull(" + IDH_WORUHD._AddedBy + ",'')='' And isnull(" + IDH_WORUHD._BthStatus + ",'1')!='0'", "");
                else
                iRecs = getData(IDH_WORUHD._ProcSts + " = " + ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString() + " And isnull(" + IDH_WORUHD._AddedBy + ",'')!='' And isnull(" + IDH_WORUHD._BthStatus + ",'1')!='0'", "");
                DataHandler.INSTANCE.doInfo("MassWOR.doProcessBatch", "Info", ">>> No. of rows found to process=" + iRecs.ToString());
                if (iRecs == 0)
                    return true;
                first();
                while (next()) {
                    bRet = true;
                        if (U_ProcSts == ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString()) {
                            bRet = doProcessBatchItem();
                        }
                        //  U_ProcSts = ((int)en_BatchProcessStatus.Processed).ToString();
                }
                //doRecallBookmark();
                return true;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_WORUHD.doProcessBatch" });
                return false;
            }
        }
        public bool doProcessBatch(bool SendToServiceForProcess) {
            try {
                bool bRet = true;
                doBookmark();
                first();
                while (next()) {
                    bRet = true;
                    if (!SendToServiceForProcess) {
                        if (U_ProcSts == ((int)en_BatchProcessStatus.SentToReview).ToString() || U_ProcSts == ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString()) {
                            bRet = doProcessBatchItem();
                        }
                        //  U_ProcSts = ((int)en_BatchProcessStatus.Processed).ToString();
                    } else if (U_ProcSts == ((int)en_BatchProcessStatus.SentToReview).ToString() && U_ProcSts != ((int)en_BatchProcessStatus.Processed).ToString()) {
                        U_ProcSts = ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString();
                        bRet = doProcessData();
                        if (bRet == false) {
                            return false;
                        }
                    }
                }
                doRecallBookmark();
                return bRet;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_WORUHD.doProcessBatch" });
                return false;
            }
        }
        public bool doProcessBatchItem(bool SendToServiceForProcess) {
            try {
                bool bRet = true;
                //IDH_JOBENTR oWOH = new IDH_JOBENTR();
                //oWOH.UnLockTable = true;
                if (!SendToServiceForProcess) {
                        if (U_ProcSts == ((int)en_BatchProcessStatus.SentToReview).ToString() || U_ProcSts == ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString()) {
                            //IDH_PBI oPBI = null;
                            bRet = doProcessBatchItem();
                        }
                        //else if (bRet && !SendToServiceForProcess) {
                        //    U_ProcSts = ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString();
                        //    bRet = doUpdateDataRow();
                        //}
                } else {
                    U_ProcSts = ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString();
                    bRet = doUpdateDataRow();
                }
                return bRet;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_WORUHD.doProcessBatchItem" });
                return false;
            }
        }

        public bool doProcessBatchItem() {
            bool bRet = true;
            try {
              //  string sWOHNumbers = "";
                return doCreateWOR( Code);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_WORUHD.doProcessBatchItem" });
                return false;
            }
            return bRet;
        }

        #endregion
        #region Create WO
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sWOHNums"></param>
        /// <param name="sBatchDetailCode"></param>
        /// <returns></returns>
        public static bool doCreateWOR(string sBatchDetailCode) {
            // <param name="sOption">One= One WOH, Multiple=Each row will have its own WOH</param>
            bool bContinue = true;
            try {

                IDH_WORUHD _moBatchDetail = new IDH_WORUHD();
                _moBatchDetail.UnLockTable = true;
                if (!_moBatchDetail.getByKey(sBatchDetailCode)) {
                    DataHandler.INSTANCE.doInfo("Failed to load Batch RowNum#" + sBatchDetailCode);
                }
                DataHandler.INSTANCE.doInfo("Processing Batch#" + _moBatchDetail.U_BatchNum + ";RowNum#" + _moBatchDetail.Code);



                string sTime = com.idh.utils.Dates.doTimeToNumStr();
                IDH_JOBSHD moWOR = new IDH_JOBSHD();
                if (!string.IsNullOrWhiteSpace(_moBatchDetail.U_WOR)) {
                    moWOR.UnLockTable = true;
                    moWOR.MustLoadChildren = true;
                    if (!moWOR.getByKey(_moBatchDetail.U_WOR)) {
                        DataHandler.INSTANCE.doInfo("Failed to load WOR#" + _moBatchDetail.U_WOR + " in Batch#" + _moBatchDetail.U_BatchNum + ", RowNum#" + _moBatchDetail.Code);
                        return false;
                    } else {
                        if (!moWOR.doCheckCanEdit()) {
                            DataHandler.INSTANCE.doInfo("Billed WOR cannot be update. WOR#" + _moBatchDetail.U_WOR + " in Batch#" + _moBatchDetail.U_BatchNum + ", RowNum#" + _moBatchDetail.Code);
                            return false;
                        }
                    }
                }
                if (!DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.StartTransaction();
                moWOR.BlockChangeNotif = true;
                if (_moBatchDetail.U_RDate != null) {
                    if (_moBatchDetail.U_RDate.Year == 1999)
                        moWOR.U_RDate = Convert.ToDateTime("1899/01/01");
                    else if (_moBatchDetail.U_RDate.Year > 1999)
                        moWOR.U_RDate = _moBatchDetail.U_RDate;
                }


                if (_moBatchDetail.U_SDate != null) {
                    if (_moBatchDetail.U_SDate.Year == 1999)
                        moWOR.U_ASDate = Convert.ToDateTime("1899/01/01");
                    else if (_moBatchDetail.U_SDate.Year > 1999)
                        moWOR.U_ASDate = _moBatchDetail.U_SDate;
                }

                if (_moBatchDetail.U_EDate != null) {
                    if (_moBatchDetail.U_EDate.Year == 1999)
                        moWOR.U_AEDate = Convert.ToDateTime("1899/01/01");
                    else if (_moBatchDetail.U_EDate.Year > 1999)
                        moWOR.U_AEDate = _moBatchDetail.U_EDate;
                }


                if (!string.IsNullOrWhiteSpace(_moBatchDetail.U_JbTypeDs))
                    moWOR.U_JobTp= _moBatchDetail.U_JbTypeDs;
                

                if (!string.IsNullOrWhiteSpace(_moBatchDetail.U_Cancelled) && _moBatchDetail.U_Cancelled.Equals("#Remove", StringComparison.OrdinalIgnoreCase))
                    moWOR.U_Driver = "";
                else if (!string.IsNullOrWhiteSpace(_moBatchDetail.U_Cancelled))
                    moWOR.U_Driver = _moBatchDetail.U_Cancelled.Trim().ToUpper();

                if (!string.IsNullOrWhiteSpace(_moBatchDetail.U_VehTyp) && _moBatchDetail.U_VehTyp.ToLower() == "#remove") {
                    moWOR.U_VehTyp = "";
                } else if (!string.IsNullOrWhiteSpace(_moBatchDetail.U_VehTyp)) {
                    moWOR.U_VehTyp = _moBatchDetail.U_VehTyp;
                }


                if (!string.IsNullOrWhiteSpace(_moBatchDetail.U_UOM) && _moBatchDetail.U_UOM.Equals("#Remove", StringComparison.OrdinalIgnoreCase)) {
                    moWOR.U_UOM = "";
                    moWOR.U_ProUOM = "";
                    moWOR.U_PUOM = "";
                } else if (!string.IsNullOrWhiteSpace(_moBatchDetail.U_UOM)) {
                    moWOR.U_UOM = _moBatchDetail.U_UOM;
                    moWOR.U_ProUOM = _moBatchDetail.U_UOM;
                    moWOR.U_PUOM = _moBatchDetail.U_UOM;

                }

                //moWOR.U_UOM = _moBatchDetail.U_UOM;
                if (!string.IsNullOrWhiteSpace(_moBatchDetail.U_CustRef) && _moBatchDetail.U_CustRef.Equals("#Remove", StringComparison.OrdinalIgnoreCase)) {
                    moWOR.U_CustRef = "";
                } else if (!string.IsNullOrWhiteSpace(_moBatchDetail.U_CustRef)) {
                    moWOR.U_CustRef = _moBatchDetail.U_CustRef;
                }

                if (!string.IsNullOrWhiteSpace(_moBatchDetail.U_MaximoNum) && _moBatchDetail.U_MaximoNum.ToLower() == "#remove") {
                    moWOR.U_MaximoNum = "";
                } else if (!string.IsNullOrWhiteSpace(_moBatchDetail.U_MaximoNum)) {
                    moWOR.U_MaximoNum = _moBatchDetail.U_MaximoNum;
                }


                if (_moBatchDetail.U_ExptQty == -999) {
                    moWOR.U_ExpLdWgt = 0;
                } else if (_moBatchDetail.U_ExptQty != 0) {
                    moWOR.U_ExpLdWgt = _moBatchDetail.U_ExptQty;
                }

                if (!string.IsNullOrWhiteSpace(_moBatchDetail.U_UOM) && !_moBatchDetail.U_UOM.Equals("#Remove", StringComparison.OrdinalIgnoreCase)) {
                    double dOutUOMFactor = Config.INSTANCE.doGetBaseWeightFactorsFromAny(_moBatchDetail.U_UOM);
                    if (dOutUOMFactor < 0) {
                        moWOR.U_UseWgt = "2";

                    } else {
                        moWOR.U_UseWgt = "1";
                        if (_moBatchDetail.U_DispQty == -999) {
                            moWOR.U_RdWgt = 0;//_moBatchDetail.U_DispQty;
                            moWOR.U_TRdWgt = 0;
                            moWOR.U_TipWgt = 0;
                            moWOR.U_CstWgt = 0;
                        } else if (_moBatchDetail.U_DispQty > 0) {
                            moWOR.U_RdWgt = _moBatchDetail.U_DispQty;
                            moWOR.U_TRdWgt = _moBatchDetail.U_DispQty;
                            moWOR.U_TipWgt = _moBatchDetail.U_DispQty;
                            moWOR.U_CstWgt = _moBatchDetail.U_DispQty;
                        }
                    }
                }
                if (moWOR.U_UseWgt == "2") {
                    if (_moBatchDetail.U_DispQty == -999) {
                        moWOR.U_AUOMQt = 0;//_moBatchDetail.U_DispQty;
                        moWOR.U_TAUOMQt = 0;
                        moWOR.U_TRdWgt = 0;
                        moWOR.U_CstWgt = 0;
                        moWOR.U_TipWgt = 0;
                        moWOR.U_RdWgt = 0;

                    } else if (_moBatchDetail.U_DispQty > 0) {
                        moWOR.U_AUOMQt = _moBatchDetail.U_DispQty;
                        moWOR.U_TAUOMQt = _moBatchDetail.U_DispQty;
                        moWOR.U_TRdWgt = _moBatchDetail.U_DispQty;

                        moWOR.U_CstWgt = _moBatchDetail.U_DispQty;
                        moWOR.U_TipWgt = _moBatchDetail.U_DispQty;

                    }
                } else if (moWOR.U_UseWgt == "1") {
                    if (_moBatchDetail.U_DispQty == -999) {
                        moWOR.U_RdWgt = 0;//_moBatchDetail.U_DispQty;
                        moWOR.U_TRdWgt = 0;
                        moWOR.U_TipWgt = 0;
                        moWOR.U_CstWgt = 0;
                    } else if (_moBatchDetail.U_DispQty > 0) {
                        moWOR.U_RdWgt = _moBatchDetail.U_DispQty;
                        moWOR.U_TRdWgt = _moBatchDetail.U_DispQty;
                        moWOR.U_TipWgt = _moBatchDetail.U_DispQty;
                        moWOR.U_CstWgt = _moBatchDetail.U_DispQty;
                    }
                }
                if (Config.ParameterAsBool("SYRDWAUM", false)) {
                    if (moWOR.U_UseWgt == "2")
                        moWOR.U_RdWgt = moWOR.U_AUOMQt;
                    else {
                        moWOR.U_AUOMQt = moWOR.U_RdWgt;
                        moWOR.U_TAUOMQt = moWOR.U_TRdWgt;
                    }

                }
                if (_moBatchDetail.U_HulgQty == -999) {
                    moWOR.U_CusQty = 0;// _moBatchDetail.U_HulgQty;
                    moWOR.U_HlSQty = 0;//_moBatchDetail.U_HulgQty;

                    moWOR.U_CarWgt = 0;//_moBatchDetail.U_HulgQty;
                    moWOR.U_OrdWgt = 0;//_moBatchDetail.U_HulgQty;

                } else if (_moBatchDetail.U_HulgQty > 0) {
                    moWOR.U_CusQty = _moBatchDetail.U_HulgQty;
                    moWOR.U_HlSQty = _moBatchDetail.U_HulgQty;

                    moWOR.U_CarWgt = _moBatchDetail.U_HulgQty;
                    moWOR.U_OrdWgt = _moBatchDetail.U_HulgQty;

                }

                if (_moBatchDetail.U_RebWgt == -999) {
                    moWOR.U_PRdWgt = 0;
                    moWOR.U_ProWgt = 0;
                    moWOR.U_PAUOMQt = 0;
                } else if (_moBatchDetail.U_RebWgt > 0) {
                    moWOR.U_PRdWgt = _moBatchDetail.U_RebWgt;
                    moWOR.U_ProWgt = _moBatchDetail.U_RebWgt;
                    if (Config.ParameterAsBool("SYRDWAUM", false)) {
                        moWOR.U_PAUOMQt = _moBatchDetail.U_RebWgt;
                    } else if (moWOR.U_UseWgt == "2")
                        moWOR.U_PAUOMQt = _moBatchDetail.U_RebWgt;
                }
                doAddAdditionalItems(_moBatchDetail, ref moWOR);
                moWOR.BlockChangeNotif = false;

                moWOR.doGetChargePrices(false,Config.INSTANCE.getParameterAsBool("OVRDPRIC",false));
                moWOR.doGetCostPrices(true, true, false, false, Config.INSTANCE.getParameterAsBool("OVRDPRIC", false));//On time #ISB17331 removed haulage calc out
                moWOR.doGetHaulageCostPrice(true, Config.INSTANCE.getParameterAsBool("OVRDPRIC", false));//On time #ISB17331 add haulage calc here with adjust wgt true

                moWOR.doCalculateTotalsWithOther();
                moWOR.doCalculateProfit();
                bContinue = moWOR.doProcessData();

                if (bContinue) {
                    _moBatchDetail.U_ProcSts = ((int)en_BatchProcessStatus.Processed).ToString();
                }

                if (bContinue && DataHandler.INSTANCE.IsInTransaction()) {
                    bContinue = _moBatchDetail.doProcessData();//true,"",);
                    if (bContinue && DataHandler.INSTANCE.IsInTransaction()) {
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    }
                    DataHandler.INSTANCE.doInfo("WOR#" + _moBatchDetail.U_WOR + " update for Batch#" + _moBatchDetail.U_BatchNum + ";RowNum#" + _moBatchDetail.Code);
                    return true;
                }

                if (!bContinue && DataHandler.INSTANCE.IsInTransaction()) {
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    return false;
                }

            } catch (Exception ex) {
                if (DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doCreateWOR") });
                return false;

            } finally {
                if (bContinue && DataHandler.INSTANCE.IsInTransaction()) {
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                } else if (!bContinue && DataHandler.INSTANCE.IsInTransaction()) {
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                }
            }
            return bContinue;
        }

        //public static bool doCreateWOR(ref IDH_JOBENTR moWOH, ref IDH_WORUHD _moBatchDetail) {
        //    bool bDOContinue = true;

        //    try {
        //        string sTime = com.idh.utils.Dates.doTimeToNumStr();
        //        IDH_JOBSHD moWOR;
        //        if (!string.IsNullOrEmpty(_moBatchDetail.U_WOR)) {
        //            moWOR = new IDH_JOBSHD();//need to check which one work;
        //            moWOR = new IDH_JOBSHD(moWOH);//need to check which one work;
        //            moWOR.UnLockTable = true;
        //            if (!moWOR.getByKey(_moBatchDetail.U_WOR)) {
        //                DataHandler.INSTANCE.doInfo("Failed to load WOR#" + _moBatchDetail.U_WOR + " in Batch#" + _moBatchDetail.U_BatchNum + ", RowNum#" + _moBatchDetail.Code);
        //                return false;
        //            }
        //        } else {
        //            moWOR = new IDH_JOBSHD(moWOH);
        //            moWOR.doAddEmptyRow(true, true);
        //            string msNextRowNum = "JOBSCHE";
        //            numbers.NumbersPair oNumbers = DataHandler.INSTANCE.doGenerateCode(IDH_JOBSHD.AUTONUMPREFIX, msNextRowNum);
        //            moWOR.Code = oNumbers.CodeCode;
        //            moWOR.Name = oNumbers.NameCode;
        //        }


        //        //moWOR.getNewKey();
        //        moWOR.DoBlockUpdateTrigger = true;
        //        moWOR.U_JobNr = moWOH.Code;
        //        //moWOR.doApplyDefaults();
        //        //moWOR.U_JobNr = moWOH.Code;

        //        //moWOR.U_WOQID = moParentWOQ.Code;
        //        // moWOR.U_WOQLID = Code;
        //        //moWOR.U_RouteCd = U_RouteCd;
        //        //moWOR.U_RtCdCode = U_RtCdCode;

        //        moWOR.U_User = DataHandler.INSTANCE.User; //bridge.lookups.Config.INSTANCE.doGetUserCode(moParentWOQ.U_User.ToString());
        //        moWOR.U_Branch = Config.INSTANCE.doGetBranch(idh.bridge.DataHandler.INSTANCE.User);
        //        moWOR.U_JobTp = _moBatchDetail.U_JbTypeDs;
        //        moWOR.U_ItemCd = _moBatchDetail.U_ItemCd;
        //        moWOR.U_ItemDsc = _moBatchDetail.U_ItemNm;
        //        string sServiceTypecode = "";
        //        string sServiceTypeDesc = "";
        //        bridge.lookups.Config.INSTANCE.GetWasteGroupByJobTypeNContainerItem(_moBatchDetail.U_JbTypeDs, _moBatchDetail.U_ItemCd, ref sServiceTypecode, ref sServiceTypeDesc);// _moWOQ.U_ItemGrp;

        //        moWOR.U_ItmGrp = sServiceTypecode;
        //        //moWOR.U_I = sServiceTypecode;
        //        moWOR.U_BDate = DateTime.Now;//moParentWOQ.U_BDate;
        //        moWOR.U_BTime = sTime;
        //        if (_moBatchDetail.U_RDate == null || _moBatchDetail.U_RDate.Year < 2000)
        //            moWOR.U_RDate = DateTime.Now;//moParentWOQ.U_BDate;
        //        else
        //            moWOR.U_RDate = _moBatchDetail.U_RDate;//moParentWOQ.U_BDate;

        //        moWOR.U_RTime = sTime;
        //        moWOR.U_RTimeT = sTime;

        //        if (!(_moBatchDetail.U_SDate == null || _moBatchDetail.U_SDate.Year < 2000))
        //            moWOR.U_ASDate = _moBatchDetail.U_SDate;

        //        if (!(_moBatchDetail.U_EDate == null || _moBatchDetail.U_EDate.Year < 2000))
        //            moWOR.U_AEDate = _moBatchDetail.U_EDate;


        //        if (Config.Parameter("CTOFJBHR") != "" && Config.Parameter("CTOFJBHR") != "0" && Config.ParameterWithDefault("MAXJOBHR", "0") != "0") {
        //            DateTime cuttoffhrs = Convert.ToDateTime(DateTime.Today.ToString("yyyy/MM/dd") + " " + Config.INSTANCE.getParameter("CTOFJBHR"));
        //            DateTime BTime = DateTime.Now;
        //            if (BTime.TimeOfDay <= cuttoffhrs.TimeOfDay) {
        //                BTime = BTime.AddHours(Convert.ToInt32(Config.ParamaterWithDefault("MAXJOBHR", "0")));
        //                moWOR.U_JbToBeDoneTm = com.idh.utils.Dates.doTimeToNumStr(BTime);
        //                moWOR.U_JbToBeDoneDt = BTime;
        //            } else {
        //                //BTime = "08:00";
        //                //if BDate is beyond the cuttof time then set to 8AM of next Day( Hayden to be confirm from Mike)
        //                moWOR.U_JbToBeDoneTm = "800";
        //                DateTime BDate = DateTime.Today.AddDays(1);
        //                moWOR.U_JbToBeDoneDt = BDate;
        //            }
        //            //Else
        //        }
        //        //if (moWOH.U_NoChgTax == "Y") {
        //        //  moWOR.DoChargeVat = false;
        //        //}

        //        //   moWOR.U_ExpLdWgt = U_ExpLdWgt;
        //        //moWOR.U_MANPRC = U_MANPRC;
        //        /// moWOR.U_HaulAC = U_HaulAC;

        //        //moWOR.U_not= U_ExpLdWgt;

        //        moWOR.U_UOM = _moBatchDetail.U_UOM;
        //        double dOutUOMFactor = Config.INSTANCE.doGetBaseWeightFactorsFromAny(_moBatchDetail.U_UOM);
        //        if (dOutUOMFactor < 0) {
        //            moWOR.U_UseWgt = "2";
        //        }
        //        //moWOR.U_TCharge = U_TCharge;
        //        //moWOR.U_CusChr = U_CusChr;

        //        moWOR.U_Tip = _moBatchDetail.U_SuppCd;
        //        moWOR.U_TipNm = _moBatchDetail.U_SuppNm;
        //        //  moWOR.
        //        // moWOR.U_SAddress = U_SAddress;
        //        // moWOR.U_SAddrsLN = U_SAddrsLN;

        //        moWOR.U_CarrCd = _moBatchDetail.U_SuppCd;
        //        moWOR.U_CarrNm = _moBatchDetail.U_SuppNm;
        //        //moWOR.U_CarrNm = U_CarrNm;

        //        moWOR.U_PUOM = _moBatchDetail.U_UOM;

        //        //moWOR.U_TipCost = U_TipCost;
        //        //moWOR.U_OrdCost = U_OrdCost;



        //        // Add Rest of Values
        //        //moWOR.U_ASDate=U_ASDate
        //        //moWOR.U_ASTime=U_ASTime
        //        //moWOR.U_AEDate=U_AEDate
        //        //moWOR.U_AETime=U_AETime
        //        //moWOR.U_VehTyp=U_VehTyp
        //        //moWOR.U_Lorry=U_Lorry
        //        //moWOR.U_Driver=U_Driver
        //        //moWOR.U_DocNum=U_DocNum

        //        //moWOR.U_SupRef=U_SupRef
        //        //moWOR.U_TipWgt=U_TipWgt
        //        //moWOR.U_TipCost=U_TipCost
        //        //moWOR.U_TipTot=U_TipTot
        //        //moWOR.U_CstWgt=U_CstWgt
        //        //moWOR.U_TCharge=U_TCharge
        //        //moWOR.U_TCTotal=U_TCTotal
        //        //moWOR.U_CongCh=U_CongCh
        //        //moWOR.U_Weight=U_Weight
        //        //moWOR.U_Price=U_Price
        //        //moWOR.U_Discnt=U_Discnt
        //        //moWOR.U_DisAmt=U_DisAmt
        //        //moWOR.U_TaxAmt=U_TaxAmt
        //        //moWOR.U_Total=U_Total

        //        //moWOR.U_SLicNr=U_SLicNr
        //        //moWOR.U_SLicExp=U_SLicExp
        //        //moWOR.U_SLicCh=U_SLicCh
        //        //moWOR.U_ItmGrp=U_ItmGrp
        //        //moWOR.U_ItemCd=U_ItemCd
        //        //moWOR.U_ItemDsc=U_ItemDsc
        //        //if (string.IsNullOrEmpty(_moBatchDetail.U_WastCd) && !string.IsNullOrEmpty(_moBatchDetail.U_WastCd))
        //        //U_WasDsc = Config.INSTANCE.doGetItemDescriptionWithNoBuffer(_moBatchDetail.U_WastCd);
        //        moWOR.U_WasCd = _moBatchDetail.U_WastCd;
        //        moWOR.U_WasDsc = _moBatchDetail.U_WasteNm;
        //        //moWOR.U_Serial=U_Serial
        //        moWOR.U_Status = com.idh.bridge.lookups.FixedValues.getStatusOpen();
        //        //moWOR.U_Quantity=U_Quantity
        //        //moWOR.U_JCost=U_JCost

        //        //moWOR.U_SAINV=U_SAINV
        //        //moWOR.U_SAORD=U_SAORD
        //        //moWOR.U_TIPPO=U_TIPPO
        //        //moWOR.U_JOBPO=U_JOBPO
        //        //moWOR.U_SLicSp=U_SLicSp
        //        //moWOR.U_Tip=U_Tip
        //        //moWOR.U_CustCd=U_CustCd
        //        //moWOR.U_CarrCd=U_CarrCd
        //        //moWOR.U_CongCd=U_CongCd
        //        //moWOR.U_TipNm=U_TipNm
        //        //moWOR.U_CustNm=U_CustNm
        //        //moWOR.U_CarrNm=U_CarrNm
        //        //moWOR.U_SLicNm=U_SLicNm
        //        //moWOR.U_SCngNm=U_SCngNm
        //        //moWOR.U_Dista=U_Dista
        //        //moWOR.U_CntrNo=U_CntrNo
        //        //moWOR.U_Wei1=U_Wei1
        //        //moWOR.U_Wei2=U_Wei2
        //        //moWOR.U_Ser1=U_Ser1
        //        //moWOR.U_Ser2=U_Ser2
        //        //moWOR.U_WDt1=U_WDt1
        //        //moWOR.U_WDt2=U_WDt2
        //        //moWOR.U_AddEx=U_AddEx
        //        moWOR.U_PStat = com.idh.bridge.lookups.FixedValues.getStatusOpen();
        //        //moWOR.U_OrdWgt=U_OrdWgt
        //        //moWOR.U_OrdCost=U_OrdCost
        //        //moWOR.U_OrdTot=U_OrdTot
        //        //moWOR.U_TRLReg=U_TRLReg
        //        //moWOR.U_TRLNM=U_TRLNM
        //        //moWOR.U_UOM=U_UOM
        //        //moWOR.U_WASLIC=U_WASLIC
        //        //moWOR.U_SLPO=U_SLPO
        //        //moWOR.U_SLicCst=U_SLicCst
        //        //moWOR.U_SLicCTo=U_SLicCTo
        //        //moWOR.U_SLicCQt=U_SLicCQt
        //        //moWOR.U_WROrd=U_WROrd
        //        //moWOR.U_WRRow=U_WRRow
        //        //moWOR.U_RTime=U_RTime
        //        //moWOR.U_RTimeT=U_RTimeT
        //        //moWOR.U_CusQty=U_CusQty
        //        //moWOR.U_CusChr=U_CusChr
        //        //moWOR.U_PayMeth=U_PayMeth
        //        //moWOR.U_CustRef=U_CustRef
        //        //moWOR.U_RdWgt=U_RdWgt
        //        //moWOR.U_LorryCd=U_LorryCd
        //        //moWOR.U_Covera=U_Covera
        //        moWOR.U_Comment = _moBatchDetail.U_Comments;
        //        //moWOR.U_PUOM=U_PUOM
        //        moWOR.U_AUOM = _moBatchDetail.U_UOM;
        //        //moWOR.U_AUOMQt=U_AUOMQt
        //        //moWOR.U_CCNum=U_CCNum
        //        //moWOR.U_CCStat=U_CCStat
        //        //moWOR.U_PayStat=U_PayStat
        //        //moWOR.U_CCType=U_CCType
        //        //moWOR.U_PARCPT=U_PARCPT
        //        //moWOR.U_ENREF=U_ENREF
        //        //moWOR.U_JobRmD=U_JobRmD
        //        //moWOR.U_JobRmT=U_JobRmT
        //        //moWOR.U_RemNot=U_RemNot
        //        //moWOR.U_RemCnt=U_RemCnt
        //        //moWOR.U_TZone=U_TZone
        //        //moWOR.U_CoverHst=U_CoverHst
        //        //moWOR.U_ProPO=U_ProPO
        //        //moWOR.U_ProCd=U_ProCd
        //        //moWOR.U_ProNm=U_ProNm
        //        //moWOR.U_User=U_User
        //        //moWOR.U_Branch=U_Branch
        //        //moWOR.U_TAddChrg=U_TAddChrg
        //        //moWOR.U_TAddCost=U_TAddCost
        //        //moWOR.U_IssQty=U_IssQty
        //        //  moWOR.U_Obligated = U_Obligated;
        //        //moWOR.U_Origin=U_Origin
        //        //moWOR.U_TChrgVtRt = U_TChrgVtRt;
        //        //moWOR.U_HChrgVtRt = U_HChrgVtRt;
        //        //moWOR.U_TCostVtRt = U_TCostVtRt;
        //        //moWOR.U_HCostVtRt = U_HCostVtRt;
        //        //moWOR.U_TChrgVtGrp = U_TChrgVtGrp;
        //        //moWOR.U_HChrgVtGrp = U_HChrgVtGrp;
        //        //moWOR.U_TCostVtGrp = U_TCostVtGrp;
        //        //moWOR.U_HCostVtGrp = U_HCostVtGrp;

        //        //moWOR.U_VtCostAmt=U_VtCostAmt
        //        //moWOR.U_CustCs=U_CustCs
        //        //moWOR.U_ConNum=U_ConNum
        //        //moWOR.U_CCExp=U_CCExp
        //        //moWOR.U_CCIs=U_CCIs
        //        //moWOR.U_CCSec=U_CCSec
        //        //moWOR.U_CCHNum=U_CCHNum
        //        //moWOR.U_CCPCd=U_CCPCd
        //        //moWOR.U_IntComm=U_IntComm
        //        //moWOR.U_ContNr=U_ContNr
        //        //moWOR.U_SealNr=U_SealNr
        //        //moWOR.U_FPCoH=U_FPCoH
        //        //moWOR.U_FPCoT=U_FPCoT
        //        //moWOR.U_FPChH=U_FPChH
        //        //moWOR.U_FPChT=U_FPChT
        //        //moWOR.U_WastTNN=U_WastTNN
        //        //moWOR.U_HazWCNN=U_HazWCNN

        //        moWOR.U_SiteRef = _moBatchDetail.U_SuppRef;

        //        //moWOR.U_ExtWeig=U_ExtWeig
        //        //moWOR.U_BDate=U_BDate
        //        //moWOR.U_BTime=U_BTime
        //        //moWOR.U_CarrReb=U_CarrReb
        //        //moWOR.U_TPCN=U_TPCN
        //        //moWOR.U_MANPRC=U_MANPRC
        //        //moWOR.U_ENNO=U_ENNO
        //        //moWOR.U_SemSolid=U_SemSolid
        //        //moWOR.U_PaMuPu=U_PaMuPu
        //        //moWOR.U_Dusty=U_Dusty
        //        //moWOR.U_Fluid=U_Fluid
        //        //moWOR.U_Firm=U_Firm
        //        //moWOR.U_Consiste=U_Consiste
        //        //moWOR.U_TypPTreat=U_TypPTreat
        //        //moWOR.U_DAAttach=U_DAAttach
        //        //moWOR.U_LnkPBI=U_LnkPBI
        //        //moWOR.U_DPRRef=U_DPRRef
        //        //moWOR.U_MDChngd=U_MDChngd
        //        //moWOR.U_Sched=U_Sched
        //        //moWOR.U_USched=U_USched
        //        //moWOR.U_Analys1=U_Analys1
        //        //moWOR.U_Analys2=U_Analys2
        //        //moWOR.U_Analys3=U_Analys3
        //        //moWOR.U_Analys4=U_Analys4
        //        //moWOR.U_Analys5=U_Analys5
        //        //  moWOR.U_CustReb = _moBatchDetail.U_Rebate;// U_CustReb;
        //        //moWOR.U_TCCN = U_TCCN;
        //        moWOR.U_RowSta = com.idh.bridge.lookups.FixedValues.getStatusOpen();
        //        //moWOR.U_ActComm=U_ActComm
        //        //moWOR.U_HlSQty=U_HlSQty
        //        //moWOR.U_HaulAC=U_HaulAC
        //        //moWOR.U_ClgCode=U_ClgCode
        //        //moWOR.U_ProRef=U_ProRef
        //        //moWOR.U_SerialC=U_SerialC
        //        //moWOR.U_PAUOMQt=U_PAUOMQt
        //        //moWOR.U_PRdWgt=U_PRdWgt
        //        //moWOR.U_ProWgt=U_ProWgt
        //        //moWOR.U_ProUOM=U_ProUOM
        //        //moWOR.U_PCost=U_PCost
        //        //moWOR.U_PCTotal=U_PCTotal
        //        //moWOR.U_WgtDed=U_WgtDed
        //        //moWOR.U_AdjWgt=U_AdjWgt
        //        //moWOR.U_ValDed=U_ValDed
        //        //moWOR.U_BookIn=U_BookIn
        //        //moWOR.U_ProGRPO=U_ProGRPO
        //        //moWOR.U_GRIn=U_GRIn
        //        //moWOR.U_CarrRef=U_CarrRef
        //        //moWOR.U_PCharge=U_PCharge
        //        //moWOR.U_CustGR=U_CustGR
        //        //moWOR.U_IDHSEQ=U_IDHSEQ
        //        //moWOR.U_IDHCMT=U_IDHCMT
        //        //moWOR.U_IDHDRVI=U_IDHDRVI
        //        //moWOR.U_IDHCHK=U_IDHCHK
        //        //moWOR.U_IDHPRTD=U_IDHPRTD
        //        //moWOR.U_IDHRTCD=U_IDHRTCD
        //        //moWOR.U_IDHRTDT=U_IDHRTDT
        //        //moWOR.U_LoadSht=U_LoadSht
        //        //moWOR.U_SODlvNot=U_SODlvNot
        //        //moWOR.U_ExpLdWgt=U_ExpLdWgt
        //        //moWOR.U_IsTrl=U_IsTrl
        //        //moWOR.U_UseWgt=U_UseWgt
        //        //moWOR.U_TFSMov=U_TFSMov
        //        //moWOR.U_PayTrm=U_PayTrm
        //        //moWOR.U_JStat=U_JStat
        //        //moWOR.U_Jrnl=U_Jrnl
        //        //moWOR.U_SAddress=U_SAddress
        //        //moWOR.U_LckPrc=U_LckPrc
        //        //moWOR.U_CBICont=U_CBICont
        //        //moWOR.U_CBIManQty=U_CBIManQty
        //        //moWOR.U_CBIManUOM=U_CBIManUOM
        //        //moWOR.U_ExtComm1=U_ExtComm1
        //        //moWOR.U_ExtComm2=U_ExtComm2
        //        moWOR.U_MaximoNum = _moBatchDetail.U_MaximoNum;
        //        //moWOR.U_TFSReq=U_TFSReq
        //        //moWOR.U_TFSNum=U_TFSNum
        //        //moWOR.U_TRdWgt=U_TRdWgt
        //        //moWOR.U_PrcLink=U_PrcLink
        //        //moWOR.U_TAUOMQt=U_TAUOMQt
        //        moWOR.U_Rebate = _moBatchDetail.U_Rebate;
        //        //moWOR.U_TAddChVat=U_TAddChVat
        //        //moWOR.U_TAddCsVat=U_TAddCsVat
        //        //moWOR.U_AddCharge=U_AddCharge
        //        //moWOR.U_AddCost=U_AddCost
        //        //moWOR.U_ReqArch=U_ReqArch
        //        //moWOR.U_JbToBeDoneDt=U_JbToBeDoneDt
        //        //moWOR.U_JbToBeDoneTm=U_JbToBeDoneTm
        //        //moWOR.U_DrivrOnSitDt=U_DrivrOnSitDt
        //        //moWOR.U_DrivrOnSitTm=U_DrivrOnSitTm
        //        //moWOR.U_DrivrOfSitDt=U_DrivrOfSitDt
        //        //moWOR.U_DrivrOfSitTm=U_DrivrOfSitTm
        //        //moWOR.U_AltWasDsc=U_AltWasDsc
        //        //moWOR.U_OTComments=U_OTComments
        //        //moWOR.U_TrnCode=U_TrnCode
        //        //moWOR.U_UseBPWgt=U_UseBPWgt
        //        //moWOR.U_BPWgt=U_BPWgt
        //        //moWOR.U_CarWgt=U_CarWgt
        //        //moWOR.U_PROINV=U_PROINV
        //        //moWOR.U_WHTRNF=U_WHTRNF
        //        //moWOR.U_ProPay=U_ProPay
        //        //moWOR.U_CustPay=U_CustPay
        //        //moWOR.U_WROrd2=U_WROrd2
        //        //moWOR.U_WRRow2=U_WRRow2
        //        //moWOR.U_DispCgCc = U_DispCgCc;
        //        //moWOR.U_CarrCgCc = U_CarrCgCc;
        //        //moWOR.U_PurcCoCc = U_PurcCoCc;
        //        //moWOR.U_DispCoCc = U_DispCoCc;
        //        //moWOR.U_CarrCoCc = U_CarrCoCc;
        //        //moWOR.U_LiscCoCc = U_LiscCoCc;
        //        //moWOR.U_SAddrsLN=U_SAddrsLN
        //        //moWOR.U_FTOrig=U_FTOrig
        //        //moWOR.U_WOQID=U_WOQID
        //        //moWOR.U_WOQLID=U_WOQLID
        //        doAddAdditionalItems(_moBatchDetail, ref moWOR);
        //        moWOR.DoBlockUpdateTrigger = false;

        //        moWOR.U_CusQty = _moBatchDetail.U_Qty;
        //        moWOR.U_CarWgt = _moBatchDetail.U_Qty;
        //        //moWOR.U_RdWgt = _moBatchDetail.U_Qty;
        //        //moWOR.U_PRdWgt = _moBatchDetail.U_Qty;
        //        //moWOR.U_TRdWgt = _moBatchDetail.U_Qty;

        //        moWOR.doGetChargePrices(true);
        //        moWOR.doGetCostPrices(true, true, true, true);

        //        //moWOR.doCalculateChargeVat('R');
        //        //moWOR.doCalculateCostVat('R');
        //        //moWOR.doCalculateTotals();
        //        moWOR.doCalculateProfit();
        //        moWOR.AddLabTaskFlagIfRequired(_moBatchDetail.U_WastCd);
        //        bDOContinue = moWOR.doAddDataRow();
        //        if (bDOContinue) {
        //            //U_WOHID = moWOH.Code;
        //            //U_WORID = moWOR.Code;
        //            //U_Status = ((int)en_WOQITMSTATUS.WOCreated).ToString();
        //            //bDOContinue = doUpdateDataRow(false, "");
        //            _moBatchDetail.U_WOR = moWOR.Code;
        //        }
        //    } catch (Exception ex) {
        //        //DataHandler.INSTANCE.doError("Exception: [Code:" + Code + "]" + e.ToString(), "Error Adding the WOR [Code:" + Code + "].");
        //        //sWOHNums = "";
        //        if (DataHandler.INSTANCE.IsInTransaction())
        //            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doCreateWObyAdHocWOH") });
        //        return false;

        //    } finally {
        //    }
        //    return true;
        //}
        public static bool doAddAdditionalItems(IDH_WORUHD moBatchDetail, ref IDH_JOBSHD moWOR) {
            if (moWOR.U_Rebate !=null && moWOR.U_Rebate == "Y") {
                if (moWOR.AdditionalExpenses.Count > 0) {
                    moWOR.AdditionalExpenses.first();
                    bool doPrices = false;
                    while (moWOR.AdditionalExpenses.next()) {
                        if (moWOR.AdditionalExpenses.U_ItemCd == moWOR.U_WasCd) {
                            if (!string.IsNullOrWhiteSpace(moBatchDetail.U_UOM) &&  moBatchDetail.U_UOM.Equals("#Remove", StringComparison.OrdinalIgnoreCase)) {
                                moWOR.AdditionalExpenses.U_UOM = "";
                                doPrices = true;
                            } else if (!string.IsNullOrWhiteSpace(moBatchDetail.U_UOM)) {
                                moWOR.AdditionalExpenses.U_UOM = moBatchDetail.U_UOM;
                                doPrices = true;
                            }
                            if (moBatchDetail.U_RebWgt == -999) {
                                moWOR.AdditionalExpenses.U_Quantity = 0;// moBatchDetail.U_RebWgt;
                                doPrices = true;
                            } else if (moBatchDetail.U_RebWgt > 0) {
                                moWOR.AdditionalExpenses.U_Quantity = moBatchDetail.U_RebWgt;
                                doPrices = true;
                            }
                            if (doPrices == true) {
                                moWOR.AdditionalExpenses.doCalculateLinePrices(); // Config.INSTANCE.getParameterAsBool("OVRDPRIC", false));
                                if (!moWOR.AdditionalExpenses.doUpdateDataRow()) {
                                    return false;
                                }
                            }
                        }

                    }
                }
            }
            if (Config.ParameterAsBool("WORDOADP", false) && moWOR.AdditionalExpenses.Count > 0) {
                moWOR.AdditionalExpenses.first();
                //bool doPrices = false;
                while (moWOR.AdditionalExpenses.next()) {
                    if (moWOR.AdditionalExpenses.U_ItemCd != moWOR.U_WasCd) {

                        //if (doPrices == true) {
                        moWOR.AdditionalExpenses.doCalculateLinePrices(); // Config.INSTANCE.getParameterAsBool("OVRDPRIC", false));
                            if (!moWOR.AdditionalExpenses.doUpdateDataRow()) {
                                return false;
                            }
                        //}
                    }

                }
            }
            
            return true;
        }
        #endregion

    }
}
