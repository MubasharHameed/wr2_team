/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 2017-07-27 09:04:37 PM
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.Common;
using System.Drawing;
using System.Windows.Forms;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base{
   [Serializable]
    public class IDH_PVDBTH: com.idh.dbObjects.DBBase { 

       private IDHAddOns.idh.forms.Base moIDHForm;
       public IDHAddOns.idh.forms.Base IDHForm {
           get { return moIDHForm; }
           set { moIDHForm = value; }
       }

       private Control moParentControl;
       public Control ParentControl {
           get { return moParentControl; }
           set { moParentControl = value; }
       }

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

        public IDH_PVDBTH() : base("@IDH_PVDBTH"){
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_PVDBTH( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_PVDBTH"){
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        public IDH_PVDBTH( Control oParentControl  ) : base( "@IDH_PVDBTH"){
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oParentControl);
            moParentControl = oParentControl;
        }

#region Properties
       /**
        * Table name
        */
        public readonly static string TableName = "@IDH_PVDBTH";

        /**
        * Decription: Code
        * Mandatory: tYes
        * Name: Code
        * Size: 8
        * Type: db_Alpha
        * CType: string
        * SubType: st_None
        * ValidValue: 
        * Value: 
        */
       public readonly static string _Code = "Code";
       public string Code { 
           get { return (string)getValue(_Code); }
           set { setValue(_Code, value); }
       }
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
       /**
        * Decription: Name
        * Mandatory: tYes
         * Name: Name
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
       public readonly static string _Name = "Name";
       public string Name { 
           get { return (string)getValue(_Name); }
           set { setValue(_Name, value); }
       }
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
        /**
         * Decription: Batch AbsEntry
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_AbsEntry
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _AbsEntry = "U_AbsEntry";
        public int U_AbsEntry { 
            get {
                return getValueAsInt(_AbsEntry); 
            }
            set { setValue(_AbsEntry, value); }
        }
           public string doValidate_AbsEntry() {
               return doValidate_AbsEntry(U_AbsEntry);
           }
           public virtual string doValidate_AbsEntry(object oValue) {
               return base.doValidation(_AbsEntry, oValue);
           }

        /**
         * Decription: Batch Number
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Batch
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Batch = "U_Batch";
        public string U_Batch { 
            get {
                return getValueAsString(_Batch); 
            }
            set { setValue(_Batch, value); }
        }
           public string doValidate_Batch() {
               return doValidate_Batch(U_Batch);
           }
           public virtual string doValidate_Batch(object oValue) {
               return base.doValidation(_Batch, oValue);
           }

        /**
         * Decription: Batch Internal Sys Number
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_BatchSysNm
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _BatchSysNm = "U_BatchSysNm";
        public string U_BatchSysNm { 
            get {
                return getValueAsString(_BatchSysNm); 
            }
            set { setValue(_BatchSysNm, value); }
        }
           public string doValidate_BatchSysNm() {
               return doValidate_BatchSysNm(U_BatchSysNm);
           }
           public virtual string doValidate_BatchSysNm(object oValue) {
               return base.doValidation(_BatchSysNm, oValue);
           }

        /**
         * Decription: Disposal Route Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_DisRCode
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _DisRCode = "U_DisRCode";
        public string U_DisRCode { 
            get {
                return getValueAsString(_DisRCode); 
            }
            set { setValue(_DisRCode, value); }
        }
           public string doValidate_DisRCode() {
               return doValidate_DisRCode(U_DisRCode);
           }
           public virtual string doValidate_DisRCode(object oValue) {
               return base.doValidation(_DisRCode, oValue);
           }

        /**
         * Decription: Container Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ItemCd
         * Size: 50
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ItemCd = "U_ItemCd";
        public string U_ItemCd { 
            get {
                return getValueAsString(_ItemCd); 
            }
            set { setValue(_ItemCd, value); }
        }
           public string doValidate_ItemCd() {
               return doValidate_ItemCd(U_ItemCd);
           }
           public virtual string doValidate_ItemCd(object oValue) {
               return base.doValidation(_ItemCd, oValue);
           }

        /**
         * Decription: Container Desc
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ItemDsc
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ItemDsc = "U_ItemDsc";
        public string U_ItemDsc { 
            get {
                return getValueAsString(_ItemDsc); 
            }
            set { setValue(_ItemDsc, value); }
        }
           public string doValidate_ItemDsc() {
               return doValidate_ItemDsc(U_ItemDsc);
           }
           public virtual string doValidate_ItemDsc(object oValue) {
               return base.doValidation(_ItemDsc, oValue);
           }

        /**
         * Decription: Provisional Batch Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_PRVHCD
         * Size: 50
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _PRVHCD = "U_PRVHCD";
        public string U_PRVHCD { 
            get {
                return getValueAsString(_PRVHCD); 
            }
            set { setValue(_PRVHCD, value); }
        }
           public string doValidate_PRVHCD() {
               return doValidate_PRVHCD(U_PRVHCD);
           }
           public virtual string doValidate_PRVHCD(object oValue) {
               return base.doValidation(_PRVHCD, oValue);
           }

        /**
         * Decription: Waste Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WastCd
         * Size: 50
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _WastCd = "U_WastCd";
        public string U_WastCd { 
            get {
                return getValueAsString(_WastCd); 
            }
            set { setValue(_WastCd, value); }
        }
           public string doValidate_WastCd() {
               return doValidate_WastCd(U_WastCd);
           }
           public virtual string doValidate_WastCd(object oValue) {
               return base.doValidation(_WastCd, oValue);
           }

        /**
         * Decription: Waste Desc
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WastDsc
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _WastDsc = "U_WastDsc";
        public string U_WastDsc { 
            get {
                return getValueAsString(_WastDsc); 
            }
            set { setValue(_WastDsc, value); }
        }
           public string doValidate_WastDsc() {
               return doValidate_WastDsc(U_WastDsc);
           }
           public virtual string doValidate_WastDsc(object oValue) {
               return base.doValidation(_WastDsc, oValue);
           }

        /**
         * Decription: Warehouse Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WhsCode
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _WhsCode = "U_WhsCode";
        public string U_WhsCode { 
            get {
                return getValueAsString(_WhsCode); 
            }
            set { setValue(_WhsCode, value); }
        }
           public string doValidate_WhsCode() {
               return doValidate_WhsCode(U_WhsCode);
           }
           public virtual string doValidate_WhsCode(object oValue) {
               return base.doValidation(_WhsCode, oValue);
           }

#endregion

#region FieldInfoSetup
        protected override void doSetFieldInfo(){
            if (moDBFields == null)
                moDBFields = new DBFields(12);

            moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_AbsEntry, "Batch AbsEntry", 2, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Batch AbsEntry
            moDBFields.Add(_Batch, "Batch Number", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Batch Number
            moDBFields.Add(_BatchSysNm, "Batch Internal Sys Number", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Batch Internal Sys Number
            moDBFields.Add(_DisRCode, "Disposal Route Code", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Disposal Route Code
            moDBFields.Add(_ItemCd, "Container Code", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Container Code
            moDBFields.Add(_ItemDsc, "Container Desc", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Container Desc
            moDBFields.Add(_PRVHCD, "Provisional Batch Code", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Provisional Batch Code
            moDBFields.Add(_WastCd, "Waste Code", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Waste Code
            moDBFields.Add(_WastDsc, "Waste Desc", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Waste Desc
            moDBFields.Add(_WhsCode, "Warehouse Code", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Warehouse Code

            doBuildSelectionList();
        }

#endregion

#region validation
        public override bool doValidation(){
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_AbsEntry());
            doBuildValidationString(doValidate_Batch());
            doBuildValidationString(doValidate_BatchSysNm());
            doBuildValidationString(doValidate_DisRCode());
            doBuildValidationString(doValidate_ItemCd());
            doBuildValidationString(doValidate_ItemDsc());
            doBuildValidationString(doValidate_PRVHCD());
            doBuildValidationString(doValidate_WastCd());
            doBuildValidationString(doValidate_WastDsc());
            doBuildValidationString(doValidate_WhsCode());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_AbsEntry)) return doValidate_AbsEntry(oValue);
            if (sFieldName.Equals(_Batch)) return doValidate_Batch(oValue);
            if (sFieldName.Equals(_BatchSysNm)) return doValidate_BatchSysNm(oValue);
            if (sFieldName.Equals(_DisRCode)) return doValidate_DisRCode(oValue);
            if (sFieldName.Equals(_ItemCd)) return doValidate_ItemCd(oValue);
            if (sFieldName.Equals(_ItemDsc)) return doValidate_ItemDsc(oValue);
            if (sFieldName.Equals(_PRVHCD)) return doValidate_PRVHCD(oValue);
            if (sFieldName.Equals(_WastCd)) return doValidate_WastCd(oValue);
            if (sFieldName.Equals(_WastDsc)) return doValidate_WastDsc(oValue);
            if (sFieldName.Equals(_WhsCode)) return doValidate_WhsCode(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
       /**
        * Link by Db Fieldname  (name or ControlName)*
       */
       public void doLink(string sDBFieldname, object oControl) {
            moLinker.doLinkDataToControl(sDBFieldname, oControl);
       }
        /**
         * Link the Code Field to the Form Item (name or ControlName).
         */
        public void doLink_Code(object oControl){
            moLinker.doLinkDataToControl(_Code, oControl);
        }
        /**
         * Link the Name Field to the Form Item (name or ControlName).
         */
        public void doLink_Name(object oControl){
            moLinker.doLinkDataToControl(_Name, oControl);
        }
        /**
         * Link the U_AbsEntry Field to the Form Item  (name or ControlName).
         */
        public void doLink_AbsEntry(object oControl){
            moLinker.doLinkDataToControl(_AbsEntry, oControl);
        }
        /**
         * Link the U_Batch Field to the Form Item  (name or ControlName).
         */
        public void doLink_Batch(object oControl){
            moLinker.doLinkDataToControl(_Batch, oControl);
        }
        /**
         * Link the U_BatchSysNm Field to the Form Item  (name or ControlName).
         */
        public void doLink_BatchSysNm(object oControl){
            moLinker.doLinkDataToControl(_BatchSysNm, oControl);
        }
        /**
         * Link the U_DisRCode Field to the Form Item  (name or ControlName).
         */
        public void doLink_DisRCode(object oControl){
            moLinker.doLinkDataToControl(_DisRCode, oControl);
        }
        /**
         * Link the U_ItemCd Field to the Form Item  (name or ControlName).
         */
        public void doLink_ItemCd(object oControl){
            moLinker.doLinkDataToControl(_ItemCd, oControl);
        }
        /**
         * Link the U_ItemDsc Field to the Form Item  (name or ControlName).
         */
        public void doLink_ItemDsc(object oControl){
            moLinker.doLinkDataToControl(_ItemDsc, oControl);
        }
        /**
         * Link the U_PRVHCD Field to the Form Item  (name or ControlName).
         */
        public void doLink_PRVHCD(object oControl){
            moLinker.doLinkDataToControl(_PRVHCD, oControl);
        }
        /**
         * Link the U_WastCd Field to the Form Item  (name or ControlName).
         */
        public void doLink_WastCd(object oControl){
            moLinker.doLinkDataToControl(_WastCd, oControl);
        }
        /**
         * Link the U_WastDsc Field to the Form Item  (name or ControlName).
         */
        public void doLink_WastDsc(object oControl){
            moLinker.doLinkDataToControl(_WastDsc, oControl);
        }
        /**
         * Link the U_WhsCode Field to the Form Item  (name or ControlName).
         */
        public void doLink_WhsCode(object oControl){
            moLinker.doLinkDataToControl(_WhsCode, oControl);
        }
#endregion

    }
}
