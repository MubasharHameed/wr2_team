/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 19/05/2017 16:51:29
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base {
    [Serializable]
    public class IDH_WRADHD : com.idh.dbObjects.DBBase {

        private Linker moLinker = null;
        public Linker ControlLinker {
            get { return moLinker; }
            set { moLinker = value; }
        }

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_WRADHD() : base("@IDH_WRADHD") {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_WRADHD(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm) : base(oForm, "@IDH_WRADHD") {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
         * Table name
         */
        public readonly static string TableName = "@IDH_WRADHD";

        /**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
        public readonly static string _Code = "Code";
        public string Code {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code() {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue) {
            return base.doValidation(_Code, oValue);
        }
        /**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
        public readonly static string _Name = "Name";
        public string Name {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name() {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue) {
            return base.doValidation(_Name, oValue);
        }
        /**
		 * Decription: Record Added By
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AddedBy
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _AddedBy = "U_AddedBy";
        public string U_AddedBy {
            get {
                return getValueAsStringNZ(_AddedBy);
            }
            set { setValue(_AddedBy, value); }
        }
        public string doValidate_AddedBy() {
            return doValidate_AddedBy(U_AddedBy);
        }
        public virtual string doValidate_AddedBy(object oValue) {
            return base.doValidation(_AddedBy, oValue);
        }

        /**
		 * Decription: Additional Item
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AddtItm
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _AddtItm = "U_AddtItm";
        public string U_AddtItm {
            get {
                return getValueAsStringNZ(_AddtItm);
            }
            set { setValue(_AddtItm, value); }
        }
        public string doValidate_AddtItm() {
            return doValidate_AddtItm(U_AddtItm);
        }
        public virtual string doValidate_AddtItm(object oValue) {
            return base.doValidation(_AddtItm, oValue);
        }

        /**
		 * Decription: BatchNum
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BatchNum
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _BatchNum = "U_BatchNum";
        public string U_BatchNum {
            get {
                return getValueAsStringNZ(_BatchNum);
            }
            set { setValue(_BatchNum, value); }
        }
        public string doValidate_BatchNum() {
            return doValidate_BatchNum(U_BatchNum);
        }
        public virtual string doValidate_BatchNum(object oValue) {
            return base.doValidation(_BatchNum, oValue);
        }

        /**
		 * Decription: Batch Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BthStatus
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _BthStatus = "U_BthStatus";
        public string U_BthStatus {
            get {
                return getValueAsStringNZ(_BthStatus);
            }
            set { setValue(_BthStatus, value); }
        }
        public string doValidate_BthStatus() {
            return doValidate_BthStatus(U_BthStatus);
        }
        public virtual string doValidate_BthStatus(object oValue) {
            return base.doValidation(_BthStatus, oValue);
        }

        /**
		 * Decription: Comments
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Comments
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Comments = "U_Comments";
        public string U_Comments {
            get {
                return getValueAsStringNZ(_Comments);
            }
            set { setValue(_Comments, value); }
        }
        public string doValidate_Comments() {
            return doValidate_Comments(U_Comments);
        }
        public virtual string doValidate_Comments(object oValue) {
            return base.doValidation(_Comments, oValue);
        }

        /**
		 * Decription: Haulage Charge
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CusChr
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CusChr = "U_CusChr";
        public string U_CusChr {
            get {
                return getValueAsStringNZ(_CusChr);
            }
            set { setValue(_CusChr, value); }
        }
        public string doValidate_CusChr() {
            return doValidate_CusChr(U_CusChr);
        }
        public virtual string doValidate_CusChr(object oValue) {
            return base.doValidation(_CusChr, oValue);
        }

        /**
		 * Decription: Post Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CusPCode
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CusPCode = "U_CusPCode";
        public string U_CusPCode {
            get {
                return getValueAsStringNZ(_CusPCode);
            }
            set { setValue(_CusPCode, value); }
        }
        public string doValidate_CusPCode() {
            return doValidate_CusPCode(U_CusPCode);
        }
        public virtual string doValidate_CusPCode(object oValue) {
            return base.doValidation(_CusPCode, oValue);
        }

        /**
		 * Decription: Customer Site Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CustAdr
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CustAdr = "U_CustAdr";
        public string U_CustAdr {
            get {
                return getValueAsStringNZ(_CustAdr);
            }
            set { setValue(_CustAdr, value); }
        }
        public string doValidate_CustAdr() {
            return doValidate_CustAdr(U_CustAdr);
        }
        public virtual string doValidate_CustAdr(object oValue) {
            return base.doValidation(_CustAdr, oValue);
        }

        /**
		 * Decription: Customer-Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CustCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CustCd = "U_CustCd";
        public string U_CustCd {
            get {
                return getValueAsStringNZ(_CustCd);
            }
            set { setValue(_CustCd, value); }
        }
        public string doValidate_CustCd() {
            return doValidate_CustCd(U_CustCd);
        }
        public virtual string doValidate_CustCd(object oValue) {
            return base.doValidation(_CustCd, oValue);
        }

        /**
		 * Decription: Customer-Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CustNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CustNm = "U_CustNm";
        public string U_CustNm {
            get {
                return getValueAsStringNZ(_CustNm);
            }
            set { setValue(_CustNm, value); }
        }
        public string doValidate_CustNm() {
            return doValidate_CustNm(U_CustNm);
        }
        public virtual string doValidate_CustNm(object oValue) {
            return base.doValidation(_CustNm, oValue);
        }

        /**
		 * Decription: EDate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_EDate
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
        public readonly static string _EDate = "U_EDate";
        public DateTime U_EDate {
            get {
                return getValueAsDateTime(_EDate);
            }
            set { setValue(_EDate, value); }
        }
        public void U_EDate_AsString(string value) {
            setValue("U_EDate", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_EDate_AsString() {
            DateTime dVal = getValueAsDateTime(_EDate);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_EDate() {
            return doValidate_EDate(U_EDate);
        }
        public virtual string doValidate_EDate(object oValue) {
            return base.doValidation(_EDate, oValue);
        }

        /**
		 * Decription: Expected Qty
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ExpLdWgt
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _ExpLdWgt = "U_ExpLdWgt";
        public string U_ExpLdWgt {
            get {
                return getValueAsStringNZ(_ExpLdWgt);
            }
            set { setValue(_ExpLdWgt, value); }
        }
        public string doValidate_ExpLdWgt() {
            return doValidate_ExpLdWgt(U_ExpLdWgt);
        }
        public virtual string doValidate_ExpLdWgt(object oValue) {
            return base.doValidation(_ExpLdWgt, oValue);
        }

        /**
		 * Decription: Haulage Qty
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_HlgQty
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _HlgQty = "U_HlgQty";
        public string U_HlgQty {
            get {
                return getValueAsStringNZ(_HlgQty);
            }
            set { setValue(_HlgQty, value); }
        }
        public string doValidate_HlgQty() {
            return doValidate_HlgQty(U_HlgQty);
        }
        public virtual string doValidate_HlgQty(object oValue) {
            return base.doValidation(_HlgQty, oValue);
        }

        /**
		 * Decription: Container Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItemCd
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _ItemCd = "U_ItemCd";
        public string U_ItemCd {
            get {
                return getValueAsStringNZ(_ItemCd);
            }
            set { setValue(_ItemCd, value); }
        }
        public string doValidate_ItemCd() {
            return doValidate_ItemCd(U_ItemCd);
        }
        public virtual string doValidate_ItemCd(object oValue) {
            return base.doValidation(_ItemCd, oValue);
        }

        /**
		 * Decription: Container
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItemNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _ItemNm = "U_ItemNm";
        public string U_ItemNm {
            get {
                return getValueAsStringNZ(_ItemNm);
            }
            set { setValue(_ItemNm, value); }
        }
        public string doValidate_ItemNm() {
            return doValidate_ItemNm(U_ItemNm);
        }
        public virtual string doValidate_ItemNm(object oValue) {
            return base.doValidation(_ItemNm, oValue);
        }

        /**
		 * Decription: Order Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_JbTypeDs
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _JbTypeDs = "U_JbTypeDs";
        public string U_JbTypeDs {
            get {
                return getValueAsStringNZ(_JbTypeDs);
            }
            set { setValue(_JbTypeDs, value); }
        }
        public string doValidate_JbTypeDs() {
            return doValidate_JbTypeDs(U_JbTypeDs);
        }
        public virtual string doValidate_JbTypeDs(object oValue) {
            return base.doValidation(_JbTypeDs, oValue);
        }

        /**
		 * Decription: Maximo Number
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_MaximoNum
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _MaximoNum = "U_MaximoNum";
        public string U_MaximoNum {
            get {
                return getValueAsStringNZ(_MaximoNum);
            }
            set { setValue(_MaximoNum, value); }
        }
        public string doValidate_MaximoNum() {
            return doValidate_MaximoNum(U_MaximoNum);
        }
        public virtual string doValidate_MaximoNum(object oValue) {
            return base.doValidation(_MaximoNum, oValue);
        }

        /**
		 * Decription: Haulage Cost
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_OrdCost
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _OrdCost = "U_OrdCost";
        public string U_OrdCost {
            get {
                return getValueAsStringNZ(_OrdCost);
            }
            set { setValue(_OrdCost, value); }
        }
        public string doValidate_OrdCost() {
            return doValidate_OrdCost(U_OrdCost);
        }
        public virtual string doValidate_OrdCost(object oValue) {
            return base.doValidation(_OrdCost, oValue);
        }

        /**
		 * Decription: ParentID ID
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ParentID
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _ParentID = "U_ParentID";
        public string U_ParentID {
            get {
                return getValueAsStringNZ(_ParentID);
            }
            set { setValue(_ParentID, value); }
        }
        public string doValidate_ParentID() {
            return doValidate_ParentID(U_ParentID);
        }
        public virtual string doValidate_ParentID(object oValue) {
            return base.doValidation(_ParentID, oValue);
        }

        /**
		 * Decription: Process Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ProcSts
		 * Size: 2
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _ProcSts = "U_ProcSts";
        public string U_ProcSts {
            get {
                return getValueAsStringNZ(_ProcSts);
            }
            set { setValue(_ProcSts, value); }
        }
        public string doValidate_ProcSts() {
            return doValidate_ProcSts(U_ProcSts);
        }
        public virtual string doValidate_ProcSts(object oValue) {
            return base.doValidation(_ProcSts, oValue);
        }

        /**
		 * Decription: Purchase Unit of Measure (PUOM
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PUOM
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _PUOM = "U_PUOM";
        public string U_PUOM {
            get {
                return getValueAsStringNZ(_PUOM);
            }
            set { setValue(_PUOM, value); }
        }
        public string doValidate_PUOM() {
            return doValidate_PUOM(U_PUOM);
        }
        public virtual string doValidate_PUOM(object oValue) {
            return base.doValidation(_PUOM, oValue);
        }

        /**
		 * Decription: Actual Qty
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Qty
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _Qty = "U_Qty";
        public double U_Qty {
            get {
                return getValueAsDouble(_Qty);
            }
            set { setValue(_Qty, value); }
        }
        public string doValidate_Qty() {
            return doValidate_Qty(U_Qty);
        }
        public virtual string doValidate_Qty(object oValue) {
            return base.doValidation(_Qty, oValue);
        }

        /**
		 * Decription: RDate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RDate
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
        public readonly static string _RDate = "U_RDate";
        public DateTime U_RDate {
            get {
                return getValueAsDateTime(_RDate);
            }
            set { setValue(_RDate, value); }
        }
        public void U_RDate_AsString(string value) {
            setValue("U_RDate", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_RDate_AsString() {
            DateTime dVal = getValueAsDateTime(_RDate);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_RDate() {
            return doValidate_RDate(U_RDate);
        }
        public virtual string doValidate_RDate(object oValue) {
            return base.doValidation(_RDate, oValue);
        }

        /**
		 * Decription: Rebate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Rebate
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Rebate = "U_Rebate";
        public string U_Rebate {
            get {
                return getValueAsStringNZ(_Rebate);
            }
            set { setValue(_Rebate, value); }
        }
        public string doValidate_Rebate() {
            return doValidate_Rebate(U_Rebate);
        }
        public virtual string doValidate_Rebate(object oValue) {
            return base.doValidation(_Rebate, oValue);
        }

        /**
		 * Decription: Dispoal Site Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SAddress
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _SAddress = "U_SAddress";
        public string U_SAddress {
            get {
                return getValueAsStringNZ(_SAddress);
            }
            set { setValue(_SAddress, value); }
        }
        public string doValidate_SAddress() {
            return doValidate_SAddress(U_SAddress);
        }
        public virtual string doValidate_SAddress(object oValue) {
            return base.doValidation(_SAddress, oValue);
        }

        /**
		 * Decription: SDate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SDate
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
        public readonly static string _SDate = "U_SDate";
        public DateTime U_SDate {
            get {
                return getValueAsDateTime(_SDate);
            }
            set { setValue(_SDate, value); }
        }
        public void U_SDate_AsString(string value) {
            setValue("U_SDate", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_SDate_AsString() {
            DateTime dVal = getValueAsDateTime(_SDate);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_SDate() {
            return doValidate_SDate(U_SDate);
        }
        public virtual string doValidate_SDate(object oValue) {
            return base.doValidation(_SDate, oValue);
        }

        /**
		 * Decription: Batch Sr No
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SrNo
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _SrNo = "U_SrNo";
        public int U_SrNo {
            get {
                return getValueAsInt(_SrNo);
            }
            set { setValue(_SrNo, value); }
        }
        public string doValidate_SrNo() {
            return doValidate_SrNo(U_SrNo);
        }
        public virtual string doValidate_SrNo(object oValue) {
            return base.doValidation(_SrNo, oValue);
        }

        /**
		 * Decription: Supplier Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SupAddr
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _SupAddr = "U_SupAddr";
        public string U_SupAddr {
            get {
                return getValueAsStringNZ(_SupAddr);
            }
            set { setValue(_SupAddr, value); }
        }
        public string doValidate_SupAddr() {
            return doValidate_SupAddr(U_SupAddr);
        }
        public virtual string doValidate_SupAddr(object oValue) {
            return base.doValidation(_SupAddr, oValue);
        }

        /**
		 * Decription: Supplier Address Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SupAdrCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _SupAdrCd = "U_SupAdrCd";
        public string U_SupAdrCd {
            get {
                return getValueAsStringNZ(_SupAdrCd);
            }
            set { setValue(_SupAdrCd, value); }
        }
        public string doValidate_SupAdrCd() {
            return doValidate_SupAdrCd(U_SupAdrCd);
        }
        public virtual string doValidate_SupAdrCd(object oValue) {
            return base.doValidation(_SupAdrCd, oValue);
        }

        /**
		 * Decription: Supplier Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SuppCd
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _SuppCd = "U_SuppCd";
        public string U_SuppCd {
            get {
                return getValueAsStringNZ(_SuppCd);
            }
            set { setValue(_SuppCd, value); }
        }
        public string doValidate_SuppCd() {
            return doValidate_SuppCd(U_SuppCd);
        }
        public virtual string doValidate_SuppCd(object oValue) {
            return base.doValidation(_SuppCd, oValue);
        }

        /**
		 * Decription: Supplier Postcode
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SupPCode
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _SupPCode = "U_SupPCode";
        public string U_SupPCode {
            get {
                return getValueAsStringNZ(_SupPCode);
            }
            set { setValue(_SupPCode, value); }
        }
        public string doValidate_SupPCode() {
            return doValidate_SupPCode(U_SupPCode);
        }
        public virtual string doValidate_SupPCode(object oValue) {
            return base.doValidation(_SupPCode, oValue);
        }

        /**
		 * Decription: Supplier Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SuppNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _SuppNm = "U_SuppNm";
        public string U_SuppNm {
            get {
                return getValueAsStringNZ(_SuppNm);
            }
            set { setValue(_SuppNm, value); }
        }
        public string doValidate_SuppNm() {
            return doValidate_SuppNm(U_SuppNm);
        }
        public virtual string doValidate_SuppNm(object oValue) {
            return base.doValidation(_SuppNm, oValue);
        }

        /**
		 * Decription: Supplier Reference
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SuppRef
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _SuppRef = "U_SuppRef";
        public string U_SuppRef {
            get {
                return getValueAsStringNZ(_SuppRef);
            }
            set { setValue(_SuppRef, value); }
        }
        public string doValidate_SuppRef() {
            return doValidate_SuppRef(U_SuppRef);
        }
        public virtual string doValidate_SuppRef(object oValue) {
            return base.doValidation(_SuppRef, oValue);
        }

        /**
		 * Decription: Tip Chrg/Pur Cost
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TChgPCst
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _TChgPCst = "U_TChgPCst";
        public string U_TChgPCst {
            get {
                return getValueAsStringNZ(_TChgPCst);
            }
            set { setValue(_TChgPCst, value); }
        }
        public string doValidate_TChgPCst() {
            return doValidate_TChgPCst(U_TChgPCst);
        }
        public virtual string doValidate_TChgPCst(object oValue) {
            return base.doValidation(_TChgPCst, oValue);
        }

        /**
		 * Decription: Disposal Site
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Tip
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Tip = "U_Tip";
        public string U_Tip {
            get {
                return getValueAsStringNZ(_Tip);
            }
            set { setValue(_Tip, value); }
        }
        public string doValidate_Tip() {
            return doValidate_Tip(U_Tip);
        }
        public virtual string doValidate_Tip(object oValue) {
            return base.doValidation(_Tip, oValue);
        }

        /**
		 * Decription: Disposal Cost
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TipCost
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _TipCost = "U_TipCost";
        public string U_TipCost {
            get {
                return getValueAsStringNZ(_TipCost);
            }
            set { setValue(_TipCost, value); }
        }
        public string doValidate_TipCost() {
            return doValidate_TipCost(U_TipCost);
        }
        public virtual string doValidate_TipCost(object oValue) {
            return base.doValidation(_TipCost, oValue);
        }

        /**
		 * Decription: Dispoal Site Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_TipNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _TipNm = "U_TipNm";
        public string U_TipNm {
            get {
                return getValueAsStringNZ(_TipNm);
            }
            set { setValue(_TipNm, value); }
        }
        public string doValidate_TipNm() {
            return doValidate_TipNm(U_TipNm);
        }
        public virtual string doValidate_TipNm(object oValue) {
            return base.doValidation(_TipNm, oValue);
        }

        /**
		 * Decription: Unit of Measure (UOM)
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UOM
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _UOM = "U_UOM";
        public string U_UOM {
            get {
                return getValueAsStringNZ(_UOM);
            }
            set { setValue(_UOM, value); }
        }
        public string doValidate_UOM() {
            return doValidate_UOM(U_UOM);
        }
        public virtual string doValidate_UOM(object oValue) {
            return base.doValidation(_UOM, oValue);
        }

        /**
		 * Decription: Validation Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_VldStatus
		 * Size: 2
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _VldStatus = "U_VldStatus";
        public string U_VldStatus {
            get {
                return getValueAsStringNZ(_VldStatus);
            }
            set { setValue(_VldStatus, value); }
        }
        public string doValidate_VldStatus() {
            return doValidate_VldStatus(U_VldStatus);
        }
        public virtual string doValidate_VldStatus(object oValue) {
            return base.doValidation(_VldStatus, oValue);
        }

        /**
		 * Decription: Waste Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WastCd
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _WastCd = "U_WastCd";
        public string U_WastCd {
            get {
                return getValueAsStringNZ(_WastCd);
            }
            set { setValue(_WastCd, value); }
        }
        public string doValidate_WastCd() {
            return doValidate_WastCd(U_WastCd);
        }
        public virtual string doValidate_WastCd(object oValue) {
            return base.doValidation(_WastCd, oValue);
        }

        /**
		 * Decription: Waste Material
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WasteNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _WasteNm = "U_WasteNm";
        public string U_WasteNm {
            get {
                return getValueAsStringNZ(_WasteNm);
            }
            set { setValue(_WasteNm, value); }
        }
        public string doValidate_WasteNm() {
            return doValidate_WasteNm(U_WasteNm);
        }
        public virtual string doValidate_WasteNm(object oValue) {
            return base.doValidation(_WasteNm, oValue);
        }

        /**
		 * Decription: WOH
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WOH
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _WOH = "U_WOH";
        public string U_WOH {
            get {
                return getValueAsStringNZ(_WOH);
            }
            set { setValue(_WOH, value); }
        }
        public string doValidate_WOH() {
            return doValidate_WOH(U_WOH);
        }
        public virtual string doValidate_WOH(object oValue) {
            return base.doValidation(_WOH, oValue);
        }

        /**
		 * Decription: WOR
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WOR
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _WOR = "U_WOR";
        public string U_WOR {
            get {
                return getValueAsStringNZ(_WOR);
            }
            set { setValue(_WOR, value); }
        }
        public string doValidate_WOR() {
            return doValidate_WOR(U_WOR);
        }
        public virtual string doValidate_WOR(object oValue) {
            return base.doValidation(_WOR, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo() {
            if (moDBFields == null)
                moDBFields = new DBFields(45);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_AddedBy, "Record Added By", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Record Added By
            moDBFields.Add(_AddtItm, "Additional Item", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Additional Item
            moDBFields.Add(_BatchNum, "BatchNum", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //BatchNum
            moDBFields.Add(_BthStatus, "Batch Status", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Batch Status
            moDBFields.Add(_Comments, "Comments", 6, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Comments
            moDBFields.Add(_CusChr, "Haulage Charge", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Haulage Charge
            moDBFields.Add(_CusPCode, "Post Code", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Post Code
            moDBFields.Add(_CustAdr, "Customer Site Address", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Customer Site Address
            moDBFields.Add(_CustCd, "Customer-Code", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Customer-Code
            moDBFields.Add(_CustNm, "Customer-Name", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Customer-Name
            moDBFields.Add(_EDate, "EDate", 12, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //EDate
            moDBFields.Add(_ExpLdWgt, "Expected Qty", 13, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Expected Qty
            moDBFields.Add(_HlgQty, "Haulage Qty", 14, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Haulage Qty
            moDBFields.Add(_ItemCd, "Container Code", 15, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //Container Code
            moDBFields.Add(_ItemNm, "Container", 16, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Container
            moDBFields.Add(_JbTypeDs, "Order Type", 17, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Order Type
            moDBFields.Add(_MaximoNum, "Maximo Number", 18, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Maximo Number
            moDBFields.Add(_OrdCost, "Haulage Cost", 19, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Haulage Cost
            moDBFields.Add(_ParentID, "ParentID ID", 20, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //ParentID ID
            moDBFields.Add(_ProcSts, "Process Status", 21, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2, EMPTYSTR, false, false); //Process Status
            moDBFields.Add(_PUOM, "Purchase Unit of Measure (PUOM", 22, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Purchase Unit of Measure (PUOM
            moDBFields.Add(_Qty, "Actual Qty", 23, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Actual Qty
            moDBFields.Add(_RDate, "RDate", 24, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //RDate
            moDBFields.Add(_Rebate, "Rebate", 25, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Rebate
            moDBFields.Add(_SAddress, "Dispoal Site Address", 26, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Dispoal Site Address
            moDBFields.Add(_SDate, "SDate", 27, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //SDate
            moDBFields.Add(_SrNo, "Batch Sr No", 28, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Batch Sr No
            moDBFields.Add(_SupAddr, "Supplier Address", 29, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Supplier Address
            moDBFields.Add(_SupAdrCd, "Supplier Address Code", 30, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Supplier Address Code
            moDBFields.Add(_SuppCd, "Supplier Code", 31, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //Supplier Code
            moDBFields.Add(_SupPCode, "Supplier Postcode", 32, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Supplier Postcode
            moDBFields.Add(_SuppNm, "Supplier Name", 33, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Supplier Name
            moDBFields.Add(_SuppRef, "Supplier Reference", 34, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Supplier Reference
            moDBFields.Add(_TChgPCst, "Tip Chrg/Pur Cost", 35, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Tip Chrg/Pur Cost
            moDBFields.Add(_Tip, "Disposal Site", 36, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Disposal Site
            moDBFields.Add(_TipCost, "Disposal Cost", 37, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Disposal Cost
            moDBFields.Add(_TipNm, "Dispoal Site Name", 38, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Dispoal Site Name
            moDBFields.Add(_UOM, "Unit of Measure (UOM)", 39, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Unit of Measure (UOM)
            moDBFields.Add(_VldStatus, "Validation Status", 40, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2, EMPTYSTR, false, false); //Validation Status
            moDBFields.Add(_WastCd, "Waste Code", 41, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //Waste Code
            moDBFields.Add(_WasteNm, "Waste Material", 42, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Waste Material
            moDBFields.Add(_WOH, "WOH", 43, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //WOH
            moDBFields.Add(_WOR, "WOR", 44, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //WOR

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation() {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_AddedBy());
            doBuildValidationString(doValidate_AddtItm());
            doBuildValidationString(doValidate_BatchNum());
            doBuildValidationString(doValidate_BthStatus());
            doBuildValidationString(doValidate_Comments());
            doBuildValidationString(doValidate_CusChr());
            doBuildValidationString(doValidate_CusPCode());
            doBuildValidationString(doValidate_CustAdr());
            doBuildValidationString(doValidate_CustCd());
            doBuildValidationString(doValidate_CustNm());
            doBuildValidationString(doValidate_EDate());
            doBuildValidationString(doValidate_ExpLdWgt());
            doBuildValidationString(doValidate_HlgQty());
            doBuildValidationString(doValidate_ItemCd());
            doBuildValidationString(doValidate_ItemNm());
            doBuildValidationString(doValidate_JbTypeDs());
            doBuildValidationString(doValidate_MaximoNum());
            doBuildValidationString(doValidate_OrdCost());
            doBuildValidationString(doValidate_ParentID());
            doBuildValidationString(doValidate_ProcSts());
            doBuildValidationString(doValidate_PUOM());
            doBuildValidationString(doValidate_Qty());
            doBuildValidationString(doValidate_RDate());
            doBuildValidationString(doValidate_Rebate());
            doBuildValidationString(doValidate_SAddress());
            doBuildValidationString(doValidate_SDate());
            doBuildValidationString(doValidate_SrNo());
            doBuildValidationString(doValidate_SupAddr());
            doBuildValidationString(doValidate_SupAdrCd());
            doBuildValidationString(doValidate_SuppCd());
            doBuildValidationString(doValidate_SupPCode());
            doBuildValidationString(doValidate_SuppNm());
            doBuildValidationString(doValidate_SuppRef());
            doBuildValidationString(doValidate_TChgPCst());
            doBuildValidationString(doValidate_Tip());
            doBuildValidationString(doValidate_TipCost());
            doBuildValidationString(doValidate_TipNm());
            doBuildValidationString(doValidate_UOM());
            doBuildValidationString(doValidate_VldStatus());
            doBuildValidationString(doValidate_WastCd());
            doBuildValidationString(doValidate_WasteNm());
            doBuildValidationString(doValidate_WOH());
            doBuildValidationString(doValidate_WOR());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_AddedBy)) return doValidate_AddedBy(oValue);
            if (sFieldName.Equals(_AddtItm)) return doValidate_AddtItm(oValue);
            if (sFieldName.Equals(_BatchNum)) return doValidate_BatchNum(oValue);
            if (sFieldName.Equals(_BthStatus)) return doValidate_BthStatus(oValue);
            if (sFieldName.Equals(_Comments)) return doValidate_Comments(oValue);
            if (sFieldName.Equals(_CusChr)) return doValidate_CusChr(oValue);
            if (sFieldName.Equals(_CusPCode)) return doValidate_CusPCode(oValue);
            if (sFieldName.Equals(_CustAdr)) return doValidate_CustAdr(oValue);
            if (sFieldName.Equals(_CustCd)) return doValidate_CustCd(oValue);
            if (sFieldName.Equals(_CustNm)) return doValidate_CustNm(oValue);
            if (sFieldName.Equals(_EDate)) return doValidate_EDate(oValue);
            if (sFieldName.Equals(_ExpLdWgt)) return doValidate_ExpLdWgt(oValue);
            if (sFieldName.Equals(_HlgQty)) return doValidate_HlgQty(oValue);
            if (sFieldName.Equals(_ItemCd)) return doValidate_ItemCd(oValue);
            if (sFieldName.Equals(_ItemNm)) return doValidate_ItemNm(oValue);
            if (sFieldName.Equals(_JbTypeDs)) return doValidate_JbTypeDs(oValue);
            if (sFieldName.Equals(_MaximoNum)) return doValidate_MaximoNum(oValue);
            if (sFieldName.Equals(_OrdCost)) return doValidate_OrdCost(oValue);
            if (sFieldName.Equals(_ParentID)) return doValidate_ParentID(oValue);
            if (sFieldName.Equals(_ProcSts)) return doValidate_ProcSts(oValue);
            if (sFieldName.Equals(_PUOM)) return doValidate_PUOM(oValue);
            if (sFieldName.Equals(_Qty)) return doValidate_Qty(oValue);
            if (sFieldName.Equals(_RDate)) return doValidate_RDate(oValue);
            if (sFieldName.Equals(_Rebate)) return doValidate_Rebate(oValue);
            if (sFieldName.Equals(_SAddress)) return doValidate_SAddress(oValue);
            if (sFieldName.Equals(_SDate)) return doValidate_SDate(oValue);
            if (sFieldName.Equals(_SrNo)) return doValidate_SrNo(oValue);
            if (sFieldName.Equals(_SupAddr)) return doValidate_SupAddr(oValue);
            if (sFieldName.Equals(_SupAdrCd)) return doValidate_SupAdrCd(oValue);
            if (sFieldName.Equals(_SuppCd)) return doValidate_SuppCd(oValue);
            if (sFieldName.Equals(_SupPCode)) return doValidate_SupPCode(oValue);
            if (sFieldName.Equals(_SuppNm)) return doValidate_SuppNm(oValue);
            if (sFieldName.Equals(_SuppRef)) return doValidate_SuppRef(oValue);
            if (sFieldName.Equals(_TChgPCst)) return doValidate_TChgPCst(oValue);
            if (sFieldName.Equals(_Tip)) return doValidate_Tip(oValue);
            if (sFieldName.Equals(_TipCost)) return doValidate_TipCost(oValue);
            if (sFieldName.Equals(_TipNm)) return doValidate_TipNm(oValue);
            if (sFieldName.Equals(_UOM)) return doValidate_UOM(oValue);
            if (sFieldName.Equals(_VldStatus)) return doValidate_VldStatus(oValue);
            if (sFieldName.Equals(_WastCd)) return doValidate_WastCd(oValue);
            if (sFieldName.Equals(_WasteNm)) return doValidate_WasteNm(oValue);
            if (sFieldName.Equals(_WOH)) return doValidate_WOH(oValue);
            if (sFieldName.Equals(_WOR)) return doValidate_WOR(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName) {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
		 * Link the Name Field to the Form Item.
		 */
        public void doLink_Name(string sControlName) {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
		 * Link the U_AddedBy Field to the Form Item.
		 */
        public void doLink_AddedBy(string sControlName) {
            moLinker.doLinkDataToControl(_AddedBy, sControlName);
        }
        /**
		 * Link the U_AddtItm Field to the Form Item.
		 */
        public void doLink_AddtItm(string sControlName) {
            moLinker.doLinkDataToControl(_AddtItm, sControlName);
        }
        /**
		 * Link the U_BatchNum Field to the Form Item.
		 */
        public void doLink_BatchNum(string sControlName) {
            moLinker.doLinkDataToControl(_BatchNum, sControlName);
        }
        /**
		 * Link the U_BthStatus Field to the Form Item.
		 */
        public void doLink_BthStatus(string sControlName) {
            moLinker.doLinkDataToControl(_BthStatus, sControlName);
        }
        /**
		 * Link the U_Comments Field to the Form Item.
		 */
        public void doLink_Comments(string sControlName) {
            moLinker.doLinkDataToControl(_Comments, sControlName);
        }
        /**
		 * Link the U_CusChr Field to the Form Item.
		 */
        public void doLink_CusChr(string sControlName) {
            moLinker.doLinkDataToControl(_CusChr, sControlName);
        }
        /**
		 * Link the U_CusPCode Field to the Form Item.
		 */
        public void doLink_CusPCode(string sControlName) {
            moLinker.doLinkDataToControl(_CusPCode, sControlName);
        }
        /**
		 * Link the U_CustAdr Field to the Form Item.
		 */
        public void doLink_CustAdr(string sControlName) {
            moLinker.doLinkDataToControl(_CustAdr, sControlName);
        }
        /**
		 * Link the U_CustCd Field to the Form Item.
		 */
        public void doLink_CustCd(string sControlName) {
            moLinker.doLinkDataToControl(_CustCd, sControlName);
        }
        /**
		 * Link the U_CustNm Field to the Form Item.
		 */
        public void doLink_CustNm(string sControlName) {
            moLinker.doLinkDataToControl(_CustNm, sControlName);
        }
        /**
		 * Link the U_EDate Field to the Form Item.
		 */
        public void doLink_EDate(string sControlName) {
            moLinker.doLinkDataToControl(_EDate, sControlName);
        }
        /**
		 * Link the U_ExpLdWgt Field to the Form Item.
		 */
        public void doLink_ExpLdWgt(string sControlName) {
            moLinker.doLinkDataToControl(_ExpLdWgt, sControlName);
        }
        /**
		 * Link the U_HlgQty Field to the Form Item.
		 */
        public void doLink_HlgQty(string sControlName) {
            moLinker.doLinkDataToControl(_HlgQty, sControlName);
        }
        /**
		 * Link the U_ItemCd Field to the Form Item.
		 */
        public void doLink_ItemCd(string sControlName) {
            moLinker.doLinkDataToControl(_ItemCd, sControlName);
        }
        /**
		 * Link the U_ItemNm Field to the Form Item.
		 */
        public void doLink_ItemNm(string sControlName) {
            moLinker.doLinkDataToControl(_ItemNm, sControlName);
        }
        /**
		 * Link the U_JbTypeDs Field to the Form Item.
		 */
        public void doLink_JbTypeDs(string sControlName) {
            moLinker.doLinkDataToControl(_JbTypeDs, sControlName);
        }
        /**
		 * Link the U_MaximoNum Field to the Form Item.
		 */
        public void doLink_MaximoNum(string sControlName) {
            moLinker.doLinkDataToControl(_MaximoNum, sControlName);
        }
        /**
		 * Link the U_OrdCost Field to the Form Item.
		 */
        public void doLink_OrdCost(string sControlName) {
            moLinker.doLinkDataToControl(_OrdCost, sControlName);
        }
        /**
		 * Link the U_ParentID Field to the Form Item.
		 */
        public void doLink_ParentID(string sControlName) {
            moLinker.doLinkDataToControl(_ParentID, sControlName);
        }
        /**
		 * Link the U_ProcSts Field to the Form Item.
		 */
        public void doLink_ProcSts(string sControlName) {
            moLinker.doLinkDataToControl(_ProcSts, sControlName);
        }
        /**
		 * Link the U_PUOM Field to the Form Item.
		 */
        public void doLink_PUOM(string sControlName) {
            moLinker.doLinkDataToControl(_PUOM, sControlName);
        }
        /**
		 * Link the U_Qty Field to the Form Item.
		 */
        public void doLink_Qty(string sControlName) {
            moLinker.doLinkDataToControl(_Qty, sControlName);
        }
        /**
		 * Link the U_RDate Field to the Form Item.
		 */
        public void doLink_RDate(string sControlName) {
            moLinker.doLinkDataToControl(_RDate, sControlName);
        }
        /**
		 * Link the U_Rebate Field to the Form Item.
		 */
        public void doLink_Rebate(string sControlName) {
            moLinker.doLinkDataToControl(_Rebate, sControlName);
        }
        /**
		 * Link the U_SAddress Field to the Form Item.
		 */
        public void doLink_SAddress(string sControlName) {
            moLinker.doLinkDataToControl(_SAddress, sControlName);
        }
        /**
		 * Link the U_SDate Field to the Form Item.
		 */
        public void doLink_SDate(string sControlName) {
            moLinker.doLinkDataToControl(_SDate, sControlName);
        }
        /**
		 * Link the U_SrNo Field to the Form Item.
		 */
        public void doLink_SrNo(string sControlName) {
            moLinker.doLinkDataToControl(_SrNo, sControlName);
        }
        /**
		 * Link the U_SupAddr Field to the Form Item.
		 */
        public void doLink_SupAddr(string sControlName) {
            moLinker.doLinkDataToControl(_SupAddr, sControlName);
        }
        /**
		 * Link the U_SupAdrCd Field to the Form Item.
		 */
        public void doLink_SupAdrCd(string sControlName) {
            moLinker.doLinkDataToControl(_SupAdrCd, sControlName);
        }
        /**
		 * Link the U_SuppCd Field to the Form Item.
		 */
        public void doLink_SuppCd(string sControlName) {
            moLinker.doLinkDataToControl(_SuppCd, sControlName);
        }
        /**
		 * Link the U_SupPCode Field to the Form Item.
		 */
        public void doLink_SupPCode(string sControlName) {
            moLinker.doLinkDataToControl(_SupPCode, sControlName);
        }
        /**
		 * Link the U_SuppNm Field to the Form Item.
		 */
        public void doLink_SuppNm(string sControlName) {
            moLinker.doLinkDataToControl(_SuppNm, sControlName);
        }
        /**
		 * Link the U_SuppRef Field to the Form Item.
		 */
        public void doLink_SuppRef(string sControlName) {
            moLinker.doLinkDataToControl(_SuppRef, sControlName);
        }
        /**
		 * Link the U_TChgPCst Field to the Form Item.
		 */
        public void doLink_TChgPCst(string sControlName) {
            moLinker.doLinkDataToControl(_TChgPCst, sControlName);
        }
        /**
		 * Link the U_Tip Field to the Form Item.
		 */
        public void doLink_Tip(string sControlName) {
            moLinker.doLinkDataToControl(_Tip, sControlName);
        }
        /**
		 * Link the U_TipCost Field to the Form Item.
		 */
        public void doLink_TipCost(string sControlName) {
            moLinker.doLinkDataToControl(_TipCost, sControlName);
        }
        /**
		 * Link the U_TipNm Field to the Form Item.
		 */
        public void doLink_TipNm(string sControlName) {
            moLinker.doLinkDataToControl(_TipNm, sControlName);
        }
        /**
		 * Link the U_UOM Field to the Form Item.
		 */
        public void doLink_UOM(string sControlName) {
            moLinker.doLinkDataToControl(_UOM, sControlName);
        }
        /**
		 * Link the U_VldStatus Field to the Form Item.
		 */
        public void doLink_VldStatus(string sControlName) {
            moLinker.doLinkDataToControl(_VldStatus, sControlName);
        }
        /**
		 * Link the U_WastCd Field to the Form Item.
		 */
        public void doLink_WastCd(string sControlName) {
            moLinker.doLinkDataToControl(_WastCd, sControlName);
        }
        /**
		 * Link the U_WasteNm Field to the Form Item.
		 */
        public void doLink_WasteNm(string sControlName) {
            moLinker.doLinkDataToControl(_WasteNm, sControlName);
        }
        /**
		 * Link the U_WOH Field to the Form Item.
		 */
        public void doLink_WOH(string sControlName) {
            moLinker.doLinkDataToControl(_WOH, sControlName);
        }
        /**
		 * Link the U_WOR Field to the Form Item.
		 */
        public void doLink_WOR(string sControlName) {
            moLinker.doLinkDataToControl(_WOR, sControlName);
        }
        #endregion

    }
}
