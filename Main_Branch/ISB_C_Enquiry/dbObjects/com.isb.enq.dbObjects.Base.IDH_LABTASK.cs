/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 15/03/2017 13:13:14
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base {
    [Serializable]
    public class IDH_LABTASK : com.idh.dbObjects.DBBase {

        private Linker moLinker = null;
        public Linker ControlLinker {
            get { return moLinker; }
            set { moLinker = value; }
        }

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_LABTASK() : base("@IDH_LABTASK") {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_LABTASK(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm) : base(oForm, "@IDH_LABTASK") {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
         * Table name
         */
        public readonly static string TableName = "@IDH_LABTASK";

        /**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
        public readonly static string _Code = "Code";
        public string Code {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code() {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue) {
            return base.doValidation(_Code, oValue);
        }
        /**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
        public readonly static string _Name = "Name";
        public string Name {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name() {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue) {
            return base.doValidation(_Name, oValue);
        }
        /**
		 * Decription: Analysis Requested
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AnalysisReq
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _AnalysisReq = "U_AnalysisReq";
        public string U_AnalysisReq {
            get {
                return getValueAsString(_AnalysisReq);
            }
            set { setValue(_AnalysisReq, value); }
        }
        public string doValidate_AnalysisReq() {
            return doValidate_AnalysisReq(U_AnalysisReq);
        }
        public virtual string doValidate_AnalysisReq(object oValue) {
            return base.doValidation(_AnalysisReq, oValue);
        }

        /**
		 * Decription: Assigned To
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AssignedTo
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _AssignedTo = "U_AssignedTo";
        public string U_AssignedTo {
            get {
                return getValueAsString(_AssignedTo);
            }
            set { setValue(_AssignedTo, value); }
        }
        public string doValidate_AssignedTo() {
            return doValidate_AssignedTo(U_AssignedTo);
        }
        public virtual string doValidate_AssignedTo(object oValue) {
            return base.doValidation(_AssignedTo, oValue);
        }

        /**
		 * Decription: Attachments
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AtcAbsEntry
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _AtcAbsEntry = "U_AtcAbsEntry";
        public int U_AtcAbsEntry {
            get {
                return getValueAsInt(_AtcAbsEntry);
            }
            set { setValue(_AtcAbsEntry, value); }
        }
        public string doValidate_AtcAbsEntry() {
            return doValidate_AtcAbsEntry(U_AtcAbsEntry);
        }
        public virtual string doValidate_AtcAbsEntry(object oValue) {
            return base.doValidation(_AtcAbsEntry, oValue);
        }

        /**
		 * Decription: BP Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BPCode
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _BPCode = "U_BPCode";
        public string U_BPCode {
            get {
                return getValueAsString(_BPCode);
            }
            set { setValue(_BPCode, value); }
        }
        public string doValidate_BPCode() {
            return doValidate_BPCode(U_BPCode);
        }
        public virtual string doValidate_BPCode(object oValue) {
            return base.doValidation(_BPCode, oValue);
        }

        /**
		 * Decription: BP Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BPName
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _BPName = "U_BPName";
        public string U_BPName {
            get {
                return getValueAsString(_BPName);
            }
            set { setValue(_BPName, value); }
        }
        public string doValidate_BPName() {
            return doValidate_BPName(U_BPName);
        }
        public virtual string doValidate_BPName(object oValue) {
            return base.doValidation(_BPName, oValue);
        }

        /**
		 * Decription: Decision
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Decision
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Decision = "U_Decision";
        public string U_Decision {
            get {
                return getValueAsString(_Decision);
            }
            set { setValue(_Decision, value); }
        }
        public string doValidate_Decision() {
            return doValidate_Decision(U_Decision);
        }
        public virtual string doValidate_Decision(object oValue) {
            return base.doValidation(_Decision, oValue);
        }

        /**
		 * Decription: Department
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Department
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Department = "U_Department";
        public string U_Department {
            get {
                return getValueAsString(_Department);
            }
            set { setValue(_Department, value); }
        }
        public string doValidate_Department() {
            return doValidate_Department(U_Department);
        }
        public virtual string doValidate_Department(object oValue) {
            return base.doValidation(_Department, oValue);
        }

        /**
		 * Decription: Disposal Route Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DipRtCd
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _DipRtCd = "U_DipRtCd";
        public string U_DipRtCd {
            get {
                return getValueAsString(_DipRtCd);
            }
            set { setValue(_DipRtCd, value); }
        }
        public string doValidate_DipRtCd() {
            return doValidate_DipRtCd(U_DipRtCd);
        }
        public virtual string doValidate_DipRtCd(object oValue) {
            return base.doValidation(_DipRtCd, oValue);
        }

        /**
		 * Decription: Header DocNum
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DocNum
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _DocNum = "U_DocNum";
        public string U_DocNum {
            get {
                return getValueAsString(_DocNum);
            }
            set { setValue(_DocNum, value); }
        }
        public string doValidate_DocNum() {
            return doValidate_DocNum(U_DocNum);
        }
        public virtual string doValidate_DocNum(object oValue) {
            return base.doValidation(_DocNum, oValue);
        }

        /**
		 * Decription: Date Analysed
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DtAnalysed
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
        public readonly static string _DtAnalysed = "U_DtAnalysed";
        public DateTime U_DtAnalysed {
            get {
                return getValueAsDateTime(_DtAnalysed);
            }
            set { setValue(_DtAnalysed, value); }
        }
        public void U_DtAnalysed_AsString(string value) {
            setValue("U_DtAnalysed", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_DtAnalysed_AsString() {
            DateTime dVal = getValueAsDateTime(_DtAnalysed);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_DtAnalysed() {
            return doValidate_DtAnalysed(U_DtAnalysed);
        }
        public virtual string doValidate_DtAnalysed(object oValue) {
            return base.doValidation(_DtAnalysed, oValue);
        }

        /**
		 * Decription: Date Received
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DtReceived
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
        public readonly static string _DtReceived = "U_DtReceived";
        public DateTime U_DtReceived {
            get {
                return getValueAsDateTime(_DtReceived);
            }
            set { setValue(_DtReceived, value); }
        }
        public void U_DtReceived_AsString(string value) {
            setValue("U_DtReceived", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_DtReceived_AsString() {
            DateTime dVal = getValueAsDateTime(_DtReceived);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_DtReceived() {
            return doValidate_DtReceived(U_DtReceived);
        }
        public virtual string doValidate_DtReceived(object oValue) {
            return base.doValidation(_DtReceived, oValue);
        }

        /**
		 * Decription: Date Requested
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_DtRequested
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
        public readonly static string _DtRequested = "U_DtRequested";
        public DateTime U_DtRequested {
            get {
                return getValueAsDateTime(_DtRequested);
            }
            set { setValue(_DtRequested, value); }
        }
        public void U_DtRequested_AsString(string value) {
            setValue("U_DtRequested", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_DtRequested_AsString() {
            DateTime dVal = getValueAsDateTime(_DtRequested);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_DtRequested() {
            return doValidate_DtRequested(U_DtRequested);
        }
        public virtual string doValidate_DtRequested(object oValue) {
            return base.doValidation(_DtRequested, oValue);
        }

        /**
		 * Decription: External Comments
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ExtComments
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _ExtComments = "U_ExtComments";
        public string U_ExtComments {
            get {
                return getValueAsString(_ExtComments);
            }
            set { setValue(_ExtComments, value); }
        }
        public string doValidate_ExtComments() {
            return doValidate_ExtComments(U_ExtComments);
        }
        public virtual string doValidate_ExtComments(object oValue) {
            return base.doValidation(_ExtComments, oValue);
        }

        /**
		 * Decription: External Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ExtStatus
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _ExtStatus = "U_ExtStatus";
        public string U_ExtStatus {
            get {
                return getValueAsString(_ExtStatus);
            }
            set { setValue(_ExtStatus, value); }
        }
        public string doValidate_ExtStatus() {
            return doValidate_ExtStatus(U_ExtStatus);
        }
        public virtual string doValidate_ExtStatus(object oValue) {
            return base.doValidation(_ExtStatus, oValue);
        }

        /**
		 * Decription: External Test Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ExtTsDt
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
        public readonly static string _ExtTsDt = "U_ExtTsDt";
        public DateTime U_ExtTsDt {
            get {
                return getValueAsDateTime(_ExtTsDt);
            }
            set { setValue(_ExtTsDt, value); }
        }
        public void U_ExtTsDt_AsString(string value) {
            setValue("U_ExtTsDt", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_ExtTsDt_AsString() {
            DateTime dVal = getValueAsDateTime(_ExtTsDt);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_ExtTsDt() {
            return doValidate_ExtTsDt(U_ExtTsDt);
        }
        public virtual string doValidate_ExtTsDt(object oValue) {
            return base.doValidation(_ExtTsDt, oValue);
        }

        /**
		 * Decription: Import Results
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ImpResults
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _ImpResults = "U_ImpResults";
        public string U_ImpResults {
            get {
                return getValueAsString(_ImpResults);
            }
            set { setValue(_ImpResults, value); }
        }
        public string doValidate_ImpResults() {
            return doValidate_ImpResults(U_ImpResults);
        }
        public virtual string doValidate_ImpResults(object oValue) {
            return base.doValidation(_ImpResults, oValue);
        }

        /**
		 * Decription: Lab Item Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_LabICd
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _LabICd = "U_LabICd";
        public string U_LabICd {
            get {
                return getValueAsString(_LabICd);
            }
            set { setValue(_LabICd, value); }
        }
        public string doValidate_LabICd() {
            return doValidate_LabICd(U_LabICd);
        }
        public virtual string doValidate_LabICd(object oValue) {
            return base.doValidation(_LabICd, oValue);
        }

        /**
		 * Decription: Lab Item Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_LabINm
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _LabINm = "U_LabINm";
        public string U_LabINm {
            get {
                return getValueAsString(_LabINm);
            }
            set { setValue(_LabINm, value); }
        }
        public string doValidate_LabINm() {
            return doValidate_LabINm(U_LabINm);
        }
        public virtual string doValidate_LabINm(object oValue) {
            return base.doValidation(_LabINm, oValue);
        }

        /**
		 * Decription: Lab Item RowCd
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_LabItemRCd
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _LabItemRCd = "U_LabItemRCd";
        public string U_LabItemRCd {
            get {
                return getValueAsString(_LabItemRCd);
            }
            set { setValue(_LabItemRCd, value); }
        }
        public string doValidate_LabItemRCd() {
            return doValidate_LabItemRCd(U_LabItemRCd);
        }
        public virtual string doValidate_LabItemRCd(object oValue) {
            return base.doValidation(_LabItemRCd, oValue);
        }

        /**
		 * Decription: Object Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ObjectCd
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _ObjectCd = "U_ObjectCd";
        public string U_ObjectCd {
            get {
                return getValueAsString(_ObjectCd);
            }
            set { setValue(_ObjectCd, value); }
        }
        public string doValidate_ObjectCd() {
            return doValidate_ObjectCd(U_ObjectCd);
        }
        public virtual string doValidate_ObjectCd(object oValue) {
            return base.doValidation(_ObjectCd, oValue);
        }

        /**
		 * Decription: Object Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ObjectType
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _ObjectType = "U_ObjectType";
        public string U_ObjectType {
            get {
                return getValueAsString(_ObjectType);
            }
            set { setValue(_ObjectType, value); }
        }
        public string doValidate_ObjectType() {
            return doValidate_ObjectType(U_ObjectType);
        }
        public virtual string doValidate_ObjectType(object oValue) {
            return base.doValidation(_ObjectType, oValue);
        }

        /**
		 * Decription: Row Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_RowStatus
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _RowStatus = "U_RowStatus";
        public string U_RowStatus {
            get {
                return getValueAsString(_RowStatus);
            }
            set { setValue(_RowStatus, value); }
        }
        public string doValidate_RowStatus() {
            return doValidate_RowStatus(U_RowStatus);
        }
        public virtual string doValidate_RowStatus(object oValue) {
            return base.doValidation(_RowStatus, oValue);
        }

        /**
		 * Decription: Saved
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Saved
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _Saved = "U_Saved";
        public int U_Saved {
            get {
                return getValueAsInt(_Saved);
            }
            set { setValue(_Saved, value); }
        }
        public string doValidate_Saved() {
            return doValidate_Saved(U_Saved);
        }
        public virtual string doValidate_Saved(object oValue) {
            return base.doValidation(_Saved, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo() {
            if (moDBFields == null)
                moDBFields = new DBFields(25);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_AnalysisReq, "Analysis Requested", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Analysis Requested
            moDBFields.Add(_AssignedTo, "Assigned To", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Assigned To
            moDBFields.Add(_AtcAbsEntry, "Attachments", 4, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Attachments
            moDBFields.Add(_BPCode, "BP Code", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //BP Code
            moDBFields.Add(_BPName, "BP Name", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //BP Name
            moDBFields.Add(_Decision, "Decision", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Decision
            moDBFields.Add(_Department, "Department", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Department
            moDBFields.Add(_DipRtCd, "Disposal Route Code", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Disposal Route Code
            moDBFields.Add(_DocNum, "Header DocNum", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Header DocNum
            moDBFields.Add(_DtAnalysed, "Date Analysed", 11, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Date Analysed
            moDBFields.Add(_DtReceived, "Date Received", 12, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Date Received
            moDBFields.Add(_DtRequested, "Date Requested", 13, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Date Requested
            moDBFields.Add(_ExtComments, "External Comments", 14, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //External Comments
            moDBFields.Add(_ExtStatus, "External Status", 15, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //External Status
            moDBFields.Add(_ExtTsDt, "External Test Date", 16, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //External Test Date
            moDBFields.Add(_ImpResults, "Import Results", 17, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Import Results
            moDBFields.Add(_LabICd, "Lab Item Code", 18, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Lab Item Code
            moDBFields.Add(_LabINm, "Lab Item Name", 19, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Lab Item Name
            moDBFields.Add(_LabItemRCd, "Lab Item RowCd", 20, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //Lab Item RowCd
            moDBFields.Add(_ObjectCd, "Object Code", 21, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Object Code
            moDBFields.Add(_ObjectType, "Object Type", 22, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Object Type
            moDBFields.Add(_RowStatus, "Row Status", 23, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Row Status
            moDBFields.Add(_Saved, "Saved", 24, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Saved

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation() {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_AnalysisReq());
            doBuildValidationString(doValidate_AssignedTo());
            doBuildValidationString(doValidate_AtcAbsEntry());
            doBuildValidationString(doValidate_BPCode());
            doBuildValidationString(doValidate_BPName());
            doBuildValidationString(doValidate_Decision());
            doBuildValidationString(doValidate_Department());
            doBuildValidationString(doValidate_DipRtCd());
            doBuildValidationString(doValidate_DocNum());
            doBuildValidationString(doValidate_DtAnalysed());
            doBuildValidationString(doValidate_DtReceived());
            doBuildValidationString(doValidate_DtRequested());
            doBuildValidationString(doValidate_ExtComments());
            doBuildValidationString(doValidate_ExtStatus());
            doBuildValidationString(doValidate_ExtTsDt());
            doBuildValidationString(doValidate_ImpResults());
            doBuildValidationString(doValidate_LabICd());
            doBuildValidationString(doValidate_LabINm());
            doBuildValidationString(doValidate_LabItemRCd());
            doBuildValidationString(doValidate_ObjectCd());
            doBuildValidationString(doValidate_ObjectType());
            doBuildValidationString(doValidate_RowStatus());
            doBuildValidationString(doValidate_Saved());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_AnalysisReq)) return doValidate_AnalysisReq(oValue);
            if (sFieldName.Equals(_AssignedTo)) return doValidate_AssignedTo(oValue);
            if (sFieldName.Equals(_AtcAbsEntry)) return doValidate_AtcAbsEntry(oValue);
            if (sFieldName.Equals(_BPCode)) return doValidate_BPCode(oValue);
            if (sFieldName.Equals(_BPName)) return doValidate_BPName(oValue);
            if (sFieldName.Equals(_Decision)) return doValidate_Decision(oValue);
            if (sFieldName.Equals(_Department)) return doValidate_Department(oValue);
            if (sFieldName.Equals(_DipRtCd)) return doValidate_DipRtCd(oValue);
            if (sFieldName.Equals(_DocNum)) return doValidate_DocNum(oValue);
            if (sFieldName.Equals(_DtAnalysed)) return doValidate_DtAnalysed(oValue);
            if (sFieldName.Equals(_DtReceived)) return doValidate_DtReceived(oValue);
            if (sFieldName.Equals(_DtRequested)) return doValidate_DtRequested(oValue);
            if (sFieldName.Equals(_ExtComments)) return doValidate_ExtComments(oValue);
            if (sFieldName.Equals(_ExtStatus)) return doValidate_ExtStatus(oValue);
            if (sFieldName.Equals(_ExtTsDt)) return doValidate_ExtTsDt(oValue);
            if (sFieldName.Equals(_ImpResults)) return doValidate_ImpResults(oValue);
            if (sFieldName.Equals(_LabICd)) return doValidate_LabICd(oValue);
            if (sFieldName.Equals(_LabINm)) return doValidate_LabINm(oValue);
            if (sFieldName.Equals(_LabItemRCd)) return doValidate_LabItemRCd(oValue);
            if (sFieldName.Equals(_ObjectCd)) return doValidate_ObjectCd(oValue);
            if (sFieldName.Equals(_ObjectType)) return doValidate_ObjectType(oValue);
            if (sFieldName.Equals(_RowStatus)) return doValidate_RowStatus(oValue);
            if (sFieldName.Equals(_Saved)) return doValidate_Saved(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName) {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
		 * Link the Name Field to the Form Item.
		 */
        public void doLink_Name(string sControlName) {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
		 * Link the U_AnalysisReq Field to the Form Item.
		 */
        public void doLink_AnalysisReq(string sControlName) {
            moLinker.doLinkDataToControl(_AnalysisReq, sControlName);
        }
        /**
		 * Link the U_AssignedTo Field to the Form Item.
		 */
        public void doLink_AssignedTo(string sControlName) {
            moLinker.doLinkDataToControl(_AssignedTo, sControlName);
        }
        /**
		 * Link the U_AtcAbsEntry Field to the Form Item.
		 */
        public void doLink_AtcAbsEntry(string sControlName) {
            moLinker.doLinkDataToControl(_AtcAbsEntry, sControlName);
        }
        /**
		 * Link the U_BPCode Field to the Form Item.
		 */
        public void doLink_BPCode(string sControlName) {
            moLinker.doLinkDataToControl(_BPCode, sControlName);
        }
        /**
		 * Link the U_BPName Field to the Form Item.
		 */
        public void doLink_BPName(string sControlName) {
            moLinker.doLinkDataToControl(_BPName, sControlName);
        }
        /**
		 * Link the U_Decision Field to the Form Item.
		 */
        public void doLink_Decision(string sControlName) {
            moLinker.doLinkDataToControl(_Decision, sControlName);
        }
        /**
		 * Link the U_Department Field to the Form Item.
		 */
        public void doLink_Department(string sControlName) {
            moLinker.doLinkDataToControl(_Department, sControlName);
        }
        /**
		 * Link the U_DipRtCd Field to the Form Item.
		 */
        public void doLink_DipRtCd(string sControlName) {
            moLinker.doLinkDataToControl(_DipRtCd, sControlName);
        }
        /**
		 * Link the U_DocNum Field to the Form Item.
		 */
        public void doLink_DocNum(string sControlName) {
            moLinker.doLinkDataToControl(_DocNum, sControlName);
        }
        /**
		 * Link the U_DtAnalysed Field to the Form Item.
		 */
        public void doLink_DtAnalysed(string sControlName) {
            moLinker.doLinkDataToControl(_DtAnalysed, sControlName);
        }
        /**
		 * Link the U_DtReceived Field to the Form Item.
		 */
        public void doLink_DtReceived(string sControlName) {
            moLinker.doLinkDataToControl(_DtReceived, sControlName);
        }
        /**
		 * Link the U_DtRequested Field to the Form Item.
		 */
        public void doLink_DtRequested(string sControlName) {
            moLinker.doLinkDataToControl(_DtRequested, sControlName);
        }
        /**
		 * Link the U_ExtComments Field to the Form Item.
		 */
        public void doLink_ExtComments(string sControlName) {
            moLinker.doLinkDataToControl(_ExtComments, sControlName);
        }
        /**
		 * Link the U_ExtStatus Field to the Form Item.
		 */
        public void doLink_ExtStatus(string sControlName) {
            moLinker.doLinkDataToControl(_ExtStatus, sControlName);
        }
        /**
		 * Link the U_ExtTsDt Field to the Form Item.
		 */
        public void doLink_ExtTsDt(string sControlName) {
            moLinker.doLinkDataToControl(_ExtTsDt, sControlName);
        }
        /**
		 * Link the U_ImpResults Field to the Form Item.
		 */
        public void doLink_ImpResults(string sControlName) {
            moLinker.doLinkDataToControl(_ImpResults, sControlName);
        }
        /**
		 * Link the U_LabICd Field to the Form Item.
		 */
        public void doLink_LabICd(string sControlName) {
            moLinker.doLinkDataToControl(_LabICd, sControlName);
        }
        /**
		 * Link the U_LabINm Field to the Form Item.
		 */
        public void doLink_LabINm(string sControlName) {
            moLinker.doLinkDataToControl(_LabINm, sControlName);
        }
        /**
		 * Link the U_LabItemRCd Field to the Form Item.
		 */
        public void doLink_LabItemRCd(string sControlName) {
            moLinker.doLinkDataToControl(_LabItemRCd, sControlName);
        }
        /**
		 * Link the U_ObjectCd Field to the Form Item.
		 */
        public void doLink_ObjectCd(string sControlName) {
            moLinker.doLinkDataToControl(_ObjectCd, sControlName);
        }
        /**
		 * Link the U_ObjectType Field to the Form Item.
		 */
        public void doLink_ObjectType(string sControlName) {
            moLinker.doLinkDataToControl(_ObjectType, sControlName);
        }
        /**
		 * Link the U_RowStatus Field to the Form Item.
		 */
        public void doLink_RowStatus(string sControlName) {
            moLinker.doLinkDataToControl(_RowStatus, sControlName);
        }
        /**
		 * Link the U_Saved Field to the Form Item.
		 */
        public void doLink_Saved(string sControlName) {
            moLinker.doLinkDataToControl(_Saved, sControlName);
        }
        #endregion

    }
}
