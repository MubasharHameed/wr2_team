/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 19/04/2016 17:40:26
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User{
   [Serializable] 
	public class IDH_LABSTATS: com.isb.enq.dbObjects.Base.IDH_LABSTATS{ 

		public IDH_LABSTATS() : base() {
			
		}

   	public IDH_LABSTATS( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	}
	}
}
