/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 14/03/2016 12:48:17
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User{
   [Serializable] 
	public class IDH_LABTSKIMP: com.isb.enq.dbObjects.Base.IDH_LABTSKIMP{ 

		public IDH_LABTSKIMP() : base() {
			
		}

   	public IDH_LABTSKIMP( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
   	}

       public int getByLabTaskCode(string sLTCode)
    {
        string sWhere = "	" + _LabTskCd + " = '" + sLTCode + "'";
        return getData(sWhere, null);
    }

   }
}
