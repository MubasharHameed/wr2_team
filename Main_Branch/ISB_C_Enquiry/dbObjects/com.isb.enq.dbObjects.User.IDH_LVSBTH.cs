/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 08/11/2016 16:37:12
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.bridge;
using com.idh.bridge.lookups;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_LVSBTH : com.isb.enq.dbObjects.Base.IDH_LVSBTH {

        public enum en_BatchProcessStatus {
            NotSent = 0,
            SentToReview = 1,
            ReadyForProcessByService = 2,
            Processed = 3,

        };

        public IDH_LVSBTH() : base() {
            msAutoNumKey = "SEQLVSBD";
        }

        public IDH_LVSBTH(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm) : base(oIDHForm, oForm) {
            msAutoNumKey = "SEQLVSBD";
        }
        /// <summary>
        /// To Apply New Amendment Type on All Loaded Records in Object. You need to Update after this.
        /// </summary>
        /// <param name="sNewAmendmentList"></param>
        /// <returns></returns>
        public bool doApplyNewAmendAction(string sNewAmendmentList) {
            doBookmark();
            first();
            while (next()) {
                if (Code != string.Empty)
                    U_AmndTp = sNewAmendmentList;
            }
            doRecallBookmark();
            return true;
        }
        /// <summary>
        /// It will Process the batch.
        /// </summary>
        /// <returns></returns>
        public bool doProcessBatch(string sProcessParm) {
            try {
                int iRecs = 0;
                if (string.IsNullOrWhiteSpace(sProcessParm))
                    iRecs = getData(IDH_LVSBTH._ProcSts + " = " + ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString() + " And isnull(" + IDH_LVSBTH._AddedBy + ",'')='' And isnull(" + IDH_LVSBTH._BthStatus + ",'1')!='0'", "");
                else
                    iRecs = getData(IDH_LVSBTH._ProcSts + " = " + ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString() + " And isnull(" + IDH_LVSBTH._AddedBy + ",'')!='' And isnull(" + IDH_LVSBTH._BthStatus + ",'1')!='0'", "");
                DataHandler.INSTANCE.doInfo("MassPBIUpdater.doProcessBatch", "Info", ">>> No. of rows found to process=" + iRecs.ToString());
                if (iRecs == 0)
                    return true;
                //first();
                //while (next()) {
                //    if (U_ProcSts == ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString()) {
                //        IDH_PBI oPBI = null;
                //        bRet = doProcessBatchItem(ref oPBI);
                //        if (bRet) {
                //            bRet = doUpdatePrices();
                //            sPBICode+=U_PBI +",";
                //        }
                //    }
                //}
                //bRet = doProcessData();
                return true;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_LVSBTH.doProcessBatch" });
                return false;

            } finally {                
            }
            return true;
        }
        /// <summary>
        /// It will Process or will send for process to commander all loaded rows of batch now.
        /// </summary>
        /// <returns></returns>
        public bool doProcessBatch(bool SendToServiceForProcess) {
            try {
                bool bRet;
                doBookmark();
                first();
                while (next()) {
                    if (!SendToServiceForProcess) {
                        if (U_ProcSts == ((int)en_BatchProcessStatus.SentToReview).ToString() || U_ProcSts == ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString()) {
                            idh.dbObjects.User.IDH_PBI oPBI = null;
                            bRet = doProcessBatchItem(ref oPBI);
                            if (bRet && !SendToServiceForProcess) {
                                bRet = doUpdatePrices();
                            }
                            if (bRet == false) {
                                return false;
                            }
                        }
                        U_ProcSts = ((int)en_BatchProcessStatus.Processed).ToString();
                    } else if (U_ProcSts == ((int)en_BatchProcessStatus.SentToReview).ToString() && U_ProcSts != ((int)en_BatchProcessStatus.Processed).ToString()) {
                        U_ProcSts = ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString();
                    }
                }
                bRet = doProcessData();
                doRecallBookmark();
                return bRet;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_LVSBTH.doProcessBatch" });
                return false;
            }
        }
        public bool doProcessBatchItem(bool SendToServiceForProcess, ref idh.dbObjects.User.IDH_PBI oPBI) {
            try {
                bool bRet=true;
                //doBookmark();
                //first();
                //while (next()) {
                // if (!SendToServiceForProcess) {
                if (U_ProcSts == ((int)en_BatchProcessStatus.SentToReview).ToString() || U_ProcSts == ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString()) {
                    //IDH_PBI oPBI = null;
                    if (!SendToServiceForProcess) {
                        bRet = doProcessBatchItem(ref oPBI);
                        if (bRet) {
                            bRet = doUpdatePrices();
                        }
                    }

                    if (bRet && SendToServiceForProcess) {
                        U_ProcSts = ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString();
                    } else if (bRet && !SendToServiceForProcess) {
                        U_ProcSts = ((int)en_BatchProcessStatus.Processed).ToString();
                    }
                    if (bRet == false)
                        return false;
                }

                //} else if (U_ProcSts == ((int)en_BatchProcessStatus.SentToReview).ToString() && U_ProcSts != ((int)en_BatchProcessStatus.Processed).ToString()) {
                //    U_ProcSts = ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString();
                //}
                //}
                bRet = doUpdateDataRow();
                //doRecallBookmark();
                return bRet;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_LVSBTH.doProcessBatchItem" });
                return false;
            }
        }
        /// <summary>
        /// It will Update PBI Object however execution PBI need to be done in PBI_Handler.Update of current object need to perform in called class.
        /// </summary>
        /// <param name="sPBI"></param>
        /// <returns></returns>
        public bool doProcessByPBI(string sPBI) {
            int iRet = getData(IDH_LVSBTH._PBI + "=\'" + sPBI + "\' And " + IDH_LVSBTH._ProcSts + "=" + en_BatchProcessStatus.ReadyForProcessByService, " Cast( " + IDH_LVSBTH._BatchNum + " As Int) ");
            if (iRet == 0) {
                com.idh.bridge.DataHandler.INSTANCE.doResError("No record found for the PBI " + sPBI + " in ready to process by service.", "LSRER001",
                        new string[] { sPBI });
                return false;
            } else if (iRet > 1) {
                com.idh.bridge.DataHandler.INSTANCE.doResError("More than one record found for PBI " + sPBI + " in ready to process by service. Please check live service for batch#" + U_BatchNum + " and delete one of PBI from active batch.", "LSRER002",
                        new string[] { sPBI, U_BatchNum });
                return false;
            }
            idh.dbObjects.User.IDH_PBI oPBI = null;
            return doProcessBatchItem(ref oPBI);
        }
        #region "Prices"
        public bool doUpdatePrices() {
            bool bret = true;
            string sCustomerLinkedBPCode = "", sCustomerLinkedBPName = "";
            string sSupplierLinkedBPCode = "", sSupplierLinkedBPName = "";

         string   sItemGrpCode = (string)Config.INSTANCE.getValueFromOITM(U_ItemCd, "ItmsGrpCod");
            string sJobSpecficPrice = (string)Config.INSTANCE.getValueFromTablebyKey("[@IDH_JOBTYPE]", "U_SuppLc", "U_JobTp", U_JbTypeDs, " And U_ItemGrp=\'" + sItemGrpCode + "\' And (isnull(U_OrdCat,'')='Waste Order' or isnull(U_OrdCat,'')='') ", "");
            if (sItemGrpCode == null)
                sItemGrpCode = "";
            if (sJobSpecficPrice == null)
                sJobSpecficPrice = "";

            if (idh.dbObjects.User.IDH_CSITPR.doCalculateNUpdateCIPPrices(U_CustCd, U_CustNm, U_CustAdr, U_CusPCode, U_SuppCd, U_SuppNm, U_SupAddr, U_SupPCode,U_CMnSVst,U_CMnChg,
                U_CVarRFix,U_JbTypeDs,
                U_CMinChg,U_CMCgVol,U_CRminVol,U_CHaulage,U_EffDate,U_WastCd,U_WasteNm,U_ItemCd,U_ItemNm,U_UOM, sItemGrpCode, sJobSpecficPrice,U_Rebate) &&
           com.isb.dbObjects.User.IDH_SUITPR.doCalculateNUpdateSIPPrices(U_SuppCd, U_SuppNm, U_SupAddr, U_SupPCode, U_CustCd, U_CustNm, U_CustAdr, U_CusPCode, U_SMnSVst, U_SMnChg, U_SVarRFix,
            U_JbTypeDs,
            U_SMinChg, U_SMCgVol, U_SRminVol, U_SHaulage, U_EffDate,
            U_WastCd, U_WasteNm, U_ItemCd, U_ItemNm, U_UOM, sItemGrpCode, sJobSpecficPrice, U_Rebate,false)) {
                bret = true;
                if (U_Rebate!=null && U_Rebate == "Y") {
                    idh.dbObjects.User.LinkedBP oLinkedBP = Config.INSTANCE.doGetLinkedBP(U_CustCd);
                    if (oLinkedBP != null) {
                        sCustomerLinkedBPCode = oLinkedBP.LinkedBPCardCode;
                        sCustomerLinkedBPName = oLinkedBP.LinkedBPName;
                        if (sCustomerLinkedBPCode != null && sCustomerLinkedBPCode.Trim() != string.Empty) {
                            //IDH_SUITPR.doCalculateNUpdateRebateSIPPrices(sCustomerLinkedBPCode, sCustomerLinkedBPName, U_CustAdr, U_CusPCode, U_CustCd, U_CustNm,
                            //    U_CMnSVst, U_CMnChg, U_CVarRFix,
                            //     U_CMinChg, U_CMCgVol, U_CRminVol, U_CHaulage, U_EffDate,
                            //     U_JbTypeDs, U_WastCd, U_ItemCd, U_UOM,
                            //     U_WasteNm, U_ItemNm, sItemGrpCode, sJobSpecficPrice);
                            com.isb.dbObjects.User.IDH_SUITPR.doCalculateNUpdateSIPPrices(sCustomerLinkedBPCode, sCustomerLinkedBPName, "", "", U_CustCd, U_CustNm,
                                U_CustAdr, U_CusPCode, U_CMnSVst, U_CMnChg, U_CVarRFix,
                                U_JbTypeDs,
                                U_CMinChg, U_CMCgVol, U_CRminVol, U_CHaulage, U_EffDate,
                                U_WastCd, U_WasteNm, U_ItemCd, U_ItemNm, U_UOM, sItemGrpCode, sJobSpecficPrice, U_Rebate,true);
                        }
                    }

                    oLinkedBP = Config.INSTANCE.doGetLinkedBP(U_SuppCd);
                    if (oLinkedBP != null) {
                        sSupplierLinkedBPCode = oLinkedBP.LinkedBPCardCode;
                        sSupplierLinkedBPName = oLinkedBP.LinkedBPName;
                        if (sSupplierLinkedBPCode != null && sSupplierLinkedBPName.Trim() != string.Empty) {
                            bret = idh.dbObjects.User.IDH_CSITPR.doCalculateNUpdateRebateCIPPrices(sSupplierLinkedBPCode, sSupplierLinkedBPName, U_CustAdr, U_CusPCode, sCustomerLinkedBPCode, sCustomerLinkedBPName,
                            U_CMnSVst, U_CMnChg, U_CVarRFix,
                            U_CMinChg, U_CMCgVol, U_CRminVol, U_CHaulage,
                            U_SRminVol, U_JbTypeDs, U_EffDate,
                            U_UOM, U_WastCd, U_WasteNm, U_ItemCd, U_ItemNm, sItemGrpCode, sJobSpecficPrice,U_Qty, U_Rebate);
                        }
                    }
                } 
            } else
                return false;
            return bret;
        }
         
        #endregion
        /// <summary>
        /// Leave null PBI if you are calling from pre-loaded object of IDH_LVSBTH and it will load PBI object from IDH_LVSBTH object. 
        /// If will Update PBI object as well However execution of PBI from PBI_Handler will not be done.
        /// </summary>
        /// <param name="oPBI"></param>
        /// <param name="doProcessPBINow"></param>
        /// <returns></returns>
        public bool doProcessBatchItem(ref idh.dbObjects.User.IDH_PBI oPBI) {//, bool doProcessPBINow, bool doUpdateBatchItem) {
            try {
                if (oPBI == null) {
                    oPBI = new idh.dbObjects.User.IDH_PBI();
                    oPBI.UnLockTable = true;
                    //oPBI.doAddEmptyRow(true, true);
                }
                IDH_LVSAMD oActionObject = new IDH_LVSAMD();
                oActionObject.UnLockTable = true;
                string sAction = U_AmndTp;
                int iRet = oActionObject.getData(IDH_LVSAMD._AmdDesc + "=\'" + sAction + "\' And " + IDH_LVSAMD._Active + "=\'Y\'", "");
                if (iRet == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doResError("Amendment type " + sAction + " is not active/available in the Amendment table.", "LSRER003",
                        new string[] { sAction });
                    return false;
                }
                if (U_ProcSts == ((int)en_BatchProcessStatus.NotSent).ToString()) {
                    com.idh.bridge.DataHandler.INSTANCE.doResError("PBI#" + U_PBI + " is not been sent for review. You must send it for review before process.", "LSRER004",
                        new string[] { U_PBI });
                    return false;
                } else if (U_ProcSts == ((int)en_BatchProcessStatus.Processed).ToString()) {
                    com.idh.bridge.DataHandler.INSTANCE.doResError("PBI#" + U_PBI + " for batc# " + U_BatchNum + " has already processed.", "LSRER005",
                        new string[] { U_PBI });
                    return false;
                }
                if (!oActionObject.doCheckAmendListChange(IDH_LVSAMD.AddPBI)) {
                    if (U_PBI != oPBI.Code && oPBI.getByKey(U_PBI) == false) {
                        com.idh.bridge.DataHandler.INSTANCE.doResError("Failed to load PBI# " + U_PBI + ".", "LSRER006",
                           new string[] { "IDH_LVSBTH.doProcessBatchItem" });
                        return false;
                    }
                } //else if (doProcessPBINow) {
                if (oPBI.Count == 0)
                    oPBI.doAddEmptyRow(true, true);
                //}


                if (oActionObject.doCheckAmendListChange(IDH_LVSAMD.RemovePBI)) {
                    oPBI.U_IDHRECEN = 2;
                    oPBI.U_IDHRECED = U_EffDate;
                    //  return oPBI.doProcessData();
                }

                DateTime dtValue;
                bool doAddNewPBI = (oActionObject.doCheckAmendListChange(IDH_LVSAMD.AddPBI));
                if (doAddNewPBI) {
                    oPBI.U_IDHCCODE = U_CustCd;
                    oPBI.U_IDHCNAME = U_CustNm;
                    oPBI.U_IDHSADDR = U_CustAdr;
                    oPBI.U_IDHCOGRP = Config.Parameter("WODCOG");
                    object oAddressLine;
                    
                        oAddressLine = Config.INSTANCE.doLookupTableField("CRD1", "U_AddrsCd", "CardCode='" + U_CustCd.Replace("'", "''") + "' And Address='" + U_CustAdr.Replace("'", "''") + "' And AdresType='S'", "", true);
                        if (oAddressLine != null && oAddressLine.ToString() != string.Empty) {
                            oPBI.U_IDHSADLN = oAddressLine.ToString();
                        }
                    
                    oPBI.U_IDHDISCD = U_SuppCd;
                    oPBI.U_IDHDISDC = U_SuppNm;
                    oPBI.U_IDHDADDR = U_SupAddr;
                    if ( string.IsNullOrWhiteSpace(U_SupAdrCd)) {
                        oAddressLine = Config.INSTANCE.doLookupTableField("CRD1", "U_AddrsCd", "CardCode='" + U_SuppCd.Replace("'", "''") + "' And Address='" + U_SupAddr.Replace("'", "''") + "' And AdresType='S'", "", true);
                        if (oAddressLine != null && oAddressLine.ToString() != string.Empty) {
                            U_SupAdrCd= oAddressLine.ToString();
                            oPBI.U_IDHDADLN = U_SupAdrCd;
                        }
                    }
                    //oPBI.U_IDHDADLN= bridge.lookups.Config.INSTANCE.doGetAddress(.ParamaterWithDefault("PCPCSADR", "DEFAULT");

                    oPBI.U_IDHCOICD = U_ItemCd;
                    oPBI.U_IDHCOIDC = U_ItemNm;

                    oPBI.U_IDHWCICD = U_WastCd;
                    oPBI.U_IDHWCIDC = U_WasteNm;
                }
                if (doAddNewPBI || oActionObject.doCheckAmendListChange(IDH_LVSAMD.FrquencyType)) {
                    switch (U_FrqType) {
                        case "D":
                            oPBI.U_IDHRECUR = "1";//Daily
                            break;
                        case "W":
                            oPBI.U_IDHRECUR = "O";//Weekly
                            break;
                        case "M":
                            oPBI.U_IDHRECUR = "M";//monthly
                            break;
                        case "Y":
                            oPBI.U_IDHRECUR = "2";//monthly
                            break;
                    }
                }
                if (doAddNewPBI || oActionObject.doCheckAmendListChange(IDH_LVSAMD.FrquencyCount)) {
                    oPBI.U_IDHRACTW = U_FrqCount;
                }
                if (doAddNewPBI || oActionObject.doCheckAmendListChange(IDH_LVSAMD.Days)) {
                    oPBI.U_IDHRDMON2 = U_Mon!=null && U_Mon.ToUpper() == "Y" ? "Y" : "N";
                    oPBI.U_IDHRDTUE2 = U_Tue!=null && U_Tue.ToUpper() == "Y" ? "Y" : "N";
                    oPBI.U_IDHRDWED2 = U_Wed!=null && U_Wed.ToUpper() == "Y" ? "Y" : "N";
                    oPBI.U_IDHRDTHU2 = U_Thu!=null && U_Thu.ToUpper() == "Y" ? "Y" : "N";
                    oPBI.U_IDHRDFRI2 = U_Fri!=null && U_Fri.ToUpper() == "Y" ? "Y" : "N";
                    oPBI.U_IDHRDSAT2 = U_Sat!=null && U_Sat.ToUpper() == "Y" ? "Y" : "N";
                    oPBI.U_IDHRDSUN2 = U_Sun!=null && U_Sun.ToUpper() == "Y" ? "Y" : "N";

                    if (U_IDHRT != null)
                        oPBI.U_IDHRT = U_IDHRT;
                    if (U_IDHRCDM != null)
                        oPBI.U_IDHRCDM = U_IDHRCDM;
                    if (U_IDHRCDTU != null)
                        oPBI.U_IDHRCDTU = U_IDHRCDTU;
                    if (U_IDHRCDW != null)
                        oPBI.U_IDHRCDW = U_IDHRCDW;
                    if (U_IDHRCDTH != null)
                        oPBI.U_IDHRCDTH = U_IDHRCDTH;
                    if (U_IDHRCDF != null)
                        oPBI.U_IDHRCDF = U_IDHRCDF;
                    if (U_IDHRCDSA != null)
                        oPBI.U_IDHRCDSA = U_IDHRCDSA;
                    if (U_IDHRCDSU != null)
                        oPBI.U_IDHRCDSU = U_IDHRCDSU;
                    
                    //if (U_IDHRSQM != null)
                    oPBI.U_IDHRSQM = U_IDHRSQM;
                    //if (U_IDHRSQTU != null)
                    oPBI.U_IDHRSQTU = U_IDHRSQTU;
                    //if (U_IDHRSQW != null)
                    oPBI.U_IDHRSQW = U_IDHRSQW;
                    oPBI.U_IDHRSQTH = U_IDHRSQTH;
                    oPBI.U_IDHRSQF = U_IDHRSQF;
                    oPBI.U_IDHRSQSA = U_IDHRSQSA;
                    oPBI.U_IDHRSQSU = U_IDHRSQSU;
                }
                if (doAddNewPBI || oActionObject.doCheckAmendListChange(IDH_LVSAMD.Qty)) {
                    oPBI.U_IDHCOIQT = U_Qty;
                }
                if (doAddNewPBI || oActionObject.doCheckAmendListChange(IDH_LVSAMD.UOM)) {
                    oPBI.U_UOM = U_UOM;
                }
                if (doAddNewPBI || oActionObject.doCheckAmendListChange(IDH_LVSAMD.SupplierRef)) {
                    oPBI.U_CustRef = U_SuppRef;
                }
                if (doAddNewPBI || oActionObject.doCheckAmendListChange(IDH_LVSAMD.JobType)) {
                    oPBI.U_IDHJTYPE = U_JbTypeDs;
                }

                if (doAddNewPBI || oActionObject.doCheckAmendListChange(IDH_LVSAMD.AutoStartDate)) {
                    oPBI.U_AUTSTRT = U_AUTSTRT;
                    if (Config.ParameterAsBool("AUSTARTY", false))//only ISS is using
                        oPBI.U_AUTSTRT = "Y";
                }
                if (doAddNewPBI || oActionObject.doCheckAmendListChange(IDH_LVSAMD.AutoComplete)) {
                    if (U_AutoComp!=null && U_AutoComp.ToUpper()=="Y")
                        oPBI.U_AUTSTRT = U_AutoComp;
                    oPBI.U_AUTEND = U_AutoComp;
                }
               
                if (doAddNewPBI || oActionObject.doCheckAmendListChange(IDH_LVSAMD.Rebate)) {
                    if (Config.ParameterAsBool("MKISSRBT", false) && !string.IsNullOrWhiteSpace(U_Rebate) && U_Rebate.ToLower().Equals("y"))
                        oPBI.U_IDHOBLGT = "Yes";
                    oPBI.U_Rebate = U_Rebate;
                }
                if (doAddNewPBI || oActionObject.doCheckAmendListChange(IDH_LVSAMD.RouteComments)) {
                    oPBI.U_IDHCMT = U_IDHCMT;
                }

                if (doAddNewPBI || oActionObject.doCheckAmendListChange(IDH_LVSAMD.DriverComments)) {
                    oPBI.U_IDHDRVI = U_IDHDRVI;
                }
                if (doAddNewPBI || oActionObject.doCheckAmendListChange(IDH_LVSAMD.WTNDateFrom)) {
                    if (DateTime.TryParse(U_WTNFrom.ToString(), out dtValue)) {
                        oPBI.U_IDHWTNS = U_WTNFrom;
                    }
                }
                if (doAddNewPBI || oActionObject.doCheckAmendListChange(IDH_LVSAMD.WTNDateTo)) {
                    if (DateTime.TryParse(U_WTNTo.ToString(), out dtValue)) {
                        oPBI.U_IDHWTNE = U_WTNTo;
                    }
                }
                if (doAddNewPBI || oActionObject.doCheckAmendListChange(IDH_LVSAMD.Periodtype)) {
                    if (!string.IsNullOrWhiteSpace(U_PERTYP) && (U_PERTYP == "Weeks" || U_PERTYP == "Months" || U_PERTYP == "Quarts"))
                        oPBI.U_PERTYP = U_PERTYP;
                    else if(string.IsNullOrWhiteSpace(oPBI.U_PERTYP))
                        oPBI.U_PERTYP = "Months";
                    int iTemp;
                    if (int.TryParse(U_IDHRECFQ.ToString(), out iTemp) && U_IDHRECFQ > 0)
                        oPBI.U_IDHRECFQ = U_IDHRECFQ;
                    else if(oPBI.U_IDHRECFQ==0)
                        oPBI.U_IDHRECFQ = 1;
                }
                DateTime dtActionDate = DateTime.Today;
                if (DateTime.TryParse(U_EffDate.ToString(), out dtValue)) {
                    dtActionDate = U_EffDate;
                }
                if (doAddNewPBI) {
                    oPBI.U_IDHRECEN = 2;
                    if (DateTime.TryParse(U_EffDate.ToString(), out dtValue)) {
                        oPBI.U_IDHRECSD = U_EffDate;
                    }
                    oPBI.U_IDHRECED = U_EffDate.AddYears(10);
                     
                    if (string.IsNullOrWhiteSpace(oPBI.U_PERTYP))
                        oPBI.U_PERTYP = U_PERTYP;
                    if (oPBI.U_IDHRECFQ==0)
                        oPBI.U_IDHRECFQ = 1;

                    oPBI.U_IDHOWNER = DataHandler.INSTANCE.User;
                    oPBI.U_IDHBRNCH = bridge.lookups.Config.INSTANCE.doGetCurrentBranch();
                    oPBI.U_IDHDETAI = "Y";
                    oPBI.U_IDHUMONE = "Y";
                    if (string.IsNullOrWhiteSpace(oPBI.U_AUTSTRT))
                        oPBI.U_AUTSTRT = "N";
                    if (string.IsNullOrWhiteSpace(oPBI.U_AUTEND))
                        oPBI.U_AUTEND = "N";

                    oPBI.U_IDHFOC = "N";
                    oPBI.U_IDHCARCD = oPBI.U_IDHDISCD;
                    oPBI.U_IDHCARDC = oPBI.U_IDHDISDC;
                }
                oPBI.U_IDHRECOM = oPBI.doDescribeCoverage();


                bool bRet = true;
                //if (doProcessPBINow) {
                if (oPBI.doProcessData()) {
                    U_PBI = oPBI.Code;
                    bRet = true;
                    //bRet = oPBI.doUpdateWORSequences(U_EffDate);
                } else
                    return false;
                //}
                U_ProcSts = ((int)en_BatchProcessStatus.Processed).ToString();
                //if (doUpdateBatchItem)
                //    bRet = doUpdateDataRow();
                //doRecallBookmark();
                return bRet;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_LVSBTH.doProcessBatchItem" });
                return false;
            } finally {
            }
        }
         
       
    }
}
