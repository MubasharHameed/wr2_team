/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 06/12/2016 13:18:26
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.idh.dbObjects.numbers;
using com.isb.enq.dbObjects;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_WRADHD : com.isb.enq.dbObjects.Base.IDH_WRADHD {

        public enum en_BatchProcessStatus {
            NotSent = 0,
            SentToReview = 1,
            ReadyForProcessByService = 2,
            Processed = 3,

        };

        public IDH_WRADHD() : base() {
            msAutoNumKey = "SEQWRDBD";
        }

        public IDH_WRADHD(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm) : base(oIDHForm, oForm) {
            msAutoNumKey = "SEQWRDBD";
        }

        #region "Process Batch"
        public bool doProcessBatch(string sProcessParm) {
            try {
                bool bRet = true;
                int iRecs = 0;
                if (string.IsNullOrWhiteSpace(sProcessParm))
                    iRecs = getData(IDH_WRADHD._ProcSts + " = " + ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString() + " And isnull(" + IDH_WRADHD._AddedBy + ",'')='' And lower(isnull(" + IDH_WRADHD._AddtItm + ",''))!='y' And isnull(" + IDH_WRADHD._BthStatus + ",'1')!='0'"," Cast(" + IDH_WRADHD._BatchNum + " as bigint)," + IDH_WRADHD._SrNo );
                else
                    iRecs = getData(IDH_WRADHD._ProcSts + " = " + ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString() + " And isnull(" + IDH_WRADHD._AddedBy + ",'')!='' And lower(isnull(" + IDH_WRADHD._AddtItm + ",''))!='y' And isnull(" + IDH_WRADHD._BthStatus + ",'1')!='0'", " Cast(" + IDH_WRADHD._BatchNum + " as bigint)," + IDH_WRADHD._SrNo);
                DataHandler.INSTANCE.doInfo("AdHocWOR.doProcessBatch", "Info", ">>> No. of rows found to process=" + iRecs.ToString());
                if (iRecs == 0)
                    return true;
                first();
                while (next()) {
                    bRet = true;
                    if (U_ProcSts == ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString()) {
                        bRet = doProcessBatchItem();
                    }
                }
                return true;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_WRADHD.doProcessBatch" });
                return false;
            }
        }
        public bool doProcessBatch(bool SendToServiceForProcess) {
            try {
                bool bRet = true;
                doBookmark();
                first();
                while (next()) {
                    bRet = true;
                    if (U_AddtItm == null || U_AddtItm.ToUpper() != "Y") {
                        if (!SendToServiceForProcess) {
                            if (U_ProcSts == ((int)en_BatchProcessStatus.SentToReview).ToString() || U_ProcSts == ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString()) {
                                if (bRet)
                                    bRet = doProcessBatchItem();

                            }
                        } else if (U_ProcSts == ((int)en_BatchProcessStatus.SentToReview).ToString() && U_ProcSts != ((int)en_BatchProcessStatus.Processed).ToString()) {
                            U_ProcSts = ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString();
                            bRet = doProcessData();
                            if (bRet == false) {
                                return false;
                            }
                        }
                    }
                }
                doRecallBookmark();
                return bRet;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_WRADHD.doProcessBatch" });
                return false;
            }
        }
        public bool doProcessBatchItem(bool SendToServiceForProcess) {
            try {
                bool bRet = true;
                if (!SendToServiceForProcess) {
                    if (U_AddtItm == null || U_AddtItm.ToUpper() != "Y") {
                        if (U_ProcSts == ((int)en_BatchProcessStatus.SentToReview).ToString() || U_ProcSts == ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString()) {
                            if (bRet)
                                bRet = doProcessBatchItem();
                        }
                    }
                } else {
                    U_ProcSts = ((int)en_BatchProcessStatus.ReadyForProcessByService).ToString();
                    bRet = doUpdateDataRow();
                }
                return bRet;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_WRADHD.doProcessBatchItem" });
                return false;
            }
        }

        private bool doProcessBatchItem() {
            bool bRet = true;
            try {

                string sWOHNumbers = "";
                bool bContinue = true;
                bContinue =doCreateWOH(ref sWOHNumbers, Code);
                if (bContinue && !string.IsNullOrWhiteSpace(sWOHNumbers)) {//if Same Sr No in next row then create WOR under same WOH
                    int srNo = U_SrNo;
                    string sBatchNum = U_BatchNum;
                    string sCode = Code;
                    string sWOH = U_WOH;// sWOHNumbers;
                    if (srNo != 0) {
                        doBookmark();
                        //int iRet = getData(IDH_WRADHD._BatchNum + "\'" + sBatchNum + "\' And " + IDH_WRADHD._SrNo + "=" + srNo + " And Upper(isnull(" + IDH_WRADHD._AddtItm + ",''))!='Y'", IDH_WRADHD._SrNo);
                        while (bContinue  && next()) {
                            if ((string.IsNullOrWhiteSpace(U_WOH)) && U_SrNo == srNo && U_BatchNum == sBatchNum ) {
                                if ((U_AddtItm == null || U_AddtItm.ToUpper() != "Y")) {
                                    U_WOH = sWOH;
                                    //bContinue= doUpdateDataRow();
                                    bContinue = doCreateWOH(ref sWOHNumbers, Code);
                                }
                                //bContinue = doCreateWOR(ref moWOH, ref _moBatchDetail);
                            } else {
                                break;
                            }
                        }
                        doRecallBookmark();
                    }
                }
                return bContinue;
                //}
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_WRADHD.doProcessBatchItem" });
                return false;
            }
            return bRet;
        }

        public bool doUpdatePrices() {
            bool bret = true;
            //string sCustomerLinkedBPCode = "", sCustomerLinkedBPName = "";
            //string sCarrierLinkedBPCode = "", sCarrierLinkedBPName = "";

            // if (IDH_CSITPR.doCalculateNUpdateCIPPrices(U_CustCd, U_CustNm, U_CustAdr, U_CusPCode, U_SuppCd, U_SuppNm, U_SupAddr, U_SupPCode, U_CMnSVst, U_CMnChg,
            //     U_CVarRFix, U_JbTypeDs,
            //     U_CMinChg, U_CMCgVol, U_CRminVol, U_CHaulage, U_RDate, U_WastCd, U_WasteNm, U_ItemCd, U_ItemNm, U_UOM) &&
            //IDH_SUITPR.doCalculateNUpdateSIPPrices(U_SuppCd, U_SuppNm, U_SupAddr, U_SupPCode, U_CustCd, U_CustNm, U_CustAdr, U_CusPCode, U_SMnSVst, U_SMnChg, U_SVarRFix,
            // U_JbTypeDs,
            // U_SMinChg, U_SMCgVol, U_SRminVol, U_SHaulage, U_RDate,
            // U_WastCd, U_WasteNm, U_ItemCd, U_ItemNm, U_UOM)) {
            //     bret = true;
            //     if (U_Rebate == "Y") {
            //         LinkedBP oLinkedBP = Config.INSTANCE.doGetLinkedBP(U_CustCd);
            //         if (oLinkedBP != null) {
            //             sCustomerLinkedBPCode = oLinkedBP.LinkedBPCardCode;
            //             sCustomerLinkedBPName = oLinkedBP.LinkedBPName;
            //             if (sCustomerLinkedBPCode != null && sCustomerLinkedBPCode.Trim() != string.Empty) {
            //                 IDH_SUITPR.doCalculateNUpdateRebateSIPPrices(sCustomerLinkedBPCode, sCustomerLinkedBPName, U_CustAdr, U_CusPCode, U_CustCd, U_CustNm,
            //                     U_CMnSVst, U_CMnChg, U_CVarRFix,
            //                      U_CMinChg, U_CMCgVol, U_CRminVol, U_CHaulage, U_RDate,
            //                      U_JbTypeDs, U_WastCd, U_ItemCd, U_UOM,
            //                      U_WasteNm, U_ItemNm);
            //             }
            //         }

            //         oLinkedBP = Config.INSTANCE.doGetLinkedBP(U_SuppCd);
            //         if (oLinkedBP != null) {
            //             sCarrierLinkedBPCode = oLinkedBP.LinkedBPCardCode;
            //             sCarrierLinkedBPName = oLinkedBP.LinkedBPName;
            //             if (sCarrierLinkedBPCode != null && sCarrierLinkedBPName.Trim() != string.Empty) {
            //                 bret = IDH_CSITPR.doCalculateNUpdateRebateCIPPrices(sCarrierLinkedBPCode, sCarrierLinkedBPName, U_SupAddr, U_SupPCode, U_SuppCd, U_SuppNm,
            //                 U_CMnSVst, U_CMnChg, U_CVarRFix,
            //                 U_CMinChg, U_CMCgVol, U_CRminVol, U_CHaulage,
            //                 U_SRminVol, U_JbTypeDs, U_RDate,
            //                 U_UOM, U_WastCd, U_WasteNm, U_ItemCd, U_ItemNm);
            //             }
            //         }
            //     }
            // } else
            //     return false;
            return bret;
        }
        #endregion
        #region Create WO
         
        public  bool doCreateWOH(ref string sWOHNums, string sBatchDetailCode) {
            // <param name="sOption">One= One WOH, Multiple=Each row will have its own WOH</param>
            bool bContinue = true;
            try {

                //IDH_WRADHD _moBatchDetail = new IDH_WRADHD();

                //_moBatchDetail.UnLockTable = true;
                //if (!_moBatchDetail.getByKey(sBatchDetailCode)) {
                //    DataHandler.INSTANCE.doInfo("Failed to load Batch RowNum#" + sBatchDetailCode);
                //}
                DataHandler.INSTANCE.doInfo("Processing Batch#" + U_BatchNum + ";RowNum#" + Code);

                if (!DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.StartTransaction();

                com.idh.dbObjects.User.IDH_JOBENTR moWOH = new com.idh.dbObjects.User.IDH_JOBENTR();
                if (!string.IsNullOrWhiteSpace(U_WOH)) {
                    bContinue = moWOH.getByKey(U_WOH);
                    if (bContinue && !string.IsNullOrWhiteSpace(U_Comments))
                        moWOH.U_SpInst += '\n' + U_Comments;
                } else if (!string.IsNullOrWhiteSpace(sWOHNums)) {
                    bContinue = moWOH.getByKey(sWOHNums);
                    if (bContinue && !string.IsNullOrWhiteSpace(U_Comments)) {
                        moWOH.U_SpInst += '\n' + U_Comments;
                        U_WOH = sWOHNums;
                    }
                } else {

                    moWOH.doSetCustomer(U_CustCd, U_CustAdr); //, true);
                    string sProducer = "";
                    idh.dbObjects.User.LinkedBP oLinkedBP = Config.INSTANCE.doGetLinkedBP(U_CustCd);
                    if (oLinkedBP != null && !string.IsNullOrWhiteSpace(oLinkedBP.LinkedBPCardCode)) {
                        sProducer = oLinkedBP.LinkedBPCardCode;

                    }
                    if (sProducer != "") {
                        moWOH.doSetProducer(sProducer, U_CustAdr); //, true);
                    }
                    moWOH.doSetCarrier(U_SuppCd); //, true);
                    if (Config.ParameterAsBool("SEPTIPFD", false)) {
                        if (!string.IsNullOrWhiteSpace(U_Tip)) {
                            if (string.IsNullOrWhiteSpace(U_SAddress))
                                moWOH.doSetDisposalSite(U_Tip, ""); //, false);
                            else
                                moWOH.doSetDisposalSite(U_Tip, U_SAddress); //, true);
                        }

                    } else
                        moWOH.doSetDisposalSite(U_SuppCd, U_SupAddr); //, true);

                    moWOH.U_SpInst = U_Comments;

                    moWOH.U_BDate = DateTime.Today;
                    moWOH.U_BTime = com.idh.sbo.utils.Dates.doTimeToNumStr();
                    moWOH.U_RSDate = U_RDate;

                    moWOH.U_User = Convert.ToInt16(com.idh.bridge.lookups.Config.INSTANCE.doGetUserId(idh.bridge.DataHandler.INSTANCE.User));
                    //Convert.ToInt16(DataHandler.INSTANCE.User);
                    string sServiceTypecode = "";
                    string sServiceTypeDesc = "";
                    bridge.lookups.Config.INSTANCE.GetWasteGroupByJobTypeNContainerItem(U_JbTypeDs, U_ItemCd, ref sServiceTypecode, ref sServiceTypeDesc);// _moWOQ.U_ItemGrp;
                    moWOH.U_ItemGrp = sServiceTypecode;

                    if (moWOH.doAddDataRow("By Adhoc Import Process") == false)
                        bContinue = false;
                }

                if (bContinue) {
                    U_WOH = moWOH.Code;
                    bContinue = doCreateWOR(ref moWOH );
                }
                if (bContinue) {
                    U_ProcSts = ((int)en_BatchProcessStatus.Processed).ToString();
                    sWOHNums = moWOH.Code;
                    bContinue = doProcessData();
                }
                if (bContinue) {
                    //bContinue = _moBatchDetail.doProcessData();//true,"",);
                    if (DataHandler.INSTANCE.IsInTransaction()) {
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    }
                    DataHandler.INSTANCE.doInfo("WOH#" + moWOH.Code + " added for Batch#" + U_BatchNum + ";RowNum#" + Code);
                    return true;
                }
                if (!bContinue && DataHandler.INSTANCE.IsInTransaction()) {
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    return false;
                }
                //doRecallBookmark();
                //moWOR.U_WOQLID = moWOH.Code;

            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: [Code:" + Code + "]" + e.ToString(), "Error Adding the WOR [Code:" + Code + "].");
                sWOHNums = "";
                if (DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doCreateWObyWOQ") });
                return false;

            } finally {
                if (bContinue && DataHandler.INSTANCE.IsInTransaction()) {
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                } else if (!bContinue && DataHandler.INSTANCE.IsInTransaction()) {
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                }
            }
            return bContinue;
        }
        public  bool doCreateWOR(ref idh.dbObjects.User.IDH_JOBENTR moWOH) {//, ref IDH_WRADHD _moBatchDetail
            bool bDOContinue = true;

            try {
                string sTime = com.idh.utils.Dates.doTimeToNumStr();
                IDH_JOBSHD moWOR;
                if (!string.IsNullOrEmpty(U_WOR)) {
                    //moWOR = new IDH_JOBSHD();//need to check which one work;
                    moWOR = new IDH_JOBSHD(moWOH);//need to check which one work;
                    moWOR.UnLockTable = true;
                    moWOR.MustLoadChildren = true;
                    if (!moWOR.getByKey(U_WOR)) {
                        DataHandler.INSTANCE.doInfo("Failed to load WOR#" + U_WOR + " in Batch#" + U_BatchNum + ", RowNum#" + Code);
                        return false;
                    }
                } else {
                    moWOR = new IDH_JOBSHD(moWOH);
                    moWOR.doAddEmptyRow(true, true);
                    string msNextRowNum = "JOBSCHE";
                    idh.dbObjects.numbers.NumbersPair oNumbers = DataHandler.INSTANCE.doGenerateCode(IDH_JOBSHD.AUTONUMPREFIX, msNextRowNum);
                    moWOR.Code = oNumbers.CodeCode;
                    moWOR.Name = oNumbers.NameCode;
                }


                //moWOR.getNewKey();
                moWOR.BlockChangeNotif = true;
                moWOR.U_JobNr = moWOH.Code;
                //moWOR.doApplyDefaults();
                //moWOR.U_JobNr = moWOH.Code;

                //moWOR.U_WOQID = moParentWOQ.Code;
                // moWOR.U_WOQLID = Code;
                //moWOR.U_RouteCd = U_RouteCd;
                //moWOR.U_RtCdCode = U_RtCdCode;

                moWOR.U_User = DataHandler.INSTANCE.User; //bridge.lookups.Config.INSTANCE.doGetUserCode(moParentWOQ.U_User.ToString());
                moWOR.U_Branch = Config.INSTANCE.doGetBranch(idh.bridge.DataHandler.INSTANCE.User);
                moWOR.U_JobTp = U_JbTypeDs;
                moWOR.U_ItemCd = U_ItemCd;
                moWOR.U_ItemDsc = U_ItemNm;
                string sServiceTypecode = "";
                string sServiceTypeDesc = "";
                bridge.lookups.Config.INSTANCE.GetWasteGroupByJobTypeNContainerItem(U_JbTypeDs, U_ItemCd, ref sServiceTypecode, ref sServiceTypeDesc);// _moWOQ.U_ItemGrp;

                moWOR.U_ItmGrp = sServiceTypecode;
                //moWOR.U_I = sServiceTypecode;
                moWOR.U_BDate = DateTime.Now;//moParentWOQ.U_BDate;
                moWOR.U_BTime = sTime;
                if (U_RDate == null || U_RDate.Year < 2000)
                    moWOR.U_RDate = DateTime.Now;//moParentWOQ.U_BDate;
                else
                    moWOR.U_RDate = U_RDate;//moParentWOQ.U_BDate;

                moWOR.U_RTime = sTime;
                moWOR.U_RTimeT = sTime;

                if (!(U_SDate == null || U_SDate.Year < 2000))
                    moWOR.U_ASDate = U_SDate;

                if (!(U_EDate == null || U_EDate.Year < 2000))
                    moWOR.U_AEDate = U_EDate;


                if (Config.Parameter("CTOFJBHR") != "" && Config.Parameter("CTOFJBHR") != "0" && Config.ParameterWithDefault("MAXJOBHR", "0") != "0") {
                    DateTime cuttoffhrs = Convert.ToDateTime(DateTime.Today.ToString("yyyy/MM/dd") + " " + Config.INSTANCE.getParameter("CTOFJBHR"));
                    DateTime BTime = DateTime.Now;
                    if (BTime.TimeOfDay <= cuttoffhrs.TimeOfDay) {
                        BTime = BTime.AddHours(Convert.ToInt32(Config.ParamaterWithDefault("MAXJOBHR", "0")));
                        moWOR.U_JbToBeDoneTm = com.idh.utils.Dates.doTimeToNumStr(BTime);
                        moWOR.U_JbToBeDoneDt = BTime;
                    } else {
                        //BTime = "08:00";
                        //if BDate is beyond the cuttof time then set to 8AM of next Day( Hayden to be confirm from Mike)
                        moWOR.U_JbToBeDoneTm = "800";
                        DateTime BDate = DateTime.Today.AddDays(1);
                        moWOR.U_JbToBeDoneDt = BDate;
                    }
                    //Else
                }
                //if (moWOH.U_NoChgTax == "Y") {
                //  moWOR.DoChargeVat = false;
                //}

                //   moWOR.U_ExpLdWgt = U_ExpLdWgt;
                //moWOR.U_MANPRC = U_MANPRC;
                /// moWOR.U_HaulAC = U_HaulAC;

                //moWOR.U_not= U_ExpLdWgt;

                moWOR.U_UOM = U_UOM;
                double dOutUOMFactor = Config.INSTANCE.doGetBaseWeightFactorsFromAny(U_UOM);
                if (dOutUOMFactor < 0) {
                    moWOR.U_UseWgt = "2";
                } else {
                    moWOR.U_UseWgt = "1";
                }
                //moWOR.U_TCharge = U_TCharge;
                //moWOR.U_CusChr = U_CusChr;
                if (Config.ParameterAsBool("SEPTIPFD", false)) {
                    if (!string.IsNullOrWhiteSpace(U_Tip)) {
                        moWOR.U_Tip = U_Tip;
                        moWOR.U_TipNm = U_TipNm;
                        if (string.IsNullOrWhiteSpace(U_SAddress)) {
                            moWOR.U_SAddress = "";
                        } else {
                            moWOR.U_SAddress = U_SAddress;
                        }
                    }

                } else {
                    moWOR.U_Tip = U_SuppCd;
                    moWOR.U_TipNm = U_SuppNm;
                    moWOR.U_SAddress = U_SupAddr;
                }

                //  moWOR.
                // moWOR.U_SAddress = U_SAddress;
                // moWOR.U_SAddrsLN = U_SAddrsLN;

                moWOR.U_CarrCd = U_SuppCd;
                moWOR.U_CarrNm = U_SuppNm;
                //moWOR.U_CarrNm = U_CarrNm;

                moWOR.U_PUOM = U_UOM;
                moWOR.U_ProUOM = U_UOM;
                //moWOR.U_TipCost = U_TipCost;
                //moWOR.U_OrdCost = U_OrdCost;



                // Add Rest of Values
                //moWOR.U_ASDate=U_ASDate
                //moWOR.U_ASTime=U_ASTime
                //moWOR.U_AEDate=U_AEDate
                //moWOR.U_AETime=U_AETime
                //moWOR.U_VehTyp=U_VehTyp
                //moWOR.U_Lorry=U_Lorry
                //moWOR.U_Driver=U_Driver
                //moWOR.U_DocNum=U_DocNum

                //moWOR.U_SupRef=U_SupRef
                //moWOR.U_TipWgt=U_TipWgt
                //moWOR.U_TipCost=U_TipCost
                //moWOR.U_TipTot=U_TipTot
                //moWOR.U_CstWgt=U_CstWgt
                //moWOR.U_TCharge=U_TCharge
                //moWOR.U_TCTotal=U_TCTotal
                //moWOR.U_CongCh=U_CongCh
                //moWOR.U_Weight=U_Weight
                //moWOR.U_Price=U_Price
                //moWOR.U_Discnt=U_Discnt
                //moWOR.U_DisAmt=U_DisAmt
                //moWOR.U_TaxAmt=U_TaxAmt
                //moWOR.U_Total=U_Total

                //moWOR.U_SLicNr=U_SLicNr
                //moWOR.U_SLicExp=U_SLicExp
                //moWOR.U_SLicCh=U_SLicCh
                //moWOR.U_ItmGrp=U_ItmGrp
                //moWOR.U_ItemCd=U_ItemCd
                //moWOR.U_ItemDsc=U_ItemDsc
                //if (string.IsNullOrEmpty(U_WastCd) && !string.IsNullOrEmpty(U_WastCd))
                //U_WasDsc = Config.INSTANCE.doGetItemDescriptionWithNoBuffer(U_WastCd);
                moWOR.U_WasCd = U_WastCd;
                moWOR.U_WasDsc = U_WasteNm;
                //moWOR.U_Serial=U_Serial
                moWOR.U_Status = com.idh.bridge.lookups.FixedValues.getStatusOpen();
                //moWOR.U_Quantity=U_Quantity
                //moWOR.U_JCost=U_JCost

                //moWOR.U_SAINV=U_SAINV
                //moWOR.U_SAORD=U_SAORD
                //moWOR.U_TIPPO=U_TIPPO
                //moWOR.U_JOBPO=U_JOBPO
                //moWOR.U_SLicSp=U_SLicSp
                //moWOR.U_Tip=U_Tip
                //moWOR.U_CustCd=U_CustCd
                //moWOR.U_CarrCd=U_CarrCd
                //moWOR.U_CongCd=U_CongCd
                //moWOR.U_TipNm=U_TipNm
                //moWOR.U_CustNm=U_CustNm
                //moWOR.U_CarrNm=U_CarrNm
                //moWOR.U_SLicNm=U_SLicNm
                //moWOR.U_SCngNm=U_SCngNm
                //moWOR.U_Dista=U_Dista
                //moWOR.U_CntrNo=U_CntrNo
                //moWOR.U_Wei1=U_Wei1
                //moWOR.U_Wei2=U_Wei2
                //moWOR.U_Ser1=U_Ser1
                //moWOR.U_Ser2=U_Ser2
                //moWOR.U_WDt1=U_WDt1
                //moWOR.U_WDt2=U_WDt2
                //moWOR.U_AddEx=U_AddEx
                moWOR.U_PStat = com.idh.bridge.lookups.FixedValues.getStatusOpen();
                //moWOR.U_OrdWgt=U_OrdWgt
                //moWOR.U_OrdCost=U_OrdCost
                //moWOR.U_OrdTot=U_OrdTot
                //moWOR.U_TRLReg=U_TRLReg
                //moWOR.U_TRLNM=U_TRLNM
                //moWOR.U_UOM=U_UOM
                //moWOR.U_WASLIC=U_WASLIC
                //moWOR.U_SLPO=U_SLPO
                //moWOR.U_SLicCst=U_SLicCst
                //moWOR.U_SLicCTo=U_SLicCTo
                //moWOR.U_SLicCQt=U_SLicCQt
                //moWOR.U_WROrd=U_WROrd
                //moWOR.U_WRRow=U_WRRow
                //moWOR.U_RTime=U_RTime
                //moWOR.U_RTimeT=U_RTimeT
                //moWOR.U_CusQty=U_CusQty
                //moWOR.U_CusChr=U_CusChr
                //moWOR.U_PayMeth=U_PayMeth
                //moWOR.U_CustRef=U_CustRef
                //moWOR.U_RdWgt=U_RdWgt
                //moWOR.U_LorryCd=U_LorryCd
                //moWOR.U_Covera=U_Covera
                moWOR.U_Comment = U_Comments;
                //moWOR.U_PUOM=U_PUOM
                moWOR.U_AUOM = U_UOM;
                //moWOR.U_AUOMQt=U_AUOMQt
                //moWOR.U_CCNum=U_CCNum
                //moWOR.U_CCStat=U_CCStat
                //moWOR.U_PayStat=U_PayStat
                //moWOR.U_CCType=U_CCType
                //moWOR.U_PARCPT=U_PARCPT
                //moWOR.U_ENREF=U_ENREF
                //moWOR.U_JobRmD=U_JobRmD
                //moWOR.U_JobRmT=U_JobRmT
                //moWOR.U_RemNot=U_RemNot
                //moWOR.U_RemCnt=U_RemCnt
                //moWOR.U_TZone=U_TZone
                //moWOR.U_CoverHst=U_CoverHst
                //moWOR.U_ProPO=U_ProPO
                //moWOR.U_ProCd=U_ProCd
                //moWOR.U_ProNm=U_ProNm
                //moWOR.U_User=U_User
                //moWOR.U_Branch=U_Branch
                //moWOR.U_TAddChrg=U_TAddChrg
                //moWOR.U_TAddCost=U_TAddCost
                //moWOR.U_IssQty=U_IssQty
                //  moWOR.U_Obligated = U_Obligated;
                //moWOR.U_Origin=U_Origin
                //moWOR.U_TChrgVtRt = U_TChrgVtRt;
                //moWOR.U_HChrgVtRt = U_HChrgVtRt;
                //moWOR.U_TCostVtRt = U_TCostVtRt;
                //moWOR.U_HCostVtRt = U_HCostVtRt;
                //moWOR.U_TChrgVtGrp = U_TChrgVtGrp;
                //moWOR.U_HChrgVtGrp = U_HChrgVtGrp;
                //moWOR.U_TCostVtGrp = U_TCostVtGrp;
                //moWOR.U_HCostVtGrp = U_HCostVtGrp;

                //moWOR.U_VtCostAmt=U_VtCostAmt
                //moWOR.U_CustCs=U_CustCs
                //moWOR.U_ConNum=U_ConNum
                //moWOR.U_CCExp=U_CCExp
                //moWOR.U_CCIs=U_CCIs
                //moWOR.U_CCSec=U_CCSec
                //moWOR.U_CCHNum=U_CCHNum
                //moWOR.U_CCPCd=U_CCPCd
                //moWOR.U_IntComm=U_IntComm
                //moWOR.U_ContNr=U_ContNr
                //moWOR.U_SealNr=U_SealNr
                //moWOR.U_FPCoH=U_FPCoH
                //moWOR.U_FPCoT=U_FPCoT
                //moWOR.U_FPChH=U_FPChH
                //moWOR.U_FPChT=U_FPChT
                //moWOR.U_WastTNN=U_WastTNN
                //moWOR.U_HazWCNN=U_HazWCNN

                moWOR.U_CustRef = U_SuppRef;

                //moWOR.U_ExtWeig=U_ExtWeig
                //moWOR.U_BDate=U_BDate
                //moWOR.U_BTime=U_BTime
                //moWOR.U_CarrReb=U_CarrReb
                //moWOR.U_TPCN=U_TPCN
                //moWOR.U_MANPRC=U_MANPRC
                //moWOR.U_ENNO=U_ENNO
                //moWOR.U_SemSolid=U_SemSolid
                //moWOR.U_PaMuPu=U_PaMuPu
                //moWOR.U_Dusty=U_Dusty
                //moWOR.U_Fluid=U_Fluid
                //moWOR.U_Firm=U_Firm
                //moWOR.U_Consiste=U_Consiste
                //moWOR.U_TypPTreat=U_TypPTreat
                //moWOR.U_DAAttach=U_DAAttach
                //moWOR.U_LnkPBI=U_LnkPBI
                //moWOR.U_DPRRef=U_DPRRef
                //moWOR.U_MDChngd=U_MDChngd
                //moWOR.U_Sched=U_Sched
                //moWOR.U_USched=U_USched
                //moWOR.U_Analys1=U_Analys1
                //moWOR.U_Analys2=U_Analys2
                //moWOR.U_Analys3=U_Analys3
                //moWOR.U_Analys4=U_Analys4
                //moWOR.U_Analys5=U_Analys5
                //  moWOR.U_CustReb = U_Rebate;// U_CustReb;
                //moWOR.U_TCCN = U_TCCN;
                moWOR.U_RowSta = com.idh.bridge.lookups.FixedValues.getStatusOpen();
                //moWOR.U_ActComm=U_ActComm
                //moWOR.U_HlSQty=U_HlSQty
                //moWOR.U_HaulAC=U_HaulAC
                //moWOR.U_ClgCode=U_ClgCode
                //moWOR.U_ProRef=U_ProRef
                //moWOR.U_SerialC=U_SerialC
                //moWOR.U_PAUOMQt=U_PAUOMQt
                //moWOR.U_PRdWgt=U_PRdWgt
                //moWOR.U_ProWgt=U_ProWgt
                //moWOR.U_ProUOM=U_ProUOM
                //moWOR.U_PCost=U_PCost
                //moWOR.U_PCTotal=U_PCTotal
                //moWOR.U_WgtDed=U_WgtDed
                //moWOR.U_AdjWgt=U_AdjWgt
                //moWOR.U_ValDed=U_ValDed
                //moWOR.U_BookIn=U_BookIn
                //moWOR.U_ProGRPO=U_ProGRPO
                //moWOR.U_GRIn=U_GRIn
                //moWOR.U_CarrRef=U_CarrRef
                //moWOR.U_PCharge=U_PCharge
                //moWOR.U_CustGR=U_CustGR
                //moWOR.U_IDHSEQ=U_IDHSEQ
                //moWOR.U_IDHCMT=U_IDHCMT
                //moWOR.U_IDHDRVI=U_IDHDRVI
                //moWOR.U_IDHCHK=U_IDHCHK
                //moWOR.U_IDHPRTD=U_IDHPRTD
                //moWOR.U_IDHRTCD=U_IDHRTCD
                //moWOR.U_IDHRTDT=U_IDHRTDT
                //moWOR.U_LoadSht=U_LoadSht
                //moWOR.U_SODlvNot=U_SODlvNot
                //moWOR.U_ExpLdWgt=U_ExpLdWgt
                //moWOR.U_IsTrl=U_IsTrl
                //moWOR.U_UseWgt=U_UseWgt
                //moWOR.U_TFSMov=U_TFSMov
                //moWOR.U_PayTrm=U_PayTrm
                //moWOR.U_JStat=U_JStat
                //moWOR.U_Jrnl=U_Jrnl
                //moWOR.U_SAddress=U_SAddress
                //moWOR.U_LckPrc=U_LckPrc
                //moWOR.U_CBICont=U_CBICont
                //moWOR.U_CBIManQty=U_CBIManQty
                //moWOR.U_CBIManUOM=U_CBIManUOM
                //moWOR.U_ExtComm1=U_ExtComm1
                //moWOR.U_ExtComm2=U_ExtComm2
                moWOR.U_MaximoNum = U_MaximoNum;
                //moWOR.U_TFSReq=U_TFSReq
                //moWOR.U_TFSNum=U_TFSNum
                //moWOR.U_TRdWgt=U_TRdWgt
                //moWOR.U_PrcLink=U_PrcLink
                //moWOR.U_TAUOMQt=U_TAUOMQt

                moWOR.U_Rebate = U_Rebate;
                //moWOR.U_TAddChVat=U_TAddChVat
                //moWOR.U_TAddCsVat=U_TAddCsVat
                //moWOR.U_AddCharge=U_AddCharge
                //moWOR.U_AddCost=U_AddCost
                //moWOR.U_ReqArch=U_ReqArch
                //moWOR.U_JbToBeDoneDt=U_JbToBeDoneDt
                //moWOR.U_JbToBeDoneTm=U_JbToBeDoneTm
                //moWOR.U_DrivrOnSitDt=U_DrivrOnSitDt
                //moWOR.U_DrivrOnSitTm=U_DrivrOnSitTm
                //moWOR.U_DrivrOfSitDt=U_DrivrOfSitDt
                //moWOR.U_DrivrOfSitTm=U_DrivrOfSitTm
                //moWOR.U_AltWasDsc=U_AltWasDsc
                //moWOR.U_OTComments=U_OTComments
                //moWOR.U_TrnCode=U_TrnCode
                //moWOR.U_UseBPWgt=U_UseBPWgt
                //moWOR.U_BPWgt=U_BPWgt
                //moWOR.U_CarWgt=U_CarWgt
                //moWOR.U_PROINV=U_PROINV
                //moWOR.U_WHTRNF=U_WHTRNF
                //moWOR.U_ProPay=U_ProPay
                //moWOR.U_CustPay=U_CustPay
                //moWOR.U_WROrd2=U_WROrd2
                //moWOR.U_WRRow2=U_WRRow2
                //moWOR.U_DispCgCc = U_DispCgCc;
                //moWOR.U_CarrCgCc = U_CarrCgCc;
                //moWOR.U_PurcCoCc = U_PurcCoCc;
                //moWOR.U_DispCoCc = U_DispCoCc;
                //moWOR.U_CarrCoCc = U_CarrCoCc;
                //moWOR.U_LiscCoCc = U_LiscCoCc;
                //moWOR.U_SAddrsLN=U_SAddrsLN
                //moWOR.U_FTOrig=U_FTOrig
                //moWOR.U_WOQID=U_WOQID
                //moWOR.U_WOQLID=U_WOQLID
                moWOR.doSetLongituteLatitute();
                moWOR.doSetWasteItemGroup();
                doAddAdditionalItems(ref moWOR);
                moWOR.BlockChangeNotif = false;
                if (!string.IsNullOrWhiteSpace(moWOR.U_UseWgt) && moWOR.U_UseWgt == "1") {
                    moWOR.U_RdWgt = U_Qty;
                } else {
                    moWOR.U_AUOMQt = U_Qty;
                }

                if (string.IsNullOrWhiteSpace(U_Rebate) || U_Rebate.ToUpper() != "Y") {
                    moWOR.setUserChanged(Config.MASK_TIPCHRG, false);
                    //moWOR.doMarkUserUpdate(IDH_JOBSHD._TCharge,);
                    moWOR.setUserChanged(Config.MASK_TIPCHRGWGT, false);
                    //moWOR.doMarkUserUpdate(IDH_JOBSHD._CstWgt);

                    //moWOR.doMarkUserUpdate(IDH_JOBSHD._TipCost);
                    moWOR.setUserChanged(Config.MASK_TIPCST, false);

                    moWOR.setUserChanged(Config.MASK_TIPCSTWGT, false);
                    //moWOR.doMarkUserUpdate(IDH_JOBSHD._TipWgt);
                }
                double tempdbl;
                if (!string.IsNullOrWhiteSpace(U_ExpLdWgt) && double.TryParse(U_ExpLdWgt, out tempdbl)) {
                    moWOR.U_ExpLdWgt = Convert.ToDouble(U_ExpLdWgt);
                }
                if (!string.IsNullOrWhiteSpace(U_PUOM)) {
                    moWOR.BlockChangeNotif = true;
                    moWOR.U_PUOM = U_PUOM;
                    moWOR.U_ProUOM = U_PUOM;
                    moWOR.BlockChangeNotif = false;
                }
                if (!string.IsNullOrWhiteSpace(U_HlgQty) && double.TryParse(U_HlgQty, out tempdbl)) {
                    moWOR.U_CusQty = Convert.ToDouble(U_HlgQty);
                    moWOR.U_HlSQty = Convert.ToDouble(U_HlgQty);
                    moWOR.U_CarWgt = Convert.ToDouble(U_HlgQty);
                    moWOR.U_OrdWgt = Convert.ToDouble(U_HlgQty);
                }

                if (Config.ParameterAsBool("SYRDWAUM", false) && moWOR.U_UseWgt == "2") {
                    moWOR.BlockChangeNotif = true;
                    if (moWOR.U_Rebate != "Y") {
                        moWOR.U_RdWgt = moWOR.U_AUOMQt;
                        moWOR.U_TRdWgt = moWOR.U_TAUOMQt;
                    } else {
                        moWOR.U_RdWgt = 0;
                        moWOR.U_AUOMQt = 0;
                        moWOR.U_PRdWgt = moWOR.U_PAUOMQt;
                    }
                    moWOR.BlockChangeNotif = false;
                } else if (Config.ParameterAsBool("SYRDWAUM", false) && moWOR.U_UseWgt == "1") {
                    moWOR.BlockChangeNotif = true;
                    if (moWOR.U_Rebate != "Y") {
                        moWOR.U_AUOMQt = moWOR.U_RdWgt;
                        moWOR.U_TAUOMQt = moWOR.U_TRdWgt;
                    } else {
                        moWOR.U_RdWgt = 0;
                        moWOR.U_AUOMQt = 0;
                        moWOR.U_TRdWgt = 0;
                        moWOR.U_PAUOMQt = moWOR.U_PRdWgt;
                    }
                    moWOR.BlockChangeNotif = false;


                }

                //moWOR.U_CusQty= U_Qty;
                //moWOR.U_CarWgt= U_Qty;


                //moWOR.U_PRdWgt = U_Qty;
                //moWOR.U_TRdWgt = U_Qty;
                if (string.IsNullOrWhiteSpace(U_TChgPCst) || string.IsNullOrWhiteSpace(U_CusChr))
                    moWOR.doGetChargePrices(true);

                if (string.IsNullOrWhiteSpace(U_TipCost) || string.IsNullOrWhiteSpace(U_OrdCost))
                    moWOR.doGetCostPrices(true, true, true, false);//2017-05-26

                if (U_Rebate == null || U_Rebate.ToUpper() != "Y") {
                    if (Config.ParameterAsBool("MKISSRBT", false))
                        moWOR.U_Obligated = "";

                    if (!string.IsNullOrWhiteSpace(moWOR.U_ProCd)) {
                        idh.dbObjects.User.LinkedBP olinkbp = Config.INSTANCE.doGetLinkedBP(moWOR.U_CustCd);
                        if (olinkbp != null && olinkbp.LinkedBPCardCode == moWOR.U_ProCd) {
                            //moWOR.U_ProCd = "";
                            //moWOR.U_ProNm = "";
                            moWOR.U_PRdWgt = 0;
                            moWOR.U_ProWgt = 0;
                            moWOR.U_PAUOMQt = 0;
                            moWOR.U_PCost = 0;//as a non rebate item, purchase cost is zero for linked BP
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(U_TChgPCst) || !string.IsNullOrWhiteSpace(U_CusChr)) {
                        if (!string.IsNullOrWhiteSpace(U_TChgPCst)) {
                            moWOR.U_TCharge = Convert.ToDouble(U_TChgPCst);
                            moWOR.doMarkUserUpdate(IDH_JOBSHD._TCharge);
                            moWOR.CCTChr = "VARIABLE";
                            moWOR.TOffset = 0;
                            //moWOR.doGetChargePrices(true);
                        }
                        if (!string.IsNullOrWhiteSpace(U_CusChr)) {
                            moWOR.U_CusChr = Convert.ToDouble(U_CusChr);
                            moWOR.doMarkUserUpdate(IDH_JOBSHD._CusChr);
                            //moWOR.doGetChargePrices(true);
                            moWOR.CCHChr = "STANDARD";
                            moWOR.HOffset =0;
                        }
                        moWOR.doCalculateChargeVat('S');
                        moWOR.doCalculateChargeTotals('S');
                    }
                    if (!string.IsNullOrWhiteSpace(U_TipCost) || !string.IsNullOrWhiteSpace(U_OrdCost)) {
                        if (!string.IsNullOrWhiteSpace(U_TipCost)) {
                            moWOR.U_TipCost = Convert.ToDouble(U_TipCost);
                         //   moWOR.dispo
                            moWOR.doMarkUserUpdate(IDH_JOBSHD._TipCost);
                            moWOR.CCTCos = "VARIABLE";
                            moWOR.TCstOffset = 0;
                            //moWOR.doGetChargePrices(true);
                        }
                        if (!string.IsNullOrWhiteSpace(U_OrdCost)) {
                            moWOR.U_OrdCost = Convert.ToDouble(U_OrdCost);
                            moWOR.doMarkUserUpdate(IDH_JOBSHD._OrdCost);
                            moWOR.CCHCos = "VARIABLE";
                            moWOR.HCstOffset = 0;
                            // moWOR.dispo
                            //moWOR.doGetChargePrices(true);
                        }
                        moWOR.doCalculateCostVat('S');
                        moWOR.doCalculateCostTotals('C');

                    }
                } else {//for rebate WOR
                    if (Config.ParameterAsBool("MKISSRBT", false))
                        moWOR.U_Obligated = "Yes";

                    moWOR.U_TCharge = 0;
                    moWOR.doMarkUserUpdate(IDH_JOBSHD._TCharge);
                    moWOR.U_CstWgt = 0;
                    moWOR.doMarkUserUpdate(IDH_JOBSHD._CstWgt);

                    moWOR.U_TipCost = 0;
                    moWOR.doMarkUserUpdate(IDH_JOBSHD._TipCost);
                    moWOR.U_TipWgt = 0;
                    moWOR.doMarkUserUpdate(IDH_JOBSHD._TipWgt);
                    //based on phone discuss @28-04-2017 18:45PM with HZ
                    moWOR.U_TAUOMQt = 0;
                    moWOR.doMarkUserUpdate(IDH_JOBSHD._TAUOMQt);

                    if (!string.IsNullOrWhiteSpace(U_CusChr)) {
                        moWOR.U_CusChr = Convert.ToDouble(U_CusChr);
                        moWOR.doMarkUserUpdate(IDH_JOBSHD._CusChr);

                    }
                    moWOR.doCalculateChargeVat('S');
                    moWOR.doCalculateChargeTotals('C');

                    if (!string.IsNullOrWhiteSpace(U_OrdCost)) {
                        moWOR.U_OrdCost = Convert.ToDouble(U_OrdCost);
                        moWOR.doMarkUserUpdate(IDH_JOBSHD._OrdCost);
                    }

                    if (!string.IsNullOrWhiteSpace(U_TChgPCst)) {
                        moWOR.U_PCost = Convert.ToDouble(U_TChgPCst);
                        moWOR.doMarkUserUpdate(IDH_JOBSHD._PCost);
                        //moWOR.doGetChargePrices(true);
                        //moWOR.doCalculateCostVat('S');
                        //moWOR.doCalculateCostTotals('S');

                    }
                    if (!string.IsNullOrWhiteSpace(U_OrdCost) || !string.IsNullOrWhiteSpace(U_TChgPCst)) {
                        moWOR.doCalculateCostVat('S');
                        moWOR.doCalculateCostTotals('C');
                    }
                }


                moWOR.doCalculateTotalsWithOther();
                //moWOR.doCalculateChargeVat('R');
                //moWOR.doCalculateCostVat('R');
                //moWOR.doCalculateTotals();
                moWOR.doCalculateProfit();
                moWOR.AddLabTaskFlagIfRequired(U_WastCd);
                moWOR.U_AHocBtNm = U_BatchNum;
                if (moWOR.hasAddedRows())
                    bDOContinue = moWOR.doAddDataRow();
                else
                    bDOContinue = moWOR.doUpdateDataRow();
                if (bDOContinue) {
                    //U_WOHID = moWOH.Code;
                    //U_WORID = moWOR.Code;
                    //U_Status = ((int)en_WOQITMSTATUS.WOCreated).ToString();
                    //bDOContinue = doUpdateDataRow(false, "");
                    U_WOR = moWOR.Code;
                }
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: [Code:" + Code + "]" + e.ToString(), "Error Adding the WOR [Code:" + Code + "].");
                //sWOHNums = "";
                if (DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doCreateWObyAdHocWOH") });
                return false;

            } finally {
            }
            return true;
        }
        public  bool doAddAdditionalItems( ref IDH_JOBSHD moWOR) {
            if (moWOR.AdditionalExpenses.Count > 0) {
                moWOR.AdditionalExpenses.first();
                while (moWOR.AdditionalExpenses.next()) {
                    moWOR.AdditionalExpenses.doMarkForDelete();
                }
            }
            if (!string.IsNullOrWhiteSpace(U_Rebate) && U_Rebate.ToUpper() == "Y") {
                idh.dbObjects.User.LinkedBP oLinkedBP = Config.INSTANCE.doGetLinkedBP(U_SuppCd);
                if (oLinkedBP != null && !string.IsNullOrEmpty(oLinkedBP.LinkedBPCardCode)) {
                    //moWOR.AdditionalExpenses.U_SuppCd = oLinkedBP.LinkedBPCardCode;
                    //moWOR.AdditionalExpenses.U_SuppNm = oLinkedBP.LinkedBPName;
                } else {
                    DataHandler.INSTANCE.doInfo("Failed to load linked BP for " + U_SuppCd);
                    return false;
                }

                moWOR.doCalculateRebate();
                //moWOR.AdditionalExpenses.doAddEmptyRow();
                //moWOR.AdditionalExpenses.U_JobNr = moWOR.U_JobNr;

                //moWOR.AdditionalExpenses.U_RowNr = moWOR.Code;
                //moWOR.AdditionalExpenses.U_ItemCd = U_WastCd;
                //moWOR.AdditionalExpenses.U_ItemDsc = U_WasteNm;


                moWOR.AdditionalExpenses.U_UOM = U_UOM;
                moWOR.AdditionalExpenses.U_Quantity = U_Qty;
                moWOR.AdditionalExpenses.U_AutoAdd = "N";
                moWOR.AdditionalExpenses.U_BPAutoAdd = "N";


                //NumbersPair oNextNum;
                //oNextNum = moWOR.AdditionalExpenses.getNewKey();
                //moWOR.AdditionalExpenses.Code = oNextNum.CodeCode;
                //moWOR.AdditionalExpenses.Name = oNextNum.NameCode;
                if (U_TipCost == string.Empty)
                    moWOR.AdditionalExpenses.doCalculateLinePrices();
                else {
                    moWOR.AdditionalExpenses.U_ItmCost = Convert.ToDouble(U_TipCost);
                    moWOR.AdditionalExpenses.U_ItmChrg = 0;
                    moWOR.AdditionalExpenses.doMarkUserUpdate(idh.dbObjects.User.IDH_WOADDEXP._ItmCost);
                    moWOR.AdditionalExpenses.doCalculateLineCostVat();
                    moWOR.AdditionalExpenses.doCalculateLineCostTotal(moWOR.AdditionalExpenses.CurrentRowIndex);
                }
            }
            IDH_WRADHD moAddItems = new IDH_WRADHD();
            moAddItems.UnLockTable = true;
            int iRet = moAddItems.getData(IDH_WRADHD._ParentID + "=\'" + Code + "\' And " + IDH_WRADHD._AddtItm + "=\'Y\' ", "Cast(Code as Int)");
            moAddItems.first();

            while (moAddItems.next()) {

                moWOR.AdditionalExpenses.doAddEmptyRow();
                moWOR.AdditionalExpenses.U_JobNr = moWOR.U_JobNr;
                moWOR.AdditionalExpenses.BlockChangeNotif = true;
                moWOR.AdditionalExpenses.U_RowNr = moWOR.Code;
                moWOR.AdditionalExpenses.U_ItemCd = moAddItems.U_ItemCd;
                moWOR.AdditionalExpenses.U_ItemDsc = moAddItems.U_ItemNm;
                moWOR.AdditionalExpenses.U_SuppCd = moAddItems.U_SuppCd;
                moWOR.AdditionalExpenses.U_SuppNm = moAddItems.U_SuppNm;


                moWOR.AdditionalExpenses.U_UOM = moAddItems.U_UOM;
                moWOR.AdditionalExpenses.U_Quantity = moAddItems.U_Qty;
                //moWOR.AdditionalExpenses.U_ItmCost = moAddItems.U_TipCost;
                //moWOR.AdditionalExpenses.U_ItmChrg = moAddItems.U_TCharge;
                //moWOR.AdditionalExpenses.U_ChrgVatGrp = U_TChrgVtGrp;//Config.INSTANCE.doGetSalesVatGroup(oWOR.U_CustCd, oBPAC.U_ItemCode);
                moWOR.AdditionalExpenses.U_AutoAdd = "N";
                moWOR.AdditionalExpenses.U_BPAutoAdd = "N";

                //moWOR.AdditionalExpenses.U_WOQLID = moAddItems.Code;
                moWOR.AdditionalExpenses.BlockChangeNotif = false;
                NumbersPair oNextNum;
                oNextNum = moWOR.AdditionalExpenses.getNewKey();
                moWOR.AdditionalExpenses.Code = oNextNum.CodeCode;
                moWOR.AdditionalExpenses.Name = oNextNum.NameCode;
                //    If Config.INSTANCE.getParameterAsBool("PLFUOM", False) = True Then
                //      sUOM = doGetFieldValue("U_UOM", iRow)
                //     End If
                //doGetAdditionalCostPrices
                if (moAddItems.U_OrdCost == string.Empty) {
                    moWOR.AdditionalExpenses.doCalculateLineCostPrices(moWOR.U_CustCd, moWOR.U_ItemCd, moWOR.U_ItmGrp, U_WastCd, "",
                    moWOR.U_Branch, moWOR.U_JobTp, moWOR.U_RDate, moAddItems.U_Qty, moAddItems.U_UOM, "");
                    //moWOR.AdditionalExpenses.doCalculateLineCostPrices(moWOR.AdditionalExpenses.U_SuppCd, moWOR.U_ItemCd, moWOR.U_ItmGrp, moAddItems.U_ItemCd, moWOR.U_SAddress,
                    //    moWOR.U_Branch, moWOR.U_JobTp, moWOR.U_RDate, moAddItems.U_Qty, moAddItems.U_UOM, moWOR.ParentOrder.U_ZpCd);

                } else {
                    moWOR.AdditionalExpenses.U_ItmCost = Convert.ToDouble(moAddItems.U_OrdCost);
                    moWOR.AdditionalExpenses.doMarkUserUpdate(idh.dbObjects.User.IDH_WOADDEXP._ItmCost);
                    moWOR.AdditionalExpenses.doCalculateLineCostVat(); // true);
                    moWOR.AdditionalExpenses.doCalculateLineCostTotal(moWOR.AdditionalExpenses.CurrentRowIndex);
                }
                if (moAddItems.U_CusChr == string.Empty) {
                    moWOR.AdditionalExpenses.doCalculateLineChargePrices(moWOR.U_CustCd, moWOR.U_ItemCd, moWOR.U_ItmGrp, U_WastCd, moWOR.ParentOrder.U_Address,
                    moWOR.U_Branch, moWOR.U_JobTp, moWOR.U_RDate, moAddItems.U_Qty, moAddItems.U_UOM, moWOR.ParentOrder.U_ZpCd);

                } else {
                    moWOR.AdditionalExpenses.U_ItmChrg = Convert.ToDouble(moAddItems.U_CusChr);
                    moWOR.AdditionalExpenses.doMarkUserUpdate(idh.dbObjects.User.IDH_WOADDEXP._ItmChrg);
                    moWOR.AdditionalExpenses.doCalculateLineChargeVat(); // true);
                    moWOR.AdditionalExpenses.doCalculateLineChargeTotal(moWOR.AdditionalExpenses.CurrentRowIndex);
                }

                //moWOR.AdditionalExpenses.doCalculateLinePrices();

                //moWOR.AdditionalExpenses.doCalculateLineChargePrices(moWOR.AdditionalExpenses.U_SuppCd, moWOR.U_ItemCd, moWOR.U_ItmGrp, moAddItems.U_ItemCd, moWOR.U_SAddress,
                //  moWOR.U_Branch, moWOR.U_JobTp, moWOR.U_RDate, moAddItems.U_Qty, moAddItems.U_UOM, moWOR.ParentOrder.U_ZpCd);

                moAddItems.U_ProcSts = ((int)en_BatchProcessStatus.Processed).ToString();
                //moAddItems.U_AddItmID = moWOR.AdditionalExpenses.Code;
                //moAddItems.U_WORID = moWOR.Code;
                //moAddItems.U_WOHID = moWOR.U_JobNr;
                //moAddItems.U_Status = ((int)IDH_WOQITEM.en_WOQITMSTATUS.WOCreated).ToString();
                if (moAddItems.doUpdateDataRow() == false)
                    return false;
                //oWOR.AdditionalExpenses = oWOAExp;
                //return  moWOR.AdditionalExpenses.doAddDataRow();
            }
            moWOR.AdditionalExpenses.doAddBPAdditionalCharges();
            return true;
        }
        #endregion
    }
}
