/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 09/02/2016 12:21:59
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User {

    [Serializable]
    public class IDH_LABITM : com.isb.enq.dbObjects.Base.IDH_LABITM {
        private IDH_LITMDTL moLabItemDetails;
        public IDH_LITMDTL LabItemDetails {
            get { return moLabItemDetails; }
            //set { moLabItemDetails = value; }
        }

        public IDH_LABITM()
            : base() {
            moLabItemDetails = new IDH_LITMDTL(this);
            msAutoNumKey = "SEQLBITM";
        }

        public IDH_LABITM(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oIDHForm, oForm) {
            moLabItemDetails = new IDH_LITMDTL(this);
            msAutoNumKey = "SEQLBITM";
        }

        public bool hasRows {
            get { return moLabItemDetails != null && moLabItemDetails.Count > 0; }
        }

        public IDH_LITMDTL getRowDB() {
            return moLabItemDetails;
        }

        public override void doClearBuffers() {
            base.doClearBuffers();
            if (MustLoadChildren)
                moLabItemDetails.doClearBuffers();
        }
        public override void doLoadChildren() {
            if (Code != null && Code.Length > 0) {
                moLabItemDetails.getByParentNumber(Code);
            } else {
                moLabItemDetails.doClearBuffers();
            }
        }


    }
}
