/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 19/05/2017 11:07:46
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base {
    [Serializable]
    public class IDH_LVSBTH : com.idh.dbObjects.DBBase {

        private Linker moLinker = null;
        public Linker ControlLinker {
            get { return moLinker; }
            set { moLinker = value; }
        }

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_LVSBTH() : base("@IDH_LVSBTH") {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_LVSBTH(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm) : base(oForm, "@IDH_LVSBTH") {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
         * Table name
         */
        public readonly static string TableName = "@IDH_LVSBTH";

        /**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
        public readonly static string _Code = "Code";
        public string Code {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code() {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue) {
            return base.doValidation(_Code, oValue);
        }
        /**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
        public readonly static string _Name = "Name";
        public string Name {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name() {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue) {
            return base.doValidation(_Name, oValue);
        }
        /**
		 * Decription: Record Added By
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AddedBy
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _AddedBy = "U_AddedBy";
        public string U_AddedBy {
            get {
                return getValueAsStringNZ(_AddedBy);
            }
            set { setValue(_AddedBy, value); }
        }
        public string doValidate_AddedBy() {
            return doValidate_AddedBy(U_AddedBy);
        }
        public virtual string doValidate_AddedBy(object oValue) {
            return base.doValidation(_AddedBy, oValue);
        }

        /**
		 * Decription: Amendment Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AmndTp
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _AmndTp = "U_AmndTp";
        public string U_AmndTp {
            get {
                return getValueAsStringNZ(_AmndTp);
            }
            set { setValue(_AmndTp, value); }
        }
        public string doValidate_AmndTp() {
            return doValidate_AmndTp(U_AmndTp);
        }
        public virtual string doValidate_AmndTp(object oValue) {
            return base.doValidation(_AmndTp, oValue);
        }

        /**
		 * Decription: Auto Complete
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AutoComp
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _AutoComp = "U_AutoComp";
        public string U_AutoComp {
            get {
                return getValueAsStringNZ(_AutoComp);
            }
            set { setValue(_AutoComp, value); }
        }
        public string doValidate_AutoComp() {
            return doValidate_AutoComp(U_AutoComp);
        }
        public virtual string doValidate_AutoComp(object oValue) {
            return base.doValidation(_AutoComp, oValue);
        }

        /**
		 * Decription: Auto Complete StartDate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AUTSTRT
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _AUTSTRT = "U_AUTSTRT";
        public string U_AUTSTRT {
            get {
                return getValueAsStringNZ(_AUTSTRT);
            }
            set { setValue(_AUTSTRT, value); }
        }
        public string doValidate_AUTSTRT() {
            return doValidate_AUTSTRT(U_AUTSTRT);
        }
        public virtual string doValidate_AUTSTRT(object oValue) {
            return base.doValidation(_AUTSTRT, oValue);
        }

        /**
		 * Decription: BatchNum
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BatchNum
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _BatchNum = "U_BatchNum";
        public string U_BatchNum {
            get {
                return getValueAsStringNZ(_BatchNum);
            }
            set { setValue(_BatchNum, value); }
        }
        public string doValidate_BatchNum() {
            return doValidate_BatchNum(U_BatchNum);
        }
        public virtual string doValidate_BatchNum(object oValue) {
            return base.doValidation(_BatchNum, oValue);
        }

        /**
		 * Decription: Batch Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BthStatus
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _BthStatus = "U_BthStatus";
        public string U_BthStatus {
            get {
                return getValueAsStringNZ(_BthStatus);
            }
            set { setValue(_BthStatus, value); }
        }
        public string doValidate_BthStatus() {
            return doValidate_BthStatus(U_BthStatus);
        }
        public virtual string doValidate_BthStatus(object oValue) {
            return base.doValidation(_BthStatus, oValue);
        }

        /**
		 * Decription: CIP.Haulage / Service / Rental
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CHaulage
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _CHaulage = "U_CHaulage";
        public double U_CHaulage {
            get {
                return getValueAsDouble(_CHaulage);
            }
            set { setValue(_CHaulage, value); }
        }
        public string doValidate_CHaulage() {
            return doValidate_CHaulage(U_CHaulage);
        }
        public virtual string doValidate_CHaulage(object oValue) {
            return base.doValidation(_CHaulage, oValue);
        }

        /**
		 * Decription: CIP.Minimum Charge Upto Volume
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CMCgVol
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _CMCgVol = "U_CMCgVol";
        public double U_CMCgVol {
            get {
                return getValueAsDouble(_CMCgVol);
            }
            set { setValue(_CMCgVol, value); }
        }
        public string doValidate_CMCgVol() {
            return doValidate_CMCgVol(U_CMCgVol);
        }
        public virtual string doValidate_CMCgVol(object oValue) {
            return base.doValidation(_CMCgVol, oValue);
        }

        /**
		 * Decription: CIP.Minimum Charge
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CMinChg
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _CMinChg = "U_CMinChg";
        public double U_CMinChg {
            get {
                return getValueAsDouble(_CMinChg);
            }
            set { setValue(_CMinChg, value); }
        }
        public string doValidate_CMinChg() {
            return doValidate_CMinChg(U_CMinChg);
        }
        public virtual string doValidate_CMinChg(object oValue) {
            return base.doValidation(_CMinChg, oValue);
        }

        /**
		 * Decription: CIP-Min Charge
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CMnChg
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CMnChg = "U_CMnChg";
        public string U_CMnChg {
            get {
                return getValueAsStringNZ(_CMnChg);
            }
            set { setValue(_CMnChg, value); }
        }
        public string doValidate_CMnChg() {
            return doValidate_CMnChg(U_CMnChg);
        }
        public virtual string doValidate_CMnChg(object oValue) {
            return base.doValidation(_CMnChg, oValue);
        }

        /**
		 * Decription: CIP-Min Site Visit
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CMnSVst
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CMnSVst = "U_CMnSVst";
        public string U_CMnSVst {
            get {
                return getValueAsStringNZ(_CMnSVst);
            }
            set { setValue(_CMnSVst, value); }
        }
        public string doValidate_CMnSVst() {
            return doValidate_CMnSVst(U_CMnSVst);
        }
        public virtual string doValidate_CMnSVst(object oValue) {
            return base.doValidation(_CMnSVst, oValue);
        }

        /**
		 * Decription: Comments
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Comments
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Comments = "U_Comments";
        public string U_Comments {
            get {
                return getValueAsStringNZ(_Comments);
            }
            set { setValue(_Comments, value); }
        }
        public string doValidate_Comments() {
            return doValidate_Comments(U_Comments);
        }
        public virtual string doValidate_Comments(object oValue) {
            return base.doValidation(_Comments, oValue);
        }

        /**
		 * Decription: CIP.Rate above min volume
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CRminVol
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _CRminVol = "U_CRminVol";
        public double U_CRminVol {
            get {
                return getValueAsDouble(_CRminVol);
            }
            set { setValue(_CRminVol, value); }
        }
        public string doValidate_CRminVol() {
            return doValidate_CRminVol(U_CRminVol);
        }
        public virtual string doValidate_CRminVol(object oValue) {
            return base.doValidation(_CRminVol, oValue);
        }

        /**
		 * Decription: Post Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CusPCode
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CusPCode = "U_CusPCode";
        public string U_CusPCode {
            get {
                return getValueAsStringNZ(_CusPCode);
            }
            set { setValue(_CusPCode, value); }
        }
        public string doValidate_CusPCode() {
            return doValidate_CusPCode(U_CusPCode);
        }
        public virtual string doValidate_CusPCode(object oValue) {
            return base.doValidation(_CusPCode, oValue);
        }

        /**
		 * Decription: Customer Site Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CustAdr
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CustAdr = "U_CustAdr";
        public string U_CustAdr {
            get {
                return getValueAsStringNZ(_CustAdr);
            }
            set { setValue(_CustAdr, value); }
        }
        public string doValidate_CustAdr() {
            return doValidate_CustAdr(U_CustAdr);
        }
        public virtual string doValidate_CustAdr(object oValue) {
            return base.doValidation(_CustAdr, oValue);
        }

        /**
		 * Decription: Customer Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CustCd
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CustCd = "U_CustCd";
        public string U_CustCd {
            get {
                return getValueAsStringNZ(_CustCd);
            }
            set { setValue(_CustCd, value); }
        }
        public string doValidate_CustCd() {
            return doValidate_CustCd(U_CustCd);
        }
        public virtual string doValidate_CustCd(object oValue) {
            return base.doValidation(_CustCd, oValue);
        }

        /**
		 * Decription: Customer-Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CustNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CustNm = "U_CustNm";
        public string U_CustNm {
            get {
                return getValueAsStringNZ(_CustNm);
            }
            set { setValue(_CustNm, value); }
        }
        public string doValidate_CustNm() {
            return doValidate_CustNm(U_CustNm);
        }
        public virtual string doValidate_CustNm(object oValue) {
            return base.doValidation(_CustNm, oValue);
        }

        /**
		 * Decription: CIP-Variable Or Fixed QTY
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_CVarRFix
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _CVarRFix = "U_CVarRFix";
        public string U_CVarRFix {
            get {
                return getValueAsStringNZ(_CVarRFix);
            }
            set { setValue(_CVarRFix, value); }
        }
        public string doValidate_CVarRFix() {
            return doValidate_CVarRFix(U_CVarRFix);
        }
        public virtual string doValidate_CVarRFix(object oValue) {
            return base.doValidation(_CVarRFix, oValue);
        }

        /**
		 * Decription: Effective Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_EffDate
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
        public readonly static string _EffDate = "U_EffDate";
        public DateTime U_EffDate {
            get {
                return getValueAsDateTime(_EffDate);
            }
            set { setValue(_EffDate, value); }
        }
        public void U_EffDate_AsString(string value) {
            setValue("U_EffDate", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_EffDate_AsString() {
            DateTime dVal = getValueAsDateTime(_EffDate);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_EffDate() {
            return doValidate_EffDate(U_EffDate);
        }
        public virtual string doValidate_EffDate(object oValue) {
            return base.doValidation(_EffDate, oValue);
        }

        /**
		 * Decription: Fri
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Fri
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Fri = "U_Fri";
        public string U_Fri {
            get {
                return getValueAsStringNZ(_Fri);
            }
            set { setValue(_Fri, value); }
        }
        public string doValidate_Fri() {
            return doValidate_Fri(U_Fri);
        }
        public virtual string doValidate_Fri(object oValue) {
            return base.doValidation(_Fri, oValue);
        }

        /**
		 * Decription: Freqency Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FrqCount
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _FrqCount = "U_FrqCount";
        public int U_FrqCount {
            get {
                return getValueAsInt(_FrqCount);
            }
            set { setValue(_FrqCount, value); }
        }
        public string doValidate_FrqCount() {
            return doValidate_FrqCount(U_FrqCount);
        }
        public virtual string doValidate_FrqCount(object oValue) {
            return base.doValidation(_FrqCount, oValue);
        }

        /**
		 * Decription: Freqency Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FrqType
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _FrqType = "U_FrqType";
        public string U_FrqType {
            get {
                return getValueAsStringNZ(_FrqType);
            }
            set { setValue(_FrqType, value); }
        }
        public string doValidate_FrqType() {
            return doValidate_FrqType(U_FrqType);
        }
        public virtual string doValidate_FrqType(object oValue) {
            return base.doValidation(_FrqType, oValue);
        }

        /**
		 * Decription: Common Comments
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHCMT
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _IDHCMT = "U_IDHCMT";
        public string U_IDHCMT {
            get {
                return getValueAsStringNZ(_IDHCMT);
            }
            set { setValue(_IDHCMT, value); }
        }
        public string doValidate_IDHCMT() {
            return doValidate_IDHCMT(U_IDHCMT);
        }
        public virtual string doValidate_IDHCMT(object oValue) {
            return base.doValidation(_IDHCMT, oValue);
        }

        /**
		 * Decription: Common Driver Instructions
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHDRVI
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _IDHDRVI = "U_IDHDRVI";
        public string U_IDHDRVI {
            get {
                return getValueAsStringNZ(_IDHDRVI);
            }
            set { setValue(_IDHDRVI, value); }
        }
        public string doValidate_IDHDRVI() {
            return doValidate_IDHDRVI(U_IDHDRVI);
        }
        public virtual string doValidate_IDHDRVI(object oValue) {
            return base.doValidation(_IDHDRVI, oValue);
        }

        /**
		 * Decription: Route Code Friday
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHRCDF
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _IDHRCDF = "U_IDHRCDF";
        public string U_IDHRCDF {
            get {
                return getValueAsStringNZ(_IDHRCDF);
            }
            set { setValue(_IDHRCDF, value); }
        }
        public string doValidate_IDHRCDF() {
            return doValidate_IDHRCDF(U_IDHRCDF);
        }
        public virtual string doValidate_IDHRCDF(object oValue) {
            return base.doValidation(_IDHRCDF, oValue);
        }

        /**
		 * Decription: Route Code Monday
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHRCDM
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _IDHRCDM = "U_IDHRCDM";
        public string U_IDHRCDM {
            get {
                return getValueAsStringNZ(_IDHRCDM);
            }
            set { setValue(_IDHRCDM, value); }
        }
        public string doValidate_IDHRCDM() {
            return doValidate_IDHRCDM(U_IDHRCDM);
        }
        public virtual string doValidate_IDHRCDM(object oValue) {
            return base.doValidation(_IDHRCDM, oValue);
        }

        /**
		 * Decription: Route Code Saturday
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHRCDSA
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _IDHRCDSA = "U_IDHRCDSA";
        public string U_IDHRCDSA {
            get {
                return getValueAsStringNZ(_IDHRCDSA);
            }
            set { setValue(_IDHRCDSA, value); }
        }
        public string doValidate_IDHRCDSA() {
            return doValidate_IDHRCDSA(U_IDHRCDSA);
        }
        public virtual string doValidate_IDHRCDSA(object oValue) {
            return base.doValidation(_IDHRCDSA, oValue);
        }

        /**
		 * Decription: Route Code Sunday
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHRCDSU
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _IDHRCDSU = "U_IDHRCDSU";
        public string U_IDHRCDSU {
            get {
                return getValueAsStringNZ(_IDHRCDSU);
            }
            set { setValue(_IDHRCDSU, value); }
        }
        public string doValidate_IDHRCDSU() {
            return doValidate_IDHRCDSU(U_IDHRCDSU);
        }
        public virtual string doValidate_IDHRCDSU(object oValue) {
            return base.doValidation(_IDHRCDSU, oValue);
        }

        /**
		 * Decription: Route Code Thursday
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHRCDTH
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _IDHRCDTH = "U_IDHRCDTH";
        public string U_IDHRCDTH {
            get {
                return getValueAsStringNZ(_IDHRCDTH);
            }
            set { setValue(_IDHRCDTH, value); }
        }
        public string doValidate_IDHRCDTH() {
            return doValidate_IDHRCDTH(U_IDHRCDTH);
        }
        public virtual string doValidate_IDHRCDTH(object oValue) {
            return base.doValidation(_IDHRCDTH, oValue);
        }

        /**
		 * Decription: Route Code Tuesday
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHRCDTU
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _IDHRCDTU = "U_IDHRCDTU";
        public string U_IDHRCDTU {
            get {
                return getValueAsStringNZ(_IDHRCDTU);
            }
            set { setValue(_IDHRCDTU, value); }
        }
        public string doValidate_IDHRCDTU() {
            return doValidate_IDHRCDTU(U_IDHRCDTU);
        }
        public virtual string doValidate_IDHRCDTU(object oValue) {
            return base.doValidation(_IDHRCDTU, oValue);
        }

        /**
		 * Decription: Route Code Wednesday
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHRCDW
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _IDHRCDW = "U_IDHRCDW";
        public string U_IDHRCDW {
            get {
                return getValueAsStringNZ(_IDHRCDW);
            }
            set { setValue(_IDHRCDW, value); }
        }
        public string doValidate_IDHRCDW() {
            return doValidate_IDHRCDW(U_IDHRCDW);
        }
        public virtual string doValidate_IDHRCDW(object oValue) {
            return base.doValidation(_IDHRCDW, oValue);
        }

        /**
		 * Decription: Recurrence Frequency
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHRECFQ
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _IDHRECFQ = "U_IDHRECFQ";
        public int U_IDHRECFQ {
            get {
                return getValueAsInt(_IDHRECFQ);
            }
            set { setValue(_IDHRECFQ, value); }
        }
        public string doValidate_IDHRECFQ() {
            return doValidate_IDHRECFQ(U_IDHRECFQ);
        }
        public virtual string doValidate_IDHRECFQ(object oValue) {
            return base.doValidation(_IDHRECFQ, oValue);
        }

        /**
		 * Decription: Route Sequence Friday
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHRSQF
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _IDHRSQF = "U_IDHRSQF";
        public int U_IDHRSQF {
            get {
                return getValueAsInt(_IDHRSQF);
            }
            set { setValue(_IDHRSQF, value); }
        }
        public string doValidate_IDHRSQF() {
            return doValidate_IDHRSQF(U_IDHRSQF);
        }
        public virtual string doValidate_IDHRSQF(object oValue) {
            return base.doValidation(_IDHRSQF, oValue);
        }

        /**
		 * Decription: Route Sequence Monday
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHRSQM
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _IDHRSQM = "U_IDHRSQM";
        public int U_IDHRSQM {
            get {
                return getValueAsInt(_IDHRSQM);
            }
            set { setValue(_IDHRSQM, value); }
        }
        public string doValidate_IDHRSQM() {
            return doValidate_IDHRSQM(U_IDHRSQM);
        }
        public virtual string doValidate_IDHRSQM(object oValue) {
            return base.doValidation(_IDHRSQM, oValue);
        }

        /**
		 * Decription: Route Sequence Saturday
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHRSQSA
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _IDHRSQSA = "U_IDHRSQSA";
        public int U_IDHRSQSA {
            get {
                return getValueAsInt(_IDHRSQSA);
            }
            set { setValue(_IDHRSQSA, value); }
        }
        public string doValidate_IDHRSQSA() {
            return doValidate_IDHRSQSA(U_IDHRSQSA);
        }
        public virtual string doValidate_IDHRSQSA(object oValue) {
            return base.doValidation(_IDHRSQSA, oValue);
        }

        /**
		 * Decription: Route Sequence Sunday
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHRSQSU
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _IDHRSQSU = "U_IDHRSQSU";
        public int U_IDHRSQSU {
            get {
                return getValueAsInt(_IDHRSQSU);
            }
            set { setValue(_IDHRSQSU, value); }
        }
        public string doValidate_IDHRSQSU() {
            return doValidate_IDHRSQSU(U_IDHRSQSU);
        }
        public virtual string doValidate_IDHRSQSU(object oValue) {
            return base.doValidation(_IDHRSQSU, oValue);
        }

        /**
		 * Decription: Route Sequence Thursday
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHRSQTH
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _IDHRSQTH = "U_IDHRSQTH";
        public int U_IDHRSQTH {
            get {
                return getValueAsInt(_IDHRSQTH);
            }
            set { setValue(_IDHRSQTH, value); }
        }
        public string doValidate_IDHRSQTH() {
            return doValidate_IDHRSQTH(U_IDHRSQTH);
        }
        public virtual string doValidate_IDHRSQTH(object oValue) {
            return base.doValidation(_IDHRSQTH, oValue);
        }

        /**
		 * Decription: Route Sequence Tuesday
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHRSQTU
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _IDHRSQTU = "U_IDHRSQTU";
        public int U_IDHRSQTU {
            get {
                return getValueAsInt(_IDHRSQTU);
            }
            set { setValue(_IDHRSQTU, value); }
        }
        public string doValidate_IDHRSQTU() {
            return doValidate_IDHRSQTU(U_IDHRSQTU);
        }
        public virtual string doValidate_IDHRSQTU(object oValue) {
            return base.doValidation(_IDHRSQTU, oValue);
        }

        /**
		 * Decription: Route Sequence Wednesday
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHRSQW
		 * Size: 11
		 * Type: db_Numeric
		 * CType: int
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _IDHRSQW = "U_IDHRSQW";
        public int U_IDHRSQW {
            get {
                return getValueAsInt(_IDHRSQW);
            }
            set { setValue(_IDHRSQW, value); }
        }
        public string doValidate_IDHRSQW() {
            return doValidate_IDHRSQW(U_IDHRSQW);
        }
        public virtual string doValidate_IDHRSQW(object oValue) {
            return base.doValidation(_IDHRSQW, oValue);
        }

        /**
		 * Decription: Routable Checkbox
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_IDHRT
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _IDHRT = "U_IDHRT";
        public string U_IDHRT {
            get {
                return getValueAsStringNZ(_IDHRT);
            }
            set { setValue(_IDHRT, value); }
        }
        public string doValidate_IDHRT() {
            return doValidate_IDHRT(U_IDHRT);
        }
        public virtual string doValidate_IDHRT(object oValue) {
            return base.doValidation(_IDHRT, oValue);
        }

        /**
		 * Decription: Container Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItemCd
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _ItemCd = "U_ItemCd";
        public string U_ItemCd {
            get {
                return getValueAsStringNZ(_ItemCd);
            }
            set { setValue(_ItemCd, value); }
        }
        public string doValidate_ItemCd() {
            return doValidate_ItemCd(U_ItemCd);
        }
        public virtual string doValidate_ItemCd(object oValue) {
            return base.doValidation(_ItemCd, oValue);
        }

        /**
		 * Decription: Container
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ItemNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _ItemNm = "U_ItemNm";
        public string U_ItemNm {
            get {
                return getValueAsStringNZ(_ItemNm);
            }
            set { setValue(_ItemNm, value); }
        }
        public string doValidate_ItemNm() {
            return doValidate_ItemNm(U_ItemNm);
        }
        public virtual string doValidate_ItemNm(object oValue) {
            return base.doValidation(_ItemNm, oValue);
        }

        /**
		 * Decription: Order Type Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_JbTypeCd
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _JbTypeCd = "U_JbTypeCd";
        public string U_JbTypeCd {
            get {
                return getValueAsStringNZ(_JbTypeCd);
            }
            set { setValue(_JbTypeCd, value); }
        }
        public string doValidate_JbTypeCd() {
            return doValidate_JbTypeCd(U_JbTypeCd);
        }
        public virtual string doValidate_JbTypeCd(object oValue) {
            return base.doValidation(_JbTypeCd, oValue);
        }

        /**
		 * Decription: Order Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_JbTypeDs
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _JbTypeDs = "U_JbTypeDs";
        public string U_JbTypeDs {
            get {
                return getValueAsStringNZ(_JbTypeDs);
            }
            set { setValue(_JbTypeDs, value); }
        }
        public string doValidate_JbTypeDs() {
            return doValidate_JbTypeDs(U_JbTypeDs);
        }
        public virtual string doValidate_JbTypeDs(object oValue) {
            return base.doValidation(_JbTypeDs, oValue);
        }

        /**
		 * Decription: LPW
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_LPW
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _LPW = "U_LPW";
        public double U_LPW {
            get {
                return getValueAsDouble(_LPW);
            }
            set { setValue(_LPW, value); }
        }
        public string doValidate_LPW() {
            return doValidate_LPW(U_LPW);
        }
        public virtual string doValidate_LPW(object oValue) {
            return base.doValidation(_LPW, oValue);
        }

        /**
		 * Decription: Mon
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Mon
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Mon = "U_Mon";
        public string U_Mon {
            get {
                return getValueAsStringNZ(_Mon);
            }
            set { setValue(_Mon, value); }
        }
        public string doValidate_Mon() {
            return doValidate_Mon(U_Mon);
        }
        public virtual string doValidate_Mon(object oValue) {
            return base.doValidation(_Mon, oValue);
        }

        /**
		 * Decription: PBI
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PBI
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _PBI = "U_PBI";
        public string U_PBI {
            get {
                return getValueAsStringNZ(_PBI);
            }
            set { setValue(_PBI, value); }
        }
        public string doValidate_PBI() {
            return doValidate_PBI(U_PBI);
        }
        public virtual string doValidate_PBI(object oValue) {
            return base.doValidation(_PBI, oValue);
        }

        /**
		 * Decription: Period type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_PERTYP
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _PERTYP = "U_PERTYP";
        public string U_PERTYP {
            get {
                return getValueAsStringNZ(_PERTYP);
            }
            set { setValue(_PERTYP, value); }
        }
        public string doValidate_PERTYP() {
            return doValidate_PERTYP(U_PERTYP);
        }
        public virtual string doValidate_PERTYP(object oValue) {
            return base.doValidation(_PERTYP, oValue);
        }

        /**
		 * Decription: Process Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ProcSts
		 * Size: 2
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _ProcSts = "U_ProcSts";
        public string U_ProcSts {
            get {
                return getValueAsStringNZ(_ProcSts);
            }
            set { setValue(_ProcSts, value); }
        }
        public string doValidate_ProcSts() {
            return doValidate_ProcSts(U_ProcSts);
        }
        public virtual string doValidate_ProcSts(object oValue) {
            return base.doValidation(_ProcSts, oValue);
        }

        /**
		 * Decription: Qty
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Qty
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Quantity
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _Qty = "U_Qty";
        public double U_Qty {
            get {
                return getValueAsDouble(_Qty);
            }
            set { setValue(_Qty, value); }
        }
        public string doValidate_Qty() {
            return doValidate_Qty(U_Qty);
        }
        public virtual string doValidate_Qty(object oValue) {
            return base.doValidation(_Qty, oValue);
        }

        /**
		 * Decription: Rebate
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Rebate
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Rebate = "U_Rebate";
        public string U_Rebate {
            get {
                return getValueAsStringNZ(_Rebate);
            }
            set { setValue(_Rebate, value); }
        }
        public string doValidate_Rebate() {
            return doValidate_Rebate(U_Rebate);
        }
        public virtual string doValidate_Rebate(object oValue) {
            return base.doValidation(_Rebate, oValue);
        }

        /**
		 * Decription: Sat
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Sat
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Sat = "U_Sat";
        public string U_Sat {
            get {
                return getValueAsStringNZ(_Sat);
            }
            set { setValue(_Sat, value); }
        }
        public string doValidate_Sat() {
            return doValidate_Sat(U_Sat);
        }
        public virtual string doValidate_Sat(object oValue) {
            return base.doValidation(_Sat, oValue);
        }

        /**
		 * Decription: SIP.Haulage / Service / Rental
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SHaulage
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _SHaulage = "U_SHaulage";
        public double U_SHaulage {
            get {
                return getValueAsDouble(_SHaulage);
            }
            set { setValue(_SHaulage, value); }
        }
        public string doValidate_SHaulage() {
            return doValidate_SHaulage(U_SHaulage);
        }
        public virtual string doValidate_SHaulage(object oValue) {
            return base.doValidation(_SHaulage, oValue);
        }

        /**
		 * Decription: SIP.Minimum Charge Upto Volume
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SMCgVol
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _SMCgVol = "U_SMCgVol";
        public double U_SMCgVol {
            get {
                return getValueAsDouble(_SMCgVol);
            }
            set { setValue(_SMCgVol, value); }
        }
        public string doValidate_SMCgVol() {
            return doValidate_SMCgVol(U_SMCgVol);
        }
        public virtual string doValidate_SMCgVol(object oValue) {
            return base.doValidation(_SMCgVol, oValue);
        }

        /**
		 * Decription: SIP.Minimum Charge
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SMinChg
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _SMinChg = "U_SMinChg";
        public double U_SMinChg {
            get {
                return getValueAsDouble(_SMinChg);
            }
            set { setValue(_SMinChg, value); }
        }
        public string doValidate_SMinChg() {
            return doValidate_SMinChg(U_SMinChg);
        }
        public virtual string doValidate_SMinChg(object oValue) {
            return base.doValidation(_SMinChg, oValue);
        }

        /**
		 * Decription: SIP-Min Charge
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SMnChg
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _SMnChg = "U_SMnChg";
        public string U_SMnChg {
            get {
                return getValueAsStringNZ(_SMnChg);
            }
            set { setValue(_SMnChg, value); }
        }
        public string doValidate_SMnChg() {
            return doValidate_SMnChg(U_SMnChg);
        }
        public virtual string doValidate_SMnChg(object oValue) {
            return base.doValidation(_SMnChg, oValue);
        }

        /**
		 * Decription: SIP-Min Site Visit
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SMnSVst
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _SMnSVst = "U_SMnSVst";
        public string U_SMnSVst {
            get {
                return getValueAsStringNZ(_SMnSVst);
            }
            set { setValue(_SMnSVst, value); }
        }
        public string doValidate_SMnSVst() {
            return doValidate_SMnSVst(U_SMnSVst);
        }
        public virtual string doValidate_SMnSVst(object oValue) {
            return base.doValidation(_SMnSVst, oValue);
        }

        /**
		 * Decription: SIP.Rate above min Volume
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SRminVol
		 * Size: 20
		 * Type: db_Float
		 * CType: double
		 * SubType: st_Price
		 * ValidValue: 
		 * Value: 0
		 * Identity: False
		 */
        public readonly static string _SRminVol = "U_SRminVol";
        public double U_SRminVol {
            get {
                return getValueAsDouble(_SRminVol);
            }
            set { setValue(_SRminVol, value); }
        }
        public string doValidate_SRminVol() {
            return doValidate_SRminVol(U_SRminVol);
        }
        public virtual string doValidate_SRminVol(object oValue) {
            return base.doValidation(_SRminVol, oValue);
        }

        /**
		 * Decription: Sun
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Sun
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Sun = "U_Sun";
        public string U_Sun {
            get {
                return getValueAsStringNZ(_Sun);
            }
            set { setValue(_Sun, value); }
        }
        public string doValidate_Sun() {
            return doValidate_Sun(U_Sun);
        }
        public virtual string doValidate_Sun(object oValue) {
            return base.doValidation(_Sun, oValue);
        }

        /**
		 * Decription: Supplier Address
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SupAddr
		 * Size: 50
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _SupAddr = "U_SupAddr";
        public string U_SupAddr {
            get {
                return getValueAsStringNZ(_SupAddr);
            }
            set { setValue(_SupAddr, value); }
        }
        public string doValidate_SupAddr() {
            return doValidate_SupAddr(U_SupAddr);
        }
        public virtual string doValidate_SupAddr(object oValue) {
            return base.doValidation(_SupAddr, oValue);
        }

        /**
		 * Decription: Supplier Address Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SupAdrCd
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _SupAdrCd = "U_SupAdrCd";
        public string U_SupAdrCd {
            get {
                return getValueAsStringNZ(_SupAdrCd);
            }
            set { setValue(_SupAdrCd, value); }
        }
        public string doValidate_SupAdrCd() {
            return doValidate_SupAdrCd(U_SupAdrCd);
        }
        public virtual string doValidate_SupAdrCd(object oValue) {
            return base.doValidation(_SupAdrCd, oValue);
        }

        /**
		 * Decription: Supplier Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SuppCd
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _SuppCd = "U_SuppCd";
        public string U_SuppCd {
            get {
                return getValueAsStringNZ(_SuppCd);
            }
            set { setValue(_SuppCd, value); }
        }
        public string doValidate_SuppCd() {
            return doValidate_SuppCd(U_SuppCd);
        }
        public virtual string doValidate_SuppCd(object oValue) {
            return base.doValidation(_SuppCd, oValue);
        }

        /**
		 * Decription: Supplier Postcode
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SupPCode
		 * Size: 20
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _SupPCode = "U_SupPCode";
        public string U_SupPCode {
            get {
                return getValueAsStringNZ(_SupPCode);
            }
            set { setValue(_SupPCode, value); }
        }
        public string doValidate_SupPCode() {
            return doValidate_SupPCode(U_SupPCode);
        }
        public virtual string doValidate_SupPCode(object oValue) {
            return base.doValidation(_SupPCode, oValue);
        }

        /**
		 * Decription: Supplier Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SuppNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _SuppNm = "U_SuppNm";
        public string U_SuppNm {
            get {
                return getValueAsStringNZ(_SuppNm);
            }
            set { setValue(_SuppNm, value); }
        }
        public string doValidate_SuppNm() {
            return doValidate_SuppNm(U_SuppNm);
        }
        public virtual string doValidate_SuppNm(object oValue) {
            return base.doValidation(_SuppNm, oValue);
        }

        /**
		 * Decription: Supplier Reference
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SuppRef
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _SuppRef = "U_SuppRef";
        public string U_SuppRef {
            get {
                return getValueAsStringNZ(_SuppRef);
            }
            set { setValue(_SuppRef, value); }
        }
        public string doValidate_SuppRef() {
            return doValidate_SuppRef(U_SuppRef);
        }
        public virtual string doValidate_SuppRef(object oValue) {
            return base.doValidation(_SuppRef, oValue);
        }

        /**
		 * Decription: SIP-Variable Or Fixed QTY
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_SVarRFix
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _SVarRFix = "U_SVarRFix";
        public string U_SVarRFix {
            get {
                return getValueAsStringNZ(_SVarRFix);
            }
            set { setValue(_SVarRFix, value); }
        }
        public string doValidate_SVarRFix() {
            return doValidate_SVarRFix(U_SVarRFix);
        }
        public virtual string doValidate_SVarRFix(object oValue) {
            return base.doValidation(_SVarRFix, oValue);
        }

        /**
		 * Decription: Thu
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Thu
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Thu = "U_Thu";
        public string U_Thu {
            get {
                return getValueAsStringNZ(_Thu);
            }
            set { setValue(_Thu, value); }
        }
        public string doValidate_Thu() {
            return doValidate_Thu(U_Thu);
        }
        public virtual string doValidate_Thu(object oValue) {
            return base.doValidation(_Thu, oValue);
        }

        /**
		 * Decription: Tue
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Tue
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Tue = "U_Tue";
        public string U_Tue {
            get {
                return getValueAsStringNZ(_Tue);
            }
            set { setValue(_Tue, value); }
        }
        public string doValidate_Tue() {
            return doValidate_Tue(U_Tue);
        }
        public virtual string doValidate_Tue(object oValue) {
            return base.doValidation(_Tue, oValue);
        }

        /**
		 * Decription: Unit of Measure (UOM)
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_UOM
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _UOM = "U_UOM";
        public string U_UOM {
            get {
                return getValueAsStringNZ(_UOM);
            }
            set { setValue(_UOM, value); }
        }
        public string doValidate_UOM() {
            return doValidate_UOM(U_UOM);
        }
        public virtual string doValidate_UOM(object oValue) {
            return base.doValidation(_UOM, oValue);
        }

        /**
		 * Decription: Validation Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_VldStatus
		 * Size: 2
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _VldStatus = "U_VldStatus";
        public string U_VldStatus {
            get {
                return getValueAsStringNZ(_VldStatus);
            }
            set { setValue(_VldStatus, value); }
        }
        public string doValidate_VldStatus() {
            return doValidate_VldStatus(U_VldStatus);
        }
        public virtual string doValidate_VldStatus(object oValue) {
            return base.doValidation(_VldStatus, oValue);
        }

        /**
		 * Decription: Waste Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WastCd
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _WastCd = "U_WastCd";
        public string U_WastCd {
            get {
                return getValueAsStringNZ(_WastCd);
            }
            set { setValue(_WastCd, value); }
        }
        public string doValidate_WastCd() {
            return doValidate_WastCd(U_WastCd);
        }
        public virtual string doValidate_WastCd(object oValue) {
            return base.doValidation(_WastCd, oValue);
        }

        /**
		 * Decription: Waste Material
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WasteNm
		 * Size: 100
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _WasteNm = "U_WasteNm";
        public string U_WasteNm {
            get {
                return getValueAsStringNZ(_WasteNm);
            }
            set { setValue(_WasteNm, value); }
        }
        public string doValidate_WasteNm() {
            return doValidate_WasteNm(U_WasteNm);
        }
        public virtual string doValidate_WasteNm(object oValue) {
            return base.doValidation(_WasteNm, oValue);
        }

        /**
		 * Decription: Wed
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Wed
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _Wed = "U_Wed";
        public string U_Wed {
            get {
                return getValueAsStringNZ(_Wed);
            }
            set { setValue(_Wed, value); }
        }
        public string doValidate_Wed() {
            return doValidate_Wed(U_Wed);
        }
        public virtual string doValidate_Wed(object oValue) {
            return base.doValidation(_Wed, oValue);
        }

        /**
		 * Decription: WORow
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WORow
		 * Size: 60
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
        public readonly static string _WORow = "U_WORow";
        public string U_WORow {
            get {
                return getValueAsStringNZ(_WORow);
            }
            set { setValue(_WORow, value); }
        }
        public string doValidate_WORow() {
            return doValidate_WORow(U_WORow);
        }
        public virtual string doValidate_WORow(object oValue) {
            return base.doValidation(_WORow, oValue);
        }

        /**
		 * Decription: WTN From Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WTNFrom
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
        public readonly static string _WTNFrom = "U_WTNFrom";
        public DateTime U_WTNFrom {
            get {
                return getValueAsDateTime(_WTNFrom);
            }
            set { setValue(_WTNFrom, value); }
        }
        public void U_WTNFrom_AsString(string value) {
            setValue("U_WTNFrom", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_WTNFrom_AsString() {
            DateTime dVal = getValueAsDateTime(_WTNFrom);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_WTNFrom() {
            return doValidate_WTNFrom(U_WTNFrom);
        }
        public virtual string doValidate_WTNFrom(object oValue) {
            return base.doValidation(_WTNFrom, oValue);
        }

        /**
		 * Decription: WTN From Date
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_WTNTo
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
        public readonly static string _WTNTo = "U_WTNTo";
        public DateTime U_WTNTo {
            get {
                return getValueAsDateTime(_WTNTo);
            }
            set { setValue(_WTNTo, value); }
        }
        public void U_WTNTo_AsString(string value) {
            setValue("U_WTNTo", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_WTNTo_AsString() {
            DateTime dVal = getValueAsDateTime(_WTNTo);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_WTNTo() {
            return doValidate_WTNTo(U_WTNTo);
        }
        public virtual string doValidate_WTNTo(object oValue) {
            return base.doValidation(_WTNTo, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo() {
            if (moDBFields == null)
                moDBFields = new DBFields(78);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_AddedBy, "Record Added By", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Record Added By
            moDBFields.Add(_AmndTp, "Amendment Type", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Amendment Type
            moDBFields.Add(_AutoComp, "Auto Complete", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Auto Complete
            moDBFields.Add(_AUTSTRT, "Auto Complete StartDate", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Auto Complete StartDate
            moDBFields.Add(_BatchNum, "BatchNum", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //BatchNum
            moDBFields.Add(_BthStatus, "Batch Status", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Batch Status
            moDBFields.Add(_CHaulage, "CIP.Haulage / Service / Rental", 8, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //CIP.Haulage / Service / Rental
            moDBFields.Add(_CMCgVol, "CIP.Minimum Charge Upto Volume", 9, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //CIP.Minimum Charge Upto Volume
            moDBFields.Add(_CMinChg, "CIP.Minimum Charge", 10, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //CIP.Minimum Charge
            moDBFields.Add(_CMnChg, "CIP-Min Charge", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //CIP-Min Charge
            moDBFields.Add(_CMnSVst, "CIP-Min Site Visit", 12, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //CIP-Min Site Visit
            moDBFields.Add(_Comments, "Comments", 13, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Comments
            moDBFields.Add(_CRminVol, "CIP.Rate above min volume", 14, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //CIP.Rate above min volume
            moDBFields.Add(_CusPCode, "Post Code", 15, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Post Code
            moDBFields.Add(_CustAdr, "Customer Site Address", 16, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Customer Site Address
            moDBFields.Add(_CustCd, "Customer Code", 17, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //Customer Code
            moDBFields.Add(_CustNm, "Customer-Name", 18, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Customer-Name
            moDBFields.Add(_CVarRFix, "CIP-Variable Or Fixed QTY", 19, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //CIP-Variable Or Fixed QTY
            moDBFields.Add(_EffDate, "Effective Date", 20, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Effective Date
            moDBFields.Add(_Fri, "Fri", 21, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Fri
            moDBFields.Add(_FrqCount, "Freqency Type", 22, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Freqency Type
            moDBFields.Add(_FrqType, "Freqency Type", 23, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Freqency Type
            moDBFields.Add(_IDHCMT, "Common Comments", 24, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Common Comments
            moDBFields.Add(_IDHDRVI, "Common Driver Instructions", 25, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Common Driver Instructions
            moDBFields.Add(_IDHRCDF, "Route Code Friday", 26, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, false); //Route Code Friday
            moDBFields.Add(_IDHRCDM, "Route Code Monday", 27, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, false); //Route Code Monday
            moDBFields.Add(_IDHRCDSA, "Route Code Saturday", 28, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, false); //Route Code Saturday
            moDBFields.Add(_IDHRCDSU, "Route Code Sunday", 29, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, false); //Route Code Sunday
            moDBFields.Add(_IDHRCDTH, "Route Code Thursday", 30, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, false); //Route Code Thursday
            moDBFields.Add(_IDHRCDTU, "Route Code Tuesday", 31, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, false); //Route Code Tuesday
            moDBFields.Add(_IDHRCDW, "Route Code Wednesday", 32, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, false); //Route Code Wednesday
            moDBFields.Add(_IDHRECFQ, "Recurrence Frequency", 33, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Recurrence Frequency
            moDBFields.Add(_IDHRSQF, "Route Sequence Friday", 34, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Route Sequence Friday
            moDBFields.Add(_IDHRSQM, "Route Sequence Monday", 35, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Route Sequence Monday
            moDBFields.Add(_IDHRSQSA, "Route Sequence Saturday", 36, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Route Sequence Saturday
            moDBFields.Add(_IDHRSQSU, "Route Sequence Sunday", 37, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Route Sequence Sunday
            moDBFields.Add(_IDHRSQTH, "Route Sequence Thursday", 38, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Route Sequence Thursday
            moDBFields.Add(_IDHRSQTU, "Route Sequence Tuesday", 39, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Route Sequence Tuesday
            moDBFields.Add(_IDHRSQW, "Route Sequence Wednesday", 40, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Route Sequence Wednesday
            moDBFields.Add(_IDHRT, "Routable Checkbox", 41, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Routable Checkbox
            moDBFields.Add(_ItemCd, "Container Code", 42, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //Container Code
            moDBFields.Add(_ItemNm, "Container", 43, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Container
            moDBFields.Add(_JbTypeCd, "Order Type Code", 44, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //Order Type Code
            moDBFields.Add(_JbTypeDs, "Order Type", 45, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Order Type
            moDBFields.Add(_LPW, "LPW", 46, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //LPW
            moDBFields.Add(_Mon, "Mon", 47, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Mon
            moDBFields.Add(_PBI, "PBI", 48, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //PBI
            moDBFields.Add(_PERTYP, "Period type", 49, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Period type
            moDBFields.Add(_ProcSts, "Process Status", 50, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2, EMPTYSTR, false, false); //Process Status
            moDBFields.Add(_Qty, "Qty", 51, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Qty
            moDBFields.Add(_Rebate, "Rebate", 52, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Rebate
            moDBFields.Add(_Sat, "Sat", 53, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Sat
            moDBFields.Add(_SHaulage, "SIP.Haulage / Service / Rental", 54, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //SIP.Haulage / Service / Rental
            moDBFields.Add(_SMCgVol, "SIP.Minimum Charge Upto Volume", 55, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //SIP.Minimum Charge Upto Volume
            moDBFields.Add(_SMinChg, "SIP.Minimum Charge", 56, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //SIP.Minimum Charge
            moDBFields.Add(_SMnChg, "SIP-Min Charge", 57, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //SIP-Min Charge
            moDBFields.Add(_SMnSVst, "SIP-Min Site Visit", 58, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //SIP-Min Site Visit
            moDBFields.Add(_SRminVol, "SIP.Rate above min Volume", 59, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 20, 0, false, false); //SIP.Rate above min Volume
            moDBFields.Add(_Sun, "Sun", 60, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Sun
            moDBFields.Add(_SupAddr, "Supplier Address", 61, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Supplier Address
            moDBFields.Add(_SupAdrCd, "Supplier Address Code", 62, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Supplier Address Code
            moDBFields.Add(_SuppCd, "Supplier Code", 63, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //Supplier Code
            moDBFields.Add(_SupPCode, "Supplier Postcode", 64, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20, EMPTYSTR, false, false); //Supplier Postcode
            moDBFields.Add(_SuppNm, "Supplier Name", 65, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Supplier Name
            moDBFields.Add(_SuppRef, "Supplier Reference", 66, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Supplier Reference
            moDBFields.Add(_SVarRFix, "SIP-Variable Or Fixed QTY", 67, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //SIP-Variable Or Fixed QTY
            moDBFields.Add(_Thu, "Thu", 68, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Thu
            moDBFields.Add(_Tue, "Tue", 69, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Tue
            moDBFields.Add(_UOM, "Unit of Measure (UOM)", 70, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Unit of Measure (UOM)
            moDBFields.Add(_VldStatus, "Validation Status", 71, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2, EMPTYSTR, false, false); //Validation Status
            moDBFields.Add(_WastCd, "Waste Code", 72, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //Waste Code
            moDBFields.Add(_WasteNm, "Waste Material", 73, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Waste Material
            moDBFields.Add(_Wed, "Wed", 74, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Wed
            moDBFields.Add(_WORow, "WORow", 75, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60, EMPTYSTR, false, false); //WORow
            moDBFields.Add(_WTNFrom, "WTN From Date", 76, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //WTN From Date
            moDBFields.Add(_WTNTo, "WTN From Date", 77, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //WTN From Date

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation() {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_AddedBy());
            doBuildValidationString(doValidate_AmndTp());
            doBuildValidationString(doValidate_AutoComp());
            doBuildValidationString(doValidate_AUTSTRT());
            doBuildValidationString(doValidate_BatchNum());
            doBuildValidationString(doValidate_BthStatus());
            doBuildValidationString(doValidate_CHaulage());
            doBuildValidationString(doValidate_CMCgVol());
            doBuildValidationString(doValidate_CMinChg());
            doBuildValidationString(doValidate_CMnChg());
            doBuildValidationString(doValidate_CMnSVst());
            doBuildValidationString(doValidate_Comments());
            doBuildValidationString(doValidate_CRminVol());
            doBuildValidationString(doValidate_CusPCode());
            doBuildValidationString(doValidate_CustAdr());
            doBuildValidationString(doValidate_CustCd());
            doBuildValidationString(doValidate_CustNm());
            doBuildValidationString(doValidate_CVarRFix());
            doBuildValidationString(doValidate_EffDate());
            doBuildValidationString(doValidate_Fri());
            doBuildValidationString(doValidate_FrqCount());
            doBuildValidationString(doValidate_FrqType());
            doBuildValidationString(doValidate_IDHCMT());
            doBuildValidationString(doValidate_IDHDRVI());
            doBuildValidationString(doValidate_IDHRCDF());
            doBuildValidationString(doValidate_IDHRCDM());
            doBuildValidationString(doValidate_IDHRCDSA());
            doBuildValidationString(doValidate_IDHRCDSU());
            doBuildValidationString(doValidate_IDHRCDTH());
            doBuildValidationString(doValidate_IDHRCDTU());
            doBuildValidationString(doValidate_IDHRCDW());
            doBuildValidationString(doValidate_IDHRECFQ());
            doBuildValidationString(doValidate_IDHRSQF());
            doBuildValidationString(doValidate_IDHRSQM());
            doBuildValidationString(doValidate_IDHRSQSA());
            doBuildValidationString(doValidate_IDHRSQSU());
            doBuildValidationString(doValidate_IDHRSQTH());
            doBuildValidationString(doValidate_IDHRSQTU());
            doBuildValidationString(doValidate_IDHRSQW());
            doBuildValidationString(doValidate_IDHRT());
            doBuildValidationString(doValidate_ItemCd());
            doBuildValidationString(doValidate_ItemNm());
            doBuildValidationString(doValidate_JbTypeCd());
            doBuildValidationString(doValidate_JbTypeDs());
            doBuildValidationString(doValidate_LPW());
            doBuildValidationString(doValidate_Mon());
            doBuildValidationString(doValidate_PBI());
            doBuildValidationString(doValidate_PERTYP());
            doBuildValidationString(doValidate_ProcSts());
            doBuildValidationString(doValidate_Qty());
            doBuildValidationString(doValidate_Rebate());
            doBuildValidationString(doValidate_Sat());
            doBuildValidationString(doValidate_SHaulage());
            doBuildValidationString(doValidate_SMCgVol());
            doBuildValidationString(doValidate_SMinChg());
            doBuildValidationString(doValidate_SMnChg());
            doBuildValidationString(doValidate_SMnSVst());
            doBuildValidationString(doValidate_SRminVol());
            doBuildValidationString(doValidate_Sun());
            doBuildValidationString(doValidate_SupAddr());
            doBuildValidationString(doValidate_SupAdrCd());
            doBuildValidationString(doValidate_SuppCd());
            doBuildValidationString(doValidate_SupPCode());
            doBuildValidationString(doValidate_SuppNm());
            doBuildValidationString(doValidate_SuppRef());
            doBuildValidationString(doValidate_SVarRFix());
            doBuildValidationString(doValidate_Thu());
            doBuildValidationString(doValidate_Tue());
            doBuildValidationString(doValidate_UOM());
            doBuildValidationString(doValidate_VldStatus());
            doBuildValidationString(doValidate_WastCd());
            doBuildValidationString(doValidate_WasteNm());
            doBuildValidationString(doValidate_Wed());
            doBuildValidationString(doValidate_WORow());
            doBuildValidationString(doValidate_WTNFrom());
            doBuildValidationString(doValidate_WTNTo());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_AddedBy)) return doValidate_AddedBy(oValue);
            if (sFieldName.Equals(_AmndTp)) return doValidate_AmndTp(oValue);
            if (sFieldName.Equals(_AutoComp)) return doValidate_AutoComp(oValue);
            if (sFieldName.Equals(_AUTSTRT)) return doValidate_AUTSTRT(oValue);
            if (sFieldName.Equals(_BatchNum)) return doValidate_BatchNum(oValue);
            if (sFieldName.Equals(_BthStatus)) return doValidate_BthStatus(oValue);
            if (sFieldName.Equals(_CHaulage)) return doValidate_CHaulage(oValue);
            if (sFieldName.Equals(_CMCgVol)) return doValidate_CMCgVol(oValue);
            if (sFieldName.Equals(_CMinChg)) return doValidate_CMinChg(oValue);
            if (sFieldName.Equals(_CMnChg)) return doValidate_CMnChg(oValue);
            if (sFieldName.Equals(_CMnSVst)) return doValidate_CMnSVst(oValue);
            if (sFieldName.Equals(_Comments)) return doValidate_Comments(oValue);
            if (sFieldName.Equals(_CRminVol)) return doValidate_CRminVol(oValue);
            if (sFieldName.Equals(_CusPCode)) return doValidate_CusPCode(oValue);
            if (sFieldName.Equals(_CustAdr)) return doValidate_CustAdr(oValue);
            if (sFieldName.Equals(_CustCd)) return doValidate_CustCd(oValue);
            if (sFieldName.Equals(_CustNm)) return doValidate_CustNm(oValue);
            if (sFieldName.Equals(_CVarRFix)) return doValidate_CVarRFix(oValue);
            if (sFieldName.Equals(_EffDate)) return doValidate_EffDate(oValue);
            if (sFieldName.Equals(_Fri)) return doValidate_Fri(oValue);
            if (sFieldName.Equals(_FrqCount)) return doValidate_FrqCount(oValue);
            if (sFieldName.Equals(_FrqType)) return doValidate_FrqType(oValue);
            if (sFieldName.Equals(_IDHCMT)) return doValidate_IDHCMT(oValue);
            if (sFieldName.Equals(_IDHDRVI)) return doValidate_IDHDRVI(oValue);
            if (sFieldName.Equals(_IDHRCDF)) return doValidate_IDHRCDF(oValue);
            if (sFieldName.Equals(_IDHRCDM)) return doValidate_IDHRCDM(oValue);
            if (sFieldName.Equals(_IDHRCDSA)) return doValidate_IDHRCDSA(oValue);
            if (sFieldName.Equals(_IDHRCDSU)) return doValidate_IDHRCDSU(oValue);
            if (sFieldName.Equals(_IDHRCDTH)) return doValidate_IDHRCDTH(oValue);
            if (sFieldName.Equals(_IDHRCDTU)) return doValidate_IDHRCDTU(oValue);
            if (sFieldName.Equals(_IDHRCDW)) return doValidate_IDHRCDW(oValue);
            if (sFieldName.Equals(_IDHRECFQ)) return doValidate_IDHRECFQ(oValue);
            if (sFieldName.Equals(_IDHRSQF)) return doValidate_IDHRSQF(oValue);
            if (sFieldName.Equals(_IDHRSQM)) return doValidate_IDHRSQM(oValue);
            if (sFieldName.Equals(_IDHRSQSA)) return doValidate_IDHRSQSA(oValue);
            if (sFieldName.Equals(_IDHRSQSU)) return doValidate_IDHRSQSU(oValue);
            if (sFieldName.Equals(_IDHRSQTH)) return doValidate_IDHRSQTH(oValue);
            if (sFieldName.Equals(_IDHRSQTU)) return doValidate_IDHRSQTU(oValue);
            if (sFieldName.Equals(_IDHRSQW)) return doValidate_IDHRSQW(oValue);
            if (sFieldName.Equals(_IDHRT)) return doValidate_IDHRT(oValue);
            if (sFieldName.Equals(_ItemCd)) return doValidate_ItemCd(oValue);
            if (sFieldName.Equals(_ItemNm)) return doValidate_ItemNm(oValue);
            if (sFieldName.Equals(_JbTypeCd)) return doValidate_JbTypeCd(oValue);
            if (sFieldName.Equals(_JbTypeDs)) return doValidate_JbTypeDs(oValue);
            if (sFieldName.Equals(_LPW)) return doValidate_LPW(oValue);
            if (sFieldName.Equals(_Mon)) return doValidate_Mon(oValue);
            if (sFieldName.Equals(_PBI)) return doValidate_PBI(oValue);
            if (sFieldName.Equals(_PERTYP)) return doValidate_PERTYP(oValue);
            if (sFieldName.Equals(_ProcSts)) return doValidate_ProcSts(oValue);
            if (sFieldName.Equals(_Qty)) return doValidate_Qty(oValue);
            if (sFieldName.Equals(_Rebate)) return doValidate_Rebate(oValue);
            if (sFieldName.Equals(_Sat)) return doValidate_Sat(oValue);
            if (sFieldName.Equals(_SHaulage)) return doValidate_SHaulage(oValue);
            if (sFieldName.Equals(_SMCgVol)) return doValidate_SMCgVol(oValue);
            if (sFieldName.Equals(_SMinChg)) return doValidate_SMinChg(oValue);
            if (sFieldName.Equals(_SMnChg)) return doValidate_SMnChg(oValue);
            if (sFieldName.Equals(_SMnSVst)) return doValidate_SMnSVst(oValue);
            if (sFieldName.Equals(_SRminVol)) return doValidate_SRminVol(oValue);
            if (sFieldName.Equals(_Sun)) return doValidate_Sun(oValue);
            if (sFieldName.Equals(_SupAddr)) return doValidate_SupAddr(oValue);
            if (sFieldName.Equals(_SupAdrCd)) return doValidate_SupAdrCd(oValue);
            if (sFieldName.Equals(_SuppCd)) return doValidate_SuppCd(oValue);
            if (sFieldName.Equals(_SupPCode)) return doValidate_SupPCode(oValue);
            if (sFieldName.Equals(_SuppNm)) return doValidate_SuppNm(oValue);
            if (sFieldName.Equals(_SuppRef)) return doValidate_SuppRef(oValue);
            if (sFieldName.Equals(_SVarRFix)) return doValidate_SVarRFix(oValue);
            if (sFieldName.Equals(_Thu)) return doValidate_Thu(oValue);
            if (sFieldName.Equals(_Tue)) return doValidate_Tue(oValue);
            if (sFieldName.Equals(_UOM)) return doValidate_UOM(oValue);
            if (sFieldName.Equals(_VldStatus)) return doValidate_VldStatus(oValue);
            if (sFieldName.Equals(_WastCd)) return doValidate_WastCd(oValue);
            if (sFieldName.Equals(_WasteNm)) return doValidate_WasteNm(oValue);
            if (sFieldName.Equals(_Wed)) return doValidate_Wed(oValue);
            if (sFieldName.Equals(_WORow)) return doValidate_WORow(oValue);
            if (sFieldName.Equals(_WTNFrom)) return doValidate_WTNFrom(oValue);
            if (sFieldName.Equals(_WTNTo)) return doValidate_WTNTo(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName) {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
		 * Link the Name Field to the Form Item.
		 */
        public void doLink_Name(string sControlName) {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
		 * Link the U_AddedBy Field to the Form Item.
		 */
        public void doLink_AddedBy(string sControlName) {
            moLinker.doLinkDataToControl(_AddedBy, sControlName);
        }
        /**
		 * Link the U_AmndTp Field to the Form Item.
		 */
        public void doLink_AmndTp(string sControlName) {
            moLinker.doLinkDataToControl(_AmndTp, sControlName);
        }
        /**
		 * Link the U_AutoComp Field to the Form Item.
		 */
        public void doLink_AutoComp(string sControlName) {
            moLinker.doLinkDataToControl(_AutoComp, sControlName);
        }
        /**
		 * Link the U_AUTSTRT Field to the Form Item.
		 */
        public void doLink_AUTSTRT(string sControlName) {
            moLinker.doLinkDataToControl(_AUTSTRT, sControlName);
        }
        /**
		 * Link the U_BatchNum Field to the Form Item.
		 */
        public void doLink_BatchNum(string sControlName) {
            moLinker.doLinkDataToControl(_BatchNum, sControlName);
        }
        /**
		 * Link the U_BthStatus Field to the Form Item.
		 */
        public void doLink_BthStatus(string sControlName) {
            moLinker.doLinkDataToControl(_BthStatus, sControlName);
        }
        /**
		 * Link the U_CHaulage Field to the Form Item.
		 */
        public void doLink_CHaulage(string sControlName) {
            moLinker.doLinkDataToControl(_CHaulage, sControlName);
        }
        /**
		 * Link the U_CMCgVol Field to the Form Item.
		 */
        public void doLink_CMCgVol(string sControlName) {
            moLinker.doLinkDataToControl(_CMCgVol, sControlName);
        }
        /**
		 * Link the U_CMinChg Field to the Form Item.
		 */
        public void doLink_CMinChg(string sControlName) {
            moLinker.doLinkDataToControl(_CMinChg, sControlName);
        }
        /**
		 * Link the U_CMnChg Field to the Form Item.
		 */
        public void doLink_CMnChg(string sControlName) {
            moLinker.doLinkDataToControl(_CMnChg, sControlName);
        }
        /**
		 * Link the U_CMnSVst Field to the Form Item.
		 */
        public void doLink_CMnSVst(string sControlName) {
            moLinker.doLinkDataToControl(_CMnSVst, sControlName);
        }
        /**
		 * Link the U_Comments Field to the Form Item.
		 */
        public void doLink_Comments(string sControlName) {
            moLinker.doLinkDataToControl(_Comments, sControlName);
        }
        /**
		 * Link the U_CRminVol Field to the Form Item.
		 */
        public void doLink_CRminVol(string sControlName) {
            moLinker.doLinkDataToControl(_CRminVol, sControlName);
        }
        /**
		 * Link the U_CusPCode Field to the Form Item.
		 */
        public void doLink_CusPCode(string sControlName) {
            moLinker.doLinkDataToControl(_CusPCode, sControlName);
        }
        /**
		 * Link the U_CustAdr Field to the Form Item.
		 */
        public void doLink_CustAdr(string sControlName) {
            moLinker.doLinkDataToControl(_CustAdr, sControlName);
        }
        /**
		 * Link the U_CustCd Field to the Form Item.
		 */
        public void doLink_CustCd(string sControlName) {
            moLinker.doLinkDataToControl(_CustCd, sControlName);
        }
        /**
		 * Link the U_CustNm Field to the Form Item.
		 */
        public void doLink_CustNm(string sControlName) {
            moLinker.doLinkDataToControl(_CustNm, sControlName);
        }
        /**
		 * Link the U_CVarRFix Field to the Form Item.
		 */
        public void doLink_CVarRFix(string sControlName) {
            moLinker.doLinkDataToControl(_CVarRFix, sControlName);
        }
        /**
		 * Link the U_EffDate Field to the Form Item.
		 */
        public void doLink_EffDate(string sControlName) {
            moLinker.doLinkDataToControl(_EffDate, sControlName);
        }
        /**
		 * Link the U_Fri Field to the Form Item.
		 */
        public void doLink_Fri(string sControlName) {
            moLinker.doLinkDataToControl(_Fri, sControlName);
        }
        /**
		 * Link the U_FrqCount Field to the Form Item.
		 */
        public void doLink_FrqCount(string sControlName) {
            moLinker.doLinkDataToControl(_FrqCount, sControlName);
        }
        /**
		 * Link the U_FrqType Field to the Form Item.
		 */
        public void doLink_FrqType(string sControlName) {
            moLinker.doLinkDataToControl(_FrqType, sControlName);
        }
        /**
		 * Link the U_IDHCMT Field to the Form Item.
		 */
        public void doLink_IDHCMT(string sControlName) {
            moLinker.doLinkDataToControl(_IDHCMT, sControlName);
        }
        /**
		 * Link the U_IDHDRVI Field to the Form Item.
		 */
        public void doLink_IDHDRVI(string sControlName) {
            moLinker.doLinkDataToControl(_IDHDRVI, sControlName);
        }
        /**
		 * Link the U_IDHRCDF Field to the Form Item.
		 */
        public void doLink_IDHRCDF(string sControlName) {
            moLinker.doLinkDataToControl(_IDHRCDF, sControlName);
        }
        /**
		 * Link the U_IDHRCDM Field to the Form Item.
		 */
        public void doLink_IDHRCDM(string sControlName) {
            moLinker.doLinkDataToControl(_IDHRCDM, sControlName);
        }
        /**
		 * Link the U_IDHRCDSA Field to the Form Item.
		 */
        public void doLink_IDHRCDSA(string sControlName) {
            moLinker.doLinkDataToControl(_IDHRCDSA, sControlName);
        }
        /**
		 * Link the U_IDHRCDSU Field to the Form Item.
		 */
        public void doLink_IDHRCDSU(string sControlName) {
            moLinker.doLinkDataToControl(_IDHRCDSU, sControlName);
        }
        /**
		 * Link the U_IDHRCDTH Field to the Form Item.
		 */
        public void doLink_IDHRCDTH(string sControlName) {
            moLinker.doLinkDataToControl(_IDHRCDTH, sControlName);
        }
        /**
		 * Link the U_IDHRCDTU Field to the Form Item.
		 */
        public void doLink_IDHRCDTU(string sControlName) {
            moLinker.doLinkDataToControl(_IDHRCDTU, sControlName);
        }
        /**
		 * Link the U_IDHRCDW Field to the Form Item.
		 */
        public void doLink_IDHRCDW(string sControlName) {
            moLinker.doLinkDataToControl(_IDHRCDW, sControlName);
        }
        /**
		 * Link the U_IDHRECFQ Field to the Form Item.
		 */
        public void doLink_IDHRECFQ(string sControlName) {
            moLinker.doLinkDataToControl(_IDHRECFQ, sControlName);
        }
        /**
		 * Link the U_IDHRSQF Field to the Form Item.
		 */
        public void doLink_IDHRSQF(string sControlName) {
            moLinker.doLinkDataToControl(_IDHRSQF, sControlName);
        }
        /**
		 * Link the U_IDHRSQM Field to the Form Item.
		 */
        public void doLink_IDHRSQM(string sControlName) {
            moLinker.doLinkDataToControl(_IDHRSQM, sControlName);
        }
        /**
		 * Link the U_IDHRSQSA Field to the Form Item.
		 */
        public void doLink_IDHRSQSA(string sControlName) {
            moLinker.doLinkDataToControl(_IDHRSQSA, sControlName);
        }
        /**
		 * Link the U_IDHRSQSU Field to the Form Item.
		 */
        public void doLink_IDHRSQSU(string sControlName) {
            moLinker.doLinkDataToControl(_IDHRSQSU, sControlName);
        }
        /**
		 * Link the U_IDHRSQTH Field to the Form Item.
		 */
        public void doLink_IDHRSQTH(string sControlName) {
            moLinker.doLinkDataToControl(_IDHRSQTH, sControlName);
        }
        /**
		 * Link the U_IDHRSQTU Field to the Form Item.
		 */
        public void doLink_IDHRSQTU(string sControlName) {
            moLinker.doLinkDataToControl(_IDHRSQTU, sControlName);
        }
        /**
		 * Link the U_IDHRSQW Field to the Form Item.
		 */
        public void doLink_IDHRSQW(string sControlName) {
            moLinker.doLinkDataToControl(_IDHRSQW, sControlName);
        }
        /**
		 * Link the U_IDHRT Field to the Form Item.
		 */
        public void doLink_IDHRT(string sControlName) {
            moLinker.doLinkDataToControl(_IDHRT, sControlName);
        }
        /**
		 * Link the U_ItemCd Field to the Form Item.
		 */
        public void doLink_ItemCd(string sControlName) {
            moLinker.doLinkDataToControl(_ItemCd, sControlName);
        }
        /**
		 * Link the U_ItemNm Field to the Form Item.
		 */
        public void doLink_ItemNm(string sControlName) {
            moLinker.doLinkDataToControl(_ItemNm, sControlName);
        }
        /**
		 * Link the U_JbTypeCd Field to the Form Item.
		 */
        public void doLink_JbTypeCd(string sControlName) {
            moLinker.doLinkDataToControl(_JbTypeCd, sControlName);
        }
        /**
		 * Link the U_JbTypeDs Field to the Form Item.
		 */
        public void doLink_JbTypeDs(string sControlName) {
            moLinker.doLinkDataToControl(_JbTypeDs, sControlName);
        }
        /**
		 * Link the U_LPW Field to the Form Item.
		 */
        public void doLink_LPW(string sControlName) {
            moLinker.doLinkDataToControl(_LPW, sControlName);
        }
        /**
		 * Link the U_Mon Field to the Form Item.
		 */
        public void doLink_Mon(string sControlName) {
            moLinker.doLinkDataToControl(_Mon, sControlName);
        }
        /**
		 * Link the U_PBI Field to the Form Item.
		 */
        public void doLink_PBI(string sControlName) {
            moLinker.doLinkDataToControl(_PBI, sControlName);
        }
        /**
		 * Link the U_PERTYP Field to the Form Item.
		 */
        public void doLink_PERTYP(string sControlName) {
            moLinker.doLinkDataToControl(_PERTYP, sControlName);
        }
        /**
		 * Link the U_ProcSts Field to the Form Item.
		 */
        public void doLink_ProcSts(string sControlName) {
            moLinker.doLinkDataToControl(_ProcSts, sControlName);
        }
        /**
		 * Link the U_Qty Field to the Form Item.
		 */
        public void doLink_Qty(string sControlName) {
            moLinker.doLinkDataToControl(_Qty, sControlName);
        }
        /**
		 * Link the U_Rebate Field to the Form Item.
		 */
        public void doLink_Rebate(string sControlName) {
            moLinker.doLinkDataToControl(_Rebate, sControlName);
        }
        /**
		 * Link the U_Sat Field to the Form Item.
		 */
        public void doLink_Sat(string sControlName) {
            moLinker.doLinkDataToControl(_Sat, sControlName);
        }
        /**
		 * Link the U_SHaulage Field to the Form Item.
		 */
        public void doLink_SHaulage(string sControlName) {
            moLinker.doLinkDataToControl(_SHaulage, sControlName);
        }
        /**
		 * Link the U_SMCgVol Field to the Form Item.
		 */
        public void doLink_SMCgVol(string sControlName) {
            moLinker.doLinkDataToControl(_SMCgVol, sControlName);
        }
        /**
		 * Link the U_SMinChg Field to the Form Item.
		 */
        public void doLink_SMinChg(string sControlName) {
            moLinker.doLinkDataToControl(_SMinChg, sControlName);
        }
        /**
		 * Link the U_SMnChg Field to the Form Item.
		 */
        public void doLink_SMnChg(string sControlName) {
            moLinker.doLinkDataToControl(_SMnChg, sControlName);
        }
        /**
		 * Link the U_SMnSVst Field to the Form Item.
		 */
        public void doLink_SMnSVst(string sControlName) {
            moLinker.doLinkDataToControl(_SMnSVst, sControlName);
        }
        /**
		 * Link the U_SRminVol Field to the Form Item.
		 */
        public void doLink_SRminVol(string sControlName) {
            moLinker.doLinkDataToControl(_SRminVol, sControlName);
        }
        /**
		 * Link the U_Sun Field to the Form Item.
		 */
        public void doLink_Sun(string sControlName) {
            moLinker.doLinkDataToControl(_Sun, sControlName);
        }
        /**
		 * Link the U_SupAddr Field to the Form Item.
		 */
        public void doLink_SupAddr(string sControlName) {
            moLinker.doLinkDataToControl(_SupAddr, sControlName);
        }
        /**
		 * Link the U_SupAdrCd Field to the Form Item.
		 */
        public void doLink_SupAdrCd(string sControlName) {
            moLinker.doLinkDataToControl(_SupAdrCd, sControlName);
        }
        /**
		 * Link the U_SuppCd Field to the Form Item.
		 */
        public void doLink_SuppCd(string sControlName) {
            moLinker.doLinkDataToControl(_SuppCd, sControlName);
        }
        /**
		 * Link the U_SupPCode Field to the Form Item.
		 */
        public void doLink_SupPCode(string sControlName) {
            moLinker.doLinkDataToControl(_SupPCode, sControlName);
        }
        /**
		 * Link the U_SuppNm Field to the Form Item.
		 */
        public void doLink_SuppNm(string sControlName) {
            moLinker.doLinkDataToControl(_SuppNm, sControlName);
        }
        /**
		 * Link the U_SuppRef Field to the Form Item.
		 */
        public void doLink_SuppRef(string sControlName) {
            moLinker.doLinkDataToControl(_SuppRef, sControlName);
        }
        /**
		 * Link the U_SVarRFix Field to the Form Item.
		 */
        public void doLink_SVarRFix(string sControlName) {
            moLinker.doLinkDataToControl(_SVarRFix, sControlName);
        }
        /**
		 * Link the U_Thu Field to the Form Item.
		 */
        public void doLink_Thu(string sControlName) {
            moLinker.doLinkDataToControl(_Thu, sControlName);
        }
        /**
		 * Link the U_Tue Field to the Form Item.
		 */
        public void doLink_Tue(string sControlName) {
            moLinker.doLinkDataToControl(_Tue, sControlName);
        }
        /**
		 * Link the U_UOM Field to the Form Item.
		 */
        public void doLink_UOM(string sControlName) {
            moLinker.doLinkDataToControl(_UOM, sControlName);
        }
        /**
		 * Link the U_VldStatus Field to the Form Item.
		 */
        public void doLink_VldStatus(string sControlName) {
            moLinker.doLinkDataToControl(_VldStatus, sControlName);
        }
        /**
		 * Link the U_WastCd Field to the Form Item.
		 */
        public void doLink_WastCd(string sControlName) {
            moLinker.doLinkDataToControl(_WastCd, sControlName);
        }
        /**
		 * Link the U_WasteNm Field to the Form Item.
		 */
        public void doLink_WasteNm(string sControlName) {
            moLinker.doLinkDataToControl(_WasteNm, sControlName);
        }
        /**
		 * Link the U_Wed Field to the Form Item.
		 */
        public void doLink_Wed(string sControlName) {
            moLinker.doLinkDataToControl(_Wed, sControlName);
        }
        /**
		 * Link the U_WORow Field to the Form Item.
		 */
        public void doLink_WORow(string sControlName) {
            moLinker.doLinkDataToControl(_WORow, sControlName);
        }
        /**
		 * Link the U_WTNFrom Field to the Form Item.
		 */
        public void doLink_WTNFrom(string sControlName) {
            moLinker.doLinkDataToControl(_WTNFrom, sControlName);
        }
        /**
		 * Link the U_WTNTo Field to the Form Item.
		 */
        public void doLink_WTNTo(string sControlName) {
            moLinker.doLinkDataToControl(_WTNTo, sControlName);
        }
        #endregion

    }
}
