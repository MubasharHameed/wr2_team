/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 08/07/2015 16:21:54
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.idh.dbObjects.numbers;

namespace com.isb.enq.dbObjects.User {
    [Serializable]
    public class IDH_ENQITEM : com.isb.enq.dbObjects.Base.IDH_ENQITEM {
        

        protected IDH_ENQUIRY moParentEnquiry;
        public IDH_ENQUIRY Enquiry {
            //set
            //{
            //    moParentEnquiry = value;
            //    if (moParentEnquiry!= null)
            //    {
            //      //  moParentEnquiry.EnquiryItems= this;
            //        SBOForm = moParentEnquiry.SBOForm;
            //        //IDHForm = moParentEnquiry.IDHForm;
            //    }
            //}
            get {
                if (moParentEnquiry == null && !string.IsNullOrWhiteSpace( U_EnqId )) {
                    moParentEnquiry = new IDH_ENQUIRY();
                    if (!moParentEnquiry.getByKey(U_EnqId)) {
                        moParentEnquiry = null;
                    }

                }
                if (moParentEnquiry == null) {
                    //DataHandler.INSTANCE.doError("The Parent Disposal Order [" + U_JobNr + "] could not be retrieved for Disposal Order Row [" + Code + "].");
                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("The Parent Enquiry [" + U_EnqId + "] could not be retrieved for Enquiry Item [" + Code + "].",
                        "ERSYFPDO", new string[] { U_EnqId, Code });
                }
                return moParentEnquiry;
            }
        }

        private bool mbDoNotSetDefault = false;
        public bool DoNotSetDefault {
            set {
                mbDoNotSetDefault = value;
            }
            get {
                return mbDoNotSetDefault;
            }
        }
        private bool mbDoNotLoadParent = false;
        public bool DoNotSetParent {
            set {
                mbDoNotLoadParent = value;
            }
            get {
                return mbDoNotLoadParent;
            }
        }        
        public IDH_ENQITEM()
            : base() {
            msAutoNumKey = "ENQUIRYD";
            // OrderByField = _Name;
            //Enquiry = null;
        }

        public IDH_ENQITEM(IDH_ENQUIRY oParent)
            : base() {
            moParentEnquiry = oParent;
            msAutoNumKey = "ENQUIRYD";
            OrderByField = _Sort;
        }

        private void setValidationRules() {
            getFieldInfo(_ItemCode).Mandatory = true;
            getFieldInfo(_ItemName).Mandatory = true;
            getFieldInfo(_ItemDesc).Mandatory = true;
        }

        //public IDH_ENQITEM(IDH_ENQUIRY oParent) : this(oParent.IDHForm, oParent.SBOForm, oParent) {
        //    //Order = oParent;
        //    //msAutoNumKey = "DISPROW";
        //    //OrderByField = _Name;
        //    //moAdditionalExpenses = new IDH_DOADDEXP(this);
        //    //moDeductions = new IDH_DODEDUCT(this);
        //}

        //    public IDH_ENQITEM(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
        //        : base(oIDHForm, oForm)
        //    {
        //        moParentEnquiry= oParent;
        //        if (moParentEnquiry!= null)
        //        {
        //            //moParentEnquiry.EnquiryItems= this;
        //            SBOForm = moParentEnquiry.SBOForm;
        //          //  IDHForm = moParentEnquiry.IDHForm;
        //        }

        //        msAutoNumKey = "ENQUIRYD";
        //        OrderByField = _Name;
        //}
        public IDH_ENQITEM(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oIDHForm, oForm) {
            msAutoNumKey = "ENQUIRYD";
        }
        public int getByParentNumber(string sParentNo) {
            return getData(IDH_ENQITEM._EnqId + " = '" + sParentNo + '\'', IDH_ENQITEM._Sort);
        }
		public void dosetCodeValue() {
            doBookmark();
            first();
            while (next()) {
                if (string.IsNullOrWhiteSpace(Code)) {
                    NumbersPair oNextNum;
                    oNextNum = this.getNewKey();
                    Code = oNextNum.CodeCode;
                    Name = oNextNum.NameNumber.ToString();
                }
            }
            doRecallBookmark();

        }
        #region overrides
        public override void doLoadRowData(bool bLoadChildren) {
         //   base.doLoadRowData(bLoadChildren);
        }

        public bool doLoadParent() {
            bool bOk = false;
            bool bLoadCh = moParentEnquiry.MustLoadChildren;
            moParentEnquiry.MustLoadChildren = false;
            try {
                bOk = moParentEnquiry.getByKey(U_EnqId);
            } catch (Exception ex) {
                throw (ex);
            } finally {
                moParentEnquiry.MustLoadChildren = bLoadCh;
            }
            return bOk;
        }

        protected void doSetDefaultsForAdd() {
            doApplyDefaults();
        }
        public override void doApplyDefaults() {
            if (mbDoNotSetDefault == true)
                return;
            base.doApplyDefaults();

            try {
                doApplyDefaultValue(_Status, "1");
                //doApplyDefaultValue(_PStat, FixedValues.getStatusOpen());

                if (Enquiry != null) {
                    doApplyDefaultValue(_EnqId, Enquiry.Code);
                    doApplyDefaultValue(_Status, "1");
                }


                string sUOM = Config.INSTANCE.getDefaultUOM();
                doApplyDefaultValue(_UOM, sUOM);
                //NumbersPair oNextNum;
                //oNextNum = this.getNewKey();
                //doApplyDefaultValue(_Code, oNextNum.CodeNumber.ToString());
                //doApplyDefaultValue(_Name, oNextNum.NameNumber.ToString());
                doApplyDefaultValue(_Sort, this.Count);
                //doApplyDefaultValue(_PCode, "");
                // doApplyDefaultValue(_Created, "N");
            } catch (Exception) { }
        }

        public override void doClearBuffers() {
            base.doClearBuffers();
        }
        public override void doSetFieldHasChanged(int iRow, string sFieldname, object oOldValue, object oNewValue) {
            base.doSetFieldHasChanged(iRow, sFieldname, oOldValue, oNewValue);

            if (sFieldname == _ItemDesc && (oNewValue == null || oNewValue.ToString().Trim() == string.Empty) && U_ItemCode != "") {
                U_ItemCode = "";
                U_ItemName = "";
            }
            if (moParentEnquiry != null && (sFieldname == _ItemCode || sFieldname == _ItemName || sFieldname == _ItemDesc || sFieldname == _WstGpCd) && oNewValue != null && !string.IsNullOrWhiteSpace(oNewValue.ToString())) {
                moParentEnquiry.FormHasDatatoSave = true;
            }
        }
        #endregion

    }
}
