/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 06/12/2016 13:18:01
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base{
   [Serializable]
	public class IDH_WRADHH: com.idh.dbObjects.DBBase { 

		private Linker moLinker = null;
       public Linker ControlLinker {
           get { return moLinker; }
           set { moLinker = value; }
       }

       private IDHAddOns.idh.forms.Base moIDHForm;
       public IDHAddOns.idh.forms.Base IDHForm {
           get { return moIDHForm; }
           set { moIDHForm = value; }
       }

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_WRADHH() : base("@IDH_WRADHH"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_WRADHH( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_WRADHH"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_WRADHH";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Alert Send By
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AlterSdBy
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AlterSdBy = "U_AlterSdBy";
		public string U_AlterSdBy { 
			get {
 				return getValueAsString(_AlterSdBy); 
			}
			set { setValue(_AlterSdBy, value); }
		}
           public string doValidate_AlterSdBy() {
               return doValidate_AlterSdBy(U_AlterSdBy);
           }
           public virtual string doValidate_AlterSdBy(object oValue) {
               return base.doValidation(_AlterSdBy, oValue);
           }

		/**
		 * Decription: Alert Send On
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AlterSdDt
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _AlterSdDt = "U_AlterSdDt";
		public DateTime U_AlterSdDt { 
			get {
 				return getValueAsDateTime(_AlterSdDt); 
			}
			set { setValue(_AlterSdDt, value); }
		}
		public void U_AlterSdDt_AsString(string value){
			setValue("U_AlterSdDt", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_AlterSdDt_AsString(){
			DateTime dVal = getValueAsDateTime(_AlterSdDt);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_AlterSdDt() {
               return doValidate_AlterSdDt(U_AlterSdDt);
           }
           public virtual string doValidate_AlterSdDt(object oValue) {
               return base.doValidation(_AlterSdDt, oValue);
           }

		/**
		 * Decription: Alert Send On
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AlterSdTm
		 * Size: 6
		 * Type: db_Date
		 * CType: string
		 * SubType: st_Time
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _AlterSdTm = "U_AlterSdTm";
		public string U_AlterSdTm { 
			get {
 				return getValueAsString(_AlterSdTm); 
			}
			set { setValue(_AlterSdTm, value); }
		}
           public string doValidate_AlterSdTm() {
               return doValidate_AlterSdTm(U_AlterSdTm);
           }
           public virtual string doValidate_AlterSdTm(object oValue) {
               return base.doValidation(_AlterSdTm, oValue);
           }

		/**
		 * Decription: Alert Send to
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AlterSdTo
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AlterSdTo = "U_AlterSdTo";
		public string U_AlterSdTo { 
			get {
 				return getValueAsString(_AlterSdTo); 
			}
			set { setValue(_AlterSdTo, value); }
		}
           public string doValidate_AlterSdTo() {
               return doValidate_AlterSdTo(U_AlterSdTo);
           }
           public virtual string doValidate_AlterSdTo(object oValue) {
               return base.doValidation(_AlterSdTo, oValue);
           }

		/**
		 * Decription: Alert Send
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_AlterSend
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _AlterSend = "U_AlterSend";
		public string U_AlterSend { 
			get {
 				return getValueAsString(_AlterSend); 
			}
			set { setValue(_AlterSend, value); }
		}
           public string doValidate_AlterSend() {
               return doValidate_AlterSend(U_AlterSend);
           }
           public virtual string doValidate_AlterSend(object oValue) {
               return base.doValidation(_AlterSend, oValue);
           }

		/**
		 * Decription: Batch Processed
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BtchClose
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BtchClose = "U_BtchClose";
		public string U_BtchClose { 
			get {
 				return getValueAsString(_BtchClose); 
			}
			set { setValue(_BtchClose, value); }
		}
           public string doValidate_BtchClose() {
               return doValidate_BtchClose(U_BtchClose);
           }
           public virtual string doValidate_BtchClose(object oValue) {
               return base.doValidation(_BtchClose, oValue);
           }

		/**
		 * Decription: Batch Status
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_BthStatus
		 * Size: 1
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _BthStatus = "U_BthStatus";
		public string U_BthStatus { 
			get {
 				return getValueAsString(_BthStatus); 
			}
			set { setValue(_BthStatus, value); }
		}
           public string doValidate_BthStatus() {
               return doValidate_BthStatus(U_BthStatus);
           }
           public virtual string doValidate_BthStatus(object oValue) {
               return base.doValidation(_BthStatus, oValue);
           }

		/**
		 * Decription: Batch Comment
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Comment
		 * Size: 10
		 * Type: db_Memo
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Comment = "U_Comment";
		public string U_Comment { 
			get {
 				return getValueAsString(_Comment); 
			}
			set { setValue(_Comment, value); }
		}
           public string doValidate_Comment() {
               return doValidate_Comment(U_Comment);
           }
           public virtual string doValidate_Comment(object oValue) {
               return base.doValidation(_Comment, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(10);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_AlterSdBy, "Alert Send By", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Alert Send By
			moDBFields.Add(_AlterSdDt, "Alert Send On", 3, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Alert Send On
			moDBFields.Add(_AlterSdTm, "Alert Send On", 4, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 6, EMPTYSTR, false, false); //Alert Send On
			moDBFields.Add(_AlterSdTo, "Alert Send to", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Alert Send to
			moDBFields.Add(_AlterSend, "Alert Send", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Alert Send
			moDBFields.Add(_BtchClose, "Batch Processed", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Batch Processed
			moDBFields.Add(_BthStatus, "Batch Status", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Batch Status
			moDBFields.Add(_Comment, "Batch Comment", 9, SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Batch Comment

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_AlterSdBy());
            doBuildValidationString(doValidate_AlterSdDt());
            doBuildValidationString(doValidate_AlterSdTm());
            doBuildValidationString(doValidate_AlterSdTo());
            doBuildValidationString(doValidate_AlterSend());
            doBuildValidationString(doValidate_BtchClose());
            doBuildValidationString(doValidate_BthStatus());
            doBuildValidationString(doValidate_Comment());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_AlterSdBy)) return doValidate_AlterSdBy(oValue);
            if (sFieldName.Equals(_AlterSdDt)) return doValidate_AlterSdDt(oValue);
            if (sFieldName.Equals(_AlterSdTm)) return doValidate_AlterSdTm(oValue);
            if (sFieldName.Equals(_AlterSdTo)) return doValidate_AlterSdTo(oValue);
            if (sFieldName.Equals(_AlterSend)) return doValidate_AlterSend(oValue);
            if (sFieldName.Equals(_BtchClose)) return doValidate_BtchClose(oValue);
            if (sFieldName.Equals(_BthStatus)) return doValidate_BthStatus(oValue);
            if (sFieldName.Equals(_Comment)) return doValidate_Comment(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_AlterSdBy Field to the Form Item.
		 */
		public void doLink_AlterSdBy(string sControlName){
			moLinker.doLinkDataToControl(_AlterSdBy, sControlName);
		}
		/**
		 * Link the U_AlterSdDt Field to the Form Item.
		 */
		public void doLink_AlterSdDt(string sControlName){
			moLinker.doLinkDataToControl(_AlterSdDt, sControlName);
		}
		/**
		 * Link the U_AlterSdTm Field to the Form Item.
		 */
		public void doLink_AlterSdTm(string sControlName){
			moLinker.doLinkDataToControl(_AlterSdTm, sControlName);
		}
		/**
		 * Link the U_AlterSdTo Field to the Form Item.
		 */
		public void doLink_AlterSdTo(string sControlName){
			moLinker.doLinkDataToControl(_AlterSdTo, sControlName);
		}
		/**
		 * Link the U_AlterSend Field to the Form Item.
		 */
		public void doLink_AlterSend(string sControlName){
			moLinker.doLinkDataToControl(_AlterSend, sControlName);
		}
		/**
		 * Link the U_BtchClose Field to the Form Item.
		 */
		public void doLink_BtchClose(string sControlName){
			moLinker.doLinkDataToControl(_BtchClose, sControlName);
		}
		/**
		 * Link the U_BthStatus Field to the Form Item.
		 */
		public void doLink_BthStatus(string sControlName){
			moLinker.doLinkDataToControl(_BthStatus, sControlName);
		}
		/**
		 * Link the U_Comment Field to the Form Item.
		 */
		public void doLink_Comment(string sControlName){
			moLinker.doLinkDataToControl(_Comment, sControlName);
		}
#endregion

	}
}
