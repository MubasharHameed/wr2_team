/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 19/10/2016 16:06:49
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base {
    [Serializable]
    public class IDH_PVHBTH : com.idh.dbObjects.DBBase {

        //private Linker moLinker = null;
        //public Linker ControlLinker {
        //    get { return moLinker; }
        //    set { moLinker = value; }
        //}

        private IDHAddOns.idh.forms.Base moIDHForm;
        public IDHAddOns.idh.forms.Base IDHForm {
            get { return moIDHForm; }
            set { moIDHForm = value; }
        }

        private static string msAUTONUMPREFIX = null;
        public static string AUTONUMPREFIX {
            get { return msAUTONUMPREFIX; }
            set { msAUTONUMPREFIX = value; }
        }

        public IDH_PVHBTH()
            : base("@IDH_PVHBTH") {
            msAutoNumPrefix = msAUTONUMPREFIX;
        }

        public IDH_PVHBTH(IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm)
            : base(oForm, "@IDH_PVHBTH") {
            msAutoNumPrefix = msAUTONUMPREFIX;
            moLinker = new Linker(this, oIDHForm);
            moIDHForm = oIDHForm;
        }

        #region Properties
        /**
		* Table name
		*/
        public readonly static string TableName = "@IDH_PVHBTH";

        /**
         * Decription: Code
         * Mandatory: tYes
         * Name: Code
         * Size: 8
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Code = "Code";
        public string Code {
            get { return (string)getValue(_Code); }
            set { setValue(_Code, value); }
        }
        public string doValidate_Code() {
            return doValidate_Code(Code);
        }
        public virtual string doValidate_Code(object oValue) {
            return base.doValidation(_Code, oValue);
        }
        /**
         * Decription: Name
         * Mandatory: tYes
         * Name: Name
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         */
        public readonly static string _Name = "Name";
        public string Name {
            get { return (string)getValue(_Name); }
            set { setValue(_Name, value); }
        }
        public string doValidate_Name() {
            return doValidate_Name(Name);
        }
        public virtual string doValidate_Name(object oValue) {
            return base.doValidation(_Name, oValue);
        }
        /**
         * Decription: Date Date
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_BatchDt
         * Size: 8
         * Type: db_Date
         * CType: DateTime
         * SubType: st_None
         * ValidValue: 
         * Value: 30/12/1899 00:00:00
         * Identity: False
         */
        public readonly static string _BatchDt = "U_BatchDt";
        public DateTime U_BatchDt {
            get {
                return getValueAsDateTime(_BatchDt);
            }
            set { setValue(_BatchDt, value); }
        }
        public void U_BatchDt_AsString(string value) {
            setValue("U_BatchDt", DateTime.Parse(value));
        }
        //Returns the Date to a SBO Date String yyyyMMdd
        public string U_BatchDt_AsString() {
            DateTime dVal = getValueAsDateTime(_BatchDt);
            if (dVal == null)
                return EMPTYSTR;
            else
                return dVal.ToString("yyyyMMdd");
        }
        public string doValidate_BatchDt() {
            return doValidate_BatchDt(U_BatchDt);
        }
        public virtual string doValidate_BatchDt(object oValue) {
            return base.doValidation(_BatchDt, oValue);
        }

        /**
         * Decription: Batch Type
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_BatchTyp
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _BatchTyp = "U_BatchTyp";
        public string U_BatchTyp {
            get {
                return getValueAsString(_BatchTyp);
            }
            set { setValue(_BatchTyp, value); }
        }
        public string doValidate_BatchTyp() {
            return doValidate_BatchTyp(U_BatchTyp);
        }
        public virtual string doValidate_BatchTyp(object oValue) {
            return base.doValidation(_BatchTyp, oValue);
        }

        /**
         * Decription: Container Qty
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_CntnrQty
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Quantity
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _CntnrQty = "U_CntnrQty";
        public double U_CntnrQty {
            get {
                return getValueAsDouble(_CntnrQty);
            }
            set { setValue(_CntnrQty, value); }
        }
        public string doValidate_CntnrQty() {
            return doValidate_CntnrQty(U_CntnrQty);
        }
        public virtual string doValidate_CntnrQty(object oValue) {
            return base.doValidation(_CntnrQty, oValue);
        }

        /**
         * Decription: Disposal Route Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_DisRCode
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _DisRCode = "U_DisRCode";
        public string U_DisRCode {
            get {
                return getValueAsString(_DisRCode);
            }
            set { setValue(_DisRCode, value); }
        }
        public string doValidate_DisRCode() {
            return doValidate_DisRCode(U_DisRCode);
        }
        public virtual string doValidate_DisRCode(object oValue) {
            return base.doValidation(_DisRCode, oValue);
        }

        /**
         * Decription: Final Batch
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_FinalBt
         * Size: 1
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _FinalBt = "U_FinalBt";
        public string U_FinalBt {
            get {
                return getValueAsString(_FinalBt);
            }
            set { setValue(_FinalBt, value); }
        }
        public string doValidate_FinalBt() {
            return doValidate_FinalBt(U_FinalBt);
        }
        public virtual string doValidate_FinalBt(object oValue) {
            return base.doValidation(_FinalBt, oValue);
        }

        /**
         * Decription: Container Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ItemCd
         * Size: 50
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ItemCd = "U_ItemCd";
        public string U_ItemCd {
            get {
                return getValueAsString(_ItemCd);
            }
            set { setValue(_ItemCd, value); }
        }
        public string doValidate_ItemCd() {
            return doValidate_ItemCd(U_ItemCd);
        }
        public virtual string doValidate_ItemCd(object oValue) {
            return base.doValidation(_ItemCd, oValue);
        }

        /**
         * Decription: Container Desc
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ItemDsc
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ItemDsc = "U_ItemDsc";
        public string U_ItemDsc {
            get {
                return getValueAsString(_ItemDsc);
            }
            set { setValue(_ItemDsc, value); }
        }
        public string doValidate_ItemDsc() {
            return doValidate_ItemDsc(U_ItemDsc);
        }
        public virtual string doValidate_ItemDsc(object oValue) {
            return base.doValidation(_ItemDsc, oValue);
        }

        /**
         * Decription: Lab Task Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_LabTskCd
         * Size: 50
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _LabTskCd = "U_LabTskCd";
        public string U_LabTskCd {
            get {
                return getValueAsString(_LabTskCd);
            }
            set { setValue(_LabTskCd, value); }
        }
        public string doValidate_LabTskCd() {
            return doValidate_LabTskCd(U_LabTskCd);
        }
        public virtual string doValidate_LabTskCd(object oValue) {
            return base.doValidation(_LabTskCd, oValue);
        }

        /**
         * Decription: Production Order
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ProdDoc
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ProdDoc = "U_ProdDoc";
        public string U_ProdDoc {
            get {
                return getValueAsString(_ProdDoc);
            }
            set { setValue(_ProdDoc, value); }
        }
        public string doValidate_ProdDoc() {
            return doValidate_ProdDoc(U_ProdDoc);
        }
        public virtual string doValidate_ProdDoc(object oValue) {
            return base.doValidation(_ProdDoc, oValue);
        }

        /**
         * Decription: Production Order Number
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_ProdNum
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _ProdNum = "U_ProdNum";
        public string U_ProdNum {
            get {
                return getValueAsString(_ProdNum);
            }
            set { setValue(_ProdNum, value); }
        }
        public string doValidate_ProdNum() {
            return doValidate_ProdNum(U_ProdNum);
        }
        public virtual string doValidate_ProdNum(object oValue) {
            return base.doValidation(_ProdNum, oValue);
        }

        /**
         * Decription: Container Qty
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Qty
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Quantity
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Qty = "U_Qty";
        public double U_Qty {
            get {
                return getValueAsDouble(_Qty);
            }
            set { setValue(_Qty, value); }
        }
        public string doValidate_Qty() {
            return doValidate_Qty(U_Qty);
        }
        public virtual string doValidate_Qty(object oValue) {
            return base.doValidation(_Qty, oValue);
        }

        /**
         * Decription: Status
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Status
         * Size: 11
         * Type: db_Numeric
         * CType: int
         * SubType: st_None
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _Status = "U_Status";
        public int U_Status {
            get {
                return getValueAsInt(_Status);
            }
            set { setValue(_Status, value); }
        }
        public string doValidate_Status() {
            return doValidate_Status(U_Status);
        }
        public virtual string doValidate_Status(object oValue) {
            return base.doValidation(_Status, oValue);
        }

        /**
         * Decription: UOM
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_UOM
         * Size: 6
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _UOM = "U_UOM";
        public string U_UOM {
            get {
                return getValueAsString(_UOM);
            }
            set { setValue(_UOM, value); }
        }
        public string doValidate_UOM() {
            return doValidate_UOM(U_UOM);
        }
        public virtual string doValidate_UOM(object oValue) {
            return base.doValidation(_UOM, oValue);
        }

        /**
         * Decription: Warehouse
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_Warehouse
         * Size: 30
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _Warehouse = "U_Warehouse";
        public string U_Warehouse {
            get {
                return getValueAsString(_Warehouse);
            }
            set { setValue(_Warehouse, value); }
        }
        public string doValidate_Warehouse() {
            return doValidate_Warehouse(U_Warehouse);
        }
        public virtual string doValidate_Warehouse(object oValue) {
            return base.doValidation(_Warehouse, oValue);
        }

        /**
         * Decription: Waste Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WastCd
         * Size: 50
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _WastCd = "U_WastCd";
        public string U_WastCd {
            get {
                return getValueAsString(_WastCd);
            }
            set { setValue(_WastCd, value); }
        }
        public string doValidate_WastCd() {
            return doValidate_WastCd(U_WastCd);
        }
        public virtual string doValidate_WastCd(object oValue) {
            return base.doValidation(_WastCd, oValue);
        }

        /**
         * Decription: Waste Desc
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WastDsc
         * Size: 100
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _WastDsc = "U_WastDsc";
        public string U_WastDsc {
            get {
                return getValueAsString(_WastDsc);
            }
            set { setValue(_WastDsc, value); }
        }
        public string doValidate_WastDsc() {
            return doValidate_WastDsc(U_WastDsc);
        }
        public virtual string doValidate_WastDsc(object oValue) {
            return base.doValidation(_WastDsc, oValue);
        }

        /**
         * Decription: Waste Qty
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WastQty
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Quantity
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _WastQty = "U_WastQty";
        public double U_WastQty {
            get {
                return getValueAsDouble(_WastQty);
            }
            set { setValue(_WastQty, value); }
        }
        public string doValidate_WastQty() {
            return doValidate_WastQty(U_WastQty);
        }
        public virtual string doValidate_WastQty(object oValue) {
            return base.doValidation(_WastQty, oValue);
        }

        /**
         * Decription: WOH Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WOHCode
         * Size: 50
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _WOHCode = "U_WOHCode";
        public string U_WOHCode {
            get {
                return getValueAsString(_WOHCode);
            }
            set { setValue(_WOHCode, value); }
        }
        public string doValidate_WOHCode() {
            return doValidate_WOHCode(U_WOHCode);
        }
        public virtual string doValidate_WOHCode(object oValue) {
            return base.doValidation(_WOHCode, oValue);
        }

        /**
         * Decription: WOR Container Qty
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WORCntQty
         * Size: 20
         * Type: db_Float
         * CType: double
         * SubType: st_Quantity
         * ValidValue: 
         * Value: 0
         * Identity: False
         */
        public readonly static string _WORCntQty = "U_WORCntQty";
        public double U_WORCntQty {
            get {
                return getValueAsDouble(_WORCntQty);
            }
            set { setValue(_WORCntQty, value); }
        }
        public string doValidate_WORCntQty() {
            return doValidate_WORCntQty(U_WORCntQty);
        }
        public virtual string doValidate_WORCntQty(object oValue) {
            return base.doValidation(_WORCntQty, oValue);
        }

        /**
         * Decription: WOR Code
         * DefaultValue: 
         * LinkedTable: 
         * Mandatory: False
         * Name: U_WORCode
         * Size: 50
         * Type: db_Alpha
         * CType: string
         * SubType: st_None
         * ValidValue: 
         * Value: 
         * Identity: False
         */
        public readonly static string _WORCode = "U_WORCode";
        public string U_WORCode {
            get {
                return getValueAsString(_WORCode);
            }
            set { setValue(_WORCode, value); }
        }
        public string doValidate_WORCode() {
            return doValidate_WORCode(U_WORCode);
        }
        public virtual string doValidate_WORCode(object oValue) {
            return base.doValidation(_WORCode, oValue);
        }

        #endregion

        #region FieldInfoSetup
        protected override void doSetFieldInfo() {
            if (moDBFields == null)
                moDBFields = new DBFields(22);

            moDBFields.Add(_Code, _Code, 0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
            moDBFields.Add(_Name, _Name, 1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
            moDBFields.Add(_BatchDt, "Date Date", 2, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Date Date
            moDBFields.Add(_BatchTyp, "Batch Type", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Batch Type
            moDBFields.Add(_CntnrQty, "Container Qty", 4, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Container Qty
            moDBFields.Add(_DisRCode, "Disposal Route Code", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Disposal Route Code
            moDBFields.Add(_FinalBt, "Final Batch", 6, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, EMPTYSTR, false, false); //Final Batch
            moDBFields.Add(_ItemCd, "Container Code", 7, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Container Code
            moDBFields.Add(_ItemDsc, "Container Desc", 8, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Container Desc
            moDBFields.Add(_LabTskCd, "Lab Task Code", 9, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Lab Task Code
            moDBFields.Add(_ProdDoc, "Production Order", 10, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Production Order
            moDBFields.Add(_ProdNum, "Production Order Number", 11, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Production Order Number
            moDBFields.Add(_Qty, "Container Qty", 12, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Container Qty
            moDBFields.Add(_Status, "Status", 13, SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, 0, false, false); //Status
            moDBFields.Add(_UOM, "UOM", 14, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 6, EMPTYSTR, false, false); //UOM
            moDBFields.Add(_Warehouse, "Warehouse", 15, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Warehouse
            moDBFields.Add(_WastCd, "Waste Code", 16, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //Waste Code
            moDBFields.Add(_WastDsc, "Waste Desc", 17, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, EMPTYSTR, false, false); //Waste Desc
            moDBFields.Add(_WastQty, "Waste Qty", 18, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //Waste Qty
            moDBFields.Add(_WOHCode, "WOH Code", 19, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //WOH Code
            moDBFields.Add(_WORCntQty, "WOR Container Qty", 20, SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 20, 0, false, false); //WOR Container Qty
            moDBFields.Add(_WORCode, "WOR Code", 21, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50, EMPTYSTR, false, false); //WOR Code

            doBuildSelectionList();
        }

        #endregion

        #region validation
        public override bool doValidation() {
            msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_BatchDt());
            doBuildValidationString(doValidate_BatchTyp());
            doBuildValidationString(doValidate_CntnrQty());
            doBuildValidationString(doValidate_DisRCode());
            doBuildValidationString(doValidate_FinalBt());
            doBuildValidationString(doValidate_ItemCd());
            doBuildValidationString(doValidate_ItemDsc());
            doBuildValidationString(doValidate_LabTskCd());
            doBuildValidationString(doValidate_ProdDoc());
            doBuildValidationString(doValidate_ProdNum());
            doBuildValidationString(doValidate_Qty());
            doBuildValidationString(doValidate_Status());
            doBuildValidationString(doValidate_UOM());
            doBuildValidationString(doValidate_Warehouse());
            doBuildValidationString(doValidate_WastCd());
            doBuildValidationString(doValidate_WastDsc());
            doBuildValidationString(doValidate_WastQty());
            doBuildValidationString(doValidate_WOHCode());
            doBuildValidationString(doValidate_WORCntQty());
            doBuildValidationString(doValidate_WORCode());

            return msLastValidationError.Length == 0;
        }
        public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_BatchDt)) return doValidate_BatchDt(oValue);
            if (sFieldName.Equals(_BatchTyp)) return doValidate_BatchTyp(oValue);
            if (sFieldName.Equals(_CntnrQty)) return doValidate_CntnrQty(oValue);
            if (sFieldName.Equals(_DisRCode)) return doValidate_DisRCode(oValue);
            if (sFieldName.Equals(_FinalBt)) return doValidate_FinalBt(oValue);
            if (sFieldName.Equals(_ItemCd)) return doValidate_ItemCd(oValue);
            if (sFieldName.Equals(_ItemDsc)) return doValidate_ItemDsc(oValue);
            if (sFieldName.Equals(_LabTskCd)) return doValidate_LabTskCd(oValue);
            if (sFieldName.Equals(_ProdDoc)) return doValidate_ProdDoc(oValue);
            if (sFieldName.Equals(_ProdNum)) return doValidate_ProdNum(oValue);
            if (sFieldName.Equals(_Qty)) return doValidate_Qty(oValue);
            if (sFieldName.Equals(_Status)) return doValidate_Status(oValue);
            if (sFieldName.Equals(_UOM)) return doValidate_UOM(oValue);
            if (sFieldName.Equals(_Warehouse)) return doValidate_Warehouse(oValue);
            if (sFieldName.Equals(_WastCd)) return doValidate_WastCd(oValue);
            if (sFieldName.Equals(_WastDsc)) return doValidate_WastDsc(oValue);
            if (sFieldName.Equals(_WastQty)) return doValidate_WastQty(oValue);
            if (sFieldName.Equals(_WOHCode)) return doValidate_WOHCode(oValue);
            if (sFieldName.Equals(_WORCntQty)) return doValidate_WORCntQty(oValue);
            if (sFieldName.Equals(_WORCode)) return doValidate_WORCode(oValue);
            return "";
        }
        #endregion

        #region LinkDataToControls
        /**
		 * Link the Code Field to the Form Item.
		 */
        public void doLink_Code(string sControlName) {
            moLinker.doLinkDataToControl(_Code, sControlName);
        }
        /**
         * Link the Name Field to the Form Item.
         */
        public void doLink_Name(string sControlName) {
            moLinker.doLinkDataToControl(_Name, sControlName);
        }
        /**
         * Link the U_BatchDt Field to the Form Item.
         */
        public void doLink_BatchDt(string sControlName) {
            moLinker.doLinkDataToControl(_BatchDt, sControlName);
        }
        /**
         * Link the U_BatchTyp Field to the Form Item.
         */
        public void doLink_BatchTyp(string sControlName) {
            moLinker.doLinkDataToControl(_BatchTyp, sControlName);
        }
        /**
         * Link the U_CntnrQty Field to the Form Item.
         */
        public void doLink_CntnrQty(string sControlName) {
            moLinker.doLinkDataToControl(_CntnrQty, sControlName);
        }
        /**
         * Link the U_DisRCode Field to the Form Item.
         */
        public void doLink_DisRCode(string sControlName) {
            moLinker.doLinkDataToControl(_DisRCode, sControlName);
        }
        /**
         * Link the U_FinalBt Field to the Form Item.
         */
        public void doLink_FinalBt(string sControlName) {
            moLinker.doLinkDataToControl(_FinalBt, sControlName);
        }
        /**
         * Link the U_ItemCd Field to the Form Item.
         */
        public void doLink_ItemCd(string sControlName) {
            moLinker.doLinkDataToControl(_ItemCd, sControlName);
        }
        /**
         * Link the U_ItemDsc Field to the Form Item.
         */
        public void doLink_ItemDsc(string sControlName) {
            moLinker.doLinkDataToControl(_ItemDsc, sControlName);
        }
        /**
         * Link the U_LabTskCd Field to the Form Item.
         */
        public void doLink_LabTskCd(string sControlName) {
            moLinker.doLinkDataToControl(_LabTskCd, sControlName);
        }
        /**
         * Link the U_ProdDoc Field to the Form Item.
         */
        public void doLink_ProdDoc(string sControlName) {
            moLinker.doLinkDataToControl(_ProdDoc, sControlName);
        }
        /**
         * Link the U_ProdNum Field to the Form Item.
         */
        public void doLink_ProdNum(string sControlName) {
            moLinker.doLinkDataToControl(_ProdNum, sControlName);
        }
        /**
         * Link the U_Qty Field to the Form Item.
         */
        public void doLink_Qty(string sControlName) {
            moLinker.doLinkDataToControl(_Qty, sControlName);
        }
        /**
         * Link the U_Status Field to the Form Item.
         */
        public void doLink_Status(string sControlName) {
            moLinker.doLinkDataToControl(_Status, sControlName);
        }
        /**
         * Link the U_UOM Field to the Form Item.
         */
        public void doLink_UOM(string sControlName) {
            moLinker.doLinkDataToControl(_UOM, sControlName);
        }
        /**
         * Link the U_Warehouse Field to the Form Item.
         */
        public void doLink_Warehouse(string sControlName) {
            moLinker.doLinkDataToControl(_Warehouse, sControlName);
        }
        /**
         * Link the U_WastCd Field to the Form Item.
         */
        public void doLink_WastCd(string sControlName) {
            moLinker.doLinkDataToControl(_WastCd, sControlName);
        }
        /**
         * Link the U_WastDsc Field to the Form Item.
         */
        public void doLink_WastDsc(string sControlName) {
            moLinker.doLinkDataToControl(_WastDsc, sControlName);
        }
        /**
         * Link the U_WastQty Field to the Form Item.
         */
        public void doLink_WastQty(string sControlName) {
            moLinker.doLinkDataToControl(_WastQty, sControlName);
        }
        /**
         * Link the U_WOHCode Field to the Form Item.
         */
        public void doLink_WOHCode(string sControlName) {
            moLinker.doLinkDataToControl(_WOHCode, sControlName);
        }
        /**
         * Link the U_WORCntQty Field to the Form Item.
         */
        public void doLink_WORCntQty(string sControlName) {
            moLinker.doLinkDataToControl(_WORCntQty, sControlName);
        }
        /**
         * Link the U_WORCode Field to the Form Item.
         */
        public void doLink_WORCode(string sControlName) {
            moLinker.doLinkDataToControl(_WORCode, sControlName);
        }
        #endregion

    }
}
