/*
 * Created by IDHSBO Generator.
 * User: Louis Viljoen
 * Date: 09/03/2016 11:24:28
 * DBObject user template
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace com.isb.enq.dbObjects.User{
   [Serializable] 
	public class IDH_DISPPRC: com.isb.enq.dbObjects.Base.IDH_DISPPRC{ 

		public IDH_DISPPRC() : base() {
            msAutoNumKey = "IDHDISPR";
		}

   	public IDH_DISPPRC( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oIDHForm, oForm){
        msAutoNumKey = "IDHDISPR";
    }
	}
}
