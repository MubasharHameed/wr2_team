/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 09/03/2016 11:24:27
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base{
   [Serializable]
	public class IDH_DISPPRC: com.idh.dbObjects.DBBase { 

		//private Linker moLinker = null;
  //     public Linker ControlLinker {
  //         get { return moLinker; }
  //         set { moLinker = value; }
  //     }

       private IDHAddOns.idh.forms.Base moIDHForm;
       public IDHAddOns.idh.forms.Base IDHForm {
           get { return moIDHForm; }
           set { moIDHForm = value; }
       }

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_DISPPRC() : base("@IDH_DISPPRC"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_DISPPRC( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_DISPPRC"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_DISPPRC";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Process Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ProcessCd
		 * Size: 10
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ProcessCd = "U_ProcessCd";
		public string U_ProcessCd { 
			get {
 				return getValueAsString(_ProcessCd); 
			}
			set { setValue(_ProcessCd, value); }
		}
           public string doValidate_ProcessCd() {
               return doValidate_ProcessCd(U_ProcessCd);
           }
           public virtual string doValidate_ProcessCd(object oValue) {
               return base.doValidation(_ProcessCd, oValue);
           }

		/**
		 * Decription: Process Name
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ProcessNm
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ProcessNm = "U_ProcessNm";
		public string U_ProcessNm { 
			get {
 				return getValueAsString(_ProcessNm); 
			}
			set { setValue(_ProcessNm, value); }
		}
           public string doValidate_ProcessNm() {
               return doValidate_ProcessNm(U_ProcessNm);
           }
           public virtual string doValidate_ProcessNm(object oValue) {
               return base.doValidation(_ProcessNm, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(4);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_ProcessCd, "Process Code", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10, EMPTYSTR, false, false); //Process Code
			moDBFields.Add(_ProcessNm, "Process Name", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Process Name

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_ProcessCd());
            doBuildValidationString(doValidate_ProcessNm());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_ProcessCd)) return doValidate_ProcessCd(oValue);
            if (sFieldName.Equals(_ProcessNm)) return doValidate_ProcessNm(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_ProcessCd Field to the Form Item.
		 */
		public void doLink_ProcessCd(string sControlName){
			moLinker.doLinkDataToControl(_ProcessCd, sControlName);
		}
		/**
		 * Link the U_ProcessNm Field to the Form Item.
		 */
		public void doLink_ProcessNm(string sControlName){
			moLinker.doLinkDataToControl(_ProcessNm, sControlName);
		}
#endregion

	}
}
