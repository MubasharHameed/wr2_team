/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 10/03/2016 11:27:12
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base{
   [Serializable]
	public class IDH_APPRSTUP: com.idh.dbObjects.DBBase { 

       private IDHAddOns.idh.forms.Base moIDHForm;
       public IDHAddOns.idh.forms.Base IDHForm {
           get { return moIDHForm; }
           set { moIDHForm = value; }
       }

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_APPRSTUP() : base("@IDH_APPRSTUP"){
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_APPRSTUP( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_APPRSTUP"){
			msAutoNumPrefix = msAUTONUMPREFIX;
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_APPRSTUP";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Approval Stage Code(OWST)
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ApStagCd
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ApStagCd = "U_ApStagCd";
		public string U_ApStagCd { 
			get {
 				return getValueAsString(_ApStagCd); 
			}
			set { setValue(_ApStagCd, value); }
		}
           public string doValidate_ApStagCd() {
               return doValidate_ApStagCd(U_ApStagCd);
           }
           public virtual string doValidate_ApStagCd(object oValue) {
               return base.doValidation(_ApStagCd, oValue);
           }

		/**
		 * Decription: Form Type
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_FormTyp
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _FormTyp = "U_FormTyp";
		public string U_FormTyp { 
			get {
 				return getValueAsString(_FormTyp); 
			}
			set { setValue(_FormTyp, value); }
		}
           public string doValidate_FormTyp() {
               return doValidate_FormTyp(U_FormTyp);
           }
           public virtual string doValidate_FormTyp(object oValue) {
               return base.doValidation(_FormTyp, oValue);
           }

		/**
		 * Decription: Originator
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Origintr
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Origintr = "U_Origintr";
		public string U_Origintr { 
			get {
 				return getValueAsString(_Origintr); 
			}
			set { setValue(_Origintr, value); }
		}
           public string doValidate_Origintr() {
               return doValidate_Origintr(U_Origintr);
           }
           public virtual string doValidate_Origintr(object oValue) {
               return base.doValidation(_Origintr, oValue);
           }

		/**
		 * Decription: Approval Stage
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Stage
		 * Size: 200
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Stage = "U_Stage";
		public string U_Stage { 
			get {
 				return getValueAsString(_Stage); 
			}
			set { setValue(_Stage, value); }
		}
           public string doValidate_Stage() {
               return doValidate_Stage(U_Stage);
           }
           public virtual string doValidate_Stage(object oValue) {
               return base.doValidation(_Stage, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(6);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_ApStagCd, "Approval Stage Code(OWST)", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Approval Stage Code(OWST)
			moDBFields.Add(_FormTyp, "Form Type", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, false); //Form Type
			moDBFields.Add(_Origintr, "Originator", 4, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Originator
			moDBFields.Add(_Stage, "Approval Stage", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200, EMPTYSTR, false, false); //Approval Stage

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_ApStagCd());
            doBuildValidationString(doValidate_FormTyp());
            doBuildValidationString(doValidate_Origintr());
            doBuildValidationString(doValidate_Stage());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_ApStagCd)) return doValidate_ApStagCd(oValue);
            if (sFieldName.Equals(_FormTyp)) return doValidate_FormTyp(oValue);
            if (sFieldName.Equals(_Origintr)) return doValidate_Origintr(oValue);
            if (sFieldName.Equals(_Stage)) return doValidate_Stage(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_ApStagCd Field to the Form Item.
		 */
		public void doLink_ApStagCd(string sControlName){
			moLinker.doLinkDataToControl(_ApStagCd, sControlName);
		}
		/**
		 * Link the U_FormTyp Field to the Form Item.
		 */
		public void doLink_FormTyp(string sControlName){
			moLinker.doLinkDataToControl(_FormTyp, sControlName);
		}
		/**
		 * Link the U_Origintr Field to the Form Item.
		 */
		public void doLink_Origintr(string sControlName){
			moLinker.doLinkDataToControl(_Origintr, sControlName);
		}
		/**
		 * Link the U_Stage Field to the Form Item.
		 */
		public void doLink_Stage(string sControlName){
			moLinker.doLinkDataToControl(_Stage, sControlName);
		}
#endregion

	}
}
