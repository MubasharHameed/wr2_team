/*
 * Created by uBCoded DBO Generator.
 * User: Louis Viljoen
 * Date: 14/03/2016 12:48:12
 *
 */

using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using com.idh.dbObjects.strct;
using com.idh.dbObjects;
using com.idh.utils;

namespace com.isb.enq.dbObjects.Base{
   [Serializable]
	public class IDH_LABTSKIMP: com.idh.dbObjects.DBBase { 

		private Linker moLinker = null;
       public Linker ControlLinker {
           get { return moLinker; }
           set { moLinker = value; }
       }

       private IDHAddOns.idh.forms.Base moIDHForm;
       public IDHAddOns.idh.forms.Base IDHForm {
           get { return moIDHForm; }
           set { moIDHForm = value; }
       }

       private static string msAUTONUMPREFIX = null;
       public static string AUTONUMPREFIX {
           get { return msAUTONUMPREFIX; }
           set { msAUTONUMPREFIX = value; }
       }

		public IDH_LABTSKIMP() : base("@IDH_LABTSKIMP"){
            msAutoNumKey = "SEQLTI";
			msAutoNumPrefix = msAUTONUMPREFIX;
		}

		public IDH_LABTSKIMP( IDHAddOns.idh.forms.Base oIDHForm, SAPbouiCOM.Form oForm ) : base( oForm, "@IDH_LABTSKIMP"){
			msAutoNumPrefix = msAUTONUMPREFIX;
            msAutoNumKey = "SEQLTI";
			moLinker = new Linker(this, oIDHForm);
			moIDHForm = oIDHForm;
		}

#region Properties
       /**
		* Table name
		*/
		public readonly static string TableName = "@IDH_LABTSKIMP";

		/**
		 * Decription: Code
		 * Mandatory: tYes
		 * Name: Code
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Code = "Code";
		public string Code { 
			get { return (string)getValue(_Code); }
			set { setValue(_Code, value); }
		}
       public string doValidate_Code() {
           return doValidate_Code(Code);
       }
       public virtual string doValidate_Code(object oValue) {
           return base.doValidation(_Code, oValue);
       }
		/**
		 * Decription: Name
		 * Mandatory: tYes
		 * Name: Name
		 * Size: 30
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 */
		public readonly static string _Name = "Name";
		public string Name { 
			get { return (string)getValue(_Name); }
			set { setValue(_Name, value); }
		}
       public string doValidate_Name() {
           return doValidate_Name(Name);
       }
       public virtual string doValidate_Name(object oValue) {
           return base.doValidation(_Name, oValue);
       }
		/**
		 * Decription: Description
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_Description
		 * Size: 250
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _Description = "U_Description";
		public string U_Description { 
			get {
 				return getValueAsString(_Description); 
			}
			set { setValue(_Description, value); }
		}
           public string doValidate_Description() {
               return doValidate_Description(U_Description);
           }
           public virtual string doValidate_Description(object oValue) {
               return base.doValidation(_Description, oValue);
           }

		/**
		 * Decription: Imported By
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ImportedBy
		 * Size: 8
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _ImportedBy = "U_ImportedBy";
		public string U_ImportedBy { 
			get {
 				return getValueAsString(_ImportedBy); 
			}
			set { setValue(_ImportedBy, value); }
		}
           public string doValidate_ImportedBy() {
               return doValidate_ImportedBy(U_ImportedBy);
           }
           public virtual string doValidate_ImportedBy(object oValue) {
               return base.doValidation(_ImportedBy, oValue);
           }

		/**
		 * Decription: Imported On
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_ImportedOn
		 * Size: 8
		 * Type: db_Date
		 * CType: DateTime
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 30/12/1899 00:00:00
		 * Identity: False
		 */
		public readonly static string _ImportedOn = "U_ImportedOn";
		public DateTime U_ImportedOn { 
			get {
 				return getValueAsDateTime(_ImportedOn); 
			}
			set { setValue(_ImportedOn, value); }
		}
		public void U_ImportedOn_AsString(string value){
			setValue("U_ImportedOn", DateTime.Parse(value));
		}
		//Returns the Date to a SBO Date String yyyyMMdd
		public string U_ImportedOn_AsString(){
			DateTime dVal = getValueAsDateTime(_ImportedOn);
			if ( dVal == null )
				return EMPTYSTR;
			else
				return dVal.ToString("yyyyMMdd");
		}
           public string doValidate_ImportedOn() {
               return doValidate_ImportedOn(U_ImportedOn);
           }
           public virtual string doValidate_ImportedOn(object oValue) {
               return base.doValidation(_ImportedOn, oValue);
           }

		/**
		 * Decription: LabTask Code
		 * DefaultValue: 
		 * LinkedTable: 
		 * Mandatory: False
		 * Name: U_LabTskCd
		 * Size: 15
		 * Type: db_Alpha
		 * CType: string
		 * SubType: st_None
		 * ValidValue: 
		 * Value: 
		 * Identity: False
		 */
		public readonly static string _LabTskCd = "U_LabTskCd";
		public string U_LabTskCd { 
			get {
 				return getValueAsString(_LabTskCd); 
			}
			set { setValue(_LabTskCd, value); }
		}
           public string doValidate_LabTskCd() {
               return doValidate_LabTskCd(U_LabTskCd);
           }
           public virtual string doValidate_LabTskCd(object oValue) {
               return base.doValidation(_LabTskCd, oValue);
           }

#endregion

#region FieldInfoSetup
		protected override void doSetFieldInfo(){
			if (moDBFields == null)
				moDBFields = new DBFields(6);

			moDBFields.Add(_Code, _Code,0, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, true);
			moDBFields.Add(_Name, _Name,1, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30, EMPTYSTR, false, true);
			moDBFields.Add(_Description, "Description", 2, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, EMPTYSTR, false, false); //Description
			moDBFields.Add(_ImportedBy, "Imported By", 3, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8, EMPTYSTR, false, false); //Imported By
			moDBFields.Add(_ImportedOn, "Imported On", 4, SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, false, false); //Imported On
			moDBFields.Add(_LabTskCd, "LabTask Code", 5, SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15, EMPTYSTR, false, false); //LabTask Code

			doBuildSelectionList();
		}

#endregion

#region validation
		public override bool doValidation(){
			msLastValidationError = "";
            doBuildValidationString(doValidate_Code());
            doBuildValidationString(doValidate_Name());
            doBuildValidationString(doValidate_Description());
            doBuildValidationString(doValidate_ImportedBy());
            doBuildValidationString(doValidate_ImportedOn());
            doBuildValidationString(doValidate_LabTskCd());

			return msLastValidationError.Length == 0;
		}
		public override string doValidation(string sFieldName, object oValue) {
            if (sFieldName.Equals(_Code)) return doValidate_Code(oValue);
            if (sFieldName.Equals(_Name)) return doValidate_Name(oValue);
            if (sFieldName.Equals(_Description)) return doValidate_Description(oValue);
            if (sFieldName.Equals(_ImportedBy)) return doValidate_ImportedBy(oValue);
            if (sFieldName.Equals(_ImportedOn)) return doValidate_ImportedOn(oValue);
            if (sFieldName.Equals(_LabTskCd)) return doValidate_LabTskCd(oValue);
           return "";
       }
#endregion

#region LinkDataToControls
		/**
		 * Link the Code Field to the Form Item.
		 */
		public void doLink_Code(string sControlName){
			moLinker.doLinkDataToControl(_Code, sControlName);
		}
		/**
		 * Link the Name Field to the Form Item.
		 */
		public void doLink_Name(string sControlName){
			moLinker.doLinkDataToControl(_Name, sControlName);
		}
		/**
		 * Link the U_Description Field to the Form Item.
		 */
		public void doLink_Description(string sControlName){
			moLinker.doLinkDataToControl(_Description, sControlName);
		}
		/**
		 * Link the U_ImportedBy Field to the Form Item.
		 */
		public void doLink_ImportedBy(string sControlName){
			moLinker.doLinkDataToControl(_ImportedBy, sControlName);
		}
		/**
		 * Link the U_ImportedOn Field to the Form Item.
		 */
		public void doLink_ImportedOn(string sControlName){
			moLinker.doLinkDataToControl(_ImportedOn, sControlName);
		}
		/**
		 * Link the U_LabTskCd Field to the Form Item.
		 */
		public void doLink_LabTskCd(string sControlName){
			moLinker.doLinkDataToControl(_LabTskCd, sControlName);
		}
#endregion

	}
}
