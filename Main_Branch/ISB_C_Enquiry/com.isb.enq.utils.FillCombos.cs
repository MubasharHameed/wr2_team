﻿using System;
using System.Collections;

using com.idh.bridge.utils;

using com.idh.utils;
using com.idh.bridge.lookups;
using com.idh.bridge;

using com.isb.enq.dbObjects.User; 

namespace com.isb.enq.utils {
    public class FillCombos {
        public static void doFillAllLiveServicesCombo(ref SAPbouiCOM.ComboBox oCombo, string sBlankValue) {
            try {

                IDH_LVSAMD oAmendList = new IDH_LVSAMD();
                oAmendList.getData();
                oCombo.ValidValues.Add("", sBlankValue);
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_ValueOnly;// = SAPbouiCOM.BoComboDisplayType.cdt_Value;
                if (oAmendList.Count > 0) {
                    int iIndex = 0;
                    for (iIndex = 0; iIndex <= oAmendList.Count - 1; iIndex++) {
                        try {
                            oAmendList.gotoRow(iIndex);
                            oCombo.ValidValues.Add(oAmendList.U_AmdDesc, oAmendList.U_AmdDesc);
                        } catch {
                        }
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }
        }
        public static void doFillAllLiveServicesCombo(ref SAPbouiCOM.ComboBoxColumn oCombo, string sBlankValue) {
            try {

                IDH_LVSAMD oAmendList = new IDH_LVSAMD();
                oAmendList.getData();
                oCombo.ValidValues.Add("", sBlankValue);
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Value;
                if (oAmendList.Count > 0) {
                    int iIndex = 0;
                    for (iIndex = 0; iIndex <= oAmendList.Count - 1; iIndex++) {
                        try {
                            oAmendList.gotoRow(iIndex);
                            oCombo.ValidValues.Add(oAmendList.U_AmdDesc, "");
                        } catch {
                        }
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }
        }
    }
}
