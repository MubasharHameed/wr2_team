﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using IDHAddOns.idh.controls;

using com.isb.enq.dbObjects.User;
using com.isb.enq;
using com.isb.enq.lookups;
using com.isb.enq.utils;
using com.idh.bridge;
using com.idh.dbObjects.numbers;
using SAPbouiCOM;

namespace com.isb.enq.forms.orders {
    class Disposal : WR1_Forms.idh.forms.orders.Disposal {

        public Disposal(IDHAddOns.idh.addon.Base oParent, int iMenuPosition) : base(oParent, iMenuPosition) {
        }

        public string doGenerateLabTask(SAPbouiCOM.Form oForm) {
            try {
                IDH_LABTASK oLabTask = new IDH_LABTASK();
                NumbersPair oNxtNo = oLabTask.getNewKey();
                oLabTask.Code = oNxtNo.CodeCode;
                oLabTask.Name = oNxtNo.NameCode;

                oLabTask.U_LabItemRCd = (string)getDFValue(oForm, msRowTable, idh.dbObjects.User.IDH_DISPROW._WasCd);
                oLabTask.U_LabICd = (string)getDFValue(oForm, msRowTable, idh.dbObjects.User.IDH_DISPROW._WasCd);
                oLabTask.U_LabINm = (string)getDFValue(oForm, msRowTable, idh.dbObjects.User.IDH_DISPROW._WasDsc);

                oLabTask.U_BPCode = (string)getFormDFValue(oForm, idh.dbObjects.User.IDH_DISPORD._CardCd);
                oLabTask.U_BPName = (string)getFormDFValue(oForm, idh.dbObjects.User.IDH_DISPORD._CardNM);

                oLabTask.U_RowStatus = "Requested";
                oLabTask.U_DtReceived = DateTime.Now;

                oLabTask.U_AssignedTo = "";
                oLabTask.U_AnalysisReq = "";
                oLabTask.U_ExtStatus = "";
                oLabTask.U_ExtComments = "";
                oLabTask.U_ImpResults = "";
                oLabTask.U_Decision = "Pending";
                oLabTask.U_ObjectCd = (string)getDFValue(oForm, msRowTable, idh.dbObjects.User.IDH_DISPROW._Code);
                oLabTask.U_ObjectType = "DO";
                oLabTask.doAddDataRow();

                return oLabTask.Code;

            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("doCreateLabTask()", "Error adding Lab Task row: " + ex.Message);
                return "-1";
            }
        }

        public override bool doItemEvent(Form oForm, ref ItemEvent pVal, ref bool BubbleEvent) {
            idh.dbObjects.User.IDH_DISPROW oDOR = thisDOR(oForm);
            idh.dbObjects.User.IDH_DISPORD oDO = thisDO(oForm);
            string sItemID = pVal.ItemUID;

            if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED) {
                if (pVal.BeforeAction) {
                    SAPbouiCOM.Item oItem = oForm.Items.Item(sItemID);
                    if (oItem.Type == SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX) {
                        //Check if Generate Sample Checkbox is checked
                        //Generate Sample record in system 
                        if (sItemID == "IDH_SAMCHK") {
                            if (pVal.FormMode == (int)SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
                                if (oDOR.U_SampleRef.Length > 0 && oDOR.U_Sample == "N") {
                                    oDOR.U_Sample = "Y";
                                    IDHAddOns.idh.addon.Base.PARENT.doMessage("Cannot untick the checkbox. There is a Lab Task linked with the DO.");
                                    return false;
                                } else {
                                    string sResult = doGenerateLabTask(oForm);
                                    if (!sResult.Equals("-1")) {
                                        oDOR.U_Sample = "Y";
                                        oDOR.U_SampleRef = sResult;
                                        oDOR.U_SampStatus = "Pending";
                                    } else {
                                        oDOR.U_Sample = "N";
                                        oDOR.U_SampleRef = string.Empty;
                                        oDOR.U_SampStatus = string.Empty;
                                        IDHAddOns.idh.addon.Base.PARENT.doMessage("System could not generate the Sample Task successfully!");
                                    }
                                }
                            } else {
                                IDHAddOns.idh.addon.Base.PARENT.doMessage("Can NOT generate sample in ADD mode!");
                                oDOR.U_Sample = "N";
                            }
                            return false;
                        }
                    }
                }
            }
            return base.doItemEvent(oForm, ref pVal, ref BubbleEvent);
        }
    }
}
