﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.bridge.lookups;
using IDHAddOns.idh.data;
using com.idh.bridge;

namespace com.isb.forms.Enquiry.admin {
    public class MainForm {

        public MainForm(IDHAddOns.idh.addon.Base oParent) {
            //IDHAddOns.idh.addon.Base oParent
            doAddForms(oParent);
        }
        //[System.Runtime.InteropServices.DllImport("User32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto, ExactSpelling = true)]
        //public static extern System.IntPtr SetParent(System.IntPtr hWndChild, System.IntPtr hWndParent);

        //[System.Runtime.InteropServices.DllImport("user32.dll")]
        //public static extern bool RedrawWindow(IntPtr hWnd, IntPtr rectUpdate, IntPtr hrgnUpdate, UInt32 flags);

        public static void doCreateMenu(string sMenuID) {
            SAPbouiCOM.Menus oMenus;
            SAPbouiCOM.MenuItem oMenuItem;
            SAPbouiCOM.MenuCreationParams oCreationPackage;

            //Enquiry And Lab
            if (Config.ParameterWithDefault("ENQMOD", "FALSE") == "ENQMOD_TRUE") {
                try {
                    oMenuItem = IDHAddOns.idh.addon.Base.APPLICATION.Menus.Item("IDHENQ");
                } catch (Exception ex) {
                    oMenuItem = IDHAddOns.idh.addon.Base.APPLICATION.Menus.Item(sMenuID);
                    oMenus = oMenuItem.SubMenus;

                    oCreationPackage = (SAPbouiCOM.MenuCreationParams)IDHAddOns.idh.addon.Base.APPLICATION.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);
                    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                    oCreationPackage.UniqueID = "IDHENQ";
                    oCreationPackage.String = Translation.getTranslatedWord("Enquiry Module");
                    oCreationPackage.Position = 7000;
                    try {
                        oMenus.AddEx(oCreationPackage);
                    } catch (Exception exx) {
                    }
                }

                try {
                    oMenuItem = IDHAddOns.idh.addon.Base.APPLICATION.Menus.Item("IDHENQADM");
                } catch (Exception ex) {
                    oMenuItem = IDHAddOns.idh.addon.Base.APPLICATION.Menus.Item("IDHENQ");
                    oMenus = oMenuItem.SubMenus;

                    oCreationPackage = (SAPbouiCOM.MenuCreationParams)IDHAddOns.idh.addon.Base.APPLICATION.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);
                    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                    oCreationPackage.UniqueID = "IDHENQADM";
                    oCreationPackage.String = Translation.getTranslatedWord("Admin");
                    oCreationPackage.Position = 1;

                    try {
                        oMenus.AddEx(oCreationPackage);
                    } catch (Exception ex1) {
                    }
                }

                try {
                    oMenuItem = IDHAddOns.idh.addon.Base.APPLICATION.Menus.Item("ENQAPR");
                } catch (Exception ex) {
                    oMenuItem = IDHAddOns.idh.addon.Base.APPLICATION.Menus.Item("IDHENQADM");
                    oMenus = oMenuItem.SubMenus;

                    oCreationPackage = (SAPbouiCOM.MenuCreationParams)IDHAddOns.idh.addon.Base.APPLICATION.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);
                    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                    oCreationPackage.UniqueID = "ENQAPR";
                    oCreationPackage.String = Translation.getTranslatedWord("Approvals");
                    oCreationPackage.Position = 2;

                    try {
                        oMenus.AddEx(oCreationPackage);
                    } catch (Exception ex1) {

                    }
                }
            }
        }

        public static void doAddForms(IDHAddOns.idh.addon.Base oParent) {
            com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.CreateBP));
             
            //  com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.CreateWOOptions));

            com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.admin.WasteItemValidations));
            com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.WOQVersionsList));
            com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.WOQuoteVersions));

            com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.Enquiry), "IDHENQ", 1);//IDHORD
            //   com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.WOQuote), "IDHWOQ");
            //IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.WOQuote(oParent, "IDHORD", 81));            
            com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.WOQuote), "IDHENQ", 81);//IDHORD
            //com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.EnqQuestions));
            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.EnqQuestions(oParent));

            com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.admin.WGValidationMaster), "IDHENQADM", 70);//IDHADM
            com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.admin.QuestionnaireMaster), "IDHENQADM", 71);//IDHADM
            com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.admin.DisposalProcess), "IDHENQADM", 72);//IDHADM
            com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.admin.DisposalTreatment), "IDHENQADM", 73);//IDHADM
            com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.admin.DisposalRoute), "IDHENQADM", 74);//IDHADM

            //doAddForm

            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.Search.WasteGroupSearch(oParent));
            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.admin.WasteGroupValidations(oParent, "IDHENQADM", 74));//IDHADM
            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.admin.AdditionalItemValidations(oParent, "IDHENQADM", 75));//IDHADM

            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.Search.SearchEnquiry(oParent));
            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.Search.EnqWasteItemSearch(oParent));
            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.Search.EnqWasteProfileItemSearch(oParent));
            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.Search.DisposalRouteSearch(oParent));


            //Approval
            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.admin.Approval.ApprovalSetup(oParent, "ENQAPR", 1));//IDHADM
            //com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.admin.Approval.Approvals), "ENQAPR", 2);
            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.admin.Approval.Approvals(oParent, "ENQAPR", 2));//

            //  IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.admin.AdditionalItemSearch(oParent));

            /*       com.idh.forms.oo.Form.doRegisterForm(GetType(com.isb.forms.Enquiry.admin.WGValidationMaster), "IDHADM", 70)
                   doAddForm(New com.isb.forms.Enquiry.admin.WasteGroupValidations(Me, "IDHADM", 71))
               */


            //Enquiry Lab Module Forms 
            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.LabTaskAnalysis(oParent));
            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.ItemLabAnalysisTempt(oParent));
            com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.admin.LabStatus), "IDHENQ", 79);
            com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.admin.LabAnalysis), "IDHENQ", 80);

            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.LabItem(oParent, "IDHENQ", 81));
            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.LabPreProcess(oParent, "IDHENQ", 82));
            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.LabTask(oParent, "IDHENQ", 83));
            //IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.LabResults(oParent, "IDHENQ", 84));
            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.LabResults(oParent));
            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.WORList(oParent));

            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.Manager.EnquiryManager(oParent, "IDHENQ", 85));
            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.Manager.WOQManager(oParent, "IDHENQ", 86));
            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.Reports.ReportSelection(oParent, "IDHENQ", -1));

            //Project DORA

            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.PJDORA.PBIMassSelector(oParent, "IDHPS", 87));
            com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.PJDORA.admin.PBIMassSelectorConfig), "IDHADM", 80);
            com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.PJDORA.admin.PBIMassUpdaterAmendmentList), "IDHADM", 81);

            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.PJDORA.PBIMassUpdater(oParent, "IDHPS", 88));
            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.PJDORA.AdHocWOMassUpdater(oParent, "IDHPS", 89));
            IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.PJDORA.WORMassUpdater(oParent, "IDHPS", 90));

           // IDHAddOns.idh.addon.Base.PARENT.doAddForm(new com.isb.forms.Enquiry.WORREquestDate(oParent, "IDHENQ", null));
            com.idh.forms.oo.Form.doRegisterForm(typeof(com.isb.forms.Enquiry.WORREquestDate));
            //com.idh.win.forms.SBOForm.doRegisterForm(typeof(Form1), "IDHENQ", "Win - Disposal Order", 60);
            //com.idh.form.SBO.Info oInfo = new com.idh.form.SBO.Info();
            //oInfo.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            //oInfo.ControlBox = true;
            //oInfo.MaximizeBox = true;
            //oInfo.MinimizeBox = true;
            //oInfo.doShowInSBO();

            //com.uBC.form.Test1 oTest1 = new com.uBC.form.Test1();
            //oTest1.Show();


            //com.idh.controls.SBOWindow oSBOWin1 = com.idh.controls.SBOWindow.getInstanceByTitle(true);
            //int iHandle = oSBOWin1.Handle;
            //oInfo.set
            ///doSBOInfoPanel(new com.isb.forms.Enquiry.Form1(), oSBOWin1);
            // oSBOWin1.doSBOInfoPanel(new com.isb.forms.Enquiry.Form1());
            //  com.idh.controls.WindowsAPIs.
            //  iHandle = oSBOWin1.Handle;


        }

        //public static void doSBOInfoPanel(System.Windows.Forms.Form oForm, com.idh.controls.SBOWindow oSBOWin1) {


        //    SetParent(oForm.Handle, oSBOWin1.Handle);
        //    oForm.TopMost = true;
        //    oForm.Show();
        //    oForm.ForeColor = System.Drawing.Color.Red;
        //    oForm.Refresh();
        //    oForm.Activate();
        //}
    }
    public class DataStructures {
        public static bool FORCECREATE = false;
        #region Add Messages
        public static void doAddMessages(string sTableName, double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            if (CurrentReleaseCount < 1054 || FORCECREATE) {
                object[,] oData32 = {
                        {"Code","Name","Cat","Val","Desc"},
                        {"WRNBPA11","WRNBPA11","Messages","Updating Address in Waste Profile.","Updating Address in Waste Profile."}};
                oBase.doUTAddValues(sTableName, oData32, true);
            }
            if (CurrentReleaseCount < 1054 || FORCECREATE) {
                object[,] oData32 = {
                    {"Code","Name","Cat","Val","Desc"},
                    {"ERVLDFLD","ERVLDFLD","Messages","Error: [%1] is missing.","Value for the field is missing."},
                    {"INFDOCNO","INFDOCNO","Messages","[%1] document added with number: [%2]","Document added with number"},
                    {"ERPKEXIS","ERPKEXIS","Messages","[%1] already exists.","Record already exists."},
                    {"ERSYBPAD","ERSYBPAD","Messages","[%1] Add - [%2] Error while adding [%1].","Error while adding data."} };
                oBase.doUTAddValues(sTableName, oData32, true);
            }
            if (CurrentReleaseCount < 1030 || FORCECREATE) {
                object[,] oData32 = {
                        {"Code","Name","Cat","Val","Desc"},
                        {"ERVEQ001","ERVEQ001","Messages","No Row in Enquiry to copy on WOQ.","No Row in Enquiry to copy on WOQ."},
                        {"ERVEQ002","ERVEQ002","Messages","Row has been copied to WOQ, you cannot delete.","Row has been copied to WOQ, you cannot delete."},
                        {"ERVEQ003","ERVEQ003","Messages","All linked additonal items will also delete. Are you sure to continue?.","All linked additonal items will also delete. Are you sure to continue?"},
                        {"ERVEQ004","ERVEQ004","Messages","you must select rows from same BP.","you must select rows from same BP."}};
                oBase.doUTAddValues(sTableName, oData32, true);
            }

            if (CurrentReleaseCount < 1054 || FORCECREATE) {
                object[,] oData32 = {
                    {"Code","Name","Cat","Val","Desc"},
                    {"ERCRETBP","ERCRETBP","Messages","Error: Please create customer","You must create customer."},
                    {"ERSAVEFR","ERSAVEFR","Messages","Error: Please save form before proceed","You must save form to proceed."}
                                    };
                oBase.doUTAddValues(sTableName, oData32, true);
            }
            if (CurrentReleaseCount < 1038 || FORCECREATE) {
                object[,] oData32 = {
                        {"Code","Name","Cat","Val","Desc"},
                        {"WRNBPA12","WRNBPA12","Messages","WOQ sent for approval.","WOQ sent for approval."},
                        {"WRNBPA13","WRNBPA13","Messages","You cannot send approval request as your name is not is not listed in approvals.","You cannot send approval request as your name is not is not listed in approvals."},
                        {"WRNBPA14", "WRNBPA14", "Messages", "Approval Message Error - [%1] Error while sending approval request message.", "Error while sending approval request message."},
                        {"WRNBPA15", "WRNBPA15", "Messages", "System failed to load approval data.", "System failed to load approval data."}
                                    };

                oBase.doUTAddValues(sTableName, oData32, true);
            }
            if (CurrentReleaseCount < 1041 || FORCECREATE) {
                object[,] oData32 = {
                        {"Code","Name","Cat","Val","Desc"},
                        {"ENQMG001","ENQMG001","Messages","Do you want to create One WO from Quote or multiple WO(s) for each row in quote?.","Do you want to create One WO from Quote or multiple WO(s) for each row in quote?"},
                        {"ENQMG002","ENQMG002","Messages","Do you want to create one Quote or multiple quote(s) for each row in enquiry?","Do you want to create one Quote or multiple quote(s) for each row in enquiry?"},
                        {"ERVEQ016","ERVEQ016","Messages","Please get approval for quote before creating WO.","Please get approval for quote before creating WO."},
                        {"ERVEQ017","ERVEQ017","Messages","WO cannot create. Invalid Quote status.","WO cannot create. Invalid Quote status."}};
                oBase.doUTAddValues(sTableName, oData32, true);
            }

            if (CurrentReleaseCount < 1047 || FORCECREATE) {
                object[,] oData32 = {
                        {"Code","Name","Cat","Val","Desc"},
                        {"ERVEQ018","ERVEQ018","Messages","WO cannot create. There is no row available to convert into WOR.","WO cannot create. There is no row available to convert into WOR"}};
                oBase.doUTAddValues(sTableName, oData32, true);
            }
            if (CurrentReleaseCount < 1051 || FORCECREATE) {
                object[,] oData32 = {
                    {"Code","Name","Cat","Val","Desc"},
                    {"ERSDSMIS","ERSDSMIS","Messages","Error: [%1] not received.","MSDS for item not received."}
                                    };
                oBase.doUTAddValues(sTableName, oData32, true);
            }
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                object[,] oData32 = {
                        {"Code","Name","Cat","Val","Desc"},
                        {"ENQMG004","ENQMG004","Messages","Some of the rows are already processed. Do you want to recreate WO(s) for them?","Some of the rows are already processed. Do you want to recreate WO(s) for them?"},
                        {"LAB00001","LAB00001","Messages","Cannot untick the checkbox. Cancel the Lab Task if not required.","Cannot untick the checkbox. Cancel the Lab Task if not required."}
                                    };
                oBase.doUTAddValues(sTableName, oData32, true);
                //{"LAB00001","LAB00001","Messages","Cannot untick the checkbox. Cancel the Lab Task if not required.","Cannot untick the checkbox. There is a Lab Task linked with the row. Cancel the Lab Task if not required."}

            }
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                object[,] oData32 = {
                    {"Code","Name","Cat","Val","Desc"},
                    {"LAB00002","LAB00002","Messages","Error sending the lab task alert - [%1].","Could not sent alert to user."},
                    {"NOSALEPR","NOSALEPR","Messages","Please assign the sales employee of BP - [%1] and update the task assignment accordingly.","Please assign sales employee to the BP."},
                    {"LABARTM1", "LABARTM1", "Messages", "A new lab task has been assign to you.", "Alert message for Sales employee(Stage 1)."},
                    {"LABARTS1", "LABARTS1", "Messages", "Lab Task Update", "Alert subject for Sales employee(Stage 1)."},

                    {"LABARTM2", "LABARTM2", "Messages", "Sample has been received.", "Alert message on receive of sample."},
                    {"LABARTS2", "LABARTS2", "Messages", "Sample received", "Alert subject on receive of sample."},

                    {"LABARTM3", "LABARTM3", "Messages", "Analysis Required for Lab Task has been updated.", "Alert message on update of Analysis Required."},
                    {"LABARTS3", "LABARTS3", "Messages", "Analysis Required updated", "Alert subject on update of Analysis Required."},


                    {"LABARTM4", "LABARTM4", "Messages", "Analysis has been completed for the lab task.", "Alert message on update of Analysis Date."},
                    {"LABARTS4", "LABARTS4", "Messages", "Analysis Completed", "Alert subject on update of Analysis Date."},

                    {"LABARTM5", "LABARTM5", "Messages", "A Lab task has been assigned to you.", "Alert message on change of user."},
                    {"LABARTS5", "LABARTS5", "Messages", "Lab Task Assigned", "Alert subject on change of user."},

                                    };
                oBase.doUTAddValues(sTableName, oData32, true);
            }

            //production order errors
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                object[,] oData32 = {
                    {"Code","Name","Cat","Val","Desc"},
                    {"ERPRD001","ERPRD001","Messages","Production Order Add Error - [%1] Error Adding the production order.", "Error adding the production order."},
                    {"ERPRD002","ERPRD002","Messages","Issue for Production Add Error - [%1] Error while adding the issue for production.", "Error adding the issue for production."},
                    {"ERPRD003","ERPRD003","Messages","Receive from Production Add Error - [%1] Error while adding the receive from production.", "Error adding the receive from production."},
                    {"ERPRD004","ERPRD004","Messages","Production Order Completion Error - [%1] Error while the completing the production order.", "Error completing the production order."},
                    {"LAB00003","LAB00003","Messages","Final Batch %1 is not completed. It has still %2 containers to dispose. Are you sure to complete the batch?","Final batch has some remaining quantity to dispose."}
                                    };
                oBase.doUTAddValues(sTableName, oData32, true);
            }

            if (CurrentReleaseCount < 1055 || FORCECREATE) {
                object[,] oData32 = {
                    {"Code","Name","Cat","Val","Desc"},
                    {"LSRER001","LSRER001","Messages","No record found for the PBI [%1] in ready to process by service.", "No record found for the PBI in ready to process by service."},
                    {"LSRER002","LSRER002","Messages","Duplicate PBI#[%1] records found in Live service. Please remove dulplicate PBI from live service batchs.", "Duplicate PBI records found in Live service. Please remove dulplicate PBI from live service batchs."},
                    {"LSRER003","LSRER003","Messages","Amendment type [%1] is not active/available in the Amendment table.", "Amendment type is not active/available in the Amendment table."},
                    {"LSRER004","LSRER004","Messages","PBI#[%1] is not been sent for review. You must send it for review before process.", "PBI is not been sent for review. You must send it for review before process."},
                    {"LSRER005","LSRER005","Messages","PBI#[%1] for the batch#[%2] has already processed.","PBI for the batch has already processed."},
                    {"LSRER005","LSRER005","Messages","Failed to load the PBI# [%1].","Failed to find the given PBI."},
                    {"LSRER006","LSRER006","Messages","Are you sure to delete batches [%1]. It will delete the complete batche(s)","Are you sure to delete batches. It will delete the complete batches."}
                                    };
                oBase.doUTAddValues(sTableName, oData32, true);
            }


			if (CurrentReleaseCount < 1062 || FORCECREATE) {
                object[,] oData32 = {
                        {"Code","Name","Cat","Val","Desc"},
                        {"ERVEQ019","ERVEQ019","Messages","1:Keep existing Sample Ref# %1 for the waste item %2?\n" +
                            "2:Create new Sample Ref# %1 for the waste item %2 ?\n"+
                            "3:Create Brand New Sample Ref for %2 ?"
                            ,"Do you want to keep old lab task reference number for the waste item?"}};
                oBase.doUTAddValues(sTableName, oData32, true);
            }
            if (CurrentReleaseCount < 1065 || FORCECREATE) {
                object[,] oData32 = {
                        {"Code","Name","Cat","Val","Desc"},
                        {"ENQCNREF","ENQCNREF","Messages","This will update contact details. Are you sure you want to continue?",
                        "This will update contact details. Are you sure you want to continue?"} };
                oBase.doUTAddValues(sTableName, oData32, true);
            }
            if (CurrentReleaseCount < 1054 || FORCECREATE) {
                object[,] oData32 = {
                    {"Code","Name","Cat","Val","Desc"},
                    {"ERCRETBP","ERCRETBP","Messages","Error: Please create customer","You must create customer."},
                    {"ERSAVEFR","ERSAVEFR","Messages","Error: Please save form before proceed","You must save form to proceed."}
                                    };
                oBase.doUTAddValues(sTableName, oData32, true);
            }
            if (CurrentReleaseCount < 1068 || FORCECREATE) {
                object[,] oData32 = {
                    {"Code","Name","Cat","Val","Desc"},
                    {"ERIVBPTP","ERIVBPTP","Messages","Error: You cannot create WOH with a Lead Business Partner. Please change BO type to continue...","You cannot create WOH with a Lead Business Partner. Please change BO type to continue..."}
                                    };
                oBase.doUTAddValues(sTableName, oData32, true);
            }

            if (CurrentReleaseCount < 1074 || FORCECREATE) {
                object[,] oData32 = {
                        {"Code","Name","Cat","Val","Desc"},
                        {"ERVEQ020","ERVEQ020","Messages","You do not have rights to create business partner.","You do not have rights to create business partner."}};
                oBase.doUTAddValues(sTableName, oData32, true);
            }
        }
        #endregion

        #region do WR Config Table
        public static void doConfigTableData(string sTableName, double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            if (CurrentReleaseCount < 1033 || FORCECREATE) {
                object[,] oData = {{"Code", "Name", "Cat", "Val", "Desc"},
                        {"WOQDOFS", "WOQDOFS", "Waste Order Quote", "TRUE", "Do use Offset prices (TRUE, FALSE)"}
                };
                oBase.doUTAddValues(sTableName, oData, false);
            }
            if (CurrentReleaseCount < 1045 || FORCECREATE) {
                object[,] oData = {{"Code", "Name", "Cat", "Val", "Desc"},
                        {"ENQPCADR", "ENQPCADR", "Enquiry", "TRUE", "Pull customer default address at Enquiry/WOQ header."}
                };
                oBase.doUTAddValues(sTableName, oData, false);
            }
            if (CurrentReleaseCount < 1045 || FORCECREATE) {
                string sQry = "Delete From [@IDH_WRCONFIG] Where Code='ENQPCADR' ";
                oBase.doUpdateQuery("Delete ENQPCADR", sQry);
            }
            if (CurrentReleaseCount < 1045 || FORCECREATE) {
                object[,] oData = {{"Code", "Name", "Cat", "Val", "Desc"},
                        {"DRTFRMAT", "DRTFRMAT", "Waste Order Quote", "U_TipCd.U_DispRtID.3,U_DisProcCd,U_DisTrtCd", "Disposal Route string."}
                };
                oBase.doUTAddValues(sTableName, oData, false);
                //Disposal Route string,for tip you can change field & length,rest of segments can use with or with out pad like FieldName.Length(e.g. U_DisProcCd.3).
            }

            if (CurrentReleaseCount < 1051 || FORCECREATE) {
                object[,] oData = {{"Code", "Name", "Cat", "Val", "Desc"},
                        {"WOQDOC", "WOQDOC", "Reports", "", "WOQ Docs Report(s)"}
                };
                oBase.doUTAddValues(sTableName, oData, false);
            }
            if (CurrentReleaseCount < 1052 || FORCECREATE) {
                object[,] oData = {{"Code", "Name", "Cat", "Val", "Desc"},
                        {"ACTSMPRQ", "ACTSMPRQ", "Acceptance", "FALSE", "Check Sample Required for the waste item and add a Lab task."},
                        {"WOQDFQTY", "WOQDFQTY", "Waste Order Quote", "1", "Default quantity for a WOQ row."}
                };
                oBase.doUTAddValues(sTableName, oData, false);
            }
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                object[,] oData = {{"Code", "Name", "Cat", "Val", "Desc"},
                        {"HIDWPTMP", "HIDWPTMP", "General - Items", "", "Comma separated list of Users who have right to create waste profile template."},
                        {"CPSMPDUP", "CPSMPDUP", "ENQ-LAB", "TRUE", "Create Sample Task on Duplication of WOQ if required."},
                        {"LABUSR02", "LABUSR02", "ENQ-LAB", "", "Alert this user after receiving pre-process sample(Trigger:Received date entered)."},
                        {"LABUSR03", "LABUSR03", "ENQ-LAB", "", "Alert this user after updating pre-process Analysis Required(Trigger:Analysed Req Add/Updat)."},
                        {"LABUSR04", "LABUSR04", "ENQ-LAB", "", "Alert this user after updating  pre-process Date Analysed(Trigger:Date Analysed entered)."}
                };
                oBase.doUTAddValues(sTableName, oData, false);
            }
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                object[,] oData = {{"Code", "Name", "Cat", "Val", "Desc"},
                        {"LABUSR10", "LABUSR10", "ENQ-LAB", "", "Alert this user after receiving WOR based sample(Trigger:Received date entered)."},
                        {"LABUSR11", "LABUSR11", "ENQ-LAB", "", "Alert this user after updating  WOR based Date Analysed(Trigger:Date Analysed entered)."}
                };
                oBase.doUTAddValues(sTableName, oData, false);
            }
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                object[,] oData = {{"Code", "Name", "Cat", "Val", "Desc"},
                        {"DFWORJPTY", "DFWORJPTY", "Acceptance", "Delivery", "Default Job Type for WOR created by Lab Pre-Process Module."}
                };
                oBase.doUTAddValues(sTableName, oData, false);
            }
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                object[,] oData = {{"Code", "Name", "Cat", "Val", "Desc"},
                        {"LABPDGGI", "LABPDGGI", "Reports", "", "Pending Good Issues After Lab Pre Processor."},
                        {"WOQMGDOC", "WOQMGDOC", "Reports", "", "WOQ Manager Doc Report(s). Use sRowNums as parameter with multi value set to true."},
                        {"FNBTDOC", "FNBTDOC", "Reports", "", "Final Batch Doc Button Report(s)."}
                };
                oBase.doUTAddValues(sTableName, oData, false);
            }
            if (CurrentReleaseCount < 1062 || FORCECREATE) {
                object[,] oData = {{"Code", "Name", "Cat", "Val", "Desc"},
                        {"DFTCDWQ", "DFTCDWQ", "Waste Order Quote", "", "Default transaction code for WOR created from WOQ."}
                };
                oBase.doUTAddValues(sTableName, oData, false);
            }
            if (CurrentReleaseCount < 1065 || FORCECREATE) {
                object[,] oData = {{"Code", "Name", "Cat", "Val", "Desc"},
                        {"DFEQBPTP", "DFEQBPTP", "Enquiry", "C", "Default BP type for a new BP created in Enquiry/WOR(C/L/V)."},
                        {"REFRSWOQ", "REFRSWOQ", "Work Order Quote", "TRUE", "Update Work Order Quote Waste/Disposal on change of Disposal Route"}
                };
                oBase.doUTAddValues(sTableName, oData, false);
            }

            if (CurrentReleaseCount < 1069 || FORCECREATE) {
                object[,] oData = {{"Code", "Name", "Cat", "Val", "Desc"},
                        {"AUSTARTY", "AUSTARTY", "Mass PBI Updater", "False", "Always sets auto start date to the jobs."}
                };
                oBase.doUTAddValues(sTableName, oData, false);
            }
            
        }
        #endregion

        #region do UserTables
        public static void doSetupUserDefined(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            DataStructures moDataStructure = new DataStructures();
            moDataStructure.doCreateEnquiryTables(CurrentReleaseCount, oBase);
            moDataStructure.doCreateWR1DocStatusTable(CurrentReleaseCount, oBase);
            moDataStructure.doCreateWasteGroupValidationTables(CurrentReleaseCount, oBase);
            moDataStructure.doCreateAdditionalItemWasteGroupValidationTables(CurrentReleaseCount, oBase);
            moDataStructure.doCreateWOQTables(CurrentReleaseCount, oBase);
            moDataStructure.doCreateLabTables(CurrentReleaseCount, oBase);
            moDataStructure.doCreateQuestionnairesTables(CurrentReleaseCount, oBase);
            moDataStructure.doCreateApprovalTables(CurrentReleaseCount, oBase);
            moDataStructure.doCreateDisposalProcessTable(CurrentReleaseCount, oBase);
            moDataStructure.doCreateDisposalTreatmentTable(CurrentReleaseCount, oBase);
            moDataStructure.doCreateDisposalRouteCodeTable(CurrentReleaseCount, oBase);

            moDataStructure.doCreateProvisioalBatchTable(CurrentReleaseCount, oBase);

            moDataStructure.doCreatePBIMassUpdaterTables(CurrentReleaseCount, oBase);
            moDataStructure.doAddWOAdhocUpdater(CurrentReleaseCount, oBase);
            moDataStructure.doAddWORMassUpdater(CurrentReleaseCount, oBase);
            moDataStructure.doAddServiceAdditionalInfo(CurrentReleaseCount, oBase);
            //
            //if (CurrentReleaseCount < 1035) {
            //    string sQry = "Delete From [@IDH_FORMSET] where U_FormID='IDH_DRTSR' and U_FieldID Like 'i.FrgnName'";
            //    oBase.doUpdateQuery(sQry);
            //}

            //Contract
          //  moDataStructure.doAddContractTable(CurrentReleaseCount, oBase);

            moDataStructure.doCreateView(CurrentReleaseCount, oBase);

        }

        private void doCreateEnquiryTables(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_ENQUIRY";
            if (CurrentReleaseCount < 1033 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Enquiry Header")) {
                    oBase.doAddNumberSeq("ENQUIRYH", "Enquiry Header Series", 0);
                     
                }
            }
            if (CurrentReleaseCount < 1033 || FORCECREATE) {
                //oBase.doMDRemoveField(sTableName, "CardCode");
                //oBase.doMDRemoveField(sTableName, "CardName");
                //oBase.doMDRemoveField(sTableName, "Address");
                //oBase.doMDRemoveField(sTableName, "Street");
                //oBase.doMDRemoveField(sTableName, "Block");
                //oBase.doMDRemoveField(sTableName, "City");
                //oBase.doMDRemoveField(sTableName, "County");
                //oBase.doMDRemoveField(sTableName, "State");
                //oBase.doMDRemoveField(sTableName, "ZipCode");
                //oBase.doMDRemoveField(sTableName, "Country");
                //oBase.doMDRemoveField(sTableName, "CName");
                //oBase.doMDRemoveField(sTableName, "Phone1");
                //oBase.doMDRemoveField(sTableName, "E_Mail");
                //oBase.doMDRemoveField(sTableName, "Notes");
                //oBase.doMDRemoveField(sTableName, "ClgCode");
                //oBase.doMDRemoveField(sTableName, "Recontact");
                //oBase.doMDRemoveField(sTableName, "Status");
                //oBase.doMDRemoveField(sTableName, "AttendUser");
                //oBase.doMDRemoveField(sTableName, "User");

                oBase.doMDCreateField(sTableName, "CardCode", "BP Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                oBase.doMDCreateField(sTableName, "CardName", "BP Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "Address", "Address ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDUpdateField(sTableName, "AddrssLN", "Address LineNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "Street", "Street", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "Block", "Block", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "City", "City", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "County", "County", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "State", "State", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);
                oBase.doMDCreateField(sTableName, "ZipCode", "Post Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "Country", "Country", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);
                oBase.doMDCreateField(sTableName, "CName", "Contact Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDCreateField(sTableName, "Phone1", "Telephone", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_Phone, 20);
                oBase.doMDCreateField(sTableName, "E_Mail", "Email Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "Notes", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 8000);
                oBase.doMDCreateField(sTableName, "ClgCode", "Number", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                oBase.doMDCreateField(sTableName, "Recontact", "Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                oBase.doMDCreateField(sTableName, "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                oBase.doMDCreateField(sTableName, "AttendUser", "Assigned To", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 155);
                oBase.doMDCreateField(sTableName, "User", "Created By", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 155);
                oBase.doMDCreateField(sTableName, "Desc", "Description", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);

            }
            if (CurrentReleaseCount < 1041 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "Source", "Source", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                oBase.doMDRemoveField(sTableName, "EnqId");

            }
            sTableName = "@IDH_ENQITEM";
            if (CurrentReleaseCount < 1033 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Enquiry Items")) {
                    oBase.doAddNumberSeq("ENQUIRYD", "Enquiry Items Series", 0);
                    //oBase.doMDCreateField(sTableName, "EnqId", "Enquiry ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    //oBase.doMDCreateField(sTableName, "ItemCode", "Material Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    //oBase.doMDCreateField(sTableName, "ItemName", "Material Description", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                    //oBase.doMDCreateField(sTableName, "WstGpCd", "Waste Group Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    //oBase.doMDCreateField(sTableName, "WstGpNm", "Waste Group Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    //oBase.doMDCreateField(sTableName, "EstQty", "Estimated Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 19);
                    //oBase.doMDCreateField(sTableName, "UOM", "Unit of Measurement", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 7);
                    //oBase.doMDCreateField(sTableName, "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    //oBase.doMDCreateField(sTableName, "Created", "Item Created", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 9);
                }
            }
            if (CurrentReleaseCount < 1033 || FORCECREATE) {
                //oBase.doMDRemoveField(sTableName, "EnqId");
                //oBase.doMDRemoveField(sTableName, "ItemCode");
                //oBase.doMDRemoveField(sTableName, "ItemName");
                //oBase.doMDRemoveField(sTableName, "WstGpCd");
                //oBase.doMDRemoveField(sTableName, "WstGpNm");
                //oBase.doMDRemoveField(sTableName, "EstQty");
                //oBase.doMDRemoveField(sTableName, "UOM");
                //oBase.doMDRemoveField(sTableName, "Status");
                //oBase.doMDRemoveField(sTableName, "Created");


                oBase.doMDCreateField(sTableName, "EnqId", "Enquiry ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "ItemCode", "Material Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "ItemName", "Material Description", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "ItemFName", "Material Description(F)", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "WstGpCd", "Waste Group Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "WstGpNm", "Waste Group Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "EstQty", "Estimated Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 19);
                oBase.doMDCreateField(sTableName, "UOM", "Unit of Measurement", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 7);
                oBase.doMDCreateField(sTableName, "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2);
                oBase.doMDCreateField(sTableName, "AddItm", "Additional Item", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "Sort", "Sort Order", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                oBase.doMDCreateField(sTableName, "PCode", "PCode", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                //oBase.doMDCreateField(sTableName, "Created", "Item Created", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 9)
                oBase.doMDCreateField(sTableName, "WOQID", "WOQ ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "WOQLineID", "WOQ Line ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
            }
            if (CurrentReleaseCount < 1044 || FORCECREATE) {
                //oBase.doMDUpdateField(sTableName, "ItemName", "Material Description(F)", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDUpdateField(sTableName, "ItemName2", "Material Description(F)", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doUpdateQuerySkipPatchQueue("Enquiry.admin.main", "Update [@IDH_ENQITEM] Set U_ItemName2=Left(U_ItemName,100)");
                oBase.doMDRemoveField(sTableName, "ItemName");
                oBase.doMDUpdateField(sTableName, "ItemName", "Material Description(F)", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doUpdateQuerySkipPatchQueue("Enquiry.admin.main", "Update [@IDH_ENQITEM] Set U_ItemName=U_ItemName2");
                oBase.doMDRemoveField(sTableName, "ItemName2");
                oBase.doMDCreateField(sTableName, "ItemDesc", "Material Description", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
            }
        }

        private void doCreateWR1DocStatusTable(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_WRSTUSNW";
            if (CurrentReleaseCount < 1033 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "WR1 Doc Status New")) {
                    //doAddNumberSeq("ObjectType", "Enquiry Header Series", 0)
                    oBase.doMDCreateField(sTableName, "ObjType", "Object Type", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "Value", "Value", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "Desc", "Description", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                }
                //Enquiry 101
                object[,] oData = {
                        {"Code", "Name", "Value", "ObjType", "Desc"},
                        {"1","1","1","101","New"},
                        {"2","2","2","101","Started"},
                        {"3", "3", "3", "101", "Completed"},
                        {"4", "4", "4", "101", "Close"},
                        {"5", "5", "5", "101", "Cancel"}};
                oBase.doUTAddValues(sTableName, oData, false);
                //Enquiry Item 102
                object[,] oData2 = {{"Code", "Name", "Value", "ObjType", "Desc"},
                          {"6", "6", "1", "102", "New"},
                          {"7", "7", "2", "102", "Started"},
                          {"8", "8", "3", "102", "Completed"},
                          {"9", "9", "4", "102", "Close"},
                          {"10", "10", "5", "102", "Cancel"}};
                oBase.doUTAddValues(sTableName, oData2, false);

                //Validation Item 103
                object[,] oData3 = {{"Code", "Name", "Value", "ObjType", "Desc"},
                          {"11", "11", "1", "103", "Field"},
                          {"12", "12", "2", "103", "Range"},
                          {"13", "13", "3", "103", "Boolean"},
                          {"14", "14", "4", "103", "Combo"}
                          };
                oBase.doUTAddValues(sTableName, oData3, false);
                //Validation DataType Item 104
                object[,] oData4 ={{"Code", "Name", "Value", "ObjType", "Desc"},
                          {"15", "15", "1", "104", "AlphaNumeric"},
                          {"16", "16", "2", "104", "Numeric"},
                          {"17", "17", "3", "104", "Decimal"}
                          };
                //{"18", "18", "4", "104", "Radio"} _
                oBase.doUTAddValues(sTableName, oData4, false);
                //'Fields Count on Waste Item Validation
                object[,] oData5 = {{"Code", "Name", "Value", "ObjType", "Desc"},
                          {"19", "19", "1", "105", "1"},
                          {"20", "20", "2", "105", "2"}
                        };
                oBase.doUTAddValues(sTableName, oData5, false);

                //WOQ 106

                object[,] oData6 = {
                        {"Code", "Name", "Value", "ObjType", "Desc"},
                        {"31","31","1","106","New"},
                        {"32","32","2","106","Waiting For Approval"},
                        {"33", "33", "3", "106", "Review Advised"},
                        {"34", "34", "4", "106", "Rejected"},
                        {"35", "35", "5", "106", "Approved"},
                        {"36", "36", "6", "106", "WO Created"},
                        {"37", "37", "7", "106", "Cancelled"},
                        {"38", "38", "8", "106", "Closed"}};
                oBase.doUTAddValues(sTableName, oData6, false);
                //WOQ Item 107
                object[,] oData7 = {{"Code", "Name", "Value", "ObjType", "Desc"},
                          {"41", "41", "1", "107", "New"},
                          {"42", "42", "2", "107", "Started"},
                          {"43", "43", "3", "107", "Quoted"},
                          {"44", "44", "4", "107", "Close"},
                          {"45", "45", "5", "107", "Cancel"}};
                oBase.doUTAddValues(sTableName, oData7, false);

                //WOQ Source 108
                object[,] oData8 = {{"Code", "Name", "Value", "ObjType", "Desc"},
                          {"51", "51", "1", "108", "Email"},
                          {"52", "52", "2", "108", "Phone"},
                          {"52", "53", "3", "108", "3rd Party"},
                          {"53", "54", "4", "108", "Sales Team"},
                          {"54", "55", "5", "108", "Other"}};
                oBase.doUTAddValues(sTableName, oData8, false);

                //Validation DataType Item 104
                object[,] oData9 ={{"Code", "Name", "Value", "ObjType", "Desc"},
                          {"70", "70", "4", "104", "Quantity"},
                          {"71", "71", "5", "104", "Price"},
                          {"72", "72", "6", "104", "Text"},
                          {"72", "72", "7", "104", "Percentage"}
                          };
                //{"18", "18", "4", "104", "Radio"} _
                oBase.doUTAddValues(sTableName, oData9, false);
                //Validation Item 103
                object[,] oData10 = {{"Code", "Name", "Value", "ObjType", "Desc"},
                          {"73", "73", "5", "103", "Checkbox"}
                          };
                oBase.doUTAddValues(sTableName, oData10, false);
            }

            if (CurrentReleaseCount < 1051 || FORCECREATE) {
                //WOQ Row Status 107// updated some of status and their order
                object[,] oData = {
                        {"Code", "Name", "Value", "ObjType", "Desc"},
                        {"44", "44", "4", "107", "WO Created"},
                        {"45", "45", "5", "107", "Close"},
                        {"46", "46", "6", "107", "Cancel"}};
                oBase.doUTAddValues(sTableName, oData, false);
            }
        }

        private void doCreateWasteGroupValidationTables(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_WGVDMS";
            if (CurrentReleaseCount < 1054 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Waste Group Validation Master")) {
                    oBase.doAddNumberSeq("WGPVLDMS", "Waste Group Validation Master", 0);
                    oBase.doMDRemoveField(sTableName, "Fields");
                    oBase.doMDCreateField(sTableName, "ValidCd", "Validation Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "ValidNm", "Validation Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "Type", "Validation Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "Fields", "Fields", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "DtType", "Data Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "DBField", "BP Field Names", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "Common", "Common Attribute", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Expression", "Expression", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "Length", "Length", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 9);

                }
            }
            //if (CurrentReleaseCount > 1030 && CurrentReleaseCount < 1032) {
            //oBase.doMDRemoveField(sTableName, "ValidCd");
            //oBase.doMDUpdateField(sTableName, "ValidCd", "Validation Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
            // oBase.doMDCreateField(sTableName, "ValidCd", "Validation Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
            //}
            if (CurrentReleaseCount < 1054 || FORCECREATE) {
                oBase.doMDUpdateField(sTableName, "ValidCd", "Validation Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
            }
            //Adding Default Rows as on from OITM
            if (CurrentReleaseCount < 1038 || FORCECREATE) {
                //string sQry = "Delete From [@IDH_WGVDMS]";
                //oBase.doUpdateQuery(sQry);
                //sQry = "Delete From [@IDH_WGVALDS]";
                //oBase.doUpdateQuery(sQry);

                com.idh.dbObjects.User.IDH_WGVDMS moDBObject = new com.idh.dbObjects.User.IDH_WGVDMS();

                object[,] oData = { { "Code", "Name", "ValidCd", "ValidNm", "Type", "Fields", "DtType", "DBField", "Common", "Expression","Length" },
                                 {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EWC Code","EWC Code","1","1","1","U_EWC","Y","CFL:IDH_EWCLUP:IDH_WTECSR:IDH_WTWC#U_EWC","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Division","Division","1","1","1","U_WTDIV","N","","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"UN#","UN#","1","1","1","U_WTUNCD","N","","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"UN Class","UN Class","1","1","1","U_WTUNCLS","N","","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Material Type","Material Type","1","1","1","U_WTUNTYP","N","","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"State","State","1","1","1","U_WTUNSTE","N","","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Flashpoint","Flashpoint","1","1","1","U_WTUNFLP","N","","30"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Density","Density","1","1","1","U_WTUNDNS","N","","30"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"PH","PH","1","1","1","U_WTUNPH","N","","30"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Pack Group","Pack Group","1","1","1","U_WTUNGRP","N","","30"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Disposal Method","Disposal Method","1","1","1","U_WTDIMTH","N","","30"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Documentation","Documentation","4","1","1","U_WTDOCUM","Y","FILLDB:@IDH_WRCONFIG:Code:U_Desc:Code like 'WDOC%'","30"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Haz Code","Haz Code","1","1","1","U_HAZCD","Y","CFL:IDH_HAZLUP:IDH_WTHZSR:IDH_HZCOD#U_HAZCD","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Default Weight","Default Weight","1","1","4","U_WTDFW","N","","11"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Use Default Weight","Use Default Weight","3","1","1","U_WTUDW","N","","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Stock Item Link","Stock Item Link","1","1","1","U_WTITMLN","Y","","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EN Category","EN Category","4","1","1","U_WTENCAT","N","FILLDB:@IDH_WTENCT:Code:U_Desc:","100"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EN WEEE % Received Re-Use","EN WEEE % Received Re-Use","1","1","2","U_WTENITU","N","","9"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EN Incomming Based On(P-Protoc","EN Incomming Based On(P-Protoc","4","1","1","U_WTENIPA","N","FILLVAL:1;Actual#P;Protocol","2"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EN WEEE % Sent for Recovery","EN WEEE % Sent for Recovery","1","1","2","U_WTENOTRC","N","","9"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EN WEEE % Sent for Re-use","EN WEEE % Sent for Re-use","1","1","2","U_WTENOTRU","N","","9"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EN WEEE % Wastage","EN WEEE % Wastage","1","1","2","U_WTENOWA","N","","9"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EN Outgoing Based On(P-Protoco","EN Outgoing Based On(P-Protoco","4","1","1","U_WTENOPA","N","FILLVAL:1;Actual#P;Protocol","2"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Indicate that Item can be boug","Indicate that Item can be boug","5","1","1","U_IDHCWBB","Y","","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"WEEE Category","WEEE Category","4","1","1","U_IDHWEEECD","N","FILLDB:@IDH_WEEE:Code:Name:","30"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"WEEE Classification","WEEE Classification","4","1","1","U_IDHWEEECL","N","FILLVAL:;None#Domestic;Domestic#Commercial;Commercial","30"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Average Load Weight","Average Load Weight","1","1","4","U_IDHALW","N","","11"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Rebate Item","Rebate Item","5","1","1","U_IDHREB","Y","","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Carrier Rebate Item","Carrier Rebate Item","5","1","1","U_IDHCREB","Y","","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Basel Code","Basel Code","1","1","1","U_BASCODE","N","","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"OECD Code","OECD Code","1","1","1","U_OECDCODE","N","","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EWC Export Code","EWC Export Code","1","1","1","U_EWCECODE","N","","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EWC Import Code","EWC Import Code","1","1","1","U_EWCICODE","N","","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"(iv) National Code Export","(iv) National Code Export","1","1","1","U_NCODEE","N","","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"(v) National Code Import","(v) National Code Import","1","1","1","U_NCODEI","N","","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"(vi) Other (specify)","(vi) Other (specify)","1","1","1","U_NCODEO","N","","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Y-Code","Y-Code","1","1","1","U_YCODE","N","","8"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"H-Code Domestic","H-Code Domestic","1","1","1","U_HCODED","N","","8"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"H-Code Foreign","H-Code Foreign","1","1","1","U_HCODEF","N","","8"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"D-Code","D-Code","4","1","1","U_DCODE","Y","FILLDB:@IDH_WTDIRE:Code:U_WTOD:","8"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"R-Code","R-Code","1","1","1","U_RCODE","Y","","8"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Proper Shipping Name","Proper Shipping Name","1","1","1","U_PSN","N","","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Mode of Transit","Mode of Transit","1","1","1","U_MODETR","N","","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"WP Template","WP Template","5","1","1","U_WPTemplate","Y","","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Approved","Approved","5","1","1","U_Approved","Y","","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Letter on File","Letter on File","5","1","1","U_LOFile","Y","","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Hazardous","Hazardous","5","1","1","U_Hazordous","Y","","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Disposal Facility","Disposal Facility","1","1","1","U_DispFacility","N","CFL:DSPLKP:IDHCSRCH:TRG#.DSPLKP;IDH_BPCOD#U_DispFacCd;IDH_NAME#U_DispFacility;IDH_TYPE#.S","150"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Is Poisonous","Is Poisonous","4","1","1","U_IsPoisonous","N","FILLVAL:1;Yes#B;No","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Is Vented Drums","Is Vented Drums","4","1","1","U_IsVentedDrums","N","FILLVAL:1;Yes#B;No","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Is Pumpable","Is Pumpable","4","1","1","U_IsPumpable","N","FILLVAL:1;Yes#B;No","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Is Polymerizable","Is Polymerizable","4","1","1","U_IsPolymerizable","N","FILLVAL:1;Yes#B;No","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Is Benzene WOp","Is Benzene WOp","4","1","1","U_IsBenzeneWO","N","FILLVAL:1;Yes#B;No","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Is Ozone","Is Ozone","4","1","1","U_IsOzone","N","FILLVAL:1;Yes#B;No","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Is Scrap","Is Scrap","4","1","1","U_IsScrap","N","FILLVAL:1;Yes#B;No","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Is Marine Polutant","Is Marine Polutant","4","1","1","U_IsMarinePolu","N","FILLVAL:1;Yes#B;No","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Special Handling","Special Handling","1","1","1","U_SpHandling","N","","200"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Color","Color","1","1","1","U_Color","N","","50"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Odor","Odor","4","1","1","U_Odor","N","FILLVAL:1;Yes#B;No","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Odor Description","Odor Description","1","1","1","U_OdorDesc","N","","100"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Layers","Layers","4","1","1","U_Layers","N","FILLVAL:1;Single Phase#B;Bi-Layered;#C;Multi-Layered","50"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Specific Gravity","Specific Gravity","1","1","1","U_SpGravity","N","","100"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Range","Range","1","1","1","U_Range","N","","50"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Physical State at 70F","Physical State at 70F","4","1","1","U_PhyState","N","FILLVAL:1;Solid#B;Liquid#C;Semi-Solid#D;Other","50"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Physical State Description","Physical State Description","1","1","1","U_PhyStateDesc","N","","100"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Viscosity","Viscosity","4","1","1","U_Viscosity","N","FILLVAL:1;Low#B;Medium#C;High","50"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"BTULB","BTULB","1","1","1","U_BTULB","N","","100"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"pH Level","pH Level","4","1","1","U_PHLevel","N","FILLVAL:1;<= 2#B;2 thru 6#C;6 thru 8#D;8 thru 12.5#E;>= 12.5","50"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"pH Exact","pH Exact","1","1","1","U_PHExact","N","","50"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Flash Point","Flash Point","4","1","1","U_FlashPoint","N","FILLVAL:1;None#B;< 140 F#C;140-199 F#D;> 200 F","50"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Flash Point Exact","Flash Point Exact","1","1","1","U_FPExact","N","","50"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Frequency","Frequency","4","1","1","U_Frequency","N","FILLVAL:1;One Time#B;Monthly#C;Annually#D;Other","50"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Volume","Volume","1","1","1","U_Volume","N","","50"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Containers","Containers","1","1","1","U_Containers","N","CFL:CNTLKP:IDHISRC:IDH_GRPCOD#.113","50"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Containers Other","Containers Other","1","1","1","U_CntOther","N","","100"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Liquid Percentage","Liquid Percentage","1","1","2","U_WCLiquidPerc","N","","9"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Solid Percentage","Solid Percentage","1","1","2","U_WCSolidPerc","N","","9"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Sludges Percentage","Sludges Percentage","1","1","2","U_WCSludgesPerc","N","","9"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Gas Percentage","Gas Percentage","1","1","2","U_WCGasPerc","N","","9"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"PCBs Percentage","PCBs Percentage","1","1","2","U_WCPCBsPerc","N","","9"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Cyanides Percentage","Cyanides Percentage","1","1","2","U_WCCyanidesPerc","N","","9"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Sulfides Percentage","Sulfides Percentage","1","1","2","U_WCSulfidesPerc","N","","9"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Phenolics Percentage","Phenolics Percentage","1","1","2","U_WCPhenolicsPerc","N","","9"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Halogen Percentage","Halogen Percentage","1","1","2","U_WCHalogenPerc","N","","9"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Dioxins Percentage","Dioxins Percentage","1","1","2","U_WCDioxinsPerc","N","","9"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Additional Info","Additional Info","4","1","1","U_WCAddInfo","N","FILLVAL:1;Yes#B;No","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Waste Compo. Comments","Waste Compo. Comments","1","1","6","U_WCWasteComm","N","","0"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Metal Atribute","Metal Atribute","4","1","1","U_MetalAtr","N","FILLVAL:1;None#B;TCLP (MG/L)#C;Total (PPM)","50"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Source Code","Source Code","4","1","1","U_SourceCd","N","FILLDB:@IDH_GCODE:U_Gcode:U_Desc","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Form Code","Form Code","4","1","1","U_FormCd","N","FILLDB:@IDH_WCODE:U_WCode:U_Desc","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Sys. Type Table Code","Sys. Type Table Code","4","1","1","U_SysTTCd","N","FILLDB:@IDH_HCode:U_HCode:U_Hdesc","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Shipping Name","Shipping Name","1","1","6","U_ShippingNm","N","","0"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Haz Class","Haz Class","1","1","1","U_HazClass","N","","100"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"UN NA No.","UN NA No.","1","1","1","U_UNNANo","N","","100"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Packing Group","Packing Group","1","1","1","U_PackGrp","N","","100"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Process Genrating","Process Genrating","1","1","1","U_ProcessGen","N","","100"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"NWW","NWW","4","1","1","U_NWW","N","FILLVAL:1;NWW#B;WW","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Waste Group","Waste Group","4","1","1","U_WSTGRP","Y","FILLDB:@ISB_WASTEGRP:U_WstGpCd:U_WstGpNm","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"WPT User","WPT User","4","1","1","U_WPTUser","Y","FILLDB:OUSR:User_Code:U_Name","8"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Approved User","Approved User","4","1","1","U_AppUser","Y","FILLDB:OUSR:User_Code:U_Name","8"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"LOF User","LOF User","4","1","1","U_LOFUser","Y","FILLDB:OUSR:User_Code:U_Name","8"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EPA Waste Code1","EPA Waste Code1","1","1","1","U_EPAWstCd1","N","CFL:IDH_EP1LUP:EPASCR:TRG#.LKP_EPACD1","200"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EPA Waste Code2","EPA Waste Code2","1","1","1","U_EPAWstCd2","N","CFL:IDH_EP2LUP:EPASCR:TRG#.LKP_EPACD2","200"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EPA Waste Code3","EPA Waste Code3","1","1","1","U_EPAWstCd3","N","CFL:IDH_EP3LUP:EPASCR:TRG#.LKP_EPACD3","200"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EPA Waste Code4","EPA Waste Code4","1","1","1","U_EPAWstCd4","N","CFL:IDH_EP4LUP:EPASCR:TRG#.LKP_EPACD4","200"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EPA Waste Code5","EPA Waste Code5","1","1","1","U_EPAWstCd5","N","CFL:IDH_EP5LUP:EPASCR:TRG#.LKP_EPACD5","200"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EPA Waste Code6","EPA Waste Code6","1","1","1","U_EPAWstCd6","N","CFL:IDH_EP6LUP:EPASCR:TRG#.LKP_EPACD6","200"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Disposal Facility Code","Disposal Facility Code","1","1","1","U_DispFacCd","N","","20"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Arsenic Percentage","Arsenic Percentage","1","1","7","U_MArsenicPerc","N","","11"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Barium Percentage","Barium Percentage","1","1","7","U_MBariumPerc","N","","11"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Cadmium Percentage","Cadmium Percentage","1","1","7","U_MCadmiumPerc","N","","11"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Chromium Percentage","Chromium Percentage","1","1","7","U_MChromiumPerc","N","","11"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Copper Percentage","Copper Percentage","1","1","7","U_MCopperPerc","N","","11"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Lead Percentage","Lead Percentage","1","1","7","U_MLeadPerc","N","","11"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Mercury Percentage","Mercury Percentage","1","1","7","U_MMercuryPerc","N","","11"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Nickel Percentage","Nickel Percentage","1","1","7","U_MNickelPerc","N","","11"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Selenium Percentage","Selenium Percentage","1","1","7","U_MSeleniumPerc","N","","11"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Silver Percentage","Silver Percentage","1","1","7","U_MSilverPerc","N","","11"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Zinc Percentage","Zinc Percentage","1","1","7","U_MZincPerc","N","","11"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"OtherMat Percentage","OtherMat Percentage","1","1","7","U_MOtherMatPerc","N","","11"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"DF Address","DF Address","1","1","1","U_DFAddress","N","CFL:DFALKP:IDHASRCH:IDH_CUSCOD#U_DispFacCd;IDH_NAME#U_DispFacility;IDH_ADRTYP#.S","100"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EPA Waste Name1","EPA Waste Name1","1","1","1","U_EPAWstNm1","N","","200"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EPA Waste Name2","EPA Waste Name2","1","1","1","U_EPAWstNm2","N","","200"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EPA Waste Name3","EPA Waste Name3","1","1","1","U_EPAWstNm3","N","","200"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EPA Waste Name4","EPA Waste Name4","1","1","1","U_EPAWstNm4","N","","200"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EPA Waste Name5","EPA Waste Name5","1","1","1","U_EPAWstNm5","N","","200"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"EPA Waste Name6","EPA Waste Name6","1","1","1","U_EPAWstNm6","N","","200"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Purchasing UoM","Purchasing UoM","1","1","1","BuyUnitMsr","Y","","50"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Sale UOM","Sale UOM","1","1","1","SalUnitMsr","Y","","50"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Inventory UOM","Inventory UOM","1","1","1","InvntryUom","Y","","50"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Stock Item","Stock Item","1","1","1","InvntItem","Y","","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Sale Item","Sale Item","1","1","1","SellItem","Y","","1"},
                        {moDBObject.getNewKey(true ).CodeCode,moDBObject.Name,"Purchase Item","Purchase Item","1","1","1","PrchseItem","Y","","1"}
                        };
                oBase.doUTAddValues(sTableName, oData, false);

            }


            sTableName = "@IDH_WGVALDS";
            if (CurrentReleaseCount < 1054 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Waste Group Validations")) {
                    oBase.doAddNumberSeq("WGPVALDS", "Waste Group Validations", 0);
                    oBase.doMDCreateField(sTableName, "ValidCd", "Validation Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "ValidNm", "Validation Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);

                    oBase.doMDCreateField(sTableName, "WstGpCd", "Waste Group Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "Select", "Select", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "ValList", "List of Values", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 2000);
                    oBase.doMDCreateField(sTableName, "Sort", "Sort Order", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                }
            }

            //if (CurrentReleaseCount > 1030 && CurrentReleaseCount < 1032) {
            //oBase.doMDRemoveField(sTableName, "ValidCd");
            //oBase.doMDCreateField(sTableName, "ValidCd", "Validation Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
            //}
            if (CurrentReleaseCount < 1054 || FORCECREATE) {
                oBase.doMDUpdateField(sTableName, "ValidCd", "Validation Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
            }
            sTableName = "@IDH_WITMVLD";
            if (CurrentReleaseCount < 1054 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Waste Item Validations")) {
                    oBase.doAddNumberSeq("WITMVLD", "Waste Item Validations", 0);
                    oBase.doMDCreateField(sTableName, "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "ItemFName", "Item F Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                    oBase.doMDCreateField(sTableName, "WstGpCd", "Waste Group Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "EnqId", "Enquiry ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "EnqLID", "Enquiry Line ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "WOQId", "WOQ ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "WOQLId", "WOQ Line ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);

                    oBase.doMDCreateField(sTableName, "ValidCd", "Validation Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "DBField", "OITM Field Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "Value1", "Value1", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254);
                    oBase.doMDCreateField(sTableName, "Value2", "Value2", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254);
                }
            }
            //if (CurrentReleaseCount > 1030 && CurrentReleaseCount < 1032) {
            //    //oBase.doMDRemoveField(sTableName, "ValidCd");
            //    //oBase.doMDCreateField(sTableName, "ValidCd", "Validation Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
            //}
            if (CurrentReleaseCount < 1054 || FORCECREATE) {
                oBase.doMDUpdateField(sTableName, "ValidCd", "Validation Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
            }
        }

        private void doCreateAdditionalItemWasteGroupValidationTables(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_ADITVLD";

            if (CurrentReleaseCount < 1054 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Waste Group Additional Items")) {
                    oBase.doAddNumberSeq("WGADITVL", "Waste Group Additional Items", 0);
                    oBase.doMDCreateField(sTableName, "WstGpCd", "Waste Group Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

                    oBase.doMDCreateField(sTableName, "ItemCd", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 40);
                    oBase.doMDCreateField(sTableName, "Sort", "Sort Order", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                }
            }

        }

        private void doCreateWOQTables(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_WOQHD";
            doCreateWOQHTable(CurrentReleaseCount, oBase, sTableName, "WOQUOTEH", "WOQ Header", "WOQ Header Series");
            sTableName = "@IDH_WOQHVR";
            doCreateWOQHTable(CurrentReleaseCount, oBase, sTableName, "IDHWOQHV", "WOQ Header Version", "WOQ Header Version Series");
            if (CurrentReleaseCount < 1038 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "BasWOQID", "Base WOQ ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "VerCrBy", "Version Created By", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "VerDate", "Version Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                oBase.doMDCreateField(sTableName, "VerDateTm", "Version Date Time", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 25);
            }

            sTableName = "@IDH_WOQITEM";
            doCreateWOQRTable(CurrentReleaseCount, oBase, sTableName, "WOQUOTED", "WOQ Items", "WOQ Items Series");

            sTableName = "@IDH_WOQITVR";
            doCreateWOQRTable(CurrentReleaseCount, oBase, sTableName, "IDHWOQIV", "WOQ Items Versions", "WOQ Items Version Series");
            if (CurrentReleaseCount < 1038 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "BasWOHID", "Base WOQ ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "BasWQRID", "Base WOQ Row ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
            }
            //  oBase.
        }

        private void doCreateLabTables(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_LABITM";
            if (CurrentReleaseCount < 1034 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Lab Item")) {
                    oBase.doAddNumberSeq("SEQLBITM", "Lab Item Series", 0);

                    oBase.doMDCreateField(sTableName, "LICode", "Lab Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                    oBase.doMDCreateField(sTableName, "LIName", "Lab Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "ObjectCd", "Object Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "ObjectType", "Object Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "UOM", "UOM", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "Quantity", "Quantity", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 10);
                    oBase.doMDCreateField(sTableName, "NewWhse", "New Warehouse", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 18);
                    oBase.doMDCreateField(sTableName, "NewBinLoc", "New BinLocation", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 228);
                    oBase.doMDCreateField(sTableName, "Comments", "Comments", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 5000);
                    oBase.doMDCreateField(sTableName, "CreatedBy", "Created By", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8);
                    oBase.doMDCreateField(sTableName, "CreatedOn", "Created On", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "LastUpdateBy", "Last Update By", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8);
                    oBase.doMDCreateField(sTableName, "LastUpdateOn", "Last Update On", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                }
            }
            if (CurrentReleaseCount < 1053 || FORCECREATE) {

                oBase.doMDUpdateField(sTableName, "LICode", "Lab Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
            }
            sTableName = "@IDH_LITMDTL";
            if (CurrentReleaseCount < 1034 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Lab Item Detail")) {
                    oBase.doAddNumberSeq("SEQLIDTL", "Lab Item Detail Series", 0);

                    oBase.doMDCreateField(sTableName, "LabCd", "Lab Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                    oBase.doMDCreateField(sTableName, "PItemCd", "Parent Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "PItemNm", "Parent Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "UOM", "UOM", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                    oBase.doMDCreateField(sTableName, "Quantity", "Quantity", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                    oBase.doMDCreateField(sTableName, "Whse", "Warehouse", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                    oBase.doMDCreateField(sTableName, "BinLoc", "BinLocation", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                }
            }

            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "Batch", "Batch Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "AbsEntry", "Batch AbsEntry", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                oBase.doMDCreateField(sTableName, "PrvBtHCd", "Provisional Batch Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDCreateField(sTableName, "PrvBtDCd", "Provisional Batch Detail Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
            }

            sTableName = "@IDH_LABANLYS";
            if (CurrentReleaseCount < 1034 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Lab Analysis")) {
                    oBase.doMDCreateField(sTableName, "AnalysisCd", "Analysis Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                    oBase.doMDCreateField(sTableName, "AnalysisDesc", "Analysis Description", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                }
            }

            sTableName = "@IDH_LABTASK";
            if (CurrentReleaseCount < 1034 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Lab Task")) {
                    oBase.doAddNumberSeq("SEQLBT", "Lab Task Series", 0);

                    oBase.doMDCreateField(sTableName, "LabItemRCd", "Lab Item RowCd", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                    oBase.doMDCreateField(sTableName, "LabICd", "Lab Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                    oBase.doMDCreateField(sTableName, "LabINm", "Lab Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "BPCode", "BP Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                    oBase.doMDCreateField(sTableName, "BPName", "BP Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "RowStatus", "Row Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "DtReceived", "Date Received", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "DtAnalysed", "Date Analysed", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "Department", "Department", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "AssignedTo", "Assigned To", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "AnalysisReq", "Analysis Requested", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "ExtStatus", "External Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "ExtComments", "External Comments", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 5000);
                    oBase.doMDCreateField(sTableName, "AtcAbsEntry", "Attachments", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "ImpResults", "Import Results", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "Decision", "Decision", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);

                }
            }

            sTableName = "@IDH_LABTSKANL";
            if (CurrentReleaseCount < 1034 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Lab Task Analysis")) {
                    oBase.doAddNumberSeq("SEQLBTA", "Lab Task Analysis Series", 0);

                    oBase.doMDCreateField(sTableName, "LabTskCd", "LabTask Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                    oBase.doMDCreateField(sTableName, "AnalysisCd", "Analysis Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                }
            }

            sTableName = "@IDH_LABTSKIMP";
            if (CurrentReleaseCount < 1034 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Lab Task Import")) {
                    oBase.doAddNumberSeq("SEQLTI", "Lab Task Import Series", 0);

                    oBase.doMDCreateField(sTableName, "LabTskCd", "LabTask Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                    oBase.doMDCreateField(sTableName, "ImportedOn", "Imported On", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "ImportedBy", "Imported By", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8);
                    oBase.doMDCreateField(sTableName, "Description", "Description", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                }
            }

            sTableName = "@IDH_LTIDETAIL";
            if (CurrentReleaseCount < 1034 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "LTImport Detail")) {
                    oBase.doAddNumberSeq("SEQLTID", "LabTaskImport Detail Series", 0);

                    oBase.doMDCreateField(sTableName, "LTICd", "LabTaskImport Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                    oBase.doMDCreateField(sTableName, "RepCol1", "Rep Col 1", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol2", "Rep Col 2", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol3", "Rep Col 3", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol4", "Rep Col 4", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol5", "Rep Col 5", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol6", "Rep Col 6", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol7", "Rep Col 7", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol8", "Rep Col 8", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol9", "Rep Col 9", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol10", "Rep Col 10", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol11", "Rep Col 11", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol12", "Rep Col 12", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol13", "Rep Col 13", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol14", "Rep Col 14", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol15", "Rep Col 15", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol16", "Rep Col 16", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol17", "Rep Col 17", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol18", "Rep Col 18", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol19", "Rep Col 19", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "RepCol20", "Rep Col 20", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                }
            }

            sTableName = "@IDH_LABSTATS";
            if (CurrentReleaseCount < 1037 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Lab Task Statuses")) {
                    oBase.doMDCreateField(sTableName, "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                }
            }
            //Add default values into LAB Task Status table
            if (CurrentReleaseCount < 1051 || FORCECREATE) {
                oBase.doUpdateQuerySkipPatchQueue("ENQ DATA STUR", "Delete from [@IDH_LABSTATS]");
                object[,] oData = { { "Code","Name","U_Status","U_Type"},
                                    { "1001","1001","Requested","RowStatus"},
                                    { "1002","1002","Dec Form Not Complete","RowStatus"},
                                    { "1003","1003","Received Analysis TBC","RowStatus"},
                                    { "1004","1004","Under Test","RowStatus"},
                                    { "1005","1005","Tested","RowStatus"},
                                    { "1006","1006","NONE","ExtStatus"},
                                    { "1007","1007","Despatched","ExtStatus"},
                                    { "1008","1008","Tested","ExtStatus"},
                                    { "1009","1009","Pending","Decision"},
                                    { "1010","1010","Accepted","Decision"},
                                    { "1011","1011","Declined","Decision"},
                                    { "1012","1012","Available","ItemStatus"},
                                    { "1013","1013","In Lab","ItemStatus"},
                                    { "1014","1014","Selected","ItemStatus"}
                                  };
                oBase.doUTAddValues("@IDH_LABSTATS", oData, true);

            }
            //Add default values into LAB Task Status table
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                object[,] oData = { { "Code","Name","U_Status","U_Type"},
                                    { "1015","1015","Cancelled","Decision"}
                                  };
                oBase.doUTAddValues("@IDH_LABSTATS", oData, true);

            }
            //This one is added as sample and they can add as many as they want
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                object[,] oData = { { "Code","Name","U_Status","U_Type"},
                                    { "1015","1015","DFT","BatchType"}
                                  };
                oBase.doUTAddValues("@IDH_LABSTATS", oData, true);

            }
            if (CurrentReleaseCount < 1037 || FORCECREATE) {
                sTableName = "@IDH_LABITM";
                oBase.doMDCreateField(sTableName, "LabTask", "Lab Task", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "GIssue", "Goods Issue", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "GReceipt", "Goods Receipt", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "ItemCode", "Item Master", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
            }

            if (CurrentReleaseCount < 1038 || FORCECREATE) {
                sTableName = "@IDH_LITMDTL";
                oBase.doMDUpdateField(sTableName, "PItemNm", "Parent Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
            }

            if (CurrentReleaseCount < 1040 || FORCECREATE) {
                sTableName = "@IDH_LABTASK";
                oBase.doMDCreateField(sTableName, "ObjectCd", "Object Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "ObjectType", "Object Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDCreateField(sTableName, "DtRequested", "Date Requested", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                oBase.doMDUpdateField(sTableName, "LabICd", "Lab Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);

                sTableName = "@IDH_LITMDTL";
                oBase.doMDUpdateField(sTableName, "PItemNm", "Parent Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
            }
            if (CurrentReleaseCount < 1052 || FORCECREATE) {
                sTableName = "@IDH_LABTASK";
                oBase.doMDCreateField(sTableName, "Saved", "Saved", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 9);
                oBase.doMDCreateField(sTableName, "DocNum", "Header DocNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDUpdateField(sTableName, "ObjectCd", "Object Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);

                sTableName = "@IDH_LABITM";
                oBase.doMDCreateField(sTableName, "Saved", "Saved", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 9);
            }
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                sTableName = "@IDH_LABTASK";
                oBase.doMDCreateField(sTableName, "ExtTsDt", "External Test Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);

                sTableName = "@IDH_LABITM";
                oBase.doMDCreateField(sTableName, "CntnrQty", "Container Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 11);

            }

            if (CurrentReleaseCount < 1063 || FORCECREATE) {
                sTableName = "@IDH_LABTASK";
                oBase.doMDCreateField(sTableName, "DipRtCd", "Disposal Route Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);

            }
            sTableName = "@IDH_ITMLANL";
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Lab Analysis Template")) {
                    oBase.doAddNumberSeq("SEQITLBTM", "Item Lab Task Template Series");
                    oBase.doMDCreateField(sTableName, "AnalysisCd", "Analysis Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                    oBase.doMDCreateField(sTableName, "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                }
            }
            sTableName = "@IDH_LBTKPRG";
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Lab Analysis Progress")) {
                    oBase.doAddNumberSeq("SEQLBTPRG", "Lab Task Progress Series");
                    oBase.doMDCreateField(sTableName, "TaskCd", "Lab Task Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "AnalysisCd", "Analysis Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Select", "Select", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Comments", "Comments", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                }
            }

            sTableName = "@IDH_LBTDPLG";
            if (CurrentReleaseCount < 1063 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Lab Task Duplicate Log")) {
                    oBase.doAddNumberSeq("SEQLTDLG", "Lab Task Duplicate Srs");
                    //oBase.doMDCreateField(sTableName, "OrgTaskCd", "Org Lab Task", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    //oBase.doMDCreateField(sTableName, "TaskCd2", "Lab Task Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    //oBase.doMDRemoveField(sTableName, "OrgTaskCd");//, "Org Lab Task", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    //oBase.doMDRemoveField(sTableName, "TaskCd2");//, "Lab Task Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                }
            }
        }

        private void doCreateQuestionnairesTables(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_ENQSMS";
            if (CurrentReleaseCount < 1036 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Enquiry Questionnaires List")) {
                    oBase.doAddNumberSeq("IDHENQSM", "Enquiry Questionnaires List", 0);
                    oBase.doMDCreateField(sTableName, "Question", "Question", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 4000);
                    oBase.doMDCreateField(sTableName, "Select", "Select", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Sort", "Sort", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                }
            }


            sTableName = "@IDH_ENQUS";
            if (CurrentReleaseCount < 1036 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Enquiry Questions")) {
                    oBase.doAddNumberSeq("IDHENQUS", "Enquiry Questions", 0);
                    oBase.doMDCreateField(sTableName, "EnqID", "Enquiry ID", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "WOQID", "WOQ ID", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "QsID", "Question ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "Question", "Question", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 4000);
                    oBase.doMDCreateField(sTableName, "Answer", "Answer", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 4000);
                }
            }
        }

        private void doCreateApprovalTables(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_APPRSTUP";
            if (CurrentReleaseCount < 1036 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Approval Setup")) {
                    oBase.doAddNumberSeq("IDHAPSTP", "Approval Setup", 0);
                    oBase.doMDCreateField(sTableName, "Origintr", "Originator", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                    oBase.doMDCreateField(sTableName, "Stage", "Approval Stage", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                    oBase.doMDCreateField(sTableName, "FormTyp", "Form Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "ApStagCd", "Approval Stage Code(OWST)", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                }
            }

            sTableName = "@IDH_APPROVLH";
            if (CurrentReleaseCount < 1036 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Doc Approvals Header")) {
                    oBase.doAddNumberSeq("IDHAPDCH", "Approval List", 0);
                    oBase.doMDCreateField(sTableName, "FormTyp", "Form Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "DocNum", "Document number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "DocVer", "Document Version", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "DocDt", "Document Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "Origintr", "Originator", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                    //oBase.doMDRemoveField(sTableName, "Stage");
                    oBase.doMDCreateField(sTableName, "Stage", "Stage", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    //oBase.doMDCreateField(sTableName, "ApprvNm", "Approver Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                    //oBase.doMDCreateField(sTableName, "ApprvUId", "Approver User ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                    oBase.doMDCreateField(sTableName, "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    //oBase.doMDCreateField(sTableName, "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "DocAddDt", "Doc Add Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "NmAprReq", "Number of Approvals Required", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "NmRejReq", "Number of Reject Required", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);

                    oBase.doMDCreateField(sTableName, "NmAprovd", "Total Approvals", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "NmRejted", "Total Rejected", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "ApprDt", "Approve Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "RejDt", "Reject Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "LastUpBy", "Last Updated By", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);

                }
            }

            sTableName = "@IDH_APPROVLD";
            if (CurrentReleaseCount < 1036 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Doc Approval Detail")) {
                    oBase.doAddNumberSeq("IDHAPDCD", "Approval Detail List", 0);
                    oBase.doMDCreateField(sTableName, "APPHID", "Approval Header Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "FormTyp", "Form Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "DocNum", "Document number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    //oBase.doMDCreateField(sTableName, "DocVer", "Document Version", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    //oBase.doMDCreateField(sTableName, "DocDt", "Document Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    //oBase.doMDCreateField(sTableName, "Origintr", "Originator", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                    //oBase.doMDCreateField(sTableName, "Stage", "Stage", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                    oBase.doMDCreateField(sTableName, "ApprvNm", "Approver Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                    oBase.doMDCreateField(sTableName, "ApprvUId", "Approver User ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                    oBase.doMDCreateField(sTableName, "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                    oBase.doMDCreateField(sTableName, "LstUpdtOn", "Last Update Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);

                    //oBase.doMDCreateField(sTableName, "DocAddDt", "Doc Add Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    //oBase.doMDCreateField(sTableName, "NmAprReq", "Number of Approvals Required", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    //oBase.doMDCreateField(sTableName, "NmRejReq", "Number of Reject Required", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                }
            }

        }
        #region "PBI Mass Updater All Live Services"
        private void doCreatePBIMassUpdaterTables(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_LVSCNG";
            if (CurrentReleaseCount < 1055 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "All Live Service Export Config")) {
                    oBase.doAddNumberSeq("SEQLVSCN", "Live Service Config", 0);
                    oBase.doMDCreateField(sTableName, "FieldName", "Field Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "Select", "Select", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                }
            }


            sTableName = "@IDH_LVSIMLT";
            if (CurrentReleaseCount < 1055 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Live Service Import Template")) {
                    //oBase.doAddNumberSeq("SEQLVSITP", "Live Service Import Template", 0);
                    oBase.doMDCreateField(sTableName, "ExFName", "Excel Field Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "PBIFName", "PBI Field Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "Index", "Excel Index", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    AddTemplateFields(CurrentReleaseCount, oBase);
                }
            }
            sTableName = "@IDH_LVSBHH";
            if (CurrentReleaseCount < 1055 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Live Service Batch Header")) {
                    oBase.doAddNumberSeq("SEQLVSBH", "Live Service Batch", 0);
                    oBase.doMDCreateField(sTableName, "AlterSend", "Alert Send", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "AlterSdDt", "Alert Send On", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "AlterSdTm", "Alert Send On", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 10);
                    oBase.doMDCreateField(sTableName, "AlterSdTo", "Alert Send to", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "AlterSdBy", "Alert Send By", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "BtchClose", "Batch Processed", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "BthStatus", "Batch Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Comment", "Batch Comment", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);

                }
            }
            sTableName = "@IDH_LVSBTH";
            if (CurrentReleaseCount < 1055 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Live Service Import Batch")) {
                    oBase.doAddNumberSeq("SEQLVSBD", "Live Service Batch Detail", 0);
                    //oBase.doMDRemoveField(sTableName, "VldStatus");
                    //oBase.doMDRemoveField(sTableName, "ProcSts");

                    oBase.doMDCreateField(sTableName, "WORow", "WORow", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "PBI", "PBI", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "AmndTp", "Amendment Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "CustCd", "Customer Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "CustNm", "Customer-Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "CustAdr", "Customer Site Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "CusPCode", "Post Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "JbTypeDs", "Order Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                    oBase.doMDCreateField(sTableName, "JbTypeCd", "Order Type Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "SuppCd", "Supplier Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "SuppNm", "Supplier Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "SuppRef", "Supplier Reference", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "ItemCd", "Container Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "ItemNm", "Container", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "WastCd", "Waste Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "WasteNm", "Waste Material", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "UOM", "Unit of Measure (UOM)", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "Qty", "Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 11);
                    oBase.doMDCreateField(sTableName, "LPW", "LPW", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 11);
                    oBase.doMDCreateField(sTableName, "Mon", "Mon", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Tue", "Tue", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Wed", "Wed", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Thu", "Thu", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Fri", "Fri", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Sat", "Sat", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Sun", "Sun", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "CMinChg", "CIP.Minimum Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    oBase.doMDCreateField(sTableName, "CMCgVol", "CIP.Minimum Charge Upto Volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    oBase.doMDCreateField(sTableName, "CRminVol", "CIP.Rate above min volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    oBase.doMDCreateField(sTableName, "CHaulage", "CIP.Haulage / Service / Rental", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    oBase.doMDCreateField(sTableName, "SMinChg", "SIP.Minimum Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    oBase.doMDCreateField(sTableName, "SMCgVol", "SIP.Minimum Charge Upto Volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    oBase.doMDCreateField(sTableName, "SRminVol", "SIP.Rate above min Volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    oBase.doMDCreateField(sTableName, "SHaulage", "SIP.Haulage / Service / Rental", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    oBase.doMDCreateField(sTableName, "EffDate", "Effective Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "WTNFrom", "WTN From Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "WTNTo", "WTN From Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "Comments", "Comments", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 2000);
                    oBase.doMDCreateField(sTableName, "BatchNum", "BatchNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "ProcSts", "Process Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2);
                    oBase.doMDCreateField(sTableName, "FrqType", "Freqency Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "FrqCount", "Freqency Type", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "AutoComp", "Auto Complete", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Rebate", "Rebate", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "VldStatus", "Validation Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2);
                    oBase.doMDCreateField(sTableName, "BthStatus", "Batch Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    //
                    //oBase.doMDCreateField(sTableName, "BatchNum", "BatchNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);

                }
            }
            if (CurrentReleaseCount < 1066 || FORCECREATE) {
                oBase.doMDCreateField(sTableName,  "IDHRT", "Routable Checkbox", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "IDHRCDM", "Route Code Monday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8);
                oBase.doMDCreateField(sTableName, "IDHRCDTU", "Route Code Tuesday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8);
                oBase.doMDCreateField(sTableName, "IDHRCDW", "Route Code Wednesday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8);
                oBase.doMDCreateField(sTableName, "IDHRCDTH", "Route Code Thursday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8);
                oBase.doMDCreateField(sTableName, "IDHRCDF", "Route Code Friday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8);
                oBase.doMDCreateField(sTableName, "IDHRCDSA", "Route Code Saturday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8);
                oBase.doMDCreateField(sTableName, "IDHRCDSU", "Route Code Sunday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8);
                oBase.doMDCreateField(sTableName, "IDHRSQM", "Route Sequence Monday", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "IDHRSQTU", "Route Sequence Tuesday", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "IDHRSQW", "Route Sequence Wednesday", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "IDHRSQTH", "Route Sequence Thursday", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "IDHRSQF", "Route Sequence Friday", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "IDHRSQSA", "Route Sequence Saturday", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "IDHRSQSU", "Route Sequence Sunday", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10);

                oBase.doMDCreateField(sTableName, "IDHCMT", "Common Comments", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                oBase.doMDCreateField(sTableName, "IDHDRVI", "Common Driver Instructions", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                oBase.doMDCreateField(sTableName, "AUTSTRT", "Auto Complete StartDate", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
               
            }
            if (CurrentReleaseCount < 1062 || FORCECREATE) {

                oBase.doMDCreateField(sTableName, "CMnSVst", "CIP-Min Site Visit", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "CMnChg", "CIP-Min Charge", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "CVarRFix", "CIP-Variable Or Fixed QTY", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);

                oBase.doMDCreateField(sTableName, "SMnSVst", "SIP-Min Site Visit", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "SMnChg", "SIP-Min Charge", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "SVarRFix", "SIP-Variable Or Fixed QTY", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);

                oBase.doMDCreateField(sTableName, "SupAddr", "Supplier Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDCreateField(sTableName, "SupPCode", "Supplier Postcode", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "SupAdrCd", "Supplier Address Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

            }
            if (CurrentReleaseCount < 1071 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "AddedBy", "Record Added By", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName,"PERTYP", "Period type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName,"IDHRECFQ", "Recurrence Frequency", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
            }
            doAddAllLiveServiceAmendmentTypes(CurrentReleaseCount, oBase);
        }

        private void AddTemplateFields(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_LVSIMLT";
            if (CurrentReleaseCount < 1055 || FORCECREATE) {
                object[,] oData1 = {
                    {"Code","Name","ExFName","PBIFName","Index"},
                    {"1","1","[WORow / PO Number]","U_IDHWOR","1"},
                    {"2","2","[PBI]","Code","2"},
                    {"3","3","[Amendment Type]","","3"},
                    {"4","4","[Customer-Name]","U_IDHCCODE","4"},
                    {"5","5","[Customer Site Address]","U_IDHSADDR","5"},
                    {"6","6","[Post Code]","","6"},
                    {"7","7","[Order Type]","U_IDHJTYPE","7"},
                    {"8","8","[Supplier Name]","U_IDHCARDC","8"},
                    {"9","9","[Supplier Name]","U_IDHDISDC","8"},
                    {"10","10","[Supplier Reference]","","9"},
                    {"11","11","[Container]","U_IDHCOIDC","10"},
                    {"12","12","[Waste Material]","U_IDHWCIDC","11"},
                    {"13","13","[Unit of Measure (UOM)]","U_UOM","12"},
                    {"14","14","[Qty]","U_ExpLdWgt","13"},
                    {"15","15","[LPW]","","14"},
                    {"16","16","[Mon]","U_IDHRDMON2","15"},
                    {"17","17","[Tue]","U_IDHRDTUE2","16"},
                    {"18","18","[Wed]","U_IDHRDWED2","17"},
                    {"19","19","[Thu]","U_IDHRDTHU2","18"},
                    {"20","20","[Fri]","U_IDHRDFRI2","19"},
                    {"21","21","[Sat]","U_IDHRDSAT2","20"},
                    {"22","22","[Sun]","U_IDHRDSUN2","21"},
                    {"23","23","[Minimum Charge]","","22"},
                    {"24","24","[Minimum Charge Upto Volume]","","23"},//Minimum Charge Upto Volume
                    {"25","25","[Rate above min volume]","","24"},//Rate above min volume
                    {"26","26","[Haulage / Service / Rental]","","25"},//Haulage / Service / Rental
                    {"27","27","[Minimum Charge]","","26"},//Minimum Charge
                    {"28","28","[Minimum Charge Upto Volume]","","27"},//Minimum Charge Upto Volume
                    {"29","29","[Rate above min Volume]","","28"},//Rate above min Volume
                    {"30","30","[Haulage / Service / Rental]","","29"},//Haulage / Service / Rental
                    {"31","31","[Effective Date]","U_ACDATE","30"},
                    {"32","32","[Comments]","U_IDHRECOM","31"},
                    {"33","33","[WTN From]","","32"},
                    {"34","34","[WTN To]","","33"},
                    };
                oBase.doUTAddValues(sTableName, oData1, true);
            }
        }

        private void doAddAllLiveServiceAmendmentTypes(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_LVSAMD";
            if (CurrentReleaseCount < 1055 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Live Service Amendments")) {
                    oBase.doAddNumberSeq("SEQLSAMD", "Live Service Amendments", 0);
                    oBase.doMDCreateField(sTableName, "AmdDesc", "Amendment Group", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "FrqTyp", "Frequency Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "FrqCount", "Frequency Count", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Days", "Days", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Qty", "Qty", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Removal", "Stop PBI", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "SDate", "Start Date", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    //oBase.doMDCreateField(sTableName, "EDate", "End Date", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "JbType", "Job Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "AutoCmp", "Auto Complete", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "WTNFDt", "WTN From Date", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "WTNTDt", "WTN To Date", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "AddPBI", "Add PBI", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Rebate", "Rebate", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "UOM", "UOM", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "SupRef", "SupRef", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);                    
                    oBase.doMDCreateField(sTableName, "Active", "Active", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "AmdBnVal", "Amendment Binary Value", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDRemoveField(sTableName, "EDate");
                    // oBase.doMDRemoveField(sTableName, "WTNDts");
                }
            }
            if (CurrentReleaseCount < 1066 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "DrvCmt", "Driver Comments", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "RoutCmt", "Route Comments", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);

            }
            if (CurrentReleaseCount < 1071|| FORCECREATE) {
                oBase.doMDCreateField(sTableName, "PERTYP", "Period type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
            }

        }
        #endregion
        #region "WO Adhoc Updater"
        private void doAddWOAdhocUpdater(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_WRADHH";
            if (CurrentReleaseCount < 1055 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Adhoc WO Mass Batch")) {
                    oBase.doAddNumberSeq("SEQWRDBH", "Adhoc WO Mass Batch", 0);
                    oBase.doMDCreateField(sTableName, "AlterSend", "Alert Send", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "AlterSdDt", "Alert Send On", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "AlterSdTm", "Alert Send On", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 10);
                    oBase.doMDCreateField(sTableName, "AlterSdTo", "Alert Send to", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "AlterSdBy", "Alert Send By", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "BtchClose", "Batch Processed", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "BthStatus", "Batch Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Comment", "Batch Comment", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);

                }
            }
            sTableName = "@IDH_WRADHD";
            if (CurrentReleaseCount < 1055 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Adhoc WO Mass Batch Detail")) {
                    oBase.doAddNumberSeq("SEQWRDBD", "Adhoc WO Mass Batch Detail", 0);

                    oBase.doMDCreateField(sTableName, "SrNo", "Batch Sr No", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "CustCd", "Customer-Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "CustNm", "Customer-Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "CustAdr", "Customer Site Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "CusPCode", "Post Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "JbTypeDs", "Order Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                    oBase.doMDCreateField(sTableName, "SuppCd", "Supplier Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "SuppNm", "Supplier Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "SuppRef", "Supplier Reference", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "MaximoNum", "Maximo Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "AddtItm", "Additional Item", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "ItemCd", "Container Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "ItemNm", "Container", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "WastCd", "Waste Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "WasteNm", "Waste Material", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "UOM", "Unit of Measure (UOM)", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "Qty", "Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 11);
                    oBase.doMDCreateField(sTableName, "WOH", "WOH", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "WOR", "WOR", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "RDate", "RDate", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "SDate", "SDate", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "EDate", "EDate", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    //oBase.doMDCreateField(sTableName, "CMinChg", "CIP.Minimum Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    //oBase.doMDCreateField(sTableName, "CMCgVol", "CIP.Minimum Charge Upto Volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    //oBase.doMDCreateField(sTableName, "CRminVol", "CIP.Rate above min volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    //oBase.doMDCreateField(sTableName, "CHaulage", "CIP.Haulage / Service / Rental", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);

                    //oBase.doMDCreateField(sTableName, "SMinChg", "SIP.Minimum Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    //oBase.doMDCreateField(sTableName, "SMCgVol", "SIP.Minimum Charge Upto Volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    //oBase.doMDCreateField(sTableName, "SRminVol", "SIP.Rate above min Volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    //oBase.doMDCreateField(sTableName, "SHaulage", "SIP.Haulage / Service / Rental", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);

                    oBase.doMDCreateField(sTableName, "Comments", "Comments", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 2000);
                    oBase.doMDCreateField(sTableName, "Rebate", "Rebate", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "BatchNum", "BatchNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "ProcSts", "Process Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2);
                    oBase.doMDCreateField(sTableName, "VldStatus", "Validation Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2);
                    oBase.doMDCreateField(sTableName, "BthStatus", "Batch Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "ParentID", "ParentID ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                }
            }
            if (CurrentReleaseCount < 1062 || FORCECREATE) {

                //oBase.doMDCreateField(sTableName, "CMnSVst", "CIP-Min Site Visit", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                //oBase.doMDCreateField(sTableName, "CMnChg", "CIP-Min Charge", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                //oBase.doMDCreateField(sTableName, "CVarRFix", "CIP-Variable Or Fixed QTY", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                
                //oBase.doMDCreateField(sTableName, "SMnSVst", "SIP-Min Site Visit", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                //oBase.doMDCreateField(sTableName, "SMnChg", "SIP-Min Charge", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                //oBase.doMDCreateField(sTableName, "SVarRFix", "SIP-Variable Or Fixed QTY", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);

                oBase.doMDCreateField(sTableName, "SupAddr", "Supplier Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDCreateField(sTableName, "SupPCode", "Supplier Postcode", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "SupAdrCd", "Supplier Address Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
            }
            if (CurrentReleaseCount < 1064 || FORCECREATE) {

                //oBase.doMDRemoveField(sTableName, "CMnSVst");//, "CIP-Min Site Visit", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                //oBase.doMDRemoveField(sTableName, "CMnChg");//, "CIP-Min Charge", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                //oBase.doMDRemoveField(sTableName, "CVarRFix");//, "CIP-Variable Or Fixed QTY", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                //oBase.doMDRemoveField(sTableName, "CMinChg");//, "CIP.Minimum Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                //oBase.doMDRemoveField(sTableName, "CMCgVol");//, "CIP.Minimum Charge Upto Volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                //oBase.doMDRemoveField(sTableName, "SMinChg");//, "SIP.Minimum Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                //oBase.doMDRemoveField(sTableName, "SMCgVol");//, "SIP.Minimum Charge Upto Volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);

                //oBase.doMDRemoveField(sTableName, "CRminVol");//, "CIP.Rate above min volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                //oBase.doMDRemoveField(sTableName, "CHaulage");//, "CIP.Haulage / Service / Rental", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);

                //oBase.doMDRemoveField(sTableName, "SMinChg");//, "SIP.Minimum Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                //oBase.doMDRemoveField(sTableName, "SMCgVol");//, "SIP.Minimum Charge Upto Volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                //oBase.doMDRemoveField(sTableName, "SRminVol");//, "SIP.Rate above min Volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                //oBase.doMDRemoveField(sTableName, "SHaulage");//, "SIP.Haulage / Service / Rental", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);


                //oBase.doMDRemoveField(sTableName, "SMnSVst");//, "SIP-Min Site Visit", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                //oBase.doMDRemoveField(sTableName, "SMnChg");//, "SIP-Min Charge", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                //oBase.doMDRemoveField(sTableName, "SVarRFix");//, "SIP-Variable Or Fixed QTY", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                
                oBase.doMDCreateField(sTableName, "TChgPCst", "Tip Chrg/Pur Cost", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "CusChr", "Haulage Charge", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "TipCost", "Disposal Cost", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDUpdateField(sTableName, "TipCost", "Disposal Cost", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "OrdCost", "Haulage Cost", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

                oBase.doMDUpdateField(sTableName, "Qty", "Actual Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 11);
                oBase.doMDCreateField(sTableName, "PUOM", "Purchase Unit of Measure (PUOM)", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "HlgQty", "Haulage Qty", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "ExpLdWgt", "Expected Qty", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

            }
            if (CurrentReleaseCount < 1071 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "AddedBy", "Record Added By", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "Tip", "Disposal Site", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "TipNm", "Dispoal Site Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "SAddress", "Dispoal Site Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
            }
            if (CurrentReleaseCount < 1071 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "AddedBy", "Record Added By", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "Tip", "Disposal Site", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "TipNm", "Dispoal Site Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "SAddress", "Dispoal Site Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
            }
             

        }

        private void doAddWORMassUpdater(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_WORUHH";
            if (CurrentReleaseCount < 1055 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "WOR Mass Batch")) {
                    oBase.doAddNumberSeq("SEQWORBH", "WOR Mass Batch", 0);
                    oBase.doMDCreateField(sTableName, "AlterSend", "Alert Send", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "AlterSdDt", "Alert Send On", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "AlterSdTm", "Alert Send On", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 10);
                    oBase.doMDCreateField(sTableName, "AlterSdTo", "Alert Send to", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "AlterSdBy", "Alert Send By", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "BtchClose", "Batch Processed", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "BthStatus", "Batch Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Comment", "Batch Comment", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);

                }
            }
            sTableName = "@IDH_WORUHD";
            if (CurrentReleaseCount < 1055 || FORCECREATE) {

                if (oBase.doMDCreateTable(sTableName, "WOR Mass Batch Detail")) {
                    oBase.doAddNumberSeq("SEQWORBD", "WOR Mass Batch Detail", 0);

                    //bool b=    oBase.doMDRemoveField(sTableName, "SrNo");
                    //oBase.doMDCreateField(sTableName, "SrNo", "Batch Sr No", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "WOR", "WOR", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "SDate", "SDate", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "EDate", "EDate", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);

                    oBase.doMDCreateField(sTableName, "Cancelled", "Cancelled", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    //oBase.doMDUpdateField(sTableName, "Cancelled", "Cancelled", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "CustRef", "Customer Reference", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "MaximoNum", "Maximo Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);

                    oBase.doMDCreateField(sTableName, "ExptQty", "Cust Reporting Weight", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 11);

                    oBase.doMDCreateField(sTableName, "UOM", "Unit of Measure (UOM)", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);


                    oBase.doMDCreateField(sTableName, "HulgQty", "Haulage Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 11);
                    oBase.doMDCreateField(sTableName, "DispQty", "Disposal Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 11);
                    oBase.doMDCreateField(sTableName, "RebWgt", "Rebate Weights", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 11);

                    oBase.doMDCreateField(sTableName, "CustCd", "Customer-Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "CustNm", "Customer-Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "CustAdr", "Customer Site Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    //  oBase.doMDCreateField(sTableName, "CusPCode", "Post Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    //  oBase.doMDCreateField(sTableName, "JbTypeDs", "Order Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                    oBase.doMDCreateField(sTableName, "CarrCd", "Carrier Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "CarrNm", "Carrier Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);

                    //   oBase.doMDCreateField(sTableName, "AddtItm", "Additional Item", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "ItemCd", "Container Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "ItemNm", "Container", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "WastCd", "Waste Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "WasteNm", "Waste Material", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);


                    //oBase.doMDCreateField(sTableName, "WOH", "WOH", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    //oBase.doMDCreateField(sTableName, "WOR", "WOR", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    //oBase.doMDCreateField(sTableName, "RDate", "RDate", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    //oBase.doMDCreateField(sTableName, "CMinChg", "CIP.Minimum Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    //oBase.doMDCreateField(sTableName, "CMCgVol", "CIP.Minimum Charge Upto Volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    //oBase.doMDCreateField(sTableName, "CRminVol", "CIP.Rate above min volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    //oBase.doMDCreateField(sTableName, "CHaulage", "CIP.Haulage / Service / Rental", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    //oBase.doMDCreateField(sTableName, "SMinChg", "SIP.Minimum Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    //oBase.doMDCreateField(sTableName, "SMCgVol", "SIP.Minimum Charge Upto Volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    //oBase.doMDCreateField(sTableName, "SRminVol", "SIP.Rate above min Volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    //oBase.doMDCreateField(sTableName, "SHaulage", "SIP.Haulage / Service / Rental", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    //oBase.doMDCreateField(sTableName, "Comments", "Comments", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 2000);
                    //oBase.doMDCreateField(sTableName, "Rebate", "Rebate", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "BatchNum", "BatchNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                    oBase.doMDCreateField(sTableName, "ProcSts", "Process Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2);
                    oBase.doMDCreateField(sTableName, "VldStatus", "Validation Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2);
                    oBase.doMDCreateField(sTableName, "BthStatus", "Batch Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    //oBase.doMDCreateField(sTableName, "ParentID", "ParentID ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 60);
                }
            }
            if (CurrentReleaseCount < 1064 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "VehTyp", "Vehicle Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
            }
            if (CurrentReleaseCount < 1069|| FORCECREATE) {
                oBase.doMDCreateField(sTableName, "RDate", "Requested Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
            }
            if (CurrentReleaseCount < 1071 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "AddedBy", "Record Added By", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
            }
            if (CurrentReleaseCount < 1079 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "JbTypeDs", "Order Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                oBase.doMDRemoveField(sTableName, "VdReason");
                oBase.doMDCreateField(sTableName, "VdReason", "Validation Reason", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254);
            }
        }

        private void doAddServiceAdditionalInfo(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_SRCALINF";
            if (CurrentReleaseCount < 1079 || FORCECREATE) {

                if (oBase.doMDCreateTable(sTableName, "Service Call Info")) {
                    oBase.doAddNumberSeq("SRCALINF", "Service call info", 0);

                    oBase.doMDCreateField(sTableName, "CallID", "Call ID", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "ServiceID", "Service Call ID", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "SMnSVst", "SIP-Min Site Visit", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                   // oBase.doMDCreateField(sTableName, "SMnSVst", "SIP-Min Site Visit", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "SMnChg", "SIP-Min Charge", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "SVarRFix", "SIP-Variable Or Fixed QTY", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "SMinChg", "SIP.Minimum Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    oBase.doMDCreateField(sTableName, "SMCgVol", "SIP.Minimum Charge Upto Volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    oBase.doMDCreateField(sTableName, "SRminVol", "SIP.Rate above min Volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    oBase.doMDCreateField(sTableName, "SHaulage", "SIP.Haulage / Service / Rental", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);

                    oBase.doMDCreateField(sTableName, "CMinChg", "CIP.Minimum Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    oBase.doMDCreateField(sTableName, "CMCgVol", "CIP.Minimum Charge Upto Volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    oBase.doMDCreateField(sTableName, "CRminVol", "CIP.Rate above min volume", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    oBase.doMDCreateField(sTableName, "CHaulage", "CIP.Haulage / Service / Rental", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                    oBase.doMDCreateField(sTableName, "CMnSVst", "CIP-Min Site Visit", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "CMnChg", "CIP-Min Charge", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "CVarRFix", "CIP-Variable Or Fixed QTY", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);

                    oBase.doMDCreateField(sTableName, "UOM", "Unit of Measure (UOM)", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "WTNFrom", "WTN From Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "WTNTo", "WTN From Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "AutoComp", "Auto Complete", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "Rebate", "Rebate", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);

                    oBase.doMDCreateField(sTableName, "IDHRT", "Routable Checkbox", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "IDHRCDM", "Route Code Monday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "IDHRCDTU", "Route Code Tuesday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "IDHRCDW", "Route Code Wednesday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "IDHRCDTH", "Route Code Thursday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "IDHRCDF", "Route Code Friday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "IDHRCDSA", "Route Code Saturday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "IDHRCDSU", "Route Code Sunday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "IDHRSQM", "Route Sequence Monday", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "IDHRSQTU", "Route Sequence Tuesday", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "IDHRSQW", "Route Sequence Wednesday", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "IDHRSQTH", "Route Sequence Thursday", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "IDHRSQF", "Route Sequence Friday", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "IDHRSQSA", "Route Sequence Saturday", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "IDHRSQSU", "Route Sequence Sunday", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10);

                    oBase.doMDCreateField(sTableName, "IDHCMT", "Common Comments", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                    oBase.doMDCreateField(sTableName, "IDHDRVI", "Common Driver Instructions", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                    oBase.doMDCreateField(sTableName, "AUTSTRT", "Auto Complete StartDate", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);

                    oBase.doMDCreateField(sTableName, "PERTYP", "Period type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "IDHRECFQ", "Recurrence Frequency", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);

                    oBase.doMDCreateField(sTableName, "TipCost", "Disposal Cost", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "OrdCost", "Haulage Cost", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "TChgPCst", "Tip Chrg/Pur Cost", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "CusChr", "Haulage Charge", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

                    oBase.doMDCreateField(sTableName, "MaximoNum", "Maximo Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);

                    oBase.doMDCreateField(sTableName, "PUOM", "Purchase Unit of Measure (PUOM)", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                 //   oBase.doMDCreateField(sTableName, "HlgQty", "Haulage Qty", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "ExpLdWgt", "Expected Qty", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

                    oBase.doMDCreateField(sTableName, "SDate", "SDate", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "EDate", "EDate", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);

                    oBase.doMDCreateField(sTableName, "Tip", "Disposal Site", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "TipNm", "Dispoal Site Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "SAddress", "Dispoal Site Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);


                }
            }
            if (CurrentReleaseCount < 1079 || FORCECREATE) {

                oBase.doMDUpdateField(sTableName, "IDHRT", "Routable Checkbox", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDUpdateField(sTableName, "IDHRCDM", "Route Code Monday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDUpdateField(sTableName, "IDHRCDTU", "Route Code Tuesday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDUpdateField(sTableName, "IDHRCDW", "Route Code Wednesday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDUpdateField(sTableName, "IDHRCDTH", "Route Code Thursday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDUpdateField(sTableName, "IDHRCDF", "Route Code Friday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDUpdateField(sTableName, "IDHRCDSA", "Route Code Saturday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDUpdateField(sTableName, "IDHRCDSU", "Route Code Sunday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);

                oBase.doMDCreateField(sTableName, "SuppRef", "Supplier Referenc", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "SupAddr", "Supplier Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDCreateField(sTableName, "SupAdrCd", "Supplier Address Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "SupPCode", "Supplier Postcode", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

                oBase.doMDUpdateField(sTableName, "Qty", "Actual Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 11);
                oBase.doMDRemoveField(sTableName, "HlgQty");
            }
        }

        #endregion
        #region Disposal Process Tables
        private void doCreateDisposalProcessTable(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_DISPPRC";
            if (CurrentReleaseCount < 1036 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Disposal Process")) {
                    oBase.doAddNumberSeq("IDHDISPR", "Disposal Process Series", 0);
                    oBase.doMDCreateField(sTableName, "ProcessNm", "Process Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "ProcessCd", "Process Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                }
            }
        }
        private void doCreateDisposalTreatmentTable(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_DISPTRT";
            if (CurrentReleaseCount < 1036 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Disposal Treatment")) {
                    oBase.doAddNumberSeq("IDHDISTR", "Disposal Treatment Series", 0);
                    oBase.doMDCreateField(sTableName, "TreatmntNm", "Treatment Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    oBase.doMDCreateField(sTableName, "TreatmntCd", "Treatment Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                }
            }
        }
        private void doCreateDisposalRouteCodeTable(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_DISPRTCD";
            if (CurrentReleaseCount < 1038 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Disposal Route Codes")) {
                    oBase.doAddNumberSeq("IDHDSPRT", "Disposal Route Code Series", 0);

                    oBase.doMDCreateField(sTableName, "DisRCode", "Disposal Route Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);

                    oBase.doMDCreateField(sTableName, "TipCd", "Disposal Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "TipNm", "Disposal Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);

                    oBase.doMDCreateField(sTableName, "DisProcCd", "Process Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    //oBase.doMDCreateField(sTableName, "DisProcNm", "Process Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    //oBase.doMDRemoveField(sTableName, "DisProcNm");

                    oBase.doMDCreateField(sTableName, "DisTrtCd", "Treatment Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    //oBase.doMDCreateField(sTableName, "DisTrtNm", "Treatment Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);
                    //oBase.doMDRemoveField(sTableName, "DisTrtNm");

                    oBase.doMDCreateField(sTableName, "State", "State", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);

                    oBase.doMDCreateField(sTableName, "ItemCd", "Container Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "ItemDsc", "Container Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);

                    oBase.doMDCreateField(sTableName, "RDCode", "R/D Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);

                    oBase.doMDCreateField(sTableName, "RDFrgnCd", "R/D Foreign Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);

                    oBase.doMDCreateField(sTableName, "TAddress", "Tip Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    //oBase.doMDCreateField(sTableName, "TStreet", "Tip Street", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    //oBase.doMDCreateField(sTableName, "TBlock", "Tip Block", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    //oBase.doMDCreateField(sTableName, "TCity", "Tip City", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "TZpCd", "Tip Postal Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    oBase.doMDCreateField(sTableName, "TAddrssLN", "Tip Address LineNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                    //oBase.doMDRemoveField(sTableName, "TStreet");
                    //oBase.doMDRemoveField(sTableName, "TBlock");
                    //oBase.doMDRemoveField(sTableName, "TCity");

                    oBase.doMDCreateField(sTableName, "WasCd", "Waste Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                    oBase.doMDCreateField(sTableName, "WasDsc", "Waste Description", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);

                    oBase.doMDCreateField(sTableName, "TipCost", "Disposal Cost", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);

                    oBase.doMDCreateField(sTableName, "UOM", "Unit of Measurment", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);


                    oBase.doMDCreateField(sTableName, "Qty", "Qunatity", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_Quantity, 11);

                    oBase.doMDCreateField(sTableName, "HaulgCost", "Haulage Cost", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);

                    oBase.doMDCreateField(sTableName, "AvgTngCmt", "Average Tonnage per load and comments", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);

                    oBase.doMDCreateField(sTableName, "HaulgPCnt", "Transport/ container", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);

                    oBase.doMDCreateField(sTableName, "CmpBioTrt", "Composting/Biological Treatment Y/N", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);

                    oBase.doMDCreateField(sTableName, "TFSyn", "TFS Y/N", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);

                    oBase.doMDCreateField(sTableName, "WhsCode", "Bay", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    //oBase.doMDCreateField(sTableName, "WhsName", "Warehose Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);

                    oBase.doMDCreateField(sTableName, "COMPrSt", "COMAH pre-set", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);

                    oBase.doMDCreateField(sTableName, "HAZCD", "HP Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);

                    oBase.doMDCreateField(sTableName, "HazClass", "ADR class", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);

                    //oBase.doMDCreateField(sTableName, "ADRClass", "ADR class", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    //oBase.doMDRemoveField(sTableName, "ADRClass");
                    oBase.doMDCreateField(sTableName, "UNNANo", "UN", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);

                    oBase.doMDCreateField(sTableName, "PackGrp", "Pkg Gp", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);

                    oBase.doMDCreateField(sTableName, "DRTCmts", "Testing Criteria and Spec", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 4000);

                    oBase.doMDCreateField(sTableName, "Active", "Active", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);

                }
            }
            if (CurrentReleaseCount < 1052 || FORCECREATE) {
                oBase.doMDRemoveField(sTableName, "TotCost");
                oBase.doMDRemoveField(sTableName, "HaulgCost");
                oBase.doMDRemoveField(sTableName, "HaulgPCnt");

                oBase.doMDCreateField(sTableName, "TotCost", "Total Cost", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                oBase.doMDCreateField(sTableName, "HaulgCost", "Haulage Cost", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                oBase.doMDCreateField(sTableName, "HaulgPCnt", "Transport/ container", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 11);
            }
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "doWeight", "Do Weight", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
            }
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "FnlTipCd", "Final Dispoal Site", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDCreateField(sTableName, "FnlTipNm", "Final Dispoal Site Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
            }

            if (CurrentReleaseCount < 1068 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "Descrp", "Description", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
            }
        }
        #endregion

        #region Create Quote Header Table
        private void doCreateWOQHTable(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase, string sTableName, string sSeriesName, string sTableDesc, string sSeriesDec) {
            if (CurrentReleaseCount < 1037 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, sTableDesc)) {
                    oBase.doAddNumberSeq(sSeriesName, sSeriesDec, 0);
                }
            }

            if (CurrentReleaseCount < 1033 || FORCECREATE) {
                oBase.doMDRemoveField(sTableName, "CardCode");
                oBase.doMDRemoveField(sTableName, "CardName");
                oBase.doMDRemoveField(sTableName, "Address");
                oBase.doMDRemoveField(sTableName, "Street");
                oBase.doMDRemoveField(sTableName, "Block");
                oBase.doMDRemoveField(sTableName, "City");
                oBase.doMDRemoveField(sTableName, "County");
                oBase.doMDRemoveField(sTableName, "State");
                oBase.doMDRemoveField(sTableName, "ZipCode");


                oBase.doMDRemoveField(sTableName, "CName");
                oBase.doMDRemoveField(sTableName, "Phone1");
                oBase.doMDRemoveField(sTableName, "Recontact");//WOQ Date
                oBase.doMDRemoveField(sTableName, "Status");
                oBase.doMDRemoveField(sTableName, "User");
                oBase.doMDRemoveField(sTableName, "SDate");

                oBase.doMDRemoveField(sTableName, "AttendUser");
                oBase.doMDRemoveField(sTableName, "Desc");

            }
            doCreateOrderTable(CurrentReleaseCount, oBase, sTableName);

            if (CurrentReleaseCount < 1038 || FORCECREATE) {
                //oBase.doMDCreateField(sTableName, "CardCode", "BP Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                //oBase.doMDCreateField(sTableName, "CardName", "BP Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                //oBase.doMDCreateField(sTableName, "Address", "Address ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                //oBase.doMDCreateField(sTableName, "Street", "Street", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                //oBase.doMDCreateField(sTableName, "Block", "Block", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                //oBase.doMDCreateField(sTableName, "City", "City", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                //oBase.doMDCreateField(sTableName, "County", "County", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                //oBase.doMDCreateField(sTableName, "State", "State", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);
                //oBase.doMDCreateField(sTableName, "ZipCode", "Post Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "Country", "Country", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);


                //oBase.doMDCreateField(sTableName, "CName", "Contact Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                //oBase.doMDCreateField(sTableName, "Phone1", "Telephone", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_Phone, 20);
                oBase.doMDCreateField(sTableName, "E_Mail", "Email Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "Notes", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 8000);
                oBase.doMDCreateField(sTableName, "ClgCode", "Number", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                //oBase.doMDCreateField(sTableName, "Recontact", "Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);//WOQ Date
                //oBase.doMDCreateField(sTableName, "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);

                //delete it                
                //oBase.doMDCreateField(sTableName, "AttendUser", "Assigned To", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 155);
                //oBase.doMDCreateField(sTableName, "User", "Created By", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 155);

                //delete it
                //oBase.doMDCreateField(sTableName, "Desc", "Description", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250);

                oBase.doMDCreateField(sTableName, "Version", "Version", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                oBase.doMDCreateField(sTableName, "Branch", "Branch", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                oBase.doMDCreateField(sTableName, "Source", "Source", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                //oBase.doMDCreateField(sTableName, "SDate", "Start Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);

                oBase.doMDCreateField(sTableName, "LstPrice", "List Price", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                oBase.doMDCreateField(sTableName, "Cost", "Cost", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);

                oBase.doMDCreateField(sTableName, "LPProfit", "List Price Profit", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                oBase.doMDCreateField(sTableName, "LPGrossM", "List Price Gross Margin", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 11);

                //U_ChgVAT
                //U_TCharge
                //U_NoChTax
                oBase.doMDCreateField(sTableName, "ChgVAT", "Tax on Charge Price", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                oBase.doMDCreateField(sTableName, "TCharge", "List Price Gross Margin", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                oBase.doMDCreateField(sTableName, "NoChgTax", "Do not Calculate Tax", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);


                oBase.doMDCreateField(sTableName, "ChgPrice", "Charge Price", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                oBase.doMDCreateField(sTableName, "CPProfit", "Charge Price Profit", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                oBase.doMDCreateField(sTableName, "CPGrossM", "Charge Price Gross Margin", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 11);
                //oBase.doMDCreateField(sTableName, "ApprSts", "Approval Status", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);

                oBase.doMDCreateField(sTableName, "WOHID", "WOH ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);

                oBase.doMDCreateField(sTableName, "ReviewNotes", "Review Notes", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 8000);

            }

            if (CurrentReleaseCount < 1038 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "AppRequired", "Approval Required", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
            }
            //Add the new Audit Table.
            if (CurrentReleaseCount < 1037 || FORCECREATE) {
                string sDataTableName = sTableName.Substring(1);
                oBase.doUpdateQuery("doMigrateWOQHeaderTable", com.idh.dbObjects.User.AUDITRL.MIGRATEMARKER + " " + sDataTableName);
            }
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "OrgDocNm", "Original WOQ Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
            }
            //if (CurrentReleaseCount < 1062 || FORCECREATE) {
            //    oBase.doMDCreateField(sTableName, "DocEntry", "DocEntry", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);

            //}
        }
        private void doCreateOrderTable(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase, string sTableName) {
            if (CurrentReleaseCount < 1038 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "CntrNo", "Contract No.", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "User", "Signed In User", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                oBase.doMDCreateField(sTableName, "Status", "Order Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "BDate", "Booking Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "BTime", "Booking Time", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 10);
                oBase.doMDCreateField(sTableName, "RSDate", "Request Start Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "RSTime", "Request End Time", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 10);
                oBase.doMDCreateField(sTableName, "REDate", "Request End Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "RETime", "Request End Time", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 10);
                oBase.doMDCreateField(sTableName, "CardCd", "Customer", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                oBase.doMDCreateField(sTableName, "CardNM", "Customer Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "Contact", "Contact Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "Address", "Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "Street", "Street", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "Block", "Block", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "City", "City", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "ZpCd", "Postal Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "Phone1", "Phone Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "CustRef", "Customer Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "ItemGrp", "Item Group", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "Qty", "Quantity", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 4);
                oBase.doMDCreateField(sTableName, "ORoad", "Off Road", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "SLicSp", "Skip License Supplier", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "SLicNr", "Skip License No", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "SLicExp", "Skip License Expiry Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "SLicCh", "Skip License Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "SpInst", "Special Instruction", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 230);



                oBase.doMDCreateField(sTableName, "Status", "WOQ Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);



                //** PRODUCER
                oBase.doMDCreateField(sTableName, "PCardCd", "Producer", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                oBase.doMDCreateField(sTableName, "PCardNM", "Producer Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "PContact", "Producer Contact Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "PAddress", "Producer Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "PStreet", "Producer Street", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "PBlock", "Producer Block", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "PCity", "Producer City", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "PZpCd", "Producer Postal Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "PPhone1", "Producer Phone Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

                //** SITE
                oBase.doMDCreateField(sTableName, "SCardCd", "Site", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                oBase.doMDCreateField(sTableName, "SCardNM", "Site Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "SContact", "Site Contact Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "SAddress", "Site Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "SStreet", "Site Street", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "SBlock", "Site Block", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "SCity", "Site City", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "SZpCd", "Site Postal Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "SPhone1", "Site Phone Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

                //** CARRIER
                oBase.doMDCreateField(sTableName, "CCardCd", "Carrier", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                oBase.doMDCreateField(sTableName, "CCardNM", "Carrier Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "CContact", "Carrier Contact Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "CAddress", "Carrier Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "CStreet", "Carrier Street", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "CBlock", "Carrier Block", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "CCity", "Carrier City", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "CZpCd", "Carrier Postal Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "CPhone1", "Carrier Phone Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);



                oBase.doMDCreateField(sTableName, "ORDTP", "Order Type (WO,DO)", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 5);



                oBase.doMDCreateField(sTableName, "SLicNm", "Skip License Supplier Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);


                oBase.doMDCreateField(sTableName, "RTIMEF", "Required Time From", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 10);
                oBase.doMDCreateField(sTableName, "RTIMET", "Required Time To", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 10);



                oBase.doMDCreateField(sTableName, "SupRef", "Supplier Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);



                oBase.doMDCreateField(sTableName, "SiteTl", "Site Phone Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);



                oBase.doMDCreateField(sTableName, "CopTRw", "Copy Instruction to rows", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 5);



                oBase.doMDCreateField(sTableName, "Route", "Route Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "Seq", "Route Sequence", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_Quantity, 4);



                oBase.doMDCreateField(sTableName, "County", "County", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);




                oBase.doMDCreateField(sTableName, "Origin", "Origin", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);



                oBase.doMDCreateField(sTableName, "PremCd", "Premises Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "SiteLic", "Site Licence Numbers", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 25);



                oBase.doMDCreateField(sTableName, "SteId", "Site Id", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);



                oBase.doMDCreateField(sTableName, "SepOrder", "Seperate Tipping and Transport", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);



                oBase.doMDCreateField(sTableName, "ENNO", "EN Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                //doUpdateQuery("UPDATE [" + sTableName + "] Set U_ENNO = '' WHERE U_ENNO Is NULL");

                oBase.doMDCreateField(sTableName, "DAAttach", "EN DA Attached", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doUpdateQuery("UPDATE [" + sTableName + "] Set U_DAAttach = 'N' WHERE U_DAAttach Is NULL");

                //doMDUpdateField(sTableName, "ENQtyOD", "EN Order Quantity", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_Measurement, 10);
                //doUpdateQuery("UPDATE [" + sTableName + "] Set U_ENQtyOD = 0 WHERE U_ENQtyOD Is NULL");

                oBase.doMDCreateField(sTableName, "ENPApFDt", "EN Period Applied From Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "ENPApTDt", "EN Period Applied To Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);

                //doMDUpdateField(sTableName, "ENPADur", "EN Period Applied Duration", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_Measurement, 10);
                //doUpdateQuery("UPDATE [" + sTableName + "] Set U_ENPADur = 0 WHERE U_ENPADur Is NULL");

                oBase.doMDCreateField(sTableName, "PBICODE", "PreBook Instruction Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);


                //
                //    doMDUpdateField(sTableName, "Analys1", "Analys1", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                //    doMDUpdateField(sTableName, "Analys2", "Analys2", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                //    doMDUpdateField(sTableName, "Analys3", "Analys3", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                //    doMDUpdateField(sTableName, "Analys4", "Analys4", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                //    doMDUpdateField(sTableName, "Analys5", "Analys5", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                //

                //641

                oBase.doMDCreateField(sTableName, "ProRef", "Producer Ref. No.", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "SiteRef", "Site Ref. No.", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);

                oBase.doMDCreateField(sTableName, "FirstBP", "First BP Selected", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                //Customer / Producer
                oBase.doUpdateQuery("UPDATE [" + sTableName + "] Set U_FirstBP = 'Customer' WHERE U_FirstBP Is NULL");



                oBase.doMDCreateField(sTableName, "ForeCS", "Forecast Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);



                oBase.doMDUpdateField(sTableName, "CardNM", "Customer Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDUpdateField(sTableName, "PCardNM", "Producer Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDUpdateField(sTableName, "SCardNM", "Site Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDUpdateField(sTableName, "CCardNM", "Carrier Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDUpdateField(sTableName, "SLicNm", "Skip License Supplier Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);


                //
                //    oBase.doMDUpdateField(sTableName, "TFSNotf", "TFS Notifier", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8);
                //


                //OnTime [#Ico000????] USA 
                //START
                //Mailing Address 

                oBase.doMDCreateField(sTableName, "MContact", "Mailing Contact Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "MAddress", "Mailing Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "MStreet", "Mailing Street", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "MBlock", "Mailing Block", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "MCity", "Mailing City", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "MZpCd", "Mailing Postal Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "MPhone1", "Mailing Phone Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);



                oBase.doMDCreateField(sTableName, "LckPrc", "Lock down Prices", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                string sQry = "UPDATE [" + sTableName + "] set U_LckPrc = 'N' WHERE U_LckPrc is NULL";
                oBase.doUpdateQuery("doCreateRowTable", sQry);

                //END
                //OnTime [#Ico000????] USA 

                //## Start 26-09-2013
                //Fields missed at WOH/DO; It was on BP Master so add fields to map

                oBase.doMDCreateField(sTableName, "WasLic", "Cus Waste Lic. Reg. No.", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "CWasLic", "Carr Waste Lic. Reg. No.", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);

                //## End

                //Adding State field in all addresses on WOH 

                oBase.doMDCreateField(sTableName, "State", "State", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "CState", "Carrier State", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "PState", "Producer State", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "SState", "Site State", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "MState", "Mailing State", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);


                //Add the new Audit Table.
                //
                //    string sDataTableName = sTableName.Substring(1);
                //    doUpdateQuery("doCreateOrderTable", AUDITRL.MIGRATEMARKER + " " + sDataTableName);

                //    //If AUDITRL.checkExistTableAuditTable(sDataTableName) = False Then
                //    //    doUpdateQuery("doCreateOrderTable", AUDITRL.doGetCreateQueryForTable(sDataTableName, True))
                //    //End If
                //

                //If moData.CurrentReleaseCount < 907 Then
                //    DataHandler.getInstance.doMDCreateArchiveTable(sTableName)
                //End If


                oBase.doMDCreateField(sTableName, "TRNCd", "Transaction Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);

                //##MA Start 12-09-2014 Issue#413

                oBase.doMDCreateField(sTableName, "MinJob", "Min. Job Required in a Day", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 4);

                //##MA End 12-09-2014 Issue#413
                //##MA Start 15-10-2014 
                //If moData.CurrentReleaseCount < 954 Then
                //    oBase.doMDCreateField(sTableName, "UseBPWgt", "Use BP Weight", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1)
                //End If
                //##MA End 15-10-2014
                //##MA Start 14-11-2014 

                oBase.doMDUpdateField(sTableName, "CustRef", "Customer Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                //doMDUpdateField(sTableName.Replace("IDH", "ARI"), "CustRef", "Customer Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50) 'temp up untill LV sync Archive Table
                //##MA End 14-11-2014

                //
                //    DataHandler.getInstance.doMDCreateArchiveTable(sTableName);
                //
                ////'MA 26-03-2015 moved field from Header to row table
                //if (moData.CurrentReleaseCount > 954 && moData.CurrentReleaseCount < 994) {
                //    doMDRemoveField(sTableName, "UseBPWgt");
                //    //', "Use BP Weight", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1)
                //

                //'MA Start 29-04-2015
                //
                oBase.doMDCreateField(sTableName, "CusFrnNm", "Customer Foreign Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);

                //'MA End 22-04-2015

                //'MA Start 22-04-2015

                oBase.doMDUpdateField(sTableName, "AddrssLN", "Customer Address LineNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDUpdateField(sTableName, "CAddrsLN", "Haulier Address LineNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDUpdateField(sTableName, "PAddrsLN", "Supplier Address LineNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDUpdateField(sTableName, "SAddrsLN", "Disposal Address LineNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDUpdateField(sTableName, "MAddrsLN", "Mailing Address LineNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);

                //'MA End 22-04-2015

            }

            //All Reference fields fixture - based on NumAtCard(100) field on mrk docs 
            if (CurrentReleaseCount < 1043 || FORCECREATE) {
                oBase.doMDUpdateField(sTableName, "CustRef", "Customer Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDUpdateField(sTableName, "SupRef", "Supplier Ref", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "ProRef", "Producer Ref. No.", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "SiteRef", "Site Ref. No.", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
            }
        }

        private void doCreateWOQRTable(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase, string sTableName, string sSeriesName, string sTableDesc, string sSeriesDec) {
            if (CurrentReleaseCount < 1038 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, sTableDesc)) {
                    oBase.doAddNumberSeq(sSeriesName, sSeriesDec, 0);//"WOQUOTED"
                }
            }
            if (CurrentReleaseCount < 1038 || FORCECREATE) {
                //oBase.doMDCreateField(sTableName, "WOQId", "WOQ ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "WOHID", "WOH ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "WORID", "WOR ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "AddItmID", "WOR Additional Item ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);

                oBase.doMDCreateField(sTableName, "EnqID", "Enquiry ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "EnqLID", "Enquiry Line ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "AdtnlItm", "Additional Item", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);

                oBase.doMDCreateField(sTableName, "PLineID", "Parent Row", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                //oBase.doMDCreateField(sTableName, "ItemCode", "Material Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                //oBase.doMDCreateField(sTableName, "ItemName", "Material Description", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                oBase.doMDCreateField(sTableName, "WstGpCd", "Waste Group Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "WstGpNm", "Waste Group Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                //oBase.doMDCreateField(sTableName, "EstQty", "Estimated Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 19);
                //oBase.doMDCreateField(sTableName, "UOM", "Unit of Measurement", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 7);
                //oBase.doMDCreateField(sTableName, "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2);


                oBase.doMDCreateField(sTableName, "LstPrice", "List Price", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);

                //oBase.doMDCreateField(sTableName, "Cost", "Cost", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);

                oBase.doMDCreateField(sTableName, "MnMargin", "Manual Margin", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 11);
                //oBase.doMDCreateField(sTableName, "ChgPrice", "Charge Price", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 11);
                oBase.doMDCreateField(sTableName, "GMargin", "Gross Margin", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 11);
                oBase.doMDCreateField(sTableName, "Sort", "Sort Order", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);

                oBase.doMDCreateField(sTableName, "Sample", "Sample", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "MSDS", "MSDS", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "RouteCd", "Route Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDCreateField(sTableName, "SampleRef", "Sample Ref.", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDCreateField(sTableName, "SampStatus", "Sample Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);

                oBase.doMDCreateField(sTableName, "WasFDsc", "Waste Cd Foreign Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);

            }
            if (CurrentReleaseCount < 1033) {
                oBase.doMDRemoveField(sTableName, "WOQId");
                oBase.doMDRemoveField(sTableName, "WOQLID");
                oBase.doMDRemoveField(sTableName, "ItemCode");
                oBase.doMDRemoveField(sTableName, "ItemName");
                oBase.doMDRemoveField(sTableName, "EstQty");
                oBase.doMDRemoveField(sTableName, "UOM");
                oBase.doMDRemoveField(sTableName, "Status");
                oBase.doMDRemoveField(sTableName, "ChgPrice");
                oBase.doMDRemoveField(sTableName, "U_ChgPrice");
                oBase.doMDRemoveField(sTableName, "Cost");
                //oBase.doUpdateQuery("UPDATE [" + sTableName + "] Set U_MANPRC = 0 WHERE U_MANPRC Is NULL");
            }
            if (CurrentReleaseCount < 1051 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "WhsCode", "Warehouse Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
            }
            if (CurrentReleaseCount < 1052 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "RtCdCode", "Route Internal Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
            }

            doCreateRowTable(CurrentReleaseCount, oBase, sTableName);
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "FnlTipCd", "Final Dispoal Site", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDCreateField(sTableName, "FnlTipNm", "Final Dispoal Site Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
            }
            //Add the new Audit Table.
            if (CurrentReleaseCount < 1038 || FORCECREATE) {
                string sDataTableName = sTableName.Substring(1);
                oBase.doUpdateQuery("doCreateWOQDetailTable", com.idh.dbObjects.User.AUDITRL.MIGRATEMARKER + " " + sDataTableName);
            }

        }
        protected virtual void doCreateRowTable(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase, string sTableName) {
            //Dim sTableName As String = "@IDH_JOBSHD"
            if (CurrentReleaseCount < 1034 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "JobNr", "Order Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                oBase.doMDCreateField(sTableName, "JobTp", "Order Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "RDate", "Required Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                //oBase.doMDCreateField(sTableName, "RTime", "Required Time", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 10)
                oBase.doMDCreateField(sTableName, "ASDate", "Action Start Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "ASTime", "Action Start Time", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 10);
                oBase.doMDCreateField(sTableName, "AEDate", "Action End Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "AETime", "Action End Time", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 10);
                oBase.doMDCreateField(sTableName, "VehTyp", "Vehicle Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "Lorry", "Lorry Registration", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "Driver", "Driver", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "DocNum", "Docket Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "SupRef", "Supplier Ref No", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "TipWgt", "Tipping Weight", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 5);
                oBase.doMDCreateField(sTableName, "TipCost", "Tipping Cost", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "TipTot", "Tipping Total Cost", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "CstWgt", "Customer Weight", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 5);
                oBase.doMDCreateField(sTableName, "TCharge", "Tipping Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "TCTotal", "Tipping Charge Total", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "CongCh", "Congestion Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "Weight", "Weight", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 5);
                oBase.doMDCreateField(sTableName, "Price", "Price", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "Discnt", "Discount", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "DisAmt", "Discount Amount", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "TaxAmt", "Tax Amount", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "Total", "Total", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "SLicNr", "Skip License No", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "SLicExp", "Skip License Expiry Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "SLicCh", "Skip License Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "ItmGrp", "Item Group Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "ItemCd", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "ItemDsc", "Item Description", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "WasCd", "Waste Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "WasDsc", "Waste Description", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "Serial", "Serial Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "Status", "Order Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "Quantity", "Order Quantity", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_Measurement, 10);
                oBase.doMDCreateField(sTableName, "JCost", "Order Row Cost Total", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "SAINV", "Sales Invoice", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "SAORD", "Sales Order", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "TIPPO", "Tipping PO", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "JOBPO", "Job PO", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

                oBase.doMDCreateField(sTableName, "SLicSp", "Skip License Supplier", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "Tip", "Disposal Site", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

                oBase.doMDCreateField(sTableName, "JCost", "Order Row Cost Total", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "Tip", "Disposal Site", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "CustCd", "Customer Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "CarrCd", "CarrierCode", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "CongCd", "Congestion Supp Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "TipNm", "Dispoal Site Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDCreateField(sTableName, "CustNm", "Customer Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "CarrNm", "Carrier Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "SLicNm", "Skip License Supplier Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "SCngNm", "Congestion Supplier Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "Dista", "Distance", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "CntrNo", "Contract Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "Wei1", "Weighridge Weigh 1", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 5);
                oBase.doMDCreateField(sTableName, "Wei2", "Weighbridge Weigh 2", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 5);
                oBase.doMDCreateField(sTableName, "Ser1", "Flash Serial 1", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "Ser2", "Flash Serial 2", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

                oBase.doMDCreateField(sTableName, "WDt1", "Weigh Date 1", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "WDt2", "Weigh Date 2", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "AddEx", "Additional Expences", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 5);
                oBase.doMDCreateField(sTableName, "PStat", "Purchase Order Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "OrdWgt", "Order (Haulage) Weight", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 5);
                oBase.doMDCreateField(sTableName, "OrdCost", "Order (Haulage) Cost", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "OrdTot", "Order (Haulage) Total Cost", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "TRLReg", "Trailer Reg. No.", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "TRLNM", "Trailer Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                //oBase.doMDCreateField(sTableName, "TRLTar", "Trailer Tare Weight", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 10)
                //oBase.doMDCreateField(sTableName, "VEHTar", "Vehicle Tare Weight", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 10)
                oBase.doMDCreateField(sTableName, "UOM", "Unit Of Measure", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "WASLIC", "Waste Lic. Reg. No.", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);

                oBase.doMDCreateField(sTableName, "SLPO", "Skip licence PO", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "SLicCst", "Skip Licence Cost", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "SLicCTo", "Skip Licence Cost Total", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "SLicCQt", "Skip Licence Quantity", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "WROrd", "WR Link Order", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                oBase.doMDCreateField(sTableName, "WRRow", "WR Link Order Row", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                oBase.doMDCreateField(sTableName, "RTime", "Required Time From", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 10);
                oBase.doMDCreateField(sTableName, "RTimeT", "Required Time To", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 10);
                oBase.doMDCreateField(sTableName, "CusQty", "Customer Quantity", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 5);
                oBase.doMDCreateField(sTableName, "CusChr", "Customer Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "PayMeth", "Payment Method", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "CustRef", "Customer Ref. No.", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDCreateField(sTableName, "RdWgt", "Read Weight", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 5);
                oBase.doMDCreateField(sTableName, "LorryCd", "Lorry Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8);
                oBase.doMDCreateField(sTableName, "Covera", "Coverage Used", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDCreateField(sTableName, "Comment", "Comment", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 230);
                oBase.doMDCreateField(sTableName, "PUOM", "Purchase Unit Of Measure", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "AUOM", "Purchase Unit Of Measure", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "AUOMQt", "Overriding Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 10);
                oBase.doMDCreateField(sTableName, "CCNum", "Credit Card No", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "CCStat", "Credit Card Approval Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "PayStat", "Payment Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);



                oBase.doMDCreateField(sTableName, "CCType", "Credit Card Type", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 5);
                oBase.doMDCreateField(sTableName, "PARCPT", "Payment Receipt", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);



                oBase.doMDCreateField(sTableName, "ENREF", "Evidence Reference Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);



                oBase.doMDCreateField(sTableName, "JobRmD", "Reminder Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "JobRmT", "Reminder Time", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 2);
                oBase.doMDCreateField(sTableName, "RemNot", "Order Job Reminder Text", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                oBase.doMDCreateField(sTableName, "RemCnt", "Reminder Sent Count", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_Quantity, 5);



                oBase.doMDCreateField(sTableName, "TZone", "Tip Zone", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);



                oBase.doMDCreateField(sTableName, "CoverHst", "Coverage History", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 230);



                oBase.doMDCreateField(sTableName, "CoverHst", "Coverage History", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 230);



                oBase.doMDCreateField(sTableName, "ProPO", "Producer PO", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "ProCd", "Producer Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                oBase.doMDCreateField(sTableName, "ProNm", "Producer Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);



                oBase.doMDCreateField(sTableName, "User", "Created By", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "Branch", "Branch", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);



                oBase.doMDCreateField(sTableName, "TAddChrg", "Additional Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "TAddCost", "Additional Cost", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);



                //** Start Use to be in WT1

                oBase.doMDCreateField(sTableName, "IssQty", "Issue Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 10);

                //** End Use to be in WT1


                oBase.doMDCreateField(sTableName, "Obligated", "Obligated", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);



                oBase.doMDCreateField(sTableName, "Origin", "Origin", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);



                oBase.doMDCreateField(sTableName, "TChrgVtRt", "Tipping Sales Vat Rate", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 5);
                oBase.doMDCreateField(sTableName, "HChrgVtRt", "Haulage Sales Vat Rate", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 5);

                oBase.doMDCreateField(sTableName, "TCostVtRt", "Tipping Buying Vat Rate", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 5);
                oBase.doMDCreateField(sTableName, "HCostVtRt", "Tipping Buying Vat Rate", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Percentage, 5);

                oBase.doMDCreateField(sTableName, "TChrgVtGrp", "Tipping Sales Vat Group", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "HChrgVtGrp", "Haulage Sales Vat Group", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

                oBase.doMDCreateField(sTableName, "TCostVtGrp", "Tipping Buying Vat Group", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "HCostVtGrp", "Tipping Buying Vat Group", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

                oBase.doMDCreateField(sTableName, "VtCostAmt", "Purchasing Vat Amount", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);



                oBase.doMDCreateField(sTableName, "CustCs", "Customer Compliance Scheme", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "ConNum", "Consignment Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

                oBase.doMDCreateField(sTableName, "CCExp", "CCard Expiry", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "CCIs", "CCard Issue #", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "CCSec", "CCard Security Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "CCHNum", "CCard House Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "CCPCd", "CCard PostCode", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);




                oBase.doMDCreateField(sTableName, "IntComm", "Internal Comment", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 230);



                oBase.doMDCreateField(sTableName, "ContNr", "Container Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDCreateField(sTableName, "SealNr", "Seal Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);



                oBase.doMDCreateField(sTableName, "FPCoH", "Freeze Price Cost Haulage", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "FPCoT", "Freeze Price Cost Tipping", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "FPChH", "Freeze Price Charge Haulage", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "FPChT", "Freeze Price Charge Tipping", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);



                oBase.doMDCreateField(sTableName, "WastTNN", "Waste Transfer Note Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "HazWCNN", "Hazardous Waste Consignment Note Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 12);
                oBase.doMDCreateField(sTableName, "SiteRef", "Site Reference Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                oBase.doMDCreateField(sTableName, "ExtWeig", "External Weighbridge Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);


                oBase.doMDCreateField(sTableName, "BDate", "Booking Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "BTime", "Booking Time", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 10);


                oBase.doMDCreateField(sTableName, "CarrReb", "Carrier Rebate Item", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);



                oBase.doMDCreateField(sTableName, "TPCN", "Disposal Purchase Credit Note", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);



                oBase.doMDCreateField(sTableName, "MANPRC", "Manual prices entered ( 1 - Tip Charge; 2 - Haul Charge, 4 - Tip Cost, 8 - Haul Cost )", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 4);



                oBase.doMDCreateField(sTableName, "ENNO", "EN Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

                oBase.doMDCreateField(sTableName, "SemSolid", "Semi Solid", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "PaMuPu", "Pasty/ Muddy/ pulpy", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "Dusty", "Dusty", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "Fluid", "Fluid", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "Firm", "Firm", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "Consiste", "Consistency", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "TypPTreat", "Type of pre-treatment", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);

                oBase.doMDCreateField(sTableName, "DAAttach", "DA Attached", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);




                oBase.doMDCreateField(sTableName, "LnkPBI", "Linked PBI Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);



                oBase.doMDCreateField(sTableName, "DPRRef", "DPR Referance", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);



                oBase.doMDCreateField(sTableName, "MDChngd", "Marketing Document Changed", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);



                oBase.doMDCreateField(sTableName, "Sched", "Scheduled Job", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "USched", "Un-Scheduled Job", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);



                oBase.doMDCreateField(sTableName, "CustReb", "Customer Rebate Item", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "TCCN", "Customer Credit Note", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);


                oBase.doMDCreateField(sTableName, "RowSta", "Row Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                oBase.doMDCreateField(sTableName, "ActComm", "Action Comment", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 200);


                oBase.doMDCreateField(sTableName, "HlSQty", "Haulage Selling Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 5);
                oBase.doMDCreateField(sTableName, "HaulAC", "Haulage Automatic Calculation", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "ContNr", "Container Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDCreateField(sTableName, "SealNr", "Seal Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDCreateField(sTableName, "ClgCode", "Activity Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "ProRef", "Producer Ref. No.", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "SerialC", "Serial Number Collection", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "PAUOMQt", "Purchase Overriding Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 5);
                oBase.doMDCreateField(sTableName, "PRdWgt", "Producer Read Weight", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 10);
                oBase.doMDCreateField(sTableName, "ProWgt", "Producer Weight", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 10);
                oBase.doMDCreateField(sTableName, "ProUOM", "Producer Unit Of Measure", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "PCost", "Producer Cost", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "PCTotal", "Producer Coharge Total", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);

                oBase.doMDCreateField(sTableName, "WgtDed", "Weight Deduction", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 5);
                oBase.doMDCreateField(sTableName, "AdjWgt", "Adjusted Weight", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 5);
                oBase.doMDCreateField(sTableName, "ValDed", "Value Deduction", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 5);
                oBase.doMDCreateField(sTableName, "BookIn", "Book In Stock", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);

                oBase.doMDCreateField(sTableName, "CarrRef", "Carrier Ref. No.", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "PCharge", "Producer Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "ProGRPO", "Producer Goods Receipt PO", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "CustGR", "Customer Goods Receipt", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "GRIn", "Goods Receipt Incomming", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "LoadSht", "Load Sheet Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "SODlvNot", "SO Delivery Note", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "IDHSEQ", "Sequence", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "IDHCMT", "Route Comments", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                oBase.doMDCreateField(sTableName, "IDHDRVI", "Driver Instructions", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 200);
                oBase.doMDCreateField(sTableName, "IDHCHK", "Check", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "IDHPRTD", "Printed", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "IDHRTCD", "Route Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8);
                oBase.doMDCreateField(sTableName, "IDHRTDT", "Route Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "ExpLdWgt", "Load Weight", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 5);
                oBase.doMDCreateField(sTableName, "IsTrl", "Trail Load", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "UseWgt", "Which Weight to Use", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "TFSMov", "TFS Movement", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 8);
                oBase.doMDCreateField(sTableName, "PayTrm", "Payment Term", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "JStat", "Journals Status", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                oBase.doMDCreateField(sTableName, "Jrnl", "Journal Entry", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "SAddress", "Site Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);

                oBase.doMDCreateField(sTableName, "LckPrc", "Lock down Prices", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "CBICont", "Containers", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "CBIManQty", "Manifest Quantity", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 50);
                oBase.doMDCreateField(sTableName, "CBIManUOM", "Manifest UOM", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                //Increasing length of the Comments and Internal Comments field 
                oBase.doMDCreateField(sTableName, "ExtComm1", "Ext. Comment1", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 1000);
                oBase.doMDCreateField(sTableName, "ExtComm2", "Ext. Comment2", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 2000);
                oBase.doMDCreateField(sTableName, "MaximoNum", "Maximo Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "TFSReq", "TFS Required", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "TFSNum", "TFS Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);

                oBase.doMDCreateField(sTableName, "TRdWgt", "Tip Read Weight", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Measurement, 10);

                oBase.doMDCreateField(sTableName, "PrcLink", "CIPSIP Link", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

                oBase.doMDCreateField(sTableName, "TAUOMQt", "Disposal Cost Overriding Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 5);

                oBase.doMDCreateField(sTableName, "Rebate", "Rebate Item", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);

                oBase.doMDCreateField(sTableName, "TAddChVat", "Additional Charge VAT", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "TAddCsVat", "Additional Cost VAT", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);

                oBase.doMDCreateField(sTableName, "AddCharge", "Additional Charge", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);
                oBase.doMDCreateField(sTableName, "AddCost", "Additional Cost", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 5);

                //System.Text.StringBuilder var1 = new System.Text.StringBuilder();

                oBase.doMDCreateField(sTableName, "JbToBeDoneDt", "Jobs To Be Done Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "JbToBeDoneTm", "Jobs To Be Done Time", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 10);

                oBase.doMDCreateField(sTableName, "DrivrOnSitDt", "Drive On Site Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "DrivrOnSitTm", "Drive On Site Time", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 10);

                oBase.doMDCreateField(sTableName, "DrivrOfSitDt", "Driver Off Site Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10);
                oBase.doMDCreateField(sTableName, "DrivrOfSitTm", "Driver Off Site Time", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_Time, 10);

                oBase.doMDCreateField(sTableName, "AltWasDsc", "Alternate Waste Descrp", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 255);

                oBase.doMDCreateField(sTableName, "OTComments", "Order Type Comments", SAPbobsCOM.BoFieldTypes.db_Memo, SAPbobsCOM.BoFldSubTypes.st_None, 2000);
                oBase.doMDCreateField(sTableName, "TrnCode", "Transaction Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 40);

                oBase.doMDCreateField(sTableName, "BPWgt", "BP Weight", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 10);
                oBase.doMDCreateField(sTableName, "CarWgt", "Carrier Weight", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 10);

                oBase.doMDCreateField(sTableName, "PROINV", "Producer Invoice", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "WHTRNF", "Warehouse Transfer", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "UseBPWgt", "Use BP Weight", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                oBase.doMDCreateField(sTableName, "ProPay", "Producer OUT Payment", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
                oBase.doMDCreateField(sTableName, "CustPay", "Customer In Payment", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

                oBase.doMDCreateField(sTableName, "WROrd2", "Extra WR Link Order", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                oBase.doMDCreateField(sTableName, "WRRow2", "Extra WR Link Order Row", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 15);
                oBase.doMDCreateField(sTableName, "DispCgCc", "Disposal Charge Currency", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);
                oBase.doMDCreateField(sTableName, "CarrCgCc", "Carrier Charge Currency", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);

                oBase.doMDCreateField(sTableName, "PurcCoCc", "Purchase Cost Currency", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);
                oBase.doMDCreateField(sTableName, "DispCoCc", "Disposal Cost Currency", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);
                oBase.doMDCreateField(sTableName, "CarrCoCc", "Carrier Cost Currency", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);
                oBase.doMDCreateField(sTableName, "LiscCoCc", "Liscence Cost Currency", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);

                oBase.doMDCreateField(sTableName, "SAddrsLN", "Disposal Address LineNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);

                com.idh.bridge.DataHandler.GETINSTANCE().doMDCreateArchiveTable(sTableName);

            }

            //All Reference fields fixture - based on NumAtCard(100) field on mrk docs 
            if (CurrentReleaseCount < 1043 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "SupRef", "Supplier Ref No", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "CustRef", "Customer Ref. No.", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "ENREF", "Evidence Reference Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "SiteRef", "Site Reference Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "DPRRef", "DPR Referance", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "ProRef", "Producer Ref. No.", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                oBase.doMDCreateField(sTableName, "CarrRef", "Carrier Ref. No.", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
            }
            if (CurrentReleaseCount < 1043 || FORCECREATE) {
                oBase.doMDUpdateField(sTableName, "TipNm", "Dispoal Site Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
            }

            //if ( CurrentReleaseCount < 1077 || FORCECREATE) {
            //    doSynchArchiveTable(True, sTableName)
            //}

        }
        #endregion

        private void doCreateProvisioalBatchTable(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            string sTableName = "@IDH_PVHBTH";
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Provisional Batch Head")) {
                    oBase.doAddNumberSeq("SEQPVHBTH", "Provisional Batch HSeries", 0);

                    oBase.doMDCreateField(sTableName, "BatchTyp", "Batch Type", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "LabTskCd", "Lab Task Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "WastCd", "Waste Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "WastDsc", "Waste Desc", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "ItemCd", "Container Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "ItemDsc", "Container Desc", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "FinalBt", "Final Batch", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
                    oBase.doMDCreateField(sTableName, "DisRCode", "Disposal Route Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "Qty", "Container Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 11);
                    oBase.doMDCreateField(sTableName, "WastQty", "Waste Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 11);
                    oBase.doMDCreateField(sTableName, "CntnrQty", "Container Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 11);
                    oBase.doMDCreateField(sTableName, "UOM", "UOM", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 6);
                    oBase.doMDCreateField(sTableName, "BatchDt", "Batch Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "ProdNum", "Production Order Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "ProdDoc", "Production Order", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "Warehouse", "Warehouse", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 30);
                    oBase.doMDCreateField(sTableName, "WOHCode", "WOH Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "WORCode", "WOR Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "WORCntQty", "WOR Container Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 11);
                    oBase.doMDCreateField(sTableName, "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);

                }
            }

            sTableName = "@IDH_PVDBTH";
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                if (oBase.doMDCreateTable(sTableName, "Provisional Batch Detail")) {
                    oBase.doAddNumberSeq("SEQPVDBTH", "Provisional Batch HSeries", 0);

                    oBase.doMDCreateField(sTableName, "Batch", "Batch Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "BatchSysNm", "Batch Internal Sys Number", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "AbsEntry", "Batch AbsEntry", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
                    oBase.doMDCreateField(sTableName, "PRVHCD", "Provisional Batch Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "WastCd", "Waste Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "WastDsc", "Waste Desc", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "ItemCd", "Container Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                    oBase.doMDCreateField(sTableName, "ItemDsc", "Container Desc", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "DisRCode", "Disposal Route Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                    oBase.doMDCreateField(sTableName, "WhsCode", "Warehouse Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
                }
            }

            //Add UDF in batch table
            sTableName = "OBTN";//batch table
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "PRVHCD", "Provisional Batch Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
            }
            sTableName = "OWOR";//production order
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "PRVHCD", "Provisional Batch Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
            }
            sTableName = "WOR1";//production order Line
            if (CurrentReleaseCount < 1053 || FORCECREATE) {
                oBase.doMDCreateField(sTableName, "PRVDCD", "Provisional Batch Line Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
                oBase.doMDCreateField(sTableName, "Batch", "Batch", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
            }
        }
        private void doCreateView(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
            //View for Enquiry Manager
            if (CurrentReleaseCount < 1050 || FORCECREATE) {
                StringBuilder varname1 = new StringBuilder();
                varname1.Append("CREATE View [dbo].[ENQ_MANAGER] \n");
                varname1.Append("As \n");
                varname1.Append("SELECT \n");
                varname1.Append("	r.U_EnqId \n");
                varname1.Append("	,r.Code \n");
                varname1.Append("	,h.U_CardCode \n");
                varname1.Append("	,h.U_CardName \n");
                varname1.Append("	,h.U_Recontact \n");
                varname1.Append("	,h.U_Address \n");
                varname1.Append("	,h.U_Street \n");
                varname1.Append("	,h.U_Block \n");
                varname1.Append("	,h.U_City \n");
                varname1.Append("	,h.U_Country \n");
                varname1.Append("	,h.U_County \n");
                varname1.Append("	,h.U_State \n");
                varname1.Append("	,h.U_ZipCode \n");
                varname1.Append("	,h.U_Desc \n");
                varname1.Append("	,h.U_CName \n");
                varname1.Append("	,h.U_E_Mail \n");
                varname1.Append("	,h.U_Phone1 \n");
                varname1.Append("	,h.U_Notes \n");
                varname1.Append("	,h.U_Source \n");
                varname1.Append("	,h.U_User \n");
                varname1.Append("	,h.U_ClgCode \n");
                varname1.Append("	,h.U_AttendUser \n");
                varname1.Append("	,r.U_AddItm \n");
                varname1.Append("	,r.U_PCode \n");
                varname1.Append("	,r.U_ItemCode \n");
                varname1.Append("	,r.U_ItemDesc \n");
                varname1.Append("	,r.U_ItemName \n");
                varname1.Append("	,r.U_WstGpCd \n");
                varname1.Append("	,r.U_WstGpNm \n");
                varname1.Append("	,r.U_EstQty \n");
                varname1.Append("	,r.U_UOM \n");
                varname1.Append("	,h.U_Status HU_Status \n");
                varname1.Append("	,hstatus.U_Desc As HStatusDesc \n");
                varname1.Append("	,r.U_Status \n");
                varname1.Append("	,rstatus.U_Desc As RStatusDesc \n");
                varname1.Append("	,r.U_WOQID \n");
                varname1.Append("	,r.U_WOQLineID \n");
                varname1.Append("	,r.U_Sort \n");
                varname1.Append("	,isnull(hsource.U_Desc,'') As HSourceDesc \n");
                varname1.Append("FROM [@IDH_ENQUIRY] h  WITH (NOLOCK) \n");
                varname1.Append("	Left Join [@IDH_WRSTUSNW] hsource WITH (NOLOCK) on (hsource.U_Value = h.U_Source and hsource.U_ObjType = 108) \n");
                varname1.Append("	,[@IDH_ENQITEM] r WITH (NOLOCK) \n");
                varname1.Append("	,[@IDH_WRSTUSNW] hstatus WITH (NOLOCK) \n");
                varname1.Append("	,[@IDH_WRSTUSNW] rstatus WITH (NOLOCK) \n");
                varname1.Append("WHERE ( h.Code = r.U_EnqId  or r.U_EnqId is null) \n");
                varname1.Append("	AND hstatus.U_ObjType = 101 \n");
                varname1.Append("	AND hstatus.U_Value = h.U_Status \n");
                varname1.Append("	AND rstatus.U_ObjType = 102 \n");
                varname1.Append("	AND rstatus.U_Value = r.U_Status \n");
                varname1.Append("Union All \n");
                varname1.Append("SELECT \n");
                varname1.Append("	h.Code as  U_EnqId \n");
                varname1.Append("	,null Code \n");
                varname1.Append("	,h.U_CardCode \n");
                varname1.Append("	,h.U_CardName \n");
                varname1.Append("	,h.U_Recontact \n");
                varname1.Append("	,h.U_Address \n");
                varname1.Append("	,h.U_Street \n");
                varname1.Append("	,h.U_Block \n");
                varname1.Append("	,h.U_City \n");
                varname1.Append("	,h.U_Country \n");
                varname1.Append("	,h.U_County \n");
                varname1.Append("	,h.U_State \n");
                varname1.Append("	,h.U_ZipCode \n");
                varname1.Append("	,h.U_Desc \n");
                varname1.Append("	,h.U_CName \n");
                varname1.Append("	,h.U_E_Mail \n");
                varname1.Append("	,h.U_Phone1 \n");
                varname1.Append("	,h.U_Notes \n");
                varname1.Append("	,h.U_Source \n");
                varname1.Append("	,h.U_User \n");
                varname1.Append("	,h.U_ClgCode \n");
                varname1.Append("	,h.U_AttendUser \n");
                varname1.Append("	,'' U_AddItm \n");
                varname1.Append("	,'' U_PCode \n");
                varname1.Append("	,'' U_ItemCode \n");
                varname1.Append("	,'' U_ItemDesc \n");
                varname1.Append("	,'' U_ItemName \n");
                varname1.Append("	,'' U_WstGpCd \n");
                varname1.Append("	,'' U_WstGpNm \n");
                varname1.Append("	,'0' U_EstQty \n");
                varname1.Append("	,'' U_UOM \n");
                varname1.Append("	,h.U_Status as HU_Status \n");
                varname1.Append("	,hstatus.U_Desc As HStatusDesc \n");
                varname1.Append("	,'' U_Status \n");
                varname1.Append("	,'' As RStatusDesc \n");
                varname1.Append("	,'' U_WOQID \n");
                varname1.Append("	,'' U_WOQLineID \n");
                varname1.Append("	,0 U_Sort \n");
                varname1.Append("	,isnull(hsource.U_Desc,'') As HSourceDesc \n");
                varname1.Append("FROM [@IDH_ENQUIRY] h  WITH (NOLOCK) \n");
                varname1.Append("	Left Join [@IDH_WRSTUSNW] hsource WITH (NOLOCK) on (hsource.U_Value = h.U_Source and hsource.U_ObjType = 108) \n");
                varname1.Append("	,[@IDH_WRSTUSNW] hstatus WITH (NOLOCK) \n");
                varname1.Append("WHERE ( h.Code Not in (Select  r.U_EnqId  from [@IDH_ENQITEM] r with(nolock))) \n");
                varname1.Append("	AND hstatus.U_ObjType = 101 \n");
                varname1.Append("	AND hstatus.U_Value = h.U_Status");
                oBase.doUpdateQuery(varname1.ToString());
            }
            if (CurrentReleaseCount < 1050 || FORCECREATE) {
                StringBuilder varname1 = new StringBuilder();
                varname1.Append("Create View [dbo].[IDH_WOQMANAGER] \n");
                varname1.Append("As \n");
                varname1.Append("SELECT \n");
                varname1.Append("	  r.Code \n");
                varname1.Append("	  ,r.[U_EnqID] \n");
                varname1.Append("      ,r.[U_EnqLID] \n");
                varname1.Append("      ,r.[U_AdtnlItm] \n");
                varname1.Append("      ,r.[U_PLineID] \n");
                varname1.Append("      ,r.[U_WstGpCd] \n");
                varname1.Append("      ,r.[U_WstGpNm] \n");
                varname1.Append("      ,r.[U_LstPrice] \n");
                varname1.Append("      ,r.[U_MnMargin] \n");
                varname1.Append("      ,r.[U_GMargin] \n");
                varname1.Append("      ,r.[U_Sort] \n");
                varname1.Append("      ,r.[U_JobNr] \n");
                varname1.Append("      ,r.[U_JobTp] \n");
                varname1.Append("      ,r.[U_RDate] \n");
                varname1.Append("      ,r.[U_ASDate] \n");
                varname1.Append("      ,r.[U_ASTime] \n");
                varname1.Append("      ,r.[U_AEDate] \n");
                varname1.Append("      ,r.[U_AETime] \n");
                varname1.Append("      ,r.[U_VehTyp] \n");
                varname1.Append("      ,r.[U_Lorry] \n");
                varname1.Append("      ,r.[U_Driver] \n");
                varname1.Append("      ,r.[U_DocNum] \n");
                varname1.Append("      ,r.[U_SupRef] \n");
                varname1.Append("      ,r.[U_TipWgt] \n");
                varname1.Append("      ,r.[U_TipCost] \n");
                varname1.Append("      ,r.[U_TipTot] \n");
                varname1.Append("      ,r.[U_CstWgt] \n");
                varname1.Append("      ,r.[U_TCharge] \n");
                varname1.Append("      ,r.[U_TCTotal] \n");
                varname1.Append("      ,r.[U_CongCh] \n");
                varname1.Append("      ,r.[U_Weight] \n");
                varname1.Append("      ,r.[U_Price] \n");
                varname1.Append("      ,r.[U_Discnt] \n");
                varname1.Append("      ,r.[U_DisAmt] \n");
                varname1.Append("      ,r.[U_TaxAmt] \n");
                varname1.Append("      ,r.[U_Total] \n");
                varname1.Append("      ,r.[U_SLicNr] \n");
                varname1.Append("      ,r.[U_SLicExp] \n");
                varname1.Append("      ,r.[U_SLicCh] \n");
                varname1.Append("      ,r.[U_ItmGrp] \n");
                varname1.Append("      ,r.[U_ItemCd] \n");
                varname1.Append("      ,r.[U_ItemDsc] \n");
                varname1.Append("      ,r.[U_WasCd] \n");
                varname1.Append("      ,r.[U_WasDsc] \n");
                varname1.Append("      ,r.[U_Serial] \n");
                varname1.Append("      ,r.[U_Quantity] \n");
                varname1.Append("      ,r.[U_JCost] \n");
                varname1.Append("      ,r.[U_SAINV] \n");
                varname1.Append("      ,r.[U_SAORD] \n");
                varname1.Append("      ,r.[U_TIPPO] \n");
                varname1.Append("      ,r.[U_JOBPO] \n");
                varname1.Append("      ,r.[U_SLicSp] As RSLicSp \n");
                varname1.Append("      ,r.[U_Tip] \n");
                varname1.Append("      ,r.[U_CustCd] \n");
                varname1.Append("      ,r.[U_CarrCd] \n");
                varname1.Append("      ,r.[U_CongCd] \n");
                varname1.Append("      ,r.[U_TipNm] \n");
                varname1.Append("      ,r.[U_CustNm] \n");
                varname1.Append("      ,r.[U_CarrNm] \n");
                varname1.Append("      ,r.[U_SLicNm] \n");
                varname1.Append("      ,r.[U_SCngNm] \n");
                varname1.Append("      ,r.[U_Dista] \n");
                varname1.Append("      ,r.[U_CntrNo] \n");
                varname1.Append("      ,r.[U_Wei1] \n");
                varname1.Append("      ,r.[U_Wei2] \n");
                varname1.Append("      ,r.[U_Ser1] \n");
                varname1.Append("      ,r.[U_Ser2] \n");
                varname1.Append("      ,r.[U_WDt1] \n");
                varname1.Append("      ,r.[U_WDt2] \n");
                varname1.Append("      ,r.[U_AddEx] \n");
                varname1.Append("      ,r.[U_PStat] \n");
                varname1.Append("      ,r.[U_OrdWgt] \n");
                varname1.Append("      ,r.[U_OrdCost] \n");
                varname1.Append("      ,r.[U_OrdTot] \n");
                varname1.Append("      ,r.[U_TRLReg] \n");
                varname1.Append("      ,r.[U_TRLNM] \n");
                varname1.Append("      --,r.[U_WASLIC]  \n");
                varname1.Append("      ,r.[U_SLPO] \n");
                varname1.Append("      ,r.[U_SLicCst] \n");
                varname1.Append("      ,r.[U_SLicCTo] \n");
                varname1.Append("      ,r.[U_SLicCQt] \n");
                varname1.Append("      ,r.[U_WROrd] \n");
                varname1.Append("      ,r.[U_WRRow] \n");
                varname1.Append("      ,r.[U_RTime] \n");
                varname1.Append("      ,r.[U_RTimeT] \n");
                varname1.Append("      ,r.[U_CusQty] \n");
                varname1.Append("      ,r.[U_CusChr] \n");
                varname1.Append("      ,r.[U_PayMeth] \n");
                varname1.Append("      ,r.[U_CustRef] as RCustRef \n");
                varname1.Append("      ,r.[U_RdWgt] \n");
                varname1.Append("      ,r.[U_LorryCd] \n");
                varname1.Append("      ,r.[U_Covera] \n");
                varname1.Append("      ,r.[U_Comment] \n");
                varname1.Append("      ,r.[U_PUOM] \n");
                varname1.Append("      ,r.[U_AUOM] \n");
                varname1.Append("      ,r.[U_AUOMQt] \n");
                varname1.Append("      ,r.[U_CCNum] \n");
                varname1.Append("      ,r.[U_CCStat] \n");
                varname1.Append("      ,r.[U_PayStat] \n");
                varname1.Append("      ,r.[U_CCType] \n");
                varname1.Append("      ,r.[U_PARCPT] \n");
                varname1.Append("      ,r.[U_ENREF] \n");
                varname1.Append("      ,r.[U_JobRmD] \n");
                varname1.Append("      ,r.[U_JobRmT] \n");
                varname1.Append("      ,r.[U_RemNot] \n");
                varname1.Append("      ,r.[U_RemCnt] \n");
                varname1.Append("      ,r.[U_TZone] \n");
                varname1.Append("      ,r.[U_CoverHst] \n");
                varname1.Append("      ,r.[U_ProPO] \n");
                varname1.Append("      ,r.[U_ProCd] \n");
                varname1.Append("      ,r.[U_ProNm] \n");
                varname1.Append("      --,r.[U_User]  \n");
                varname1.Append("      ,r.[U_Branch] \n");
                varname1.Append("      ,r.[U_TAddChrg] \n");
                varname1.Append("      ,r.[U_TAddCost] \n");
                varname1.Append("      ,r.[U_IssQty] \n");
                varname1.Append("      ,r.[U_Obligated] \n");
                varname1.Append("      ,r.[U_Origin] \n");
                varname1.Append("      ,r.[U_TChrgVtRt] \n");
                varname1.Append("      ,r.[U_HChrgVtRt] \n");
                varname1.Append("      ,r.[U_TCostVtRt] \n");
                varname1.Append("      ,r.[U_HCostVtRt] \n");
                varname1.Append("      ,r.[U_TChrgVtGrp] \n");
                varname1.Append("      ,r.[U_HChrgVtGrp] \n");
                varname1.Append("      ,r.[U_TCostVtGrp] \n");
                varname1.Append("      ,r.[U_HCostVtGrp] \n");
                varname1.Append("      ,r.[U_VtCostAmt] \n");
                varname1.Append("      ,r.[U_CustCs] \n");
                varname1.Append("      ,r.[U_ConNum] \n");
                varname1.Append("      ,r.[U_CCExp] \n");
                varname1.Append("      ,r.[U_CCIs] \n");
                varname1.Append("      ,r.[U_CCSec] \n");
                varname1.Append("      ,r.[U_CCHNum] \n");
                varname1.Append("      ,r.[U_CCPCd] \n");
                varname1.Append("      ,r.[U_IntComm] \n");
                varname1.Append("      ,r.[U_ContNr] \n");
                varname1.Append("      ,r.[U_SealNr] \n");
                varname1.Append("      ,r.[U_FPCoH] \n");
                varname1.Append("      ,r.[U_FPCoT] \n");
                varname1.Append("      ,r.[U_FPChH] \n");
                varname1.Append("      ,r.[U_FPChT] \n");
                varname1.Append("      ,r.[U_WastTNN] \n");
                varname1.Append("      ,r.[U_HazWCNN] \n");
                varname1.Append("      ,r.[U_SiteRef] \n");
                varname1.Append("      ,r.[U_ExtWeig] \n");
                varname1.Append("      ,r.[U_BDate] As RBDAte \n");
                varname1.Append("      ,r.[U_BTime] As RBTime \n");
                varname1.Append("      ,r.[U_CarrReb] \n");
                varname1.Append("      ,r.[U_TPCN] \n");
                varname1.Append("      ,r.[U_MANPRC] \n");
                varname1.Append("      ,r.[U_ENNO] \n");
                varname1.Append("      ,r.[U_SemSolid] \n");
                varname1.Append("      ,r.[U_PaMuPu] \n");
                varname1.Append("      ,r.[U_Dusty] \n");
                varname1.Append("      ,r.[U_Fluid] \n");
                varname1.Append("      ,r.[U_Firm] \n");
                varname1.Append("      ,r.[U_Consiste] \n");
                varname1.Append("      ,r.[U_TypPTreat] \n");
                varname1.Append("      ,r.[U_DAAttach] \n");
                varname1.Append("      ,r.[U_LnkPBI] \n");
                varname1.Append("      ,r.[U_DPRRef] \n");
                varname1.Append("      ,r.[U_MDChngd] \n");
                varname1.Append("      ,r.[U_Sched] \n");
                varname1.Append("      ,r.[U_USched] \n");
                varname1.Append("      ,r.[U_CustReb] \n");
                varname1.Append("      ,r.[U_TCCN] \n");
                varname1.Append("      ,r.[U_RowSta] \n");
                varname1.Append("      ,r.[U_ActComm] \n");
                varname1.Append("      ,r.[U_HlSQty] \n");
                varname1.Append("      ,r.[U_HaulAC] \n");
                varname1.Append("      ,r.[U_ClgCode] \n");
                varname1.Append("      ,r.[U_ProRef] \n");
                varname1.Append("      ,r.[U_SerialC] \n");
                varname1.Append("      ,r.[U_PAUOMQt] \n");
                varname1.Append("      ,r.[U_PRdWgt] \n");
                varname1.Append("      ,r.[U_ProWgt] \n");
                varname1.Append("      ,r.[U_ProUOM] \n");
                varname1.Append("      ,r.[U_PCost] \n");
                varname1.Append("      ,r.[U_PCTotal] \n");
                varname1.Append("      ,r.[U_WgtDed] \n");
                varname1.Append("      ,r.[U_AdjWgt] \n");
                varname1.Append("      ,r.[U_ValDed] \n");
                varname1.Append("      ,r.[U_BookIn] \n");
                varname1.Append("      ,r.[U_CarrRef] \n");
                varname1.Append("      ,r.[U_PCharge] \n");
                varname1.Append("      ,r.[U_ProGRPO] \n");
                varname1.Append("      ,r.[U_CustGR] \n");
                varname1.Append("      ,r.[U_GRIn] \n");
                varname1.Append("      ,r.[U_LoadSht] \n");
                varname1.Append("      ,r.[U_SODlvNot] \n");
                varname1.Append("      ,r.[U_IDHSEQ] \n");
                varname1.Append("      ,r.[U_IDHCMT] \n");
                varname1.Append("      ,r.[U_IDHDRVI] \n");
                varname1.Append("      ,r.[U_IDHCHK] \n");
                varname1.Append("      ,r.[U_IDHPRTD] \n");
                varname1.Append("      ,r.[U_IDHRTCD] \n");
                varname1.Append("      ,r.[U_IDHRTDT] \n");
                varname1.Append("      ,r.[U_ExpLdWgt] \n");
                varname1.Append("      ,r.[U_IsTrl] \n");
                varname1.Append("      ,r.[U_UseWgt] \n");
                varname1.Append("      ,r.[U_TFSMov] \n");
                varname1.Append("      ,r.[U_PayTrm] \n");
                varname1.Append("      ,r.[U_JStat] \n");
                varname1.Append("      ,r.[U_Jrnl] \n");
                varname1.Append("      ,r.[U_SAddress] \n");
                varname1.Append("      --,r.[U_LckPrc]  \n");
                varname1.Append("      ,r.[U_CBICont] \n");
                varname1.Append("      ,r.[U_CBIManQty] \n");
                varname1.Append("      ,r.[U_CBIManUOM] \n");
                varname1.Append("      ,r.[U_ExtComm1] \n");
                varname1.Append("      ,r.[U_ExtComm2] \n");
                varname1.Append("      ,r.[U_MaximoNum] \n");
                varname1.Append("      ,r.[U_TFSReq] \n");
                varname1.Append("      ,r.[U_TFSNum] \n");
                varname1.Append("      ,r.[U_TRdWgt] \n");
                varname1.Append("      ,r.[U_PrcLink] \n");
                varname1.Append("      ,r.[U_TAUOMQt] \n");
                varname1.Append("      ,r.[U_Rebate] \n");
                varname1.Append("      ,r.[U_TAddChVat] \n");
                varname1.Append("      ,r.[U_TAddCsVat] \n");
                varname1.Append("      ,r.[U_AddCharge] \n");
                varname1.Append("      ,r.[U_AddCost] \n");
                varname1.Append("      ,r.[U_JbToBeDoneDt] \n");
                varname1.Append("      ,r.[U_JbToBeDoneTm] \n");
                varname1.Append("      ,r.[U_DrivrOnSitDt] \n");
                varname1.Append("      ,r.[U_DrivrOnSitTm] \n");
                varname1.Append("      ,r.[U_DrivrOfSitDt] \n");
                varname1.Append("      ,r.[U_DrivrOfSitTm] \n");
                varname1.Append("      ,r.[U_AltWasDsc] \n");
                varname1.Append("      ,r.[U_OTComments] \n");
                varname1.Append("      ,r.[U_TrnCode] \n");
                varname1.Append("      ,r.[U_BPWgt] \n");
                varname1.Append("      ,r.[U_CarWgt] \n");
                varname1.Append("      ,r.[U_PROINV] \n");
                varname1.Append("      ,r.[U_WHTRNF] \n");
                varname1.Append("      ,r.[U_UseBPWgt] \n");
                varname1.Append("      ,r.[U_ProPay] \n");
                varname1.Append("      ,r.[U_CustPay] \n");
                varname1.Append("      ,r.[U_WROrd2] \n");
                varname1.Append("      ,r.[U_WRRow2] \n");
                varname1.Append("      ,r.[U_DispCgCc] \n");
                varname1.Append("      ,r.[U_CarrCgCc] \n");
                varname1.Append("      ,r.[U_PurcCoCc] \n");
                varname1.Append("      ,r.[U_DispCoCc] \n");
                varname1.Append("      ,r.[U_CarrCoCc] \n");
                varname1.Append("      ,r.[U_LiscCoCc] \n");
                varname1.Append("      ,r.[U_SAddrsLN] \n");
                varname1.Append("      ,r.[U_ReqArch] \n");
                varname1.Append("      ,r.[U_Sample] \n");
                varname1.Append("      ,r.[U_MSDS] \n");
                varname1.Append("      ,r.[U_RouteCd] \n");
                varname1.Append("      ,r.[U_SampleRef] \n");
                varname1.Append("      ,r.[U_SampStatus] \n");
                varname1.Append("      ,r.[U_WasFDsc] \n");
                varname1.Append("      ,r.[U_WOHID] \n");
                varname1.Append("      ,r.[U_WORID] \n");
                varname1.Append("      ,r.[U_AddItmID] \n");
                varname1.Append("      ,r.[U_Status] as R_Status \n");
                varname1.Append("      ,r.[U_UOM] \n");
                varname1.Append("	  ,rstatus.U_Desc As RStatusDesc \n");
                varname1.Append("      --,h.[U_User]  \n");
                varname1.Append("      ,h.[U_Status] \n");
                varname1.Append("      ,h.[U_BDate] As HBDate \n");
                varname1.Append("      ,h.[U_BTime] As HBTime \n");
                varname1.Append("      ,h.[U_RSDate] \n");
                varname1.Append("      ,h.[U_RSTime] \n");
                varname1.Append("      ,h.[U_REDate] \n");
                varname1.Append("      ,h.[U_RETime] \n");
                varname1.Append("      ,h.[U_CardCd] \n");
                varname1.Append("      ,h.[U_CardNM] \n");
                varname1.Append("      ,h.[U_Contact] \n");
                varname1.Append("      ,h.[U_Address] \n");
                varname1.Append("      ,h.[U_Street] \n");
                varname1.Append("      ,h.[U_Block] \n");
                varname1.Append("      ,h.[U_City] \n");
                varname1.Append("      ,h.[U_ZpCd] \n");
                varname1.Append("      ,h.[U_Phone1] \n");
                varname1.Append("      ,h.[U_CustRef] as HCustRef \n");
                varname1.Append("      ,h.[U_ItemGrp] \n");
                varname1.Append("      ,h.[U_Qty] \n");
                varname1.Append("      ,h.[U_ORoad] \n");
                varname1.Append("      ,h.[U_SLicSp] as HSLicSp \n");
                varname1.Append("      ,h.[U_SLicNr] As HSLicNr \n");
                varname1.Append("      --,h.[U_SLicExp]  \n");
                varname1.Append("      --,h.[U_SLicCh]  \n");
                varname1.Append("      ,h.[U_SpInst] \n");
                varname1.Append("      ,h.[U_PCardCd] \n");
                varname1.Append("      ,h.[U_PCardNM] \n");
                varname1.Append("      ,h.[U_PContact] \n");
                varname1.Append("      ,h.[U_PAddress] \n");
                varname1.Append("      ,h.[U_PStreet] \n");
                varname1.Append("      ,h.[U_PBlock] \n");
                varname1.Append("      ,h.[U_PCity] \n");
                varname1.Append("      ,h.[U_PZpCd] \n");
                varname1.Append("      ,h.[U_PPhone1] \n");
                varname1.Append("      ,h.[U_SCardCd] \n");
                varname1.Append("      ,h.[U_SCardNM] \n");
                varname1.Append("      ,h.[U_SContact] \n");
                varname1.Append("      ,h.[U_SAddress] as HSAddress \n");
                varname1.Append("      ,h.[U_SStreet] \n");
                varname1.Append("      ,h.[U_SBlock] \n");
                varname1.Append("      ,h.[U_SCity] \n");
                varname1.Append("      ,h.[U_SZpCd] \n");
                varname1.Append("      ,h.[U_SPhone1] \n");
                varname1.Append("      ,h.[U_CCardCd] \n");
                varname1.Append("      ,h.[U_CCardNM] \n");
                varname1.Append("      ,h.[U_CContact] \n");
                varname1.Append("      ,h.[U_CAddress] \n");
                varname1.Append("      ,h.[U_CStreet] \n");
                varname1.Append("      ,h.[U_CBlock] \n");
                varname1.Append("      ,h.[U_CCity] \n");
                varname1.Append("      ,h.[U_CZpCd] \n");
                varname1.Append("      ,h.[U_CPhone1] \n");
                varname1.Append("      ,h.[U_ORDTP] \n");
                varname1.Append("      --,h.[U_SLicNm]  \n");
                varname1.Append("      ,h.[U_RTIMEF] \n");
                varname1.Append("      ,h.[U_RTIMET] as HRTIMET \n");
                varname1.Append("      ,h.[U_SupRef] as HSupRef \n");
                varname1.Append("      ,h.[U_SiteTl] \n");
                varname1.Append("      ,h.[U_CopTRw] \n");
                varname1.Append("      ,h.[U_Route] \n");
                varname1.Append("      ,h.[U_Seq] \n");
                varname1.Append("      ,h.[U_County] \n");
                varname1.Append("      ,h.[U_Origin] HOrigin \n");
                varname1.Append("      ,h.[U_PremCd] \n");
                varname1.Append("      ,h.[U_SiteLic] \n");
                varname1.Append("      ,h.[U_SteId] \n");
                varname1.Append("      ,h.[U_SepOrder] \n");
                varname1.Append("      ,h.[U_ENNO] as HENNO \n");
                varname1.Append("      --,h.[U_DAAttach]  \n");
                varname1.Append("      ,h.[U_ENPApFDt] \n");
                varname1.Append("      ,h.[U_ENPApTDt] \n");
                varname1.Append("      ,h.[U_PBICODE] \n");
                varname1.Append("      --,h.[U_ProRef]  \n");
                varname1.Append("      ,h.[U_SiteRef] as HSiteRef \n");
                varname1.Append("      ,h.[U_FirstBP] \n");
                varname1.Append("      ,h.[U_ForeCS] \n");
                varname1.Append("      ,h.[U_MContact] \n");
                varname1.Append("      ,h.[U_MAddress] \n");
                varname1.Append("      ,h.[U_MStreet] \n");
                varname1.Append("      ,h.[U_MBlock] \n");
                varname1.Append("      ,h.[U_MCity] \n");
                varname1.Append("      ,h.[U_MZpCd] \n");
                varname1.Append("      ,h.[U_MPhone1] \n");
                varname1.Append("      --,h.[U_LckPrc]  \n");
                varname1.Append("      --,h.[U_WasLic]  \n");
                varname1.Append("      --,h.[U_CWasLic]  \n");
                varname1.Append("      ,h.[U_State] \n");
                varname1.Append("      ,h.[U_CState] \n");
                varname1.Append("      ,h.[U_PState] \n");
                varname1.Append("      ,h.[U_SState] --As HStatus  \n");
                varname1.Append("      ,h.[U_MState] \n");
                varname1.Append("      ,h.[U_TRNCd] \n");
                varname1.Append("      ,h.[U_MinJob] \n");
                varname1.Append("      ,h.[U_CusFrnNm] \n");
                varname1.Append("      ,h.[U_AddrssLN] \n");
                varname1.Append("      ,h.[U_CAddrsLN] \n");
                varname1.Append("      ,h.[U_PAddrsLN] \n");
                varname1.Append("      --,h.[U_SAddrsLN]  \n");
                varname1.Append("      ,h.[U_MAddrsLN] \n");
                varname1.Append("      ,h.[U_Country] \n");
                varname1.Append("      ,h.[U_E_Mail] \n");
                varname1.Append("      ,h.[U_Notes] \n");
                varname1.Append("      --,h.[U_ClgCode]  \n");
                varname1.Append("      ,h.[U_Version] \n");
                varname1.Append("      ,h.[U_Branch] As HBranch \n");
                varname1.Append("      ,h.[U_Source] \n");
                varname1.Append("      ,h.[U_LstPrice] as HLstPrice \n");
                varname1.Append("      ,h.[U_Cost] \n");
                varname1.Append("      ,h.[U_LPProfit] \n");
                varname1.Append("      ,h.[U_LPGrossM] \n");
                varname1.Append("      ,h.[U_ChgVAT] \n");
                varname1.Append("      ,h.[U_TCharge] as HTCharge \n");
                varname1.Append("      ,h.[U_NoChgTax] \n");
                varname1.Append("      ,h.[U_ChgPrice] \n");
                varname1.Append("      ,h.[U_CPProfit] \n");
                varname1.Append("      ,h.[U_CPGrossM] \n");
                varname1.Append("      ,h.[U_AppRequired] \n");
                varname1.Append("      ,h.[U_WOHID]  As HWOHID \n");
                varname1.Append("      ,h.[U_ReviewNotes] \n");
                varname1.Append("	  ,hstatus.U_Desc As HStatusDesc \n");
                varname1.Append("	  ,isnull(hssource.U_Desc,'') As HSourceDesc \n");
                varname1.Append("	  ,OUSR.USER_CODE \n");
                varname1.Append("	  ,OUBR.Name As BranchName \n");
                varname1.Append("	  ,HItemGroup.ItmsGrpNam as HItemGrp \n");
                varname1.Append("	  ,RItemGroup.ItmsGrpNam as RItemGrp \n");
                varname1.Append("	   \n");
                varname1.Append("FROM  [dbo].[@IDH_WOQHD] h WITH(NOLOCK) \n");
                varname1.Append("		Left Join OITB HItemGroup With(NoLock) on h.U_ItemGrp=HItemGroup.ItmsGrpCod \n");
                varname1.Append("		Left Join [@IDH_WRSTUSNW] hssource WITH(NOLOCK)  on (h.U_Status= hssource.U_Value And hssource.U_ObjType = 108 ) \n");
                varname1.Append("		,[dbo].[@IDH_WOQITEM] r WITH(NOLOCK) \n");
                varname1.Append("		Left Join OITB RItemGroup With(NoLock) on r.[U_ItmGrp]=RItemGroup.ItmsGrpCod \n");
                varname1.Append("		,[@IDH_WRSTUSNW] rstatus WITH(NOLOCK) \n");
                varname1.Append("	,[@IDH_WRSTUSNW] hstatus WITH(NOLOCK) \n");
                varname1.Append("	,OUSR with(nolock) \n");
                varname1.Append("	,OUBR With(Nolock) \n");
                varname1.Append("Where  ( h.Code = r.U_JobNr ) \n");
                varname1.Append("	And rstatus.U_ObjType = 107 \n");
                varname1.Append("	AND rstatus.U_Value = r.U_Status \n");
                varname1.Append("	And hstatus.U_ObjType = 106 \n");
                varname1.Append("	AND hstatus.U_Value = h.U_Status \n");
                varname1.Append("	And OUSR.USERID=h.U_User \n");
                varname1.Append("	And OUBR.Code=h.U_Branch \n");
                varname1.Append("Union All \n");
                varname1.Append("SELECT \n");
                varname1.Append("	  '' Code \n");
                varname1.Append("	  ,'' [U_EnqID] \n");
                varname1.Append("      ,'' [U_EnqLID] \n");
                varname1.Append("      ,'' [U_AdtnlItm] \n");
                varname1.Append("      ,'' [U_PLineID] \n");
                varname1.Append("      ,'' [U_WstGpCd] \n");
                varname1.Append("      ,'' [U_WstGpNm] \n");
                varname1.Append("      ,0 [U_LstPrice] \n");
                varname1.Append("      ,0 [U_MnMargin] \n");
                varname1.Append("      ,0 [U_GMargin] \n");
                varname1.Append("      ,0 [U_Sort] \n");
                varname1.Append("      ,h.code [U_JobNr] \n");
                varname1.Append("      ,'' [U_JobTp] \n");
                varname1.Append("      ,null  [U_RDate] \n");
                varname1.Append("      ,null [U_ASDate] \n");
                varname1.Append("      ,null [U_ASTime] \n");
                varname1.Append("      ,null [U_AEDate] \n");
                varname1.Append("      ,'' [U_AETime] \n");
                varname1.Append("      ,'' [U_VehTyp] \n");
                varname1.Append("      ,'' [U_Lorry] \n");
                varname1.Append("      ,'' [U_Driver] \n");
                varname1.Append("      ,'' [U_DocNum] \n");
                varname1.Append("      ,'' [U_SupRef] \n");
                varname1.Append("      ,0 [U_TipWgt] \n");
                varname1.Append("      ,0 [U_TipCost] \n");
                varname1.Append("      ,0 [U_TipTot] \n");
                varname1.Append("      ,0 [U_CstWgt] \n");
                varname1.Append("      ,0 [U_TCharge] \n");
                varname1.Append("      ,0 [U_TCTotal] \n");
                varname1.Append("      ,0 [U_CongCh] \n");
                varname1.Append("      ,0 [U_Weight] \n");
                varname1.Append("      ,0 [U_Price] \n");
                varname1.Append("      ,0 [U_Discnt] \n");
                varname1.Append("      ,0 [U_DisAmt] \n");
                varname1.Append("      ,0 [U_TaxAmt] \n");
                varname1.Append("      ,0 [U_Total] \n");
                varname1.Append("      ,'' [U_SLicNr] \n");
                varname1.Append("      ,0 [U_SLicExp] \n");
                varname1.Append("      ,0 [U_SLicCh] \n");
                varname1.Append("      ,'' [U_ItmGrp] \n");
                varname1.Append("      ,'' [U_ItemCd] \n");
                varname1.Append("      ,'' [U_ItemDsc] \n");
                varname1.Append("      ,'' [U_WasCd] \n");
                varname1.Append("      ,'' [U_WasDsc] \n");
                varname1.Append("      ,'' [U_Serial] \n");
                varname1.Append("      ,0 [U_Quantity] \n");
                varname1.Append("      ,0 [U_JCost] \n");
                varname1.Append("      ,'' [U_SAINV] \n");
                varname1.Append("      ,'' [U_SAORD] \n");
                varname1.Append("      ,'' [U_TIPPO] \n");
                varname1.Append("      ,'' [U_JOBPO] \n");
                varname1.Append("      ,''  As RSLicSp \n");
                varname1.Append("      ,'' [U_Tip] \n");
                varname1.Append("      ,'' [U_CustCd] \n");
                varname1.Append("      ,'' [U_CarrCd] \n");
                varname1.Append("      ,'' [U_CongCd] \n");
                varname1.Append("      ,'' [U_TipNm] \n");
                varname1.Append("      ,'' [U_CustNm] \n");
                varname1.Append("      ,'' [U_CarrNm] \n");
                varname1.Append("      ,'' [U_SLicNm] \n");
                varname1.Append("      ,'' [U_SCngNm] \n");
                varname1.Append("      ,0 [U_Dista] \n");
                varname1.Append("      ,'' [U_CntrNo] \n");
                varname1.Append("      ,0 [U_Wei1] \n");
                varname1.Append("      ,0 [U_Wei2] \n");
                varname1.Append("      ,'' [U_Ser1] \n");
                varname1.Append("      ,'' [U_Ser2] \n");
                varname1.Append("      ,'' [U_WDt1] \n");
                varname1.Append("      ,'' [U_WDt2] \n");
                varname1.Append("      ,0 [U_AddEx] \n");
                varname1.Append("      ,'' [U_PStat] \n");
                varname1.Append("      ,0 [U_OrdWgt] \n");
                varname1.Append("      ,0 [U_OrdCost] \n");
                varname1.Append("      ,0 [U_OrdTot] \n");
                varname1.Append("      ,'' [U_TRLReg] \n");
                varname1.Append("      ,'' [U_TRLNM] \n");
                varname1.Append("      --,r.[U_WASLIC]  \n");
                varname1.Append("      ,'' [U_SLPO] \n");
                varname1.Append("      ,0 [U_SLicCst] \n");
                varname1.Append("      ,0 [U_SLicCTo] \n");
                varname1.Append("      ,0 [U_SLicCQt] \n");
                varname1.Append("      ,'' [U_WROrd] \n");
                varname1.Append("      ,'' [U_WRRow] \n");
                varname1.Append("      ,null [U_RTime] \n");
                varname1.Append("      ,null [U_RTimeT] \n");
                varname1.Append("      ,0 [U_CusQty] \n");
                varname1.Append("      ,'0' [U_CusChr] \n");
                varname1.Append("      ,'' [U_PayMeth] \n");
                varname1.Append("      ,'' as RCustRef \n");
                varname1.Append("      ,0 [U_RdWgt] \n");
                varname1.Append("      ,'' [U_LorryCd] \n");
                varname1.Append("      ,'' [U_Covera] \n");
                varname1.Append("      ,'' [U_Comment] \n");
                varname1.Append("      ,'' [U_PUOM] \n");
                varname1.Append("      ,'' [U_AUOM] \n");
                varname1.Append("      ,0 [U_AUOMQt] \n");
                varname1.Append("      ,'' [U_CCNum] \n");
                varname1.Append("      ,'' [U_CCStat] \n");
                varname1.Append("      ,'' [U_PayStat] \n");
                varname1.Append("      ,'' [U_CCType] \n");
                varname1.Append("      ,'' [U_PARCPT] \n");
                varname1.Append("      ,'' [U_ENREF] \n");
                varname1.Append("      ,null [U_JobRmD] \n");
                varname1.Append("      ,null [U_JobRmT] \n");
                varname1.Append("      ,null [U_RemNot] \n");
                varname1.Append("      ,null [U_RemCnt] \n");
                varname1.Append("      ,null [U_TZone] \n");
                varname1.Append("      ,null [U_CoverHst] \n");
                varname1.Append("      ,null [U_ProPO] \n");
                varname1.Append("      ,'' [U_ProCd] \n");
                varname1.Append("      ,'' [U_ProNm] \n");
                varname1.Append("      --,'' [U_User]  \n");
                varname1.Append("      ,'' [U_Branch] \n");
                varname1.Append("      ,0 [U_TAddChrg] \n");
                varname1.Append("      ,0 [U_TAddCost] \n");
                varname1.Append("      ,0 [U_IssQty] \n");
                varname1.Append("      ,'' [U_Obligated] \n");
                varname1.Append("      ,'' [U_Origin] \n");
                varname1.Append("      ,0 [U_TChrgVtRt] \n");
                varname1.Append("      ,0 [U_HChrgVtRt] \n");
                varname1.Append("      ,0 [U_TCostVtRt] \n");
                varname1.Append("      ,0 [U_HCostVtRt] \n");
                varname1.Append("      ,'' [U_TChrgVtGrp] \n");
                varname1.Append("      ,'' [U_HChrgVtGrp] \n");
                varname1.Append("      ,'' [U_TCostVtGrp] \n");
                varname1.Append("      ,'' [U_HCostVtGrp] \n");
                varname1.Append("      ,0 [U_VtCostAmt] \n");
                varname1.Append("      ,'' [U_CustCs] \n");
                varname1.Append("      ,'' [U_ConNum] \n");
                varname1.Append("      ,'' [U_CCExp] \n");
                varname1.Append("      ,null [U_CCIs] \n");
                varname1.Append("      ,null [U_CCSec] \n");
                varname1.Append("      ,null [U_CCHNum] \n");
                varname1.Append("      ,'' [U_CCPCd] \n");
                varname1.Append("      ,'' [U_IntComm] \n");
                varname1.Append("      ,'' [U_ContNr] \n");
                varname1.Append("      ,'' [U_SealNr] \n");
                varname1.Append("      ,'' [U_FPCoH] \n");
                varname1.Append("      ,'' [U_FPCoT] \n");
                varname1.Append("      ,'' [U_FPChH] \n");
                varname1.Append("      ,'' [U_FPChT] \n");
                varname1.Append("      ,'' [U_WastTNN] \n");
                varname1.Append("      ,'' [U_HazWCNN] \n");
                varname1.Append("      ,'' [U_SiteRef] \n");
                varname1.Append("      ,'' [U_ExtWeig] \n");
                varname1.Append("      ,null   As RBDAte \n");
                varname1.Append("      ,null   As RBTime \n");
                varname1.Append("      ,null [U_CarrReb] \n");
                varname1.Append("      ,null [U_TPCN] \n");
                varname1.Append("      ,null [U_MANPRC] \n");
                varname1.Append("      ,null [U_ENNO] \n");
                varname1.Append("      ,null [U_SemSolid] \n");
                varname1.Append("      ,null [U_PaMuPu] \n");
                varname1.Append("      ,null [U_Dusty] \n");
                varname1.Append("      ,null [U_Fluid] \n");
                varname1.Append("      ,null [U_Firm] \n");
                varname1.Append("      ,null [U_Consiste] \n");
                varname1.Append("      ,null [U_TypPTreat] \n");
                varname1.Append("      ,null [U_DAAttach] \n");
                varname1.Append("      ,null [U_LnkPBI] \n");
                varname1.Append("      ,null [U_DPRRef] \n");
                varname1.Append("      ,null [U_MDChngd] \n");
                varname1.Append("      ,null [U_Sched] \n");
                varname1.Append("      ,null [U_USched] \n");
                varname1.Append("      ,null [U_CustReb] \n");
                varname1.Append("      ,null [U_TCCN] \n");
                varname1.Append("      ,null [U_RowSta] \n");
                varname1.Append("      ,null [U_ActComm] \n");
                varname1.Append("      ,null [U_HlSQty] \n");
                varname1.Append("      ,null [U_HaulAC] \n");
                varname1.Append("      ,null [U_ClgCode] \n");
                varname1.Append("      ,null [U_ProRef] \n");
                varname1.Append("      ,null [U_SerialC] \n");
                varname1.Append("      ,null [U_PAUOMQt] \n");
                varname1.Append("      ,null [U_PRdWgt] \n");
                varname1.Append("      ,null [U_ProWgt] \n");
                varname1.Append("      ,null [U_ProUOM] \n");
                varname1.Append("      ,null [U_PCost] \n");
                varname1.Append("      ,null [U_PCTotal] \n");
                varname1.Append("      ,null [U_WgtDed] \n");
                varname1.Append("      ,null [U_AdjWgt] \n");
                varname1.Append("      ,null [U_ValDed] \n");
                varname1.Append("      ,null [U_BookIn] \n");
                varname1.Append("      ,null [U_CarrRef] \n");
                varname1.Append("      ,null [U_PCharge] \n");
                varname1.Append("      ,null [U_ProGRPO] \n");
                varname1.Append("      ,null [U_CustGR] \n");
                varname1.Append("      ,null [U_GRIn] \n");
                varname1.Append("      ,null [U_LoadSht] \n");
                varname1.Append("      ,null [U_SODlvNot] \n");
                varname1.Append("      ,null [U_IDHSEQ] \n");
                varname1.Append("      ,null [U_IDHCMT] \n");
                varname1.Append("      ,null [U_IDHDRVI] \n");
                varname1.Append("      ,null [U_IDHCHK] \n");
                varname1.Append("      ,null [U_IDHPRTD] \n");
                varname1.Append("      ,null [U_IDHRTCD] \n");
                varname1.Append("      ,null [U_IDHRTDT] \n");
                varname1.Append("      ,null [U_ExpLdWgt] \n");
                varname1.Append("      ,null [U_IsTrl] \n");
                varname1.Append("      ,null [U_UseWgt] \n");
                varname1.Append("      ,null [U_TFSMov] \n");
                varname1.Append("      ,'' [U_PayTrm] \n");
                varname1.Append("      ,'' [U_JStat] \n");
                varname1.Append("      ,'' [U_Jrnl] \n");
                varname1.Append("      ,'' [U_SAddress] \n");
                varname1.Append("      --,'' [U_LckPrc]  \n");
                varname1.Append("      ,'' [U_CBICont] \n");
                varname1.Append("      ,0 [U_CBIManQty] \n");
                varname1.Append("      ,'' [U_CBIManUOM] \n");
                varname1.Append("      ,'' [U_ExtComm1] \n");
                varname1.Append("      ,'' [U_ExtComm2] \n");
                varname1.Append("      ,'' [U_MaximoNum] \n");
                varname1.Append("      ,'' [U_TFSReq] \n");
                varname1.Append("      ,'' [U_TFSNum] \n");
                varname1.Append("      ,0 [U_TRdWgt] \n");
                varname1.Append("      ,'' [U_PrcLink] \n");
                varname1.Append("      ,0 [U_TAUOMQt] \n");
                varname1.Append("      ,'' [U_Rebate] \n");
                varname1.Append("      ,0 [U_TAddChVat] \n");
                varname1.Append("      ,0 [U_TAddCsVat] \n");
                varname1.Append("      ,0 [U_AddCharge] \n");
                varname1.Append("      ,0 [U_AddCost] \n");
                varname1.Append("      ,NULL [U_JbToBeDoneDt] \n");
                varname1.Append("      ,NULL [U_JbToBeDoneTm] \n");
                varname1.Append("      ,NULL [U_DrivrOnSitDt] \n");
                varname1.Append("      ,NULL [U_DrivrOnSitTm] \n");
                varname1.Append("      ,NULL [U_DrivrOfSitDt] \n");
                varname1.Append("      ,NULL [U_DrivrOfSitTm] \n");
                varname1.Append("      ,'' [U_AltWasDsc] \n");
                varname1.Append("      ,'' [U_OTComments] \n");
                varname1.Append("      ,'' [U_TrnCode] \n");
                varname1.Append("      ,0 [U_BPWgt] \n");
                varname1.Append("      ,0 [U_CarWgt] \n");
                varname1.Append("      ,'' [U_PROINV] \n");
                varname1.Append("      ,'' [U_WHTRNF] \n");
                varname1.Append("      ,'' [U_UseBPWgt] \n");
                varname1.Append("      ,'' [U_ProPay] \n");
                varname1.Append("      ,'' [U_CustPay] \n");
                varname1.Append("      ,'' [U_WROrd2] \n");
                varname1.Append("      ,'' [U_WRRow2] \n");
                varname1.Append("      ,'' [U_DispCgCc] \n");
                varname1.Append("      ,'' [U_CarrCgCc] \n");
                varname1.Append("      ,'' [U_PurcCoCc] \n");
                varname1.Append("      ,'' [U_DispCoCc] \n");
                varname1.Append("      ,'' [U_CarrCoCc] \n");
                varname1.Append("      ,'' [U_LiscCoCc] \n");
                varname1.Append("      ,'' [U_SAddrsLN] \n");
                varname1.Append("      ,'' [U_ReqArch] \n");
                varname1.Append("      ,'' [U_Sample] \n");
                varname1.Append("      ,'' [U_MSDS] \n");
                varname1.Append("      ,'' [U_RouteCd] \n");
                varname1.Append("      ,'' [U_SampleRef] \n");
                varname1.Append("      ,'' [U_SampStatus] \n");
                varname1.Append("      ,'' [U_WasFDsc] \n");
                varname1.Append("      ,'' [U_WOHID] \n");
                varname1.Append("      ,'' [U_WORID] \n");
                varname1.Append("      ,'' [U_AddItmID] \n");
                varname1.Append("      ,'' as R_Status \n");
                varname1.Append("      ,'' [U_UOM] \n");
                varname1.Append("	  ,'' As RStatusDesc \n");
                varname1.Append("      --,h.[U_User]  \n");
                varname1.Append("      ,h.[U_Status] \n");
                varname1.Append("      ,h.[U_BDate] As HBDate \n");
                varname1.Append("      ,h.[U_BTime] As HBTime \n");
                varname1.Append("      ,h.[U_RSDate] \n");
                varname1.Append("      ,h.[U_RSTime] \n");
                varname1.Append("      ,h.[U_REDate] \n");
                varname1.Append("      ,h.[U_RETime] \n");
                varname1.Append("      ,h.[U_CardCd] \n");
                varname1.Append("      ,h.[U_CardNM] \n");
                varname1.Append("      ,h.[U_Contact] \n");
                varname1.Append("      ,h.[U_Address] \n");
                varname1.Append("      ,h.[U_Street] \n");
                varname1.Append("      ,h.[U_Block] \n");
                varname1.Append("      ,h.[U_City] \n");
                varname1.Append("      ,h.[U_ZpCd] \n");
                varname1.Append("      ,h.[U_Phone1] \n");
                varname1.Append("      ,h.[U_CustRef] as HCustRef \n");
                varname1.Append("      ,h.[U_ItemGrp] \n");
                varname1.Append("      ,h.[U_Qty] \n");
                varname1.Append("      ,h.[U_ORoad] \n");
                varname1.Append("      ,h.[U_SLicSp] as HSLicSp \n");
                varname1.Append("      ,h.[U_SLicNr] As HSLicNr \n");
                varname1.Append("      --,h.[U_SLicExp]  \n");
                varname1.Append("      --,h.[U_SLicCh]  \n");
                varname1.Append("      ,h.[U_SpInst] \n");
                varname1.Append("      ,h.[U_PCardCd] \n");
                varname1.Append("      ,h.[U_PCardNM] \n");
                varname1.Append("      ,h.[U_PContact] \n");
                varname1.Append("      ,h.[U_PAddress] \n");
                varname1.Append("      ,h.[U_PStreet] \n");
                varname1.Append("      ,h.[U_PBlock] \n");
                varname1.Append("      ,h.[U_PCity] \n");
                varname1.Append("      ,h.[U_PZpCd] \n");
                varname1.Append("      ,h.[U_PPhone1] \n");
                varname1.Append("      ,h.[U_SCardCd] \n");
                varname1.Append("      ,h.[U_SCardNM] \n");
                varname1.Append("      ,h.[U_SContact] \n");
                varname1.Append("      ,h.[U_SAddress] as HSAddress \n");
                varname1.Append("      ,h.[U_SStreet] \n");
                varname1.Append("      ,h.[U_SBlock] \n");
                varname1.Append("      ,h.[U_SCity] \n");
                varname1.Append("      ,h.[U_SZpCd] \n");
                varname1.Append("      ,h.[U_SPhone1] \n");
                varname1.Append("      ,h.[U_CCardCd] \n");
                varname1.Append("      ,h.[U_CCardNM] \n");
                varname1.Append("      ,h.[U_CContact] \n");
                varname1.Append("      ,h.[U_CAddress] \n");
                varname1.Append("      ,h.[U_CStreet] \n");
                varname1.Append("      ,h.[U_CBlock] \n");
                varname1.Append("      ,h.[U_CCity] \n");
                varname1.Append("      ,h.[U_CZpCd] \n");
                varname1.Append("      ,h.[U_CPhone1] \n");
                varname1.Append("      ,h.[U_ORDTP] \n");
                varname1.Append("      --,h.[U_SLicNm]  \n");
                varname1.Append("      ,h.[U_RTIMEF] \n");
                varname1.Append("      ,h.[U_RTIMET] as HRTIMET \n");
                varname1.Append("      ,h.[U_SupRef] as HSupRef \n");
                varname1.Append("      ,h.[U_SiteTl] \n");
                varname1.Append("      ,h.[U_CopTRw] \n");
                varname1.Append("      ,h.[U_Route] \n");
                varname1.Append("      ,h.[U_Seq] \n");
                varname1.Append("      ,h.[U_County] \n");
                varname1.Append("      ,h.[U_Origin] HOrigin \n");
                varname1.Append("      ,h.[U_PremCd] \n");
                varname1.Append("      ,h.[U_SiteLic] \n");
                varname1.Append("      ,h.[U_SteId] \n");
                varname1.Append("      ,h.[U_SepOrder] \n");
                varname1.Append("      ,h.[U_ENNO] as HENNO \n");
                varname1.Append("      --,h.[U_DAAttach]  \n");
                varname1.Append("      ,h.[U_ENPApFDt] \n");
                varname1.Append("      ,h.[U_ENPApTDt] \n");
                varname1.Append("      ,h.[U_PBICODE] \n");
                varname1.Append("      --,h.[U_ProRef]  \n");
                varname1.Append("      ,h.[U_SiteRef] as HSiteRef \n");
                varname1.Append("      ,h.[U_FirstBP] \n");
                varname1.Append("      ,h.[U_ForeCS] \n");
                varname1.Append("      ,h.[U_MContact] \n");
                varname1.Append("      ,h.[U_MAddress] \n");
                varname1.Append("      ,h.[U_MStreet] \n");
                varname1.Append("      ,h.[U_MBlock] \n");
                varname1.Append("      ,h.[U_MCity] \n");
                varname1.Append("      ,h.[U_MZpCd] \n");
                varname1.Append("      ,h.[U_MPhone1] \n");
                varname1.Append("      --,h.[U_LckPrc]  \n");
                varname1.Append("      --,h.[U_WasLic]  \n");
                varname1.Append("      --,h.[U_CWasLic]  \n");
                varname1.Append("      ,h.[U_State] \n");
                varname1.Append("      ,h.[U_CState] \n");
                varname1.Append("      ,h.[U_PState] \n");
                varname1.Append("      ,h.[U_SState] --As HStatus  \n");
                varname1.Append("      ,h.[U_MState] \n");
                varname1.Append("      ,h.[U_TRNCd] \n");
                varname1.Append("      ,h.[U_MinJob] \n");
                varname1.Append("      ,h.[U_CusFrnNm] \n");
                varname1.Append("      ,h.[U_AddrssLN] \n");
                varname1.Append("      ,h.[U_CAddrsLN] \n");
                varname1.Append("      ,h.[U_PAddrsLN] \n");
                varname1.Append("      --,h.[U_SAddrsLN]  \n");
                varname1.Append("      ,h.[U_MAddrsLN] \n");
                varname1.Append("      ,h.[U_Country] \n");
                varname1.Append("      ,h.[U_E_Mail] \n");
                varname1.Append("      ,h.[U_Notes] \n");
                varname1.Append("      --,h.[U_ClgCode]  \n");
                varname1.Append("      ,h.[U_Version] \n");
                varname1.Append("      ,h.[U_Branch] As HBranch \n");
                varname1.Append("      ,h.[U_Source] \n");
                varname1.Append("      ,h.[U_LstPrice] as HLstPrice \n");
                varname1.Append("      ,h.[U_Cost] \n");
                varname1.Append("      ,h.[U_LPProfit] \n");
                varname1.Append("      ,h.[U_LPGrossM] \n");
                varname1.Append("      ,h.[U_ChgVAT] \n");
                varname1.Append("      ,h.[U_TCharge] as HTCharge \n");
                varname1.Append("      ,h.[U_NoChgTax] \n");
                varname1.Append("      ,h.[U_ChgPrice] \n");
                varname1.Append("      ,h.[U_CPProfit] \n");
                varname1.Append("      ,h.[U_CPGrossM] \n");
                varname1.Append("      ,h.[U_AppRequired] \n");
                varname1.Append("      ,h.[U_WOHID]  As HWOHID \n");
                varname1.Append("      ,h.[U_ReviewNotes] \n");
                varname1.Append("	  ,hstatus.U_Desc As HStatusDesc \n");
                varname1.Append("	  ,isnull(hssource.U_Desc,'') As HSourceDesc \n");
                varname1.Append("	  ,OUSR.USER_CODE \n");
                varname1.Append("	  ,OUBR.Name As BranchName \n");
                varname1.Append("	  ,HItemGroup.ItmsGrpNam HItemGrp \n");
                varname1.Append("	  ,'' RItemGrp \n");
                varname1.Append("	  --into #T1  \n");
                varname1.Append("FROM [dbo].[@IDH_WOQHD] h WITH (NOLOCK) \n");
                varname1.Append("		Left Join OITB HItemGroup With(NoLock) on h.U_ItemGrp=HItemGroup.ItmsGrpCod \n");
                varname1.Append("		Left Join [@IDH_WRSTUSNW] hssource WITH(NOLOCK)  on (h.U_Status= hssource.U_Value And hssource.U_ObjType = 108 ) \n");
                varname1.Append("	,[@IDH_WRSTUSNW] hstatus WITH (NOLOCK) \n");
                varname1.Append("	,OUSR with(nolock) \n");
                varname1.Append("	,OUBR With(nolock) \n");
                varname1.Append("Where \n");
                varname1.Append("	hstatus.U_ObjType = 106 \n");
                varname1.Append("	AND hstatus.U_Value = h.U_Status \n");
                varname1.Append("	And OUSR.USERID=h.U_User \n");
                varname1.Append("	And h.code not in(Select isnull(r.U_JobNr,'0') from [@IDH_WOQITEM] r with(nolock)) \n");
                varname1.Append("	And OUBR.Code=h.U_Branch");

                oBase.doUpdateQuery(varname1.ToString());
            }
            if (CurrentReleaseCount < 1063 || FORCECREATE) {
                StringBuilder varname1 = new StringBuilder();
                varname1.Append("CREATE VIEW dbo.[IDH_GetFinalBatchAvailableQty] \n");
                varname1.Append("As \n");
                //varname1.Append("( \n");
                varname1.Append(" \n");
                varname1.Append("Select fb2.Code, Sum(OIBT.Quantity) As TotalAvailableQty  \n");
                varname1.Append(" from [@IDH_PVHBTH] fb2 with(nolock) \n");
                //varname1.Append(" --Inner Join [@IDH_JOBSHD] r2 \n");
                //varname1.Append("-- on r.U_LABFBTCD=fb2.Code \n");
                varname1.Append(" Inner Join OWOR  with(nolock) on OWOR.U_PRVHCD=fb2.Code \n");
                varname1.Append(" Inner Join IGN1  with(nolock) on OWOR.DocEntry=IGN1.BaseEntry AND IGN1.BaseType=202 \n");
                varname1.Append(" Inner Join OIGN  with(nolock) on OIGN.DocEntry=IGN1.DocEntry AND IGN1.ObjType=59 And OIGN.DocType='I' \n");
                varname1.Append(" Inner Join dbo.OIBT  with(nolock) on OIBT.BaseEntry=OIGN.DocEntry and OIBT.BaseType=59 \n");
                varname1.Append(" where \n");

                varname1.Append(" OIBT.Status=0 \n");
                //varname1.Append(" --And r.U_CarWgt>0 \n");
                //varname1.Append(" --And r.U_RowSta<>'Deleted' \n");
                //varname1.Append(" And OIBT.Quantity>0 \n");
                varname1.Append("group by OIBT.ItemCode,fb2.Code \n");
                varname1.Append(" Union Select '' , 0 ");
                oBase.doUpdateQuery(varname1.ToString());
            }

        }

        //#region "Contract Tables"
        //private void doAddContractTable(double CurrentReleaseCount, IDHAddOns.idh.data.Base oBase) {
        //    string sTableName = "@IDH_CONTRACT";
        //    if (CurrentReleaseCount < 1081 || FORCECREATE) {

        //        if (oBase.doMDCreateTable(sTableName, "Contract Header")) {
        //            oBase.doAddNumberSeq("SEQCONTH", "Service Header", 0);
                  
        //            oBase.doMDCreateField(sTableName, "CardCd", "Call ID", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
        //            oBase.doMDCreateField(sTableName, "CardNm", "Service Call ID", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);

        //            oBase.doMDCreateField(sTableName, "CName", "Name", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);

        //            oBase.doMDCreateField(sTableName, "SDate", "Start Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
        //            oBase.doMDCreateField(sTableName, "EDate", "End Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);
        //            oBase.doMDCreateField(sTableName, "CDate", "Create Date", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 11);


        //            oBase.doMDCreateField(sTableName, "BAddress", "Bill Address ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
        //            oBase.doMDUpdateField(sTableName, "BAddrssLN", "Bill Address LineNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
        //            oBase.doMDCreateField(sTableName, "BStreet", "Bill Street", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
        //            oBase.doMDCreateField(sTableName, "BBlock", "Bill Block", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
        //            oBase.doMDCreateField(sTableName, "BCity", "Bill City", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
        //            oBase.doMDCreateField(sTableName, "BCounty", "Bill County", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
        //            oBase.doMDCreateField(sTableName, "BState", "Bill State", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);
        //            oBase.doMDCreateField(sTableName, "BZipCode", "Bill Post Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
        //            oBase.doMDCreateField(sTableName, "BCountry", "Bill Country", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);

        //            oBase.doMDCreateField(sTableName, "SAddress", "Ship Address ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
        //            oBase.doMDUpdateField(sTableName, "SAddrssLN", "Ship Address LineNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
        //            oBase.doMDCreateField(sTableName, "SStreet", "Ship Street", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
        //            oBase.doMDCreateField(sTableName, "SBlock", "Ship Block", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
        //            oBase.doMDCreateField(sTableName, "SCity", "Ship City", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
        //            oBase.doMDCreateField(sTableName, "SCounty", "Ship County", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
        //            oBase.doMDCreateField(sTableName, "SState", "Ship State", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);
        //            oBase.doMDCreateField(sTableName, "SZipCode", "Ship Post Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
        //            oBase.doMDCreateField(sTableName, "SCountry", "Ship Country", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);

        //            oBase.doMDCreateField(sTableName, "TAddress", "Tip Address ID", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
        //            oBase.doMDUpdateField(sTableName, "TAddrssLN", "Tip Address LineNum", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 10);
        //            oBase.doMDCreateField(sTableName, "TStreet", "Tip Street", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
        //            oBase.doMDCreateField(sTableName, "TBlock", "Tip Block", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
        //            oBase.doMDCreateField(sTableName, "TCity", "Tip City", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
        //            oBase.doMDCreateField(sTableName, "TCounty", "Tip County", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
        //            oBase.doMDCreateField(sTableName, "TState", "Tip State", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);
        //            oBase.doMDCreateField(sTableName, "TZipCode", "Tip Post Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
        //            oBase.doMDCreateField(sTableName, "TCountry", "Tip Country", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 3);


        //            oBase.doMDCreateField(sTableName, "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
        //            oBase.doMDCreateField(sTableName, "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11);
        //            oBase.doMDCreateField(sTableName, "AsignTo", "Assigned To", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 155);
        //            oBase.doMDCreateField(sTableName, "User", "Created By", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 155);
        //            oBase.doMDCreateField(sTableName, "Branch", "Branch", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 40);
        //            oBase.doMDCreateField(sTableName, "Route", "Route", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);

        //        }
        //    }
        //    if (CurrentReleaseCount < 1079 || FORCECREATE) {

        //        oBase.doMDUpdateField(sTableName, "IDHRT", "Routable Checkbox", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1);
        //        oBase.doMDUpdateField(sTableName, "IDHRCDM", "Route Code Monday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
        //        oBase.doMDUpdateField(sTableName, "IDHRCDTU", "Route Code Tuesday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
        //        oBase.doMDUpdateField(sTableName, "IDHRCDW", "Route Code Wednesday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
        //        oBase.doMDUpdateField(sTableName, "IDHRCDTH", "Route Code Thursday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
        //        oBase.doMDUpdateField(sTableName, "IDHRCDF", "Route Code Friday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
        //        oBase.doMDUpdateField(sTableName, "IDHRCDSA", "Route Code Saturday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
        //        oBase.doMDUpdateField(sTableName, "IDHRCDSU", "Route Code Sunday", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);

        //        oBase.doMDCreateField(sTableName, "SuppRef", "Supplier Referenc", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100);
        //        oBase.doMDCreateField(sTableName, "SupAddr", "Supplier Address", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50);
        //        oBase.doMDCreateField(sTableName, "SupAdrCd", "Supplier Address Code", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);
        //        oBase.doMDCreateField(sTableName, "SupPCode", "Supplier Postcode", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 20);

        //        oBase.doMDUpdateField(sTableName, "Qty", "Actual Qty", SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 11);
        //        oBase.doMDRemoveField(sTableName, "HlgQty");
        //    }
        //}

        //#endregion

        #endregion
    }
}
