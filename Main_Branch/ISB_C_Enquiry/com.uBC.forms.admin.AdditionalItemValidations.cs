﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using IDHAddOns.idh.controls;
using com.idh.bridge.data;
using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry.admin {
    //Inherits 
    //msKeyGen="WGPVALDS"
    public class AdditionalItemValidations : IDHAddOns.idh.forms.Base {
        //protected bool  = false;
        protected string msKeyGen = "WGADITVL";
        public AdditionalItemValidations(IDHAddOns.idh.addon.Base oParent, string sParMenu, int iMenuPosition)
            : base(oParent, "IDHADITMVLD", sParMenu, iMenuPosition, "Enq_Additional Items Group Listing.srf", true, true, false, "Additional Items Group Listing", load_Types.idh_LOAD_NORMAL) {

        }
        protected string getTable() {
            return "@" + getUserTable();
        }

        protected string getUserTable() {
            return "IDH_ADITVLD";
        }

        protected override void doSetEventFilters() {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);

        }
        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            try {
                oForm.Title = gsTitle;

                SAPbouiCOM.Item oItem = default(SAPbouiCOM.Item);
                oItem = oForm.Items.Item("IDH_CONTA");
                UpdateGrid oGridN = new UpdateGrid(this, oForm, "LINESGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane);
                oGridN.getSBOItem().AffectsFormMode = false;
                oForm.Items.Item("IDH_WSTGRP").AffectsFormMode = false;
                doAddUF(oForm, "IDH_SRMOD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, true, false);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
                BubbleEvent = false;
            }
        }

        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCloseForm(oForm, ref BubbleEvent);
            FilterGrid.doRemoveGrid(oForm, "LINESGRID");
        }

        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {
            doFillCombos(oForm);
            setUFValue(oForm, "IDH_SRMOD", "N");
            //Object s = getUFValue(oForm, "IDH_SRMOD",true);
            doReadInputParams(oForm);
            if (getUFValue(oForm, "IDH_SRMOD").ToString() == "Y") {
                oForm.SupportedModes = 1;
                oForm.AutoManaged = false;

                FilterGrid oGridN = (FilterGrid)FilterGrid.getInstance(oForm, "LINESGRID");
                if (oGridN == null) {
                    oGridN = new FilterGrid(this, oForm, "LINESGRID", true);
                }

                doSetFilterFields(oGridN);
                doSetListFields(oGridN);

                oGridN.doAddGridTable(new GridTable(getTable(), null, "Code", false), true);
                oGridN.AddEditLine = false;
                oGridN.doSetDeleteActive(false);
                oGridN.doSetDoCount(true);
                //oGridN.doSetHistory(getUserTable(), "Code");
                oForm.Items.Item("LINESGRID").AffectsFormMode = false;
                oForm.Items.Item("1").AffectsFormMode = false;


            } else {
                UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
                if (oGridN == null) {
                    oGridN = new UpdateGrid(this, oForm, "LINESGRID");
                }

                doSetFilterFields(oGridN);
                doSetListFields(oGridN);

                oGridN.doAddGridTable(new GridTable(getTable(), null, "Code", true), true);
                oGridN.AddEditLine = true;
                oGridN.doSetDeleteActive(true);
                oGridN.doSetDoCount(true);
                oGridN.doSetHistory(getTable(), "Code");
            }
        }


        private void doFillCombos(SAPbouiCOM.Form oForm) {

            doFillCombo(oForm, "IDH_WSTGRP", "[@ISB_WASTEGRP]", "U_WstGpCd", "U_WstGpNm", null, "U_WstGpCd", "Select", "0");

            SAPbouiCOM.ComboBox oCombo = default(SAPbouiCOM.ComboBox);
            oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_WSTGRP").Specific;
            oCombo.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index);
        }



        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                return;
            }
            if (getUFValue(oForm, "IDH_SRMOD").ToString() == "N") {
                SAPbouiCOM.ComboBox oComboWasteGroup = default(SAPbouiCOM.ComboBox);
                oComboWasteGroup =(SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_WSTGRP").Specific;
                if ((oComboWasteGroup.Selected.Value != "0")) {
                    UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
                    //if (oGridN.doProcessData() == false) {
                    //   BubbleEvent = false;
                    //}
                    IDH_ADITVLD moIDH_ADITVLD = new IDH_ADITVLD();
                    for (Int16 iRow = 0; iRow <= oGridN.getRowCount() - 1; iRow++) {

                        if (!string.IsNullOrEmpty(oGridN.doGetFieldValue("Code", iRow).ToString()) && moIDH_ADITVLD.getByKey(oGridN.doGetFieldValue("Code", iRow).ToString())) {
                            moIDH_ADITVLD.U_ItemCd = oGridN.doGetFieldValue("U_ItemCd", iRow).ToString();
                            moIDH_ADITVLD.U_Sort = (int)oGridN.doGetFieldValue("U_Sort", iRow);
                            // moIDH_ADITVLD.doSetFieldHasChanged("U_ItemCd", moIDH_ADITVLD.U_ItemCd, oGridN.doGetFieldValue("U_ItemCd", iRow).ToString());
                            //moIDH_ADITVLD.doSetFieldHasChanged("U_Sort", moIDH_ADITVLD.U_Sort, (int)oGridN.doGetFieldValue("U_Sort", iRow));
                            moIDH_ADITVLD.doUpdateDataRow();
                        } else if (!string.IsNullOrEmpty(oGridN.doGetFieldValue("U_ItemCd", iRow).ToString())) {
                            moIDH_ADITVLD.doAddEmptyRow(true, true, false);
                            moIDH_ADITVLD.U_ItemCd = oGridN.doGetFieldValue("U_ItemCd", iRow).ToString();
                            moIDH_ADITVLD.U_WstGpCd = oComboWasteGroup.Selected.Value;
                            moIDH_ADITVLD.U_Sort = (int)oGridN.doGetFieldValue("U_Sort", iRow);
                            moIDH_ADITVLD.doAddDataRow();
                        }
                    }
                    moIDH_ADITVLD.doProcessData();
                    ArrayList aDeletedRows = oGridN.getDeletedRowList();
                    if (aDeletedRows == null || aDeletedRows.Count > 0) {
                        String sRow;
                        int iIndex = 0;
                        while (iIndex < aDeletedRows.Count) {
                            sRow = aDeletedRows[iIndex].ToString();
                            if (sRow.Trim() != "" && moIDH_ADITVLD.getByKey(sRow)) {
                                moIDH_ADITVLD.doDeleteData();
                            }
                            iIndex++;
                        }
                    }
                    moIDH_ADITVLD.doProcessData();
                    doLoadData(oForm);
                }
            } else if (this.goParent.doCheckModal(oForm.UniqueID)) {
                System.Collections.ArrayList aSelectItems = new ArrayList();
                UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
                for (Int16 iRow = 0; iRow <= oGridN.getRowCount() - 1; iRow++) {
                    if (oGridN.doGetFieldValue("U_Selected", iRow).ToString() == "Y")
                        aSelectItems.Add(oGridN.doGetFieldValue("U_ItemCd", iRow).ToString());
                }
                setParentSharedData(oForm, "IDH_WSTGRP", aSelectItems);
                base.doReturnFromModalShared(oForm, true);
                BubbleEvent = false;
            }

        }
        protected override void doReturnNormal(SAPbouiCOM.Form oForm) {
            base.doReturnNormal(oForm);
        }
        public override void doFinalizeShow(SAPbouiCOM.Form oForm) {
            base.doFinalizeShow(oForm);
            if (getUFValue(oForm, "IDH_SRMOD").ToString() == "Y") {
                //oForm.ActiveItem = "IDH_PRKITM";
                //setUFValue(oForm, "IDH_WSTGRP", "973");//getParentSharedData(oForm, "IDH_WSTGRP"), true, false);
                //setItemValue(oForm, "IDH_WSTGRP", "973");//getParentSharedData(oForm, "IDH_WSTGRP").ToString());
                setItemValue(oForm, "IDH_WSTGRP", getParentSharedData(oForm, "IDH_WSTGRP").ToString());

                doSetFocus(oForm, "IDH_PRKITM");
                setEnableItem(oForm, false, "IDH_WSTGRP");
                // oForm.Items.Item("IDH_WSTGRP").Enabled = false;
                oForm.SupportedModes = 1;// SAPbouiCOM.BoFormMode.fm_OK_MODE;
                oForm.Items.Item("LINESGRID").AffectsFormMode = false;
            }
        }

        protected void doSetFilterFields(IDHAddOns.idh.controls.FilterGrid oGridN) {
            //oGridN.doAddFilterField("IDH_WSTGRP", "U_WstGpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 50);
        }

        protected void doSetListFields(IDHAddOns.idh.controls.FilterGrid oGridN) {

            oGridN.doAddListField("Code", "Code", false, 0, null, null);
            oGridN.doAddListField("Name", "Name", false, 0, null, null);

            oGridN.doAddListField("U_WstGpCd", "Waste Group Code", false, 0, null, null);
            string sAddGrp = com.idh.bridge.lookups.Config.INSTANCE.doGetAdditionalExpGroup();

            oGridN.doAddListField("U_ItemCd", "Item Code", ((getUFValue(oGridN.getSBOForm(), "IDH_SRMOD").ToString() == "Y") ? false : true), -1, "SRC:IDHISRC(WGADDITM)[IDH_ITMCOD;IDH_GRPCOD=" + sAddGrp + "][ITEMCODE;ItemName=ITEMNAME]", null);
            oGridN.doAddListField("ItemName", "Item Name", ((getUFValue(oGridN.getSBOForm(), "IDH_SRMOD").ToString() == "Y") ? false : true), -1, "SRC:IDHISRC(WGADDITM)[IDH_ITMNAM;IDH_GRPCOD=" + sAddGrp + "][ITEMNAME;U_ItemCd=ITEMCODE]", null);
            oGridN.doAddListField("U_Sort", "Sort Order", ((getUFValue(oGridN.getSBOForm(), "IDH_SRMOD").ToString() == "Y") ? false : true), -1, null, null);
            oGridN.doAddListField("U_Selected", "Select", ((getUFValue(oGridN.getSBOForm(), "IDH_SRMOD").ToString() == "Y") ? true : false), ((getUFValue(oGridN.getSBOForm(), "IDH_SRMOD").ToString() == "Y") ? (-1) : (0)), "CHECKBOX", null);

            oGridN.doApplyRules();
        }
        protected override void doReadInputParams(SAPbouiCOM.Form oForm) {
            //if (getHasSharedData(oForm)) {
            if (getParentSharedData(oForm, "IDH_WSTGRP") != null && getParentSharedData(oForm, "IDH_WSTGRP").ToString() != string.Empty) {
                setUFValue(oForm, "IDH_SRMOD", "Y");
                //        setUFValue(oForm, "IDH_WSTGRP", "973 V2", true, false);
                //setUFValue(oForm, "IDH_WSTGRP", "973 V2");//getParentSharedData(oForm, "IDH_WSTGRP"), true, false);

                // setFocus(oForm, "IDH_PRKITM");
                // oForm.ActiveItem = "IDH_PRKITM";
                // setEnableItem(oForm, false, "IDH_WSTGRP");
                // oForm.Items.Item("IDH_WSTGRP").Enabled = false;
            }
            //}
        }
        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            string sWasteGroupCode = "";

            if (getUFValue(oForm, "IDH_SRMOD").ToString() == "N") {
                SAPbouiCOM.ComboBox oComboWasteGroups = default(SAPbouiCOM.ComboBox);
                oComboWasteGroups = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_WSTGRP").Specific;
                if ((oComboWasteGroups.Selected != null) && (oComboWasteGroups.Selected.Value != "0"))
                    sWasteGroupCode = oComboWasteGroups.Selected.Value;
            } else
                sWasteGroupCode = getParentSharedDataAsString(oForm, "IDH_WSTGRP");//"973"; //
            IDHGrid oGridN;
            string sSql = null;
            if (getUFValue(oForm, "IDH_SRMOD").ToString() == "Y") {
                oGridN = (FilterGrid)FilterGrid.getInstance(oForm, "LINESGRID");
            } else {
                oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
            }
            if (sWasteGroupCode.Trim() != string.Empty) {
                sSql = "Select T0.Code,T0.Name,T0.U_WstGpCd,T0.U_ItemCd,T1.ItemName,T0.U_Sort,'N' As U_Selectd from [@IDH_ADITVLD] T0 , OITM T1 where T0.U_ItemCd=T1.ItemCode And T0.U_WstGpCd='" + sWasteGroupCode.Trim() + "' Order by U_Sort";
            } else {
                sSql = "Select T0.Code,T0.Name,T0.U_WstGpCd,T0.U_ItemCd,T1.ItemName,T0.U_Sort,'N' As U_Selectd from [@IDH_ADITVLD] T0 , OITM T1 where T0.U_ItemCd=T1.ItemCode And 1=0 Order by U_Sort";
            }
            oGridN.getSBOGrid().DataTable.ExecuteQuery(sSql);
            oGridN.doApplyRules();
            if (sWasteGroupCode.Trim() != string.Empty && (getUFValue(oForm, "IDH_SRMOD").ToString() == "N")) {
                if (oGridN.getRowCount() == 1 && oGridN.doGetFieldValue("U_ItemCd", 0).ToString() != "") {
                    oGridN.AddEditLine = true;
                    oGridN.doAddEditLine(true);
                } else if (oGridN.getRowCount() > 1 && oGridN.doGetFieldValue("U_ItemCd", oGridN.getRowCount() - 1).ToString() != "") {
                    oGridN.AddEditLine = true;
                    oGridN.doAddEditLine(true);
                    //oGridN.doAddEditLine(0, false, true);
                }
            } else if (sWasteGroupCode.Trim() == string.Empty && (getUFValue(oForm, "IDH_SRMOD").ToString() == "N")) {
                if (oGridN.getRowCount() == 1 && oGridN.doGetFieldValue("U_ItemCd", oGridN.getRowCount() - 1).ToString() == "") {
                    oGridN.doRemoveRow(0);
                }
            } else if (getUFValue(oForm, "IDH_SRMOD").ToString() == "Y") {
                if (oGridN.getRowCount() == 1 && oGridN.doGetFieldValue("U_ItemCd", 0).ToString() == "") {
                    oGridN.doRemoveRow(0);
                } else if (oGridN.getRowCount() > 1 && oGridN.doGetFieldValue("U_ItemCd", oGridN.getRowCount() - 1).ToString() == "") {
                    oGridN.doRemoveRow(oGridN.getRowCount() - 1);
                }
            } else if (oGridN.getRowCount() == 1) {
                if (oGridN.doGetFieldValue("U_ItemCd", 0).ToString() != "") {
                    oGridN.AddEditLine = true;
                    oGridN.doAddEditLine(true);
                }
                //  oGridN.doRemoveRow(0, false, true);
            } else if (oGridN.getRowCount() > 1) {
                oGridN.AddEditLine = true;
                oGridN.doAddEditLine(true);
                //oGridN.doAddEditLine(0, false, true);
            }

        }

        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            if (!pVal.BeforeAction && pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SEARCH_VALUESET) {
                IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");
                oGridN.doAddDataLine(true);
            } else if (pVal.BeforeAction == false && pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY) {
                IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");
                oGridN.doSetFieldValue("Name", pVal.Row, "1", true);

                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED && (getUFValue(oForm, "IDH_SRMOD").ToString() == "Y")) {
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
            }

            return base.doCustomItemEvent(oForm, ref pVal);

        }

        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.ItemUID == "1" && (getUFValue(oForm, "IDH_SRMOD").ToString() == "Y")) {
                if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_CLICK && pVal.BeforeAction == false) {

                    if (this.goParent.doCheckModal(oForm.UniqueID)) {
                        System.Collections.ArrayList aSelectItems = new ArrayList();
                        FilterGrid oGridN = (FilterGrid)FilterGrid.getInstance(oForm, "LINESGRID");
                        for (Int16 iRow = 0; iRow <= oGridN.getRowCount() - 1; iRow++) {
                            if (oGridN.doGetFieldValue("U_Selected", iRow).ToString() == "Y")
                                aSelectItems.Add(oGridN.doGetFieldValue("U_ItemCd", iRow).ToString());
                        }
                        setParentSharedData(oForm, "IDH_WSTGRP", aSelectItems);
                        base.doReturnFromModalShared(oForm, true);
                        BubbleEvent = false;
                    }
                }
                return true;
            }
            base.doItemEvent(oForm, ref pVal, ref BubbleEvent);

            if ((pVal.ItemUID == "IDH_WSTGRP") && (getUFValue(oForm, "IDH_SRMOD").ToString() == "N") && pVal.EventType == SAPbouiCOM.BoEventTypes.et_COMBO_SELECT && pVal.BeforeAction == false) {
                doLoadData(oForm);
            }
            return true;
        }

        public override void doClose() {
        }
    }
}