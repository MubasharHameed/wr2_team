﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using IDHAddOns.idh.controls;

using com.isb.enq.dbObjects.User;
using com.isb.enq;
using com.isb.enq.lookups;
using com.isb.enq.utils;
using com.idh.bridge;
using com.idh.dbObjects.numbers;
using SAPbouiCOM;

namespace com.isb.enq.forms {
    class OrderRow : WR1_Forms.idh.forms.OrderRow {

        public OrderRow(IDHAddOns.idh.addon.Base oParent, int iMenuPosition) : base(oParent, iMenuPosition) {
        }

       public override bool doAfterBtn1(SAPbouiCOM.Form oForm, bool bDoNew, bool bMustDoUpdates) {
            IDH_JOBSHD oWOR = (IDH_JOBSHD)thisWOR(oForm);

            oWOR.doSaveSampleTask();
            oWOR.doUpdateFinalBatchQty();
            oWOR.doSentNCRAlert();
            oWOR.doMarketingDocuments();

            return true;
        }

        public override bool doItemEvent(Form oForm, ref ItemEvent pVal, ref bool BubbleEvent) {
            return base.doItemEvent(oForm, ref pVal, ref BubbleEvent);
        }
    }
}
