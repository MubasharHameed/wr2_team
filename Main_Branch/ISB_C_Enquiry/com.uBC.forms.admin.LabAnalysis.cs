﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//using IDHAddOns.idh.controls;
//using com.uBC.utils;
using com.idh.controls;
//using com.idh.bridge.utils;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry.admin {
    public class LabAnalysis : com.uBC.forms.fr3.dbGrid.Base {

        public LabAnalysis(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
                mbIsSearch = false;
                SkipFormSettings = true;
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDHLABANY", sParMenu, iMenuPos, "admin.srf", false, true, false, "Lab Analysis", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        protected override void doInitialize() {
            moDBObject = new IDH_LABANLYS(); 
        }

#region FormOpenCreateFunctions
        /*
         * Do Complete the Form
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            base.doCompleteCreate(ref BubbleEvent);
        }

       /* 
        * Do the final form steps to show before loaddata
        */
        public override void doBeforeLoadData() {
            base.doBeforeLoadData();
        }

        /* 
         * Load the Form Data
         */
        public override void doLoadData() {
            base.doLoadData();
            //moAdminGrid.doReloadSetExtra("",false,true, false);
            //moAdminGrid.doApplyRules();
            doFillCombos();
        }

        /*
         * Set the List Fields
         */
        public override void doSetListFields() {
            moAdminGrid.doAddListField(IDH_LABANLYS._Code, "Code", false, 0, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_LABANLYS._Name, "Name", false, 0, ListFields.LISTTYPE_IGNORE, null, -1);

            moAdminGrid.doAddListField(IDH_LABANLYS._AnalysisCd, "Analysis Code", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_LABANLYS._AnalysisDesc, "Analysis Description", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            //moAdminGrid.doAddListField("'Select'", "Select", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
           
        }
#endregion

#region Events
        protected override void doSetHandlers() {
            base.doSetHandlers();
        }

        protected override void doSetGridHandlers() {
            base.doSetGridHandlers();

          //  moAdminGrid.Handler_GRID_FIELD_CHANGED = new IDHGrid.ev_GRID_EVENTS(doFieldChangeEvent); 
        }
#endregion 

#region ItemEventHandlers
        //public bool doFieldChangeEvent(ref IDHAddOns.idh.events.Base pVal) {
        //    if (pVal.BeforeAction) {
        //    } else {
        //        IDH_WGVDMS oTrnCode = ((IDH_WGVDMS)moDBObject);
        //        string sTrnCode = "" +
        //        setVal(oTrnCode.U_Stock) + '-' + setVal(oTrnCode.U_WhseFrom) + '-' + setVal(oTrnCode.U_WhseTo) + '-' +
        //        setVal(oTrnCode.U_Trigger) + '-' + setVal(oTrnCode.U_MarkDoc1) + '-' + setVal(oTrnCode.U_MarkDoc2) + '-' +
        //        setVal(oTrnCode.U_MarkDoc3) + '-' + setVal(oTrnCode.U_MarkDoc4);
        //        oTrnCode.U_TrnCode = sTrnCode;
        //    }
        //    return true;
        //}

        private string setVal(string sVal) {
            if (sVal.Length == 0)
                return "0";
            else
                return sVal;
        }
        public override bool doAddUpdateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                Freeze(true);
                if (doValidate() == false) {
                    BubbleEvent = false;
                    return true;
                }
                if (moAdminGrid.doProcessData()) {
                    doLoadData();
                    moAdminGrid.doApplyRules();
                }
                Freeze(false);
            }
            return true;
        }
        private bool doValidate() {
            bool bResult = true;
            ////int iRes=0;
            //IDH_WGVDMS _IDH_WGVDMS = new IDH_WGVDMS();
            //for (int iRow1 = 0; iRow1 <= moAdminGrid.getRowCount()-1; iRow1++) {
            //    for (int iRow2 = iRow1+1; iRow2 <= moAdminGrid.getRowCount()-1; iRow2++) {
            //        if (moAdminGrid.doGetFieldValue(IDH_WGVDMS._ValidCd, iRow1).ToString() != string.Empty && moAdminGrid.doGetFieldValue(IDH_WGVDMS._ValidCd, iRow2).ToString() != string.Empty 
            //            && moAdminGrid.doGetFieldValue(IDH_WGVDMS._ValidCd, iRow1).ToString() == moAdminGrid.doGetFieldValue(IDH_WGVDMS._ValidCd, iRow2).ToString()) {
            //            //iRes = _IDH_WGVDMS.getData(IDH_WGVDMS._ValidCd + "='" + moAdminGrid.doGetFieldValue(IDH_WGVDMS._ValidCd, iRow) + "'", "");
            //            //if (iRes != 0) {
            //                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Duplicate entries are not allowed:" + moAdminGrid.doGetFieldValue(IDH_WGVDMS._ValidCd, iRow2), "ERUSDBDU",
            //                    new string[] { moAdminGrid.doGetFieldValue(IDH_WGVDMS._ValidCd, iRow2).ToString() });
            //                bResult = false;
            //            //}
            //        }
            //    }
            //}
            return bResult;
        }
#endregion

        #region fillCombos
        private void doFillCombos() {
//            SAPbouiCOM.ComboBoxColumn oCombo;

//            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_WGVDMS._Type));
//            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
//            FillCombos.FillWR1StatusNew(oCombo,103);//Validations Master 103

//            oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_WGVDMS._DtType));
//            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
//            FillCombos.FillWR1StatusNew(oCombo, 104);//WG Validations Join 104
           
////            try {
//  //              string id = moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_WGVDMS._Fields)).UniqueID;
//    //            moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_WGVDMS._Fields)).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
//                oCombo = (SAPbouiCOM.ComboBoxColumn)moAdminGrid.getSBOGrid().Columns.Item(moAdminGrid.doIndexFieldWC(IDH_WGVDMS._Fields));
//                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
//                FillCombos.FillWR1StatusNew(oCombo, 105);//WG Validations Join 105

//      //      } catch (Exception ex) { 
//        //    }  
             

        }
#endregion
    }
}
