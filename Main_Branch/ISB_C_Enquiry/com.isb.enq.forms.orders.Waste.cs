﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using IDHAddOns.idh.controls;

using com.isb.enq.dbObjects.User;
using com.isb.enq;
using com.isb.enq.lookups;
using com.isb.enq.utils;

namespace com.isb.enq.forms.orders {
    class Waste : WR1_Forms.idh.forms.orders.Waste {

        public Waste(IDHAddOns.idh.addon.Base oParent, int iMenuPosition) : base(oParent, iMenuPosition) {
        }

        protected override bool doUpdateGridRows(SAPbouiCOM.Form oForm) {
            UpdateGrid oUpdateGrid;
            oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID");

            ArrayList oAddedRows = null;
            if (oUpdateGrid.hasAddedRows()) {
                oAddedRows = (ArrayList)oUpdateGrid.doGetAddedRows().Clone();
            }

            ArrayList oModifiedRows = null;
            if (oUpdateGrid.hasChangedRows())
                oModifiedRows = (ArrayList)oUpdateGrid.doGetChangedRows().Clone();

            ArrayList oNCRRows_NCR = new ArrayList();
            ArrayList oNCRRows_NCRCompliant = new ArrayList();
            ArrayList oNCRRows_NCRResolved = new ArrayList();
            ArrayList oLabTaskRows = new ArrayList();
            //If Config.INSTANCE.getParameterAsBool("NCRALRT", False) Then
            doGetNCRnLabChangedRows(oForm, oAddedRows, oModifiedRows, oNCRRows_NCR, oNCRRows_NCRCompliant, oNCRRows_NCRResolved, oLabTaskRows);
            //End If

            if (oUpdateGrid.doProcessData()) {
                doRemoteAlert(oForm, oAddedRows);
                if (oModifiedRows != null && oModifiedRows.Count > 0)
                    doUpdateFinalBatch(oForm);

                //''call here the Update of Batch Qty
                if (Config.ParameterAsBool("NCRALRT", false)) {
                    if (oNCRRows_NCR != null && oNCRRows_NCR.Count > 0) {
                        for (int iIndex = 0; iIndex < oNCRRows_NCR.Count; iIndex++) {
                            int iRow = (int)oNCRRows_NCR[iIndex];
                            oUpdateGrid.setCurrentDataRowIndex(iRow);
                            General.SendInternalAlert(
                                Config.INSTANCE.getParameterWithDefault("NCRRECP1", ""), 
                                Config.INSTANCE.getParameterWithDefault("NCRMSG1", ""), 
                                Config.INSTANCE.getParameterWithDefault("NCRSUB1", ""), 
                                "NCR", 
                                (string)oUpdateGrid.doGetFieldValue("Code"), 
                                "WOR");
                        }
                    }
                    if (oNCRRows_NCRCompliant != null && oNCRRows_NCRCompliant.Count > 0) {
                        for (int iIndex = 0; iIndex < oNCRRows_NCRCompliant.Count; iIndex++) {
                            int iRow = (int)oNCRRows_NCRCompliant[iIndex];
                            oUpdateGrid.setCurrentDataRowIndex(iRow);
                            General.SendInternalAlert(
                                Config.INSTANCE.getParameterWithDefault("NCRRECP2", ""), 
                                Config.INSTANCE.getParameterWithDefault("NCRMSG2", ""), 
                                Config.INSTANCE.getParameterWithDefault("NCRSUB2", ""), 
                                "NCR Compliant", 
                                (string)oUpdateGrid.doGetFieldValue("Code"),
                                "WOR");
                        }
                    }
                    if (oNCRRows_NCRResolved != null && oNCRRows_NCRResolved.Count > 0) {
                        for (int iIndex = 0; iIndex < oNCRRows_NCRResolved.Count; iIndex++) {
                            int iRow = (int)oNCRRows_NCRResolved[iIndex];
                            oUpdateGrid.setCurrentDataRowIndex(iRow);
                            General.SendInternalAlert(
                                Config.INSTANCE.getParameterWithDefault("NCRRECP3", ""), 
                                Config.INSTANCE.getParameterWithDefault("NCRMSG3", ""), 
                                Config.INSTANCE.getParameterWithDefault("NCRSUB3", ""), 
                                "NCR Resolved", 
                                (string)oUpdateGrid.doGetFieldValue("Code"), 
                                "WOR");
                        }
                    }
                }

                if (oLabTaskRows != null && oLabTaskRows.Count > 0) {
                    for (int iIndex = 0; iIndex < oLabTaskRows.Count; iIndex ++) {
                        int iRow = (int)oLabTaskRows[iIndex];
                        oUpdateGrid.setCurrentDataRowIndex(iRow);
                        if ((string)oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Sample) == "Y" && (string)oUpdateGrid.doGetFieldValue(IDH_JOBSHD._SampleRef) != "") {
                            IDH_LABTASK oLabTask = new IDH_LABTASK();
                            oLabTask.UnLockTable = true;
                            if (oLabTask.getByKey((string)oUpdateGrid.doGetFieldValue(IDH_JOBSHD._SampleRef)) && oLabTask.U_Saved == 0) {
                                oLabTask.U_Saved = 1;
                                string sUser = oLabTask.U_AssignedTo;

                                oLabTask.doProcessData();

                                IDH_LABITM oLabItem = new IDH_LABITM();
                                oLabItem.UnLockTable = true;
                                oLabItem.getByKey(oLabTask.U_LabItemRCd);
                                oLabItem.U_Saved = 1;
                                oLabItem.doProcessData();
                                if (!string.IsNullOrEmpty(sUser)) {
                                    IDH_LABTASK.dosendAlerttoUser(sUser, com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTM1", null), com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTS1", null), false, oLabTask.Code);
                                }
                            }
                        }
                        //'com.idh.bridge.utils.General.SendInternalAlert(Config.INSTANCE.getParameterWithDefault("NCRRECP3", ""), Config.INSTANCE.getParameterWithDefault("NCRMSG3", ""), Config.INSTANCE.getParameterWithDefault("NCRSUB3", ""), "NCR Resolved", oUpdateGrid.doGetFieldValue("Code"), "WOR")
                    }
                }
                return true;
            } else
                return false;
        }

        //**
        // Creates a duplicate row from the source index
        //**
        protected override void doDuplicateRow__(IDHGrid oGridN, int iSrcRow, int iDstRow, string sNewCode, string sNewName, bool bCopyComm, bool bCopyADDI, bool bCopyPrice, bool bCopyQty, bool bCopyVehReg, bool bCopyDriver, string sCoverageVal = null) {
            base.doDuplicateRow__(oGridN, iSrcRow, iDstRow, sNewCode, sNewName, bCopyComm, bCopyADDI, bCopyPrice, bCopyQty, bCopyVehReg, bCopyDriver, sCoverageVal);

            oGridN.doSetFieldValue(IDH_JOBSHD._Sample, iDstRow, "");
            oGridN.doSetFieldValue(IDH_JOBSHD._SampleRef, iDstRow, "");
            oGridN.doSetFieldValue(IDH_JOBSHD._SampStatus, iDstRow, "");

            if (oGridN.doGetFieldValue(IDH_JOBSHD._WasCd, iDstRow) != null && oGridN.doGetFieldValue(IDH_JOBSHD._WasCd, iDstRow).ToString() != String.Empty && Config.INSTANCE.getParameterAsBool("CPSMRWOR", true)) {
                string sSampleRef = "", sSampleStatus = "";
                if (General.doCheckIfLabTaskRequired(oGridN.doGetFieldValue(IDH_JOBSHD._WasCd, iDstRow).ToString())) {
                    General.doCreateLabItemNTask((string)oGridN.doGetFieldValue(IDH_JOBSHD._WasCd, iDstRow),
                        (string)oGridN.doGetFieldValue(IDH_JOBSHD._WasDsc, iDstRow),
                        "",
                        (string)oGridN.doGetFieldValue(IDH_JOBSHD._UOM, iDstRow),
                        (string)oGridN.doGetFieldValue(IDH_JOBSHD._Quantity, iDstRow), 
                        (string)oGridN.doGetFieldValue(IDH_JOBSHD._Comment, iDstRow),
                        (string)oGridN.doGetFieldValue(IDH_JOBSHD._Code, iDstRow), 
                        (string)oGridN.doGetFieldValue(IDH_JOBSHD._JobNr, iDstRow), 
                        "WOR",
                        (string)oGridN.doGetFieldValue(IDH_JOBSHD._CustCd, iDstRow), 
                        (string)oGridN.doGetFieldValue(IDH_JOBSHD._CustNm, iDstRow), 
                        "", false, 0, ref sSampleRef, ref sSampleStatus, "",
                        (string)oGridN.doGetFieldValue(IDH_JOBSHD._RouteCd, iDstRow));
                    oGridN.doSetFieldValue(IDH_JOBSHD._Sample, iDstRow, "Y");
                    oGridN.doSetFieldValue(IDH_JOBSHD._SampleRef, iDstRow, sSampleRef);
                    oGridN.doSetFieldValue(IDH_JOBSHD._SampStatus, iDstRow, sSampleStatus);
                }
            }
        }

        protected void doUpdateFinalBatch(SAPbouiCOM.Form oForm) {
            IDH_PVHBTH.doUpdateContainerQtyOfWORs(getFormDFValue(oForm, "Code").ToString());
        }

        protected void doGetNCRnLabChangedRows(SAPbouiCOM.Form oForm, ArrayList oAddedRows, ArrayList oModfieldRows, ArrayList oNCRRows_NCR, ArrayList oNCRRows_NCRCompliant, ArrayList oNCRRows_NCRResolved, System.Collections.ArrayList oLabTaskRows) {
            UpdateGrid oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID");
            if ( oAddedRows != null && oAddedRows.Count > 0 ) {
                int iRow;
                for (int iIndex = 0; iIndex < oAddedRows.Count; iIndex++) {
                    iRow = (int)oAddedRows[iIndex];
                    oUpdateGrid.setCurrentDataRowIndex(iRow);
                    if ((string)oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys1) == "Yes" || (string)oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys1) == "Y")
                        oNCRRows_NCR.Add(iRow);

                    if ((string)oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys2) == "Yes" || (string)oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys2) == "Y")
                        oNCRRows_NCRCompliant.Add(iRow);

                    if ((string)oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys3) == "Yes" || (string)oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys3) == "Y")
                        oNCRRows_NCRResolved.Add(iRow);

                    if ((string)oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Sample) == "Y")
                        oLabTaskRows.Add(iRow);
                }
            }

            if (oModfieldRows != null && oModfieldRows.Count > 0) {
                int iRow;
                for (int iIndex = 0; iIndex < oModfieldRows.Count; iIndex++) {
                    iRow = (int)oModfieldRows[iIndex];
                    oUpdateGrid.setCurrentDataRowIndex(iRow);
                    if (oUpdateGrid.hasFieldChanged(IDH_JOBSHD._Analys1) && ((string)oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys1) == "Yes" || (string)oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys1) == "Y"))
                        oNCRRows_NCR.Add(iRow);

                    if (oUpdateGrid.hasFieldChanged(IDH_JOBSHD._Analys2) && ((string)oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys2) == "Yes" || (string)oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys2) == "Y"))
                        oNCRRows_NCRCompliant.Add(iRow);

                    if (oUpdateGrid.hasFieldChanged(IDH_JOBSHD._Analys3) && ((string)oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys3) == "Yes" || (string)oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Analys3) == "Y"))
                        oNCRRows_NCRResolved.Add(iRow);

                    if ((oUpdateGrid.hasFieldChanged(IDH_JOBSHD._Sample) || oUpdateGrid.hasFieldChanged(IDH_JOBSHD._SampleRef)) && (string)oUpdateGrid.doGetFieldValue(IDH_JOBSHD._Sample) == "Y")
                        oLabTaskRows.Add(iRow);
                }
            }
        }

        protected bool doRemoteAlert(SAPbouiCOM.Form oForm, ArrayList oAddedRows) {
            UpdateGrid oUpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID");

            string sOrderNr = null;
            if (oAddedRows != null && oAddedRows.Count > 0) {
                sOrderNr = (string)getFormDFValue(oForm, "Code");
                int iRow;
                if (oAddedRows != null) {
                    for (int iIndex = 0; iIndex < oAddedRows.Count; iIndex++) {
                        iRow = (int)oAddedRows[iIndex];
                        oUpdateGrid.setCurrentDataRowIndex(iRow);
                        string sCode = (string)oUpdateGrid.doGetFieldValue("Code");
                        string sCarrCd = (string)oUpdateGrid.doGetFieldValue("U_CarrCd");
                        string sSiteCd = (string)oUpdateGrid.doGetFieldValue("U_Tip");

                        string sSubject = "Remote Waste Order";
                        DateTime oDate = (DateTime)oUpdateGrid.doGetFieldValue("U_RDate");
                        string sMessage = "The Following Waste Order (" + sOrderNr + "." + sCode + ") was Raised on " + goParent.goDICompany.CompanyName + ":\n" +
                                    "Customer: [" + oUpdateGrid.doGetFieldValue("U_CustCd") + "] " + oUpdateGrid.doGetFieldValue("U_CustNm") + '\n' +
                                    "Customer Site: [" + getFormDFValue(oForm, "U_Address") + "] " + getFormDFValue(oForm, "U_Street") +
                                             ", " + getFormDFValue(oForm, "U_Block") + ", " + getFormDFValue(oForm, "U_City") +
                                             ", " + getFormDFValue(oForm, "U_ZpCd") + '\n' +
                                    "Contact Number: " + getFormDFValue(oForm, "U_SiteTl") + '\n' +
                                    "Customer RefNr: " + oUpdateGrid.doGetFieldValue("U_CustRef") + '\n' +
                                    "Waste Carrier: [" + sCarrCd + "] " + oUpdateGrid.doGetFieldValue("U_CarrNm") + '\n' +
                                    "Disposal Site: [" + sSiteCd + "] " + oUpdateGrid.doGetFieldValue("U_TipNm") + '\n' +
                                    "Requested Date: " + goParent.doDateToSBODisplay(ref oDate) + '\n' +
                                    "Special Instruction: " + getFormDFValue(oForm, "U_SpInst") + '\n' +
                                    "Order Type: " + oUpdateGrid.doGetFieldValue("U_JobTp") + '\n' +
                                    "Container: [" + oUpdateGrid.doGetFieldValue("U_ItemCd") + "] " + oUpdateGrid.doGetFieldValue("U_ItemDsc") + '\n' +
                                    "Waste Material: [" + oUpdateGrid.doGetFieldValue("U_WasCd") + "] " + oUpdateGrid.doGetFieldValue("U_WasDsc") + '\n' +
                                    "Comment: " + oUpdateGrid.doGetFieldValue("U_Comment") + '\n';

                        SAPbouiCOM.BoLinkedObject oLinkType = SAPbouiCOM.BoLinkedObject.lf_None;
                        string sLinkValue = "";
                        string sCompName = goParent.goDICompany.CompanyName;

                        if (!string.IsNullOrWhiteSpace(sCarrCd) && !string.IsNullOrWhiteSpace(sSiteCd) && sCarrCd.Equals(sSiteCd) && !sCarrCd.Equals(sCompName))
                            ((WR1_Forms.idh.main.MainForm)goParent).doSendRemoteAlert(sCarrCd, goParent.goDICompany.UserName, sSubject, sMessage, oLinkType, sLinkValue, true);
                        else {
                            if (!string.IsNullOrWhiteSpace(sCarrCd) && !sCarrCd.Equals(sCompName))
                                ((WR1_Forms.idh.main.MainForm)goParent).doSendRemoteAlert(sCarrCd, goParent.goDICompany.UserName, sSubject, sMessage, oLinkType, sLinkValue, true);
                        
                            if (!string.IsNullOrWhiteSpace(sSiteCd) && !sSiteCd.Equals(sCompName))
                                ((WR1_Forms.idh.main.MainForm)goParent).doSendRemoteAlert(sSiteCd, goParent.goDICompany.UserName, sSubject, sMessage, oLinkType, sLinkValue, true);
     
                        }
                    }
                }
            }
            return true;
        }
    }
}
