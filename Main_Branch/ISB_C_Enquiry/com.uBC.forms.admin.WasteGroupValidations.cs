﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using IDHAddOns.idh.controls;
using com.idh.bridge.data;


namespace com.isb.forms.Enquiry.admin {
    //Inherits idh.forms.admin.Tmpl
    //msKeyGen="WGPVALDS"
    public class WasteGroupValidations : IDHAddOns.idh.forms.Base {

        protected string msKeyGen = "GENSEQ";
        public WasteGroupValidations(IDHAddOns.idh.addon.Base oParent, string sParMenu, int iMenuPosition)
            : base(oParent, "IDHWGVALDS", sParMenu, iMenuPosition, "Enq_Waste Group Validations.srf", true, true, false, "Waste Group Validations", load_Types.idh_LOAD_NORMAL) {

        }
        protected string getTable() {
            return "@" + getUserTable();
        }

        protected string getUserTable() {
            return "IDH_WGVALDS";
        }

        protected override void doSetEventFilters() {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);

        }
        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            try {
                oForm.Title = gsTitle;

                SAPbouiCOM.Item oItem = default(SAPbouiCOM.Item);
                oItem = oForm.Items.Item("IDH_CONTA");
                UpdateGrid oGridN = new UpdateGrid(this, oForm, "LINESGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane);

                //doSetFilterLayout(oForm)
                oForm.Items.Item("IDH_WSTGRP").AffectsFormMode = false;

            } catch (Exception ex) {
                //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
                BubbleEvent = false;
            }
        }

        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCloseForm(oForm, ref BubbleEvent);
            FilterGrid.doRemoveGrid(oForm, "LINESGRID");
        }

        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {
            //goParent.goDB.doSelectQuery("If (Select Count(1) from [@IDH_FORMSET])=1211 Begin  Exec IDH_CopyDeptForAllUsers; Exec IDH_UpdateFormSettings; end ")
            //doe("Select 1; Exec IDH_CopyDeptForAllUsers; ")
            //doUpdateQuery("Select 1; Exec IDH_UpdateFormSettings")
            UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
            if (oGridN == null) {
                oGridN = new UpdateGrid(this, oForm, "LINESGRID");
            }
            oGridN.AddEditLine = false;

            doSetFilterFields(oGridN);
            doSetListFields(oGridN);

            oGridN.doAddGridTable(new GridTable(getTable(), null, "Code", true), true);

            oGridN.doSetDoCount(true);
            oGridN.doSetHistory(getUserTable(), "Code");
            //oGridN.setOrderValue("U_GridName ASC, U_Index Asc,U_Name Asc,Cast(Code as Int) ASC ")

            doFillCombos(oForm);
        }


        private void doFillCombos(SAPbouiCOM.Form oForm) {
            //Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            //oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            //oRecordSet = goParent.goDB.doSelectQuery("Select * from [@ISB_WASTEGRP] Order by U_WstGpCd")
            //If oRecordSet IsNot Nothing Then IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)

            doFillCombo(oForm, "IDH_WSTGRP", "[@ISB_WASTEGRP]", "U_WstGpCd", "U_WstGpNm", null, "U_WstGpCd", "Select", "0");

            SAPbouiCOM.ComboBox oCombo = default(SAPbouiCOM.ComboBox);
            oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_WSTGRP").Specific;
            oCombo.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index);
            //   doFormsCombo(oForm)
            //doLoadUserCombo(oForm)
        }



        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                return;
            }
            SAPbouiCOM.ComboBox oComboWasteGroup = default(SAPbouiCOM.ComboBox);
            oComboWasteGroup = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_WSTGRP").Specific;
            if ((oComboWasteGroup.Selected.Value != "0")) {
                UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
                //If oGridN.hasFieldChanged("U_Select") Then
                com.idh.dbObjects.User.IDH_WGVALDS moIDH_WGVALDS = new com.idh.dbObjects.User.IDH_WGVALDS();
                for (Int16 iRow = 0; iRow <= oGridN.getRowCount() - 1; iRow++) {
                    //If oGridN.dataHasChanged Then ', "U_Select"

                    if (!string.IsNullOrEmpty(oGridN.doGetFieldValue("Code", iRow).ToString()) && moIDH_WGVALDS.getByKey(oGridN.doGetFieldValue("Code", iRow).ToString())) {
                        moIDH_WGVALDS.U_Select = oGridN.doGetFieldValue("U_Select", iRow).ToString();
                        moIDH_WGVALDS.U_ValList = oGridN.doGetFieldValue("U_ValList", iRow).ToString();
                        moIDH_WGVALDS.U_Sort = (int)oGridN.doGetFieldValue("U_Sort", iRow);

                    } else if (!string.IsNullOrEmpty(oGridN.doGetFieldValue("U_Select", iRow).ToString())) {
                        moIDH_WGVALDS.doAddEmptyRow(true, true, false);
                        moIDH_WGVALDS.U_Select = oGridN.doGetFieldValue("U_Select", iRow).ToString();
                        moIDH_WGVALDS.U_ValidCd = oGridN.doGetFieldValue("U_ValidCd", iRow).ToString();
                        moIDH_WGVALDS.U_ValidNm = oGridN.doGetFieldValue("U_ValidNm", iRow).ToString();
                        moIDH_WGVALDS.U_WstGpCd = oComboWasteGroup.Selected.Value;
                        //oGridN.doGetFieldValue("U_WstGpCd", iRow).ToString()
                        moIDH_WGVALDS.U_ValList = oGridN.doGetFieldValue("U_ValList", iRow).ToString();
                        moIDH_WGVALDS.U_Sort = (int)oGridN.doGetFieldValue("U_Sort", iRow);

                    }
                    moIDH_WGVALDS.doProcessData();
                    //End If
                }
                //If moIDH_WGVALDS.hasDataToProcess() Then
                //Dim iResult As Boolean = moIDH_WGVALDS.doReLoadData
                //If iResult Then
                //doLoadData(oForm)
                //End If
                //End If

                //End If
                //MyBase.doButtonID1(oForm, pVal, BubbleEvent)
                //If oComboUser.Selected.Value = "All" And pVal.BeforeAction = False Then
                //    goParent.STATUSBAR.SetText("Please wait while system is updating data...", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                //    ''Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
                //    '' oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                //    'goParent.goDB.doSelectQuery("sGetUpdateQueryForUsers", sGetUpdateQueryForUsers(oComboDept.Selected.Value, oComboForm.Selected.Value))
                //    ''If oRecordSet IsNot Nothing Then IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
                //    goParent.STATUSBAR.SetText("Operation completed successfully.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                //End If
            }

        }

        public override void doFinalizeShow(SAPbouiCOM.Form oForm) {
            base.doFinalizeShow(oForm);
        }

        protected void doSetFilterFields(IDHAddOns.idh.controls.FilterGrid oGridN) {
            //oGridN.doAddFilterField("IDH_DEPT", "U_DeptCode", SAPbouiCOM.BoDataType.dt_SHORT_NUMBER, "=", 10, Nothing)
            //oGridN.doAddFilterField("IDH_USERS", "U_UserName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "")
            //oGridN.doAddFilterField("IDH_FORMS", "U_FormID", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "")
        }

        protected void doSetListFields(IDHAddOns.idh.controls.FilterGrid oGridN) {
            oGridN.doAddListField("Code", "Code", false, 0, null, null);
            oGridN.doAddListField("Name", "Name", false, 0, null, null);

            oGridN.doAddListField("U_ValidCd", "Name", false, 30, null, null);
            oGridN.doAddListField("U_ValidNm", "Description", false, -1, null, null);
            oGridN.doAddListField("U_Select", "Select", true, -1, "CHECKBOX", null);
            oGridN.doAddListField("U_ValList", "Valid Values(Val:Desc;)", true, -1, null, null);
            oGridN.doAddListField("U_Sort", "Sort Order", true, -1, null, null);
            // oGridN.doAddListField("U_WstGpCd", "Waste Group", False, 0, Nothing, Nothing)

            //oGridN.doAddListField("U_OrderBy", "Order By", True, -1, "COMBOBOX", Nothing)


        }

        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            SAPbouiCOM.ComboBox oComboWasteGroups = default(SAPbouiCOM.ComboBox);
            oComboWasteGroups = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_WSTGRP").Specific;
            string sSql = null;
            if ((oComboWasteGroups.Selected != null) && (oComboWasteGroups.Selected.Value != "0")) {
                //sSql = "Select a.U_ValidCd,a.U_ValidNm,(Select b.U_Select from [@IDH_WGVALDS]  b with(nolock) where a.U_ValidCd=b.U_ValidCd and b.U_WstGpCd='" & oComboWasteGroups.Selected.Value & "') U_Select,a.Code,a.Name from [@IDH_WGVDMS] a with(nolock) order by a.U_ValidCd"
                //sSql = "Select b.code,b.Name ,a.U_ValidCd,a.U_ValidNm,b.U_Select from [@IDH_WGVDMS] a with(nolock) left join [@IDH_WGVALDS]  b on a.U_ValidCd=b.U_ValidCd Where isnull(b.U_WstGpCd,'" & oComboWasteGroups.Selected.Value & "')='" & oComboWasteGroups.Selected.Value & "'"
                sSql = "Select T0.* From (Select b.code,b.Name,a.U_ValidCd,a.U_ValidNm,b.U_Select,b.U_ValList,b.U_Sort  from [@IDH_WGVDMS] a with(nolock), " + " [@IDH_WGVALDS] b where a.U_ValidCd=b.U_ValidCd and b.U_WstGpCd='" + oComboWasteGroups.Selected.Value + "'" + " UNION ALL " + " Select null code,null Name,a.U_ValidCd,a.U_ValidNm,null U_Select, U_Expression U_ValList,0 as U_Sort from [@IDH_WGVDMS] a with(nolock) where a.U_Common<>'Y' AND " + " not a.U_ValidCd in (Select b.U_ValidCd from [@IDH_WGVALDS]  b where b.U_WstGpCd='" + oComboWasteGroups.Selected.Value + "') )T0 Order by U_ValidCd";
            } else {
                sSql = "Select b.code,b.Name ,a.U_ValidCd,a.U_ValidNm,b.U_Select,b.U_ValList,b.U_Sort   from [@IDH_WGVDMS] a with(nolock) Inner join [@IDH_WGVALDS]  b on a.U_ValidCd=b.U_ValidCd Where b.U_WstGpCd='-1'";
            }
            //Dim oRecSet As SAPbobsCOM.Recordset = PARENT.goDB.doSelectQuery(sSql)
            UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
            oGridN.getSBOGrid().DataTable.ExecuteQuery(sSql);
            oGridN.doApplyRules();
            //  MyBase.doLoadData(oForm)
        }

        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY) {
                IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");
                oGridN.doSetFieldValue("Name", pVal.Row, "1", true);
                //Dim oNumbers As DataHandler.NextNumbers = doSetKeyValue(oForm)
                //oGridN.doSetFieldValue("Code", pVal.Row, oNumbers.CodeCode, True)
                //oGridN.doSetFieldValue("Name", pVal.Row, oNumbers.NameCode, True)

                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
            }
            return base.doCustomItemEvent(oForm, ref pVal);

        }

        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            base.doItemEvent(oForm, ref pVal, ref BubbleEvent);
            if ((pVal.ItemUID == "IDH_WSTGRP") && pVal.EventType == SAPbouiCOM.BoEventTypes.et_COMBO_SELECT && pVal.BeforeAction == false) {
                doLoadData(oForm);
            }
            return true;
        }

        public override void doClose() {
        }
    }
}