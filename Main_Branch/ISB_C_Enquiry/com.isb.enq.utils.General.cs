﻿using System;
using System.Collections;
using com.idh.bridge.lookups;
using com.idh.dbObjects.User;
using com.isb.enq.dbObjects.User;
using com.idh.dbObjects.numbers;
using com.idh.bridge;

namespace com.isb.enq.utils {
    public class General : com.isb.bridge.utils.General {

        #region Lab Tasks

        public static bool doCreateLabItemNTask(string sWasteCode, string sWasteDesc, string sWasteForeignName, string sUOM, string sQTY, string sComments,
            string sDocLine, string sDocNum, string sObjectType, string sCardCode, string sCardName, string sWhse,
            bool doUpdateLabItem, int iSaved, ref string sSampleRef, ref string sSampleStatus, string sLabTasktoDuplicate, string sDispRouteCode) {
            try {
                //do validate WOrk flow step 1
                //do check if BP have linked Sales emp
                string sUser = "", sDept = "";

                if (sLabTasktoDuplicate == "" && !string.IsNullOrEmpty(sCardCode) && !sObjectType.Contains("WOR")) {
                    sUser = Config.INSTANCE.doUserNameofBPSalesEmployee(sCardCode);
                    if (iSaved == 1 && (sUser == null || sUser.Trim() == string.Empty)) {
                        DataHandler.INSTANCE.doUserError(Translation.getTranslatedMessage("NOSALEPR", "Please assign the sales employee of BP " + sCardName + ", and update the task assignment accordingly."));
                    } else if (sUser != "") {
                        sDept = Config.INSTANCE.getValueFromTablebyKey("OUSR", "Department", "USER_CODE", sUser).ToString();
                    }
                }
                IDH_LABITM oLabItem = new IDH_LABITM();
                IDH_LITMDTL oLIDetail = new IDH_LITMDTL();

                //Validate selected row 
                string sICode = sWasteCode;// moWOQ.WOQItems.getValueAsString(iRow, IDH_WOQITEM._WasCd);
                string sIName = sWasteDesc; //moWOQ.WOQItems.getValueAsString(iRow, IDH_WOQITEM._WasDsc);

                if (sICode.Length <= 0) {
                    sICode = sWasteForeignName;// moWOQ.WOQItems.getValueAsString(iRow, IDH_WOQITEM._WasFDsc);
                    sIName = sICode;

                    if (sICode.Length <= 0)
                        return false;
                }

                sIName = (sIName.Length <= 0 ? sIName = sICode : sIName);

                oLabItem = new IDH_LABITM();
                oLIDetail = new IDH_LITMDTL();

                // string sUOM = moWOQ.WOQItems.getValueAsString(iRow, IDH_WOQITEM._UOM);
                // string sQty = moWOQ.WOQItems.getValueAsString(iRow, IDH_WOQITEM._Quantity);
                //string sWhse = moWOQ.WOQItems.getValueAsString(pVal.Row, IDH_WOQITEM._wse);
                // string sComments = moWOQ.WOQItems.getValueAsString(iRow, IDH_WOQITEM._Comment);

                NumbersPair oNextLINo = oLabItem.getNewKey();
                oLabItem.Code = oNextLINo.CodeCode;
                oLabItem.Name = oNextLINo.NameCode;
                oLabItem.U_ObjectCd = sDocLine;//moWOQ.WOQItems.getValueAsString(iRow, IDH_WOQITEM._Code);
                oLabItem.U_ObjectType = sObjectType;// "WOQ";
                oLabItem.U_LICode = sICode;
                oLabItem.U_LIName = sIName;

                if (sUOM.Length > 0)
                    oLabItem.U_UOM = sUOM;

                if (sQTY.Length > 0)
                    oLabItem.U_Quantity = Convert.ToDouble(sQTY);

                if (sWhse.Length > 0)
                    oLabItem.U_NewWhse = sWhse;

                if (sComments.Length > 0)
                    oLabItem.U_Comments = sComments;

                oLabItem.U_CreatedBy = idh.bridge.DataHandler.INSTANCE.User;//com.idh.bridge.DataHandler.INSTANCE.SBOCompany.UserName;
                oLabItem.U_CreatedOn = DateTime.Now;

                //Add Lab Item Details 
                NumbersPair oNextLIDNo = oLIDetail.getNewKey();
                oLIDetail.Code = oNextLIDNo.CodeCode;
                oLIDetail.Name = oNextLIDNo.NameCode;
                oLIDetail.U_LabCd = oLabItem.Code;

                oLIDetail.U_PItemCd = sICode;
                oLIDetail.U_PItemNm = sIName;
                oLIDetail.U_UOM = sUOM;
                oLIDetail.U_Quantity = sQTY;
                //oLIDetail.U_Whse = oNewItemGridN.doGetFieldValue("NI.FrgnName", i).ToString();
                oLIDetail.doAddDataRow();

                oLabItem.U_Saved = iSaved;
                oLabItem.doAddDataRow();
                if (doCreateLabTask(ref oLabItem, sDocLine, sDocNum, sObjectType, sCardCode, sCardName, ref sSampleRef, ref sSampleStatus,
                    iSaved, sDept, sUser, sLabTasktoDuplicate, sDispRouteCode)) {
                    if (doUpdateLabItem)
                        oLabItem.doUpdateDataRow();
                    if (iSaved == 1 && !string.IsNullOrEmpty(sUser))
                        IDH_LABTASK.dosendAlerttoUser(sUser, com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTM1", null), com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTS1", null), false, sSampleRef);
                    return true;
                } else
                    return false;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doCreateLabItem()", "Error adding Lab Item: " + ex.Message);
                return false;
            }

        }

        private static bool doCreateLabTask(ref IDH_LABITM oLabItem, string sDocLine, string sDocNum, string sObjectType,
            string sCardCode, string sCardName, ref string SampleRef, ref string SampleStatus, int iSaved,
            string sDept, string sUser, string sLabTasktoDuplicate, string sDispRouteCode) {
            try {
                IDH_LABTASK oLabTask = new IDH_LABTASK();
                IDH_LABTASK oLabTaskSrc = new IDH_LABTASK();
                if (sLabTasktoDuplicate != "") {
                    sLabTasktoDuplicate = sLabTasktoDuplicate.Split('-')[0];
                    bool bRet = oLabTaskSrc.getByKey(sLabTasktoDuplicate);
                    if (bRet) {
                        IDH_LBTDPLG oDuplicateLabTaskSuffix = new IDH_LBTDPLG();

                        NumbersPair oNxtNoSufix = oDuplicateLabTaskSuffix.getNewKey();
                        oLabTask.Code = sLabTasktoDuplicate + "-" + oNxtNoSufix.CodeCode;
                        oLabTask.Name = sLabTasktoDuplicate + "-" + oNxtNoSufix.NameCode;
                        oLabTask.BlockChangeNotif = true;
                        try {
                            oLabTask.U_LabItemRCd = oLabItem.Code;
                            oLabTask.U_LabICd = oLabItem.U_LICode;
                            oLabTask.U_LabINm = oLabItem.U_LIName;

                            oLabTask.U_BPCode = sCardCode;// moWOQ.U_CardCd;
                            oLabTask.U_BPName = sCardName;// moWOQ.U_CardNM;

                            oLabTask.U_RowStatus = oLabTaskSrc.U_RowStatus;// "Requested";
                            oLabTask.U_DtRequested = oLabTaskSrc.U_DtRequested;// DateTime.Now;


                            //oLabTask.U_Department = "";
                            oLabTask.U_Department = oLabTaskSrc.U_Department;// sDept;
                            oLabTask.U_AssignedTo = oLabTaskSrc.U_AssignedTo;//sUser;
                            oLabTask.U_AnalysisReq = oLabTaskSrc.U_AnalysisReq;// "";
                            oLabTask.U_ExtStatus = oLabTaskSrc.U_ExtStatus;
                            oLabTask.U_ExtComments = oLabTaskSrc.U_ExtComments;
                            //oLabTask.U_AtcAbsEntry = -1;
                            oLabTask.U_ImpResults = oLabTaskSrc.U_ImpResults;
                            oLabTask.U_Decision = "Accepted";
                            oLabTask.U_ObjectCd = sDocLine; //moWOQ.WOQItems.getValue(iRow, IDH_WOQITEM._Code).ToString();
                            oLabTask.U_ObjectType = sObjectType;// "WOQ";
                            oLabTask.U_DocNum = sDocNum;// "Header Num";

                            oLabTask.U_DtReceived = oLabTaskSrc.U_DtReceived;
                            oLabTask.U_DtAnalysed = oLabTaskSrc.U_DtAnalysed;
                            oLabTask.U_ExtTsDt = oLabTaskSrc.U_ExtTsDt;
                            oLabTask.U_DipRtCd = sDispRouteCode;


                            oLabTask.U_Saved = iSaved;
                        } finally {
                            oLabTask.BlockChangeNotif = false;
                        }
                        oLabTask.doAddDataRow();

                        oLabItem.U_LabTask = oLabTask.Code;
                        //iLabTaskCd = Convert.ToInt32(oLabTask.Code);

                        //Also update the WOQ row with LabTask ID 
                        SampleRef = oLabTask.Code;
                        SampleStatus = "Accepted";// moWOQ.WOQItems.setValue(IDH_WOQITEM._SampStatus, "Pending");
                        IDH_LBTKPRG oLabTskPrgOrg = new IDH_LBTKPRG();
                        int iRet = oLabTskPrgOrg.getByField(IDH_LBTKPRG._TaskCd, sLabTasktoDuplicate);
                        if (iRet > 0) {
                            IDH_LBTKPRG oLabTskPrg = new IDH_LBTKPRG();
                            oLabTskPrgOrg.first();
                            while (oLabTskPrgOrg.next()) {
                                oNxtNoSufix = oLabTskPrg.getNewKey();
                                oLabTskPrg.doAddEmptyRow(true);
                                oLabTskPrg.U_AnalysisCd = oLabTskPrgOrg.U_AnalysisCd;
                                oLabTskPrg.U_Comments = oLabTskPrgOrg.U_Comments;
                                oLabTskPrg.U_Select = oLabTskPrgOrg.U_Select;
                                oLabTskPrg.U_Status = oLabTskPrgOrg.U_Status;
                                oLabTskPrg.U_TaskCd = oLabTask.Code;
                            }
                            oLabTskPrg.doProcessData();
                        }


                    } else {
                        com.idh.bridge.DataHandler.INSTANCE.doError("doCreateLabTask()", "Error duplicating lab task: Unable to get the orignal lab task#" + sLabTasktoDuplicate);
                        return false;
                    }

                } else {
                    NumbersPair oNxtNo = oLabTask.getNewKey();
                    oLabTask.Code = oNxtNo.CodeCode;
                    oLabTask.Name = oNxtNo.NameCode;

                    oLabTask.U_LabItemRCd = oLabItem.Code;
                    oLabTask.U_LabICd = oLabItem.U_LICode;
                    oLabTask.U_LabINm = oLabItem.U_LIName;

                    oLabTask.U_BPCode = sCardCode;// moWOQ.U_CardCd;
                    oLabTask.U_BPName = sCardName;// moWOQ.U_CardNM;

                    oLabTask.U_RowStatus = "Requested";
                    oLabTask.U_DtRequested = DateTime.Now;

                    //oLabTask.U_DtAnalysed = DateTime.Now; //will be updated by user 

                    //oLabTask.U_Department = "";
                    oLabTask.U_Department = sDept;
                    oLabTask.U_AssignedTo = sUser;
                    oLabTask.U_AnalysisReq = "";
                    oLabTask.U_ExtStatus = "";
                    oLabTask.U_ExtComments = "";
                    //oLabTask.U_AtcAbsEntry = -1;
                    oLabTask.U_ImpResults = "";
                    oLabTask.U_Decision = "Pending";
                    oLabTask.U_ObjectCd = sDocLine; //moWOQ.WOQItems.getValue(iRow, IDH_WOQITEM._Code).ToString();
                    oLabTask.U_ObjectType = sObjectType;// "WOQ";
                    oLabTask.U_DocNum = sDocNum;// "Header Num";
                    oLabTask.U_DipRtCd = sDispRouteCode;

                    oLabTask.U_Saved = iSaved;
                    oLabTask.doAddDataRow();

                    oLabItem.U_LabTask = oLabTask.Code;
                    //iLabTaskCd = Convert.ToInt32(oLabTask.Code);

                    //Also update the WOQ row with LabTask ID 
                    SampleRef = oLabTask.Code;
                    SampleStatus = "Pending";// moWOQ.WOQItems.setValue(IDH_WOQITEM._SampStatus, "Pending");
                }
                return true;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doCreateLabTask()", "Error adding Lab Task row: " + ex.Message);
                //iLabTaskCd = -1;
                return false;
            }
        }

        public static bool doCheckIfLabTaskRequired(string sWasCode) {
            if (Config.INSTANCE.getParameterAsBool("ACTSMPRQ", false)) {
                string sSampleReq = Config.INSTANCE.getValueFromOITMWithNoBuffer(sWasCode, "U_SAMPLEREQ").ToString();
                if (sSampleReq != null && sSampleReq == "Y") {
                    return true;
                } else
                    return false;
            } else
                return false;
        }

#endregion
    }
}
