using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using IDHAddOns.idh.controls;
using com.idh.bridge.data;
using com.idh.dbObjects;
using com.idh.dbObjects.User;
using com.idh.bridge;
using com.idh.bridge.lookups;


namespace com.isb.forms.Enquiry {
    public class WORList : IDHAddOns.idh.forms.Base {

        protected string msKeyGen = "GENSEQ";

        

        #region Initialization

        public WORList(IDHAddOns.idh.addon.Base oParent)
             : base(oParent, "IDHFNBATHWORS", 0, "admin.srf", false, true, false, "Linked WOR(s)") {
        }

        protected override void doSetEventFilters() {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK);
        }
        public override void doClose() {
           // throw new NotImplementedException();
        }
        #endregion
        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            try {
                oForm.Title = gsTitle;
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;

                //Grid 
                SAPbouiCOM.Item oItem;
                oItem = oForm.Items.Item("IDH_CONTA");
                UpdateGrid oWHSGrid = new UpdateGrid(this, oForm, "LINESGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane);
                oWHSGrid.getSBOItem().AffectsFormMode = false;

                base.doCompleteCreate(ref oForm, ref BubbleEvent);

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
                BubbleEvent = false;
            }
        }
        #region FormnDataFunctions
        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {

            try {
                FilterGrid oGridN = (FilterGrid)FilterGrid.getInstance(oForm, "LINESGRID");
                if (oGridN == null)
                    oGridN = new UpdateGrid(this, oForm, "LINESGRID");

                oGridN.doAddGridTable(new GridTable( IDH_JOBSHD.TableName , "r", IDH_JOBSHD._Code, false), true);
                string sFinalBatchCode = getParentSharedData(oForm, "FINALBATCHCODE").ToString();
                if (sFinalBatchCode == null || sFinalBatchCode == string.Empty)
                    sFinalBatchCode = "-1";
                oForm.Title = "Linked WOR(s) For Batch#" + sFinalBatchCode;
                oGridN.setRequiredFilter(" isnull(" + IDH_JOBSHD._LABFBTCD + ",\'\')!=\'\' And " + IDH_JOBSHD._RowSta + "!=\'" + FixedValues.getStatusDeleted() + "\' And "+ IDH_JOBSHD._LABFBTCD + "=\'"+ sFinalBatchCode  +"\'");
                oGridN.setOrderValue("r.Code  For Browse ");
                oGridN.AddEditLine = false;
                oGridN.doSetDoCount(true);
                
                oGridN.doAddListField("r." + IDH_JOBSHD._JobNr, "WOH Code", false, -1, null, null);
                oGridN.doAddListField("r." + IDH_JOBSHD._Code, "WOR Code", false, -1, null, null);
                oGridN.doAddListField("r." + IDH_JOBSHD._ItemCd, "Container", false, -1, null, null);
                oGridN.doAddListField("r." + IDH_JOBSHD._ItemDsc, "Container Desc", false, -1, null, null);
                oGridN.doAddListField("r." + IDH_JOBSHD._CarWgt, "Container Qty", false, -1, null, null);
                oGridN.doAddListField("r." + IDH_JOBSHD._BDate, "Create Date", false, -1, null, null);
                oGridN.doAddListField("r." + IDH_JOBSHD._ASDate, "Start Date", false, -1, null, null);
                oGridN.doAddListField("r." + IDH_JOBSHD._AEDate, "End Date", false, -1, null, null);
         
                oGridN.doApplyRules();
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }
        }
        
        /* 
         * Load the Form Data
         */
        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            try {
                FilterGrid oGridN = (FilterGrid)FilterGrid.getInstance(oForm, "LINESGRID");
                oGridN.doReloadData("", false, false);
                oGridN.doApplyRules();

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doLoadData()", "Error in Load Data: " + ex.Message);
            }
        }
        #endregion
        #region Events
        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            try {
                if (!pVal.BeforeAction && pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK ) {
                    FilterGrid oGridN = FilterGrid.getInstance(oForm, "LINESGRID");
                    if (oGridN.getRowCount() == 0)
                        return false;
                    if (pVal.ColUID == "r." + IDH_JOBSHD._JobNr  && oGridN.doGetFieldValue("r." + IDH_JOBSHD._JobNr, pVal.Row).ToString() != string.Empty) {
                        //open WOH Form 
                        ArrayList oData = new ArrayList();
                        oData.Add("DoUpdate");
                        oData.Add(oGridN.doGetFieldValue("r." + IDH_JOBSHD._JobNr, pVal.Row).ToString().Trim());
                        goParent.doOpenModalForm("IDH_WASTORD", oForm, oData);
                    } else if(oGridN.doGetFieldValue("r." + IDH_JOBSHD._Code, pVal.Row).ToString() != string.Empty){
                        //open WOR Form 
                        ArrayList oData = new ArrayList();
                        oData.Add("DoUpdate");
                        oData.Add(oGridN.doGetFieldValue("r." + IDH_JOBSHD._Code, pVal.Row).ToString().Trim());
                        setSharedData(oForm, "ROWCODE", oGridN.doGetFieldValue("r." + IDH_JOBSHD._Code, pVal.Row).ToString().Trim());
                        goParent.doOpenModalForm("IDHJOBS", oForm, oData);
                    }
                }  

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXITME", null);
            }
            return base.doCustomItemEvent(oForm, ref pVal);
        }

        #endregion

    }
}
