using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

using com.idh.forms.oo;
using com.idh.bridge.data;
using com.isb.enq.dbObjects.User;
using com.idh.dbObjects.User;
using IDHAddOns.idh.controls;
using com.uBC.utils;
using com.idh.controls;
using com.idh.bridge.utils;
using com.uBC.forms.fr3;
using com.idh.dbObjects.strct;
using com.idh.bridge.lookups;
//using com.idh.dbObjects.Base;
using com.idh.bridge;
using WR1_FR2Forms.idh.forms.fr2.prices;
//using com.uBc.forms;
namespace com.isb.forms.Enquiry.admin {
    public class WasteItemValidations : com.idh.forms.oo.Form {

        private Hashtable clsValidatioControls = new Hashtable();
        Hashtable clsCFLExpressions = new Hashtable();

        protected class ValidationControls {
            public ValidationControls() {
                ValidationCode = "";
                DBField = "";
                sExpression = "";
                ControlID = new ArrayList();
            }
            public string sExpression;
            public string ValidationCode;
            public string DBField;
            public ArrayList ControlID;
            public SAPbouiCOM.BoFormItemTypes ControlType;
        }

        public enum DataType {
            AlphaNumeric=1 ,
            Numeric=2, 
            Decimal=3,
            Quantity=4,
            Price=5,
            Text=6,
            Percentage=7
        }
        public enum FieldType {
            Field=1,
            Range=2,
            Boolean=3,
            Combo=4,
            Checkbox=5,
        }
        private string sDefaultNewControl = "";
        private IDH_WITMVLD moCreateWasteItemValidation;
        private bool bManualItemCode = true;
        private string[] FixedItems_SBO = { "1", "2", "ItemCode", "ItemName", "FrgnName", "IDH_WSTGRP", "InvntItem", "SellItem", 
                                              "PrchseItem", "BuyUnitMsr", "SalUnitMsr", "InvntryUom" };
        private string[] FixedItems_UDF = { "WPTemplate", "Approved", "LOFile", "Hazordous", "WPTUser", "AppUser", "LOFUser", "IDHCWBB", "IDHREB", "IDHCREB", "IDHCREB" 
                                          , "WTITMLN","HAZCD","DCODE","WTDOCUM","EWC","RCODE"};
        private string[] CommonUFItems ={"InvntItem", "SellItem", "PrchseItem", "BuyUnitMsr", "SalUnitMsr", "InvntryUom", "WPTemplate", "Approved", "LOFile", "Hazordous", 
                                        "WPTUser", "AppUser", "LOFUser", "IDHCWBB", "IDHREB", "IDHCREB", "IDHCREB" 
                                        , "WTITMLN","HAZCD","DCODE","WTDOCUM","EWC","RCODE" };
        private string[] CommonUFForOITM ={"InvntItem", "SellItem", "PrchseItem", "BuyUnitMsr", "SalUnitMsr", "InvntryUom", "U_WPTemplate", "U_Approved", "U_LOFile", "U_Hazordous", 
                                        "U_WPTUser", "U_AppUser", "U_LOFUser", "U_IDHCWBB", "U_IDHREB", "U_IDHCREB", "U_IDHCREB" 
                                        , "U_WTITMLN","U_HAZCD","U_DCODE","U_WTDOCUM","U_EWC","U_RCODE" };
        //Select IsManual from NNM1,ONNM where ONNM.ObjectCode='2' And NNM1.ObjectCode='2' and ONNM.DfltSeries=NNM1.Series and NNM1.DocSubType='C'
        //private IDH_CREATEBP moCreateNewBP;
        //public IDH_CREATEBP ObjCreateNewBP {
        //    get { return (IDH_CREATEBP)moCreateNewBP; }
        //}
        public WasteItemValidations(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
            doInitialize();
            doSetHandlers();
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_WGITM", sParMenu, iMenuPos, "Enq_Waste Item Validations.srf", false, true, false, "Waste Item Validations", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        protected void doInitialize() {
            moCreateWasteItemValidation = new IDH_WITMVLD();
        }



        #region Properties
        private int _iParentRow = -1;
        public int iParentRow {
            get { return _iParentRow; }
            set { _iParentRow = value; }
        }
        private string _ItemCode = "";
        public string ItemCode {
            get { return _ItemCode; }
            set { _ItemCode = value; }
        }

        private string _ItemName = "";
        public string ItemName {
            get { return _ItemName; }
            set { _ItemName = value; }
        }
        private string _ItemFName = "";
        public string ItemFName {
            get { return _ItemFName; }
            set { _ItemFName = value; }
        }
        private string _WasteGroupCode = "";
        public string WasteGroupCode {
            get { return _WasteGroupCode; }
            set { _WasteGroupCode = value; }
        }

        private string _EnquiryID = "";
        public string EnquiryID {
            get { return _EnquiryID; }
            set { _EnquiryID = value; }
        }
        private string _sEnquiryLineID = "";
        public string sEnquiryLineID {
            get { return _sEnquiryLineID; }
            set { _sEnquiryLineID = value; }
        }
        private string _WOQID = "";
        public string WOQID {
            get { return _WOQID; }
            set { _WOQID = value; }
        }
        private string _sWOQLineID = "";
        public string sWOQLineID {
            get { return _sWOQLineID; }
            set { _sWOQLineID = value; }
        }
        private SAPbouiCOM.BoFormMode _ParentFormMode;
        public SAPbouiCOM.BoFormMode ParentFormMode {
            get { return _ParentFormMode; }
            set { _ParentFormMode = value; }
        }

        private bool _bUpdateParent = false;
        public bool bUpdateParent {
            get { return _bUpdateParent; }
            set { _bUpdateParent = value; }
        }

        private string _AddItm = "";
        public string AddItm {
            get { return _AddItm; }
            set { _AddItm = value; }
        }

        private string _CardCode = "";
        public string CardCode {
            get { return _CardCode; }
            set { _CardCode = value; }
        }

        private string _CardName= "";
        public string CardName {
            get { return _CardName; }
            set { _CardName = value; }
        }


        #endregion

        #region FormOpenCreateFunctions
        /** 
         * This Function will be called by the controller to allow the class to last minute Form Layout adjustments before it gets displayed
         * All changes made in hete will be cached if this is a cached form, the method will not be called again once the form has benn cached.
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            base.doCompleteCreate(ref BubbleEvent);
            SAPbobsCOM.Recordset oRecordSet = null;

            try {

                string sQry = "Select * from [@IDH_WGVDMS] where U_Common='Y'";

                oRecordSet = DataHandler.INSTANCE.doSBOSelectQuery("doGetCommonWGValidationList", sQry);
                if (oRecordSet != null && oRecordSet.RecordCount > 0) {
                    int iType, iDataType, iFields,iLength;
                    string sExpression = "", sDBField = "";
                    //string sValue1 = "";
                    //string sValue2 = "";
                    for (int iRecord = 0; iRecord <= oRecordSet.RecordCount - 1; iRecord++) {
                        iType = Convert.ToInt32(oRecordSet.Fields.Item(IDH_WGVDMS._Type).Value);
                        iDataType = Convert.ToInt32(oRecordSet.Fields.Item(IDH_WGVDMS._DtType).Value);
                        iFields = Convert.ToInt32(oRecordSet.Fields.Item(IDH_WGVDMS._Fields).Value);
                        sExpression = Convert.ToString(oRecordSet.Fields.Item(IDH_WGVDMS._Expression).Value);
                        sDBField = Convert.ToString(oRecordSet.Fields.Item(IDH_WGVDMS._DBField).Value).Replace("U_", "");
                        iLength = Convert.ToInt16(oRecordSet.Fields.Item(IDH_WGVDMS._Length).Value);
                        if (FixedItems_SBO.Contains(sDBField) || (FixedItems_UDF.Contains(sDBField))) {
                            doAddUF(sDBField, iLength, (DataType)iDataType); 
                            if (iType == (Int16)FieldType.Combo) {
                                SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)SBOForm.Items.Item(sDBField).Specific;
                                SBOForm.Items.Item(sDBField).DisplayDesc = true;
                                FillCombobox(oCombo, sExpression);
                            }
                            //if (sExpression.StartsWith("CFL:")) {
                            //    clsCFLExpressions.Add(sValidationCode, sExpression);
                            //}
                        } //else if (FixedItems_UDF.Contains(sValidationCode)){
                        
                      //  }

                        oRecordSet.MoveNext();
                    }
                }
                doAddUF("ItemCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 15);
                doAddUF("ItemName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100);
                doAddUF("FrgnName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100);
                doAddUF("IDH_WSTGRP", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50);//Waste Grouo Combo

                //doAddUF("InvntItem", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);//Waste Grouo Combo
                //doAddUF("PrchseItem", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);//Waste Grouo Combo
                //doAddUF("IDHREB", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);//Waste Grouo Combo
                //doAddUF("WPTemplate", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);//Waste Grouo Combo
                doAddUF("IDH_ENQID", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 8);

                doConfigureForm();

                
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", new string[] { "doCompleteCreate" });
            } finally {
                DataHandler.INSTANCE.doReleaseRecordset(ref oRecordSet);
            }
            AutoManaged = false;
            /*Items.Item("1").AffectsFormMode = false;
            for (int i = 0; i <= Items.Count - 1; i++) {
                Items.Item(i).AffectsFormMode = false;
            }*/
        }
        private void doConfigureForm() {
            //Note: Need to clean that function 
            //adding Items need to move in function
            //will do later 
            SAPbobsCOM.Recordset oRecordSet = null;
            SAPbobsCOM.Recordset oRecordSetValues = null;

            try {
                clsValidatioControls.Clear();
                string sQry = "Select a.*,b." + IDH_WGVDMS._DtType + ",b." + IDH_WGVDMS._Fields + ",b." + IDH_WGVDMS._Type
                    + ", b." + IDH_WGVDMS._DBField + ", b." + IDH_WGVDMS._Length + ", b." + IDH_WGVDMS._Expression + ", b." + IDH_WGVDMS._Common
                    + " from [" + IDH_WGVALDS.TableName + "] a,[" + IDH_WGVDMS.TableName + "] b Where a." + IDH_WGVALDS._ValidCd + "=b." + IDH_WGVDMS._ValidCd
                    + " And  a." + IDH_WGVALDS._WstGpCd + "='" + _WasteGroupCode + "' And b." + IDH_WGVDMS ._Common+ "<>'Y' And IsNull(a." + IDH_WGVALDS._Select + ",'N')='Y' Order by "
                    + IDH_WGVALDS._Sort;
                oRecordSet = DataHandler.INSTANCE.doSBOSelectQuery("doGetWasteGroupValidationList", sQry);
                int top = SBOForm.Items.Item("lblTechInf").Top + SBOForm.Items.Item("lblTechInf").Height + 5;
                int lblleft = 5;
                int txtleft = SBOForm.Items.Item("ItemCode").Left;
                int lblWidth = 150;//SBOForm.Items.Item("6").Width + 10;
                int txtwidth = SBOForm.Items.Item("BuyUnitMsr").Width;
                int height = SBOForm.Items.Item("lblTechInf").Height;
                int maxtop = SBOForm.Items.Item("1").Top-height-5;
                ValidationControls obj;
                SAPbouiCOM.StaticText oLabel;
                //SAPbouiCOM.EditText oTextbox;
                SAPbouiCOM.ComboBox oCombo;
                SAPbouiCOM.CheckBox oCheckBox;
                SAPbouiCOM.Item oItem;
                if (oRecordSet != null && oRecordSet.RecordCount > 0) {
                    int iFields, iFieldType, iDataType; 
                    FieldType oFieldType;
                    DataType oDataType;
                    int iLength = 0;
                    string sLableUID, sItemUID;
                    string sDBField = "";
                    string sListofValues = "";//or say it expression
                    string sValidationCode = "";//Unique ID
                    string sValidationName = "";//Unique ID
                    string sValue1 = "";
                    string sValue2 = "";
                    //string sCommon = "";
                    for (int iRecord = 0; iRecord <= oRecordSet.RecordCount - 1; iRecord++) {
                        iFieldType = Convert.ToInt16(oRecordSet.Fields.Item(IDH_WGVDMS._Type).Value);
                        iDataType = Convert.ToInt16(oRecordSet.Fields.Item(IDH_WGVDMS._DtType).Value);
                        iFields = Convert.ToInt32(oRecordSet.Fields.Item(IDH_WGVDMS._Fields).Value);
                        iLength = Convert.ToInt32(oRecordSet.Fields.Item(IDH_WGVDMS._Length).Value);
                        sListofValues = Convert.ToString(oRecordSet.Fields.Item(IDH_WGVDMS._Expression).Value);
                        sValidationCode = Convert.ToString(oRecordSet.Fields.Item(IDH_WGVALDS._ValidCd).Value);
                        sValidationName= Convert.ToString(oRecordSet.Fields.Item(IDH_WGVALDS._ValidNm).Value);
                        sDBField = Convert.ToString(oRecordSet.Fields.Item(IDH_WGVDMS._DBField).Value);
                        //sCommon= Convert.ToString(oRecordSet.Fields.Item(IDH_WGVDMS._Common).Value);
                        sValue1 = "";
                        sValue2 = "";
                        oFieldType = (FieldType)iFieldType;
                        oDataType = (DataType)iDataType;
                        //if (_ItemCode != string.Empty) {
                        sQry = "Select a.* from [" + IDH_WITMVLD.TableName + "] a WITH(NOLOCK) Where a." + IDH_WITMVLD._ValidCd + "='" + sValidationCode + "' "
                            //+ " And a." + IDH_WITMVLD._ItemCode + "='" + _ItemCode + "' " 
                            + " And  a." + IDH_WITMVLD._WstGpCd + "='" + _WasteGroupCode + "' ";
                        if (_EnquiryID != string.Empty)
                            sQry += " And (a." + IDH_WITMVLD._EnqId + "='" + _EnquiryID + "' And a." + IDH_WITMVLD._EnqLID + "='" + _sEnquiryLineID + "')";
                        else if (_WOQID != string.Empty)
                            sQry += " And (a." + IDH_WITMVLD._WOQId + "='" + _WOQID + "' And a." + IDH_WITMVLD._WOQLId + "='" + _sWOQLineID + "')";
                        oRecordSetValues = DataHandler.INSTANCE.doSBOSelectQuery("doGetWasteItemValuesByEnquiry", sQry);
                        if (oRecordSetValues != null && oRecordSetValues.RecordCount > 0) {
                            sValue1 = Convert.ToString(oRecordSetValues.Fields.Item(IDH_WITMVLD._Value1).Value);
                            sValue2 = Convert.ToString(oRecordSetValues.Fields.Item(IDH_WITMVLD._Value2).Value);
                        }
                        sLableUID = "";
                        sItemUID = "";
                        switch (oFieldType) {
                            case FieldType.Field:
                                //Field
                                sLableUID ="lbl" + iRecord.ToString();
                                sItemUID="IDH" + iRecord.ToString() + "_1";
                                SBOForm.Items.Add(sLableUID, SAPbouiCOM.BoFormItemTypes.it_STATIC);
                                SBOForm.Items.Add(sItemUID, SAPbouiCOM.BoFormItemTypes.it_EDIT);
                                if (sDefaultNewControl == string.Empty)
                                    sDefaultNewControl = sItemUID;
                                oItem = SBOForm.Items.Item(sLableUID);
                                oItem.Left = lblleft;
                                oItem.Top = top;
                                oItem.Width = lblWidth;
                                oItem.Height = height;
                                oItem.LinkTo = sItemUID ;
                                oItem.Description=sValidationCode;
                                oItem.DisplayDesc = true;
                                oLabel = (SAPbouiCOM.StaticText)oItem.Specific;
                                oLabel.Caption = sValidationName;


                                oItem = SBOForm.Items.Item(sItemUID);
                                oItem.Left = txtleft;
                                oItem.Top = top;
                                oItem.Width = txtwidth;
                                oItem.Height = height;
                                doAddUF(sItemUID, iLength, oDataType);
                                //if (iDataType == DataType.AlphaNumeric) //AlphaNumeric
                                //    doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, iLength);
                                //else if (iDataType == DataType.Numeric) //Numeric
                                //    doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_LONG_NUMBER, iLength);
                                //else if (iDataType == DataType.Decimal) //Decimal
                                //    doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_MEASURE, iLength);
                                //else if (iDataType == DataType.Percentage) //Percentage
                                //    doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_PERCENT, iLength);
                                //else if (iDataType == DataType.Price) //Price
                                //    doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_PRICE, iLength);
                                //else if (iDataType == DataType.Quantity) //Quantity
                                //    doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_QUANTITY, iLength);
                                //else if (iDataType == DataType.Text) //Text
                                //    doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_LONG_TEXT, iLength);    
                            //ArrayList arry = new ArrayList();
                                obj = new ValidationControls();
                                obj.ControlType = SAPbouiCOM.BoFormItemTypes.it_EDIT;
                                obj.ControlID.Add(sItemUID);//"IDH" + iRecord.ToString() + "_1"
                                obj.ValidationCode = (sValidationCode);
                                obj.DBField = (sDBField);
                                obj.sExpression = sListofValues.Trim();

                                clsValidatioControls.Add(sDBField, obj);

                                  if (sListofValues.Trim()!=string.Empty){
                                    string[] aExpression = sListofValues.Split(':');
                                    if (aExpression[0].Trim().ToUpper()=="CFL"){//Added CFL control
                                      oItem = SBOForm.Items.Add(aExpression[1],SAPbouiCOM.BoFormItemTypes.it_BUTTON);

                                    oItem.Left = txtleft+txtwidth+1;
                                    oItem.Top = top;
                                    oItem.Width = 14;
                                    oItem.Height = height;
                                    SAPbouiCOM.Button oButton =(SAPbouiCOM.Button) oItem.Specific;
                                    oButton.Type = SAPbouiCOM.BoButtonTypes.bt_Image;
                                    oButton.Image = IDHAddOns.idh.addon.Base.doFixImagePath("CFL.bmp");
                                    addHandler_ITEM_PRESSED(oItem.UniqueID, doHandleCFLButtonClick);
                                  clsCFLExpressions.Add(oItem.UniqueID,obj);}
                                  }

                                if (top + height + 2 + height + 2 > maxtop) {
                                    top = SBOForm.Items.Item("lblTechInf").Top + SBOForm.Items.Item("lblTechInf").Height + 5;
                                    lblleft = txtleft + txtwidth + 18;
                                    txtleft = lblleft + lblWidth + 10;
                                } else
                                    top += height + 2;

                                if (sValue1 != string.Empty) setUFValue(sItemUID, sValue1);
                              
                                break;
                            case FieldType.Range:                               
                                    //not in use at the moment
                                //Range
                                //if (false) {
                                //    SBOForm.Items.Add("lbl" + iRecord.ToString(), SAPbouiCOM.BoFormItemTypes.it_STATIC);
                                //    oItem = SBOForm.Items.Item("lbl" + iRecord.ToString());
                                //    oItem.Left = lblleft;
                                //    oItem.Top = top;
                                //    oItem.Width = lblWidth;
                                //    oItem.Height = height;
                                //    oItem.LinkTo = "IDH" + iRecord.ToString() + "_1";
                                //    oItem.Description = sValidationCode;
                                //    oItem.DisplayDesc = true;
                                //    oLabel = (SAPbouiCOM.StaticText)oItem.Specific;
                                //    oLabel.Caption = sValidationName;
                                //    if (iFields == 1) {
                                //        SBOForm.Items.Add("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoFormItemTypes.it_EDIT);
                                //        oItem = SBOForm.Items.Item("IDH" + iRecord.ToString() + "_1");
                                //        if (sDefaultNewControl == string.Empty)
                                //            sDefaultNewControl = "IDH" + iRecord.ToString() + "_1";
                                //        oItem.Left = txtleft;
                                //        oItem.Top = top;
                                //        oItem.Width = txtwidth;
                                //        oItem.Height = height;
                                //        SBOForm.Items.Item("lbl" + iRecord.ToString()).LinkTo = oItem.UniqueID;
                                //        doAddUF("IDH" + iRecord.ToString() + "_1", iLength, oDataType);
                                        
                                //        obj = new ValidationControls();
                                //        obj.ControlType = SAPbouiCOM.BoFormItemTypes.it_EDIT;
                                //        obj.ControlID.Add("IDH" + iRecord.ToString() + "_1");
                                //        obj.ValidationCode = (sValidationCode);
                                //        obj.DBField = (sDBField);
                                //        obj.sExpression = sListofValues.Trim();
                                //        clsValidatioControls.Add(sDBField, obj);

                                //        if (sValue1 != string.Empty) setUFValue("IDH" + iRecord.ToString() + "_1", sValue1);
                                //    } else {
                                //        int _txtwidth = (txtwidth / iFields) - 2;
                                //        int _txtleft = txtleft;
                                //        obj = new ValidationControls();
                                //        for (int iCtrls = 1; iCtrls <= iFields; iCtrls++) {
                                //            SBOForm.Items.Add("IDH" + iRecord.ToString() + "_" + iCtrls.ToString(), SAPbouiCOM.BoFormItemTypes.it_EDIT);
                                //            oItem = SBOForm.Items.Item("IDH" + iRecord.ToString() + "_" + iCtrls.ToString());
                                //            SBOForm.Items.Item("lbl" + iRecord.ToString()).LinkTo = oItem.UniqueID;
                                //            //oItem.LinkTo= SBOForm.Items.Item("IDH" + iRecord.ToString() + "_" + iCtrls.ToString())
                                //            oItem.Left = _txtleft;
                                //            oItem.Top = top;
                                //            oItem.Width = _txtwidth;
                                //            oItem.Height = height;

                                //            doAddUF("IDH" + iRecord.ToString() + "_" + iCtrls.ToString(), iLength, oDataType);
                                //            //if (iDataType == 2) //Numeric
                                //            //    doAddUF("IDH" + iRecord.ToString() + "_" + iCtrls.ToString(), SAPbouiCOM.BoDataType.dt_LONG_NUMBER, 11);
                                //            //else if (iDataType == 3) //Decimal
                                //            //    doAddUF("IDH" + iRecord.ToString() + "_" + iCtrls.ToString(), SAPbouiCOM.BoDataType.dt_MEASURE, 11);
                                //            _txtleft += _txtwidth + 2;
                                //            obj.ControlType = SAPbouiCOM.BoFormItemTypes.it_EDIT;
                                //            obj.ControlID.Add("IDH" + iRecord.ToString() + "_" + iCtrls.ToString());
                                //            obj.ValidationCode = (sValidationCode);


                                //            if (sValue1 != string.Empty) setUFValue("IDH" + iRecord.ToString() + "_1", sValue1);
                                //            if (sValue1 != string.Empty) setUFValue("IDH" + iRecord.ToString() + "_2", sValue2);
                                //        }
                                //        if (sDefaultNewControl == string.Empty)
                                //            sDefaultNewControl = "IDH" + iRecord.ToString() + "_1";
                                //        clsValidatioControls.Add(sDBField, obj);
                                //    }
                                //    if (top + height + 2 + height + 2 > maxtop) {
                                //        top = SBOForm.Items.Item("lblTechInf").Top + SBOForm.Items.Item("lblTechInf").Height + 5;
                                //        lblleft = txtleft + txtwidth + 15;
                                //        txtleft = lblleft + lblWidth + 10;
                                //    } else
                                //        top += height + 2;
                                //}
                                break;
                            //End of Range
                            case FieldType.Boolean:                           
                                //Boolean
                                //Combo
                                sLableUID ="lbl" + iRecord.ToString();
                                sItemUID="IDH" + iRecord.ToString() + "_1";

                                SBOForm.Items.Add(sLableUID, SAPbouiCOM.BoFormItemTypes.it_STATIC);
                                SBOForm.Items.Add(sItemUID, SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX);
                                if (sDefaultNewControl == string.Empty)
                                    sDefaultNewControl = sItemUID;
                                oItem = SBOForm.Items.Item(sLableUID);
                                oItem.Left = lblleft;
                                oItem.Top = top;
                                oItem.Width = lblWidth;
                                oItem.Height = height;
                                oItem.LinkTo = sItemUID;
                                oItem.Description=sValidationCode;
                                oItem.DisplayDesc = true;                               
                                oLabel = (SAPbouiCOM.StaticText)oItem.Specific;
                                oLabel.Caption = sValidationName;

                                oItem = SBOForm.Items.Item(sItemUID);
                                oItem.Left = txtleft;
                                oItem.Top = top;
                                oItem.Width = txtwidth;
                                oItem.Height = height;

                                doAddUF(sItemUID, iLength, oDataType);

                                oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
                                oItem.DisplayDesc = true;
                                oCombo.ValidValues.Add("Y", "Yes");
                                oCombo.ValidValues.Add("N", "No");

                                obj = new ValidationControls();
                                obj.ControlType = SAPbouiCOM.BoFormItemTypes.it_EDIT;
                                obj.ControlID.Add(sItemUID);
                                obj.ValidationCode = (sValidationCode);
                                obj.DBField = (sDBField);
                                obj.sExpression = sListofValues.Trim();
                                clsValidatioControls.Add(sDBField, obj);


                                if (top + height + 2 + height + 2 > maxtop) {
                                    top = SBOForm.Items.Item("lblTechInf").Top + SBOForm.Items.Item("lblTechInf").Height + 5;
                                    lblleft = txtleft + txtwidth + 15;
                                    txtleft = lblleft + lblWidth + 10;
                                } else
                                    top += height + 2;
                                if (sValue1 != string.Empty) setUFValue(sItemUID, sValue1);
                                break;
                            //End of Boolean
                            case FieldType.Combo:
                                //Combo
                                sLableUID ="lbl" + iRecord.ToString();
                                sItemUID="IDH" + iRecord.ToString() + "_1";

                                SBOForm.Items.Add(sLableUID , SAPbouiCOM.BoFormItemTypes.it_STATIC);
                                SBOForm.Items.Add(sItemUID, SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX);
                                if (sDefaultNewControl == string.Empty)
                                    sDefaultNewControl = sItemUID;
                                oItem = SBOForm.Items.Item(sLableUID );
                                oItem.Left = lblleft;
                                oItem.Top = top;
                                oItem.Width = lblWidth;
                                oItem.Height = height;
                                oItem.LinkTo = sItemUID;
                                oItem.Description=sValidationCode;
                                oItem.DisplayDesc = true;                               
                                oLabel = (SAPbouiCOM.StaticText)oItem.Specific;
                                oLabel.Caption = sValidationName;

                                oItem = SBOForm.Items.Item(sItemUID);
                                oItem.Left = txtleft;
                                oItem.Top = top;
                                oItem.Width = txtwidth;
                                oItem.Height = height;

                                doAddUF(sItemUID, iLength, oDataType);
                                
                                oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
                                oItem.DisplayDesc = true;
                                FillCombobox(oCombo, sListofValues);    
                            
                                obj = new ValidationControls();
                                obj.ControlType = SAPbouiCOM.BoFormItemTypes.it_EDIT;
                                obj.ControlID.Add(sItemUID);
                                obj.ValidationCode = (sValidationCode);
                                obj.DBField = (sDBField);
                                obj.sExpression = sListofValues.Trim();
                                clsValidatioControls.Add(sDBField, obj);
                                
                                if (top + height + 2 + height + 2 > maxtop) {
                                    top = SBOForm.Items.Item("lblTechInf").Top + SBOForm.Items.Item("lblTechInf").Height + 5;
                                    lblleft = txtleft + txtwidth + 15;
                                    txtleft = lblleft + lblWidth + 10;
                                } else
                                    top += height + 2;
                                if (sValue1 != string.Empty) setUFValue(sItemUID, sValue1);
                                break;
                            // End of Combo
                            case FieldType.Checkbox:
                                //Checkbox
                                SBOForm.Items.Add(sLableUID , SAPbouiCOM.BoFormItemTypes.it_STATIC);
                                SBOForm.Items.Add(sItemUID, SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX);
                                if (sDefaultNewControl == string.Empty)
                                    sDefaultNewControl = sItemUID;
                                oItem = SBOForm.Items.Item(sLableUID );
                                oItem.Left = lblleft;
                                oItem.Top = top;
                                oItem.Width = lblWidth;
                                oItem.Height = height;
                                oItem.LinkTo = sItemUID;
                                oItem.Description=sValidationCode;
                                oItem.DisplayDesc = true;                                
                                oLabel = (SAPbouiCOM.StaticText)oItem.Specific;
                                oLabel.Caption = sValidationName;

                                oItem = SBOForm.Items.Item(sItemUID);
                                oItem.Left = txtleft;
                                oItem.Top = top;
                                oItem.Width = txtwidth;
                                oItem.Height = height;

                                doAddUF(sItemUID, iLength, oDataType);

                                oCheckBox = (SAPbouiCOM.CheckBox)oItem.Specific;
                                oItem.DisplayDesc = true;
                                oCheckBox.ValOff = "N";    
                                oCheckBox.ValOn= "Y";    

                                obj = new ValidationControls();
                                obj.ControlType = SAPbouiCOM.BoFormItemTypes.it_EDIT;
                                obj.ControlID.Add(sItemUID);
                                obj.ValidationCode = (sValidationCode);
                                obj.DBField = (sDBField);
                                obj.sExpression = sListofValues.Trim();
                                clsValidatioControls.Add(sDBField, obj);

                                if (top + height + 2 + height + 2 > maxtop) {
                                    top = SBOForm.Items.Item("lblTechInf").Top + SBOForm.Items.Item("lblTechInf").Height + 5;
                                    lblleft = txtleft + txtwidth + 15;
                                    txtleft = lblleft + lblWidth + 10;
                                } else
                                    top += height + 2;
                                if (sValue1 != string.Empty) setUFValue(sItemUID, sValue1);
                                break;
                            // End of CheckBox
                        }
                        oRecordSet.MoveNext();
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", new string[] { "Waste Group Validations" });
            } finally {
                DataHandler.INSTANCE.doReleaseRecordset(ref oRecordSet);
                DataHandler.INSTANCE.doReleaseRecordset(ref oRecordSetValues);
                
            }
        }
        //private void doConfigureForm() {

        //    SAPbobsCOM.Recordset oRecordSet = null;
        //    SAPbobsCOM.Recordset oRecordSetValues = null;

        //    try {
        //        clsValidatioControls.Clear();
        //        string sQry = "Select a.*,b." + IDH_WGVDMS._DtType + ",b." + IDH_WGVDMS._Fields + ",b." + IDH_WGVDMS._Type 
        //            + " from [" + IDH_WGVALDS.TableName + "] a,[" + IDH_WGVDMS.TableName + "] b Where a." + IDH_WGVALDS._ValidCd + "=b." + IDH_WGVDMS._ValidCd 
        //            + " And  a." + IDH_WGVALDS._WstGpCd + "='" + _WasteGroupCode + "' And IsNull(a." + IDH_WGVALDS._Select + ",'N')='Y' Order by " 
        //            + IDH_WGVALDS._Sort;
        //        oRecordSet = DataHandler.INSTANCE.doSBOSelectQuery("doGetWasteGroupValidationList", sQry);
        //        int top = SBOForm.Items.Item("lblTechInf").Top + SBOForm.Items.Item("lblTechInf").Height + 5;
        //        int lblleft = 5;
        //        int txtleft = SBOForm.Items.Item("ItemCode").Left;
        //        int lblWidth = SBOForm.Items.Item("6").Width + 10;
        //        int txtwidth = SBOForm.Items.Item("ItemCode").Width;
        //        int height = SBOForm.Items.Item("lblTechInf").Height;
        //        int maxtop = SBOForm.Items.Item("1").Top;
        //        ValidationControls obj;
        //        SAPbouiCOM.StaticText oLabel;
        //        //SAPbouiCOM.EditText oTextbox;
        //        SAPbouiCOM.ComboBox oCombo;
        //        SAPbouiCOM.Item oItem;
        //        if (oRecordSet != null && oRecordSet.RecordCount > 0) {
        //            int iType, iDataType, iFields;
        //            string sListofValues = "", sValidationCode = "";
        //            string sValue1 = "";
        //            string sValue2 = "";
        //            for (int iRecord = 0; iRecord <= oRecordSet.RecordCount - 1; iRecord++) {
        //                iType = Convert.ToInt32(oRecordSet.Fields.Item(IDH_WGVDMS._Type).Value);
        //                iDataType = Convert.ToInt32(oRecordSet.Fields.Item(IDH_WGVDMS._DtType).Value);
        //                iFields = Convert.ToInt32(oRecordSet.Fields.Item(IDH_WGVDMS._Fields).Value);
        //                sListofValues = Convert.ToString(oRecordSet.Fields.Item(IDH_WGVALDS._ValList).Value);
        //                sValidationCode = Convert.ToString(oRecordSet.Fields.Item(IDH_WGVALDS._ValidCd).Value);
        //                sValue1 = "";
        //                sValue2 = "";
        //                //if (_ItemCode != string.Empty) {
        //                    sQry = "Select a.* from [" + IDH_WITMVLD.TableName + "] a WITH(NOLOCK) Where a." + IDH_WITMVLD._ValidCd + "='" + sValidationCode + "' "
        //                        //+ " And a." + IDH_WITMVLD._ItemCode + "='" + _ItemCode + "' " 
        //                        + " And  a." + IDH_WITMVLD._WstGpCd + "='" + _WasteGroupCode + "' ";
        //                    if (_EnquiryID!=string.Empty)
        //                        sQry += " And (a." + IDH_WITMVLD._EnqId + "='" + _EnquiryID + "' And a." + IDH_WITMVLD._EnqLID + "='" + _sEnquiryLineID + "')";
        //                    else if (_WOQID != string.Empty)
        //                        sQry += " And (a." + IDH_WITMVLD._WOQId + "='" + _WOQID + "' And a." + IDH_WITMVLD._WOQLId + "='" + _sWOQLineID + "')";
        //                    oRecordSetValues = DataHandler.INSTANCE.doSBOSelectQuery("doGetWasteItemValuesByEnquiry", sQry);
        //                    if (oRecordSetValues != null && oRecordSetValues.RecordCount > 0) {
        //                        sValue1 = Convert.ToString(oRecordSetValues.Fields.Item(IDH_WITMVLD._Value1).Value);
        //                        sValue2 = Convert.ToString(oRecordSetValues.Fields.Item(IDH_WITMVLD._Value2).Value);
        //                    }
        //                //}
        //                switch (iType) {
        //                    case 1:
        //                        //Field
        //                        SBOForm.Items.Add("lbl" + iRecord.ToString(), SAPbouiCOM.BoFormItemTypes.it_STATIC);
        //                        SBOForm.Items.Add("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoFormItemTypes.it_EDIT);
        //                        if (sDefaultNewControl == string.Empty)
        //                            sDefaultNewControl = "IDH" + iRecord.ToString() + "_1";
        //                        oItem = SBOForm.Items.Item("lbl" + iRecord.ToString());
        //                        oItem.Left = lblleft;
        //                        oItem.Top = top;
        //                        oItem.Width = lblWidth;
        //                        oItem.Height = height;
        //                        oItem.LinkTo = "IDH" + iRecord.ToString() + "_1";
        //                        oLabel = (SAPbouiCOM.StaticText)oItem.Specific;
        //                        oLabel.Caption = sValidationCode;

        //                        oItem = SBOForm.Items.Item("IDH" + iRecord.ToString() + "_1");
        //                        oItem.Left = txtleft;
        //                        oItem.Top = top;
        //                        oItem.Width = txtwidth;
        //                        oItem.Height = height;
        //                        if (iDataType == 1) //AlphaNumeric
        //                            doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
        //                        else if (iDataType == 2) //Numeric
        //                            doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_LONG_NUMBER, 11);
        //                        else if (iDataType == 3) //Decimal
        //                            doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_MEASURE, 11);
        //                        //ArrayList arry = new ArrayList();
        //                        obj = new ValidationControls();
        //                        obj.ControlType = SAPbouiCOM.BoFormItemTypes.it_EDIT;
        //                        obj.ControlID.Add("IDH" + iRecord.ToString() + "_1");
        //                        obj.ValidationCode = (sValidationCode);

        //                        clsValidatioControls.Add(sValidationCode, obj);


        //                        if (top + height + 2 + height + 2 > maxtop) {
        //                            top = SBOForm.Items.Item("lblTechInf").Top + SBOForm.Items.Item("lblTechInf").Height + 5;
        //                            lblleft = txtleft + txtwidth + 15;
        //                            txtleft = lblleft + lblWidth + 10;
        //                        } else
        //                            top += height + 2;

        //                        if (sValue1 != string.Empty) setUFValue("IDH" + iRecord.ToString() + "_1", sValue1);
        //                        break;
        //                    case 2:
        //                        //Range
        //                        SBOForm.Items.Add("lbl" + iRecord.ToString(), SAPbouiCOM.BoFormItemTypes.it_STATIC);
        //                        oItem = SBOForm.Items.Item("lbl" + iRecord.ToString());
        //                        oItem.Left = lblleft;
        //                        oItem.Top = top;
        //                        oItem.Width = lblWidth;
        //                        oItem.Height = height;
        //                        oItem.LinkTo = "IDH" + iRecord.ToString() + "_1";
        //                        oLabel = (SAPbouiCOM.StaticText)oItem.Specific;
        //                        oLabel.Caption = sValidationCode;
        //                        if (iFields == 1) {
        //                            SBOForm.Items.Add("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoFormItemTypes.it_EDIT);
        //                            oItem = SBOForm.Items.Item("IDH" + iRecord.ToString() + "_1");
        //                            if (sDefaultNewControl == string.Empty)
        //                                sDefaultNewControl = "IDH" + iRecord.ToString() + "_1";
        //                            oItem.Left = txtleft;
        //                            oItem.Top = top;
        //                            oItem.Width = txtwidth;
        //                            oItem.Height = height;
        //                            SBOForm.Items.Item("lbl" + iRecord.ToString()).LinkTo = oItem.UniqueID;

        //                            if (iDataType == 2) //Numeric
        //                                doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_LONG_NUMBER, 11);
        //                            else if (iDataType == 3) //Decimal
        //                                doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_MEASURE, 11);
        //                            obj = new ValidationControls();
        //                            obj.ControlType = SAPbouiCOM.BoFormItemTypes.it_EDIT;
        //                            obj.ControlID.Add("IDH" + iRecord.ToString() + "_1");
        //                            obj.ValidationCode = (sValidationCode);
        //                            clsValidatioControls.Add(sValidationCode, obj);

        //                            if (sValue1 != string.Empty) setUFValue("IDH" + iRecord.ToString() + "_1", sValue1);
        //                        } else {
        //                            int _txtwidth = (txtwidth / iFields) - 2;
        //                            int _txtleft = txtleft;
        //                            obj = new ValidationControls();
        //                            for (int iCtrls = 1; iCtrls <= iFields; iCtrls++) {
        //                                SBOForm.Items.Add("IDH" + iRecord.ToString() + "_" + iCtrls.ToString(), SAPbouiCOM.BoFormItemTypes.it_EDIT);
        //                                oItem = SBOForm.Items.Item("IDH" + iRecord.ToString() + "_" + iCtrls.ToString());
        //                                SBOForm.Items.Item("lbl" + iRecord.ToString()).LinkTo = oItem.UniqueID;
        //                                //oItem.LinkTo= SBOForm.Items.Item("IDH" + iRecord.ToString() + "_" + iCtrls.ToString())
        //                                oItem.Left = _txtleft;
        //                                oItem.Top = top;
        //                                oItem.Width = _txtwidth;
        //                                oItem.Height = height;
        //                                if (iDataType == 2) //Numeric
        //                                    doAddUF("IDH" + iRecord.ToString() + "_" + iCtrls.ToString(), SAPbouiCOM.BoDataType.dt_LONG_NUMBER, 11);
        //                                else if (iDataType == 3) //Decimal
        //                                    doAddUF("IDH" + iRecord.ToString() + "_" + iCtrls.ToString(), SAPbouiCOM.BoDataType.dt_MEASURE, 11);
        //                                _txtleft += _txtwidth + 2;
        //                                obj.ControlType = SAPbouiCOM.BoFormItemTypes.it_EDIT;
        //                                obj.ControlID.Add("IDH" + iRecord.ToString() + "_" + iCtrls.ToString());
        //                                obj.ValidationCode = (sValidationCode);

        //                                if (sValue1 != string.Empty) setUFValue("IDH" + iRecord.ToString() + "_1", sValue1);
        //                                if (sValue1 != string.Empty) setUFValue("IDH" + iRecord.ToString() + "_2", sValue2);
        //                            }
        //                            if (sDefaultNewControl == string.Empty)
        //                                sDefaultNewControl = "IDH" + iRecord.ToString() + "_1";
        //                            clsValidatioControls.Add(sValidationCode, obj);
        //                        }
        //                        if (top + height + 2 + height + 2 > maxtop) {
        //                            top = SBOForm.Items.Item("lblTechInf").Top + SBOForm.Items.Item("lblTechInf").Height + 5;
        //                            lblleft = txtleft + txtwidth + 15;
        //                            txtleft = lblleft + lblWidth + 10;
        //                        } else
        //                            top += height + 2;
        //                        break;
        //                    //End of Range
        //                    case 3:
        //                        //Boolean
        //                        SBOForm.Items.Add("lbl" + iRecord.ToString(), SAPbouiCOM.BoFormItemTypes.it_STATIC);
        //                        SBOForm.Items.Add("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX);
        //                        if (sDefaultNewControl == string.Empty)
        //                            sDefaultNewControl = "IDH" + iRecord.ToString() + "_1";
        //                        oItem = SBOForm.Items.Item("lbl" + iRecord.ToString());
        //                        oItem.Left = lblleft;
        //                        oItem.Top = top;
        //                        oItem.Width = lblWidth;
        //                        oItem.Height = height;
        //                        oItem.LinkTo = "IDH" + iRecord.ToString() + "_1";
        //                        oLabel = (SAPbouiCOM.StaticText)oItem.Specific;
        //                        oLabel.Caption = sValidationCode;

        //                        oItem = SBOForm.Items.Item("IDH" + iRecord.ToString() + "_1");
        //                        oItem.Left = txtleft;
        //                        oItem.Top = top;
        //                        oItem.Width = txtwidth;
        //                        oItem.Height = height;

        //                        doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);

        //                        oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
        //                        oItem.DisplayDesc = true;
        //                        oCombo.ValidValues.Add("Y", "Yes");
        //                        oCombo.ValidValues.Add("N", "No");

        //                        obj = new ValidationControls();
        //                        obj.ControlType = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX;
        //                        obj.ControlID.Add("IDH" + iRecord.ToString() + "_1");
        //                        obj.ValidationCode = (sValidationCode);
        //                        clsValidatioControls.Add(sValidationCode, obj);


        //                        if (top + height + 2 + height + 2 > maxtop) {
        //                            top = SBOForm.Items.Item("lblTechInf").Top + SBOForm.Items.Item("lblTechInf").Height + 5;
        //                            lblleft = txtleft + txtwidth + 15;
        //                            txtleft = lblleft + lblWidth + 10;
        //                        } else
        //                            top += height + 2;
        //                        if (sValue1 != string.Empty) setUFValue("IDH" + iRecord.ToString() + "_1", sValue1);
        //                        break;
        //                    //End of Boolean
        //                    case 4:
        //                        //Combo
        //                        SBOForm.Items.Add("lbl" + iRecord.ToString(), SAPbouiCOM.BoFormItemTypes.it_STATIC);
        //                        SBOForm.Items.Add("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX);
        //                        if (sDefaultNewControl == string.Empty)
        //                            sDefaultNewControl = "IDH" + iRecord.ToString() + "_1";
        //                        oItem = SBOForm.Items.Item("lbl" + iRecord.ToString());
        //                        oItem.Left = lblleft;
        //                        oItem.Top = top;
        //                        oItem.Width = lblWidth;
        //                        oItem.Height = height;
        //                        oItem.LinkTo = "IDH" + iRecord.ToString() + "_1";
        //                        oLabel = (SAPbouiCOM.StaticText)oItem.Specific;
        //                        oLabel.Caption = sValidationCode;

        //                        oItem = SBOForm.Items.Item("IDH" + iRecord.ToString() + "_1");
        //                        oItem.Left = txtleft;
        //                        oItem.Top = top;
        //                        oItem.Width = txtwidth;
        //                        oItem.Height = height;


        //                        doAddUF("IDH" + iRecord.ToString() + "_1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);

        //                        oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
        //                        oItem.DisplayDesc = true;
        //                        string[] aListofValues = sListofValues.Split(';');
        //                        for (int iValidValues = 0; iValidValues <= aListofValues.Count() - 1; iValidValues++) {
        //                            string[] aValues = aListofValues[iValidValues].Split(':');
        //                            if (aValues.Count() == 2) {
        //                                oCombo.ValidValues.Add(aValues[0], aValues[1]);
        //                            }
        //                        }
        //                        obj = new ValidationControls();
        //                        obj.ControlType = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX;
        //                        obj.ControlID.Add("IDH" + iRecord.ToString() + "_1");
        //                        obj.ValidationCode = (sValidationCode);
        //                        clsValidatioControls.Add(sValidationCode, obj);
        //                        if (top + height + 2 + height + 2 > maxtop) {
        //                            top = SBOForm.Items.Item("lblTechInf").Top + SBOForm.Items.Item("lblTechInf").Height + 5;
        //                            lblleft = txtleft + txtwidth + 15;
        //                            txtleft = lblleft + lblWidth + 10;
        //                        } else
        //                            top += height + 2;
        //                        if (sValue1 != string.Empty) setUFValue("IDH" + iRecord.ToString() + "_1", sValue1);
        //                        break;
        //                    // End of Combo
        //                }
        //                oRecordSet.MoveNext();
        //            }
        //        }
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", new string[] { "Waste Group Validations" });
        //    } finally {
        //        DataHandler.INSTANCE.doReleaseRecordset(ref oRecordSet);
        //    }
        //}
        private void FillCombobox(SAPbouiCOM.ComboBox oCombo, string sListofValues) {
            try {

                string[] aExpression = sListofValues.Split(':');
                //FILLDB:TableName:Value:Desc:
                if (aExpression[0] == "FILLDB") {
                    string sWhere = null;
                    if (aExpression.Length == 5 && aExpression[4] != null && aExpression[4] != string.Empty)
                        sWhere = aExpression[4];
                    doFillCombo(oCombo, "[" + aExpression[1] + "]", aExpression[2], aExpression[3], sWhere, null, null, null);
                } else if (aExpression[0] == "FILLVAL") {
                    //FILLVAL:A;Actual#P;Protocol
                    string[] sValues = aExpression[1].Split('#');
                    for (int i = 0; i < sValues.Length; i++) {
                        string[] sVal = sValues[i].Split(';');
                        oCombo.ValidValues.Add(sVal[0], sVal[1]);
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", new string[] { "FillCombobox" });
            }

        }
        private void doAddUF(string sID, int iLength, DataType iDataType) {
            if (iDataType == DataType.AlphaNumeric)
                doAddUF(sID, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, iLength);
            if (iDataType == DataType.Decimal)
                doAddUF(sID, SAPbouiCOM.BoDataType.dt_QUANTITY, iLength);
            else if (iDataType == DataType.Numeric)
                doAddUF(sID, SAPbouiCOM.BoDataType.dt_LONG_NUMBER, iLength);
            else if (iDataType == DataType.Percentage)
                doAddUF(sID, SAPbouiCOM.BoDataType.dt_PERCENT, iLength);
            else if (iDataType == DataType.Price)
                doAddUF(sID, SAPbouiCOM.BoDataType.dt_PRICE, iLength);
            else if (iDataType == DataType.Percentage)
                doAddUF(sID, SAPbouiCOM.BoDataType.dt_PERCENT, iLength);
            else if (iDataType == DataType.Quantity)
                doAddUF(sID, SAPbouiCOM.BoDataType.dt_QUANTITY, iLength);
            else if (iDataType == DataType.Text)
                doAddUF(sID, SAPbouiCOM.BoDataType.dt_LONG_TEXT, iLength);
                            
                            
        } 
        public override void doBeforeLoadData() {

            //  moCreateNewBP.doAddEmptyRow(true, true);

            SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_Ok;

            Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;

            doFillCombos();
            base.doBeforeLoadData();
            //mo.doAddEmptyRow(true, true);
            //setUFValue("ItemCode", moCreateNewBP.U_CardCode);
            //setUFValue("IDH_CARDNM", moCreateNewBP.U_CardName);
            //setUFValue("IDH_CARDTY", moCreateNewBP.U_CardType);

            //setUFValue("IDH_ADRID", moCreateNewBP.U_Address);//Address ID 50
            //setUFValue("IDH_STREET", moCreateNewBP.U_Street);//Street 100
            //setUFValue("IDH_BLOCK", moCreateNewBP.U_Block);//Block 100
            //setUFValue("IDH_CITY", moCreateNewBP.U_City);//City 100
            //setUFValue("IDH_PCODE", moCreateNewBP.U_ZipCode);//State 3
            //setUFValue("IDH_COUNTY", moCreateNewBP.U_County);//PostCode 20
            //setUFValue("IDH_STATE", moCreateNewBP.U_State);//County 100
            //setUFValue("IDH_CONTRY", moCreateNewBP.U_Country);//Country 3

            //setUFValue("IDH_CNTPRS", moCreateNewBP.U_CName);//Contact Person 50
            //setUFValue("IDH_EMAIL", moCreateNewBP.U_Email);//Email 100
            //setUFValue("IDH_PHONE", moCreateNewBP.U_Phone);//Phone 20
            //setUFValue("IDH_INFO", "Address:");//Phone 20

        }
        /** 
         * Do the final form steps to show after loaddata
         */
        public override void doFinalizeShow() {
            base.doFinalizeShow();
            try{
            string sQry = "Select IsManual from NNM1,ONNM where ONNM.ObjectCode='4' And NNM1.ObjectCode='4' and ONNM.DfltSeries=NNM1.Series ";
            com.idh.bridge.DataRecords oRecords = null;
            oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("doCheckItemCodeSeries", sQry);
            if (oRecords != null && oRecords.RecordCount > 0) {
                if (oRecords.getValue("IsManual").ToString() == "Y") {
                    bManualItemCode = true;
                    EnableItem(true, "ItemCode");
                    // moCreateNewBP.U_CardCode = "";
                    setFocus("ItemCode");

                } else
                    bManualItemCode = false;
            }
            //_ItemName = "";
            setUFValue("ItemCode", _ItemCode);
            if (doValidateItembyCode()) {
                setUFValue("ItemName", _ItemName);
                
                doDisableItemFields();
                EnableItem(true, "IDH_CIPSIP");
                //sQry = "Select InvntItem,PrchseItem,U_IDHREB,U_WPTemplate FROM OITM where ItemCode='" + _ItemCode + "'";
                //try {
                //    oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("doFinalizeShow", sQry);
                //    if (oRecords != null && oRecords.RecordCount > 0) {
                //        if (oRecords.getValue("InvntItem").ToString() == "Y")
                //            setUFValue("InvntItem", "Y");
                //        if (oRecords.getValue("PrchseItem").ToString() == "Y")
                //            setUFValue("PrchseItem", "Y");
                //        if (oRecords.getValue("U_IDHREB").ToString() == "Y")
                //            setUFValue("IDHREB", "Y");
                //        if (oRecords.getValue("U_WPTemplate").ToString() == "Y")
                //            setUFValue("WPTemplate", "Y");
                //    }
                //} catch (Exception ex) {
                //    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", new string[] { "doFinalizeShow:Update Checkbox" });
                //}

            } else{
                _ItemCode = "";
                //_ItemName = "";
                setUFValue("ItemCode", _ItemCode);
            }
            setUFValue("FrgnName", _ItemFName);
            setUFValue("ItemName", _ItemName);
            setUFValue("IDH_WSTGRP", _WasteGroupCode);
            setUFValue("IDH_ENQID", _EnquiryID);
            doSetDataForCommon();

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", new string[] { "doFinalizeShow" });
            }
        }

        private void doSetDataForCommon() {
            SAPbobsCOM.Recordset oRecordSetValues = null;

            try {
                string sValue1 = "",sDBField;
                string sQry = "Select a.U_DBField,a.U_ValidCd,b.U_Value1 from [@IDH_WGVDMS] a with(nolock) Left Join [@IDH_WITMVLD] b with(nolock) " 
                    + "on a.U_DBField=b.U_DBField Where a.U_Common='Y' ";
                if (_EnquiryID != string.Empty)
                    sQry += " And (b." + IDH_WITMVLD._EnqId + "='" + _EnquiryID + "' And b." + IDH_WITMVLD._EnqLID + "='" + _sEnquiryLineID + "')";
                else if (_WOQID != string.Empty)
                    sQry += " And (b." + IDH_WITMVLD._WOQId + "='" + _WOQID + "' And b." + IDH_WITMVLD._WOQLId + "='" + _sWOQLineID + "')";
                oRecordSetValues = DataHandler.INSTANCE.doSBOSelectQuery("doGetCommonWasteItemValuesByEnquiry", sQry);
                if (oRecordSetValues != null && oRecordSetValues.RecordCount > 0) {
                    for (int i = 0; i <= oRecordSetValues.RecordCount - 1; i++) {
                        sDBField= Convert.ToString(oRecordSetValues.Fields.Item(IDH_WGVDMS._DBField).Value);
                        sValue1= Convert.ToString(oRecordSetValues.Fields.Item(IDH_WITMVLD._Value1).Value);
                        setUFValue(sDBField.Replace("U_", ""), sValue1);
                        oRecordSetValues.MoveNext();
                    }
                }
                    
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBQ", new string[] { "doSetDataForCommon" });
            } finally {
                DataHandler.INSTANCE.doReleaseRecordset(ref oRecordSetValues);
            }

        }
        /* 
         * Load the Form Data
         */
        public override void doLoadData() {
            base.doLoadData();
        }


        #endregion

        #region Events
        protected void doSetHandlers() {
            //Handler_Button_Ok = doCreateWasteItem;
            doStartEventFilterBatch(true);
            Handler_Button_Update = doCreateWasteItem;
            addHandler_ITEM_PRESSED("IDH_CIPSIP", doHandleButtonCIPSIP);
            addHandler_ITEM_PRESSED("IDH_EWCLUP", doHandleCommonCFLPress);
            addHandler_ITEM_PRESSED("IDH_HAZLUP", doHandleCommonCFLPress);
            addHandler_ITEM_PRESSED("IDH_ITMLUP", doHandleCommonCFLPress);
            doCommitEventFilters();

            //   base.doSetHandlers();
            //addHandler_ITEM_PRESSED("1", doCreateBP);

        }

        protected void doSetGridHandlers() {
            //base.doSetGridHandlers();
            doStartEventFilterBatch(true);
            addHandler_COMBO_SELECT("IDH_WSTGRP", doHandleWasteComboEvent);
            doCommitEventFilters();
            //moAdminGrid.Handler_GRID_DOUBLE_CLICK = new IDHGrid.ev_GRID_EVENTS(doRowDoubleClickEvent);
        }
        #endregion
        #region ItemEventHandlers
        public bool doHandleCommonCFLPress(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) { 
            try {
                if (pVal.ItemUID == "IDH_ITMLUP") {
                    setSharedData("TP", "WAST");
                    setSharedData("IDH_GRPCOD", Config.Parameter("IGR-WMS"));
                    //Dim sItem As String
                    //sItem = CType(getDFValue(oForm, "OITM", "WTITMLN"), String)
                    setSharedData("IDH_ITMCOD", getUFValue("WTITMLN"));
                    setSharedData("IDH_INVENT", "Y");
                    doOpenModalForm("IDHWISRC");
                } else if (pVal.ItemUID == "IDH_EWCLUP") {
                    setSharedData("IDH_WTWC", getUFValue("EWC"));
                    doOpenModalForm("IDH_WTECSR");
                } else if (pVal.ItemUID == "IDH_HAZLUP") {
                    setSharedData("IDH_HZCOD", getUFValue("HAZCD"));
                    doOpenModalForm("IDH_WTHZSR");
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
            }
            return true;
        }
        public bool doHandleCFLButtonClick(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            try {
                ValidationControls objControls = (ValidationControls)clsCFLExpressions[pVal.ItemUID];
                string[] aExpression = objControls.sExpression.Split(':');
                string SearchForm = aExpression[2];
                string[] aParms = aExpression[3].Split(';');
                for (int i = 0; i < aParms.Length ; i++) {
                    setSharedData("WVITEMOBJ", objControls);
                    string[] aVals=aParms[i].Split('#');
                    if (aVals[1].StartsWith(".")) {
                        setSharedData(aVals[0], aVals[1].Substring(1));
                    } else {
                        ValidationControls obj;
                        foreach (System.Collections.DictionaryEntry entry in clsValidatioControls) {
                            //entry.Value
                            //entry.Key ;
                            //sValidationCode = (string)entry.Key;
                            obj = (ValidationControls)entry.Value;
                            if (obj.DBField.Trim().Equals(aVals[1].Trim())) {
                                setSharedData(aVals[0], getUFValue(obj.ControlID[0].ToString()) );                            
                            }
                        }
                    
                    }
                }
                doOpenModalForm(SearchForm);
                                //goParent.doOpenModalForm("IDH_WTECSR", oForm)

                //foreach (System.Collections.DictionaryEntry entry in clsValidatioControls) {
                //    //entry.Value
                //    //entry.Key ;
                //    sValidationCode = (string)entry.Key;
                //    obj = (ValidationControls)entry.Value;
                //}
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);                       
            }
            return true;
        }
        public bool doHandleButtonCIPSIP(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent){
            if (pVal.BeforeAction)
                return true;

            if (_ItemCode.Trim() == "" || _ItemName.Trim() == "") {
            com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Item missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Item") });
                return true;
            }
            SAPbobsCOM.Items oItem = (SAPbobsCOM.Items)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
            if (oItem.GetByKey(_ItemCode) == false) {
                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Item not found in Item Master.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Item") });                
                return true;
            }                        
            string sSupplier = "";// (string)getDFValue("OITM", "U_DispFacCd");
            string sBPCd = "";
            string sAddress = "";
            if (_CardCode.Trim()!=""){
            SAPbobsCOM.BusinessPartners oBP = (SAPbobsCOM.BusinessPartners)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
                        if (oBP.GetByKey(_CardCode)){
                        sBPCd =_CardCode;
                         
                            sAddress=oBP.ShipToDefault;
                        }
                       // IDHAddOns.idh.data.Base.doReleaseObject(ref oBP));
            }
            
          //  string[] saWPFlagSplit =(string[]) getWFValue("IDH_WPFLAG");
            //if () {
                //sBPCd = saWPFlagSplit[0];
                //sAddress = saWPFlagSplit[2];

                //string sICode = saWPFlagSplit[3];
                //Dim sIDesc As String = saWPFlagSplit(4)
                //string sFlag = saWPFlagSplit[6];
                //string sNewICode = saWPFlagSplit[7];
                //string sCIPCPD = saWPFlagSplit[8];

                //Dim sNewICode As String = getDFValue(oForm, "OITM", "ItemCode")
                //string sIDesc =(string) getDFValue("OITM", "ItemName");
                //if (sFlag.Equals("BNEW") == false) {
                //    if (sCIPCPD == "N") {
                //        IDH_CSITPR oCIP = new IDH_CSITPR();
                //        if (sNewICode == null || sNewICode.Length == 0) {
                //            sNewICode = getDFValue( "OITM", "ItemCode").ToString();
                //        }
                //        if (sNewICode != null && sNewICode.Length > 0 && sICode != null && sICode.Length > 0) {
                //            oCIP.doCopyItemCIP(sICode, sNewICode, sIDesc, sBPCd, sAddress, sSupplier);
                //            saWPFlagSplit[8] = "Y";
                //        }
                //    }
                //    saWPFlagSplit[9] = sSupplier;
                //}
            //}
          //  ItemProfileCIPSIP obj;

            WR1_FR2Forms.idh.forms.fr2.prices.ItemProfileCIPSIP oOOForm = new WR1_FR2Forms.idh.forms.fr2.prices.ItemProfileCIPSIP(SBOForm.UniqueID);
            oOOForm.ItemCode = _ItemCode;
            oOOForm.ItemDesc = _ItemName;
            //if (oItem.ItemsGroupCode != null && oItem.ItemsGroupCode != 0)
            if (oItem.ItemsGroupCode != 0)
                oOOForm.ItemGroup = Convert.ToString(oItem.ItemsGroupCode);

            oOOForm.Supplier = sSupplier;
            oOOForm.Customer = sBPCd;
            oOOForm.Address = sAddress;

            oOOForm.doShowModal(SBOForm);
            return true;
        }
        public bool doCreateWasteItem(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                
                if (_ItemCode == string.Empty && getUFValue("ItemName").ToString() != string.Empty) {
                    string sItemName = "";
                    SAPbobsCOM.Items oItem;    
                    if (bManualItemCode == true && getUFValue("ItemCode").ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Item Code missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Item Code") });
                        BubbleEvent = false;
                        return true;
                    }
                    if (bManualItemCode == true && doValidateItembyCode() == true) {
                        com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage(("ERPKEXIS"), new string[] { getUFValue("ItemCode").ToString() });
                        BubbleEvent = false;
                        return true;
                    } else if (getUFValue("ItemName").ToString().Trim() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: Item Description missing.", "ERVLDFLD", new string[] { com.idh.bridge.Translation.getTranslatedWord("Item Description") });
                        BubbleEvent = false;
                        return true;
                    }
                    try {
                        DataHandler.INSTANCE.StartTransaction();
                        oItem= (SAPbobsCOM.Items)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
                        if (bManualItemCode)
                            oItem.ItemCode = getUFValue("ItemCode").ToString();
                       // oItem.Series = 3;
                        oItem.ItemName = getUFValue("ItemName").ToString();
                        oItem.ForeignName = getUFValue("FrgnName").ToString();
                        oItem.Valid = SAPbobsCOM.BoYesNoEnum.tYES;

                        if (getUFValue("InvntItem").ToString() == "Y")
                            oItem.InventoryItem = SAPbobsCOM.BoYesNoEnum.tYES;
                        else
                            oItem.InventoryItem = SAPbobsCOM.BoYesNoEnum.tNO;

                        if (getUFValue("PrchseItem").ToString() == "Y")
                            oItem.PurchaseItem = SAPbobsCOM.BoYesNoEnum.tYES;
                        else
                            oItem.PurchaseItem = SAPbobsCOM.BoYesNoEnum.tNO;

                        if (getUFValue("SellItem").ToString() == "Y")
                            oItem.SalesItem = SAPbobsCOM.BoYesNoEnum.tYES;
                        else
                            oItem.SalesItem = SAPbobsCOM.BoYesNoEnum.tNO;

                        //if (getUFValue("IDHREB").ToString() == "Y")
                        //    oItem.UserFields.Fields.Item("U_IDHREB").Value = "Y";
                        //else
                        //    oItem.UserFields.Fields.Item("U_IDHREB").Value = "N";

                        //if (getUFValue("WPTemplate").ToString() == "Y")
                        //    oItem.UserFields.Fields.Item("U_WPTemplate").Value = "Y";
                        //else
                        //    oItem.UserFields.Fields.Item("U_WPTemplate").Value = "N";
                        string sWastGrp = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup();
                        if (_AddItm == "A")
                            sWastGrp = com.idh.bridge.lookups.Config.INSTANCE.doGetAdditionalExpGroup();
                        if (sWastGrp.IndexOf(",") > -1) {
                            sWastGrp = (sWastGrp.Split(','))[0];
                        }
                        oItem.ItemsGroupCode = Convert.ToInt16(sWastGrp);
                        string stem = oItem.DefaultWarehouse;
                        int iResult = oItem.Add();
                        if (iResult != 0) {
                            BubbleEvent = false;
                            string sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
                            //DataHandler.INSTANCE.doSystemError("Stocktransfer Error - " + sResult, "Error processing the Wharehouse to Wharehouse stock transfer.");
                            com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Item Add Error - " + sResult + " Error while adding item.", "ERSYBPAD",
                                        new string[] { "Item", sResult });
                            if (DataHandler.INSTANCE.IsInTransaction())
                                DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            return false;
                        } else {
                            if (bManualItemCode)
                                _ItemCode = oItem.ItemCode;
                            else
                                _ItemCode = DataHandler.INSTANCE.SBOCompany.GetNewObjectKey();//this line is yet to verify by using a db having BP code serice
                            string msg = "Item successfully added: " + _ItemCode;
                            _ItemName = oItem.ItemName;
                            EnableItem(true, "IDH_CIPSIP");
                            com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(msg, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                            doUpdateParentForm(_ItemCode);
                            doDisableItemFields();
                        }
                    } catch (Exception Ex) {
                        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EREXGEN", null);
                        return false;
                    }finally{
                        //DataHandler.INSTANCE.doReleaseObject(  (SAPbobsCOM.SBObob)ref oItem);
                    }
                }
                //only save validation for item row ( either of Enq or WOQ)
                //if (_ItemCode != string.Empty) {
                if (!DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.StartTransaction();

                if (doSaveValidationValues( )) {

                    if (DataHandler.INSTANCE.IsInTransaction())
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);

                } else {
                    if (DataHandler.INSTANCE.IsInTransaction())
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                }
                //}
            }
            return true;

        }
        private bool doSaveValidationValues() {
            try {
                //moCreateWasteItemValidation.doClearBuffers();
              //  moCreateWasteItemValidation.doReLoadData();
              //  moCreateWasteItemValidation.first();
                ValidationControls obj;
                string sValidationCode;
                string sDBField;
                string UF, sValue;
                int iresult = 0;
                bool doItemUpdate=false;
                SAPbobsCOM.Items oItem= (SAPbobsCOM.Items)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
                       
                if (_ItemCode != string.Empty) {
                    ///objOITM = new OITM();
                    if (oItem.GetByKey(_ItemCode))
                        doItemUpdate = true;
                }
                foreach (System.Collections.DictionaryEntry entry in clsValidatioControls) {
                    //entry.Value
                    //entry.Key ;
                    sDBField = (string)entry.Key;
                    obj = (ValidationControls)entry.Value;
                    sValidationCode = obj.ValidationCode;
                    string sRequired="( (" + IDH_WITMVLD._EnqId + "='" + _EnquiryID + "' And " + IDH_WITMVLD._EnqLID + "='" + _sEnquiryLineID + "') or (" + IDH_WITMVLD._WOQId + "='" + _WOQID + "' And " + IDH_WITMVLD._WOQLId + "='" + _sWOQLineID + "') ) AND "  
                        + IDH_WITMVLD._WstGpCd + "='" + _WasteGroupCode + "' AND " + 
                        IDH_WITMVLD._ValidCd + "='" + sValidationCode + "'";
                    if (_sEnquiryLineID != string.Empty)
                        sRequired="( (" + IDH_WITMVLD._EnqId + "='" + _EnquiryID + "' And " + IDH_WITMVLD._EnqLID + "='" + _sEnquiryLineID + "')  ) AND "  
                        + IDH_WITMVLD._WstGpCd + "='" + _WasteGroupCode + "' AND " + 
                        IDH_WITMVLD._ValidCd + "='" + sValidationCode + "'";
                    else if (_sWOQLineID!="")
                        sRequired = "( (" + IDH_WITMVLD._WOQId + "='" + _WOQID + "' And " + IDH_WITMVLD._WOQLId + "='" + _sWOQLineID + "') ) AND "
                        + IDH_WITMVLD._WstGpCd + "='" + _WasteGroupCode + "' AND " +
                        IDH_WITMVLD._ValidCd + "='" + sValidationCode + "'";
                    iresult = moCreateWasteItemValidation.getData(sRequired, "Code");
                    if (iresult != 0) {
                        for (int ifields = 0; ifields <= obj.ControlID.Count - 1; ifields++) {
                            UF = obj.ControlID[ifields].ToString();
                            sValue = getUFValue(UF).ToString();
                            if (((moCreateWasteItemValidation.U_EnqId == _EnquiryID && moCreateWasteItemValidation.U_EnqLID == _sEnquiryLineID) || (moCreateWasteItemValidation.U_WOQId == _WOQID && moCreateWasteItemValidation.U_WOQLId == _sWOQLineID)) && moCreateWasteItemValidation.U_ItemCode == _ItemCode
                              && moCreateWasteItemValidation.U_WstGpCd == _WasteGroupCode && moCreateWasteItemValidation.U_ValidCd == sValidationCode) {
                                if (UF.EndsWith("1"))
                                    moCreateWasteItemValidation.U_Value1 = sValue;
                                else if (UF.EndsWith("2"))
                                    moCreateWasteItemValidation.U_Value2 = sValue;
                                if (_EnquiryID != "") {
                                    moCreateWasteItemValidation.U_EnqId = _EnquiryID;
                                    moCreateWasteItemValidation.U_EnqLID = _sEnquiryLineID; 
                                }
                                if (_WOQID != "") {
                                    moCreateWasteItemValidation.U_WOQId = _WOQID;
                                    moCreateWasteItemValidation.U_WOQLId = _sWOQLineID;
                                }
                                moCreateWasteItemValidation.U_ItemCode = _ItemCode;
                                moCreateWasteItemValidation.U_ItemFName= _ItemFName;
                            }
                        }

                    } else {
                        moCreateWasteItemValidation.doAddEmptyRow(true, true, false);
                        if (_EnquiryID != "") {
                            moCreateWasteItemValidation.U_EnqId = _EnquiryID;
                            moCreateWasteItemValidation.U_EnqLID = _sEnquiryLineID;
                        }
                        if (_WOQID != "") {
                            moCreateWasteItemValidation.U_WOQId = _WOQID;
                            moCreateWasteItemValidation.U_WOQLId = _sWOQLineID;
                        }
                        moCreateWasteItemValidation.U_ItemCode = _ItemCode;
                        moCreateWasteItemValidation.U_ItemFName = _ItemFName;
                        moCreateWasteItemValidation.U_ValidCd = sValidationCode;
                        moCreateWasteItemValidation.U_DBField = sDBField;
                        moCreateWasteItemValidation.U_WstGpCd = _WasteGroupCode;
                        for (int ifields = 0; ifields <= obj.ControlID.Count - 1; ifields++) {
                            UF = obj.ControlID[ifields].ToString();
                            sValue = getUFValue(UF).ToString();
                            if (UF.EndsWith("1"))
                                moCreateWasteItemValidation.U_Value1 = sValue;
                            else if (UF.EndsWith("2"))
                                moCreateWasteItemValidation.U_Value2 = sValue;
                        }
                    }
                    moCreateWasteItemValidation.doProcessData();
                    if (doItemUpdate ) {
                        if (obj.DBField.StartsWith("U_"))
                            oItem.UserFields.Fields.Item(obj.DBField).Value = moCreateWasteItemValidation.U_Value1;
                        else {
                            switch (obj.DBField) {
                                case "BuyUnitMsr":
                                    oItem.PurchaseUnit = moCreateWasteItemValidation.U_Value1;
                                    break;
                                case "SalUnitMsr":
                                    oItem.SalesUnit = moCreateWasteItemValidation.U_Value1;
                                    break;
                                case "InvntryUom":
                                    oItem.InventoryUOM= moCreateWasteItemValidation.U_Value1;                                    
                                    break;
                            }
                        }
                        //"BuyUnitMsr", "SalUnitMsr", "InvntryUom"
                    }
                }
                string sWhere = "";
                for (int i = 0; i < CommonUFItems.Length; i++) {
                    if (_EnquiryID != "" && _sEnquiryLineID != "" && _EnquiryID != "0" && _sEnquiryLineID != "0")
                        sWhere = "( (" + IDH_WITMVLD._EnqId + "='" + _EnquiryID + "' And " + IDH_WITMVLD._EnqLID + "='" + _sEnquiryLineID + "') ) AND "
                        + IDH_WITMVLD._WstGpCd + "='" + _WasteGroupCode + "' AND " +
                        IDH_WITMVLD._ValidCd + "='" + CommonUFItems[i] + "' AND " + IDH_WITMVLD._DBField + "='" + CommonUFForOITM[i] + "' ";
                    else
                        sWhere = "( (" + IDH_WITMVLD._WOQId + "='" + _WOQID + "' And " + IDH_WITMVLD._WOQLId + "='" + _sWOQLineID + "') ) AND "
                        + IDH_WITMVLD._WstGpCd + "='" + _WasteGroupCode + "' AND " +
                        IDH_WITMVLD._ValidCd + "='" + CommonUFItems[i] + "' AND " + IDH_WITMVLD._DBField + "='" + CommonUFForOITM[i] + "' ";
                    iresult = moCreateWasteItemValidation.getData(sWhere, "Code");
                    if (iresult != 0) {
                        sValue = getUFValue(CommonUFItems[i]).ToString();
                        if (((moCreateWasteItemValidation.U_EnqId == _EnquiryID && moCreateWasteItemValidation.U_EnqLID == _sEnquiryLineID) || (moCreateWasteItemValidation.U_WOQId == _WOQID && moCreateWasteItemValidation.U_WOQLId == _sWOQLineID)) && moCreateWasteItemValidation.U_ItemCode == _ItemCode
                          && moCreateWasteItemValidation.U_WstGpCd == _WasteGroupCode && moCreateWasteItemValidation.U_ValidCd == CommonUFItems[i]) {
                            moCreateWasteItemValidation.U_Value1 = sValue;
                            if (_EnquiryID != "") {
                                moCreateWasteItemValidation.U_EnqId = _EnquiryID;
                                moCreateWasteItemValidation.U_EnqLID = _sEnquiryLineID;
                            }
                            if (_WOQID != "") {
                                moCreateWasteItemValidation.U_WOQId = _WOQID;
                                moCreateWasteItemValidation.U_WOQLId = _sWOQLineID;
                            }
                            moCreateWasteItemValidation.U_ItemCode = _ItemCode;
                            moCreateWasteItemValidation.U_ItemFName = _ItemFName;
                        }
                    } else {
                        moCreateWasteItemValidation.doAddEmptyRow(true, true, false);
                        if (_EnquiryID != "") {
                            moCreateWasteItemValidation.U_EnqId = _EnquiryID;
                            moCreateWasteItemValidation.U_EnqLID = _sEnquiryLineID;
                        }
                        if (_WOQID != "") {
                            moCreateWasteItemValidation.U_WOQId = _WOQID;
                            moCreateWasteItemValidation.U_WOQLId = _sWOQLineID;
                        }
                        moCreateWasteItemValidation.U_ItemCode = _ItemCode;
                        moCreateWasteItemValidation.U_ItemFName = _ItemFName;
                        moCreateWasteItemValidation.U_ValidCd = CommonUFItems[i];
                        moCreateWasteItemValidation.U_DBField = CommonUFForOITM[i];
                        moCreateWasteItemValidation.U_WstGpCd = _WasteGroupCode;
                        sValue = getUFValue(CommonUFItems[i]).ToString();
                        moCreateWasteItemValidation.U_Value1 = sValue;
                            
                    }
                    moCreateWasteItemValidation.doProcessData();
                    if (doItemUpdate) {
                        if (CommonUFForOITM[i].StartsWith("U_"))
                            oItem.UserFields.Fields.Item(CommonUFForOITM[i]).Value = moCreateWasteItemValidation.U_Value1;
                        else {
                            switch (CommonUFForOITM[i]) {
                                case "BuyUnitMsr":
                                    oItem.PurchaseUnit = moCreateWasteItemValidation.U_Value1;
                                    break;
                                case "SalUnitMsr":
                                    oItem.SalesUnit = moCreateWasteItemValidation.U_Value1;
                                    break;
                                case "InvntryUom":
                                    oItem.InventoryUOM = moCreateWasteItemValidation.U_Value1;
                                    break;
                            }
                        }
                        //"BuyUnitMsr", "SalUnitMsr", "InvntryUom"
                    }
                }
                if (doItemUpdate)
                    oItem.Update();
                return true;
            } catch (Exception Ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EREXGEN", null);
                return false;
            }
        }


        private bool doHandleWasteComboEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)SBOForm.Items.Item("IDH_WSTGRP").Specific;
                if (oCombo.Selected == null) {
                    return true;
                }
                string sWasteCode = oCombo.Selected.Value;

            }
            return true;
        }
        #endregion

        #region doFillCombos
        private void doFillCombos() {
            try {
                //FillCombos.FillCountry(Items.Item("IDH_CARDTY").Specific);
                SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)SBOForm.Items.Item("IDH_WSTGRP").Specific;
                //string sDfltCountry = Config.INSTANCE.getParameterWithDefault("DFTCNTRY", "GB");
                doFillCombo(oCombo, "[@ISB_WASTEGRP]", "U_WstGpCd", "U_WstGpNm", null, null, null, null);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXFC", new string[] { com.idh.bridge.Translation.getTranslatedWord("Waste Group") });
            }
        }
        #endregion

        #region Methods
        private void doDisableItemFields() {
            try {
                if (sDefaultNewControl != string.Empty)
                    setFocus(sDefaultNewControl);
                EnableItem(false, "ItemName");
                EnableItem(false, "ItemCode");
                EnableItem(false, "FrgnName");
            } catch (Exception Ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EREXGEN", null);
            }
        }
        private bool doUpdateParentForm(string sItemCode) {
            try {
                if (_EnquiryID != "" && _sEnquiryLineID != string.Empty) {
                    IDH_ENQITEM moEnqItem = new IDH_ENQITEM();
                    moEnqItem.U_EnqId = _EnquiryID;
                    moEnqItem.DoNotSetDefault = true;
                    moEnqItem.DoNotSetParent= true;
                    if (moEnqItem.getByKey(_sEnquiryLineID)) {
                        moEnqItem.U_ItemCode = sItemCode;
                        moEnqItem.doProcessData("Item code updated");
                        bUpdateParent = true;
                    }
                }
                if (_WOQID != "" && _sWOQLineID != string.Empty) {
                    IDH_WOQITEM moWOQItem = new IDH_WOQITEM();
                    moWOQItem.U_JobNr = _WOQID;
                    //moWOQItem.DoNotSetDefault = true;
                    //moWOQItem.DoNotSetParent = true;
                    if (moWOQItem.getByKey(_sWOQLineID)) {
                        moWOQItem.U_WasCd= sItemCode;
                        moWOQItem.doProcessData("Item code updated");
                        bUpdateParent = true;
                    }
                }
                return true;
            } catch (Exception Ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(Ex.Message, "EREXGEN", null);
                return false;
            }
        }
        private bool doValidateItembyCode() {
            string sItemName = "";
            //object oVal = doLookupTableField("OITM a", "ItemName", "ItemCode = '" + getUFValue("ItemCode").ToString() + '\'');
            //return Conversions.ToString(oVal);
            sItemName = Config.INSTANCE.doGetItemDescriptionWithNoBuffer(getUFValue("ItemCode").ToString());
            if (sItemName == string.Empty)
                return false;
            else {
             //   _ItemCode = getUFValue("ItemCode").ToString();
                return true;
            }
        }
        #endregion
        #region Old Events
        public override void doHandleModalResultShared(string sModalFormType, string sLastButton) {

            try {
                if (sModalFormType == "IDH_WTECSR") {
                    string sEWCCd = getUFValue("EWC").ToString();
                    //getFormDFValue( "U_EWC")
                    if (sEWCCd != null && sEWCCd.Length > 0) {
                        sEWCCd = sEWCCd + "," + getSharedData("IDH_WTWCO").ToString();
                    } else {
                        sEWCCd = getSharedData("IDH_WTWCO").ToString();
                    }
                    setUFValue("EWC", sEWCCd, false);
                } else if (sModalFormType == "IDH_WTHZSR") {
                    setUFValue("HAZCD", getSharedData("IDH_HZCODO").ToString(), false);
                } else if (sModalFormType == "EPASCR") {
                    string sTrg = getSharedData("TRG").ToString();
                    ValidationControls obj = (ValidationControls)getSharedData("WVITEMOBJ");
                    setUFValue(obj.ControlID[0].ToString(), getSharedData("IDH_EPACD"), false);
                    //setSharedData("WVITEMOBJ", objControls);
                    //setItemValue( "LKP_EPACD", getSharedData( "IDH_EPANM"), False)
                    switch (sTrg) {
                        case "LKP_EPACD1":
                            obj = (ValidationControls)clsValidatioControls["U_EPAWstNm1"];
                            setUFValue(obj.ControlID[0].ToString(), getSharedData("IDH_EPANM"), false);
                            break;
                        case "LKP_EPACD2":
                            obj = (ValidationControls)clsValidatioControls["U_EPAWstNm2"];
                            setUFValue(obj.ControlID[0].ToString(), getSharedData("IDH_EPANM"), false);
                            break;
                        case "LKP_EPACD3":
                             obj = (ValidationControls)clsValidatioControls["U_EPAWstNm3"];
                            setUFValue(obj.ControlID[0].ToString(), getSharedData("IDH_EPANM"), false);
                            break;
                        case "LKP_EPACD4":
                             obj = (ValidationControls)clsValidatioControls["U_EPAWstNm4"];
                            setUFValue(obj.ControlID[0].ToString(), getSharedData("IDH_EPANM"), false);
                            break;
                        case "LKP_EPACD5":
                             obj = (ValidationControls)clsValidatioControls["U_EPAWstNm5"];
                            setUFValue(obj.ControlID[0].ToString(), getSharedData("IDH_EPANM"), false);
                            break;
                        case "LKP_EPACD6":
                             obj = (ValidationControls)clsValidatioControls["U_EPAWstNm6"];
                            setUFValue(obj.ControlID[0].ToString(), getSharedData("IDH_EPANM"), false);
                            break;
                    }

                } else if (sModalFormType == "IDHWISRC") {
                    setUFValue("WTITMLN", getSharedData("ITEMCODE").ToString());
                    //ElseIf sModalFormType = "IDHSNSRCH" Then
                } else if (sModalFormType == "IDHISRC") {
                    ValidationControls obj = (ValidationControls)getSharedData("WVITEMOBJ");
                    setUFValue(obj.ControlID[0].ToString(), getSharedData("ITEMNAME").ToString());
                } else if (sModalFormType == "IDHCSRCH") {
                    string sTrg = getSharedData("TRG").ToString();
                    if (sTrg == "DSPLKP") {
                        string sCrdCd = getSharedData("CARDCODE").ToString();
                        string sCrdNm = getSharedData("CARDNAME").ToString();
                        ValidationControls obj=(ValidationControls )clsValidatioControls["U_DispFacCd"];
                        setUFValue(obj.ControlID[0].ToString(), sCrdCd);//U_DispFacCd
                         obj = (ValidationControls)clsValidatioControls["U_DispFacility"];
                        setUFValue(obj.ControlID[0].ToString(), sCrdNm);//U_DispFacility
                        //Set DF Address
                        ArrayList oData = default(ArrayList);
                        oData = WR1_Search.idh.forms.search.AddressSearch.doGetAddress(base.IDHForm.goParent, sCrdCd, "S");
                        if (clsValidatioControls.ContainsKey("U_DFAddress")){
                        obj = (ValidationControls)clsValidatioControls["U_DFAddress"];
                        if (oData != null && oData.Count > 0) {
                            setUFValue(obj.ControlID[0].ToString(), Convert.ToString(oData[0]), true, true);
                        } else {
                            setUFValue(obj.ControlID[0].ToString(), "", true, true);
                        }}
                    }
                } else if (sModalFormType == "IDHASRCH") {
                    string sAddres = getSharedData("ADDRESS").ToString();
                    ValidationControls  obj = (ValidationControls)clsValidatioControls["U_DFAddress"];
                     setUFValue(obj.ControlID[0].ToString(), sAddres);
                }
                //KA -- END -- 20121025
                base.IDHForm.doClearSharedData(this.SBOForm);
            } catch (Exception ex) {
                //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error processing the Modal Results - " & sModalFormType)
                DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPMR", new string[] { sModalFormType });
            }
            //base.doHandleModalResultShared(sModalFormType, sLastButton);
        }
        #endregion
    }
}
