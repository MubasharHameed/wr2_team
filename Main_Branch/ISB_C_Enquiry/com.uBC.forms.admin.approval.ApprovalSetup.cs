using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using IDHAddOns.idh.controls;
using com.idh.bridge.data;
using com.idh.bridge;
using com.idh.dbObjects.numbers;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry.admin.Approval {
    //Inherits idh.forms.admin.Tmpl
    //msKeyGen="WGPVALDS"
    
    public class ApprovalSetup : IDHAddOns.idh.forms.Base {

        protected string msKeyGen =  "IDHAPSTP";
        public ApprovalSetup(IDHAddOns.idh.addon.Base oParent, string sParMenu, int iMenuPosition)
            : base(oParent, "IDHAPPSETUP", sParMenu, iMenuPosition, "ApprovalSetup.srf", true, true, false, "WOQ Approval Setup", load_Types.idh_LOAD_NORMAL) {

        }
        protected string getTable() {
            return getUserTable();
        }

        protected string getUserTable() {
            return IDH_APPRSTUP.TableName;
        }

        protected override void doSetEventFilters() {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST);

        }
        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            try {
                base.doCompleteCreate(ref oForm, ref BubbleEvent);
                oForm.Title = gsTitle;

                SAPbouiCOM.Item oItem = default(SAPbouiCOM.Item);
                oItem = oForm.Items.Item("IDH_CONTA");
                UpdateGrid oGridN = new UpdateGrid(this, oForm, "LINESGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane);
                //doSetFilterLayout(oForm)
                oForm.Items.Item("LINESGRID").AffectsFormMode = true;
                oForm.Items.Item("IDH_FRMTYP").AffectsFormMode = false;
                //SAPbouiCOM.ChooseFromListCollection oCFLS =new SAPbouiCOM.ChooseFromListCollection();
                SAPbouiCOM.ChooseFromListCreationParams oCFLSCreationpack =(SAPbouiCOM.ChooseFromListCreationParams)goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams);
                oCFLSCreationpack.ObjectType = "120";//Approval Template
                oCFLSCreationpack.MultiSelection = false;
                oCFLSCreationpack.UniqueID = "CFL_APRTMP";
                oForm.ChooseFromLists.Add(oCFLSCreationpack);

                oCFLSCreationpack = oCFLSCreationpack = (SAPbouiCOM.ChooseFromListCreationParams)goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams);
                oCFLSCreationpack.ObjectType = "12";//Users
                oCFLSCreationpack.MultiSelection = false;
                oCFLSCreationpack.UniqueID = "CFL_Users";
                oForm.ChooseFromLists.Add(oCFLSCreationpack);

                //oForm.ChooseFromLists.Add(
            } catch (Exception ex) {
                //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
                BubbleEvent = false;
            }
        }

        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCloseForm(oForm, ref BubbleEvent);
            UpdateGrid.doRemoveGrid(oForm, "LINESGRID");
        }

        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {
            //goParent.goDB.doSelectQuery("If (Select Count(1) from [@IDH_FORMSET])=1211 Begin  Exec IDH_CopyDeptForAllUsers; Exec IDH_UpdateFormSettings; end ")
            //doe("Select 1; Exec IDH_CopyDeptForAllUsers; ")
            //doUpdateQuery("Select 1; Exec IDH_UpdateFormSettings")
            UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
            if (oGridN == null) {
                oGridN = new UpdateGrid(this, oForm, "LINESGRID");
            }
            oGridN.AddEditLine = false;

            doSetFilterFields(oGridN);
            doSetListFields(oGridN);

            oGridN.doAddGridTable(new GridTable(getTable(), null, "Code", true), true);

            oGridN.doSetDoCount(true);
            oGridN.doSetHistory(getUserTable(), "Code");
            //oGridN.setOrderValue("U_GridName ASC, U_Index Asc,U_Name Asc,Cast(Code as Int) ASC ")

            doFillCombos(oForm);
        }


        private void doFillCombos(SAPbouiCOM.Form oForm) {
            //Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            //oRecordSet = goParent.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            //oRecordSet = goParent.goDB.doSelectQuery("Select * from [@ISB_WASTEGRP] Order by U_WstGpCd")
            //If oRecordSet IsNot Nothing Then IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)

            //doFillCombo(oForm, "IDH_FRMTYP", "[@ISB_WASTEGRP]", "U_WstGpCd", "U_WstGpNm", null, "U_WstGpCd", "Select", "0");

            SAPbouiCOM.ComboBox oCombo = default(SAPbouiCOM.ComboBox);
            oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_FRMTYP").Specific;
            doClearCombo(oForm, "IDH_FRMTYP");
            oCombo.ValidValues.Add("",getTranslatedWord("--Select--"));
            oCombo.ValidValues.Add("IDHWOQ",getTranslatedWord("Work Order Quote"));
            oCombo.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index);
            //   doFormsCombo(oForm)
            //doLoadUserCombo(oForm)
        }



        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                return;
            }
            try {
                //SAPbouiCOM.ComboBox oComboWasteGroup = default(SAPbouiCOM.ComboBox);
                //oComboWasteGroup = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_FRMTYP").Specific;
                if ((getItemValue(oForm, "IDH_FRMTYP").Trim() != "0")) {
                    oForm.Freeze(true);
                    UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
                    if (oGridN.doGetFieldValue("U_Origintr", oGridN.getRowCount() - 1).ToString() == "" && oGridN.doGetFieldValue("U_Stage", oGridN.getRowCount() - 1).ToString() == "")
                        oGridN.doRemoveRow(oGridN.getRowCount() - 1, false, true);

                    if (oGridN.doProcessData()) {
                        oGridN.doAddEditLine();
                        doSetGridDefaultValue(oGridN, oForm);
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);

            } finally {
                oForm.Freeze(false);
            }
            
        }

        public override void doFinalizeShow(SAPbouiCOM.Form oForm) {
            base.doFinalizeShow(oForm);
        }

        protected void doSetFilterFields(IDHAddOns.idh.controls.UpdateGrid oGridN) {
         //   oGridN.doAddFilterField("IDH_FRMTYP", "U_FormTyp", SAPbouiCOM.BoDataType.dt_LONG_TEXT,"=",30,null,true);
            //oGridN.doAddFilterField("IDH_USERS", "U_UserName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "")
            //oGridN.doAddFilterField("IDH_FORMS", "U_FormID", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "")
        }

        protected void doSetListFields(IDHAddOns.idh.controls.UpdateGrid oGridN) {
            oGridN.doAddListField("Code", "Code", false, 0, null, null);
            oGridN.doAddListField("Name", "Name", false, 0, null, null);

            oGridN.doAddListField("U_Origintr", getTranslatedWord( "Originator"), true, -1, null, null);
            oGridN.doAddListField("U_Stage", getTranslatedWord( "Approval Stage"), true, -1, null, null);
            oGridN.doAddListField("U_ApStagCd", getTranslatedWord( "Approval Stage Code"), true, 0, null, null);
            oGridN.doAddListField("U_FormTyp", getTranslatedWord("Form Type"), true, 0, null, null);
        }

        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            base.doLoadData(oForm);
            //SAPbouiCOM.ComboBox oComboFormType = default(SAPbouiCOM.ComboBox);
            //oComboFormType = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_FRMTYP").Specific;
            //string sSql = null;
            //if ((oComboFormType.Selected != null) && (oComboFormType.Selected.Value != "0")) {
            //    sSql = "Select T0.* From [@IDH_APPDTAIL] t0 Where U_FormTyp='" + oComboFormType.Selected.Value.ToString() + "'";
            //} else {
            //    sSql = "Select T0.* From [@IDH_APPDTAIL] t0 Where 1=0";
            //}
            
            UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
            oGridN.doSetDeleteActive(true);
            bool doAddLine = false;
            if (getItemValue(oForm,"IDH_FRMTYP").Trim() == "") {
                doAddLine = false;
                oGridN.AddEditLine = false;
                oGridN.setRequiredFilter(" U_FormTyp='-1' ");           
           
            } else {
                oGridN.setRequiredFilter(" U_FormTyp='" + getItemValue(oForm, "IDH_FRMTYP").Trim() + "' ");           
                oGridN.AddEditLine = true;
                doAddLine = true;
            }
            oGridN.doReloadData("", doAddLine, true);
            
            //oGridN.getSBOGrid().DataTable.ExecuteQuery(sSql);
            oGridN.doApplyRules();
            if (!doAddLine) {
                oGridN.doRemoveRow(oGridN.getRowCount() - 1, false, true);
            } else {
                oGridN.AddEditLine = true;
                if (oGridN.doGetFieldValue("Code",oGridN.getRowCount()-1).ToString()=="")
                oGridN.doAddEditLine();
                doSetGridDefaultValue(oGridN, oForm);
                //com.idh.bridge.DataHandler.NextNumbers oNumbers = doSetKeyValue(oForm);
                //oGridN.doSetFieldValue("Code", oGridN.getRowCount()-1, oNumbers.CodeCode, true);
                //oGridN.doSetFieldValue("Name", oGridN.getRowCount()-1, oNumbers.NameCode, true);
                //oGridN.doSetFieldValue("U_FormTyp", oGridN.getRowCount()-1, getItemValue(oForm, "IDH_FRMTYP"));
               
            }
            int iIndex = oGridN.doIndexFieldWC("U_Origintr");
            SAPbouiCOM.EditTextColumn oCol = (SAPbouiCOM.EditTextColumn)oGridN.Columns.Item(iIndex);
            oCol.ChooseFromListUID = "CFL_Users";
            oCol.ChooseFromListAlias = "USER_CODE";
            
            iIndex = oGridN.doIndexFieldWC("U_Stage");
            oCol = (SAPbouiCOM.EditTextColumn)oGridN.Columns.Item(iIndex);
            oCol.ChooseFromListUID = "CFL_APRTMP";
            oCol.ChooseFromListAlias= "Name";


            //SAPbouiCOM.EditTextColumnClass oEdit = (SAPbouiCOM.EditTextColumnClass)oCol;
            //oEdit.ChooseFromListUID
            //  MyBase.doLoadData(oForm)
        }
        public void doSetGridDefaultValue(IDHGrid oGridN,SAPbouiCOM.Form oForm) {
            NumbersPair oNumbers = doSetKeyValue(oForm);
            oGridN.doSetFieldValue("Code", oGridN.getRowCount() - 1, oNumbers.CodeCode, true);
            oGridN.doSetFieldValue("Name", oGridN.getRowCount() - 1, oNumbers.NameCode, true);
            oGridN.doSetFieldValue("U_FormTyp", oGridN.getRowCount() - 1, getItemValue(oForm, "IDH_FRMTYP"));
               
        }
         public NumbersPair doSetKeyValue(SAPbouiCOM.Form oForm ) {
            if (msKeyGen == null) {
                return DataHandler.INSTANCE.doGenerateCode("");
            } else {
                return DataHandler.INSTANCE.doGenerateCode(msKeyGen);
            }
         }
        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            //if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY || pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_ADD) {

            //    IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");

            //    com.idh.bridge.DataHandler.NextNumbers oNumbers = doSetKeyValue(oForm);
            //    oGridN.doSetFieldValue("Code", pVal.Row, oNumbers.CodeCode, true);
            //    oGridN.doSetFieldValue("Name", pVal.Row, oNumbers.NameCode, true);
            //    oGridN.doSetFieldValue("U_FormTyp", pVal.Row, getItemValue(oForm, "IDH_FRMTYP"));
            //    oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
            //}
            return base.doCustomItemEvent(oForm, ref pVal);

        }

        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            base.doItemEvent(oForm, ref pVal, ref BubbleEvent);
            if ((pVal.ItemUID == "IDH_FRMTYP") && pVal.EventType == SAPbouiCOM.BoEventTypes.et_COMBO_SELECT && pVal.BeforeAction == false) {
                doLoadData(oForm);
            }
            else if(pVal.EventType==SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST && pVal.BeforeAction==false && pVal.ItemUID=="LINESGRID"){

                SAPbouiCOM.IChooseFromListEvent oCFLE = default(SAPbouiCOM.IChooseFromListEvent);
                oCFLE = (SAPbouiCOM.IChooseFromListEvent)pVal;
               // string Orig = null;
                string CFLID = oCFLE.ChooseFromListUID;
                //Approvals_Frm = sbo_app.Forms.Item(FormUID);
                SAPbouiCOM.ChooseFromList oCFL = default(SAPbouiCOM.ChooseFromList);
                UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");

                oCFL = oForm.ChooseFromLists.Item(CFLID);
                SAPbouiCOM.DataTable oDT = default(SAPbouiCOM.DataTable);
                oDT = oCFLE.SelectedObjects;
                if ((oDT != null)) {
                    if (oCFL.UniqueID == "CFL_Users") {
                        try {
                            bool bAlreadyExists = false;
                            string sCFLValue = oDT.GetValue("USER_CODE", 0).ToString();
                            int iIndex = oGridN.doIndexFieldWC("U_Origintr");
                            int currentrow = pVal.Row;
                             for (int i = 0; i < oGridN.getRowCount(); i++) {
                                
                                 if (currentrow!=i && oGridN.doGetFieldValue(iIndex, i).ToString().Equals( sCFLValue)) {
                                     bAlreadyExists = true;
                                     break;
                                 }
                            }
                            if (!bAlreadyExists) {
                                oGridN.doSetFieldValue(iIndex, pVal.Row, oDT.GetValue("USER_CODE", 0).ToString());   
                            }
                            else
                            oGridN.doSetFieldValue(iIndex, pVal.Row, "");
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;

                        } catch (Exception ex) {
                            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { com.idh.bridge.Translation.getTranslatedWord("doItemEvent") });
                            //sbo_app.StatusBar.SetText("Item  Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        } finally {
                        }
                    } else if (oCFL.UniqueID == "CFL_APRTMP") {
                        try {
                            int iIndex = oGridN.doIndexFieldWC("U_Stage");
                            oGridN.doSetFieldValue(iIndex, pVal.Row, oDT.GetValue("Name", 0).ToString());

                            iIndex = oGridN.doIndexFieldWC("U_ApStagCd");
                            oGridN.doSetFieldValue(iIndex, pVal.Row, oDT.GetValue(0, 0).ToString());
                            oGridN.AddEditLine = true;
                            oGridN.doAddEditLine(true);
                            doSetGridDefaultValue(oGridN, oForm);
                            //IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");
                            //com.idh.bridge.DataHandler.NextNumbers oNumbers = doSetKeyValue(oForm);
                            //oGridN.doSetFieldValue("Code", oGridN.getRowCount()-1, oNumbers.CodeCode, true);
                            //oGridN.doSetFieldValue("Name", oGridN.getRowCount() - 1, oNumbers.NameCode, true);
                            //oGridN.doSetFieldValue("U_FormTyp", oGridN.getRowCount() - 1, getItemValue(oForm, "IDH_FRMTYP"));
                            oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;

                        } catch (Exception ex) {
                            com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
                        } finally {
                        }
                    }
                }

            }
            return true;
        }

        public override void doClose() {
        }
    }
}
