﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;

using IDHAddOns.idh.controls;
//using com.idh.bridge.reports;
//using com.idh.utils.Conversions;
using com.idh.bridge;
using com.idh.bridge.data;
//using WR1_Grids.idh.controls.grid;
using com.idh.bridge.lookups;
using com.idh.bridge.action;
using com.idh.controls;
using com.idh.controls.strct;
using Microsoft.Office.Interop.Excel;
using System.Windows.Forms;
using System.Threading;
using System.Linq;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry.PJDORA {
    class PBIMassSelector :
        IDHAddOns.idh.forms.Base {

        public PBIMassSelector(IDHAddOns.idh.addon.Base oParent, string sParent, int iMenuPosition)
            : base(oParent, "IDH_ALLLIVSRVS", sParent, iMenuPosition, "AllLiveServices_Manager.srf", true, true, false, "PBI Selector", load_Types.idh_LOAD_NORMAL) {
        }

        protected virtual void doTheGridLayout(UpdateGrid oGridN) {
            if (com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", false)) {
                string sFormTypeId = oGridN.getSBOForm().TypeEx;
                idh.dbObjects.User.IDH_FORMSET oFormSettings = (idh.dbObjects.User.IDH_FORMSET)getWFValue("FRMSET", sFormTypeId + "." + oGridN.GridId);
                if (oFormSettings == null) {
                    oFormSettings = new idh.dbObjects.User.IDH_FORMSET();
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, "");
                    setWFValue("FRMSET", sFormTypeId + "." + oGridN.GridId, oFormSettings);
                    oFormSettings.doAddFieldToGrid(oGridN);
                    doSetListFields(oGridN);
                    oFormSettings.doSyncDB(oGridN);
                } else {
                    if (oFormSettings.SkipFormSettings) {
                        doSetListFields(oGridN);
                    } else {
                        oFormSettings.doAddFieldToGrid(oGridN);
                    }
                }
            } else {
                doSetListFields(oGridN);
            }
        }

        protected void doSetListFields(UpdateGrid moGrid) {

            moGrid.doAddListField("[WOHeader]", "WOHeader", false, -1, null, null);
            moGrid.doAddListField("[WORow]", "WORow", false, 20, null, null);

            moGrid.doAddListField("[Customer Code]", "Customer code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            moGrid.doAddListField("[Customer Name]", "Customer Name", false, -1, null, null);

            moGrid.doAddListField("[Site Address1]", "Site Address1", false, -1, null, null);
            moGrid.doAddListField("[Site Address2]", "Site Address2", false, -1, null, null);


            moGrid.doAddListField("[Site Address3]", "Site Address3", false, -1, null, null);
            moGrid.doAddListField("[Site Address4]", "Site Address4", false, -1, null, null);
            moGrid.doAddListField("[Site Address5]", "Site Address5", false, -1, null, null);
            moGrid.doAddListField("[Post Code]", "Post Code", false, -1, null, null);
            moGrid.doAddListField("[Supplier Code]", "Supplier Code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            moGrid.doAddListField("[Supplier Name]", "Supplier Name", false, -1, null, null);
            moGrid.doAddListField("[Disposal site / Supplier]", "Disposal site / Supplier", false, -1, null, null);
            moGrid.doAddListField("[Waste Carrier Licence]", "Waste Carrier Licence", false, -1, null, null);

            moGrid.doAddListField("PBINumber", "PBI Number", false, -1, null, null);
            moGrid.doAddListField("[Order Type]", "Order Type", false, -1, ListFields.LISTTYPE_COMBOBOX, null);
            moGrid.doAddListField("[Container Code]", "Container Code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            moGrid.doAddListField("[Container Desc]", "Container Desc", false, -1, null, null);
            moGrid.doAddListField("[Waste Code]", "Waste Code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            moGrid.doAddListField("[Waste Desc]", "Waste Desc", false, -1, null, null);

            moGrid.doAddListField("[EWC Code]", "EWC Code", false, -1, null, null);
            moGrid.doAddListField("[Site Clearance]", "Site Clearance", false, -1, null, null);
            moGrid.doAddListField("[Site Contact]", "Site Contact", false, -1, null, null);
            moGrid.doAddListField("[Site Tel]", "Site Tel", false, -1, null, null);
            moGrid.doAddListField("[Site Fax]", "Site Fax", false, -1, null, null);
            moGrid.doAddListField("[LiftType]", "LiftType", false, -1, null, null);
            moGrid.doAddListField("[Auto Complete StartDate]", "Auto Complete StartDate", false, -1, null, null);
            moGrid.doAddListField("[Auto Complete EndDate]", "Auto Complete EndDate", false, -1, null, null);
            moGrid.doAddListField("[Qty]", "Qty", false, -1, null, null);
            moGrid.doAddListField("[Schedule Start Date]", "Schedule Start Date", false, -1, null, null);
            moGrid.doAddListField("[Schedule END Date]", "Schedule END Date", false, -1, null, null);
            moGrid.doAddListField("[Txt]", "Txt", false, -1, null, null);
            moGrid.doAddListField("[Lifts Per Week]", "Lifts Per Week", false, -1, null, null);
            moGrid.doAddListField("[Mon]", "Mon", false, -1, null, null);
            moGrid.doAddListField("[Tue]", "Tue", false, -1, null, null);
            moGrid.doAddListField("[Wed]", "Wed", false, -1, null, null);
            moGrid.doAddListField("[Thu]", "Thu", false, -1, null, null);
            moGrid.doAddListField("[Fri]", "Fri", false, -1, null, null);
            moGrid.doAddListField("[Sat]", "Sat", false, -1, null, null);
            moGrid.doAddListField("[Sun]", "Sun", false, -1, null, null);
            moGrid.doAddListField("[Cust Disposal Units]", "Cust Disposal Units", false, -1, null, null);
            moGrid.doAddListField("[Cust Read Weight]", "Cust Read Weight", false, -1, null, null);
            moGrid.doAddListField("[Cust Charge Weight]", "Cust Charge Weight", false, -1, null, null);
            moGrid.doAddListField("[Cust Disposal UOM]", "Cust Disposal UOM", false, -1, null, null);
            moGrid.doAddListField("[Cust Disposal Rate]", "Cust Disposal Rate", false, -1, null, null);
            moGrid.doAddListField("[Cust Disposal Total]", "Cust Disposal Total", false, -1, null, null);
            moGrid.doAddListField("[Cust Haulage Qty]", "Cust Haulage Qty", false, -1, null, null);

            moGrid.doAddListField("[Cust Haulage Rate]", "Cust Haulage Rate", false, -1, null, null);
            moGrid.doAddListField("[Cust Haulage Total]", "Cust Haulage Total", false, -1, null, null);
            moGrid.doAddListField("[Cust SubTotal Before Discount]", "Cust SubTotal Before Discount", false, -1, null, null);
            moGrid.doAddListField("[Cust Discount Amt]", "Cust Discount Amt", false, -1, null, null);
            moGrid.doAddListField("[Cust SubTotal After Discount]", "Cust SubTotal After Discount", false, -1, null, null);
            moGrid.doAddListField("[Cust VAT Amount]", "Cust VAT Amount", false, -1, null, null);

            moGrid.doAddListField("[Cust Additional Charges]", "Cust Additional Charges", false, -1, null, null);
            moGrid.doAddListField("[Cust Order Total]", "Cust Order Total", false, -1, null, null);
            moGrid.doAddListField("[Supp Disposal Weight]", "Supp Disposal Weight", false, -1, null, null);
            moGrid.doAddListField("[Supp Disposal UOM]", "Supp Disposal UOM", false, -1, null, null);
            moGrid.doAddListField("[Supp Disposal Rate]", "Supp Disposal Rate", false, -1, null, null);
            moGrid.doAddListField("[Supp Disposal Total]", "Supp Disposal Total", false, -1, null, null);
            moGrid.doAddListField("[Supp Haulage Qty]", "Supp Haulage Qty", false, -1, null, null);
            moGrid.doAddListField("[Supp Haulage Rate]", "Supp Haulage Rate", false, -1, null, null);
            moGrid.doAddListField("[Supp Haulage Total]", "Supp Haulage Total", false, -1, null, null);
            moGrid.doAddListField("[Supp SubTotal]", "Supp SubTotal", false, -1, null, null);
            moGrid.doAddListField("[Supp VAT Amount]", "Supp VAT Amount", false, -1, null, null);
            moGrid.doAddListField("[Supp Additional Costs]", "Supp Additional Costs", false, -1, null, null);
            moGrid.doAddListField("[Supp Order Total]", "Supp Order Total", false, -1, null, null);
            moGrid.doAddListField("[Profit/Loss]", "Profit/Loss", false, -1, null, null);
            moGrid.doAddListField("[Total Cust Charge]", "Total Cust Charge", false, -1, null, null);
            moGrid.doAddListField("[Total Supp Cost]", "Total Supp Cost", false, -1, null, null);
            moGrid.doAddListField("[Expr1]", "Expr1", false, -1, null, null);
            moGrid.doAddListField("[Expr2]", "Expr2", false, -1, null, null);
            moGrid.doAddListField("[Origin]", "Origin", false, -1, null, null);
            moGrid.doAddListField("[SubsDone]", "SubsDone", false, -1, null, null);
            moGrid.doAddListField("[U_Covera]", "U_Covera", false, -1, null, null);
            moGrid.doAddListField("[JobRequestedDate]", "JobRequestedDate", false, -1, null, null);
            moGrid.doAddListField("[JobRequestedDateMonth]", "JobRequestedDateMonth", false, -1, null, null);
            moGrid.doAddListField("[JobRequestedDateYear]", "JobRequestedDateYear", false, -1, null, null);
            moGrid.doAddListField("[JobRequestedDateYearMonth]", "JobRequestedDateYearMonth", false, -1, null, null);
            moGrid.doAddListField("[JobStartDate]", "JobStartDate", false, -1, null, null);
            moGrid.doAddListField("[JobEndDate]", "JobEndDate", false, -1, null, null);
            moGrid.doAddListField("[PBIJobEndDate]", "PBIJobEndDate", false, -1, null, null);
            moGrid.doAddListField("[PO Ref]", "PO Ref", false, -1, null, null);
            moGrid.doAddListField("[Freqency]", "Freqency", false, -1, null, null);

        }


        protected void doSetGridFilters(FilterGrid moGridN) {

            moGridN.doAddFilterField("WOHCode", "[WOHeader]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("WORCode", "[WORow]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_CUST", "[Customer Code]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_NAME", "[Customer Name]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_CADDR", "[Site Address1]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_CSTRET", "[Site Address2]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_CITY", "[Site Address4]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_CPCODE", "[Post Code]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_SUPCD", "[Supplier Code]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_SUPNM", "[Supplier Name]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_SUPADR", "[Disposal site / Supplier]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_WSTCRL", "[Waste Carrier Licence]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_PBINUM", "[PBINumber]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_JOBTYP", "[Order Type]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_ITEMCD", "[Container Code]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_ITEMNM", "[Container Desc]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WASTCD", "[Waste Code]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_WASTNM", "[Waste Desc]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_EWCCD", "[EWC Code]", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_SCDSDT", "[Schedule Start Date]", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10);


            moGridN.doAddFilterField("IDH_SCDEDT", "[Schedule End Date]", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10);
            //moGridN.doAddFilterField("IDH_WSFDC", "" + IDH_WOQITEM._WasFDsc, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            //moGridN.doAddFilterField("IDH_WSDC", "" + IDH_WOQITEM._WasDsc, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            //doAddUF(moGridN.getSBOForm(), "IDH_SEARCH", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 250, false, false);
            //doAddUF(moGridN.getSBOForm(), "IDH_SHADIT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, false, false);


        }

        //*** Add event filters to avoid receiving all events from SBO
        protected override void doSetEventFilters() {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE);
        }

        protected override void doReadInputParams(SAPbouiCOM.Form oForm) {
            //base.doReadInputParams(oForm);
            if (getHasSharedData(oForm)) {
                UpdateGrid oGridN = default(UpdateGrid);
                oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");

                int iIndex = 0;
                for (iIndex = 0; iIndex <= oGridN.getGridControl().getFilterFields().Count - 1; iIndex++) {
                    com.idh.controls.strct.FilterField oField = (com.idh.controls.strct.FilterField)(oGridN.getGridControl().getFilterFields()[iIndex]);
                    string sFieldName = oField.msFieldName;
                    string sFieldValue = getParentSharedData(oForm, sFieldName).ToString();
                    try {
                        setUFValue(oForm, sFieldName, sFieldValue);
                    } catch (Exception ex) {
                    }
                }
            }
        }

        //** Create the form
        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            try {
                oForm.Title = gsTitle;
                int iWidth = oForm.Width;

                UpdateGrid oGridN = new UpdateGrid(this, oForm, "LINESGRID", 7, 100, iWidth - 20, 255, 0, 0);

                oGridN.getSBOGrid().SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto;
                oForm.AutoManaged = false;
                oGridN.getSBOItem().AffectsFormMode = false;

                oForm.EnableMenu(Config.NAV_ADD, false);
                oForm.EnableMenu(Config.NAV_FIND, false);
                oForm.EnableMenu(Config.NAV_FIRST, false);
                oForm.EnableMenu(Config.NAV_NEXT, false);
                oForm.EnableMenu(Config.NAV_PREV, false);
                oForm.EnableMenu(Config.NAV_LAST, false);

                ////Set Documents button caption on Job Managers
                //SAPbouiCOM.Item oItm = default(SAPbouiCOM.Item);
                //SAPbouiCOM.Button oBtn = default(SAPbouiCOM.Button);
                //try {
                //    oItm = oForm.Items.Item("IDH_DOC");
                //    if (oItm != null) {
                //        oBtn = (SAPbouiCOM.Button)oItm.Specific;
                //        oBtn.Caption = Config.ParameterWithDefault("BTDOCCAP", "Documents");
                //    }
                //} catch (Exception ex) {
                //}
                oForm.SupportedModes = Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_All);

                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);
                base.doCompleteCreate(ref oForm, ref BubbleEvent);

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDLF", null);
                BubbleEvent = false;
            }
        }

        protected void doSetMode(SAPbouiCOM.Form oForm, SAPbouiCOM.BoFormMode iMode) {
            try {
                object sFormType = getParentSharedData(oForm, "FTYPE");
                bool bIsSearch = false;
                if ((sFormType != null) && sFormType.Equals("SEARCH") == true) {
                    bIsSearch = true;
                }

                if (iMode == SAPbouiCOM.BoFormMode.fm_FIND_MODE) {
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = Translation.getTranslatedWord("Find");
                    //if (bIsSearch) {
                    //    oForm.Items.Item("2").Visible = true;
                    //} else {
                    //    oForm.Items.Item("2").Visible = false;
                    //}
                } else if (iMode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = Translation.getTranslatedWord("Update");
                   // oForm.Items.Item("2").Visible = true;
                }
            } catch (Exception ex) {
            }
        }

        public string getListRequiredStr(SAPbouiCOM.Form OForm) {
            return " 1=1 ";
            //return " r." + IDH_WOQITEM._JobNr + " = h.Code And h." + IDH_WOQHD._User + "=u.USERID ";
        }

        //private void doFillGridCombos(UpdateGrid moGrid) {
        //   // SAPbouiCOM.ComboBoxColumn oCombo;
        //    //int iIndex;
        //    //iIndex = moGrid.doIndexFieldWC("" + IDH_WOQITEM._UOM);
        //    //oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
        //    //oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
        //    //com.uBC.utils.FillCombos.UOMCombo(oCombo);

        //    //iIndex = moGrid.doIndexFieldWC("R_Status");
        //    //oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
        //    //oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
        //    //com.uBC.utils.FillCombos.FillWR1StatusNew(oCombo, 107);

        //    //iIndex = moGrid.doIndexFieldWC(IDH_WOQHD._Status);
        //    //oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
        //    //oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
        //    //com.uBC.utils.FillCombos.FillWR1StatusNew(oCombo, 106);

        //    //iIndex = moGrid.doIndexFieldWC(IDH_WOQHD._Source);
        //    //oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
        //    //oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
        //    //com.uBC.utils.FillCombos.FillWR1StatusNew(oCombo, 108);

        //    //iIndex = moGrid.doIndexFieldWC("[Order Type]");
        //    //oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
        //    //oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
        //    //string msOrdCat = "Waste Order";
        //    //doFillCombo(moGrid.getSBOForm(), oCombo, "OITB g, [@IDH_JOBTYPE] jt", "ItmsGrpCod", "ItmsGrpNam", "g.ItmsGrpCod=jt.U_ItemGrp AND jt.U_OrdCat='" + msOrdCat + "'", "", "", "");

        //    ////doFillJobTypeCombo(moGrid);
        //    ////FillCombos.FillWR1StatusNew(Items.Item("IDH_STATUS").Specific, 101);



        //}
        private void doFillJobTypeCombo(SAPbouiCOM.Form oForm) {
            try {
                SAPbouiCOM.ComboBox oCombo;
                //int iIndex = moGrid.doIndexFieldWC("" + IDH_WOQITEM._JobTp);
                oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_JOBTYP").Specific;
                oCombo.Item.DisplayDesc = true;
                doClearValidValues(oCombo.ValidValues);
                string sItemGrp = "";// moWOQ.U_ItemGrp;
                com.uBC.utils.FillCombos.doFillJobTypeCombo(ref oCombo, sItemGrp);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }
        }
        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {

            
            UpdateGrid oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");

            if (oGridN == null) {
                //int iWidth = oForm.Width;
                oGridN = new UpdateGrid(this, oForm, "LINESGRID"); //, 7, 100, iWidth - 20, 255, 0, 0);
            }

            //Required tables
            // oGridN.doAddGridTable(new GridTable(IDH_WOQITEM.TableName, "r", "Code", true, true), true);
            // oGridN.doAddGridTable(new GridTable(IDH_WOQHD.TableName, "h", "Code", true, true));
            // oGridN.doAddGridTable(new GridTable("OUSR", "u", "USERID", true, true));
            string sLiveServiceView = Config.INSTANCE.getParameter("ALLVSRVW");
            if (sLiveServiceView == null || sLiveServiceView == string.Empty) {
                com.idh.bridge.DataHandler.INSTANCE.doUserError("Please enter a valid view for All live services under the config ALLVSRVW.");
                return;
            }
            oGridN.doAddGridTable(new GridTable(sLiveServiceView, "r", "PBINumber", false, true), true);

            doSetGridFilters(oGridN);
            oGridN.setRequiredFilter(getListRequiredStr(oForm));
            oGridN.doSetDoCount(true);
            oGridN.doSetBlankLine(false);

            //Set the required List Fields
            doTheGridLayout(oGridN);
            //doFillJobTypeCombo(oForm);

            //doFillHeaderCombos(oForm);

            bool bHasParams = false;
            if (getHasSharedData(oForm)) {
                doReadInputParams(oForm);
                bHasParams = true;

                string sCardCode = getParentSharedData(oForm, "CARDCODE").ToString();
                if ((sCardCode != null) && sCardCode.Length > 0) {
                    string sExtraReqFilter = oGridN.getRequiredFilter();

                    if (sExtraReqFilter.Length > 0) {
                        sExtraReqFilter = sExtraReqFilter + " AND ";
                    }

                    sExtraReqFilter = sExtraReqFilter + " ( r.[Customer Code] " + " LIKE '" + sCardCode + "%') ";

                    oGridN.setRequiredFilter(sExtraReqFilter);
                }
            }

            if (bHasParams == false) {
                oGridN.setInitialFilterValue("PBINumber = '-100'");
            }
        }

        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            doReLoadData(oForm, !getHasSharedData(oForm));
        }


        //** The Initializer
        protected void doReLoadData(SAPbouiCOM.Form oForm, bool bIsFirst) {
            try {
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);

                UpdateGrid oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");
                string sFilter = oGridN.getRequiredFilter();
                // doSetCustomeFilter(oGridN);

                //oGridN.setOrderValue("Cast(" + IDH_WOQITEM._JobNr + " as Int)," + IDH_WOQITEM._Sort + " ");
                oGridN.setMaxRows(100000);

                oGridN.doReloadData("", false, true);
                // doFillGridCombos(oGridN);
                oGridN.setRequiredFilter(sFilter);

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBL", null);
            }
        }

        //protected void doSetCustomeFilter(UpdateGrid oGridN) {
        //    SAPbouiCOM.Form oForm = oGridN.getSBOForm();
        //    if (getItemValue(oForm, "IDH_SHADIT").ToString().Trim() == "Y") {
        //    } else {
        //        string sStatusQry = IDH_WOQITEM._AdtnlItm + "=''";//doGetStatusFilterQueryDynamic(getItemValue(oForm, "IDH_PROG").ToString.Trim.ToUpper);
        //        oGridN.setRequiredFilter(oGridN.getRequiredFilter() + " And " + sStatusQry);
        //    }
        //    if (getItemValue(oForm, "IDH_SEARCH").ToString().Trim() != string.Empty) {
        //        string sSearchWord = getItemValue(oForm, "IDH_SEARCH").ToString().Trim().Replace("*", "%").Replace("'", "''");
        //        string sExistingReqFilter = oGridN.getRequiredFilter();
        //        string sSQL = "";
        //        bool bAddNot = false;
        //        string sNotOp = "";

        //        if (sSearchWord.StartsWith("!")) {
        //            bAddNot = true;
        //            sSearchWord = sSearchWord.TrimStart('!');
        //            sNotOp = " NOT ";
        //        }
        //        if (sSearchWord.Contains(",")) {
        //            string[] aList = sSearchWord.Split(',');
        //            sSearchWord = "";
        //            int i = 0;
        //            for (i = 0; i < aList.Length; i++) {

        //                sSQL += "¬  (" +
        //                            "Cast(" + IDH_WOQHD._Code + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._Address + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            " USER_CODE" + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._Block + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            " BranchName" + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CardCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CardNM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CCardCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CCardNM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._ClgCode + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CCity + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CContact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._City + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CntrNo + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._Contact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._County + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CPhone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CState + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CStreet + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CusFrnNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            //"Cast(" + IDH_WOQHD._CustRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._E_Mail + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._ENNO + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            " HItemGrp" + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._MAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._MBlock + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._MCity + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._MContact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._MPhone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._MState + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._MStreet + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._MZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._Notes + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PBlock + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PCardCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PCardNM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PCity + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PContact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._Phone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PPhone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PremCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._ProRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PState + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PStreet + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._ReviewNotes + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._Route + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SBlock + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SCardCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SCardNM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SCity + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SContact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SPhone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SpInst + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SState + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SStreet + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._State + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._Street + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SupRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._TCharge + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            //"Cast(" + IDH_WOQHD._WasLic + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._WOHID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._ZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            " HSourceDesc" + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            " HStatusDesc " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._AddItmID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._AdtnlItm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._AltWasDsc + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._AUOM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "(" + IDH_WOQITEM._AUOM + sNotOp + "  IN  (Select UnitDisply from OWGT WITH(NOLOCK) where UnitName Like " + "'" + aList[i].Trim() + "'" + ") ) OR " +
        //                            "Cast(" + IDH_WOQITEM._CarrCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._CarrNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._CarrRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._CCPCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._Comment + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._CusQty + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._CustCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._CustNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            //"Cast(" + IDH_WOQITEM._CustRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._EnqID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._ItemCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._ItemDsc + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            " RItemGrp " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._JobNr + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._JobTp + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._Lorry + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._LorryCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._PayMeth + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._ProCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._ProNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._ProRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._SAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._SupRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._Tip + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._TipCost + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._TipNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._TipTot + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._Total + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._UOM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._WasCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._WasDsc + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._WasFDsc + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._WOHID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._WORID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._WstGpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._WstGpNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            " RStatusDesc " + sNotOp + "Like " + "'" + aList[i].Trim() + "'" + " OR " +
        //                            "(" + IDH_WOQITEM._UOM + sNotOp + "  IN  (Select UnitDisply from OWGT WITH(NOLOCK) where UnitName Like " + "'" + aList[i].Trim() + "'" + ") ) OR " +
        //                            "Cast(" + IDH_WOQITEM._CusQty + " As NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + "  " +
        //                             ")";
        //            }
        //            sSQL = sSQL.Trim().TrimStart('¬').Trim();
        //            sSQL = sSQL.Replace("¬", " OR ");

        //            if (bAddNot)
        //                sSQL = sSQL.Replace(" OR ", " AND ");
        //            sSQL = " And " + sSQL;

        //        } else {
        //            //sSearchWord = "'" + sSearchWord + "'";
        //            sSQL += "  (" +
        //                            "Cast(" + IDH_WOQHD._Code + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._Address + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            " USER_CODE" + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._Block + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            " BranchName" + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CardCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CardNM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CCardCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CCardNM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._ClgCode + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CCity + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CContact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._City + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CntrNo + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._Contact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._County + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CPhone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CState + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CStreet + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CusFrnNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            ///"Cast(" + IDH_WOQHD._CustRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._CZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._E_Mail + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._ENNO + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            " HItemGrp" + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._MAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._MBlock + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._MCity + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._MContact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._MPhone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._MState + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._MStreet + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._MZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._Notes + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PBlock + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PCardCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PCardNM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PCity + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PContact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._Phone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PPhone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PremCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._ProRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PState + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PStreet + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._PZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._ReviewNotes + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._Route + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SBlock + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SCardCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SCardNM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SCity + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SContact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SPhone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SpInst + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SState + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SStreet + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._State + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._Street + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SupRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._SZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._TCharge + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            //"Cast(" + IDH_WOQHD._WasLic + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._WOHID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQHD._ZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            " HSourceDesc" + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            " HStatusDesc " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._AddItmID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._AdtnlItm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._AltWasDsc + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._AUOM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "(" + IDH_WOQITEM._AUOM + sNotOp + "  IN  (Select UnitDisply from OWGT WITH(NOLOCK) where UnitName Like " + "'" + sSearchWord + "'" + ") ) OR " +
        //                            "Cast(" + IDH_WOQITEM._CarrCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._CarrNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._CarrRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._CCPCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._Comment + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._CusQty + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._CustCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._CustNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            //"Cast(" + IDH_WOQITEM._CustRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._EnqID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._ItemCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._ItemDsc + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            " RItemGrp " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._JobNr + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._JobTp + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._Lorry + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._LorryCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._PayMeth + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._ProCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._ProNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._ProRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._SAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._SupRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._Tip + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._TipCost + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._TipNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._TipTot + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._Total + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._UOM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._WasCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._WasDsc + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._WasFDsc + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._WOHID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._WORID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._WstGpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "Cast(" + IDH_WOQITEM._WstGpNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
        //                            " RStatusDesc " + sNotOp + "Like " + "'" + sSearchWord + "'" + " OR " +
        //                            "(" + IDH_WOQITEM._UOM + sNotOp + "  IN  (Select UnitDisply from OWGT WITH(NOLOCK) where UnitName Like " + "'" + sSearchWord + "'" + ") ) OR " +
        //                            "Cast(" + IDH_WOQITEM._CusQty + " As NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + "  " +
        //                             ")";


        //            if (bAddNot)
        //                sSQL = sSQL.Replace(" OR ", " AND ");
        //            sSQL = " And " + sSQL;
        //        }
        //        oGridN.setRequiredFilter(oGridN.getRequiredFilter() + " " + sSQL);
        //    }
        //}

        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED) {
                if (pVal.ItemUID == "IDH_EXRT") {
                    doExporttoExcelODBC(oForm);
                }
            }
            return base.doItemEvent(oForm, ref pVal, ref BubbleEvent);
        }
        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == true) {
                if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update") || oForm.Mode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {

                } else if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find")) {
                    doReLoadData(oForm, true);
                }
                BubbleEvent = false;
            }
        }
        //public override void doButtonID2(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (pVal.BeforeAction == true) {
        //        doReLoadData(oForm, true);
        //    }
        //}

        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCloseForm(oForm, ref BubbleEvent);
            UpdateGrid.doRemoveGrid(oForm, "LINESGRID");
        }
        public override void doClose() {
        }
        protected void doGridDoubleClick(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
            if (pVal.Row >= 0) {

                string sRowCode = oGridN.doGetFieldValue(IDH_WOQITEM._Code).ToString();
                string sWOQID = oGridN.doGetFieldValue(IDH_WOQITEM._JobNr).ToString();

                ArrayList oData = new ArrayList();
                if (sWOQID.Length == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Valid row must be selected.", "ERUSJOBS", null);
                } else {
                    //if (oGridN.doCheckIsSameCol(pVal.ColUID,  IDH_WOQITEM._EnqID) || oGridN.doCheckIsSameCol(pVal.ColUID,  IDH_WOQITEM._EnqLID)) {
                    //    //Open Enquiry
                    //    com.isb.forms.Enquiry.Enquiry oOOForm = new com.isb.forms.Enquiry.Enquiry(null, oForm.UniqueID, null);
                    //    oOOForm.EnquiryID = oGridN.doGetFieldValue( IDH_WOQITEM._EnqID).ToString();
                    //    oOOForm.bLoadEnquiry = true;
                    //    oOOForm.bLoadReadOnly = false;
                    //    oOOForm.Handler_DialogButton1Return = new com.idh.forms.oo.Form.DialogReturn(doHandleEnquiryButton1Return);
                    //    oOOForm.doShowModal(oForm);
                    //    oOOForm.DBEnquiry.SBOForm = oOOForm.SBOForm;

                    //} else
                    if (oGridN.doCheckIsSameCol(pVal.ColUID, IDH_WOQITEM._WOHID)) {
                        //Open WOH
                        ArrayList moData = new ArrayList();
                        moData.Add("DoUpdate");
                        moData.Add(oGridN.doGetFieldValue(IDH_WOQITEM._WOHID).ToString());
                        goParent.doOpenModalForm("IDH_WASTORD", oForm, moData);
                    } else if (oGridN.doCheckIsSameCol(pVal.ColUID, IDH_WOQITEM._WORID)) {
                        //Open WOR
                        ArrayList moData = new ArrayList();
                        moData.Add("DoUpdate");
                        moData.Add(oGridN.doGetFieldValue(IDH_WOQITEM._WORID).ToString());
                        setSharedData("IDHJOBS", "ROWCODE", oGridN.doGetFieldValue(IDH_WOQITEM._WORID).ToString());
                        goParent.doOpenModalForm("IDHJOBS", oForm, moData);
                    } else {//PBI

                    }
                }
            }
        }

        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK) {
                if (pVal.ItemUID == "LINESGRID") {
                    if (pVal.BeforeAction == false) {
                        doGridDoubleClick(oForm, ref pVal);
                    }
                }
            }
            //else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT && !pVal.BeforeAction) {
            //    //		oData.GetType()	{Name = "MenuEventClass" FullName = "SAPbouiCOM.MenuEventClass"}	System.Type {System.RuntimeType}
            //    SAPbouiCOM.MenuEventClass oData = (SAPbouiCOM.MenuEventClass)pVal.oData;
            //    if (oData.MenuUID == IDHGrid.GRIDMENUSORTASC) {
            //        string sField = pVal.oGrid.doFindDBField(pVal.ColUID).Trim();
            //        if (sField.Length > 0 && (sField ==  IDH_WOQITEM._Code || sField == IDH_WOQITEM._JobNr || sField == IDH_WOQITEM._EnqID || sField == IDH_WOQITEM._EnqLID)) {
            //            UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
            //            string sFilter = oGridN.getRequiredFilter();
            //            doSetCustomeFilter(oGridN);

            //            oGridN.setOrderValue("Cast( " + sField + " As Int) ");
            //            oGridN.doReloadData("ASC", false);
            //            //doFillGridCombos(oGridN);
            //            oGridN.doApplyRules();
            //            oGridN.setRequiredFilter(sFilter);
            //            return false;
            //        }
            //    } else if (oData.MenuUID == IDHGrid.GRIDMENUSORTDESC) {
            //        string sField = pVal.oGrid.doFindDBField(pVal.ColUID).Trim();
            //        if (sField.Length > 0 && (sField == IDH_WOQITEM._Code || sField ==  IDH_WOQITEM._JobNr || sField == IDH_WOQITEM._EnqID || sField == IDH_WOQITEM._EnqLID)) {
            //            UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
            //            string sFilter = oGridN.getRequiredFilter();
            //            doSetCustomeFilter(oGridN);

            //            oGridN.setOrderValue("Cast( " + sField + " As Int) ");
            //            oGridN.doReloadData("DESC", false);
            //            //doFillGridCombos(oGridN);
            //            oGridN.doApplyRules();
            //            oGridN.setRequiredFilter(sFilter);
            //            return false;
            //        }
            //    }
            //}
            else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SORT && pVal.BeforeAction == false) {
                UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
                //doFillGridCombos(oGridN);
                oGridN.doApplyRules();
            }
            return base.doCustomItemEvent(oForm, ref pVal);
        }

        //public bool doHandleEnquiryButton1Return(com.idh.forms.oo.Form oDialogForm) {
        //    try {
        //        com.isb.forms.Enquiry.Enquiry oOOForm = (com.isb.forms.Enquiry.Enquiry)oDialogForm;
        //        doReLoadData(oOOForm.SBOParentForm, true);
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doHandleEnquiryButton1Return" });
        //    }
        //    return true;
        //}
        //public bool doHandleWOQButton1Return(com.idh.forms.oo.Form oDialogForm) {
        //    try {
        //        com.isb.forms.Enquiry.WOQuote oOOForm = (com.isb.forms.Enquiry.WOQuote)oDialogForm;
        //        doReLoadData(oOOForm.SBOParentForm, true);
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doHandleWOQButton1Return" });
        //    }
        //    return true;
        //}

        #region Export to Excel
        private void doExporttoExcel(SAPbouiCOM.Form oForm) {
            //   CSharpJExcel.Jxl.Workbook.getVersion( new FileInfo("F:\temp\text.xls"));
            try {
                FilterGrid oGridN = FilterGrid.getInstance(oForm, "LINESGRID");
                if (oGridN == null) {
                    oGridN = new FilterGrid(this, oForm, "LINESGRID", false); //, 7, 100, iWidth - 20, 255, 0, 0);
                }
                if (oGridN.getRowCount() == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("No data available for export.");
                    return;
                }

                IDH_LVSCNG objConfig = new IDH_LVSCNG();
                objConfig.UnLockTable = true;
                objConfig.getData(IDH_LVSCNG._Select + "=\'Y\' ", " Code for browse ");
                string sFileName = "";
                doCallWinFormOpener(delegate { ShowFolderBrowser(oForm, ref sFileName); }, oForm);
                if (sFileName == null || sFileName.Trim() == "") {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Please provide a valid file name.");
                    return;
                }

                int iRow = 1;
                ArrayList aColList = new ArrayList();
                StringWriter osw = new StringWriter();

                osw.WriteLine("sep=\t");
                if (objConfig.Count > 0) {
                    objConfig.first();
                    int iCol = 1;
                    while (objConfig.next()) {
                        osw.Write(objConfig.U_FieldName + "\t");
                        aColList.Add(oGridN.doIndexFieldWC("[" + objConfig.U_FieldName + "]"));
                        iCol++;
                    }
                    osw.Write("\n");
                    iRow++;

                } else {
                    int iCol = 1;
                    for (int i = 0; i <= oGridN.getColumnCount() - 1; i++) {
                        osw.Write(oGridN.doGetListFieldName(i) + "\t");
                        aColList.Add(i);
                        iCol++;
                    }
                    osw.Write("\n");
                    iRow++;
                }
                com.idh.bridge.DataHandler.INSTANCE.doInfo("Please wait while system is exporting data to csv file.");//, SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                
                for (int iR = 0; iR <= oGridN.getRowCount() - 1; iR++) {
                    for (int iCol = 1; iCol <= aColList.Count; iCol++) {
                        osw.Write(oGridN.doGetFieldValue((int)aColList[iCol - 1], iR) + "\t");
                    }
                    osw.Write("\n");
                    iRow++;
                    DataHandler.INSTANCE.doProgress("EXPCSV", iRow, oGridN.getRowCount());
                }
                DataHandler.INSTANCE.doProgressDone("EXPCSV");
                using (StreamWriter sw = new StreamWriter(sFileName)) {
                    sw.Write(osw.ToString());
                    sw.Close();
                }

                osw.Close();
                com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(Translation.getTranslatedMessage("EXPEXLDN", "Successfully export the data in " + sFileName), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error retrieving the Customers Address - " + sCardCode);
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new String[] { "Exporting All live services to csv." });
            }
        }

        
        private void doExporttoExcelODBC(SAPbouiCOM.Form oForm) {
            //   CSharpJExcel.Jxl.Workbook.getVersion( new FileInfo("F:\temp\text.xls"));
            try {
                FilterGrid oGridN = FilterGrid.getInstance(oForm, "LINESGRID");
                if (oGridN == null) {
                    oGridN = new FilterGrid(this, oForm, "LINESGRID", false); //, 7, 100, iWidth - 20, 255, 0, 0);
                }
                if (oGridN.getRowCount() == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("No data available for export.");
                    return;
                }

                IDH_LVSCNG objConfig = new IDH_LVSCNG();
                objConfig.UnLockTable = true;
                objConfig.getData(IDH_LVSCNG._Select + "=\'Y\' ", " Code for browse ");
                string sFileName = "";
                doCallWinFormOpener(delegate { ShowFolderBrowser(oForm, ref sFileName); }, oForm);
                if (sFileName == null || sFileName.Trim() == "") {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Please provide a valid file name.");
                    return;
                }

                int iRow = 1;
                ArrayList aColList = new ArrayList();
                string sODBC = Config.INSTANCE.getParameter("REPDSN");
                string sODBCUser = Config.INSTANCE.getParameter("REPUSR");
                string sODBCPwd = Config.INSTANCE.getParameter("REPPAS");
                string sLastSQL = oGridN.getGridControl().getLastGeneratedQuery();
                StringWriter osw = new StringWriter();
                osw.WriteLine("sep=\t");

                using (System.Data.Odbc.OdbcConnection cn = new System.Data.Odbc.OdbcConnection("dsn="+sODBC+";UID="+ sODBCUser+";PWD="+ sODBCPwd +";")) {
                    //System.Data.Odbc.OdbcConnection cn;
                    //System.Data.Odbc.OdbcCommand cmd;
                    //string MyString;

                    //MyString = "Select * from Customers";

                    //cn = new System.Data.Odbc.OdbcConnection("dsn=myDSN;UID=myUid;PWD=myPwd;");
                    com.idh.bridge.DataHandler.INSTANCE.doInfo("Please wait while system is exporting data to csv file.");//, SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);

                    cn.Open();
                    //MessageBox.Show("Connected");
                    //cmd.ex
                    //System.Data.DataSet dt=new System.Data.DataSet();
                    //dt.sa
                    //cmd = new System.Data.Odbc.OdbcCommand(sLastSQL, cn);
                    //System.Data.DataSet ds = cmd.ExecuteScalar();
                    System.Data.Odbc.OdbcDataAdapter adapter =new System.Data.Odbc.OdbcDataAdapter(sLastSQL, cn);
                    System.Data.DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    System.Data.DataTable dt = ds.Tables[0];

                    if (objConfig.Count > 0) {
                        objConfig.first();
                        int iCol = 1;
                        while (objConfig.next()) {
                            osw.Write(objConfig.U_FieldName + "\t");
                            aColList.Add(oGridN.doIndexFieldWC("[" + objConfig.U_FieldName + "]"));
                            //if (!dt.Columns.Contains(objConfig.U_FieldName)) {
                            //    dt.Columns.Remove(objConfig.U_FieldName);
                            //}
                            iCol++;
                            //dt.AcceptChanges();
                            
                        }
                        osw.Write("\n");
                        iRow++;
                        ArrayList aColsToRemove = new ArrayList();
                        for (iCol = dt.Columns.Count-1; iCol >=0 ; iCol--) {
                            if (!aColList.Contains(iCol)) {
                                dt.Columns.RemoveAt(iCol);
                            }
                        }
                        dt.AcceptChanges();

                    } else {
                        int iCol = 1;
                        for (int i = 0; i <= oGridN.getColumnCount() - 1; i++) {
                            osw.Write(oGridN.doGetListFieldName(i) + "\t");
                            aColList.Add(i);
                            iCol++;
                        }
                        osw.Write("\n");
                        iRow++;
                    }
                   
                    com.idh.bridge.DataHandler.INSTANCE.doInfo("Please wait while system is exporting data to csv file.");//, SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                    if (dt.Rows.Count>0) {
                        int iCRow = 0;
                        foreach (DataRow row in dt.Rows) {
                            IEnumerable<string> fields = row.ItemArray.Select(field =>
                              string.Concat("", field.ToString().Replace("\"", "\"\""), ""));
                            osw.WriteLine(string.Join("\t", fields));
                            DataHandler.INSTANCE.doProgress("EXPCSV", iCRow++, dt.Rows.Count);
                        }
                        //while (dr.Read()) {
                        //    string[] columnValues =
                        //        Enumerable.Range(0, aColList.Count)
                        //                  .Select(i => dr.GetValue(i).ToString())
                        //                  .Select(field => string.Concat("\"", field.Replace("\"", "\"\""), "\""))
                        //                  .ToArray();
                        //    osw.WriteLine(string.Join("\t", columnValues));
                        //    DataHandler.INSTANCE.doProgress("EXPCSV", iRow, oGridN.getRowCount());
                        //}
                    }
                    cn.Close();
                }

                
                //for (int iR = 0; iR <= oGridN.getRowCount() - 1; iR++) {
                //    for (int iCol = 1; iCol <= aColList.Count; iCol++) {
                //        osw.Write(oGridN.doGetFieldValue((int)aColList[iCol - 1], iR) + "\t");
                //    }
                //    osw.Write("\n");
                //    iRow++;
                //    DataHandler.INSTANCE.doProgress("EXPCSV", iRow, oGridN.getRowCount());
                //}
                DataHandler.INSTANCE.doProgressDone("EXPCSV");
                using (StreamWriter sw = new StreamWriter(sFileName)) {
                    sw.Write(osw.ToString());
                    sw.Close();
                }

                osw.Close();
                com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(Translation.getTranslatedMessage("EXPEXLDN", "Successfully export the data in " + sFileName), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error retrieving the Customers Address - " + sCardCode);
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new String[] { "Exporting All live services to csv." });
            }
        }
        public void ShowFolderBrowser(SAPbouiCOM.Form oForm, ref string sFileName) {
            System.Windows.Forms.SaveFileDialog oSaveFile = new System.Windows.Forms.SaveFileDialog();
            oSaveFile.Filter = "CSV File(*.csv)|*.csv";
            com.idh.controls.SBOWindow sbowindow1 = com.idh.controls.SBOWindow.getInstanceByTitle();
            if (oSaveFile.ShowDialog(sbowindow1) == DialogResult.OK) {
                sFileName = oSaveFile.FileName;
            }

        }
        #endregion
    }//end of class

}//end of namespace
