﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.dbObjects.User;
using IDHAddOns.idh.controls;
using com.uBC.utils;
using com.idh.controls;
using com.idh.bridge.utils;

namespace com.isb.forms.Enquiry.admin {
    public class DisposalProcess : com.uBC.forms.fr3.dbGrid.Base {

        public DisposalProcess(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
                mbIsSearch = false;
                SkipFormSettings = true;
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDHDISPPROC", sParMenu, iMenuPos, "admin.srf", false, true, false, "Disposal Process", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        protected override void doInitialize() {
            moDBObject = new IDH_DISPPRC();
        }

#region FormOpenCreateFunctions
        /*
         * Do Complete the Form
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            base.doCompleteCreate(ref BubbleEvent);
        }

       /* 
        * Do the final form steps to show before loaddata
        */
        public override void doBeforeLoadData() {
            base.doBeforeLoadData();
        }

        /* 
         * Load the Form Data
         */
        public override void doLoadData() {
            base.doLoadData();
            //moAdminGrid.doReloadSetExtra("",false,true, false);
            //moAdminGrid.doApplyRules();
            //doFillCombos();
        }

        /*
         * Set the List Fields
         */
        public override void doSetListFields() {
            moAdminGrid.doAddListField(IDH_DISPPRC._Code, "Code", false, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_DISPPRC._Name, "Name", false, -1, ListFields.LISTTYPE_IGNORE, null, -1);

            moAdminGrid.doAddListField(IDH_DISPPRC._ProcessNm, "Process Name", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_DISPPRC._ProcessCd, "Process Code", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);           
        }
#endregion

#region Events
        protected override void doSetHandlers() {
            doStartEventFilterBatch(true);          
            base.doSetHandlers();
            doCommitEventFilters();

        }

        protected override void doSetGridHandlers() {
            doStartEventFilterBatch(true);                      
            base.doSetGridHandlers();
            doCommitEventFilters();

          //  moAdminGrid.Handler_GRID_FIELD_CHANGED = new IDHGrid.ev_GRID_EVENTS(doFieldChangeEvent); 
        }
#endregion 

#region ItemEventHandlers
         
        private string setVal(string sVal) {
            if (sVal.Length == 0)
                return "0";
            else
                return sVal;
        }
        public override bool doAddUpdateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                Freeze(true);
                if (doValidate() == false) {
                    BubbleEvent = false;
                    Freeze(false);
                    return true;
                }
                if (moAdminGrid.doProcessData()) {
                    doLoadData();
                    moAdminGrid.doApplyRules();
                }
                Freeze(false);
            }
            return true;
        }
        private bool doValidate() {
            bool bResult = true;
            //int iRes=0;
            IDH_DISPPRC _IDH_DISPPRC = new IDH_DISPPRC();
            for (int iRow1 = 0; iRow1 <= moAdminGrid.getRowCount()-1; iRow1++) {
                for (int iRow2 = iRow1+1; iRow2 <= moAdminGrid.getRowCount()-1; iRow2++) {
                    if (moAdminGrid.doGetFieldValue(IDH_DISPPRC._ProcessCd, iRow1).ToString() != string.Empty && moAdminGrid.doGetFieldValue(IDH_DISPPRC._ProcessCd, iRow2).ToString() != string.Empty 
                        && moAdminGrid.doGetFieldValue(IDH_DISPPRC._ProcessCd, iRow1).ToString() == moAdminGrid.doGetFieldValue(IDH_DISPPRC._ProcessCd, iRow2).ToString()) {
                            com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Duplicate entries are not allowed:" + moAdminGrid.doGetFieldValue(IDH_DISPPRC._ProcessCd, iRow2), "ERUSDBDU",
                                new string[] { moAdminGrid.doGetFieldValue(IDH_DISPPRC._ProcessCd, iRow2).ToString() });
                            bResult = false;
                        //}
                    }
                }
            }
            return bResult;
        }
#endregion
    }
}
