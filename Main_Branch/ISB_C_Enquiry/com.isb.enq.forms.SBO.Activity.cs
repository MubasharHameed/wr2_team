﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using IDHAddOns.idh.controls;

using com.isb.enq.dbObjects.User;
using com.isb.enq;
using com.isb.enq.lookups;
using com.isb.enq.utils;
using com.idh.bridge;
using com.idh.dbObjects.numbers;
using SAPbouiCOM;

namespace com.isb.enq.forms.SBO {
    class Activity : WR1_SBOForms.idh.forms.SBO.Activity {

        public Activity(IDHAddOns.idh.addon.Base oParent) : base(oParent) {
        }

        public override bool doItemEvent(Form oForm, ref ItemEvent pVal, ref bool BubbleEvent) {
            string sItemUID = pVal.ItemUID;
            if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED ) {
                if (!pVal.BeforeAction) {
                    if (sItemUID == "IDH_ENQIRY" || (sItemUID == "IDH_ENQLK" && getDFValue(oForm, "OCLG", "U_IDHEnqID").ToString().Trim() != "")) {
                        setWFValue(oForm, "CardCode", "");
                        setWFValue(oForm, "Personal", "");
                        setWFValue(oForm, "SelectEnquiry", "");
                        setWFValue(oForm, "Remarks", "");

                        string sEnqID = (string)getDFValue(oForm, "OCLG", "U_IDHEnqID");
                        com.isb.forms.Enquiry.Enquiry oOOForm = new com.isb.forms.Enquiry.Enquiry(null, oForm.UniqueID, null);
                        oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleEnquiryOKReturn);
                        oOOForm.ActivityID = (int)getDFValue(oForm, "OCLG", "ClgCode");
                        if (!string.IsNullOrWhiteSpace(sEnqID) && sEnqID != "0") {
                            oOOForm.EnquiryID = sEnqID;
                            oOOForm.bLoadEnquiry = true;
                        } else {
                            oOOForm.CardCode = (string)getDFValue(oForm, "OCLG", "CardCode");
                            oOOForm.CardName = (string)getDFValue(oForm, "OCRD", "CardName");
                            if (!string.IsNullOrEmpty((string)getDFValue(oForm, "OCLG", "AttendUser"))) {
                                string sUserID = Config.INSTANCE.doGetUserCode(getDFValue(oForm, "OCLG", "AttendUser").ToString());
                                oOOForm.AttendUser = sUserID;
                            }
                            oOOForm.Recontact = com.idh.utils.Dates.doStrToDate((string)getDFValue(oForm, "OCLG", "Recontact"));
                            if (!string.IsNullOrEmpty((string)getDFValue(oForm, "OCLG", "CntctCode"))) {
                                string sContactName = (string)Config.INSTANCE.doLookupTableField("OCPR", "[Name]", "CardCode='" + getDFValue(oForm, "OCLG", "CardCode") + "' And [CntctCode] = '" + getDFValue(oForm, "OCLG", "CntctCode") + "'");
                                if (!string.IsNullOrEmpty(sContactName)) {
                                    oOOForm.ContactName = sContactName.ToString();
                                }
                            }
                            oOOForm.ContactPhone = (string)getDFValue(oForm, "OCLG", "Tel");
                            oOOForm.Notes = (string)getDFValue(oForm, "OCLG", "Notes");
                        }
                        oOOForm.bFromActivity = true;
                        oOOForm.doShowModal(oForm);
                        oOOForm.DBEnquiry.SBOForm = oOOForm.SBOForm;
                    } else if (!pVal.BeforeAction && (sItemUID == "IDH_WOQLK" && getDFValue(oForm, "OCLG", "U_IDHWOQID").ToString().Trim() != "")) {
                        setWFValue(oForm, "CardCode", "");
                        setWFValue(oForm, "Personal", "");
                        setWFValue(oForm, "SelectEnquiry", "");
                        setWFValue(oForm, "Remarks", "");

                        string sWOQID = (string)getDFValue(oForm, "OCLG", "U_IDHWOQID");
                        com.isb.forms.Enquiry.WOQuote oOOForm = new com.isb.forms.Enquiry.WOQuote(null, oForm.UniqueID, null);
                        oOOForm.Handler_DialogButton1Return = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQOKReturn);
                        if (!string.IsNullOrWhiteSpace(sWOQID) && sWOQID != "0") {
                            oOOForm.WOQID = sWOQID;
                            oOOForm.bLoadWOQ = true;
                        }
                        oOOForm.doShowModal(oForm);
                        oOOForm.DBWOQ.SBOForm = oOOForm.SBOForm;
                    }
                }
            }
            return base.doItemEvent(oForm, ref pVal, ref BubbleEvent);
        }

        public bool doHandleEnquiryOKReturn(com.idh.forms.oo.Form oDialogForm) {
            com.isb.forms.Enquiry.Enquiry oOOForm = (com.isb.forms.Enquiry.Enquiry)oDialogForm;
            if (!oOOForm.bLoadEnquiry && oOOForm.bAddUpdateOk) {
                SAPbouiCOM.Form oForm = oOOForm.SBOParentForm;
                setItemValue(oForm, "IDH_EnqID", oOOForm.EnquiryID);

                setWFValue(oForm, "SETENQIDs", "Y");
                setWFValue(oForm, "CardCode", "");
                setWFValue(oForm, "Personal", "");
                setWFValue(oForm, "SelectEnquiry", "");
                setWFValue(oForm, "Remarks", oOOForm.DBEnquiry.U_Notes);
                if (oOOForm.DBEnquiry.U_CardCode == "")
                    setWFValue(oForm, "Personal", "Y");
                else
                    setWFValue(oForm, "CardCode", oOOForm.DBEnquiry.U_CardCode);

                setWFValue(oForm, "SelectEnquiry", "Y");
                try {
                    oForm.Items.Item("9").Click();
                } catch (Exception ex) {
                }
            } else if (oOOForm.bLoadEnquiry && oOOForm.bAddUpdateOk) {
                SAPbouiCOM.Form oForm = oOOForm.SBOParentForm;
                setWFValue(oForm, "SETENQIDs", "");
                setWFValue(oForm, "CardCode", "");
                setWFValue(oForm, "Personal", "");
                setWFValue(oForm, "SelectEnquiry", "");
                setWFValue(oForm, "Remarks", oOOForm.DBEnquiry.U_Notes);

                //string sCardCode = getItemValue(oForm, "9");
                setWFValue(oForm, "SETENQIDs", "Y");
                setWFValue(oForm, "CardCode", oOOForm.DBEnquiry.U_CardCode);
                setWFValue(oForm, "Personal", "N");
                try {
                    oForm.Items.Item("9").Click();
                } catch (Exception ex) { }
            }

            return true;
        }

        public bool doHandleWOQOKReturn(com.idh.forms.oo.Form oDialogForm) {
            com.isb.forms.Enquiry.WOQuote oOOForm = (com.isb.forms.Enquiry.WOQuote)oDialogForm;
            if ( !oOOForm.bLoadWOQ && oOOForm.bAddUpdateOk ) {
                SAPbouiCOM.Form oForm = oOOForm.SBOParentForm;
                setItemValue(oForm, "IDH_WOQID", oOOForm.EnquiryID);

                setWFValue(oForm, "SETENQIDs", "Y");
                setWFValue(oForm, "CardCode", "");
                setWFValue(oForm, "Personal", "");
                setWFValue(oForm, "SelectEnquiry", "");
                setWFValue(oForm, "Remarks", oOOForm.DBWOQ.U_Notes);
                if (oOOForm.DBWOQ.U_CardCd == "")
                    setWFValue(oForm, "Personal", "Y");
                else
                    setWFValue(oForm, "CardCode", oOOForm.DBWOQ.U_CardCd);

                setWFValue(oForm, "SelectEnquiry", "Y");
                try {
                    oForm.Items.Item("9").Click();
                } catch (Exception ex) { }
            } else if (oOOForm.bLoadWOQ && oOOForm.bAddUpdateOk) {
                SAPbouiCOM.Form oForm = oOOForm.SBOParentForm;
                setWFValue(oForm, "SETENQIDs", "");
                setWFValue(oForm, "CardCode", "");
                setWFValue(oForm, "Personal", "");
                setWFValue(oForm, "SelectEnquiry", "");
                setWFValue(oForm, "Remarks", oOOForm.DBWOQ.U_Notes);

                setWFValue(oForm, "SETENQIDs", "Y");
                setWFValue(oForm, "CardCode", oOOForm.DBWOQ.U_CardCd);
                setWFValue(oForm, "Personal", "N");
                try {
                    oForm.Items.Item("9").Click();
                } catch (Exception ex) {
                }
            }
            return true;
        }
    }
}
