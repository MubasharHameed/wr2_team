﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using IDHAddOns.idh.controls;
using com.idh.bridge.data;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry.Search {
    //Inherits 
    //msKeyGen="WGPVALDS"
    public class SearchEnquiry : IDHAddOns.idh.forms.Base{
        //protected bool  = false;
        /*
        public SearchEnquiry(IDHAddOns.idh.addon.Base oParent)
            : base(oParent, "IDHSRENQ", "Search Enquiry.srf", 5, 27, 636, 305, "Select Enquiry(s)") {//, true, false, "Select Enquiry", load_Types.idh_LOAD_NORMAL) {
            // : base(oParent, "IDHSRENQ", "Search Enquiry.srf", 5, 27, 636, 305, "Select Enquiry(s)") {//, true, false, "Select Enquiry", load_Types.idh_LOAD_NORMAL) {
        }
        */

        
        public SearchEnquiry(IDHAddOns.idh.addon.Base oParent)
            : base(oParent, "IDHSRENQ", 0, "Enq_Search Enquiry.srf", true) {//, true, false, "Select Enquiry", load_Types.idh_LOAD_NORMAL) {
            // : base(oParent, "IDHSRENQ", "Search Enquiry.srf", 5, 27, 636, 305, "Select Enquiry(s)") {//, true, false, "Select Enquiry", load_Types.idh_LOAD_NORMAL) {
        }
        
        protected override void doSetEventFilters() {
         //   doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);

        }
        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent)
		{
			try {
				//base.doCompleteCreate(ref oForm,ref BubbleEvent);
                //return;
                oForm.Title = "Select Enquiry";

				SAPbouiCOM.Item oItem = default(SAPbouiCOM.Item);
				oItem = oForm.Items.Item("IDH_CONTA");
				FilterGrid oGridN = new FilterGrid(this, oForm, "LINESGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane,true);
                oGridN.getSBOItem().AffectsFormMode = false;
				oForm.Items.Item("IDH_BPNAME").AffectsFormMode = false;
                doAddUF(oForm, "IDH_BPNAME", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100, false, false);
                //doAddUF(oForm, "IDH_BPCODE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100, false, false);
                oItem = oForm.Items.Item("1");
                SAPbouiCOM.Button oButn = (SAPbouiCOM.Button)oItem.Specific;
                oButn.Caption = getTranslatedWord("Choose");
            } catch (Exception ex) {
				com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null );
				BubbleEvent = false;
			}
		}

        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCloseForm(oForm,ref BubbleEvent);
            FilterGrid.doRemoveGrid(oForm, "LINESGRID");
        }

        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {
            doReadInputParams(oForm);
                FilterGrid oGridN = (FilterGrid)FilterGrid.getInstance(oForm, "LINESGRID");
                if (oGridN == null) {
                    oGridN = new FilterGrid(this, oForm, "LINESGRID",true);
                }
                doSetFilterFields(oGridN);
                //doSetListFields(oGridN);
                doSetGridOptions(oGridN);


            
            oGridN.AddEditLine = false;
                oGridN.doSetDeleteActive(false);
                oGridN.doSetDoCount(true);
                //oGridN.doSetHistory(getUserTable(), "Code");
                oForm.Items.Item("LINESGRID").AffectsFormMode = false;
                oForm.Items.Item("1").AffectsFormMode = false;    
            
        }


        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {

                if (this.goParent.doCheckModal(oForm.UniqueID)) {
                    System.Collections.ArrayList aSelectItems = new ArrayList();
                    FilterGrid oGridN = (FilterGrid)FilterGrid.getInstance(oForm, "LINESGRID");
                    for (Int16 iRow = 0; iRow <= oGridN.getRowCount() - 1; iRow++) {
                        if (oGridN.doGetFieldValue("T1.Code", iRow).ToString() != string.Empty && oGridN.doGetFieldValue("[Select]", iRow).ToString() == "Y")//T1.U_EnqID, T1.Code
                            aSelectItems.Add(oGridN.doGetFieldValue("T1.U_EnqID", iRow).ToString() + ":" + oGridN.doGetFieldValue("T1.Code", iRow).ToString());
                    }
                    setParentSharedData(oForm, "IDH_SELENQIDS", aSelectItems);
                    base.doReturnFromModalShared(oForm, true);
                    BubbleEvent = false;
                }
            }
        }
        protected override void doReturnNormal(SAPbouiCOM.Form oForm) {
            base.doReturnNormal(oForm);
        }
        public override void doFinalizeShow(SAPbouiCOM.Form oForm) {
            setItemValue(oForm, "IDH_BPNAME", Common.getStringValue(getParentSharedData(oForm, "IDH_BPNAME")).ToString());
            base.doFinalizeShow(oForm);
            
            if (Common.getStringValue(getParentSharedData(oForm, "IDH_BPNAME")).ToString().Trim() != string.Empty) {
                doSetFocus(oForm, "IDH_PRKITM");
                setEnableItem(oForm, false, "IDH_BPNAME"); }
           
            oForm.SupportedModes = 1;// SAPbouiCOM.BoFormMode.fm_OK_MODE;
            oForm.Items.Item("LINESGRID").AffectsFormMode = false;
            FilterGrid oGridN = FilterGrid.getInstance(oForm, "LINESGRID");
            for(int i=0;i<=oGridN.Columns.Count - 1;i++){
                if(oGridN.Columns.Item(i).AffectsFormMode )
                    oGridN.Columns.Item(i).AffectsFormMode = false;
                
            }
           SAPbouiCOM.Item oItem = oForm.Items.Item("1");
            SAPbouiCOM.Button oButn = (SAPbouiCOM.Button)oItem.Specific;
            oButn.Caption = getTranslatedWord("Choose");
            //oGridN.doEnableColumn("[Select]", true);
        }

        protected void doSetFilterFields(IDHAddOns.idh.controls.FilterGrid oGridN) {
            oGridN.doAddFilterField("IDH_BPNAME", "T0." + IDH_ENQUIRY._CardName, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 100);
        }

        public  void  doSetGridOptions(IDHAddOns.idh.controls.FilterGrid oGridN) {

            //oGridN.doAddGridTable(new GridTable(com.idh.dbObjects.User.IDH_ENQUIRY.TableName, "T0", "Code", false), true);
            oGridN.doAddGridTable(new GridTable(IDH_ENQITEM.TableName, "T1", IDH_ENQITEM._Code, false), true);
            //oGridN.setRequiredFilter("T0.Code=T1.U_EnqID And T0.U_Status in('1','2') And T1.U_Status ='6' ");

            oGridN.doAddListField("[Select]", "Select", true , -1, "CHECKBOX", null);            
            oGridN.doAddListField("T1.U_EnqID", "Enquiry", false, -1, null, null);
            oGridN.doAddListField("T1.Code", "Line ID", false, 0, null, null);
            oGridN.doAddListField("T0." + IDH_ENQUIRY._CardName, "Business Partner", false, -1, null, null);

            oGridN.doAddListField("T1." + IDH_ENQITEM._WstGpNm, "Waste Group", false, -1, null, null);
            oGridN.doAddListField("T1." + IDH_ENQITEM._ItemCode, "Item Code", false, -1, null, null);
            //oGridN.doAddListField("T1." + IDH_ENQITEM._ItemCode, "Item Code", false, -1, null, null);
            oGridN.doAddListField("T1." + IDH_ENQITEM._ItemName, "Item Name", false, -1, null, null);
            oGridN.doAddListField("T1." + IDH_ENQITEM._EstQty, "Est. Qty", false, -1, null, null);            
           
            oGridN.doApplyRules();
        }
        //protected override void doReadInputParams(SAPbouiCOM.Form oForm) {
        //    //if (getParentSharedData(oForm, "IDH_WSTGRP") != null && getParentSharedData(oForm, "IDH_WSTGRP").ToString() != string.Empty) {
        //    //    setUFValue(oForm, "IDH_BPNAME", getParentSharedData(oForm, "IDH_WSTGRP"));
        //    //}
        //}
        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            string sBPName = "";
            sBPName = Common.getStringValue(getParentSharedData(oForm, "IDH_BPNAME")).ToString();//"973"; //

            doReLoadData(oForm, sBPName);
        }

        protected void doReLoadData(SAPbouiCOM.Form oForm,string sBPName) {
            base.doLoadData(oForm);
            
            sBPName=sBPName.Replace("'", "").Replace("--", "").Replace("*","%");
            FilterGrid oGridN;
            string sSql = null;
            oGridN = (FilterGrid)FilterGrid.getInstance(oForm, "LINESGRID");
            sSql = "Select ";
            if (getParentSharedData(oForm, "IDH_SOURCE").ToString().Trim() == "ENQ") {
            sSql+=" cast('Y' as nvarchar(2)) As [Select], ";
                }
            else
                sSql += " cast('' as nvarchar(2)) As [Select], ";
            sSql += " T1.U_EnqID, T1.Code,T0." + IDH_ENQUIRY._CardName + ",T1." 
                + IDH_ENQITEM._WstGpNm +
                ",T1." + IDH_ENQITEM._ItemCode + ",T1." + IDH_ENQITEM._ItemName +
                ",T1." + IDH_ENQITEM._EstQty + " From [" + IDH_ENQUIRY.TableName + "] T0 " +
                ",[" + IDH_ENQITEM.TableName + "] T1 where T0.Code=T1.U_EnqID And T0.U_Status in('1','2') And T1.U_Status ='1' " +
                " And T0.U_CardName Like '" + sBPName + "%' And Isnull(T1.U_PCode,'')='' ";
            if (getParentSharedData(oForm, "IDH_SOURCE").ToString().Trim() == "ENQ") {
                string sENQID = getParentSharedData(oForm, "IDH_ENQNUM").ToString().Trim();
                sSql += " And T0.Code='"+ sENQID +"' ";
            }
            string sVal = (string)getParentSharedData(oForm, "IDH_EXISTINGENQLINES");
            if ( !string.IsNullOrWhiteSpace(sVal) ) {
                string sExludeList = getParentSharedData(oForm, "IDH_EXISTINGENQLINES").ToString().Trim();
               // sExludList = sExludList.TrimEnd(',');
                sSql += " AND Not T1.Code in(" + sExludeList + ")"; 
            }
          //  +"T1.Code,T0.Name,T0.U_WstGpCd,T0.U_ItemCd,T1.ItemName,T0.U_Sort,'N' As U_Selectd from [@IDH_ADITVLD] T0 , OITM T1 where T0.U_ItemCd=T1.ItemCode And T0.U_WstGpCd='" + sBPName.Trim() + "' Order by U_Sort";
            
            //oGridN.doReloadData("",false);
            //return;
        //    sSql = "Select T1.Code,T0.Name,T0.U_WstGpCd,T0.U_ItemCd,T1.ItemName,T0.U_Sort,'N' As U_Selectd from [@IDH_ADITVLD] T0 , OITM T1 where T0.U_ItemCd=T1.ItemCode And T0.U_WstGpCd='" + sBPName.Trim() + "' Order by U_Sort";
            oGridN.getSBOGrid().DataTable.ExecuteQuery(sSql);
            oGridN.doApplyRules();
            if (oGridN.getRowCount() > 1 && oGridN.doGetFieldValue("T1.Code", oGridN.getRowCount() - 1).ToString() == "") {
                oGridN.doRemoveRow(oGridN.getRowCount() - 1);
            }        
        }
        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            //doWarnMess(pVal.EventType.ToString());
            if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED && pVal.BeforeAction == false ) { 
                FilterGrid oGridN = (FilterGrid)FilterGrid.getInstance(oForm, "LINESGRID");
                if (pVal.ColUID == "[Select]" && oGridN.doGetFieldValue("[Select]",pVal.Row).ToString()=="Y"){
                    string sBPName="";
                    for (Int16 iRow = 0; iRow <= oGridN.getRowCount() - 1; iRow++) {
                        if (oGridN.doGetFieldValue("[Select]", iRow).ToString() == "Y")//T1.U_EnqID, T1.Code
                            if (sBPName == string.Empty) {
                                sBPName = oGridN.doGetFieldValue("T0." + IDH_ENQUIRY._CardName, iRow).ToString();
                            } else if (sBPName != oGridN.doGetFieldValue("T0." + IDH_ENQUIRY._CardName, iRow).ToString()) {
                                com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: you must select rows from same BP.", "ERVEQ004", null);
                                oGridN.doSetFieldValue("[Select]", pVal.Row, "");
                                break;
                            }
                            //aSelectItems.Add(oGridN.doGetFieldValue("T1.U_EnqID", iRow).ToString() + ":" + oGridN.doGetFieldValue("T1.Code", iRow).ToString());
                    }
                }
            
            }
           //     ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = getTranslatedWord("Choose");
           // }
            //if (!pVal.BeforeAction  && pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SEARCH_VALUESET) {
            //    IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");
            //    oGridN.doAddDataLine(true);
            //}else if (pVal.BeforeAction == false && pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DATA_KEY_EMPTY) {
            //    IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");
            //    oGridN.doSetFieldValue("Name", pVal.Row, "1", true);
                
            //    oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
            //} else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED && (getUFValue(oForm, "IDH_SRMOD").ToString() == "Y")) {
            //    oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
            //}
            //if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED && (getUFValue(oForm, "IDH_SRMOD").ToString() == "Y")) {
            //   oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
            //}
            return base.doCustomItemEvent(oForm,ref pVal);

        }

        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            
             if (pVal.ItemUID == "LINESGRID" && pVal.BeforeAction && pVal.EventType==SAPbouiCOM.BoEventTypes.et_CLICK && oForm.Mode==SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
               //  oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
                 ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = getTranslatedWord("Choose");
            } else if (pVal.ItemUID == "1"  && pVal.EventType == SAPbouiCOM.BoEventTypes.et_CLICK && pVal.BeforeAction == false) {

                    if (this.goParent.doCheckModal(oForm.UniqueID)) {
                        System.Collections.ArrayList aSelectItems = new ArrayList();
                        FilterGrid oGridN = (FilterGrid)FilterGrid.getInstance(oForm, "LINESGRID");
                        for (Int16 iRow = 0; iRow <= oGridN.getRowCount() - 1; iRow++) {
                            if (oGridN.doGetFieldValue("T1.Code", iRow).ToString() != string.Empty && oGridN.doGetFieldValue("[Select]", iRow).ToString() == "Y")//T1.U_EnqID, T1.Code
                                aSelectItems.Add(oGridN.doGetFieldValue("T1.U_EnqID", iRow).ToString() + ":" + oGridN.doGetFieldValue("T1.Code", iRow).ToString());
                        }
                        setParentSharedData(oForm, "IDH_SELENQIDS", aSelectItems);
                        base.doReturnFromModalShared(oForm, true);
                        BubbleEvent = false;
                    }                
                return true;
            }
            base.doItemEvent(oForm, ref pVal, ref BubbleEvent);

            if ((pVal.ItemUID == "IDH_BPNAME") && pVal.EventType == SAPbouiCOM.BoEventTypes.et_LOST_FOCUS && pVal.BeforeAction == false) {
              doReLoadData(oForm,getUFValue(oForm,"IDH_BPNAME").ToString().Trim());
            }
            return true;
        }

        public override void doClose() {
        }
    }
}