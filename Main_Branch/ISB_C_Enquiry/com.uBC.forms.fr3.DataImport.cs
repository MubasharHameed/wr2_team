using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using com.idh.utils;
using com.idh.dbObjects.User;
using com.idh.bridge.lookups;

using com.idh.bridge;
using com.idh.dbObjects.numbers;

namespace com.isb.forms.Enquiry
{
    public class DataImport
    {
        //private bool mbUseDBObjects = false;
        //private bool mbIsUDT = false;

        public DataImport()
        {
        }

        public bool doReadFile(string sExcelFile, string sNewLabResultCode)
        {
            ExcelReader oExcel = new ExcelReader(sExcelFile, 1, Config.INSTANCE.getParameterAsBool("IMPRFC97", false));
            bool bResult = true;

            if (!oExcel.doReadFile())
            {
                //DataHandler.INSTANCE.doError(oExcel.LastError, "Error importing the file.");
                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error importing the file. Excel Error: " + oExcel.LastError, "ERSYIMPF", new String[] { idh.bridge.Translation.getTranslatedWord(oExcel.LastError) });
                return false;
            }

            long lRows = oExcel.Rows;
            long lCols = oExcel.Cols;

            /*
             * File Layout [1:1] = TableName
             *             [1:2] = (Y/N) indicate that the DB Objects must be used for the insert/update
             *             [1:3] = KEYNAME - the field in the numbers table to use for the Unique number only if it needs to be auto genrated ... Look at comments below
             *             [1:4] = KEYPrefix - the prefix to use for the number
             *             [2:1,2,3,4........] Field Names as in the table
             *             [3:1,2,3,4........] The Filed descriptions - For information purposes only
             *             [4,5,6........:1,2,3,4........] The Data
             *             If the table is a User defined table and the Code and name field are not set or are not declared the keys will be generated automatically
             */
            if (lRows > 5 && lCols > 1)
            {
                string sTableName = oExcel.getValue(1, 1);
                if (sTableName == null || sTableName.Length < 1)
                {
                    //DataHandler.INSTANCE.doError("User: The Import file format is incorrect [" + sExcelFile + "]");
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("The Import file format is incorrect [" + sExcelFile + "]", "ERUSINIF", new String[] { sExcelFile });
                }
                else
                {
                    //bool bDoAutoKey = false;
                    bool bAutoCode = false;
                    bool bAutoName = false;
                    string sFieldList = "";
                    string sFieldName = "";
                    string sKeyField = "";
                    string sKeyPrefixField = null;

                    if (sTableName.Length > 1 && sTableName[0] == '@')
                    {
                        //mbIsUDT = true;
                        bAutoCode = true;
                        bAutoName = true;
                    }

                    //Compile the Field List
                    for (int c = 1; c < lCols; c++)
                    {
                        sFieldName = oExcel.getValue(2, c).ToUpper();
                        if (sFieldName.Equals("CODE"))
                            bAutoCode = false;
                        else if (sFieldName.Equals("NAME"))
                            bAutoName = false;

                        sFieldList += (sFieldList.Length > 0 ? "," : "") + sFieldName;
                    }

                    if (bAutoName)
                        sFieldList = "Name" + (sFieldList.Length > 0 ? "," : "") + sFieldList;

                    if (bAutoCode)
                        sFieldList = "Code" + (sFieldList.Length > 0 ? "," : "") + sFieldList;

                    if (bAutoCode || bAutoName)
                    {
                        sKeyField = oExcel.getValue(1, 3);
                        if (sKeyField == null || sKeyField.Length == 0)
                        {
                            sKeyField = "GENSEQ";
                        }

                        sKeyPrefixField = oExcel.getValue(1, 4);
                    }

                    //Incorporate Lab Task Code in the Field List 
                    if (sNewLabResultCode.Length > 0)
                        //sFieldList = "U_LTICd" + (sFieldList.Length > 0 ? "," : "") + sFieldList;
                        sFieldList = sFieldList + ", U_LTICd";

                    string sSQLPRE = "INSERT INTO [" + sTableName + "] (" + sFieldList + ") VALUES (";

                    string sSQL = "";
                    int iRowsInserted = 0;
                    int iDataLength;
                    string sFieldValues = "";
                    string sFieldValue;
                    //Compile the Values

                    //Pre Book the Seq numbers
                    int iKeyValue = 0;
                    NumbersPair oNumbers = null;
                    if (bAutoCode || bAutoName)
                    {
                        oNumbers = DataHandler.INSTANCE.doGenerateCode(sKeyField, (int)lRows);
                        iKeyValue = oNumbers.CodeNumber;
                    }

                    int iProgressDiv = 10;
                    if (lRows > 1000)
                        iProgressDiv = 100;
                    else if (lRows > 10)
                        iProgressDiv = 10;
                    else
                        iProgressDiv = 2;

                    com.idh.bridge.DataHandler.INSTANCE.doProgress("IMP", 1, (int)lRows);
                    int iProgressInc = (int)(lRows / iProgressDiv);
                    int iProgress = iProgressInc;

                    for (int r = 4; r < lRows; r++)
                    {
                        if (r == iProgress)
                        {
                            com.idh.bridge.DataHandler.INSTANCE.doProgress("IMP", iProgress, (int)lRows);
                            iProgress += iProgressInc;

                            if (sSQL.Length > 0)
                            {
                                com.idh.bridge.DataHandler.INSTANCE.doUpdateQuery(sSQL);
                                sSQL = "";
                            }
                        }
                        sFieldValues = "";

                        iDataLength = 0;
                        for (int c = 1; c < lCols; c++)
                        {
                            sFieldValue = oExcel.getValue(r, c);
                            iDataLength += sFieldValue.Length;
                            sFieldValues += (sFieldValues.Length > 0 ? "," : "") + "'" + sFieldValue + "'";
                        }

                        //Append LabTask Code here
                        if (sNewLabResultCode.Length > 0)
                            sFieldValues = sFieldValues + (",'" + sNewLabResultCode + "'");

                        if (iDataLength > 1)
                        {
                            if (bAutoCode || bAutoName)
                            {
                                //int iKeyValue = DataHandler.INSTANCE.doNextNumber(sKeyField);
                                //string sCode = DataHandler.doPackCode(sKeyPrefixField, iKeyValue);
                                if (bAutoCode)
                                {
                                    //sCode = oNumbers.doPackCodeCode(iRowsInserted);
                                    sFieldValues = "'" + oNumbers.doPackCodeCode(iRowsInserted) + "'" + (sFieldList.Length > 0 ? "," : "") + sFieldValues;
                                }
                                if (bAutoName)
                                {
                                    sFieldValues = "'" + (oNumbers.NameNumber + iRowsInserted) + "'" + (sFieldList.Length > 0 ? "," : "") + sFieldValues;
                                }
                                iKeyValue++;
                            }

                            sSQL += (sSQL.Length > 0 ? ";\r " : "") + sSQLPRE + sFieldValues + ')';

                            iRowsInserted++;
                        }
                        else
                        {

                        }
                    }
                    if (sSQL.Length > 0)
                        com.idh.bridge.DataHandler.INSTANCE.doUpdateQuery(sSQL);

                    com.idh.bridge.DataHandler.INSTANCE.doProgressDone("IMP");

                    com.idh.bridge.DataHandler.INSTANCE.doInfo("User: The file [" + sExcelFile + "] imported (" + iRowsInserted + " ) rows");
                }
            }
            return bResult;
        }
    }
}
