﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;

using IDHAddOns.idh.controls;
//using com.idh.bridge.reports;
//using com.idh.utils.Conversions;
using com.idh.bridge;
using com.idh.bridge.data;
//using WR1_Grids.idh.controls.grid;
using com.idh.bridge.lookups;
using com.idh.dbObjects.User;
using com.isb.enq.dbObjects.User;
using com.idh.bridge.action;
using com.idh.controls;
using com.idh.controls.strct;
using System.Windows.Forms;
using System.Threading;
using com.idh.utils;
using System.Text;
using com.idh.bridge.resources;
using SAPbouiCOM;
using System.Linq;
using System.Text.RegularExpressions;

using com.isb.enq;

namespace com.isb.forms.Enquiry.PJDORA {
    class PBIMassUpdater :
        IDHAddOns.idh.forms.Base {

        public PBIMassUpdater(IDHAddOns.idh.addon.Base oParent, string sParent, int iMenuPosition)
            : base(oParent, "IDH_IMPALLLIVSRVS", sParent, iMenuPosition, "ImportAllLiveServices_Manager.srf", true, true, false, "PBI Mass Updater", load_Types.idh_LOAD_NORMAL) {
        }

        protected void doTheGridLayout(DBOGrid oGridN) {
            if (com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", false)) {
                string sFormTypeId = oGridN.getSBOForm().TypeEx;
                IDH_FORMSET oFormSettings = (IDH_FORMSET)getWFValue("FRMSET", sFormTypeId + "." + oGridN.GridId);
                if (oFormSettings == null) {
                    oFormSettings = new IDH_FORMSET();
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, "");
                    setWFValue("FRMSET", sFormTypeId + "." + oGridN.GridId, oFormSettings);
                    oFormSettings.doAddFieldToGrid(oGridN);
                    doSetListFields(oGridN);
                    oFormSettings.doSyncDB(oGridN);
                } else {
                    if (oFormSettings.SkipFormSettings) {
                        doSetListFields(oGridN);
                    } else {
                        oFormSettings.doAddFieldToGrid(oGridN);
                    }
                }
            } else {
                doSetListFields(oGridN);
            }
        }

        protected void doSetListFields(DBOGrid moGrid) {

            moGrid.doAddListField(IDH_LVSBTH._Code, "Code", false, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._BatchNum, "Batch", false, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._WORow, "WORow / PO Nunber", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._PBI, "PBI", false, 20, null, null);
            moGrid.doAddListField(IDH_LVSBTH._AmndTp, "Amendment Type", true, -1, ListFields.LISTTYPE_COMBOBOX, null);

            moGrid.doAddListField(IDH_LVSBTH._CustCd, "Customer code", true, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            moGrid.doAddListField(IDH_LVSBTH._CustNm, "Customer Name", true, -1, null, null);

            moGrid.doAddListField(IDH_LVSBTH._CustAdr, "Customer Site Address", true, -1, null, null);

            moGrid.doAddListField(IDH_LVSBTH._CusPCode, "Post Code", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._JbTypeDs, "Order Type", true, -1, null, null);
            //moGrid.doAddListField(IDH_LVSBTH._JbTypeCd, "Order Type Cd", true, -1, null, null);

            moGrid.doAddListField(IDH_LVSBTH._SuppCd, "Supplier Code", true, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            moGrid.doAddListField(IDH_LVSBTH._SuppNm, "Supplier Name", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._SuppRef, "Supplier Reference", true, -1, null, null);

            moGrid.doAddListField(IDH_LVSBTH._SupAddr, "Supplier Address", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._SupAdrCd, "Supplier Address Code", true, 0, null, null);
            moGrid.doAddListField(IDH_LVSBTH._SupPCode, "Supplier Postcode", true, 1, null, null);

            moGrid.doAddListField(IDH_LVSBTH._ItemCd, "Container Code", true, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            moGrid.doAddListField(IDH_LVSBTH._ItemNm, "Container Desc", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._WastCd, "Waste Code", true, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            moGrid.doAddListField(IDH_LVSBTH._WasteNm, "Waste Material", true, -1, null, null);

            moGrid.doAddListField(IDH_LVSBTH._UOM, "Unit of Measure (UOM)", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._Qty, "Qty", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._FrqType, "Frequency Type", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._FrqCount, "Frequency Count", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._AutoComp, "Auto Complete", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._Rebate, "Rebate", true, -1, ListFields.LISTTYPE_CHECKBOX, null);

            moGrid.doAddListField(IDH_LVSBTH._LPW, "LPW(Lifts Per Week)", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._Mon, "Mon", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._Tue, "Tue", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._Wed, "Wed", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._Thu, "Thu", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._Fri, "Fri", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._Sat, "Sat", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._Sun, "Sun", true, -1, null, null);


            moGrid.doAddListField(IDH_LVSBTH._CMnSVst, "CIP-Min Site Visit", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._CMnChg, "CIP-Min Charge", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._CVarRFix, "CIP-Variable Or Fixed QTY", true, -1, null, null);

            moGrid.doAddListField(IDH_LVSBTH._CMinChg, "CIP.Minimum Charge", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._CMCgVol, "CIP.Minimum Charge Upto Volume", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._CRminVol, "CIP.Rate above min volume", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._CHaulage, "CIP.Haulage / Service / Rental", true, -1, null, null);


            moGrid.doAddListField(IDH_LVSBTH._SMinChg, "SIP.Minimum Charge", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._SMCgVol, "SIP.Minimum Charge Upto Volume", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._SRminVol, "SIP.Rate above min Volume", true, -1, null, null);

            moGrid.doAddListField(IDH_LVSBTH._SHaulage, "SIP.Haulage / Service / Rental", true, -1, null, null);

            moGrid.doAddListField(IDH_LVSBTH._SMnSVst, "SIP-Min Site Visit", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._SMnChg, "SIP-Min Charge", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._SVarRFix, "SIP-Variable Or Fixed QTY", true, -1, null, null);


            moGrid.doAddListField(IDH_LVSBTH._EffDate, "Effective Date", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._WTNFrom, "WTN Date From", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._WTNTo, "WTN Date To", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._Comments, "Comments", true, -1, null, null);

            moGrid.doAddListField(IDH_LVSBTH._ProcSts, "Process Status", false, -1, ListFields.LISTTYPE_COMBOBOX, null);
            moGrid.doAddListField(IDH_LVSBTH._VldStatus, "Validation Status", false, -1, ListFields.LISTTYPE_COMBOBOX, null);
            moGrid.doAddListField(IDH_LVSBTH._BthStatus, "Batch Status", false, -1, ListFields.LISTTYPE_COMBOBOX, null);

            moGrid.doAddListField(IDH_LVSBTH._IDHRT, "Routable Checkbox", true, -1, ListFields.LISTTYPE_COMBOBOX, null);
            moGrid.doAddListField(IDH_LVSBTH._IDHRCDM, "Route Code Monday", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._IDHRCDTU, "Route Code Tuesday", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._IDHRCDW, "Route Code Wednesday", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._IDHRCDTH, "Route Code Thursday", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._IDHRCDF, "Route Code Friday", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._IDHRCDSA, "Route Code Saturday", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._IDHRCDSU, "Route Code Sunday", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._IDHRSQM, "Route Sequence Monday", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._IDHRSQTU, "Route Sequence Tuesday", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._IDHRSQW, "Route Sequence Wednesday", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._IDHRSQTH, "Route Sequence Thursday", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._IDHRSQF, "Route Sequence Friday", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._IDHRSQSA, "Route Sequence Saturday", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._IDHRSQSU, "Route Sequence Sunday", true, -1, null, null);

            moGrid.doAddListField(IDH_LVSBTH._IDHCMT, "Route Comments", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._IDHDRVI, "Driver Comments", true, -1, null, null);
            moGrid.doAddListField(IDH_LVSBTH._AUTSTRT, "Auto Start Date", true, -1, null, null);

            moGrid.doAddListField(IDH_LVSBTH._PERTYP, "Period type", true, -1, ListFields.LISTTYPE_COMBOBOX, null);
            moGrid.doAddListField(IDH_LVSBTH._IDHRECFQ, "Recurrence Frequency", true, -1, null, null);


            moGrid.doAddListField(IDH_LVSBTH._AddedBy, "Added By", true, 0, null, null);

        }

        protected void doSetGridFilters(DBOGrid moGridN) {
            doAddUF(moGridN.getSBOForm(), "IDH_ACTNAP", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 250).AffectsFormMode = false;

            moGridN.doAddFilterField("WORCode", IDH_LVSBTH._WORow, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_AMDTYP", IDH_LVSBTH._AmndTp, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_CUST", IDH_LVSBTH._CustCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_NAME", IDH_LVSBTH._CustNm, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_CADDR", IDH_LVSBTH._CustAdr, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_CPCODE", IDH_LVSBTH._CusPCode, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_SUPCD", IDH_LVSBTH._SuppCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_SUPNM", IDH_LVSBTH._SuppNm, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_PBINUM", IDH_LVSBTH._PBI, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_JOBTYP", IDH_LVSBTH._JbTypeDs, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_ITEMCD", IDH_LVSBTH._ItemCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_ITEMNM", IDH_LVSBTH._ItemNm, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WASTCD", IDH_LVSBTH._WastCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WASTNM", IDH_LVSBTH._WasteNm, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_EFCDTF", IDH_LVSBTH._EffDate, SAPbouiCOM.BoDataType.dt_DATE, ">=", 10);
            moGridN.doAddFilterField("IDH_EFCDTT", IDH_LVSBTH._EffDate, SAPbouiCOM.BoDataType.dt_DATE, "<=", 10);

            moGridN.doAddFilterField("IDH_BATCHF", IDH_LVSBTH._BatchNum, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 250);

            //Process Status
            moGridN.doAddFilterField("IDH_BTPRST", IDH_LVSBTH._ProcSts, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 1);

            //IDH_VLDSTS Validation Status
            moGridN.doAddFilterField("IDH_VLDSTS", IDH_LVSBTH._VldStatus, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 2);

            ////IDH_VLDSTS Validation Status
            //moGridN.doAddFilterField("IDH_VLDSTS ", IDH_LVSBTH._VldStatus, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 1);

            //IDH_BTCSTS Batch Status Active/Deleted
            moGridN.doAddFilterField("IDH_BTCSTS", IDH_LVSBTH._BthStatus, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 1);

        }
        //*** Add event filters to avoid receiving all events from SBO
        protected override void doSetEventFilters() {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE);
        }

        protected override void doReadInputParams(SAPbouiCOM.Form oForm) {

        }

        //** Create the form
        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            try {
                oForm.Title = gsTitle;
                int iWidth = oForm.Width;

                oForm.AutoManaged = false;

                oForm.EnableMenu(Config.NAV_ADD, false);
                oForm.EnableMenu(Config.NAV_FIND, false);
                oForm.EnableMenu(Config.NAV_FIRST, false);
                oForm.EnableMenu(Config.NAV_NEXT, false);
                oForm.EnableMenu(Config.NAV_PREV, false);
                oForm.EnableMenu(Config.NAV_LAST, false);

                oForm.SupportedModes = Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_All);
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);
                base.doCompleteCreate(ref oForm, ref BubbleEvent);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDLF", null);
                BubbleEvent = false;
            }
        }

        protected void doSetMode(SAPbouiCOM.Form oForm, SAPbouiCOM.BoFormMode iMode) {
            try {
                object sFormType = getParentSharedData(oForm, "FTYPE");
                bool bIsSearch = false;
                if ((sFormType != null) && sFormType.Equals("SEARCH") == true) {
                    bIsSearch = true;
                }

                if (iMode == SAPbouiCOM.BoFormMode.fm_FIND_MODE) {
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = Translation.getTranslatedWord("Find");
                    if (bIsSearch) {
                        oForm.Items.Item("2").Visible = true;
                    } else {
                        oForm.Items.Item("2").Visible = false;
                    }
                } else if (iMode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = Translation.getTranslatedWord("Update");
                    oForm.Items.Item("2").Visible = true;
                }
            } catch (Exception ex) {
            }
        }

        public string getListRequiredStr(SAPbouiCOM.Form OForm) {
            return " 1=1 ";
            //return " r." + IDH_WOQITEM._JobNr + " = h.Code And h." + IDH_WOQHD._User + "=u.USERID ";
        }

        private void doFillGridCombos(UpdateGrid moGrid) {
            SAPbouiCOM.ComboBoxColumn oCombo;
            int iIndex;
            iIndex = moGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            com.isb.enq.utils.FillCombos.doFillAllLiveServicesCombo(ref oCombo, "");

            iIndex = moGrid.doIndexFieldWC(IDH_LVSBTH._ProcSts);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            oCombo.ValidValues.Add("0", Translation.getTranslatedWord("Not Processed"));
            oCombo.ValidValues.Add("1", Translation.getTranslatedWord("Sent For Review"));
            oCombo.ValidValues.Add("2", Translation.getTranslatedWord("Ready for Process"));
            oCombo.ValidValues.Add("3", Translation.getTranslatedWord("Processed"));


            iIndex = moGrid.doIndexFieldWC(IDH_LVSBTH._VldStatus);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            oCombo.ValidValues.Add("", "");
            oCombo.ValidValues.Add("0", Translation.getTranslatedWord("Failed"));
            oCombo.ValidValues.Add("1", Translation.getTranslatedWord("Passed"));


            iIndex = moGrid.doIndexFieldWC(IDH_LVSBTH._BthStatus);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            oCombo.ValidValues.Add("", Translation.getTranslatedWord("All"));
            oCombo.ValidValues.Add("0", Translation.getTranslatedWord("Deleted"));
            oCombo.ValidValues.Add("1", Translation.getTranslatedWord("Active"));


            iIndex = moGrid.doIndexFieldWC(IDH_LVSBTH._PERTYP);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.ValidValues.Add("", "");
            oCombo.ValidValues.Add(IDH_PBI_Handler.PER_Months, IDH_PBI_Handler.PER_Months);
            oCombo.ValidValues.Add(IDH_PBI_Handler.PER_Quarts, IDH_PBI_Handler.PER_Quarts);
            oCombo.ValidValues.Add(IDH_PBI_Handler.PER_Weeks, IDH_PBI_Handler.PER_Weeks);

        }

        private void doFillCombos(SAPbouiCOM.Form oForm) {
            try {
                SAPbouiCOM.ComboBox oCombo;

                oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_JOBTYP").Specific;
                oCombo.Item.DisplayDesc = true;
                doClearValidValues(oCombo.ValidValues);
                string sItemGrp = "";
                com.uBC.utils.FillCombos.doFillJobTypeCombo(ref oCombo, sItemGrp);

                //Amendment
                oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_AMDTYP").Specific;
                oCombo.Item.DisplayDesc = false;
                doClearValidValues(oCombo.ValidValues);

                com.isb.enq.utils.FillCombos.doFillAllLiveServicesCombo(ref oCombo, "");
                //IDH_ACTNAP-- Select action
                oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_ACTNAP").Specific;
                oCombo.Item.DisplayDesc = false;
                doClearValidValues(oCombo.ValidValues);

                com.isb.enq.utils.FillCombos.doFillAllLiveServicesCombo(ref oCombo, "");



                //IDH_BTCSTS Batch Status
                oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_BTCSTS").Specific;
                doClearValidValues(oCombo.ValidValues);
                oCombo.Item.DisplayDesc = true;
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
                oCombo.ValidValues.Add("1", Translation.getTranslatedWord("Active"));
                oCombo.ValidValues.Add("0", Translation.getTranslatedWord("Deleted"));
                oCombo.ValidValues.Add("", Translation.getTranslatedWord("All"));
                //oCombo.ValidValues.Add("3", Translation.getTranslatedWord("Processed"));
                oCombo.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index);

                //IDH_BTPRST Process Status
                oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_BTPRST").Specific;
                doClearValidValues(oCombo.ValidValues);

                oCombo.Item.DisplayDesc = true;
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
                oCombo.ValidValues.Add("", Translation.getTranslatedWord("All"));
                oCombo.ValidValues.Add("0", Translation.getTranslatedWord("Not Processed"));
                oCombo.ValidValues.Add("1", Translation.getTranslatedWord("Sent For Review"));
                oCombo.ValidValues.Add("2", Translation.getTranslatedWord("Ready for Process"));
                oCombo.ValidValues.Add("3", Translation.getTranslatedWord("Processed"));


                //IDH_VLDSTS Validation Status
                oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_VLDSTS").Specific;
                doClearValidValues(oCombo.ValidValues);

                oCombo.Item.DisplayDesc = true;
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
                oCombo.ValidValues.Add("", Translation.getTranslatedWord("All"));
                oCombo.ValidValues.Add("1", Translation.getTranslatedWord("Passed"));
                oCombo.ValidValues.Add("0", Translation.getTranslatedWord("Failed"));
                //  oCombo.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index);
                //oCombo.ValidValues.Add("3", Translation.getTranslatedWord("Processed"));


            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }
        }

        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {


            DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");
            if (oGridN == null) {
                oGridN = (DBOGrid)DBOGrid.getInstance(oForm, "LINESGRID");
                if (oGridN == null) {
                    IDH_LVSBTH oLiveSrvcBatch = new IDH_LVSBTH(this, oForm);
                    oGridN = new DBOGrid(this, oForm, "LINESGRID", 7, 120, oForm.Width - 20, 255, 0, 0, oLiveSrvcBatch);
                    // GridJoin oJoin = new GridJoin("[" + IDH_LVSBHH.TableName + "]", GridJoin.TYPE_JOIN.INNER, "H", IDH_LVSBHH._Code, "H."+IDH_LVSBHH._Code + "=" + IDH_LVSBTH._BatchNum, true);
                    //   oGridN.doAddGridJoin(oJoin);
                    //GridTable oTable = new GridTable("[" + IDH_LVSBHH.TableName + "]", "H", IDH_LVSBHH._Code, false, true);
                    //oGridN.doAddGridTable(oTable);
                    //oGridN.getSBOItem().AffectsFormMode = true;
                }
                setWFValue(oForm, "LINESGRID", oGridN);
            }
            oGridN.doSetDeleteActive(true);
            doSetGridFilters(oGridN);
            //oGridN.setRequiredFilter(IDH_LVSBTH._ProcSts+ "<="+ (int)IDH_LVSBTH.en_BatchProcessStatus.ReadyForProcessByService );
            oGridN.doSetDoCount(true);
            oGridN.doSetBlankLine(false);

            //Set the required List Fields
            doTheGridLayout(oGridN);
            doFillCombos(oForm);

            oGridN.setInitialFilterValue(IDH_LVSBTH._BatchNum + " =\'-999\' ");
            if (DataHandler.INSTANCE.User != "manager") {//&& DataHandler.INSTANCE.User!="manager"
                string sUsersForProcess = Config.INSTANCE.getParameter("ULSPRBAT");
                if (sUsersForProcess == string.Empty) {
                    setVisible(oForm, "IDH_PRCBTC", false);
                    setVisible(oForm, "IDH_PRCLTR", false);
                }

                string[] aUsersForProcess = sUsersForProcess.Split(',');
                bool bFound = false;
                for (int i = 0; i < aUsersForProcess.Length; i++) {
                    if (aUsersForProcess[i].Trim().Equals(DataHandler.INSTANCE.User, StringComparison.OrdinalIgnoreCase)) {
                        setVisible(oForm, "IDH_PRCBTC", true);
                        setVisible(oForm, "IDH_PRCLTR", true);
                        bFound = true;
                        break;
                    }
                }
                if (!bFound) {
                    setVisible(oForm, "IDH_PRCBTC", false);
                    setVisible(oForm, "IDH_PRCLTR", false);
                }

            }
        }

        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            doReLoadData(oForm, !getHasSharedData(oForm));
        }

        //** The Initializer
        protected void doReLoadData(SAPbouiCOM.Form oForm, bool bIsFirst) {
            try {
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);

                DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");//DBOGrid.getInstance(oForm, "LINESGRID");
                string sFilter = oGridN.getRequiredFilter();
                // doSetCustomeFilter(oGridN);
                //#MA  20170301 start Issue=292
                if (!getItemValue(oForm, "IDH_BATCHF").Contains("*") & getItemValue(oForm, "IDH_BATCHF").Length > 0) {
                    sFilter = IDH_WRADHD._BatchNum + "='" + getItemValue(oForm, "IDH_BATCHF") + "'";
                    // setUFValue(oForm, "IDH_BATCHF", "");
                }
                if (sFilter != "" & sFilter != null) {
                    if (oGridN.getRequiredFilter().Length > 0) {
                        oGridN.setRequiredFilter(oGridN.getRequiredFilter() + " AND " + sFilter);
                    } else {
                        oGridN.setRequiredFilter(sFilter);
                    }
                }
                //#MA  20170301 End
                //oGridN.setOrderValue("Cast(" + IDH_WOQITEM._JobNr + " as Int)," + IDH_WOQITEM._Sort + " ");
                oGridN.setOrderValue("Cast(Code as int)");
                oGridN.doReloadData("ASC", true);

                doFillGridCombos(oGridN);
                oGridN.setRequiredFilter(string.Empty);

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBL", null);
            }
        }

        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED) {
                if (pVal.ItemUID == "IDH_IMPEXC") {
                    string sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("WRNUSAVE", null);
                    //int iRow = pVal.Row;
                    //if (IDHAddOns.idh.addon.Base.PARENT.doMessage(sResourceMessage, 2, "Yes", "No", null, true) == 1) {
                    if ((((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find")) || (
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption != Translation.getTranslatedWord("Find") &&
                    (IDHAddOns.idh.addon.Base.PARENT.doMessage(sResourceMessage, 2, "Yes", "No", null, true) == 1))
                    ) {
                        doImportExcel(oForm);
                    }
                } else if (pVal.ItemUID == "IDH_APLYAC") {
                    doApplyNewAmendAction(oForm);
                    //} else if (pVal.ItemUID == "IDH_LDBATC") {
                    //    doLoadSavedBatch(oForm);//
                    //IDH_BTCSTS Active/Del
                    //IDH_BTPRST Process status
                } else if (pVal.ItemUID == "IDH_DELBAT") {
                    // Delete Batch
                    doPreDeleteBatch(oForm);
                } else if (pVal.ItemUID == "IDH_SNDBTH") {
                    if ((((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find"))) {
                        doSendBatchForReview(oForm);
                    } else {
                        string sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("SAVEFORM", null);
                        com.idh.bridge.DataHandler.INSTANCE.doError(sResourceMessage);
                    }

                } else if (pVal.ItemUID == "IDH_PRCBTC") {
                    if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find"))
                        doProcessBatch(oForm, true);
                } else if (pVal.ItemUID == "IDH_PRCLTR") {
                    if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find"))
                        doProcessBatch(oForm, false);
                }
            }
            return base.doItemEvent(oForm, ref pVal, ref BubbleEvent);
        }
        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == true) {
                if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Save") || ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update")) {
                    doSaveBatch(oForm);
                } else if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find")) {
                    doReLoadData(oForm, true);
                }
                BubbleEvent = false;
            }
        }
        public override void doButtonID2(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == true) {
                if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Save") || ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update")) {
                    doReLoadData(oForm, true);
                    BubbleEvent = false;
                }
            }
        }

        protected override void doHandleModalResultShared(SAPbouiCOM.Form oParentForm, string sModalFormType, string sLastButton = null) {
            if (sModalFormType.Equals("IDH_COMMFRM")) {
                DBOGrid oGridN = (DBOGrid)getWFValue(oParentForm, "LINESGRID");
                if (getSharedData(oParentForm, "CMMNT") == null || getSharedData(oParentForm, "CMMNT").ToString().Trim() == string.Empty) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Please provide reason for deletion of batch(s).");
                    return;
                }
                string sComments = getSharedData(oParentForm, "CMMNT").ToString().Trim();
                doDeleteBatch(oParentForm, sComments);
            }
            base.doHandleModalResultShared(oParentForm, sModalFormType, sLastButton);
        }
        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCloseForm(oForm, ref BubbleEvent);
            UpdateGrid.doRemoveGrid(oForm, "LINESGRID");
        }
        public override void doClose() {
        }
        protected void doGridDoubleClick(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
            if (pVal.Row >= 0) {
                try {
                    string sRowCode = oGridN.doGetFieldValue(IDH_WOQITEM._Code).ToString();
                    //string sWOQID = oGridN.doGetFieldValue(IDH_WOQITEM._JobNr).ToString();

                    //ArrayList oData = new ArrayList();
                    //if (sWOQID.Length == 0)
                    //{
                    //    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Valid row must be selected.", "ERUSJOBS", null);
                    //}
                    //else
                    //{
                    //if (oGridN.doCheckIsSameCol(pVal.ColUID,  IDH_WOQITEM._EnqID) || oGridN.doCheckIsSameCol(pVal.ColUID,  IDH_WOQITEM._EnqLID)) {
                    //    //Open Enquiry
                    //    com.isb.forms.Enquiry.Enquiry oOOForm = new com.isb.forms.Enquiry.Enquiry(null, oForm.UniqueID, null);
                    //    oOOForm.EnquiryID = oGridN.doGetFieldValue( IDH_WOQITEM._EnqID).ToString();
                    //    oOOForm.bLoadEnquiry = true;
                    //    oOOForm.bLoadReadOnly = false;
                    //    oOOForm.Handler_DialogButton1Return = new com.idh.forms.oo.Form.DialogReturn(doHandleEnquiryButton1Return);
                    //    oOOForm.doShowModal(oForm);
                    //    oOOForm.DBEnquiry.SBOForm = oOOForm.SBOForm;

                    //} else
                    if (oGridN.doCheckIsSameCol(pVal.ColUID, IDH_WOQITEM._WOHID)) {
                        //Open WOH
                        ArrayList moData = new ArrayList();
                        moData.Add("DoUpdate");
                        moData.Add(oGridN.doGetFieldValue(IDH_WOQITEM._WOHID).ToString());
                        goParent.doOpenModalForm("IDH_WASTORD", oForm, moData);
                    } else if (oGridN.doCheckIsSameCol(pVal.ColUID, IDH_WOQITEM._WORID)) {
                        //Open WOR
                        ArrayList moData = new ArrayList();
                        moData.Add("DoUpdate");
                        moData.Add(oGridN.doGetFieldValue(IDH_WOQITEM._WORID).ToString());
                        setSharedData("IDHJOBS", "ROWCODE", oGridN.doGetFieldValue(IDH_WOQITEM._WORID).ToString());
                        goParent.doOpenModalForm("IDHJOBS", oForm, moData);
                    } else if (oGridN.doCheckIsSameCol(pVal.ColUID, IDH_LVSBTH._PBI)) {//PBI
                                                                                       //MA  Issue:270 2017215 start
                        string testval = oGridN.doGetFieldValue(IDH_LVSBTH._PBI).ToString();
                        if (oGridN.doGetFieldValue(IDH_LVSBTH._PBI).ToString() != "") {
                            ArrayList moData = new ArrayList();
                            moData.Add("DoUpdate");
                            moData.Add(oGridN.doGetFieldValue(IDH_LVSBTH._PBI).ToString());
                            setSharedData("IDH_PBI", "PBICode", oGridN.doGetFieldValue(IDH_LVSBTH._PBI).ToString());
                            goParent.doOpenModalForm("IDH_PBI", oForm, moData);
                        }
                        //MA  Issue:270 2017215 End
                    }
                    //}
                } catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("PBI Mass Updater" + ex.Message);
                }
            }
        }

        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            //MA  Issue:270 2017215
            if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK) {
                if (pVal.ItemUID == "LINESGRID") {
                    if (pVal.BeforeAction == false) {
                        doGridDoubleClick(oForm, ref pVal);
                    }
                }
            }   //MA  Issue:270 2017215 end
            if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED) {
                if (pVal.ItemUID == "LINESGRID") {
                    if (pVal.BeforeAction == false) {
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE);
                        //doGridDoubleClick(oForm, ref pVal);
                    }
                }
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_ADD && !pVal.BeforeAction) {
                pVal.oGrid.getSBOGrid().CommonSetting.SetRowFontColor(pVal.Row + 1, Convert.ToInt32("0000FF", 16));
            }
             //else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT && !pVal.BeforeAction) {
             //    //		oData.GetType()	{Name = "MenuEventClass" FullName = "SAPbouiCOM.MenuEventClass"}	System.Type {System.RuntimeType}
             //    SAPbouiCOM.MenuEventClass oData = (SAPbouiCOM.MenuEventClass)pVal.oData;
             //    if (oData.MenuUID == IDHGrid.GRIDMENUSORTASC) {
             //        string sField = pVal.oGrid.doFindDBField(pVal.ColUID).Trim();
             //        if (sField.Length > 0 && (sField ==  IDH_WOQITEM._Code || sField == IDH_WOQITEM._JobNr || sField == IDH_WOQITEM._EnqID || sField == IDH_WOQITEM._EnqLID)) {
             //            UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
             //            string sFilter = oGridN.getRequiredFilter();
             //            doSetCustomeFilter(oGridN);

             //            oGridN.setOrderValue("Cast( " + sField + " As Int) ");
             //            oGridN.doReloadData("ASC", false);
             //            //doFillGridCombos(oGridN);
             //            oGridN.doApplyRules();
             //            oGridN.setRequiredFilter(sFilter);
             //            return false;
             //        }
             //    } else if (oData.MenuUID == IDHGrid.GRIDMENUSORTDESC) {
             //        string sField = pVal.oGrid.doFindDBField(pVal.ColUID).Trim();
             //        if (sField.Length > 0 && (sField == IDH_WOQITEM._Code || sField ==  IDH_WOQITEM._JobNr || sField == IDH_WOQITEM._EnqID || sField == IDH_WOQITEM._EnqLID)) {
             //            UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
             //            string sFilter = oGridN.getRequiredFilter();
             //            doSetCustomeFilter(oGridN);

             //            oGridN.setOrderValue("Cast( " + sField + " As Int) ");
             //            oGridN.doReloadData("DESC", false);
             //            //doFillGridCombos(oGridN);
             //            oGridN.doApplyRules();
             //            oGridN.setRequiredFilter(sFilter);
             //            return false;
             //        }
             //    }
             //}
             else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SORT && pVal.BeforeAction == false) {
                UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
                doFillGridCombos(oGridN);
                oGridN.doApplyRules();
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK && pVal.Row >= 0 && pVal.BeforeAction) {
                try {
                    //oJobSeq.gotoRow(oJobTypes.Item(iIndex))
                    //sJobType = oJobSeq.U_JobTp
                    if (pVal.oGrid.getSBOGrid().CommonSetting.GetCellFontColor(pVal.Row + 1, pVal.oGrid.doIndexFieldWC(pVal.ColUID) + 1) == Convert.ToInt32("0000FF", 16)) {
                        SAPbouiCOM.MenuItem oMenuItem;
                        oMenuItem = goParent.goApplication.Menus.Item(IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU);

                        SAPbouiCOM.Menus oMenus;
                        SAPbouiCOM.MenuCreationParams oCreationPackage;
                        oCreationPackage = (SAPbouiCOM.MenuCreationParams)goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);

                        oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                        oCreationPackage.UniqueID = "ValidateData";
                        //'oCreationPackage.String = getTranslatedWord("Duplicate Selection")
                        oCreationPackage.String = getTranslatedWord("Validate Data");//
                        oCreationPackage.Enabled = true;
                        oMenus = oMenuItem.SubMenus;
                        oMenus.AddEx(oCreationPackage);

                    }
                } catch (Exception ex) {
                    //com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doApplyNewAmendAction" });
                } finally {
                    //    oForm.Freeze(false);
                }
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT && pVal.Row >= 0 && pVal.BeforeAction) {
                SAPbouiCOM.MenuEvent opVal = (SAPbouiCOM.MenuEvent)pVal.oData;
                if (opVal.MenuUID.Equals("ValidateData")) {

                    doValidateGridCell(oForm, pVal.Row, pVal.ColUID);
                }
            }
            return base.doCustomItemEvent(oForm, ref pVal);
        }
        private void doValidateGridCell(SAPbouiCOM.Form oForm, int r, string sCOlID) {
            try {
                DBOGrid oDBGrid = (DBOGrid)getWFValue(oForm, "LINESGRID");
                IDH_LVSAMD oActionObject = new IDH_LVSAMD();
                oActionObject.UnLockTable = true;
                int iRecCount = oActionObject.getData(IDH_LVSAMD._Active + "=\'Y\'", "");
                if (iRecCount == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Failed to find the Amendment List in the Amendment table.");
                    return;
                }
                ArrayList aAmendmentDesc = new ArrayList();
                oActionObject.first();
                while (oActionObject.next()) {
                    aAmendmentDesc.Add(oActionObject.U_AmdDesc);
                }
                IDH_PBI oPBI = new IDH_PBI();
                oPBI.UnLockTable = true;

                int RGBRed = Convert.ToInt32("0000FF", 16);

                bool bRowValidated = doValidateAmendmentList(oForm,true, r);
                if (bRowValidated) {
                    oDBGrid.getSBOGrid().CommonSetting.SetRowFontColor(r + 1, 0);
                    oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "1");
                    if (oDBGrid.doProcessData()) {
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                 new string[] { "IDH_LVSBTH.doValidateGridCell" });
            }
        }
        private void doApplyNewAmendAction(SAPbouiCOM.Form oForm) {
            try {
                DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");
                IDH_LVSBTH oBatchDetail = (IDH_LVSBTH)oGridN.DBObject;
                if (oBatchDetail.Count >= 0) {
                    oForm.Freeze(true);
                    //oBatchDetail.first();
                    string sNewAmendmentList = getUFValue(oForm, "IDH_ACTNAP").ToString();
                    //    while (oBatchDetail.next()) {
                    //        oBatchDetail.U_AmndTp = sNewAmendmentList;
                    //    }
                    oBatchDetail.doApplyNewAmendAction(sNewAmendmentList);
                    doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE);
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doApplyNewAmendAction" });
            } finally {
                oForm.Freeze(false);
            }
        }
        private void doSendBatchForReview(SAPbouiCOM.Form oForm) {
            try {
                DBOGrid oDBGrid = (DBOGrid)getWFValue(oForm, "LINESGRID");
                if (oDBGrid.getRowCount() == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("No batch loaded to send for process.");
                    return;
                }

                string sUsersForProcess = Config.INSTANCE.getParameter("ULSPRBAT");
                if (sUsersForProcess == string.Empty) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("No user configured for batch process. Please update config list.");
                    return;
                }
                string sBatchNum = "";
                for (int r = 0; r <= oDBGrid.getRowCount() - 1; r++) {
                    if (sBatchNum == "")
                        sBatchNum = oDBGrid.doGetFieldValue(IDH_LVSBTH._BatchNum, r).ToString();
                    else if (sBatchNum != oDBGrid.doGetFieldValue(IDH_LVSBTH._BatchNum, r).ToString() && oDBGrid.doGetFieldValue(IDH_LVSBTH._BatchNum, r).ToString() != "") {
                        if (r != oDBGrid.getRowCount() - 1) {
                            com.idh.bridge.DataHandler.INSTANCE.doUserError("Please only one batch to process.");
                            return;
                        }
                    }
                }
                if (sBatchNum == "") {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Please save data to assign batch number.");
                    return;
                }
                IDH_LVSBHH obatch = new IDH_LVSBHH();
                obatch.UnLockTable = true;
                if (obatch.getByKey(sBatchNum)) {
                    string sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("ALRARDST", new string[] { sBatchNum });
                    if (obatch.U_AlterSend == "Y" && (IDHAddOns.idh.addon.Base.PARENT.doMessage(sResourceMessage, 2, "Yes", "No", null, true) != 1)) {
                        return;
                    }
                }

                if (doValidateAmendmentList(oForm, true,-1) == false) {
                    return;
                }
                
                bool bRet = true;
                obatch = new IDH_LVSBHH();
                obatch.UnLockTable = true;
                if (obatch.getByKey(sBatchNum) &&
                    com.idh.bridge.utils.General.SendInternalAlert(sUsersForProcess, Messages.INSTANCE.getMessage("ALRBTMSG", new string[] { "Mass PBI Updater", sBatchNum }), "Mass PBI Updater Batch#" + sBatchNum)
                    ) {
                    DataHandler.INSTANCE.StartTransaction();
                    //if (obatch.getByKey(sBatchNum)) {
                    obatch.U_AlterSdBy = DataHandler.INSTANCE.User;
                    obatch.U_AlterSdDt = DateTime.Today;
                    obatch.U_AlterSdTm = DateTime.Now.ToString("HHmm");
                    obatch.U_AlterSdTo = sUsersForProcess;
                    obatch.U_AlterSend = "Y";
                    if (obatch.doProcessData()) {
                        IDH_LVSBTH oBatchDetail = (IDH_LVSBTH)oDBGrid.DBObject;

                        if (oBatchDetail.Count >= 0) {
                            oForm.Freeze(true);
                            oBatchDetail.first();
                            while (oBatchDetail.next() && oBatchDetail.Code != string.Empty) {
                                oBatchDetail.U_ProcSts = ((int)IDH_LVSBTH.en_BatchProcessStatus.SentToReview).ToString();
                                if (oBatchDetail.doUpdateDataRow() == false) {
                                    bRet = false;
                                    break;
                                }
                            }

                        }
                    } else {
                        bRet = false;
                    }
                    //}
                    if (DataHandler.INSTANCE.IsInTransaction()) {
                        if (bRet)
                            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                        else
                            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    }
                }

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doSendBatchForReview" });
                if (DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
            } finally {
                oForm.Freeze(false);
            }
        }
        #region"Process Batch"
        private void doProcessBatch(SAPbouiCOM.Form oForm, bool bProcessNow) {
            try {
                string sMsg = "Are you sure to process batch now? It may take several minutes to process batch.";
                if (!bProcessNow)
                    sMsg = "Are you sure to send batch to process later by ISB Commander service?";

                if (IDHAddOns.idh.addon.Base.PARENT.doMessage(sMsg, 2, "Yes", "No", null, true) != 1) {
                    return;
                }
                DBOGrid oDBGrid = (DBOGrid)getWFValue(oForm, "LINESGRID");
                if (oDBGrid.getRowCount() == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("No batch loaded for process.");
                    return;
                }
                bool doCheckForMultiBatch = true;
                string sBatchNum = "";
                try {
                    string sLastSQL = oDBGrid.DBObject.LastQuery.ToLower();
                    string[] aLastSQL = sLastSQL.Split(new string[] { "from" }, StringSplitOptions.None);
                    if (aLastSQL.Length > 1) {
                        sLastSQL = aLastSQL[aLastSQL.Length - 1];
                        string[] aLastSQL2 = sLastSQL.ToLower().Split(new string[] { "order by" }, StringSplitOptions.None);
                        if (aLastSQL2.Length > 0) {
                            sLastSQL = aLastSQL2[0];
                            sLastSQL = "Select Distinct " + IDH_LVSBTH._BatchNum + " From " + sLastSQL;
                            DataRecords oRecs = null;
                            //string sQry = "select DocEntry from " + sDocTable + " where U_FlagToClose = 'Y' AND DocStatus != 'C'";
                            oRecs = DataHandler.INSTANCE.doBufferedSelectQuery("doGetDistinctBatch" + DateTime.Now.ToString("HHmmttss"), sLastSQL);
                            if (oRecs != null && oRecs.RecordCount > 0) {
                                if (oRecs.RecordCount == 1) {
                                    sBatchNum = oRecs.getValue(IDH_LVSBTH._BatchNum).ToString();

                                } else {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Please only one batch to process.");
                                    return;
                                }
                                doCheckForMultiBatch = false;
                            }
                        }
                    }
                } catch (Exception ex) {
                }
                if (doCheckForMultiBatch) {
                    for (int r = 0; r <= oDBGrid.getRowCount() - 1; r++) {
                        if (sBatchNum == "")
                            sBatchNum = oDBGrid.doGetFieldValue(IDH_LVSBTH._BatchNum, r).ToString();
                        else if (sBatchNum != oDBGrid.doGetFieldValue(IDH_LVSBTH._BatchNum, r).ToString() && oDBGrid.doGetFieldValue(IDH_LVSBTH._BatchNum, r).ToString() != "") {
                            if (r != oDBGrid.getRowCount() - 1) {
                                com.idh.bridge.DataHandler.INSTANCE.doUserError("Please only one batch to process.");
                                return;
                            }
                        }
                    }
                }

                if (sBatchNum == "") {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Please save data to assign batch number.");
                    return;
                }
                IDH_LVSBHH obatch = new IDH_LVSBHH();
                obatch.UnLockTable = true;
                if (obatch.getByKey(sBatchNum)) {
                    // string sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("ALRARDST", new string[] { sBatchNum });
                    if (obatch.U_AlterSend != "Y") {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Select batch is not ready to process.");
                        // && (IDHAddOns.idh.addon.Base.PARENT.doMessage(sResourceMessage, 2, "Yes", "No", null, true) != 1)) {
                        return;
                    } else if (obatch.U_BtchClose == "Y") {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Select batch is already closed.");
                        return;
                    }
                } else {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Failed to load batch header.");
                    return;
                }
                //remove re-validate after test on ISS

                IDH_LVSBTH oBatchDetail = (IDH_LVSBTH)oDBGrid.DBObject;
                oBatchDetail.UnLockTable = true;

                // DataHandler.INSTANCE.StartTransaction();
                bool bDone = true;
                oBatchDetail.doBookmark();
                oBatchDetail.first();

                while (oBatchDetail.next()) {
                    if (oBatchDetail.Code != string.Empty) {
                        DataHandler.INSTANCE.StartTransaction();
                        IDH_PBI oPBI = new IDH_PBI();
                        oPBI.UnLockTable = true;
                        if (bProcessNow ) {
                            if ((oBatchDetail.U_ProcSts == ((int)IDH_LVSBTH.en_BatchProcessStatus.SentToReview).ToString() || oBatchDetail.U_ProcSts == ((int)IDH_LVSBTH.en_BatchProcessStatus.ReadyForProcessByService).ToString()) && !string.IsNullOrWhiteSpace(oBatchDetail.U_PBI) && oPBI.getByKey(oBatchDetail.U_PBI) && oPBI.U_IDHNEXTR!=null && oPBI.U_IDHNEXTR!=DateTime.MinValue) {
                                while (oBatchDetail.U_EffDate!=null && oBatchDetail.U_EffDate > oPBI.U_IDHNEXTR) {
                                    IDH_PBI_Handler.doExecutePBIHandler(ref oPBI, oBatchDetail, goParent.goDICompany, false,true, oPBI.U_IDHNEXTR);
                                    oPBI.getByKey(oPBI.Code);
                                    //doExecutePBIHandler(ref oPBI, oBatchDetail);
                                }
                            }
                        }
                        oPBI = null;
                        if (oBatchDetail.doProcessBatchItem(!bProcessNow, ref oPBI)) {
                            if (oPBI != null && !string.IsNullOrEmpty(oPBI.Code))
                                DataHandler.INSTANCE.doInfo("PBI#" + oPBI.Code + " Updated. Starting PBI Execution process");
                            //now process call the PBI Handler
                            if (bProcessNow && (oPBI != null && doExecutePBIHandler(ref oPBI, oBatchDetail))) {
                                //bDone = false;
                                //break;
                                DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            }else if (!bProcessNow) {
                                DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            }
                        } else {
                            bDone = false;
                            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                        }
                    }
                }
                if (bDone) {
                    if (DataHandler.INSTANCE.IsInTransaction())
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    if (bProcessNow)
                        DataHandler.INSTANCE.doInfo("Successfully processed the batch.");
                    else
                        DataHandler.INSTANCE.doInfo("Successfully send the batch for processed later.");
                } else if (bDone == false) {
                    if (DataHandler.INSTANCE.IsInTransaction())
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    DataHandler.INSTANCE.doUserError("Failed to process batch. Please review the error messages.");
                }

                oBatchDetail.doRecallBookmark();
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doProcessBatch" });
                if (DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
            } finally {
                if (DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);

            }
        }
        private bool doValidateAmendmentList(SAPbouiCOM.Form oForm, bool bValidatePBINextRunDate, int row) {
            try {
                DBOGrid oDBGrid = (DBOGrid)getWFValue(oForm, "LINESGRID");
                IDH_LVSAMD oActionObject = new IDH_LVSAMD();
                oActionObject.UnLockTable = true;
                int iRecCount = oActionObject.getData(IDH_LVSAMD._Active + "=\'Y\'", "");
                if (iRecCount == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Failed to find the Amendment List in the Amendment table.");
                    return false;
                }
                ArrayList aAmendmentDesc = new ArrayList();
                oActionObject.first();
                while (oActionObject.next()) {
                    aAmendmentDesc.Add(oActionObject.U_AmdDesc);
                }
                IDH_PBI oPBI = new IDH_PBI();
                oPBI.UnLockTable = true;

                int RGBRed = Convert.ToInt32("0000FF", 16);

                //oDBGrid.getSBOGrid().CommonSetting.GetCellBackColor(1, 1);
                bool bRowValidated = true;
                bool bValidated = true;
                //int iDefaultRGB = 0;
                //if ((getWFValue(oForm, "iDefaultRGB") == null)){
                //    for (int r = 0; r <= oDBGrid.getRowCount() - 1; r++) {
                //        iDefaultRGB = oDBGrid.doGetRowColor(r);
                //        doAddWF(oForm, "iDefaultRGB", iDefaultRGB);
                //        break;
                //    }
                //}
                com.idh.bridge.DataHandler.INSTANCE.doInfo("Please wait while system is validating batch.");
                for (int r = 0; r <= oDBGrid.getRowCount() - 1; r++) {
                    if (row != -1) {
                        r = row;
                        oDBGrid.getSBOGrid().CommonSetting.SetRowFontColor(r + 1, 0);
                    }
                    if (oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString() == string.Empty)
                        continue;
                    DataHandler.INSTANCE.doProgress("IDHVLDPBIBATCH", r, (int)(oDBGrid.getRowCount() - 1));
                    //if (oDBGrid.doGetFieldValue(IDH_LVSBTH._AmndTp, r).ToString() == string.Empty || oActionObject.getData(IDH_LVSAMD._AmdDesc + "=\'" + oDBGrid.doGetFieldValue(IDH_LVSBTH._AmndTp, r).ToString() + "\' And " + IDH_LVSAMD._Active + "=\'Y\'", "") != 1) {
                    bRowValidated = true;
                    if (oDBGrid.doGetFieldValue(IDH_LVSBTH._AmndTp, r).ToString() == string.Empty && oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString() != string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Amendment Type is missineg.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (!aAmendmentDesc.Contains(oDBGrid.doGetFieldValue(IDH_LVSBTH._AmndTp, r).ToString())) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Failed to find Amendment Type " + oDBGrid.doGetFieldValue(IDH_LVSBTH._AmndTp, r).ToString() + ". Please Check Amendment List Table.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._CustCd, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer Code missing.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._CustCd) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._CustNm, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer name missing.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._CustNm) + 1, RGBRed);
                        bRowValidated = false;
                    } else if (!com.isb.enq.lookups.Config.INSTANCE.ValidateBPCodenName(oDBGrid.doGetFieldValue(IDH_LVSBTH._CustCd, r).ToString(), oDBGrid.doGetFieldValue(IDH_LVSBTH._CustNm, r).ToString().Replace("'", "''"))) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid Customer Code /Name.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._CustCd) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._CustAdr, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer address is missing.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._CustAdr) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._CustAdr, r).ToString() != string.Empty
                       && !com.isb.enq.lookups.Config.INSTANCE.ValidateBPAddress(oDBGrid.doGetFieldValue(IDH_LVSBTH._CustCd, r).ToString().Replace("'", "''"), oDBGrid.doGetFieldValue(IDH_LVSBTH._CustAdr, r).ToString().Replace("'", "''"), "S", oDBGrid.doGetFieldValue(IDH_LVSBTH._CusPCode, r).ToString().Replace("'", "''"))) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer address/postcode is not valid.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._CustAdr) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._JbTypeDs, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Order Type is missing.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._JbTypeDs) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._JbTypeDs, r).ToString() != string.Empty && Config.INSTANCE.getValueFromTablebyKey("[@IDH_JOBTYPE]", "Code", "U_JobTp", IDHAddOns.idh.data.Base.doFixForSQL(oDBGrid.doGetFieldValue(IDH_LVSBTH._JbTypeDs, r).ToString())) == null) {
                        //Config.INSTANCE.getValueFromTablebyKey("[@IDH_JOBTYPE]", "Code", "U_JobTp", oDBGrid.doGetFieldValue(IDH_LVSBTH._JbTypeDs, r).ToString()).ToString();
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid Order Type. Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._JbTypeDs) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._SuppCd, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Supplier code is missing.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._SuppCd) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._SuppNm, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Supplier name is missing.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._SuppNm) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (!com.isb.enq.lookups.Config.INSTANCE.ValidateBPCodenName(oDBGrid.doGetFieldValue(IDH_LVSBTH._SuppCd, r).ToString(), oDBGrid.doGetFieldValue(IDH_LVSBTH._SuppNm, r).ToString().Replace("'", "''"))) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid Supplier Code /Name.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._SuppCd) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._ItemCd, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Container code is missing.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._ItemCd) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._ItemNm, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Container name is missing.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._ItemNm) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._WastCd, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Waste item code is missing.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._WastCd) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._WasteNm, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Waste item name is missing.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._WasteNm) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._WastCd, r).ToString() != string.Empty 
                            && !com.isb.enq.lookups.Config.INSTANCE.ValidateWasteItem(oDBGrid.doGetFieldValue(IDH_LVSBTH._WastCd, r).ToString())) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Waste item: " + oDBGrid.doGetFieldValue(IDH_LVSBTH._WastCd, r).ToString() + " is not a valid waste item.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._WastCd) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._ItemCd, r).ToString() != string.Empty 
                            && !com.isb.enq.lookups.Config.INSTANCE.ValidateContainerItem(oDBGrid.doGetFieldValue(IDH_LVSBTH._ItemCd, r).ToString())) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Container item: " + oDBGrid.doGetFieldValue(IDH_LVSBTH._WastCd, r).ToString() + " is not a valid container item.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._ItemCd) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._UOM, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("UOM is missing.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._UOM) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._UOM, r).ToString() != string.Empty && !Config.INSTANCE.isValidUOM(oDBGrid.doGetFieldValue(IDH_LVSBTH._UOM, r).ToString())) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid UOM.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._UOM) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._FrqType, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Frequency type is missing.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._FrqType) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._FrqType, r).ToString() != "W" && oDBGrid.doGetFieldValue(IDH_LVSBTH._FrqType, r).ToString() != "D" && oDBGrid.doGetFieldValue(IDH_LVSBTH._FrqType, r).ToString() != "M" && oDBGrid.doGetFieldValue(IDH_LVSBTH._FrqType, r).ToString() != "Y") {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid frequency type. Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._FrqType) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._EffDate, r) == null || oDBGrid.doGetFieldValue(IDH_LVSBTH._EffDate, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Effective date is missing. Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._EffDate) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._Rebate, r).ToString() == "Y" && oDBGrid.doGetFieldValue(IDH_LVSBTH._CMnSVst, r).ToString() != "" && (oDBGrid.doGetFieldValue(IDH_LVSBTH._CMnSVst, r).ToString().ToLower() != "no")) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("For Rebate job CIP-Min Site Visit  must be \'No\' or blank. Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._CMnSVst) + 1, RGBRed);
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._Rebate, r).ToString() == "Y" && oDBGrid.doGetFieldValue(IDH_LVSBTH._CMnChg, r).ToString() != "" && (oDBGrid.doGetFieldValue(IDH_LVSBTH._CMnChg, r).ToString().ToLower() != "no")) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("For Rebate job CIP-Min Charge must be \'No\'. Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._CMnChg) + 1, RGBRed);
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._Rebate, r).ToString() == "Y" && oDBGrid.doGetFieldValue(IDH_LVSBTH._CVarRFix, r).ToString() != "" && (oDBGrid.doGetFieldValue(IDH_LVSBTH._CVarRFix, r).ToString().ToLower() != "variable")) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("For Rebate job CIP-Variable Or Fixed QTY must be \'Variable\'. Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._CVarRFix) + 1, RGBRed);
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._Rebate, r).ToString() == "Y" && oDBGrid.doGetFieldValue(IDH_LVSBTH._SMnSVst, r).ToString() != "" && (oDBGrid.doGetFieldValue(IDH_LVSBTH._SMnSVst, r).ToString().ToLower() != "no")) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("For Rebate job SIP-Min Site Visit  must be \'No\' or blank. Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._SMnSVst) + 1, RGBRed);
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._Rebate, r).ToString() == "Y" && oDBGrid.doGetFieldValue(IDH_LVSBTH._SMnChg, r).ToString() != "" && (oDBGrid.doGetFieldValue(IDH_LVSBTH._SMnChg, r).ToString().ToLower() != "no")) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("For Rebate job SIP-Min Charge must be \'No\'. Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._SMnChg) + 1, RGBRed);
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._Rebate, r).ToString() == "Y" && oDBGrid.doGetFieldValue(IDH_LVSBTH._SVarRFix, r).ToString() != "" && (oDBGrid.doGetFieldValue(IDH_LVSBTH._SVarRFix, r).ToString().ToLower() != "variable")) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("For Rebate job SIP-Variable Or Fixed QTY must be \'Variable\'. Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._SVarRFix) + 1, RGBRed);
                        bRowValidated = false;
                    } else if ((oDBGrid.doGetFieldValue(IDH_LVSBTH._PERTYP, r) != null && !string.IsNullOrWhiteSpace( oDBGrid.doGetFieldValue(IDH_LVSBTH._PERTYP, r).ToString())  &&
                                oDBGrid.doGetFieldValue(IDH_LVSBTH._PERTYP, r).ToString() != IDH_PBI_Handler.PER_Weeks && oDBGrid.doGetFieldValue(IDH_LVSBTH._PERTYP, r).ToString()!=IDH_PBI_Handler.PER_Months && oDBGrid.doGetFieldValue(IDH_LVSBTH._PERTYP, r).ToString() != IDH_PBI_Handler.PER_Quarts)) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid Period type value. Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._PERTYP) + 1, RGBRed);
                        bRowValidated = false;
                    } else if ((oDBGrid.doGetFieldValue(IDH_LVSBTH._IDHRECFQ, r) != null && Convert.ToInt32(oDBGrid.doGetFieldValue(IDH_LVSBTH._IDHRECFQ, r))<0)) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid Recurrence Frequency. Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._IDHRECFQ) + 1, RGBRed);
                        bRowValidated = false;
                    } else {
                        oActionObject.getData(IDH_LVSAMD._Active + "=\'Y\' And " + IDH_LVSAMD._AmdDesc + "=\'" + oDBGrid.doGetFieldValue(IDH_LVSBTH._AmndTp, r).ToString() + "\'", "");
                        if (oActionObject.doCheckAmendListChange(IDH_LVSAMD.AddPBI) && oDBGrid.doGetFieldValue(IDH_LVSBTH._PBI, r).ToString() != string.Empty) {
                            com.idh.bridge.DataHandler.INSTANCE.doUserError("PBI# provided for amendment Type " + oDBGrid.doGetFieldValue(IDH_LVSBTH._AmndTp, r).ToString() + ". PBI#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._PBI, r).ToString());
                            oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                            oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._PBI) + 1, RGBRed);
                            //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                            bRowValidated = false;
                        } else if (!oActionObject.doCheckAmendListChange(IDH_LVSAMD.AddPBI)) {
                            if (oDBGrid.doGetFieldValue(IDH_LVSBTH._PBI, r).ToString() == string.Empty) {
                                com.idh.bridge.DataHandler.INSTANCE.doUserError("PBI number is missing.");
                                oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                                oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._PBI) + 1, RGBRed);
                                bRowValidated = false;
                            } else if (!oPBI.getByKey(oDBGrid.doGetFieldValue(IDH_LVSBTH._PBI, r).ToString())) {
                                com.idh.bridge.DataHandler.INSTANCE.doUserError("Failed to load PBI#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._PBI, r).ToString() + ".");
                                oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                                oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._PBI) + 1, RGBRed);
                                //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                                bRowValidated = false;
                            } else if (oPBI.Count > 0) {
                                if (oPBI.U_IDHRECEN == 2 && oPBI.U_IDHRECED < DateTime.Today) {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("PBI#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._PBI, r).ToString() + " already ended.");
                                    oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                                    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._PBI) + 1, RGBRed);
                                    bRowValidated = false;
                                } else if (oPBI.U_IDHRECEN == 1 && oPBI.U_IDHRECCE <= oPBI.U_IDHRECCT) {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("PBI#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._PBI, r).ToString() + " already ended.");
                                    oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                                    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._PBI) + 1, RGBRed);
                                    bRowValidated = false;
                                } else if (oPBI.U_IDHCCODE != oDBGrid.doGetFieldValue(IDH_LVSBTH._CustCd, r).ToString()) {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer code is different than on PBI#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._PBI, r).ToString() + ".");
                                    oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                                    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._CustCd) + 1, RGBRed);
                                    bRowValidated = false;
                                } else if (oPBI.U_IDHCNAME != oDBGrid.doGetFieldValue(IDH_LVSBTH._CustNm, r).ToString()) {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer name is different than on PBI#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._PBI, r).ToString() + ".");
                                    oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                                    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._CustNm) + 1, RGBRed);
                                    bRowValidated = false;
                                } else if (oPBI.U_IDHSADDR != oDBGrid.doGetFieldValue(IDH_LVSBTH._CustAdr, r).ToString()) {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer address is different than on PBI#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._PBI, r).ToString() + ".");
                                    oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                                    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._CustAdr) + 1, RGBRed);
                                    bRowValidated = false;
                                } else if (oPBI.U_IDHDISCD != oDBGrid.doGetFieldValue(IDH_LVSBTH._SuppCd, r).ToString()) {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Supplier code is different than on PBI#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._PBI, r).ToString() + ".");
                                    oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                                    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._SuppCd) + 1, RGBRed);
                                    bRowValidated = false;
                                } else if (oPBI.U_IDHDISDC != oDBGrid.doGetFieldValue(IDH_LVSBTH._SuppNm, r).ToString()) {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Supplier name is different than on PBI#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._PBI, r).ToString() + ".");
                                    oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                                    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._SuppNm) + 1, RGBRed);
                                    bRowValidated = false;
                                } else if (oPBI.U_IDHWCICD != oDBGrid.doGetFieldValue(IDH_LVSBTH._WastCd, r).ToString()) {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Waste code is different than on PBI#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._PBI, r).ToString() + ".");
                                    oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                                    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._WastCd) + 1, RGBRed);
                                    bRowValidated = false;
                                } else if (oPBI.U_IDHWCIDC != oDBGrid.doGetFieldValue(IDH_LVSBTH._WasteNm, r).ToString()) {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Waste name is different than on PBI#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._PBI, r).ToString() + ".");
                                    oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                                    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._WasteNm) + 1, RGBRed);
                                    bRowValidated = false;
                                } else if (oPBI.U_IDHCOICD != oDBGrid.doGetFieldValue(IDH_LVSBTH._ItemCd, r).ToString()) {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Container code is different than on PBI#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._PBI, r).ToString() + ".");
                                    oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                                    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._ItemCd) + 1, RGBRed);
                                    bRowValidated = false;
                                } else if (oPBI.U_IDHCOIDC != oDBGrid.doGetFieldValue(IDH_LVSBTH._ItemNm, r).ToString()) {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Container name is different than on PBI#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._PBI, r).ToString() + ".");
                                    oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                                    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._ItemNm) + 1, RGBRed);
                                    bRowValidated = false;
                                } else if (Config.INSTANCE.getParameterAsBool("PBIRTDAY",false)==false && oPBI.U_IDHNEXTR < com.idh.utils.Dates.doStrToDate(oDBGrid.doGetFieldValue(IDH_LVSBTH._EffDate, r).ToString())) {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Row#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString() + ";PBI#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._PBI, r).ToString() + " has next run date earlier than the effective date. You need to run the PBI before updating the PBI.");
                                    oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                                    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._EffDate) + 1, RGBRed);
                                    bRowValidated = false;
                                }
                                //else if (false && oPBI.U_IDHLASTR_AsString() != null && oPBI.U_IDHLASTR_AsString() != string.Empty &&
                                //            oPBI.U_IDHLASTR.Month > Convert.ToDateTime(com.idh.utils.Dates.doStrToDate(oDBGrid.doGetFieldValue(IDH_LVSBTH._EffDate, r).ToString())).Month) {
                                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Row#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString() + ";Action date for PBI#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._PBI, r).ToString() + " in more than one month earlier.");
                                //    oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._EffDate) + 1, RGBRed);
                                //    bRowValidated = false;
                                //}
                            }
                        }
                    }
                    //}
                    //else if (oDBGrid.doGetFieldValue(IDH_LVSBTH._CusPCode, r).ToString() == string.Empty && Config.ParameterAsBool("IGNPCPBI", false) == false) {
                    //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer postcode is missing.Code#" + oDBGrid.doGetFieldValue(IDH_LVSBTH._Code, r).ToString());
                    //    oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "0");
                    //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_LVSBTH._CusPCode) + 1, RGBRed);
                    //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_LVSBTH._AmndTp));
                    //    bRowValidated = false;
                    //}
                    if (!bRowValidated) {
                        //  oDBGrid.getSBOGrid().CommonSetting.SetRowFontColor(r + 1, RGBRed);
                        bValidated = false;
                    } else { //if (oDBGrid.getSBOGrid().CommonSetting.GetRowFontColor(r + 1)==RGBRed)
                        oDBGrid.getSBOGrid().CommonSetting.SetRowFontColor(r + 1, 0);
                        oDBGrid.doSetFieldValue(IDH_LVSBTH._VldStatus, r, "1");
                    }
                    if (row != -1) {
                        break;
                    }
                }
                if (!bValidated)
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Failed to validate batch rows. Please review the errors listed above.");
                else
                    com.idh.bridge.DataHandler.INSTANCE.doInfo("Batch validation completed.");
                return bValidated;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_LVSBTH.doValidateAmendmentList" });
                return false;
            } finally {
                DataHandler.INSTANCE.doProgressDone("IDHVLDPBIBATCH");
            }
        }
        private bool doExecutePBIHandler(ref IDH_PBI oPBI, IDH_LVSBTH oBatchDetail) {
            try {

                return IDH_PBI_Handler.doExecutePBIHandler(ref oPBI, oBatchDetail, goParent.goDICompany, false,false);

                //bool DoAddNewPBI = false;
                //IDH_PBI_Handler oWOM = new IDH_PBI_Handler(ref goParent.goDICompany);
                ////This DLL Creates/updates waste orders and row based on details of the WasteOrder Instruction. Also updates any documents linked to WO
                ////######################################################################################################################
                //// string sActDate=null;//= oBatchDetail.U_EffDate.Year?1:null, ;//getUFValue(oForm, "IDH_ACDATE");
                ////if (!(oBatchDetail.U_EffDate.Year == 1 || oBatchDetail.U_EffDate.Year == 1900))
                ////   sActDate = oBatchDetail.U_EffDate_AsString();

                //IDH_LVSAMD oActionObject = new IDH_LVSAMD();
                //oActionObject.UnLockTable = true;
                //string sAction = oBatchDetail.U_AmndTp;
                //int iRet = oActionObject.getData(IDH_LVSAMD._AmdDesc + "=\'" + sAction + "\' And " + IDH_LVSAMD._Active + "=\'Y\'", "");
                //DoAddNewPBI = oActionObject.doCheckAmendListChange(IDH_LVSAMD.AddPBI);
                //DateTime dActDate = oBatchDetail.U_EffDate;// = default(DateTime);
                //                                           // DateTime dPBIStartDate;

                //if (!(oBatchDetail.U_EffDate.Year == 1 || oBatchDetail.U_EffDate.Year == 1900)) {
                //    string sPBIStart = oPBI.U_IDHRECSD_AsString();
                //    System.DateTime dPBIStart = Conversions.ToDateTime(sPBIStart);
                //    dActDate = oBatchDetail.U_EffDate;
                //    if (System.DateTime.Compare(dActDate, dPBIStart) < 0) {
                //        dActDate = dPBIStart;
                //    }
                //}

                //bool bDoRestOfUpdates = true;
                //bool bResult = true;
                //bool bDoToEnd = false;
                //string sValToEnd = "";// a check box labeled To End on PBI; right to Action date getUFValue(oForm, "IDHTOE");

                //if (sValToEnd == "Y") {
                //    bDoToEnd = true;
                //}

                //if (!DoAddNewPBI) {
                //    bool doRemovePBI = false;
                //    if (oActionObject.doCheckAmendListChange(IDH_LVSAMD.RemovePBI)) {
                //        doRemovePBI = true;
                //    }
                //    if (doRemovePBI && bDoRestOfUpdates && oPBI.U_IDHRECEN == 2 && (oPBI.U_IDHRECED != null)) {
                //        if ((oPBI.U_IDHRECED != null) && oPBI.U_IDHRECED_AsString().Length >= 6) {
                //            bResult = oWOM.doClearAfterTerminationDate(oPBI.Code, oPBI.U_IDHRECED);
                //        }
                //        bDoRestOfUpdates = false;
                //    }

                //}

                //if (bDoRestOfUpdates) {

                //    if (bDoToEnd || oPBI.U_IDHNEXTR > dActDate) {
                //        DateTime dSetNextRun = oPBI.U_IDHNEXTR;
                //        bResult = oWOM.ProcessOrderRunToEnd(oPBI.Code, dActDate, dSetNextRun);
                //    } else {
                //        bResult = oWOM.ProcessOrderRun(oPBI.Code, dActDate);
                //        if (bResult && Config.ParameterAsBool("PBIRTDAY", true)) {
                //            while (bResult && DateTime.Compare(oPBI.U_IDHNEXTR, new DateTime(DateTime.Today.AddMonths(1).Year, DateTime.Today.AddMonths(1).Month, 1)) < 0) {
                //                dActDate = dActDate.AddMonths(oPBI.U_IDHRECFQ);
                //                bResult = oWOM.ProcessOrderRun(oPBI.Code, dActDate);
                //            }
                //        }
                //    }
                //    if (bResult == true) {
                //    } else {
                //        return false;
                //    }
                //} else {
                //    if (bResult == true) {
                //    } else {
                //        return false;
                //    }
                //}
                //return bResult;
                //######################################################################################################################

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_LVSBTH.doExecutePBIHandler" });
                return false;
            } finally {
            }
            return true;
        }         
        #endregion
        #region "Save Batch"
        private void doSaveBatch(SAPbouiCOM.Form oForm) {
            try {
                bool bret;
                DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");
                oGridN.doProcessData();
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);
                oGridN.setInitialFilterValue("");

                //IDH_LVSBTH oBatchDetail = (IDH_LVSBTH)oGridN.DBObject;
                //idh.dbObjects.numbers.NumbersPair oNextNum;

                //IDH_LVSBHH objBatch = new IDH_LVSBHH();
                //if (oBatchDetail.Count >= 0) {
                //    oForm.Freeze(true);
                //    oBatchDetail.first();
                //    if (oBatchDetail.U_BatchNum == string.Empty) {
                //        oNextNum = objBatch.getNewKey();
                //        while (oBatchDetail.next() && oBatchDetail.Code!=string.Empty) {
                //            oBatchDetail.U_BatchNum = oNextNum.CodeCode;
                //        }
                //        objBatch.doAddEmptyRow(true);
                //        objBatch.Code = oNextNum.CodeCode;
                //        objBatch.Name = oNextNum.NameCode;
                //    }
                //    bret = oBatchDetail.doProcessData();
                //    bret = objBatch.doProcessData();
                //    doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);
                //    oGridN.setInitialFilterValue("");
                //}
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doSaveBatch" });
            } finally {
                oForm.Freeze(false);
            }
        }
        #endregion
        #region "Delete Batch"
        private void doDeleteBatch(SAPbouiCOM.Form oForm, string sComments) {
            try {
                string sBatches = getItemValue(oForm, "IDH_BATCHF").Trim();
                DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");
                // string sDeleteComments = "";
                string[] aBatches = sBatches.Split(',');
                IDH_LVSBHH objBatchHeader = new IDH_LVSBHH();
                objBatchHeader.UnLockTable = true;
                IDH_LVSBTH objBatch = new IDH_LVSBTH();
                objBatch.UnLockTable = true;
                bool bRet = true;
                DataHandler.INSTANCE.StartTransaction();
                int iRet = objBatchHeader.getData(IDH_LVSBHH._Code + " In (" + sBatches + ") ", "");
                if (iRet > 0) {
                    objBatchHeader.first();
                    while (objBatchHeader.next()) {
                        objBatchHeader.U_BthStatus = "0";
                        objBatchHeader.U_Comment = sComments;
                        bRet = objBatchHeader.doUpdateDataRow("Batch Deleted");
                        if (!bRet) {
                            break;
                        }
                    }
                }
                if (bRet) {
                    //delete detailed batch
                    iRet = objBatch.getData(IDH_LVSBTH._BatchNum + " In (" + sBatches + ") ", "");
                    if (iRet > 0) {
                        objBatch.first();
                        int iR = 0;
                        while (objBatch.next()) {
                            objBatch.U_BthStatus = "0";
                            //objBatch.U_Comments = sComments;
                            bRet = objBatch.doUpdateDataRow("Batch Deleted");
                            if (!bRet) {
                                break;
                            }
                            DataHandler.INSTANCE.doProgress("BATCHDELETE", iR++, iRet);
                        }
                    }
                }
                if (!bRet && DataHandler.INSTANCE.IsInTransaction()) {
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Failed to delete batched.");
                } else if (bRet && DataHandler.INSTANCE.IsInTransaction()) {
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText("Successfully deleted batches [" + sBatches + "]", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    doReLoadData(oForm, true);
                }


            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doDeleteBatch" });
            } finally {
                oForm.Freeze(false);
                DataHandler.INSTANCE.doProgressDone("BATCHDELETE");
            }
        }
        private void doPreDeleteBatch(SAPbouiCOM.Form oForm) {
            try {
                string sResourceMessage = "";
                if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Save") || ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update")) {
                    sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("SAVEFORM", null);
                    com.idh.bridge.DataHandler.INSTANCE.doError(sResourceMessage);
                    return;
                }
                string sBatches = getItemValue(oForm, "IDH_BATCHF").Trim();
                //  DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");

                if (sBatches == null || sBatches == string.Empty) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Please provide batches to delete.");
                    doSetFocus(oForm, "IDH_BATCHF");
                    return;
                }
                doReLoadData(oForm, true);
                sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("LSRER006", new string[] { sBatches });

                if (IDHAddOns.idh.addon.Base.PARENT.doMessage(sResourceMessage, 2, "Yes", "No", null, true) != 1) {
                    return;
                }
                goParent.doOpenModalForm("IDH_COMMFRM", oForm);


            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doPreDeleteBatch" });
            } finally {
                oForm.Freeze(false);
            }
        }
        #endregion

        #region Import From Excel

        private void doImportExcel(SAPbouiCOM.Form oForm) {
            try {
                DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");
                bool bCancelled = false;
                string sFileName = "";
                doCallWinFormOpener(delegate { BrowserExcelFile(oForm, ref sFileName, ref bCancelled); }, oForm);
                // sFileName = @"F:\ISB Projects\ISS\Dora\ISS_ServiceAmendments_Mansoor4.xlsx";
                if (bCancelled)
                    return;
                if (sFileName == null || sFileName.Trim() == "") {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Please provide a valid file name.");
                    return;
                }

                com.idh.bridge.DataHandler.INSTANCE.doInfo("Please wait while system is importing file.");//, SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                if (FileOpen(sFileName))//MA  Issue:269 2017217
                {
                    doReadExcelFile(oForm, sFileName, oGridN);
                    doFillGridCombos(oGridN);
                    if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update")) {
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE);
                    }
                }// MA  Issue: 269 2017217 end
                //
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error retrieving the Customers Address - " + sCardCode);
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new String[] { "Exporting All live services to csv." });
            }
        }
        private void doReadExcelFile(SAPbouiCOM.Form oForm, string sFileName, DBOGrid oDBGrid) {
            //  DBOGrid oDBGrid = (DBOGrid)getWFValue(oForm, "LINESGRID");
            try {
                string sNewBatch = "";
                ImportLiveService oReadFile = new ImportLiveService();
                oReadFile.doReadFileODBC(oDBGrid, oForm, sFileName, ref sNewBatch);
                FilterFields moFilterFields = oDBGrid.getGridControl().getFilterFields();
                try {
                    for (int i = 0; i <= moFilterFields.Count - 1; i++) {
                        FilterField moFilterFiled = (FilterField)moFilterFields[i];
                        setUFValue(oForm, moFilterFiled.msFieldName, "");
                    }
                } catch (Exception ex) {

                }
                //IDH_BATCHF

                setItemValue(oForm, "IDH_BATCHF", sNewBatch);
                oDBGrid.doReloadData("", false, true);
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error retrieving the Customers Address - " + sCardCode);
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new String[] { "Error in doReadExcelFile." });
            }
        }
        public void BrowserExcelFile(SAPbouiCOM.Form oForm, ref string sFileName, ref bool bCancelled) {
            System.Windows.Forms.OpenFileDialog oOpenFile = new System.Windows.Forms.OpenFileDialog();
            try {
                string sExt = Config.INSTANCE.getImportExtension();
                if (sExt != null && sExt.Length > 0) {
                    if (sExt.IndexOf("|") == -1) {
                        sExt = "(" + sExt + ")|" + sExt;
                    }
                    oOpenFile.Filter = sExt;
                }

            } catch (Exception ex) {
                //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error importing the data.");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", new string[] { string.Empty });
            }
            oOpenFile.FileName = "OpenExcelFile";
            oOpenFile.InitialDirectory = com.isb.enq.lookups.Config.INSTANCE.getImportLiveServicesFolder();
            if (com.idh.win.AppForm.OpenFileDialog(oOpenFile) == System.Windows.Forms.DialogResult.OK) {
                sFileName = oOpenFile.FileName;
            } else
                bCancelled = true;

        }

        #region "Read and Process Excel File"
        public class ImportLiveService {
            public static int miMinRows = 8;
            public static string SheetName = "Service Amendments";
            public static string[] TRIGGERS = {"", "WORow / PO Number", "PBI", "Amendment Type", "Customer-Code", "Customer-Name", "Customer Site Address", "Post Code",
                "Order Type", "Supplier Code", "Supplier Name", "Supplier Reference","Supplier Address","Supplier Postcode", "Container-Code", "Container", "Waste Material-Code", "Waste Material",
                "Unit of Measure (UOM)", "Qty", "LPW", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun",
                "SIP-Min Site Visit", "SIP-Min Charge", "SIP-Variable Or Fixed QTY",
                "SIP-Minimum Charge", "SIP-Minimum Charge Upto Volume", "SIP-Rate above min volume", "SIP-Haulage / Service / Rental",
                "CIP-Minimum Charge", "CIP-Minimum Charge Upto Volume", "CIP-Rate above min Volume", "CIP-Haulage / Service / Rental","CIP-Min Site Visit" ,
                "CIP-Min Charge" ,"CIP-Variable Or Fixed QTY",
                "Effective Date", "Comments", "WTN From", "WTN To", "Freqency Type" , "Frequency Count" , "Auto Complete","Rebate",
            "Routable Checkbox",
                "Route Code Monday","Route Code Tuesday","Route Code Wednesday","Route Code Thursday","Route Code Friday",
            "Route Code Saturday","Route Code Sunday",
                "Route Sequence Monday","Route Sequence Tuesday","Route Sequence Wednesday","Route Sequence Thursday","Route Sequence Friday",
            "Route Sequence Saturday","Route Sequence Sunday",
                "Route Comments","Driver Comments"  ,"Auto Start Date","Period type","Recurrence Frequency"  };
            public class ColPos {

                public static int WORowPONumber = 1;
                public static int PBI = 2;
                public static int AmendmentType = 3;
                public static int CustomerCode = 4;
                public static int CustomerName = 5;

                public static int CustomerSiteAddress = 6;
                public static int PostCode = 7;
                public static int OrderType = 8;
                public static int SupplierCode = 9;
                public static int SupplierName = 10;
                //public static int Supplier Name =  ;
                public static int SupplierReference = 11;

                public static int SupplierAddress = 12;
                public static int SupplierPostCode = 13;

                public static int ContainerCode = 14;
                public static int Container = 15;
                public static int WasteMaterialCode = 16;
                public static int WasteMaterial = 17;
                public static int UnitofMeasure = 18;
                public static int Qty = 19;
                public static int LPW = 20;
                public static int Mon = 21;
                public static int Tue = 22;
                public static int Wed = 23;
                public static int Thu = 24;
                public static int Fri = 25;
                public static int Sat = 26;
                public static int Sun = 27;

                public static int SIPMinSiteVisit = 28;
                public static int SIPMinCharge = 29;
                public static int SIPVariableOrFixedQTY = 30;

                public static int SIPMinimumCharge = 31;
                public static int SIPMinimumChargeUptoVolume = 32;
                public static int SIPRateAboveMinVolume = 33;
                public static int SIPHaulageServiceRental = 34;

                public static int CIPMinimumCharge = 35;
                public static int CIPMinimumChargeUptoVolume = 36;
                public static int CIPRateAboveMinVolume = 37;
                public static int CIPHaulageServiceRental = 38;


                public static int CIPMinSiteVisit = 39;
                public static int CIPMinCharge = 40;
                public static int CIPVariableOrFixedQTY = 41;

                public static int EffectiveDate = 42;
                public static int Comments = 43;
                public static int WTNDateFrom = 44;
                public static int WTNDateTo = 45;
                public static int FrqType = 46;
                public static int FrqCount = 47;
                public static int AutoComplete = 48;
                public static int Rebate = 49;

                public static int RoutableCheckbox= 50;
                public static int RouteCodeMonday = 51;
                public static int RouteCodeTuesday= 52;
                public static int RouteCodeWednesday= 53;
                public static int RouteCodeThursday= 54;
                public static int RouteCodeFriday= 55;
                public static int RouteCodeSaturday= 56;
                public static int RouteCodeSunday= 57;
                public static int RouteSequenceMonday = 58;
                public static int RouteSequenceTuesday = 59;
                public static int RouteSequenceWednesday= 60;
                public static int RouteSequenceThursday= 61;
                public static int RouteSequenceFriday= 62;
                public static int RouteSequenceSaturday= 63;
                public static int RouteSequenceSunday= 64;

                public static int RouteComments = 65;
                public static int DriverComments= 66;
                public static int AutoStartDate = 67;

                //"Period type","Recurrence Frequency"
                public static int Periodtype= 68;
                public static int RecurrenceFrequency= 69;

            }

            //class Coord {
            //    public int Row;
            //    public int Col;
            //    public Coord(int iRow, int iCol) {
            //        Row = iRow;
            //        Col = iCol;
            //    }
            //}
            public ImportLiveService() { }
            //public void doReadFile(DBOGrid oGridN, SAPbouiCOM.Form oForm, string sExcelFile) {
            //    ExcelReader oExcel = new ExcelReader(sExcelFile, 1, Config.INSTANCE.getParameterAsBool("IMPRFC97", false));
            //    if (!oExcel.doReadFile()) {
            //        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error importing the file. Excel Error: " + oExcel.LastError, "ERSYIMPF", new String[] { oExcel.LastError });
            //        return;
            //    }
            //    long lRows = oExcel.Rows;
            //    long lCols = oExcel.Cols;
            //    bool bItemsFound = false;
            //    string sFilter;
            //    //DBOGrid oGridN=null;//= (DBOGrid)getWFValue(oForm, "LINESGRID");
            //    //IDH_LVSBTH oImportLog =oGridN.DBObject ;// new IDH_INCPMLG();
            //    try {
            //        if (lRows >= miMinRows) {
            //            if (lCols > 30) {
            //                if (oExcel.getValue(miMinRows - 1, ColPos.WORowPONumber).Equals(TRIGGERS[1], StringComparison.OrdinalIgnoreCase) &&
            //                        oExcel.getValue(miMinRows - 1, ColPos.PBI).Equals(TRIGGERS[2], StringComparison.OrdinalIgnoreCase) &&
            //                        oExcel.getValue(miMinRows - 1, ColPos.AmendmentType).Equals(TRIGGERS[3], StringComparison.OrdinalIgnoreCase) &&
            //                         oExcel.getValue(miMinRows - 1, ColPos.CustomerName).Equals(TRIGGERS[4], StringComparison.OrdinalIgnoreCase) &&
            //                          oExcel.getValue(miMinRows - 1, ColPos.CustomerSiteAddress).Equals(TRIGGERS[5], StringComparison.OrdinalIgnoreCase) &&
            //                           oExcel.getValue(miMinRows - 1, ColPos.PostCode).Equals(TRIGGERS[6], StringComparison.OrdinalIgnoreCase) &&
            //                            oExcel.getValue(miMinRows - 1, ColPos.OrderType).Equals(TRIGGERS[7], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.SupplierName).Equals(TRIGGERS[8], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.SupplierReference).Equals(TRIGGERS[9], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Container).Equals(TRIGGERS[10], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.WasteMaterial).Equals(TRIGGERS[11], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.UnitofMeasure).Equals(TRIGGERS[12], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Qty).Equals(TRIGGERS[13], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.LPW).Equals(TRIGGERS[14], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Mon).Equals(TRIGGERS[15], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Tue).Equals(TRIGGERS[16], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Wed).Equals(TRIGGERS[17], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Thu).Equals(TRIGGERS[18], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Fri).Equals(TRIGGERS[19], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Sat).Equals(TRIGGERS[20], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Sun).Equals(TRIGGERS[21], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.CIPMinimumCharge).Equals(TRIGGERS[22], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.CIPMinimumChargeUptoVolume).Equals(TRIGGERS[23], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.CIPRateAboveMinVolume).Equals(TRIGGERS[24], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.CIPHaulageServiceRental).Equals(TRIGGERS[25], StringComparison.OrdinalIgnoreCase) &&
            //                              oExcel.getValue(miMinRows - 1, ColPos.SIPMinimumCharge).Equals(TRIGGERS[26], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.SIPMinimumChargeUptoVolume).Equals(TRIGGERS[27], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.SIPRateAboveMinVolume).Equals(TRIGGERS[28], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.SIPHaulageServiceRental).Equals(TRIGGERS[29], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.EffectiveDate).Equals(TRIGGERS[30], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Comments).Equals(TRIGGERS[31], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.WTNDateFrom).Equals(TRIGGERS[32], StringComparison.OrdinalIgnoreCase) &&
            //                        oExcel.getValue(miMinRows - 1, ColPos.WTNDateTo).Equals(TRIGGERS[33], StringComparison.OrdinalIgnoreCase) &&
            //                        oExcel.getValue(miMinRows - 1, ColPos.FrqType).Equals(TRIGGERS[34], StringComparison.OrdinalIgnoreCase) &&
            //                        oExcel.getValue(miMinRows - 1, ColPos.FrqCount).Equals(TRIGGERS[35], StringComparison.OrdinalIgnoreCase) &&
            //                        oExcel.getValue(miMinRows - 1, ColPos.AutoComplete).Equals(TRIGGERS[36], StringComparison.OrdinalIgnoreCase) &&
            //                        oExcel.getValue(miMinRows - 1, ColPos.Rebate).Equals(TRIGGERS[37], StringComparison.OrdinalIgnoreCase)
            //                    ) {
            //                    //string sFilter = oGridN.getRequiredFilter();
            //                    //oGridN.doReloadData("", false, true);
            //                    //oGridN.setRequiredFilter(sFilter);
            //                    oGridN.DBObject.doClearBuffers();
            //                    sFilter = oGridN.getRequiredFilter();
            //                    oGridN.setRequiredFilter(IDH_LVSBTH._BatchNum + " =\'-999\' ");
            //                    oGridN.doReloadData("", false, true);                                
            //                    oGridN.doApplyRules();
            //                    oGridN.doSetDoCount(true);
            //                    oGridN.setRequiredFilter(sFilter);

            //                    oGridN.AddEditLine = true;
            //                    IDH_LVSAMD oAmendList = new IDH_LVSAMD();
            //                    oAmendList.UnLockTable = true;
            //                    DataRecords oRS = null;
            //                    idh.dbObjects.numbers.NumbersPair oNextNum;
            //                    int iGridRow = 0,iFrqCount=1;
            //                    string sAmedType = "",sFrqType="";

            //                    for (int r = miMinRows; r < lRows; r++) {
            //                        if (r%5==0)
            //                        DataHandler.INSTANCE.doProgress("IDHIMPLSAMD", r, (int)lRows);


            //                        if (Conversions.ToString(oExcel.getValue(r, ColPos.CustomerName)).Trim() == string.Empty
            //                            && Conversions.ToString(oExcel.getValue(r, ColPos.WORowPONumber)).Trim() == string.Empty
            //                            && Conversions.ToString(oExcel.getValue(r, ColPos.PBI)).Trim() == string.Empty) {
            //                            continue;
            //                        }

            //                        oGridN.setCurrentDataRowIndex(oGridN.getRowCount()-1);

            //                        oNextNum = oGridN.DBObject.getNewKey();
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._Code, oNextNum.CodeCode);
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._Name, oNextNum.NameCode);

            //                        oGridN.doSetFieldValue(IDH_LVSBTH._WORow, Conversions.ToString(oExcel.getValue(r, ColPos.WORowPONumber)));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._PBI, Conversions.ToString(oExcel.getValue(r, ColPos.PBI)));
            //                        sAmedType = "";
            //                        sAmedType = Conversions.ToString(oExcel.getValue(r, ColPos.AmendmentType)).Trim();
            //                        //if (sAmedType != string.Empty) {
            //                        //    if (oAmendList.getData(IDH_LVSAMD._AmdDesc + "=\'" + sAmedType + "\' And " + IDH_LVSAMD._Active + "=\'Y\' ", "") > 0)
            //                        //        sAmedType = oAmendList.Code;
            //                        //}
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._AmndTp, sAmedType);

            //                        string sName = Conversions.ToString(oExcel.getValue(r, ColPos.CustomerName)).Trim();
            //                        string sCode = "";
            //                        if (sName != string.Empty) {
            //                            oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGetCustomerCodeByName" + sName, "Select CardCode from OCRD with(nolock) Where CardName=\'" + sName + "\'");
            //                            if (oRS != null && oRS.RecordCount > 0) {
            //                                sCode = oRS.getValueAsString("CardCode").Trim();
            //                            }
            //                        }
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._CustCd, sCode);
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._CustNm, sName);

            //                        oGridN.doSetFieldValue(IDH_LVSBTH._CustAdr, Conversions.ToString(oExcel.getValue(r, ColPos.CustomerSiteAddress)));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._CusPCode, Conversions.ToString(oExcel.getValue(r, ColPos.PostCode)));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._JbTypeDs, Conversions.ToString(oExcel.getValue(r, ColPos.OrderType)));
            //                        // IDH_JOBTYPE_SEQ objJobTyp = new IDH_JOBTYPE_SEQ();
            //                        //objJobTyp.getData()


            //                        sName = Conversions.ToString(oExcel.getValue(r, ColPos.SupplierName)).Trim();
            //                        sCode = "";
            //                        if (sName != string.Empty) {
            //                            oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGetCustomerCodeByName" + sName, "Select CardCode from OCRD with(nolock) Where CardName=\'" + sName + "\'");
            //                            if (oRS != null && oRS.RecordCount > 0) {
            //                                sCode = oRS.getValueAsString("CardCode").Trim();
            //                            }
            //                        }
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._SuppCd, sCode);
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._SuppNm, sName);

            //                        oGridN.doSetFieldValue(IDH_LVSBTH._SuppRef, Conversions.ToString(oExcel.getValue(r, ColPos.SupplierReference)));

            //                        sName = Conversions.ToString(oExcel.getValue(r, ColPos.Container)).Trim();
            //                        sCode = "";
            //                        if (sName != string.Empty) {
            //                            oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGetItemCodeByName" + sName, "Select ItemCode from OITM with(nolock) Where ItemName=\'" + sName + "\'");
            //                            if (oRS != null && oRS.RecordCount > 0) {
            //                                sCode = oRS.getValueAsString("ItemCode").Trim();
            //                            }
            //                        }
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._ItemCd, sCode);
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._ItemNm, sName);
            //                       // Config.INSTANCE.getValueFromOITMWithNoBuffer

            //                        sName = Conversions.ToString(oExcel.getValue(r, ColPos.WasteMaterial)).Trim();
            //                        sCode = "";
            //                        if (sName != string.Empty) {
            //                            oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGetItemCodeByName" + sName, "Select ItemCode from OITM with(nolock) Where ItemName=\'" + sName + "\'");
            //                            if (oRS != null && oRS.RecordCount > 0) {
            //                                sCode = oRS.getValueAsString("ItemCode").Trim();
            //                            }
            //                        }
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._WastCd, sCode);
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._WasteNm, sName);

            //                        oGridN.doSetFieldValue(IDH_LVSBTH._UOM, Conversions.ToString(oExcel.getValue(r, ColPos.UnitofMeasure)));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._Qty, Conversions.ToFloat(oExcel.getValue(r, ColPos.Qty)));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._LPW, Conversions.ToFloat(oExcel.getValue(r, ColPos.LPW)));
            //                        sFrqType = Conversions.ToString(oExcel.getValue(r, ColPos.FrqType));
            //                        if (sFrqType == null || sFrqType == string.Empty)
            //                            sFrqType = "W";
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._FrqType, sFrqType);
            //                        iFrqCount = Conversions.ToInt(oExcel.getValue(r, ColPos.FrqCount));
            //                        if (iFrqCount <= 0)
            //                            iFrqCount = 1;
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._FrqCount, iFrqCount);
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._AutoComp, Conversions.ToString(oExcel.getValue(r, ColPos.AutoComplete)));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._Rebate, Conversions.ToString(oExcel.getValue(r, ColPos.Rebate)));

            //                        oGridN.doSetFieldValue(IDH_LVSBTH._Mon, Conversions.ToString(oExcel.getValue(r, ColPos.Mon)));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._Tue, Conversions.ToString(oExcel.getValue(r, ColPos.Tue)));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._Wed, Conversions.ToString(oExcel.getValue(r, ColPos.Wed)));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._Thu, Conversions.ToString(oExcel.getValue(r, ColPos.Thu)));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._Fri, Conversions.ToString(oExcel.getValue(r, ColPos.Fri)));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._Sat, Conversions.ToString(oExcel.getValue(r, ColPos.Sat)));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._Sun, Conversions.ToString(oExcel.getValue(r, ColPos.Sun)));

            //                        oGridN.doSetFieldValue(IDH_LVSBTH._CMinChg, Conversions.ToFloat(oExcel.getValue(r, ColPos.CIPMinimumCharge)));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._CMCgVol, Conversions.ToFloat(oExcel.getValue(r, ColPos.CIPMinimumChargeUptoVolume)));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._CRminVol, Conversions.ToFloat(oExcel.getValue(r, ColPos.CIPRateAboveMinVolume)));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._CHaulage, Conversions.ToFloat(oExcel.getValue(r, ColPos.CIPHaulageServiceRental)));

            //                        oGridN.doSetFieldValue(IDH_LVSBTH._SMinChg, Conversions.ToFloat(oExcel.getValue(r, ColPos.SIPMinimumCharge)));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._SMCgVol, Conversions.ToFloat(oExcel.getValue(r, ColPos.SIPMinimumChargeUptoVolume)));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._SRminVol, Conversions.ToFloat(oExcel.getValue(r, ColPos.SIPRateAboveMinVolume)));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._SHaulage, Conversions.ToFloat(oExcel.getValue(r, ColPos.SIPHaulageServiceRental)));
            //                        if (doFixStr(oExcel.getValue(r, ColPos.EffectiveDate), 10)!=string.Empty)
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._EffDate, doFixStr(oExcel.getValue(r, ColPos.EffectiveDate), 10));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._Comments, Conversions.ToString(oExcel.getValue(r, ColPos.Comments)));
            //                        //oGridN.doSetFieldValue(IDH_LVSBTH._EffDate, doFixStr(oExcel.getValue(r, ColPos.EffectiveDate), 10));
            //                        //oGridN.doSetFieldValue(IDH_LVSBTH._EffDate, doFixStr(oExcel.getValue(r, ColPos.EffectiveDate), 10));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._WTNFrom, doFixStr(oExcel.getValue(r, ColPos.WTNDateFrom), 10));
            //                        oGridN.doSetFieldValue(IDH_LVSBTH._WTNTo, doFixStr(oExcel.getValue(r, ColPos.WTNDateTo), 10));
            //                        //oGridN.doAddRow();
            //                        bItemsFound = true;

            //                    }
            //                    oGridN.doApplyRules();
            //                    oGridN.doSetDoCount(true);
            //                    oGridN.doRecount();

            //                    DataHandler.INSTANCE.doProgressDone("IDHIMPLSAMD");
            //                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = Translation.getTranslatedWord("Update");
            //                   // doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE);
            //                }// end of col header validation
            //                else {
            //                    //oImportLog.doAddRow(sExcelFile, "WR1:Import Incoming", "Header validation failed", DataHandler.INSTANCE.User, 0, "", 0, "", 0, 0, 0, 0, "N", sBatchName);
            //                    ////oImportLog.doProcessData();
            //                }
            //            }//end if icol>31
            //        }
            //    } catch (Exception e) {
            //        //DataHandler.INSTANCE.doError("Exception: " + e.ToString(), "Error importing the Journals.");
            //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(e.Message, "EREXIMPJ", null);
            //    } finally {
            //        if (oGridN != null) {
            //            oGridN.AddEditLine = false;
            //            DataHandler.INSTANCE.doProgressDone("IDHIMPLSAMD");
            //        }

            //    }

            //    if (!bItemsFound) {
            //        com.idh.bridge.DataHandler.INSTANCE.doResUserError("This is not a valid Import File - " + sExcelFile, "ERUSINIF", new String[] { sExcelFile });
            //    }

            //}

            //public void doReadFileODBC(DBOGrid oGridN, SAPbouiCOM.Form oForm, string sExcelFile,ref string sNewBatch) {
            //    ExcelReader oExcel = new ExcelReader(sExcelFile, 1, Config.INSTANCE.getParameterAsBool("IMPRFC97", false));
            //    if (!oExcel.doReadFile()) {
            //        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error importing the file. Excel Error: " + oExcel.LastError, "ERSYIMPF", new String[] { oExcel.LastError });
            //        return;
            //    }
            //    long lRows = oExcel.Rows;
            //    long lCols = oExcel.Cols;
            //    bool bItemsFound = false;
            //    string sFilter;
            //    //DBOGrid oGridN=null;//= (DBOGrid)getWFValue(oForm, "LINESGRID");
            //    //IDH_LVSBTH oImportLog =oGridN.DBObject ;// new IDH_INCPMLG();
            //    try {
            //        if (lRows >= miMinRows) {
            //            if (lCols > 30) {
            //                if (oExcel.getValue(miMinRows - 1, ColPos.WORowPONumber).Equals(TRIGGERS[1], StringComparison.OrdinalIgnoreCase) &&
            //                        oExcel.getValue(miMinRows - 1, ColPos.PBI).Equals(TRIGGERS[2], StringComparison.OrdinalIgnoreCase) &&
            //                        oExcel.getValue(miMinRows - 1, ColPos.AmendmentType).Equals(TRIGGERS[3], StringComparison.OrdinalIgnoreCase) &&
            //                         oExcel.getValue(miMinRows - 1, ColPos.CustomerName).Equals(TRIGGERS[4], StringComparison.OrdinalIgnoreCase) &&
            //                          oExcel.getValue(miMinRows - 1, ColPos.CustomerSiteAddress).Equals(TRIGGERS[5], StringComparison.OrdinalIgnoreCase) &&
            //                           oExcel.getValue(miMinRows - 1, ColPos.PostCode).Equals(TRIGGERS[6], StringComparison.OrdinalIgnoreCase) &&
            //                            oExcel.getValue(miMinRows - 1, ColPos.OrderType).Equals(TRIGGERS[7], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.SupplierName).Equals(TRIGGERS[8], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.SupplierReference).Equals(TRIGGERS[9], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Container).Equals(TRIGGERS[10], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.WasteMaterial).Equals(TRIGGERS[11], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.UnitofMeasure).Equals(TRIGGERS[12], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Qty).Equals(TRIGGERS[13], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.LPW).Equals(TRIGGERS[14], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Mon).Equals(TRIGGERS[15], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Tue).Equals(TRIGGERS[16], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Wed).Equals(TRIGGERS[17], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Thu).Equals(TRIGGERS[18], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Fri).Equals(TRIGGERS[19], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Sat).Equals(TRIGGERS[20], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Sun).Equals(TRIGGERS[21], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.CIPMinimumCharge).Equals(TRIGGERS[22], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.CIPMinimumChargeUptoVolume).Equals(TRIGGERS[23], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.CIPRateAboveMinVolume).Equals(TRIGGERS[24], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.CIPHaulageServiceRental).Equals(TRIGGERS[25], StringComparison.OrdinalIgnoreCase) &&
            //                              oExcel.getValue(miMinRows - 1, ColPos.SIPMinimumCharge).Equals(TRIGGERS[26], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.SIPMinimumChargeUptoVolume).Equals(TRIGGERS[27], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.SIPRateAboveMinVolume).Equals(TRIGGERS[28], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.SIPHaulageServiceRental).Equals(TRIGGERS[29], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.EffectiveDate).Equals(TRIGGERS[30], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.Comments).Equals(TRIGGERS[31], StringComparison.OrdinalIgnoreCase) &&
            //                             oExcel.getValue(miMinRows - 1, ColPos.WTNDateFrom).Equals(TRIGGERS[32], StringComparison.OrdinalIgnoreCase) &&
            //                        oExcel.getValue(miMinRows - 1, ColPos.WTNDateTo).Equals(TRIGGERS[33], StringComparison.OrdinalIgnoreCase) &&
            //                        oExcel.getValue(miMinRows - 1, ColPos.FrqType).Equals(TRIGGERS[34], StringComparison.OrdinalIgnoreCase) &&
            //                        oExcel.getValue(miMinRows - 1, ColPos.FrqCount).Equals(TRIGGERS[35], StringComparison.OrdinalIgnoreCase) &&
            //                        oExcel.getValue(miMinRows - 1, ColPos.AutoComplete).Equals(TRIGGERS[36], StringComparison.OrdinalIgnoreCase) &&
            //                        oExcel.getValue(miMinRows - 1, ColPos.Rebate).Equals(TRIGGERS[37], StringComparison.OrdinalIgnoreCase)
            //                    ) {

            //                    oGridN.DBObject.doClearBuffers();
            //                    sFilter = oGridN.getRequiredFilter();
            //                    oGridN.setRequiredFilter(IDH_LVSBTH._BatchNum + " =\'-999\' ");
            //                    oGridN.doReloadData("", false, true);
            //                    oGridN.doApplyRules();
            //                    oGridN.doSetDoCount(true);
            //                    oGridN.setRequiredFilter(sFilter);

            //                    oGridN.AddEditLine = true;
            //                    //IDH_LVSAMD oAmendList = new IDH_LVSAMD();
            //                    //oAmendList.UnLockTable = true;
            //                    DataRecords oRS = null;
            //                    IDH_LVSBHH oBatch = new IDH_LVSBHH();
            //                    /////////////////////////////////
            //                    idh.dbObjects.numbers.NumbersPair oNextNum;
            //                    oNextNum = oBatch.getNewKey();
            //                     sNewBatch= oNextNum.CodeCode;
            //                    oBatch.doAddEmptyRow(true);
            //                    oBatch.Code = oNextNum.CodeCode;
            //                    oBatch.Name = oNextNum.NameCode;

            //                    DataHandler.INSTANCE.StartTransaction();
            //                    bool bret = oBatch.doProcessData();
            //                    /////////////////////////////////
            //                    //string sName="",sCode="";
            //                    IDH_LVSBTH oBatchDetail = new IDH_LVSBTH();
            //                    oBatchDetail.UnLockTable = true;                              
            //                    com.idh.bridge.DataHandler.INSTANCE.doInfo("Please wait while system is importing PBI sheet using SSIS Package.");
            //                    System.Data.DataTable dt = doReadExcelFile_ExcelReader(sExcelFile);
            //                    dt.Columns.Add("Code", typeof(string));
            //                    dt.Columns.Add("Name", typeof(string));
            //                    dt.Columns.Add("BatchNumber", typeof(string));

            //                    dt.Columns.Add("ProcSts", typeof(string));
            //                    dt.Columns.Add("VldStatus", typeof(string));
            //                    dt.Columns.Add("BthStatus", typeof(string));


            //                    dt.Columns.Add("CustomerCd", typeof(string));
            //                    dt.Columns.Add("SupplierCd", typeof(string));
            //                    //WasteCd
            //                    dt.Columns.Add("WasteCd", typeof(string));
            //                    //ItemCd
            //                    dt.Columns.Add("ItemCd", typeof(string));

            //                    string sBatchNum = sNewBatch;
            //                    // dt.AsEnumerable().Select(b => b["Code"] = oBatchDetail.getNewKey().CodeCode ).ToList();

            //                    //dt.Rows[r]["SupplierCd"]
            //                    //dt.Rows[r]["WasteCd"] = sCode;
            //                    // dt.Rows[r]["ItemCd"] = sCode;


            //                    //dt.Select().ToList<DataRow>()
            //                    //.ForEach(r => { r["BatchNumber"] = sBatchNum; r["ProcSts"] = "0"; r["VldStatus"] = ""; r["BthStatus"] = "1";
            //                    //    r["Code"] = oBatchDetail.getNewKey().CodeCode; r["Name"] = r["Code"];
            //                    //    r["CustomerCd"] = doGetSBOKey("OCRD", "CardName", "CardCode", r[TRIGGERS[ColPos.CustomerName]].ToString(), oRS);
            //                    //    r["SupplierCd"] = doGetSBOKey("OCRD", "CardName", "CardCode", r[TRIGGERS[ColPos.SupplierName]].ToString(), oRS);
            //                    //    r["ItemCd"] = doGetSBOKey("OITM", "ItemName", "ItemCode", r[TRIGGERS[ColPos.Container]].ToString(), oRS);
            //                    //    r["WasteCd"] = doGetSBOKey("OITM", "ItemName", "ItemCode", r[TRIGGERS[ColPos.WasteMaterial]].ToString(), oRS);
            //                    //});

            //                    dt.Select().ToList<DataRow>()
            //                   .ForEach(r => {
            //                       r["BatchNumber"] = sBatchNum; r["ProcSts"] = "0"; r["VldStatus"] = ""; r["BthStatus"] = "1";
            //                       r["Code"] = oBatchDetail.getNewKey().CodeCode; r["Name"] = r["Code"];                                   
            //                   });

            //                    //dt.ToList<DataRow>()
            //                    //             .ForEach(r => {
            //                    //                 r["BatchNumber"] = sNewBatch;
            //                    //                 r["ProcSts"] = "0";
            //                    //             });
            //                    dt.AcceptChanges();
            //                    StringWriter osw = new StringWriter();
            //                    int iCol = 1;
            //                    for (iCol = 0; iCol < dt.Columns.Count; iCol++) {
            //                        osw.Write(dt.Columns[iCol].ColumnName + "\t");
            //                    }
            //                    int iCRow = 0;
            //                    osw.Write("\n");
            //                    foreach (DataRow row in dt.Rows) {
            //                        IEnumerable<string> fields = row.ItemArray.Select(field =>
            //                          string.Concat("", field.ToString().Replace("\"", "\"\""), ""));
            //                        osw.WriteLine(string.Join("\t", fields));
            //                       DataHandler.INSTANCE.doProgress("IDHIMPLSAMD", iCRow++, dt.Rows.Count);
            //                    }
            //                    using (StreamWriter sw = new StreamWriter(@"F:\\PBIMassUpdater.CSV")) {
            //                        sw.Write(osw.ToString());
            //                        sw.Close();
            //                    }

            //                    osw.Close();
            //                    string sODBC = Config.INSTANCE.getParameter("REPDSN");
            //                    string sODBCUser = Config.INSTANCE.getParameter("REPUSR");
            //                    string sODBCPwd = Config.INSTANCE.getParameter("REPPAS");
            //                    string sTempCSVFIle = @"F:\PBIMassUpdater.CSV";
            //                    Microsoft.SqlServer.Dts.Runtime.Application app = new Microsoft.SqlServer.Dts.Runtime.Application();
            //                    Package package = null;
            //                    string message;
            //                    package = app.LoadPackage(@"F:\Package.dtsx", null);
            //                    package.Variables["User::SqlConString"].Value = "Dsn="+ sODBC +";uid="+ sODBCUser +";pwd="+sODBCPwd+";";
            //                    package.Variables["User::CSVFileName"].Value = sTempCSVFIle;

            //                    Microsoft.SqlServer.Dts.Runtime.DTSExecResult results = package.Execute();
            //                    //Check the results for Failure and Success
            //                    if (results == Microsoft.SqlServer.Dts.Runtime.DTSExecResult.Failure) {
            //                        string err = "";
            //                        foreach (Microsoft.SqlServer.Dts.Runtime.DtsError local_DtsError in package.Errors) {
            //                            string error = local_DtsError.Description.ToString();
            //                            err = err + error;
            //                        }
            //                        if (err != string.Empty) {
            //                            com.idh.bridge.DataHandler.INSTANCE.doUserError("SSIS Package failed: " + err);
            //                            bItemsFound = false;
            //                        }
            //                    }
            //                    if (results == Microsoft.SqlServer.Dts.Runtime.DTSExecResult.Success) {
            //                        bItemsFound = true;
            //                        message = "Package Executed Successfully....";
            //                    }
            //                    System.IO.File.Delete(sTempCSVFIle);
            //                    //dt.DataSet.WriteXml(@"F:\\PBIMassUpdater.XML");
            //                    //dt.WriteXmlSchema(@"F:\\PBIMassUpdater.XML");
            //                    //IDH_LVSBTH.

            //                    //using (System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection("dsn=" + sODBC + ";UID=" + sODBCUser + ";PWD=" + sODBCPwd + ";")) {

            //                    //    using (System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy(cn)) {
            //                    //        bulkCopy.DestinationTableName =
            //                    //            "dbo.[" + IDH_LVSBTH.TableName + "]";

            //                    //        try {
            //                    //            bulkCopy.BatchSize = 500;
            //                    //            //////
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.WORowPONumber], IDH_LVSBTH._WORow);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.PBI], IDH_LVSBTH._PBI);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.AmendmentType], IDH_LVSBTH._AmndTp);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CustomerName], IDH_LVSBTH._CustNm);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CustomerSiteAddress], IDH_LVSBTH._CustAdr);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.PostCode], IDH_LVSBTH._CusPCode);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.OrderType], IDH_LVSBTH._JbTypeDs);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SupplierName], IDH_LVSBTH._SuppNm);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SupplierReference], IDH_LVSBTH._SuppRef);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Container], IDH_LVSBTH._ItemCd);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.WasteMaterial], IDH_LVSBTH._WastCd);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.UnitofMeasure], IDH_LVSBTH._UOM);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Qty], IDH_LVSBTH._Qty);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.LPW], IDH_LVSBTH._LPW);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Mon], IDH_LVSBTH._Mon);

            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Tue], IDH_LVSBTH._Tue);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Wed], IDH_LVSBTH._Wed);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Thu], IDH_LVSBTH._Thu);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Fri], IDH_LVSBTH._Fri);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Sat], IDH_LVSBTH._Sat);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Sun], IDH_LVSBTH._Sun);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CIPMinimumCharge], IDH_LVSBTH._CMinChg);

            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CIPMinimumChargeUptoVolume], IDH_LVSBTH._CMCgVol);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CIPRateAboveMinVolume], IDH_LVSBTH._CRminVol);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CIPHaulageServiceRental], IDH_LVSBTH._CHaulage);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SIPMinimumCharge], IDH_LVSBTH._SMinChg);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SIPMinimumChargeUptoVolume], IDH_LVSBTH._SMCgVol);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SIPRateAboveMinVolume], IDH_LVSBTH._SRminVol);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SIPHaulageServiceRental], IDH_LVSBTH._SHaulage);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.EffectiveDate], IDH_LVSBTH._EffDate);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Comments], IDH_LVSBTH._Comments);

            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.WTNDateFrom], IDH_LVSBTH._WTNFrom);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.WTNDateTo], IDH_LVSBTH._WTNTo);

            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.FrqType], IDH_LVSBTH._FrqType);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.FrqCount], IDH_LVSBTH._FrqCount);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.AutoComplete], IDH_LVSBTH._AutoComp);
            //                    //            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Rebate], IDH_LVSBTH._Rebate);
            //                    //            //////
            //                    //            // Write from the source to the destination.
            //                    //            bulkCopy.WriteToServer(dt);
            //                    //        } catch (Exception ex) {
            //                    //            Console.WriteLine(ex.Message);
            //                    //        }
            //                    //    }
            //                    //}
            //                    //for (int r = 0; r < dt.Rows.Count; r++) {
            //                    //    doReadRow(dt, r, sCode, sName, lRows, oRS, oNextNum, oBatchDetail);
            //                    //}
            //                    DataHandler.INSTANCE.doProgressDone("IDHIMPLSAMD");
            //                    com.idh.bridge.DataHandler.INSTANCE.doInfo("PBI import process completed.");
            //                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
            //                    // doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE);
            //                }// end of col header validation
            //                else {
            //                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Header validation failed");
            //                    return;
            //                    //oImportLog.doAddRow(sExcelFile, "WR1:Import Incoming", "Header validation failed", DataHandler.INSTANCE.User, 0, "", 0, "", 0, 0, 0, 0, "N", sBatchName);
            //                    ////oImportLog.doProcessData();
            //                }
            //                //if (bItemsFound)
            //                //    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = Translation.getTranslatedWord("Update");
            //            }//end if icol>31
            //        }
            //    } catch (Exception e) {
            //        //DataHandler.INSTANCE.doError("Exception: " + e.ToString(), "Error importing the Journals.");
            //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(e.Message, "EREXIMPJ", null);
            //        if (DataHandler.INSTANCE.IsInTransaction())
            //            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
            //    } finally {
            //        if (DataHandler.INSTANCE.IsInTransaction())
            //            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
            //        if (oGridN != null) {
            //            oGridN.AddEditLine = false;
            //            DataHandler.INSTANCE.doProgressDone("IDHIMPLSAMD");
            //        }

            //    }

            //    if (!bItemsFound) {
            //        com.idh.bridge.DataHandler.INSTANCE.doResUserError("This is not a valid Import File - " + sExcelFile, "ERUSINIF", new String[] { sExcelFile });
            //    }

            //}
            public void doReadFileODBC(DBOGrid oGridN, SAPbouiCOM.Form oForm, string sExcelFile, ref string sNewBatch) {

                bool bItemsFound = false;
                //string sTempPBICSVFile = "";
                string sFilter;
                try {
                    miMinRows = Config.INSTANCE.getParameterAsInt("MSPBIUPR", 7, false) + 1;
                    SheetName = Config.INSTANCE.getParameter("MSPBIUPS");

                    System.Data.DataTable dtExcel = doReadExcelFile_ExcelReader(sExcelFile);
                    if (dtExcel == null) {
                        return;
                    }
                    //public static int miMinRows = 8;
                    //public static string SheetName = "Service Amendments";
                    if (true) {
                        if (dtExcel.Columns.Count > 40) {
                            if (dtExcel.Columns[ColPos.WORowPONumber - 1].ColumnName.Equals(TRIGGERS[1], StringComparison.OrdinalIgnoreCase) &&
                                    dtExcel.Columns[ColPos.PBI - 1].ColumnName.Equals(TRIGGERS[2], StringComparison.OrdinalIgnoreCase) &&
                                    dtExcel.Columns[ColPos.AmendmentType - 1].ColumnName.Equals(TRIGGERS[3], StringComparison.OrdinalIgnoreCase) &&
                                     dtExcel.Columns[ColPos.CustomerCode - 1].ColumnName.Equals(TRIGGERS[4], StringComparison.OrdinalIgnoreCase) &&
                                     dtExcel.Columns[ColPos.CustomerName - 1].ColumnName.Equals(TRIGGERS[5], StringComparison.OrdinalIgnoreCase) &&
                                      dtExcel.Columns[ColPos.CustomerSiteAddress - 1].ColumnName.Equals(TRIGGERS[6], StringComparison.OrdinalIgnoreCase) &&
                                       dtExcel.Columns[ColPos.PostCode - 1].ColumnName.Equals(TRIGGERS[7], StringComparison.OrdinalIgnoreCase) &&
                                        dtExcel.Columns[ColPos.OrderType - 1].ColumnName.Equals(TRIGGERS[8], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.SupplierCode - 1].ColumnName.Equals(TRIGGERS[9], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.SupplierName - 1].ColumnName.Equals(TRIGGERS[10], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.SupplierReference - 1].ColumnName.Equals(TRIGGERS[11], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.SupplierAddress - 1].ColumnName.Equals(TRIGGERS[12], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.ContainerCode - 1].ColumnName.Equals(TRIGGERS[14], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.Container - 1].ColumnName.Equals(TRIGGERS[15], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.WasteMaterialCode - 1].ColumnName.Equals(TRIGGERS[16], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.WasteMaterial - 1].ColumnName.Equals(TRIGGERS[17], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.UnitofMeasure - 1].ColumnName.Equals(TRIGGERS[18], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.Qty - 1].ColumnName.Equals(TRIGGERS[19], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.LPW - 1].ColumnName.Equals(TRIGGERS[20], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.Mon - 1].ColumnName.Equals(TRIGGERS[21], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.Tue - 1].ColumnName.Equals(TRIGGERS[22], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.Wed - 1].ColumnName.Equals(TRIGGERS[23], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.Thu - 1].ColumnName.Equals(TRIGGERS[24], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.Fri - 1].ColumnName.Equals(TRIGGERS[25], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.Sat - 1].ColumnName.Equals(TRIGGERS[26], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.Sun - 1].ColumnName.Equals(TRIGGERS[27], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.EffectiveDate - 1].ColumnName.Equals(TRIGGERS[42], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.Comments - 1].ColumnName.Equals(TRIGGERS[43], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.WTNDateFrom - 1].ColumnName.Equals(TRIGGERS[44], StringComparison.OrdinalIgnoreCase) &&
                                    dtExcel.Columns[ColPos.WTNDateTo - 1].ColumnName.Equals(TRIGGERS[45], StringComparison.OrdinalIgnoreCase) &&
                                    dtExcel.Columns[ColPos.FrqType - 1].ColumnName.Equals(TRIGGERS[46], StringComparison.OrdinalIgnoreCase) &&
                                    dtExcel.Columns[ColPos.FrqCount - 1].ColumnName.Equals(TRIGGERS[47], StringComparison.OrdinalIgnoreCase) &&
                                    dtExcel.Columns[ColPos.AutoComplete - 1].ColumnName.Equals(TRIGGERS[48], StringComparison.OrdinalIgnoreCase) &&
                                    dtExcel.Columns[ColPos.Rebate - 1].ColumnName.Equals(TRIGGERS[49], StringComparison.OrdinalIgnoreCase)
                                ) {

                                oGridN.DBObject.doClearBuffers();
                                sFilter = oGridN.getRequiredFilter();
                                oGridN.setRequiredFilter(IDH_LVSBTH._BatchNum + " =\'-999\' ");
                                //oGridN.doReloadData("", false, true);
                                oGridN.setOrderValue("Cast(Code as int)");//#MA  Issue:269 2017216 (Mass PBI Updater - code order on import)
                                oGridN.doReloadData("ASC", false);
                                oGridN.doApplyRules();
                                oGridN.doSetDoCount(true);
                                oGridN.setRequiredFilter(sFilter);
                                oGridN.AddEditLine = true;

                                DataRecords oRS = null;
                                IDH_LVSBHH oBatch = new IDH_LVSBHH();
                                /////////////////////////////////
                                idh.dbObjects.numbers.NumbersPair oNextNum;
                                oNextNum = oBatch.getNewKey();
                                sNewBatch = oNextNum.CodeCode;
                                oBatch.doAddEmptyRow(true);
                                oBatch.Code = oNextNum.CodeCode;
                                oBatch.Name = oNextNum.NameCode;

                                
                                bool bret = oBatch.doProcessData();
                                /////////////////////////////////
                                //string sName="",sCode="";
                                IDH_LVSBTH oBatchDetail = new IDH_LVSBTH();
                                oBatchDetail.UnLockTable = true;
                                com.idh.bridge.DataHandler.INSTANCE.doInfo("Please wait while system is importing PBI sheet using SSIS Package.");
                                System.Data.DataTable dt = doReadExcelFile_ExcelReader(sExcelFile);
                                dt.Columns.Add("Code", typeof(string));
                                dt.Columns.Add("Name", typeof(string));
                                dt.Columns.Add("BatchNumber", typeof(string));

                                dt.Columns.Add("ProcSts", typeof(string));
                                dt.Columns.Add("VldStatus", typeof(string));
                                dt.Columns.Add("BthStatus", typeof(string));

                                if(!dt.Columns.Contains(TRIGGERS[ColPos.RoutableCheckbox]))
                                    dt.Columns.Add(TRIGGERS[ColPos.RoutableCheckbox], typeof(string));

                                if (!dt.Columns.Contains(TRIGGERS[ColPos.RouteCodeMonday]))
                                    dt.Columns.Add(TRIGGERS[ColPos.RouteCodeMonday], typeof(string));

                                if (!dt.Columns.Contains(TRIGGERS[ColPos.RouteCodeTuesday]))
                                    dt.Columns.Add(TRIGGERS[ColPos.RouteCodeTuesday], typeof(string));

                                if (!dt.Columns.Contains(TRIGGERS[ColPos.RouteCodeWednesday]))
                                    dt.Columns.Add(TRIGGERS[ColPos.RouteCodeWednesday], typeof(string));

                                if (!dt.Columns.Contains(TRIGGERS[ColPos.RouteCodeThursday]))
                                    dt.Columns.Add(TRIGGERS[ColPos.RouteCodeThursday], typeof(string));

                                if (!dt.Columns.Contains(TRIGGERS[ColPos.RouteCodeFriday]))
                                    dt.Columns.Add(TRIGGERS[ColPos.RouteCodeFriday], typeof(string));

                                if (!dt.Columns.Contains(TRIGGERS[ColPos.RouteCodeSaturday]))
                                    dt.Columns.Add(TRIGGERS[ColPos.RouteCodeSaturday], typeof(string));

                                if (!dt.Columns.Contains(TRIGGERS[ColPos.RouteCodeSunday]))
                                    dt.Columns.Add(TRIGGERS[ColPos.RouteCodeSunday], typeof(string));

                                if (!dt.Columns.Contains(TRIGGERS[ColPos.RouteSequenceMonday]))
                                    dt.Columns.Add(TRIGGERS[ColPos.RouteSequenceMonday], typeof(int));

                                if (!dt.Columns.Contains(TRIGGERS[ColPos.RouteSequenceTuesday]))
                                    dt.Columns.Add(TRIGGERS[ColPos.RouteSequenceTuesday], typeof(int));

                                if (!dt.Columns.Contains(TRIGGERS[ColPos.RouteSequenceWednesday]))
                                    dt.Columns.Add(TRIGGERS[ColPos.RouteSequenceWednesday], typeof(int));

                                if (!dt.Columns.Contains(TRIGGERS[ColPos.RouteSequenceThursday]))
                                    dt.Columns.Add(TRIGGERS[ColPos.RouteSequenceThursday], typeof(int));

                                if (!dt.Columns.Contains(TRIGGERS[ColPos.RouteSequenceFriday]))
                                    dt.Columns.Add(TRIGGERS[ColPos.RouteSequenceFriday], typeof(int));

                                if (!dt.Columns.Contains(TRIGGERS[ColPos.RouteSequenceSaturday]))
                                    dt.Columns.Add(TRIGGERS[ColPos.RouteSequenceSaturday], typeof(int));

                                if (!dt.Columns.Contains(TRIGGERS[ColPos.RouteSequenceSunday]))
                                    dt.Columns.Add(TRIGGERS[ColPos.RouteSequenceSunday], typeof(int));

                                if (!dt.Columns.Contains(TRIGGERS[ColPos.RouteComments]))
                                    dt.Columns.Add(TRIGGERS[ColPos.RouteComments], typeof(string));

                                if (!dt.Columns.Contains(TRIGGERS[ColPos.DriverComments]))
                                    dt.Columns.Add(TRIGGERS[ColPos.DriverComments], typeof(string));

                                if (!dt.Columns.Contains(TRIGGERS[ColPos.AutoStartDate]))
                                    dt.Columns.Add(TRIGGERS[ColPos.AutoStartDate], typeof(string));

                                //  r[IDH_LVSBTH._AddedBy] = "WR1";
                                if (!dt.Columns.Contains(TRIGGERS[ColPos.Periodtype]))
                                    dt.Columns.Add(TRIGGERS[ColPos.Periodtype], typeof(string));

                                if (!dt.Columns.Contains(TRIGGERS[ColPos.RecurrenceFrequency]))
                                    dt.Columns.Add(TRIGGERS[ColPos.RecurrenceFrequency], typeof(string));


                                if (!dt.Columns.Contains(IDH_LVSBTH._AddedBy))
                                    dt.Columns.Add(IDH_LVSBTH._AddedBy, typeof(string));

                                string sBatchNum = sNewBatch;
                                // dt.AsEnumerable().Select(b => b["Code"] = oBatchDetail.getNewKey().CodeCode ).ToList();
                                //dt.Select().ToList<DataRow>()
                                //.ForEach(r => { r["BatchNumber"] = sBatchNum; r["ProcSts"] = "0"; r["VldStatus"] = ""; r["BthStatus"] = "1";
                                //    r["Code"] = oBatchDetail.getNewKey().CodeCode; r["Name"] = r["Code"];
                                //    r["CustomerCd"] = doGetSBOKey("OCRD", "CardName", "CardCode", r[TRIGGERS[ColPos.CustomerName]].ToString(), oRS);
                                //    r["SupplierCd"] = doGetSBOKey("OCRD", "CardName", "CardCode", r[TRIGGERS[ColPos.SupplierName]].ToString(), oRS);
                                //    r["ItemCd"] = doGetSBOKey("OITM", "ItemName", "ItemCode", r[TRIGGERS[ColPos.Container]].ToString(), oRS);
                                //    r["WasteCd"] = doGetSBOKey("OITM", "ItemName", "ItemCode", r[TRIGGERS[ColPos.WasteMaterial]].ToString(), oRS);
                                //});
                                RegexOptions options = RegexOptions.None;
                                Regex regex = new Regex("[ ]{2,}", options);

                                double dbTemp = 0;
                                dt.Select().ToList<DataRow>()
                               .ForEach(r => {
                                   r["BatchNumber"] = sBatchNum; r["ProcSts"] = "0"; r["VldStatus"] = ""; r["BthStatus"] = "1";
                                   r["Code"] = oBatchDetail.getNewKey().CodeCode; r["Name"] = r["Code"];
                                   //r["Customer-Code"] = doGetSBOKey("OCRD", new string[] { "CardName", "CardType" }, "CardCode", new string[] { r[TRIGGERS[ColPos.CustomerName]].ToString(), "C" }, oRS);
                                   //r["Supplier Code"] = doGetSBOKey("OCRD", new string[] { "CardName", "CardType" }, "CardCode", new string[] { r[TRIGGERS[ColPos.SupplierName]].ToString(), "S"}, oRS);
                                   //r["Container-Code"] = doGetSBOKey("OITM", new string[] { "ItemName" }, "ItemCode", new string[] { r[TRIGGERS[ColPos.Container]].ToString() }, oRS);
                                   //r["Waste Material-Code"] = doGetSBOKey("OITM", new string[] { "ItemName" }, "ItemCode", new string[] { r[TRIGGERS[ColPos.WasteMaterial]].ToString() }, oRS);
                                   
                                   if (r[TRIGGERS[ColPos.Qty]] == null ||  r[TRIGGERS[ColPos.Qty]].ToString().Trim() == "" || !double.TryParse(r[TRIGGERS[ColPos.Qty]].ToString().Trim(), out dbTemp)) {
                                       r[TRIGGERS[ColPos.Qty]] = 0;
                                   }
                                   if (r[TRIGGERS[ColPos.FrqType]] == null || r[TRIGGERS[ColPos.FrqType]].ToString().Trim() == string.Empty) {
                                       r[TRIGGERS[ColPos.FrqType]] = "W";
                                   }
                                   if (r[TRIGGERS[ColPos.FrqCount]] == null || r[TRIGGERS[ColPos.FrqCount]].ToString().Trim() == string.Empty || !double.TryParse(r[TRIGGERS[ColPos.FrqCount]].ToString().Trim(), out dbTemp)) {
                                       r[TRIGGERS[ColPos.FrqCount]] = "0";
                                   }
                                   if (r[TRIGGERS[ColPos.EffectiveDate]] != null && r[TRIGGERS[ColPos.EffectiveDate]].ToString().Length > 0)
                                       r[TRIGGERS[ColPos.EffectiveDate]] = Convert.ToDateTime(DateTime.Today.Year.ToString().Substring(0, 2) + r[TRIGGERS[ColPos.EffectiveDate]].ToString().Substring(6, 2)
                                       + "-" + r[TRIGGERS[ColPos.EffectiveDate]].ToString().Substring(3, 2) + "-" + r[TRIGGERS[ColPos.EffectiveDate]].ToString().Substring(0, 2));

                                   if (r[TRIGGERS[ColPos.WTNDateFrom]] != null && r[TRIGGERS[ColPos.WTNDateFrom]].ToString().Length > 0)
                                       r[TRIGGERS[ColPos.WTNDateFrom]] = Convert.ToDateTime(DateTime.Today.Year.ToString().Substring(0, 2) + r[TRIGGERS[ColPos.WTNDateFrom]].ToString().Substring(6, 2)
                                       + "-" + r[TRIGGERS[ColPos.WTNDateFrom]].ToString().Substring(3, 2) + "-" + r[TRIGGERS[ColPos.WTNDateFrom]].ToString().Substring(0, 2));

                                   if (r[TRIGGERS[ColPos.WTNDateTo]] != null && r[TRIGGERS[ColPos.WTNDateTo]].ToString().Length > 0)
                                       r[TRIGGERS[ColPos.WTNDateTo]] = Convert.ToDateTime(DateTime.Today.Year.ToString().Substring(0, 2) + r[TRIGGERS[ColPos.WTNDateTo]].ToString().Substring(6, 2)
                                       + "-" + r[TRIGGERS[ColPos.WTNDateTo]].ToString().Substring(3, 2) + "-" + r[TRIGGERS[ColPos.WTNDateTo]].ToString().Substring(0, 2));

                                   if (r[TRIGGERS[ColPos.RouteSequenceMonday]] != null && r[TRIGGERS[ColPos.RouteSequenceMonday]].ToString() != string.Empty) {
                                       if (r[TRIGGERS[ColPos.RouteSequenceMonday]].ToString().Trim() == string.Empty || !double.TryParse(r[TRIGGERS[ColPos.RouteSequenceMonday]].ToString().Trim(), out dbTemp))
                                           r[TRIGGERS[ColPos.RouteSequenceMonday]] = null;

                                       else r[TRIGGERS[ColPos.RouteSequenceMonday]] = Convert.ToInt32(r[TRIGGERS[ColPos.RouteSequenceMonday]]);
                                   }

                                   if (r[TRIGGERS[ColPos.RouteSequenceTuesday]] != null && r[TRIGGERS[ColPos.RouteSequenceTuesday]].ToString() != string.Empty) {
                                       if (r[TRIGGERS[ColPos.RouteSequenceTuesday]].ToString().Trim() == string.Empty || !double.TryParse(r[TRIGGERS[ColPos.RouteSequenceTuesday]].ToString().Trim(), out dbTemp))
                                           r[TRIGGERS[ColPos.RouteSequenceTuesday]] = null;

                                       else r[TRIGGERS[ColPos.RouteSequenceTuesday]] = Convert.ToInt32(r[TRIGGERS[ColPos.RouteSequenceTuesday]]);
                                   }


                                   if (r[TRIGGERS[ColPos.RouteSequenceWednesday]] != null && r[TRIGGERS[ColPos.RouteSequenceWednesday]].ToString() != string.Empty) {
                                       if (r[TRIGGERS[ColPos.RouteSequenceWednesday]].ToString().Trim() == string.Empty || !double.TryParse(r[TRIGGERS[ColPos.RouteSequenceWednesday]].ToString().Trim(), out dbTemp))
                                           r[TRIGGERS[ColPos.RouteSequenceWednesday]] = null;

                                       else r[TRIGGERS[ColPos.RouteSequenceWednesday]] = Convert.ToInt32(r[TRIGGERS[ColPos.RouteSequenceWednesday]]);
                                   }

                                   if (r[TRIGGERS[ColPos.RouteSequenceThursday]] != null && r[TRIGGERS[ColPos.RouteSequenceThursday]].ToString() != string.Empty) {
                                       if (r[TRIGGERS[ColPos.RouteSequenceThursday]].ToString().Trim() == string.Empty || !double.TryParse(r[TRIGGERS[ColPos.RouteSequenceThursday]].ToString().Trim(), out dbTemp))
                                           r[TRIGGERS[ColPos.RouteSequenceThursday]] = null;

                                       else r[TRIGGERS[ColPos.RouteSequenceThursday]] = Convert.ToInt32(r[TRIGGERS[ColPos.RouteSequenceThursday]]);
                                   }

                                   if (r[TRIGGERS[ColPos.RouteSequenceFriday]] != null && r[TRIGGERS[ColPos.RouteSequenceFriday]].ToString() != string.Empty) {
                                       if (r[TRIGGERS[ColPos.RouteSequenceFriday]].ToString().Trim() == string.Empty || !double.TryParse(r[TRIGGERS[ColPos.RouteSequenceFriday]].ToString().Trim(), out dbTemp))
                                           r[TRIGGERS[ColPos.RouteSequenceFriday]] = null;

                                       else r[TRIGGERS[ColPos.RouteSequenceFriday]] = Convert.ToInt32(r[TRIGGERS[ColPos.RouteSequenceFriday]]);
                                   }

                                   if (r[TRIGGERS[ColPos.RouteSequenceSaturday]] != null && r[TRIGGERS[ColPos.RouteSequenceSaturday]].ToString() != string.Empty) {
                                       if (r[TRIGGERS[ColPos.RouteSequenceSaturday]].ToString().Trim() == string.Empty || !double.TryParse(r[TRIGGERS[ColPos.RouteSequenceSaturday]].ToString().Trim(), out dbTemp))
                                           r[TRIGGERS[ColPos.RouteSequenceSaturday]] = null;

                                       else r[TRIGGERS[ColPos.RouteSequenceSaturday]] = Convert.ToInt32(r[TRIGGERS[ColPos.RouteSequenceSaturday]]);
                                   }

                                   if (r[TRIGGERS[ColPos.RouteSequenceSunday]] != null && r[TRIGGERS[ColPos.RouteSequenceSunday]].ToString() != string.Empty) {
                                       if (r[TRIGGERS[ColPos.RouteSequenceSunday]].ToString().Trim() == string.Empty || !double.TryParse(r[TRIGGERS[ColPos.RouteSequenceSunday]].ToString().Trim(), out dbTemp))
                                           r[TRIGGERS[ColPos.RouteSequenceSunday]] = null;

                                       else r[TRIGGERS[ColPos.RouteSequenceSunday]] = Convert.ToInt32(r[TRIGGERS[ColPos.RouteSequenceSunday]]);
                                   }
                                   ///////////
                                   if (r[TRIGGERS[ColPos.SIPHaulageServiceRental]] != null && r[TRIGGERS[ColPos.SIPHaulageServiceRental]].ToString() != string.Empty) {
                                       if (r[TRIGGERS[ColPos.SIPHaulageServiceRental]].ToString().Trim() == string.Empty || !double.TryParse(r[TRIGGERS[ColPos.SIPHaulageServiceRental]].ToString().Trim(), out dbTemp))
                                           r[TRIGGERS[ColPos.SIPHaulageServiceRental]] = null;

                                       else r[TRIGGERS[ColPos.SIPHaulageServiceRental]] = Convert.ToDecimal(r[TRIGGERS[ColPos.SIPHaulageServiceRental]]);
                                   }

                                   if (r[TRIGGERS[ColPos.SIPRateAboveMinVolume]] != null && r[TRIGGERS[ColPos.SIPRateAboveMinVolume]].ToString() != string.Empty) {
                                       if (r[TRIGGERS[ColPos.SIPRateAboveMinVolume]].ToString().Trim() == string.Empty || !double.TryParse(r[TRIGGERS[ColPos.SIPRateAboveMinVolume]].ToString().Trim(), out dbTemp))
                                           r[TRIGGERS[ColPos.SIPRateAboveMinVolume]] = null;

                                       else r[TRIGGERS[ColPos.SIPRateAboveMinVolume]] = Convert.ToDecimal(r[TRIGGERS[ColPos.SIPRateAboveMinVolume]]);
                                   }

                                   if (r[TRIGGERS[ColPos.SIPMinimumChargeUptoVolume]] != null && r[TRIGGERS[ColPos.SIPMinimumChargeUptoVolume]].ToString() != string.Empty) {
                                       if (r[TRIGGERS[ColPos.SIPMinimumChargeUptoVolume]].ToString().Trim() == string.Empty || !double.TryParse(r[TRIGGERS[ColPos.SIPMinimumChargeUptoVolume]].ToString().Trim(), out dbTemp))
                                           r[TRIGGERS[ColPos.SIPMinimumChargeUptoVolume]] = null;

                                       else r[TRIGGERS[ColPos.SIPMinimumChargeUptoVolume]] = Convert.ToDecimal(r[TRIGGERS[ColPos.SIPMinimumChargeUptoVolume]]);
                                   }

                                   //if (r[TRIGGERS[ColPos.SIPHaulageServiceRental]] != null && r[TRIGGERS[ColPos.SIPMinCharge]].ToString() != string.Empty) {
                                   //    if (r[TRIGGERS[ColPos.SIPMinCharge]].ToString().Trim() == string.Empty || !double.TryParse(r[TRIGGERS[ColPos.SIPMinCharge]].ToString().Trim(), out dbTemp))
                                   //        r[TRIGGERS[ColPos.SIPMinCharge]] = null;

                                   //    else r[TRIGGERS[ColPos.SIPMinCharge]] = Convert.ToDecimal(r[TRIGGERS[ColPos.SIPMinCharge]]);
                                   //}


                                   //////////

                                   if (r[TRIGGERS[ColPos.CIPHaulageServiceRental]] != null && r[TRIGGERS[ColPos.CIPHaulageServiceRental]].ToString() != string.Empty) {
                                       if (r[TRIGGERS[ColPos.CIPHaulageServiceRental]].ToString().Trim() == string.Empty || !double.TryParse(r[TRIGGERS[ColPos.CIPHaulageServiceRental]].ToString().Trim(), out dbTemp))
                                           r[TRIGGERS[ColPos.CIPHaulageServiceRental]] = null;

                                       else r[TRIGGERS[ColPos.CIPHaulageServiceRental]] = Convert.ToDecimal(r[TRIGGERS[ColPos.CIPHaulageServiceRental]]);
                                   }

                                   if (r[TRIGGERS[ColPos.CIPRateAboveMinVolume]] != null && r[TRIGGERS[ColPos.CIPRateAboveMinVolume]].ToString() != string.Empty) {
                                       if (r[TRIGGERS[ColPos.CIPRateAboveMinVolume]].ToString().Trim() == string.Empty || !double.TryParse(r[TRIGGERS[ColPos.CIPRateAboveMinVolume]].ToString().Trim(), out dbTemp))
                                           r[TRIGGERS[ColPos.CIPRateAboveMinVolume]] = null;

                                       else r[TRIGGERS[ColPos.CIPRateAboveMinVolume]] = Convert.ToDecimal(r[TRIGGERS[ColPos.CIPRateAboveMinVolume]]);
                                   }

                                   if (r[TRIGGERS[ColPos.CIPMinimumChargeUptoVolume]] != null && r[TRIGGERS[ColPos.CIPMinimumChargeUptoVolume]].ToString() != string.Empty) {
                                       if (r[TRIGGERS[ColPos.CIPMinimumChargeUptoVolume]].ToString().Trim() == string.Empty || !double.TryParse(r[TRIGGERS[ColPos.CIPMinimumChargeUptoVolume]].ToString().Trim(), out dbTemp))
                                           r[TRIGGERS[ColPos.CIPMinimumChargeUptoVolume]] = null;

                                       else r[TRIGGERS[ColPos.CIPMinimumChargeUptoVolume]] = Convert.ToDecimal(r[TRIGGERS[ColPos.CIPMinimumChargeUptoVolume]]);
                                   }

                                   //if (r[TRIGGERS[ColPos.CIPHaulageServiceRental]] != null && r[TRIGGERS[ColPos.CIPHaulageServiceRental]].ToString() != string.Empty) {
                                   //    if (r[TRIGGERS[ColPos.CIPHaulageServiceRental]].ToString().Trim() == string.Empty || !double.TryParse(r[TRIGGERS[ColPos.CIPHaulageServiceRental]].ToString().Trim(), out dbTemp))
                                   //        r[TRIGGERS[ColPos.CIPHaulageServiceRental]] = null;

                                   //    else r[TRIGGERS[ColPos.CIPHaulageServiceRental]] = Convert.ToDecimal(r[TRIGGERS[ColPos.CIPHaulageServiceRental]]);
                                   //}


                                   if (r[TRIGGERS[ColPos.RouteComments]] != null && r[TRIGGERS[ColPos.RouteComments]].ToString() != string.Empty) {
                                       r[TRIGGERS[ColPos.RouteComments]] = r[TRIGGERS[ColPos.RouteComments]].ToString().Trim();
                                       r[TRIGGERS[ColPos.RouteComments]] = regex.Replace(r[TRIGGERS[ColPos.RouteComments]].ToString(), " ");
                                   }
                                   if (r[TRIGGERS[ColPos.DriverComments]] != null && r[TRIGGERS[ColPos.DriverComments]].ToString() != string.Empty) {
                                       r[TRIGGERS[ColPos.DriverComments]] = r[TRIGGERS[ColPos.DriverComments]].ToString().Trim();//.Replace("  ", "");
                                       r[TRIGGERS[ColPos.DriverComments]] = regex.Replace(r[TRIGGERS[ColPos.DriverComments]].ToString(), " ");
                                   }
                                   if (r[TRIGGERS[ColPos.AutoStartDate]] != null && r[TRIGGERS[ColPos.AutoStartDate]].ToString() != string.Empty) {
                                       r[TRIGGERS[ColPos.AutoStartDate]] = r[TRIGGERS[ColPos.AutoStartDate]].ToString().Trim().Replace(" ", "");
                                      
                                   }

                                   r[IDH_LVSBTH._AddedBy] = "WR1";
                               });

                                ////dt.ToList<DataRow>()
                                ////             .ForEach(r => {
                                ////                 r["BatchNumber"] = sNewBatch;
                                ////                 r["ProcSts"] = "0";
                                ////             });

                                dt.AcceptChanges();
                                //StringWriter osw = new StringWriter();
                                //int iCol = 1;
                                //for (iCol = 0; iCol < dt.Columns.Count; iCol++) {
                                //    osw.Write(dt.Columns[iCol].ColumnName + "\t");
                                //}
                                //int iCRow = 0;
                                //osw.Write("\n");
                                //foreach (DataRow row in dt.Rows) {
                                //    IEnumerable<string> fields = row.ItemArray.Select(field =>
                                //      string.Concat("", field.ToString().Replace("\"", "\"\""), ""));
                                //    osw.WriteLine(string.Join("\t", fields));
                                //    DataHandler.INSTANCE.doProgress("IDHIMPLSAMD", iCRow++, dt.Rows.Count);
                                //}
                                //com.idh.bridge.DataHandler.INSTANCE.doInfo("Files has been copied to Datatable.");

                                //string sAssemblyPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\";

                                //if (Config.INSTANCE.getParameter("IMPDIRLS").Trim() != string.Empty) {
                                //    sTempPBICSVFile = Config.INSTANCE.getParameter("IMPDIRLS").Trim();
                                //    if (!sTempPBICSVFile.EndsWith("\\")) {
                                //        sTempPBICSVFile += "\\";
                                //    }
                                //    sTempPBICSVFile += "PBIMassUpdater" + DateTime.Now.ToString("ddMMyyhhmmss") + ".CSV";
                                //} else {
                                //    sTempPBICSVFile = sAssemblyPath;

                                //    sTempPBICSVFile += "PBIMassUpdater" + DateTime.Now.ToString("ddMMyyhhmmss") + ".CSV";
                                //}
                                //using (StreamWriter sw = new StreamWriter(@sTempPBICSVFile)) {
                                //    sw.Write(osw.ToString());
                                //    sw.Close();
                                //}

                                //osw.Close();
                                //com.idh.bridge.DataHandler.INSTANCE.doInfo("Files has been converted to csv named "+ sTempPBICSVFile);

                                string sODBC = Config.INSTANCE.getParameter("REPDSN");
                                string sODBCUser = Config.INSTANCE.getParameter("REPUSR");
                                string sODBCPwd = Config.INSTANCE.getParameter("REPPAS");

                                //Microsoft.SqlServer.Dts.Runtime.Application app = new Microsoft.SqlServer.Dts.Runtime.Application();
                                //Package package = null;
                                //string message;
                                //com.idh.bridge.DataHandler.INSTANCE.doInfo("Dts Package name is  " + sAssemblyPath + "PBIBatchUpdaterPackage.dtsx");

                                //package = app.LoadPackage(sAssemblyPath + "PBIBatchUpdaterPackage.dtsx", null);
                                //package.Variables["User::SqlConString"].Value = "Dsn=" + sODBC + ";uid=" + sODBCUser + ";pwd=" + sODBCPwd + ";";
                                //package.Variables["User::CSVFileName"].Value = sTempPBICSVFile;
                                //com.idh.bridge.DataHandler.INSTANCE.doInfo("DTsx packages variable has been set");

                                //Microsoft.SqlServer.Dts.Runtime.DTSExecResult results = package.Execute();
                                ////Check the results for Failure and Success
                                //if (results == Microsoft.SqlServer.Dts.Runtime.DTSExecResult.Failure) {
                                //    string err = "";
                                //    foreach (Microsoft.SqlServer.Dts.Runtime.DtsError local_DtsError in package.Errors) {
                                //        string error = local_DtsError.Description.ToString();
                                //        err = err + error;
                                //    }
                                //    if (err != string.Empty) {
                                //        com.idh.bridge.DataHandler.INSTANCE.doUserError("SSIS Package failed: " + err);
                                //        bItemsFound = false;
                                //    }
                                //}
                                //if (results == Microsoft.SqlServer.Dts.Runtime.DTSExecResult.Success) {
                                //    bItemsFound = true;
                                //    message = "Package Executed Successfully....";
                                //}

                                //IDH_LVSBTH.
                                DataHandler.INSTANCE.StartTransaction();
                                System.Data.SqlClient.SqlConnection cn1 = new System.Data.SqlClient.SqlConnection("Data Source=" + DataHandler.INSTANCE.SBOCompany.Server + ";Initial Catalog=" + DataHandler.INSTANCE.SBOCompany.CompanyDB + ";User ID=" + sODBCUser + ";Password=" + sODBCPwd + ";");
                                using (System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection("Data Source=" + DataHandler.INSTANCE.SBOCompany.Server + ";Initial Catalog=" + DataHandler.INSTANCE.SBOCompany.CompanyDB + ";User ID=" + sODBCUser + ";Password=" + sODBCPwd + ";")) {
                                        cn.Open();
                                    using (System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy(cn)) {
                                        bulkCopy.DestinationTableName =
                                            "dbo.[" + IDH_LVSBTH.TableName + "]";

                                        try {
                                            bulkCopy.BatchSize = 500;
                                            //////
                                            bulkCopy.ColumnMappings.Add("Code", IDH_LVSBTH._Code);
                                            bulkCopy.ColumnMappings.Add("Name", IDH_LVSBTH._Name);

                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.WORowPONumber], IDH_LVSBTH._WORow);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.PBI], IDH_LVSBTH._PBI);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.AmendmentType], IDH_LVSBTH._AmndTp);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CustomerCode], IDH_LVSBTH._CustCd);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CustomerName], IDH_LVSBTH._CustNm);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CustomerSiteAddress], IDH_LVSBTH._CustAdr);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.PostCode], IDH_LVSBTH._CusPCode);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.OrderType], IDH_LVSBTH._JbTypeDs);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SupplierCode], IDH_LVSBTH._SuppCd);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SupplierName], IDH_LVSBTH._SuppNm);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SupplierReference], IDH_LVSBTH._SuppRef);

                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SupplierAddress], IDH_LVSBTH._SupAddr);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SupplierPostCode], IDH_LVSBTH._SupPCode);


                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.ContainerCode], IDH_LVSBTH._ItemCd);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Container], IDH_LVSBTH._ItemNm);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.WasteMaterialCode], IDH_LVSBTH._WastCd);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.WasteMaterial], IDH_LVSBTH._WasteNm);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.UnitofMeasure], IDH_LVSBTH._UOM);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Qty], IDH_LVSBTH._Qty);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.LPW], IDH_LVSBTH._LPW);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Mon], IDH_LVSBTH._Mon);

                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Tue], IDH_LVSBTH._Tue);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Wed], IDH_LVSBTH._Wed);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Thu], IDH_LVSBTH._Thu);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Fri], IDH_LVSBTH._Fri);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Sat], IDH_LVSBTH._Sat);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Sun], IDH_LVSBTH._Sun);

                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SIPMinSiteVisit], IDH_LVSBTH._SMnSVst);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SIPMinCharge], IDH_LVSBTH._SMnChg);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SIPVariableOrFixedQTY], IDH_LVSBTH._SVarRFix);


                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SIPMinimumCharge], IDH_LVSBTH._SMinChg);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SIPMinimumChargeUptoVolume], IDH_LVSBTH._SMCgVol);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SIPRateAboveMinVolume], IDH_LVSBTH._SRminVol);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SIPHaulageServiceRental], IDH_LVSBTH._SHaulage);

                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CIPMinimumCharge], IDH_LVSBTH._CMinChg);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CIPMinimumChargeUptoVolume], IDH_LVSBTH._CMCgVol);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CIPRateAboveMinVolume], IDH_LVSBTH._CRminVol);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CIPHaulageServiceRental], IDH_LVSBTH._CHaulage);

                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CIPMinSiteVisit], IDH_LVSBTH._CMnSVst);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CIPMinCharge], IDH_LVSBTH._CMnChg);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CIPVariableOrFixedQTY], IDH_LVSBTH._CVarRFix);


                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.EffectiveDate], IDH_LVSBTH._EffDate);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Comments], IDH_LVSBTH._Comments);

                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.WTNDateFrom], IDH_LVSBTH._WTNFrom);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.WTNDateTo], IDH_LVSBTH._WTNTo);

                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.FrqType], IDH_LVSBTH._FrqType);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.FrqCount], IDH_LVSBTH._FrqCount);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.AutoComplete], IDH_LVSBTH._AutoComp);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Rebate], IDH_LVSBTH._Rebate);

                                            bulkCopy.ColumnMappings.Add("VldStatus", IDH_LVSBTH._VldStatus);
                                            bulkCopy.ColumnMappings.Add("BthStatus", IDH_LVSBTH._BthStatus);
                                            bulkCopy.ColumnMappings.Add("ProcSts", IDH_LVSBTH._ProcSts);
                                            bulkCopy.ColumnMappings.Add("BatchNumber", IDH_LVSBTH._BatchNum);

                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RoutableCheckbox], IDH_LVSBTH._IDHRT);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RouteCodeMonday], IDH_LVSBTH._IDHRCDM);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RouteCodeTuesday], IDH_LVSBTH._IDHRCDTU);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RouteCodeWednesday], IDH_LVSBTH._IDHRCDW);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RouteCodeThursday], IDH_LVSBTH._IDHRCDTH);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RouteCodeFriday], IDH_LVSBTH._IDHRCDF);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RouteCodeSaturday], IDH_LVSBTH._IDHRCDSA);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RouteCodeSunday], IDH_LVSBTH._IDHRCDSU);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RouteSequenceMonday], IDH_LVSBTH._IDHRSQM);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RouteSequenceTuesday], IDH_LVSBTH._IDHRSQTU);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RouteSequenceWednesday], IDH_LVSBTH._IDHRSQW);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RouteSequenceThursday], IDH_LVSBTH._IDHRSQTH);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RouteSequenceFriday], IDH_LVSBTH._IDHRSQF);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RouteSequenceSaturday], IDH_LVSBTH._IDHRSQSA);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RouteSequenceSunday], IDH_LVSBTH._IDHRSQSU);


                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RouteComments], IDH_LVSBTH._IDHCMT);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.DriverComments], IDH_LVSBTH._IDHDRVI);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.AutoStartDate], IDH_LVSBTH._AUTSTRT);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Periodtype], IDH_LVSBTH._PERTYP);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RecurrenceFrequency], IDH_LVSBTH._IDHRECFQ);

                                            bulkCopy.ColumnMappings.Add(IDH_LVSBTH._AddedBy, IDH_LVSBTH._AddedBy);

                                            //////
                                            // Write from the source to the destination.
                                            bulkCopy.WriteToServer(dt);
                                            bItemsFound = true;
                                            DataHandler.INSTANCE.doProgressDone("IDHIMPLSAMD");
                                            com.idh.bridge.DataHandler.INSTANCE.doInfo("PBI import process completed.");
                                            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                            com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(Translation.getTranslatedMessage("EXPEXLDN", "Successfully import the file"), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);

                                        } catch (Exception ex) {
                                            bItemsFound = false;
                                            DataHandler.INSTANCE.doResSBOError(ex.Message, "EREXSGEN", new String[] { "Failed Copying Data to SBO Table" });
                                            //Console.WriteLine(ex.Message);
                                        }
                                    }
                                }

                            }// end of col header validation
                            else {
                                com.idh.bridge.DataHandler.INSTANCE.doUserError("Header validation failed");
                                return;
                            }
                        }//end if icol>31
                    }
                } catch (Exception e) {
                    //DataHandler.INSTANCE.doError("Exception: " + e.ToString(), "Error importing the Journals.");
                    DataHandler.INSTANCE.doInfo(e.Message);
                    if (DataHandler.INSTANCE.IsInTransaction())
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    DataHandler.INSTANCE.doResSBOError(e.Message, "EREXSGEN", new String[] { "Importing PBI Update Sheet" });
                    bItemsFound = false;
                } finally {
                    if (DataHandler.INSTANCE.IsInTransaction())
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    if (oGridN != null) {
                        oGridN.AddEditLine = false;
                        DataHandler.INSTANCE.doProgressDone("IDHIMPLSAMD");
                    }
                }
                if (!bItemsFound) {
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("This is not a valid Import File - " + sExcelFile, "ERUSINIF", new String[] { sExcelFile });
                }

            }
            private string doGetSBOKey(string sTableName, string[] sKeyField, string sFiledName, string[] sKeyValue, DataRecords oRS) {
                try {
                    // sName = Conversions.ToString(dt.Rows[r][TRIGGERS[ColPos.CustomerName]]);// oExcel.getValue(r, )).Trim();
                    string sCode = "";
                    string sWhere = "";
                    for (int i = 0; i < sKeyField.Length; i++) {
                        sWhere += " [" + sKeyField[i] + "] = \'" + sKeyValue[i] + "\' And";
                    }
                    //char[] MyChar = { 'A', 'n', 'd'};
                    sWhere = sWhere.Substring(0, sWhere.Length - 3);
                    if (sWhere != string.Empty) {
                        //oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGet" + sTableName + "CodeByName" + sKeyValue, "Select Top 1 " + sFiledName + " from " + sTableName + " with(nolock) Where " + sKeyField + "=\'" + sKeyValue + "\'");
                        oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGet" + sTableName + "CodeByName" + sKeyValue, "Select Top 1 " + sFiledName + " from " + sTableName + " with(nolock) Where " + sWhere);
                        if (oRS != null && oRS.RecordCount > 0) {
                            sCode = oRS.getValueAsString(sFiledName).Trim();
                        }
                    }
                    return sCode;
                    // dt.Rows[r]["CustomerCd"] = sCode;
                } catch (Exception e) {
                    //DataHandler.INSTANCE.doError("Exception: " + e.ToString(), "Error importing the Journals.");
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(e.Message, "EREXIMPJ", new string[] { "doGetSBOKey" });
                }
                return "";
                ////Customer
                //  sName = Conversions.ToString(dt.Rows[r][TRIGGERS[ColPos.CustomerName]]);// oExcel.getValue(r, )).Trim();
                //  sCode = "";
                //  if (sName != string.Empty) {
                //      oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGetCustomerCodeByName" + sName, "Select CardCode from OCRD with(nolock) Where CardName=\'" + sName + "\'");
                //      if (oRS != null && oRS.RecordCount > 0) {
                //          sCode = oRS.getValueAsString("CardCode").Trim();
                //      }
                //  }
                //  dt.Rows[r]["CustomerCd"] = sCode;
                //  //oBatchDetail.U_CustNm = sName;
                //  //Supplier
                //  sName = Conversions.ToString(dt.Rows[r][TRIGGERS[ColPos.SupplierName]]);
                //  sCode = "";
                //  if (sName != string.Empty) {
                //      oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGetCustomerCodeByName" + sName, "Select CardCode from OCRD with(nolock) Where CardName=\'" + sName + "\'");
                //      if (oRS != null && oRS.RecordCount > 0) {
                //          sCode = oRS.getValueAsString("CardCode").Trim();
                //      }
                //  }
                //  //  oBatchDetail.U_SuppCd = sCode;
                //  dt.Rows[r]["SupplierCd"] = sCode; //oBatchDetail.U_SuppNm = sName;
                //  //Container
                //  sName = Conversions.ToString(dt.Rows[r][TRIGGERS[ColPos.Container]]);
                //  sCode = "";
                //  if (sName != string.Empty) {
                //      oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGetItemCodeByName" + sName, "Select ItemCode from OITM with(nolock) Where ItemName=\'" + sName + "\'");
                //      if (oRS != null && oRS.RecordCount > 0) {
                //          sCode = oRS.getValueAsString("ItemCode").Trim();
                //      }
                //  }
                //  dt.Rows[r]["ItemCd"] = sCode;
                // /// oBatchDetail.U_ItemNm = sName;
                //  //Waste Item
                //  sName = sName = Conversions.ToString(dt.Rows[r][TRIGGERS[ColPos.WasteMaterial]]);
                //  sCode = "";
                //  if (sName != string.Empty) {
                //      oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGetItemCodeByName" + sName, "Select ItemCode from OITM with(nolock) Where ItemName=\'" + sName + "\'");
                //      if (oRS != null && oRS.RecordCount > 0) {
                //          sCode = oRS.getValueAsString("ItemCode").Trim();
                //      }
                //  }
                //  dt.Rows[r]["WasteCd"] = sCode;

            }
            //private void doReadRow(ExcelReader oExcel,int r, string sCode, string sName,long lRows, DataRecords oRS, idh.dbObjects.numbers.NumbersPair oNextNum, IDH_LVSBTH oBatchDetail) {
            //    if (r % 5 == 0)
            //        DataHandler.INSTANCE.doProgress("IDHIMPLSAMD", r, (int)lRows);


            //    if (Conversions.ToString(oExcel.getValue(r, ColPos.CustomerName)).Trim() == string.Empty
            //        && Conversions.ToString(oExcel.getValue(r, ColPos.WORowPONumber)).Trim() == string.Empty
            //        && Conversions.ToString(oExcel.getValue(r, ColPos.PBI)).Trim() == string.Empty) {
            //        return;
            //    }
            //    //oBatchDetail.doAddEmptyRow();
            //   // oNextNum = oBatchDetail.getNewKey();


            //    //oBatchDetail.Code = oNextNum.CodeCode;
            //    //oBatchDetail.Name = oNextNum.NameCode;
            //    //oBatchDetail.U_BatchNum = sNewBatchNum;
            //    //oBatchDetail.U_WORow = Conversions.ToString(oExcel.getValue(r, ColPos.WORowPONumber));
            //    //oBatchDetail.U_PBI = Conversions.ToString(oExcel.getValue(r, ColPos.PBI));

            //    //oBatchDetail.U_AmndTp = Conversions.ToString(oExcel.getValue(r, ColPos.AmendmentType)).Trim();

            //    sName = Conversions.ToString(oExcel.getValue(r, ColPos.CustomerName)).Trim();
            //    sCode = "";
            //    if (sName != string.Empty) {
            //        oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGetCustomerCodeByName" + sName, "Select CardCode from OCRD with(nolock) Where CardName=\'" + sName + "\'");
            //        if (oRS != null && oRS.RecordCount > 0) {
            //            sCode = oRS.getValueAsString("CardCode").Trim();
            //        }
            //    }
            //    //oBatchDetail.U_CustCd = sCode;
            //    //oBatchDetail.U_CustNm = sName;

            //    //oBatchDetail.U_CustAdr = Conversions.ToString(oExcel.getValue(r, ColPos.CustomerSiteAddress));
            //    //oBatchDetail.U_CusPCode = Conversions.ToString(oExcel.getValue(r, ColPos.PostCode));
            //    //oBatchDetail.U_JbTypeDs = Conversions.ToString(oExcel.getValue(r, ColPos.OrderType));


            //    sName = Conversions.ToString(oExcel.getValue(r, ColPos.SupplierName)).Trim();
            //    sCode = "";
            //    if (sName != string.Empty) {
            //        oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGetCustomerCodeByName" + sName, "Select CardCode from OCRD with(nolock) Where CardName=\'" + sName + "\'");
            //        if (oRS != null && oRS.RecordCount > 0) {
            //            sCode = oRS.getValueAsString("CardCode").Trim();
            //        }
            //    }
            //    //oBatchDetail.U_SuppCd = sCode;
            //    //oBatchDetail.U_SuppNm = sName;

            //    //oBatchDetail.U_SuppRef = Conversions.ToString(oExcel.getValue(r, ColPos.SupplierReference));

            //    sName = Conversions.ToString(oExcel.getValue(r, ColPos.Container)).Trim();
            //    sCode = "";
            //    if (sName != string.Empty) {
            //        oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGetItemCodeByName" + sName, "Select ItemCode from OITM with(nolock) Where ItemName=\'" + sName + "\'");
            //        if (oRS != null && oRS.RecordCount > 0) {
            //            sCode = oRS.getValueAsString("ItemCode").Trim();
            //        }
            //    }
            //    //oBatchDetail.U_ItemCd = sCode;
            //    //oBatchDetail.U_ItemNm = sName;

            //    sName = Conversions.ToString(oExcel.getValue(r, ColPos.WasteMaterial)).Trim();
            //    sCode = "";
            //    if (sName != string.Empty) {
            //        oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGetItemCodeByName" + sName, "Select ItemCode from OITM with(nolock) Where ItemName=\'" + sName + "\'");
            //        if (oRS != null && oRS.RecordCount > 0) {
            //            sCode = oRS.getValueAsString("ItemCode").Trim();
            //        }
            //    }
            //    //oBatchDetail.U_WastCd = sCode;
            //    //oBatchDetail.U_WasteNm = sName;

            //    //oBatchDetail.U_UOM = Conversions.ToString(oExcel.getValue(r, ColPos.UnitofMeasure));
            //    //oBatchDetail.U_Qty = Conversions.ToFloat(oExcel.getValue(r, ColPos.Qty));
            //    //oBatchDetail.U_LPW = Conversions.ToFloat(oExcel.getValue(r, ColPos.LPW));

            //    //oBatchDetail.U_FrqType = Conversions.ToString(oExcel.getValue(r, ColPos.FrqType));
            //    //oBatchDetail.U_FrqCount = Conversions.ToInt(oExcel.getValue(r, ColPos.FrqCount));
            //    //oBatchDetail.U_AutoComp = Conversions.ToString(oExcel.getValue(r, ColPos.AutoComplete));
            //    //oBatchDetail.U_Rebate = Conversions.ToString(oExcel.getValue(r, ColPos.Rebate));

            //    //oBatchDetail.U_Mon = Conversions.ToString(oExcel.getValue(r, ColPos.Mon));
            //    //oBatchDetail.U_Tue = Conversions.ToString(oExcel.getValue(r, ColPos.Tue));
            //    //oBatchDetail.U_Wed = Conversions.ToString(oExcel.getValue(r, ColPos.Wed));
            //    //oBatchDetail.U_Thu = Conversions.ToString(oExcel.getValue(r, ColPos.Thu));
            //    //oBatchDetail.U_Fri = Conversions.ToString(oExcel.getValue(r, ColPos.Fri));
            //    //oBatchDetail.U_Sat = Conversions.ToString(oExcel.getValue(r, ColPos.Sat));
            //    //oBatchDetail.U_Sun = Conversions.ToString(oExcel.getValue(r, ColPos.Sun));

            //    //oBatchDetail.U_CMinChg = Conversions.ToFloat(oExcel.getValue(r, ColPos.CIPMinimumCharge));
            //    //oBatchDetail.U_CMCgVol = Conversions.ToFloat(oExcel.getValue(r, ColPos.CIPMinimumChargeUptoVolume));
            //    //oBatchDetail.U_CRminVol = Conversions.ToFloat(oExcel.getValue(r, ColPos.CIPRateAboveMinVolume));
            //    //oBatchDetail.U_CHaulage = Conversions.ToFloat(oExcel.getValue(r, ColPos.CIPHaulageServiceRental));

            //    //oBatchDetail.U_SMinChg = Conversions.ToFloat(oExcel.getValue(r, ColPos.SIPMinimumCharge));
            //    //oBatchDetail.U_SMCgVol = Conversions.ToFloat(oExcel.getValue(r, ColPos.SIPMinimumChargeUptoVolume));
            //    //oBatchDetail.U_SRminVol = Conversions.ToFloat(oExcel.getValue(r, ColPos.SIPRateAboveMinVolume));
            //    //oBatchDetail.U_SHaulage = Conversions.ToFloat(oExcel.getValue(r, ColPos.SIPHaulageServiceRental));
            //    //if (doFixStr(oExcel.getValue(r, ColPos.EffectiveDate), 10) != string.Empty)
            //    //    oBatchDetail.U_EffDate = com.idh.utils.Dates.doStrToDate(doFixStr(oExcel.getValue(r, ColPos.EffectiveDate), 10));
            //    //oBatchDetail.U_Comments = Conversions.ToString(oExcel.getValue(r, ColPos.Comments));
            //    //if (doFixStr(oExcel.getValue(r, ColPos.WTNDateFrom), 10) != string.Empty)
            //    //    oBatchDetail.U_WTNFrom = com.idh.utils.Dates.doStrToDate(doFixStr(oExcel.getValue(r, ColPos.WTNDateFrom), 10));
            //    //if (doFixStr(oExcel.getValue(r, ColPos.WTNDateTo), 10) != string.Empty)
            //    //    oBatchDetail.U_WTNTo = com.idh.utils.Dates.doStrToDate(doFixStr(oExcel.getValue(r, ColPos.WTNDateTo), 10));

            //    //oBatchDetail.U_ProcSts = "0";

            //}

            public System.Data.DataTable doReadExcelFile_ExcelReader(string sFileName) {
                try {
                    // int iFirstRow = Config.INSTANCE.getParameterAsInt("MSPBIUPR",7,false);
                    // string sSheetName= Config.INSTANCE.getParameter("MSPBIUPS");

                    FileStream stream = File.Open(sFileName, FileMode.Open, FileAccess.Read);
                    Excel.IExcelDataReader excelReader;
                    System.Data.DataTable dtdataTable = new System.Data.DataTable();
                    ////3. DataSet - The result of each spreadsheet will be created in the result.Tables
                    //DataSet result = excelReader.AsDataSet();
                    if (sFileName.ToLower().EndsWith(".xlsx")) {
                        //97+ .xlslx
                        //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                        excelReader = Excel.ExcelReaderFactory.CreateOpenXmlReader(stream);
                        //4. DataSet - Create column names from first row
                        excelReader.FirstRow = miMinRows - 1;
                        excelReader.SheetNameToRead = SheetName;
                        excelReader.IsFirstRowAsColumnNames = true;
                        //int r = excelReader.cou[;

                        dtdataTable = excelReader.AsDataSet().Tables[0];

                        //dtdataTable.Columns.Add("Code", new Type() { "string" });
                        excelReader.Close();
                    } else {
                        //2003-97
                        //.xls
                        //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                        excelReader = Excel.ExcelReaderFactory.CreateBinaryReader(stream);
                        //4. DataSet - Create column names from first row
                        excelReader.IsFirstRowAsColumnNames = true;
                        excelReader.FirstRow = miMinRows - 1;
                        excelReader.SheetNameToRead = SheetName;
                        dtdataTable = excelReader.AsDataSet().Tables[0];
                        excelReader.Close();
                    }
                    //using (System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy("CONNECTION_STRING")) {
                    //    bulkCopy.DestinationTableName =
                    //        "dbo.[" + sTableName + "]";

                    //    try {
                    //        // Write from the source to the destination.
                    //        bulkCopy.WriteToServer(dsdataSet.Tables[0]);
                    //    } catch (Exception ex) {
                    //        Console.WriteLine(ex.Message);
                    //    }
                    //}
                    return dtdataTable;
                } catch (Exception ex) {
                    DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doReadExcelFile_ExcelReader" });
                    DataHandler.INSTANCE.doUserError(ex.Message, " exception in doReadExcelFile_ExcelReader");
                }

                return null;
            }
            //private void doReadRow(System.Data.DataTable dt, int r, string sCode, string sName, long lRows, DataRecords oRS, idh.dbObjects.numbers.NumbersPair oNextNum, IDH_LVSBTH oBatchDetail) {
            //    if (r % 5 == 0)
            //        DataHandler.INSTANCE.doProgress("IDHIMPLSAMD", r, (int)lRows);


            //    if (Conversions.ToString(dt.Rows[r][ColPos.CustomerName-1]).Trim() == string.Empty
            //        && Conversions.ToString(dt.Rows[r][ColPos.WORowPONumber-1]).Trim() == string.Empty
            //        && Conversions.ToString(dt.Rows[r][ColPos.PBI-1]).Trim() == string.Empty) {
            //        return;
            //    }
            //    //oBatchDetail.doAddEmptyRow();
            //  //  oNextNum = oBatchDetail.getNewKey();


            //    //oBatchDetail.Code = oNextNum.CodeCode;
            //    //oBatchDetail.Name = oNextNum.NameCode;
            //    //oBatchDetail.U_BatchNum = sNewBatchNum;
            //    //oBatchDetail.U_WORow = Conversions.ToString(dt.Rows[r][ ColPos.WORowPONumber));
            //    //oBatchDetail.U_PBI = Conversions.ToString(dt.Rows[r][ ColPos.PBI));

            //    //oBatchDetail.U_AmndTp = Conversions.ToString(dt.Rows[r][ ColPos.AmendmentType)).Trim();

            //    sName = Conversions.ToString(dt.Rows[r][ColPos.CustomerName-1]).Trim();
            //    sCode = "";
            //    if (sName != string.Empty) {
            //        oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGetCustomerCodeByName" + sName, "Select CardCode from OCRD with(nolock) Where CardName=\'" + sName + "\'");
            //        if (oRS != null && oRS.RecordCount > 0) {
            //            sCode = oRS.getValueAsString("CardCode").Trim();
            //        }
            //    }
            //    //oBatchDetail.U_CustCd = sCode;
            //    //oBatchDetail.U_CustNm = sName;

            //    //oBatchDetail.U_CustAdr = Conversions.ToString(dt.Rows[r][ ColPos.CustomerSiteAddress));
            //    //oBatchDetail.U_CusPCode = Conversions.ToString(dt.Rows[r][ ColPos.PostCode));
            //    //oBatchDetail.U_JbTypeDs = Conversions.ToString(dt.Rows[r][ ColPos.OrderType));


            //    sName = Conversions.ToString(dt.Rows[r][ColPos.SupplierName-1]).Trim();
            //    sCode = "";
            //    if (sName != string.Empty) {
            //        oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGetCustomerCodeByName" + sName, "Select CardCode from OCRD with(nolock) Where CardName=\'" + sName + "\'");
            //        if (oRS != null && oRS.RecordCount > 0) {
            //            sCode = oRS.getValueAsString("CardCode").Trim();
            //        }
            //    }
            //    //oBatchDetail.U_SuppCd = sCode;
            //    //oBatchDetail.U_SuppNm = sName;

            //    //oBatchDetail.U_SuppRef = Conversions.ToString(dt.Rows[r][ ColPos.SupplierReference));

            //    sName = Conversions.ToString(dt.Rows[r][ColPos.Container-1]).Trim();
            //    sCode = "";
            //    if (sName != string.Empty) {
            //        oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGetItemCodeByName" + sName, "Select ItemCode from OITM with(nolock) Where ItemName=\'" + sName + "\'");
            //        if (oRS != null && oRS.RecordCount > 0) {
            //            sCode = oRS.getValueAsString("ItemCode").Trim();
            //        }
            //    }
            //    //oBatchDetail.U_ItemCd = sCode;
            //    //oBatchDetail.U_ItemNm = sName;

            //    sName = Conversions.ToString(dt.Rows[r][ColPos.WasteMaterial-1]).Trim();
            //    sCode = "";
            //    if (sName != string.Empty) {
            //        oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGetItemCodeByName" + sName, "Select ItemCode from OITM with(nolock) Where ItemName=\'" + sName + "\'");
            //        if (oRS != null && oRS.RecordCount > 0) {
            //            sCode = oRS.getValueAsString("ItemCode").Trim();
            //        }
            //    }
            //    //oBatchDetail.U_WastCd = sCode;
            //    //oBatchDetail.U_WasteNm = sName;

            //    //oBatchDetail.U_UOM = Conversions.ToString(dt.Rows[r][ ColPos.UnitofMeasure));
            //    //oBatchDetail.U_Qty = Conversions.ToFloat(dt.Rows[r][ ColPos.Qty));
            //    //oBatchDetail.U_LPW = Conversions.ToFloat(dt.Rows[r][ ColPos.LPW));

            //    //oBatchDetail.U_FrqType = Conversions.ToString(dt.Rows[r][ ColPos.FrqType));
            //    //oBatchDetail.U_FrqCount = Conversions.ToInt(dt.Rows[r][ ColPos.FrqCount));
            //    //oBatchDetail.U_AutoComp = Conversions.ToString(dt.Rows[r][ ColPos.AutoComplete));
            //    //oBatchDetail.U_Rebate = Conversions.ToString(dt.Rows[r][ ColPos.Rebate));

            //    //oBatchDetail.U_Mon = Conversions.ToString(dt.Rows[r][ ColPos.Mon));
            //    //oBatchDetail.U_Tue = Conversions.ToString(dt.Rows[r][ ColPos.Tue));
            //    //oBatchDetail.U_Wed = Conversions.ToString(dt.Rows[r][ ColPos.Wed));
            //    //oBatchDetail.U_Thu = Conversions.ToString(dt.Rows[r][ ColPos.Thu));
            //    //oBatchDetail.U_Fri = Conversions.ToString(dt.Rows[r][ ColPos.Fri));
            //    //oBatchDetail.U_Sat = Conversions.ToString(dt.Rows[r][ ColPos.Sat));
            //    //oBatchDetail.U_Sun = Conversions.ToString(dt.Rows[r][ ColPos.Sun));

            //    //oBatchDetail.U_CMinChg = Conversions.ToFloat(dt.Rows[r][ ColPos.CIPMinimumCharge));
            //    //oBatchDetail.U_CMCgVol = Conversions.ToFloat(dt.Rows[r][ ColPos.CIPMinimumChargeUptoVolume));
            //    //oBatchDetail.U_CRminVol = Conversions.ToFloat(dt.Rows[r][ ColPos.CIPRateAboveMinVolume));
            //    //oBatchDetail.U_CHaulage = Conversions.ToFloat(dt.Rows[r][ ColPos.CIPHaulageServiceRental));

            //    //oBatchDetail.U_SMinChg = Conversions.ToFloat(dt.Rows[r][ ColPos.SIPMinimumCharge));
            //    //oBatchDetail.U_SMCgVol = Conversions.ToFloat(dt.Rows[r][ ColPos.SIPMinimumChargeUptoVolume));
            //    //oBatchDetail.U_SRminVol = Conversions.ToFloat(dt.Rows[r][ ColPos.SIPRateAboveMinVolume));
            //    //oBatchDetail.U_SHaulage = Conversions.ToFloat(dt.Rows[r][ ColPos.SIPHaulageServiceRental));
            //    //if (doFixStr(dt.Rows[r][ ColPos.EffectiveDate), 10) != string.Empty)
            //    //    oBatchDetail.U_EffDate = com.idh.utils.Dates.doStrToDate(doFixStr(dt.Rows[r][ ColPos.EffectiveDate), 10));
            //    //oBatchDetail.U_Comments = Conversions.ToString(dt.Rows[r][ ColPos.Comments));
            //    //if (doFixStr(dt.Rows[r][ ColPos.WTNDateFrom), 10) != string.Empty)
            //    //    oBatchDetail.U_WTNFrom = com.idh.utils.Dates.doStrToDate(doFixStr(dt.Rows[r][ ColPos.WTNDateFrom), 10));
            //    //if (doFixStr(dt.Rows[r][ ColPos.WTNDateTo), 10) != string.Empty)
            //    //    oBatchDetail.U_WTNTo = com.idh.utils.Dates.doStrToDate(doFixStr(dt.Rows[r][ ColPos.WTNDateTo), 10));

            //    //oBatchDetail.U_ProcSts = "0";

            //}
            private string noNull(object oValue) {
                return oValue == null ? "" : oValue.ToString();
            }
            private static string doFixStr(object oValue, int iLength) {
                string sVal = oValue == null ? "" : oValue.ToString();
                sVal = (sVal.Length > iLength ? sVal.Substring(0, (iLength - 3)) + "..." : sVal);
                return sVal;
            }
        }
        #endregion
        #endregion
        //MA  2017217 Issue:269 Code Order on Import
        //Stop imort process if excel file in Open
        private static bool FileOpen(String path) {
            System.IO.FileStream fs;
            try {
                fs = System.IO.File.Open(path, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                fs.Close();
                return true;
            } catch (Exception) {
                com.idh.bridge.DataHandler.INSTANCE.doError("This file :" + path + " Is already Opened.Close File before Importing");
                return false;
            }

        }
    }//end of class

}//end of namespace
