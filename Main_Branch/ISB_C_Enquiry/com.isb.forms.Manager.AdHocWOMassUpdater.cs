﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
//using System.Windows.Forms;
//using System.Threading;
//using com.idh.utils;
//using System.Text;

using IDHAddOns.idh.controls;
//using com.idh.bridge.reports;
//using com.idh.utils.Conversions;
using com.idh.bridge;
//using com.idh.bridge.data;
//using WR1_Grids.idh.controls.grid;
using com.idh.bridge.lookups;
//using com.idh.bridge.action;
using com.idh.controls;
using com.idh.controls.strct;
using com.idh.bridge.resources;
//using WR1_PBI;
using SAPbouiCOM;
using System.Linq;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry.PJDORA {
    class AdHocWOMassUpdater :
        IDHAddOns.idh.forms.Base {

        public AdHocWOMassUpdater(IDHAddOns.idh.addon.Base oParent, string sParent, int iMenuPosition)
            : base(oParent, "IDH_WOADHCMSUPT", sParent, iMenuPosition, "WOR_AdHoc_MassUpdater.srf", true, true, false, "Adhoc WO Creator", load_Types.idh_LOAD_NORMAL) {
        }

        protected void doTheGridLayout(DBOGrid oGridN) {
            if (com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", false)) {
                string sFormTypeId = oGridN.getSBOForm().TypeEx;
                idh.dbObjects.User.IDH_FORMSET oFormSettings = (idh.dbObjects.User.IDH_FORMSET)getWFValue("FRMSET", sFormTypeId + "." + oGridN.GridId);
                if (oFormSettings == null) {
                    oFormSettings = new idh.dbObjects.User.IDH_FORMSET();
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, "");
                    setWFValue("FRMSET", sFormTypeId + "." + oGridN.GridId, oFormSettings);
                    oFormSettings.doAddFieldToGrid(oGridN);
                    doSetListFields(oGridN);
                    oFormSettings.doSyncDB(oGridN);
                } else {
                    if (oFormSettings.SkipFormSettings) {
                        doSetListFields(oGridN);
                    } else {
                        oFormSettings.doAddFieldToGrid(oGridN);
                    }
                }
            } else {
                doSetListFields(oGridN);
            }
        }

        protected void doSetListFields(DBOGrid moGrid) {

            moGrid.doAddListField(IDH_WRADHD._Code, "Code", false, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._BatchNum, "Batch", false, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._SrNo, "Sr No.", false, -1, null, null);

            moGrid.doAddListField(IDH_WRADHD._CustCd, "Customer code", true, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            moGrid.doAddListField(IDH_WRADHD._CustNm, "Customer Name", true, -1, null, null);

            moGrid.doAddListField(IDH_WRADHD._CustAdr, "Customer Site Address", true, -1, null, null);

            moGrid.doAddListField(IDH_WRADHD._CusPCode, "Post Code", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._JbTypeDs, "Order Type", true, -1, null, null);
            //moGrid.doAddListField(IDH_WRADHD._JbTypeCd, "Order Type Cd", true, -1, null, null);

            moGrid.doAddListField(IDH_WRADHD._SuppCd, "Supplier Code", true, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            moGrid.doAddListField(IDH_WRADHD._SuppNm, "Supplier Name", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._SuppRef, "Supplier Reference", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._SupAddr, "Supplier Address", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._SupPCode, "Supplier Postcode", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._MaximoNum, "Maximo number", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._AddtItm, "Additional Item", true, -1, ListFields.LISTTYPE_IGNORE, null);
            moGrid.doAddListField(IDH_WRADHD._ItemCd, "Container Code", true, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            moGrid.doAddListField(IDH_WRADHD._ItemNm, "Container Desc", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._WastCd, "Waste Code", true, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            moGrid.doAddListField(IDH_WRADHD._WasteNm, "Waste Material", true, -1, null, null);

            moGrid.doAddListField(IDH_WRADHD._UOM, "Unit of Measure(UOM)", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._PUOM, "Purchase Unit of Measure(PUOM)", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._Qty, "Actual Qty", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._HlgQty, "Haulage Qty", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._ExpLdWgt, "Expected Qty", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._WOH, "WOH#", false, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._WOR, "WOR#", false, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._RDate, "Request Date", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._SDate, "Start Date", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._EDate, "End Date", true, -1, null, null);

            moGrid.doAddListField(IDH_WRADHD._TChgPCst, "Tip Charge/Purchase Cost", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._CusChr, "Haulage Charge", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._TipCost, "Disposal Cost", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._OrdCost, "Haulage Cost", true, -1, null, null);

            moGrid.doAddListField(IDH_WRADHD._Rebate, "Rebate", true, -1, ListFields.LISTTYPE_CHECKBOX, null);

            moGrid.doAddListField(IDH_WRADHD._Comments, "Comments", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._ProcSts, "Process Status", false, -1, ListFields.LISTTYPE_COMBOBOX, null);
            moGrid.doAddListField(IDH_WRADHD._VldStatus, "Validation Status", false, -1, ListFields.LISTTYPE_COMBOBOX, null);
            moGrid.doAddListField(IDH_WRADHD._BthStatus, "Batch Status", false, -1, ListFields.LISTTYPE_COMBOBOX, null);

            moGrid.doAddListField(IDH_WRADHD._Tip, "Disposal Site", true, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            moGrid.doAddListField(IDH_WRADHD._TipNm, "Disposal Site Name", true, -1, null, null);
            moGrid.doAddListField(IDH_WRADHD._SAddress, "Disposal Site Address", true, -1, null, null);


            moGrid.doAddListField(IDH_WRADHD._AddedBy, "Added By", true, 0, null, null);

        }

        protected void doSetGridFilters(DBOGrid moGridN) {
            //doAddUF(moGridN.getSBOForm(), "IDH_ACTNAP", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 250).AffectsFormMode = false;

            //moGridN.doAddFilterField("WORCode", IDH_WRADHD._WORow, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            //  moGridN.doAddFilterField("IDH_AMDTYP", IDH_WRADHD._AmndTp, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_CUST", IDH_WRADHD._CustCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_NAME", IDH_WRADHD._CustNm, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_CADDR", IDH_WRADHD._CustAdr, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_CPCODE", IDH_WRADHD._CusPCode, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_SUPCD", IDH_WRADHD._SuppCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_SUPNM", IDH_WRADHD._SuppNm, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);


            moGridN.doAddFilterField("IDH_JOBTYP", IDH_WRADHD._JbTypeDs, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_ITEMCD", IDH_WRADHD._ItemCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_ITEMNM", IDH_WRADHD._ItemNm, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WASTCD", IDH_WRADHD._WastCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WASTNM", IDH_WRADHD._WasteNm, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_BATCHF", IDH_WRADHD._BatchNum, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 250);
            //Process Status
            moGridN.doAddFilterField("IDH_BTPRST", IDH_WRADHD._ProcSts, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 1);
            //IDH_VLDSTS Validation Status
            moGridN.doAddFilterField("IDH_VLDSTS", IDH_WRADHD._VldStatus, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 2);
            //IDH_BTCSTS Batch Status Active/Deleted
            moGridN.doAddFilterField("IDH_BTCSTS", IDH_WRADHD._BthStatus, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 1);

        }
        //*** Add event filters to avoid receiving all events from SBO
        protected override void doSetEventFilters() {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE);
        }

        protected override void doReadInputParams(SAPbouiCOM.Form oForm) {

        }

        //** Create the form
        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            try {
                oForm.Title = gsTitle;
                int iWidth = oForm.Width;

                oForm.AutoManaged = false;

                oForm.EnableMenu(Config.NAV_ADD, false);
                oForm.EnableMenu(Config.NAV_FIND, false);
                oForm.EnableMenu(Config.NAV_FIRST, false);
                oForm.EnableMenu(Config.NAV_NEXT, false);
                oForm.EnableMenu(Config.NAV_PREV, false);
                oForm.EnableMenu(Config.NAV_LAST, false);

                oForm.SupportedModes = Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_All);
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);
                base.doCompleteCreate(ref oForm, ref BubbleEvent);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDLF", null);
                BubbleEvent = false;
            }
        }

        protected void doSetMode(SAPbouiCOM.Form oForm, SAPbouiCOM.BoFormMode iMode) {
            try {
                object sFormType = getParentSharedData(oForm, "FTYPE");
                bool bIsSearch = false;
                if ((sFormType != null) && sFormType.Equals("SEARCH") == true) {
                    bIsSearch = true;
                }

                if (iMode == SAPbouiCOM.BoFormMode.fm_FIND_MODE) {
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = Translation.getTranslatedWord("Find");
                    if (bIsSearch) {
                        oForm.Items.Item("2").Visible = true;
                    } else {
                        oForm.Items.Item("2").Visible = false;
                    }
                } else if (iMode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = Translation.getTranslatedWord("Update");
                    oForm.Items.Item("2").Visible = true;
                }
            } catch (Exception ex) {
            }
        }

        public string getListRequiredStr(SAPbouiCOM.Form OForm) {
            return " 1=1 ";
        }

        private void doFillGridCombos(UpdateGrid moGrid) {
            SAPbouiCOM.ComboBoxColumn oCombo;
            int iIndex;

            iIndex = moGrid.doIndexFieldWC(IDH_WRADHD._ProcSts);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            oCombo.ValidValues.Add("0", Translation.getTranslatedWord("Not Processed"));
            oCombo.ValidValues.Add("1", Translation.getTranslatedWord("Sent For Review"));
            oCombo.ValidValues.Add("2", Translation.getTranslatedWord("Ready for Process"));
            oCombo.ValidValues.Add("3", Translation.getTranslatedWord("Processed"));


            iIndex = moGrid.doIndexFieldWC(IDH_WRADHD._VldStatus);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            oCombo.ValidValues.Add("", "");
            oCombo.ValidValues.Add("0", Translation.getTranslatedWord("Failed"));
            oCombo.ValidValues.Add("1", Translation.getTranslatedWord("Passed"));


            iIndex = moGrid.doIndexFieldWC(IDH_WRADHD._BthStatus);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            oCombo.ValidValues.Add("", Translation.getTranslatedWord("All"));
            oCombo.ValidValues.Add("0", Translation.getTranslatedWord("Deleted"));
            oCombo.ValidValues.Add("1", Translation.getTranslatedWord("Active"));

        }

        private void doFillCombos(SAPbouiCOM.Form oForm) {
            try {
                SAPbouiCOM.ComboBox oCombo;

                oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_JOBTYP").Specific;
                oCombo.Item.DisplayDesc = true;
                doClearValidValues(oCombo.ValidValues);
                string sItemGrp = "";
                com.uBC.utils.FillCombos.doFillJobTypeCombo(ref oCombo, sItemGrp);

                //IDH_BTCSTS Batch Status
                oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_BTCSTS").Specific;
                doClearValidValues(oCombo.ValidValues);
                oCombo.Item.DisplayDesc = true;
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
                oCombo.ValidValues.Add("1", Translation.getTranslatedWord("Active"));
                oCombo.ValidValues.Add("0", Translation.getTranslatedWord("Deleted"));
                oCombo.ValidValues.Add("", Translation.getTranslatedWord("All"));
                //oCombo.ValidValues.Add("3", Translation.getTranslatedWord("Processed"));
                oCombo.SelectExclusive(0, SAPbouiCOM.BoSearchKey.psk_Index);

                //IDH_BTPRST Process Status
                oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_BTPRST").Specific;
                doClearValidValues(oCombo.ValidValues);

                oCombo.Item.DisplayDesc = true;
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
                oCombo.ValidValues.Add("", Translation.getTranslatedWord("All"));
                oCombo.ValidValues.Add("0", Translation.getTranslatedWord("Not Processed"));
                oCombo.ValidValues.Add("1", Translation.getTranslatedWord("Sent For Review"));
                oCombo.ValidValues.Add("2", Translation.getTranslatedWord("Ready for Process"));
                oCombo.ValidValues.Add("3", Translation.getTranslatedWord("Processed"));


                //IDH_VLDSTS Validation Status
                oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_VLDSTS").Specific;
                doClearValidValues(oCombo.ValidValues);

                oCombo.Item.DisplayDesc = true;
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
                oCombo.ValidValues.Add("", Translation.getTranslatedWord("All"));
                oCombo.ValidValues.Add("1", Translation.getTranslatedWord("Passed"));
                oCombo.ValidValues.Add("0", Translation.getTranslatedWord("Failed"));

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }
        }

        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {



            DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");
            if (oGridN == null) {
                oGridN = (DBOGrid)DBOGrid.getInstance(oForm, "LINESGRID");
                if (oGridN == null) {
                    IDH_WRADHD oLiveSrvcBatch = new IDH_WRADHD(this, oForm);
                    oGridN = new DBOGrid(this, oForm, "LINESGRID", 7, 120, oForm.Width - 20, 255, 0, 0, oLiveSrvcBatch);

                }
                setWFValue(oForm, "LINESGRID", oGridN);
            }
            oGridN.doSetDeleteActive(true);
            doSetGridFilters(oGridN);
            oGridN.doSetDoCount(true);
            oGridN.doSetBlankLine(false);

            //Set the required List Fields
            doTheGridLayout(oGridN);
            doFillCombos(oForm);

            oGridN.setInitialFilterValue(IDH_WRADHD._BatchNum + " =\'-999\' ");
            if (DataHandler.INSTANCE.User != "manager") {//&& DataHandler.INSTANCE.User!="manager"
                string sUsersForProcess = Config.INSTANCE.getParameter("WOUPURLT");
                if (sUsersForProcess == string.Empty) {
                    setVisible(oForm, "IDH_PRCBTC", false);
                    setVisible(oForm, "IDH_PRCLTR", false);
                }

                string[] aUsersForProcess = sUsersForProcess.Split(',');
                bool bFound = false;
                for (int i = 0; i < aUsersForProcess.Length; i++) {
                    if (aUsersForProcess[i].Trim().Equals(DataHandler.INSTANCE.User, StringComparison.OrdinalIgnoreCase)) {
                        setVisible(oForm, "IDH_PRCBTC", true);
                        setVisible(oForm, "IDH_PRCLTR", true);
                        bFound = true;
                        break;
                    }
                }
                if (!bFound) {
                    setVisible(oForm, "IDH_PRCBTC", false);
                    setVisible(oForm, "IDH_PRCLTR", false);
                }

            }
        }

        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            doReLoadData(oForm, !getHasSharedData(oForm));
        }

        //** The Initializer
        protected void doReLoadData(SAPbouiCOM.Form oForm, bool bIsFirst) {
            try {
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);

                DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");//DBOGrid.getInstance(oForm, "LINESGRID");
                string sFilter = oGridN.getRequiredFilter();
                // doSetCustomeFilter(oGridN);

                //#MA 20170301 start Issue=292
                //string BatchNum = getItemValue(oForm, "IDH_BATCHF");
                if (!getItemValue(oForm, "IDH_BATCHF").Contains("*") & getItemValue(oForm, "IDH_BATCHF").Length > 0) {
                    sFilter = IDH_WRADHD._BatchNum + "='" + getItemValue(oForm, "IDH_BATCHF") + "'";
                    //setUFValue(oForm, "IDH_BATCHF","");
                }
                if (sFilter != "" & sFilter != null) {
                    if (oGridN.getRequiredFilter().Length > 0) {
                        oGridN.setRequiredFilter(oGridN.getRequiredFilter() + " AND " + sFilter);
                    } else {
                        oGridN.setRequiredFilter(sFilter);
                    }
                }
                //#MA 20170301 End

                //oGridN.setOrderValue("Cast(" + IDH_WOQITEM._JobNr + " as Int)," + IDH_WOQITEM._Sort + " ");
                oGridN.setOrderValue("Cast(" + IDH_WRADHD._SrNo + " as Int),Cast(" + IDH_WRADHD._Code + " as Int)");
                oGridN.doReloadData("", false, true);
                doFillGridCombos(oGridN);
                oGridN.setRequiredFilter(string.Empty);

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBL", null);
            }
        }

        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED) {
                if (pVal.ItemUID == "IDH_IMPEXC") {
                    string sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("WRNUSAVE", null);
                    //int iRow = pVal.Row;
                    //if (IDHAddOns.idh.addon.Base.PARENT.doMessage(sResourceMessage, 2, "Yes", "No", null, true) == 1) {
                    if ((((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find")) || (
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption != Translation.getTranslatedWord("Find") &&
                    (IDHAddOns.idh.addon.Base.PARENT.doMessage(sResourceMessage, 2, "Yes", "No", null, true) == 1))
                    ) {
                        doImportExcel(oForm);
                    }
                } else if (pVal.ItemUID == "IDH_DELBAT") {
                    // Delete Batch
                    doPreDeleteBatch(oForm);
                } else if (pVal.ItemUID == "IDH_SNDBTH") {
                    if ((((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find"))) {
                        doSendBatchForReview(oForm);
                    } else {
                        string sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("SAVEFORM", null);
                        com.idh.bridge.DataHandler.INSTANCE.doError(sResourceMessage);
                    }

                } else if (pVal.ItemUID == "IDH_PRCBTC") {
                    if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find"))
                        doProcessBatch(oForm, true);
                } else if (pVal.ItemUID == "IDH_PRCLTR") {
                    if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find"))
                        doProcessBatch(oForm, false);
                }
            }
            return base.doItemEvent(oForm, ref pVal, ref BubbleEvent);
        }
        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == true) {
                if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Save") || ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update")) {
                    doSaveBatch(oForm);
                } else if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find")) {
                    doReLoadData(oForm, true);
                }
                BubbleEvent = false;
            }
        }
        public override void doButtonID2(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == true) {
                if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Save") || ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update")) {
                    doReLoadData(oForm, true);
                    BubbleEvent = false;
                }
            }
        }



        protected override void doHandleModalResultShared(SAPbouiCOM.Form oParentForm, string sModalFormType, string sLastButton = null) {
            if (sModalFormType.Equals("IDH_COMMFRM")) {
                DBOGrid oGridN = (DBOGrid)getWFValue(oParentForm, "LINESGRID");
                if (getSharedData(oParentForm, "CMMNT") == null || getSharedData(oParentForm, "CMMNT").ToString().Trim() == string.Empty) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Please provide reason for deletion of batch(s).");
                    return;
                }
                string sComments = getSharedData(oParentForm, "CMMNT").ToString().Trim();
                doDeleteBatch(oParentForm, sComments);
            }
            base.doHandleModalResultShared(oParentForm, sModalFormType, sLastButton);
        }
        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCloseForm(oForm, ref BubbleEvent);
            UpdateGrid.doRemoveGrid(oForm, "LINESGRID");
        }
        public override void doClose() {
        }
        protected void doGridDoubleClick(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
            if (pVal.Row >= 0) {

                string sRowCode = oGridN.doGetFieldValue(IDH_WRADHD._WOR).ToString();
                string sWOHID = oGridN.doGetFieldValue(IDH_WRADHD._WOH).ToString();

                ArrayList oData = new ArrayList();
                //if (sWOQID.Length == 0) {
                //   com.idh.bridge.DataHandler.INSTANCE.doResUserError("Valid row must be selected.", "ERUSJOBS", null);
                //}else 
                if (!string.IsNullOrEmpty(sWOHID) && oGridN.doCheckIsSameCol(pVal.ColUID, IDH_WRADHD._WOH)) {
                    //Open WOH
                    ArrayList moData = new ArrayList();
                    moData.Add("DoUpdate");
                    moData.Add(oGridN.doGetFieldValue(IDH_WRADHD._WOH).ToString());
                    goParent.doOpenModalForm("IDH_WASTORD", oForm, moData);
                } else if (!string.IsNullOrEmpty(sRowCode) && oGridN.doCheckIsSameCol(pVal.ColUID, IDH_WRADHD._WOR)) {
                    //Open WOR
                    ArrayList moData = new ArrayList();
                    moData.Add("DoUpdate");
                    moData.Add(oGridN.doGetFieldValue(IDH_WRADHD._WOR).ToString());
                    setSharedData("IDHJOBS", "ROWCODE", oGridN.doGetFieldValue(IDH_WRADHD._WOR).ToString());
                    goParent.doOpenModalForm("IDHJOBS", oForm, moData);
                } else {//PBI

                }

            }
        }

        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED) {
                if (pVal.ItemUID == "LINESGRID") {
                    if (pVal.BeforeAction == false) {
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE);
                        //  doGridDoubleClick(oForm, ref pVal);
                    }
                }
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK && !pVal.BeforeAction) {
                if (pVal.ItemUID == "LINESGRID") {
                    if (pVal.BeforeAction == false) {
                        //doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE);
                        doGridDoubleClick(oForm, ref pVal);
                    }
                }
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_ADD && !pVal.BeforeAction) {
                pVal.oGrid.getSBOGrid().CommonSetting.SetRowFontColor(pVal.Row + 1, Convert.ToInt32("0000FF", 16));
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SORT && pVal.BeforeAction == false) {
                UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
                doFillGridCombos(oGridN);
                oGridN.doApplyRules();
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK && pVal.Row >= 0 && pVal.BeforeAction) {
                try {
                    //oJobSeq.gotoRow(oJobTypes.Item(iIndex))
                    //sJobType = oJobSeq.U_JobTp
                    if (pVal.oGrid.getSBOGrid().CommonSetting.GetCellFontColor(pVal.Row + 1, pVal.oGrid.doIndexFieldWC(pVal.ColUID) + 1) == Convert.ToInt32("0000FF", 16)) {
                        SAPbouiCOM.MenuItem oMenuItem;
                        oMenuItem = goParent.goApplication.Menus.Item(IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU);

                        SAPbouiCOM.Menus oMenus;
                        SAPbouiCOM.MenuCreationParams oCreationPackage;
                        oCreationPackage = (SAPbouiCOM.MenuCreationParams)goParent.goApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);

                        oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                        oCreationPackage.UniqueID = "ValidateData";
                        //'oCreationPackage.String = getTranslatedWord("Duplicate Selection")
                        oCreationPackage.String = getTranslatedWord("Validate Data");//
                        oCreationPackage.Enabled = true;
                        oMenus = oMenuItem.SubMenus;
                        oMenus.AddEx(oCreationPackage);

                    }
                } catch (Exception ex) {
                    //com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doApplyNewAmendAction" });
                } finally {
                    //    oForm.Freeze(false);
                }
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT && pVal.Row >= 0 && pVal.BeforeAction) {
                SAPbouiCOM.MenuEvent opVal = (SAPbouiCOM.MenuEvent)pVal.oData;
                if (opVal.MenuUID.Equals("ValidateData")) {

                    doValidateGridCell(oForm, pVal.Row, pVal.ColUID);
                }
            }
            return base.doCustomItemEvent(oForm, ref pVal);
        }

        private void doValidateGridCell(SAPbouiCOM.Form oForm, int r, string sCOlID) {
            try {
                DBOGrid oDBGrid = (DBOGrid)getWFValue(oForm, "LINESGRID");

                //IDH_JOBENTR oWOH = new IDH_JOBENTR();
                //oWOH.UnLockTable = true;

                //               int RGBRed = Convert.ToInt32("0000FF", 16);

                bool bRowValidated = doValidateBatch(oForm, r);
                //               int iLastWOR = 0;
                //               bool bAdditionalItem = false;

                //               if (oDBGrid.doGetFieldValue(IDH_WRADHD._AddtItm, r).ToString() == string.Empty) {
                //                   bAdditionalItem = false;
                //                   iLastWOR = r;
                //               } else {
                //bAdditionalItem = true;
                //               }

                //if (sCOlID == IDH_WRADHD._CustCd && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._CustCd, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer Code missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CustCd) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WRADHD._CustNm && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._CustNm, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer name missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CustNm) + 1, RGBRed);
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WRADHD._CustCd && bAdditionalItem == false && !Config.INSTANCE.ValidateBPCodenName(oDBGrid.doGetFieldValue(IDH_WRADHD._CustCd, r).ToString(), oDBGrid.doGetFieldValue(IDH_WRADHD._CustNm, r).ToString())) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid Customer Code /Name.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CustCd) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WRADHD._CustAdr && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._CustAdr, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer address is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CustAdr) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WRADHD._CusPCode && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._CusPCode, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer postcode is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CusPCode) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WRADHD._CustAdr && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._CustAdr, r).ToString() != string.Empty
                //    && !Config.INSTANCE.ValidateBPAddress(oDBGrid.doGetFieldValue(IDH_WRADHD._CustCd, r).ToString(), oDBGrid.doGetFieldValue(IDH_WRADHD._CustAdr, r).ToString(), "S")) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer address/postcode is not valid.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CustAdr) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WRADHD._JbTypeDs && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._JbTypeDs, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Order Type is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._JbTypeDs) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WRADHD._SuppCd && oDBGrid.doGetFieldValue(IDH_WRADHD._SuppCd, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Supplier code is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._SuppCd) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WRADHD._SuppNm && oDBGrid.doGetFieldValue(IDH_WRADHD._SuppNm, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Supplier name is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._SuppNm) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WRADHD._SuppCd && !Config.INSTANCE.ValidateBPCodenName(oDBGrid.doGetFieldValue(IDH_WRADHD._SuppCd, r).ToString(), oDBGrid.doGetFieldValue(IDH_WRADHD._SuppNm, r).ToString())) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid Supplier Code /Name.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._SuppCd) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WRADHD._ItemCd && oDBGrid.doGetFieldValue(IDH_WRADHD._ItemCd, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Container code is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._ItemCd) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WRADHD._ItemNm && oDBGrid.doGetFieldValue(IDH_WRADHD._ItemNm, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Container name is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._ItemNm) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WRADHD._WastCd && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._WastCd, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Waste item code is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._WastCd) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WRADHD._WasteNm && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._WasteNm, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Waste item name is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._WasteNm) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    bRowValidated = false;
                //} else if ((sCOlID == IDH_WRADHD._WasteNm || sCOlID == IDH_WRADHD._WastCd) && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._WastCd, r).ToString() != string.Empty  && bAdditionalItem == false&& !Config.INSTANCE.ValidateWasteItem(oDBGrid.doGetFieldValue(IDH_WRADHD._WastCd, r).ToString())) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Waste item: " + oDBGrid.doGetFieldValue(IDH_WRADHD._WastCd, r).ToString() + " is not a valid waste item.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._WastCd) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WRADHD._ItemCd && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._ItemCd, r).ToString() != string.Empty && !Config.INSTANCE.ValidateContainerItem(oDBGrid.doGetFieldValue(IDH_WRADHD._ItemCd, r).ToString())) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Container item: " + oDBGrid.doGetFieldValue(IDH_WRADHD._WastCd, r).ToString() + " is not a valid container item.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._ItemCd) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WRADHD._JbTypeDs && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._JbTypeDs, r).ToString() != string.Empty && !Config.INSTANCE.ValidateJobTypeWithContainerItem(oDBGrid.doGetFieldValue(IDH_WRADHD._JbTypeDs, r).ToString(), oDBGrid.doGetFieldValue(IDH_WRADHD._ItemCd, r).ToString())) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Order Type : " + oDBGrid.doGetFieldValue(IDH_WRADHD._JbTypeDs, r).ToString() + " is not a valid with attached container.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._JbTypeDs) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WRADHD._UOM && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._UOM, r).ToString() == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("UOM is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._UOM) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    bRowValidated = false;
                //} else if (sCOlID == IDH_WRADHD._Qty && bAdditionalItem == false && (oDBGrid.doGetFieldValue(IDH_WRADHD._Qty, r).ToString() == string.Empty || (int)oDBGrid.doGetFieldValue(IDH_WRADHD._Qty, r) <= 0)) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Qty must be greater than zero. Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._Qty) + 1, RGBRed);
                //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                //    bRowValidated = false;
                //} else {
                //    if (sCOlID == IDH_WRADHD._WOH && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._WOH, r).ToString() != string.Empty && !oWOH.getByKey(oDBGrid.doGetFieldValue(IDH_WRADHD._WOH, r).ToString())) {
                //        com.idh.bridge.DataHandler.INSTANCE.doUserError("Failed to load WOH#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOH, r).ToString() + ".");
                //        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._WOH) + 1, RGBRed);
                //        bRowValidated = false;
                //    }
                //    if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._WOH, r).ToString() != string.Empty && oWOH.Code != string.Empty) {
                //          if (sCOlID == IDH_WRADHD._CustCd && oWOH.U_CardCd != oDBGrid.doGetFieldValue(IDH_WRADHD._CustCd, r).ToString()) {
                //            com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer code is different than on WOH#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOH, r).ToString() + ".");
                //            oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //            oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CustCd) + 1, RGBRed);
                //            bRowValidated = false;
                //        } else if (sCOlID == IDH_WRADHD._CustNm && oWOH.U_CardNM != oDBGrid.doGetFieldValue(IDH_WRADHD._CustNm, r).ToString()) {
                //            com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer name is different than on WOH#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOH, r).ToString() + ".");
                //            oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //            oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CustNm) + 1, RGBRed);
                //            bRowValidated = false;
                //        } else if (sCOlID == IDH_WRADHD._CustAdr && oWOH.U_CAddress != oDBGrid.doGetFieldValue(IDH_WRADHD._CustAdr, r).ToString()) {
                //            com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer address is different than on WOH#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOH, r).ToString() + ".");
                //            oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //            oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CustAdr) + 1, RGBRed);
                //            bRowValidated = false;
                //        } else if (sCOlID == IDH_WRADHD._SuppCd && oWOH.U_PCardCd != oDBGrid.doGetFieldValue(IDH_WRADHD._SuppCd, r).ToString()) {
                //            com.idh.bridge.DataHandler.INSTANCE.doUserError("Supplier code is different than on WOH#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOH, r).ToString() + ".");
                //            oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //            oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._SuppCd) + 1, RGBRed);
                //            bRowValidated = false;
                //        } else if (sCOlID == IDH_WRADHD._SuppNm && oWOH.U_PCardNM != oDBGrid.doGetFieldValue(IDH_WRADHD._SuppNm, r).ToString()) {
                //            com.idh.bridge.DataHandler.INSTANCE.doUserError("Supplier name is different than on WOH#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOH, r).ToString() + ".");
                //            oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                //            oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._SuppNm) + 1, RGBRed);
                //            bRowValidated = false;
                //        } 
                //    }

                //}
                //}
                if (bRowValidated) {
                    //oDBGrid.getSBOGrid().CommonSetting.SetRowFontColor(r + 1, 0);
                    //oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "1");
                    if (oDBGrid.doProcessData()) {
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);
                        // GridRows oRows= oDBGrid.getSBOGrid().Rows;//.Columns.Item(1).ce
                        //CellPosition cp = oDBGrid.getSBOGrid().GetCellFocus();
                        //cp.
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                 new string[] { "IDH_WRADHD.doValidateGridCell" });
            }
        }
        private void doSendBatchForReview(SAPbouiCOM.Form oForm) {
            try {
                DBOGrid oDBGrid = (DBOGrid)getWFValue(oForm, "LINESGRID");
                if (oDBGrid.getRowCount() == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("No batch loaded to send for process.");
                    return;
                }

                string sUsersForProcess = Config.INSTANCE.getParameter("WOUPURLT");
                if (sUsersForProcess == string.Empty) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("No user configured for batch process. Please update config list.");
                    return;
                }
                string sBatchNum = "";
                for (int r = 0; r <= oDBGrid.getRowCount() - 1; r++) {
                    if (sBatchNum == "")
                        sBatchNum = oDBGrid.doGetFieldValue(IDH_WRADHD._BatchNum, r).ToString();
                    else if (sBatchNum != oDBGrid.doGetFieldValue(IDH_WRADHD._BatchNum, r).ToString() && oDBGrid.doGetFieldValue(IDH_WRADHD._BatchNum, r).ToString() != "") {
                        if (r != oDBGrid.getRowCount() - 1) {
                            com.idh.bridge.DataHandler.INSTANCE.doUserError("Please only one batch to process.");
                            return;
                        }
                    }
                }
                if (sBatchNum == "") {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Please save data to assign batch number.");
                    return;
                }
                IDH_WRADHH obatch = new IDH_WRADHH();
                obatch.UnLockTable = true;
                if (obatch.getByKey(sBatchNum)) {
                    string sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("ALRARDST", new string[] { sBatchNum });
                    if (obatch.U_AlterSend.ToUpper() == "Y" && (IDHAddOns.idh.addon.Base.PARENT.doMessage(sResourceMessage, 2, "Yes", "No", null, true) != 1)) {
                        return;
                    }
                }

                if (doValidateBatch(oForm, -1) == false) {
                    return;
                }

                bool bRet = true;
                obatch = new IDH_WRADHH();
                obatch.UnLockTable = true;
                if (obatch.getByKey(sBatchNum) &&
                    com.idh.bridge.utils.General.SendInternalAlert(sUsersForProcess, Messages.INSTANCE.getMessage("ALRBTMSG", new string[] { "Adhoc WO Creater", sBatchNum }), "Adhoc WO Batch#" + sBatchNum)
                    ) {
                    DataHandler.INSTANCE.StartTransaction();
                    //if (obatch.getByKey(sBatchNum)) {
                    obatch.U_AlterSdBy = DataHandler.INSTANCE.User;
                    obatch.U_AlterSdDt = DateTime.Today;
                    obatch.U_AlterSdTm = DateTime.Now.ToString("HHmm");
                    obatch.U_AlterSdTo = sUsersForProcess;
                    obatch.U_AlterSend = "Y";
                    if (obatch.doProcessData()) {
                        IDH_WRADHD oBatchDetail = (IDH_WRADHD)oDBGrid.DBObject;

                        if (oBatchDetail.Count >= 0) {
                            oForm.Freeze(true);
                            oBatchDetail.first();
                            while (oBatchDetail.next() && oBatchDetail.Code != string.Empty) {
                                oBatchDetail.U_ProcSts = ((int)IDH_WRADHD.en_BatchProcessStatus.SentToReview).ToString();
                                if (oBatchDetail.doUpdateDataRow() == false) {
                                    bRet = false;
                                    break;
                                }
                            }

                        }
                    } else {
                        bRet = false;
                    }
                    //}
                    if (DataHandler.INSTANCE.IsInTransaction()) {
                        if (bRet)
                            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                        else
                            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    }
                }

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doSendBatchForReview" });
                if (DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
            } finally {
                oForm.Freeze(false);
            }
        }
        #region"Process Batch"
        private void doProcessBatch(SAPbouiCOM.Form oForm, bool bProcessNow) {
            try {
                string sMsg = "Are you sure to process batch now? It may take several minutes to process batch.";
                if (!bProcessNow)
                    sMsg = "Are you sure to send batch to process later by ISB Commander service?";

                if (IDHAddOns.idh.addon.Base.PARENT.doMessage(sMsg, 2, "Yes", "No", null, true) != 1) {
                    return;
                }
                DBOGrid oDBGrid = (DBOGrid)getWFValue(oForm, "LINESGRID");
                if (oDBGrid.getRowCount() == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("No batch loaded for process.");
                    return;
                }
                bool doCheckForMultiBatch = true;
                string sBatchNum = "";
                try {
                    string sLastSQL = oDBGrid.DBObject.LastQuery.ToLower();
                    string[] aLastSQL = sLastSQL.Split(new string[] { "from" }, StringSplitOptions.None);
                    if (aLastSQL.Length > 1) {
                        sLastSQL = aLastSQL[aLastSQL.Length - 1];
                        aLastSQL = sLastSQL.Split(new string[] { "order by" }, StringSplitOptions.None);
                        if (aLastSQL.Length > 0)
                            sLastSQL = aLastSQL[0];

                        sLastSQL = "Select Distinct " + IDH_WRADHD._BatchNum + " From " + sLastSQL;
                        DataRecords oRecs = null;
                        //string sQry = "select DocEntry from " + sDocTable + " where U_FlagToClose = 'Y' AND DocStatus != 'C'";
                        oRecs = DataHandler.INSTANCE.doBufferedSelectQuery("doGetDistinctBatch" + DateTime.Now.ToString("HHmmttss"), sLastSQL);
                        if (oRecs != null && oRecs.RecordCount > 0) {
                            if (oRecs.RecordCount == 1) {
                                sBatchNum = oRecs.getValue(IDH_WRADHD._BatchNum).ToString();

                            } else {
                                com.idh.bridge.DataHandler.INSTANCE.doUserError("Please only one batch to process.");
                                return;
                            }
                            doCheckForMultiBatch = false;
                        }
                    }
                } catch (Exception ex) {
                }
                if (doCheckForMultiBatch) {
                    for (int r = 0; r <= oDBGrid.getRowCount() - 1; r++) {
                        if (sBatchNum == "")
                            sBatchNum = oDBGrid.doGetFieldValue(IDH_WRADHD._BatchNum, r).ToString();
                        else if (sBatchNum != oDBGrid.doGetFieldValue(IDH_WRADHD._BatchNum, r).ToString() && oDBGrid.doGetFieldValue(IDH_WRADHD._BatchNum, r).ToString() != "") {
                            if (r != oDBGrid.getRowCount() - 1) {
                                com.idh.bridge.DataHandler.INSTANCE.doUserError("Please only one batch to process.");
                                return;
                            }
                        }
                    }
                }

                if (sBatchNum == "") {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Please save data to assign batch number.");
                    return;
                }
                IDH_WRADHH obatch = new IDH_WRADHH();
                obatch.UnLockTable = true;
                if (obatch.getByKey(sBatchNum)) {
                    // string sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("ALRARDST", new string[] { sBatchNum });
                    if (obatch.U_AlterSend.ToUpper() != "Y") {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Select batch is not ready to process.");
                        // && (IDHAddOns.idh.addon.Base.PARENT.doMessage(sResourceMessage, 2, "Yes", "No", null, true) != 1)) {
                        return;
                    } else if (obatch.U_BtchClose.ToUpper() == "Y") {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Select batch is already closed.");
                        return;
                    }
                } else {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Failed to load batch header.");
                    return;
                }
                //remove re-validate after test on ISS                
                //if (doValidateBatch(oForm,-1) == false) {
                //    return;
                //}

                //DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");
                IDH_WRADHD oBatchDetail = (IDH_WRADHD)oDBGrid.DBObject;
                oBatchDetail.UnLockTable = true;
                //if (!bProcessNow)
                  //  DataHandler.INSTANCE.StartTransaction();
                bool bDone = true;
                oBatchDetail.doBookmark();
                oBatchDetail.first();
                int iRow = 0;
                DataHandler.INSTANCE.doInfo("Please Wait: System is processing your batch request, this may take some time depending on the batch size.");
                while (oBatchDetail.next()) {
                    if (oBatchDetail.Code != string.Empty) {
                        if (oBatchDetail.doProcessBatchItem(!bProcessNow)) {
                            //now process call the PBI Handler
                            DataHandler.INSTANCE.doProgress("PROCEDDADHOCWOR", iRow++, oBatchDetail.Count);
                        } else {
                            bDone = false;
                            //break;
                        }
                    }
                }
                if (bDone) {
                    if (DataHandler.INSTANCE.IsInTransaction())
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    if (bProcessNow)
                        com.idh.bridge.DataHandler.INSTANCE.doInfo("Successfully processed the batch.");
                    else
                        com.idh.bridge.DataHandler.INSTANCE.doInfo("Successfully send the batch for processed later.");
                } else if (bDone == false) {
                    if (DataHandler.INSTANCE.IsInTransaction())
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    DataHandler.INSTANCE.doUserError("Failed to process batch. Please review the error messages.");
                }
                oBatchDetail.doRecallBookmark();
                oDBGrid.doReloadData("", false, true);
                doFillGridCombos(oDBGrid);
            } catch (Exception ex) {
                if (DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doProcessBatch" });
            } finally {
                DataHandler.INSTANCE.doProgressDone("PROCEDDADHOCWOR");
                if (DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);

            }
        }
        private bool doValidateBatch(SAPbouiCOM.Form oForm, int row) {
            try {
                DBOGrid oDBGrid = (DBOGrid)getWFValue(oForm, "LINESGRID");


                idh.dbObjects.User.IDH_JOBENTR oWOH = new idh.dbObjects.User.IDH_JOBENTR();
                oWOH.UnLockTable = true;

                IDH_JOBSHD oWOR = new IDH_JOBSHD();
                oWOR.UnLockTable = true;

                int RGBRed = Convert.ToInt32("0000FF", 16);

                //oDBGrid.getSBOGrid().CommonSetting.GetCellBackColor(1, 1);
                bool bRowValidated = true;
                bool bValidated = true;
                //int iDefaultRGB = 0;
                //if ((getWFValue(oForm, "iDefaultRGB") == null)){
                //    for (int r = 0; r <= oDBGrid.getRowCount() - 1; r++) {
                //        iDefaultRGB = oDBGrid.doGetRowColor(r);
                //        doAddWF(oForm, "iDefaultRGB", iDefaultRGB);
                //        break;
                //    }
                //}
                com.idh.bridge.DataHandler.INSTANCE.doInfo("Please wait while system is validating batch.");
                int iLastWOR = 0;
                bool bAdditionalItem = false;
                double dTemp = 0;
                for (int r = 0; r <= oDBGrid.getRowCount() - 1; r++) {
                    if (row != -1) {
                        r = row;
                        oDBGrid.getSBOGrid().CommonSetting.SetRowFontColor(r + 1, 0);
                    }
                    if (oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString() == string.Empty)
                        continue;
                    DataHandler.INSTANCE.doProgress("IDHVLDPBIBATCH", r, (int)(oDBGrid.getRowCount() - 1));
                    bRowValidated = true;
                    if (oDBGrid.doGetFieldValue(IDH_WRADHD._AddtItm, r).ToString() == string.Empty) {
                        bAdditionalItem = false;
                        iLastWOR = r;
                    } else {
                        bAdditionalItem = true;
                    }
                    

                    if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._CustCd, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer Code missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CustCd) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    } else if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._CustNm, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer name missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CustNm) + 1, RGBRed);
                        bRowValidated = false;
                    } else if (bAdditionalItem == false && !com.isb.enq.lookups.Config.INSTANCE.ValidateBPCodenName(oDBGrid.doGetFieldValue(IDH_WRADHD._CustCd, r).ToString(), oDBGrid.doGetFieldValue(IDH_WRADHD._CustNm, r).ToString().Replace("'", "''"))) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid Customer Code /Name.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CustCd) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    } else if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._CustAdr, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer address is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CustAdr) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    }  else if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._CustAdr, r).ToString() != string.Empty
                        && !com.isb.enq.lookups.Config.INSTANCE.ValidateBPAddress(oDBGrid.doGetFieldValue(IDH_WRADHD._CustCd, r).ToString().Replace("'", "''"), oDBGrid.doGetFieldValue(IDH_WRADHD._CustAdr, r).ToString().Replace("'","''"), "S", oDBGrid.doGetFieldValue(IDH_WRADHD._CusPCode, r).ToString().Replace("'", "''"))) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer address/postcode is not valid.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CustAdr) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    } else if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._JbTypeDs, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Order Type is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._JbTypeDs) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_WRADHD._JbTypeDs, r).ToString() != string.Empty && Config.INSTANCE.getValueFromTablebyKey("[@IDH_JOBTYPE]", "Code", "U_JobTp", oDBGrid.doGetFieldValue(IDH_WRADHD._JbTypeDs, r).ToString()) == null) {
                        //Config.INSTANCE.getValueFromTablebyKey("[@IDH_JOBTYPE]", "Code", "U_JobTp", oDBGrid.doGetFieldValue(IDH_WRADHD._JbTypeDs, r).ToString()).ToString();
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid Order Type. Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._JbTypeDs) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    } else if ((bAdditionalItem == false || (bAdditionalItem && oDBGrid.doGetFieldValue(IDH_WRADHD._Rebate, iLastWOR).ToString() == "")) && oDBGrid.doGetFieldValue(IDH_WRADHD._SuppCd, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Supplier code is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._SuppCd) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    } else if ((bAdditionalItem == false || (bAdditionalItem && oDBGrid.doGetFieldValue(IDH_WRADHD._Rebate, iLastWOR).ToString() == "")) && oDBGrid.doGetFieldValue(IDH_WRADHD._SuppNm, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Supplier name is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._SuppNm) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    } else if ((bAdditionalItem == false || (bAdditionalItem && oDBGrid.doGetFieldValue(IDH_WRADHD._Rebate, iLastWOR).ToString() == "")) && !com.isb.enq.lookups.Config.INSTANCE.ValidateBPCodenName(oDBGrid.doGetFieldValue(IDH_WRADHD._SuppCd, r).ToString(), oDBGrid.doGetFieldValue(IDH_WRADHD._SuppNm, r).ToString().Replace("'", "''"))) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid Supplier Code /Name.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._SuppCd) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    } else if (bAdditionalItem && oDBGrid.doGetFieldValue(IDH_WRADHD._Rebate, iLastWOR).ToString().ToUpper() == "Y" &&
                        (Config.INSTANCE.doGetLinkedBP(oDBGrid.doGetFieldValue(IDH_WRADHD._SuppCd, iLastWOR).ToString()) == null
                        || Config.INSTANCE.doGetLinkedBP(oDBGrid.doGetFieldValue(IDH_WRADHD._SuppCd, iLastWOR).ToString()).LinkedBPCardCode == null)) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid Linked customer for a rebate item.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._SuppCd) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    } else if ((bAdditionalItem && oDBGrid.doGetFieldValue(IDH_WRADHD._Rebate, iLastWOR).ToString().ToUpper() != "Y" && oDBGrid.doGetFieldValue(IDH_WRADHD._ItemCd, r).ToString() == string.Empty) || ((!bAdditionalItem && oDBGrid.doGetFieldValue(IDH_WRADHD._ItemCd, r).ToString() == string.Empty))) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Container code is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._ItemCd) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_WRADHD._ItemNm, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Container name is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._ItemNm) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    } else if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._WastCd, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Waste item code is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._WastCd) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    } else if (bAdditionalItem == false && bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._WasteNm, r).ToString() == string.Empty) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Waste item name is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._WasteNm) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    } else if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._WastCd, r).ToString() != string.Empty && !com.isb.enq.lookups.Config.INSTANCE.ValidateWasteItem(oDBGrid.doGetFieldValue(IDH_WRADHD._WastCd, r).ToString())) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Waste item: " + oDBGrid.doGetFieldValue(IDH_WRADHD._WastCd, r).ToString() + " is not a valid waste item.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._WastCd) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    } else if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._ItemCd, r).ToString() != string.Empty && !com.isb.enq.lookups.Config.INSTANCE.ValidateContainerItem(oDBGrid.doGetFieldValue(IDH_WRADHD._ItemCd, r).ToString())) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Container item: " + oDBGrid.doGetFieldValue(IDH_WRADHD._ItemCd, r).ToString() + " is not a valid container item.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._ItemCd) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    } else if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._JbTypeDs, r).ToString() != string.Empty && !com.isb.enq.lookups.Config.INSTANCE.ValidateJobTypeWithContainerItem(oDBGrid.doGetFieldValue(IDH_WRADHD._JbTypeDs, r).ToString(), oDBGrid.doGetFieldValue(IDH_WRADHD._ItemCd, r).ToString())) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Order Type : " + oDBGrid.doGetFieldValue(IDH_WRADHD._JbTypeDs, r).ToString() + " is not a valid with attached container.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._JbTypeDs) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_WRADHD._UOM, r).ToString() == string.Empty || !Config.INSTANCE.isValidUOM(oDBGrid.doGetFieldValue(IDH_WRADHD._UOM, r).ToString())) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("UOM is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._UOM) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    } else if (oDBGrid.doGetFieldValue(IDH_WRADHD._PUOM, r).ToString() != string.Empty && !Config.INSTANCE.isValidUOM(oDBGrid.doGetFieldValue(IDH_WRADHD._PUOM, r).ToString())) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Purchase UOM is invalid.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._PUOM) + 1, RGBRed);
                        //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                        bRowValidated = false;
                    } else if ((oDBGrid.doGetFieldValue(IDH_WRADHD._HlgQty, r).ToString() != string.Empty && double.TryParse(oDBGrid.doGetFieldValue(IDH_WRADHD._HlgQty, r).ToString(), out dTemp) == false)) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid haulage qty. Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._HlgQty) + 1, RGBRed);
                        bRowValidated = false;
                    } else if ((oDBGrid.doGetFieldValue(IDH_WRADHD._ExpLdWgt, r).ToString() != string.Empty && double.TryParse(oDBGrid.doGetFieldValue(IDH_WRADHD._ExpLdWgt, r).ToString(), out dTemp) == false)) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid expected qty. Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._ExpLdWgt) + 1, RGBRed);
                        bRowValidated = false;
                    } else if ((oDBGrid.doGetFieldValue(IDH_WRADHD._TChgPCst, r).ToString() != string.Empty && double.TryParse(oDBGrid.doGetFieldValue(IDH_WRADHD._TChgPCst, r).ToString(), out dTemp) == false)) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid numeric value for Tip Chrg/Pur Cost. Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._TChgPCst) + 1, RGBRed);
                        bRowValidated = false;
                    } else if ((oDBGrid.doGetFieldValue(IDH_WRADHD._CusChr, r).ToString() != string.Empty && double.TryParse(oDBGrid.doGetFieldValue(IDH_WRADHD._CusChr, r).ToString(), out dTemp) == false)) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid numeric value for Haulage Charge. Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CusChr) + 1, RGBRed);
                        bRowValidated = false;
                    } else if ((oDBGrid.doGetFieldValue(IDH_WRADHD._TipCost, r).ToString() != string.Empty && double.TryParse(oDBGrid.doGetFieldValue(IDH_WRADHD._TipCost, r).ToString(), out dTemp) == false)) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid numeric value for Disposal Cost. Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._TipCost) + 1, RGBRed);
                        bRowValidated = false;
                    } else if ((oDBGrid.doGetFieldValue(IDH_WRADHD._OrdCost, r).ToString() != string.Empty && double.TryParse(oDBGrid.doGetFieldValue(IDH_WRADHD._OrdCost, r).ToString(), out dTemp) == false)) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid numeric value for Haulage Cost. Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._OrdCost) + 1, RGBRed);
                        bRowValidated = false;
                    } else if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._Tip, r) != null && oDBGrid.doGetFieldValue(IDH_WRADHD._Tip, r).ToString() != string.Empty && (oDBGrid.doGetFieldValue(IDH_WRADHD._TipNm, r) == null || oDBGrid.doGetFieldValue(IDH_WRADHD._TipNm, r).ToString() == string.Empty)) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid Disposal Code /Name.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._Tip) + 1, RGBRed);
                        bRowValidated = false;
                    } else if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._TipNm, r) != null && oDBGrid.doGetFieldValue(IDH_WRADHD._TipNm, r).ToString() != string.Empty && (oDBGrid.doGetFieldValue(IDH_WRADHD._Tip, r) == null || oDBGrid.doGetFieldValue(IDH_WRADHD._Tip, r).ToString() == string.Empty)) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid Disposal Code /Name.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._Tip) + 1, RGBRed);
                        bRowValidated = false;
                    } else if (bAdditionalItem == false 
                            && oDBGrid.doGetFieldValue(IDH_WRADHD._Tip, r) != null 
                            && oDBGrid.doGetFieldValue(IDH_WRADHD._TipNm, r) != null && oDBGrid.doGetFieldValue(IDH_WRADHD._Tip, r).ToString() != string.Empty && oDBGrid.doGetFieldValue(IDH_WRADHD._TipNm, r).ToString() != string.Empty 
                            && !com.isb.enq.lookups.Config.INSTANCE.ValidateBPCodenName(oDBGrid.doGetFieldValue(IDH_WRADHD._Tip, r).ToString(), oDBGrid.doGetFieldValue(IDH_WRADHD._TipNm, r).ToString().Replace("'", "''"))) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Invalid Disposal Code /Name.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._Tip) + 1, RGBRed);
                        bRowValidated = false;
                    } else if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._SAddress, r) != null && oDBGrid.doGetFieldValue(IDH_WRADHD._SAddress, r).ToString() != string.Empty
                       && !com.isb.enq.lookups.Config.INSTANCE.ValidateBPAddress(oDBGrid.doGetFieldValue(IDH_WRADHD._Tip, r).ToString().Replace("'", "''"), oDBGrid.doGetFieldValue(IDH_WRADHD._SAddress, r).ToString().Replace("'", "''"), "S")) {
                        com.idh.bridge.DataHandler.INSTANCE.doUserError("Disposal site address is not valid.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                        oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._SAddress) + 1, RGBRed);
                        bRowValidated = false;
                    } else {
                        if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._WOH, r).ToString() != string.Empty && !oWOH.getByKey(oDBGrid.doGetFieldValue(IDH_WRADHD._WOH, r).ToString())) {
                            com.idh.bridge.DataHandler.INSTANCE.doUserError("Failed to load WOH#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOH, r).ToString() + ".");
                            oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                            oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._WOH) + 1, RGBRed);
                            bRowValidated = false;
                        }
                        if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._WOH, r).ToString() != string.Empty && oWOH.Code != string.Empty) {
                            if (oWOH.U_CardCd != oDBGrid.doGetFieldValue(IDH_WRADHD._CustCd, r).ToString()) {
                                com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer code is different than on WOH#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOH, r).ToString() + ".");
                                oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                                oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CustCd) + 1, RGBRed);
                                bRowValidated = false;
                            } else if (oWOH.U_CardNM != oDBGrid.doGetFieldValue(IDH_WRADHD._CustNm, r).ToString()) {
                                com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer name is different than on WOH#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOH, r).ToString() + ".");
                                oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                                oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CustNm) + 1, RGBRed);
                                bRowValidated = false;
                            } else if (oWOH.U_Address != oDBGrid.doGetFieldValue(IDH_WRADHD._CustAdr, r).ToString()) {
                                com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer address is different than on WOH#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOH, r).ToString() + ".");
                                oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                                oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CustAdr) + 1, RGBRed);
                                bRowValidated = false;
                            } else if (oWOH.U_CCardCd != oDBGrid.doGetFieldValue(IDH_WRADHD._SuppCd, r).ToString()) {
                                com.idh.bridge.DataHandler.INSTANCE.doUserError("Supplier code is different than on WOH#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOH, r).ToString() + ".");
                                oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                                oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._SuppCd) + 1, RGBRed);
                                bRowValidated = false;
                            } else if (oWOH.U_CCardNM != oDBGrid.doGetFieldValue(IDH_WRADHD._SuppNm, r).ToString()) {
                                com.idh.bridge.DataHandler.INSTANCE.doUserError("Supplier name is different than on WOH#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOH, r).ToString() + ".");
                                oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                                oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._SuppNm) + 1, RGBRed);
                                bRowValidated = false;
                            }
                        }
                        if (bRowValidated) {
                            if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._WOR, r).ToString() != string.Empty && !oWOR.getByKey(oDBGrid.doGetFieldValue(IDH_WRADHD._WOR, r).ToString())) {
                                com.idh.bridge.DataHandler.INSTANCE.doUserError("Failed to load WOR#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOR, r).ToString() + ".");
                                oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                                oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._WOR) + 1, RGBRed);
                                bRowValidated = false;
                            }
                            if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._WOR, r).ToString() != string.Empty && oWOR.Code != string.Empty) {
                                if (oWOR.U_CustCd != oDBGrid.doGetFieldValue(IDH_WRADHD._CustCd, r).ToString()) {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer code is different than on WOR#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOR, r).ToString() + ".");
                                    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                                    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CustCd) + 1, RGBRed);
                                    bRowValidated = false;
                                } else if (oWOR.U_CustNm != oDBGrid.doGetFieldValue(IDH_WRADHD._CustNm, r).ToString()) {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer name is different than on WOR#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOR, r).ToString() + ".");
                                    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                                    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CustNm) + 1, RGBRed);
                                    bRowValidated = false;
                                } else if (oWOR.U_ItemCd != oDBGrid.doGetFieldValue(IDH_WRADHD._ItemCd, r).ToString()) {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Container is different than on WOR#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOR, r).ToString() + ".");
                                    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                                    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._ItemCd) + 1, RGBRed);
                                    bRowValidated = false;
                                } else if (oWOR.U_WasCd != oDBGrid.doGetFieldValue(IDH_WRADHD._WastCd, r).ToString()) {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Waste material is different than on WOR#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOR, r).ToString() + ".");
                                    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                                    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._WastCd) + 1, RGBRed);
                                    bRowValidated = false;
                                } else if (!oWOR.doCheckCanEdit(false)) {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("WOR#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOR, r).ToString() + " already billed.");
                                    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                                    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._WOR) + 1, RGBRed);
                                    bRowValidated = false;
                                } else if (oWOR.U_Driver.ToUpper() == "Y" && Config.ParameterAsBool("SKPCNLWO", true)) {
                                    com.idh.bridge.DataHandler.INSTANCE.doUserError("WOR#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOR, r).ToString() + " has been cancelled, please uncancel to process.");
                                    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                                    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._ItemCd) + 1, RGBRed);
                                    bRowValidated = false;
                                }
                                //else if ( oWOR.U_JobTp != oDBGrid.doGetFieldValue(IDH_WRADHD._JbTypeDs, r).ToString()) {
                                //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Job Type is different than on WOR#" + oDBGrid.doGetFieldValue(IDH_WRADHD._WOR, r).ToString() + ".");
                                //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                                //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._JbTypeDs) + 1, RGBRed);
                                //    bRowValidated = false;
                                //} 
                            }
                        }

                    }
                    //}
                    //else if ((oDBGrid.doGetFieldValue(IDH_WRADHD._Qty, r).ToString() == string.Empty || Convert.ToInt32(oDBGrid.doGetFieldValue(IDH_WRADHD._Qty, r)) <= 0)) {
                    //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Actual Qty must be greater than zero. Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                    //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                    //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._Qty) + 1, RGBRed);
                    //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                    //    bRowValidated = false;
                    //}
                    //else if (bAdditionalItem == false && oDBGrid.doGetFieldValue(IDH_WRADHD._CusPCode, r).ToString() == string.Empty && Config.ParameterAsBool("IGNPCAWO", false) == false) {
                    //    com.idh.bridge.DataHandler.INSTANCE.doUserError("Customer postcode is missing.Code#" + oDBGrid.doGetFieldValue(IDH_WRADHD._Code, r).ToString());
                    //    oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "0");
                    //    oDBGrid.getSBOGrid().CommonSetting.SetCellFontColor(r + 1, oDBGrid.doIndexFieldWC(IDH_WRADHD._CusPCode) + 1, RGBRed);
                    //    //oDBGrid.getSBOGrid().SetCellFocus(r, oDBGrid.doIndexFieldWC(IDH_WRADHD._AmndTp));
                    //    bRowValidated = false;
                    //}
                    if (!bRowValidated) {
                        //  oDBGrid.getSBOGrid().CommonSetting.SetRowFontColor(r + 1, RGBRed);
                        bValidated = false;
                    } else { //if (oDBGrid.getSBOGrid().CommonSetting.GetRowFontColor(r + 1)==RGBRed)
                        oDBGrid.getSBOGrid().CommonSetting.SetRowFontColor(r + 1, 0);
                        oDBGrid.doSetFieldValue(IDH_WRADHD._VldStatus, r, "1");
                    }
                    if (row != -1) {
                        break;
                    }
                }
                if (!bValidated)
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Failed to validate batch rows. Please review the errors listed above.");
                else
                    com.idh.bridge.DataHandler.INSTANCE.doInfo("Batch validation completed.");
                return bValidated;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN",
                  new string[] { "IDH_WRADHD.doValidateAmendmentList" });
                return false;
            } finally {
                DataHandler.INSTANCE.doProgressDone("IDHVLDPBIBATCH");
            }
        }
         

        #endregion
        #region "Save Batch"
        private void doSaveBatch(SAPbouiCOM.Form oForm) {
            try {
                bool bret;
                DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");
                oGridN.doProcessData();
                oGridN.doReloadData(true);
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);
                oGridN.setInitialFilterValue("");

                //IDH_WRADHD oBatchDetail = (IDH_WRADHD)oGridN.DBObject;
                //idh.dbObjects.numbers.NumbersPair oNextNum;

                //IDH_WRADHH objBatch = new IDH_WRADHH();
                //if (oBatchDetail.Count >= 0) {
                //    oForm.Freeze(true);
                //    oBatchDetail.first();
                //    if (oBatchDetail.U_BatchNum == string.Empty) {
                //        oNextNum = objBatch.getNewKey();
                //        while (oBatchDetail.next() && oBatchDetail.Code!=string.Empty) {
                //            oBatchDetail.U_BatchNum = oNextNum.CodeCode;
                //        }
                //        objBatch.doAddEmptyRow(true);
                //        objBatch.Code = oNextNum.CodeCode;
                //        objBatch.Name = oNextNum.NameCode;
                //    }
                //    bret = oBatchDetail.doProcessData();
                //    bret = objBatch.doProcessData();
                //    doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);
                //    oGridN.setInitialFilterValue("");
                //}
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doSaveBatch" });
            } finally {
                oForm.Freeze(false);
            }
        }
        #endregion
        #region "Delete Batch"
        private void doDeleteBatch(SAPbouiCOM.Form oForm, string sComments) {
            try {
                string sBatches = getItemValue(oForm, "IDH_BATCHF").Trim();
                DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");
                // string sDeleteComments = "";
                string[] aBatches = sBatches.Split(',');
                IDH_WRADHH objBatchHeader = new IDH_WRADHH();
                objBatchHeader.UnLockTable = true;
                IDH_WRADHD objBatch = new IDH_WRADHD();
                objBatch.UnLockTable = true;
                bool bRet = true;
                DataHandler.INSTANCE.StartTransaction();
                int iRet = objBatchHeader.getData(IDH_WRADHH._Code + " In (" + sBatches + ") ", "");
                if (iRet > 0) {
                    objBatchHeader.first();
                    while (objBatchHeader.next()) {
                        objBatchHeader.U_BthStatus = "0";
                        objBatchHeader.U_Comment = sComments;
                        bRet = objBatchHeader.doUpdateDataRow("Batch Deleted");
                        if (!bRet) {
                            break;
                        }
                    }
                }
                if (bRet) {
                    //delete detailed batch
                    iRet = objBatch.getData(IDH_WRADHD._BatchNum + " In (" + sBatches + ") ", "");
                    if (iRet > 0) {
                        objBatch.first();
                        int iR = 0;
                        while (objBatch.next()) {
                            objBatch.U_BthStatus = "0";
                            //objBatch.U_Comments = sComments;
                            bRet = objBatch.doUpdateDataRow("Batch Deleted");
                            if (!bRet) {
                                break;
                            }
                            DataHandler.INSTANCE.doProgress("BATCHDELETE", iR++, iRet);
                        }
                    }
                }
                if (!bRet && DataHandler.INSTANCE.IsInTransaction()) {
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Failed to delete batched.");
                } else if (bRet && DataHandler.INSTANCE.IsInTransaction()) {
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText("Successfully deleted batches [" + sBatches + "]", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    doReLoadData(oForm, true);
                }


            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doDeleteBatch" });
            } finally {
                oForm.Freeze(false);
                DataHandler.INSTANCE.doProgressDone("BATCHDELETE");
            }
        }
        private void doPreDeleteBatch(SAPbouiCOM.Form oForm) {
            try {
                string sResourceMessage = "";
                if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Save") || ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update")) {
                    sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("SAVEFORM", null);
                    com.idh.bridge.DataHandler.INSTANCE.doError(sResourceMessage);
                    return;
                }
                string sBatches = getItemValue(oForm, "IDH_BATCHF").Trim();
                //  DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");

                if (sBatches == null || sBatches == string.Empty) {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Please provide batches to delete.");
                    doSetFocus(oForm, "IDH_BATCHF");
                    return;
                }
                doReLoadData(oForm, true);
                sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("LSRER006", new string[] { sBatches });

                if (IDHAddOns.idh.addon.Base.PARENT.doMessage(sResourceMessage, 2, "Yes", "No", null, true) != 1) {
                    return;
                }
                goParent.doOpenModalForm("IDH_COMMFRM", oForm);


            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doPreDeleteBatch" });
            } finally {
                oForm.Freeze(false);
            }
        }
        #endregion

        #region Import From Excel
        private void doImportExcel(SAPbouiCOM.Form oForm) {
            try {
                DBOGrid oGridN = (DBOGrid)getWFValue(oForm, "LINESGRID");
                bool bCancelled = false;
                string sFileName = "";
                doCallWinFormOpener(delegate { BrowserExcelFile(oForm, ref sFileName, ref bCancelled); }, oForm);
                //    sFileName = @"F:\ISB Projects\ISS\Dora\ISS_ServiceAmendments_Addhoc WORs_2.xlsx";
                if (bCancelled)
                    return;
                if (sFileName == null || sFileName.Trim() == "") {
                    com.idh.bridge.DataHandler.INSTANCE.doUserError("Please provide a valid file name.");
                    return;
                }

                com.idh.bridge.DataHandler.INSTANCE.doInfo("Please wait while system is importing file.");//, SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                doReadExcelFile(oForm, sFileName, oGridN);
                doFillGridCombos(oGridN);
                if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update")) {
                    doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE);
                }

                //
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error retrieving the Customers Address - " + sCardCode);
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new String[] { "Exporting All live services to csv." });
            }
        }
        private void doReadExcelFile(SAPbouiCOM.Form oForm, string sFileName, DBOGrid oDBGrid) {
            //  DBOGrid oDBGrid = (DBOGrid)getWFValue(oForm, "LINESGRID");
            try {
                string sNewBatch = "";
                ImportAdHocWORSheet oReadFile = new ImportAdHocWORSheet();
                oReadFile.doReadFileODBC(oDBGrid, oForm, sFileName, ref sNewBatch);
                FilterFields moFilterFields = oDBGrid.getGridControl().getFilterFields();
                try {
                    for (int i = 0; i <= moFilterFields.Count - 1; i++) {
                        FilterField moFilterFiled = (FilterField)moFilterFields[i];
                        setUFValue(oForm, moFilterFiled.msFieldName, "");
                    }
                } catch (Exception ex) {

                }
                //IDH_BATCHF

                setItemValue(oForm, "IDH_BATCHF", sNewBatch);
                oDBGrid.doReloadData("", false, true);
            } catch (Exception ex) {
                //DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error retrieving the Customers Address - " + sCardCode);
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new String[] { "Error in doReadExcelFile." });
            }
        }
        public void BrowserExcelFile(SAPbouiCOM.Form oForm, ref string sFileName, ref bool bCancelled) {
            System.Windows.Forms.OpenFileDialog oOpenFile = new System.Windows.Forms.OpenFileDialog();
            try {
                string sExt = Config.INSTANCE.getImportExtension();
                if (sExt != null && sExt.Length > 0) {
                    if (sExt.IndexOf("|") == -1) {
                        sExt = "(" + sExt + ")|" + sExt;
                    }
                    oOpenFile.Filter = sExt;
                }

            } catch (Exception ex) {
                //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error importing the data.");
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXIDT", new string[] { string.Empty });
            }
            oOpenFile.FileName = "OpenExcelFile";
            oOpenFile.InitialDirectory = com.isb.enq.lookups.Config.INSTANCE.getImportLiveServicesFolder();
            if (com.idh.win.AppForm.OpenFileDialog(oOpenFile) == System.Windows.Forms.DialogResult.OK) {
                sFileName = oOpenFile.FileName;
            } else
                bCancelled = true;
        }
        #region "Read and Process Excel File"
        public class ImportAdHocWORSheet {
            public static int miMinRows = 6;
            public static string SheetName = "Service Amendments";
            public static string[] TRIGGERS = {"SrNo","Customer-Code",  "Customer-Name",    "Customer Site Address",    "Post Code",    "Order Type",
                "Supplier-Code",    "Supplier-Name", "Ref #","Supplier Address","Supplier Postcode",       "Maximo number (Tonnage to be calc'd yes/no)",  "Additional Item ",
                "Container/Additional Code",    "Container/Additional", "Waste Code",   "Waste Material",   "Unit of Measure (UOM)","Purchase Unit of Measure (PUOM)",
                "Actual Qty", "Haulage Qty","Expected Qty", "W O H","WOR",
                "Request Date", "Start Date",   "End Date",
                "Disposal Cost", "Haulage Cost", "Disposal Charge/Purchase Cost",
                "Haulage Charge",
                "Rebate",   "Comments","Disposal Site" ,"Disposal Site Name" ,"Disposal Site Address"};
            public class ColPos {

                public static int SrNo = 0;
                public static int CustomerCode = 1;
                public static int CustomerName = 2;
                public static int CustomerSiteAddress = 3;

                public static int PostCode = 4;
                public static int OrderType = 5;
                public static int SupplierCode = 6;
                public static int SupplierName = 7;
                public static int SupplierReference = 8;

                public static int SupplierAddress = 9;
                public static int SupplierPostcode = 10;
                //public static int Supplier Name =  ;
                public static int MaximoNum = 11;
                public static int AdditionalItem = 12;
                public static int ContainerCode = 13;
                public static int Container = 14;
                public static int WasteMaterialCode = 15;
                public static int WasteMaterial = 16;

                public static int UnitofMeasure = 17;
                public static int PurchaseUnitofMeasure = 18;
                public static int ActualQty = 19;
                public static int HaulageQty = 20;
                public static int ExpectedQty = 21;
                public static int WOH = 22;
                public static int WOR = 23;
                public static int RequestDate = 24;
                public static int StartDate = 25;
                public static int EndDate = 26;

                public static int DisposalCost = 27;
                public static int HaulageCost = 28;
                public static int DisposalCharge_PurchaseCost = 29;
                public static int HaulageCharge = 30;

                public static int Rebate = 31;
                public static int Comments = 32;

                public static int DisposalSite = 33;
                public static int DisposalSiteName = 34;
                public static int DisposalSiteAddress = 35;
            }


            public ImportAdHocWORSheet() { }
            public void doReadFileODBC(DBOGrid oGridN, SAPbouiCOM.Form oForm, string sExcelFile, ref string sNewBatch) {

                bool bItemsFound = false;
                //string sTempWORCSVFile = "";
                string sFilter;
                try {
                    miMinRows = Config.INSTANCE.getParameterAsInt("WOUPFRNM", 6, false) + 1;
                    SheetName = Config.INSTANCE.getParameter("WOUPSHNM");

                    System.Data.DataTable dtExcel = doReadExcelFile_ExcelReader(sExcelFile);
                    if (dtExcel == null) {
                        return;
                    }
                    //public static int miMinRows = 8;
                    //public static string SheetName = "Service Amendments";
                    if (true) {
                        if (dtExcel.Columns.Count > 29) {
                            if (dtExcel.Columns[ColPos.SrNo].ColumnName.Equals(TRIGGERS[0], StringComparison.OrdinalIgnoreCase) &&
                                     dtExcel.Columns[ColPos.CustomerCode].ColumnName.Equals(TRIGGERS[1], StringComparison.OrdinalIgnoreCase) &&
                                     dtExcel.Columns[ColPos.CustomerName].ColumnName.Equals(TRIGGERS[2], StringComparison.OrdinalIgnoreCase) &&
                                      dtExcel.Columns[ColPos.CustomerSiteAddress].ColumnName.Equals(TRIGGERS[3], StringComparison.OrdinalIgnoreCase) &&
                                       dtExcel.Columns[ColPos.PostCode].ColumnName.Equals(TRIGGERS[4], StringComparison.OrdinalIgnoreCase) &&
                                        dtExcel.Columns[ColPos.OrderType].ColumnName.Equals(TRIGGERS[5], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.SupplierCode].ColumnName.Equals(TRIGGERS[6], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.SupplierName].ColumnName.Equals(TRIGGERS[7], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.SupplierReference].ColumnName.Equals(TRIGGERS[8], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.SupplierAddress].ColumnName.Equals(TRIGGERS[9], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.SupplierPostcode].ColumnName.Equals(TRIGGERS[10], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.MaximoNum].ColumnName.Equals(TRIGGERS[11], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.AdditionalItem].ColumnName.Equals(TRIGGERS[12], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.ContainerCode].ColumnName.Equals(TRIGGERS[13], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.Container].ColumnName.Equals(TRIGGERS[14], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.WasteMaterialCode].ColumnName.Equals(TRIGGERS[15], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.WasteMaterial].ColumnName.Equals(TRIGGERS[16], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.UnitofMeasure].ColumnName.Equals(TRIGGERS[17], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.PurchaseUnitofMeasure].ColumnName.Equals(TRIGGERS[18], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.ActualQty].ColumnName.Equals(TRIGGERS[19], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.HaulageQty].ColumnName.Equals(TRIGGERS[20], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.ExpectedQty].ColumnName.Equals(TRIGGERS[21], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.WOH].ColumnName.Equals(TRIGGERS[22], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.WOR].ColumnName.Equals(TRIGGERS[23], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.RequestDate].ColumnName.Equals(TRIGGERS[24], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.StartDate].ColumnName.Equals(TRIGGERS[25], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.EndDate].ColumnName.Equals(TRIGGERS[26], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.DisposalCost].ColumnName.Equals(TRIGGERS[27], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.HaulageCost].ColumnName.Equals(TRIGGERS[28], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.DisposalCharge_PurchaseCost].ColumnName.Equals(TRIGGERS[29], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.HaulageCharge].ColumnName.Equals(TRIGGERS[30], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.Rebate].ColumnName.Equals(TRIGGERS[31], StringComparison.OrdinalIgnoreCase) &&
                                         dtExcel.Columns[ColPos.Comments].ColumnName.Equals(TRIGGERS[32], StringComparison.OrdinalIgnoreCase)

                                ) {

                                oGridN.DBObject.doClearBuffers();
                                sFilter = oGridN.getRequiredFilter();
                                oGridN.setRequiredFilter(IDH_WRADHD._BatchNum + " =\'-999\' ");
                                oGridN.doReloadData("", false, true);
                                oGridN.doApplyRules();
                                oGridN.doSetDoCount(true);
                                oGridN.setRequiredFilter(sFilter);

                                oGridN.AddEditLine = true;
                                //IDH_LVSAMD oAmendList = new IDH_LVSAMD();
                                //oAmendList.UnLockTable = true;
                                DataRecords oRS = null;
                                IDH_WRADHH oBatch = new IDH_WRADHH();
                                /////////////////////////////////
                                idh.dbObjects.numbers.NumbersPair oNextNum;
                                oNextNum = oBatch.getNewKey();
                                sNewBatch = oNextNum.CodeCode;
                                oBatch.doAddEmptyRow(true);
                                oBatch.Code = oNextNum.CodeCode;
                                oBatch.Name = oNextNum.NameCode;

                                bool bret = oBatch.doProcessData();
                                /////////////////////////////////
                                //string sName="",sCode="";
                                IDH_WRADHD oBatchDetail = new IDH_WRADHD();
                                oBatchDetail.UnLockTable = true;
                                com.idh.bridge.DataHandler.INSTANCE.doInfo("Please wait while system is importing Adhoc WO sheet using SSIS Package.");


                                //"Disposal Site","Disposal Site Name"
                                if (!dtExcel.Columns.Contains(TRIGGERS[ColPos.DisposalSite]))
                                    dtExcel.Columns.Add(TRIGGERS[ColPos.DisposalSite], typeof(string));

                                if (!dtExcel.Columns.Contains(TRIGGERS[ColPos.DisposalSiteName]))
                                    dtExcel.Columns.Add(TRIGGERS[ColPos.DisposalSiteName], typeof(string));

                                if (!dtExcel.Columns.Contains(TRIGGERS[ColPos.DisposalSiteAddress]))
                                    dtExcel.Columns.Add(TRIGGERS[ColPos.DisposalSiteAddress], typeof(string));

                          
                                //System.Data.DataTable dt = doReadExcelFile_ExcelReader(sExcelFile);
                                dtExcel.Columns.Add("Code", typeof(string));
                                dtExcel.Columns.Add("Name", typeof(string));
                                dtExcel.Columns.Add("BatchNumber", typeof(string));

                                dtExcel.Columns.Add("ProcSts", typeof(string));
                                dtExcel.Columns.Add("VldStatus", typeof(string));
                                dtExcel.Columns.Add("BthStatus", typeof(string));
                                dtExcel.Columns.Add("ParentID", typeof(string));

                                
                                if (!dtExcel.Columns.Contains(IDH_WRADHD._AddedBy))
                                    dtExcel.Columns.Add(IDH_WRADHD._AddedBy, typeof(string));

                                string sBatchNum = sNewBatch;
                                //dtExcel.AsEnumerable().Select(b => b["Code"] = oBatchDetail.getNewKey().CodeCode).ToList();
                                string sLastParent = "";

                                dtExcel.DefaultView.Sort = "SrNo";
                                dtExcel = dtExcel.DefaultView.ToTable();

                                //dtExcel.Select().ToList<DataRow>()
                                //.ForEach(r => {
                                //    r["BatchNumber"] = sBatchNum; r["ProcSts"] = "0"; r["VldStatus"] = ""; r["BthStatus"] = "1";
                                //    r["Code"] = oBatchDetail.getNewKey().CodeCode; r["Name"] = r["Code"];
                                //    r[TRIGGERS[ColPos.CustomerCode]] = doGetSBOKey("OCRD", "CardName", "CardCode", r[TRIGGERS[ColPos.CustomerName]].ToString(), oRS, "CardType", "C");
                                //    r[TRIGGERS[ColPos.SupplierCode]] = doGetSBOKey("OCRD", "CardName", "CardCode", r[TRIGGERS[ColPos.SupplierName]].ToString(), oRS, "CardType", "S");
                                //    r[TRIGGERS[ColPos.ContainerCode]] = doGetSBOKey("OITM", "ItemName", "ItemCode", r[TRIGGERS[ColPos.Container]].ToString(), oRS, "", "");
                                //    r[TRIGGERS[ColPos.WasteMaterialCode]] = doGetSBOKey("OITM", "ItemName", "ItemCode", r[TRIGGERS[ColPos.WasteMaterial]].ToString(), oRS, "", "");
                                //    if (!r[TRIGGERS[ColPos.AdditionalItem]].ToString().Equals("Y", StringComparison.OrdinalIgnoreCase))
                                //        sLastParent = r["Code"].ToString();
                                //    else
                                //        r["ParentID"] = sLastParent;
                                //});

                                dtExcel.Rows.Cast<DataRow>().Where(
                                          r => (r[TRIGGERS[ColPos.Container]] == DBNull.Value || r[TRIGGERS[ColPos.Container]] == null || r[TRIGGERS[ColPos.Container]].ToString() == string.Empty)).ToList().ForEach(r => r.Delete());


                                dtExcel.Select().ToList<DataRow>()
                               .ForEach(r => {
                                   r["BatchNumber"] = sBatchNum; r["ProcSts"] = "0"; r["VldStatus"] = ""; r["BthStatus"] = "1";
                                   r["Code"] = oBatchDetail.getNewKey().CodeCode; r["Name"] = r["Code"];
                                   if (!r[TRIGGERS[ColPos.AdditionalItem]].ToString().ToUpper().Equals("Y", StringComparison.OrdinalIgnoreCase))
                                       sLastParent = r["Code"].ToString();
                                   else
                                       r["ParentID"] = sLastParent;
                                   if (r[TRIGGERS[ColPos.ActualQty]] == null || r[TRIGGERS[ColPos.ActualQty]].ToString().Trim() == "") {
                                       r[TRIGGERS[ColPos.ActualQty]] = 0;
                                   }

                                   r[IDH_WRADHD._AddedBy] = "WR1";
                               });
                                
                                dtExcel.AcceptChanges();
                                
                                string sODBC = Config.INSTANCE.getParameter("REPDSN");
                                string sODBCUser = Config.INSTANCE.getParameter("REPUSR");
                                string sODBCPwd = Config.INSTANCE.getParameter("REPPAS");

                                
                                DataHandler.INSTANCE.StartTransaction();

                                using (System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection("Data Source=" + DataHandler.INSTANCE.SBOCompany.Server + ";Initial Catalog=" + DataHandler.INSTANCE.DBName + ";User ID=" + sODBCUser + ";Password=" + sODBCPwd + ";")) {
                                    cn.Open();
                                    using (System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy(cn)) {
                                        bulkCopy.DestinationTableName =
                                            "dbo.[" + IDH_WRADHD.TableName + "]";

                                        try {
                                            bulkCopy.BatchSize = 500;
                                            //////
                                            bulkCopy.ColumnMappings.Add("Code", IDH_WRADHD._Code);
                                            bulkCopy.ColumnMappings.Add("Name", IDH_WRADHD._Name);

                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SrNo], IDH_WRADHD._SrNo);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CustomerCode], IDH_WRADHD._CustCd);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CustomerName], IDH_WRADHD._CustNm);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.CustomerSiteAddress], IDH_WRADHD._CustAdr);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.PostCode], IDH_WRADHD._CusPCode);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.OrderType], IDH_WRADHD._JbTypeDs);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SupplierCode], IDH_WRADHD._SuppCd);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SupplierName], IDH_WRADHD._SuppNm);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SupplierReference], IDH_WRADHD._SuppRef);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SupplierAddress], IDH_WRADHD._SupAddr);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.SupplierPostcode], IDH_WRADHD._SupPCode);

                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.MaximoNum], IDH_WRADHD._MaximoNum);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.AdditionalItem], IDH_WRADHD._AddtItm);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.ContainerCode], IDH_WRADHD._ItemCd);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Container], IDH_WRADHD._ItemNm);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.WasteMaterialCode], IDH_WRADHD._WastCd);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.WasteMaterial], IDH_WRADHD._WasteNm);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.UnitofMeasure], IDH_WRADHD._UOM);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.PurchaseUnitofMeasure], IDH_WRADHD._PUOM);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.ActualQty], IDH_WRADHD._Qty);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.HaulageQty], IDH_WRADHD._HlgQty);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.ExpectedQty], IDH_WRADHD._ExpLdWgt);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.WOH], IDH_WRADHD._WOH);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.WOR], IDH_WRADHD._WOR);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.RequestDate], IDH_WRADHD._RDate);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.StartDate], IDH_WRADHD._SDate);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.EndDate], IDH_WRADHD._EDate);

                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.DisposalCharge_PurchaseCost], IDH_WRADHD._TChgPCst);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.HaulageCharge], IDH_WRADHD._CusChr);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.DisposalCost], IDH_WRADHD._TipCost);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.HaulageCost], IDH_WRADHD._OrdCost);

                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Rebate], IDH_WRADHD._Rebate);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.Comments], IDH_WRADHD._Comments);

                                            bulkCopy.ColumnMappings.Add("VldStatus", IDH_WRADHD._VldStatus);
                                            bulkCopy.ColumnMappings.Add("BthStatus", IDH_WRADHD._BthStatus);
                                            bulkCopy.ColumnMappings.Add("ProcSts", IDH_WRADHD._ProcSts);
                                            bulkCopy.ColumnMappings.Add("BatchNumber", IDH_WRADHD._BatchNum);
                                            bulkCopy.ColumnMappings.Add("ParentID", IDH_WRADHD._ParentID);


                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.DisposalSite], IDH_WRADHD._Tip);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.DisposalSiteName], IDH_WRADHD._TipNm);
                                            bulkCopy.ColumnMappings.Add(TRIGGERS[ColPos.DisposalSiteAddress], IDH_WRADHD._SAddress);

                                            bulkCopy.ColumnMappings.Add(IDH_WRADHD._AddedBy, IDH_WRADHD._AddedBy);

                                            //////
                                            // Write from the source to the destination.
                                            bulkCopy.WriteToServer(dtExcel);
                                            bItemsFound = true;
                                            DataHandler.INSTANCE.doProgressDone("IDHIMPLSAMD");
                                            com.idh.bridge.DataHandler.INSTANCE.doInfo("Adhoc WO import process completed.");
                                            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                            com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(Translation.getTranslatedMessage("EXPEXLDN", "Successfully import the file"), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                                        } catch (Exception ex) {
                                            bItemsFound = false;
                                            DataHandler.INSTANCE.doResSBOError(ex.Message, "EREXSGEN", new String[] { "Failed Copying Data to SBO Table" });
                                            //Console.WriteLine(ex.Message);
                                        }
                                    }
                                }

                            }// end of col header validation
                            else {
                                com.idh.bridge.DataHandler.INSTANCE.doUserError("Header validation failed");
                                return;
                            }
                        }//end if icol>31
                    }
                } catch (Exception e) {
                    //DataHandler.INSTANCE.doError("Exception: " + e.ToString(), "Error importing the Journals.");
                    com.idh.bridge.DataHandler.INSTANCE.doResSBOError(e.Message, "EREXIMPJ", null);
                    bItemsFound = false;
                    if (DataHandler.INSTANCE.IsInTransaction())
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                } finally {
                    if (DataHandler.INSTANCE.IsInTransaction())
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    if (oGridN != null) {
                        oGridN.AddEditLine = false;
                        DataHandler.INSTANCE.doProgressDone("IDHIMPLSAMD");
                    }

                }

                if (!bItemsFound) {
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("This is not a valid Import File - " + sExcelFile, "ERUSINIF", new String[] { sExcelFile });
                }

            }
            private string doGetSBOKey(string sTableName, string sKeyField, string sFiledName, string sKeyValue, DataRecords oRS, string sTypeField, string sType) {
                try {
                    // sName = Conversions.ToString(dt.Rows[r][TRIGGERS[ColPos.CustomerName]]);// oExcel.getValue(r, )).Trim();
                    string sCode = "";
                    if (sKeyValue != string.Empty) {
                        if (sType != "")
                            oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGet" + sTableName + "CodeByName" + sKeyValue, "Select Top 1 " + sFiledName + " from " + sTableName + " with(nolock) Where " + sKeyField + "=\'" + sKeyValue + "\' And " + sTypeField + "=\'" + sType + "\'");
                        else
                            oRS = DataHandler.INSTANCE.doBufferedSelectQuery("doGet" + sTableName + "CodeByName" + sKeyValue, "Select Top 1 " + sFiledName + " from " + sTableName + " with(nolock) Where " + sKeyField + "=\'" + sKeyValue + "\' ");

                        if (oRS != null && oRS.RecordCount > 0) {
                            sCode = oRS.getValueAsString(sFiledName).Trim();
                        }
                    }
                    return sCode;
                } catch (Exception e) {
                    //DataHandler.INSTANCE.doError("Exception: " + e.ToString(), "Error importing the Journals.");
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(e.Message, "EREXIMPJ", new string[] { "doGetSBOKey" });
                }
                return "";

            }

            public System.Data.DataTable doReadExcelFile_ExcelReader(string sFileName) {
                try {
                    // int iFirstRow = Config.INSTANCE.getParameterAsInt("WOUPFRNM",7,false);
                    // string sSheetName= Config.INSTANCE.getParameter("WOUPSHNM");

                    FileStream stream = File.Open(sFileName, FileMode.Open, FileAccess.Read);
                    Excel.IExcelDataReader excelReader;
                    System.Data.DataTable dtdataTable = new System.Data.DataTable();
                    ////3. DataSet - The result of each spreadsheet will be created in the result.Tables
                    //DataSet result = excelReader.AsDataSet();
                    if (sFileName.ToLower().EndsWith(".xlsx")) {
                        //97+ .xlslx
                        //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                        excelReader = Excel.ExcelReaderFactory.CreateOpenXmlReader(stream);
                        //4. DataSet - Create column names from first row
                        excelReader.FirstRow = miMinRows - 1;
                        excelReader.SheetNameToRead = SheetName;
                        excelReader.IsFirstRowAsColumnNames = true;
                        //int r = excelReader.cou[;

                        dtdataTable = excelReader.AsDataSet().Tables[0];

                        //dtdataTable.Columns.Add("Code", new Type() { "string" });
                        excelReader.Close();
                    } else {
                        //2003-97
                        //.xls
                        //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                        excelReader = Excel.ExcelReaderFactory.CreateBinaryReader(stream);
                        //4. DataSet - Create column names from first row
                        excelReader.IsFirstRowAsColumnNames = true;
                        excelReader.FirstRow = miMinRows - 1;
                        excelReader.SheetNameToRead = SheetName;
                        dtdataTable = excelReader.AsDataSet().Tables[0];
                        excelReader.Close();
                    }
                    //using (System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy("CONNECTION_STRING")) {
                    //    bulkCopy.DestinationTableName =
                    //        "dbo.[" + sTableName + "]";

                    //    try {
                    //        // Write from the source to the destination.
                    //        bulkCopy.WriteToServer(dsdataSet.Tables[0]);
                    //    } catch (Exception ex) {
                    //        Console.WriteLine(ex.Message);
                    //    }
                    //}
                    return dtdataTable;
                } catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doReadExcelFile_ExcelReader" });
                }
                return null;
            }

            private string noNull(object oValue) {
                return oValue == null ? "" : oValue.ToString();
            }

            private static string doFixStr(object oValue, int iLength) {
                string sVal = oValue == null ? "" : oValue.ToString();
                sVal = (sVal.Length > iLength ? sVal.Substring(0, (iLength - 3)) + "..." : sVal);
                return sVal;
            }
        }
        #endregion
        #endregion

    }//end of class

}//end of namespace
