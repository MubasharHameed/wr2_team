﻿using System;
using System.Collections;
using System.Xml;

using com.idh.dbObjects.User;
using com.idh.utils;
using com.idh.bridge.resources;
using com.idh.bridge;
using com.idh.controls;
//using com.isb.bridge.lookups;

using System.IO;

namespace com.isb.enq.lookups {

    public class Config : com.isb.bridge.lookups.Config {

        private static Config moConfig;

        public static Config INSTANCE {
            get {
                if (moConfig == null) {
                    if (IDHAddOns.idh.addon.Base.PARENT.goDICompany != null) {
                        moConfig = new Config(ref IDHAddOns.idh.addon.Base.PARENT.goDICompany, "[@" + IDHAddOns.idh.addon.Base.PARENT.goConfigTable + "]");
                    } else {
                        moConfig = new Config("[@" + IDHAddOns.idh.addon.Base.PARENT.goConfigTable + "]");
                    }
                }
                return moConfig;
            }
            set { moConfig = value; }
        }

        public Config(ref SAPbobsCOM.Company oSBOCompany, string sConfigTable) : base(oSBOCompany, sConfigTable) {
            INSTANCE = this;
        }

        public Config(string sConfigTable) : base(sConfigTable) {
            INSTANCE = this;
        }

        //public Config() : base() {
        //    INSTANCE = this;
        //}

        public string getImportLiveServicesFolder() {
            return com.isb.bridge.lookups.Config.INSTANCE.getParameterWithDefault("IMPDIRLS", "C:\\Temp\\");
        }

        public bool ValidateBPAddress(string sCardCode, string sAddress, string sAddressType) {
            if (sAddress == null || sAddress == string.Empty || sCardCode == null || sCardCode.Length == 0 || sCardCode.Equals("DONA"))
                return false;

            string sQry = null;


            //sWasteGroup = 
            sQry = "Select [Address] from CRD1 With(NoLock) Where CardCode=\'" + sCardCode + "\' And [Address]=\'" + sAddress + "\' and AdresType=\'" + sAddressType + "\'";

            DataRecords oResult = null;
            try {
                oResult = DataHandler.INSTANCE.doBufferedSelectQuery(sCardCode + sAddress + sAddress + "ValidateBPAddress", sQry);
                if (oResult != null && oResult.RecordCount > 0) {
                    return true;
                    //oValue = oResult.getValue(0);
                } else
                    return false; ;
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error getting OCRD info with no buffer.");
                return false;
            } finally {
                DataHandler.INSTANCE.doReleaseDataRecords(ref oResult);
            }
            //return oValue;
        }

        public bool ValidateBPAddress(string sCardCode, string sAddress, string sAddressType, string sPostCode) {
            if (sAddress == null || sAddress == string.Empty || sCardCode == null || sCardCode.Length == 0 || sCardCode.Equals("DONA"))
                return false;

            string sQry = null;

            //sWasteGroup = 
            sQry = "Select [Address] from CRD1 With(NoLock) Where CardCode=\'" + sCardCode + "\' And [Address]=\'" + sAddress + "\' And isnull([ZipCode],'')=\'" + sPostCode + "\' and AdresType=\'" + sAddressType + "\'";

            DataRecords oResult = null;
            try {
                oResult = DataHandler.INSTANCE.doBufferedSelectQuery(sCardCode + sAddress + sAddress + "ValidateBPAddress", sQry);
                if (oResult != null && oResult.RecordCount > 0) {
                    return true;
                    //oValue = oResult.getValue(0);
                } else
                    return false; ;
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error getting OCRD info with no buffer.");
                return false;
            } finally {
                DataHandler.INSTANCE.doReleaseDataRecords(ref oResult);
            }
            //return oValue;
        }

        public bool ValidateBPCodenName(string sCardCode, string sCardName) {
            if (sCardCode == null || sCardCode.Length == 0 || sCardCode.Equals("DONA"))
                return false;

            string sQry = null;


            //sWasteGroup = 
            sQry = "Select CardCode from OCRD WITH(NOLOCK) Where CardCode=\'" + sCardCode + "\' And  CardName=\'" + sCardName + "\' " +
                com.isb.bridge.lookups.Config.INSTANCE.getActiveCheckSQL("OCRD", DateTime.Now);

            DataRecords oResult = null;
            try {
                oResult = DataHandler.INSTANCE.doBufferedSelectQuery(sCardCode + "ValidateBPCodenName", sQry);
                if (oResult != null && oResult.RecordCount > 0) {
                    return true;
                    //oValue = oResult.getValue(0);
                } else
                    return false; ;
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error getting OCRD info with no buffer.");
                return false;
            } finally {
                DataHandler.INSTANCE.doReleaseDataRecords(ref oResult);
            }
            //return oValue;
        }

        public bool ValidateJobTypeWithContainerItem(string sJobType, string sContainerCode) {
            if (sContainerCode == null || sContainerCode.Length == 0 || sContainerCode.Equals("DONA"))
                return false;

            string sQry = null;
            object oValue = null;
            //string sWasteGroup = Config.Parameter("IGR-WM");
            //sWasteGroup = 
            //if (string.IsNullOrEmpty(sWasteGroup))
            //    sQry = "SELECT top 1 ItemCode FROM OITM with(nolock) WHERE ItemCode = \'" + sItemCode + "\' ";
            //else
            //    sQry = "SELECT top 1 ItemCode FROM OITM with(nolock) WHERE ItemCode = \'" + sItemCode + "\' And ItmsGrpCod Not in (" + sWasteGroup + ")";
            sQry = "Select jbtyp.U_JobTp,jbtyp.U_ItemGrp from [@IDH_JOBTYPE] jbtyp with(nolock) " +
                    " Inner Join OITB with(nolock) on OITB.ItmsGrpCod = jbtyp.U_ItemGrp " +
                    " Inner join OITM with(nolock) on OITM.ItmsGrpCod = OITB.ItmsGrpCod " +
                    " where U_JobTp = \'" + sJobType + "\' And OITM.ItemCode = \'" + sContainerCode + "\' ";

            DataRecords oResult = null;
            try {
                oResult = DataHandler.INSTANCE.doBufferedSelectQuery(sJobType + "ValidJobTypeWithContainerCode", sQry);
                if (oResult != null && oResult.RecordCount > 0) {
                    return true;
                    //oValue = oResult.getValue(0);
                } else
                    return false; ;
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error getting OITM info with no buffer.");
                return false;
            } finally {
                DataHandler.INSTANCE.doReleaseDataRecords(ref oResult);
            }
            //return oValue;
        }

        public bool ValidateContainerItem(string sItemCode) {
            if (sItemCode == null || sItemCode.Length == 0 || sItemCode.Equals("DONA"))
                return false;

            string sQry = null;
            object oValue = null;
            string sWasteGroup = com.isb.bridge.lookups.Config.Parameter("IGR-WM");
            //sWasteGroup = 
            if (string.IsNullOrEmpty(sWasteGroup))
                sQry = "SELECT top 1 ItemCode FROM OITM with(nolock) WHERE ItemCode = \'" + sItemCode + "\' ";
            else
                sQry = "SELECT top 1 ItemCode FROM OITM with(nolock) WHERE ItemCode = \'" + sItemCode + "\' And ItmsGrpCod Not in (" + sWasteGroup + ")";

            DataRecords oResult = null;
            try {
                oResult = DataHandler.INSTANCE.doBufferedSelectQuery(sItemCode + "ValidContainerCode", sQry);
                if (oResult != null && oResult.RecordCount > 0) {
                    return true;
                    //oValue = oResult.getValue(0);
                } else
                    return false; ;
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error getting OITM info with no buffer.");
                return false;
            } finally {
                DataHandler.INSTANCE.doReleaseDataRecords(ref oResult);
            }
            //return oValue;
        }
        public bool ValidateWasteItem(string sItemCode) {
            if (sItemCode == null || sItemCode.Length == 0 || sItemCode.Equals("DONA"))
                return false;

            string sQry = null;
            object oValue = null;
            string sWasteGroup = com.isb.bridge.lookups.Config.Parameter("IGR-WM");
            //sWasteGroup = 
            if (string.IsNullOrEmpty(sWasteGroup))
                sQry = "SELECT top 1 ItemCode FROM OITM with(nolock) WHERE ItemCode = \'" + sItemCode + "\' ";
            else
                sQry = "SELECT top 1 ItemCode FROM OITM with(nolock) WHERE ItemCode = \'" + sItemCode + "\' And ItmsGrpCod in (" + sWasteGroup + ")";

            DataRecords oResult = null;
            try {
                oResult = DataHandler.INSTANCE.doBufferedSelectQuery(sItemCode + "ValidWasteCode", sQry);
                if (oResult != null && oResult.RecordCount > 0) {
                    return true;
                    //oValue = oResult.getValue(0);
                } else
                    return false; ;
            } catch (Exception ex) {
                DataHandler.INSTANCE.doError("Exception: " + ex.ToString(), "Error getting OITM info with no buffer.");
                return false;
            } finally {
                DataHandler.INSTANCE.doReleaseDataRecords(ref oResult);
            }
            //return oValue;
        }
    }
}
