﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using IDHAddOns.idh.controls;
using com.idh.bridge.lookups;

namespace com.isb.forms.Enquiry.Search {
    public class WasteGroupSearch : IDHAddOns.idh.forms.Search {

        public WasteGroupSearch(IDHAddOns.idh.addon.Base oParent)
            : base(oParent, "IDH_WGSRCH", "Enq_WasteGroup Search.srf", 5, 30, 485, 330, "Waste Group Search") {
        }

        public WasteGroupSearch(IDHAddOns.idh.addon.Base oParent, string sType, string sSRF, int iGL, int iGT, int iGW, int iGH, string sTitle)
            : base(oParent, sType, sSRF, iGL, iGT, iGW, iGH, sTitle) {
        }


        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            this.setParentSharedData(oForm, "IDH_WGSRCHOPEND", "LOADED");
            base.doLoadData(oForm);
        }

        protected override void doSetEventFilters() {
            base.doSetEventFilters();
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD);
        }
        //## End

        public override void doSetGridOptions(FilterGrid oGridN) {

            oGridN.doAddGridTable(new com.idh.bridge.data.GridTable("@ISB_WASTEGRP", "a", null, false, true));

            oGridN.doAddFilterField("IDH_WGCOD", "a.U_WstGpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 20, null, true);
            oGridN.doAddFilterField("IDH_NAME", "a.U_WstGpNm", SAPbouiCOM.BoDataType.dt_LONG_TEXT, "LIKE", 100, null, true);


            oGridN.doAddListField("a.U_WstGpCd", "Group Code", false, 50, null, "WGCODE");
            oGridN.doAddListField("a.U_WstGpNm", "Group Name", false, 150, null, "WGNAME");
            oGridN.setOrderValue("a.U_WstGpCd ASC ");

        }



        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {
            base.doBeforeLoadData(oForm);
        }

        protected override void doReadInputParams(SAPbouiCOM.Form oForm) {

            base.doReadInputParams(oForm);
            setParentSharedData(oForm, "CALLEDITEM", getParentSharedData(oForm, "CALLEDITEM"));
            //Dim sVal As String = getParentSharedData(oForm, "IDH_TYPE")
        }


        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == true) {
                if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE || pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD || ((pVal.EventType ==SAPbouiCOM.BoEventTypes.et_KEY_DOWN) && (pVal.CharPressed.ToString().Equals("27")))) {
                    this.setParentSharedData(oForm, "IDH_WGSRCHOPEND", "");
                    base.doButtonID2(oForm,ref pVal,ref BubbleEvent);
                    //End If
                }
            }
            base.doItemEvent(oForm,ref pVal,ref BubbleEvent);
            return true;
        }

        //Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)

        //End Sub

    }
}