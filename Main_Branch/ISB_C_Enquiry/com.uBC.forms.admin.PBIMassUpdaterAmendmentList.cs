﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.idh.dbObjects.User;
using IDHAddOns.idh.controls;
using com.uBC.utils;
using com.idh.controls;
using com.idh.bridge.utils;
using com.idh.dbObjects.numbers;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry.PJDORA.admin {
    public class PBIMassUpdaterAmendmentList : com.uBC.forms.fr3.dbGrid.Base {

        public PBIMassUpdaterAmendmentList(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
                mbIsSearch = false;
                SkipFormSettings = true;
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuPos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDHLVAMDLST", sParMenu, iMenuPos, "admin.srf", false, true, false, "PBI Mass Updater Amendment List", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            return owForm;
        }

        protected override void doInitialize() {
            moDBObject = new IDH_LVSAMD(); 
        }

#region FormOpenCreateFunctions
        /*
         * Do Complete the Form
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            base.doCompleteCreate(ref BubbleEvent);
        }

       /* 
        * Do the final form steps to show before loaddata
        */
        public override void doBeforeLoadData() {
            base.doBeforeLoadData();
        }

        /* 
         * Load the Form Data
         */
        public override void doLoadData() {
            base.doLoadData();
            //moAdminGrid.doReloadSetExtra("",false,true, false);
            //moAdminGrid.doApplyRules();
            //doFillCombos();
             
        }

       
        /*
         * Set the List Fields
         */
        public override void doSetListFields() {
            moAdminGrid.doAddListField(IDH_LVSAMD._Code, "Code", false, 0, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._Name, "Name", false, 0, ListFields.LISTTYPE_IGNORE, null, -1);

            moAdminGrid.doAddListField(IDH_LVSAMD._AmdDesc, "Amendment Desc", true, -1, ListFields.LISTTYPE_IGNORE, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._FrqTyp, "Frequency Type", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._FrqCount, "Frequency Count", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._Days, "Days", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._Qty, "Qty", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._Removal, "Remove PBI", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._SDate, "Start Date", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._JbType, "Order Type", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._AutoCmp, "Auto Complete", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._WTNFDt, "WTN From Date", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._WTNTDt, "WTN To Date", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._AddPBI, "Add PBI", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._Rebate, "Rebate", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._UOM, "UOM", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._SupRef, "Supplier Ref#", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._RoutCmt, "Route Comments", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._DrvCmt, "Driver Comments", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._PERTYP, "Period type", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);

            //moAdminGrid.doAddListField(IDH_LVSAMD._SupRef, "Supplier Ref#", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._Active, "Active", true, -1, ListFields.LISTTYPE_CHECKBOX, null, -1);
            moAdminGrid.doAddListField(IDH_LVSAMD._AmdBnVal, "Amendment Binary Value", false, 0, ListFields.LISTTYPE_IGNORE, null, -1);

        }
        #endregion

        #region Events
        protected override void doSetHandlers() {
            base.doSetHandlers();
        }

        protected override void doSetGridHandlers() {
            base.doSetGridHandlers();

          //  moAdminGrid.Handler_GRID_FIELD_CHANGED = new IDHGrid.ev_GRID_EVENTS(doFieldChangeEvent); 
        }
#endregion 

#region ItemEventHandlers
        //public bool doFieldChangeEvent(ref IDHAddOns.idh.events.Base pVal) {
        //    if (pVal.BeforeAction) {
        //    } else {
        //        IDH_WGVDMS oTrnCode = ((IDH_WGVDMS)moDBObject);
        //        string sTrnCode = "" +
        //        setVal(oTrnCode.U_Stock) + '-' + setVal(oTrnCode.U_WhseFrom) + '-' + setVal(oTrnCode.U_WhseTo) + '-' +
        //        setVal(oTrnCode.U_Trigger) + '-' + setVal(oTrnCode.U_MarkDoc1) + '-' + setVal(oTrnCode.U_MarkDoc2) + '-' +
        //        setVal(oTrnCode.U_MarkDoc3) + '-' + setVal(oTrnCode.U_MarkDoc4);
        //        oTrnCode.U_TrnCode = sTrnCode;
        //    }
        //    return true;
        //}

        private string setVal(string sVal) {
            if (sVal.Length == 0)
                return "0";
            else
                return sVal;
        }
        public override bool doAddUpdateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                Freeze(true);
                if (doValidate() == false) {
                    BubbleEvent = false;
                    return true;
                }
                if (moAdminGrid.doProcessData()) {
                    doLoadData();
                    moAdminGrid.doApplyRules();
                }
                Freeze(false);
            }
            return true;
        }
        private bool doValidate() {
            bool bResult = true;
            ////int iRes=0;
            //IDH_WGVDMS _IDH_WGVDMS = new IDH_WGVDMS();
            //for (int iRow1 = 0; iRow1 <= moAdminGrid.getRowCount()-1; iRow1++) {
            //    for (int iRow2 = iRow1+1; iRow2 <= moAdminGrid.getRowCount()-1; iRow2++) {
            //        if (moAdminGrid.doGetFieldValue(IDH_WGVDMS._ValidCd, iRow1).ToString() != string.Empty && moAdminGrid.doGetFieldValue(IDH_WGVDMS._ValidCd, iRow2).ToString() != string.Empty 
            //            && moAdminGrid.doGetFieldValue(IDH_WGVDMS._ValidCd, iRow1).ToString() == moAdminGrid.doGetFieldValue(IDH_WGVDMS._ValidCd, iRow2).ToString()) {
            //            //iRes = _IDH_WGVDMS.getData(IDH_WGVDMS._ValidCd + "='" + moAdminGrid.doGetFieldValue(IDH_WGVDMS._ValidCd, iRow) + "'", "");
            //            //if (iRes != 0) {
            //                com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Duplicate entries are not allowed:" + moAdminGrid.doGetFieldValue(IDH_WGVDMS._ValidCd, iRow2), "ERUSDBDU",
            //                    new string[] { moAdminGrid.doGetFieldValue(IDH_WGVDMS._ValidCd, iRow2).ToString() });
            //                bResult = false;
            //            //}
            //        }
            //    }
            //}
            return bResult;
        }
        #endregion

        #region General Functions
         

        #endregion
    }
}
