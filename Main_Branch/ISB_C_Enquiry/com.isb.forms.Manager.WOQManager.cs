﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;

using IDHAddOns.idh.controls;
//using com.idh.bridge.reports;
//using com.idh.utils.Conversions;
using com.idh.bridge;
using com.idh.bridge.data;
//using WR1_Grids.idh.controls.grid;
using com.idh.bridge.lookups;
using com.idh.bridge.action;
using com.idh.controls;
using com.idh.controls.strct;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry.Manager {
    class WOQManager : 
        IDHAddOns.idh.forms.Base {

        public WOQManager(IDHAddOns.idh.addon.Base oParent,string sParent, int iMenuPosition)
            : base(oParent, "IDHWOQMGR", sParent, iMenuPosition, "ENQ_WOQManager.srf", true, true, false, "WOQ Manager", load_Types.idh_LOAD_NORMAL) {
        }

        protected virtual void doTheGridLayout(UpdateGrid oGridN) {
            if (com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", false)) {
                string sFormTypeId = oGridN.getSBOForm().TypeEx;
                idh.dbObjects.User.IDH_FORMSET oFormSettings = (idh.dbObjects.User.IDH_FORMSET)getWFValue("FRMSET", sFormTypeId + "." + oGridN.GridId);
                if (oFormSettings == null) {
                    oFormSettings = new idh.dbObjects.User.IDH_FORMSET();
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, "");
                    setWFValue("FRMSET", sFormTypeId + "." + oGridN.GridId, oFormSettings);
                    oFormSettings.doAddFieldToGrid(oGridN);
                    doSetListFields(oGridN);
                    oFormSettings.doSyncDB(oGridN);
                } else {
                    if (oFormSettings.SkipFormSettings) {
                        doSetListFields(oGridN);
                    } else {
                        oFormSettings.doAddFieldToGrid(oGridN);
                    }
                }
            } else {
                doSetListFields(oGridN);
            }
        }

        protected void doSetListFields(UpdateGrid moGrid) {

            moGrid.doAddListField("" + IDH_WOQITEM._JobNr, "WOQ ID", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._Code, "Row ID", false, 20, null, null);
            
            moGrid.doAddListField("" + IDH_WOQHD._CardCd, "Customer code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            moGrid.doAddListField("" + IDH_WOQHD._CardNM, "Customer Name", false, -1, null, null);

            moGrid.doAddListField("RBDAte" , "Date", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQHD._RSDate, "Start Date", false, -1, null, null);


            moGrid.doAddListField("" + IDH_WOQHD._Address, "Address", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQHD._Street, "Street", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQHD._Block, "Block", false, 0, null, null);
            moGrid.doAddListField("" + IDH_WOQHD._City, "City", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQHD._County, "County", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQHD._State, "State", false, 0, null, null);            
            moGrid.doAddListField("" + IDH_WOQHD._ZpCd, "Postcode", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQHD._Country, "Country", false, -1, null, null);
            
            moGrid.doAddListField("" + IDH_WOQHD._SpInst, "Description", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQHD._Contact, "Contact Name", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQHD._E_Mail, "Email", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQHD._Phone1, "Phone", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQHD._Notes, "Notes", false, -1, null, null);

            moGrid.doAddListField("" + IDH_WOQHD._Source, "Source", false, -1, ListFields.LISTTYPE_COMBOBOX, null);

            moGrid.doAddListField("" + IDH_WOQHD._ClgCode, "Activity", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_ContactWithCustAndVend);

            moGrid.doAddListField("" + IDH_WOQHD._Branch, "Branch", false, -1, ListFields.LISTTYPE_COMBOBOX, null);
            moGrid.doAddListField("" + IDH_WOQHD._ItemGrp, "Container Group", false, -1, ListFields.LISTTYPE_COMBOBOX, null);
            moGrid.doAddListField("USER_CODE" , "Assigned to", false, -1, null, null);


            moGrid.doAddListField("" + IDH_WOQITEM._AdtnlItm, "A", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._PLineID, "Linked ID", false, -1, null, null);

            moGrid.doAddListField("" + IDH_WOQITEM._CarrCd, "Carrier code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            moGrid.doAddListField("" + IDH_WOQITEM._CarrNm, "Carrier Name", false, -1, null, null);

            moGrid.doAddListField("" + IDH_WOQITEM._Tip, "Disposal code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            moGrid.doAddListField("" + IDH_WOQITEM._TipNm, "Disposal Name", false, -1, null, null);

            moGrid.doAddListField("" + IDH_WOQITEM._WstGpCd, "Waste Group", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._WstGpNm, "Waste Group Name", true, 0, null, null);

            moGrid.doAddListField("" + IDH_WOQITEM._JobTp, "Order Type", true, 70, null, null);

            moGrid.doAddListField("" + IDH_WOQITEM._ItemCd, "Container Code", false, 120, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            moGrid.doAddListField("" + IDH_WOQITEM._ItemDsc, "Container Name", false, 200, null, null);


            moGrid.doAddListField("" + IDH_WOQITEM._WasCd, "Waste Code", false, 120, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            moGrid.doAddListField("" + IDH_WOQITEM._WasDsc, "Waste Name", false, 200, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._WasFDsc, "Waste Description(F)", false, 200, null, null);

            moGrid.doAddListField("" + IDH_WOQITEM._ExpLdWgt, "Estimated Qty", false, 100, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._UOM, "UOM", false, 50, ListFields.LISTTYPE_COMBOBOX, null);

            moGrid.doAddListField("" + IDH_WOQITEM._TipCost, "Disposal Cost", false, 50, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._MnMargin, "MU%", false, 50, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._LstPrice, "List Price", false, 50, null, null);

            moGrid.doAddListField("" + IDH_WOQITEM._TCharge, "Charge Price", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._HlSQty, "Carrier Qty", false, 0, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._CusChr, "Carrier Charge", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._Price, "Total Carrier Chg", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._OrdCost, "Carrier Cost", false, 0, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._OrdTot, "Total Carrier Cost", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._TipTot, "Total Disposal Cost", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._TaxAmt, "Tax Amount", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._Total, "Total Charge", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._JCost, "Total Cost", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._GMargin, "G Margin%", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._Sample, "Sample", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._RouteCd, "Disposal Route", false, 0, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._SampleRef, "Sample Ref", false, 0, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._SampStatus, "Sample Status", false, 0, null, null);
            

            moGrid.doAddListField("" + IDH_WOQHD._Status, "Doc Status", false, 70, ListFields.LISTTYPE_COMBOBOX, null);
            moGrid.doAddListField("R_Status" , "Row Status", false, 70, ListFields.LISTTYPE_COMBOBOX, null);


            moGrid.doAddListField("" + IDH_WOQITEM._WOHID, "WO Id", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._WORID, "WOR Id", false, -1, null, null);


            moGrid.doAddListField("" + IDH_WOQITEM._EnqID, "ENQ Id", false, -1, null, null);
            moGrid.doAddListField("" + IDH_WOQITEM._EnqLID, "ENQ Line Id", false, 0, null, null);

            moGrid.doAddListField("" + IDH_WOQITEM._Sort, "Sort", false, 0, null, null);

        }


        protected void doSetGridFilters(UpdateGrid moGridN) {

            moGridN.doAddFilterField("IDH_DATEF", "" + IDH_WOQHD._RSDate, SAPbouiCOM.BoDataType.dt_DATE, ">=", 10);
            moGridN.doAddFilterField("IDH_DATET", "" + IDH_WOQHD._RSDate, SAPbouiCOM.BoDataType.dt_DATE, "<=", 10);
            moGridN.doAddFilterField("IDH_CUST", "" + IDH_WOQHD._CardCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_NAME", "" + IDH_WOQHD._CardNM, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WOQSTS", "" + IDH_WOQHD._Status, SAPbouiCOM.BoDataType.dt_SHORT_NUMBER, "=", 11);
            moGridN.doAddFilterField("IDH_ACTVTY", "" + IDH_WOQHD._ClgCode, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_ENQID", "" + IDH_WOQITEM._EnqID, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_EQROWN", "" + IDH_WOQITEM._EnqLID, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_WOQID", "" + IDH_WOQITEM._JobNr, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WQROWN", "" + IDH_WOQITEM._Code, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);


            moGridN.doAddFilterField("IDH_WSGPCD", "" + IDH_WOQITEM._WstGpCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WSGPNM", "" + IDH_WOQITEM._WstGpNm, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_ASGNTO", "u.USER_CODE", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_ADDR", "" + IDH_WOQHD._Address, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_STREET", "" + IDH_WOQHD._Street, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_POSTCD", "" + IDH_WOQHD._ZpCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            moGridN.doAddFilterField("IDH_CONNM", "" + IDH_WOQHD._Contact, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_CONPH", "" + IDH_WOQHD._Phone1, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_EMAIL", "" + IDH_WOQHD._E_Mail, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);


            moGridN.doAddFilterField("IDH_WSCD", "" + IDH_WOQITEM._WasCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WSFDC", "" + IDH_WOQITEM._WasFDsc, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);
            moGridN.doAddFilterField("IDH_WSDC", "" + IDH_WOQITEM._WasDsc, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250);

            doAddUF(moGridN.getSBOForm(), "IDH_SEARCH", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 250, false, false);
            doAddUF(moGridN.getSBOForm(), "IDH_SHADIT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, false, false);

            
        }

        //*** Add event filters to avoid receiving all events from SBO
        protected override void doSetEventFilters() {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE);
        }

        protected override void doReadInputParams(SAPbouiCOM.Form oForm) {
            //base.doReadInputParams(oForm);
            if (getHasSharedData(oForm)) {
                UpdateGrid oGridN = default(UpdateGrid);
                oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");

                int iIndex = 0;
                for (iIndex = 0; iIndex <= oGridN.getGridControl().getFilterFields().Count - 1; iIndex++) {
                    com.idh.controls.strct.FilterField oField = (com.idh.controls.strct.FilterField)(oGridN.getGridControl().getFilterFields()[iIndex]);
                    string sFieldName = oField.msFieldName;
                    string sFieldValue = getParentSharedData(oForm, sFieldName).ToString();
                    try {
                        setUFValue(oForm, sFieldName, sFieldValue);
                    } catch (Exception ex) {
                    }
                }
            }
        }

        //** Create the form
        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            try {
                oForm.Title = gsTitle;
                int iWidth = oForm.Width;

                UpdateGrid oGridN = new UpdateGrid(this, oForm, "LINESGRID", 7, 100, iWidth - 20, 255, 0,0);
                
                oGridN.getSBOGrid().SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto;
                oForm.AutoManaged = false;
                oGridN.getSBOItem().AffectsFormMode = false;

                oForm.EnableMenu(Config.NAV_ADD, false);
                oForm.EnableMenu(Config.NAV_FIND, false);
                oForm.EnableMenu(Config.NAV_FIRST, false);
                oForm.EnableMenu(Config.NAV_NEXT, false);
                oForm.EnableMenu(Config.NAV_PREV, false);
                oForm.EnableMenu(Config.NAV_LAST, false);

                //Set Documents button caption on Job Managers
                SAPbouiCOM.Item oItm = default(SAPbouiCOM.Item);
                SAPbouiCOM.Button oBtn = default(SAPbouiCOM.Button);
                try {
                    oItm = oForm.Items.Item("IDH_DOC");
                    if (oItm != null) {
                        oBtn = (SAPbouiCOM.Button)oItm.Specific;
                        oBtn.Caption = Config.ParameterWithDefault("BTDOCCAP", "Documents");
                    }
                } catch (Exception ex) {
                }
                oForm.SupportedModes = Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_All);
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);
                base.doCompleteCreate(ref oForm, ref BubbleEvent);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDLF", null);
                BubbleEvent = false;
            }
        }

        protected void doSetMode(SAPbouiCOM.Form oForm, SAPbouiCOM.BoFormMode iMode) {
            try {
                object sFormType = getParentSharedData(oForm, "FTYPE");
                bool bIsSearch = false;
                if ((sFormType != null) && sFormType.Equals("SEARCH") == true) {
                    bIsSearch = true;
                }

                if (iMode == SAPbouiCOM.BoFormMode.fm_FIND_MODE) {
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = Translation.getTranslatedWord("Find");
                    if (bIsSearch) {
                        oForm.Items.Item("2").Visible = true;
                    } else {
                        oForm.Items.Item("2").Visible = false;
                    }
                } else if (iMode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = Translation.getTranslatedWord("Update");
                    oForm.Items.Item("2").Visible = true;
                }
            } catch (Exception ex) {
            }
        }
        private void doFillHeaderCombos(SAPbouiCOM.Form oForm) {
            com.uBC.utils.FillCombos.FillWR1StatusNew(oForm.Items.Item("IDH_WOQSTS").Specific, 106);
        }

        public string getListRequiredStr(SAPbouiCOM.Form OForm) {
            return " 1=1 ";
            //return " r." + IDH_WOQITEM._JobNr + " = h.Code And h." + IDH_WOQHD._User + "=u.USERID ";
        }

        private void doFillGridCombos(UpdateGrid moGrid) {
            SAPbouiCOM.ComboBoxColumn oCombo;

            int iIndex = moGrid.doIndexFieldWC(""+IDH_WOQITEM._UOM);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            com.uBC.utils.FillCombos.UOMCombo(oCombo);

            iIndex = moGrid.doIndexFieldWC("R_Status");
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            com.uBC.utils.FillCombos.FillWR1StatusNew(oCombo, 107);

            iIndex = moGrid.doIndexFieldWC( IDH_WOQHD._Status);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            com.uBC.utils.FillCombos.FillWR1StatusNew(oCombo, 106);

            iIndex = moGrid.doIndexFieldWC(IDH_WOQHD._Source);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            com.uBC.utils.FillCombos.FillWR1StatusNew(oCombo, 108);
            
            iIndex = moGrid.doIndexFieldWC( IDH_WOQHD._ItemGrp);
            oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            string msOrdCat = "Waste Order";
            doFillCombo(moGrid.getSBOForm(), oCombo, "OITB g, [@IDH_JOBTYPE] jt", "ItmsGrpCod", "ItmsGrpNam", "g.ItmsGrpCod=jt.U_ItemGrp AND jt.U_OrdCat='" + msOrdCat + "'","","","");

            //doFillJobTypeCombo(moGrid);
            //FillCombos.FillWR1StatusNew(Items.Item("IDH_STATUS").Specific, 101);

           

        }
        private void doFillJobTypeCombo(UpdateGrid moGrid) {
            try {
                SAPbouiCOM.ComboBoxColumn oCombo;
                int iIndex = moGrid.doIndexFieldWC("" + IDH_WOQITEM._JobTp);
                oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            
                doClearValidValues(oCombo.ValidValues);

                string sItemGrp = "";// moWOQ.U_ItemGrp;
                com.uBC.utils.FillCombos.doFillJobTypeCombo(ref oCombo, sItemGrp);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }
        }
        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {

           
            UpdateGrid oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");

            if (oGridN == null) {
                //int iWidth = oForm.Width;
                oGridN = new UpdateGrid(this, oForm, "LINESGRID"); //, 7, 100, iWidth - 20, 255, 0, 0);
            }

            //Required tables
           // oGridN.doAddGridTable(new GridTable(IDH_WOQITEM.TableName, "r", "Code", true, true), true);
           // oGridN.doAddGridTable(new GridTable(IDH_WOQHD.TableName, "h", "Code", true, true));
           // oGridN.doAddGridTable(new GridTable("OUSR", "u", "USERID", true, true));
            oGridN.doAddGridTable(new GridTable("IDH_WOQMANAGER", "", "Code", true, true), true);

            doSetGridFilters(oGridN);
            oGridN.setRequiredFilter(getListRequiredStr(oForm));
            oGridN.doSetDoCount(true);
            oGridN.doSetBlankLine(false);

            //Set the required List Fields
            doTheGridLayout(oGridN);


            doFillHeaderCombos(oForm);

            bool bHasParams = false;
            if (getHasSharedData(oForm)) {
                doReadInputParams(oForm);
                bHasParams = true;

                string sCardCode = getParentSharedData(oForm, "CARDCODE").ToString();
                if ((sCardCode != null) && sCardCode.Length > 0) {
                    string sExtraReqFilter = oGridN.getRequiredFilter();

                    if (sExtraReqFilter.Length > 0) {
                        sExtraReqFilter = sExtraReqFilter + " AND ";
                    }

                    sExtraReqFilter = sExtraReqFilter + " ( "+IDH_WOQHD._CardCd+" LIKE '" + sCardCode + "%') ";

                    oGridN.setRequiredFilter(sExtraReqFilter);
                }
            }

            if (bHasParams == false) {
                oGridN.setInitialFilterValue("Code = '-100'");
            }
        }

        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            doReLoadData(oForm, !getHasSharedData(oForm));
        }

        
        //** The Initializer
        protected void doReLoadData(SAPbouiCOM.Form oForm, bool bIsFirst) {
            try {
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);

                UpdateGrid oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");
                string sFilter = oGridN.getRequiredFilter();
                doSetCustomeFilter(oGridN);

                oGridN.setOrderValue("Cast(" + IDH_WOQITEM._JobNr + " as Int)," + IDH_WOQITEM._Sort + " ");

                oGridN.doReloadData("", false, true);
                doFillGridCombos(oGridN);
                oGridN.setRequiredFilter(sFilter);

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBL", null);
            }
        }

        protected void doSetCustomeFilter(UpdateGrid oGridN) {
            SAPbouiCOM.Form oForm = oGridN.getSBOForm();
            if (getItemValue(oForm, "IDH_SHADIT").ToString().Trim() == "Y") {
            } else {
                string sStatusQry = IDH_WOQITEM._AdtnlItm + "=''";//doGetStatusFilterQueryDynamic(getItemValue(oForm, "IDH_PROG").ToString.Trim.ToUpper);
                oGridN.setRequiredFilter(oGridN.getRequiredFilter() + " And " + sStatusQry);
            }
            if (getItemValue(oForm, "IDH_SEARCH").ToString().Trim() != string.Empty) {
                string sSearchWord = getItemValue(oForm, "IDH_SEARCH").ToString().Trim().Replace("*", "%").Replace("'", "''");
                string sExistingReqFilter = oGridN.getRequiredFilter();
                string sSQL = "";
                bool bAddNot = false;
                string sNotOp = "";

                if (sSearchWord.StartsWith("!")) {
                    bAddNot = true;
                    sSearchWord = sSearchWord.TrimStart('!');
                    sNotOp = " NOT ";
                }
                if (sSearchWord.Contains(",")) {
                    string[] aList = sSearchWord.Split(',');
                    sSearchWord = "";
                    int i = 0;
                    for (i = 0; i < aList.Length; i++) {
                    
                        sSQL += "¬  (" +
                                    "Cast(" + IDH_WOQHD._Code + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._Address + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    " USER_CODE" + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._Block + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    " BranchName" + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CardCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CardNM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CCardCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CCardNM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._ClgCode + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CCity + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CContact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._City + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CntrNo + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._Contact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._County + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CPhone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CState + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CStreet + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CusFrnNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    //"Cast(" + IDH_WOQHD._CustRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._E_Mail + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._ENNO + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    " HItemGrp" + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._MAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._MBlock + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._MCity + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._MContact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._MPhone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._MState + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._MStreet + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._MZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._Notes + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PBlock + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PCardCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PCardNM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PCity + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PContact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._Phone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PPhone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PremCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._ProRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PState + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PStreet + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._ReviewNotes + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._Route + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SBlock + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SCardCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SCardNM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SCity + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SContact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SPhone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SpInst + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SState + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SStreet + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._State + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._Street + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SupRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._TCharge + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    //"Cast(" + IDH_WOQHD._WasLic + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._WOHID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._ZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    " HSourceDesc" + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    " HStatusDesc " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._AddItmID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._AdtnlItm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._AltWasDsc + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._AUOM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "(" + IDH_WOQITEM._AUOM + sNotOp + "  IN  (Select UnitDisply from OWGT WITH(NOLOCK) where UnitName Like " + "'" + aList[i].Trim() + "'" + ") ) OR " +
                                    "Cast(" + IDH_WOQITEM._CarrCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._CarrNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._CarrRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._CCPCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._Comment + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._CusQty + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._CustCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._CustNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    //"Cast(" + IDH_WOQITEM._CustRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._EnqID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._ItemCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._ItemDsc + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    " RItemGrp " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._JobNr + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._JobTp + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._Lorry + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._LorryCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._PayMeth + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._ProCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._ProNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._ProRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._SAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._SupRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._Tip + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._TipCost + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._TipNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._TipTot + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._Total + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._UOM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._WasCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._WasDsc + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._WasFDsc + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._WOHID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._WORID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._WstGpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._WstGpNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    " RStatusDesc " + sNotOp + "Like " + "'" + aList[i].Trim() + "'" + " OR " +
                                    "(" + IDH_WOQITEM._UOM + sNotOp + "  IN  (Select UnitDisply from OWGT WITH(NOLOCK) where UnitName Like " + "'" + aList[i].Trim() + "'" + ") ) OR " +
                                    "Cast(" + IDH_WOQITEM._CusQty + " As NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + aList[i].Trim() + "'" + "  " +
                                     ")";
                    }
                    sSQL = sSQL.Trim().TrimStart('¬').Trim();
                    sSQL = sSQL.Replace("¬", " OR ");
                    
                    if (bAddNot)
                        sSQL = sSQL.Replace(" OR ", " AND ");
                    sSQL = " And " + sSQL;

                } else {
                    //sSearchWord = "'" + sSearchWord + "'";
                    sSQL += "  (" +
                                    "Cast(" + IDH_WOQHD._Code + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._Address + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    " USER_CODE" + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._Block + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    " BranchName" + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CardCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CardNM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CCardCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CCardNM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._ClgCode + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CCity + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CContact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._City + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CntrNo + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._Contact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._County + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CPhone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CState + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CStreet + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CusFrnNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    ///"Cast(" + IDH_WOQHD._CustRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._CZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._E_Mail + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._ENNO + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    " HItemGrp" + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._MAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._MBlock + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._MCity + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._MContact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._MPhone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._MState + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._MStreet + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._MZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._Notes + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PBlock + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PCardCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PCardNM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PCity + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PContact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._Phone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PPhone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PremCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._ProRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PState + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PStreet + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._PZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._ReviewNotes + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._Route + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SBlock + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SCardCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SCardNM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SCity + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SContact + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SPhone1 + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SpInst + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SState + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SStreet + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._State + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._Street + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SupRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._SZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._TCharge + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    //"Cast(" + IDH_WOQHD._WasLic + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._WOHID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQHD._ZpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    " HSourceDesc" + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    " HStatusDesc " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._AddItmID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._AdtnlItm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._AltWasDsc + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._AUOM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "(" + IDH_WOQITEM._AUOM + sNotOp + "  IN  (Select UnitDisply from OWGT WITH(NOLOCK) where UnitName Like " + "'" + sSearchWord + "'" + ") ) OR " +
                                    "Cast(" + IDH_WOQITEM._CarrCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._CarrNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._CarrRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._CCPCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._Comment + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._CusQty + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._CustCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._CustNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    //"Cast(" + IDH_WOQITEM._CustRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._EnqID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._ItemCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._ItemDsc + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    " RItemGrp " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._JobNr + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._JobTp + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._Lorry + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._LorryCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._PayMeth + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._ProCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._ProNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._ProRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._SAddress + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._SupRef + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._Tip + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._TipCost + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._TipNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._TipTot + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._Total + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._UOM + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._WasCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._WasDsc + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._WasFDsc + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._WOHID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._WORID + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._WstGpCd + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    "Cast(" + IDH_WOQITEM._WstGpNm + " AS NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + " OR " +
                                    " RStatusDesc " + sNotOp + "Like " + "'" + sSearchWord + "'" + " OR " +
                                    "(" + IDH_WOQITEM._UOM + sNotOp + "  IN  (Select UnitDisply from OWGT WITH(NOLOCK) where UnitName Like " + "'" + sSearchWord + "'" + ") ) OR " +
                                    "Cast(" + IDH_WOQITEM._CusQty + " As NVARCHAR(MAX)) " + sNotOp + " Like " + "'" + sSearchWord + "'" + "  " +
                                     ")";


                    if (bAddNot)
                        sSQL = sSQL.Replace(" OR ", " AND ");
                    sSQL = " And " + sSQL;
                }
                oGridN.setRequiredFilter(oGridN.getRequiredFilter() + " " + sSQL);
            }
        }

        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED) {
                if (pVal.ItemUID == "IDH_DOC" && Config.INSTANCE.getParameter("WOQMGDOC")!=string.Empty) {
                    string sReportName = Config.INSTANCE.getParameter("WOQMGDOC");
                    UpdateGrid oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");
                    if (oGridN == null) {
                        oGridN = new UpdateGrid(this, oForm, "LINESGRID"); //, 7, 100, iWidth - 20, 255, 0, 0);
                    }
                    ArrayList oSelectedRows = oGridN.doPrepareListFromSelection(IDH_WOQITEM._JobNr);
                    if (oSelectedRows.Count == 0) {
                        com.idh.bridge.DataHandler.INSTANCE.doResUserError("Please select rows.", "ERUSJOBS", null);
                        return false;
                    }
                       Hashtable oParams =new Hashtable();
                       oParams.Add("sRowNums", oSelectedRows);
                       string sCallerForm = oForm.TypeEx;
                       IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportName, "TRUE", "FALSE", "1", ref oParams, ref sCallerForm);
                    //if (sReportName.ToLower().EndsWith(".rpt")) {
                    //    Hashtable oParams = new Hashtable();
                    //    //oParams.Add("OrdNum", sCode)
                    //    oParams.Add("RowNum", moWOQ.Code);
                    //    string sCallerForm = SBOForm.TypeEx;
                    //    IDHAddOns.idh.report.Base.doCallReportDefaults(SBOForm, sReportName, "TRUE", "FALSE", "1", ref oParams, ref sCallerForm);
                    //} else if (sReportName != string.Empty) {
                    //    setSharedData("WOQDOCCODES", sReportName);
                    //    setSharedData("ROWNUM", moWOQ.Code);
                    //    doOpenModalForm("IDHWOQRPTS");
                    //}
                }
            }
            return base.doItemEvent(oForm, ref pVal, ref BubbleEvent);
        }
        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == true) {
                if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update") || oForm.Mode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
                   
                } else if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Find")) {
                    doReLoadData(oForm, true);
                }
                BubbleEvent = false;
            }
        }
        public override void doButtonID2(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == true) {
                doReLoadData(oForm, true);
            }
        }

        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCloseForm(oForm,ref BubbleEvent);
            UpdateGrid.doRemoveGrid(oForm, "LINESGRID");
        }
        public override void doClose() {
        }
        protected void doGridDoubleClick(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
            if (pVal.Row >= 0) {

                string sRowCode = oGridN.doGetFieldValue(IDH_WOQITEM._Code).ToString();
                string sWOQID = oGridN.doGetFieldValue(IDH_WOQITEM._JobNr).ToString();

                ArrayList oData = new ArrayList();
                if (sWOQID.Length == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Valid row must be selected.", "ERUSJOBS", null);
                } else {
                    if (oGridN.doCheckIsSameCol(pVal.ColUID,  IDH_WOQITEM._EnqID) || oGridN.doCheckIsSameCol(pVal.ColUID,  IDH_WOQITEM._EnqLID)) {
                        //Open Enquiry
                        com.isb.forms.Enquiry.Enquiry oOOForm = new com.isb.forms.Enquiry.Enquiry(null, oForm.UniqueID, null);
                        oOOForm.EnquiryID = oGridN.doGetFieldValue( IDH_WOQITEM._EnqID).ToString();
                        oOOForm.bLoadEnquiry = true;
                        oOOForm.bLoadReadOnly = false;
                        oOOForm.Handler_DialogButton1Return = new com.idh.forms.oo.Form.DialogReturn(doHandleEnquiryButton1Return);
                        oOOForm.doShowModal(oForm);
                        oOOForm.DBEnquiry.SBOForm = oOOForm.SBOForm;

                    } else if (oGridN.doCheckIsSameCol(pVal.ColUID,  IDH_WOQITEM._WOHID)) {
                        //Open WOH
                        ArrayList moData = new ArrayList();
                        moData.Add("DoUpdate");
                        moData.Add(oGridN.doGetFieldValue( IDH_WOQITEM._WOHID).ToString());
                        goParent.doOpenModalForm("IDH_WASTORD", oForm, moData);
                    } else if (oGridN.doCheckIsSameCol(pVal.ColUID,  IDH_WOQITEM._WORID)) {
                        //Open WOR
                        ArrayList moData = new ArrayList();
                        moData.Add("DoUpdate");
                        moData.Add(oGridN.doGetFieldValue(IDH_WOQITEM._WORID).ToString());
                        setSharedData("IDHJOBS", "ROWCODE", oGridN.doGetFieldValue( IDH_WOQITEM._WORID).ToString());
                        goParent.doOpenModalForm("IDHJOBS", oForm, moData);
                    } else {
                        //if (oGridN.doCheckIsSameCol(pVal.ColUID, "r." + IDH_ENQITEM._WOQID) || oGridN.doCheckIsSameCol(pVal.ColUID, "r." + IDH_ENQITEM._WOQLineID)) {                        
                        //Open WOQ                      
                        //com.isb.forms.Enquiry.WOQuote oOOForm = new com.isb.forms.Enquiry.WOQuote(null,oForm.UniqueID, null);
                        com.isb.forms.Enquiry.WOQuote oOOForm = new com.isb.forms.Enquiry.WOQuote(oForm.UniqueID, oGridN.doGetFieldValue(IDH_WOQITEM._JobNr).ToString());
                        oOOForm.WOQID = oGridN.doGetFieldValue( IDH_WOQITEM._JobNr).ToString();
                        oOOForm.bLoadWOQ = true;
                        
                        // oOOForm.bLoadWOQ
                        oOOForm.bLoadReadOnly = false;
                        oOOForm.Handler_DialogButton1Return = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQButton1Return);
                        oOOForm.doShowModal(oForm);
                       // oOOForm.doShow(oForm);
                        oOOForm.DBWOQ.SBOForm = oOOForm.SBOForm;
                    }
                }
            }
        }

        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK) {
                if (pVal.ItemUID == "LINESGRID") {
                    if (pVal.BeforeAction == false) {
                        doGridDoubleClick(oForm, ref pVal);
                    }
                }
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT && !pVal.BeforeAction) {
                //		oData.GetType()	{Name = "MenuEventClass" FullName = "SAPbouiCOM.MenuEventClass"}	System.Type {System.RuntimeType}
                SAPbouiCOM.MenuEventClass oData = (SAPbouiCOM.MenuEventClass)pVal.oData;
                if (oData.MenuUID == IDHGrid.GRIDMENUSORTASC) {
                    string sField = pVal.oGrid.doFindDBField(pVal.ColUID).Trim();
                    if (sField.Length > 0 && (sField ==  IDH_WOQITEM._Code || sField == IDH_WOQITEM._JobNr || sField == IDH_WOQITEM._EnqID || sField == IDH_WOQITEM._EnqLID)) {
                        UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
                        string sFilter = oGridN.getRequiredFilter();
                        doSetCustomeFilter(oGridN);

                        oGridN.setOrderValue("Cast( " + sField + " As Int) ");
                        oGridN.doReloadData("ASC", false);
                        doFillGridCombos(oGridN);
                        oGridN.doApplyRules();
                        oGridN.setRequiredFilter(sFilter);
                        return false;
                    }
                } else if (oData.MenuUID == IDHGrid.GRIDMENUSORTDESC) {
                    string sField = pVal.oGrid.doFindDBField(pVal.ColUID).Trim();
                    if (sField.Length > 0 && (sField == IDH_WOQITEM._Code || sField ==  IDH_WOQITEM._JobNr || sField == IDH_WOQITEM._EnqID || sField == IDH_WOQITEM._EnqLID)) {
                        UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
                        string sFilter = oGridN.getRequiredFilter();
                        doSetCustomeFilter(oGridN);

                        oGridN.setOrderValue("Cast( " + sField + " As Int) ");
                        oGridN.doReloadData("DESC", false);
                        doFillGridCombos(oGridN);
                        oGridN.doApplyRules();
                        oGridN.setRequiredFilter(sFilter);
                        return false;
                    }
                }
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_SORT && pVal.BeforeAction == false) {
                UpdateGrid oGridN = (UpdateGrid)pVal.oGrid;
                doFillGridCombos(oGridN);
                oGridN.doApplyRules();
            }
            return base.doCustomItemEvent(oForm, ref pVal);
        }

        public bool doHandleEnquiryButton1Return(com.idh.forms.oo.Form oDialogForm) {
            try {
                com.isb.forms.Enquiry.Enquiry oOOForm = (com.isb.forms.Enquiry.Enquiry)oDialogForm;
                doReLoadData(oOOForm.SBOParentForm, true);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doHandleEnquiryButton1Return" });
            }
            return true;
        }
        public bool doHandleWOQButton1Return(com.idh.forms.oo.Form oDialogForm) {
            try {
                com.isb.forms.Enquiry.WOQuote oOOForm = (com.isb.forms.Enquiry.WOQuote)oDialogForm;
                doReLoadData(oOOForm.SBOParentForm, true);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doHandleWOQButton1Return" });
            }
            return true;
        }
    }//end of class
}//end of namespace
