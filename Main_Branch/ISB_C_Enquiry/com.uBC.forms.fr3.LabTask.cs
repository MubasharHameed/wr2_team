﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
//using System.Data;
//using System.Diagnostics;
//using System.IO;
using IDHAddOns.idh.controls;
using com.idh.bridge.data;
//using com.uBC.utils;
using com.idh.bridge.lookups;
using com.idh.bridge;
using com.idh.dbObjects.numbers;
using SAPbouiCOM;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry {
    public class LabTask : IDHAddOns.idh.forms.Base {

        protected string msKeyGen = "SEQLBT";//"GENSEQ";

        #region Initialization

        public LabTask(IDHAddOns.idh.addon.Base oParent, string sParMenu, int iMenuPos)
            : base(oParent, "IDHLABTODO", sParMenu, iMenuPos, "Enq_LabTask.srf", true, true, false, "Lab Task List", load_Types.idh_LOAD_NORMAL) {
        }

        protected override void doSetEventFilters() {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE);
        }

        protected override void doReadInputParams(SAPbouiCOM.Form oForm) {
            //base.doReadInputParams(oForm);
            if (getHasSharedData(oForm)) {
                UpdateGrid oGridN = default(UpdateGrid);
                oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");

                int iIndex = 0;
                for (iIndex = 0; iIndex <= oGridN.getGridControl().getFilterFields().Count - 1; iIndex++) {
                    com.idh.controls.strct.FilterField oField = (com.idh.controls.strct.FilterField)(oGridN.getGridControl().getFilterFields()[iIndex]);
                    string sFieldName = oField.msFieldName;
                    string sFieldValue = getParentSharedData(oForm, sFieldName).ToString();
                    try {
                        setUFValue(oForm, sFieldName, sFieldValue);
                    } catch (Exception ex) {
                    }
                }
            }
        }

        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            try {
                oForm.Title = gsTitle;
                //oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE;

                doAddUF(oForm, "IDH_LABTSK", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100, true, false);

                SAPbouiCOM.Item oItem = oForm.Items.Item("IDH_RECTSK");
                UpdateGrid oGridN = new UpdateGrid(this, oForm, "LINESGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane);
                oGridN.getSBOItem().AffectsFormMode = false;
                oForm.EnableMenu(Config.NAV_ADD, false);
                oForm.EnableMenu(Config.NAV_FIND, false);
                oForm.EnableMenu(Config.NAV_FIRST, false);
                oForm.EnableMenu(Config.NAV_NEXT, false);
                oForm.EnableMenu(Config.NAV_PREV, false);
                oForm.EnableMenu(Config.NAV_LAST, false);


                //oGridN.AddEditLine = false;
                oForm.SupportedModes = Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_All);
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);
                oForm.AutoManaged = true;
                base.doCompleteCreate(ref oForm, ref BubbleEvent);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
                BubbleEvent = false;
            }
        }

        protected void doSetMode(SAPbouiCOM.Form oForm, SAPbouiCOM.BoFormMode iMode) {
            try {
                object sFormType = getParentSharedData(oForm, "FTYPE");
                bool bIsSearch = false;
                if ((sFormType != null) && sFormType.Equals("SEARCH") == true) {
                    bIsSearch = true;
                }

                if (iMode == SAPbouiCOM.BoFormMode.fm_FIND_MODE) {
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption =com.idh.bridge.Translation.getTranslatedWord("Find");
                    if (bIsSearch) {
                        oForm.Items.Item("2").Visible = true;
                    } else {
                        oForm.Items.Item("2").Visible = false;
                    }
                } else if (iMode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = com.idh.bridge.Translation.getTranslatedWord("Update");
                    oForm.Items.Item("2").Visible = true;
                }
            } catch (Exception ex) {
            }
        }
        #endregion

        #region LoadingDate

        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {
            string sLabTaskId = string.Empty;
            UpdateGrid oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");
            if (oGridN == null) {
                oGridN = new UpdateGrid(this, oForm, "LINESGRID"); //, 7, 100, iWidth - 20, 255, 0, 0);
            }
            if (getHasSharedData(oForm)) {
                if (getParentSharedData(oForm, "LABTASK") != null) {
                    sLabTaskId = getParentSharedData(oForm, "LABTASK").ToString();
                    setUFValue(oForm, "IDH_LABTSK", sLabTaskId);
                    doClearParentSharedData(oForm);
                }  

            }
            //UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
            //if (oGridN == null) {
            //    oGridN = new UpdateGrid(this, oForm, "LINESGRID");
            //}

            oGridN.doAddGridTable(new GridTable(IDH_LABTASK.TableName, null, "Code", true), true);
            oGridN.setOrderValue("Code");
            oGridN.doSetDoCount(true);
            oGridN.doSetBlankLine(false);

            doSetFilterFields(oGridN);
            doSetListFields(oGridN);
            oGridN.doApplyRules();
            //oGridN.getSBOItem().AffectsFormMode = true;

            doFillCombos(oForm);
        }

        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            oForm.Freeze(true);
            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);            
            FilterGrid oGridN = (FilterGrid)FilterGrid.getInstance(oForm, "LINESGRID");
            string sLabTaskCd = getUFValue(oForm, "IDH_LABTSK").ToString();
            if (sLabTaskCd.Length > 0) {
                oGridN.setRequiredFilter(" Code = '" + sLabTaskCd + "' And U_Saved=1");
                SAPbouiCOM.EditText oLabCd = (SAPbouiCOM.EditText)oForm.Items.Item("IDH_Code").Specific;
                oLabCd.Value = sLabTaskCd;
                setUFValue(oForm, "IDH_LABTSK", "");
            } else {
                oGridN.setRequiredFilter("U_Saved=1");
            }
            oGridN.doReloadData("", false, false);
            //oGridN.getSBOItem().AffectsFormMode = true;
            oGridN.doApplyRules();

            //Fill Grid Combos 
            doFillGridCombos(oForm);
            oForm.Freeze(false);
            IDH_LBTKPRG.oDataToSave.Clear();
        }

        protected void doSetFilterFields(IDHAddOns.idh.controls.UpdateGrid oGridN) {
            oGridN.doAddFilterField("IDH_Code", "Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            oGridN.doAddFilterField("IDH_ICode", "U_LabICd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            oGridN.doAddFilterField("IDH_DEPART", "U_AssignedTo", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            oGridN.doAddFilterField("IDH_Asign", "U_AssignedTo", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            oGridN.doAddFilterField("IDH_TSKST", "U_RowStatus", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            oGridN.doAddFilterField("IDH_DECI", "U_Decision", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            oGridN.doAddFilterField("IDH_BPCD", "U_BPCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            oGridN.doAddFilterField("IDH_BPNM", "U_BPName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            //oGridN.doAddFilterField("IDH_SRCTP", "(select U_ObjectType from [@IDH_LABITM] li where li.Code = U_LabItemRCd )", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 60, "");
            //oGridN.doAddFilterField("IDH_SRCCD", "(select U_ObjectCd from [@IDH_LABITM] li where li.Code = U_LabItemRCd )", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 60, "");
            oGridN.doAddFilterField("IDH_SRCTP", IDH_LABTASK._ObjectType, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            oGridN.doAddFilterField("IDH_SRCCD", IDH_LABTASK._ObjectCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            oGridN.doAddFilterField("IDH_SRDNUM", IDH_LABTASK._DocNum, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
        
        }

        protected void doSetListFields(IDHAddOns.idh.controls.UpdateGrid oGridN) {
            string sImgPath1 = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn1.png");
            string sImgPath1g = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn1_Green.png");
            string sImgPath2 = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn2.png");
            string sImgPath2g = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn2_Green.png");
            string sImgPath3 = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn3.png");
            string sImgPath3g = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn3_Green.png");

            oGridN.doAddListField("Code", "Code", false, -1, null, null);
            oGridN.doAddListField("Name", "Name", false, 0, null, null);
            oGridN.doAddListField("U_LabItemRCd", "Row Code", false, 0, null, null);
            oGridN.doAddListField("U_LabICd", "Lab Item Code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            oGridN.doAddListField(IDH_LABTASK._LabINm, "Lab Item Name", false, 0, null, null);

            //(select U_ObjectCd from [@IDH_LABITM] li where li.Code = lt.U_LabItemRCd )
            //(select U_ObjectType from [@IDH_LABITM] li where li.Code = lt.U_LabItemRCd )

            //oGridN.doAddListField("(select U_ObjectType from [@IDH_LABITM] li where li.Code = U_LabItemRCd )", "Source Type", false, -1, null, null);
            //oGridN.doAddListField("(select U_ObjectCd from [@IDH_LABITM] li where li.Code = U_LabItemRCd )", "Source ID", false, -1, null, null);

            oGridN.doAddListField("U_ObjectType", "Source Type", false, -1, null, null);
            //oGridN.doAddListField("( CASE WHEN Len(IsNull(U_ObjectCd, '')) > 0 THEN (select U_JobNr from [@IDH_WOQITEM] WQR where WQR.Code = U_ObjectCd) ELSE '' END )", "Source Header", false, -1, null, null);
            oGridN.doAddListField(IDH_LABTASK._DocNum, "Source Doc Num", false, -1, null, null);            
            oGridN.doAddListField("U_ObjectCd", "Source Doc Row", false, -1, null, null);
            oGridN.doAddListField(IDH_LABTASK._DipRtCd, "Disposal Route Code", false, -1, null, null);


            oGridN.doAddListField("U_BPCode", "BP Code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            oGridN.doAddListField("U_BPName", "BP Name", false, -1, null, null);
            oGridN.doAddListField("U_DtRequested", "Date Requested", false, -1, null, null);
            oGridN.doAddListField("U_RowStatus", "Status", true, -1, "COMBOBOX", null);
            oGridN.doAddListField("U_DtReceived", "Date Received", true, -1, null, null);
            oGridN.doAddListField("U_DtAnalysed", "Date Analysed", true, -1, null, null);
            oGridN.doAddListField("U_Department", "Department", true, -1, "COMBOBOX", null);
            oGridN.doAddListField("U_AssignedTo", "Assigned To", true, -1, "COMBOBOX", null);

            oGridN.doAddListField("U_AnalysisReq", "Analysis Required", false, -1, null, null);
            oGridN.doAddListField("( CASE WHEN LEN(ISNULL(U_AnalysisReq, '')) > 0 THEN  ( select '" + sImgPath1g + "') ELSE (select '" + sImgPath1 + "') END )", "Analy. Req.", false, 10, "PICTURE", null);

            oGridN.doAddListField(IDH_LABTASK._ExtTsDt, "External Test Date", true, -1, null, null);
            oGridN.doAddListField("U_ExtStatus", "External Status", true, -1, "COMBOBOX", null);
            oGridN.doAddListField("U_ExtComments", "External Comments", true, -1, null, null);


            oGridN.doAddListField("U_AtcAbsEntry", "Attachments ID", false, 0, null, null);
            //oGridN.doAddListField("(select '" + sImgPath2 + "')", "Atch", false, 10, "PICTURE", null);
            oGridN.doAddListField("( CASE WHEN U_AtcAbsEntry > 0 THEN  ( select '" + sImgPath2g + "') ELSE (select '" + sImgPath2 + "') END )", "Atch", false, 10, "PICTURE", null);

            oGridN.doAddListField("U_ImpResults", "Import Results ID", false, 0, null, null);
            //oGridN.doAddListField("(select '" + sImgPath3 + "')", "Imp. Res.", false, 10, "PICTURE", null);
            oGridN.doAddListField("( CASE WHEN LEN(ISNULL(U_ImpResults, '')) > 0 THEN  ( select '" + sImgPath3g + "') ELSE (select '" + sImgPath3 + "') END )", "Imp. Res.", false, 10, "PICTURE", null);

            ////oGridN.doAddListField("U_ImpResults", "Import Results", false, -1, "PICTURE", null);

            ////Dim sImgPath As String = Config.ParameterWithDefault("IMGPATH", "").Trim
            //string sImgPath = com.idh.bridge.lookups.Config.ParamaterWithDefault("IMGPATH", "").Trim();
            //string sBtnImg = "GridDotBtn.png";
            //string sFinalPath = sImgPath + sBtnImg;

            ////oGridN.doAddListField(sFinalPath, "Import Results", false, -1, "PICTURE", null);
            //string sTmp = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn.png"); 
            //oGridN.doAddListField("(select '" + sTmp + "')", "Import Results", false, 10, "PICTURE", null);
            //oGridN.doAddListField("(select '" + sFinalPath + "')", "Import Results1", false, 10, "PICTURE", null);
            ////oGridN.doAddListField(doGetProgressImageQuery, "", False, 10, "PICTURE", Nothing);


            //oGridN.doAddListField("U_ImpResults", "Import Results ID", false, -1, null, null);

            oGridN.doAddListField("U_Decision", "Decision", true, -1, "COMBOBOX", null);
            oGridN.doAddListField(IDH_LABTASK._Saved, "Save Row", false, 0, null, null);

        }

        #endregion

        #region UnloadingData

        public override void doClose() {
        }

        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCloseForm(oForm, ref BubbleEvent);
            FilterGrid.doRemoveGrid(oForm, "LINESGRID");
        }

        #endregion

        #region EventHandlers
		public override void doButtonID2(Form oForm, ref ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction && ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update")) {
                doLoadData(oForm);
                BubbleEvent = false;
            }

        }
        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update")) {
                    oForm.Freeze(true);
                    UpdateGrid oGridN = (UpdateGrid)FilterGrid.getInstance(oForm, "LINESGRID");

                    //Check if the Lab Task linked with a WOQ Row 
                    ArrayList aObjectID = oGridN.doGetChangedRows();
                    ArrayList aNewRowsList = oGridN.doGetAddedRows();
                    ArrayList aChangedFieldList;
                    int iIndx = 0;
                    int iRow = 0;
                    //string sLabItemRCd = string.Empty;
                    //string sLabItemCd = string.Empty; 
                    string sLabDecision = string.Empty;
                    string sLabTaskCd = string.Empty;
                    string sLTSource = string.Empty;
                    string sDecision = "";
                    if (aObjectID != null && aObjectID.Count > 0) {
                        while (iIndx < aObjectID.Count) {
                            iRow = (int)aObjectID[iIndx];
                            oGridN.setCurrentDataRowIndex(iRow);
                            aChangedFieldList = oGridN.doGetChangedFields(iRow);
                            sDecision = oGridN.doGetFieldValue(IDH_LABTASK._Decision).ToString();
                            sLTSource = oGridN.doGetFieldValue(IDH_LABTASK._ObjectType).ToString();
                            if (aChangedFieldList.Contains(IDH_LABTASK._Decision)) {
                                sLabTaskCd = oGridN.doGetFieldValue(IDH_LABTASK._Code).ToString();
                                //sLabItemRCd = oGridN.doGetFieldValue(IDH_LABTASK._LabItemRCd).ToString();
                                //sLabItemCd = oGridN.doGetFieldValue(IDH_LABTASK._LabICd).ToString();
                                sLabDecision = oGridN.doGetFieldValue(IDH_LABTASK._Decision).ToString();

                                sLTSource = oGridN.doGetFieldValue(IDH_LABTASK._ObjectType).ToString();

                                if (sLTSource.Equals("WOQ")) {
                                    IDH_WOQITEM oWOQRow = new IDH_WOQITEM();
                                    oWOQRow.DoNotSetDefault = true; //will stop to load parent 
                                    int iCnt = oWOQRow.getBySampleRef(sLabTaskCd);
                                    if (iCnt > 0 && oWOQRow.U_SampleRef != null && oWOQRow.U_SampleRef.Length > 0 && oWOQRow.U_SampStatus.Length > 0) {
                                        oWOQRow.U_SampStatus = sLabDecision;
                                        oWOQRow.doUpdateDataRow();
                                    }
                                    oWOQRow = null;
                                } else if (sLTSource.Equals("DO") && oGridN.doGetFieldValue(IDH_LABTASK._ObjectCd).ToString().Length > 0) {
                                    idh.dbObjects.User.IDH_DISPROW oDORow = new idh.dbObjects.User.IDH_DISPROW();
                                    oDORow.getByKey(oGridN.doGetFieldValue(IDH_LABTASK._ObjectCd).ToString());
                                    oDORow.U_SampStatus = sLabDecision;
                                    oDORow.doUpdateDataRow();
                                }

                            } else if (!sDecision.ToLower().Equals("accepted") && aChangedFieldList.Contains(IDH_LABTASK._DtAnalysed) &&//Analysis Date updated
                                oGridN.doGetFieldValue(IDH_LABTASK._AssignedTo) != null && oGridN.doGetFieldValue(IDH_LABTASK._DtAnalysed) != null
                                    && !string.IsNullOrEmpty(oGridN.doGetFieldValue(IDH_LABTASK._AssignedTo).ToString()) &&
                                    !string.IsNullOrEmpty(oGridN.doGetFieldValue(IDH_LABTASK._DtAnalysed).ToString())) {
                                IDH_LABTASK.dosendAlerttoUser(oGridN.doGetFieldValue(IDH_LABTASK._AssignedTo).ToString(), com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTM4", null), com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTS4", null), true, oGridN.doGetFieldValue(IDH_LABTASK._Code).ToString());
                            } else if (!sDecision.ToLower().Equals("accepted") && sLTSource!="WOR" && aChangedFieldList.Contains(IDH_LABTASK._AnalysisReq) &&//Analysis required updated
                                oGridN.doGetFieldValue(IDH_LABTASK._AssignedTo) != null &&
                                    oGridN.doGetFieldValue(IDH_LABTASK._AnalysisReq) != null &&
                                    !string.IsNullOrEmpty(oGridN.doGetFieldValue(IDH_LABTASK._AssignedTo).ToString()) &&
                                    !string.IsNullOrEmpty(oGridN.doGetFieldValue(IDH_LABTASK._AnalysisReq).ToString())) {
                                IDH_LABTASK.dosendAlerttoUser(oGridN.doGetFieldValue(IDH_LABTASK._AssignedTo).ToString(), com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTM3", null), com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTS3", null), true, oGridN.doGetFieldValue(IDH_LABTASK._Code).ToString());

                            } else if (!sDecision.ToLower().Equals("accepted") && aChangedFieldList.Contains(IDH_LABTASK._DtReceived) &&//Sample received
                                oGridN.doGetFieldValue(IDH_LABTASK._AssignedTo) != null &&
                                    oGridN.doGetFieldValue(IDH_LABTASK._DtReceived) != null &&
                                    !string.IsNullOrEmpty(oGridN.doGetFieldValue(IDH_LABTASK._AssignedTo).ToString()) &&
                                    !string.IsNullOrEmpty(oGridN.doGetFieldValue(IDH_LABTASK._DtReceived).ToString())) {
                                IDH_LABTASK.dosendAlerttoUser(oGridN.doGetFieldValue(IDH_LABTASK._AssignedTo).ToString(), com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTM2", null), com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTS2", null), true, oGridN.doGetFieldValue(IDH_LABTASK._Code).ToString());

                            } else if (aChangedFieldList.Contains(IDH_LABTASK._AssignedTo) &&//User changed
                                oGridN.doGetFieldValue(IDH_LABTASK._AssignedTo) != null && !string.IsNullOrEmpty(oGridN.doGetFieldValue(IDH_LABTASK._AssignedTo).ToString())) {
                                IDH_LABTASK.dosendAlerttoUser(oGridN.doGetFieldValue(IDH_LABTASK._AssignedTo).ToString(), com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTM5", null), com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTS5", null), true, oGridN.doGetFieldValue(IDH_LABTASK._Code).ToString());

                            }
                            iIndx++;
                        }
                    }
                    bool bRet=true;
                    //if (bRet) {
                    if (aNewRowsList != null && aNewRowsList.Count > 0) {
                        DataHandler.INSTANCE.StartTransaction();
                        iIndx = 0;
                        string sAssignTo = "", sCardCode = "";

                        while (iIndx < aNewRowsList.Count) {
                            iRow = (int)aNewRowsList[iIndx];
                            iIndx++;
                            string LabItemRCd = oGridN.doGetFieldValue("U_LabItemRCd", iRow).ToString();
                            if (string.IsNullOrEmpty(LabItemRCd)) {
                                continue;
                            }
                            sAssignTo = oGridN.doGetFieldValue(IDH_LABTASK._AssignedTo, iRow).ToString();
                            sCardCode = oGridN.doGetFieldValue(IDH_LABTASK._BPCode, iRow).ToString();
                            if (sAssignTo == "" && sCardCode != string.Empty && !oGridN.doGetFieldValue(IDH_LABTASK._ObjectType, iRow).ToString().Contains("WOR")) {
                                string sDept = "";
                                if (!string.IsNullOrEmpty(sCardCode)) {
                                    string sUserCode = Config.INSTANCE.doUserNameofBPSalesEmployee(sCardCode);
                                    if (sUserCode == null || sUserCode.ToString().Trim() == string.Empty) {
                                        DataHandler.INSTANCE.doUserError(Translation.getTranslatedMessage("NOSALEPR", "Please assign the sales employee of BP " + sCardCode + ", and update the task assignment accordingly."));
                                    } else if (sUserCode!=""){
                                        sAssignTo = sUserCode;//Config.INSTANCE.getValueFromTablebyKey("OUSR", "USER_CODE", "UserID", sSalesEmp.ToString()).ToString();
                                        sDept = Config.INSTANCE.getValueFromTablebyKey("OUSR", "Department", "USER_CODE", sAssignTo).ToString();
                                        oGridN.doSetFieldValue(IDH_LABTASK._AssignedTo, iRow, sAssignTo);
                                        oGridN.doSetFieldValue(IDH_LABTASK._Department, iRow, sDept);
                                    }
                                }
                            }
                            IDH_LABITM oLabItem = new IDH_LABITM();
                            IDH_LABITM oNewLabItem = new IDH_LABITM();
                            if (oLabItem.getByKey(LabItemRCd)) {
                                oNewLabItem.doAddEmptyRow(true);
                                NumbersPair oNextLINo = oNewLabItem.getNewKey();
                                for (int i = 0; i <= oLabItem.getColumnCount() - 1; i++) {
                                    oNewLabItem.setValue(oNewLabItem.getFieldInfo(i).FieldName, oLabItem.getValue(oNewLabItem.getFieldInfo(i).FieldName));
                                }
                                oNewLabItem.Code = oNextLINo.CodeCode;
                                oNewLabItem.Name = oNextLINo.NameCode;
                                oNewLabItem.U_ObjectType = "MAN-" + oLabItem.U_ObjectType;
                                oNewLabItem.U_CreatedOn = DateTime.Now;
                                oNewLabItem.U_CreatedBy = DataHandler.INSTANCE.User;
                                string snewLabTaskCode = oNewLabItem.Code;
                                if (oNewLabItem.doProcessData()) {
                                    bRet = true;
                                    oGridN.doSetFieldValue("U_LabItemRCd", iRow, snewLabTaskCode);
                                    IDH_LITMDTL oLIDetail = new IDH_LITMDTL();
                                    //add lab item detail
                                    //dont forget to update grid
                                    int iRet = oLIDetail.getData(IDH_LITMDTL._LabCd + "='" + oLabItem.Code + "'", "");
                                    if (iRet > 0) {
                                        IDH_LITMDTL oNewLIDetail = new IDH_LITMDTL();
                                        oLIDetail.first();
                                        while (oLIDetail.next()) {
                                            oNewLIDetail.doAddEmptyRow(true);
                                            NumbersPair oNextLIDNo = oNewLIDetail.getNewKey();
                                            for (int i = 0; i <= oLIDetail.getColumnCount() - 1; i++) {
                                                oNewLIDetail.setValue(oLIDetail.getFieldInfo(i).FieldName, oLIDetail.getValue(oLIDetail.getFieldInfo(i).FieldName));
                                            }
                                            oNewLIDetail.Code = oNextLIDNo.CodeCode;
                                            oNewLIDetail.Name = oNextLIDNo.NameCode;
                                            oNewLIDetail.U_LabCd = snewLabTaskCode;
                                        }
                                        bRet = oNewLIDetail.doProcessData();
                                    }
                                } else
                                    bRet = false;
                            }
                            if (bRet && sAssignTo != string.Empty) {
                                IDH_LABTASK.dosendAlerttoUser(sAssignTo, com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTM1", null), com.idh.bridge.resources.Messages.INSTANCE.getMessage("LABARTS1", null), true, oGridN.doGetFieldValue(IDH_LABTASK._Code).ToString());
                            }
                        }
                    }
                    //}
                    bRet = oGridN.doProcessData();
                    if (bRet)
                       bRet= IDH_LBTKPRG.doProcessPendingData();
                    if (DataHandler.INSTANCE.IsInTransaction()) {
                        if (bRet) {
                            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                        } else if (bRet == false ) {
                            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                        }
                    } 
                    if (bRet) {
                        doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_OK_MODE);
                        doLoadData(oForm);
                        IDH_LABTASK.dosendAllAlerttoUser();
                    } else if (bRet == false && DataHandler.INSTANCE.IsInTransaction()) {
                        oGridN.doLoadLastChangeIndicators();
                    }
                    oForm.Freeze(false);
                } else {
                    oForm.Freeze(true);
                    doLoadData(oForm);
                    oForm.Freeze(false);
                }
                BubbleEvent = false;
                return;
            }
            //UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
            ////If oGridN.hasFieldChanged("U_Select") Then
            //Int32 _WOQID = getParentSharedDataAsInteger(oForm, "WOQID");
            //Int32 _EnqID = getParentSharedDataAsInteger(oForm, "ENQID");
            //com.idh.dbObjects.User.IDH_ENQUS moIDH_ENQUS = new com.idh.dbObjects.User.IDH_ENQUS();
            //for (Int16 iRow = 0; iRow <= oGridN.getRowCount() - 1; iRow++)
            //{

            //    if (!string.IsNullOrEmpty(oGridN.doGetFieldValue("Code", iRow).ToString()) && moIDH_ENQUS.getByKey(oGridN.doGetFieldValue("Code", iRow).ToString()))
            //    {
            //        moIDH_ENQUS.U_Answer = oGridN.doGetFieldValue("U_Answer", iRow).ToString();
            //    }
            //    else
            //    {
            //        moIDH_ENQUS.doAddEmptyRow(true, true, false);
            //        moIDH_ENQUS.U_Answer = oGridN.doGetFieldValue("U_Answer", iRow).ToString();
            //        if (_EnqID != 0)
            //            moIDH_ENQUS.U_EnqID = Convert.ToInt32(oGridN.doGetFieldValue("U_EnqID", iRow).ToString());
            //        moIDH_ENQUS.U_QsID = oGridN.doGetFieldValue("QID", iRow).ToString();
            //        moIDH_ENQUS.U_Question = oGridN.doGetFieldValue("Question", iRow).ToString();
            //        if (_WOQID != 0)
            //            moIDH_ENQUS.U_WOQID = Convert.ToInt32(oGridN.doGetFieldValue("U_WOQID", iRow).ToString());

            //    }
            //    moIDH_ENQUS.doProcessData();
            //    //End If
            //}
          //  doLoadData(oForm);

        }

        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            base.doItemEvent(oForm, ref pVal, ref BubbleEvent);
            if ((pVal.ItemUID == "IDH_WSTGRP") && pVal.EventType == SAPbouiCOM.BoEventTypes.et_COMBO_SELECT && pVal.BeforeAction == false) {
                doLoadData(oForm);
            }
            return true;
        }

        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED) {
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
				doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE);
                if (!pVal.BeforeAction && pVal.ColUID == IDH_LABTASK._Decision) {//part of work flow if Decision updated
                    IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");
                    if (oGridN.doGetFieldValue(IDH_LABTASK._Decision, pVal.Row) != null && oGridN.doGetFieldValue(IDH_LABTASK._Decision, pVal.Row).ToString().Trim() != "Pending") {
                        oGridN.doSetFieldValue(IDH_LABTASK._AssignedTo, "");
                        oGridN.doSetFieldValue(IDH_LABTASK._Department, "");
                    }
                } else if (!pVal.BeforeAction && pVal.ColUID == IDH_LABTASK._DtAnalysed) {//part of work flow if Analysis Date updated
                    IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");
                    if (oGridN.doGetFieldValue(IDH_LABTASK._DtAnalysed, pVal.Row) != null && oGridN.doGetFieldValue(IDH_LABTASK._DtAnalysed, pVal.Row).ToString().Trim() != string.Empty) {
                        //Analysis date provided
                        //update user and its dept
                        string sUser = "";
                        if (oGridN.doGetFieldValue(IDH_LABTASK._ObjectType, pVal.Row).ToString().Contains("WOR"))
                            sUser = Config.ParamaterWithDefault("LABUSR11", "");
                        else
                            sUser = Config.ParamaterWithDefault("LABUSR04", "");
                        if (sUser != string.Empty) {
                            string sDept = Config.INSTANCE.getValueFromTablebyKey("OUSR", "Department", "USER_CODE", sUser).ToString();

                            oGridN.doSetFieldValue(IDH_LABTASK._AssignedTo, sUser);
                            oGridN.doSetFieldValue(IDH_LABTASK._Department, sDept);
                        }
                    }
                } else if (!pVal.BeforeAction && pVal.ColUID == IDH_LABTASK._AnalysisReq) {//part of work flow if Analysis Required updated
                    IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");
                    if (!oGridN.doGetFieldValue(IDH_LABTASK._ObjectType).ToString().Contains("WOR") && oGridN.doGetFieldValue(IDH_LABTASK._AnalysisReq, pVal.Row) != null && oGridN.doGetFieldValue(IDH_LABTASK._AnalysisReq, pVal.Row).ToString().Trim() != string.Empty) {
                        //receieve date provided
                        //update user and its dept
                        string sUser = Config.ParamaterWithDefault("LABUSR03", "");
                        if (sUser != string.Empty) {
                            string sDept = Config.INSTANCE.getValueFromTablebyKey("OUSR", "Department", "USER_CODE", sUser).ToString();
                            oGridN.doSetFieldValue(IDH_LABTASK._AssignedTo, sUser);
                            oGridN.doSetFieldValue(IDH_LABTASK._Department, sDept);
                        }
                    }
                } else if (!pVal.BeforeAction && pVal.ColUID == IDH_LABTASK._DtReceived) {//part of work flow if Received date is entered
                    IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");
                    if (oGridN.doGetFieldValue(IDH_LABTASK._DtReceived, pVal.Row) != null && oGridN.doGetFieldValue(IDH_LABTASK._DtReceived, pVal.Row).ToString().Trim() != string.Empty) {
                        //receieve date provided
                        //update user and its dept
                        string sUser ="";
                        if (oGridN.doGetFieldValue(IDH_LABTASK._ObjectType, pVal.Row).ToString().Contains("WOR"))
                            sUser = Config.ParamaterWithDefault("LABUSR10", "");                        
                        else
                        sUser = Config.ParamaterWithDefault("LABUSR02", "");
                        if (sUser != string.Empty) {
                            string sDept = Config.INSTANCE.getValueFromTablebyKey("OUSR", "Department", "USER_CODE", sUser).ToString();

                            oGridN.doSetFieldValue(IDH_LABTASK._AssignedTo, sUser);
                            oGridN.doSetFieldValue(IDH_LABTASK._Department, sDept);
                        }
                    }
                }
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK) {
                if (pVal.ItemUID == "LINESGRID") {
                    if (pVal.BeforeAction == false) {
                        IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");

                        if (pVal.ColUID.Contains("GridDotBtn1.png")) {
                            setSharedData(oForm, "TSKANLCD", oGridN.doGetFieldValue("U_AnalysisReq", pVal.Row));
                            setSharedData(oForm, "TASKCODE", oGridN.doGetFieldValue("Code", pVal.Row));
                            setSharedData(oForm, "WASTECODE", oGridN.doGetFieldValue("U_LabICd", pVal.Row));
                            goParent.doOpenModalForm("IDHLABTANL", oForm);
                        } else if (pVal.ColUID.Contains("GridDotBtn2.png")) {
                            string sLabTaskCd = oGridN.doGetFieldValue("Code", pVal.Row).ToString();
                            string sAttachmentEntry = oGridN.doGetFieldValue("U_AtcAbsEntry", pVal.Row).ToString();
                            if (sAttachmentEntry != "0") {
                                //Open Existing Attachment Entry 
                                setSharedData(oForm, "ANEXCD", sAttachmentEntry);
                            } else {
                                //New Attachment Entry 
                                setSharedData(oForm, "TFSCODE", sLabTaskCd);
                                setSharedData(oForm, "TFSNUM", sLabTaskCd);
                            }
                            goParent.doOpenModalForm("IDHATTACH", oForm);
                        } else if (pVal.ColUID.Contains("GridDotBtn3.png")) {
                            string sLabTaskCd = oGridN.doGetFieldValue("Code", pVal.Row).ToString();
                            //string sImpResultsID = oGridN.doGetFieldValue("U_ImpResults", pVal.Row).ToString();
                            setSharedData(oForm, "LABTSKID", sLabTaskCd);
                            //setSharedData(oForm, "IMPRSLID", sImpResultsID);
                            goParent.doOpenModalForm("IDHLABRESULTS", oForm);
                        } else if (pVal.ColUID == IDH_LABTASK._DocNum || pVal.ColUID == IDH_LABTASK._ObjectCd || pVal.ColUID == IDH_LABTASK._ObjectType) {
                            string sObjectType = oGridN.doGetFieldValue(IDH_LABTASK._ObjectType).ToString().Trim().ToUpper();
                            string sObjectCode = oGridN.doGetFieldValue(IDH_LABTASK._ObjectCd).ToString().Trim().ToUpper();
                            string sDocNum = oGridN.doGetFieldValue(IDH_LABTASK._DocNum).ToString().Trim().ToUpper();
                            if (sObjectType.Contains("WOR") && sObjectCode!=string.Empty && sDocNum!=string.Empty) {
                                if (pVal.ColUID == IDH_LABTASK._ObjectCd) {//open wor form
                                    ArrayList oData = new ArrayList();
                                    oData.Add("DoUpdate");
                                    oData.Add(sObjectCode);
                                    setSharedData(oForm, "ROWCODE", sObjectCode);
                                    goParent.doOpenModalForm("IDHJOBS", oForm, oData);
                                } else {
                                    //open WOH Form 
                                    ArrayList oData = new ArrayList();
                                    oData.Add("DoUpdate");
                                    oData.Add(sDocNum);
                                    goParent.doOpenModalForm("IDH_WASTORD", oForm, oData);
                                }
                            } else if (sObjectType.Contains("WOQ") && sObjectCode != string.Empty && sDocNum != string.Empty) {
                                //com.isb.forms.Enquiry.WOQuote oOOForm = new com.isb.forms.Enquiry.WOQuote(null, oForm.UniqueID, null);
                                com.isb.forms.Enquiry.WOQuote oOOForm = new com.isb.forms.Enquiry.WOQuote( oForm.UniqueID, sDocNum);
                                oOOForm.WOQID = sDocNum;
                                oOOForm.bLoadWOQ = true;
                                //oOOForm.bLoadReadOnly = true;
                                //oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQOKReturn);
                                //oOOForm.Handler_DialogButton1Return = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQOKReturn);
                                //oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQCancelReturn);
                                oOOForm.doShowModal(oForm);
                            }
                        }
                    }
                }
            }

            return base.doCustomItemEvent(oForm, ref pVal);

        }

        protected override void doHandleModalResultShared(SAPbouiCOM.Form oParentForm, string sModalFormType, string sLastButton = null) {
            if (sModalFormType == "IDHLABTANL") {
                IDHGrid oGridN = IDHGrid.getInstance(oParentForm, "LINESGRID");
                string sExistingTAC = oGridN.doGetFieldValue("U_AnalysisReq").ToString();
                string sTskAnalysisCds = getSharedData(oParentForm, "NEWTSKANLCD").ToString();

                if (!sExistingTAC.Equals(sTskAnalysisCds)) {
                    oGridN.doSetFieldValue("U_AnalysisReq", sTskAnalysisCds);
                    oParentForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
                    //if (!pVal.BeforeAction && pVal.ColUID == IDH_LABTASK._AnalysisReq) {//part of work flow if Analysis Required updated
                    //IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");
                    if (!oGridN.doGetFieldValue(IDH_LABTASK._ObjectType).ToString().Contains("WOR") && oGridN.doGetFieldValue(IDH_LABTASK._AnalysisReq) != null && oGridN.doGetFieldValue(IDH_LABTASK._AnalysisReq).ToString().Trim() != string.Empty) {
                        //receieve date provided
                        //update user and its dept
                        string sUser = Config.ParamaterWithDefault("LABUSR03", "");
                        if (sUser != string.Empty) {
                            string sDept = Config.INSTANCE.getValueFromTablebyKey("OUSR", "Department", "USER_CODE", sUser).ToString();
                            oGridN.doSetFieldValue(IDH_LABTASK._AssignedTo, sUser);
                            oGridN.doSetFieldValue(IDH_LABTASK._Department, sDept);
                        }
                    }
                    //}
                } else if(IDH_LBTKPRG.oDataToSave.Count>0) {
                    IDH_LBTKPRG.doProcessPendingData();
                }
            } else if (sModalFormType == "IDHATTACH") {
                IDHGrid oGridN = IDHGrid.getInstance(oParentForm, "LINESGRID");
                string sExistingACd = oGridN.doGetFieldValue("U_AtcAbsEntry").ToString();
                string sAttachmentCode = getSharedData(oParentForm, "ATCHCD").ToString();

                if (!sExistingACd.Equals(sAttachmentCode)) {
                    oGridN.doSetFieldValue("U_AtcAbsEntry", sAttachmentCode);
                    oParentForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
                }
            } else if (sModalFormType == "IDHLABRESULTS") {
                string sLTID = getSharedData(oParentForm, "LABTSKID").ToString();
                IDH_LABTSKIMP oLTImport = new IDH_LABTSKIMP();
                oLTImport.getByLabTaskCode(sLTID);
                if (oLTImport.Code.Length > 0) {
                    IDHGrid oGridN = IDHGrid.getInstance(oParentForm, "LINESGRID");
                    oGridN.doSetFieldValue("U_ImpResults", oLTImport.Code);
                    oParentForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
                }

            }
            base.doHandleModalResultShared(oParentForm, sModalFormType, sLastButton);
        }

        public override bool doMenuEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == false && pVal.MenuUID == "IDHGMADD") {
                UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
                oGridN.AddEditLine = true;
                oGridN.doAddDataLine(true);
                oGridN.AddEditLine = false;      
                doAddNewRow(oGridN);

            }
            return base.doMenuEvent(oForm, ref pVal, ref BubbleEvent);
        }
        #endregion

        #region CustomFunctions

        protected void doFillCombos(SAPbouiCOM.Form oForm) {
            //SAPbouiCOM.Item oItem = oForm.Items.Item("IDH_TYPE");
            ////Date Combo 
            //SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
            //oCombo.ValidValues.Add("", "Both"); 
            //oCombo.ValidValues.Add("SAM", "Sample Items");
            //oCombo.ValidValues.Add("BAT", "Batch Items");
            //oCombo.Select("Both", SAPbouiCOM.BoSearchKey.psk_ByValue);

            SAPbouiCOM.Item oItem = oForm.Items.Item("IDH_DEPART");
            SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
            doFillCombo(oForm, oCombo, "OUDP", "OUDP.Name", "OUDP.Name", null, "OUDP.Name", "Any", "");

            oItem = oForm.Items.Item("IDH_TSKST");
            oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
            doFillCombo(oForm, oCombo, "[@IDH_LABSTATS]", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Type = 'ExtStatus'", "[@IDH_LABSTATS].U_Status", "Any", "");

            oItem = oForm.Items.Item("IDH_DECI");
            oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
            doFillCombo(oForm, oCombo, "[@IDH_LABSTATS]", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Type = 'Decision'", "[@IDH_LABSTATS].U_Status", "Any", "");

        }

        protected void doFillGridCombos(SAPbouiCOM.Form oForm) {
            UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
            SAPbouiCOM.ComboBoxColumn oCombo;

            oCombo = (SAPbouiCOM.ComboBoxColumn)oGridN.getSBOGrid().Columns.Item(oGridN.doIndexFieldWC("U_RowStatus"));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            oCombo.ExpandType = BoExpandType.et_DescriptionOnly;// ; = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            com.idh.bridge.utils.Combobox2.doClearCombo(oCombo);
            com.idh.bridge.utils.Combobox2.doFillCombo(oCombo, "[@IDH_LABSTATS]", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Type = 'RowStatus'", "[@IDH_LABSTATS].U_Status");

            //doFillDepartmentComboinGrid(oForm);
            oCombo = (SAPbouiCOM.ComboBoxColumn)oGridN.getSBOGrid().Columns.Item(oGridN.doIndexFieldWC("U_Department"));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            oCombo.ExpandType = BoExpandType.et_DescriptionOnly;
            com.idh.bridge.utils.Combobox2.doClearCombo(oCombo);
            com.idh.bridge.utils.Combobox2.doFillCombo(oCombo, "OUDP", "OUDP.Code", "OUDP.Name", null, "OUDP.Name","");

            //doFillUserComboinGrid(oForm);
            oCombo = (SAPbouiCOM.ComboBoxColumn)oGridN.getSBOGrid().Columns.Item(oGridN.doIndexFieldWC("U_AssignedTo"));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            com.idh.bridge.utils.Combobox2.doClearCombo(oCombo);
            //oCombo.ValidValues.Add("", "");
            oCombo.ExpandType = BoExpandType.et_DescriptionOnly;
            com.idh.bridge.utils.Combobox2.doFillCombo(oCombo, "OUSR", "USER_CODE", "U_NAME", null, "U_NAME","");

            oCombo = (SAPbouiCOM.ComboBoxColumn)oGridN.getSBOGrid().Columns.Item(oGridN.doIndexFieldWC("U_ExtStatus"));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            oCombo.ExpandType = BoExpandType.et_DescriptionOnly;
            com.idh.bridge.utils.Combobox2.doClearCombo(oCombo);
            com.idh.bridge.utils.Combobox2.doFillCombo(oCombo, "[@IDH_LABSTATS]", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Type = 'ExtStatus'", "[@IDH_LABSTATS].U_Status");

            oCombo = (SAPbouiCOM.ComboBoxColumn)oGridN.getSBOGrid().Columns.Item(oGridN.doIndexFieldWC("U_Decision"));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            oCombo.ExpandType = BoExpandType.et_DescriptionOnly;
            com.idh.bridge.utils.Combobox2.doClearCombo(oCombo);
            com.idh.bridge.utils.Combobox2.doFillCombo(oCombo, "[@IDH_LABSTATS]", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Type = 'Decision'", "[@IDH_LABSTATS].U_Status");
        }
        //private void doFillUserComboinGrid(SAPbouiCOM.Form oForm) {
        //    UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
        //    //SAPbouiCOM.ComboBoxColumn oComboUser;            
        //    SAPbouiCOM.ComboBoxColumn oCombo;
        //    oCombo = (SAPbouiCOM.ComboBoxColumn)oGridN.getSBOGrid().Columns.Item(oGridN.doIndexFieldWC("U_AssignedTo"));
        //    oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
        //    com.idh.bridge.utils.Combobox2.doClearCombo(oCombo);
        //    com.idh.bridge.utils.Combobox2.doFillCombo(oCombo, "OUSR", "USER_CODE", "U_NAME", null, "U_NAME");
        //    oCombo.ValidValues.Add("", "");

        //}
        //private void doFillDepartmentComboinGrid(SAPbouiCOM.Form oForm) {
        //    UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
        //    SAPbouiCOM.ComboBoxColumn oCombo;
        //    oCombo = (SAPbouiCOM.ComboBoxColumn)oGridN.getSBOGrid().Columns.Item(oGridN.doIndexFieldWC("U_AssignedTo"));
        //    oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
        //    com.idh.bridge.utils.Combobox2.doClearCombo(oCombo);
        //    com.idh.bridge.utils.Combobox2.doFillCombo(oCombo, "OUSR", "USER_CODE", "U_NAME", null, "U_NAME");
        //    oCombo.ValidValues.Add("", "");
        //}

        protected void doAddNewRow(IDHAddOns.idh.controls.UpdateGrid oGridN) {
            string sImgPath1 = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn1.png");
            string sImgPath1g = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn1_Green.png");
            string sImgPath2 = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn2.png");
            string sImgPath2g = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn2_Green.png");
            string sImgPath3 = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn3.png");
            string sImgPath3g = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn3_Green.png");


            IDH_LABTASK oLabTask = new IDH_LABTASK();
            NumbersPair oNxtNo = oLabTask.getNewKey();
            
            int iNewRow = oGridN.getRowCount() - 1;

            int iCurrentRow = 0; oGridN.getCurrentDataRowIndex();
            if (oGridN.getSelectedRows().Count > 0)
                iCurrentRow =(int) oGridN.getSelectedRows()[0];
            //oGridN.setCurrentDataRowIndex(iNewRow);
            oGridN.doSetFieldValue("Code", iNewRow, oNxtNo.CodeCode,true,false,false);
            oGridN.doSetFieldValue("Name", iNewRow, oNxtNo.NameCode,true,false,false);
            oGridN.doSetFieldValue("U_LabItemRCd", iNewRow, oGridN.doGetFieldValue("U_LabItemRCd", iCurrentRow));
            oGridN.doSetFieldValue("U_LabICd", iNewRow, oGridN.doGetFieldValue("U_LabICd", iCurrentRow));
            oGridN.doSetFieldValue(IDH_LABTASK._LabINm, iNewRow, oGridN.doGetFieldValue(IDH_LABTASK._LabINm, iCurrentRow));
            //oGridN.doSetFieldValue(IDH_LABTASK._Saved, iNewRow,1);
            string sObjectType = oGridN.doGetFieldValue("U_ObjectType", iCurrentRow).ToString().Trim();
            if (sObjectType == null || sObjectType == string.Empty)
                sObjectType = "MAN";
            else if (!sObjectType.Contains("MAN"))
                sObjectType = "MAN-" + sObjectType;
            oGridN.doSetFieldValue("U_ObjectType", iNewRow, sObjectType);
            oGridN.doSetFieldValue(IDH_LABTASK._DocNum, iNewRow, oGridN.doGetFieldValue(IDH_LABTASK._DocNum, iCurrentRow));
            oGridN.doSetFieldValue("U_ObjectCd", iNewRow, oGridN.doGetFieldValue("U_ObjectCd", iCurrentRow));
            oGridN.doSetFieldValue("U_BPCode", iNewRow, oGridN.doGetFieldValue("U_BPCode", iCurrentRow));
            oGridN.doSetFieldValue("U_BPName", iNewRow, oGridN.doGetFieldValue("U_BPName", iCurrentRow));

            oGridN.doSetFieldValue("U_DtRequested", iNewRow, DateTime.Now);

            oGridN.doSetFieldValue("U_BPName", iNewRow, oGridN.doGetFieldValue("U_BPName", iCurrentRow));
            oGridN.doSetFieldValue("U_RowStatus", iNewRow, "Requested");

            oGridN.doSetFieldValue("U_DtReceived", iNewRow, "");
            oGridN.doSetFieldValue("U_DtAnalysed", iNewRow, "");
            oGridN.doSetFieldValue("U_Department", iNewRow, oGridN.doGetFieldValue("U_Department", iCurrentRow));
            oGridN.doSetFieldValue("U_AssignedTo", iNewRow, oGridN.doGetFieldValue("U_AssignedTo", iCurrentRow));

            oGridN.doSetFieldValue("U_AnalysisReq", iNewRow, oGridN.doGetFieldValue("U_AnalysisReq", iCurrentRow));
            oGridN.doSetFieldValue("( CASE WHEN LEN(ISNULL(U_AnalysisReq, '')) > 0 THEN  ( select '" + sImgPath1g + "') ELSE (select '" + sImgPath1 + "') END )" ,
               iNewRow, oGridN.doGetFieldValue("( CASE WHEN LEN(ISNULL(U_AnalysisReq, '')) > 0 THEN  ( select '" + sImgPath1g + "') ELSE (select '" + sImgPath1 + "') END )", iCurrentRow));

            oGridN.doSetFieldValue(IDH_LABTASK._ExtTsDt, iNewRow, oGridN.doGetFieldValue(IDH_LABTASK._ExtTsDt, iCurrentRow));


            oGridN.doSetFieldValue("U_ExtStatus", iNewRow, "NONE");
            oGridN.doSetFieldValue("U_ExtComments", iNewRow, "");


            //oGridN.doSetFieldValue("U_AtcAbsEntry", iNewRow, 0);
            oGridN.doSetFieldValue("( CASE WHEN U_AtcAbsEntry > 0 THEN  ( select '" + sImgPath2g + "') ELSE (select '" + sImgPath2 + "') END )",
               iNewRow, oGridN.doGetFieldValue("( CASE WHEN U_AtcAbsEntry > 0 THEN  ( select '" + sImgPath2g + "') ELSE (select '" + sImgPath2 + "') END )", iCurrentRow));

            oGridN.doSetFieldValue("U_ImpResults", iNewRow, "");
            oGridN.doSetFieldValue("( CASE WHEN LEN(ISNULL(U_ImpResults, '')) > 0 THEN  ( select '" + sImgPath3g + "') ELSE (select '" + sImgPath3 + "') END )",
                iNewRow, oGridN.doGetFieldValue("( CASE WHEN LEN(ISNULL(U_ImpResults, '')) > 0 THEN  ( select '" + sImgPath3g + "') ELSE (select '" + sImgPath3 + "') END )", iCurrentRow));

            oGridN.doSetFieldValue("U_Decision", iNewRow, "Pending");
            oGridN.doSetFieldValue(IDH_LABTASK._Saved, iNewRow, 1);
            
            doSetMode(oGridN.getSBOForm(), SAPbouiCOM.BoFormMode.fm_UPDATE_MODE);
        }
        #endregion
    }
}