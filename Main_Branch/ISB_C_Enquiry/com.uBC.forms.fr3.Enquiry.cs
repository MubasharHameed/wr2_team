﻿using System.IO;

using System.Collections;
using System;
using SAPbouiCOM;

//Needed for the Framework controls
using IDHAddOns.idh.controls;
using IDHAddOns;
using com.idh.bridge.data;
using com.idh.bridge;
using com.idh.bridge.lookups;
using com.uBC.utils;
using com.idh.controls;
using com.idh.forms.oo;
using com.idh.dbObjects;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry {
    public class Enquiry : com.idh.forms.oo.Form {
        private IDH_ENQUIRY moEnquiry;
        public IDH_ENQUIRY DBEnquiry {
            get { return moEnquiry; }
        }
        private DBOGrid moGrid;
        private string msEnquiryCode = "-1";
       
        private bool mbAddUpdateOk = false;
        private bool mbLoadEnquiryById = false;
        private Int32 mbActivityID = 0;
        private bool mbLoadReadOnly = false;
        private idh.dbObjects.User.IDH_FORMSET moFormSettings;

        private string _CardCode = "", _CardName = "", _AttendUser = "", _Notes = "", _Phone1 = "", _CName = "";
        private DateTime _Recontact;

        public Enquiry(IDHAddOns.idh.forms.Base oIDHForm, string sParentId, SAPbouiCOM.Form oSBOForm)
            : base(oIDHForm, sParentId, oSBOForm) {
            doSetHandlers();

            //doInitialize();

            //Set the Data Object for this Form
            moEnquiry = new IDH_ENQUIRY(oIDHForm, oSBOForm);
            moEnquiry.MustLoadChildren = true;
        }

        public Enquiry(string sParentId, string sEnquiryCode)
            : base(null, sParentId, null) {
            doSetHandlers();

            msEnquiryCode = sEnquiryCode;
            mbLoadEnquiryById = true;
            bLoadReadOnly = false;
        }

        public new static IDHAddOns.idh.forms.Base doRegisterFormClass(string sParMenu, int iMenuePos) {
            com.idh.forms.oo.FormController owForm = new com.idh.forms.oo.FormController("IDH_ENQ", sParMenu, iMenuePos, "Enq_Enquiry.srf", true, true, false, "Enquiry", IDHAddOns.idh.forms.Base.load_Types.idh_LOAD_NORMAL);
            owForm.doEnableHistory(IDH_ENQUIRY.TableName);
            return owForm;
        }

        //protected void doInitialize() {
        //    if (moEnquiry == null )
        //        moEnquiry = new IDH_ENQUIRY(IDHForm, SBOForm);
        //    moEnquiry.MustLoadChildren = true;

        //    doSetHandlers();
        //    //moEnquiry.AutoAddEmptyLine = false;
        //    //moEnquiry.EnquiryItems.AutoAddEmptyLine = true;
        //}

#region Properties
        //private string moEnquiryID;
        public Int32 ActivityID {
            get {
                if (moEnquiry.U_ClgCode != 0)
                    mbActivityID = moEnquiry.U_ClgCode;
                return mbActivityID;
            }
            set { mbActivityID = value; }
        }

        //LPV - This can cause issues as the two values does not set and reflect the same variable
        public string EnquiryID {
            get { return moEnquiry.Code; }
            set { msEnquiryCode = value; }
        }

        public bool bLoadEnquiry {
            get { return mbLoadEnquiryById; }
            set { mbLoadEnquiryById = value; }
        }
        public bool bAddUpdateOk {
            get { return mbAddUpdateOk; }
            set { mbAddUpdateOk = value; }
        }
        public bool bFromActivity {
            get { return moEnquiry.bFromActivity; }
            set { moEnquiry.bFromActivity = value; }
        }
        public bool bLoadReadOnly {
            get { return mbLoadReadOnly; }
            set { mbLoadReadOnly = value; }
        }

        public string CardCode {
            get { return moEnquiry.U_CardCode; }
            set { _CardCode = value; }
        }
        public string CardName {
            get { return moEnquiry.U_CardName; }
            set { _CardName= value; }
        }

        public DateTime Recontact {
            get { return moEnquiry.U_Recontact; }
            set { _Recontact = value; }
        }
 
        public string AttendUser{
            get { return moEnquiry.U_AttendUser; }
            set { _AttendUser = value; }
        }
        
        public string Notes {
            get { return moEnquiry.U_Notes; }
            set { _Notes = value; }
        }
        
        public string ContactName {
            get { return moEnquiry.U_CName; }
            set { _CName= value; }
        }

        public string ContactPhone {
            get { return moEnquiry.U_Phone1; }
            set { _Phone1= value; }
        }
       
        //public bool bLoadEnquiry {
        //    get { return mbLoadEnquiryById; }
        //    set { mbLoadEnquiryById = value; }
        //}

#endregion

#region FormOpenCreateFunctions
        /** 
         * This Function will be called by the controller to allow the class to last minute Form Layout adjustments before it gets displayed
         * All changes made in hete will be cached if this is a cached form, the method will not be called again once the form has benn cached.
         */
        public override void doCompleteCreate(ref bool BubbleEvent) {
            /**
             * Browser By is set using the Form Item Id for the Data Key field
             */
            DataBrowser.BrowseBy = "IDH_DOCNUM"; // "uBC_VEHREG";
            SupportedModes = BoAutoFormMode.afm_All;
            AutoManaged = true;

            // doSetFilterFields();
            //if (!mbLoadEnquiryById)
            //    Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE;
            if (SBOParentForm!=null || moEnquiry.bFromActivity || mbLoadReadOnly) {
                //disable Menue Buttons
                SBOForm.EnableMenu("1281", false);
                SBOForm.EnableMenu("1282", false);
                SBOForm.EnableMenu("1290", false);
                SBOForm.EnableMenu("1288", false);
                SBOForm.EnableMenu("1289", false);
                SBOForm.EnableMenu("1291", false);
            }
        }

        /** 
         * Do the final form steps to show before loaddata
         */
        public override void doBeforeLoadData() {
            try {
                if (moEnquiry == null) {
                    moEnquiry = new IDH_ENQUIRY(IDHForm, SBOForm);
                    moEnquiry.MustLoadChildren = true;
                }
                Title = IDHForm.gsTitle;
                moGrid = new DBOGrid(IDHForm, SBOForm, "LINESGRID", moEnquiry.EnquiryItems);
                if (getSharedData("IDH_OPNENQ") == null || getSharedData("IDH_OPNENQ").ToString() != "Y") {
                    moGrid.doSetDeleteActive(true);
                }
                doTheGridLayout();
                doSetDBHandlers();
                doSetGridHandlers();
                doFillCombos();
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
            }
        }

        /** 
         * Load the Form Data
         */
        public override void doLoadData() {
            SAPbouiCOM.Item oItem;
            oItem = Items.Item("IDH_DOCNUM");
            oItem.SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable,
                (int)SAPbouiCOM.BoFormMode.fm_ADD_MODE + (int)SAPbouiCOM.BoFormMode.fm_EDIT_MODE +
                (int)SAPbouiCOM.BoFormMode.fm_OK_MODE + (int)SAPbouiCOM.BoFormMode.fm_PRINT_MODE +
                (int)SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + (int)SAPbouiCOM.BoFormMode.fm_VIEW_MODE,
                SAPbouiCOM.BoModeVisualBehavior.mvb_False);
            //  Items.Item("IDH_DOCNUM").SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, (int)SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + (int)SAPbouiCOM.BoFormMode.fm_ADD_MODE +(int)SAPbouiCOM.BoFormMode.fm_OK_MODE  , BoModeVisualBehavior.mvb_False);
            oItem.SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable,
                (int)SAPbouiCOM.BoFormMode.fm_FIND_MODE,
                BoModeVisualBehavior.mvb_True);

            oItem = Items.Item("IDH_CRTWOQ");
            oItem.SetAutoManagedAttribute(BoAutoManagedAttr.ama_Visible,
                (int)SAPbouiCOM.BoFormMode.fm_ADD_MODE + (int)SAPbouiCOM.BoFormMode.fm_EDIT_MODE +
                (int)SAPbouiCOM.BoFormMode.fm_PRINT_MODE + (int)SAPbouiCOM.BoFormMode.fm_FIND_MODE +
                (int)SAPbouiCOM.BoFormMode.fm_UPDATE_MODE,
                SAPbouiCOM.BoModeVisualBehavior.mvb_False);

            //  Items.Item("IDH_DOCNUM").SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, (int)SAPbouiCOM.BoFormMode.fm_UPDATE_MODE + (int)SAPbouiCOM.BoFormMode.fm_ADD_MODE +(int)SAPbouiCOM.BoFormMode.fm_OK_MODE  , BoModeVisualBehavior.mvb_False);
            oItem.SetAutoManagedAttribute(BoAutoManagedAttr.ama_Visible,
                (int)SAPbouiCOM.BoFormMode.fm_OK_MODE +
                (int)SAPbouiCOM.BoFormMode.fm_VIEW_MODE,
                BoModeVisualBehavior.mvb_True);

            if (mbLoadEnquiryById) {//((mbFromActivity || mbLoadReadOnly) && mbLoadEnquiryById) {
                if (!string.IsNullOrWhiteSpace(msEnquiryCode) && msEnquiryCode != "0") {
                    if (moEnquiry.getByKey(msEnquiryCode)) {
                        //Freeze(true);
                        FillCombos.FillState(Items.Item("IDH_STATE").Specific, moEnquiry.U_Country, null);
                        doSwitchToEdit();
                        moGrid.doApplyRules();
                        doFillGridCombos();
                        if (bLoadReadOnly)
                            Mode = BoFormMode.fm_VIEW_MODE;
                        else
                            Mode = BoFormMode.fm_OK_MODE;
                        //Freeze(false);
                    }
                }
                // Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
            } else {
                Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE;
                if (!string.IsNullOrEmpty(_CardCode))
                    moEnquiry.U_CardCode = _CardCode;
                if (!string.IsNullOrEmpty(_CardName))
                    moEnquiry.U_CardName = _CardName;
                if (!string.IsNullOrEmpty(_AttendUser))
                    moEnquiry.U_AttendUser = _AttendUser;
                if (_Recontact != null && _Recontact.ToString() != string.Empty && _Recontact!=DateTime.MinValue)
                    moEnquiry.U_Recontact = _Recontact;
                if (!string.IsNullOrEmpty(_CName))
                    moEnquiry.U_CName = _CName;
                if (!string.IsNullOrEmpty(_Phone1))
                    moEnquiry.U_Phone1 = _Phone1;
                if (!string.IsNullOrEmpty(_Notes))
                    moEnquiry.U_Notes = _Notes;
            }

            //doFillGridCombos();
        }
        /** 
         * Do the final form steps to show after loaddata
         */
        public override void doFinalizeShow() {
            base.doFinalizeShow();
        }
        #endregion

        #region Events
        /**
         * Register all your event Handlers in here.
         * Handler_ALL_EVENTS 0 All events, for use when filtering events 
         * Handler_ITEM_PRESSED 1 The main mouse button was clicked on one of the following: 
         * 		Tab 
         * 		Button 
         * 		Option button 
         * 		Check box (standalone or within a cell)
         * 		This event occurs when a mouse is released within an item, that is, mouse up.
         * Handler_KEY_DOWN 2 A key was pressed. 
         * Handler_GOT_FOCUS 3 An item received focus. 
         * Handler_LOST_FOCUS 4 An item lost focus. 
         * Handler_LOST_FOCUS_CHANGED 4 An item lost focus with the value changed
         * Handler_COMBO_SELECT 5 A value was selected in a combo box. 
         * Handler_CLICK 6 The main mouse button was clicked on an item, that is, mouse down. 
         * Handler_DOUBLE_CLICK 7 The main mouse button was double-clicked on an item. 
         * Handler_MATRIX_LINK_PRESSED 8 A link arrow in a matrix was pressed. 
         * Handler_MATRIX_COLLAPSE_PRESSED 9 A matrix list was collapsed or expanded. 
         * Handler_VALIDATE 10 An item lost focus and validation is required.
         * Handler_VALIDATE_CHANGED 10 An item lost focus and validation is required - only when the data has changed.
         * Handler_MATRIX_LOAD 11 Data was loaded from the database into a matrix data source. 
         * 		This event occurs for user-defined matrix objects only. If a form contains two matrix objects, the system generates two events.
         * Handler_DATASOURCE_LOAD 12 Data was loaded from the GUI into a matrix data source. 
         * 		This event occurs once per matrix. If a form has two matrix objects, the system generates two events.
         * Handler_FORM_LOAD 16 A form was opened (FormDataEvent). 
         * Handler_FORM_UNLOAD 17 A form was closed. 
         * Handler_FORM_ACTIVATE 18 A form received focus. 
         * Handler_FORM_DEACTIVATE 19 A form lost focus. 
         * Handler_FORM_CLOSE 20 A form is about to be closed. 
         * Handler_FORM_RESIZE 21 A form has been resized. 
         * Handler_FORM_KEY_DOWN 22 A key was pressed when no form had the focus. 
         * Handler_FORM_MENU_HILIGHT 23 A form is modifying the status of toolbar items, that is, enabling and disabling icons. 
         * 		By default, these events are not thrown. You can activate these events by setting an event filter.
         * Handler_PRINT 24 A print preview was requested for a report or document (PrintEvent). 
         * 		This event lets you exit from the application print operation and use your own printing.
         * Handler_PRINT_DATA 25 A print preview was requested for a report (ReportDataEvent).  
         * 		This event lets you get the report data in XML format.
         * Handler_CHOOSE_FROM_LIST 27 A ChooseFromList event occurred, as follows:  
         * 		The Before event occurs before the ChooseFromList form is displayed. If the BubbleEvent parameter is set to False, the form is not displayed. 
         * 		The After event occurs after the user makes a selection or chooses Cancel.		 
         * Handler_RIGHT_CLICK 28 The right mouse button was clicked on an item (RightClickEvent). 
         * Handler_MENU_CLICK 32 The main mouse button was released on a menu item without submenus. 
         * 		For future use.
         * Handler_FORM_DATA_ADD 33 A record in a business object was added (FormDataEvent). 
         * Handler_FORM_DATA_UPDATE 34 A record in a business object was updated (FormDataEvent). 
         * Handler_FORM_DATA_DELETE 35 A record in a business object was deleted (FormDataEvent). 
         * Handler_FORM_DATA_LOAD 36 A record in a business object was loaded -- via browse, link button, or find (FormDataEvent). 
         * Handler_PICKER_CLICKED 37 A Picker event occurred, as follows:  
         * 		The Before event occurs before the Picker form is displayed. If the BubbleEvent parameter is set to False, the picker is not displayed. 
         * 		The After event occurs after the user makes a selection or chooses Cancel.
         * Handler_GRID_SORT 38 A grid column was sorted, either by a user clicking the column title or an add-on calling the Sort method of the Grid object. 
         * Handler_Drag 39 The item was dragged and dropped at the position you want it to appear. 
         * Handler_PRINT_LAYOUT_KEY 80 A print or print preview was requested for an add-on form, and the layout needs the key of the add-on form 		
         * 
         * To Add a Item PressEvent for a specific Control use 
         * addHandler_ITEM_PRESSED("ITEMID", new ev_Item_Event(EventFunction));
         *      e.g. addHandler_ITEM_PRESSED("uBCCardSR", new ev_Item_Event(doHandleCardCodeSearch));
         *      
         *           public bool doHandleCardCodeSearch(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
         *               return false;
         *           }
         *           
         *  The following Special Event Handlers can be set when using the SBO default where Button 1 is OK 
         *  Handler_Button_Ok - When the Form is in Ok or View Mode
         *  Handler_Button_Find - When the Form is in Find Mode
         *  Handler_Button_Add - When the Form is in Add Mode
         *  Handler_Button_Update - When the Form is in Update Mode
         *   
         * The Menu Events
         * Handler_Menu_NAV_ADD
		 * Handler_Menu_NAV_FIND
		 * Handler_Menu_NAV_FIRST
		 * Handler_Menu_NAV_LAST
		 * Handler_Menu_NAV_NEXT
         * Handler_Menu_NAV_PREV
         * --------> If the BubbleEvent is not set to False the normal Overridable Function doMenuEvent will be called 
         *           doMenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent)
         *
         * Special Handler that will be called if this form was opened as a Dialog (Opened from another Form)
         * Handler_DialogButton1Return - When Button 1 was pressed irrespective of the Form Mode
         * Handler_DialogOkReturn - When Button 1 was Pressed and Form was in Ok/View Mode
         * Handler_DialogCancelReturn = When Button 2 was Pressed
         * --------> This will only be called if all the other Button 1 /2 events where called including the Overridable 
         *           Function doButtonID1 and the Bubble event is still true.
         */
        private void doSetHandlers() {
            doStartEventFilterBatch(true);

            Handler_Button_Add += doAddUpdateEvent;
            Handler_Button_Update += doAddUpdateEvent;
            Handler_Button_Find += doFindEvent;
            Handler_VALIDATE_CHANGED += Handle_ValidateEvent;

            Handler_FormDataEvent += doHandleFormDataLoad;
            //addHandler_CLICK("2", doCancelButton);
            //Handler_FORM_CLOSE = dohandleformclose;
            //addHandler_ITEM_PRESSED("2", doCancelButton);
            doAddEvent(BoEventTypes.et_ITEM_PRESSED);
            addHandler_ITEM_PRESSED("IDH_CONREF", doRefeshContact);
            addHandler_ITEM_PRESSED("IDH_QUSTN", doShowQuestionnaire);
            addHandler_ITEM_PRESSED("IDH_FINDAD", doFindAddress);
            addHandler_ITEM_PRESSED("IDH_CRETBP", doCreateBP);
            addHandler_ITEM_PRESSED("IDH_CRTWOQ", doCreateWOQNew);
            addHandler_ITEM_PRESSED("IDH_ADRCFL", doSearchBPAddressCFL);
            //addHandler_VALIDATE("IDH_CardNM", dovalidateBPEvent);
            addHandler_VALIDATE_CHANGED("IDH_CardNM", dovalidateBPChangeEvent);
           // addHandler_VALIDATE_CHANGED_DELAYED("IDH_CardNM",new[] string{ "IDH_NAME"} dovalidateBPEvent)
            addHandler_COMBO_SELECT("IDH_CUNTRY", doHandleCountryCombo);

            addHandler_VALIDATE_CHANGED("IDH_ADR1", doHandleAddressChanged);
            addHandler_VALIDATE_CHANGED("IDH_STREET", doHandleAddressChanged);
            addHandler_VALIDATE_CHANGED("IDH_BLOCK", doHandleAddressChanged);
            addHandler_VALIDATE_CHANGED("IDH_CITY", doHandleAddressChanged);
            addHandler_VALIDATE_CHANGED("IDH_PCODE", doHandleAddressChanged);
            addHandler_VALIDATE_CHANGED("IDH_COUNTY", doHandleAddressChanged);
            addHandler_COMBO_SELECT("IDH_STATE", doHandleAddressChanged);
            //Handler_RIGHT_CLICK = doHandleFormRightClick;
            //  Handler_FORM_CLOSE = doHandleFormClose;

            //   addHandler_ITEM_PRESSED("B1_Arrow", new ev_Item_Event(doWOOpenEvent));
            //  addHandler_ITEM_PRESSED("B2_Arrow", new ev_Item_Event(doDISPORDOpenEvent));

            //  addHandler_COMBO_SELECT("uBC_CDFDNU", new ev_Item_Event(doSelectDriverEvent));

            //addHandler_VALIDATE_CHANGED("uBC_VEHBTO", new ev_Item_Event(doBelongsToCodeChangedEvent));
            // addHandler_CLICK("IDH_CRCFL", new ev_Item_Event(doBelongsToCFLClickEvent));
            //Menu Events
            Handler_Menu_NAV_ADD += doMenuAddEvent;
            Handler_Menu_NAV_FIRST += doMenuBrowseEvents;
            Handler_Menu_NAV_LAST += doMenuBrowseEvents;
            Handler_Menu_NAV_NEXT += doMenuBrowseEvents;
            Handler_Menu_NAV_PREV += doMenuBrowseEvents;
            Handler_Menu_NAV_FIND += doMenuFindBUttonEvent;
            Handler_FormMode_Changed += doHandleModeChange;
            
            doCommitEventFilters();
        }
        private void doSetDBHandlers() {
            //BO - Business Object
            moEnquiry.Handler_RecordAdded += Handle_RecordProcessed;
            moEnquiry.Handler_RecordUpdated += Handle_RecordProcessed;
            moEnquiry.Handler_RecordDeleted += Handle_RecordProcessed;

            moEnquiry.Handler_RowDataLoaded += Handle_EnquiryLoaded;
            moEnquiry.EnquiryItems.Handler_RowDataLoaded += Handle_EnquiryItemsLoaded;
        }
        protected void doSetGridHandlers() {
            // moGrid.Handler_GRID_FIELD_CHANGED = doHandleGridFieldValueChanged;
            doStartEventFilterBatch(true);
            moGrid.Handler_GRID_SEARCH = doHandleGridSearch;
            moGrid.Handler_GRID_SEARCH_VALUESET = doHandleGridSearchValueSet;
            moGrid.Handler_GRID_DOUBLE_CLICK = doHandleGridDoubleClick;
            moGrid.Handler_GRID_RIGHT_CLICK = doHandleGridRightClick;
            moGrid.Handler_GRID_MENU_EVENT = doHandleGridMenuEvent;
            //moGrid.Handler_GRID_DOUBLE_CLICK = doHandleGridDoubleClick;
            moGrid.Handler_GRID_SORT = doHandleGridSort;
            //base.doSetGridHandlers();
            // moGrid.Handler_GRID_ROW_ADD_EMPTY = new IDHGrid.ev_GRID_EVENTS(doItemRowAdded);
            //moGrid.Handler_GRID_DOUBLE_CLICK = new IDHGrid.ev_GRID_EVENTS(doRowDoubleClickEvent);
            doCommitEventFilters();
        }

        #region synchData
        /**
         * This will update the Underlying DataLayer and the update the form with the calculated values from the DataLayer
         */
        public void doSynchFormItemData(string sItemId) {
            Freeze(true);
            try {
                FormController.DataBindInfo oBindInfo = getBindInfo(sItemId);
                if (oBindInfo.msTableName != null && oBindInfo.msTableName.Equals(moEnquiry.DBOTableName)) {
                    moEnquiry.doMarkUserUpdate(oBindInfo.msField);

                    if (!moEnquiry.IsActiveRow())
                        moEnquiry.doActivateRow(moEnquiry.CurrentRowIndex);
                }
            } catch (Exception e) {
                DataHandler.INSTANCE.doError(e.Message);
            } finally {
                Freeze(false);
            }
        }
#endregion

#region Handlers
        private void Handle_RecordProcessed(DBBase oDBObject) {
            //if (Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE || Mode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE || Mode == SAPbouiCOM.BoFormMode.fm_EDIT_MODE) {
            //    if (Mode == BoFormMode.fm_ADD_MODE && mbFromActivity == false) {
            //        string msg = com.idh.bridge.resources.Messages.getGMessage("INFDOCNO", new string[] { "Enquiry", moEnquiry.Code });
            //        com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(msg, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            //        doCreateActivity(false);
            //        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
            //        Mode = BoFormMode.fm_ADD_MODE;
            //    } else {
            //        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
            //        doCreateActivity(true);
            //        Mode = BoFormMode.fm_OK_MODE;
            //    }
            //}
        }

        private void Handle_EnquiryItemsLoaded(DBBase oCaller) {
            FillCombos.FillState(Items.Item("IDH_STATE").Specific, moEnquiry.U_Country, null);
            //moEnquiry.doLoadChildren();
            if (moEnquiry.EnquiryItems.Count == 0) {
                moGrid.doAddEditLine(true);
            }
            moGrid.doApplyRules();
            doFillGridCombos();
           
            //EnableItem(false, "IDH_DOCNUM");
            //string sCardName = "";
            //if (doValidateBPbyCode(ref sCardName) == true) {
            //    EnableItem(false, "IDH_CRETBP");
            //} else
            //    EnableItem(true, "IDH_CRETBP");
            //moEnquiry.DoAddressChanged = false;
            ////  doSetWidths();
        }

        private void Handle_EnquiryLoaded(DBBase oCaller) {
            moEnquiry.DoAddressChanged = false;
            EnableItem(false, "IDH_DOCNUM");
            string sCardName = "";
            if (doValidateBPbyCode(ref sCardName) == true) {
                EnableItem(false, "IDH_CRETBP");
            } else
                EnableItem(true, "IDH_CRETBP");
        }

        public bool Handle_ValidateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction && pVal.ActionSuccess) {
                doSynchFormItemData(pVal.ItemUID);
            }
            return true;
        }

        private bool doFindEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                Freeze(true);
                try {
                    string sHeader = (string)getFormDFValue(IDH_ENQUIRY._Code, true);
                    //moEnquiry.sed= false;
                    if (!moEnquiry.getByKey(sHeader)) {
                        //moGrid.doAddEditLine(true);
                        //moGrid.doApplyRules();
                        Mode = BoFormMode.fm_FIND_MODE;
                        doSwitchToFind();
                       // doSetWidths();
                        BubbleEvent = false;
                        return false;
                    } else {
                        Mode = BoFormMode.fm_OK_MODE;
                        FillCombos.FillState(Items.Item("IDH_STATE").Specific, moEnquiry.U_Country, null);
                        doSwitchToEdit();
                        //moGrid.doApplyRules();
                        doFillGridCombos();
                      //  doSetWidths();
                        BubbleEvent = false;
                        return false;
                    }
                } catch (Exception ex) {
                    throw (ex);
                } finally {
                    //moEnquiry.ApplyDefaultsOnNewRows = true;
                    Freeze(false);
                }
            }

            return true;
        }
        private bool doSearchBPAddressCFL(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction && moEnquiry.U_CardCode.Trim() != string.Empty) {
                doChooseBPAddress();
            }
            return true;
        }
        private bool doHandleAddressChanged(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            //if (pVal.BeforeAction) {
            //if (pVal.ItemUID == "IDH_STATE") {
            //    moEnquiry.doMarkUserUpdate(IDH_ENQUIRY._State, true);
            //    return true;
            //}
            //SAPbouiCOM.Item oItem = SBOForm.Items.Item(pVal.ItemUID);
            //SAPbouiCOM.EditText oTxt = (SAPbouiCOM.EditText)oItem.Specific;
            //switch (pVal.ItemUID) {
            //    case "IDH_ADR1":
            //        moEnquiry.doMarkUserUpdate(IDH_ENQUIRY._Address, true);
            //        break;
            //    case "IDH_STREET":
            //        moEnquiry.doMarkUserUpdate(IDH_ENQUIRY._Street, true);
            //        break;
            //    case "IDH_BLOCK":
            //        moEnquiry.doMarkUserUpdate(IDH_ENQUIRY._Block, true);
            //        break;
            //    case "IDH_CITY":
            //        moEnquiry.doMarkUserUpdate(IDH_ENQUIRY._City, true);
            //        break;
            //    case "IDH_PCODE":
            //        moEnquiry.doMarkUserUpdate(IDH_ENQUIRY._ZipCode, true);
            //        break;
            //    case "IDH_COUNTY":
            //        moEnquiry.doMarkUserUpdate(IDH_ENQUIRY._County, true);
            //        break;
            //    case "IDH_STATE":
            //        moEnquiry.doMarkUserUpdate(IDH_ENQUIRY._State, true);
            //        break;
            //}
            //}
            if (pVal.BeforeAction) {
                doSynchFormItemData(pVal.ItemUID);
            }
            return true;
        }
        private bool doHandleCountryCombo(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {

                //   SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_CUNTRY").Specific;
                //  SAPbouiCOM.ValidValue oValue = oCombo.Selected;
                FillCombos.FillState(Items.Item("IDH_STATE").Specific, moEnquiry.U_Country, null);

            }
            return true;
        }
        private bool dovalidateBPEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction && moEnquiry.U_CardName.Trim().IndexOf("*") > -1) {
                doChooseBPCode();
            }
            return true;
        }

        private bool dovalidateBPChangeEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction && moEnquiry.U_CardName.Trim() != string.Empty) {
                doChooseBPCode();
            } else if (pVal.BeforeAction && moEnquiry.U_CardName.Trim() == string.Empty) {
               moEnquiry.doMarkUserUpdate(IDH_ENQUIRY._CardName);
                // moEnquiry.U_CardCode = "";
            }
            return true;
        }
        public bool doCreateBP(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                if (Mode == BoFormMode.fm_UPDATE_MODE) {
                    com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage("UNSAVED");
                    BubbleEvent = false;
                } else if (moEnquiry.U_CardCode.Trim() != string.Empty && Config.INSTANCE.doGetBPName(moEnquiry.U_CardCode, false) != string.Empty) {
                    com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage(("ERPKEXIS"), new string[] { moEnquiry.U_CardCode });
                    BubbleEvent = false;
                }
            } else {
                //setSharedData("IDH_WSTGRP", "973");
                //doOpenModalForm("IDHADITMVLD");//IDHADDITMSR
                //return true;

                setFocus("IDH_ADR1");
                com.isb.forms.Enquiry.CreateBP oOOForm = new com.isb.forms.Enquiry.CreateBP(null, SBOForm.UniqueID, null);
                //string sAddress = (getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._Address).ToString() + " " +
                //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._Street).ToString() + " " +
                //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._City).ToString() + " " +
                //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._Country).ToString() + " " +
                //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._State).ToString() + " " +
                //                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._ZipCode).ToString()).Replace("  ", " ").Trim();
                oOOForm.EnqID = EnquiryID;
                oOOForm.setUFValue("IDH_CARDCD", moEnquiry.U_CardCode);
                oOOForm.setUFValue("IDH_CARDNM", moEnquiry.U_CardName);

                oOOForm.setUFValue("IDH_CARDTY", Config.INSTANCE.getParameterWithDefault("DFEQBPTP","C").ToString());

                oOOForm.setUFValue("IDH_ADRID", moEnquiry.U_Address);//Address ID 50
                oOOForm.setUFValue("IDH_STREET", moEnquiry.U_Street);//Street 100
                oOOForm.setUFValue("IDH_BLOCK", moEnquiry.U_Block);//Block 100
                oOOForm.setUFValue("IDH_CITY", moEnquiry.U_City);//City 100
                oOOForm.setUFValue("IDH_PCODE", moEnquiry.U_ZipCode);//State 3
                oOOForm.setUFValue("IDH_COUNTY", moEnquiry.U_County);//PostCode 20
                oOOForm.setUFValue("IDH_STATE", moEnquiry.U_State);//County 100
                oOOForm.setUFValue("IDH_CONTRY", moEnquiry.U_Country);//Country 3

                oOOForm.setUFValue("IDH_CNTPRS", moEnquiry.U_CName);//Contact Person 50
                oOOForm.setUFValue("IDH_EMAIL", moEnquiry.U_E_Mail);//Email 100
                oOOForm.setUFValue("IDH_PHONE", moEnquiry.U_Phone1);//Phone 20
                oOOForm.setUFValue("IDH_INFO", "Address:");//Phone 20
                oOOForm.setUFValue("IDH_INFO2", "Contact:");//Phone 20
                oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleCreateBPOKReturn);
                oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleCreateBPCancelReturn);
                oOOForm.doShowModal(SBOForm);
            }
            return true;
        }
        public bool doShowQuestionnaire(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            //com.isb.forms.Enquiry.EnqQuestions oOOForm = new com.isb.forms.Enquiry.EnqQuestions(null, SBOForm.UniqueID, null);
            //oOOForm.EnqID =Convert.ToInt32(moEnquiry.Code);
            //oOOForm.doShowModal(SBOForm);
            setSharedData("ENQID", moEnquiry.Code);
            setSharedData("WOQID", 0);
            doOpenModalForm("IDH_ENQQS");
        
            return true;
        }
        public bool doFindAddress(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction) {
                com.uBC.forms.fr3.PostCode oOOForm = new com.uBC.forms.fr3.PostCode(null, SBOForm.UniqueID, null);
                string sAddress = (getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._Address).ToString() + " " +
                                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._Street).ToString() + " " +
                                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._City).ToString() + " " +
                                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._Country).ToString() + " " +
                                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._State).ToString() + " " +
                                    getDFValue(IDH_ENQUIRY.TableName, IDH_ENQUIRY._ZipCode).ToString()).Replace("  ", " ").Trim();

                oOOForm.setUFValue("IDH_ADDRES", sAddress);
                oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandlePostCodeOKReturn);
                oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doHandlePostCodeCancelReturn);
                oOOForm.doShowModal(SBOForm);
            }
            return true;
        }
        private bool doRefeshContact(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction && moEnquiry.U_CardCode.Trim() != string.Empty && (Mode == BoFormMode.fm_OK_MODE || Mode == BoFormMode.fm_UPDATE_MODE || Mode == BoFormMode.fm_ADD_MODE)) {
                if (moEnquiry.U_CName != "") {
                    if (com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYNAsk("ENQCNREF", null, 2) != 1) {
                        return true;
                    }
                }
                ArrayList oContactData = Config.INSTANCE.doGetBPContactEmployee(moEnquiry.U_CardCode, "DEFAULTCONTACT");
                if (oContactData != null) {
                    moEnquiry.U_CName = oContactData[0].ToString();
                    moEnquiry.U_Phone1 = oContactData[4].ToString();
                    moEnquiry.U_E_Mail = oContactData[7].ToString();
                    if (Mode != BoFormMode.fm_ADD_MODE)
                        Mode = BoFormMode.fm_UPDATE_MODE;
                } else
                    DataHandler.INSTANCE.doUserError("Failed to find default contact.");
            }
            return true;
        }
        //public bool dohandleformclose(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (pVal.BeforeAction && (Mode == BoFormMode.fm_UPDATE_MODE || (Mode == BoFormMode.fm_ADD_MODE && moEnquiry.doFormHaveSomeData())) && com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYN("WRNUSAVE", null) == 2) {
        //        BubbleEvent = false;
        //        return false;
        //    } else if (!pVal.BeforeAction) {
        //        //this.doCloseForm(ref BubbleEvent);
        //        // return false;
        //    }
        //    return true;
        //}
        public bool doCancelButton(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction && (Mode == BoFormMode.fm_UPDATE_MODE || (Mode == BoFormMode.fm_ADD_MODE && moEnquiry.doFormHaveSomeData()))) {
                if (com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYN("WRNUSAVE", null) == 2) {
                    BubbleEvent = false;
                    return false;
                }
            }
            return true;
        }
        
        private bool doHandleFormDataLoad(ref SAPbouiCOM.BusinessObjectInfo oBusinessObjectInfo, ref bool BubbleEvent) {
            if (oBusinessObjectInfo.ActionSuccess && !oBusinessObjectInfo.BeforeAction) {
                //if (!string.IsNullOrEmpty(msEnquiryCode) && mbLoadEnquiryById) {
                if (!string.IsNullOrEmpty(moEnquiry.Code)) {
                    moEnquiry.doClearAddUpdateBuffers();
                    moEnquiry.doLoadRowData();
                }

                //Mode = BoFormMode.fm_OK_MODE;
                setFocus("IDH_ADR1");
                EnableItem(false, "IDH_DOCNUM");
       //         if (moEnquiry.U_CardCode != string.Empty) EnableItem(false, "IDH_CardNM");

                // doSetWidths();
            }
            return true;
        }

        //        if (BusinessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_LOAD && BusinessObjectInfo.ActionSuccess && BusinessObjectInfo.BeforeAction == false) {
        //            Mode = BoFormMode.fm_OK_MODE;
        //            setFocus("IDH_ADR1");
        //            EnableItem(false, "IDH_DOCNUM");
        //            if (moEnquiry.U_CardCode != string.Empty) EnableItem(false, "IDH_CardNM");
        //        }
        
        public bool doCreateWOQNew(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                if (Mode != BoFormMode.fm_OK_MODE) {
                    com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage("UNSAVED");
                    BubbleEvent = false;
                }
            } else {
                if (moGrid.getRowCount() == 0) {
                    doCreateWOQ("One", null);
                } else if (moGrid.getRowCount() == 1) {
                    for (Int16 iRow = 0; iRow <= moGrid.getRowCount() - 1; iRow++) {
                        if (moGrid.doGetFieldValue(IDH_ENQITEM._WOQID, iRow).ToString() == "")
                            doCreateWOQ("One", null);
                        //aSelectItems.Add(oGridN.doGetFieldValue("T1.U_EnqID", iRow).ToString() + ":" + oGridN.doGetFieldValue("T1.Code", iRow).ToString());
                    }
                } else if (moGrid.getRowCount() > 1) {
                    System.Collections.ArrayList aSelectItems = new ArrayList();
                    int iTotalItems = 0;
                    for (Int16 iRow = 0; iRow <= moGrid.getRowCount() - 1; iRow++) {
                        if ((moGrid.doGetFieldValue(IDH_ENQITEM._ItemDesc, iRow).ToString() != string.Empty || moGrid.doGetFieldValue(IDH_ENQITEM._ItemName, iRow).ToString() != string.Empty)
                            && moGrid.doGetFieldValue(IDH_ENQITEM._AddItm, iRow).ToString() == "" && moGrid.doGetFieldValue(IDH_ENQITEM._WOQID, iRow).ToString() == "")
                            iTotalItems++;
                        //aSelectItems.Add(oGridN.doGetFieldValue("T1.U_EnqID", iRow).ToString() + ":" + oGridN.doGetFieldValue("T1.Code", iRow).ToString());
                    }
                    if (iTotalItems == 1)
                        doCreateWOQ("One", null);
                    else if (iTotalItems > 1) {
                        setSharedData("IDH_BPNAME", moEnquiry.U_CardName);
                        setSharedData("IDH_BPCODE", moEnquiry.U_CardCode);
                        setSharedData("IDH_SOURCE", "ENQ");
                        setSharedData("IDH_ENQNUM", moEnquiry.Code);
                        if (moGrid.getRowCount() > 0) {
                            string aExistingEnquiryRows = "";// new ArrayList();                   
                            setSharedData("IDH_EXISTINGENQLINES", aExistingEnquiryRows);
                        }
                        doOpenModalForm("IDHSRENQ");
                    }

                }
            }
            return true;
        }
        //ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent
        public void doCreateWOQ(string sOption, System.Collections.ArrayList aSelectItems) {
             
                bool bAnyNewRow = false;
                try {

                    for (int i = 0; i <= moGrid.getRowCount() - 1; i++) {
                        if (moGrid.doGetFieldValue(IDH_ENQITEM._ItemName, i).ToString() != "" && moGrid.doGetFieldValue(IDH_ENQITEM._Status, i).ToString() != "" && moGrid.doGetFieldValue(IDH_ENQITEM._Status, i).ToString() == "1") {
                            bAnyNewRow = true;
                            break;
                        }
                    }
                } catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "Validating Enquiry Items before copying to WOQ." });
                    return;
                }
                if (false && !bAnyNewRow) {
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Error: No Row in Enquiry to copy on WOQ.", "ERVEQ001", null);
                   return;
                }
            //int iOption = 1;
            //int iTotalRows = 0;
            //for (int i = 0; i <= moEnquiry.EnquiryItems.Count - 1; i++) {

            //    if ((moEnquiry.EnquiryItems.getValueAsString(IDH_ENQITEM._ItemName) == string.Empty )
            //                 || (moEnquiry.EnquiryItems.getValueAsString(IDH_ENQITEM._AddItm) == "A")) {

            //    } else
            //        iTotalRows++;
            //}
            //if (iTotalRows > 1) {
            //    string sMsg = com.idh.bridge.resources.Messages.INSTANCE.getMessage("ENQMG002", null);
            //    iOption = com.idh.bridge.resources.Messages.INSTANCE.doMessage(sMsg, 1, Translation.getTranslatedWord("Combine"), Translation.getTranslatedWord("Split"));
            //}
            try {
                if (sOption == "One") {
                    //com.isb.forms.Enquiry.WOQuote oOOForm = new com.isb.forms.Enquiry.WOQuote(null, SBOForm.UniqueID, SBOForm);
                    com.isb.forms.Enquiry.WOQuote oOOForm = new com.isb.forms.Enquiry.WOQuote(SBOForm.UniqueID, "-1");
                    oOOForm.EnquiryID = moEnquiry.Code;
                    oOOForm.SelectItems = aSelectItems;
                    oOOForm.bLoadWOQ = true;
                    oOOForm.ENQRESULT = false;
                    oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQOKReturn);
                    oOOForm.Handler_DialogButton1Return = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQOKReturn);
                    oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQCancelReturn);
                    oOOForm.doShowModal(SBOForm);
                } else {
                    for (int i = 0; i <= aSelectItems.Count - 1; i++) {
                        ArrayList aEnqRow = new ArrayList();
                        aEnqRow.Add(aSelectItems[i]);
                       // com.isb.forms.Enquiry.WOQuote oOOForm = new com.isb.forms.Enquiry.WOQuote(null, SBOForm.UniqueID, SBOForm);
                        com.isb.forms.Enquiry.WOQuote oOOForm = new com.isb.forms.Enquiry.WOQuote(SBOForm.UniqueID, "-1");
                        oOOForm.EnquiryID = moEnquiry.Code;
                        oOOForm.SelectItems = aEnqRow;
                        oOOForm.bLoadWOQ = true;
                        oOOForm.ENQRESULT = false;
                      //  oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQOKReturn);
                      //  oOOForm.Handler_DialogButton1Return = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQOKReturn);
                      //  oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQCancelReturn);
                        oOOForm.doOpenForm(oOOForm.IDHForm.gsType);
                        //oOOForm.doOpenForm()
                        

                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doCreateWOQ" });
            }
            //setSharedData("IDH_ENQID", moEnquiry.Code);
            //setSharedData("IDH_CPTO", "True");
            //setSharedData("ENQRESULT","False");
            //doOpenModalForm("IDHWOQ");

            setFocus("IDH_ADR1");
        }

        //public bool doHandleFormRightClick(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        //    if (IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("AddAdItm"))
        //        IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("AddAdItm");
        //    return true;
        //}
#endregion

#region CreateBP
        public bool doHandleCreateBPOKReturn(com.idh.forms.oo.Form oDialogForm) {
            com.isb.forms.Enquiry.CreateBP oOOForm = (com.isb.forms.Enquiry.CreateBP)oDialogForm;
            moEnquiry.U_CardCode = oOOForm.CardCode;
            if (moEnquiry.U_CardCode!=string.Empty )
                moEnquiry.U_AddrssLN = "0";
            if (moEnquiry.U_ClgCode != 0) {
                moEnquiry.doCreateActivity(true);
            }
            setFocus("IDH_ADR1");
            //Mode = BoFormMode.fm_UPDATE_MODE;
            EnableItem(false, "IDH_CRETBP");
            EnableItem(false, "IDH_CardNM");

            return true;
        }
        public bool doHandleCreateBPCancelReturn(com.idh.forms.oo.Form oDialogForm) {
            com.isb.forms.Enquiry.CreateBP oOOForm = (com.isb.forms.Enquiry.CreateBP)oDialogForm;
            if (oOOForm.CardCode.Trim() != string.Empty) {
                moEnquiry.U_CardCode = oOOForm.CardCode;
                if (moEnquiry.U_CardCode != string.Empty)
                    moEnquiry.U_AddrssLN = "0";
                if (moEnquiry.U_ClgCode != 0) {
                    moEnquiry.doCreateActivity(true);
                }
                setFocus("IDH_ADR1");
                //Mode = BoFormMode.fm_UPDATE_MODE;             
                EnableItem(false, "IDH_CRETBP");
                EnableItem(false, "IDH_CardNM");
            }
            return true;
        }
#endregion

#region CreateWasteItem
        public bool doHandleCreateWasteItemOKReturn(com.idh.forms.oo.Form oDialogForm) {
            com.isb.forms.Enquiry.admin.WasteItemValidations oOOForm = (com.isb.forms.Enquiry.admin.WasteItemValidations)oDialogForm;
            moGrid.doSetFieldValue(IDH_ENQITEM._ItemCode, oOOForm.iParentRow, oOOForm.ItemCode);
            //if (Mode != BoFormMode.fm_ADD_MODE)
            //    Mode = BoFormMode.fm_UPDATE_MODE;
            return true;
        }
        public bool doHandleCreateWasteItemCancelReturn(com.idh.forms.oo.Form oDialogForm) {
            com.isb.forms.Enquiry.admin.WasteItemValidations oOOForm = (com.isb.forms.Enquiry.admin.WasteItemValidations)oDialogForm;
            //if (oOOForm.ItemCode.Trim() != string.Empty && moGrid.doGetFieldValue(IDH_ENQITEM._ItemCode, oOOForm.iParentRow)==string.Empty) {
            if (oOOForm.bUpdateParent) {
                moGrid.doSetFieldValue(IDH_ENQITEM._ItemCode, oOOForm.iParentRow, oOOForm.ItemCode);
                //  if (Mode != BoFormMode.fm_ADD_MODE)
                //     Mode = BoFormMode.fm_UPDATE_MODE;
            }
            return true;
        }
#endregion

#region Postcode
        public bool doHandlePostCodeOKReturn(com.idh.forms.oo.Form oDialogForm) {
            try {
                com.uBC.forms.fr3.PostCode oOOForm = (com.uBC.forms.fr3.PostCode)oDialogForm;
                string Company = oOOForm.Company;
                string SubBuilding = oOOForm.SubBuilding;
                string BuildingNumber = oOOForm.BuildingNumber;
                string BuildingName = oOOForm.BuildingName;
                string SecondaryStreet = oOOForm.SecondaryStreet;
                string Street = oOOForm.Street;
                string Block = oOOForm.Block;
                ////Neighbourhood
                string District = oOOForm.District;
                string City = oOOForm.City;
                string Line1 = oOOForm.Line1;
                string Line2 = oOOForm.Line2;
                string Line3 = oOOForm.Line3;
                string Line4 = oOOForm.Line4;
                string Line5 = oOOForm.Line5;
                string AdminAreaName = oOOForm.AdminAreaName;
                ////AdminAreaCode
                ////Province
                string ProvinceName = oOOForm.ProvinceName;
                string ProvinceCode = oOOForm.ProvinceCode;
                string PostalCode = oOOForm.PostalCode;
                string CountryName = oOOForm.CountryName;
                string CountryIso2 = oOOForm.CountryIso2;
                ////CountryIso3
                ////CountryIsoNumber
                ////SortingNumber1
                ////SortingNumber2
                ////Barcode
                string POBoxNumber = oOOForm.POBoxNumber;
                string Label = oOOForm.Label;
                string Type = oOOForm.Type;
                moEnquiry.BlockChangeNotif = true;
                Freeze(true);
                moEnquiry.U_Address = (SubBuilding + ' ' + BuildingNumber + ' ' + BuildingName).Replace("  ", " ");
                moEnquiry.U_AddrssLN = "";
                moEnquiry.U_Street = SecondaryStreet + Street;
                moEnquiry.U_Block = Block;
                moEnquiry.U_City = City;
                moEnquiry.U_ZipCode = PostalCode;
                moEnquiry.U_County = AdminAreaName;
                //Later will do with DB Object based event handler 
                bool bLoadStates = false;
                if (moEnquiry.U_Country != CountryIso2)
                    bLoadStates = true;
                moEnquiry.U_Country = CountryIso2;
                if (bLoadStates)
                    FillCombos.FillState(Items.Item("IDH_STATE").Specific, moEnquiry.U_Country, null);
                moEnquiry.U_State = ProvinceCode;
                moEnquiry.BlockChangeNotif = false;
                if (Mode != BoFormMode.fm_ADD_MODE)
                    Mode = BoFormMode.fm_UPDATE_MODE;
                /*com.idh.bridge.DataRecords oRecords = null;
                try {
                    string sQry = "Select Code from OCRY where Code Like '" + CountryIso2 + "' ";
                    if (CountryName != string.Empty) {
                        oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("dogetcountry" + CountryName, sQry);
                        if (oRecords != null && oRecords.RecordCount > 0) {
                            SAPbouiCOM.ComboBox oCOmbo = (SAPbouiCOM.ComboBox)Items.Item("IDH_CUNTRY").Specific;
                            if (oCOmbo.Selected == null || (oCOmbo.Selected != null && oCOmbo.Selected.Value != CountryIso2)) {
                                oCOmbo.SelectExclusive(CountryIso2, BoSearchKey.psk_ByValue);
                                FillCombos.FillState(Items.Item("IDH_STATE").Specific, moEnquiry.U_Country, null);
                            }
                        }
                        if (ProvinceCode != string.Empty) {
                            sQry = "Select Code from OCST where Code Like '" + ProvinceCode + "' And Country='" + CountryIso2 + "' ";
                            oRecords = com.idh.bridge.DataHandler.INSTANCE.doBufferedSelectQuery("dogetstate" + ProvinceCode, sQry);
                            if (oRecords != null && oRecords.RecordCount > 0) {
                                SAPbouiCOM.ComboBox oCOmbo = (SAPbouiCOM.ComboBox)Items.Item("IDH_STATE").Specific;
                                if ((oCOmbo.Selected == null) || (oCOmbo.Selected != null && oCOmbo.Selected.Value != ProvinceCode)) {
                                    oCOmbo.SelectExclusive(ProvinceCode, BoSearchKey.psk_ByValue);
                                }
                            }
                        }
                    }
                } catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
                }*/
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "HandlePostCodeOKReturn" });
            }finally{
                Freeze(false);
            }
            return true;
        }
        public bool doHandlePostCodeCancelReturn(com.idh.forms.oo.Form oDeialogForm) {
            com.uBC.forms.fr3.PostCode oOOForm = (com.uBC.forms.fr3.PostCode)oDeialogForm;

            return true;
        }
#endregion

#region WOQHandler
        public bool doHandleWOQOKReturn(com.idh.forms.oo.Form oDialogForm) {
            com.isb.forms.Enquiry.WOQuote oOOForm = (com.isb.forms.Enquiry.WOQuote)oDialogForm;
            if (oOOForm.ENQRESULT) {
                Freeze(true);
                moEnquiry.doLoadChildren();
                moGrid.doApplyRules();
                doFillGridCombos();
                Freeze(false);
            }
            //moGrid.doSetFieldValue(IDH_ENQITEM._ItemCode, oOOForm.iParentRow, oOOForm.ItemCode);
            //if (Mode != BoFormMode.fm_ADD_MODE)
            //    Mode = BoFormMode.fm_UPDATE_MODE;
            return true;
        }
        public bool doHandleWOQCancelReturn(com.idh.forms.oo.Form oDialogForm) {
            com.isb.forms.Enquiry.WOQuote oOOForm = (com.isb.forms.Enquiry.WOQuote)oDialogForm;
            if (oOOForm.ENQRESULT) {
                Freeze(true);
                //moEnquiry.doLoadChildren();
                //moGrid.doApplyRules();
                doFillGridCombos();
                Freeze(false);
            }
            //if (oOOForm.ItemCode.Trim() != string.Empty && moGrid.doGetFieldValue(IDH_ENQITEM._ItemCode, oOOForm.iParentRow)==string.Empty) {
            //if (oOOForm.bUpdateParent) {
            //  moGrid.doSetFieldValue(IDH_ENQITEM._ItemCode, oOOForm.iParentRow, oOOForm.ItemCode);
            //  if (Mode != BoFormMode.fm_ADD_MODE)
            //     Mode = BoFormMode.fm_UPDATE_MODE;
            //  }
            return true;
        }
#endregion

#region ItemEventHandlers
        /** 
         * Update the Row Header Ref
         */
        private void doUpdateLineHeaderRef() {
            moEnquiry.EnquiryItems.setValueForAllRows(IDH_ENQITEM._EnqId, moEnquiry.Code, true);
        }
        public bool doAddUpdateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                //if (moEnquiry.doValidation() == false) {
                //    com.idh.bridge.DataHandler.INSTANCE.doUserError(moEnquiry.LastValidationError);
                //    BubbleEvent = false;
                //    return true;
                //}

                BoFormMode CurrentMode = Mode;
                Freeze(true);
                try {
                    // DateTime oDate = DateTime.Now;
                    //  moEnquiry.U_Recontact = oDate;
                    //FUTURE USE - Updater if ( moVehicleMaster.doProcessData( )) {
                    DataHandler.INSTANCE.StartTransaction();
                    // mbAddUpdateOk = moEnquiry.doAddUpdateAddress();
                    //if (mbAddUpdateOk)
                    //  mbAddUpdateOk = doAutoSave(IDH_ENQUIRY.AUTONUMPREFIX, moEnquiry.AutoNumKey);
                    //if (mbAddUpdateOk) {
                    //doUpdateLineHeaderRef();//moEnquiry.EnquiryItems.doUpdateAllDataRows("NC");                    
                    if (moEnquiry.doProcessData()) {
                        //if (CurrentMode == BoFormMode.fm_ADD_MODE && mbFromActivity == false) {
                        //    //string msg = com.idh.bridge.resources.Messages.getGMessage("INFDOCNO", new string[] { "Enquiry", moEnquiry.Code });
                        //    //com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(msg, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                        //    //doCreateActivity(false);
                        //    if (DataHandler.INSTANCE.IsInTransaction()) {
                        //        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                        //    }

                        //        Mode = BoFormMode.fm_OK_MODE;
                        //} else {
                        //    if (DataHandler.INSTANCE.IsInTransaction()) {
                        //        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                        //    }

                        //    //doCreateActivity(true);
                        //    Mode = BoFormMode.fm_OK_MODE;
                        //}
                        Mode = BoFormMode.fm_OK_MODE;
                        if (DataHandler.INSTANCE.IsInTransaction()) {
                            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                        }

                        BubbleEvent = false;
                    } else {
                        if (DataHandler.INSTANCE.IsInTransaction()) {
                            DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                        }

                    }
                    //moEnquiry.DoAddressChanged = false;
                    //} else {
                    //    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                     //   BubbleEvent = false;
                     //   return true;
                    //}

                    //BoFormMode CurrentMode = Mode;

                    //Freeze(true);
                    //try {
                    //    // DateTime oDate = DateTime.Now;
                    //    //  moEnquiry.U_Recontact = oDate;
                    //    //FUTURE USE - Updater if ( moVehicleMaster.doProcessData( )) {
                    //    DataHandler.INSTANCE.StartTransaction();
                    //    mbAddUpdateOk = moEnquiry.doAddUpdateAddress();
                    //    if (mbAddUpdateOk)
                    //        mbAddUpdateOk = doAutoSave(IDH_ENQUIRY.AUTONUMPREFIX, moEnquiry.AutoNumKey);
                    //    if (mbAddUpdateOk) {

                    //        doUpdateLineHeaderRef();//moEnquiry.EnquiryItems.doUpdateAllDataRows("NC");                    

                    //        /*
                    //        IDH_ENQITEM objEnqItm=new IDH_ENQITEM();                        
                    //        moEnquiry.EnquiryItems.AddedRows.Clear();
                    //        moEnquiry.EnquiryItems.ChangedRows.Clear();
                    //        for (int i = 0; i <= moEnquiry.EnquiryItems.Count - 1; i++) {

                    //            moEnquiry.EnquiryItems.gotoRow(i);
                    //            objEnqItm.DoNotSetDefault = true;
                    //            if (!objEnqItm.getByKey(moEnquiry.EnquiryItems.Code)) {
                    //                moEnquiry.EnquiryItems.AddedRows.Add(i);
                    //            } else
                    //                moEnquiry.EnquiryItems.ChangedRows.Add(i);
                    //        }
                    //         */
                    //        //IDH_ENQITEM objEnqItm = new IDH_ENQITEM();
                    //        if (moGrid.getDeletedRowList() != null) {
                    //            ArrayList aDeletedRows = moGrid.getDeletedRowList();
                    //            for (int i = 0; i <= aDeletedRows.Count - 1; i++) {
                    //                IDHAddOns.idh.data.AuditObject oAuditObjGrid = new IDHAddOns.idh.data.AuditObject(IDHAddOns.idh.addon.Base.PARENT, IDH_ENQITEM.TableName);
                    //                if (oAuditObjGrid.setKeyPair(aDeletedRows[i].ToString(), IDHAddOns.idh.data.AuditObject.ac_Types.idh_REMOVE) == true) {
                    //                    oAuditObjGrid.Commit();
                    //                }
                    //            }

                    //        }
                    //        int iwResult = 0;
                    //        for (int i = 0; i <= moEnquiry.EnquiryItems.Count - 1; i++) {
                    //            moEnquiry.EnquiryItems.gotoRow(i);
                    //            IDHAddOns.idh.data.AuditObject oAuditObj = new IDHAddOns.idh.data.AuditObject(base.IDHForm.goParent, IDH_ENQITEM.TableName);
                    //            if (oAuditObj.setKeyPair(moEnquiry.EnquiryItems.Code, IDHAddOns.idh.data.AuditObject.ac_Types.idh_UPDATE) == false) {
                    //                oAuditObj.setKeyPair(moEnquiry.EnquiryItems.Code, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD);
                    //            }
                    //            if (moEnquiry.EnquiryItems.U_ItemName.Trim() == string.Empty && moEnquiry.EnquiryItems.U_ItemDesc.Trim() == string.Empty && moEnquiry.EnquiryItems.U_ItemCode.Trim() == string.Empty) {
                    //                continue;
                    //            }
                    //            oAuditObj.setName(moEnquiry.EnquiryItems.Code);
                    //            oAuditObj.setFieldValue(IDH_ENQITEM._AddItm, moEnquiry.EnquiryItems.U_AddItm);
                    //            oAuditObj.setFieldValue(IDH_ENQITEM._EnqId, moEnquiry.EnquiryItems.U_EnqId);
                    //            oAuditObj.setFieldValue(IDH_ENQITEM._EstQty, moEnquiry.EnquiryItems.U_EstQty);
                    //            oAuditObj.setFieldValue(IDH_ENQITEM._ItemCode, moEnquiry.EnquiryItems.U_ItemCode);
                    //            oAuditObj.setFieldValue(IDH_ENQITEM._ItemName, moEnquiry.EnquiryItems.U_ItemName);//FName
                    //            oAuditObj.setFieldValue(IDH_ENQITEM._ItemDesc, moEnquiry.EnquiryItems.U_ItemDesc);//Item Desc(ItemName)
                    //            oAuditObj.setFieldValue(IDH_ENQITEM._Sort, moEnquiry.EnquiryItems.U_Sort);
                    //            oAuditObj.setFieldValue(IDH_ENQITEM._Status, moEnquiry.EnquiryItems.U_Status);
                    //            oAuditObj.setFieldValue(IDH_ENQITEM._UOM, moEnquiry.EnquiryItems.U_UOM);
                    //            oAuditObj.setFieldValue(IDH_ENQITEM._WstGpCd, moEnquiry.EnquiryItems.U_WstGpCd);
                    //            oAuditObj.setFieldValue(IDH_ENQITEM._WstGpNm, moEnquiry.EnquiryItems.U_WstGpNm);
                    //            oAuditObj.setFieldValue(IDH_ENQITEM._PCode, moEnquiry.EnquiryItems.U_PCode);
                    //            iwResult = oAuditObj.Commit();
                    //        }

                    //        moEnquiry.EnquiryItems.AddedRows.Clear();
                    //        moEnquiry.EnquiryItems.ChangedRows.Clear();
                    //        if (moEnquiry.EnquiryItems.doProcessData()) {
                    //            //if (moGrid.doProcessData())
                    //            //{
                    //            if (CurrentMode == BoFormMode.fm_ADD_MODE && mbFromActivity == false) {
                    //                string msg = com.idh.bridge.resources.Messages.getGMessage("INFDOCNO", new string[] { "Enquiry", moEnquiry.Code });
                    //                com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(msg, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    //                doCreateActivity(false);
                    //                DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    //                Mode = BoFormMode.fm_ADD_MODE;
                    //            } else {
                    //                DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    //                doCreateActivity(true);
                    //                Mode = BoFormMode.fm_OK_MODE;
                    //            } BubbleEvent = false;
                    //        }

                    //        moEnquiry.DoAddressChanged = false;
                    //    } else {
                    //        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    //        BubbleEvent = false;
                    //        return true;    
                    //    }

                } catch (Exception ex) {
                    if (DataHandler.INSTANCE.IsInTransaction())
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    throw (ex);
                } finally {
                    Freeze(false);
                }
            }
            return true;
        }
        //private bool doAddUpdateAddress() {
        //    SAPbobsCOM.BusinessPartners oBP=null;
        //    SAPbobsCOM.BPAddresses oAddress=null;
        //    try {
        //        if (moEnquiry.U_CardCode != string.Empty && moEnquiry.U_Address != string.Empty) {
        //            object oAddressLine =null;
        //            oAddressLine = Config.INSTANCE.doLookupTableField("CRD1", "LineNum", "CardCode='" + moEnquiry.U_CardCode.Replace("'", "''") + "' And Address='" + moEnquiry.U_Address.Replace("'", "''") + "' And AdresType='S'", "", true);
        //            if (moEnquiry.DoAddressChanged || oAddressLine == null) {
        //                //Add Address

        //                //If sCardCode.Length > 0 Then
        //                //  Try
        //                oBP = (SAPbobsCOM.BusinessPartners)IDHAddOns.idh.addon.Base.PARENT.goDICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
        //                if (oBP.GetByKey(moEnquiry.U_CardCode)) {
        //                    oAddress = oBP.Addresses;
        //                    if (oAddressLine == null) {
        //                        oAddress.Add();
        //                        oAddress.SetCurrentLine(oAddress.Count - 1);
        //                    } else if (moEnquiry.DoAddressChanged) {
        //                        int i = 0;
        //                        for (i = 0; i < oAddress.Count; i++) {
        //                            oAddress.SetCurrentLine(i);
        //                            if (oAddress.AddressName == moEnquiry.U_Address && oAddress.AddressType == SAPbobsCOM.BoAddressType.bo_ShipTo)
        //                                break;
        //                        }
        //                        if (i == oAddress.Count) {
        //                            oAddress.Add();
        //                            oAddress.SetCurrentLine(oAddress.Count - 1);
        //                        }
        //                    }
                            
        //                    oAddress.AddressName = moEnquiry.U_Address;
        //                    oAddress.Street = moEnquiry.U_Street;
        //                    oAddress.Block = moEnquiry.U_Block;
        //                    oAddress.ZipCode = moEnquiry.U_ZipCode;
        //                    oAddress.City = moEnquiry.U_City;
        //                    oAddress.County = moEnquiry.U_County;
        //                    oAddress.AddressType = SAPbobsCOM.BoAddressType.bo_ShipTo;
        //                    oAddress.State = moEnquiry.U_State;
        //                    oAddress.Country = moEnquiry.U_Country;
        //                    int iwResult;
        //                    string swResult;
        //                    iwResult = oBP.Update();
        //                    oAddressLine = Config.INSTANCE.doLookupTableField("CRD1", "LineNum", "CardCode Like '" + moEnquiry.U_CardCode.Replace("'", "''") + "' And Address Like '" + moEnquiry.U_Address.Replace("'", "''") + "' And AdresType='S'", "LineNum", true);
        //                    if(oAddressLine!=null)
        //                        moEnquiry.U_AddrssLN = Convert.ToString(oAddressLine);
        //                    if (iwResult != 0) {
        //                        IDHAddOns.idh.addon.Base.PARENT.goDICompany.GetLastError(out iwResult, out swResult);
        //                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Adding Address: " + swResult, "ERSYADDA", new string[] { swResult });
        //                        return false;
        //                    }
        //                    return true;
        //                }
                       
        //            }
        //        }
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doAddUpdateAddress:Add/Update BP's Address" });
        //        return false;
        //    } finally {
        //        if (oBP != null) {
        //            object oObj = (object)oBP;
        //            IDHAddOns.idh.data.Base.doReleaseObject(ref oObj);
        //            oObj = (object)oAddress;
        //            IDHAddOns.idh.data.Base.doReleaseObject(ref oObj);
        //        }
        //    }
        //    return true;
        //}

        private bool doHandleGridSort(ref IDHAddOns.idh.events.Base pVal) {
            return false;
        }
        private bool doHandleGridMenuEvent(ref IDHAddOns.idh.events.Base pVal) {
			//WORK ON THE DB OBJECT OF THE GRID

            SAPbouiCOM.MenuEvent opVal = (SAPbouiCOM.MenuEvent)pVal.oData;
            if (Mode == BoFormMode.fm_VIEW_MODE && (opVal.MenuUID.Equals("IDHGMADD") || opVal.MenuUID.Equals("IDHGMREML") || opVal.MenuUID.Equals("AddAdItm"))) {
                setSharedData("ROWDELTED", "True");
                return false;
            }

            if (!pVal.BeforeAction && opVal.MenuUID.Equals("AddAdItm")) {
                SAPbouiCOM.SelectedRows oSelected = moGrid.getGrid().Rows.SelectedRows;
                int iSelectionCount = oSelected.Count;
                if (iSelectionCount > 0) {
					//will copy all to BO at later stage
                    Freeze(true);
                    moEnquiry.EnquiryItems.dosetCodeValue();
                    string sPCode = (string)moGrid.doGetFieldValue("Code", pVal.Row);
                    int iSort = (int)moGrid.doGetFieldValue(IDH_ENQITEM._Sort, pVal.Row);
					
                    moEnquiry.EnquiryItems.doAddEmptyRow(false, true);
                    string iCode = (string)moGrid.doGetFieldValue("Code", moGrid.getRowCount() - 1);
                    string sName = (string)moGrid.doGetFieldValue("Name", moGrid.getRowCount() - 1);
                    for (int i = moGrid.getRowCount() - 1; i >= pVal.Row + 2; i--) {
                        for (int icol = 0; icol <= moGrid.Columns.Count - 1; icol++) {
                            //      if (!(moGrid.Columns.Item(icol).UniqueID=="Code" || moGrid.Columns.Item(icol).UniqueID=="Name"))
                            moGrid.doSetFieldValue(icol, i, moGrid.doGetFieldValue(icol, i - 1));
                        }
                    }
                    //return true;
                    for (int icol = 0; icol <= moGrid.Columns.Count - 1; icol++) {
                        if ((moGrid.Columns.Item(icol).UniqueID == "Code"))
                            moGrid.doSetFieldValue(icol, pVal.Row + 1, iCode);
                        else if ((moGrid.Columns.Item(icol).UniqueID == "Name"))
                            moGrid.doSetFieldValue(icol, pVal.Row + 1, sName);
                        else //if (!(moGrid.Columns.Item(icol).UniqueID == "Code" || moGrid.Columns.Item(icol).UniqueID == "Name"))
                            moGrid.doSetFieldValue(icol, pVal.Row + 1, moGrid.doGetFieldValue(icol, moGrid.getRowCount() - 1));
                    }
                    //return true;
                    for (int i = pVal.Row + 1; i <= moGrid.getRowCount() - 2; i++) {
                        moGrid.doSetFieldValue(IDH_ENQITEM._Sort, i, i + 1);
                    }
                    moGrid.doSetFieldValue(IDH_ENQITEM._PCode, pVal.Row + 1, sPCode);
                    moGrid.doSetFieldValue(IDH_ENQITEM._AddItm, pVal.Row + 1, "A");
                    if (IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("AddAdItm"))
                        IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("AddAdItm");
                    for (int i = 0; i <= moGrid.getRowCount() - 1; i++) {
                        if (moGrid.doGetFieldValue(IDH_ENQITEM._Status, i).ToString() == "1")
                            moGrid.Settings.SetRowEditable(i + 1, true);
                        else
                            moGrid.Settings.SetRowEditable(i + 1, false);
                    }
                    moGrid.doApplyRules();
					doUpdateGridCells(true);
                    Freeze(false);

                    // moEnquiry.EnquiryItems.doReload();
                    //   moEnquiry.EnquiryItems.BufferFile
                    //  moEnquiry.EnquiryItems.postDataLoad();// ("LINESGRID");
                    // moGrid.doRecount();
                }
            } else if (pVal.BeforeAction && opVal.MenuUID.Equals("IDHGMADD") || opVal.MenuUID.Equals("IDHGMREML")) {
                setSharedData("ROWDELTED", "False");
                SAPbouiCOM.SelectedRows oSelected = moGrid.getGrid().Rows.SelectedRows;
                int iSelectionCount = oSelected.Count;
                if (iSelectionCount == 0)
                    return true;
                if (moGrid.doGetFieldValue(IDH_ENQITEM._ItemName, pVal.Row).ToString().Trim() != string.Empty && (moGrid.doGetFieldValue(IDH_ENQITEM._Status, pVal.Row).ToString() != "1")) {
                    return false;
                } else if (opVal.MenuUID.Equals("IDHGMREML") &&
                  moGrid.doGetFieldValue(IDH_ENQITEM._ItemName, pVal.Row).ToString().Trim() != string.Empty
                  && (moGrid.doGetFieldValue(IDH_ENQITEM._Status, pVal.Row).ToString() == "1")
                  ) {
                    bool bHasLinkedItems = false;
                    string sLineCode = moGrid.doGetFieldValue(IDH_ENQITEM._Code, pVal.Row).ToString();
                    for (int i = pVal.Row; i <= moGrid.getRowCount() - 1; i++) {
                        if (moGrid.doGetFieldValue(IDH_ENQITEM._PCode, i).ToString() == sLineCode) {
                            bHasLinkedItems = true;
                            break;
                        }
                    }
                    if (!bHasLinkedItems)
                        return true;
                    string sResourceMessage = com.idh.bridge.resources.Messages.INSTANCE.getMessage("ERVEQ003", null);
                    int iRow = pVal.Row;
                    if (IDHAddOns.idh.addon.Base.PARENT.doMessage(sResourceMessage, 2, "Yes", "No", null, true) == 1) {
                        Freeze(true);
                        moGrid.doRemoveRow(iRow, false, true);
                        for (int i = moGrid.getRowCount() - 1; i >= iRow; i--) {
                            if (moGrid.doGetFieldValue(IDH_ENQITEM._PCode, i).ToString() == sLineCode) {
                                moGrid.doRemoveRow(i, false, true);
                                setSharedData("ROWDELTED", "True");
                            }
                        }
                        for (int i = 0; i <= moGrid.getRowCount() - 1; i++) {
                            moGrid.doSetFieldValue(IDH_ENQITEM._Sort, i, i + 1);
                        }
                        if (Mode == BoFormMode.fm_OK_MODE)
                            Mode = BoFormMode.fm_UPDATE_MODE;
                        moGrid.doApplyRules();
                        Freeze(false);
                        return false;
                        //else 
                        //            return false;
                    } else {
                        setSharedData("ROWDELTED", "True");
                        return false;
                    }
                }
            }
            return true;
        }
        private bool doHandleGridRightClick(ref IDHAddOns.idh.events.Base pVal) {
            if (Mode != BoFormMode.fm_VIEW_MODE && pVal.BeforeAction) {
                if (moGrid.getSBOGrid().Rows.IsLeaf(pVal.Row)) {
                    SAPbouiCOM.SelectedRows oSelected = moGrid.getGrid().Rows.SelectedRows;
                    int iSelectionCount = oSelected.Count;
                    if (iSelectionCount > 0 && (moGrid.doGetFieldValue(IDH_ENQITEM._ItemName, pVal.Row).ToString() != string.Empty || moGrid.doGetFieldValue(IDH_ENQITEM._ItemCode, pVal.Row).ToString() != string.Empty || moGrid.doGetFieldValue(IDH_ENQITEM._ItemDesc, pVal.Row).ToString() != string.Empty)
                        && moGrid.doGetFieldValue(IDH_ENQITEM._AddItm, pVal.Row).ToString() == string.Empty
                        && moGrid.doGetFieldValue(IDH_ENQITEM._Status, pVal.Row).ToString() == "1") {
                        SAPbouiCOM.MenuItem oMenuItem;
                        string sRMenuId = IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU;
                        oMenuItem = IDHAddOns.idh.addon.Base.APPLICATION.Menus.Item(sRMenuId);
                        if (IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("AddAdItm"))
                            IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("AddAdItm");

                        SAPbouiCOM.Menus oMenus;
                        int iMenuPos = 1;
                        SAPbouiCOM.MenuCreationParams oCreationPackage;
                        oCreationPackage = (SAPbouiCOM.MenuCreationParams)IDHAddOns.idh.addon.Base.APPLICATION.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);
                        oCreationPackage.Enabled = true;
                        oMenus = oMenuItem.SubMenus;
                        oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;

                        oCreationPackage.UniqueID = "AddAdItm";
                        oCreationPackage.String = Translation.getTranslatedWord("Add Additional Item");
                        oCreationPackage.Position = iMenuPos;
                        iMenuPos += 1;
                        oMenus.AddEx(oCreationPackage);
                    } else {
                        if (IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("AddAdItm"))
                            IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("AddAdItm");
                        setWFValue("LASTJOBMENU", null);
                    }
                    //GMADD
                    //GMREML
                } else {
                    if (IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("AddAdItm"))
                        IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("AddAdItm");
                    setWFValue("LASTJOBMENU", null);
                }
            }
            return true;
        }
        private bool doHandleGridSearch(ref IDHAddOns.idh.events.Base pVal) {
            if (!pVal.BeforeAction) {
                if (pVal.ColUID == IDH_ENQITEM._ItemName && moEnquiry.EnquiryItems.getValueAsString(pVal.Row, IDH_ENQITEM._ItemCode) != string.Empty) {
                    moEnquiry.EnquiryItems.setValue(IDH_ENQITEM._ItemCode, "", false);
                } else if (pVal.ColUID == IDH_ENQITEM._WstGpNm && moEnquiry.EnquiryItems.getValueAsString(pVal.Row, IDH_ENQITEM._WstGpCd) != string.Empty) {
                    moEnquiry.EnquiryItems.setValue(IDH_ENQITEM._WstGpCd, "", false);
                }
            }
            return true;
        }

        private bool doHandleGridSearchValueSet(ref IDHAddOns.idh.events.Base pVal) {
            if (!pVal.BeforeAction) {
                if (pVal.ColUID == IDH_ENQITEM._ItemName && moEnquiry.EnquiryItems.getValueAsString(pVal.Row, IDH_ENQITEM._ItemCode) != string.Empty) {
                    IDH_ADITVLD obj = new IDH_ADITVLD();
                    string sWG = "";
                    if (moEnquiry.EnquiryItems.getValueAsString(pVal.Row, IDH_ENQITEM._WstGpCd) != string.Empty) {
                        sWG = " And U_WstGpCd='" + moEnquiry.EnquiryItems.getValueAsString(pVal.Row, IDH_ENQITEM._WstGpCd) + "'";
                    }
                    int ret = obj.getData("U_ItemCd='" + moEnquiry.EnquiryItems.getValueAsString(pVal.Row, IDH_ENQITEM._ItemCode) + "' " + sWG, "");
                    if (ret != 0) {
                        moEnquiry.EnquiryItems.setValue(IDH_ENQITEM._AddItm, "A");
                        moEnquiry.EnquiryItems.setValue(IDH_ENQITEM._WstGpCd, obj.U_WstGpCd);
                        if (obj.U_WstGpCd != string.Empty) {
                            object oVal = com.idh.bridge.lookups.Config.INSTANCE.doLookupTableField("@ISB_WASTEGRP", "U_WstGpNm", "U_WstGpCd = '" + obj.U_WstGpCd + '\'');
                            moEnquiry.EnquiryItems.setValue(IDH_ENQITEM._WstGpNm, com.idh.utils.Conversions.ToString(oVal));
                        }
                    } else {
                        string sAddGrp = com.idh.bridge.lookups.Config.INSTANCE.doGetAdditionalExpGroup();
                        if (getSharedData("ITMSGRPCOD") != null && getSharedData("ITMSGRPCOD").ToString() == sAddGrp) {
                            moEnquiry.EnquiryItems.setValue(IDH_ENQITEM._AddItm, "A");
                        }
                    }
                }
            }
            return true;
        }
        private bool doHandleGridDoubleClick(ref IDHAddOns.idh.events.Base pVal) {
            if (Mode != BoFormMode.fm_VIEW_MODE && Mode != BoFormMode.fm_ADD_MODE && pVal.BeforeAction) {
                if (pVal.ColUID == IDH_ENQITEM._ItemCode && (moGrid.doGetFieldValue(IDH_ENQITEM._ItemName, pVal.Row).ToString().Trim() != string.Empty || moGrid.doGetFieldValue(IDH_ENQITEM._ItemDesc, pVal.Row).ToString().Trim() != string.Empty)) {//&& moEnquiry.EnquiryItems.getValueAsString(pVal.Row, IDH_ENQITEM._ItemCode) == string.Empty) {
                    com.isb.forms.Enquiry.admin.WasteItemValidations oOOForm = new com.isb.forms.Enquiry.admin.WasteItemValidations(null, SBOForm.UniqueID, null);
                    oOOForm.iParentRow = pVal.Row;
                    oOOForm.sEnquiryLineID = moGrid.doGetFieldValue(IDH_ENQITEM._Code, pVal.Row).ToString();
                    oOOForm.sWOQLineID = moGrid.doGetFieldValue(IDH_ENQITEM._WOQLineID, pVal.Row).ToString();
                    //oOOForm.setUFValue("IDH_ITEMCD", moGrid.doGetFieldValue(IDH_ENQITEM._ItemCode, pVal.Row));
                    //oOOForm.setUFValue("IDH_ITEMNM", "");
                    //oOOForm.setUFValue("IDH_FRNNAM", moGrid.doGetFieldValue(IDH_ENQITEM._ItemName, pVal.Row));
                    //oOOForm.setUFValue("IDH_WSTGRP", moGrid.doGetFieldValue(IDH_ENQITEM._WstGpCd, pVal.Row));//Address ID 50
                    
                    oOOForm.WasteGroupCode = (string)moGrid.doGetFieldValue(IDH_ENQITEM._WstGpCd, pVal.Row);
                    oOOForm.EnquiryID = (string)moGrid.doGetFieldValue(IDH_ENQITEM._EnqId, pVal.Row);
                    oOOForm.WOQID = (string)moGrid.doGetFieldValue(IDH_ENQITEM._WOQID, pVal.Row);
                    oOOForm.AddItm = (string)moGrid.doGetFieldValue(IDH_ENQITEM._AddItm, pVal.Row);
                    oOOForm.ItemCode = (string)moGrid.doGetFieldValue(IDH_ENQITEM._ItemCode, pVal.Row);
                    oOOForm.ItemFName = (string)moGrid.doGetFieldValue(IDH_ENQITEM._ItemName, pVal.Row);
                    oOOForm.ItemName = (string)moGrid.doGetFieldValue(IDH_ENQITEM._ItemDesc, pVal.Row);
                    oOOForm.ParentFormMode = Mode;
                    oOOForm.CardCode = moEnquiry.U_CardCode;
                    oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleCreateWasteItemOKReturn);
                    oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleCreateWasteItemCancelReturn);
                    oOOForm.doShowModal(SBOForm);

                }
            }
            if (Mode != BoFormMode.fm_VIEW_MODE && !pVal.BeforeAction && pVal.ColUID == IDH_ENQITEM._WOQID && moGrid.doGetFieldValue(IDH_ENQITEM._WOQID, pVal.Row).ToString().Trim() != string.Empty) {
                //com.isb.forms.Enquiry.WOQuote oOOForm = new com.isb.forms.Enquiry.WOQuote(null, SBOForm.UniqueID, null);
                com.isb.forms.Enquiry.WOQuote oOOForm = new com.isb.forms.Enquiry.WOQuote(SBOForm.UniqueID, moGrid.doGetFieldValue(IDH_ENQITEM._WOQID, pVal.Row).ToString().Trim());
                oOOForm.WOQID = moGrid.doGetFieldValue(IDH_ENQITEM._WOQID, pVal.Row).ToString().Trim();
                oOOForm.bLoadWOQ = true;
                oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQOKReturn);
                //oOOForm.Handler_DialogButton1Return = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQOKReturn);
                oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQCancelReturn);
                oOOForm.doShowModal(SBOForm);
                setFocus("IDH_ADR1");
                //setshareddata("idh_enqid", "");
                //setshareddata("idh_cpto", "false");
                //setshareddata("idh_opnwoq", mogrid.dogetfieldvalue(idh_enqitem._woqid, pval.row).tostring().trim());
                //doopenmodalform("idhwoq");
            }
            return true;
        }

        #region FolderEvents

        #endregion

#endregion

#region MenuEventHandlers
        public bool doHandleModeChange(SAPbouiCOM.BoFormMode oSBOMode) {
            Freeze(true);
            if (oSBOMode == BoFormMode.fm_ADD_MODE) {
                doSwitchToEdit();
                EnableItem(false, "IDH_CRTWOQ");
                doClearFormUFValues();
                moEnquiry.doClearBuffers();
                //moEnquiry.doAddEmptyRow(true, true);
                //DataHandler.NextNumbers oNextNum = moEnquiry.getNewKey();
                //moEnquiry.Code = oNextNum.CodeCode;
                //moEnquiry.Name = oNextNum.NameCode;

                moGrid.doApplyRules();
                doFillGridCombos();
                FillCombos.FillState(Items.Item("IDH_STATE").Specific, moEnquiry.U_Country, null);
                if (moEnquiry.bFromActivity && mbActivityID != 0)
                    moEnquiry.U_ClgCode = mbActivityID;
                //   DateTime oDate = DateTime.Now;
                //moEnquiry.U_Recontact = oDate;
                doUpdateGridCells(true);
                setFocus("IDH_CardNM");
              //  doSetWidths();
            } else if (oSBOMode == BoFormMode.fm_FIND_MODE) {
                doSwitchToFind();

                doClearFormDFValues();
                doClearFormUFValues();
             //   doSetWidths();
                ////moGrid.doApplyRules();
            } else if (oSBOMode == BoFormMode.fm_OK_MODE || oSBOMode == BoFormMode.fm_UPDATE_MODE) {

                if (moEnquiry.U_CardCode != string.Empty) EnableItem(false, "IDH_CardNM");
                //setFocus("IDH_CardNM");
                //EnableItem(false, "IDH_DOCNUM");
                if (oSBOMode == BoFormMode.fm_OK_MODE)
                    EnableItem(true, "IDH_CRTWOQ");
                else
                    EnableItem(false, "IDH_CRTWOQ");
            }

            Freeze(false);
            return true;
        }
        public bool doMenuAddEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction && (Mode == BoFormMode.fm_UPDATE_MODE || (Mode == BoFormMode.fm_ADD_MODE && moEnquiry.doFormHaveSomeData())) && com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYN("WRNUSAVE", null) == 2) {
                BubbleEvent = false;
                return false;
            } else
                if (pVal.BeforeAction) {
                    //moEnquiry.doClearBuffers();
                    Mode = BoFormMode.fm_ADD_MODE;
                }
            return true;
        }

        public bool doMenuFindBUttonEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction && (Mode == BoFormMode.fm_UPDATE_MODE || (Mode == BoFormMode.fm_ADD_MODE && moEnquiry.doFormHaveSomeData())) && com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYN("WRNUSAVE", null) == 2) {
                BubbleEvent = false;
                return false;
            } else
                if (pVal.BeforeAction) {
                    Mode = BoFormMode.fm_FIND_MODE;
                }
            return true;
        }
        public bool doMenuBrowseEvents(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                if ((Mode == BoFormMode.fm_UPDATE_MODE || (Mode == BoFormMode.fm_ADD_MODE && moEnquiry.doFormHaveSomeData())) && com.idh.bridge.resources.Messages.INSTANCE.doResourceMessageYN("WRNUSAVE", null) == 2) {
                    BubbleEvent = false;
                    return false;
                }
                Freeze(true);
                try {
                    doClearFormUFValues();
                } catch (Exception ex) {
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHANB", null);
                } finally {
                    Freeze(false);
                }
            } else {
                  FillCombos.FillState(Items.Item("IDH_STATE").Specific, moEnquiry.U_Country, null);
               // moEnquiry.doLoadChildren();
                if (moEnquiry.EnquiryItems.Count == 0) {
                    moGrid.doAddEditLine(true);
                }
                moGrid.doApplyRules();
                doFillGridCombos();
                EnableItem(false, "IDH_DOCNUM");
                string sCardName = "";
                if (doValidateBPbyCode(ref sCardName) == true) {
                    EnableItem(false, "IDH_CRETBP");
                } else
                    EnableItem(true, "IDH_CRETBP");
                moEnquiry.DoAddressChanged = false;
                //  doSetWidths();
            }
            return true;
        }
#endregion
#endregion

#region OldEventHandlers
        /**
         * Most of these are replaced with Handlers as above... only use these if there is really no Event or in special events
         */

        /*
         * Handle all the Menu Events.
         * Return True if the Event must be handled by the other Objects
         */
        public override bool doMenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (getSharedData("ROWDELTED") != null && getSharedData("ROWDELTED").ToString() == "True") {
                BubbleEvent = false;
                setSharedData("ROWDELTED", "False");
                return false;
            }
            return true;
        }

        /*
         * The Event Handler that will receive all Activated Events for this Form
         * Return True if the Event must be handled by the other Objects
         */
        public override bool doItemEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (!pVal.BeforeAction && ((int)pVal.FormMode == (int)BoFormMode.fm_UPDATE_MODE) && (pVal.EventType == BoEventTypes.et_KEY_DOWN || pVal.EventType == BoEventTypes.et_COMBO_SELECT)) {
                //  com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText(pVal.EventType.ToString(), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                //EnableItem(false, "IDH_DOCNUM");
                if (moEnquiry.U_CardCode != string.Empty) {
                    EnableItem(false, "IDH_CRETBP");
                    EnableItem(false, "IDH_CardNM");
                }
            }
            return true;
        }

        /*
         * Send By Custom Controls
         */
        public override bool doCustomItemEvent(ref IDHAddOns.idh.events.Base pVal) {
            return true;
        }

        /*
         * Handle the Request for a Help File
         */
        public override bool doHelpFile(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            return true;
        }

        /*
         * Handles the right click event
         */
        public override bool doRightClickEvent(ref SAPbouiCOM.ContextMenuInfo pVal, ref bool BubbleEvent) {
            if (pVal.ItemUID != "LINESGRID" && pVal.BeforeAction && IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("AddAdItm")) {
                IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("AddAdItm");
            }
            return true;
        }

        /*
         * Handle the Button One Pressed Event
         */
        public override void doButtonID1(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        }
        /* 
         * Handle Button Two Presssed Event
         */
        public override void doButtonID2(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
        }

        /* 
         * Return As Canceled If this was Opened as a Dialog from another form
         */
        public override void doReturnCanceled(string sModalFormType) {
        }

        ///* 
        // * Return as Ok If this was Opened as a Dialog from another form setting the Data in the Global Shared Buffer
        // * This will be called from the Conroller after all the other Button 1 Handlers and the BubbleEvent is still True.
        // */
        //public override void doReturnFromModalShared(bool bState) {
        //}

        /* 
         * Handle the Cancel Event when a Dialog returns to this form.
         */
        public override void doHandleModalCanceled(string sModalFormType) {
            if (sModalFormType == "IDHCSRCH")//BP Search
            {
                // setFocus("IDH_CardNM");
                if (moEnquiry.U_CardCode != string.Empty) {
                    string sCardName = Config.INSTANCE.doGetBPName(moEnquiry.U_CardCode,false);
                    if (!moEnquiry.U_CardName.Equals(sCardName))
                        moEnquiry.U_CardCode = "";
                }

            } else if (sModalFormType == "IDHASRCH" || sModalFormType == "IDHASRCH3") {
                setFocus("IDH_ADR1");

            }
        }

        /* 
         * Handle the Return from a Dialog called from this Form with the data in oData
         */
        public override void doHandleModalBufferedResult(ref object oData, string sModalFormType, string sLastButton) {
        }

        /* 
         * Handle the Return from a Dialog called from this Form with the data in the Global Shared Buffer
         */
        public override void doHandleModalResultShared(string sModalFormType, string sLastButton) {
            try {
            if (sModalFormType == "IDHCSRCH")//BP
            {
                moEnquiry.U_CardCode = (string)getSharedData("CARDCODE");
                moEnquiry.U_CardName = (string)getSharedData("CARDNAME");
                if (Mode == BoFormMode.fm_OK_MODE)
                    Mode = BoFormMode.fm_UPDATE_MODE;
                //ArrayList oData = Config.INSTANCE.doGetBPContactEmployee(moEnquiry.U_CardCode, "DEFAULTCONTACT");
                //if (oData != null) {
                //    moEnquiry.U_CName = oData[0].ToString();
                //    moEnquiry.U_Phone1 = oData[4].ToString();
                //    moEnquiry.U_E_Mail = oData[7].ToString();
                //}
            } else if (sModalFormType == "IDHASRCH" || sModalFormType == "IDHASRCH3")//BP Address
            {
                moEnquiry.BlockChangeNotif = true;
                moEnquiry.DoAddressChanged = false;
                moEnquiry.U_Address = (string)getSharedData("ADDRESS");
                moEnquiry.U_AddrssLN = getSharedData("ADDRSCD").ToString();
                moEnquiry.U_Block = (string)getSharedData("BLOCK");
                moEnquiry.U_Street = (string)getSharedData("STREET");
                moEnquiry.U_City = (string)getSharedData("CITY");
                moEnquiry.U_State = (string)getSharedData("STATE");
                moEnquiry.U_ZipCode = (string)getSharedData("ZIPCODE");
                moEnquiry.U_Country = (string)getSharedData("COUNTRY");
                moEnquiry.U_County = (string)getSharedData("COUNTY");
                moEnquiry.BlockChangeNotif = false;
                if (Mode == BoFormMode.fm_OK_MODE)
                    Mode = BoFormMode.fm_UPDATE_MODE;

            } else if (sModalFormType == "IDHWOQ") {
                if (getSharedData("ENQRESULT") != null) {
                    string sResult = getSharedData("ENQRESULT").ToString();
                    if (sResult == "True") {
                        moEnquiry.doLoadChildren();
                        moGrid.doApplyRules();
                        doFillGridCombos();
                    }
                }
            } else if (sModalFormType == "IDHSRENQ") {//Enquiry Search
                System.Collections.ArrayList aSelectItems = (ArrayList)getSharedData("IDH_SELENQIDS");
                if (aSelectItems == null || aSelectItems.Count == 0)
                    return;
                int iOption = 1;
                if (aSelectItems.Count > 1) {
                    string sMsg = com.idh.bridge.resources.Messages.INSTANCE.getMessage("ENQMG002", null);
                    iOption = com.idh.bridge.resources.Messages.INSTANCE.doMessage(sMsg, 1, Translation.getTranslatedWord("One"), Translation.getTranslatedWord("Multiple"));
                }
                doCreateWOQ(iOption == 1 ? "One" : "Multiple", aSelectItems);
            
            }
            } catch(Exception ex){
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", new string[] { "doHandleModalResultShared" });
            }
        }

        //public void HandleFormDataEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.BusinessObjectInfo BusinessObjectInfo, ref bool BubbleEvent) {
        //    try {
        //        if (BusinessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_LOAD && BusinessObjectInfo.ActionSuccess && BusinessObjectInfo.BeforeAction == false) {
        //            Mode = BoFormMode.fm_OK_MODE;
        //            setFocus("IDH_ADR1");
        //            EnableItem(false, "IDH_DOCNUM");
        //            if (moEnquiry.U_CardCode != string.Empty) EnableItem(false, "IDH_CardNM");
        //        }
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHFE", null);
        //    }
        //    //'Try
        //    //'Catch ex As Exception
        //    //'    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXHFE", {Nothing})
        //    //'End Try
        //}
#endregion

#region RestOfLogic
        private void doFillGridCombos() {
            SAPbouiCOM.ComboBoxColumn oCombo;

            if (moGrid.getRowCount() > 0) {
                int iIndex = moGrid.doIndexFieldWC(IDH_ENQITEM._UOM);
                oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                FillCombos.UOMCombo(oCombo);

                iIndex = moGrid.doIndexFieldWC(IDH_ENQITEM._Status);
                oCombo = (SAPbouiCOM.ComboBoxColumn)moGrid.getSBOGrid().Columns.Item(iIndex);
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                FillCombos.FillWR1StatusNew(oCombo, 102);
                doUpdateGridCells(false);
            }
        }

        protected void doTheGridLayout() {
            if (Config.INSTANCE.getParameterAsBool("FORMSET", false)) {
                if (moFormSettings == null) {
                    string sFormTypeId = SBOForm.TypeEx;
                    //Dim sGridID As String = sFormTypeId & "." & moDispRoutGrid.GridId
                    moFormSettings = new idh.dbObjects.User.IDH_FORMSET();
                    moFormSettings.getFormGridFormSettings(sFormTypeId, moGrid.GridId, "");
                    moFormSettings.doAddFieldToGrid(moGrid);

                    doSetListFields();
                    moFormSettings.doSyncDB(moGrid);
                } else {
                    if (moFormSettings.SkipFormSettings) {
                        doSetListFields();
                    } else {
                        moFormSettings.doAddFieldToGrid(moGrid);
                    }
                }
            } else {
                doSetListFields();
            }
            moGrid.getSBOGrid().SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto;
        }

        protected virtual void doSetListFields() {
            string sWastGrp = com.idh.bridge.lookups.Config.INSTANCE.doWasteMaterialGroup();
            string sType = "";
            moGrid.doAddListField(IDH_ENQITEM._Code, "ID", false, 20, null, null);
            moGrid.doAddListField(IDH_ENQITEM._Name, "Name", false, 0, null, null);

            moGrid.doAddListField(IDH_ENQITEM._EnqId, "Enquiry ID", false, -1, null, null);
            moGrid.doAddListField(IDH_ENQITEM._AddItm, "A", false, 30, null, null);
            moGrid.doAddListField(IDH_ENQITEM._PCode, "Linked ID", false, 30, null, null);

            moGrid.doAddListField(IDH_ENQITEM._ItemCode, "Item Code", false, 120, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);

            if (Config.INSTANCE.getParameterAsBool("USAREL", false) || Config.INSTANCE.getParameterAsBool("ENBWPITM", false)) {

                sType = "SRC*IDHWPSRCENQ(FROMENQUIRY)[IDH_ITMNAM;IDH_GRPCOD=" + sWastGrp + ";IDH_ADDITMFLG=#" + IDH_ENQITEM._PCode + ";IDH_ADDITMGRP=#" + IDH_ENQITEM._WstGpCd + "][ITEMNAME;" + IDH_ENQITEM._ItemCode + "=ITEMCODE;" + IDH_ENQITEM._ItemName + "=FNAME]";
            } else {
                sType = "SRC*IDHWISRCENQ(FROMENQUIRY)[IDH_ITMNAM;IDH_GRPCOD=" + sWastGrp + ";IDH_ADDITMFLG=#" + IDH_ENQITEM._PCode + ";IDH_ADDITMGRP=#" + IDH_ENQITEM._WstGpCd + "][ITEMNAME;" + IDH_ENQITEM._ItemCode + "=ITEMCODE;" + IDH_ENQITEM._ItemName + "=FNAME]";
            }
            moGrid.doAddListField(IDH_ENQITEM._ItemDesc, "Item Name", true, 200, sType, null);
            if (moGrid.getGridControl().getListFields().Count > 0 && moGrid.doIndexFieldWC(IDH_ENQITEM._ItemDesc)>0) {
                int index = moGrid.doIndexFieldWC(IDH_ENQITEM._ItemDesc);
                if (index > 0) {
                    moGrid.getGridControl().getListField(index).msFieldType = sType;
                }
            }

            sType = "SRC*IDHWISRCENQ(FROMENQUIRY)[IDH_ITMFNM;IDH_GRPCOD=" + sWastGrp + ";IDH_ADDITMFLG=#" + IDH_ENQITEM._PCode + ";IDH_ADDITMGRP=#" + IDH_ENQITEM._WstGpCd + "][FNAME;" + IDH_ENQITEM._ItemCode + "=ITEMCODE;" + IDH_ENQITEM._ItemDesc + "=ITEMNAME]";
            moGrid.doAddListField(IDH_ENQITEM._ItemName, "Description(F)", true, 200, sType, null);
            if (moGrid.getGridControl().getListFields().Count > 0 && moGrid.doIndexFieldWC(IDH_ENQITEM._ItemName) > 0) {
                int index = moGrid.doIndexFieldWC(IDH_ENQITEM._ItemName);
                if (index > 0) {
                    moGrid.getGridControl().getListField(index).msFieldType = sType;
                }
            }

            sType = "SRC*IDH_WGSRCH(WG01)[IDH_WGCOD][WGCODE;" + IDH_ENQITEM._WstGpNm + "=WGNAME]";
            moGrid.doAddListField(IDH_ENQITEM._WstGpCd, "Waste Group", true, 120, sType, null);
            if (moGrid.getGridControl().getListFields().Count > 0 && moGrid.doIndexFieldWC(IDH_ENQITEM._WstGpCd) > 0) {
                int index = moGrid.doIndexFieldWC(IDH_ENQITEM._WstGpCd);
                if (index > 0) {
                    moGrid.getGridControl().getListField(index).msFieldType = sType;
                }
            }


            //moGrid.doAddListField(IDH_ENQITEM._WstGpCd, "Waste Group", true, -1, "SRC*IDHWISRCENQ(WST01)[IDH_ITMCOD;IDH_INVENT=;IDH_GRPCOD=115][ITEMCODE;U_WasteDs=ITEMNAME]", null, -1, SAPbouiCOM.BoLinkedObject.lf_None);
            sType = "SRC*IDH_WGSRCH(WG02)[IDH_NAME][WGNAME;" + IDH_ENQITEM._WstGpCd + "=WGCODE]";
            moGrid.doAddListField(IDH_ENQITEM._WstGpNm, "Waste Group Name", true, 0, sType, null);
            if (moGrid.getGridControl().getListFields().Count > 0 && moGrid.doIndexFieldWC(IDH_ENQITEM._WstGpNm) > 0) {
                int index = moGrid.doIndexFieldWC(IDH_ENQITEM._WstGpNm);
                if (index > 0) {
                    moGrid.getGridControl().getListField(index).msFieldType = sType;
                }
            }

            moGrid.doAddListField(IDH_ENQITEM._EstQty, "Estimated Qty", true, 100, null, null);
            moGrid.doAddListField(IDH_ENQITEM._UOM, "UOM", true, 50, ListFields.LISTTYPE_COMBOBOX, null);
            moGrid.doAddListField(IDH_ENQITEM._Status, "Status", true, 70, ListFields.LISTTYPE_COMBOBOX, null);
            moGrid.doAddListField(IDH_ENQITEM._Sort, "Sort", false, -1, null, null);

            moGrid.doAddListField(IDH_ENQITEM._WOQID, "WOQ", false, 30, null, null);
            moGrid.doAddListField(IDH_ENQITEM._WOQLineID, "WOQ Line Id", false, 0, null, null);


            //moGrid.doAddListField(IDH_ENQITEM._Status, "Status", true, -1, ListFields.LISTTYPE_SEARCHALWAYS, null);
            // moGrid.doAddListField(IDH_ENQITEM._Created, "Created", false, 0, null, null);
            moGrid.doSynchDBandGridFieldPos();
            //    bool b=false;

            // moGrid.doAutoExpand(ref b);

            //moGrid.doSetSBOAutoReSize(false);

        }

        /*
         * Set the Filter Fields
         */
        protected void doSetFilterFields() {
            moGrid.doAddFilterField("IDH_DOCNUM", IDH_ENQITEM._EnqId, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30);
        }
        
         
        private void doChooseBPCode() {
            //string sCardCode = (string)getUFValue("uBC_VEHBTO");
            //  moEnquiry.U_CardCode = "";
            setSharedData("IDH_BPCOD", "");
            setSharedData("IDH_NAME", moEnquiry.U_CardName);
            setSharedData("TRG", "BP");
            //setSharedData("IDH_TYPE", "F-S");
            setSharedData("SILENT", "SHOWMULTI");
            doOpenModalForm("IDHCSRCH");
        }
        private void doChooseBPAddress() {
            string sCardCode = moEnquiry.U_CardCode.Trim();
            string sAddress = moEnquiry.U_Address.Trim();
            setSharedData("TRG", "IDH_ADR1");
            setSharedData("IDH_CUSCOD", sCardCode);
            setSharedData("IDH_ADRTYP", "S");
            setSharedData("CALLEDITEM", "IDH_ADR1");
            //setSharedData("SILENT", "SHOWMULTI");
            setSharedData("IDH_ADDRES", sAddress);
            if (Config.ParameterAsBool("UNEWSRCH", false) == false) {
                doOpenModalForm("IDHASRCH");
            } else {
                doOpenModalForm("IDHASRCH3");
            }

        }

        private bool doValidateBPbyCode(ref string sCardName) {
            object oCardName = Config.INSTANCE.doLookupTableField("OCRD", "CardName", "CardCode='" + moEnquiry.U_CardCode.Replace("'", "''") + "'", null, true);
            //sCardName = Config.INSTANCE.doGetBPName(moEnquiry.U_CardCode,false);
            if (oCardName==null || oCardName.ToString().Trim()==string.Empty) {
                sCardName = "";
                return false;
            } else {
                sCardName = oCardName.ToString();
                return true;
            }
        }

        /*
         * Clear the form Data
         */
        private void doClearAll() {
            doClearFormDFValues();
        }

        /**
         * Fill the ComboBoxes
         */
        private void doFillCombos() {
            FillCombos.UsersCombo(Items.Item("IDH_ASGNTO").Specific, idh.bridge.DataHandler.INSTANCE.User, null, true);
            FillCombos.FillCountry(Items.Item("IDH_CUNTRY").Specific);

            //SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_CUNTRY").Specific;
            //FillCombos.FillState(Items.Item("IDH_STATE").Specific,oCombo.Selected.Value);

            FillCombos.FillWR1StatusNew(Items.Item("IDH_STATUS").Specific, 101);
            FillCombos.FillWR1StatusNew(Items.Item("IDH_SOURCE").Specific, 108);

            SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_ASGNTO").Specific;
            oCombo.SelectExclusive(idh.bridge.DataHandler.INSTANCE.User, SAPbouiCOM.BoSearchKey.psk_ByValue);

            //oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_STATE").Specific;
            //oCombo.SelectExclusive(idh.bridge.DataHandler.INSTANCE.User, SAPbouiCOM.BoSearchKey.psk_ByValue);

            oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_STATUS").Specific;
            oCombo.SelectExclusive("1", SAPbouiCOM.BoSearchKey.psk_ByValue);


            oCombo = (SAPbouiCOM.ComboBox)Items.Item("IDH_CUNTRY").Specific;
            //string sDfltCountry = Config.INSTANCE.getParameterWithDefault("DFTCNTRY", "GB");
            oCombo.SelectExclusive(Config.INSTANCE.getParameterWithDefault("DFTCNTRY", "GB"), SAPbouiCOM.BoSearchKey.psk_ByValue);

        }
        private void doSwitchToFind() {
            Items.Item("IDH_DOCNUM").Enabled = true;
            setFocus("IDH_DOCNUM");
            EnableItem(false, "IDH_CRETBP");
            EnableItem(false, "IDH_CRTWOQ");
            EnableItem(false, "IDH_QUSTN");
            string[] oExl = { "IDH_DOCNUM" };
            DisableAllEditItems(oExl);

        }

        private void doSwitchToEdit() {
            setFocus("IDH_ADR1");
            string[] oExl = { "IDH_DOCNUM", "IDH_ACTVTY", "IDH_STATUS", "IDH_USER", "IDH_CardCD" };
            EnableAllEditItems(oExl);
            EnableItem(false, "IDH_DOCNUM");
            setFocus("IDH_ADR1");
            EnableItem(true, "IDH_CRETBP");
            EnableItem(true, "IDH_QUSTN");
        }
        
        private void doUpdateGridCells(bool bForceUpdate) {
            if (!bForceUpdate &&(Mode == BoFormMode.fm_ADD_MODE || Mode == BoFormMode.fm_FIND_MODE))
                return;
            try {
               // moEnquiry.EnquiryItems.doBookmark();
                //moEnquiry.EnquiryItems.first();
                //while(moEnquiry.EnquiryItems.next()) {
                //    if (!string.IsNullOrWhiteSpace( moEnquiry.EnquiryItems.U_Status)  && moEnquiry.EnquiryItems.U_Status != "1") {
                //         moGrid.Settings.SetRowEditable(moEnquiry.EnquiryItems.CurrentRowIndex + 1, false);
                //    }else {
                //        moGrid.Settings.SetRowEditable(moEnquiry.EnquiryItems.CurrentRowIndex + 1, true);
                //    }
                //}
                for (int i=0;i<moGrid.getRowCount();i++) {
                    if (!string.IsNullOrWhiteSpace((string)moGrid.doGetFieldValue(IDH_ENQITEM._Status,i)) && (string)moGrid.doGetFieldValue(IDH_ENQITEM._Status, i) != "1") {
                        moGrid.Settings.SetRowEditable(i+1, false);
                    } else {
                        moGrid.Settings.SetRowEditable(i+1, true);
                    }
                }
                //if (moEnquiry.EnquiryItems.Count > 0) {
                    moGrid.doApplyRules();
                    ListFields olist = moGrid.getListfields();
                    for (int i = 0; i <= olist.Count - 1; i++) {
                       if (!olist.getListField(i).mbCanEdit) {
                            moGrid.Columns.Item(i).Editable = false;
                            //moGrid.doEnableColumn(olist.getListField(i).msFieldName, false);
                        }
                        //if(moGrid.getFieldCanedit(moGrid.doGetListFieldName)) {
                         //   moGrid.doEnableColumn(i, false);
                        //}
                    
                    }
                //}////for (int i = 0; i <= moGrid.getRowCount() - 1; i++) {
                //for (int i = 0; i < moEnquiry.EnquiryItems.CountIncludingEmpty; i++ ) {  
                //    //if (moGrid.doGetFieldValue(IDH_ENQITEM._Status, i).ToString() != "" && moGrid.doGetFieldValue(IDH_ENQITEM._Status, i).ToString() != "1") {
                //    //if (string.IsNullOrWhiteSpace(moEnquiry.Code)) { // moGrid.doGetFieldValue(IDH_ENQITEM._Status, i).ToString() != "" && moGrid.doGetFieldValue(IDH_ENQITEM._Status, i).ToString() != "1") {
                //    //    moGrid.Settings.SetRowEditable(i + 1, false);
                //    //}
                //    moGrid.Settings.SetRowEditable(i + 1, string.IsNullOrWhiteSpace((string)moEnquiry.EnquiryItems.getValue(i,IDH_ENQITEM._Code)));
                //}
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXGEN", null);
            } finally {
               // moEnquiry.EnquiryItems.doRecallBookmark();
            }
        }

#endregion


    }
}

