﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using IDHAddOns.idh.controls;
using com.idh.bridge.data;
using com.uBC.utils;
using com.idh.dbObjects.User;
using com.idh.bridge.lookups;
using com.idh.bridge;

namespace com.isb.forms.Enquiry {
    public class LabTask2 : IDHAddOns.idh.forms.Base {

        protected string msKeyGen = "SEQLBT";//"GENSEQ";

        #region Initialization

        public LabTask(IDHAddOns.idh.addon.Base oParent, string sParMenu, int iMenuPos)
            : base(oParent, "IDHLABTODO", sParMenu, iMenuPos, "Enq_LabTask.srf", true, true, false, "Lab Task List", load_Types.idh_LOAD_NORMAL) {
        }

        protected override void doSetEventFilters() {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE);
        }

        protected override void doReadInputParams(SAPbouiCOM.Form oForm) {
            //base.doReadInputParams(oForm);
            if (getHasSharedData(oForm)) {
                UpdateGrid oGridN = default(UpdateGrid);
                oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");

                int iIndex = 0;
                for (iIndex = 0; iIndex <= oGridN.getGridControl().getFilterFields().Count - 1; iIndex++) {
                    com.idh.controls.strct.FilterField oField = (com.idh.controls.strct.FilterField)(oGridN.getGridControl().getFilterFields()[iIndex]);
                    string sFieldName = oField.msFieldName;
                    string sFieldValue = getParentSharedData(oForm, sFieldName).ToString();
                    try {
                        setUFValue(oForm, sFieldName, sFieldValue);
                    } catch (Exception ex) {
                    }
                }
            }
        }

        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            try {
                oForm.Title = gsTitle;
                //oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE;

                doAddUF(oForm, "IDH_LABTSK", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 100, true, false);

                SAPbouiCOM.Item oItem = oForm.Items.Item("IDH_RECTSK");
                UpdateGrid oGridN = new UpdateGrid(this, oForm, "LINESGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane);
                oGridN.getSBOItem().AffectsFormMode = false;
                oForm.EnableMenu(Config.NAV_ADD, false);
                oForm.EnableMenu(Config.NAV_FIND, false);
                oForm.EnableMenu(Config.NAV_FIRST, false);
                oForm.EnableMenu(Config.NAV_NEXT, false);
                oForm.EnableMenu(Config.NAV_PREV, false);
                oForm.EnableMenu(Config.NAV_LAST, false);


                //oGridN.AddEditLine = false;
                oForm.SupportedModes = Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_All);
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);
                oForm.AutoManaged = true;
                base.doCompleteCreate(ref oForm, ref BubbleEvent);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
                BubbleEvent = false;
            }
        }

        protected void doSetMode(SAPbouiCOM.Form oForm, SAPbouiCOM.BoFormMode iMode) {
            try {
                object sFormType = getParentSharedData(oForm, "FTYPE");
                bool bIsSearch = false;
                if ((sFormType != null) && sFormType.Equals("SEARCH") == true) {
                    bIsSearch = true;
                }

                if (iMode == SAPbouiCOM.BoFormMode.fm_FIND_MODE) {
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption =com.idh.bridge.Translation.getTranslatedWord("Find");
                    if (bIsSearch) {
                        oForm.Items.Item("2").Visible = true;
                    } else {
                        oForm.Items.Item("2").Visible = false;
                    }
                } else if (iMode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
                    ((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption = com.idh.bridge.Translation.getTranslatedWord("Update");
                    oForm.Items.Item("2").Visible = true;
                }
            } catch (Exception ex) {
            }
        }
        #endregion

        #region LoadingDate

        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {
            string sLabTaskId = string.Empty;
            UpdateGrid oGridN = UpdateGrid.getInstance(oForm, "LINESGRID");
            if (oGridN == null) {
                oGridN = new UpdateGrid(this, oForm, "LINESGRID"); //, 7, 100, iWidth - 20, 255, 0, 0);
            }
            if (getHasSharedData(oForm)) {
                sLabTaskId = getParentSharedData(oForm, "LABTASK").ToString();
                setUFValue(oForm, "IDH_LABTSK", sLabTaskId);
                doClearParentSharedData(oForm);
                //SAPbouiCOM.EditText oLabCd = (SAPbouiCOM.EditText)oForm.Items.Item("IDH_ICode").Specific;
                //oLabCd.Value = sLabTaskId;

            }
            //UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
            //if (oGridN == null) {
            //    oGridN = new UpdateGrid(this, oForm, "LINESGRID");
            //}

            oGridN.doAddGridTable(new GridTable(idh.dbObjects.Base.IDH_LABTASK.TableName, null, "Code", true), true);
            oGridN.setOrderValue("Code");
            oGridN.doSetDoCount(true);
            oGridN.doSetBlankLine(false);

            doSetFilterFields(oGridN);
            doSetListFields(oGridN);
            oGridN.doApplyRules();
            //oGridN.getSBOItem().AffectsFormMode = true;

            doFillCombos(oForm);
        }

        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            oForm.Freeze(true);
            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);            
            FilterGrid oGridN = (FilterGrid)FilterGrid.getInstance(oForm, "LINESGRID");
            string sLabTaskCd = getUFValue(oForm, "IDH_LABTSK").ToString();
            if (sLabTaskCd.Length > 0) {
                oGridN.setRequiredFilter(" Code = '" + sLabTaskCd + "' And U_Saved=1");
                SAPbouiCOM.EditText oLabCd = (SAPbouiCOM.EditText)oForm.Items.Item("IDH_Code").Specific;
                oLabCd.Value = sLabTaskCd;
                setUFValue(oForm, "IDH_LABTSK", "");
            } else {
                oGridN.setRequiredFilter("U_Saved=1");
            }
            oGridN.doReloadData("", false, false);
            //oGridN.getSBOItem().AffectsFormMode = true;
            oGridN.doApplyRules();

            //Fill Grid Combos 
            doFillGridCombos(oForm);
            oForm.Freeze(false);
        }

        protected void doSetFilterFields(IDHAddOns.idh.controls.UpdateGrid oGridN) {
            oGridN.doAddFilterField("IDH_Code", "Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            oGridN.doAddFilterField("IDH_ICode", "U_LabICd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            oGridN.doAddFilterField("IDH_DEPART", "U_AssignedTo", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            oGridN.doAddFilterField("IDH_Asign", "U_AssignedTo", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            oGridN.doAddFilterField("IDH_TSKST", "U_RowStatus", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            oGridN.doAddFilterField("IDH_DECI", "U_Decision", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            oGridN.doAddFilterField("IDH_BPCD", "U_BPCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            oGridN.doAddFilterField("IDH_BPNM", "U_BPName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            //oGridN.doAddFilterField("IDH_SRCTP", "(select U_ObjectType from [@IDH_LABITM] li where li.Code = U_LabItemRCd )", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 60, "");
            //oGridN.doAddFilterField("IDH_SRCCD", "(select U_ObjectCd from [@IDH_LABITM] li where li.Code = U_LabItemRCd )", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 60, "");
            oGridN.doAddFilterField("IDH_SRCTP", IDH_LABTASK._ObjectType, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            oGridN.doAddFilterField("IDH_SRCCD", IDH_LABTASK._ObjectCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
            oGridN.doAddFilterField("IDH_SRDNUM", IDH_LABTASK._DocNum, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 60, "");
        
        }

        protected void doSetListFields(IDHAddOns.idh.controls.UpdateGrid oGridN) {
            string sImgPath1 = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn1.png");
            string sImgPath1g = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn1_Green.png");
            string sImgPath2 = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn2.png");
            string sImgPath2g = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn2_Green.png");
            string sImgPath3 = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn3.png");
            string sImgPath3g = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn3_Green.png");

            oGridN.doAddListField("Code", "Code", false, -1, null, null);
            oGridN.doAddListField("Name", "Name", false, 0, null, null);
            oGridN.doAddListField("U_LabItemRCd", "Row Code", false, 0, null, null);
            oGridN.doAddListField("U_LabICd", "Lab Item Code", false, -1, null, null);

            //(select U_ObjectCd from [@IDH_LABITM] li where li.Code = lt.U_LabItemRCd )
            //(select U_ObjectType from [@IDH_LABITM] li where li.Code = lt.U_LabItemRCd )

            //oGridN.doAddListField("(select U_ObjectType from [@IDH_LABITM] li where li.Code = U_LabItemRCd )", "Source Type", false, -1, null, null);
            //oGridN.doAddListField("(select U_ObjectCd from [@IDH_LABITM] li where li.Code = U_LabItemRCd )", "Source ID", false, -1, null, null);

            oGridN.doAddListField("U_ObjectType", "Source Type", false, -1, null, null);
            //oGridN.doAddListField("( CASE WHEN Len(IsNull(U_ObjectCd, '')) > 0 THEN (select U_JobNr from [@IDH_WOQITEM] WQR where WQR.Code = U_ObjectCd) ELSE '' END )", "Source Header", false, -1, null, null);
            oGridN.doAddListField(IDH_LABTASK._DocNum, "Source Doc Num", false, -1, null, null);            
            oGridN.doAddListField("U_ObjectCd", "Source Doc Row", false, -1, null, null);


            oGridN.doAddListField("U_BPCode", "BP Code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner);
            oGridN.doAddListField("U_BPName", "BP Name", false, -1, null, null);
            oGridN.doAddListField("U_DtRequested", "Date Requested", false, -1, null, null);
            oGridN.doAddListField("U_RowStatus", "Status", true, -1, "COMBOBOX", null);
            oGridN.doAddListField("U_DtReceived", "Date Received", true, -1, null, null);
            oGridN.doAddListField("U_DtAnalysed", "Date Analysed", true, -1, null, null);
            oGridN.doAddListField("U_Department", "Department", true, -1, "COMBOBOX", null);
            oGridN.doAddListField("U_AssignedTo", "Assigned To", true, -1, "COMBOBOX", null);

            oGridN.doAddListField("U_AnalysisReq", "Analysis Required", false, -1, null, null);
            oGridN.doAddListField("( CASE WHEN LEN(ISNULL(U_AnalysisReq, '')) > 0 THEN  ( select '" + sImgPath1g + "') ELSE (select '" + sImgPath1 + "') END )", "Analy. Req.", false, 10, "PICTURE", null);

            oGridN.doAddListField(IDH_LABTASK._ExtTsDt, "External Test Date", true, -1, null, null);
            oGridN.doAddListField("U_ExtStatus", "External Status", true, -1, "COMBOBOX", null);
            oGridN.doAddListField("U_ExtComments", "External Comments", true, -1, null, null);


            oGridN.doAddListField("U_AtcAbsEntry", "Attachments ID", false, 0, null, null);
            //oGridN.doAddListField("(select '" + sImgPath2 + "')", "Atch", false, 10, "PICTURE", null);
            oGridN.doAddListField("( CASE WHEN U_AtcAbsEntry > 0 THEN  ( select '" + sImgPath2g + "') ELSE (select '" + sImgPath2 + "') END )", "Atch", false, 10, "PICTURE", null);

            oGridN.doAddListField("U_ImpResults", "Import Results ID", false, 0, null, null);
            //oGridN.doAddListField("(select '" + sImgPath3 + "')", "Imp. Res.", false, 10, "PICTURE", null);
            oGridN.doAddListField("( CASE WHEN LEN(ISNULL(U_ImpResults, '')) > 0 THEN  ( select '" + sImgPath3g + "') ELSE (select '" + sImgPath3 + "') END )", "Imp. Res.", false, 10, "PICTURE", null);

            ////oGridN.doAddListField("U_ImpResults", "Import Results", false, -1, "PICTURE", null);

            ////Dim sImgPath As String = Config.ParameterWithDefault("IMGPATH", "").Trim
            //string sImgPath = com.idh.bridge.lookups.Config.ParamaterWithDefault("IMGPATH", "").Trim();
            //string sBtnImg = "GridDotBtn.png";
            //string sFinalPath = sImgPath + sBtnImg;

            ////oGridN.doAddListField(sFinalPath, "Import Results", false, -1, "PICTURE", null);
            //string sTmp = IDHAddOns.idh.addon.Base.doFixImagePath("GridDotBtn.png"); 
            //oGridN.doAddListField("(select '" + sTmp + "')", "Import Results", false, 10, "PICTURE", null);
            //oGridN.doAddListField("(select '" + sFinalPath + "')", "Import Results1", false, 10, "PICTURE", null);
            ////oGridN.doAddListField(doGetProgressImageQuery, "", False, 10, "PICTURE", Nothing);


            //oGridN.doAddListField("U_ImpResults", "Import Results ID", false, -1, null, null);

            oGridN.doAddListField("U_Decision", "Decision", true, -1, "COMBOBOX", null);
        }

        #endregion

        #region UnloadingData

        public override void doClose() {
        }

        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCloseForm(oForm, ref BubbleEvent);
            FilterGrid.doRemoveGrid(oForm, "LINESGRID");
        }

        #endregion

        #region EventHandlers

        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                if (((SAPbouiCOM.Button)oForm.Items.Item("1").Specific).Caption == Translation.getTranslatedWord("Update") ) {
                    oForm.Freeze(true);
                    UpdateGrid oGridN = (UpdateGrid)FilterGrid.getInstance(oForm, "LINESGRID");

                    //Check if the Lab Task linked with a WOQ Row 
                    ArrayList aObjectID = oGridN.doGetChangedRows();
                    ArrayList aChangedFieldList;
                    int iIndx = 0;
                    int iRow = 0;
                    //string sLabItemRCd = string.Empty;
                    //string sLabItemCd = string.Empty; 
                    string sLabDecision = string.Empty;
                    string sLabTaskCd = string.Empty;
                    string sLTSource = string.Empty;

                    if (aObjectID.Count > 0) {
                        while (iIndx < aObjectID.Count) {
                            iRow = (int)aObjectID[iIndx];
                            oGridN.setCurrentDataRowIndex(iRow);
                            aChangedFieldList = oGridN.doGetChangedFields(iRow);

                            if (aChangedFieldList.Contains(IDH_LABTASK._Decision)) {
                                sLabTaskCd = oGridN.doGetFieldValue(IDH_LABTASK._Code).ToString();
                                //sLabItemRCd = oGridN.doGetFieldValue(IDH_LABTASK._LabItemRCd).ToString();
                                //sLabItemCd = oGridN.doGetFieldValue(IDH_LABTASK._LabICd).ToString();
                                sLabDecision = oGridN.doGetFieldValue(IDH_LABTASK._Decision).ToString();

                                sLTSource = oGridN.doGetFieldValue(IDH_LABTASK._ObjectType).ToString();

                                if (sLTSource.Equals("WOQ")) {
                                    IDH_WOQITEM oWOQRow = new IDH_WOQITEM();
                                    oWOQRow.DoNotSetDefault = true; //will stop to load parent 
                                    int iCnt = oWOQRow.getBySampleRef(sLabTaskCd);
                                    if (iCnt > 0 && oWOQRow.U_SampleRef != null && oWOQRow.U_SampleRef.Length > 0 && oWOQRow.U_SampStatus.Length > 0) {
                                        oWOQRow.U_SampStatus = sLabDecision;
                                        oWOQRow.doUpdateDataRow();
                                    }
                                    oWOQRow = null;
                                } else if (sLTSource.Equals("DO") && oGridN.doGetFieldValue(IDH_LABTASK._ObjectCd).ToString().Length > 0) {
                                    IDH_DISPROW oDORow = new IDH_DISPROW();
                                    oDORow.getByKey(oGridN.doGetFieldValue(IDH_LABTASK._ObjectCd).ToString());
                                    oDORow.U_SampStatus = sLabDecision;
                                    oDORow.doUpdateDataRow();
                                }

                            }
                            iIndx++;
                        }
                    }

                    oGridN.doProcessData();

                    //oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE;
                    doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_OK_MODE);
                    doLoadData(oForm);
                    oForm.Freeze(false);
                } else {
                    oForm.Freeze(true);
                    doLoadData(oForm);
                    oForm.Freeze(false);               
                }
                BubbleEvent = false;
                return;
            }
            //UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
            ////If oGridN.hasFieldChanged("U_Select") Then
            //Int32 _WOQID = getParentSharedDataAsInteger(oForm, "WOQID");
            //Int32 _EnqID = getParentSharedDataAsInteger(oForm, "ENQID");
            //com.idh.dbObjects.User.IDH_ENQUS moIDH_ENQUS = new com.idh.dbObjects.User.IDH_ENQUS();
            //for (Int16 iRow = 0; iRow <= oGridN.getRowCount() - 1; iRow++)
            //{

            //    if (!string.IsNullOrEmpty(oGridN.doGetFieldValue("Code", iRow).ToString()) && moIDH_ENQUS.getByKey(oGridN.doGetFieldValue("Code", iRow).ToString()))
            //    {
            //        moIDH_ENQUS.U_Answer = oGridN.doGetFieldValue("U_Answer", iRow).ToString();
            //    }
            //    else
            //    {
            //        moIDH_ENQUS.doAddEmptyRow(true, true, false);
            //        moIDH_ENQUS.U_Answer = oGridN.doGetFieldValue("U_Answer", iRow).ToString();
            //        if (_EnqID != 0)
            //            moIDH_ENQUS.U_EnqID = Convert.ToInt32(oGridN.doGetFieldValue("U_EnqID", iRow).ToString());
            //        moIDH_ENQUS.U_QsID = oGridN.doGetFieldValue("QID", iRow).ToString();
            //        moIDH_ENQUS.U_Question = oGridN.doGetFieldValue("Question", iRow).ToString();
            //        if (_WOQID != 0)
            //            moIDH_ENQUS.U_WOQID = Convert.ToInt32(oGridN.doGetFieldValue("U_WOQID", iRow).ToString());

            //    }
            //    moIDH_ENQUS.doProcessData();
            //    //End If
            //}
          //  doLoadData(oForm);

        }

        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            base.doItemEvent(oForm, ref pVal, ref BubbleEvent);
            if ((pVal.ItemUID == "IDH_WSTGRP") && pVal.EventType == SAPbouiCOM.BoEventTypes.et_COMBO_SELECT && pVal.BeforeAction == false) {
                doLoadData(oForm);
            }
            return true;
        }

        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED) {
                //IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");

                //com.idh.bridge.DataHandler.NextNumbers oNumbers;// = com.idh.bridge.DataHandler.NextNumbers("GENSEQ");

                //oGridN.doSetFieldValue("Code", pVal.Row, oNumbers.CodeCode, true);
                //oGridN.doSetFieldValue("Name", pVal.Row, oNumbers.NameCode, true);

                //oGridN.doSetFieldValue("Name", pVal.Row, "1", true);
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
            } else if (pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK) {
                if (pVal.ItemUID == "LINESGRID") {
                    if (pVal.BeforeAction == false) {
                        IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");

                        if (pVal.ColUID.Contains("GridDotBtn1.png")) {
                            setSharedData(oForm, "TSKANLCD", oGridN.doGetFieldValue("U_AnalysisReq", pVal.Row));
                            goParent.doOpenModalForm("IDHLABTANL", oForm);
                        }

                        if (pVal.ColUID.Contains("GridDotBtn2.png")) {
                            string sLabTaskCd = oGridN.doGetFieldValue("Code", pVal.Row).ToString();
                            string sAttachmentEntry = oGridN.doGetFieldValue("U_AtcAbsEntry", pVal.Row).ToString();
                            if (sAttachmentEntry != "0") {
                                //Open Existing Attachment Entry 
                                setSharedData(oForm, "ANEXCD", sAttachmentEntry);
                            } else {
                                //New Attachment Entry 
                                setSharedData(oForm, "TFSCODE", sLabTaskCd);
                                setSharedData(oForm, "TFSNUM", sLabTaskCd);
                            }
                            goParent.doOpenModalForm("IDHATTACH", oForm);
                        }

                        if (pVal.ColUID.Contains("GridDotBtn3.png")) {
                            string sLabTaskCd = oGridN.doGetFieldValue("Code", pVal.Row).ToString();
                            //string sImpResultsID = oGridN.doGetFieldValue("U_ImpResults", pVal.Row).ToString();
                            setSharedData(oForm, "LABTSKID", sLabTaskCd);
                            //setSharedData(oForm, "IMPRSLID", sImpResultsID);
                            goParent.doOpenModalForm("IDHLABRESULTS", oForm);
                        }

                    }
                }

            }

            return base.doCustomItemEvent(oForm, ref pVal);

        }

        protected override void doHandleModalResultShared(SAPbouiCOM.Form oParentForm, string sModalFormType, string sLastButton = null) {
            if (sModalFormType == "IDHLABTANL") {
                IDHGrid oGridN = IDHGrid.getInstance(oParentForm, "LINESGRID");
                string sExistingTAC = oGridN.doGetFieldValue("U_AnalysisReq").ToString();
                string sTskAnalysisCds = getSharedData(oParentForm, "NEWTSKANLCD").ToString();

                if (!sExistingTAC.Equals(sTskAnalysisCds)) {
                    oGridN.doSetFieldValue("U_AnalysisReq", sTskAnalysisCds);
                    oParentForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
                }
            } else if (sModalFormType == "IDHATTACH") {
                IDHGrid oGridN = IDHGrid.getInstance(oParentForm, "LINESGRID");
                string sExistingACd = oGridN.doGetFieldValue("U_AtcAbsEntry").ToString();
                string sAttachmentCode = getSharedData(oParentForm, "ATCHCD").ToString();

                if (!sExistingACd.Equals(sAttachmentCode)) {
                    oGridN.doSetFieldValue("U_AtcAbsEntry", sAttachmentCode);
                    oParentForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
                }
            } else if (sModalFormType == "IDHLABRESULTS") {
                string sLTID = getSharedData(oParentForm, "LABTSKID").ToString();
                IDH_LABTSKIMP oLTImport = new IDH_LABTSKIMP();
                oLTImport.getByLabTaskCode(sLTID);
                if (oLTImport.Code.Length > 0) {
                    IDHGrid oGridN = IDHGrid.getInstance(oParentForm, "LINESGRID");
                    oGridN.doSetFieldValue("U_ImpResults", oLTImport.Code);
                    oParentForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
                }

            }
            base.doHandleModalResultShared(oParentForm, sModalFormType, sLastButton);
        }

        public override bool doMenuEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction == false && pVal.MenuUID == "IDHGMADD") {
                UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
                oGridN.AddEditLine = true;
                oGridN.doAddDataLine(true);
                oGridN.AddEditLine = false;

            }
            return base.doMenuEvent(oForm, ref pVal, ref BubbleEvent);
        }
        #endregion

        #region CustomFunctions

        protected void doFillCombos(SAPbouiCOM.Form oForm) {
            //SAPbouiCOM.Item oItem = oForm.Items.Item("IDH_TYPE");
            ////Date Combo 
            //SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
            //oCombo.ValidValues.Add("", "Both"); 
            //oCombo.ValidValues.Add("SAM", "Sample Items");
            //oCombo.ValidValues.Add("BAT", "Batch Items");
            //oCombo.Select("Both", SAPbouiCOM.BoSearchKey.psk_ByValue);

            SAPbouiCOM.Item oItem = oForm.Items.Item("IDH_DEPART");
            SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
            doFillCombo(oForm, oCombo, "OUDP", "OUDP.Name", "OUDP.Name", null, "OUDP.Name", "Any", "");

            oItem = oForm.Items.Item("IDH_TSKST");
            oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
            doFillCombo(oForm, oCombo, "[@IDH_LABSTATS]", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Type = 'ExtStatus'", "[@IDH_LABSTATS].U_Status", "Any", "");

            oItem = oForm.Items.Item("IDH_DECI");
            oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
            doFillCombo(oForm, oCombo, "[@IDH_LABSTATS]", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Type = 'Decision'", "[@IDH_LABSTATS].U_Status", "Any", "");

        }

        protected void doFillGridCombos(SAPbouiCOM.Form oForm) {
            UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
            SAPbouiCOM.ComboBoxColumn oCombo;

            oCombo = (SAPbouiCOM.ComboBoxColumn)oGridN.getSBOGrid().Columns.Item(oGridN.doIndexFieldWC("U_RowStatus"));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            com.idh.bridge.utils.Combobox2.doClearCombo(oCombo);
            com.idh.bridge.utils.Combobox2.doFillCombo(oCombo, "[@IDH_LABSTATS]", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Type = 'RowStatus'", "[@IDH_LABSTATS].U_Status");

            oCombo = (SAPbouiCOM.ComboBoxColumn)oGridN.getSBOGrid().Columns.Item(oGridN.doIndexFieldWC("U_Department"));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            com.idh.bridge.utils.Combobox2.doClearCombo(oCombo);
            com.idh.bridge.utils.Combobox2.doFillCombo(oCombo, "OUDP", "OUDP.Name", "OUDP.Name", null, "OUDP.Name");

            oCombo = (SAPbouiCOM.ComboBoxColumn)oGridN.getSBOGrid().Columns.Item(oGridN.doIndexFieldWC("U_AssignedTo"));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            com.idh.bridge.utils.Combobox2.doClearCombo(oCombo);
            com.idh.bridge.utils.Combobox2.doFillCombo(oCombo, "OUSR", "USER_CODE", "U_NAME", null, "U_NAME");

            oCombo = (SAPbouiCOM.ComboBoxColumn)oGridN.getSBOGrid().Columns.Item(oGridN.doIndexFieldWC("U_ExtStatus"));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            com.idh.bridge.utils.Combobox2.doClearCombo(oCombo);
            com.idh.bridge.utils.Combobox2.doFillCombo(oCombo, "[@IDH_LABSTATS]", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Type = 'ExtStatus'", "[@IDH_LABSTATS].U_Status");

            oCombo = (SAPbouiCOM.ComboBoxColumn)oGridN.getSBOGrid().Columns.Item(oGridN.doIndexFieldWC("U_Decision"));
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            com.idh.bridge.utils.Combobox2.doClearCombo(oCombo);
            com.idh.bridge.utils.Combobox2.doFillCombo(oCombo, "[@IDH_LABSTATS]", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Type = 'Decision'", "[@IDH_LABSTATS].U_Status");
        }

        #endregion






    }
}