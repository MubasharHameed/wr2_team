﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.isb.forms.Enquiry {
    public class Common {
        public static object getNumericValue(object Val) {
            if (Val == null) {
                return 0;
            } else
                return Val;
        }
        public static object getStringValue(object Val) {
            if (Val == null) {
                return "";
            } else
                return Val;
        }
    }
}
