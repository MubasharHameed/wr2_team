﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using IDHAddOns.idh.controls;
using com.idh.bridge.data;
using com.uBC.utils;
using com.idh.bridge.utils;
using com.idh.bridge;

using com.isb.enq.dbObjects.User;

namespace com.isb.forms.Enquiry.admin.Approval {
    //Inherits idh.forms.admin.Tmpl
    //msKeyGen="WGPVALDS"
    public class Approvals : IDHAddOns.idh.forms.Base {

        //protected string msKeyGen = "GENSEQ";
        public Approvals(IDHAddOns.idh.addon.Base oParent, string sParMenu, int iMenuPosition)
            : base(oParent, "IDHAPVLS", sParMenu, iMenuPosition, "admin.srf", true, true, false, "Approvals", load_Types.idh_LOAD_NORMAL) {

        }
        //protected string getTable() {
        //    return "@" + getUserTable();
        //}

        //protected string getUserTable() {
        //    return "IDH_WGVALDS";
        //}

        protected override void doSetEventFilters() {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);

        }
        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            try {
                oForm.Title = gsTitle;

                SAPbouiCOM.Item oItem = default(SAPbouiCOM.Item);
                oItem = oForm.Items.Item("IDH_CONTA");
                UpdateGrid oGridN = new UpdateGrid(this, oForm, "LINESGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane);

                //doSetFilterLayout(oForm)
               // oForm.Items.Item("IDH_WSTGRP").AffectsFormMode = false;

            } catch (Exception ex) {
                //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
                BubbleEvent = false;
            }
        }

        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCloseForm(oForm, ref BubbleEvent);
            FilterGrid.doRemoveGrid(oForm, "LINESGRID");
        }

        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {
            //goParent.goDB.doSelectQuery("If (Select Count(1) from [@IDH_FORMSET])=1211 Begin  Exec IDH_CopyDeptForAllUsers; Exec IDH_UpdateFormSettings; end ")
            //doe("Select 1; Exec IDH_CopyDeptForAllUsers; ")
            //doUpdateQuery("Select 1; Exec IDH_UpdateFormSettings")
            UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
            if (oGridN == null) {
                oGridN = new UpdateGrid(this, oForm, "LINESGRID");
            }
            oGridN.AddEditLine = false;

            doSetFilterFields(oGridN);
            doSetListFields(oGridN);

            oGridN.doAddGridTable(new GridTable(IDH_APPROVLH.TableName, "h", "Code", false,true), false);
            oGridN.doAddGridTable(new GridTable(IDH_APPROVLD.TableName, "d", "Code", true,true), true);

            oGridN.doSetDoCount(true);
            oGridN.doSetHistory(IDH_APPROVLD.TableName,"Code");
            //oGridN.setOrderValue("U_GridName ASC, U_Index Asc,U_Name Asc,Cast(Code as Int) ASC ")
            string UserID = com.idh.bridge.lookups.Config.INSTANCE.doGetUserId(idh.bridge.DataHandler.INSTANCE.User);
            //idh.bridge.DataHandler.loo.INSTANCE..SBOCompany.UserSignature
            string sRequired = "h.Code=d.U_APPHID And h.U_Status='" + ((int)IDH_APPROVLH.en_APPROVALSTATUS.Draft).ToString() + "' And h.U_FormTyp='IDHWOQ' And d.U_Status='" + ((int)IDH_APPROVLD.en_APPROVALSTATUS.Waiting).ToString() + "'  And d.U_ApprvUId='" + UserID + "'";
            oGridN.setRequiredFilter(sRequired);
            oGridN.setOrderValue("h." + IDH_APPROVLH._DocAddDt + " Desc," + "cast(h." + IDH_APPROVLH._DocNum + " as int) Desc");
            //doFillCombos(oForm);
        }

        public override void doFinalizeShow(SAPbouiCOM.Form oForm) {
            base.doFinalizeShow(oForm);
        }

        protected void doSetFilterFields(IDHAddOns.idh.controls.FilterGrid oGridN) {
            //oGridN.doAddFilterField("IDH_DEPT", "U_DeptCode", SAPbouiCOM.BoDataType.dt_SHORT_NUMBER, "=", 10, Nothing)
            //oGridN.doAddFilterField("IDH_USERS", "U_UserName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "")
            //oGridN.doAddFilterField("IDH_FORMS", "U_FormID", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "")
        }

        protected void doSetListFields(IDHAddOns.idh.controls.FilterGrid oGridN) {
            oGridN.doAddListField("d.Code", "DCode", false, -1, null, null);
            oGridN.doAddListField("h.Code", "HCode", false, -1, null, null);

            oGridN.doAddListField("h." + IDH_APPROVLH._Origintr, "Originator", false, 50, null, null);
            oGridN.doAddListField("h." + IDH_APPROVLH._DocAddDt, "Request Date", false, -1, null, null);
            oGridN.doAddListField("h." + IDH_APPROVLH._DocNum, "Doc Num", false, -1, null, null);
            oGridN.doAddListField("h." + IDH_APPROVLH._DocVer, "Version", false, 10, null, null);
            oGridN.doAddListField("h." + IDH_APPROVLH._FormTyp, "Doc Type", false, 20, null, null);
            oGridN.doAddListField("d." + IDH_APPROVLD._Status, "Status", true, 20, "COMBOBOX", null);
            oGridN.doAddListField("d." + IDH_APPROVLD._Remarks, "Remarks", true, 100, null, null);
            // oGridN.doAddListField("U_WstGpCd", "Waste Group", False, 0, Nothing, Nothing)

            //oGridN.doAddListField("U_OrderBy", "Order By", True, -1, "COMBOBOX", Nothing)


        }

        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            //SAPbouiCOM.ComboBox oComboWasteGroups = default(SAPbouiCOM.ComboBox);
            //oComboWasteGroups = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_WSTGRP").Specific;
            doReLoadData(oForm);
            //  string sSql = null;
            //if ((oComboWasteGroups.Selected != null) && (oComboWasteGroups.Selected.Value != "0")) {
            //sSql = "Select a.U_ValidCd,a.U_ValidNm,(Select b.U_Select from [@IDH_WGVALDS]  b with(nolock) where a.U_ValidCd=b.U_ValidCd and b.U_WstGpCd='" & oComboWasteGroups.Selected.Value & "') U_Select,a.Code,a.Name from [@IDH_WGVDMS] a with(nolock) order by a.U_ValidCd"
            //sSql = "Select b.code,b.Name ,a.U_ValidCd,a.U_ValidNm,b.U_Select from [@IDH_WGVDMS] a with(nolock) left join [@IDH_WGVALDS]  b on a.U_ValidCd=b.U_ValidCd Where isnull(b.U_WstGpCd,'" & oComboWasteGroups.Selected.Value & "')='" & oComboWasteGroups.Selected.Value & "'"
            //  sSql = "Select T0.* From (Select b.code,b.Name,a.U_ValidCd,a.U_ValidNm,b.U_Select,b.U_ValList,b.U_Sort  from [@IDH_WGVDMS] a with(nolock), " + " [@IDH_WGVALDS] b where a.U_ValidCd=b.U_ValidCd and b.U_WstGpCd='" + oComboWasteGroups.Selected.Value + "'" + " UNION ALL " + " Select null code,null Name,a.U_ValidCd,a.U_ValidNm,null U_Select, U_Expression U_ValList,0 as U_Sort from [@IDH_WGVDMS] a with(nolock) where a.U_Common<>'Y' AND " + " not a.U_ValidCd in (Select b.U_ValidCd from [@IDH_WGVALDS]  b where b.U_WstGpCd='" + oComboWasteGroups.Selected.Value + "') )T0 Order by U_ValidCd";
            //} else {
            //  sSql = "Select b.code,b.Name ,a.U_ValidCd,a.U_ValidNm,b.U_Select,b.U_ValList,b.U_Sort   from [@IDH_WGVDMS] a with(nolock) Inner join [@IDH_WGVALDS]  b on a.U_ValidCd=b.U_ValidCd Where b.U_WstGpCd='-1'";
            //}
            //Dim oRecSet As SAPbobsCOM.Recordset = PARENT.goDB.doSelectQuery(sSql)
            //UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
            //oGridN.getSBOGrid().DataTable.ExecuteQuery(sSql);
            //oGridN.doApplyRules();
            //  MyBase.doLoadData(oForm)
        }

        protected void doReLoadData(SAPbouiCOM.Form oForm) {
            try {
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE);

                UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
                //'If bIsFirst = False Then
                //'    oGridN.getGridControl().doCreateFilterString()
                //'    If oGridN.doCheckForQueryChange() = False Then
                //'        Return
                //'    End If
                //'End If


                string sFilter = oGridN.getRequiredFilter();
                //doSetStatusFilter(oGridN)
                //doUpdateStatuses(oGridN)
                //doSetFormSpecificFilters(oGridN)
                oGridN.doReloadData("", false, true);
                doFillGridCombos(oForm, oGridN);
                //oGridN.doApplyRules();
                //oGridN.setRequiredFilter(sFilter)
                //''Dim eTime As DateTime = Now
                //''PARENT.APPLICATION.StatusBar.SetText("Milliseconds to load Grid by Use of IDH_VPROGRESS=" & com.idh.bridge.lookups.Config.INSTANCE.getParameter("USEVIEW") & ": " & eTime.Subtract(sTime).TotalMilliseconds.ToString, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                //''## MA test 27-08-2014

            } catch (Exception ex) {
                //com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error reloading the Data.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBL", null);
            }
        }

        private void doSetMode(SAPbouiCOM.Form oForm, SAPbouiCOM.BoFormMode iMode) {
            try {
                if (iMode == SAPbouiCOM.BoFormMode.fm_FIND_MODE) {
                    ((SAPbouiCOM.Button)(oForm.Items.Item("1").Specific)).Caption = com.idh.bridge.Translation.getTranslatedWord("Find");

                    // oForm.Items.Item("2").Visible = true;
                    //  oForm.Items.Item("IDH_CHOOSE").Visible = true;

                } else if (iMode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
                    ((SAPbouiCOM.Button)(oForm.Items.Item("1").Specific)).Caption = com.idh.bridge.Translation.getTranslatedWord("Update");
                    // oForm.Items.Item("2").Visible = true;
                    // oForm.Items.Item("IDH_CHOOSE").Visible = false;
                }
                //                'oForm.Mode = iMode
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBL", null);
            }
        }

        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                if (((SAPbouiCOM.Button)(oForm.Items.Item("1").Specific)).Caption == Translation.getTranslatedWord("Update") &&
                    oForm.Mode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) {
                    if (doSaveApprovalRows(oForm)) {
                        doReLoadData(oForm);
                    }
                } else if (((SAPbouiCOM.Button)(oForm.Items.Item("1").Specific)).Caption == Translation.getTranslatedWord("Find")) {
                    doReLoadData(oForm);
                }
                BubbleEvent = false;
                return;
            }
            //BubbleEvent = false;

            SAPbouiCOM.ComboBox oComboWasteGroup = default(SAPbouiCOM.ComboBox);
            oComboWasteGroup = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_WSTGRP").Specific;
            if ((oComboWasteGroup.Selected.Value != "0")) {
                UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
                //If oGridN.hasFieldChanged("U_Select") Then
                com.idh.dbObjects.User.IDH_WGVALDS moIDH_WGVALDS = new com.idh.dbObjects.User.IDH_WGVALDS();
                for (Int16 iRow = 0; iRow <= oGridN.getRowCount() - 1; iRow++) {
                    //If oGridN.dataHasChanged Then ', "U_Select"

                    if (!string.IsNullOrEmpty(oGridN.doGetFieldValue("Code", iRow).ToString()) && moIDH_WGVALDS.getByKey(oGridN.doGetFieldValue("Code", iRow).ToString())) {
                        moIDH_WGVALDS.U_Select = oGridN.doGetFieldValue("U_Select", iRow).ToString();
                        moIDH_WGVALDS.U_ValList = oGridN.doGetFieldValue("U_ValList", iRow).ToString();
                        moIDH_WGVALDS.U_Sort = (int)oGridN.doGetFieldValue("U_Sort", iRow);

                    } else if (!string.IsNullOrEmpty(oGridN.doGetFieldValue("U_Select", iRow).ToString())) {
                        moIDH_WGVALDS.doAddEmptyRow(true, true, false);
                        moIDH_WGVALDS.U_Select = oGridN.doGetFieldValue("U_Select", iRow).ToString();
                        moIDH_WGVALDS.U_ValidCd = oGridN.doGetFieldValue("U_ValidCd", iRow).ToString();
                        moIDH_WGVALDS.U_ValidNm = oGridN.doGetFieldValue("U_ValidNm", iRow).ToString();
                        moIDH_WGVALDS.U_WstGpCd = oComboWasteGroup.Selected.Value;
                        //oGridN.doGetFieldValue("U_WstGpCd", iRow).ToString()
                        moIDH_WGVALDS.U_ValList = oGridN.doGetFieldValue("U_ValList", iRow).ToString();
                        moIDH_WGVALDS.U_Sort = (int)oGridN.doGetFieldValue("U_Sort", iRow);

                    }
                    moIDH_WGVALDS.doProcessData();
                } 
            }

        }

        private bool doSaveApprovalRows(SAPbouiCOM.Form oForm) {
            bool bContinue = true;

            try {
                UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "LINESGRID");
                ArrayList aRows = oGridN.doGetChangedRows();
                if (aRows != null) {
                    string sHKey1 = "", sDKey2 = "";
                    IDH_APPROVLD.en_APPROVALSTATUS enStatus;
                    string UserID = com.idh.bridge.lookups.Config.INSTANCE.doGetUserId(idh.bridge.DataHandler.INSTANCE.User);

                    DataHandler.INSTANCE.StartTransaction();

                    IDH_APPROVLH moAppH = new IDH_APPROVLH();
                    IDH_APPROVLD moAppD = new IDH_APPROVLD();
                    for (int iIndex = 0; iIndex <= aRows.Count - 1; iIndex++) {
                        oGridN.setCurrentDataRowIndex(Convert.ToInt32(aRows[iIndex]));
                        sHKey1 = oGridN.doGetFieldValue("h.Code").ToString();
                        sDKey2 = oGridN.doGetFieldValue("d.Code").ToString();
                        enStatus = (IDH_APPROVLD.en_APPROVALSTATUS)Convert.ToInt16(oGridN.doGetFieldValue("d." + IDH_APPROVLD._Status));
                        IDH_WOQHD moWOQ;
                        //Combobox2.doAddValueToCombo(oCombo, "Approve", "Approve", true);
                        if (!(moAppH.getByKey(sHKey1) && moAppD.getByKey(sDKey2))) {
                            com.idh.bridge.DataHandler.INSTANCE.doResUserError("System failed to load approval data.", "WRNBPA15", null);
                            continue;
                        }
                        if (enStatus == IDH_APPROVLD.en_APPROVALSTATUS.Approve) { //execute approve process and update APP Detail table
                            //also if required then update header table based on Max approve/rejection required

                            if (moAppH.U_NmAprReq <= moAppH.U_NmAprovd + 1) {

                                string sRequestedBy = moAppH.U_Origintr;
                                string sDocNum = moAppH.U_DocNum;
                                string sDocVersion = moAppH.U_DocVer.ToString();
                                bool bFinalDecision = true;
                                string sDecisionType = "Approved";

                                moAppH.U_Status = ((int)IDH_APPROVLH.en_APPROVALSTATUS.Approve).ToString();

                                moAppH.U_LastUpBy = Convert.ToInt16(UserID);
                                moAppH.U_ApprDt = DateTime.Now;

                                moAppD.U_Status = ((int)IDH_APPROVLD.en_APPROVALSTATUS.Approve).ToString();
                                moAppD.U_LstUpdtOn = DateTime.Now;
                                moAppH.U_NmAprovd++;
                                moAppD.U_Remarks = oGridN.doGetFieldValue("d." + IDH_APPROVLD._Remarks).ToString();
                                string sNotes = moAppD.U_Remarks;
                                if (moAppH.doUpdateDataRow()) {
                                    bContinue = moAppD.doUpdateDataRow();
                                    if (!bContinue)
                                        break;
                                    moWOQ = new IDH_WOQHD();
                                    moWOQ.MustLoadChildren = false;
                                    moWOQ.DoNotSetDefault = true;
                                    moWOQ.getByKey(moAppD.U_DocNum);
                                    moWOQ.U_Status = ((int)IDH_WOQHD.en_WOQSTATUS.Approved).ToString();
                                    bContinue= moWOQ.doUpdateDataRow();
                                    if (!bContinue)
                                        break;
                                    bContinue = doSendApprovalDecisionMessage(sRequestedBy, sDocNum, sDocVersion, sDecisionType, bFinalDecision, sNotes);
                                    if (!bContinue)
                                        break;
                                }
                            } else {
                                string sRequestedBy = moAppH.U_Origintr;
                                string sDocNum = moAppH.U_DocNum;
                                string sDocVersion = moAppH.U_DocVer.ToString();
                                bool bFinalDecision = false;
                                string sDecisionType = "Approved";
                                moAppH.U_NmAprovd++;

                                moAppD.U_Status = ((int)IDH_APPROVLD.en_APPROVALSTATUS.Approve).ToString();
                                moAppD.U_LstUpdtOn = DateTime.Now;
                                moAppD.U_Remarks = oGridN.doGetFieldValue("d." + IDH_APPROVLD._Remarks).ToString();
                                string sNotes = moAppD.U_Remarks;
                                if (moAppH.doUpdateDataRow()) {
                                    bContinue = moAppD.doUpdateDataRow();
                                    if (!bContinue)
                                        break;
                                    bContinue = doSendApprovalDecisionMessage(sRequestedBy, sDocNum, sDocVersion, sDecisionType, bFinalDecision, sNotes);
                                    if (!bContinue)
                                        break;
                                } else {
                                    bContinue = false;
                                    break;
                                }
                            }
                        } else if (enStatus == IDH_APPROVLD.en_APPROVALSTATUS.Reject) { //execute reject process and update APP Detail table
                            //also if required then update header table based on Max approve/rejection required
                         
                            if (moAppH.U_NmRejReq <= moAppH.U_NmRejted + 1) {

                                string sRequestedBy = moAppH.U_Origintr;
                                string sDocNum = moAppH.U_DocNum;
                                string sDocVersion = moAppH.U_DocVer.ToString();
                                bool bFinalDecision = true;
                                string sDecisionType = "Reject";

                                moAppH.U_Status = ((int)IDH_APPROVLH.en_APPROVALSTATUS.Reject).ToString();// "Rejected";

                                moAppH.U_LastUpBy = Convert.ToInt16(UserID);
                                moAppH.U_RejDt = DateTime.Now;
                                moAppH.U_NmRejted++;
                                moAppD.U_Status = ((int)IDH_APPROVLD.en_APPROVALSTATUS.Reject).ToString();// "Reject";
                                moAppD.U_LstUpdtOn = DateTime.Now;
                                moAppD.U_Remarks = oGridN.doGetFieldValue("d." + IDH_APPROVLD._Remarks).ToString();
                                string sNotes = moAppD.U_Remarks;
                                if (moAppH.doUpdateDataRow()) {
                                    bContinue = moAppD.doUpdateDataRow();
                                    moWOQ = new IDH_WOQHD();
                                    moWOQ.MustLoadChildren = false;
                                    moWOQ.DoNotSetDefault = true;
                                    moWOQ.getByKey(moAppD.U_DocNum);
                                    moWOQ.U_Status = ((int)IDH_WOQHD.en_WOQSTATUS.Rejected).ToString();
                                    bContinue = moWOQ.doUpdateDataRow();
                                    if (!bContinue)
                                        break;
                                    bContinue = doSendApprovalDecisionMessage(sRequestedBy, sDocNum, sDocVersion, sDecisionType, bFinalDecision, sNotes);
                                } else {
                                    bContinue = false;
                                        break;
                                }
                            } else {

                                string sRequestedBy = moAppH.U_Origintr;
                                string sDocNum = moAppH.U_DocNum;
                                string sDocVersion = moAppH.U_DocVer.ToString();
                                bool bFinalDecision = false;
                                string sDecisionType = "Reject";


                                moAppH.U_NmRejted++;
                                moAppD.U_Status = ((int)IDH_APPROVLD.en_APPROVALSTATUS.Reject).ToString();
                                moAppD.U_LstUpdtOn = DateTime.Now;
                                moAppD.U_Remarks = oGridN.doGetFieldValue("d." + IDH_APPROVLD._Remarks).ToString();
                                string sNotes = moAppD.U_Remarks;
                                if (moAppH.doUpdateDataRow())
                                    bContinue = moAppD.doUpdateDataRow();
                                else {
                                    bContinue = false;
                                        break;
                                }
                                bContinue = doSendApprovalDecisionMessage(sRequestedBy, sDocNum, sDocVersion, sDecisionType, bFinalDecision, sNotes);
                                if (!bContinue)
                                    break;
                            }
                        } else if (enStatus == IDH_APPROVLD.en_APPROVALSTATUS.ReviewAdvised) { //execute reject process and update APP Detail table
                            //also if required then update header table based on Max approve/rejection required
                         
                            string sRequestedBy = moAppH.U_Origintr;
                            string sDocNum = moAppH.U_DocNum;
                            string sDocVersion = moAppH.U_DocVer.ToString();
                            bool bFinalDecision = true;
                            string sDecisionType = "Review";

                            moAppH.U_Status = ((int)IDH_APPROVLH.en_APPROVALSTATUS.ReviewAdvised).ToString();//"Review";

                            moAppH.U_LastUpBy = Convert.ToInt16(UserID);
                            //  moAppH.U_RejDt = DateTime.Now;

                            moAppD.U_Status = ((int)IDH_APPROVLD.en_APPROVALSTATUS.ReviewAdvised).ToString(); ;
                            moAppD.U_LstUpdtOn = DateTime.Now;
                            moAppD.U_Remarks = oGridN.doGetFieldValue("d." + IDH_APPROVLD._Remarks).ToString();
                            string sReviewNotes = moAppD.U_Remarks;
                            if (moAppH.doUpdateDataRow()) {
                                bContinue = moAppD.doUpdateDataRow();
                                if (!bContinue)
                                    break;
                                
                                moWOQ = new IDH_WOQHD();
                                moWOQ.MustLoadChildren = true;
                                moWOQ.DoNotSetDefault = true;
                                bContinue = moWOQ.getByKey(moAppD.U_DocNum);
                                if (!bContinue)
                                    break;
                                bContinue = CopyCurrentVersionToHistory(moWOQ,sReviewNotes);
                                if (!bContinue)
                                    break;
                                moWOQ.U_Status = ((int)IDH_WOQHD.en_WOQSTATUS.ReviewAdvised).ToString();
                                moWOQ.U_Version++;
                                moWOQ.U_ReviewNotes += "Review by " + idh.bridge.DataHandler.INSTANCE.User + "\n" + sReviewNotes + "\n";
                                bContinue = moWOQ.doUpdateDataRow();
                                if (!bContinue)
                                    break;
                                bContinue = doSendApprovalDecisionMessage(sRequestedBy, sDocNum, sDocVersion, sDecisionType, bFinalDecision, sReviewNotes);
                                if (!bContinue)
                                    break;
                            } else {
                                bContinue = false;
                                break;
                            }
                        }
                        //Combobox2.doAddValueToCombo(oCombo, "Reject", "Reject", true);
                        //Combobox2.doAddValueToCombo(oCombo, "Review", "Review Advised", true);

                        if (!bContinue)
                            break;
                    }// end of for loop

            
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", null);
                return false;
            } finally {
                if (bContinue == false && DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);

                else if (bContinue && DataHandler.INSTANCE.IsInTransaction()) {
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);

                }

            }
           return true;
         }
        private bool CopyCurrentVersionToHistory(IDH_WOQHD _moWOQ, string sReviewNotes) {
            bool bContinue = true;            
            try {
                IDH_WOQHVR moWOQ_His = new IDH_WOQHVR();
                int icolcount = _moWOQ.getColumnCount();
                moWOQ_His.doAddEmptyRow(true);
                for (int icol = 0; icol <= icolcount - 1; icol++) {
                    if (!(_moWOQ.getFieldInfo(icol).FieldName == "Code" || _moWOQ.getFieldInfo(icol).FieldName == "Name"))
                        moWOQ_His.setValue(_moWOQ.getFieldInfo(icol).FieldName, _moWOQ.getValue(_moWOQ.getFieldInfo(icol).FieldName));
                }
                moWOQ_His.U_BasWOQID = _moWOQ.Code;
                moWOQ_His.U_VerCrBy = com.idh.bridge.lookups.Config.INSTANCE.doGetUserId(idh.bridge.DataHandler.INSTANCE.User);
                moWOQ_His.U_VerDate = DateTime.Now;
                moWOQ_His.U_VerDateTm = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                moWOQ_His.U_ReviewNotes += "\r" + "Review by " + idh.bridge.DataHandler.INSTANCE.User + "\r" + sReviewNotes + "\r";
                
                bContinue=moWOQ_His.doAddDataRow();
                
                if (_moWOQ.WOQItems.Count > 0) {
                    IDH_WOQITVR moWOQITM_His = new IDH_WOQITVR();
                    _moWOQ.first();
                    icolcount = _moWOQ.WOQItems.getColumnCount();
                    while (_moWOQ.WOQItems.next()) {
                        //for (int irow = 0; irow <= _moWOQ.WOQItems.Count - 1; irow++) {
                        moWOQITM_His = new IDH_WOQITVR();
                        moWOQITM_His.doAddEmptyRow(false, true);

                        moWOQITM_His.U_BasWOHID = moWOQ_His.Code;
                        moWOQITM_His.U_BasWQRID = _moWOQ.WOQItems.Code;

                        for (int icol = 0; icol <= icolcount - 1; icol++) {
                                if (!(_moWOQ.WOQItems.getFieldInfo(icol).FieldName == "Code" || _moWOQ.WOQItems.getFieldInfo(icol).FieldName == "Name"))
                                    moWOQITM_His.setValue(_moWOQ.WOQItems.getFieldInfo(icol).FieldName, _moWOQ.WOQItems.getValue(_moWOQ.WOQItems.getFieldInfo(icol).FieldName));
                        }
                   bContinue=     moWOQITM_His.doAddDataRow();
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", null);
                return false;
            }
            return bContinue;
        }

        public bool doSendApprovalDecisionMessage(string sRequestedBy, string sDocNum, string sDocVersion, string sDecisionType, bool bFinalDecision, string sNotes) {
            try {
                string[] aReceipients = sRequestedBy.Split(',');
                if (aReceipients.Length == 0)
                    return true;
                string sHeaderMsg = "";
                string sAlertMsg = "";
                string sUser = idh.bridge.DataHandler.INSTANCE.User;
                if (bFinalDecision) {
                    if (sDecisionType == "Approved") {
                        sHeaderMsg = Translation.getTranslatedWord("WOQ Approved") + ": " + sDocNum + " V." + sDocVersion;
                        sAlertMsg = Translation.getTranslatedWord("WOQ Approved:");
                        sAlertMsg += "\n";
                        sAlertMsg += sUser + " has approved WOQ " + sDocNum + ".\n";
                        if (sNotes != "")
                            sAlertMsg += "Review Notes are \n" + sNotes + "\n";
                        sAlertMsg += "From : " + idh.bridge.DataHandler.INSTANCE.User + "\n";
                        //sAlertMsg += "Cc: " + sAlertUsers;
                    } else if (sDecisionType == "Reject") {
                        sHeaderMsg = Translation.getTranslatedWord("WOQ Rejected") + ": " + sDocNum + " V." + sDocVersion;
                        sAlertMsg = Translation.getTranslatedWord("WOQ Rejected:");
                        sAlertMsg += "\n";
                        sAlertMsg += sUser + " has rejected WOQ " + sDocNum + ".\n";
                        if (sNotes != "")
                            sAlertMsg += "Review Notes are \n" + sNotes + "\n";
                        sAlertMsg += "From : " + idh.bridge.DataHandler.INSTANCE.User + "\n";

                    } else if (sDecisionType == "Review") {
                        sHeaderMsg = Translation.getTranslatedWord("WOQ Sent back for review") + ": " + sDocNum + " V." + sDocVersion;
                        sAlertMsg = Translation.getTranslatedWord("WOQ Sent back for review:");
                        sAlertMsg += "\n";
                        sAlertMsg += sUser + " has sent WOQ " + sDocNum + " back to review.\n";
                        if (sNotes != "")
                            sAlertMsg += "Review Notes are \n" + sNotes + "\n";
                        sAlertMsg += "From : " + idh.bridge.DataHandler.INSTANCE.User + "\n";

                    }
                } else {
                    if (sDecisionType == "Approved") {
                        sHeaderMsg = Translation.getTranslatedWord("WOQ Approved") + ": " + sDocNum + " V." + sDocVersion;
                        sAlertMsg = Translation.getTranslatedWord("WOQ Approved:");
                        sAlertMsg += "\n";
                        sAlertMsg += sUser + " has approved WOQ " + sDocNum + ". However still more approvals required for the document.\n";
                        if (sNotes != "")
                            sAlertMsg += "Review Notes are \n" + sNotes + "\n";
                        sAlertMsg += "From : " + idh.bridge.DataHandler.INSTANCE.User + "\n";
                        //sAlertMsg += "Cc: " + sAlertUsers;
                    } else if (sDecisionType == "Reject") {
                        sHeaderMsg = Translation.getTranslatedWord("WOQ Rejected") + ": " + sDocNum + " V." + sDocVersion;
                        sAlertMsg = Translation.getTranslatedWord("WOQ Rejected:");
                        sAlertMsg += "\n";
                        sAlertMsg += sUser + " has rejected WOQ " + sDocNum + ". However still more decision required for the document to approve/reject.\n";
                        if (sNotes != "")
                            sAlertMsg += "Review Notes are \n" + sNotes + "\n";
                        sAlertMsg += "From : " + idh.bridge.DataHandler.INSTANCE.User + "\n";

                    }
                }


                //base.IDHForm.goParent.doSendAlert(sAlertUsers.Split(','), sHeaderMsg + ": " + moWOQ.Code, sAlertMsg, BoLinkedObject.lf_BusinessPartner, "BP 163", true);

                SAPbobsCOM.Messages msg;
                msg = (SAPbobsCOM.Messages)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oMessages);
                msg.MessageText = sAlertMsg;
                msg.Subject = sHeaderMsg;
                bool bValidUsers = false;
                for (int i = 0; i <= aReceipients.Length - 1; i++) {
                    if (aReceipients[i].Trim() == "") {
                        continue;
                    }
                    bValidUsers = true;
                    msg.Recipients.Add();
                    msg.Recipients.SetCurrentLine(i);
                    msg.Recipients.UserCode = aReceipients[i];
                    msg.Recipients.NameTo = aReceipients[i];
                    msg.Recipients.SendInternal = SAPbobsCOM.BoYesNoEnum.tYES;
                    msg.Recipients.UserType = SAPbobsCOM.BoMsgRcpTypes.rt_InternalUser;
                    object sEMail = com.idh.bridge.lookups.Config.INSTANCE.getValueFromTablebyKey("OUSR", "E_Mail", "USER_CODE", aReceipients[i]);
                    if (sEMail != null && sEMail.ToString() != string.Empty) {
                        msg.Recipients.EmailAddress = sEMail.ToString();//Config.INSTANCE.getValueFromTablebyKey("OUSR", "E_Mail", "USER_CODE", sUserList[i]);
                        msg.Recipients.SendEmail = SAPbobsCOM.BoYesNoEnum.tYES;
                    }
                }
                if (bValidUsers == false)
                    return true;
                int iRet = msg.Add();
                if (iRet != 0) {
                    string sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("WOQ Approval request message Error - " + sResult + " Error sending the approval request.", "WRNBPA14",
                        new string[] { sResult });
                    return false;
                }
                return true;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSGEN", null);
                return false;
            }
        }

        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            if (pVal.BeforeAction == false && pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK ) {
                string scolid = pVal.ColUID;
                IDHGrid oGridN = IDHGrid.getInstance(oForm, "LINESGRID");

                string sWOQID = oGridN.doGetFieldValue("h." + IDH_APPROVLH._DocNum, pVal.Row).ToString();
                com.isb.forms.Enquiry.WOQuote oOOForm = new com.isb.forms.Enquiry.WOQuote(oForm.UniqueID, sWOQID);
                //com.isb.forms.Enquiry.WOQuote oOOForm = new com.isb.forms.Enquiry.WOQuote(null, oForm.UniqueID, null);
                oOOForm.WOQID = sWOQID;
                oOOForm.bLoadWOQ = true;
                oOOForm.bLoadReadOnly = true;
                //oOOForm.Handler_DialogOkReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQOKReturn);
                //oOOForm.Handler_DialogButton1Return = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQOKReturn);
                //oOOForm.Handler_DialogCancelReturn = new com.idh.forms.oo.Form.DialogReturn(doHandleWOQCancelReturn);
                oOOForm.doShowModal(oForm);
            } else if (pVal.BeforeAction == false && pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED && 
                (pVal.ColUID == ("d." + IDH_APPROVLD._Status) || pVal.ColUID == ("d." + IDH_APPROVLD._Status) )) {
                    doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE);
            }
            return base.doCustomItemEvent(oForm, ref pVal);
        }

        
        public override void doClose() {
        }

        private void doFillGridCombos(SAPbouiCOM.Form oForm, UpdateGrid oGridN) {
            SAPbouiCOM.ComboBoxColumn oCombo;

            int iIndex = oGridN.doIndexFieldWC("d."+ IDH_APPROVLD._Status);
            oCombo = (SAPbouiCOM.ComboBoxColumn)oGridN.getSBOGrid().Columns.Item(iIndex);
            oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
            Combobox2.doClearCombo(oCombo);
            Combobox2.doAddValueToCombo(oCombo, "1", getTranslatedWord( "Waiting"), true);
            Combobox2.doAddValueToCombo(oCombo, "2", getTranslatedWord("Approve"), true);
            Combobox2.doAddValueToCombo(oCombo, "3", getTranslatedWord("Reject"), true);
            Combobox2.doAddValueToCombo(oCombo, "4", getTranslatedWord("Review Advised"), true);
        }
         
    }
}