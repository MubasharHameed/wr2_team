﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using IDHAddOns.idh.controls;
using com.idh.bridge.data;
using com.idh.dbObjects;
using com.idh.bridge;
using com.idh.dbObjects.numbers;
using com.idh.bridge.lookups;

using com.isb.enq.dbObjects.User;
//using com.idh.dbObjects.User;

namespace com.isb.forms.Enquiry {
    public class LabPreProcess : IDHAddOns.idh.forms.Base {

        protected string msKeyGen = "GENSEQ";

        long iLabItemCd = 0;
        long iLabTaskCd = 0;
        //long iGoodsIssue = 0;
        //long iGoodsReceipt = 0;
        string sItemMaster = string.Empty;

        #region Initialization

        public LabPreProcess(IDHAddOns.idh.addon.Base oParent, string sParMenu, int iMenuPos)
            : base(oParent, "IDHLABPREPRO", sParMenu, iMenuPos, "Enq_LabPreProcess.srf", true, true, false, "Lab Pre-process Manager", load_Types.idh_LOAD_NORMAL) {
        }

        protected override void doSetEventFilters() {
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK);
            doAddEvent(SAPbouiCOM.BoEventTypes.et_VALIDATE);

        }

        public override void doCompleteCreate(ref SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            try {
                oForm.Title = gsTitle;
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE;

                doAddUF(oForm, "IDH_BATCH2", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 255);
                doAddUF(oForm, "IDH_CNTCD2", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 255);
                doAddUF(oForm, "IDH_WASCD2", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 255);
                doAddUF(oForm, "IDH_WASDS2", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 255);
                doAddUF(oForm, "IDH_BATYP2", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 255);
                doAddUF(oForm, "IDH_PRVBCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 255);
                doAddUF(oForm, "IDH_LBTSK", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 255);
                doAddUF(oForm, "IDH_ROTCD2", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 255);
                doAddUF(oForm, "IDH_SRCHDT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);

                //Warehouse Grid 
                SAPbouiCOM.Item oItem;
                oItem = oForm.Items.Item("IDH_RECWHS");
                UpdateGrid oWHSGrid = new UpdateGrid(this, oForm, "WHSGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane);
                oWHSGrid.getSBOItem().AffectsFormMode = true;

                //Batch Grid 
                oItem = oForm.Items.Item("IDH_RECBAT");
                UpdateGrid oBatchGridN = new UpdateGrid(this, oForm, "BATCHGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane);
                oBatchGridN.getSBOItem().AffectsFormMode = true;

                //Provisional  Grid 
                oItem = oForm.Items.Item("IDH_RECPRH");
                UpdateGrid oProvisionalHGrid = new UpdateGrid(this, oForm, "PROVHGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane);
                oProvisionalHGrid.getSBOItem().AffectsFormMode = true;
                oProvisionalHGrid.getSBOGrid().SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single;

                //Provisional Detail Grid 
                oItem = oForm.Items.Item("IDH_RECPRD");
                UpdateGrid oProvisionalDGrid = new UpdateGrid(this, oForm, "PROVDGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane);
                oProvisionalDGrid.getSBOItem().AffectsFormMode = true;

                //Provisional  Grid 
                oItem = oForm.Items.Item("IDH_RECFNL");
                UpdateGrid oFinalBatchHGrid = new UpdateGrid(this, oForm, "FNLVHGRID", oItem.Left, oItem.Top, oItem.Width, oItem.Height, oItem.FromPane, oItem.ToPane);
                oFinalBatchHGrid.getSBOItem().AffectsFormMode = false;
                //oFinalBatchHGrid.getSBOGrid().SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single;
                oFinalBatchHGrid.AddEditLine = false;

                base.doCompleteCreate(ref oForm, ref BubbleEvent);

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", null);
                BubbleEvent = false;
            }
        }

        #endregion

        #region LoadingDate
        #region Form Settings for Grids

        private void doGridLayout_Batch(UpdateGrid oGridN) {
            if (com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", false)) {
                string sFormTypeId = oGridN.getSBOForm().TypeEx;
              com.idh.dbObjects.User.IDH_FORMSET oFormSettings = (com.idh.dbObjects.User.IDH_FORMSET)getWFValue(oGridN.getSBOForm(), "FRMSET" + "." + sFormTypeId + "." + oGridN.GridId);
                if (oFormSettings == null) {
                    oFormSettings = new com.idh.dbObjects.User.IDH_FORMSET();
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, "");
                    setWFValue(oGridN.getSBOForm(), "FRMSET" + "." + sFormTypeId + "." + oGridN.GridId, oFormSettings);

                    oFormSettings.doAddFieldToGrid(oGridN);

                    doSetBatchesGridListFields(oGridN);
                    oFormSettings.doSyncDB(oGridN);
                } else {
                    if (oFormSettings.SkipFormSettings) {
                        doSetBatchesGridListFields(oGridN);
                    } else {
                        oFormSettings.doAddFieldToGrid(oGridN);
                    }
                }
            } else {
                doSetBatchesGridListFields(oGridN);
            }
        }

        private void doGridLayout_ProvisionalHeader(UpdateGrid oGridN) {
            if (com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", false)) {
                string sFormTypeId = oGridN.getSBOForm().TypeEx;
                com.idh.dbObjects.User.IDH_FORMSET oFormSettings = (com.idh.dbObjects.User.IDH_FORMSET)getWFValue(oGridN.getSBOForm(), "FRMSET" + "." + sFormTypeId + "." + oGridN.GridId);
                if (oFormSettings == null) {
                    oFormSettings = new com.idh.dbObjects.User.IDH_FORMSET();
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, "");
                    setWFValue(oGridN.getSBOForm(), "FRMSET" + "." + sFormTypeId + "." + oGridN.GridId, oFormSettings);

                    oFormSettings.doAddFieldToGrid(oGridN);

                    doSetProvisionalHGridListFields(oGridN);
                    oFormSettings.doSyncDB(oGridN);
                } else {
                    if (oFormSettings.SkipFormSettings) {
                        doSetProvisionalHGridListFields(oGridN);
                    } else {
                        oFormSettings.doAddFieldToGrid(oGridN);
                    }
                }
            } else {
                doSetProvisionalHGridListFields(oGridN);
            }
        }

        private void doGridLayout_ProvisionalDetail(UpdateGrid oGridN) {
            if (com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", false)) {
                string sFormTypeId = oGridN.getSBOForm().TypeEx;
                com.idh.dbObjects.User.IDH_FORMSET oFormSettings = (com.idh.dbObjects.User.IDH_FORMSET)getWFValue(oGridN.getSBOForm(), "FRMSET" + "." + sFormTypeId + "." + oGridN.GridId);
                if (oFormSettings == null) {
                    oFormSettings = new com.idh.dbObjects.User.IDH_FORMSET();
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, "");
                    setWFValue(oGridN.getSBOForm(), "FRMSET" + "." + sFormTypeId + "." + oGridN.GridId, oFormSettings);

                    oFormSettings.doAddFieldToGrid(oGridN);

                    doSetProvisionalDGridListFields(oGridN);
                    oFormSettings.doSyncDB(oGridN);
                } else {
                    if (oFormSettings.SkipFormSettings) {
                        doSetProvisionalDGridListFields(oGridN);
                    } else {
                        oFormSettings.doAddFieldToGrid(oGridN);
                    }
                }
            } else {
                doSetProvisionalDGridListFields(oGridN);
            }
        }

        private void doGridLayout_FinalBatch(UpdateGrid oGridN) {
            if (com.idh.bridge.lookups.Config.INSTANCE.getParameterAsBool("FORMSET", false)) {
                string sFormTypeId = oGridN.getSBOForm().TypeEx;
                com.idh.dbObjects.User.IDH_FORMSET oFormSettings = (com.idh.dbObjects.User.IDH_FORMSET)getWFValue(oGridN.getSBOForm(), "FRMSET" + "." + sFormTypeId + "." + oGridN.GridId);
                if (oFormSettings == null) {
                    oFormSettings = new com.idh.dbObjects.User.IDH_FORMSET();
                    oFormSettings.getFormGridFormSettings(sFormTypeId, oGridN.GridId, "");
                    setWFValue(oGridN.getSBOForm(), "FRMSET" + "." + sFormTypeId + "." + oGridN.GridId, oFormSettings);

                    oFormSettings.doAddFieldToGrid(oGridN);

                    doSetFinalHGridListFields(oGridN);
                    oFormSettings.doSyncDB(oGridN);
                } else {
                    if (oFormSettings.SkipFormSettings) {
                        doSetFinalHGridListFields(oGridN);
                    } else {
                        oFormSettings.doAddFieldToGrid(oGridN);
                    }
                }
            } else {
                doSetFinalHGridListFields(oGridN);
            }
        }

        #endregion
        public override void doBeforeLoadData(SAPbouiCOM.Form oForm) {
            doFillCombos(oForm);

            doManageWhsGrid(oForm);
            doManageBatchGrid(oForm);
            doManageProvisionalBatchHeadGrid(oForm);
            doManageProvisionalBatchDetailGrid(oForm);
            doSetBatchGridFilterFields(oForm);
            doManageFinalBatchHeadGrid(oForm);
        }

        protected override void doLoadData(SAPbouiCOM.Form oForm) {
            try {
                FilterGrid oWGridN = (FilterGrid)FilterGrid.getInstance(oForm, "WHSGRID");
                //oGridN.setRequiredFilter("W.WhsCode Like '%ECO000%'");
                oWGridN.doReloadData("", false, false);
                oWGridN.doApplyRules();

                FilterGrid oBatchGridN = (FilterGrid)FilterGrid.getInstance(oForm, "BATCHGRID");
                //oIGridN.setRequiredFilter("c.ItemCode = '-999'");
                string sReqFiler = oBatchGridN.getRequiredFilter();
                oBatchGridN.setRequiredFilter(sReqFiler + " And c.ItemCode = '-999' ");
                oBatchGridN.AddEditLine = false;
                oBatchGridN.doReloadData("", false, false);
                oBatchGridN.doApplyRules();
                oBatchGridN.setRequiredFilter(sReqFiler);
                if (oBatchGridN.getRowCount() == 1 && oBatchGridN.isBlankRow(oBatchGridN.getLastRowIndex())) {
                    oBatchGridN.doRemoveRow(oBatchGridN.getLastRowIndex(), false, true);
                }
                FilterGrid oProvHGridN = (FilterGrid)FilterGrid.getInstance(oForm, "PROVHGRID");
                string sOrgRequiredFilter = oProvHGridN.getRequiredFilter();
                oProvHGridN.setRequiredFilter("prvh.Code = '-999'");
                oBatchGridN.AddEditLine = false;
                oProvHGridN.doReloadData("", false, false);
                oProvHGridN.doApplyRules();
                oProvHGridN.setRequiredFilter(sOrgRequiredFilter);
                if (oProvHGridN.getRowCount() > 0)
                    oProvHGridN.doRemoveRow(oProvHGridN.getLastRowIndex(), false, true);

                //will do after provisional head grid
                FilterGrid oProvDGridN = (FilterGrid)FilterGrid.getInstance(oForm, "PROVDGRID");
                sOrgRequiredFilter = oProvDGridN.getRequiredFilter();
                oProvDGridN.setRequiredFilter("prvd.Code = '-999'");
                oProvDGridN.doReloadData("", false, false);
                oProvDGridN.doApplyRules();
                oProvDGridN.setRequiredFilter(sOrgRequiredFilter);
                if (oProvDGridN.getRowCount() > 0)
                    oProvDGridN.doRemoveRow(oProvDGridN.getLastRowIndex(), false, true);

                //Final Batach Grid
                FilterGrid oFinalHGridN = (FilterGrid)FilterGrid.getInstance(oForm, "FNLVHGRID");
                sOrgRequiredFilter = oFinalHGridN.getRequiredFilter();
                oFinalHGridN.setRequiredFilter("prvh.Code = '-999'");
                oFinalHGridN.AddEditLine = false;
                oFinalHGridN.doReloadData("", false, false);
                oFinalHGridN.doApplyRules();
                oFinalHGridN.setRequiredFilter(sOrgRequiredFilter);


                doFillProvisionalGridCombos(oForm);
                doFillFinalGridCombos(oForm);


                //setUFValue(oForm, "IDH_FNSTAT", "0");

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doLoadData()", "Error in Load Data: " + ex.Message);
            }
        }

        public override void doFinalizeShow(SAPbouiCOM.Form oForm) {
            base.doFinalizeShow(oForm);
            if (oForm.Width < 1700) {
                oForm.MaxWidth = 1700;
                oForm.Width = 1700;
            }
            if (oForm.Height < 558) {
                oForm.MaxHeight = 558;
                oForm.Height = 558;
            }

        }

        protected void doSetBatchGridFilterFields(SAPbouiCOM.Form oForm) {
            try {
                UpdateGrid oItemGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "BATCHGRID");
                if (oItemGridN == null)
                    oItemGridN = new UpdateGrid(this, oForm, "BATCHGRID");

                oItemGridN.doAddFilterField("IDH_WSTCD", "i.ItemCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250, "");
                oItemGridN.doAddFilterField("IDH_WSTNM", "i.ItemName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 250, "");
                oItemGridN.doAddFilterField("IDH_CNTCD", "c.ItemCode", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 50, "");
                oItemGridN.doAddFilterField("IDH_CNTNM", "c.ItemName", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");
                oItemGridN.doAddFilterField("IDH_BATCH", "batch.DistNumber", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");
                //oItemGridN.doAddFilterField("IDH_DRTCOD", "d." + IDH_DISPRTCD._ItemCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");
                oItemGridN.doAddFilterField("IDH_DRTCOD", "batch.LotNumber", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");

                oItemGridN.doAddFilterField("IDH_ITMGRP", "c.ItmsGrpCod", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 30, "");
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doSetBatchGridFilterFields()", "Error: " + ex.Message);
            }
        }

        protected void doSetWarehouseGridListFields(IDHAddOns.idh.controls.UpdateGrid oWhsGridN) {
            oWhsGridN.doAddListField("'Select'", "Select", true, -1, "CHECKBOX", null);
            oWhsGridN.doAddListField("W.WhsCode", "Whse Code", false, -1, null, null);
            oWhsGridN.doAddListField("W.WhsName", "Whse Name", false, -1, null, null);
        }

        protected void doSetBatchesGridListFields(IDHAddOns.idh.controls.UpdateGrid oItemGridN) {
            oItemGridN.doAddListField("'Select'", "Select", true, -1, "CHECKBOX", null);
            oItemGridN.doAddListField("batch.AbsEntry", "Batch#", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_ItemBatchNumbers);//
            oItemGridN.doAddListField("batch.DistNumber", "Batch Name", false, -1, null, null);//
            oItemGridN.doAddListField("c.ItemCode", "Container Code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            oItemGridN.doAddListField("c.ItemName", "Container Name", false, -1, null, null);
            //oItemGridN.doAddListField("d." + IDH_DISPRTCD._DisRCode, "Disposal Route Cd", false, -1, null, null);
            oItemGridN.doAddListField("batch.LotNumber", "Disposal Route Cd", false, -1, null, null);
            oItemGridN.doAddListField("i.ItemCode", "Waste Code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            oItemGridN.doAddListField("i.ItemName", "Waste Name", false, -1, null, null);
            oItemGridN.doAddListField("w.WhsCode", "Warehouse", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Warehouses);
            oItemGridN.doAddListField("w.WhsName", "WH Name", false, -1, null, null);
            oItemGridN.doAddListField("batch.SysNumber", "Batch Internal Code", false, 0, null, null);//SAPbouiCOM.BoLinkedObject.lf_ItemBatchNumbers);
            oItemGridN.doAddListField("batch.[Status]", "Batch Status", false, 0, null, null);

        }

        protected void doSetProvisionalHGridListFields(IDHAddOns.idh.controls.UpdateGrid oProvHGridN) {

            oProvHGridN.doAddListField("prvh." + IDH_PVHBTH._Code, "Prov. Batch ID", false, -1, null, null);
            oProvHGridN.doAddListField("prvh." + IDH_PVHBTH._Name, "Prov. Batch Name", false, 0, null, null);
            oProvHGridN.doAddListField("prvh." + IDH_PVHBTH._BatchTyp, "Batch Type", true, -1, "COMBOBOX", null);
            oProvHGridN.doAddListField("prvh." + IDH_PVHBTH._LabTskCd, "Lab Task", false, -1, null, null);
            oProvHGridN.doAddListField("labtask." + IDH_LABTASK._Decision, "Lab Decision", false, -1, null, null);

            string sWastGrp = Config.INSTANCE.doWasteMaterialGroup();
            string sItemCFL = "SRC*IDHWISRC(WST01)[IDH_ITMCOD;IDH_INVENT=;IDH_GRPCOD=" + sWastGrp + "][ITEMCODE;" + "prvh." + IDH_PVHBTH._WastDsc + "=ITEMNAME;" + "prvh." + IDH_PVHBTH._DisRCode + "=DISPRTCD]";
            oProvHGridN.doAddListField("prvh." + IDH_PVHBTH._WastCd, "Waste Code", true, -1, sItemCFL, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            sItemCFL = "SRC*IDHWISRC(WST01)[IDH_ITMNAM;IDH_ITMCOD=;IDH_GRPCOD=" + sWastGrp + "][ITEMNAME;" + "prvh." + IDH_PVHBTH._WastCd + "=ITEMCODE;" + "prvh." + IDH_PVHBTH._DisRCode + "=DISPRTCD]";
            oProvHGridN.doAddListField("prvh." + IDH_PVHBTH._WastDsc, "Waste Name", true, -1, sItemCFL, null);
            string sContGrp = Config.INSTANCE.getParameterWithDefault("WODCOG", "");

            string sContainerUID = "";// "SRC*IDHISRC(CONTCD)[IDH_ITMCOD;IDH_ITMNAM=;IDH_GRPCOD=" + moWOQ.U_ItemGrp + "][ITEMCODE;" + "prvh." + IDH_PVHBTH._ItemDsc + "=ITEMNAME]";
            sContainerUID = "SRC*IDHISRC(CONTCD)[IDH_ITMCOD;IDH_ITMNAM=;IDH_GRPCOD=" + sContGrp + ";IDH_ONLYFINISHED=Y;ManBtchNum=Y][ITEMCODE;" + "prvh." + IDH_PVHBTH._ItemDsc + "=ITEMNAME]";

            oProvHGridN.doAddListField("prvh." + IDH_PVHBTH._ItemCd, "Container Code", true, -1, sContainerUID, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            sContainerUID = "SRC*IDHISRC(CONTCD)[IDH_ITMNAM;IDH_ITMCOD=;IDH_GRPCOD=" + sContGrp + ";IDH_ONLYFINISHED=Y;ManBtchNum=Y][ITEMNAME;" + "prvh." + IDH_PVHBTH._ItemCd + "=ITEMCODE]";
            oProvHGridN.doAddListField("prvh." + IDH_PVHBTH._ItemDsc, "Container Name", true, -1, sContainerUID, null);
            oProvHGridN.doAddListField("prvh." + IDH_PVHBTH._CntnrQty, "Container Qty", true, -1, null, null);
            oProvHGridN.doAddListField("prvh." + IDH_PVHBTH._DisRCode, "Disposal Route Code", true, -1, null, null);
            oProvHGridN.doAddListField("prvh." + IDH_PVHBTH._WastQty, "Waste Qty", true, -1, null, null);
            oProvHGridN.doAddListField("prvh." + IDH_PVHBTH._Warehouse, "Warehouse", true, -1, "COMBOBOX", null, -1, SAPbouiCOM.BoLinkedObject.lf_Warehouses);
            //oProvHGridN.doAddListField("prvh." + IDH_PVHBTH._WastQty, "Waste Qty", false, -1, null, null);

        }
        protected void doSetProvisionalDGridListFields(IDHAddOns.idh.controls.UpdateGrid oProvDGridN) {

            oProvDGridN.doAddListField("prvd." + IDH_PVDBTH._Code, "Code", false, -1, null, null);
            oProvDGridN.doAddListField("prvd." + IDH_PVDBTH._Name, "Name", false, 0, null, null);
            oProvDGridN.doAddListField("prvd." + IDH_PVDBTH._PRVHCD, "Prov. Batch ID", false, -1, null, null);
            oProvDGridN.doAddListField("prvh." + IDH_PVHBTH._BatchTyp, "Batch Type", true, -1, null, null);
            oProvDGridN.doAddListField("prvd." + IDH_PVDBTH._ItemCd, "Container Code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            oProvDGridN.doAddListField("prvd." + IDH_PVDBTH._ItemDsc, "Container Name", false, -1, null, null);
            oProvDGridN.doAddListField("prvd." + IDH_PVDBTH._WastCd, "Waste Code", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            oProvDGridN.doAddListField("prvd." + IDH_PVDBTH._WastDsc, "Waste Name", false, -1, null, null);

            oProvDGridN.doAddListField("prvd." + IDH_PVDBTH._DisRCode, "Disposal Route Cd", false, -1, null, null);

            // oProvDGridN.doAddListField("batch.LotNumber", "Disposal Route Cd", false, -1, null, null);
            oProvDGridN.doAddListField("prvd." + IDH_PVDBTH._AbsEntry, "Batch#", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_ItemBatchNumbers);
            oProvDGridN.doAddListField("prvd." + IDH_PVDBTH._Batch, "Batch Name", false, -1, null, null);
            oProvDGridN.doAddListField("prvd." + IDH_PVDBTH._BatchSysNm, "Batch Internal Code", false, 0, null, null);//, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_ItemBatchNumbers);
            //oProvDGridN.doAddListField("batch.DistNumber", "Batch#", false, -1, null, null, -1, SAPbouiCOM.BoLinkedObject.lf_ItemBatchNumbers);
            oProvDGridN.doAddListField("prvd." + IDH_PVDBTH._WhsCode, "Warehouse", false, 0, null, null);

        }


        protected void doSetFinalHGridListFields(IDHAddOns.idh.controls.UpdateGrid oFinalHGridN) {

            oFinalHGridN.doAddListField("prvh." + IDH_PVHBTH._Code, "Batch ID", false, -1, null, null);
            oFinalHGridN.doAddListField("prvh." + IDH_PVHBTH._Name, "Batch Name", false, 0, null, null);
            oFinalHGridN.doAddListField("prvh." + IDH_PVHBTH._BatchTyp, "Batch Type", false, -1, "COMBOBOX", null);
            //oFinalHGridN.doAddListField("prvh." + IDH_PVHBTH._LabTskCd, "Lab Task", false, -1, null, null);
            //oFinalHGridN.doAddListField("labtask." + IDH_LABTASK._Decision, "Lab Decision", false, -1, null, null);

            string sWastGrp = Config.INSTANCE.doWasteMaterialGroup();
            string sItemCFL = "SRC*IDHWISRC(WST01)[IDH_ITMCOD;IDH_INVENT=;IDH_GRPCOD=" + sWastGrp + "][ITEMCODE;" + "prvh." + IDH_PVHBTH._WastDsc + "=ITEMNAME;" + "prvh." + IDH_PVHBTH._DisRCode + "=DISPRTCD]";
            oFinalHGridN.doAddListField("prvh." + IDH_PVHBTH._WastCd, "Waste Code", false, -1, sItemCFL, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            sItemCFL = "SRC*IDHWISRC(WST01)[IDH_ITMNAM;IDH_ITMCOD=;IDH_GRPCOD=" + sWastGrp + "][ITEMNAME;" + "prvh." + IDH_PVHBTH._WastCd + "=ITEMCODE;" + "prvh." + IDH_PVHBTH._DisRCode + "=DISPRTCD]";
            oFinalHGridN.doAddListField("prvh." + IDH_PVHBTH._WastDsc, "Waste Name", false, -1, sItemCFL, null);
            string sContGrp = Config.INSTANCE.getParameterWithDefault("WODCOG", "");

            string sContainerUID = "";// "SRC*IDHISRC(CONTCD)[IDH_ITMCOD;IDH_ITMNAM=;IDH_GRPCOD=" + moWOQ.U_ItemGrp + "][ITEMCODE;" + "prvh." + IDH_PVHBTH._ItemDsc + "=ITEMNAME]";
            //sContainerUID = "SRC*IDHISRC(CONTCD)[IDH_ITMCOD;IDH_ITMNAM=;IDH_GRPCOD=" + sContGrp + ";IDH_ONLYFINISHED=Y][ITEMCODE;" + "prvh." + IDH_PVHBTH._ItemDsc + "=ITEMNAME]";

            oFinalHGridN.doAddListField("prvh." + IDH_PVHBTH._ItemCd, "Container Code", false, -1, sContainerUID, null, -1, SAPbouiCOM.BoLinkedObject.lf_Items);
            //sContainerUID = "SRC*IDHISRC(CONTCD)[IDH_ITMNAM;IDH_ITMCOD=;IDH_GRPCOD=" + sContGrp + "][ITEMNAME;" + "prvh." + IDH_PVHBTH._ItemCd + "=ITEMCODE]";
            oFinalHGridN.doAddListField("prvh." + IDH_PVHBTH._ItemDsc, "Container Name", false, -1, null, null);
            oFinalHGridN.doAddListField("prvh." + IDH_PVHBTH._CntnrQty, "Container Qty", false, -1, null, null);
            //string sSQL = " isnull((Select v1.TotalAvailableQty from IDH_GetFinalBatchAvailableQty v1 Where v1.code=prvh." + IDH_PVHBTH._Code + "),0) ";
            //                "(Select Sum(OIBT.Quantity) As TotalAvailableQty "
            //+ "from [@IDH_PVHBTH] fb2 with(nolock) "
            //+ "Inner Join OWOR  with(nolock) on OWOR.U_PRVHCD = fb2.Code "
            //+ "Inner Join IGN1  with(nolock) on OWOR.DocEntry = IGN1.BaseEntry AND IGN1.BaseType = 202 "
            //+ "Inner Join OIGN  with(nolock) on OIGN.DocEntry = IGN1.DocEntry AND IGN1.ObjType = 59 And OIGN.DocType = 'I' "
            //+ "Inner Join dbo.OIBT  with(nolock) on OIBT.BaseEntry = OIGN.DocEntry and OIBT.BaseType = 59 "
            //+ "where "
            //+ "OIBT.Status = 0 "
            //+ "And OIBT.Quantity > 0 "
            //+ "and fb2.code = prvh." + IDH_PVHBTH._CntnrQty + " "
            //+ "group by OIBT.ItemCode, fb2.Code "
            //+ ")";
            oFinalHGridN.doAddListField("v1.TotalAvailableQty", "Available Qty", false, -1, null, null);
            oFinalHGridN.doAddListField("prvh." + IDH_PVHBTH._DisRCode, "Disposal Route Code", false, -1, null, null);
            oFinalHGridN.doAddListField("prvh." + IDH_PVHBTH._WastQty, "Waste Qty", false, -1, null, null);
            oFinalHGridN.doAddListField("prvh." + IDH_PVHBTH._Warehouse, "Warehouse", false, -1, "COMBOBOX", null, -1, SAPbouiCOM.BoLinkedObject.lf_Warehouses);
            //oProvHGridN.doAddListField("prvh." + IDH_PVHBTH._WastQty, "Waste Qty", false, -1, null, null);
            oFinalHGridN.doAddListField("prvh." + IDH_PVHBTH._WOHCode, "WOH Code", false, -1, null, null);
            oFinalHGridN.doAddListField("prvh." + IDH_PVHBTH._WORCode, "WOR Code", false, -1, null, null);
            oFinalHGridN.doAddListField("prvh." + IDH_PVHBTH._WORCntQty, "Committed Qty", false, -1, null, null);
            oFinalHGridN.doAddListField("prvh." + IDH_PVHBTH._Status, "Status", false, 0, null, null);

        }
        #endregion

        #region UnloadingData

        public override void doClose() {
        }

        public override void doCloseForm(SAPbouiCOM.Form oForm, ref bool BubbleEvent) {
            base.doCloseForm(oForm, ref BubbleEvent);
            FilterGrid.doRemoveGrid(oForm, "BATCHGRID");
        }

        #endregion

        #region EventHandlers

        public override void doButtonID1(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            if (pVal.BeforeAction) {
                return;
            }
            //UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "BATCHGRID");
            ////If oGridN.hasFieldChanged("U_Select") Then
            //Int32 _WOQID = getParentSharedDataAsInteger(oForm, "WOQID");
            //Int32 _EnqID = getParentSharedDataAsInteger(oForm, "ENQID");
            //com.idh.dbObjects.User.IDH_ENQUS moIDH_ENQUS = new com.idh.dbObjects.User.IDH_ENQUS();
            //for (Int16 iRow = 0; iRow <= oGridN.getRowCount() - 1; iRow++)
            //{

            //    if (!string.IsNullOrEmpty(oGridN.doGetFieldValue("Code", iRow).ToString()) && moIDH_ENQUS.getByKey(oGridN.doGetFieldValue("Code", iRow).ToString()))
            //    {
            //        moIDH_ENQUS.U_Answer = oGridN.doGetFieldValue("U_Answer", iRow).ToString();
            //    }
            //    else
            //    {
            //        moIDH_ENQUS.doAddEmptyRow(true, true, false);
            //        moIDH_ENQUS.U_Answer = oGridN.doGetFieldValue("U_Answer", iRow).ToString();
            //        if (_EnqID != 0)
            //            moIDH_ENQUS.U_EnqID = Convert.ToInt32(oGridN.doGetFieldValue("U_EnqID", iRow).ToString());
            //        moIDH_ENQUS.U_QsID = oGridN.doGetFieldValue("QID", iRow).ToString();
            //        moIDH_ENQUS.U_Question = oGridN.doGetFieldValue("Question", iRow).ToString();
            //        if (_WOQID != 0)
            //            moIDH_ENQUS.U_WOQID = Convert.ToInt32(oGridN.doGetFieldValue("U_WOQID", iRow).ToString());

            //    }
            //    moIDH_ENQUS.doProcessData();
            //    //End If
            //}
            doLoadData(oForm);

        }

        public override bool doItemEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent) {
            try {
                if (pVal.BeforeAction == true) {
                    if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED || pVal.EventType == SAPbouiCOM.BoEventTypes.et_CLICK || pVal.EventType == SAPbouiCOM.BoEventTypes.et_KEY_DOWN) {
                        if (pVal.ItemUID == "PROVHGRID" && (getButtonStatus(oForm, "IDH_FDPRVB") == Translation.getTranslatedWord("Update"))) {
                            UpdateGrid oProvisionalHGrid = (UpdateGrid)FilterGrid.getInstance(oForm, "PROVHGRID");
                            ArrayList aSelectedRows = oProvisionalHGrid.getSelectedRows();

                            if (aSelectedRows.Count > 0 && (int)aSelectedRows[0] != pVal.Row) {
                                BubbleEvent = false;
                                Console.WriteLine(pVal.EventType.ToString() + " : Before Action" + pVal.BeforeAction.ToString());
                                return false;
                            }
                        }//end of provisional Head grid
                    }//end of item press
                }//end of before action
                if (pVal.BeforeAction == false) {
                    if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED) {
                        if (pVal.ItemUID == "IDH_FNDOC") {
                            doHandleDocButton(oForm);
                            //  doHandleCloseFinalBatchButton(oForm);
                        } else if (pVal.ItemUID == "BTN_PGI") {
                            doHandlePendingGoodIssueButton(oForm);
                        } else if (pVal.ItemUID == "IDH_NEWPRV") {
                            doHandleNewProvisionalBatch(oForm);
                            //end of IDH_NEWPRV= Create new Provisional Batch
                        } else if (pVal.ItemUID == "IDH_ADDITM") {
                            doAddBatchToProvisionalBatch(oForm);
                        } else if (pVal.ItemUID == "IDH_SUBITM") {
                            doRemoveBatchFromProvisionalBatch(oForm);
                        } else if (pVal.ItemUID == "IDH_FDPRVB") {
                            doHandleFindNUpdateButton(oForm);
                        } else if (pVal.ItemUID == "IDH_FDFNLB") {
                            doHandleFinalGridFind(oForm, true);
                        } else if (pVal.ItemUID == "IDH_CANPRV") {
                            if (getButtonStatus(oForm, "IDH_FDPRVB") == Translation.getTranslatedWord("Update")) {
                                string sMsg = Translation.getTranslatedWord("You have some changes to save. Are you sure to cancel the changes you made?");
                                if (com.idh.bridge.resources.Messages.INSTANCE.doMessage(sMsg, 1, "Yes", "No") != 1)
                                    return true;
                                doHandleFindProvisinalGrid(oForm, true);
                            }

                        } else if (pVal.ItemUID == "IDH_NEWLAB") {
                            doHandleLabTaskButton(oForm);
                        } else if (pVal.ItemUID == "IDH_FIND") {
                            doHandleBatchSearch(oForm);
                        } else if (pVal.ItemUID == "IDH_NWFBAC") {
                            doHandleFinalBatchClick(oForm);
                        } else if (pVal.ItemUID == "IDH_CRTWO") {
                            doHandleWOsCreateClick(oForm);
                        }//end of item press
                    }
                }
                //return true;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXITME", null);
            }
            return base.doItemEvent(oForm, ref pVal, ref BubbleEvent);
        }

        public override bool doCustomItemEvent(SAPbouiCOM.Form oForm, ref IDHAddOns.idh.events.Base pVal) {
            try {
                //right click on Final Bathc to view the list of all woh/wor
                if (pVal.BeforeAction && pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK && pVal.ItemUID == "FNLVHGRID") {
                    FilterGrid oFinalGridN = FilterGrid.getInstance(oForm, "FNLVHGRID");

                    if (oFinalGridN.getSBOGrid().Rows.IsLeaf(pVal.Row)) {
                        ArrayList oSelected = oFinalGridN.getSelectedRows();
                        int iSelectionCount = oSelected.Count;
                        if (iSelectionCount == 1 && oFinalGridN.doGetFieldValue("prvh." + IDH_PVHBTH._WORCode, pVal.Row).ToString() != string.Empty) {
                            SAPbouiCOM.MenuItem oMenuItem;
                            string sRMenuId = IDHAddOns.idh.lookups.Base.RIGHTCLICKMENU;
                            oMenuItem = IDHAddOns.idh.addon.Base.APPLICATION.Menus.Item(sRMenuId);
                            if (IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("ViewFnBtWORLst"))
                                IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("ViewFnBtWORLst");

                            SAPbouiCOM.Menus oMenus;
                            int iMenuPos = 1;
                            SAPbouiCOM.MenuCreationParams oCreationPackage;
                            oCreationPackage = (SAPbouiCOM.MenuCreationParams)IDHAddOns.idh.addon.Base.APPLICATION.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);
                            oCreationPackage.Enabled = true;
                            oMenus = oMenuItem.SubMenus;
                            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;

                            oCreationPackage.UniqueID = "ViewFnBtWORLst";
                            oCreationPackage.String = Translation.getTranslatedWord("View List of WOH(s)/WOR(s)");
                            oCreationPackage.Position = iMenuPos;
                            iMenuPos += 1;
                            oMenus.AddEx(oCreationPackage);
                        } else {
                            if (IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("ViewFnBtWORLst"))
                                IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("ViewFnBtWORLst");
                            //setWFValue("LASTJOBMENU", null);
                        }
                        //GMADD
                        //GMREML
                    } else {
                        if (IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("ViewFnBtWORLst"))
                            IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("ViewFnBtWORLst");
                        //setWFValue("LASTJOBMENU", null);
                    }
                } else if (!pVal.BeforeAction && pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_MENU_EVENT && pVal.ItemUID == "FNLVHGRID") {
                    SAPbouiCOM.MenuEventClass oData = (SAPbouiCOM.MenuEventClass)pVal.oData;
                    if (oData.MenuUID == "ViewFnBtWORLst") {
                        if (IDHAddOns.idh.addon.Base.APPLICATION.Menus.Exists("ViewFnBtWORLst"))
                            IDHAddOns.idh.addon.Base.APPLICATION.Menus.RemoveEx("ViewFnBtWORLst");

                        FilterGrid oFinalGridN = FilterGrid.getInstance(oForm, "FNLVHGRID");
                        string sBatchCode = oFinalGridN.doGetFieldValue("prvh." + IDH_PVHBTH._Code, pVal.Row).ToString();
                        if (sBatchCode != string.Empty) {
                            setParentSharedData(oForm, "FINALBATCHCODE", sBatchCode);
                            goParent.doOpenForm("IDHFNBATHWORS", oForm, false);
                        }
                        //return false;
                    }
                } else if (!pVal.BeforeAction && pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_ROW_SELECT && pVal.ItemUID == "PROVHGRID"
                       && (getButtonStatus(oForm, "IDH_FDPRVB") == Translation.getTranslatedWord("Find"))) {

                    FilterGrid oHGridN = FilterGrid.getInstance(oForm, "PROVHGRID");
                    ArrayList oSelectedRows = oHGridN.getSelectedRows();
                    FilterGrid oDGridN = FilterGrid.getInstance(oForm, "PROVDGRID");
                    if (oSelectedRows.Count > 0) {
                        string sOrgReqFilter = oDGridN.getRequiredFilter();
                        oDGridN.setRequiredFilter(sOrgReqFilter + " And prvh." + IDH_PVHBTH._Code + "=" + oHGridN.doGetFieldValue("prvh." + IDH_PVHBTH._Code).ToString());
                        oDGridN.doReloadData("", false, false);
                        oDGridN.doApplyRules();
                        oDGridN.setRequiredFilter(sOrgReqFilter);
                    } else {
                        string sOrgReqFilter = oDGridN.getRequiredFilter();
                        oDGridN.setRequiredFilter("prvd.Code = '-999'");
                        oDGridN.doReloadData("", false, false);
                        oDGridN.doApplyRules();
                        oDGridN.setRequiredFilter(sOrgReqFilter);
                    }
                } else if (!pVal.BeforeAction && pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_FIELD_CHANGED && pVal.ItemUID == "PROVHGRID") {

                    if (getButtonStatus(oForm, "IDH_FDPRVB") == Translation.getTranslatedWord("Find"))
                        setButtonStatus(oForm, "IDH_FDPRVB", "Update");
                    if (false && pVal.ColUID == "prvh." + IDH_PVHBTH._DisRCode) {
                        FilterGrid oHGridN = FilterGrid.getInstance(oForm, "PROVHGRID");
                        if (oHGridN.doGetFieldValue("prvh." + IDH_PVHBTH._DisRCode, pVal.Row).ToString() != string.Empty) {
                            IDH_DISPRTCD oDispRtCode = new IDH_DISPRTCD();
                            int iRet = oDispRtCode.getData(" " + IDH_DISPRTCD._Active + "= \'Y\' And " + IDH_DISPRTCD._DisRCode + "=\'" + oHGridN.doGetFieldValue("prvh." + IDH_PVHBTH._DisRCode, pVal.Row).ToString() + "\' ", "");
                            if (iRet > 0) {
                                oHGridN.doSetFieldValue("prvh." + IDH_PVHBTH._Warehouse, pVal.Row, oDispRtCode.U_WhsCode);
                            }
                        }
                    } else if (pVal.ColUID == "prvh." + IDH_PVHBTH._WastCd) {
                        FilterGrid oHGridN = FilterGrid.getInstance(oForm, "PROVHGRID");
                        if (oHGridN.doGetFieldValue("prvh." + IDH_PVHBTH._WastCd, pVal.Row).ToString() == string.Empty) {
                            oHGridN.doSetFieldValue("prvh." + IDH_PVHBTH._WastDsc, pVal.Row, "");
                            oHGridN.doSetFieldValue("prvh." + IDH_PVHBTH._DisRCode, pVal.Row, "");

                        }
                    } else if (pVal.ColUID == "prvh." + IDH_PVHBTH._WastDsc) {
                        FilterGrid oHGridN = FilterGrid.getInstance(oForm, "PROVHGRID");
                        if (oHGridN.doGetFieldValue("prvh." + IDH_PVHBTH._WastDsc, pVal.Row).ToString() == string.Empty) {
                            oHGridN.doSetFieldValue("prvh." + IDH_PVHBTH._WastCd, pVal.Row, "");
                            oHGridN.doSetFieldValue("prvh." + IDH_PVHBTH._DisRCode, pVal.Row, "");
                        }
                    } else if (pVal.ColUID == "prvh." + IDH_PVHBTH._ItemCd || pVal.ColUID == "prvh." + IDH_PVHBTH._ItemDsc) {
                        FilterGrid oHGridN = FilterGrid.getInstance(oForm, "PROVHGRID");
                        if (oHGridN.doGetFieldValue("prvh." + IDH_PVHBTH._ItemCd, pVal.Row).ToString() != string.Empty &&
                            oHGridN.doGetFieldValue("prvh." + IDH_PVHBTH._ItemDsc, pVal.Row).ToString() != string.Empty &&
                            oHGridN.doGetFieldValue("prvh." + IDH_PVHBTH._ItemCd, pVal.Row).ToString().IndexOf("*") == -1 &&
                            oHGridN.doGetFieldValue("prvh." + IDH_PVHBTH._ItemDsc, pVal.Row).ToString().IndexOf("*") == -1) {
                            IDH_DISPRTCD oDispRtCode = new IDH_DISPRTCD();
                            string sLinkedItemCode = Config.INSTANCE.getValueFromOITM(oHGridN.doGetFieldValue("prvh." + IDH_PVHBTH._ItemCd, pVal.Row).ToString().Trim(), idh.dbObjects.User.OITM._WTITMLN).ToString();
                            if (string.IsNullOrWhiteSpace(sLinkedItemCode)) {
                                com.idh.bridge.DataHandler.INSTANCE.doUserError(Translation.getTranslatedWord("Container does not have a linked non-stock item required for outgoing WOR."));
                            } else {
                                int iRet = oDispRtCode.getData(IDH_DISPRTCD._WasCd + "=\'" + com.idh.utils.Formatter.doFixForSQL(oHGridN.doGetFieldValue("prvh." + IDH_PVHBTH._WastCd, pVal.Row).ToString()) + "\' And " + IDH_DISPRTCD._ItemCd + "=\'" + com.idh.utils.Formatter.doFixForSQL(sLinkedItemCode) + "\' And " + IDH_DISPRTCD._Active + "=\'Y\'", "");
                                //int iRet = oDispRtCode.getData(" " + IDH_DISPRTCD._Active + "= \'Y\' And " + IDH_DISPRTCD._DisRCode + "=\'" + oHGridN.doGetFieldValue("prvh." + IDH_PVHBTH._DisRCode, pVal.Row).ToString() + "\' ", "");
                                if (iRet > 0) {
                                    oHGridN.doSetFieldValue("prvh." + IDH_PVHBTH._DisRCode, pVal.Row, oDispRtCode.U_DisRCode);
                                    oHGridN.doSetFieldValue("prvh." + IDH_PVHBTH._Warehouse, pVal.Row, oDispRtCode.U_WhsCode);

                                }
                            }
                        }
                    }

                } else if (!pVal.BeforeAction && pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK && pVal.ItemUID == "PROVHGRID") {
                    FilterGrid oHGridN = FilterGrid.getInstance(oForm, "PROVHGRID");
                    if (oHGridN.getRowCount() > 0 && oHGridN.doGetFieldValue("prvh." + IDH_PVHBTH._LabTskCd, pVal.Row).ToString() != string.Empty) {
                        setSharedData(oForm, "LABTASK", oHGridN.doGetFieldValue("prvh." + IDH_PVHBTH._LabTskCd, pVal.Row).ToString());
                        goParent.doOpenModalForm("IDHLABTODO", oForm);
                    }
                } else if (!pVal.BeforeAction && pVal.EventType == IDHAddOns.idh.events.Base.ev_Types.idh_GRID_DOUBLE_CLICK && pVal.ItemUID == "FNLVHGRID") {
                    FilterGrid oFinalGridN = FilterGrid.getInstance(oForm, "FNLVHGRID");
                    if (oFinalGridN.getRowCount() > 0) {//&& pVal.ColUID= && oHGridN.doGetFieldValue("prvh." + IDH_PVHBTH._LabTskCd, pVal.Row).ToString() != string.Empty) {
                        if (pVal.ColUID == "prvh." + IDH_PVHBTH._WOHCode && oFinalGridN.doGetFieldValue("prvh." + IDH_PVHBTH._WOHCode, pVal.Row).ToString() != string.Empty) {
                            //open WOH Form 
                            ArrayList oData = new ArrayList();
                            oData.Add("DoUpdate");
                            oData.Add(oFinalGridN.doGetFieldValue("prvh." + IDH_PVHBTH._WOHCode, pVal.Row).ToString().Trim());
                            goParent.doOpenModalForm("IDH_WASTORD", oForm, oData);
                        } else if (pVal.ColUID == "prvh." + IDH_PVHBTH._WORCode && oFinalGridN.doGetFieldValue("prvh." + IDH_PVHBTH._WORCode, pVal.Row).ToString() != string.Empty) {
                            //open WOR Form 
                            ArrayList oData = new ArrayList();
                            oData.Add("DoUpdate");
                            oData.Add(oFinalGridN.doGetFieldValue("prvh." + IDH_PVHBTH._WORCode, pVal.Row).ToString().Trim());
                            setSharedData(oForm, "ROWCODE", oFinalGridN.doGetFieldValue("prvh." + IDH_PVHBTH._WORCode, pVal.Row).ToString().Trim());
                            goParent.doOpenModalForm("IDHJOBS", oForm, oData);
                        }
                    }
                }

                /*
                 if (pVal.BeforeAction && pVal.ColUID == IDH_WOQITEM._WOHID && moGrid.doGetFieldValue(IDH_WOQITEM._WOHID, pVal.Row).ToString() != string.Empty) {
                    //open WOH Form 
                    ArrayList oData = new ArrayList();
                    oData.Add("DoUpdate");
                    oData.Add(moGrid.doGetFieldValue(IDH_WOQITEM._WOHID, pVal.Row).ToString().Trim());
                    doOpenModalForm("IDH_WASTORD", oData);
                } else if (pVal.BeforeAction && pVal.ColUID == IDH_WOQITEM._WORID && moGrid.doGetFieldValue(IDH_WOQITEM._WORID, pVal.Row).ToString() != string.Empty) {
                    //Dim sRow As String = getDFValue(oForm, "OCLG", "U_IDHWOR")
                        //If sRow.Length > 0 Then
                           ArrayList oData =new ArrayList();
                            oData.Add("DoUpdate");
                              oData.Add(moGrid.doGetFieldValue(IDH_WOQITEM._WORID, pVal.Row).ToString().Trim());
                            setSharedData("ROWCODE", moGrid.doGetFieldValue(IDH_WOQITEM._WORID, pVal.Row).ToString().Trim());
                            doOpenModalForm("IDHJOBS", oData);
                }
                 */


            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXITME", null);
            }
            return base.doCustomItemEvent(oForm, ref pVal);
        }

        protected override void doHandleModalBufferedResult(SAPbouiCOM.Form oParentForm, ref object oData, string sModalFormType, string sLastButton = null) {
            if (sModalFormType == "IDHJOBS" || sModalFormType == "IDH_WASTORD") {
                FilterGrid oFinalHGrid = FilterGrid.getInstance(oParentForm, "FNLVHGRID");
                oFinalHGrid.doReloadData("", false, false);
                oFinalHGrid.doApplyRules();
                oFinalHGrid.doRecount();
            }
        }

        private void doHandlePendingGoodIssueButton(SAPbouiCOM.Form oForm) {
            try {
                string sReportName = Config.INSTANCE.getParameter("LABPDGGI");

                if (sReportName != null && sReportName != string.Empty && sReportName.ToLower().EndsWith(".rpt")) {
                    Hashtable oParams = new Hashtable();
                    //oParams.Add("OrdNum", sCode)
                    //oParams.Add("RowNum", moWOQ.Code);
                    string sCallerForm = oForm.TypeEx;
                    IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportName, "TRUE", "FALSE", "1", ref oParams, ref sCallerForm);
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXITME", null);
            }
        }

        protected void doHandleDocButton(SAPbouiCOM.Form oForm) {

            FilterGrid oFinalHGrid = FilterGrid.getInstance(oForm, "FNLVHGRID");
            ArrayList aRows = oFinalHGrid.getSelectedRows();
            if (aRows.Count != 1) {
                com.idh.bridge.DataHandler.INSTANCE.doUserError("Please select a row.");
                return;
            }
            string sBatchCode = "";
            int i = (int)aRows[0];
            sBatchCode = oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, i).ToString();
            string sReportName = Config.INSTANCE.getParameterWithDefault("FNBTDOC", "");
            if (sReportName.ToLower().EndsWith(".rpt")) {
                Hashtable oParams = new Hashtable();
                //oParams.Add("OrdNum", sCode)
                oParams.Add("RowNum", sBatchCode);
                string sCallerForm = oForm.TypeEx;
                IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportName, "TRUE", "FALSE", "1", ref oParams, ref sCallerForm);
            } else if (sReportName != string.Empty) {
                setSharedData(oForm, "WOQDOCCODES", sReportName);
                setSharedData(oForm, "ROWNUM", sBatchCode);
                goParent.doOpenModalForm("IDHWOQRPTS", oForm);
            }
            return;
        }

        private void doHandleCloseFinalBatchButton(SAPbouiCOM.Form oForm) {
            try {
                FilterGrid oFinalHGrid = FilterGrid.getInstance(oForm, "FNLVHGRID");
                ArrayList aRows = oFinalHGrid.getSelectedRows();
                if (aRows.Count == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("Please select rows.", "ERUSJOBS", null);
                    return;
                }
                string sBatchCode = "";
                double dRemainingQty = 0;
                for (int isel = aRows.Count - 1; isel >= 0; isel--) {
                    int i = -1;
                    i = (int)aRows[isel];
                    sBatchCode = oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, i).ToString();
                    dRemainingQty = Convert.ToDouble(oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._CntnrQty, i)) - Convert.ToDouble(oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WORCntQty, i));
                    if (dRemainingQty > 0) {
                        int iRet = com.idh.bridge.resources.Messages.INSTANCE.doResourceMessage("LAB00003", new string[] { sBatchCode, dRemainingQty.ToString() }, 2, "Yes", "No");
                        if (iRet != 1)
                            return;
                    }
                }
                IDH_PVHBTH oFinalBatch = new IDH_PVHBTH();
                for (int isel = aRows.Count - 1; isel >= 0; isel--) {
                    int i = -1;
                    i = (int)aRows[isel];
                    sBatchCode = oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, i).ToString();
                    dRemainingQty = Convert.ToDouble(oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._CntnrQty, i)) - Convert.ToDouble(oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WORCntQty, i));
                    oFinalBatch.UnLockTable = true;
                    bool bRet = oFinalBatch.getByKey(sBatchCode);
                    if (bRet) {
                        oFinalBatch.U_Status = 1;
                        oFinalBatch.doProcessData();
                    }
                }
                oFinalHGrid.doReloadData("", false, false);
                oFinalHGrid.doApplyRules();
                oFinalHGrid.doRecount();
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXITME", null);
            }
        }
        #endregion
        #region "Right Click Menue"
        public override bool doRightClickEvent(SAPbouiCOM.Form oForm, ref SAPbouiCOM.ContextMenuInfo pVal, ref bool BubbleEvent) {

            return base.doRightClickEvent(oForm, ref pVal, ref BubbleEvent);
        }
        #endregion

        #region CustomFunctions

        protected void doFillProvisionalGridCombos(SAPbouiCOM.Form oForm) {
            try {
                UpdateGrid oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "PROVHGRID");
                SAPbouiCOM.ComboBoxColumn oCombo;

                oCombo = (SAPbouiCOM.ComboBoxColumn)oGridN.getSBOGrid().Columns.Item(oGridN.doIndexField("prvh." + IDH_PVHBTH._BatchTyp));
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                com.idh.bridge.utils.Combobox2.doClearCombo(oCombo);
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
                com.idh.bridge.utils.Combobox2.doFillCombo(oCombo, "[@IDH_LABSTATS]", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Type = 'BatchType'", "[@IDH_LABSTATS].U_Status");

                oCombo = (SAPbouiCOM.ComboBoxColumn)oGridN.getSBOGrid().Columns.Item(oGridN.doIndexField("prvh." + IDH_PVHBTH._Warehouse));
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                com.idh.bridge.utils.Combobox2.doClearCombo(oCombo);
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
                doFillCombo(oForm, oCombo, "OWHS", "WhsCode", "WhsName", null, null, "", "");

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doFillProvisionalGridCombos()", "Error updating Grid combo: " + ex.Message);
            }
        }
        protected void doFillFinalGridCombos(SAPbouiCOM.Form oForm) {
            try {
                UpdateGrid oGridN;//= (UpdateGrid)UpdateGrid.getInstance(oForm, "PROVHGRID");
                SAPbouiCOM.ComboBoxColumn oCombo;

                oGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "FNLVHGRID");
                oCombo = (SAPbouiCOM.ComboBoxColumn)oGridN.getSBOGrid().Columns.Item(oGridN.doIndexField("prvh." + IDH_PVHBTH._BatchTyp));
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                com.idh.bridge.utils.Combobox2.doClearCombo(oCombo);
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
                com.idh.bridge.utils.Combobox2.doFillCombo(oCombo, "[@IDH_LABSTATS]", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Type = 'BatchType'", "[@IDH_LABSTATS].U_Status");

                oCombo = (SAPbouiCOM.ComboBoxColumn)oGridN.getSBOGrid().Columns.Item(oGridN.doIndexField("prvh." + IDH_PVHBTH._Warehouse));
                oCombo.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
                com.idh.bridge.utils.Combobox2.doClearCombo(oCombo);
                doFillCombo(oForm, oCombo, "OWHS", "WhsCode", "WhsName", null, null, "", "");


            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doFillFinalGridCombos()", "Error updating Grid combo: " + ex.Message);
            }
        }
        protected void doFillCombos(SAPbouiCOM.Form oForm) {
            try {
                SAPbouiCOM.Item oItem; // = oForm.Items.Item("IDH_Date");
                //Date Combo 
                SAPbouiCOM.ComboBox oCombo; // = (SAPbouiCOM.ComboBox)oItem.Specific;


                oItem = oForm.Items.Item("IDH_BATYP2");
                oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
                com.idh.bridge.utils.Combobox2.doClearCombo(oCombo);
                com.idh.bridge.utils.Combobox2.doFillCombo(oCombo, "[@IDH_LABSTATS]", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Type = 'BatchType'", "[@IDH_LABSTATS].U_Status", "");


                oItem = oForm.Items.Item("IDH_BATYP3");
                oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
                com.idh.bridge.utils.Combobox2.doClearCombo(oCombo);
                com.idh.bridge.utils.Combobox2.doFillCombo(oCombo, "[@IDH_LABSTATS]", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Status", "[@IDH_LABSTATS].U_Type = 'BatchType'", "[@IDH_LABSTATS].U_Status", "");


                //oCombo.ValidValues.Add("PD", "Posting Date");
                //oCombo.ValidValues.Add("SD", "System Date");
                //oCombo.Select("PD", SAPbouiCOM.BoSearchKey.psk_ByValue);

                //Item Group Combo 
                oItem = oForm.Items.Item("IDH_ITMGRP");
                oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
                doFillCombo(oForm, oCombo, "OITB", "ItmsGrpCod", "ItmsGrpNam", null, null, "", "");
                //doFillCombo(oForm, oComboBox, "[@IDH_BASEL]", "U_BASELCd", "U_Desc", Nothing, Nothing, "N/A", "")

                //Final Batch Status
                oItem = oForm.Items.Item("IDH_FNSTAT");
                oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
                SAPbouiCOM.ValidValues oValidValues;
                oValidValues = oCombo.ValidValues;
                doClearValidValues(oValidValues);
                oValidValues.Add("", com.idh.bridge.lookups.FixedValues.getStatusAll());
                oValidValues.Add("0", com.idh.bridge.lookups.FixedValues.getStatusOpen());
                oValidValues.Add("1", com.idh.bridge.lookups.FixedValues.getStatusClosed());
                oCombo.SelectExclusive("0", SAPbouiCOM.BoSearchKey.psk_ByValue);
                //New UOM dropod
                //oItem = oForm.Items.Item("IDHNEWUOM");
                //oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
                //doFillCombo(oForm, oCombo, "OWGT", "UnitDisply", "UnitName", null, null, "", "");

                ////New Warehouse dropod
                //oItem = oForm.Items.Item("IDHNEWWHS");
                //oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
                //doFillCombo(oForm, oCombo, "OWHS", "WhsCode", "WhsName", null, null, "", "");
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doFillCombos()", "Error updating combo: " + ex.Message);
            }
        }

        protected void doManageWhsGrid(SAPbouiCOM.Form oForm) {
            try {
                UpdateGrid oWhsGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "WHSGRID");
                if (oWhsGridN == null)
                    oWhsGridN = new UpdateGrid(this, oForm, "WHSGRID");

                oWhsGridN.doAddGridTable(new GridTable("OWHS", "W", "WhsCode", true), true);
                oWhsGridN.setOrderValue("W.WhsCode");
                oWhsGridN.doSetDoCount(true);
                doSetWarehouseGridListFields(oWhsGridN);
                oWhsGridN.doApplyRules();
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doManageWhsGrid()", "Error Managing Warehouse Grid: " + ex.Message);
            }
        }

        protected void doManageBatchGrid(SAPbouiCOM.Form oForm) {
            try {
                UpdateGrid oItemGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "BATCHGRID");
                if (oItemGridN == null)
                    oItemGridN = new UpdateGrid(this, oForm, "BATCHGRID");

                oItemGridN.doAddGridTable(new GridTable("OITM", "i", "ItemCode", true, true), true);
                oItemGridN.doAddGridTable(new GridTable("OITM", "c", "ItemCode", true, true), true);
                //oItemGridN.doAddGridTable(new GridTable("OITM", "c_linked_NP", "ItemCode", true, true), true);
                //oItemGridN.doAddGridTable(new GridTable(IDH_DISPRTCD.TableName, "d", IDH_DISPRTCD._DisProcCd, true, true), true);
                oItemGridN.doAddGridTable(new GridTable("OWHS", "w", "WhsCode", true, true), true);
                oItemGridN.doAddGridTable(new GridTable("OBTW", "bw", "WhsCode", true, true), true);
                oItemGridN.doAddGridTable(new GridTable("OBTN", "batch", "SysNumber", true, true), true);
                //remove flag of outgoing container SWW on oitm so we can get final batches as well
                ////oItemGridN.setRequiredFilter(" c.ItemCode=batch.ItemCode and isnull(c.SWW,'')<>'Y' and batch.[Status]=0 And batch.MnfSerial=i.ItemCode and (Select sum(Quantity) from OBTQ With(nolock) where OBTQ.ItemCode=batch.ItemCode and OBTQ.WhsCode=bw.WhsCode and OBTQ.SysNumber=batch.SysNumber) >0 And bw.ItemCode=batch.ItemCode and bw.SysNumber=batch.SysNumber And w.WhsCode=bw.WhsCode  and d.U_WasCd=i.ItemCode and d." + IDH_DISPRTCD._DisRCode + "=batch.LotNumber ");
                //oItemGridN.setRequiredFilter(" c.ItemCode=batch.ItemCode and c_linked_NP.ItemCode=c.U_WTITMLN and batch.[Status]=0 And batch.MnfSerial=i.ItemCode and (Select sum(Quantity) from OBTQ With(nolock) where OBTQ.ItemCode=batch.ItemCode and OBTQ.WhsCode=bw.WhsCode and OBTQ.SysNumber=batch.SysNumber) >0 And bw.ItemCode=batch.ItemCode and bw.SysNumber=batch.SysNumber And w.WhsCode=bw.WhsCode  and d.U_WasCd=i.ItemCode and d." + IDH_DISPRTCD._DisRCode + "=batch.LotNumber and d.U_ItemCd=c_linked_NP.ItemCode ");
                oItemGridN.setRequiredFilter(" c.ItemCode=batch.ItemCode and batch.[Status]=0 And batch.MnfSerial=i.ItemCode and (Select sum(Quantity) from OBTQ With(nolock) where OBTQ.ItemCode=batch.ItemCode and OBTQ.WhsCode=bw.WhsCode and OBTQ.SysNumber=batch.SysNumber) >0 And bw.ItemCode=batch.ItemCode and bw.SysNumber=batch.SysNumber And w.WhsCode=bw.WhsCode ");
                oItemGridN.setOrderValue("c.ItemCode");
                oItemGridN.AddEditLine = false;
                oItemGridN.doSetDoCount(true);
                doGridLayout_Batch(oItemGridN);
                //doSetBatchesGridListFields(oItemGridN);
                oItemGridN.doApplyRules();
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doManageBatchGrid()", "Error Managing Batch Grid: " + ex.Message);
            }
        }

        protected void doManageProvisionalBatchHeadGrid(SAPbouiCOM.Form oForm) {
            try {
                UpdateGrid oProvHGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "PROVHGRID");
                if (oProvHGridN == null)
                    oProvHGridN = new UpdateGrid(this, oForm, "PROVHGRID");

                oProvHGridN.doAddGridTable(new GridTable(IDH_PVHBTH.TableName, "prvh", "Code", true, true), true);
                GridJoin oLabTaskJoin = new GridJoin("[" + IDH_LABTASK.TableName + "]", GridJoin.TYPE_JOIN.LEFT, "labtask", IDH_LABTASK._Code, "prvh." + IDH_PVHBTH._LabTskCd + " = labtask." + IDH_LABTASK._Code, true);

                oProvHGridN.doAddGridJoin(oLabTaskJoin);
                oProvHGridN.setRequiredFilter(" " + IDH_PVHBTH._FinalBt + "<>\'Y\' ");
                oProvHGridN.setOrderValue(" prvh." + IDH_PVHBTH._Code + " For Browse ");
                oProvHGridN.AddEditLine = false;
                oProvHGridN.doSetDoCount(true);
                //doSetProvisionalHGridListFields(oProvHGridN);
                doGridLayout_ProvisionalHeader(oProvHGridN);
                oProvHGridN.doApplyRules();

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doManageProvisionalBatchHeadGrid()", "Error Managing Provisional Grid: " + ex.Message);
            }
        }

        protected void doManageProvisionalBatchDetailGrid(SAPbouiCOM.Form oForm) {
            try {
                UpdateGrid oProvDGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "PROVDGRID");
                if (oProvDGridN == null)
                    oProvDGridN = new UpdateGrid(this, oForm, "PROVDGRID");

                oProvDGridN.doAddGridTable(new GridTable(IDH_PVDBTH.TableName, "prvd", "Code", true, true), true);
                oProvDGridN.doAddGridTable(new GridTable(IDH_PVHBTH.TableName, "prvh", "Code", false, true), false);
                //oProvDGridN.doAddGridTable(new GridTable("OITM", "c", "ItemCode", true, true), true);
                //oProvDGridN.doAddGridTable(new GridTable("OITM", "i", "WhsCode", true, true), true);
                //oProvDGridN.doAddGridTable(new GridTable("OBTN", "batch", "SysNumber", true, true), true);

                oProvDGridN.setRequiredFilter(" " + IDH_PVHBTH._FinalBt + "<>\'Y\' AND  prvd." + IDH_PVDBTH._PRVHCD + "=prvh." + IDH_PVHBTH._Code + " ");
                //oProvDGridN.setRequiredFilter(" " + IDH_PVHBTH._FinalBt + "<>\'Y\' AND c.ItemCode=batch.ItemCode  And batch.MnfSerial=i.ItemCode And bw.ItemCode=batch.ItemCode and bw.SysNumber=batch.SysNumber And w.WhsCode=bw.WhsCode  and d.U_WasCd=i.ItemCode and d." + IDH_DISPRTCD._DisRCode + "=batch.LotNumber And " + IDH_PVHBTH._Code + "=" + IDH_PVDBTH._PRVHCD + " ");
                oProvDGridN.setOrderValue("Cast (prvd." + IDH_PVDBTH._Code + " As bigint )");
                oProvDGridN.AddEditLine = false;
                oProvDGridN.doSetDoCount(true);
                //doSetProvisionalDGridListFields(oProvDGridN);
                doGridLayout_ProvisionalDetail(oProvDGridN);
                oProvDGridN.doApplyRules();
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doManageProvisionalBatchDetailGrid()", "Error Managing Provisional Detail Grid: " + ex.Message);
            }
        }

        protected void doManageFinalBatchHeadGrid(SAPbouiCOM.Form oForm) {
            try {
                UpdateGrid oFinalHGridN = (UpdateGrid)UpdateGrid.getInstance(oForm, "FNLVHGRID");
                if (oFinalHGridN == null)
                    oFinalHGridN = new UpdateGrid(this, oForm, "FNLVHGRID");

                doSetFinalGridFilters(oForm);
                oFinalHGridN.doAddGridTable(new GridTable(IDH_PVHBTH.TableName, "prvh", "Code", false, true), true);
                //.U_CarWgt
                GridJoin oLabTaskJoin = new GridJoin("IDH_GetFinalBatchAvailableQty", GridJoin.TYPE_JOIN.LEFT, "v1", "code", "prvh." + IDH_PVHBTH._Code + " = v1.code", true);

                oFinalHGridN.doAddGridJoin(oLabTaskJoin);
                //oFinalHGridN.setRequiredFilter(" " + IDH_PVHBTH._FinalBt + "=\'Y\' And " + IDH_PVHBTH._Status + "=0");
                oFinalHGridN.setRequiredFilter(" " + IDH_PVHBTH._FinalBt + "=\'Y\' ");
                oFinalHGridN.setOrderValue(" prvh." + IDH_PVHBTH._Code + " For Browse ");
                oFinalHGridN.AddEditLine = false;
                oFinalHGridN.doSetDoCount(true);
                //doSetFinalHGridListFields(oFinalHGridN);
                doGridLayout_FinalBatch(oFinalHGridN);
                oFinalHGridN.doApplyRules();
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doManageFinalBatchHeadGrid()", "Error Managing Final Grid: " + ex.Message);
            }
        }


        protected bool doValidate(SAPbouiCOM.Form oForm, bool bIsSample) {
            //Validate form 
            string sNewICD = ((SAPbouiCOM.EditText)oForm.Items.Item("IDHNEWICD").Specific).Value.ToString();
            string sNewINM = ((SAPbouiCOM.EditText)oForm.Items.Item("IDHNEWINM").Specific).Value.ToString();
            string sNewQty = ((SAPbouiCOM.EditText)oForm.Items.Item("IDHNEWQTY").Specific).Value.ToString();

            string sNewUOM = ((SAPbouiCOM.ComboBox)oForm.Items.Item("IDHNEWUOM").Specific).Selected.Value.ToString();
            string sNewWhs = ((SAPbouiCOM.ComboBox)oForm.Items.Item("IDHNEWWHS").Specific).Selected.Value.ToString();

            if (bIsSample) {
                if (sNewICD == string.Empty || sNewINM == string.Empty) {
                    doWarnMess("Validation Failed: The NEW Item Code and Name are mandatory fields to create a Lab Sample. Please provide all of them!", SAPbouiCOM.BoMessageTime.bmt_Medium);
                    return false;
                }
            } else {
                if (sNewICD == string.Empty || sNewINM == string.Empty || sNewQty == string.Empty || sNewUOM == string.Empty || sNewWhs == string.Empty) {
                    doWarnMess("Validation Failed: The NEW Item Code, Name, UOM, Quantity and Warehouse are mandatory fields. Please provide all of them!", SAPbouiCOM.BoMessageTime.bmt_Medium);
                    return false;
                }
            }
            return true;
        }

        /* 
             * Create sample item 
             * 1 - Create entry in LabItem table 
             * 2 - Create a NEW Lab Tsak record 
             * 3 - Generate GI For the items
             * Documents(oInventoryGenEntry) - OIGN - Goods Receipt
             * Documents(oInventoryGenExit) - OIGE - Goods Issue 
             */
        //doCreateSampleItem(oForm,FilterGrid oProvDGrid ,iRow,FilterGrid oProvHGrid )

        //public bool doCreateGoodsIssue(SAPbouiCOM.Form oForm, ref IDH_LABITM oLabItem) {
        //    try {
        //        oLabItem.doLoadChildren();

        //        SAPbobsCOM.Documents objGIDoc;
        //        objGIDoc = (SAPbobsCOM.Documents)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit);

        //        //Fill the object Header 
        //        objGIDoc.CardCode = "";
        //        objGIDoc.CardName = "";

        //        //Document dates 
        //        objGIDoc.DocDate = DateTime.Now;
        //        objGIDoc.TaxDate = DateTime.Now;


        //        int iLICount = oLabItem.LabItemDetails.Count;
        //        oLabItem.LabItemDetails.first();

        //        int iErr = 0;
        //        //Filling Document Rows 
        //        for (int iLine = 0; iLine < iLICount; iLine++) {
        //            oLabItem.LabItemDetails.next();

        //            if (iLine > 0)
        //                objGIDoc.Lines.Add();

        //            objGIDoc.DocDueDate = DateTime.Now;
        //            objGIDoc.Lines.ItemCode = oLabItem.LabItemDetails.U_PItemCd;
        //            objGIDoc.Lines.ItemDescription = oLabItem.LabItemDetails.U_PItemNm;
        //            objGIDoc.Lines.Quantity = Convert.ToDouble(oLabItem.LabItemDetails.U_Quantity);
        //            objGIDoc.Lines.WarehouseCode = oLabItem.LabItemDetails.U_Whse;

        //        }

        //        objGIDoc.Comments = "Doc Generated from Lab Item: " + oLabItem.Code;
        //        iErr = objGIDoc.Add();
        //        if (iErr != 0) {
        //            iGoodsIssue = -1;

        //            if (goParent.goDICompany.InTransaction)
        //                goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);

        //            string sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
        //            com.idh.bridge.DataHandler.INSTANCE.doResSystemError("doCreateGoodsIssue() Error - " + sResult + " Error adding Goods Issue.", "ERSYSTWT",
        //                new string[] { sResult });

        //            return false;
        //        } else {
        //            //iGoodsIssue = objGIDoc.DocEntry;
        //            iGoodsIssue = Convert.ToInt32(DataHandler.INSTANCE.SBOCompany.GetNewObjectKey());
        //            return true;
        //        }
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doError("doCreateGoodsIssue()", "Error adding Goods Issue: " + ex.Message);
        //        iGoodsIssue = -1;
        //        return false;
        //    }

        //}

        //public bool doCreateGoodsReceipt(SAPbouiCOM.Form oForm, ref IDH_LABITM oLabItem) {
        //    try {
        //        oLabItem.doLoadChildren();

        //        SAPbobsCOM.Documents objGRDoc;
        //        objGRDoc = (SAPbobsCOM.Documents)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);

        //        //Fill the object Header 
        //        objGRDoc.CardCode = "";
        //        objGRDoc.CardName = "";

        //        //Document dates 
        //        objGRDoc.DocDate = DateTime.Now;
        //        objGRDoc.TaxDate = DateTime.Now;

        //        int iErr = 0;
        //        ////Filling Document Rows 
        //        //for (int iLine = 0; iLine < oLabItem.LabItemDetails.Count; iLine++)
        //        //{
        //        //    if (iLine > 0)
        //        //    {
        //        //        objGRDoc.Lines.Add();
        //        //        oLabItem.LabItemDetails.next();
        //        //    }
        //        //    objGRDoc.DocDueDate = DateTime.Now;
        //        //    objGRDoc.Lines.ItemCode = oLabItem.LabItemDetails.U_PItemCd;
        //        //    objGRDoc.Lines.ItemDescription = oLabItem.LabItemDetails.U_PItemNm;
        //        //    objGRDoc.Lines.Quantity = Convert.ToDouble(oLabItem.LabItemDetails.U_Quantity);
        //        //    //objGRDoc.Lines.LineTotal = objGRDoc.Lines.Quantity * objGRDoc.Lines.UnitPrice;

        //        //    //if (IsManagedByBatch(objCompany, sItemCode))
        //        //    //{
        //        //    //    objGRDoc.Lines.BatchNumbers.BatchNumber = getNewBatchNumber(objCompany);
        //        //    //    objGRDoc.Lines.BatchNumbers.Quantity = objGRDoc.Lines.Quantity;
        //        //    //}

        //        //}

        //        string sNewIC = ((SAPbouiCOM.EditText)oForm.Items.Item("IDHNEWICD").Specific).Value.ToString();
        //        string sNewIN = ((SAPbouiCOM.EditText)oForm.Items.Item("IDHNEWINM").Specific).Value.ToString();
        //        double dQty = Convert.ToDouble(((SAPbouiCOM.EditText)oForm.Items.Item("IDHNEWQTY").Specific).Value.ToString());

        //        SAPbouiCOM.ComboBox oCmb;
        //        oCmb = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDHNEWUOM").Specific;
        //        string sUOM = oCmb.Selected.Value;
        //        oCmb = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDHNEWWHS").Specific;
        //        string sWhs = oCmb.Selected.Value.ToString();

        //        objGRDoc.DocDueDate = DateTime.Now;
        //        objGRDoc.Lines.ItemCode = sNewIC;
        //        objGRDoc.Lines.ItemDescription = sNewIN;
        //        objGRDoc.Lines.WarehouseCode = sWhs;
        //        objGRDoc.Lines.Quantity = dQty;
        //        objGRDoc.Lines.Add();

        //        objGRDoc.Comments = "Doc Generated from Lab Item: " + oLabItem.Code;
        //        iErr = objGRDoc.Add();
        //        if (iErr != 0) {
        //            iGoodsReceipt = -1;

        //            if (goParent.goDICompany.InTransaction)
        //                goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);

        //            string sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
        //            com.idh.bridge.DataHandler.INSTANCE.doResSystemError("doCreateGoodsReceipt() Error - " + sResult + " Error adding Goods Receipt.", "ERSYSTWT",
        //                new string[] { sResult });

        //            return false;
        //        } else {
        //            //iGoodsReceipt = objGRDoc.DocEntry;
        //            iGoodsReceipt = Convert.ToInt32(DataHandler.INSTANCE.SBOCompany.GetNewObjectKey());
        //            return true;
        //        }
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doError("doCreateGoodsReceipt()", "Error adding Goods Receipt: " + ex.Message);
        //        return false;
        //    }
        //}

        //public bool IsManagedByBatch(SAPbouiCOM.Form oForm, string sItemCode) {
        //    SAPbobsCOM.Recordset rsBales;
        //    string strQry = string.Empty;
        //    strQry = "select ItemCode from OITM where ManBtchNum = 'Y' AND ItemCode = '" + sItemCode + "'";

        //    rsBales = (SAPbobsCOM.Recordset)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
        //    rsBales.DoQuery(strQry);

        //    if (rsBales.RecordCount > 0)
        //        return true;
        //    else
        //        return false;
        //}

        //public string getAvailableBatchNumber(SAPbouiCOM.Form oForm, string sItemCode, double dDocQty, string sLoadNo) {
        //    SAPbobsCOM.Recordset rsBales;
        //    string strQry = string.Empty;
        //    //strQry = "select BatchNum from OIBT where ItemCode = '" + sItemCode + "' AND Quantity >= " + dDocQty.ToString();
        //    //for same batch number 
        //    strQry = "select BatchNum from OIBT where ItemCode = '" + sItemCode + "' AND BatchNum = '" + sLoadNo + "'";

        //    rsBales = (SAPbobsCOM.Recordset)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
        //    rsBales.DoQuery(strQry);

        //    if (rsBales.RecordCount > 0)
        //        return rsBales.Fields.Item(0).Value.ToString();
        //    else
        //        return string.Empty;
        //}

        //public bool doCreateItemMaster(SAPbouiCOM.Form oForm) {
        //    try {
        //        int lRetVal;
        //        SAPbobsCOM.Items oItem;

        //        SAPbouiCOM.ComboBox oCmb;
        //        oCmb = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDHNEWUOM").Specific;
        //        string sUOM = oCmb.Selected.Value;
        //        oCmb = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDHNEWWHS").Specific;
        //        string sWhs = oCmb.Selected.Value.ToString();

        //        oItem = (SAPbobsCOM.Items)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
        //        oItem.ItemCode = ((SAPbouiCOM.EditText)oForm.Items.Item("IDHNEWICD").Specific).Value.ToString();
        //        oItem.ItemName = ((SAPbouiCOM.EditText)oForm.Items.Item("IDHNEWINM").Specific).Value.ToString();
        //        oItem.InventoryUOM = sUOM;
        //        oItem.WhsInfo.WarehouseCode = sWhs;

        //        goParent.goDICompany.StartTransaction();

        //        lRetVal = oItem.Add();

        //        if (lRetVal != 0) {
        //            sItemMaster = "-1";
        //            return false;
        //        } else {
        //            sItemMaster = oItem.ItemCode;
        //            return true;
        //        }
        //    } catch (Exception ex) {
        //        com.idh.bridge.DataHandler.INSTANCE.doError("doCreateItemMaster()", "Error adding New Item Master record: " + ex.Message);
        //        return false;
        //    }

        //}

        protected void doResetForm(SAPbouiCOM.Form oForm) {
            //Reset Item Code Between Filter 
            SAPbouiCOM.EditText oEdit;

            oEdit = (SAPbouiCOM.EditText)oForm.Items.Item("IDH_ITMFRM").Specific;
            oEdit.Value = string.Empty;

            oEdit = (SAPbouiCOM.EditText)oForm.Items.Item("IDH_ITMTO").Specific;
            oEdit.Value = string.Empty;

            oEdit = (SAPbouiCOM.EditText)oForm.Items.Item("IDHNEWICD").Specific;
            oEdit.Value = string.Empty;

            oEdit = (SAPbouiCOM.EditText)oForm.Items.Item("IDHNEWINM").Specific;
            oEdit.Value = string.Empty;

            oEdit = (SAPbouiCOM.EditText)oForm.Items.Item("IDHNEWQTY").Specific;
            oEdit.Value = string.Empty;

            oEdit = (SAPbouiCOM.EditText)oForm.Items.Item("IDHNEWCMT").Specific;
            oEdit.Value = string.Empty;

            SAPbouiCOM.ComboBox oCombo;

            oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDH_ITMGRP").Specific;
            oCombo.Select("", SAPbouiCOM.BoSearchKey.psk_ByValue);

            oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDHNEWUOM").Specific;
            oCombo.Select("", SAPbouiCOM.BoSearchKey.psk_ByValue);

            oCombo = (SAPbouiCOM.ComboBox)oForm.Items.Item("IDHNEWWHS").Specific;
            oCombo.Select("", SAPbouiCOM.BoSearchKey.psk_ByValue);

            doLoadData(oForm);
        }

        private void setButtonStatus(SAPbouiCOM.Form oForm, string sButtonID, string sStaus) {
            SAPbouiCOM.Button oBtn = (SAPbouiCOM.Button)oForm.Items.Item(sButtonID).Specific;
            oBtn.Caption = Translation.getTranslatedWord(sStaus);
        }
        private string getButtonStatus(SAPbouiCOM.Form oForm, string sButtonID) {
            SAPbouiCOM.Button oBtn = (SAPbouiCOM.Button)oForm.Items.Item(sButtonID).Specific;
            return Translation.getTranslatedWord(oBtn.Caption);
        }

        private bool doUpdateBatchStatus(string sBatch_AbsEntry, SAPbobsCOM.BoDefaultBatchStatus oBatchStatus, string sProvisionalBatchCode) {
            try {
                //now unlock the batch 
                SAPbobsCOM.CompanyService oCompanyService;
                oCompanyService = DataHandler.INSTANCE.SBOCompany.GetCompanyService();
                SAPbobsCOM.BatchNumberDetailsService oBatchNumbersService;
                oBatchNumbersService = (SAPbobsCOM.BatchNumberDetailsService)oCompanyService.GetBusinessService(SAPbobsCOM.ServiceTypes.BatchNumberDetailsService);
                SAPbobsCOM.BatchNumberDetailParams oBatchNumberDetailParams;
                oBatchNumberDetailParams = (SAPbobsCOM.BatchNumberDetailParams)oBatchNumbersService.GetDataInterface(SAPbobsCOM.BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams);
                oBatchNumberDetailParams.DocEntry = Convert.ToInt32(sBatch_AbsEntry);
                SAPbobsCOM.BatchNumberDetail oBatchNumberDetail;
                oBatchNumberDetail = oBatchNumbersService.Get(oBatchNumberDetailParams);
                oBatchNumberDetail.Status = oBatchStatus;// SAPbobsCOM.BoDefaultBatchStatus.dbs_Released; //put here the status you want  
                oBatchNumberDetail.UserFields.Item("U_PRVHCD").Value = sProvisionalBatchCode;
                oBatchNumbersService.Update(oBatchNumberDetail);
                return true;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doUpdateBatchStatus()", "Error updating batch status: " + ex.Message);
                return false;
            }
        }

        private void doHandleBatchSearch(SAPbouiCOM.Form oForm) {
            if (getButtonStatus(oForm, "IDH_FDPRVB") == Translation.getTranslatedWord("Update")) {
                com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("You have made some changes in batch(s). Please save or cancel the changes."));
                return;
            }
            FilterGrid oBatchGridN = (FilterGrid)FilterGrid.getInstance(oForm, "BATCHGRID");
            string sOrgReqFilter = oBatchGridN.getRequiredFilter();
            string sWhereOp = string.Empty;
            string sReqFilter = sOrgReqFilter;
            sWhereOp = " AND ";
            //Set selected Whse 
            FilterGrid oWhsGrid = (FilterGrid)FilterGrid.getInstance(oForm, "WHSGRID");
            string sWhsCodes = string.Empty;
            for (int i = 0; i < oWhsGrid.getRowCount(); i++) {
                if (oWhsGrid.doGetFieldValue(1, i).ToString().Equals("Y"))
                    sWhsCodes = sWhsCodes + "'" + oWhsGrid.doGetFieldValue(2, i) + "'" + ", ";
            }
            if (sWhsCodes.Length > 0)
                sReqFilter = sReqFilter + sWhereOp + " w.WhsCode IN (" + (sWhsCodes.Trim()).Trim(',') + ")";
            oBatchGridN.setRequiredFilter(sReqFilter);
            oBatchGridN.doReloadData("", false, false);
            oBatchGridN.doApplyRules();
            oBatchGridN.setRequiredFilter(sOrgReqFilter);
            if (oBatchGridN.getRowCount() == 1 && oBatchGridN.isBlankRow(oBatchGridN.getLastRowIndex())) {
                oBatchGridN.doRemoveRow(oBatchGridN.getLastRowIndex(), false, true);
            }
        }

        #endregion

        #region "Handle Detail Provisional Batch"
        private void doAddBatchToProvisionalBatch(SAPbouiCOM.Form oForm) {
            //Get source row selected 
            FilterGrid oBatchGridN = FilterGrid.getInstance(oForm, "BATCHGRID");
            if (oBatchGridN.getRowCount() == 0 || (oBatchGridN.getRowCount() == 1 && string.IsNullOrEmpty(oBatchGridN.doGetFieldValue("batch.DistNumber", 0).ToString()))) {
                com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("No batch available to create provisional batch."));
                return;
            }

            UpdateGrid oProvisionalHGrid = UpdateGrid.getInstance(oForm, "PROVHGRID");
            if (oProvisionalHGrid.getRowCount() == 0 || oProvisionalHGrid.getSelectedRows().Count == 0 || (oProvisionalHGrid.getRowCount() == 1 && string.IsNullOrEmpty(oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, 0).ToString()))) {
                com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Please select/create a provisional batch to add containers in it."));
                return;
            } else if (oProvisionalHGrid.getSelectedRows().Count > 1) {
                com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Please select only one provisional batch to add containers in it."));
                return;
            }
            UpdateGrid oProvDGrid = UpdateGrid.getInstance(oForm, "PROVDGRID");

            //ArrayList aSelectRows = oProvisionalHGrid.getSelectedRows();
            //int     iPGIndex =0;
            //if (aSelectRows.Count>0)
            //    iPGIndex = (int)aSelectRows[0];

            string sMessage = string.Empty;
            // double dSelectedQty = 0;
            string sProvBatchCode = oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code).ToString();
            string sProvBatchType = oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._BatchTyp).ToString();
            oProvDGrid.AddEditLine = true;
            for (int i = oBatchGridN.getRowCount() - 1; i >= 0; i--) {
                if (oBatchGridN.doGetFieldValue("Select", i).ToString().Equals("Y")
                    && oBatchGridN.doGetFieldValue("batch.[Status]", i).ToString() == "0"
                    ) {
                    int iNGRowCnt = 0;
                    iNGRowCnt = oProvDGrid.getLastRowIndex();// oProvDGrid.getRowCount() - 1;
                    if (oProvDGrid.getRowCount() == 0 || (oProvDGrid.getRowCount() > 0 && oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._Code, iNGRowCnt).ToString() != "")) {
                        oProvDGrid.doAddEditLine();
                        iNGRowCnt = oProvDGrid.getLastRowIndex();// oProvDGrid.getRowCount() - 1;
                    }
                    IDH_PVDBTH oProv = new IDH_PVDBTH();
                    int iRet = oProv.getData(IDH_PVDBTH._AbsEntry.ToString() + "=" + oBatchGridN.doGetFieldValue("batch.AbsEntry", i).ToString(), "");
                    if (iRet > 0) {
                        oProvDGrid.doSetFieldValue("prvd." + IDH_PVDBTH._Code, iNGRowCnt, oProv.Code);
                        oProvDGrid.doSetFieldValue("prvd." + IDH_PVDBTH._Name, iNGRowCnt, oProv.Name);
                    } else {
                        NumbersPair oNumbers = oProv.getNewKey();
                        oProvDGrid.setCurrentDataRowIndex(iNGRowCnt);
                        oProvDGrid.doSetFieldValue("prvd." + IDH_PVDBTH._Code, iNGRowCnt, oNumbers.CodeCode);
                        oProvDGrid.doSetFieldValue("prvd." + IDH_PVDBTH._Name, iNGRowCnt, oNumbers.NameCode);
                    }
                    oProvDGrid.doSetFieldValue("prvd." + IDH_PVDBTH._PRVHCD, iNGRowCnt, sProvBatchCode);
                    oProvDGrid.doSetFieldValue("prvh." + IDH_PVHBTH._BatchTyp, iNGRowCnt, sProvBatchType);

                    oProvDGrid.doSetFieldValue("prvd." + IDH_PVDBTH._ItemCd, iNGRowCnt, oBatchGridN.doGetFieldValue("c.ItemCode", i));
                    oProvDGrid.doSetFieldValue("prvd." + IDH_PVDBTH._ItemDsc, iNGRowCnt, oBatchGridN.doGetFieldValue("c.ItemName", i));

                    oProvDGrid.doSetFieldValue("prvd." + IDH_PVDBTH._WastCd, iNGRowCnt, oBatchGridN.doGetFieldValue("i.ItemCode", i));
                    oProvDGrid.doSetFieldValue("prvd." + IDH_PVDBTH._WastDsc, iNGRowCnt, oBatchGridN.doGetFieldValue("i.ItemName", i));
                    //oProvDGrid.doSetFieldValue("prvd." + IDH_PVDBTH._DisRCode, iNGRowCnt, oBatchGridN.doGetFieldValue("d." + IDH_DISPRTCD._DisRCode, i));
                    oProvDGrid.doSetFieldValue("prvd." + IDH_PVDBTH._DisRCode, iNGRowCnt, oBatchGridN.doGetFieldValue("batch.LotNumber", i));
                    oProvDGrid.doSetFieldValue("prvd." + IDH_PVDBTH._BatchSysNm, iNGRowCnt, oBatchGridN.doGetFieldValue("batch.SysNumber", i));
                    oProvDGrid.doSetFieldValue("prvd." + IDH_PVDBTH._Batch, iNGRowCnt, oBatchGridN.doGetFieldValue("batch.DistNumber", i));
                    oProvDGrid.doSetFieldValue("prvd." + IDH_PVDBTH._AbsEntry, iNGRowCnt, oBatchGridN.doGetFieldValue("batch.AbsEntry", i));
                    oProvDGrid.doSetFieldValue("prvd." + IDH_PVDBTH._WhsCode, iNGRowCnt, oBatchGridN.doGetFieldValue("w.WhsCode", i));

                    oBatchGridN.doRemoveRow(i, false, false);
                    setButtonStatus(oForm, "IDH_FDPRVB", "Update");
                }
            }
            oProvDGrid.AddEditLine = false;
            oBatchGridN.doRecount();
        }
        private void doRemoveBatchFromProvisionalBatch(SAPbouiCOM.Form oForm) {
            //Get source row selected 
            FilterGrid oBatchGridN = FilterGrid.getInstance(oForm, "BATCHGRID");
            UpdateGrid oProvDGrid = UpdateGrid.getInstance(oForm, "PROVDGRID");
            if (oProvDGrid.getRowCount() == 0 || (oProvDGrid.getRowCount() == 1 && string.IsNullOrEmpty(oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._Batch, 0).ToString()))) {
                com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("No batch available to create provisional batch."));
                return;
            }
            string sMessage = string.Empty;

            oBatchGridN.AddEditLine = true;
            ArrayList aSelectRows = oProvDGrid.getSelectedRows();
            ArrayList aSelectedCodes = new ArrayList();
            for (int isel = aSelectRows.Count - 1; isel >= 0; isel--) {
                int i = -1;
                i = (int)aSelectRows[isel];
                int iNGRowCnt = 0;

                iNGRowCnt = oBatchGridN.getLastRowIndex();// oProvDGrid.getRowCount() - 1;
                if (oBatchGridN.getRowCount() == 0 || (oBatchGridN.getRowCount() > 0 && oBatchGridN.doGetFieldValue("batch.DistNumber", iNGRowCnt).ToString() != "")) {
                    oBatchGridN.doAddEditLine();
                    iNGRowCnt = oBatchGridN.getLastRowIndex();// oProvDGrid.getRowCount() - 1;
                }

                oBatchGridN.doSetFieldValue("c.ItemCode", iNGRowCnt, oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._ItemCd, i));
                oBatchGridN.doSetFieldValue("c.ItemName", iNGRowCnt, oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._ItemDsc, i));

                oBatchGridN.doSetFieldValue("i.ItemCode", iNGRowCnt, oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._WastCd, i));
                oBatchGridN.doSetFieldValue("i.ItemName", iNGRowCnt, oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._WastDsc, i));
                //oBatchGridN.doSetFieldValue("d." + IDH_DISPRTCD._DisRCode, iNGRowCnt, oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._DisRCode, i));
                oBatchGridN.doSetFieldValue("batch.LotNumber", iNGRowCnt, oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._DisRCode, i));

                oBatchGridN.doSetFieldValue("batch.AbsEntry", iNGRowCnt, oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._AbsEntry, i));
                oBatchGridN.doSetFieldValue("batch.SysNumber", iNGRowCnt, oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._BatchSysNm, i));
                oBatchGridN.doSetFieldValue("batch.DistNumber", iNGRowCnt, oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._Batch, i));
                oBatchGridN.doSetFieldValue("w.WhsCode", iNGRowCnt, oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._WhsCode, i));
                if (oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._WhsCode, i).ToString().Trim() != string.Empty) {
                    string sWhsName = Config.INSTANCE.getValueFromTablebyKey("OWHS", "WhsName", "WhsCode", oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._WhsCode, i).ToString()).ToString();
                    if (sWhsName != null && sWhsName != string.Empty)
                        oBatchGridN.doSetFieldValue("w.WhsName", iNGRowCnt, sWhsName);
                }
                oBatchGridN.doSetFieldValue("batch.[Status]", iNGRowCnt, "0");
                oBatchGridN.doSetFieldValue("Select", iNGRowCnt, "N");
                setButtonStatus(oForm, "IDH_FDPRVB", "Update");
                aSelectedCodes.Add(oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._Code, i).ToString());
            }
            while (aSelectedCodes.Count > 0) {
                for (int i = oProvDGrid.getRowCount() - 1; i >= 0; i--) {
                    if (aSelectedCodes.Count == 0)
                        break;
                    if (oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._Code, i).ToString() == aSelectedCodes[0].ToString()) {
                        oProvDGrid.doRemoveRow(i, false, false);
                        aSelectedCodes.RemoveAt(0);
                        break;
                    }
                }
            }
            oProvDGrid.doRecount();
            oBatchGridN.AddEditLine = false;
        }
        #endregion

        #region "New Provisional Batch"
        private void doHandleNewProvisionalBatch(SAPbouiCOM.Form oForm) {
            //Batch Grid 
            UpdateGrid oBatchGridN = (UpdateGrid)FilterGrid.getInstance(oForm, "BATCHGRID");
            if (oBatchGridN.getRowCount() == 0 || (oBatchGridN.getRowCount() == 1 || string.IsNullOrEmpty(oBatchGridN.doGetFieldValue("batch.DistNumber", 1).ToString()))) {
                com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("No batch available to create provisional batch."));
                return;
            }
            if (getButtonStatus(oForm, "IDH_FDPRVB") == Translation.getTranslatedWord("Update")) {
                com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("You have made some changes. Please save the changes before creating another batch."));
                return;
            }
            //Provisional  Grid 
            UpdateGrid oProvisionalHGrid = (UpdateGrid)FilterGrid.getInstance(oForm, "PROVHGRID");
            oProvisionalHGrid.AddEditLine = true;
            oProvisionalHGrid.doAddEditLine();
            oProvisionalHGrid.AddEditLine = false;
            int iNewRow = 0;
            iNewRow = oProvisionalHGrid.getRowCount() - 1;
            IDH_PVHBTH oProv = new IDH_PVHBTH();
            NumbersPair oNumbers = oProv.getNewKey();
            oProvisionalHGrid.doSetFieldValue("prvh." + IDH_PVHBTH._Code, iNewRow, oNumbers.CodeCode);
            oProvisionalHGrid.doSetFieldValue("prvh." + IDH_PVHBTH._Name, iNewRow, oNumbers.NameCode);
            oProvisionalHGrid.getSBOGrid().Rows.SelectedRows.Clear();
            oProvisionalHGrid.setCurrentLineByClick(iNewRow);
            oProvisionalHGrid.getSBOGrid().Rows.SelectedRows.Add(iNewRow);
            UpdateGrid oProvDGrid = UpdateGrid.getInstance(oForm, "PROVDGRID");
            string sOrgRequiredFilter = oProvDGrid.getRequiredFilter();
            oProvDGrid.setRequiredFilter("prvd.Code = '-999'");
            oProvDGrid.doReloadData("", false, false);
            oProvDGrid.doApplyRules();
            oProvDGrid.setRequiredFilter(sOrgRequiredFilter);
            setButtonStatus(oForm, "IDH_FDPRVB", "Update");
        }
        #endregion

        #region "Handle Find and Update Provisional Grid"
        private void doHandleFindNUpdateButton(SAPbouiCOM.Form oForm) {
            //update middle and left forms
            if (getButtonStatus(oForm, "IDH_FDPRVB") == Translation.getTranslatedWord("Update")) {
                doHandleUpdateProvisinalGrid(oForm);

            } else if (getButtonStatus(oForm, "IDH_FDPRVB") == Translation.getTranslatedWord("Find")) {
                doHandleFindProvisinalGrid(oForm, false);
            }
        }
        private void doHandleUpdateProvisinalGrid(SAPbouiCOM.Form oForm) {
            FilterGrid oBatchGridN = FilterGrid.getInstance(oForm, "BATCHGRID");
            FilterGrid oProvHGrid = FilterGrid.getInstance(oForm, "PROVHGRID");
            FilterGrid oProvDGrid = FilterGrid.getInstance(oForm, "PROVDGRID");
            if (getButtonStatus(oForm, "IDH_FDPRVB") == Translation.getTranslatedWord("Update") && oProvHGrid.getRowCount() > 0) {
                int iCurrentIndex = 0;
                if (oProvHGrid.getSelectedRows().Count == 1)
                    iCurrentIndex = (int)oProvHGrid.getSelectedRows()[0];
                else
                    iCurrentIndex = oProvHGrid.getCurrentDataRowIndex();
                string sProvBatchCode = "";
                string sProvBatchType = "";
                IDH_PVHBTH objHeader;
                //ArrayList aRowsToDeltee = oProvHGrid.getDeletedRowList();
                //if (aRowsToDeltee!=null && aRowsToDeltee.Count>0)
                //for (int iIndex = 0; iIndex <= aRowsToDeltee.Count - 1; iIndex++) {
                //    int i = Convert.ToInt32(aRowsToDeltee[iIndex]);
                //}
                ArrayList aRows = oProvHGrid.doGetChangedRows();
                ArrayList aRowsAdded = oProvHGrid.doGetAddedRows();
                if (aRowsAdded != null && aRowsAdded.Count > 0) {
                    if (aRows != null)
                        aRows.AddRange(aRowsAdded);
                    else
                        aRows = aRowsAdded;
                }
                if (aRows != null) {
                    for (int iIndex = 0; iIndex <= aRows.Count - 1; iIndex++) {
                        int i = Convert.ToInt32(aRows[iIndex]);
                        sProvBatchCode = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, i).ToString();
                        sProvBatchType = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._BatchTyp, i).ToString();
                        if (sProvBatchType == string.Empty) {
                            com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Please select provisional batch type."));
                            oProvHGrid.getSBOGrid().SetCellFocus(oProvHGrid.getCurrentDataRowIndex(), oProvHGrid.doFieldIndex("prvh." + IDH_PVHBTH._BatchTyp));
                            return;
                        }
                        objHeader = new IDH_PVHBTH();
                        if (objHeader.getByKey(sProvBatchCode)) {
                        } else {
                            objHeader.doAddEmptyRow();
                            objHeader.Code = sProvBatchCode;
                            objHeader.Name = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Name, i).ToString();
                        }
                        objHeader.U_BatchTyp = sProvBatchType;
                        objHeader.U_DisRCode = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._DisRCode, i).ToString();
                        objHeader.U_ItemCd = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ItemCd, i).ToString();
                        objHeader.U_ItemDsc = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ItemDsc, i).ToString();
                        objHeader.U_LabTskCd = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._LabTskCd, i).ToString();
                        objHeader.U_CntnrQty = (double)oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._CntnrQty, i);
                        objHeader.U_WastQty = (double)oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WastQty, i);
                        objHeader.U_WastCd = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WastCd, i).ToString();
                        objHeader.U_WastDsc = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WastDsc, i).ToString();
                        objHeader.U_Warehouse = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Warehouse, i).ToString();
                        objHeader.doProcessData();

                    }
                }
                //  for (int i = 0; i <= oProvHGrid.getRowCount() - 1; i++) {
                //sProvBatchCode = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, i).ToString();
                //sProvBatchType = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._BatchTyp, i).ToString();
                //if (sProvBatchType == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Please select provisional batch type."));
                //    oProvHGrid.getSBOGrid().SetCellFocus(oProvHGrid.getCurrentDataRowIndex(), oProvHGrid.doFieldIndex("prvh." + IDH_PVHBTH._BatchTyp));
                //    return;
                //}
                //objHeader = new IDH_PVHBTH();
                //if (objHeader.getByKey(sProvBatchCode)) {
                //} else {
                //    objHeader.doAddEmptyRow();
                //    objHeader.Code = sProvBatchCode;
                //    objHeader.Name = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Name, i).ToString();
                //}
                //objHeader.U_BatchTyp = sProvBatchType;
                //objHeader.U_DisRCode = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._DisRCode, i).ToString();
                ////objHeader.U_FinalBt = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._FinalBt).ToString();
                //objHeader.U_ItemCd = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ItemCd, i).ToString();
                //objHeader.U_ItemDsc = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ItemDsc, i).ToString();
                //objHeader.U_LabTskCd = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._LabTskCd, i).ToString();
                ////objHeader.U_ProdDoc = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ProdDoc).ToString();
                ////objHeader.U_ProdNum = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ProdNum).ToString();
                //objHeader.U_CntnrQty = (double)oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._CntnrQty, i);
                //objHeader.U_WastQty = (double)oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WastQty, i);
                ////objHeader.U_UOM = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._UOM).ToString();
                //objHeader.U_WastCd = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WastCd, i).ToString();
                //objHeader.U_WastDsc = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WastDsc, i).ToString();
                //objHeader.U_Warehouse = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Warehouse, i).ToString();
                ////objHeader.U_WastQty = (double)oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WastQty);
                //objHeader.doProcessData();
                //   }
                sProvBatchCode = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, iCurrentIndex).ToString();
                sProvBatchType = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._BatchTyp, iCurrentIndex).ToString();
                //if (sProvBatchType == string.Empty) {
                //    com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Please select provisional batch type."));
                //    oProvHGrid.getSBOGrid().SetCellFocus(oProvHGrid.getCurrentDataRowIndex(), oProvHGrid.doFieldIndex("prvh." + IDH_PVHBTH._BatchTyp));
                //    return true;
                //}
                oProvHGrid.doRemoveAddedRowFlag();
                oProvHGrid.doRemoveChangedRowFlag();

                IDH_PVDBTH objDetail;
                for (int i = 0; i <= oProvDGrid.getRowCount() - 1; i++) {
                    oProvDGrid.doSetFieldValue("prvd." + IDH_PVDBTH._PRVHCD, i, sProvBatchCode);
                    if (oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._Code, i).ToString() == string.Empty)
                        continue;
                    objDetail = new IDH_PVDBTH();
                    if (!objDetail.getByKey(oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._Code, i).ToString())) {
                        objDetail.doAddEmptyRow();
                        objDetail.Code = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._Code, i).ToString();
                        objDetail.Name = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._Name, i).ToString();
                    }
                    objDetail.U_AbsEntry = (int)oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._AbsEntry, i);
                    objDetail.U_Batch = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._Batch, i).ToString();
                    objDetail.U_BatchSysNm = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._BatchSysNm, i).ToString();
                    objDetail.U_DisRCode = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._DisRCode, i).ToString();
                    objDetail.U_ItemCd = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._ItemCd, i).ToString();
                    objDetail.U_ItemDsc = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._ItemDsc, i).ToString();
                    objDetail.U_PRVHCD = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._PRVHCD, i).ToString();
                    objDetail.U_WastCd = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._WastCd, i).ToString();
                    objDetail.U_WastDsc = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._WastDsc, i).ToString();
                    objDetail.U_WhsCode = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._WhsCode, i).ToString();
                    objDetail.doProcessData();
                    string sBatchAbsNumber = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._AbsEntry, i).ToString();
                    if (sBatchAbsNumber != "0")
                        doUpdateBatchStatus(sBatchAbsNumber, SAPbobsCOM.BoDefaultBatchStatus.dbs_Locked, sProvBatchCode);
                }
                //release the batches which was locked earlier
                bool bRet;
                string sBatch_AbsEntry = "";
                string sStatus = "";
                int iRow = 0;
                if (oBatchGridN.hasChangedRows()) {
                    //Hashtable oChangedRows= oBatchGridN.doGetChangedRowsBuffer();
                    for (int i = 0; i <= oBatchGridN.doGetChangedRows().Count - 1; i++) {
                        iRow = Convert.ToInt32(oBatchGridN.doGetChangedRows()[i]);
                        sBatch_AbsEntry = oBatchGridN.doGetFieldValue("batch.AbsEntry", iRow).ToString(); //Put here the actual AbsEntry of the Batchnumber record  
                        if (sBatch_AbsEntry == "" || sBatch_AbsEntry == "0")
                            continue;
                        sStatus = Config.INSTANCE.getValueFromTablebyKey("OBTN", "Status", "AbsEntry", sBatch_AbsEntry).ToString();
                        if (sStatus != null && sStatus == "0")
                            continue;
                        //now unlock the batch 
                        doUpdateBatchStatus(sBatch_AbsEntry, SAPbobsCOM.BoDefaultBatchStatus.dbs_Released, "");
                        objDetail = new IDH_PVDBTH();
                        int iRet = objDetail.getData(IDH_PVDBTH._AbsEntry.ToString() + "=" + sBatch_AbsEntry, "");
                        if (iRet > 0) {
                            bRet = objDetail.doDeleteData();
                        }
                        oBatchGridN.doSetFieldValue("batch.[Status]", iRow, "0");
                    }
                    /*
                for (int i = 0; i <= oBatchGridN.getRowCount() - 1; i++) {
                    sBatch_AbsEntry = oBatchGridN.doGetFieldValue("batch.AbsEntry", i).ToString(); //Put here the actual AbsEntry of the Batchnumber record  
                    if (sBatch_AbsEntry == "" || sBatch_AbsEntry == "0")
                        continue;
                    sStatus = Config.INSTANCE.getValueFromTablebyKey("OBTN", "Status", "AbsEntry", sBatch_AbsEntry).ToString();
                    if (sStatus != null && sStatus == "0")
                        continue;
                    //now unlock the batch 
                    doUpdateBatchStatus(sBatch_AbsEntry, SAPbobsCOM.BoDefaultBatchStatus.dbs_Released, "");
                    objDetail = new IDH_PVDBTH();
                    int iRet = objDetail.getData(IDH_PVDBTH._AbsEntry.ToString() + "=" + sBatch_AbsEntry, "");
                    if (iRet > 0) {
                        bRet = objDetail.doDeleteData();
                    }
                    oBatchGridN.doSetFieldValue("batch.[Status]", i, "0");
                }*/
                    oBatchGridN.doGetChangedRows().Clear();
                    oBatchGridN.doGetChangedRowsBuffer().Clear();
                }
                setButtonStatus(oForm, "IDH_FDPRVB", "Find");
            }
        }

        private void doHandleFindProvisinalGrid(SAPbouiCOM.Form oForm, bool BYFORCE) {
            FilterGrid oBatchGridN = FilterGrid.getInstance(oForm, "BATCHGRID");
            FilterGrid oProvHGrid = FilterGrid.getInstance(oForm, "PROVHGRID");
            FilterGrid oProvDGrid = FilterGrid.getInstance(oForm, "PROVDGRID");
            if (BYFORCE || getButtonStatus(oForm, "IDH_FDPRVB") == Translation.getTranslatedWord("Find")) {
                //oProvHGrid = FilterGrid.getInstance(oForm, "PROVHGRID");
                //FilterGrid oProvDGrid = FilterGrid.getInstance(oForm, "PROVDGRID");
                string sBatchName = getItemValue(oForm, "IDH_BATCH2").ToString();
                string sContainerCode = getItemValue(oForm, "IDH_CNTCD2").ToString();
                string sWasteCode = getItemValue(oForm, "IDH_WASCD2").ToString();
                string sWasteDesc = getItemValue(oForm, "IDH_WASDS2").ToString();
                string sBatchType = getItemValue(oForm, "IDH_BATYP2").ToString();
                string sProvisionalBatchCD = getItemValue(oForm, "IDH_PRVBCD").ToString();
                string sLabTask = getItemValue(oForm, "IDH_LBTSK").ToString();
                string sDispRoutCd = getItemValue(oForm, "IDH_ROTCD2").ToString();
                string sSearchInContects = getItemValue(oForm, "IDH_SRCHDT").ToString();

                string sOrgRequiredFilter = oProvHGrid.getRequiredFilter();
                string sReqFilter = sOrgRequiredFilter;
                if (sBatchName != "") {
                    sReqFilter += " And prvh." + IDH_PVHBTH._Code + " In (Select " + IDH_PVDBTH._PRVHCD + " from [" + IDH_PVDBTH.TableName + "] d with(nolock) where " + IDH_PVDBTH._Batch + " Like \'" + sBatchName.Replace("\'", "\'\'").Replace("*", "%") + "\' ) ";
                }
                if (sSearchInContects == "Y") {//search in contents
                    //oProvHGrid.setDoFilter(false);
                    // oProvHGrid.doAddFilterField("IDH_BATYP2, "prvh." + IDH_PVHBTH._BatchTyp, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 255, "", false);                            
                } else {//search in provisional grid
                    //oProvHGrid.setDoFilter(true);
                    oProvHGrid.doAddFilterField("IDH_CNTCD2", "prvh." + IDH_PVHBTH._ItemCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");
                    oProvHGrid.doAddFilterField("IDH_WASCD2", "prvh." + IDH_PVHBTH._WastCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");
                    oProvHGrid.doAddFilterField("IDH_WASDS2", "prvh." + IDH_PVHBTH._WastDsc, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");
                    oProvHGrid.doAddFilterField("IDH_BATYP2", "prvh." + IDH_PVHBTH._BatchTyp, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 255, "");
                    oProvHGrid.doAddFilterField("IDH_PRVBCD", "prvh." + IDH_PVHBTH._Code, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");
                    oProvHGrid.doAddFilterField("IDH_LBTSK", "prvh." + IDH_PVHBTH._LabTskCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");
                    oProvHGrid.doAddFilterField("IDH_ROTCD2", "prvh." + IDH_PVHBTH._DisRCode, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");
                }
                oProvHGrid.setRequiredFilter(sReqFilter);
                oProvHGrid.doReloadData("", false, false);
                oProvHGrid.doApplyRules();
                oProvHGrid.setRequiredFilter(sOrgRequiredFilter);

                sOrgRequiredFilter = oProvDGrid.getRequiredFilter();
                oProvDGrid.setRequiredFilter("prvd.Code = '-999'");
                oProvDGrid.doReloadData("", false, false);
                oProvDGrid.doApplyRules();
                oProvDGrid.setRequiredFilter(sOrgRequiredFilter);

                doFillProvisionalGridCombos(oForm);
                setButtonStatus(oForm, "IDH_FDPRVB", "Find");
            }
        }
        #endregion
        #region "Handle Find Final Grid"
        private void doHandleFinalGridFind(SAPbouiCOM.Form oForm, bool BYFORCE) {
            FilterGrid oFinalHGrid = FilterGrid.getInstance(oForm, "FNLVHGRID");
            if (BYFORCE || getButtonStatus(oForm, "IDH_FDPRVB") == Translation.getTranslatedWord("Find")) {
                string sStatus = getItemValue(oForm, "IDH_FNSTAT").ToString();
                //oFinalHGrid.doReloadData("", false, false);
                string sOrgRequiredFilter = oFinalHGrid.getRequiredFilter();
                string sReqFilter = sOrgRequiredFilter;
                if (sStatus == "0") {
                    sReqFilter += " And isnull((Select v1.TotalAvailableQty from IDH_GetFinalBatchAvailableQty v1 Where v1.code = prvh." + IDH_PVHBTH._Code + "),0)>0 ";


                } else if (sStatus == "1") {
                    //sReqFilter += " And isnull((Select Sum(OIBT.Quantity) As TotalAvailableQty"
                    //    + " from [@IDH_PVHBTH] fb2 "
                    //    + " Inner Join OWOR on OWOR.U_PRVHCD=fb2.Code"
                    //    + " Inner Join IGN1 on OWOR.DocEntry=IGN1.BaseEntry AND IGN1.BaseType=202"
                    //    + " Inner Join OIGN on OIGN.DocEntry=IGN1.DocEntry AND IGN1.ObjType=59 And OIGN.DocType='I'"
                    //    + " Inner Join dbo.OIBT on OIBT.BaseEntry=OIGN.DocEntry and OIBT.BaseType=59"
                    //    + " where fb2.U_CntnrQty>0 and fb2.U_WORCntQty>0"
                    //    + " And OIBT.Quantity>0 And fb2.Code=prvh." + IDH_PVHBTH._Code + "),0)=0 ";
                    sReqFilter += " And isnull((Select v1.TotalAvailableQty from IDH_GetFinalBatchAvailableQty v1 Where v1.code = prvh." + IDH_PVHBTH._Code + "),0)<=0 ";
                }
                oFinalHGrid.setRequiredFilter(sReqFilter);
                oFinalHGrid.doReloadData("", false, false);
                oFinalHGrid.doApplyRules();
                oFinalHGrid.setRequiredFilter(sOrgRequiredFilter);
                doFillFinalGridCombos(oForm);
            }
        }
        private void doSetFinalGridFilters(SAPbouiCOM.Form oForm) {
            FilterGrid oFinalHGrid = FilterGrid.getInstance(oForm, "FNLVHGRID");
            if (getButtonStatus(oForm, "IDH_FDPRVB") == Translation.getTranslatedWord("Find")) {
                //oProvHGrid = FilterGrid.getInstance(oForm, "PROVHGRID");
                //FilterGrid oProvDGrid = FilterGrid.getInstance(oForm, "PROVDGRID");
                //string sBatchName = getItemValue(oForm, "IDH_BATCH3").ToString();
                //string sContainerCode = getItemValue(oForm, "IDH_CNTCD3").ToString();
                //string sWasteCode = getItemValue(oForm, "IDH_WASCD2").ToString();
                //string sWasteDesc = getItemValue(oForm, "IDH_WASDS2").ToString();
                //string sBatchType = getItemValue(oForm, "IDH_BATYP2").ToString();
                //string sProvisionalBatchCD = getItemValue(oForm, "IDH_PRVBCD").ToString();
                //string sLabTask = getItemValue(oForm, "IDH_LBTSK").ToString();
                //string sDispRoutCd = getItemValue(oForm, "IDH_ROTCD2").ToString();
                //string sSearchInContects = getItemValue(oForm, "IDH_SRCHDT").ToString();

                //string sOrgRequiredFilter = oFinalHGrid.getRequiredFilter();
                // string sReqFilter = sOrgRequiredFilter;
                //if (sBatchName != "") {
                //    sReqFilter += " And prvh." + IDH_PVHBTH._Code + " In (Select " + IDH_PVDBTH._PRVHCD + " from [" + IDH_PVDBTH.TableName + "] d with(nolock) where " + IDH_PVDBTH._Batch + " Like \'" + sBatchName.Replace("\'", "\'\'").Replace("*", "%") + "\' ) ";
                //}
                //if (sSearchInContects == "Y") {//search in contents
                //    //oProvHGrid.setDoFilter(false);
                //    // oProvHGrid.doAddFilterField("IDH_BATYP2, "prvh." + IDH_PVHBTH._BatchTyp, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 255, "", false);                            
                //} else {//search in provisional grid
                //oProvHGrid.setDoFilter(true);

                oFinalHGrid.doAddFilterField("IDH_BATCH3", "prvh." + IDH_PVHBTH._Code, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");
                oFinalHGrid.doAddFilterField("IDH_BATYP3", "prvh." + IDH_PVHBTH._BatchTyp, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");

                oFinalHGrid.doAddFilterField("IDH_CNTCD3", "prvh." + IDH_PVHBTH._ItemCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");
                oFinalHGrid.doAddFilterField("IDH_CNTNM3", "prvh." + IDH_PVHBTH._ItemDsc, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");

                oFinalHGrid.doAddFilterField("IDH_WASCD3", "prvh." + IDH_PVHBTH._WastCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");
                oFinalHGrid.doAddFilterField("IDH_WASDS3", "prvh." + IDH_PVHBTH._WastDsc, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");
                //  oProvHGrid.doAddFilterField("IDH_BATYP2, "prvh." + IDH_PVHBTH._BatchTyp, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 255, "");
                //     oProvHGrid.doAddFilterField("IDH_PRVBCD", "prvh." + IDH_PVHBTH._Code, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");
                //   oProvHGrid.doAddFilterField("IDH_LBTSK", "prvh." + IDH_PVHBTH._LabTskCd, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");
                oFinalHGrid.doAddFilterField("IDH_ROTCD3", "prvh." + IDH_PVHBTH._DisRCode, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255, "");
                //oFinalHGrid.doAddFilterField("IDH_FNSTAT", "prvh." + IDH_PVHBTH._Status, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 10, "");
                setUFValue(oForm, "IDH_FNSTAT", "0");
                //SAPbouiCOM.Item oItem = oForm.Items.Item("IDH_FNSTAT");
                //SAPbouiCOM.ComboBox oCombo = (SAPbouiCOM.ComboBox)oItem.Specific;
                //oCombo.SelectExclusive("0", SAPbouiCOM.BoSearchKey.psk_ByValue);
                //oCombo.SelectExclusive(1, SAPbouiCOM.BoSearchKey.psk_Index);
                //
                // }
                //     oProvHGrid.setRequiredFilter(sReqFilter);
                //  oProvHGrid.setRequiredFilter(sOrgRequiredFilter);

                //   sOrgRequiredFilter = oProvDGrid.getRequiredFilter();
                //   oProvDGrid.setRequiredFilter("prvd.Code = '-999'");
                //    oProvDGrid.doReloadData("", false, false);
                //   oProvDGrid.doApplyRules();
                //   oProvDGrid.setRequiredFilter(sOrgRequiredFilter);

                //doFillFinalGridCombos(oForm);
                //   setButtonStatus(oForm, "IDH_FDPRVB", "Find");
            }
        }
        #endregion
        #region "Lab Task"
        private bool doHandleLabTaskButton(SAPbouiCOM.Form oForm) {
            if (getButtonStatus(oForm, "IDH_FDPRVB") == Translation.getTranslatedWord("Update")) {
                com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("You have made some changes. Please save the changes made before creating lab task."));
                return true;
            }
            //FilterGrid oBatchGridN = FilterGrid.getInstance(oForm, "BATCHGRID");
            FilterGrid oProvHGrid = FilterGrid.getInstance(oForm, "PROVHGRID");
            //FilterGrid oProvDGrid = FilterGrid.getInstance(oForm, "PROVDGRID");

            ArrayList aSelectRow = oProvHGrid.getSelectedRows();
            if (aSelectRow.Count == 0) {
                com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Please select a batch to create lab task."));
                return true;
            }
            int iRow = (int)aSelectRow[0];
            if (oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WastCd, iRow).ToString() == string.Empty) {
                com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Please select a waste code for the provisional batch."));
                oProvHGrid.getSBOGrid().SetCellFocus(oProvHGrid.getCurrentDataRowIndex(), oProvHGrid.doFieldIndex("prvh." + IDH_PVHBTH._WastCd));
                return true;
            }
            if (oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._LabTskCd, iRow).ToString() != string.Empty) {
                string sMsg = "Selected provisional batch already have an assigned lab task. Are you sure you want to re-create lab task for the batch?";
                if (com.idh.bridge.resources.Messages.INSTANCE.doMessage(sMsg, 1, "Yes", "No") != 1)
                    return true;
            }
            FilterGrid oProvDGrid = FilterGrid.getInstance(oForm, "PROVDGRID");
            if (oProvDGrid.getRowCount() == 0 || oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._ItemCd, oProvDGrid.getRowCount() - 1).ToString() == string.Empty) {
                string sMsg = "Selected provisional batch does not have any waste item(s). Are you sure you want to create lab task for the batch?";
                if (com.idh.bridge.resources.Messages.INSTANCE.doMessage(sMsg, 1, "Yes", "No") != 1)
                    return true;
            }
            doCreateLabTaskForProvBatch(oForm, oProvDGrid, iRow, oProvHGrid);
            return true;
        }
        protected void doCreateLabTaskForProvBatch(SAPbouiCOM.Form oForm, FilterGrid oProvDGrid, int iHRow, FilterGrid oProvHGrid) {
            try {
                IDH_LABITM oLabItem = new IDH_LABITM();
                IDH_LITMDTL oLIDetail = new IDH_LITMDTL();
                bool bResult = false;
                goParent.goDICompany.StartTransaction();

                bResult = doCreateLabItem(oForm, ref oLabItem, ref oLIDetail, oProvDGrid, iHRow, oProvHGrid); //Inserting new record in Lab Item and LI Details 
                if (bResult)
                    bResult = doCreateLabTask(oForm, ref oLabItem, oProvDGrid, iHRow, oProvHGrid);

                if (bResult) {
                    //Set relevent documents codes into IDH_LABITM object 
                    oLabItem.U_LabTask = iLabTaskCd.ToString();
                    oLabItem.doUpdateDataRow();
                    IDH_PVHBTH oProvBatch = new IDH_PVHBTH();
                    oProvBatch.UnLockTable = true;
                    if (oProvBatch.getByKey(oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, iHRow).ToString())) {
                        oProvBatch.U_LabTskCd = iLabTaskCd.ToString();
                        oProvBatch.doProcessData();
                        oProvHGrid.doSetFieldValue("prvh." + IDH_PVHBTH._LabTskCd, iHRow, iLabTaskCd);
                        oProvHGrid.doSetFieldValue("labtask." + IDH_LABTASK._Decision, iHRow, "Pending");
                    }
                    if (goParent.goDICompany.InTransaction)
                        goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    IDHAddOns.idh.addon.Base.STATUSBAR.SetText("Lab task Number " + iLabTaskCd.ToString() + " has been added successfully.", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                    //doResetForm(oForm);
                    // goParent.doOpenForm("IDHLABITM", oForm, false);
                } else
                    if (goParent.goDICompany.InTransaction) {
                    goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors();
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doCreateLabTaskForProvBatch()", "Error Creating Lab Task: " + ex.Message);
            } finally {
                if (goParent.goDICompany.InTransaction) {
                    goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                    com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors();
                }
            }
        }
        /*
         * Create Batch Item 
         * 1 - Create a New Item Master Record 
         * 2 - Create entry in LabItem table 
         * 3 - Generate GI for the selectd items
         * 4 - Generate GR for the New Item Created 
         * 5 - Create New Lab Task record for newly created item??
         * Documents(oInventoryGenEntry) - OIGN - Goods Receipt
         * Documents(oInventoryGenExit) - OIGE - Goods Issue 
         */
        //protected void doCreateBatchItem(SAPbouiCOM.Form oForm) {

        //    IDH_LABITM oLabItem = new IDH_LABITM();
        //    IDH_LITMDTL oLIDetail = new IDH_LITMDTL();
        //    bool bResult = false;

        //    bResult = doCreateItemMaster(oForm);

        //    //if (bResult)
        //    //    bResult = doCreateLabItem(oForm, ref oLabItem, ref oLIDetail); //Inserting new record in Lab Item and LI Details 

        //    //Dont need to create a Lab Task when it is a Batch 
        //    //if (bResult)
        //    //    bResult = doCreateLabTask(oForm, ref oLabItem);

        //    if (bResult)
        //        bResult = doCreateGoodsIssue(oForm, ref oLabItem);

        //    if (bResult)
        //        bResult = doCreateGoodsReceipt(oForm, ref oLabItem);

        //    if (bResult) {
        //        //Set relevent documents codes into IDH_LABITM object 
        //        oLabItem.U_GIssue = iGoodsIssue.ToString();
        //        oLabItem.U_GReceipt = iGoodsReceipt.ToString();
        //        oLabItem.U_ItemCode = sItemMaster;
        //        oLabItem.doUpdateDataRow();

        //        if (goParent.goDICompany.InTransaction)
        //            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
        //        IDHAddOns.idh.addon.Base.STATUSBAR.SetText("Process completed successfully.", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
        //        doResetForm(oForm);
        //        goParent.doOpenForm("IDHLABITM", oForm, false);
        //    } else
        //        if (goParent.goDICompany.InTransaction) {
        //            goParent.goDICompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
        //            com.idh.bridge.DataHandler.INSTANCE.doBufferedErrors();
        //        }
        //}
        protected bool doCreateLabItem(SAPbouiCOM.Form oForm, ref IDH_LABITM oLabItem, ref IDH_LITMDTL oLIDetail, FilterGrid oProvDGrid, int iHRow, FilterGrid oProvHGrid) {
            try {
                oLabItem = new IDH_LABITM();
                oLIDetail = new IDH_LITMDTL();

                NumbersPair oNextLINo = oLabItem.getNewKey();
                oLabItem.Code = oNextLINo.CodeCode;
                oLabItem.Name = oNextLINo.NameCode;
                oLabItem.U_ObjectCd = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, iHRow).ToString();
                oLabItem.U_ObjectType = "LPP";
                oLabItem.U_LICode = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WastCd, iHRow).ToString();
                oLabItem.U_LIName = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WastDsc, iHRow).ToString();
                oLabItem.U_ItemCode = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ItemCd, iHRow).ToString();
                oLabItem.U_Saved = 1;
                //if (sUOM.Length > 0)
                oLabItem.U_UOM = "";

                //if (sQty.Length > 0)
                oLabItem.U_Quantity = Convert.ToDouble(oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WastQty, iHRow));
                oLabItem.U_CntnrQty = Convert.ToDouble(oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._CntnrQty, iHRow));

                //if (sWhse.Length > 0)
                oLabItem.U_NewWhse = "";// sWhse;

                //if (sComments.Length > 0)
                oLabItem.U_Comments = "";//sComments;

                oLabItem.U_CreatedBy = goParent.gsUserName;
                oLabItem.U_CreatedOn = DateTime.Now;

                //Fill line items 
                NumbersPair oNextLIDNo;
                for (int i = 0; i <= oProvDGrid.getRowCount() - 1; i++) {
                    try {
                        if (oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._Batch).ToString() == string.Empty)
                            continue;
                        oNextLIDNo = oLIDetail.getNewKey();
                        oLIDetail.Code = oNextLIDNo.CodeCode;
                        oLIDetail.Name = oNextLIDNo.NameCode;
                        oLIDetail.U_LabCd = oLabItem.Code;

                        oLIDetail.U_PItemCd = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._WastCd).ToString();
                        oLIDetail.U_PItemNm = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._WastDsc).ToString();
                        oLIDetail.U_UOM = "";// oNewItemGridN.doGetFieldValue("NI.SalUnitMsr", i).ToString();
                        oLIDetail.U_Quantity = "1";// oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._q).ToString();
                        oLIDetail.U_Whse = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._WhsCode).ToString();// oNewItemGridN.doGetFieldValue("NI.FrgnName", i).ToString();
                        oLIDetail.U_Batch = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._Batch).ToString();
                        oLIDetail.U_AbsEntry = (int)oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._AbsEntry);
                        oLIDetail.U_PrvBtDCd = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._Code).ToString();
                        oLIDetail.U_PrvBtHCd = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._PRVHCD).ToString();
                        oLIDetail.doAddDataRow();
                    } catch (Exception ex) {
                        com.idh.bridge.DataHandler.INSTANCE.doError("doCreateLabItem()", "Error adding Lab Item Detail row: " + ex.Message);
                    }
                }

                oLabItem.doAddDataRow();
                iLabItemCd = Convert.ToInt32(oLabItem.Code);
                return true;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doCreateLabItem()", "Error adding Lab Item Main row: " + ex.Message);
                iLabItemCd = -1;
                return false;
            }
        }
        protected bool doCreateLabTask(SAPbouiCOM.Form oForm, ref IDH_LABITM oLabItem, FilterGrid oProvDGrid, int iHRow, FilterGrid oProvHGrid) {
            try {

                IDH_LABTASK oLabTask = new IDH_LABTASK();
                NumbersPair oNxtNo = oLabTask.getNewKey();
                oLabTask.Code = oNxtNo.CodeCode;
                oLabTask.Name = oNxtNo.NameCode;

                oLabTask.U_LabItemRCd = oLabItem.Code;
                oLabTask.U_LabICd = oLabItem.U_LICode;
                oLabTask.U_LabINm = oLabItem.U_LIName;

                //oLabTask.U_BPCode = ""; //No BP while creating the Lab Task 
                //oLabTask.U_BPName = "";

                oLabTask.U_RowStatus = "Requested"; //Need to upgrade this code with dbOjects [@IDH_LABSTATS]
                oLabTask.U_DtRequested = DateTime.Now;

                //oLabTask.U_DtAnalysed = DateTime.Now; //will be updated by user 

                //oLabTask.U_Department = "";
                oLabTask.U_AssignedTo = "";
                oLabTask.U_AnalysisReq = "";
                oLabTask.U_ExtStatus = "";
                oLabTask.U_ExtComments = "";
                //oLabTask.U_AtcAbsEntry = -1;
                oLabTask.U_ImpResults = "";
                oLabTask.U_Decision = "Pending";
                oLabTask.U_ObjectCd = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, iHRow).ToString(); //moWOQ.WOQItems.getValue(iRow, IDH_WOQITEM._Code).ToString();
                oLabTask.U_DocNum = oProvHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, iHRow).ToString();// "Header Num";
                oLabTask.U_ObjectType = "LPP";

                oLabTask.U_Saved = 1;
                oLabTask.doAddDataRow();
                iLabTaskCd = Convert.ToInt32(oLabTask.Code);
                return true;
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doCreateLabTask()", "Error adding Lab Task row: " + ex.Message);
                iLabTaskCd = -1;
                return false;
            }

        }
        #endregion

        #region "Create Final Batch"
        private void doHandleFinalBatchClick(SAPbouiCOM.Form oForm) {
            try {

                string sMsg = "";
                if (getButtonStatus(oForm, "IDH_FDPRVB") == Translation.getTranslatedWord("Update")) {
                    com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("You have made some changes in provisional batch. Please save the changes made before creating final batch."));
                    return;
                }
                UpdateGrid oProvisionalHGrid = (UpdateGrid)FilterGrid.getInstance(oForm, "PROVHGRID");
                ArrayList aSelectRow = oProvisionalHGrid.getSelectedRows();
                if (aSelectRow.Count == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Please select a provisional batch to create final batch."));
                    return;
                }
                int iCRow = (int)aSelectRow[0];
                if (oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WastCd, iCRow).ToString() == "") {
                    com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Please provide the waste code for final batch."));
                    oProvisionalHGrid.getSBOGrid().SetCellFocus(iCRow, oProvisionalHGrid.doFieldIndex("prvh." + IDH_PVHBTH._WastCd));
                    return;
                }
                if (oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._DisRCode, iCRow).ToString() == "") {
                    com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Selected waste item does not have disposal route code. Please select a valid waste item."));
                    oProvisionalHGrid.getSBOGrid().SetCellFocus(iCRow, oProvisionalHGrid.doFieldIndex("prvh." + IDH_PVHBTH._WastCd));
                    return;
                } else {
                    IDH_DISPRTCD oDispRtCd = new IDH_DISPRTCD();
                    int iRet = oDispRtCd.getData(IDH_DISPRTCD._DisRCode + " = \'" + oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._DisRCode, iCRow).ToString() + " \' And " + IDH_DISPRTCD._Active + " =\'Y\' ", "");
                    if (iRet == 0) {
                        com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Route code " + oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._DisRCode, iCRow).ToString() + " is not active. Please select a valid waste code with active route code."));
                        return;
                    }
                }
                if (oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ItemCd, iCRow).ToString() == "") {
                    com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Please provide the container code for final batch."));
                    oProvisionalHGrid.getSBOGrid().SetCellFocus(iCRow, oProvisionalHGrid.doFieldIndex("prvh." + IDH_PVHBTH._ItemCd));
                    return;
                } else {
                    string sItemcode = oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ItemCd, iCRow).ToString();
                    object sItemByStock = Config.INSTANCE.getValueFromOITMWithNoBuffer(sItemcode, "InvntItem");
                    if (sItemByStock == null || sItemByStock.ToString() != "Y") {
                        com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Container item is not a stock item. Please select a valid stock item."));
                        oProvisionalHGrid.getSBOGrid().SetCellFocus(iCRow, oProvisionalHGrid.doFieldIndex("prvh." + IDH_PVHBTH._ItemCd));
                        return;
                    }
                }
                if (oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._BatchTyp, iCRow).ToString() == "") {
                    com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Please select the batch type."));
                    oProvisionalHGrid.getSBOGrid().SetCellFocus(iCRow, oProvisionalHGrid.doFieldIndex("prvh." + IDH_PVHBTH._BatchTyp));
                    return;
                }
                if (oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Warehouse, iCRow).ToString() == "") {
                    com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Please enter a valid warehouse."));
                    oProvisionalHGrid.getSBOGrid().SetCellFocus(iCRow, oProvisionalHGrid.doFieldIndex("prvh." + IDH_PVHBTH._Warehouse));
                    return;
                }
                if ((double)oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._CntnrQty, iCRow) <= 0) {
                    sMsg = "Container Quantity is missing. Please enter the quantity.";
                    com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord(sMsg));
                    oProvisionalHGrid.getSBOGrid().SetCellFocus(iCRow, oProvisionalHGrid.doFieldIndex("prvh." + IDH_PVHBTH._CntnrQty));
                    return;
                }
                if ((double)oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WastQty, iCRow) <= 0) {
                    sMsg = "Waste Quantity is missing. Are you sure to create final batch?";
                    if (com.idh.bridge.resources.Messages.INSTANCE.doMessage(sMsg, 1, "Yes", "No") != 1) {
                        oProvisionalHGrid.getSBOGrid().SetCellFocus(iCRow, oProvisionalHGrid.doFieldIndex("prvh." + IDH_PVHBTH._WastQty));
                        return;
                    }
                }
                if (oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._LabTskCd, iCRow).ToString() != string.Empty && oProvisionalHGrid.doGetFieldValue("labtask." + IDH_LABTASK._Decision, iCRow).ToString() != "Accepted") {
                    sMsg = "You cannot create final batch as the Lab task is not accepted.";
                    com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord(sMsg));
                    //oProvisionalHGrid.getSBOGrid().SetCellFocus(iCRow, oProvisionalHGrid.doFieldIndex("prvh." + IDH_PVHBTH._CntnrQty));
                    return;
                }
                sMsg = "This process cannot be undo. Are you sure to create final batch?";
                if (com.idh.bridge.resources.Messages.INSTANCE.doMessage(sMsg, 1, "Yes", "No") != 1)
                    return;

                UpdateGrid oProvDGrid = UpdateGrid.getInstance(oForm, "PROVDGRID");
                if (oProvDGrid.getRowCount() == 0 || (oProvDGrid.getRowCount() == 1 && oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._Code, 0).ToString() == "")) {
                    com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("There is no item avaiable in provisional batch to create final batch."));
                    return;
                }
                for (int i = 0; i <= oProvDGrid.getRowCount() - 1; i++) {
                    if (oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ItemCd, iCRow).ToString() == oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._ItemCd, i).ToString()) {
                        com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Final Batch Container must be different from the provisioanl batch containers."));
                        oProvisionalHGrid.getSBOGrid().SetCellFocus(iCRow, oProvisionalHGrid.doFieldIndex("prvh." + IDH_PVHBTH._ItemCd));
                        return;
                    }
                }

                doCreateFinalBatch(oForm, oProvisionalHGrid, oProvDGrid, iCRow);
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doHandleFinalBatchClick()", "Error in Final Batch Create: " + ex.Message);
            }
        }
        private bool doCreateFinalBatch(SAPbouiCOM.Form oForm, UpdateGrid oProvisionalHGrid, UpdateGrid oProvDGrid, int iCRow) {
            bool bResult = true;
            try {

                DataHandler.INSTANCE.StartTransaction();
                int iRet = 0;
                string sResult = "";
                //0: Unlock Batches
                for (int i = 0; i <= oProvDGrid.getRowCount() - 1; i++) {
                    if (oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._ItemCd, i).ToString().Trim() != "")
                        doUpdateBatchStatus(oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._AbsEntry, i).ToString(), SAPbobsCOM.BoDefaultBatchStatus.dbs_Released, oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._PRVHCD, i).ToString());
                }
                //1: Create Production Order
                SAPbobsCOM.ProductionOrders oProductionOrder = (SAPbobsCOM.ProductionOrders)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);
                string sProductionOrderNo = "";//docentry
                string sProductionOrderNumber = "";//docnum
                object oProductionOrderNo = Config.INSTANCE.getValueFromTablebyKey("OWOR", "DocEntry", "U_PRVHCD", oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, iCRow).ToString(), " And (OWOR.Status='P' Or OWOR.Status='R') ", "");
                if (oProductionOrderNo != null && oProductionOrderNo.ToString() != string.Empty) {
                    sProductionOrderNo = oProductionOrderNo.ToString();
                    if (oProductionOrder.GetByKey(Convert.ToInt32(sProductionOrderNo))) {
                        iRet = oProductionOrder.Cancel();
                        if (iRet != 0) {
                            sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
                            com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Production Order cancel Error - " + sResult + " Error while cancelling the old production order#" + sProductionOrderNo + ".", "ERPRD001",
                                new string[] { sResult });
                            bResult = false;
                            return false;
                        }
                        oProductionOrder = (SAPbobsCOM.ProductionOrders)com.idh.bridge.DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);
                        //iRet = oProductionOrder.Update();
                        //if (iRet != 0) {
                        //    sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
                        //    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Production Order Add Error - " + sResult + " Error while adding the production order.", "ERPRD001",
                        //        new string[] { sResult });
                        //    bResult = false;
                        //    return false;
                        //}
                        sProductionOrderNo = "";
                    }
                }
                if (sProductionOrderNo == string.Empty) {
                    oProductionOrder.ProductionOrderType = SAPbobsCOM.BoProductionOrderTypeEnum.bopotSpecial;
                    oProductionOrder.ItemNo = oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ItemCd, iCRow).ToString();
                    oProductionOrder.PlannedQuantity = (double)oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._CntnrQty, iCRow);
                    oProductionOrder.Warehouse = oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Warehouse, iCRow).ToString();
                    oProductionOrder.PostingDate = DateTime.Today;
                    oProductionOrder.DueDate = DateTime.Today;
                    oProductionOrder.Remarks = "Provisional Batch:" + oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, iCRow).ToString();
                    oProductionOrder.UserFields.Fields.Item("U_PRVHCD").Value = oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, iCRow).ToString();
                    for (int i = 0; i <= oProvDGrid.getRowCount() - 1; i++) {
                        if (oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._ItemCd, i).ToString() != string.Empty) {
                            oProductionOrder.Lines.SetCurrentLine(oProductionOrder.Lines.Count - 1);
                            oProductionOrder.Lines.ItemNo = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._ItemCd, i).ToString();
                            oProductionOrder.Lines.PlannedQuantity = 1;// oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._ItemCd, i).ToString();
                            oProductionOrder.Lines.UserFields.Fields.Item("U_PRVDCD").Value = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._Code, i).ToString();
                            oProductionOrder.Lines.UserFields.Fields.Item("U_Batch").Value = oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._Batch, i).ToString();

                            object BatchWh = Config.INSTANCE.getValueFromTablebyKey("OBTQ", "WhsCode", "MdAbsEntry", oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._AbsEntry, i).ToString(), " And OBTQ.Quantity>0 ", " Order by OBTQ.AbsEntry desc ");
                            // BatchWh = Config.INSTANCE.getValueFromTablebyKey("OBTQ", "WhsCode", "MdAbsEntry", oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._AbsEntry, i).ToString());//, " And OBTQ.Quantity>0 ", " Order by OBTQ.AbsEntry desc ");
                            if (BatchWh == null || BatchWh.ToString() == "") {
                                com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("System failed to find warehouse for batch ") + oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._Batch, i));
                                return false;
                            }
                            oProductionOrder.Lines.Warehouse = BatchWh.ToString();
                            //Config.INSTANCE.getValueFromTablebyKey("OBTQ","WhsCode" ,"MdAbsEntry",oProvDGrid.doGetFieldValue("prvd." + IDH_PVDBTH._AbsEntry, i).ToString())
                            //Select top 1 bw.* from OBTQ bw,OBTN batch where bw.MdAbsEntry=batch.AbsEntry Order by bw.AbsEntry desc
                            oProductionOrder.Lines.Add();
                        }
                    }

                    sResult = "";
                    //oProductionOrder.ProductionOrderStatus = SAPbobsCOM.BoProductionOrderStatusEnum.boposReleased;
                    iRet = oProductionOrder.Add();
                    if (iRet != 0) {
                        sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Production Order Add Error - " + sResult + " Error while adding the production order.", "ERPRD001",
                            new string[] { sResult });
                        bResult = false;
                        return false;
                    } else {
                        sProductionOrderNo = DataHandler.INSTANCE.SBOCompany.GetNewObjectKey();
                        com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText("Production Order number " + sProductionOrderNo + " added. Please wait while system is busy in completion of process.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                    }
                }
                //if prod order added as released then no need below
                if (sProductionOrderNo == "" || oProductionOrder.GetByKey(Convert.ToInt32(sProductionOrderNo)) == false) {
                    com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Invalid Production Order"));
                    bResult = false;
                    return false;
                }
                sProductionOrderNumber = oProductionOrder.DocumentNumber.ToString();
                oProductionOrder.ProductionOrderStatus = SAPbobsCOM.BoProductionOrderStatusEnum.boposReleased;
                iRet = oProductionOrder.Update();
                if (iRet != 0) {
                    sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Production Order Update Error - " + sResult + " Error while update the production order status to release.", "ERPRD001",
                        new string[] { sResult });
                    bResult = false;
                    return false;
                }
                //
                //Issue from production
                SAPbobsCOM.Documents IssueProdOrd;
                IssueProdOrd = (SAPbobsCOM.Documents)(DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit));
                string sBatchProcess = "";
                for (int i = 0; i <= oProductionOrder.Lines.Count - 1; i++) {
                    oProductionOrder.Lines.SetCurrentLine(i);
                    IssueProdOrd.Lines.SetCurrentLine(IssueProdOrd.Lines.Count - 1);
                    IssueProdOrd.Lines.BaseEntry = Convert.ToInt32(sProductionOrderNo);
                    IssueProdOrd.Lines.BaseLine = oProductionOrder.Lines.LineNumber; //LineNum from WOR1 table;
                    IssueProdOrd.Lines.BaseType = 202; // 202 means it is an Issue For Production. -1 means it is a Goods Issue
                    IssueProdOrd.Lines.Quantity = 1;// Planned Quantity from WOR1 table

                    //IssueProdOrd.Lines.ItemCode = oProductionOrder.Lines.ItemNo;//may be we dont need this line
                    sBatchProcess = Config.INSTANCE.getValueFromOITM(oProductionOrder.Lines.ItemNo, idh.dbObjects.User.OITM._ManBtchNum).ToString();
                    if (sBatchProcess != null && sBatchProcess == "Y") {
                        //check if item is batch managed then execute code                        
                        IssueProdOrd.Lines.BatchNumbers.BatchNumber = oProductionOrder.Lines.UserFields.Fields.Item("U_Batch").Value.ToString();
                        //IssueProdOrd.Lines.BatchNumbers.InternalSerialNumber
                        //IssueProdOrd.Lines.BatchNumbers.BaseLineNumber = 1;// oProductionOrder.Lines.UserFields.Fields.Item("U_Batch").Value.ToString();
                        IssueProdOrd.Lines.BatchNumbers.Quantity = 1;
                        IssueProdOrd.Lines.BatchNumbers.SetCurrentLine(0);
                        //IssueProdOrd.Lines.BatchNumbers.Add();
                    }
                    IssueProdOrd.Lines.Quantity = 1;
                    IssueProdOrd.Lines.WarehouseCode = oProductionOrder.Lines.Warehouse;
                    //IssueProdOrd.Lines.BatchNumbers.ManufacturerSerialNumber= 1;
                    IssueProdOrd.Lines.Add();
                }
                iRet = IssueProdOrd.Add();
                if (iRet != 0) {
                    sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Issue for Production Add Error - " + sResult + " Error while adding the issue for production.", "ERPRD002",
                        new string[] { sResult });
                    bResult = false;
                    return false;
                } else {
                    com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText("Issue for production added successfully. Please wait while system is busy in completion of process.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                    //    sProductionOrderNo = DataHandler.INSTANCE.SBOCompany.GetNewObjectKey();
                }

                //here Receive from production
                SAPbobsCOM.Documents ReceiveProdOrd;
                ReceiveProdOrd = (SAPbobsCOM.Documents)(DataHandler.INSTANCE.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry));

                ReceiveProdOrd.Lines.BaseEntry = Convert.ToInt32(sProductionOrderNo);// doc_entry
                ReceiveProdOrd.Lines.BaseType = 202;
                ReceiveProdOrd.Lines.Quantity = oProductionOrder.PlannedQuantity;
                ReceiveProdOrd.Lines.TransactionType = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                ReceiveProdOrd.Lines.WarehouseCode = oProductionOrder.Warehouse;
                sBatchProcess = Config.INSTANCE.getValueFromOITM(oProductionOrder.Lines.ItemNo, idh.dbObjects.User.OITM._ManBtchNum).ToString();
                if (sBatchProcess != null && sBatchProcess == "Y") {
                    //check if item is batch managed then execute code
                    for (int i = 0; i <= Math.Ceiling(oProductionOrder.PlannedQuantity) - 1; i++) {
                        ReceiveProdOrd.Lines.BatchNumbers.Quantity = 1;
                        ReceiveProdOrd.Lines.BatchNumbers.ManufacturerSerialNumber = oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WastCd, iCRow).ToString();//oOrderRow.U_WasCd;
                        ReceiveProdOrd.Lines.BatchNumbers.InternalSerialNumber = oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._DisRCode, iCRow).ToString();//oOrderRow.U_RouteCd;
                        //Note:BatchNumber must be unique in company
                        ReceiveProdOrd.Lines.BatchNumbers.BatchNumber = "FINAL." + oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._BatchTyp, iCRow).ToString() + "." + oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, iCRow).ToString() + "/" + (i + 1).ToString(); // oOrderRow.U_JobNr + "." + oOrderRow.Code + "/" + (iBtnm + 1).ToString();
                        ReceiveProdOrd.Lines.BatchNumbers.SetCurrentLine(i);
                        ReceiveProdOrd.Lines.BatchNumbers.Add();
                        //ReceiveProdOrd.Lines.BatchNumbers.BatchNumber = oProductionOrder.Lines.UserFields.Fields.Item("U_Batch").Value.ToString();
                        ////IssueProdOrd.Lines.BatchNumbers.BaseLineNumber = 1;// oProductionOrder.Lines.UserFields.Fields.Item("U_Batch").Value.ToString();
                        //ReceiveProdOrd.Lines.BatchNumbers.Quantity = 1;
                        //ReceiveProdOrd.Lines.BatchNumbers.SetCurrentLine(i);
                        //IssueProdOrd.Lines.BatchNumbers.Add();
                    }
                }
                iRet = ReceiveProdOrd.Add();
                if (iRet != 0) {
                    sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Receive from Production Add Error - " + sResult + " Error while adding the receive from production.", "ERPRD003",
                        new string[] { sResult });
                    bResult = false;
                    return false;
                } else {
                    com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText("Receive from production added successfully. Please wait while system is busy in completion of process.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                    //    sProductionOrderNo = DataHandler.INSTANCE.SBOCompany.GetNewObjectKey();
                }

                /*not required
                ReceiveProdOrd.Lines.Add();
                ReceiveProdOrd.Lines.BaseEntry = Convert.ToInt32(sProductionOrderNo);// doc_entry
                ReceiveProdOrd.Lines.BaseType = 202;
                ReceiveProdOrd.Lines.Quantity = 1;
                ReceiveProdOrd.Lines.TransactionType = SAPbobsCOM.BoTransactionTypeEnum.botrntReject
                ReceiveProdOrd.Lines.WarehouseCode = "02";
                iRet= ReceiveProdOrd.Add();*/

                //complete production order
                oProductionOrder.GetByKey(Convert.ToInt32(sProductionOrderNo));
                oProductionOrder.ProductionOrderStatus = SAPbobsCOM.BoProductionOrderStatusEnum.boposClosed;
                iRet = oProductionOrder.Update();
                if (iRet != 0) {
                    sResult = idh.bridge.Translation.getTranslatedWord(DataHandler.INSTANCE.SBOCompany.GetLastErrorDescription());
                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Production Order Completion Error - " + sResult + " Error while adding the completing the production order.", "ERPRD004",
                        new string[] { sResult });
                    bResult = false;
                    return false;
                } else {
                    //    sProductionOrderNo = DataHandler.INSTANCE.SBOCompany.GetNewObjectKey();
                    com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText("Production order closed successfully. Please wait while system is busy in completion of process.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                }
                //oProvisionalHGrid.doSetFieldValue("prvh." + IDH_PVHBTH._FinalBt, iCRow, "Y");
                IDH_PVHBTH oProvBatch = new IDH_PVHBTH();
                oProvBatch.UnLockTable = true;
                if (oProvBatch.getByKey(oProvisionalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, iCRow).ToString())) {
                    oProvBatch.U_FinalBt = "Y";
                    oProvBatch.U_BatchDt = DateTime.Today;
                    oProvBatch.U_ProdDoc = sProductionOrderNo;
                    oProvBatch.U_ProdNum = sProductionOrderNumber;
                    bResult = oProvBatch.doProcessData("Prodcution Order Number " + sProductionOrderNumber + ";Prod DocEntry=" + sProductionOrderNo);
                }
                if (bResult = true && DataHandler.INSTANCE.IsInTransaction()) {
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    com.idh.bridge.DataHandler.INSTANCE.Application.StatusBar.SetText("Final batch created successfully.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                }
                doHandleFindProvisinalGrid(oForm, false);
                doHandleFinalGridFind(oForm, true);
                return true;
            } catch (Exception ex) {
                bResult = false;
                com.idh.bridge.DataHandler.INSTANCE.doError("doCreateFinalBatch()", "Error in Final Batch Create: " + ex.Message);
            } finally {
                if (bResult == false && DataHandler.INSTANCE.IsInTransaction()) {
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                }
            }
            return true;
        }
        #endregion

        #region Create WOH and WORs
        private void doHandleWOsCreateClick(SAPbouiCOM.Form oForm) {
            try {
                if (getButtonStatus(oForm, "IDH_FDPRVB") != Translation.getTranslatedWord("Find")) {
                    com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("You have made some changes in provisional batch(s). Please save or cancel the changes."));
                    return;
                }
                FilterGrid oFinalHGrid = FilterGrid.getInstance(oForm, "FNLVHGRID");
                if (doValidateForWOs(oFinalHGrid) == false)
                    return;
                if (doCreateWOH(oFinalHGrid)) {
                    oFinalHGrid.doReloadData("", false, false);
                    oFinalHGrid.doApplyRules();
                    oFinalHGrid.doRecount();
                }

            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doHandleWOsCreateClick()", "Error handle button Create WO(s): " + ex.Message);
            }
        }
        private bool doCreateWOH(FilterGrid oFinalHGrid) {
            bool bRet = true;
            try {
                ArrayList aSelectedRows = oFinalHGrid.getSelectedRows();
                if (aSelectedRows.Count == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Please select at least one row from final batch to create WO."));
                    return false;
                }
                string sWOH = "";
                string sWOR = "";
                for (int isel = 0; isel <= aSelectedRows.Count - 1; isel++) {
                    int i = -1;
                    i = (int)aSelectedRows[isel];
                    if ((double)oFinalHGrid.doGetFieldValue("v1.TotalAvailableQty", i) <= 0) {
                        com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("No container avaiable for final batch#" + oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, i).ToString()));
                        bRet = false;
                        return false;
                    }
                }

                IDH_PVHBTH oFinalBatch = new IDH_PVHBTH();
                com.idh.dbObjects.User.IDH_JOBENTR oWOH = new com.idh.dbObjects.User.IDH_JOBENTR();
                NumbersPair oWOHNumbers = oWOH.getNewKey();
                oWOH.Code = oWOHNumbers.CodeCode;
                oWOH.Name = oWOHNumbers.NameCode;
                sWOH = oWOH.Code;
                DataHandler.INSTANCE.StartTransaction();
                double dTemp;
                for (int isel = 0; isel <= aSelectedRows.Count - 1; isel++) {

                    string sTipCost = "";

                    int i = -1;
                    i = (int)aSelectedRows[isel];
                    if (oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ItemCd, i).ToString() == "") {
                        continue;
                    }
                    IDH_DISPRTCD oDispRtCd = new IDH_DISPRTCD();
                    oDispRtCd.UnLockTable = true;
                    int iRet = oDispRtCd.getData(IDH_DISPRTCD._DisRCode + " = \'" + oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._DisRCode, i).ToString() + " \' And " + IDH_DISPRTCD._Active + " =\'Y\' ", "");
                    if (isel == 0) {
                        if (iRet > 0) {
                            if (string.IsNullOrWhiteSpace(oDispRtCd.U_TAddress)) {
                                //oWOH.doSetProducer(oDispRtCd.U_TipCd, true);
                                oWOH.doSetProducer(oDispRtCd.U_TipCd);
                                //oWOH.doSetDisposalSite(oDispRtCd.U_TipCd, true);
                                oWOH.doSetDisposalSite(oDispRtCd.U_TipCd);
                            } else {
                                oWOH.doSetProducer(oDispRtCd.U_TipCd, oDispRtCd.U_TAddress);
                               // oWOH.doSetDisposalSite(oDispRtCd.U_TipCd, oDispRtCd.U_TAddress, false);
                                oWOH.doSetDisposalSite(oDispRtCd.U_TipCd, oDispRtCd.U_TAddress);
                            }

                            //oWOH.U_WOQRID = oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, i);
                            if (oWOH.doAddDataRow("By Lab Pre-Process") == false) {
                                com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("failed to add WOH for final batch#" + oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, i).ToString()));
                                bRet = false;
                                return false;
                            }
                        }
                    }

                    //pass data to woh and wors objects
                    IDH_JOBSHD moWOR = new IDH_JOBSHD(oWOH);
                    moWOR.doAddEmptyRow(true, true);
                    string msNextRowNum = "JOBSCHE";
                    NumbersPair oNumbers = DataHandler.INSTANCE.doGenerateCode(IDH_JOBSHD.AUTONUMPREFIX, msNextRowNum);
                    moWOR.Code = oNumbers.CodeCode;
                    moWOR.Name = oNumbers.NameCode;
                    moWOR.BlockChangeNotif = true;
                    if (iRet > 0) {
                        sTipCost = oDispRtCd.U_TipCost.ToString();
                        // dHaulageCost = oDispRtCd.U_HaulgCost.ToString();
                        //   moWOR.U_CarrCd = oDispRtCd.U_hau;
                        //   oWOH.doSetDisposalSite(oDispRtCd.U_FnlTipCd, true);
                    }
                    moWOR.U_User = DataHandler.INSTANCE.SBOCompany.UserName;
                    moWOR.U_BDate = DateTime.Now;//moParentWOQ.U_BDate;
                    moWOR.U_BTime = com.idh.utils.Dates.doTimeToNumStr();

                    string sLinkedItemCode = Config.INSTANCE.getValueFromOITM(oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ItemCd, i).ToString(), idh.dbObjects.User.OITM._WTITMLN).ToString();
                    moWOR.U_ItemCd = sLinkedItemCode;//oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ItemCd, i).ToString();
                    moWOR.U_ItemDsc = Config.INSTANCE.getValueFromOITM(sLinkedItemCode, "ItemName").ToString(); //oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ItemDsc, i).ToString();

                    moWOR.U_WasCd = oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WastCd, i).ToString();
                    moWOR.U_WasDsc = oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WastDsc, i).ToString();

                    moWOR.U_JobTp = Config.INSTANCE.getParameterWithDefault("DFWORJPTY", "Delivery");

                    moWOR.U_RDate = DateTime.Today;
                    moWOR.U_TRdWgt = (double)oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._WastQty, i);
                    moWOR.U_CarWgt = (double)oFinalHGrid.doGetFieldValue("v1.TotalAvailableQty", i);
                    moWOR.BlockChangeNotif = false;
                    //if (dHaulageCost != null && double.TryParse(dHaulageCost.Trim(), out dTemp) ) {
                    if (sTipCost != null && double.TryParse(sTipCost.Trim(), out dTemp)) {
                        moWOR.U_TipCost = Convert.ToDouble(sTipCost);
                        moWOR.doMarkUserUpdate(IDH_JOBSHD._TipCost);
                        moWOR.doCalculateCostVat('C');
                    }
                    moWOR.doGetCostPrices(false, true, true, true);
                    moWOR.U_LABFBTCD = oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, i).ToString();
                    moWOR.doCalculateProfit();
                    sWOR = moWOR.Code;
                    double dWORContainerQty = moWOR.U_TRdWgt;
                    bRet = moWOR.doAddDataRow();
                    if (bRet == true) {
                        oFinalBatch.UnLockTable = true;
                        bRet = oFinalBatch.getByKey(oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, i).ToString());
                        if (bRet == false) {
                            com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("failed to load final batch#" + oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, i).ToString()));
                            return false;
                        }
                        oFinalBatch.U_WORCode = sWOR;
                        oFinalBatch.U_WOHCode = sWOH;
                        //oFinalBatch.U_WORCntQty += dWORContainerQty;
                        bRet = oFinalBatch.doProcessData();
                        if (bRet == false) {
                            com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("failed to update final batch#" + oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, i).ToString()));
                            return false;
                        }
                    } else {
                        com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("failed to add WOR for final batch#" + oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._Code, i).ToString()));
                        return false;
                    }
                }
                bRet = true;
                if (bRet == true) {
                    if (DataHandler.INSTANCE.IsInTransaction())
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doCreateWOH()", "Error handle Create WO(s): " + ex.Message);
                bRet = false;
            } finally {
                if (bRet == false) {
                    if (DataHandler.INSTANCE.IsInTransaction())
                        DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                } else if (DataHandler.INSTANCE.IsInTransaction())
                    DataHandler.INSTANCE.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
            }
            return bRet;
        }
        private bool doValidateForWOs(FilterGrid oFinalHGrid) {
            try {
                ArrayList aSelectedRows = oFinalHGrid.getSelectedRows();
                if (aSelectedRows.Count == 0) {
                    com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Please select at least one row from final batch to create WO."));
                    return false;
                }
                string sDsiposalCd = "";

                for (int isel = aSelectedRows.Count - 1; isel >= 0; isel--) {
                    int i = -1;
                    i = (int)aSelectedRows[isel];
                    if (oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ItemCd, i).ToString() == "") {
                        continue;
                    }
                    string sLinkedItemCode = Config.INSTANCE.getValueFromOITM(oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ItemCd, i).ToString(), idh.dbObjects.User.OITM._WTITMLN).ToString();
                    if (string.IsNullOrEmpty(sLinkedItemCode)) {
                        com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Container " + oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ItemCd, i).ToString() + " does not have a non-stock linked item."));
                        return false;
                    }
                    if (Config.INSTANCE.isStockItem(sLinkedItemCode)) {
                        com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Container " + oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._ItemCd, i).ToString() + " does not have a non-stock linked item."));
                        return false;
                    }
                    if (oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._DisRCode, i).ToString() == "") {
                        com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Selected waste item does not have disposal route code. Please select a valid waste item."));
                        //oFinalHGrid.getSBOGrid().CommonSetting.(i, oFinalHGrid.doFieldIndex("prvh." + IDH_PVHBTH._WastCd));
                        return false;
                    } else if (oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._DisRCode, i).ToString() != "") {
                        IDH_DISPRTCD oDispRtCd = new IDH_DISPRTCD();
                        int iRet = oDispRtCd.getData(IDH_DISPRTCD._DisRCode + " = \'" + oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._DisRCode, i).ToString() + " \' And " + IDH_DISPRTCD._Active + " =\'Y\' ", "");
                        if (iRet == 0) {
                            com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Failed to find route code " + oFinalHGrid.doGetFieldValue("prvh." + IDH_PVHBTH._DisRCode, i).ToString() + " or it is is not active."));
                            return false;
                        } else {
                            if (sDsiposalCd != string.Empty && sDsiposalCd != oDispRtCd.U_TipCd) {
                                com.idh.bridge.DataHandler.INSTANCE.doError(Translation.getTranslatedWord("Select route code(s) have different final disposal sites. You cannot select disposal routs with different final disposal sites."));
                                return false;
                            } else
                                sDsiposalCd = oDispRtCd.U_TipCd;
                        }
                    }
                }
            } catch (Exception ex) {
                com.idh.bridge.DataHandler.INSTANCE.doError("doValidateForWOs()", "Error validating for WO(s): " + ex.Message);
                return false;
            }
            return true;
        }
        #endregion
    }
}
