Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System.Net
Imports System.Web
Imports System
Imports com.idh.utils.Conversions
Imports com.idh.bridge
Imports com.idh.dbObjects.User
Imports com.idh.bridge.lookups
Imports com.idh.dbObjects.numbers
Imports com.idh.bridge.res

Namespace idh.forms.manager
    Public Class Compliance
        Inherits idh.forms.manager.Disposal

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, iMenuPosition, "IDHCM", "Compliance Manager.srf", "Compliance Manager")
        End Sub

        '        Protected Overrides Function getTitle() As String
        '            Return "Compliance Manager"
        '        End Function

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.UpdateGrid)
            oGridN.doAddListField("r.U_Status", "Sales Status", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_JobNr", "Order No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.Code", "Row No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_BDate", "Booking Date", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ENREF", "Evidence Note", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_SAINV", "Invoice", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Invoice)
            oGridN.doAddListField("r.U_SAORD", "Sales Order", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Order)
            oGridN.doAddListField("r.U_WROrd", "WO No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_WRRow", "WO Row No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_JobTp", "Order Type", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Lorry", "Vehicle Reg.", False, 50, Nothing, Nothing)
            oGridN.doAddListField("r.U_VehTyp", "Vehicle", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CustCs", "Compliance Scheme", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_CarrCd", "Carrier Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_CarrNm", "Carrier Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_CardCd", "Customer Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("e.U_CardNM", "Customer Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItemCd", "Container Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("r.U_ItemDsc", "Container Desc.", False, 90, Nothing, Nothing, -1)
            oGridN.doAddListField("r.U_WasCd", "Waste Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("r.U_WasDsc", "Waste Description", False, 50, Nothing, Nothing)
            oGridN.doAddListField("r.U_RdWgt", "Read Weight", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CstWgt", "Charge Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_UOM", "Charge UOM", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCharge", "Tip Charge", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCTotal", "Tip Total", False, -1, Nothing, Nothing)

            ''oGridN.doAddListField("(U_TCTotal +(U_CusQty * U_CusChr)) - r.U_DisAmt", "Sub After Discount", False, -1, Nothing, "SubBefDisc")
            oGridN.doAddListField("CAST(((U_TCTotal + U_Price) - r.U_DisAmt) As Numeric(19,2))", "Sub After Discount", False, -1, Nothing, "SubAftDisc")
            oGridN.doAddListField("r.U_DisAmt", "Discount Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AddEx", "Additional Expenses", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TaxAmt", "Sales Vat Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Total", "Total Charge", False, -1, Nothing, Nothing)

            oGridN.doAddListField("r.U_PayMeth", "Payment Method", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_PayStat", "Payment Status", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CustRef", "Customer Ref No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Tip", "Disposal Site", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)

            oGridN.doAddListField("r.U_LorryCd", "Vehicle Code.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipCost", "TipCost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PStat", "Purchase Status", False, 0, Nothing, Nothing)
            'oGridN.doAddListField("e.U_CardCd", "Customer Code", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            'oGridN.doAddListField("e.U_CardNM", "Customer Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_Street", "Street", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_Block", "Block", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_ZpCd", "Post Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_CntrNo", "Contract No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RDate", "Req. Start", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RTime", "Req. Time", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_ASDate", "Act. Start", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ASTime", "Act. Time", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_AEDate", "Act. End", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AETime", "Act ETime", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_ItmGrp", "Container Grp", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_ItemGroups)
            oGridN.doAddListField("r.U_Serial", "Item Serial No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicSp", "SLicSp", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicNr", "Skip Lic. No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicExp", "Skip Exp.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Driver", "Driver", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipWgt", "Sup. Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Price", "Price", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicCh", "SLicCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_Address", "Contact Person", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_SteId", "Site Id", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BTime", "Booking Time", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipTot", "TipTot", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CongCh", "CongCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DocNum", "DocNum", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Discnt", "Discnt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SupRef", "Supplier Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PARCPT", "Payment", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Receipt)

            oGridN.doAddListField("r.U_CCNum", "CC Num.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CCType", "CC Type", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CCStat", "CC Status", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_TChrgVtRt", "Tipping Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtRt", "Haulage Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtRt", "Tipping Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCostVtRt", "Supplier Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtRt", "Haulage Vat Purchase Rate", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_TChrgVtGrp", "Tipping Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtGrp", "Haulage Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtGrp", "Tipping Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCostVtGrp", "Supplier Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtGrp", "Haulage Vat Purchase Group", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_VtCostAmt", "Purchase Vat Amount", False, 0, Nothing, Nothing)

        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            MyBase.doCompleteCreate(oForm, BubbleEvent)
            oForm.Items.Item("IDH_CERT").Visible = True
            oForm.Items.Item("IDH_ENOTE").Visible = True
        End Sub

        Protected Overrides Sub doHandleModalResultShared(ByVal oForm As SAPbouiCOM.Form, ByVal sModalFormType As String, Optional ByVal sLastButton As String = Nothing)
            If sModalFormType = "IDHENGEN" Then
                Dim sENREF As String = getSharedData(oForm, "ENREF")
                If sLastButton = "IDH_DUPL" Then
                    doDuplicateEN(oForm, sENREF)
                ElseIf sLastButton = "IDH_SUBS" Then
                    doPrepareSubstitudeEN(oForm, sENREF)
                End If
            End If
        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim bResult As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK Then
                If pVal.ItemUID = "LINESGRID" Then
                    Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                    If pVal.Row >= 0 AndAlso oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                        If pVal.BeforeAction = False Then
                            If oGridN.doCheckIsSameCol(pVal.ColUID, "r.U_ENREF") Then
                                Dim sENREF As String = oGridN.doGetFieldValue("r.U_ENREF")
                                If Not sENREF Is Nothing AndAlso sENREF.Length > 0 Then
                                    setSharedData(oForm, "ENREF", sENREF)
                                    goParent.doOpenModalForm("IDHENGEN", oForm)

                                    'doEvidenceReport(oForm, sENREF, "D")

                                    ''Dim aENCodes As New ArrayList

                                    ''Dim sENReport As String = Config.Parameter( "MNENRP")
                                    ''Dim oParams As New Hashtable

                                    ''aENCodes.Add(sENREF)
                                    ''oParams.Add("EnNum", aENCodes)
                                    ''idh.report.Base.doCallReport(goParent, oForm, sENReport, "TRUE", "FALSE", "FALSE", "1", oParams)
                                End If
                                Return True
                            End If
                        End If
                    End If
                End If
            End If
            bResult = MyBase.doItemEvent(oForm, pVal, BubbleEvent)
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = False Then
                    If pVal.ItemUID = "IDH_CERT" Then
                        doCertBtn(oForm)
                    ElseIf pVal.ItemUID = "IDH_ENOTE" Then
                        doPrepareNewEN(oForm)
                    End If
                End If
            End If
            Return bResult
        End Function

        Protected Sub doDuplicateEN(ByVal oForm As SAPbouiCOM.Form, ByVal sOrigRef As String)
            Dim sQry As String
            sQry = "select * from [@IDH_WTENHD] e " &
                   "where e.U_REFNUM = '" & IDHAddOns.idh.data.Base.doFixForSQL(sOrigRef) & "' "

            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing

            Dim oENHeadNew As IDHAddOns.idh.data.AuditObject = Nothing
            Dim oENRowNew As IDHAddOns.idh.data.AuditObject = Nothing
            Try
                'Dim iHeadCode As Integer
                'Dim sHeadCode As String
                Dim sRefPrefix As String = Config.Parameter("ENREFP")
                Dim sValRef As String
                Dim iwResult As Integer
                Dim swResult As Integer

                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, "ENHDSEQ")
                sValRef = sRefPrefix & oNumbers.CodeCode

                oRecordSet = goParent.goDB.doSelectQuery(sQry)
                If Not oRecordSet Is Nothing AndAlso oRecordSet.RecordCount > 0 Then
                    oENHeadNew = New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_WTENHD")
                    oENRowNew = New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_WTENRW")

                    oENHeadNew.setKeyPair(oNumbers.CodeCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
                    oENHeadNew.setName(oNumbers.NameCode)
                    oENHeadNew.setFieldValue("U_REFNUM", sValRef)

                    Dim sVal As String = oRecordSet.Fields.Item("U_OPERATOR").Value
                    oENHeadNew.setFieldValue("U_OPERATOR", sVal)
                    sVal = oRecordSet.Fields.Item("U_SITENAME").Value
                    oENHeadNew.setFieldValue("U_SITENAME", sVal)
                    sVal = oRecordSet.Fields.Item("U_SITEADDR").Value
                    oENHeadNew.setFieldValue("U_SITEADDR", sVal)
                    sVal = oRecordSet.Fields.Item("U_SCHNAME").Value
                    oENHeadNew.setFieldValue("U_SCHNAME", sVal)
                    sVal = oRecordSet.Fields.Item("U_SCHOPER").Value
                    oENHeadNew.setFieldValue("U_SCHOPER", sVal)

                    Dim daVal As Date
                    daVal = oRecordSet.Fields.Item("U_STARTDAT").Value
                    oENHeadNew.setFieldValue("U_STARTDAT", daVal)
                    daVal = oRecordSet.Fields.Item("U_ENDDATE").Value
                    oENHeadNew.setFieldValue("U_ENDDATE", daVal)

                    Dim iCount As Integer = oRecordSet.Fields.Item("U_COUNT").Value
                    oENHeadNew.setFieldValue("U_COUNT", iCount + 1)
                    oENHeadNew.setFieldValue("U_ACTION", "D")
                    oENHeadNew.setFieldValue("U_BREFNUM", sOrigRef)

                    iwResult = oENHeadNew.Commit()

                    If iwResult <> 0 Then
                        goParent.goDICompany.GetLastError(iwResult, swResult)
                        'com.idh.bridge.DataHandler.INSTANCE.doError("Error Duplicating the Head: " + swResult)
                        com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Duplicating the Head: " + swResult, "ERSYDUPH", {com.idh.bridge.Translation.getTranslatedWord(swResult)})
                    Else
                        sQry = "select * from [@IDH_WTENRW] where U_REFNUM = '" & IDHAddOns.idh.data.Base.doFixForSQL(sOrigRef) & "' "
                        oRecordSet = goParent.goDB.doSelectQuery(sQry)
                        If Not oRecordSet Is Nothing AndAlso oRecordSet.RecordCount > 0 Then
                            Dim iSize As Integer = oRecordSet.RecordCount
                            For iRow As Integer = 0 To iSize - 1
                                'Dim iRowCode As Integer = goParent.goDB.doNextNumber("ENRWSEQ")
                                oNumbers = DataHandler.INSTANCE.doGenerateCode(Nothing, "ENRWSEQ")

                                oENRowNew.setKeyPair(oNumbers.CodeCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
                                oENRowNew.setName(oNumbers.NameCode)

                                oENRowNew.setFieldValue("U_REFNUM", sValRef)

                                sVal = oRecordSet.Fields.Item("U_ENCAT").Value
                                oENRowNew.setFieldValue("U_ENCAT", sVal)
                                sVal = oRecordSet.Fields.Item("U_ENCATD").Value
                                oENRowNew.setFieldValue("U_ENCATD", sVal)

                                Dim dVal As Double
                                sVal = oRecordSet.Fields.Item("U_ENIPA").Value
                                oENRowNew.setFieldValue("U_ENIPA", sVal)
                                sVal = oRecordSet.Fields.Item("U_ENOPA").Value
                                oENRowNew.setFieldValue("U_ENOPA", sVal)

                                dVal = oRecordSet.Fields.Item("U_ENITR").Value
                                oENRowNew.setFieldValue("U_ENITR", dVal)
                                dVal = oRecordSet.Fields.Item("U_ENITRU").Value
                                oENRowNew.setFieldValue("U_ENITRU", dVal)
                                dVal = oRecordSet.Fields.Item("U_ENOTRC").Value
                                oENRowNew.setFieldValue("U_ENOTRC", dVal)
                                dVal = oRecordSet.Fields.Item("U_ENOTRU").Value
                                oENRowNew.setFieldValue("U_ENOTRU", dVal)
                                dVal = oRecordSet.Fields.Item("U_ENOTWA").Value
                                oENRowNew.setFieldValue("U_ENOTWA", dVal)

                                iwResult = oENRowNew.Commit()
                                If iwResult <> 0 Then
                                    goParent.goDICompany.GetLastError(iwResult, swResult)
                                    'com.idh.bridge.DataHandler.INSTANCE.doError("Error Duplicating the Row: " + swResult)
                                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Duplicating the Row: " + swResult, "ERSYDUPR", {com.idh.bridge.Translation.getTranslatedWord(swResult)})
                                End If
                                oRecordSet.MoveNext()
                            Next

                            sQry = "UPDATE [@IDH_DISPROW] set U_ENREF = '" & sValRef & "' WHERE U_ENREF = '" & IDHAddOns.idh.data.Base.doFixForSQL(sOrigRef) & "'"
                            goParent.goDB.doUpdateQuery(sQry)

                            doEvidenceReport(oForm, sValRef, "N")

                            doReLoadData(oForm, True)
                        End If
                    End If

                End If
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Preparing for Duplication.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXDBD", Nothing)
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
                IDHAddOns.idh.data.Base.doReleaseObject(oENHeadNew)
                IDHAddOns.idh.data.Base.doReleaseObject(oENRowNew)
            End Try
        End Sub

        Protected Sub doPrepareSubstitudeEN(ByVal oForm As SAPbouiCOM.Form, ByVal sOrigRef As String)
            Dim sQry As String
            sQry = "select e.U_STARTDAT, e.U_ENDDATE, r.Code from [@IDH_WTENHD] e, [@IDH_DISPROW] r " &
                   "where e.U_REFNUM = '" & IDHAddOns.idh.data.Base.doFixForSQL(sOrigRef) & "' " &
                   "AND e.U_REFNUM = r.U_ENREF"

            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                Dim sListDo As String = ""
                oRecordSet = goParent.goDB.doSelectQuery(sQry)
                If Not oRecordSet Is Nothing Then
                    Dim sStartDate As String = goParent.doDateToStr(oRecordSet.Fields.Item(0).Value)
                    Dim sEndDate As String = goParent.doDateToStr(oRecordSet.Fields.Item(1).Value)
                    Dim iSize As Integer = oRecordSet.RecordCount
                    For iC As Integer = 0 To iSize - 1
                        If iC > 0 Then
                            sListDo = sListDo & ","
                        End If
                        sListDo = sListDo & "'" & oRecordSet.Fields.Item(2).Value & "'"
                        oRecordSet.MoveNext()
                    Next

                    doEvidenceNote(oForm, sListDo, sStartDate, sEndDate, sOrigRef)
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Preparing for Substitution.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPRES", Nothing)
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
            End Try
        End Sub

        Protected Sub doPrepareNewEN(ByVal oForm As SAPbouiCOM.Form)
            Try
                Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
                Dim oSelectedRows As SAPbouiCOM.SelectedRows = oGridN.getGrid.Rows.SelectedRows
                If oSelectedRows Is Nothing OrElse oSelectedRows.Count = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("No rows selected.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("No rows selected.", "ERUSROWS", Nothing)
                    Exit Sub
                End If

                Dim sStartDate As String = getUFValue(oForm, "IDH_BOOSTF")
                If sStartDate.Length = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("The Startdate is a required field.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("StartDate must be set", "ERUSGENS", {com.idh.bridge.Translation.getTranslatedWord("StartDate")})
                    Exit Sub
                End If

                Dim sEndDate As String = getUFValue(oForm, "IDH_BOOSTT")
                If sEndDate.Length = 0 Then
                    'com.idh.bridge.DataHandler.INSTANCE.doError("The Enddate is a required field.")
                    com.idh.bridge.DataHandler.INSTANCE.doResUserError("EndDate must be set", "ERUSGENS", {com.idh.bridge.Translation.getTranslatedWord("EndDate")})
                    Exit Sub
                End If

                Dim iSelected As Integer = oSelectedRows.Count
                Dim iRowNum As Integer
                Dim sRow As String
                Dim sENCode As String
                Dim dRdWgt As Double
                Dim sListDo As String = ""
                Dim aListDontRow As New ArrayList

                For iIndex As Integer = 0 To iSelected - 1
                    'iRowNum = oSelectedRows.Item(iIndex, SAPbouiCOM.BoOrderType.ot_SelectionOrder)
                    'iRowNum = oGridN.getGrid.GetDataTableRowIndex(iRowNum)
                    iRowNum = oGridN.getDataTableRowIndex(oSelectedRows, iIndex)

                    sRow = oGridN.doGetFieldValue("r.Code", iRowNum)
                    sENCode = oGridN.doGetFieldValue("r.U_ENREF", iRowNum)
                    dRdWgt = oGridN.doGetFieldValue("r.U_RdWgt", iRowNum)

                    If (sENCode Is Nothing OrElse sENCode.Length = 0) _
                        AndAlso dRdWgt > 0 Then
                        If sListDo.Length > 0 Then
                            sListDo = sListDo & ","
                        End If
                        sListDo = sListDo & "'" & sRow & "'"
                    Else
                        aListDontRow.Add(iRowNum)
                    End If
                Next

                If aListDontRow.Count > 0 Then
                    '                    Dim sMessage As String = ""
                    '                    If aListDontRow.Count() > 1 Then
                    '                        sMessage = 	getTranslatedWord("The following") & " " & _
                    '									aListDontRow.Count() & " " & _
                    '									getTranslatedWord("rows") & " (" & doCreateCSL(aListDontRow) & ") " & _
                    '									getTranslatedWord("will not be processed, because they have already been processed or are incomplete.") & Chr(10)
                    '                    Else
                    '                        sMessage = 	getTranslatedWord("The following row") & " (" & doCreateCSL(aListDontRow) & ") " & _
                    '                        			getTranslatedWord("will not be processed, because it has already been processed or is incomplete.") & Chr(10)
                    '                    End If
                    '                    goParent.goApplication.M_essageBox(sMessage, 1, getTranslatedWord("Ok"))
                    Dim sMessageId As String
                    Dim sParams As String()
                    If aListDontRow.Count() > 1 Then
                        sMessageId = "MANPICRS"
                        sParams = New String() {aListDontRow.Count().ToString(), doCreateCSL(aListDontRow)}
                    Else
                        sMessageId = "MANPICR"
                        sParams = New String() {doCreateCSL(aListDontRow)}
                    End If
                    Messages.INSTANCE.doResourceMessage(sMessageId, sParams, 1, com.idh.bridge.Translation.getTranslatedWord("Ok"))
                    For iIndex As Integer = 0 To aListDontRow.Count - 1
                        oSelectedRows.Remove(aListDontRow(iIndex))
                    Next
                End If

                doEvidenceNote(oForm, sListDo, sStartDate, sEndDate, Nothing)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Preparing the New Evidence Note.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXPNEN", Nothing)
            End Try
        End Sub

        Protected Overridable Sub doEvidenceNote(ByVal oForm As SAPbouiCOM.Form, ByVal sList As String, ByVal sStartDate As String, ByVal sEndDate As String, ByVal sOrigRef As String)

            Dim oAuditObjHead As IDHAddOns.idh.data.AuditObject = Nothing
            Dim oAuditObjRow As IDHAddOns.idh.data.AuditObject = Nothing
            Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
            Try
                If sList.Length > 0 Then
                    Dim sQry As String
                    sQry = "SELECT		h.U_CardNM AS CustName, " &
                     "  h.U_CardCd AS CustCode, " &
                     "  i.U_WTENCAT AS 'QryGroup', " &
                     "  i.U_WTENIPA AS 'InPA', " &
                     "  i.U_WTENOPA AS 'OutPA', " &
                     "  c.U_Desc AS CATDesc, " &
                     "  SUM(ROUND(r.U_RdWgt / 1000, 2)) AS ReadWeight, " &
                     "  SUM(ROUND(r.U_RdWgt / 1000, 2) * i.U_WTENITU/100) AS INUsed, " &
                     "  SUM(ROUND(r.U_RdWgt / 1000, 2) * i.U_WTENOTRC/100) AS OUTRecycle, " &
                     "  SUM(ROUND(r.U_RdWgt / 1000, 2) * i.U_WTENOTRU/100) AS OUTReuse, " &
                     "  SUM(ROUND(r.U_RdWgt / 1000, 2) * i.U_WTENOWA/100) AS Wastage " &
                     "  FROM         [@IDH_DISPROW] AS r, OITM i, [@IDH_DISPORD] AS h, [@IDH_WTENCT] c " &
                     "  WHERE  r.U_WasCd = i.ItemCode " &
                     "     AND r.U_JobNr = h.Code " &
                     "     AND i.U_WTENCAT = c.Code " &
                     "     AND r.U_JobTp = N'incoming' " &
                     "     AND h.U_BDate >='" & sStartDate & "' AND h.U_BDate <='" & sEndDate & "' " &
                     "     AND r.Code IN (" & sList & ") " &
                     "     AND r.U_RdWgt != 0 " &
                     "  GROUP BY i.U_WTENCAT, i.U_WTENIPA, i.U_WTENOPA, c.U_Desc, h.U_CardCd, h.U_CardNM " &
                     "  ORDER BY h.U_CardCd, 'QryGroup', 'InPA', 'OutPA'"

                    oRecordSet = goParent.goDB.doSelectQuery(sQry)

                    Dim sCustomerCode As String = ""
                    'Dim iHeadCode As Integer
                    'Dim sHeadCode As String = Nothing
                    Dim sRefPrefix As String = Config.Parameter("ENREFP")

                    Dim sValCustCd As String = Nothing
                    Dim sValCustName As String = Nothing
                    Dim sValRef As String = Nothing
                    Dim iwResult As Integer
                    Dim swResult As String = Nothing

                    Dim aENCodes As New ArrayList
                    Dim aENCust As New ArrayList

                    If Not oRecordSet Is Nothing AndAlso oRecordSet.RecordCount > 0 Then
                        oAuditObjHead = New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_WTENHD")
                        oAuditObjRow = New IDHAddOns.idh.data.AuditObject(goParent, "@IDH_WTENRW")

                        For iRow As Integer = 0 To oRecordSet.RecordCount - 1
                            sValCustCd = oRecordSet.Fields.Item("CustCode").Value

                            If sCustomerCode.Equals(sValCustCd) = False Then
                                sCustomerCode = sValCustCd
                                'iHeadCode = goParent.goDB.doNextNumber("ENHDSEQ")
                                'sHeadCode = iHeadCode
                                'sHeadCode = sHeadCode.PadLeft(6, "0")
                                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, "ENHDSEQ")
                                sValRef = sRefPrefix & oNumbers.CodeCode
                                sValCustName = oRecordSet.Fields.Item("CustName").Value

                                oAuditObjHead.setKeyPair(oNumbers.CodeCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
                                oAuditObjHead.setName(oNumbers.NameCode)
                                oAuditObjHead.setFieldValue("U_REFNUM", sValRef)
                                oAuditObjHead.setFieldValue("U_OPERATOR", "Global Environmental Recycling Company Limited")
                                oAuditObjHead.setFieldValue("U_SITENAME", "Unit D Maritime Business Park")
                                oAuditObjHead.setFieldValue("U_SITEADDR", "Campbelltown Road, Birkenhead" & ToChar(10) & "Merseyside CH41 9HP")
                                oAuditObjHead.setFieldValue("U_SCHNAME", sValCustName)
                                oAuditObjHead.setFieldValue("U_SCHOPER", sValCustCd)
                                oAuditObjHead.setFieldValue("U_STARTDAT", com.idh.utils.Dates.doStrToDate(sStartDate))
                                oAuditObjHead.setFieldValue("U_ENDDATE", com.idh.utils.Dates.doStrToDate(sEndDate))

                                If sOrigRef Is Nothing Then
                                    oAuditObjHead.setFieldValue("U_COUNT", 0)
                                    oAuditObjHead.setFieldValue("U_ACTION", "C")
                                Else
                                    oAuditObjHead.setFieldValue("U_COUNT", 1)
                                    oAuditObjHead.setFieldValue("U_ACTION", "S")
                                    oAuditObjHead.setFieldValue("U_BREFNUM", sOrigRef)
                                End If

                                iwResult = oAuditObjHead.Commit()

                                If iwResult <> 0 Then
                                    goParent.goDICompany.GetLastError(iwResult, swResult)
                                    'com.idh.bridge.DataHandler.INSTANCE.doError("Error Adding Head: " + swResult)
                                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Adding Head: " + swResult, "ERSYADDH", {com.idh.bridge.Translation.getTranslatedWord(swResult)})
                                Else
                                    aENCodes.Add(sValRef)
                                    aENCust.Add(sValCustCd)
                                End If
                            End If

                            If iwResult = 0 Then
                                Dim irResult As Integer
                                Dim srResult As String = Nothing

                                'Dim iRowCode As Integer = goParent.goDB.doNextNumber("ENRWSEQ")
                                Dim oNumbers As NumbersPair = DataHandler.INSTANCE.doGenerateCode(Nothing, "ENRWSEQ")

                                Dim sValCategory As String = oRecordSet.Fields.Item("QryGroup").Value
                                Dim sValCatDesc As String = oRecordSet.Fields.Item("CATDesc").Value
                                Dim sValInAP As String = oRecordSet.Fields.Item("InPA").Value
                                Dim sValOutAP As String = oRecordSet.Fields.Item("OutPA").Value

                                Dim dValInReceived As Double = oRecordSet.Fields.Item("ReadWeight").Value
                                Dim dValInReUsed As Double = oRecordSet.Fields.Item("INUsed").Value

                                Dim dValOutRecovery As Double = oRecordSet.Fields.Item("OUTRecycle").Value
                                Dim dValOutReUsed As Double = oRecordSet.Fields.Item("OUTReuse").Value
                                Dim dValOutWaste As Double = oRecordSet.Fields.Item("Wastage").Value

                                oAuditObjRow.setKeyPair(oNumbers.CodeCode, IDHAddOns.idh.data.AuditObject.ac_Types.idh_ADD)
                                oAuditObjRow.setName(oNumbers.NameCode)

                                oAuditObjRow.setFieldValue("U_REFNUM", sValRef)
                                oAuditObjRow.setFieldValue("U_ENCAT", sValCategory)
                                oAuditObjRow.setFieldValue("U_ENCATD", sValCatDesc)
                                oAuditObjRow.setFieldValue("U_ENIPA", sValInAP)
                                oAuditObjRow.setFieldValue("U_ENOPA", sValOutAP)
                                oAuditObjRow.setFieldValue("U_ENITR", dValInReceived)
                                oAuditObjRow.setFieldValue("U_ENITRU", dValInReUsed)
                                oAuditObjRow.setFieldValue("U_ENOTRC", dValOutRecovery)
                                oAuditObjRow.setFieldValue("U_ENOTRU", dValOutReUsed)
                                oAuditObjRow.setFieldValue("U_ENOTWA", dValOutWaste)

                                irResult = oAuditObjRow.Commit()
                                If irResult <> 0 Then
                                    goParent.goDICompany.GetLastError(irResult, srResult)
                                    'com.idh.bridge.DataHandler.INSTANCE.doError("Error Adding Row: " + srResult)
                                    com.idh.bridge.DataHandler.INSTANCE.doResSystemError("Error Adding Row: " + srResult, "ERSYADDR", {com.idh.bridge.Translation.getTranslatedWord(srResult)})
                                End If
                            End If
                            oRecordSet.MoveNext()
                        Next
                    End If
                    If aENCodes.Count > 0 Then
                        'Update the rows
                        For iEN As Integer = 0 To aENCodes.Count - 1
                            Dim sUpQry As String
                            sUpQry = "UPDATE [@IDH_DISPROW] set U_ENREF = '" & aENCodes.Item(iEN) & "' WHERE U_CustCd = '" & aENCust.Item(iEN) & "' And Code IN (" & sList & ")"
                            goParent.goDB.doUpdateQuery(sUpQry)
                        Next

                        For iEN As Integer = 0 To aENCodes.Count - 1
                            doEvidenceReport(oForm, aENCodes(iEN), "N")
                        Next

                        doReLoadData(oForm, True)
                    End If
                End If

            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error Creating the Evidence Note.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCNEN", Nothing)
            Finally
                IDHAddOns.idh.data.Base.doReleaseObject(oAuditObjHead)
                IDHAddOns.idh.data.Base.doReleaseObject(oAuditObjRow)
                IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
            End Try
        End Sub

        Protected Sub doEvidenceReport(ByVal oForm As SAPbouiCOM.Form, ByVal sENRef As String, ByVal sAction As String)
            Dim sENReport As String = Config.Parameter("MNENRP")
            Dim oParams As New Hashtable
            oParams.Add("EnNum", "")
            oParams.Item("EnNum") = sENRef
            If Config.INSTANCE.getParameterAsBool("UNEWRPTV", False) Then
                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sENReport)
                IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
            Else
                IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sENReport, "TRUE", "FALSE", "1", oParams, gsType)
            End If
        End Sub

        Protected Overridable Sub doCertBtn(ByVal oForm As SAPbouiCOM.Form)
            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
            Dim sReportFile As String
            sReportFile = Config.Parameter("MNCRTRP")

            Dim oParams As New Hashtable
            oParams.Add("WONumber", "")

            Dim oList As New ArrayList
            oList = oGridN.doPrepareListFromSelection("r.Code")
            oParams.Add("RowNum", oList)
            If Config.INSTANCE.getParameterAsBool("UNEWRPTV", False) Then
                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTPARMS", oParams)
                IDHAddOns.idh.forms.Base.setSharedData(oForm, "IDH_RPTNAME", sReportFile)
                IDHAddOns.idh.forms.Base.PARENT.doOpenForm("IDH_CUSRPT", oForm, False)
            Else
                IDHAddOns.idh.report.Base.doCallReportDefaults(oForm, sReportFile, "TRUE", "FALSE", "1", oParams, gsType)
            End If
        End Sub

        '        Protected Overrides Sub doDocBtn(ByVal oForm As SAPbouiCOM.Form)
        '            Dim oGridN As FilterGrid = FilterGrid.getInstance(oForm, "LINESGRID")
        '
        '            'Get Documents selected
        '            Dim sList As String = oGridN.doPrepareCSLFromSelection("r.Code")
        '            If sList.Length > 0 Then
        '                Dim oRecordSet As SAPbobsCOM.Recordset = Nothing
        '
        '                Dim oList As ArrayList = Nothing
        '                Dim sReportFile As String = Nothing
        '                Dim sReportType As String = ""
        '                Dim oReports As New Hashtable
        '                Dim sCardCode As String = Nothing
        '                Dim sWasteCode As String = Nothing
        '
        '                Try
        '                    Dim sQry As String
        '                    sQry = "SELECT r.Code, r.U_JobNr, r.U_WasCd, i.U_WTDOCUM, U_CustCd FROM [@IDH_DISPROW] r, OITM i " & _
        '                            "WHERE(r.U_WasCd = i.ItemCode) " & _
        '                            "AND r.Code in (" & sList & ") " & _
        '                            "Order by i.U_WTDOCUM "
        '
        '                    oRecordSet = goParent.goDB.doSelectQuery(sQry)
        '                    Dim sReportTypeTst As String
        '                    Dim sRowCode As String
        '
        '                    If Not oRecordSet Is Nothing Then
        '                        Dim iSize As Integer = oRecordSet.RecordCount
        '                        For iC As Integer = 0 To iSize - 1
        '                            sRowCode = oRecordSet.Fields.Item(0).Value
        '                            sWasteCode = oRecordSet.Fields.Item(2).Value
        '                            sReportTypeTst = oRecordSet.Fields.Item(3).Value
        '                            sCardCode = oRecordSet.Fields.Item(4).Value
        '
        '                            If sReportTypeTst Is Nothing OrElse sReportTypeTst.Length = 0 Then
        '                                sReportTypeTst = "DEFAULT"
        '                            End If
        '
        '                            If sReportType = sReportTypeTst Then
        '                                oList.Add(sRowCode)
        '                            Else
        '                                If Not oList Is Nothing AndAlso oList.Count > 0 Then
        '                                    oReports.Add(sReportType, oList)
        '                                End If
        '
        '                                sReportType = sReportTypeTst
        '                                oList = New ArrayList
        '                                oList.Add(sRowCode)
        '                            End If
        '
        '                            If iC = iSize - 1 Then
        '                                oReports.Add(sReportTypeTst, oList)
        '                            End If
        '
        '                            oRecordSet.MoveNext()
        '                        Next
        '                    End If
        '                Catch ex As Exception
        '                   com.idh.bridge.DataHandler.INSTANCE.doError("doDocBtn", "Exception: " & ex.ToString, "Error doing the report")
        '                Finally
        '                    IDHAddOns.idh.data.Base.doReleaseObject(oRecordSet)
        '                End Try
        '
        '                'Step through all the report types and print the list of that type
        '                If oReports.Count > 0 Then
        '                    Dim col As ICollection = oReports.Keys
        '                    Dim en As IEnumerator = col.GetEnumerator
        '
        '                    While en.MoveNext
        '                        sReportType = en.Current()
        '                        oList = oReports.Item(sReportType)
        '                        If sReportType = "DEFAULT" Then
        '                            sReportFile = Nothing
        '                        Else
        '                            sReportFile = Config.Parameter( sReportType)
        '                        End If
        '
        '                        If sReportFile Is Nothing OrElse sReportFile.Length = 0 Then
        '                        	Dim oData As ArrayList = WR.idh.const.Lookup.INSTANCE.doGetDOCReportLevel1a(goParent, "DO", sCardCode, sWasteCode, True)
        '                            sReportFile = oData.Item(0)
        '                            
        '                            If oData.Item(1) = True Then
        '                            	'....
        '                            End If
        '                        End If
        '
        '                        Dim oParams As New Hashtable
        '                        oParams.Add("WONumber", "")
        '                        oParams.Add("RowNum", oList)
        '                        idh.report.Base.doCallReport(goParent, oForm, sReportFile, "TRUE", "FALSE", "FALSE", "1", oParams)
        '                    End While
        '                End If
        '            End If
        '        End Sub
    End Class
End Namespace
