Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System.Net
Imports System.Web
Imports System

Imports com.idh.bridge.data
Imports WR1_Grids.idh.controls.grid
Imports com.idh.bridge.lookups

Namespace idh.forms.manager
    Public Class ConsignmentWO
    	Inherits idh.forms.manager.Order

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, iMenuPosition, "IDHCONMWO", "WOConsignmentManager.srf", "Consignment Manager WO")
        End Sub
        
        '** Create the form
        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                oForm.Title = gsTitle
                
                Dim oGridN As OrderRowGrid = New OrderRowGrid(Me, oForm, "LINESGRID", 7, 70, 800, 345, "r")

                oGridN.getSBOGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto
                oForm.AutoManaged = False
                oGridN.getSBOItem().AffectsFormMode = False

                oForm.EnableMenu(Config.NAV_ADD, False)
                oForm.EnableMenu(Config.NAV_FIND, False)
                oForm.EnableMenu(Config.NAV_FIRST, False)
                oForm.EnableMenu(Config.NAV_NEXT, False)
                oForm.EnableMenu(Config.NAV_PREV, False)
                oForm.EnableMenu(Config.NAV_LAST, False)

                Dim fSettings As SAPbouiCOM.FormSettings
                fSettings = oForm.Settings
                fSettings.Enabled = False
                
                'doAddTotalFields(oForm)

                'oForm.SupportedModes = SAPbouiCOM.BoFormMode.fm_FIND_MODE Or SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                oForm.SupportedModes = SAPbouiCOM.BoAutoFormMode.afm_All
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", Nothing)
                BubbleEvent = False
            End Try
        End Sub

		Public Overrides Sub doSetTables(ByVal oGridN As FilterGrid)
        	'Return "[@IDH_JOBSHD] r, [@IDH_JOBENTR] e, IDH_VPROGRESS v, OITM i, CRD1 a "
        	'Added from the Head
        	'oGridN.doAddGridTable( New GridTable("@IDH_JOBSHD", "r", "Code", True), True )
        	'oGridN.doAddGridTable( New GridTable("@IDH_JOBENTR", "e", "Code", True) )
        	'oGridN.doAddGridTable( New GridTable("IDH_VPROGRESS", "v") )
            oGridN.doAddGridTable(New GridTable("OITM", "i", Nothing, False, True))
            oGridN.doAddGridTable(New GridTable("CRD1", "a", Nothing, False, True))
        End Sub        
        
        Public Overrides Function getListRequiredStr(oForm As SAPbouiCOM.Form) As String
            'Return "r.U_JobNr = e.Code And v.Code = r.Code And e.U_Status in ('1','2','3','4','5') And i.ItemCode = r.U_WasCd And a.CardCode = e.U_CardCd And a.AdresType = 'S' And a.Address = e.U_Address  AND (i.U_HAZCD IS NOT NULL AND i.U_HAZCD != '')" 'SELECT U_IDHPCD, * FROM CRD1 WHERE CardCode = 'C-A0003' AND AdresType = 'S' AND Address = ''
            Return "r.U_JobNr = e.Code And v.Code = r.Code And e.U_Status in ('1','2','3','4','5') " & _
                        "And i.ItemCode = r.U_WasCd And a.CardCode = e.U_CardCd And a.AdresType = 'S' " & _
                        "And a.Address = e.U_Address  AND (i.U_HAZCD IS NOT NULL AND i.U_HAZCD != '') AND bp.CardCode = r.U_CustCd "
            'SELECT U_IDHPCD, * FROM CRD1 WHERE CardCode = 'C-A0003' AND AdresType = 'S' AND Address = ''
        End Function

		Protected Overrides Sub doSetGridFilters(ByVal oGridN As FilterGrid)
            oGridN.doAddFilterField("IDH_REQSTF", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_REQSTT", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_CUST", "e.U_CardCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ITMGRP", "r.U_ItmGrp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 255)
            oGridN.doAddFilterField("IDH_VEHREG", "r.U_Lorry", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            
            oGridN.doAddFilterField("IDH_ACTSTF", "r.U_ASDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_ACTSTT", "r.U_ASDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_NAME", "e.U_CardNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WSCD", "r.U_WasCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WSDC", "r.U_WasDsc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            
            oGridN.doAddFilterField("IDH_PROG", "v.State", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)            
            oGridN.doAddFilterField("IDH_ADDR", "e.U_Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 255)
            oGridN.doAddFilterField("IDH_POSTCD", "e.U_ZpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ROWNO", "r.Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
			oGridN.doAddFilterField("IDH_ORDNO", "r.U_JobNr", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)

            oGridN.doAddFilterField("IDH_BRANCH", "r.U_Branch", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 255)
			oGridN.doAddFilterField("IDH_HAZCN", "r.U_ConNum", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_PREMCD", "e.U_PremCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_PREMCD", "a.U_IDHPCD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ON", "r.U_WROrd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_OR", "r.U_WRRow", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
                        
			'oGridN.doAddFilterField("IDH_STATUS", "r.U_Status", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_PSTAT", "r.U_PStat", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_PROD", "r.U_ItemCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_JOBTYP", "r.U_JobTp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_DRIVER", "r.U_Driver", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_CUSPHO", "e.U_Phone1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_SITETL", "e.U_SiteTl", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_REF", "r.U_CustRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_USER", "r.U_User", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_STEID", "e.U_SteId", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_STREET", "e.U_Street", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_CARCD", "r.U_CarrCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_CARNM", "r.U_CarrNm", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)            
		End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.UpdateGrid)
        	oGridN.doAddListField("v.State", "Progress", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_JobNr", "Job No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.Code", "Job Row No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_WROrd", "Do No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_WRRow", "Do Row No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ASDate", "Act. Start", False,-1, Nothing, Nothing)
            oGridN.doAddListField("e.U_BDate", "Booking Date", False, 0, Nothing, Nothing) ' DO
            oGridN.doAddListField("r.U_ConNum", "Consignment Number", True, -1, Nothing, Nothing)
            oGridN.doAddListField("a.U_IDHPCD", "Premesis Code", False, 50, Nothing, Nothing)
            oGridN.doAddListField("a.Address", "Site Address", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_ZpCd", "Post Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItemDsc", "Container Desc.", False, -1, Nothing, Nothing, -1)
            oGridN.doAddListField("i.U_EWC", "EWC Code", False, -1, Nothing, Nothing) 'From Item - U_EWC
            oGridN.doAddListField("i.U_HAZCD", "Hazard", False, -1, Nothing, Nothing) 'From Item - U_HAZCD
            oGridN.doAddListField("i.U_WTUNSTE", "Physical", False, -1, Nothing, Nothing) 'From Item - U_WTUNSTE
            oGridN.doAddListField("r.U_RdWgt", "Kilos", False,-1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CstWgt", "Charge Qty", False, 0, Nothing, Nothing)
            oGridN.doAddListField("i.U_WTDIMTH", "Mode Of Disposal", False, -1, Nothing, Nothing) 'From Item - U_WTDIMTH
            
            oGridN.doAddListField("r.U_Status", "Sales Status", False, 0, Nothing, Nothing)
            oGridN.doAddListField("' '", "S/M", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AEDate", "Act. End", False, 0, Nothing, Nothing) ' WO
            
            'oGridN.doAddListField("a.U_IDHPCD", "Producer Registration", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SAINV", "Invoice", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Invoice)
            oGridN.doAddListField("r.U_SAORD", "Sales Order", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Order)
            oGridN.doAddListField("r.U_JobTp", "Order Type", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Lorry", "Vehicle Reg.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_VehTyp", "Vehicle", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CarrCd", "Carrier Code", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_CarrNm", "Carrier Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_CardCd", "Customer Code", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("e.U_CardNM", "Customer Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItemCd", "Container Code", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            
            oGridN.doAddListField("r.U_WasCd", "Waste Code", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("r.U_WasDsc", "Waste Description", False, 0, Nothing, Nothing)
            
            oGridN.doAddListField("r.U_CstWgt", "Charge Qty", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_UOM", "Charge UOM", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCharge", "Tip Charge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCTotal", "Tip Total", False, 0, Nothing, Nothing)
			oGridN.doAddListField("r.U_ENREF", "Evidence Note", False, 0, Nothing, Nothing)
            ''oGridN.doAddListField("(U_TCTotal +(U_CusQty * U_CusChr)) - r.U_DisAmt", "Sub After Discount", False, -1, Nothing, "SubBefDisc")
            oGridN.doAddListField("CAST(((U_TCTotal + U_Price) - r.U_DisAmt) As Numeric(19,2))", "Sub After Discount", False, 0, Nothing, "SubAftDisc")
            oGridN.doAddListField("r.U_DisAmt", "Discount Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AddEx", "Additional Expenses", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TaxAmt", "Sales Vat Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Total", "Total Charge", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_PayMeth", "Payment Method", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PayStat", "Payment Status", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CustRef", "Customer Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Tip", "Disposal Site", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)

            oGridN.doAddListField("r.U_LorryCd", "Vehicle Code.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipCost", "TipCost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PStat", "Purchase Status", False, 0, Nothing, Nothing)
            'oGridN.doAddListField("e.U_CardCd", "Customer Code", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            'oGridN.doAddListField("e.U_CardNM", "Customer Name", False, 0, Nothing, Nothing)
            
            oGridN.doAddListField("e.U_Street", "Street", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_Block", "Block", False, 0, Nothing, Nothing)
            
            oGridN.doAddListField("e.U_CntrNo", "Contract No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RDate", "Req. Start", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RTime", "Req. Time", False, 0, "TIME", Nothing)
            
            oGridN.doAddListField("r.U_ASTime", "Act. Time", False, 0, "TIME", Nothing)
            
            oGridN.doAddListField("r.U_AETime", "Act ETime", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_ItmGrp", "Container Grp", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_ItemGroups)
            oGridN.doAddListField("r.U_Serial", "Item Serial No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicSp", "SLicSp", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicNr", "Skip Lic. No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicExp", "Skip Exp.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Driver", "Driver", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipWgt", "Sup. Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Price", "Price", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicCh", "SLicCh", False, 0, Nothing, Nothing)
            
            oGridN.doAddListField("e.U_SteId", "Site Id", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BTime", "Booking Time", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipTot", "TipTot", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CongCh", "CongCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DocNum", "DocNum", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Discnt", "Discnt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SupRef", "Supplier Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PARCPT", "Payment", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Receipt)

            oGridN.doAddListField("r.U_CCNum", "CC Num.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CCType", "CC Type", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CCStat", "CC Status", False, 0, Nothing, Nothing)
            
            oGridN.doAddListField("r.U_TChrgVtRt", "Tipping Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtRt", "Haulage Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtRt", "Tipping Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCostVtRt", "Supplier Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtRt", "Haulage Vat Purchase Rate", False, 0, Nothing, Nothing)
            
            oGridN.doAddListField("r.U_TChrgVtGrp", "Tipping Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtGrp", "Haulage Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtGrp", "Tipping Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PCostVtGrp", "Supplier Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtGrp", "Haulage Vat Purchase Group", False, 0, Nothing, Nothing)
            
            oGridN.doAddListField("e.U_Address", "Address", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_PAddress", "Producer Address", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_PZpCd", "Producer PostCode", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_PPhone1", "Producer Phone", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_FirstBP", "FirstBP", False, 0, Nothing, Nothing)

            
        End Sub
		Public Overrides Sub doBeforeLoadData(oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)

        End Sub
        Public Overrides Sub doHideTotals(ByVal oForm As SAPbouiCOM.Form)
        End Sub
        Protected Overrides Sub doItemGroupCombo(ByRef oForm As SAPbouiCOM.Form)

        End Sub
        Protected Overrides Sub doReLoadData(ByVal oForm As SAPbouiCOM.Form, ByVal bIsFirst As Boolean)
            MyBase.doReLoadData(oForm, bIsFirst)
            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
            oGridN.doSetDeleteActive(False)
        End Sub
        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
            If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
                Return False
            Else
                Return MyBase.doCustomItemEvent(oForm, pVal)
            End If
        End Function

    End Class
End Namespace
