Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System.Net
Imports System.Web

Imports com.idh.utils.Conversions
Imports com.idh.bridge
Imports com.idh.bridge.data

Imports WR1_Grids.idh.controls.grid

Namespace idh.forms.manager
    Public Class Coverage
        Inherits idh.forms.manager.Order

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, iMenuPosition, "IDHJOBCO", "PreBookManager.srf", "PreBook Manager")
        End Sub
'
'        Protected Overrides Function getTitle() As String
'            Return "PreBook Manager"
'        End Function

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.UpdateGrid)
            'OSM Column changes :- Column layout for the OSM for ERITH needs to be as follows: 
            '* Progress| 
            '* Order Number| 
            '* Row Number| 
            '* Order Type| 
            '* Order Address| 
            '* Waste Code| 
            '* Container Code|    
            '* Req Start| 
            '* Req Time From| 
            '* Req Time To| 
            '* Actual Start| 
            '* Actual End| 
            '* Vehice Reg| 
            'Driver|
            '��.all other fields��.

            oGridN.doAddListField("v.State", "Progress", False, -1, Nothing, Nothing)
            'oGridN.doAddListField("'" & IDHAddOns.idh.addon.Base.CURRENTWORKDIR & "\' + v.IMG", "", False, 10, "PICTURE", Nothing)
            oGridN.doAddListField("v.IMG", "", False, 10, "PICTURE", Nothing)
            oGridN.doAddListField("r.U_JobNr", "Order No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.Code", "Row No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_JobTp", "Order Type", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_CardNM", "Customer Name", False, 70, Nothing, Nothing)
            oGridN.doAddListField("e.U_Address", "Order Address", False, 70, Nothing, Nothing)
            oGridN.doAddListField("e.U_City", "City", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_WasCd", "Waste Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("r.U_ItemCd", "Container Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)

            oGridN.doAddListField("f.StartDate", "PB-StartDate", False, -1, Nothing, Nothing)
            oGridN.doAddListField("f.EndDate", "PB-EndDate", False, -1, Nothing, Nothing)
            oGridN.doAddListField("f.Pattern", "Pattern", False, -1, Nothing, Nothing)
            oGridN.doAddListField("f.Freq", "Freq", False, -1, Nothing, Nothing)
            oGridN.doAddListField("f.Sun", "Sun", False, -1, Nothing, Nothing)
            oGridN.doAddListField("f.Mon", "Mon", False, -1, Nothing, Nothing)
            oGridN.doAddListField("f.Tue", "Tue", False, -1, Nothing, Nothing)
            oGridN.doAddListField("f.Wed", "Wed", False, -1, Nothing, Nothing)
            oGridN.doAddListField("f.Thu", "Thu", False, -1, Nothing, Nothing)
            oGridN.doAddListField("f.Fri", "Fri", False, -1, Nothing, Nothing)
            oGridN.doAddListField("f.Sat", "Sat", False, -1, Nothing, Nothing)

            oGridN.doAddListField("r.U_Lorry", "Vehicle Reg.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_LorryCd", "Veh Reg. Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Driver", "Driver", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_RDate", "Req. Start", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_RTime", "From", False, -1, "TIME", Nothing)

            oGridN.doAddListField("r.U_ASDate", "Act. Start", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_AEDate", "Act. End", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_AETime", "Act ETime", False, 0, "TIME", Nothing)
            oGridN.doAddListField("e.U_Street", "Street", False, 70, Nothing, Nothing)
            oGridN.doAddListField("e.U_Block", "Block", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_ZpCd", "Post Code", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_Phone1", "Main No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_SiteTl", "Site Tel No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_SteId", "Site Id", False, 0, Nothing, Nothing)
            
            'Added to handle the BP Switching
            oGridN.doAddListField("e.U_PAddress", "Producer Address", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_PZpCd", "Producer Post Code", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_PPhone1", "Producer Phone", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_FirstBP", "First BP Selected", False, 0, Nothing, Nothing)
            
            oGridN.doAddListField("e.U_CardCd", "Customer Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_CustRef", "Customer Ref No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CstWgt", "Waste Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CusQty", "Cont Qty", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HlSQty", "Cont Qty", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Price", "Haulage Total", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCTotal", "Tip Total", False, -1, Nothing, Nothing)
            oGridN.doAddListField("CAST(((U_TCTotal + U_Price) - r.U_DisAmt) As Numeric(9,2))", "Sub After Discount", False, -1, Nothing, "SubAftDisc")
            'oGridN.doAddListField("(U_TCTotal +(U_CusQty * U_CusChr)) - r.U_DisAmt", "Sub After Discount", False, -1, Nothing, "SubBefDisc")
            oGridN.doAddListField("r.U_SLicCh", "Skip Lic Charge", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_PayMeth", "Payment Method", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_CarrCd", "Carrier Code", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_Tip", "Disposal Site", False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_JCost", "Total Order Cost", False, -1, Nothing, "TOrdCost")
            oGridN.doAddListField("e.U_CntrNo", "Contract No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicNr", "Skip Lic. No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Comment", "Comment", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ASTime", "Time", False, -1, "TIME", Nothing)
            oGridN.doAddListField("r.U_RTimeT", "Req. Time To", False, -1, "TIME", Nothing)
            oGridN.doAddListField("r.U_VehTyp", "Vehicle", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_Route", "Route", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_Seq", "Sequence", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Status", "Sales Status", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_PStat", "Purchase Status", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItemDsc", "Container Desc.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_ItmGrp", "Container Grp", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_ItemGroups)
            oGridN.doAddListField("r.U_Serial", "Item Serial No.", False, 0, Nothing, Nothing)
            '..
            '..
            oGridN.doAddListField("r.U_SLicSp", "SLicSp", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_SLicExp", "Skip Exp.", False, 0, Nothing, Nothing)
            '..
            oGridN.doAddListField("r.U_TipWgt", "Sup. Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RdWgt", "Read Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DisAmt", "Discount Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AddEx", "Additional Expenses", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TaxAmt", "Sales Vat Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Total", "Total Charge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BDate", "Booking Date", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BTime", "Booking Time", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipCost", "TipCost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipTot", "TipTot", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCharge", "TCharge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CongCh", "CongCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DocNum", "DocNum", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Discnt", "Discnt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SupRef", "Supplier Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WasDsc", "Waste Description", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CarrNm", "Carrier Name", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Covera", "Coverage.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WROrd", "WO No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WRRow", "WO Row No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CoverHst", "Coverage Hist.", False, 0, Nothing, Nothing)
            
            oGridN.doAddListField("r.U_TChrgVtRt", "Tipping Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtRt", "Haulage Vat Charge Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtRt", "Tipping Vat Purchase Rate", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtRt", "Haulage Vat Purchase Rate", False, 0, Nothing, Nothing)
            
            oGridN.doAddListField("r.U_TChrgVtGrp", "Tipping Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HChrgVtGrp", "Haulage Vat Charge Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCostVtGrp", "Tipping Vat Purchase Group", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_HCostVtGrp", "Haulage Vat Purchase Group", False, 0, Nothing, Nothing)
            
            oGridN.doAddListField("r.U_VtCostAmt", "Purchase Vat Amount", False, 0, Nothing, Nothing)
            
        End Sub
        
        Protected Overrides Sub doSetGridFilters(ByVal oGridN As FilterGrid)
            oGridN.doAddFilterField("IDH_REQSTF", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_REQSTT", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_ACTSTF", "f.EndDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_ACTSTT", "f.EndDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_CUST", "e.U_CardCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_NAME", "e.U_CardNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_PROD", "r.U_ItemCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ITMGRP", "r.U_ItmGrp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 255)
            oGridN.doAddFilterField("IDH_JOBTYP", "r.U_JobTp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ORDNO", "e.Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_VEHREG", "r.U_Lorry", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DRIVER", "r.U_Driver", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ADDR", "e.U_Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 255)
            oGridN.doAddFilterField("IDH_POSTCD", "e.U_ZpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_CUSPHO", "e.U_Phone1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_SITETL", "e.U_SiteTl", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_REF", "r.U_CustRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            
            oGridN.doAddFilterField("IDH_STEID", "e.U_SteId", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_STREET", "e.U_Street", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
		End Sub

		Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
			MyBase.doCompleteCreate(oForm, BubbleEvent)
			doAddUF(oForm,"IDH_SDATE", SAPbouiCOM.BoDataType.dt_DATE, 30,False,True)
			doAddUF(oForm,"IDH_EDATE", SAPbouiCOM.BoDataType.dt_DATE, 30,False,True)
'			Dim oItem As SAPbouiCOM.Item
'			
'			Dim oLabel As SAPbouiCOM.StaticText
'			oLabel = oForm.Items.Item("IDH_LA2").Specific
            '			oLabel.Caption = getTranslatedWord("Expire from")
'			
'			oLabel = oForm.Items.Item("IDH_LB2").Specific
            '			oLabel.Caption = getTranslatedWord("To ")
			'oForm.AutoManaged = True
		End Sub
		
		Protected Overrides Sub doReLoadData(ByVal oForm As SAPbouiCOM.Form, ByVal bIsFirst As Boolean)
            MyBase.doReLoadData(oForm, bIsFirst)
            
            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
            
            oGridN.doSetDeleteActive(False)
            doGridTotals( oGridN, False )
            oForm.Items.Item("IDH_CALC").Visible = False
        End Sub
		
		Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            MyBase.doBeforeLoadData(oForm)
            
            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
           
            Dim sExtraSelect As String
            sExtraSelect = " ( SELECT Code,  " & _
            			   " CONVERT(VARCHAR, (SUBSTRING(U_COVERA, LEN(U_COVERA)-15,8)),101) AS StartDate,   " & _
            			   " CONVERT(VARCHAR, (SUBSTRING(U_COVERA, LEN(U_COVERA)-7,8)),101) AS EndDate, " & _
            			   " SUBSTRING(U_COVERA, LEN(U_COVERA)-16,1) As Sat, " & _
            			   " SUBSTRING(U_COVERA, LEN(U_COVERA)-17,1) As Fri, " & _
            			   " SUBSTRING(U_COVERA, LEN(U_COVERA)-18,1) As Thu, " & _
            			   " SUBSTRING(U_COVERA, LEN(U_COVERA)-19,1) As Wed, " & _
            			   " SUBSTRING(U_COVERA, LEN(U_COVERA)-20,1) As Tue, " & _
            			   " SUBSTRING(U_COVERA, LEN(U_COVERA)-21,1) As Mon, " & _
            			   " SUBSTRING(U_COVERA, LEN(U_COVERA)-22,1) As Sun, " & _
            			   " CAST(SUBSTRING(U_COVERA, LEN(U_COVERA)-25,3) AS INT) As Freq, " & _
            			   " CASE WHEN SUBSTRING(U_COVERA, LEN(U_COVERA)-26,1) = 'O' THEN 'Other'  " & _
            			   " WHEN SUBSTRING(U_COVERA, LEN(U_COVERA)-26,1) = 'D' THEN 'Daily'  " & _
            			   " WHEN SUBSTRING(U_COVERA, LEN(U_COVERA)-26,1) = 'W' THEN 'Weekly' " & _
            			   " WHEN SUBSTRING(U_COVERA, LEN(U_COVERA)-26,1) = 'M' THEN 'Monthly'  " & _
            			   " WHEN SUBSTRING(U_COVERA, LEN(U_COVERA)-26,1) = 'Y' THEN 'Yearly'  " & _
            			   " ELSE 'DN' " & _
            			   " END AS 'Pattern', " & _
            			   " CASE WHEN CAST(SUBSTRING(U_COVERA, LEN(U_COVERA)-7,8) AS INT) < 20060000 THEN 'ISFREQ' ELSE 'ISDATE' END As DATEORFREQ,    " & _
            			   " U_COVERA " & _
						   " FROM [@IDH_JOBSHD] " & _
						   " WHERE " & _ 
						   "    NOT (U_COVERA Is NULL OR U_COVERA LIKE 'Cre%' Or CHARINDEX('-', U_COVERA) > 0) " & _
						   "    And U_COVERA > '' " & _
						   "    AND NOT U_COVERA LIKE 'PBI-%' " & _
						   "	AND ( " & _
						   "   		( " & _
						   "			U_COVERA LIKE '_D000%' OR U_COVERA LIKE '_W000%' OR U_COVERA LIKE '_M000%' OR U_COVERA LIKE '_Y000%' " & _      
						   "			OR  " & _
						   "			U_COVERA LIKE 'D000%' OR U_COVERA LIKE 'W000%' OR U_COVERA LIKE 'M000%' OR U_COVERA LIKE 'Y000%' " & _
						   "		) " & _
						   "		OR NOT ( " & _
						   "       		(CHARINDEX('_', U_COVERA) = 1 AND SUBSTRING(U_COVERA, 3,3) = '000' ) " & _
						   "        	OR " & _
						   "       		(SUBSTRING(U_COVERA, 2,3) = '000') " & _
						   "    	)" & _
						   "    ) ) As f"
						   
            'oGridN.setTableValue("[@IDH_JOBSHD] r, [@IDH_JOBENTR] e, IDH_VPROGRESS v, (" & sExtraSelect & ") As f ")
            
            'Added from the base
            'oGridN.doAddGridTable( New GridTable( "@IDH_JOBSHD", "r", "Code", True ), True)
            'oGridN.doAddGridTable( New GridTable( "@IDH_JOBENTR", "e", "Code", True ))
            'oGridN.doAddGridTable( New GridTable( "IDH_VPROGRESS", "v" ))
            oGridN.doAddGridTable( New GridTable( sExtraSelect ), False, True )
            
            Dim sRFilter As String = oGridN.getRequiredFilter()
            sRFilter = sRFilter & _
            	" AND f.Code = r.Code "
            	
            oGridN.setRequiredFilter(sRFilter)
            setUFValue(oForm,"IDH_STATUS", "")
            setUFValue(oForm,"IDH_PSTAT", "")
            setUFValue(oForm,"IDH_PROG", "")
            
            Dim oItem As SAPbouiCOM.Item
            oItem = oForm.Items.Item("IDH_LOC")
            oItem.Visible = False
            
            oItem = oForm.Items.Item("IDH_WS")
            oItem.Visible = False
            
            oItem = oForm.Items.Item("IDH_DOC")
            oItem.Visible = False
            
            doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)
        End Sub
        
        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_VALIDATE Then
                If pVal.BeforeAction = False Then 
                	If pVal.ItemUID = "IDH_SDATE" Then
                		doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                	Else If pVal.ItemUID = "IDH_EDATE" Then	
                		doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                	End If
				End If
			End If
			Return MyBase.doItemEvent(oForm,PVal,BubbleEvent)
		End Function
        
        '** Send By Custom Events
        'Return True If All is OK and others need to continue
        Public Overrides Function doCustomItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base) As Boolean
        	If pVal.EventType = IDHAddOns.idh.events.Base.ev_Types.idh_GRID_RIGHT_CLICK Then
        		Return False
        	Else
        		Return MyBase.doCustomItemEvent(oForm,pVal)
        	End If
        End Function
        
        
'        '** The Initializer
'        Protected Overrides Sub doReLoadData(ByVal oForm As SAPbouiCOM.Form, ByVal bIsFirst As Boolean)
'            oForm.Freeze(True)
'            Try
'                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)
'
'                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
'                If bIsFirst = False Then
'                    oGridN.doCreateFilterString()
'                    If oGridN.doCheckDoQuery() = False Then
'                        Return
'                    End If
'                End If
'                oGridN.doReloadData()
'            Catch ex As Exception
'                com.idh.bridge.DataHandler.INSTANCE.doError("doReLoadData", "Exception: " & ex.ToString, "Incorrect Form Mode")
'            Finally
'                oForm.Freeze(False)
'            End Try
'        End Sub

        Public Overrides Sub doButtonID1(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
'            Dim oButton As SAPbouiCOM.Button
            If pVal.BeforeAction = True Then
                If oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Update") Then
                	Dim sStartDate As String = getUFValue(oForm,"IDH_SDATE")
                	Dim sEndDate As String = getUFValue(oForm,"IDH_EDATE")
                	If sStartDate.Length > 0 AndAlso sEndDate.Length > 0 Then
                		Dim oGridN As IDHGrid = IDHGrid.getInstance(oForm, "LINESGRID")
		                Dim oSelected As SAPbouiCOM.SelectedRows
		                oSelected = oGridN.getGrid().Rows.SelectedRows()
                
                		If Not oSelected Is Nothing Then
                    		Dim iRow As Integer
		                    For iIndex As Integer = 0 To oSelected.Count - 1
    	                    	'iRow = oSelected.Item(iIndex, SAPbouiCOM.BoOrderType.ot_RowOrder)
    	                    	'iRow = oGridN.getSBOGrid().GetDataTableRowIndex(iRow)
    	                    	iRow = oGridN.getDataTableRowIndex(oSelected, iIndex)
    	                    	
        	                	Dim sRowCode As String = oGridN.doGetFieldValue("r.Code", iRow)
        	                	idh.forms.OrderRow.doPreBookRows(goParent,sRowCode,Nothing,sStartDate,sEndDate)
	        	            Next
		                End If
		                doReLoadData(oForm, True)
                	End If
                	setUFValue(oForm,"IDH_SDATE", "")
                	setUFValue(oForm,"IDH_EDATE","")
                	oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Find")
                ElseIf oForm.Items.Item("1").Specific.Caption = Translation.getTranslatedWord("Find") Then
                    doReLoadData(oForm, True)
                End If
                BubbleEvent = False
            End If
        End Sub
        
        'Protected Overrides Sub doGridDoubleClick(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Protected Overridable Sub doGridDoubleClick(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As IDHAddOns.idh.events.Base)
        	Dim oGridN As UpdateGrid = pVal.oGrid 'UpdateGrid.getInstance(oForm, "LINESGRID")
            If pVal.Row >= 0 AndAlso oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                oGridN.setCurrentLineByClick(pVal.Row)

                Dim sRowCode As String = oGridN.doGetFieldValue("r.Code")
                Dim sJobEntr As String = oGridN.doGetFieldValue("r.U_JobNr")

                Dim oData As New ArrayList
                If sJobEntr.Length = 0 Then
                    com.idh.bridge.DataHandler.INSTANCE.doError("A Job Entry must be selected.")
                Else
                    oData.Add("DoUpdate")
                    If oGridN.doCheckIsSameCol(pVal.ColUID, "e.U_ZpCd") Then
                        oData.Add("PC=" & oGridN.doGetFieldValue("e.U_ZpCd"))
                        goParent.doOpenModalForm("IDHMAP", oForm, oData)
                    ElseIf oGridN.doCheckIsSameCol(pVal.ColUID, "r.U_JobNr") Then
                        oData.Add(sJobEntr)
                        goParent.doOpenModalForm("IDH_WASTORD", oForm, oData)
                    Else
                        If sRowCode.Length > 0 Then
                            oData.Add(sRowCode)
                            oData.Add(oGridN.doGetFieldValue("e.U_BDate"))
                            oData.Add(goParent.doTimeStrToStr(oGridN.doGetFieldValue("e.U_BTime")))
                            oData.Add(oGridN.doGetFieldValue("e.U_ZpCd"))
                            oData.Add(oGridN.doGetFieldValue("e.U_Address"))
                            oData.Add("IDHJOBCO")
                            oData.Add(oGridN.doGetFieldValue("e.U_SteId"))
                            
                            'Added to handle the BP Switching
                            oData.Add(Nothing)
                            oData.Add(oGridN.doGetFieldValue("e.U_PAddress"))
                            oData.Add(oGridN.doGetFieldValue("e.U_PZpCd"))
                            oData.Add(oGridN.doGetFieldValue("e.U_PPhone1"))
                            oData.Add(oGridN.doGetFieldValue("e.U_FirstBP"))
                            
                            goParent.doOpenModalForm("IDHJOBS", oForm, oData)
                        End If
                    End If
                End If
            End If
        End Sub

    End Class
End Namespace

