Imports System.IO
Imports System.Collections
Imports IDHAddOns.idh.controls
Imports System
Imports com.idh.utils.Conversions
Imports WR1_Grids.idh.controls.grid
Imports com.idh.bridge.data

Imports com.idh.bridge.lookups
Imports com.idh.dbObjects.User

Namespace idh.forms.manager
    Public Class Vehicle2
        Inherits idh.forms.manager.Templ

        Public Sub New(ByVal oParent As IDHAddOns.idh.addon.Base, ByVal iMenuPosition As Integer)
            MyBase.New(oParent, "IDHVECR2", Nothing, "Vehicle Manager2.srf", iMenuPosition, "Vehicle Schedule Manager 2")
        End Sub

        '*** Add event filters to avoid receiving all events from SBO
        Protected Overrides Sub doSetEventFilters()
            MyBase.doSetEventFilters()
            doAddEvent(SAPbouiCOM.BoEventTypes.et_FORM_RESIZE)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
            doAddEvent(SAPbouiCOM.BoEventTypes.et_CLICK)
        End Sub

        Protected Overrides Sub doSetGridFilters(ByVal oGridN As FilterGrid)
            Dim oVehicleGrid As UpdateGrid = UpdateGrid.getInstance(oGridN.getSBOForm, "VEHGRID")
            oVehicleGrid.doAddFilterField("IDH_VEHREG", "VehReg", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oVehicleGrid.doAddFilterField("IDH_VEHRT", "Route", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oVehicleGrid.doAddFilterField("IDH_VRQSTF", "ReqDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oVehicleGrid.doAddFilterField("IDH_VRQSTT", "ReqDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oVehicleGrid.doAddFilterField("IDH_TYPE", "Type", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 100)

            oGridN.doAddFilterField("IDH_REQSTF", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_REQSTT", "r.U_RDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_ACTSTF", "r.U_ASDate", SAPbouiCOM.BoDataType.dt_DATE, ">=", 10)
            oGridN.doAddFilterField("IDH_ACTSTT", "r.U_ASDate", SAPbouiCOM.BoDataType.dt_DATE, "<=", 10)
            oGridN.doAddFilterField("IDH_CUST", "e.U_CardCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_NAME", "e.U_CardNM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_PROD", "r.U_ItemCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            'oGridN.doAddFilterField("IDH_ITMGRP", "r.U_ItmGrp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 255)
            oGridN.doAddFilterField("IDH_ROUTE", "e.U_Route", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 255)
            oGridN.doAddFilterField("IDH_JOBTYP", "r.U_JobTp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ORDNO", "e.Code", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            ''oGridN.doAddFilterField("IDH_VEHREG", "r.U_Lorry", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_DRIVER", "r.U_Driver", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_ADDR", "e.U_Address", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "LIKE", 255)
            oGridN.doAddFilterField("IDH_POSTCD", "e.U_ZpCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_CUSPHO", "e.U_Phone1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_SITETL", "e.U_SiteTl", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_REF", "r.U_CustRef", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WSCD", "r.U_WasCd", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_WSDC", "r.U_WasDsc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_USER", "r.U_User", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_BRANCH", "r.U_Branch", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "=", 255)

			''## MA Start 29-08-2014 Issue#421
            oGridN.doAddFilterField("IDH_BRANC2", "r.U_Branch", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            ''## MA End 29-08-2014 Issue#421
            oGridN.doAddFilterField("IDH_STEID", "e.U_SteId", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
            oGridN.doAddFilterField("IDH_STREET", "e.U_Street", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, "AUTO", 255)
        End Sub

        Protected Overrides Sub doSetListFields(ByVal oGridN As IDHAddOns.idh.controls.UpdateGrid)
            oGridN.doAddListField("'LINK'", "", False, 15, "CHECKBOX", Nothing)
            oGridN.doAddListField("r.U_Lorry", "Vehicle Reg.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_LorryCd", "Veh Reg. Code", True, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Driver", "Driver", True, -1, Nothing, Nothing)

            oGridN.doAddListField("v.State", "Progress", False, -1, Nothing, Nothing)
            'oGridN.doAddListField("'" & IDHAddOns.idh.addon.Base.CURRENTWORKDIR & "\' + v.IMG", "", False, 10, "PICTURE", Nothing)
            oGridN.doAddListField("v.IMG", "", False, 10, "PICTURE", Nothing)
            oGridN.doAddListField("r.U_Status", "Sales Status", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_PStat", "Purchase Status", False, -1, Nothing, Nothing)

            oGridN.doAddListField("e.U_CntrNo", "Contract No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_JobNr", "Order No.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.Code", "Row No.", False, -1, Nothing, Nothing)

            oGridN.doAddListField("e.U_CardCd", "Customer Code", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("e.U_CardNM", "Customer Name", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_ZpCd", "Post Code", False, -1, Nothing, Nothing)

            oGridN.doAddListField("e.U_Route", "Route", False, -1, Nothing, Nothing)

            oGridN.doAddListField("r." + IDH_JOBSHD._ProCd, getTranslatedWord("Producer Code"), False, 70, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r." + IDH_JOBSHD._ProNm, getTranslatedWord("Producer Name"), False, 70, Nothing, Nothing)

            oGridN.doAddListField("r.U_JobTp", "Order Type", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_RDate", "Req. Start", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_RTime", "Req. Time", False, -1, "TIME", Nothing)

            oGridN.doAddListField("r.U_ASDate", "Act. Start", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_ASTime", "Act. Time", False, 0, "TIME", Nothing)
            oGridN.doAddListField("r.U_AEDate", "Act EDate", True, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_AETime", "Act ETime", False, 0, "TIME", Nothing)

            oGridN.doAddListField("r.U_ItemCd", "Container Code", False, -1, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_Items)
            oGridN.doAddListField("r.U_ItemDsc", "Container Desc.", False, -1, Nothing, Nothing)
            oGridN.doAddListField("r.U_Serial", "Container Serial No.", False, -1, Nothing, Nothing)
            '..
            oGridN.doAddListField("r.U_SLicSp", "SLicSp", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_SLicNr", "Skip Lic. No.", False, 0, Nothing, Nothing)

            oGridN.doAddListField("e.U_Address", "Address", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_Street", "Street", False, -1, Nothing, Nothing)
            oGridN.doAddListField("e.U_SteId", "Site Id", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_ItmGrp", "Item Grp", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_ItemGroups)

            oGridN.doAddListField("r.U_SLicExp", "Skip Exp.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Tip", "Supplier", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            '..
            oGridN.doAddListField("r.U_TipWgt", "Sup. Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CstWgt", "Cust. Wgt", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_RdWgt", "Read Weight", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Price", "Price", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_AddEx", "Additional Expenses", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_SLicCh", "SLicCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Total", "Charge Total", False, -1, Nothing, Nothing)

            oGridN.doAddListField("r.U_VehTyp", "Vehicle", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BDate", "Booking Date", False, 0, Nothing, Nothing)
            oGridN.doAddListField("e.U_BTime", "Booking Time", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipCost", "TipCost", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TipTot", "TipTot", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCharge", "TCharge", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TCTotal", "TCTotal", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_CongCh", "CongCh", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_DocNum", "DocNum", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_Discnt", "Discnt", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_DisAmt", "Discount Amount", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_TaxAmt", "Sales Vat Amount", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_SupRef", "Supplier Ref No.", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_WasCd", "Waste Code", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_WasDsc", "Waste Description", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_CarrCd", "Carrier Code", False, 0, Nothing, Nothing, -1, SAPbouiCOM.BoLinkedObject.lf_BusinessPartner)
            oGridN.doAddListField("r.U_CarrNm", "Carrier Name", False, 0, Nothing, Nothing)

            oGridN.doAddListField("r.U_PayMeth", "Payment Method", False, -1, Nothing, Nothing)

            oGridN.doAddListField("r.U_CustRef", "Customer Ref No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WROrd", "WO No.", False, 0, Nothing, Nothing)
            oGridN.doAddListField("r.U_WRRow", "WO Row No.", False, 0, Nothing, Nothing)
        End Sub

        Public Overrides Sub doCompleteCreate(ByRef oForm As SAPbouiCOM.Form, ByRef BubbleEvent As Boolean)
            Try
                oForm.Title = gsTitle

                Dim oItem As SAPbouiCOM.Item

                oItem = doAddUFCheck(oForm, "IDH_AFOJR", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oItem.AffectsFormMode = False

                oItem = doAddUFCheck(oForm, "IDH_AFRDT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1, False, True, "Y", "N")
                oItem.AffectsFormMode = False

                Dim oVehicleGrid As UpdateGrid = New UpdateGrid(Me, oForm, "VEHGRID", 7, 103, 300, 362)
                Dim oGridN As OrderRowGrid = New OrderRowGrid(Me, oForm, "LINESGRID", 307, 103, 635, 362, "r")

                oVehicleGrid.getSBOItem().AffectsFormMode = False

                oGridN.getSBOGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto
                oForm.AutoManaged = False
                oGridN.getSBOItem().AffectsFormMode = False

                oForm.SupportedModes = SAPbouiCOM.BoFormMode.fm_FIND_MODE Or SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                doSetMode(oForm, SAPbouiCOM.BoFormMode.fm_FIND_MODE)

                oForm.EnableMenu(Config.NAV_ADD, False)
                oForm.EnableMenu(Config.NAV_FIND, False)
                oForm.EnableMenu(Config.NAV_FIRST, False)
                oForm.EnableMenu(Config.NAV_NEXT, False)
                oForm.EnableMenu(Config.NAV_PREV, False)
                oForm.EnableMenu(Config.NAV_LAST, False)

                Dim fSettings As SAPbouiCOM.FormSettings
                fSettings = oForm.Settings
                fSettings.Enabled = False
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error completing the form.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXCTF", {Nothing})
                BubbleEvent = False
            End Try
        End Sub

        Protected Overridable Sub doSetVehGridFilters(ByRef oVehicleGrid As UpdateGrid)
        End Sub

        Public Overridable Function getListVehRequiredStr() As String
            Return "VehReg != 'Zz-Ignore'"
        End Function

        Protected Overridable Sub doSetListVehFields(ByRef oVehicleGrid As IDHAddOns.idh.controls.UpdateGrid)
            oVehicleGrid.doAddListField("VehReg", "Vehicle Reg.", False, 50, Nothing, Nothing)
            oVehicleGrid.doAddListField("ReqDate", "Requested Date", False, 50, Nothing, Nothing)
            oVehicleGrid.doAddListField("Description", "Description", False, 100, Nothing, Nothing)
            'oVehicleGrid.doAddListField("'" & IDHAddOns.idh.addon.Base.CURRENTWORKDIR & "\' + " & doGetCapacityQuery(), "", False, 10, "PICTURE","CAP")
            oVehicleGrid.doAddListField(doGetCapacityQuery(), "", False, 10, "PICTURE", "CAP")
            oVehicleGrid.doAddListField("Total", "Day Jobs", False, 20, Nothing, Nothing)
            oVehicleGrid.doAddListField("NotStarted", "Waiting", False, 20, Nothing, Nothing)
            oVehicleGrid.doAddListField("Started", "Busy", False, 20, Nothing, Nothing)
            oVehicleGrid.doAddListField("DayCap", "Capacity", False, 20, Nothing, Nothing)
        End Sub

        Protected Overrides Function doGetCapacityQuery() As String
            Dim sPerc As String = Config.Parameter("VSFULLW")
            If sPerc Is Nothing OrElse sPerc.Length = 0 Then
                sPerc = "50"
            End If
            Dim sFull, sNearly, sAvailable As String
            If Config.ParameterWithDefault("IMGPATH", "").Trim = "" Then
                sFull = IDHAddOns.idh.addon.Base.doFixImagePath("full.png")
                sNearly = IDHAddOns.idh.addon.Base.doFixImagePath("nearly.png")
                sAvailable = IDHAddOns.idh.addon.Base.doFixImagePath("available.png")

            Else
                Dim sImgPath As String = Config.ParameterWithDefault("IMGPATH", "").Trim
                If Not sImgPath.EndsWith("\") Then
                    sImgPath &= "\"
                End If
                sFull = sImgPath & ("full.png")
                sNearly = sImgPath & ("nearly.png")
                sAvailable = sImgPath & ("available.png")
            End If
            Return "CASE WHEN Total >= DayCap THEN '" & sFull & "' ELSE CASE WHEN Total > (DayCap * " & sPerc & "/100 ) THEN '" & sNearly & "' ELSE '" & sAvailable & "' END End"
        End Function

        Public Overrides Sub doBeforeLoadData(ByVal oForm As SAPbouiCOM.Form)
            doAddWF(oForm, "VehData", Nothing)
            doAddWF(oForm, "JobData", Nothing)

            Dim oVehicleGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "VEHGRID")
            If oVehicleGrid Is Nothing Then
                oVehicleGrid = New UpdateGrid(Me, oForm, "VEHGRID")
            End If
            oVehicleGrid.doSetSBOAutoReSize(False)

            MyBase.doBeforeLoadData(oForm)

			'## MA Start 02-09-2014 Issue#421
            doSetMultiBranch(oForm)
            '## MA End 02-09-2014 Issue#421
            setUFValue(oForm, "IDH_PROG", "!Complete")

            doSetVehGridFilters(oVehicleGrid)
            oVehicleGrid.doAutoExpand(True)

            oVehicleGrid.doAddGridTable(New GridTable(Config.INSTANCE.getVehicleCapacityView()), True)

            oVehicleGrid.setOrderValue("VehReg, Description, ReqDate")
            oVehicleGrid.setRequiredFilter(getListVehRequiredStr())
            oVehicleGrid.doSetDoCount(False)
            oVehicleGrid.doSetBlankLine(False)
            doSetListVehFields(oVehicleGrid)

            Dim oGridN As OrderRowGrid = OrderRowGrid.getInstance(oForm, "LINESGRID")
            oGridN.doSetDoCount(False)
            oGridN.BlockPopupMenu = True

            doFillTypes(oForm)
        End Sub


        Private Sub doFillTypes(ByVal oForm As SAPbouiCOM.Form)
            Dim oItem As SAPbouiCOM.Item
            Dim oCombo As SAPbouiCOM.ComboBox
            oItem = oForm.Items.Item("IDH_TYPE")
            oItem.DisplayDesc = True
            oCombo = oItem.Specific

            doClearValidValues(oCombo.ValidValues)

            oCombo.ValidValues.Add("", getTranslatedWord("All"))
            oCombo.ValidValues.Add("A", getTranslatedWord("Asset-OnRoad"))
            oCombo.ValidValues.Add("O", getTranslatedWord("Asset-OffRoad"))
            oCombo.ValidValues.Add("S", getTranslatedWord("Sub Contract"))
            oCombo.ValidValues.Add("H", getTranslatedWord("Owner Driver"))
        End Sub

        Protected Sub doReloadVehicle(ByVal oForm As SAPbouiCOM.Form)
            oForm.Freeze(True)
            Try
                If getUFValue(oForm, "IDH_AFRDT").Equals("Y") Then
                    Dim sDateF As String = getUFValue(oForm, "IDH_REQSTF")
                    Dim sDateT As String = getUFValue(oForm, "IDH_REQSTT")

                    setUFValue(oForm, "IDH_VRQSTF", sDateF)
                    setUFValue(oForm, "IDH_VRQSTT", sDateT)
                Else
                    setUFValue(oForm, "IDH_VRQSTF", "")
                    setUFValue(oForm, "IDH_VRQSTT", "")
                End If
                Dim oVehicleGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "VEHGRID")
                oVehicleGrid.doReloadData()
            Catch ex As Exception
                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error reloading the Vehicle.")
                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXRVEH", {Nothing})
            Finally
                oForm.Freeze(False)
            End Try
        End Sub

        'Protected Overrides Sub doLoadData(ByVal oForm As SAPbouiCOM.Form)
        Protected Overrides Sub doReLoadData(ByVal oForm As SAPbouiCOM.Form, ByVal bIsFirst As Boolean)
            MyBase.doReLoadData(oForm, bIsFirst)
            doReloadVehicle(oForm)
        End Sub

        Private Sub doSizeGrids(ByVal oForm As SAPbouiCOM.Form)
            Dim oVehicleGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "VEHGRID")
            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
            If Not oVehicleGrid Is Nothing Then
                oVehicleGrid.getItem().Width = 300
                oVehicleGrid.getItem().Height = oGridN.getItem().Height
            End If
            oForm.Freeze(False)
        End Sub

        Public Overrides Function doItemEvent(ByVal oForm As SAPbouiCOM.Form, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) As Boolean
            Dim bRes As Boolean = MyBase.doItemEvent(oForm, pVal, BubbleEvent)

            If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_RESIZE Then
                If pVal.BeforeAction = False Then
                    doSizeGrids(oForm)
                End If
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_CLICK Then
                '*** Using this and not the Custom Event bacause of the drag and drop function, 
                '*** requiring the selection list before the button is realsed to register the normal selection

                Try
                    If pVal.BeforeAction = True Then
                        If pVal.ItemUID = "VEHGRID" Then
                            oForm.Freeze(True)
                            Dim oVehicleGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "VEHGRID")
                            Dim sVehReg As String = ""
                            If pVal.Row >= 0 AndAlso oVehicleGrid.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                                oVehicleGrid.setCurrentLineByClick(pVal.Row)
                                sVehReg = oVehicleGrid.doGetFieldValue("VehReg")
                                setWFValue(oForm, "VehData", sVehReg)
                            End If
                            oForm.Freeze(False)
                        ElseIf pVal.ItemUID = "LINESGRID" Then
                            oForm.Freeze(True)
                            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                            If pVal.Row >= 0 AndAlso oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                                Dim oSelected As ArrayList
                                oSelected = oGridN.getDataTableSelection()

                                Dim oRows As New ArrayList
                                Dim iRow As Integer = pVal.Row
                                Dim iClkRow As Integer = oGridN.getSBOGrid().GetDataTableRowIndex(iRow)

                                Dim sTVehReg As String
                                Dim sRDate As String
                                Dim sSDate As String
                                Dim sEDate As String
                                Dim sRowCode As String
                                Dim bRowInSelection As Boolean = False

                                If Not oSelected Is Nothing Then
                                    For iIndex As Integer = 0 To oSelected.Count - 1
                                        Dim oRow As New ArrayList
                                        iRow = oSelected.Item(iIndex)

                                        sTVehReg = oGridN.doGetFieldValue("r.U_Lorry", iRow)
                                        sRDate = goParent.doDateToStr(oGridN.doGetFieldValue("r.U_RDate", iRow), True)
                                        sSDate = goParent.doDateToStr(oGridN.doGetFieldValue("r.U_ASDate", iRow), True)
                                        sEDate = goParent.doDateToStr(oGridN.doGetFieldValue("r.U_AEDate", iRow), True)
                                        sRowCode = oGridN.doGetFieldValue("r.Code", iRow)

                                        oRow.Add(sTVehReg)
                                        oRow.Add(sRDate)
                                        oRow.Add(sSDate)
                                        oRow.Add(sEDate)
                                        oRow.Add(iRow)
                                        oRow.Add(sRowCode)

                                        oRows.Add(oRow)

                                        If iRow = iClkRow Then
                                            bRowInSelection = True
                                        End If
                                    Next
                                End If

                                If bRowInSelection = False Then
                                    '** deselect previous and select the current **'
                                    If Not pVal.Modifiers = SAPbouiCOM.BoModifiersEnum.mt_SHIFT AndAlso _
                                     oRows.Count() > 0 Then

                                        Dim oSelectedR As SAPbouiCOM.SelectedRows
                                        oSelectedR = oGridN.getSBOGrid.Rows.SelectedRows()
                                        oSelectedR.Clear()
                                        oSelectedR.Add(iRow)
                                    End If

                                    oRows.Clear()
                                    Dim oRow As New ArrayList

                                    sTVehReg = oGridN.doGetFieldValue("r.U_Lorry", iClkRow)
                                    sRDate = goParent.doDateToStr(oGridN.doGetFieldValue("r.U_RDate", iClkRow), True)
                                    sSDate = goParent.doDateToStr(oGridN.doGetFieldValue("r.U_ASDate", iClkRow), True)
                                    sEDate = goParent.doDateToStr(oGridN.doGetFieldValue("r.U_AEDate", iClkRow), True)

                                    oRow.Add(sTVehReg)
                                    oRow.Add(sRDate)
                                    oRow.Add(sSDate)
                                    oRow.Add(sEDate)
                                    oRow.Add(iClkRow) 'oData.Add(oGridN.getCurrentLine())

                                    oRows.Add(oRow)
                                End If

                                setWFValue(oForm, "JobData", oRows)
                            End If
                            oForm.Freeze(False)
                        End If
                    Else
                    End If
                Catch ex As Exception
                    'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error doing the item event - " & pVal.ItemUID & "->" & pVal.EventType)
                    com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXITME", {pVal.ItemUID, pVal.EventType})
                End Try
            ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
                If pVal.BeforeAction = True Then
                    If pVal.ItemUID = "VEHGRID" Then
                        oForm.Freeze(True)
                        oForm.Freeze(False)
                    End If
                Else
                    If pVal.ItemUID = "IDH_VEHRF" Then
                        doReloadVehicle(oForm)
                    ElseIf pVal.ItemUID = "VEHGRID" Then
                        oForm.Freeze(True)
                        Dim oRows As ArrayList = getWFValue(oForm, "JobData")
                        If Not oRows Is Nothing Then
                            Dim sVehReg As String = Nothing
                            oForm.Freeze(True)
                            Try
                                Dim oVehicleGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "VEHGRID")
                                If pVal.Row >= 0 AndAlso oVehicleGrid.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                                    oVehicleGrid.setCurrentLineByClick(pVal.Row)
                                    sVehReg = oVehicleGrid.doGetFieldValue("VehReg")

                                    Dim oRow As ArrayList
                                    For iIndex As Integer = 0 To oRows.Count - 1
                                        oRow = oRows.Item(iIndex)

                                        If sVehReg Is Nothing OrElse sVehReg.Equals(oRow.Item(0)) = False Then
                                            Dim iRow As Integer = oRow.Item(4)
                                            Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")

                                            oGridN.doSetFieldValue("r.U_Lorry", iRow, sVehReg)
                                            doAddVehicleToCap(oForm, sVehReg, oRow.Item(1), oRow.Item(2), oRow.Item(3))
                                        End If
                                    Next

                                End If
                            Catch ex As Exception
                                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the registration..")
                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSREG", {Nothing})
                            End Try
                            oForm.Freeze(False)
                        End If
                        oForm.Freeze(False)
                    ElseIf pVal.ItemUID = "LINESGRID" Then
                        oForm.Freeze(True)
                        Dim sVehReg As String = getWFValue(oForm, "VehData")
                        If Not sVehReg Is Nothing Then
                            oForm.Freeze(True)
                            Try
                                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                                If pVal.Row >= 0 AndAlso oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                                    oGridN.setCurrentLineByClick(pVal.Row)
                                    Dim sTVehReg As String = oGridN.doGetFieldValue("r.U_Lorry")
                                    If sTVehReg Is Nothing OrElse sTVehReg.Equals(sVehReg) = False Then
                                        oGridN.doSetFieldValue("r.U_Lorry", sVehReg)

                                        Dim sRDate As String = goParent.doDateToStr(oGridN.doGetFieldValue("r.U_RDate"), True)
                                        Dim sSDate As String = goParent.doDateToStr(oGridN.doGetFieldValue("r.U_ASDate"), True)
                                        Dim sEDate As String = goParent.doDateToStr(oGridN.doGetFieldValue("r.U_AEDate"), True)
                                        doAddVehicleToCap(oForm, sVehReg, sRDate, sSDate, sEDate)
                                    End If
                                End If
                            Catch ex As Exception
                                'com.idh.bridge.DataHandler.INSTANCE.doError("Exception: " & ex.ToString, "Error setting the registration..")
                                com.idh.bridge.DataHandler.INSTANCE.doResExceptionError(ex.Message, "EREXSREG", {Nothing})
                            End Try
                            oForm.Freeze(False)
                        Else
                            If getUFValue(oForm, "IDH_AFOJR").Equals("Y") Then
                                Dim oGridN As UpdateGrid = UpdateGrid.getInstance(oForm, "LINESGRID")
                                If oGridN.getSBOGrid.Rows.IsLeaf(pVal.Row) = True Then
                                    oGridN.setCurrentLineByClick(pVal.Row)
                                    Dim sRoute As String = oGridN.doGetFieldValue("e.U_Route")
                                    If Not sRoute Is Nothing AndAlso sRoute.Length > 0 Then
                                        setUFValue(oForm, "IDH_VEHRT", sRoute)
                                    Else
                                        setUFValue(oForm, "IDH_VEHRT", "")
                                    End If
                                    doReloadVehicle(oForm)
                                End If
                            End If
                        End If
                        oForm.Freeze(False)
                    End If
                    setWFValue(oForm, "JobData", Nothing)
                    setWFValue(oForm, "VehData", Nothing)
                End If
            End If
            Return True
        End Function

        Private Sub doAddVehicleToCap(ByVal oForm As SAPbouiCOM.Form, ByVal sReg As String, ByVal sRDate As String, ByVal sSDate As String, ByVal sEDate As String)
            If Not sEDate Is Nothing AndAlso sEDate.Length > 0 Then
                Exit Sub
            End If
            Dim oVehicleGrid As UpdateGrid = UpdateGrid.getInstance(oForm, "VEHGRID")
            Dim sTVehReg As String
            Dim sTRDate As String
            Dim iTDJ As Integer
            Dim iTWJ As Integer
            Dim iTBJ As Integer
            Dim iCap As Integer = 1
            Dim sImg As String

            Dim iIndex As Integer
            Dim bFound As Boolean = False

            Dim bVehFound As Boolean = False
            For iIndex = 0 To oVehicleGrid.getRowCount() - 1
                sTVehReg = oVehicleGrid.doGetFieldValue("VehReg", iIndex)
                If Not sTVehReg Is Nothing AndAlso sTVehReg.Equals(sReg) Then
                    bVehFound = True
                    iCap = oVehicleGrid.doGetFieldValue("DayCap", iIndex)

                    sTRDate = goParent.doDateToStr(oVehicleGrid.doGetFieldValue("ReqDate", iIndex))
                    If Not sTRDate Is Nothing AndAlso sTRDate.Equals(sRDate) Then
                        iTDJ = oVehicleGrid.doGetFieldValue("Total", iIndex)
                        iTWJ = oVehicleGrid.doGetFieldValue("NotStarted", iIndex)
                        iTBJ = oVehicleGrid.doGetFieldValue("Started", iIndex)

                        iTDJ += 1
                        oVehicleGrid.doSetFieldValue("Total", iIndex, iTDJ)

                        If Not sSDate Is Nothing AndAlso sSDate.Length > 0 Then
                            oVehicleGrid.doSetFieldValue("Started", iIndex, iTBJ + 1)
                        Else
                            oVehicleGrid.doSetFieldValue("NotStarted", iIndex, iTWJ + 1)
                        End If

                        Dim sPerc As String = Config.Parameter("VSFULLW")
                        If sPerc Is Nothing OrElse sPerc.Length = 0 Then
                            sPerc = "50"
                        End If

                        Dim dPerc As Double = ToDouble(sPerc)
                        Dim sFull, sNearly, sAvailable As String
                        If Config.ParameterWithDefault("IMGPATH", "").Trim = "" Then
                            sFull = IDHAddOns.idh.addon.Base.doFixImagePath("full.png")
                            sNearly = IDHAddOns.idh.addon.Base.doFixImagePath("nearly.png")
                            sAvailable = IDHAddOns.idh.addon.Base.doFixImagePath("available.png")

                        Else
                            Dim sImgPath As String = Config.ParameterWithDefault("IMGPATH", "").Trim
                            If Not sImgPath.EndsWith("\") Then
                                sImgPath &= "\"
                            End If
                            sFull = sImgPath & ("full.png")
                            sNearly = sImgPath & ("nearly.png")
                            sAvailable = sImgPath & ("available.png")
                        End If
                        If iTDJ >= iCap Then
                            sImg = sFull
                        ElseIf iTDJ > (iCap * dPerc / 100) Then
                            sImg = sNearly
                        Else
                            sImg = sAvailable
                        End If
                        oVehicleGrid.doSetFieldValue("CAP", iIndex, sImg)

                        bFound = True
                        Exit For
                    End If
                Else
                    If bVehFound Then
                        If oVehicleGrid.getGridControl().getOrderString().StartsWith(" ORDER BY VehReg") Then
                            Exit For
                        End If
                    End If
                    bVehFound = False
                End If
            Next
            If bFound = False Then
                oVehicleGrid.doAddRow()
                iIndex = oVehicleGrid.getLastRowIndex()
                oVehicleGrid.doSetFieldValue("VehReg", iIndex, sReg)
                oVehicleGrid.doSetFieldValue("ReqDate", iIndex, com.idh.utils.dates.doStrToDate(sRDate))
                oVehicleGrid.doSetFieldValue("DayCap", iIndex, iCap)

                oVehicleGrid.doSetFieldValue("Total", iIndex, 1)
                If Not sSDate Is Nothing AndAlso sSDate.Length > 0 Then
                    oVehicleGrid.doSetFieldValue("Started", iIndex, 1)
                    oVehicleGrid.doSetFieldValue("NotStarted", iIndex, 0)
                Else
                    oVehicleGrid.doSetFieldValue("NotStarted", iIndex, 1)
                    oVehicleGrid.doSetFieldValue("Started", iIndex, 0)
                End If

                Dim sPerc As String = Config.Parameter("VSFULLW")
                If sPerc Is Nothing OrElse sPerc.Length = 0 Then
                    sPerc = "50"
                End If
                Dim dPerc As Double = ToDouble(sPerc)
                Dim sFull, sNearly, sAvailable As String
                If Config.ParameterWithDefault("IMGPATH", "").Trim = "" Then
                    sFull = IDHAddOns.idh.addon.Base.doFixImagePath("full.png")
                    sNearly = IDHAddOns.idh.addon.Base.doFixImagePath("nearly.png")
                    sAvailable = IDHAddOns.idh.addon.Base.doFixImagePath("available.png")

                Else
                    Dim sImgPath As String = Config.ParameterWithDefault("IMGPATH", "").Trim
                    If Not sImgPath.EndsWith("\") Then
                        sImgPath &= "\"
                    End If
                    sFull = sImgPath & ("full.png")
                    sNearly = sImgPath & ("nearly.png")
                    sAvailable = sImgPath & ("available.png")
                End If
                If 1 >= iCap Then
                    sImg = sFull
                ElseIf 1 > (iCap * dPerc / 100) Then
                    sImg = sNearly
                Else
                    sImg = sAvailable
                End If
                oVehicleGrid.doSetFieldValue("CAP", iIndex, sImg)

            End If
            doSetUpdate(oForm)
        End Sub
    End Class
End Namespace
